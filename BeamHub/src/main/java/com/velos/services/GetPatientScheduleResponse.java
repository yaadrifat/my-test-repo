
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getPatientScheduleResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getPatientScheduleResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PatientSchedule" type="{http://velos.com/services/}patientSchedule" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getPatientScheduleResponse", propOrder = {
    "patientSchedule"
})
public class GetPatientScheduleResponse {

    @XmlElement(name = "PatientSchedule")
    protected PatientSchedule patientSchedule;

    /**
     * Gets the value of the patientSchedule property.
     * 
     * @return
     *     possible object is
     *     {@link PatientSchedule }
     *     
     */
    public PatientSchedule getPatientSchedule() {
        return patientSchedule;
    }

    /**
     * Sets the value of the patientSchedule property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientSchedule }
     *     
     */
    public void setPatientSchedule(PatientSchedule value) {
        this.patientSchedule = value;
    }

}
