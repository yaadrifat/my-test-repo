
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for eventNameIdentifier complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="eventNameIdentifier">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}eventIdentifier">
 *       &lt;sequence>
 *         &lt;element name="eventCostIdentifiers" type="{http://velos.com/services/}eventCostIdentifiers" minOccurs="0"/>
 *         &lt;element name="eventName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="parentOID" type="{http://velos.com/services/}simpleIdentifier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "eventNameIdentifier", propOrder = {
    "eventCostIdentifiers",
    "eventName",
    "parentOID"
})
public class EventNameIdentifier
    extends EventIdentifier
{

    protected EventCostIdentifiers eventCostIdentifiers;
    protected String eventName;
    protected SimpleIdentifier parentOID;

    /**
     * Gets the value of the eventCostIdentifiers property.
     * 
     * @return
     *     possible object is
     *     {@link EventCostIdentifiers }
     *     
     */
    public EventCostIdentifiers getEventCostIdentifiers() {
        return eventCostIdentifiers;
    }

    /**
     * Sets the value of the eventCostIdentifiers property.
     * 
     * @param value
     *     allowed object is
     *     {@link EventCostIdentifiers }
     *     
     */
    public void setEventCostIdentifiers(EventCostIdentifiers value) {
        this.eventCostIdentifiers = value;
    }

    /**
     * Gets the value of the eventName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventName() {
        return eventName;
    }

    /**
     * Sets the value of the eventName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventName(String value) {
        this.eventName = value;
    }

    /**
     * Gets the value of the parentOID property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleIdentifier }
     *     
     */
    public SimpleIdentifier getParentOID() {
        return parentOID;
    }

    /**
     * Sets the value of the parentOID property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleIdentifier }
     *     
     */
    public void setParentOID(SimpleIdentifier value) {
        this.parentOID = value;
    }

}
