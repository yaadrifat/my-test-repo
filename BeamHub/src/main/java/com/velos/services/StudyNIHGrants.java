
package com.velos.services;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for studyNIHGrants complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="studyNIHGrants">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="studyNIHGrantInfo" type="{http://velos.com/services/}studyNIHGrantInfo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "studyNIHGrants", propOrder = {
    "studyNIHGrantInfo"
})
public class StudyNIHGrants
    extends ServiceObject
{

    @XmlElement(nillable = true)
    protected List<StudyNIHGrantInfo> studyNIHGrantInfo;

    /**
     * Gets the value of the studyNIHGrantInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the studyNIHGrantInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStudyNIHGrantInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StudyNIHGrantInfo }
     * 
     * 
     */
    public List<StudyNIHGrantInfo> getStudyNIHGrantInfo() {
        if (studyNIHGrantInfo == null) {
            studyNIHGrantInfo = new ArrayList<StudyNIHGrantInfo>();
        }
        return this.studyNIHGrantInfo;
    }

}
