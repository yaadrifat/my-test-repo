
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for budgetDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="budgetDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="bgtTemplate" type="{http://velos.com/services/}bgtTemplate" minOccurs="0"/>
 *         &lt;element name="budgetIdentifier" type="{http://velos.com/services/}budgetIdentifier" minOccurs="0"/>
 *         &lt;element name="budgetInfo" type="{http://velos.com/services/}budgetPojo" minOccurs="0"/>
 *         &lt;element name="calendarIdentifier" type="{http://velos.com/services/}calendarNameIdentifier" minOccurs="0"/>
 *         &lt;element name="studyIdentifier" type="{http://velos.com/services/}studyIdentifier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "budgetDetail", propOrder = {
    "bgtTemplate",
    "budgetIdentifier",
    "budgetInfo",
    "calendarIdentifier",
    "studyIdentifier"
})
public class BudgetDetail {

    protected BgtTemplate bgtTemplate;
    protected BudgetIdentifier budgetIdentifier;
    protected BudgetPojo budgetInfo;
    protected CalendarNameIdentifier calendarIdentifier;
    protected StudyIdentifier studyIdentifier;

    /**
     * Gets the value of the bgtTemplate property.
     * 
     * @return
     *     possible object is
     *     {@link BgtTemplate }
     *     
     */
    public BgtTemplate getBgtTemplate() {
        return bgtTemplate;
    }

    /**
     * Sets the value of the bgtTemplate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BgtTemplate }
     *     
     */
    public void setBgtTemplate(BgtTemplate value) {
        this.bgtTemplate = value;
    }

    /**
     * Gets the value of the budgetIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link BudgetIdentifier }
     *     
     */
    public BudgetIdentifier getBudgetIdentifier() {
        return budgetIdentifier;
    }

    /**
     * Sets the value of the budgetIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link BudgetIdentifier }
     *     
     */
    public void setBudgetIdentifier(BudgetIdentifier value) {
        this.budgetIdentifier = value;
    }

    /**
     * Gets the value of the budgetInfo property.
     * 
     * @return
     *     possible object is
     *     {@link BudgetPojo }
     *     
     */
    public BudgetPojo getBudgetInfo() {
        return budgetInfo;
    }

    /**
     * Sets the value of the budgetInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BudgetPojo }
     *     
     */
    public void setBudgetInfo(BudgetPojo value) {
        this.budgetInfo = value;
    }

    /**
     * Gets the value of the calendarIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link CalendarNameIdentifier }
     *     
     */
    public CalendarNameIdentifier getCalendarIdentifier() {
        return calendarIdentifier;
    }

    /**
     * Sets the value of the calendarIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link CalendarNameIdentifier }
     *     
     */
    public void setCalendarIdentifier(CalendarNameIdentifier value) {
        this.calendarIdentifier = value;
    }

    /**
     * Gets the value of the studyIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link StudyIdentifier }
     *     
     */
    public StudyIdentifier getStudyIdentifier() {
        return studyIdentifier;
    }

    /**
     * Sets the value of the studyIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyIdentifier }
     *     
     */
    public void setStudyIdentifier(StudyIdentifier value) {
        this.studyIdentifier = value;
    }

}
