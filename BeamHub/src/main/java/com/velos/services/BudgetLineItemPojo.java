
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for budgetLineItemPojo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="budgetLineItemPojo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="eventNameIdent" type="{http://velos.com/services/}eventNameIdentfier" minOccurs="0"/>
 *         &lt;element name="LINEITEM_APPLYINDIRECTS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LINEITEM_APPLYINFUTURE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LINEITEM_APPLYPATIENTCOUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LINEITEM_CDM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LINEITEM_CLINICNOFUNIT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LINEITEM_CPTCODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LINEITEM_DELFLAG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LINEITEM_DESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LINEITEM_INCOSTDISC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LINEITEM_INPERSEC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LINEITEM_INVCOST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LINEITEM_NOTES" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LINEITEM_OTHERCOST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LINEITEM_PARENTID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LINEITEM_REPEAT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LINEITEM_RESCOST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LINEITEM_SEQ" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LINEITEM_SPONSORAMOUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LINEITEM_SPONSORUNIT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LINEITEM_STDCARECOST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LINEITEM_TMID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LINEITEM_TOTALCOST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LINEITEM_VARIANCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lineItemIdent" type="{http://velos.com/services/}lineItemNameIdentifier" minOccurs="0"/>
 *         &lt;element name="PK_CODELST_CATEGORY" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="PK_CODELST_COST_TYPE" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="SUBCOST_ITEM_FLAG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "budgetLineItemPojo", propOrder = {
    "eventNameIdent",
    "lineitemapplyindirects",
    "lineitemapplyinfuture",
    "lineitemapplypatientcount",
    "lineitemcdm",
    "lineitemclinicnofunit",
    "lineitemcptcode",
    "lineitemdelflag",
    "lineitemdesc",
    "lineitemincostdisc",
    "lineiteminpersec",
    "lineiteminvcost",
    "lineitemnotes",
    "lineitemothercost",
    "lineitemparentid",
    "lineitemrepeat",
    "lineitemrescost",
    "lineitemseq",
    "lineitemsponsoramount",
    "lineitemsponsorunit",
    "lineitemstdcarecost",
    "lineitemtmid",
    "lineitemtotalcost",
    "lineitemvariance",
    "lineItemIdent",
    "pkcodelstcategory",
    "pkcodelstcosttype",
    "subcostitemflag"
})
public class BudgetLineItemPojo {

    protected EventNameIdentfier eventNameIdent;
    @XmlElement(name = "LINEITEM_APPLYINDIRECTS")
    protected String lineitemapplyindirects;
    @XmlElement(name = "LINEITEM_APPLYINFUTURE")
    protected String lineitemapplyinfuture;
    @XmlElement(name = "LINEITEM_APPLYPATIENTCOUNT")
    protected String lineitemapplypatientcount;
    @XmlElement(name = "LINEITEM_CDM")
    protected String lineitemcdm;
    @XmlElement(name = "LINEITEM_CLINICNOFUNIT")
    protected String lineitemclinicnofunit;
    @XmlElement(name = "LINEITEM_CPTCODE")
    protected String lineitemcptcode;
    @XmlElement(name = "LINEITEM_DELFLAG")
    protected String lineitemdelflag;
    @XmlElement(name = "LINEITEM_DESC")
    protected String lineitemdesc;
    @XmlElement(name = "LINEITEM_INCOSTDISC")
    protected String lineitemincostdisc;
    @XmlElement(name = "LINEITEM_INPERSEC")
    protected String lineiteminpersec;
    @XmlElement(name = "LINEITEM_INVCOST")
    protected String lineiteminvcost;
    @XmlElement(name = "LINEITEM_NOTES")
    protected String lineitemnotes;
    @XmlElement(name = "LINEITEM_OTHERCOST")
    protected String lineitemothercost;
    @XmlElement(name = "LINEITEM_PARENTID")
    protected String lineitemparentid;
    @XmlElement(name = "LINEITEM_REPEAT")
    protected String lineitemrepeat;
    @XmlElement(name = "LINEITEM_RESCOST")
    protected String lineitemrescost;
    @XmlElement(name = "LINEITEM_SEQ")
    protected String lineitemseq;
    @XmlElement(name = "LINEITEM_SPONSORAMOUNT")
    protected String lineitemsponsoramount;
    @XmlElement(name = "LINEITEM_SPONSORUNIT")
    protected String lineitemsponsorunit;
    @XmlElement(name = "LINEITEM_STDCARECOST")
    protected String lineitemstdcarecost;
    @XmlElement(name = "LINEITEM_TMID")
    protected String lineitemtmid;
    @XmlElement(name = "LINEITEM_TOTALCOST")
    protected String lineitemtotalcost;
    @XmlElement(name = "LINEITEM_VARIANCE")
    protected String lineitemvariance;
    protected LineItemNameIdentifier lineItemIdent;
    @XmlElement(name = "PK_CODELST_CATEGORY")
    protected Code pkcodelstcategory;
    @XmlElement(name = "PK_CODELST_COST_TYPE")
    protected Code pkcodelstcosttype;
    @XmlElement(name = "SUBCOST_ITEM_FLAG")
    protected String subcostitemflag;

    /**
     * Gets the value of the eventNameIdent property.
     * 
     * @return
     *     possible object is
     *     {@link EventNameIdentfier }
     *     
     */
    public EventNameIdentfier getEventNameIdent() {
        return eventNameIdent;
    }

    /**
     * Sets the value of the eventNameIdent property.
     * 
     * @param value
     *     allowed object is
     *     {@link EventNameIdentfier }
     *     
     */
    public void setEventNameIdent(EventNameIdentfier value) {
        this.eventNameIdent = value;
    }

    /**
     * Gets the value of the lineitemapplyindirects property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLINEITEMAPPLYINDIRECTS() {
        return lineitemapplyindirects;
    }

    /**
     * Sets the value of the lineitemapplyindirects property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLINEITEMAPPLYINDIRECTS(String value) {
        this.lineitemapplyindirects = value;
    }

    /**
     * Gets the value of the lineitemapplyinfuture property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLINEITEMAPPLYINFUTURE() {
        return lineitemapplyinfuture;
    }

    /**
     * Sets the value of the lineitemapplyinfuture property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLINEITEMAPPLYINFUTURE(String value) {
        this.lineitemapplyinfuture = value;
    }

    /**
     * Gets the value of the lineitemapplypatientcount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLINEITEMAPPLYPATIENTCOUNT() {
        return lineitemapplypatientcount;
    }

    /**
     * Sets the value of the lineitemapplypatientcount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLINEITEMAPPLYPATIENTCOUNT(String value) {
        this.lineitemapplypatientcount = value;
    }

    /**
     * Gets the value of the lineitemcdm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLINEITEMCDM() {
        return lineitemcdm;
    }

    /**
     * Sets the value of the lineitemcdm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLINEITEMCDM(String value) {
        this.lineitemcdm = value;
    }

    /**
     * Gets the value of the lineitemclinicnofunit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLINEITEMCLINICNOFUNIT() {
        return lineitemclinicnofunit;
    }

    /**
     * Sets the value of the lineitemclinicnofunit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLINEITEMCLINICNOFUNIT(String value) {
        this.lineitemclinicnofunit = value;
    }

    /**
     * Gets the value of the lineitemcptcode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLINEITEMCPTCODE() {
        return lineitemcptcode;
    }

    /**
     * Sets the value of the lineitemcptcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLINEITEMCPTCODE(String value) {
        this.lineitemcptcode = value;
    }

    /**
     * Gets the value of the lineitemdelflag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLINEITEMDELFLAG() {
        return lineitemdelflag;
    }

    /**
     * Sets the value of the lineitemdelflag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLINEITEMDELFLAG(String value) {
        this.lineitemdelflag = value;
    }

    /**
     * Gets the value of the lineitemdesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLINEITEMDESC() {
        return lineitemdesc;
    }

    /**
     * Sets the value of the lineitemdesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLINEITEMDESC(String value) {
        this.lineitemdesc = value;
    }

    /**
     * Gets the value of the lineitemincostdisc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLINEITEMINCOSTDISC() {
        return lineitemincostdisc;
    }

    /**
     * Sets the value of the lineitemincostdisc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLINEITEMINCOSTDISC(String value) {
        this.lineitemincostdisc = value;
    }

    /**
     * Gets the value of the lineiteminpersec property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLINEITEMINPERSEC() {
        return lineiteminpersec;
    }

    /**
     * Sets the value of the lineiteminpersec property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLINEITEMINPERSEC(String value) {
        this.lineiteminpersec = value;
    }

    /**
     * Gets the value of the lineiteminvcost property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLINEITEMINVCOST() {
        return lineiteminvcost;
    }

    /**
     * Sets the value of the lineiteminvcost property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLINEITEMINVCOST(String value) {
        this.lineiteminvcost = value;
    }

    /**
     * Gets the value of the lineitemnotes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLINEITEMNOTES() {
        return lineitemnotes;
    }

    /**
     * Sets the value of the lineitemnotes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLINEITEMNOTES(String value) {
        this.lineitemnotes = value;
    }

    /**
     * Gets the value of the lineitemothercost property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLINEITEMOTHERCOST() {
        return lineitemothercost;
    }

    /**
     * Sets the value of the lineitemothercost property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLINEITEMOTHERCOST(String value) {
        this.lineitemothercost = value;
    }

    /**
     * Gets the value of the lineitemparentid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLINEITEMPARENTID() {
        return lineitemparentid;
    }

    /**
     * Sets the value of the lineitemparentid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLINEITEMPARENTID(String value) {
        this.lineitemparentid = value;
    }

    /**
     * Gets the value of the lineitemrepeat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLINEITEMREPEAT() {
        return lineitemrepeat;
    }

    /**
     * Sets the value of the lineitemrepeat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLINEITEMREPEAT(String value) {
        this.lineitemrepeat = value;
    }

    /**
     * Gets the value of the lineitemrescost property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLINEITEMRESCOST() {
        return lineitemrescost;
    }

    /**
     * Sets the value of the lineitemrescost property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLINEITEMRESCOST(String value) {
        this.lineitemrescost = value;
    }

    /**
     * Gets the value of the lineitemseq property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLINEITEMSEQ() {
        return lineitemseq;
    }

    /**
     * Sets the value of the lineitemseq property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLINEITEMSEQ(String value) {
        this.lineitemseq = value;
    }

    /**
     * Gets the value of the lineitemsponsoramount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLINEITEMSPONSORAMOUNT() {
        return lineitemsponsoramount;
    }

    /**
     * Sets the value of the lineitemsponsoramount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLINEITEMSPONSORAMOUNT(String value) {
        this.lineitemsponsoramount = value;
    }

    /**
     * Gets the value of the lineitemsponsorunit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLINEITEMSPONSORUNIT() {
        return lineitemsponsorunit;
    }

    /**
     * Sets the value of the lineitemsponsorunit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLINEITEMSPONSORUNIT(String value) {
        this.lineitemsponsorunit = value;
    }

    /**
     * Gets the value of the lineitemstdcarecost property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLINEITEMSTDCARECOST() {
        return lineitemstdcarecost;
    }

    /**
     * Sets the value of the lineitemstdcarecost property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLINEITEMSTDCARECOST(String value) {
        this.lineitemstdcarecost = value;
    }

    /**
     * Gets the value of the lineitemtmid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLINEITEMTMID() {
        return lineitemtmid;
    }

    /**
     * Sets the value of the lineitemtmid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLINEITEMTMID(String value) {
        this.lineitemtmid = value;
    }

    /**
     * Gets the value of the lineitemtotalcost property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLINEITEMTOTALCOST() {
        return lineitemtotalcost;
    }

    /**
     * Sets the value of the lineitemtotalcost property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLINEITEMTOTALCOST(String value) {
        this.lineitemtotalcost = value;
    }

    /**
     * Gets the value of the lineitemvariance property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLINEITEMVARIANCE() {
        return lineitemvariance;
    }

    /**
     * Sets the value of the lineitemvariance property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLINEITEMVARIANCE(String value) {
        this.lineitemvariance = value;
    }

    /**
     * Gets the value of the lineItemIdent property.
     * 
     * @return
     *     possible object is
     *     {@link LineItemNameIdentifier }
     *     
     */
    public LineItemNameIdentifier getLineItemIdent() {
        return lineItemIdent;
    }

    /**
     * Sets the value of the lineItemIdent property.
     * 
     * @param value
     *     allowed object is
     *     {@link LineItemNameIdentifier }
     *     
     */
    public void setLineItemIdent(LineItemNameIdentifier value) {
        this.lineItemIdent = value;
    }

    /**
     * Gets the value of the pkcodelstcategory property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getPKCODELSTCATEGORY() {
        return pkcodelstcategory;
    }

    /**
     * Sets the value of the pkcodelstcategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setPKCODELSTCATEGORY(Code value) {
        this.pkcodelstcategory = value;
    }

    /**
     * Gets the value of the pkcodelstcosttype property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getPKCODELSTCOSTTYPE() {
        return pkcodelstcosttype;
    }

    /**
     * Sets the value of the pkcodelstcosttype property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setPKCODELSTCOSTTYPE(Code value) {
        this.pkcodelstcosttype = value;
    }

    /**
     * Gets the value of the subcostitemflag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSUBCOSTITEMFLAG() {
        return subcostitemflag;
    }

    /**
     * Sets the value of the subcostitemflag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSUBCOSTITEMFLAG(String value) {
        this.subcostitemflag = value;
    }

}
