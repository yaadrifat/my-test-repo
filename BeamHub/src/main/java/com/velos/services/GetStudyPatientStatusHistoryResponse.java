
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getStudyPatientStatusHistoryResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getStudyPatientStatusHistoryResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PatientStudyStatuses" type="{http://velos.com/services/}studyPatientStatuses" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getStudyPatientStatusHistoryResponse", propOrder = {
    "patientStudyStatuses"
})
public class GetStudyPatientStatusHistoryResponse {

    @XmlElement(name = "PatientStudyStatuses")
    protected StudyPatientStatuses patientStudyStatuses;

    /**
     * Gets the value of the patientStudyStatuses property.
     * 
     * @return
     *     possible object is
     *     {@link StudyPatientStatuses }
     *     
     */
    public StudyPatientStatuses getPatientStudyStatuses() {
        return patientStudyStatuses;
    }

    /**
     * Sets the value of the patientStudyStatuses property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyPatientStatuses }
     *     
     */
    public void setPatientStudyStatuses(StudyPatientStatuses value) {
        this.patientStudyStatuses = value;
    }

}
