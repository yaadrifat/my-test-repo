
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for budgetSectionPojo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="budgetSectionPojo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BGTSECTION_DELFLAG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BGTSECTION_NOTES" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BGTSECTION_PATNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BGTSECTION_PERSONLFLAG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BGTSECTION_SEQUENCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BGTSECTION_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BGTSECTION_VISIT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="bgtSectionName" type="{http://velos.com/services/}bgtSectionNameIdentifier" minOccurs="0"/>
 *         &lt;element name="lineItems" type="{http://velos.com/services/}bgtLiniItemDetail" minOccurs="0"/>
 *         &lt;element name="SOC_COST_GRANDTOTAL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SOC_COST_SPONSOR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SOC_COST_TOTAL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SOC_COST_VARIANCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SRT_COST_GRANDTOTAL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SRT_COST_SPONSOR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SRT_COST_TOTAL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SRT_COST_VARIANCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="visitIdent" type="{http://velos.com/services/}visitNameIdentifier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "budgetSectionPojo", propOrder = {
    "bgtsectiondelflag",
    "bgtsectionnotes",
    "bgtsectionpatno",
    "bgtsectionpersonlflag",
    "bgtsectionsequence",
    "bgtsectiontype",
    "bgtsectionvisit",
    "bgtSectionName",
    "lineItems",
    "soccostgrandtotal",
    "soccostsponsor",
    "soccosttotal",
    "soccostvariance",
    "srtcostgrandtotal",
    "srtcostsponsor",
    "srtcosttotal",
    "srtcostvariance",
    "visitIdent"
})
public class BudgetSectionPojo {

    @XmlElement(name = "BGTSECTION_DELFLAG")
    protected String bgtsectiondelflag;
    @XmlElement(name = "BGTSECTION_NOTES")
    protected String bgtsectionnotes;
    @XmlElement(name = "BGTSECTION_PATNO")
    protected String bgtsectionpatno;
    @XmlElement(name = "BGTSECTION_PERSONLFLAG")
    protected String bgtsectionpersonlflag;
    @XmlElement(name = "BGTSECTION_SEQUENCE")
    protected String bgtsectionsequence;
    @XmlElement(name = "BGTSECTION_TYPE")
    protected String bgtsectiontype;
    @XmlElement(name = "BGTSECTION_VISIT")
    protected String bgtsectionvisit;
    protected BgtSectionNameIdentifier bgtSectionName;
    protected BgtLiniItemDetail lineItems;
    @XmlElement(name = "SOC_COST_GRANDTOTAL")
    protected String soccostgrandtotal;
    @XmlElement(name = "SOC_COST_SPONSOR")
    protected String soccostsponsor;
    @XmlElement(name = "SOC_COST_TOTAL")
    protected String soccosttotal;
    @XmlElement(name = "SOC_COST_VARIANCE")
    protected String soccostvariance;
    @XmlElement(name = "SRT_COST_GRANDTOTAL")
    protected String srtcostgrandtotal;
    @XmlElement(name = "SRT_COST_SPONSOR")
    protected String srtcostsponsor;
    @XmlElement(name = "SRT_COST_TOTAL")
    protected String srtcosttotal;
    @XmlElement(name = "SRT_COST_VARIANCE")
    protected String srtcostvariance;
    protected VisitNameIdentifier visitIdent;

    /**
     * Gets the value of the bgtsectiondelflag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBGTSECTIONDELFLAG() {
        return bgtsectiondelflag;
    }

    /**
     * Sets the value of the bgtsectiondelflag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBGTSECTIONDELFLAG(String value) {
        this.bgtsectiondelflag = value;
    }

    /**
     * Gets the value of the bgtsectionnotes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBGTSECTIONNOTES() {
        return bgtsectionnotes;
    }

    /**
     * Sets the value of the bgtsectionnotes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBGTSECTIONNOTES(String value) {
        this.bgtsectionnotes = value;
    }

    /**
     * Gets the value of the bgtsectionpatno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBGTSECTIONPATNO() {
        return bgtsectionpatno;
    }

    /**
     * Sets the value of the bgtsectionpatno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBGTSECTIONPATNO(String value) {
        this.bgtsectionpatno = value;
    }

    /**
     * Gets the value of the bgtsectionpersonlflag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBGTSECTIONPERSONLFLAG() {
        return bgtsectionpersonlflag;
    }

    /**
     * Sets the value of the bgtsectionpersonlflag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBGTSECTIONPERSONLFLAG(String value) {
        this.bgtsectionpersonlflag = value;
    }

    /**
     * Gets the value of the bgtsectionsequence property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBGTSECTIONSEQUENCE() {
        return bgtsectionsequence;
    }

    /**
     * Sets the value of the bgtsectionsequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBGTSECTIONSEQUENCE(String value) {
        this.bgtsectionsequence = value;
    }

    /**
     * Gets the value of the bgtsectiontype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBGTSECTIONTYPE() {
        return bgtsectiontype;
    }

    /**
     * Sets the value of the bgtsectiontype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBGTSECTIONTYPE(String value) {
        this.bgtsectiontype = value;
    }

    /**
     * Gets the value of the bgtsectionvisit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBGTSECTIONVISIT() {
        return bgtsectionvisit;
    }

    /**
     * Sets the value of the bgtsectionvisit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBGTSECTIONVISIT(String value) {
        this.bgtsectionvisit = value;
    }

    /**
     * Gets the value of the bgtSectionName property.
     * 
     * @return
     *     possible object is
     *     {@link BgtSectionNameIdentifier }
     *     
     */
    public BgtSectionNameIdentifier getBgtSectionName() {
        return bgtSectionName;
    }

    /**
     * Sets the value of the bgtSectionName property.
     * 
     * @param value
     *     allowed object is
     *     {@link BgtSectionNameIdentifier }
     *     
     */
    public void setBgtSectionName(BgtSectionNameIdentifier value) {
        this.bgtSectionName = value;
    }

    /**
     * Gets the value of the lineItems property.
     * 
     * @return
     *     possible object is
     *     {@link BgtLiniItemDetail }
     *     
     */
    public BgtLiniItemDetail getLineItems() {
        return lineItems;
    }

    /**
     * Sets the value of the lineItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link BgtLiniItemDetail }
     *     
     */
    public void setLineItems(BgtLiniItemDetail value) {
        this.lineItems = value;
    }

    /**
     * Gets the value of the soccostgrandtotal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSOCCOSTGRANDTOTAL() {
        return soccostgrandtotal;
    }

    /**
     * Sets the value of the soccostgrandtotal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSOCCOSTGRANDTOTAL(String value) {
        this.soccostgrandtotal = value;
    }

    /**
     * Gets the value of the soccostsponsor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSOCCOSTSPONSOR() {
        return soccostsponsor;
    }

    /**
     * Sets the value of the soccostsponsor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSOCCOSTSPONSOR(String value) {
        this.soccostsponsor = value;
    }

    /**
     * Gets the value of the soccosttotal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSOCCOSTTOTAL() {
        return soccosttotal;
    }

    /**
     * Sets the value of the soccosttotal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSOCCOSTTOTAL(String value) {
        this.soccosttotal = value;
    }

    /**
     * Gets the value of the soccostvariance property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSOCCOSTVARIANCE() {
        return soccostvariance;
    }

    /**
     * Sets the value of the soccostvariance property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSOCCOSTVARIANCE(String value) {
        this.soccostvariance = value;
    }

    /**
     * Gets the value of the srtcostgrandtotal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSRTCOSTGRANDTOTAL() {
        return srtcostgrandtotal;
    }

    /**
     * Sets the value of the srtcostgrandtotal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSRTCOSTGRANDTOTAL(String value) {
        this.srtcostgrandtotal = value;
    }

    /**
     * Gets the value of the srtcostsponsor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSRTCOSTSPONSOR() {
        return srtcostsponsor;
    }

    /**
     * Sets the value of the srtcostsponsor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSRTCOSTSPONSOR(String value) {
        this.srtcostsponsor = value;
    }

    /**
     * Gets the value of the srtcosttotal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSRTCOSTTOTAL() {
        return srtcosttotal;
    }

    /**
     * Sets the value of the srtcosttotal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSRTCOSTTOTAL(String value) {
        this.srtcosttotal = value;
    }

    /**
     * Gets the value of the srtcostvariance property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSRTCOSTVARIANCE() {
        return srtcostvariance;
    }

    /**
     * Sets the value of the srtcostvariance property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSRTCOSTVARIANCE(String value) {
        this.srtcostvariance = value;
    }

    /**
     * Gets the value of the visitIdent property.
     * 
     * @return
     *     possible object is
     *     {@link VisitNameIdentifier }
     *     
     */
    public VisitNameIdentifier getVisitIdent() {
        return visitIdent;
    }

    /**
     * Sets the value of the visitIdent property.
     * 
     * @param value
     *     allowed object is
     *     {@link VisitNameIdentifier }
     *     
     */
    public void setVisitIdent(VisitNameIdentifier value) {
        this.visitIdent = value;
    }

}
