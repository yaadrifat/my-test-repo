
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for searchLibraryEventsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="searchLibraryEventsResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Library" type="{http://velos.com/services/}libraryEvents" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "searchLibraryEventsResponse", propOrder = {
    "library"
})
public class SearchLibraryEventsResponse {

    @XmlElement(name = "Library")
    protected LibraryEvents library;

    /**
     * Gets the value of the library property.
     * 
     * @return
     *     possible object is
     *     {@link LibraryEvents }
     *     
     */
    public LibraryEvents getLibrary() {
        return library;
    }

    /**
     * Sets the value of the library property.
     * 
     * @param value
     *     allowed object is
     *     {@link LibraryEvents }
     *     
     */
    public void setLibrary(LibraryEvents value) {
        this.library = value;
    }

}
