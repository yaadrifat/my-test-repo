
package com.velos.services;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for studyINDIDEs complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="studyINDIDEs">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="studyINDIDEInfo" type="{http://velos.com/services/}studyINDIDEInfo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "studyINDIDEs", propOrder = {
    "studyINDIDEInfo"
})
public class StudyINDIDEs
    extends ServiceObject
{

    @XmlElement(nillable = true)
    protected List<StudyINDIDEInfo> studyINDIDEInfo;

    /**
     * Gets the value of the studyINDIDEInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the studyINDIDEInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStudyINDIDEInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StudyINDIDEInfo }
     * 
     * 
     */
    public List<StudyINDIDEInfo> getStudyINDIDEInfo() {
        if (studyINDIDEInfo == null) {
            studyINDIDEInfo = new ArrayList<StudyINDIDEInfo>();
        }
        return this.studyINDIDEInfo;
    }

}
