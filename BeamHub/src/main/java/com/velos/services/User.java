
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for user complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="user">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}nonSystemUser">
 *       &lt;sequence>
 *         &lt;element name="PK" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="sendNotifaction" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="timeZoneId" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="userDefaultGroup" type="{http://velos.com/services/}groupIdentifier" minOccurs="0"/>
 *         &lt;element name="userESign" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="userLoginName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="userPassword" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="userSecurityAnswer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="userSecurityQuestion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="userTrialPhaseInvolvement" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="userWorkExperience" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "user", propOrder = {
    "pk",
    "sendNotifaction",
    "timeZoneId",
    "userDefaultGroup",
    "userESign",
    "userLoginName",
    "userPassword",
    "userSecurityAnswer",
    "userSecurityQuestion",
    "userTrialPhaseInvolvement",
    "userWorkExperience"
})
public class User
    extends NonSystemUser
{

    @XmlElement(name = "PK")
    protected Integer pk;
    protected Boolean sendNotifaction;
    protected Code timeZoneId;
    protected GroupIdentifier userDefaultGroup;
    protected String userESign;
    protected String userLoginName;
    protected String userPassword;
    protected String userSecurityAnswer;
    protected String userSecurityQuestion;
    protected String userTrialPhaseInvolvement;
    protected String userWorkExperience;

    /**
     * Gets the value of the pk property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPK() {
        return pk;
    }

    /**
     * Sets the value of the pk property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPK(Integer value) {
        this.pk = value;
    }

    /**
     * Gets the value of the sendNotifaction property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSendNotifaction() {
        return sendNotifaction;
    }

    /**
     * Sets the value of the sendNotifaction property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSendNotifaction(Boolean value) {
        this.sendNotifaction = value;
    }

    /**
     * Gets the value of the timeZoneId property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getTimeZoneId() {
        return timeZoneId;
    }

    /**
     * Sets the value of the timeZoneId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setTimeZoneId(Code value) {
        this.timeZoneId = value;
    }

    /**
     * Gets the value of the userDefaultGroup property.
     * 
     * @return
     *     possible object is
     *     {@link GroupIdentifier }
     *     
     */
    public GroupIdentifier getUserDefaultGroup() {
        return userDefaultGroup;
    }

    /**
     * Sets the value of the userDefaultGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupIdentifier }
     *     
     */
    public void setUserDefaultGroup(GroupIdentifier value) {
        this.userDefaultGroup = value;
    }

    /**
     * Gets the value of the userESign property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserESign() {
        return userESign;
    }

    /**
     * Sets the value of the userESign property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserESign(String value) {
        this.userESign = value;
    }

    /**
     * Gets the value of the userLoginName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserLoginName() {
        return userLoginName;
    }

    /**
     * Sets the value of the userLoginName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserLoginName(String value) {
        this.userLoginName = value;
    }

    /**
     * Gets the value of the userPassword property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserPassword() {
        return userPassword;
    }

    /**
     * Sets the value of the userPassword property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserPassword(String value) {
        this.userPassword = value;
    }

    /**
     * Gets the value of the userSecurityAnswer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserSecurityAnswer() {
        return userSecurityAnswer;
    }

    /**
     * Sets the value of the userSecurityAnswer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserSecurityAnswer(String value) {
        this.userSecurityAnswer = value;
    }

    /**
     * Gets the value of the userSecurityQuestion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserSecurityQuestion() {
        return userSecurityQuestion;
    }

    /**
     * Sets the value of the userSecurityQuestion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserSecurityQuestion(String value) {
        this.userSecurityQuestion = value;
    }

    /**
     * Gets the value of the userTrialPhaseInvolvement property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserTrialPhaseInvolvement() {
        return userTrialPhaseInvolvement;
    }

    /**
     * Sets the value of the userTrialPhaseInvolvement property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserTrialPhaseInvolvement(String value) {
        this.userTrialPhaseInvolvement = value;
    }

    /**
     * Gets the value of the userWorkExperience property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserWorkExperience() {
        return userWorkExperience;
    }

    /**
     * Sets the value of the userWorkExperience property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserWorkExperience(String value) {
        this.userWorkExperience = value;
    }

}
