
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getMPatientSurvivalStatus complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getMPatientSurvivalStatus">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PatientIdentifiers" type="{http://velos.com/services/}patientIdentifiers" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getMPatientSurvivalStatus", propOrder = {
    "patientIdentifiers"
})
public class GetMPatientSurvivalStatus {

    @XmlElement(name = "PatientIdentifiers")
    protected PatientIdentifiers patientIdentifiers;

    /**
     * Gets the value of the patientIdentifiers property.
     * 
     * @return
     *     possible object is
     *     {@link PatientIdentifiers }
     *     
     */
    public PatientIdentifiers getPatientIdentifiers() {
        return patientIdentifiers;
    }

    /**
     * Sets the value of the patientIdentifiers property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientIdentifiers }
     *     
     */
    public void setPatientIdentifiers(PatientIdentifiers value) {
        this.patientIdentifiers = value;
    }

}
