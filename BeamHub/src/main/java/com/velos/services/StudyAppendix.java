
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for studyAppendix complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="studyAppendix">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="fileStream" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="STUDYAPNDX_DESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="STUDYAPNDX_FILE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="STUDYAPNDX_FILEOBJ" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="STUDYAPNDX_FILESIZE" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="STUDYAPNDX_PUBFLAG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="STUDYAPNDX_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="STUDYAPNDX_URI" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="studyApndxIdent" type="{http://velos.com/services/}studyApndxIdentifier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "studyAppendix", propOrder = {
    "fileStream",
    "studyapndxdesc",
    "studyapndxfile",
    "studyapndxfileobj",
    "studyapndxfilesize",
    "studyapndxpubflag",
    "studyapndxtype",
    "studyapndxuri",
    "studyApndxIdent"
})
public class StudyAppendix {

    protected byte[] fileStream;
    @XmlElement(name = "STUDYAPNDX_DESC")
    protected String studyapndxdesc;
    @XmlElement(name = "STUDYAPNDX_FILE")
    protected String studyapndxfile;
    @XmlElement(name = "STUDYAPNDX_FILEOBJ")
    protected byte[] studyapndxfileobj;
    @XmlElement(name = "STUDYAPNDX_FILESIZE")
    protected Integer studyapndxfilesize;
    @XmlElement(name = "STUDYAPNDX_PUBFLAG")
    protected String studyapndxpubflag;
    @XmlElement(name = "STUDYAPNDX_TYPE")
    protected String studyapndxtype;
    @XmlElement(name = "STUDYAPNDX_URI")
    protected String studyapndxuri;
    protected StudyApndxIdentifier studyApndxIdent;

    /**
     * Gets the value of the fileStream property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getFileStream() {
        return fileStream;
    }

    /**
     * Sets the value of the fileStream property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setFileStream(byte[] value) {
        this.fileStream = value;
    }

    /**
     * Gets the value of the studyapndxdesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTUDYAPNDXDESC() {
        return studyapndxdesc;
    }

    /**
     * Sets the value of the studyapndxdesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTUDYAPNDXDESC(String value) {
        this.studyapndxdesc = value;
    }

    /**
     * Gets the value of the studyapndxfile property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTUDYAPNDXFILE() {
        return studyapndxfile;
    }

    /**
     * Sets the value of the studyapndxfile property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTUDYAPNDXFILE(String value) {
        this.studyapndxfile = value;
    }

    /**
     * Gets the value of the studyapndxfileobj property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getSTUDYAPNDXFILEOBJ() {
        return studyapndxfileobj;
    }

    /**
     * Sets the value of the studyapndxfileobj property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setSTUDYAPNDXFILEOBJ(byte[] value) {
        this.studyapndxfileobj = value;
    }

    /**
     * Gets the value of the studyapndxfilesize property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSTUDYAPNDXFILESIZE() {
        return studyapndxfilesize;
    }

    /**
     * Sets the value of the studyapndxfilesize property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSTUDYAPNDXFILESIZE(Integer value) {
        this.studyapndxfilesize = value;
    }

    /**
     * Gets the value of the studyapndxpubflag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTUDYAPNDXPUBFLAG() {
        return studyapndxpubflag;
    }

    /**
     * Sets the value of the studyapndxpubflag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTUDYAPNDXPUBFLAG(String value) {
        this.studyapndxpubflag = value;
    }

    /**
     * Gets the value of the studyapndxtype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTUDYAPNDXTYPE() {
        return studyapndxtype;
    }

    /**
     * Sets the value of the studyapndxtype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTUDYAPNDXTYPE(String value) {
        this.studyapndxtype = value;
    }

    /**
     * Gets the value of the studyapndxuri property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTUDYAPNDXURI() {
        return studyapndxuri;
    }

    /**
     * Sets the value of the studyapndxuri property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTUDYAPNDXURI(String value) {
        this.studyapndxuri = value;
    }

    /**
     * Gets the value of the studyApndxIdent property.
     * 
     * @return
     *     possible object is
     *     {@link StudyApndxIdentifier }
     *     
     */
    public StudyApndxIdentifier getStudyApndxIdent() {
        return studyApndxIdent;
    }

    /**
     * Sets the value of the studyApndxIdent property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyApndxIdentifier }
     *     
     */
    public void setStudyApndxIdent(StudyApndxIdentifier value) {
        this.studyApndxIdent = value;
    }

}
