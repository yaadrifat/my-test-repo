package com.velos.integration.authentication;

import java.util.Properties;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import org.apache.ws.security.WSPasswordCallback;
import com.velos.integration.util.PropertiesUtil;

public class Site2AuthCallbackHandler implements CallbackHandler {

	private PropertiesUtil propertiesUtil;

	public PropertiesUtil getPropertiesUtil() {
		return propertiesUtil;
	}

	public void setPropertiesUtil(PropertiesUtil propertiesUtil) {
		this.propertiesUtil = propertiesUtil;
	}

	public void handle(Callback[] callbacks) {
		Properties prop = propertiesUtil.getAllProperties();
		WSPasswordCallback pc = (WSPasswordCallback) callbacks[0];
		if (pc.getIdentifier().equals(prop.getProperty("site2.userID"))) {
			pc.setPassword(prop.getProperty("site2.password"));
		}
	}
}
