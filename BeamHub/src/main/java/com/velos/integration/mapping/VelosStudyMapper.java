package com.velos.integration.mapping;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.log4j.Logger;

import com.velos.integration.util.CodelstsCache;
import com.velos.integration.util.StringToXMLGregorianCalendar;
import com.velos.services.Code;
import com.velos.services.Codes;
import com.velos.services.Duration;
import com.velos.services.GetStudySummaryResponse;
import com.velos.services.GroupIdentifier;
import com.velos.services.NvPair;
import com.velos.services.OrganizationIdentifier;
import com.velos.services.Study;
import com.velos.services.StudyAttachment;
import com.velos.services.StudyAttachments;
import com.velos.services.StudyINDIDEInfo;
import com.velos.services.StudyINDIDEs;
import com.velos.services.StudyIdentifier;
import com.velos.services.StudyIndIde;
import com.velos.services.StudyNIHGrantInfo;
import com.velos.services.StudyNIHGrants;
import com.velos.services.StudyOrganization;
import com.velos.services.StudyOrganizations;
import com.velos.services.StudyStatus;
import com.velos.services.StudyStatuses;
import com.velos.services.StudySummary;
import com.velos.services.StudyTeamMember;
import com.velos.services.StudyTeamMembers;
import com.velos.services.TimeUnits;
import com.velos.services.User;
import com.velos.services.UserIdentifier;
import com.velos.services.UserSearchResults;
import com.velos.services.UserType;

public class VelosStudyMapper {

	private static Logger logger = Logger.getLogger(VelosStudyMapper.class);
	private String endpoint = null;
	private Map<VelosKeys, Object> dataMap = null;
	Study mappedStudy = null;
	Properties prop = null;

	public VelosStudyMapper(String endpoint) {
		this.endpoint = endpoint;
		/*prop = new Properties();
		try {
			prop.load(this.getClass().getClassLoader()
					.getResourceAsStream("mapping.properties"));

			prop.load(this.getClass().getClassLoader()
					.getResourceAsStream("config.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}*/
	}

	public Map<VelosKeys, Object> mapStudySummary(GetStudySummaryResponse resp) {
		if (EndpointKeys.CTXpress.toString().equals(endpoint)) {
			mapStudySummaryForEpic(resp);
		}
		return dataMap;
	}

	private void mapStudySummaryForEpic(GetStudySummaryResponse resp) {
		mapStudySummaryForEpic(resp.getStudySummary());
	}

	public Map<VelosKeys, Object> mapStudySummary(StudySummary summary) {
		if (EndpointKeys.CTXpress.toString().equals(endpoint)) {
			mapStudySummaryForEpic(summary);
		}
		return dataMap;
	}

	private void mapStudySummaryForEpic(StudySummary summary) {
		dataMap = new HashMap<VelosKeys, Object>();
		dataMap.put(ProtocolKeys.Title,
				MapUtil.escapeSpecial(summary.getStudyTitle()));
		dataMap.put(ProtocolKeys.Text,
				MapUtil.escapeSpecial(summary.getSummaryText()));
		dataMap.put(ProtocolKeys.IdExtension,
				MapUtil.escapeSpecial(summary.getStudyNumber()));
		List<NvPair> moreList = summary.getMoreStudyDetails();
		for (NvPair more : moreList) {
			if (ProtocolKeys.IrbNumber.toString().equalsIgnoreCase(
					more.getKey())) {
				dataMap.put(ProtocolKeys.IrbNumber, more.getValue());
			} else if (ProtocolKeys.Irb.toString().equalsIgnoreCase(
					more.getKey())) {
				dataMap.put(ProtocolKeys.Irb, more.getValue());
			} else if (ProtocolKeys.NctNumber.toString().equalsIgnoreCase(
					more.getKey())) {
				dataMap.put(ProtocolKeys.NctNumber, more.getValue());
			} else if (ProtocolKeys.Nct.toString().equalsIgnoreCase(
					more.getKey())) {
				dataMap.put(ProtocolKeys.Nct, more.getValue());
			}
		}
	}

	public Map<VelosKeys, Object> mapStudy(Study study) {
		if (EndpointKeys.CTXpress.toString().equals(endpoint)) {
			mapStudyForEpic(study);
		}
		return dataMap;
	}

	private void mapStudyForEpic(Study study) {
		dataMap = new HashMap<VelosKeys, Object>();
		List<Map<VelosKeys, Object>> orgList = new ArrayList<Map<VelosKeys, Object>>();

		StudyOrganizations studyOrganizations = study.getStudyOrganizations();

		for (StudyOrganization studyOrganization : studyOrganizations
				.getStudyOrganizaton()) {
			Map<VelosKeys, Object> organizationNameMap = new HashMap<VelosKeys, Object>();
			OrganizationIdentifier organizationIdentifier = studyOrganization
					.getOrganizationIdentifier();
			String OrganizationName = organizationIdentifier.getSiteName();
		
			organizationNameMap.put(ProtocolKeys.OrganizationName,
					MapUtil.escapeSpecial(OrganizationName));
			orgList.add(organizationNameMap);
		}
		logger.info("OrgList = " + orgList);
		dataMap.put(ProtocolKeys.OrganizationList, orgList);

	}

	// Returned Map data for OrgList added to study
	public Map<VelosKeys, Object> mapStudyOrgList(Study study,Map<VelosKeys, Object> requestMap) {
		if (EndpointKeys.CTXpress.toString().equals(endpoint)) {
			mapForStudyOrgList(study,requestMap);
		}
		return dataMap;
	}

	private void mapForStudyOrgList(Study study,Map<VelosKeys, Object> requestMap) {
		dataMap = new HashMap<VelosKeys, Object>();
		List<String> orgList = new ArrayList<String>();

		StudyOrganizations studyOrganizations = study.getStudyOrganizations();

		for (StudyOrganization studyOrganization : studyOrganizations
				.getStudyOrganizaton()) {
			OrganizationIdentifier organizationIdentifier = studyOrganization
					.getOrganizationIdentifier();
			String OrganizationName = organizationIdentifier.getSiteName();
			orgList.add(OrganizationName);
		}
		logger.info("OrgList = " + orgList);
		
		String stdStatus = ((String)requestMap.get(ProtocolKeys.StudyStatus)).trim();
		
		StudyStatuses stdStatuses = study.getStudyStatuses();
		List<StudyStatus> statusList = stdStatuses.getStudyStatus();
		for(StudyStatus status : statusList){
			Code code = status.getStatus();
			if(code!=null){
				if(stdStatus.equals(code.getDescription())){
					if(status.isCurrentForStudy()){
						UserIdentifier statusUI = status.getDocumentedBy();
						if(statusUI!=null){
							String fName = statusUI.getFirstName();
							String lName = statusUI.getLastName();
							String loginName = statusUI.getUserLoginName();
							dataMap.put(ProtocolKeys.DocumentedByFirstName, fName);
							dataMap.put(ProtocolKeys.DocumentedByLastName, lName);
							dataMap.put(ProtocolKeys.DocumentedByLoginName,loginName);
						}
						break;
					}
				}
			}
		}
		
		dataMap.put(ProtocolKeys.StudyObj, study);
		dataMap.put(ProtocolKeys.OrganizationList, orgList);

	}

	// Returns study based on the mappings
	public Study getMappedOrgStudy(Study study, Map<VelosKeys, Object> requestMap,UserSearchResults userSearchResults) throws Exception {
		if (EndpointKeys.CTXpress.toString().equals(endpoint)) {
			getMappedStudyForOrg(study, requestMap,userSearchResults);
		}
		logger.info("Returns Mapped Study");
		return mappedStudy;
	}

	private void getMappedStudyForOrg(Study study,Map<VelosKeys, Object> requestMap,UserSearchResults userSearchResults) throws Exception {
		//dataMap = new HashMap<VelosKeys, Object>();
		
		prop = (Properties)requestMap.get(ProtocolKeys.PropObj);
		
		String orgName = ((String)requestMap.get(ProtocolKeys.OrganizationName)).trim();
    	logger.info("OrganizationName = "+orgName);
    	
    	String stdStatus = ((String)requestMap.get(ProtocolKeys.StudyStatus)).trim();
    	logger.info("Study Status Changed = "+stdStatus);

		logger.info("Getting Mapping values for study of org = " + orgName);

		mappedStudy = new Study();

		StudyIdentifier mappedStudyIdentifier = new StudyIdentifier();

		String studyNumber = null;
		String autoGenError = "StudyNumber Autogeneration not configured for participating site  "+orgName;
		StudyIdentifier studyIdentifier = study.getStudyIdentifier();
		if (studyIdentifier != null) {
			logger.info("Study Number ="+studyIdentifier.getStudyNumber());
			
			if (prop.containsKey(orgName + "_StudyAutoGeneration") && !"".equals(prop.getProperty(orgName + "_StudyAutoGeneration").trim())) {
				String studyAutoGen = prop.getProperty(orgName + "_StudyAutoGeneration").trim();
				if("Y".equalsIgnoreCase(studyAutoGen)){
					studyNumber = "";
					logger.info("Study Number ="+studyNumber);
					mappedStudyIdentifier.setStudyNumber(studyNumber);
				}else if("N".equalsIgnoreCase(studyAutoGen)){
					studyNumber = studyIdentifier.getStudyNumber();
					logger.info("Study Number ="+studyNumber);
					mappedStudyIdentifier.setStudyNumber(studyNumber);
				}else{
					logger.fatal(autoGenError);
					throw new Exception(autoGenError);
				}
			}else{
				logger.fatal(autoGenError);
				throw new Exception(autoGenError);
			}
			
		}

		// set Identifier
		mappedStudy.setStudyIdentifier(mappedStudyIdentifier);
		
		logger.info("Set Study Identifier");

		StudyOrganizations mappedstudyOrgs = new StudyOrganizations();

		OrganizationIdentifier mappedOrgIdentifier = new OrganizationIdentifier();
		mappedOrgIdentifier.setSiteName(orgName);

		//Study Team Members
		Long totalCount = userSearchResults.getTotalCount();
		logger.info("Total Count = "+totalCount);
		List<Integer> searchUserPkList = new ArrayList<Integer>();
		List<User> users = null;
		List<User> studyTeamUsers = new ArrayList<User>();
		if(totalCount <= 0){
			//throw error saying no team members for org
			logger.info("Total users for org "+orgName+"is less than zero");
		}else if(totalCount > 0){
			users = userSearchResults.getUser();
		    for(User user:users){
		    	logger.info("*****Study Team Users*****");
		    	logger.info("user = "+user);
		    	logger.info("User Pk = "+user.getPK());
		    	logger.info("User FirstName = "+user.getFirstName());
		    	logger.info("User Last name = "+user.getLastName());
		    	logger.info("User Login = "+user.getUserLoginName());
		    	logger.info("User Org = "+user.getOrganization().getSiteName());
		    	logger.info("User Email = "+user.getEmail());
		    	logger.info("***************************");
		    	/*GroupIdentifier gpi = user.getUserdefaultgroup();
		    	logger.info("User Default Group = "+gpi);
		    	if(gpi!=null){
		    		gpi.getGroupName();
		    		logger.info("Group Name = "+gpi.getGroupName());
		    	}*/
		    	searchUserPkList.add(user.getPK());
		    }
		}
				
		logger.info("searchUserPkList = "+searchUserPkList);
				
		UserIdentifier dataManager = null;
		UserIdentifier prinInvestigator = null;
		UserIdentifier studyCoorinator = null;
		
		StudyTeamMembers mappedStudyTeamMembers = new StudyTeamMembers();
		List<StudyTeamMember> mappedStudyTeamMemberList = new ArrayList<StudyTeamMember>();
		StudyTeamMember mappedStudyTeamMember = null;
		logger.info("Study = "+study);
		StudyTeamMembers studyTeamMembers = study.getStudyTeamMembers();
		logger.info("studyTeamMembers = "+studyTeamMembers);
		List<StudyTeamMember> studyTeamMemberList = studyTeamMembers.getStudyTeamMember();
		logger.info("studyTeamMemberList = "+studyTeamMemberList);
		for(StudyTeamMember studyTeamMember : studyTeamMemberList){
		UserIdentifier stui = studyTeamMember.getUserId();
		logger.info("stui = "+stui);
		/*OrganizationIdentifier orgId = stui.getOrganizationIdentifier();
			logger.info("orgId = "+orgId);
			String siteName = orgId.getSiteName();
			logger.info("Site name = "+siteName);*/
			logger.info("Study Team Pk = "+stui.getPK());
						
			//if(searchUserPkList.contains(stui.getPK()) && siteName.equals(orgName)){
			if(searchUserPkList.contains(stui.getPK())){
				logger.info("***Adding StudyTeamMember with pk = "+stui.getPK()+"***");
				
				for(User user:users){		
					if(user.getPK().equals(stui.getPK())){
						studyTeamUsers.add(user);
					}
				}

				mappedStudyTeamMember = new StudyTeamMember();
					
				Code stCode= studyTeamMember.getStatus();
				Code mappedStCode = null;
				if(stCode != null){
					mappedStCode = new Code();
					String code = stCode.getCode();
					logger.info("Status Code = "+code);
					if(code!=null && !"".equals(code)){
						mappedStCode.setCode(code);
					}
					String desc = stCode.getDescription();
					logger.info("Status Desc = "+desc);
					if(desc!=null && !"".equals(desc)){
						mappedStCode.setDescription(desc);
					}
					String type = stCode.getType();
					logger.info("Status Type = "+type);
					if(type!=null && !"".equals(type)){
						mappedStCode.setType(type);
					}
				}
				if(mappedStCode!=null){
					logger.info("Set Study status");
					mappedStudyTeamMember.setStatus(mappedStCode);
				}
							
				Code trCode = studyTeamMember.getTeamRole();
				Code mappedTrCode = null;
				if(trCode!=null){
						
					/*
					mappedTrCode = new Code();
					 
					String code = trCode.getCode();
					logger.info("Team Code = "+code);
					if(code!=null && !"".equals(code)){
						mappedTrCode.setCode(code);
					}
					String desc = trCode.getDescription();
					logger.info("Team Desc = "+desc);
					if(desc!=null && !"".equals(desc)){
						mappedTrCode.setDescription(desc);
					}
					String type = trCode.getType();
					logger.info("Team Type = "+type);
					if(type!=null && !"".equals(type)){
						mappedTrCode.setType(type);
					}*/
					String teamRoleSubType = trCode.getCode();
					String teamRoleDesc = trCode.getDescription();
					String teamRoleType = trCode.getType();
					logger.info("Team Role codelst subtype = "+teamRoleSubType);
					logger.info("Team Role codelst Desc = "+teamRoleDesc);
					logger.info("Team Role codelst Type = "+teamRoleType);
					if(teamRoleSubType!=null && !"".equals(teamRoleSubType)){
						if (prop.containsKey(orgName + "_StudyTeamRole."+teamRoleSubType) && !"".equals(prop.getProperty(orgName + "_StudyTeamRole."+teamRoleSubType).trim())) {
							mappedTrCode = new Code();
							String mappedCodelstSubtyp = prop.getProperty(orgName + "_StudyTeamRole."+teamRoleSubType).trim();
							mappedTrCode.setCode(mappedCodelstSubtyp);
							mappedTrCode.setType(teamRoleType);
							mappedStudyTeamMember.setTeamRole(mappedTrCode);
						}else{
							mappedTrCode=trCode;
							mappedStudyTeamMember.setTeamRole(trCode);
						}
					}
				}
				/*if(mappedTrCode!=null){
					logger.info("Set Study Team Role");
					mappedStudyTeamMember.setTeamRole(mappedTrCode);
				}*/
							
				UserIdentifier stUI = studyTeamMember.getUserId();
				UserIdentifier mappedstUI = null;
				if(stUI!=null){
					mappedstUI = new UserIdentifier();
						
					Integer pk = stUI.getPK();
					logger.info("User Pk = "+pk);
					mappedstUI.setPK(pk);
						
					String firstName = stUI.getFirstName();
					logger.info("First name = "+firstName);
					if(firstName!=null && !"".equals(firstName)){
						mappedstUI.setFirstName(firstName);
					}
					String lastName = stUI.getLastName();
					logger.info("last Name = "+lastName);
					if(lastName!=null && !"".equals(lastName)){
						mappedstUI.setLastName(lastName);
					}
					String stLoginName = stUI.getUserLoginName();
					logger.info("stLoginName  = "+stLoginName);
					if(stLoginName!=null && !"".equals(stLoginName)){
						mappedstUI.setUserLoginName(stLoginName);
					}
				}
				if(mappedstUI!=null){
					logger.info("Set Study Team User ID");
					mappedStudyTeamMember.setUserId(mappedstUI);
					
					logger.info("Mapped Team Role Code = "+mappedTrCode);
					if(mappedTrCode!=null){
						String role = mappedTrCode.getCode();
						logger.info("Role = "+role);
						if(role!=null && !"".equals(role)){
							if(role.equals("role_dm")){
								logger.info("Setting Data Manager");
								dataManager = mappedstUI;
							}
							if(role.equals("role_prin")){
								logger.info("Setting Principal Investigator");
								prinInvestigator = mappedstUI;
							}
							if(role.equals("role_coord")){
								logger.info("Setting Study coordinator");
								studyCoorinator = mappedstUI;
							}
						}
					}
				}
						
				UserType userType = studyTeamMember.getUserType();
				logger.info("user Type = "+userType);
				if(userType!=null){
					mappedStudyTeamMember.setUserType(userType);
				}
				logger.info("Add Study Team member to List");
				mappedStudyTeamMemberList.add(mappedStudyTeamMember);
			}
		}
		
		logger.info("mappedStudyTeamMemberList = "+mappedStudyTeamMemberList);
		if(mappedStudyTeamMemberList==null || mappedStudyTeamMemberList.isEmpty()){
			//Throw error saying no team members added for organization(May be not needed)
			logger.info("No Study Team members added to study");
		}
		logger.info("Set All Study Team Members");
		mappedStudyTeamMembers.getStudyTeamMember().addAll(mappedStudyTeamMemberList);
		logger.info("Set All Study Team Members to Study");
		mappedStudy.setStudyTeamMembers(mappedStudyTeamMembers);
		
		logger.info("StudyTeam Users List = "+studyTeamUsers);
		requestMap.put(ProtocolKeys.StudyTeamUsers, studyTeamUsers);
		
		StudyOrganization mappedOrg = null;
		
		StudyOrganizations sorgs = (StudyOrganizations) study
				.getStudyOrganizations();
		logger.info("study Orgs = "+sorgs);
		if(sorgs!=null){
			mappedOrg = getMappedStudyOrganization(sorgs,orgName,mappedOrgIdentifier);
		}
		
		if(mappedOrg!=null){
			//set organization
			mappedstudyOrgs.getStudyOrganizaton().add(mappedOrg);
			logger.info("Set Study Organization");
		}
		//set all organizations
		mappedStudy.setStudyOrganizations(mappedstudyOrgs);
		logger.info("Set Study Organizations");
		
		StudyStatus mappedStudyStatus = getMappedStudyStatus(requestMap,dataManager);

		StudyStatuses mappedStudyStatuses = new StudyStatuses();
		mappedStudyStatuses.getStudyStatus().add(mappedStudyStatus);

		mappedStudy.setStudyStatuses(mappedStudyStatuses);
		
		StudySummary mappedStudySummary = new StudySummary();
		StudySummary summary = study.getStudySummary();
		if(summary!=null){
			mappedStudySummary = getMappedStudySummary(requestMap,orgName,summary,dataManager,prinInvestigator,studyCoorinator,studyNumber);
		}
		
		logger.info("Set Study Summary");
		mappedStudy.setStudySummary(mappedStudySummary);
	}
	
	//Method for getting StudyOrganization based on mapping configurations
	private StudyOrganization getMappedStudyOrganization(StudyOrganizations sorgs,String orgName,OrganizationIdentifier mappedOrgIdentifier){
		
		StudyOrganization mappedOrg = null;
		for(StudyOrganization sorg : sorgs.getStudyOrganizaton()){
			logger.info("study Org = "+sorg);
			if(sorg!=null){
				logger.info("Org Name = "+sorg.getOrganizationIdentifier().getSiteName());
				if((sorg.getOrganizationIdentifier().getSiteName()).equals(orgName)){
			
					mappedOrg = new StudyOrganization();
					
					logger.info("*******Getting Org specific values");
					logger.info("Organization = "+orgName);
					logger.info("mpublicInfoflag = " + sorg.isPublicInformationFlag());
					logger.info("mreportableFlag = " + sorg.isReportableFlag());
					logger.info("mrbEnrollFlag = " + sorg.isReviewBasedEnrollmentFlag());
					logger.info("Local Sample Size = "+sorg.getLocalSampleSize());
					
					mappedOrg.setOrganizationIdentifier(mappedOrgIdentifier);
					mappedOrg.setPublicInformationFlag(sorg.isPublicInformationFlag());
					mappedOrg.setReportableFlag(sorg.isReportableFlag());
					mappedOrg.setReviewBasedEnrollmentFlag(sorg.isReviewBasedEnrollmentFlag());
					mappedOrg.setLocalSampleSize(sorg.getLocalSampleSize());
					
					Code typeCode = sorg.getType();
					if(typeCode!=null){
						logger.info("Type code value = "+typeCode.getCode());
						logger.info("Type Desc = "+typeCode.getDescription());
						logger.info("Type Value = "+typeCode.getType());
						mappedOrg.setType(typeCode);
					}
					
					List<UserIdentifier> enrollList = sorg.getNewEnrollNotificationTo();
					logger.info("Enroll List = "+enrollList);
					logger.info("Enroll List Size = "+enrollList.size());
					if(enrollList!=null && !enrollList.isEmpty()){
						mappedOrg.getNewEnrollNotificationTo().addAll(enrollList);
						logger.info("Added NewEnrollNotification List");
						for(UserIdentifier enroll : enrollList){
							logger.info("**********************");
							logger.info("Enroller First Name = "+enroll.getFirstName());
							logger.info("Enroller Last Name = "+enroll.getLastName());
							logger.info("Enroller Login Name = "+enroll.getUserLoginName());
						}
					}
					
					List<UserIdentifier> approveList = sorg.getApproveEnrollNotificationTo();
					logger.info("Approve List = "+approveList);
					logger.info("Approve List Size = "+approveList.size());
					if(approveList!=null && !approveList.isEmpty()){
						mappedOrg.getApproveEnrollNotificationTo().addAll(approveList);
						logger.info("Added ApproveEnrollNotification List");
						for(UserIdentifier approve : approveList){
							logger.info("**********************");
							logger.info("Approver First Name = "+approve.getFirstName());
							logger.info("Approver Last Name = "+approve.getLastName());
							logger.info("Approver Login Name = "+approve.getUserLoginName());
						}
					}
				}
			}
		}
		return mappedOrg;
	}
	
	//Method for getting StudySummary based on mapping configurations
	private StudySummary getMappedStudySummary(Map<VelosKeys, Object> requestMap,String orgName,StudySummary summary,UserIdentifier dataManager,UserIdentifier prinInvestigator,UserIdentifier studyCoorinator,String studyNumber){
		
		StudySummary mappedStudySummary = new StudySummary();
		//Principal Investigator	
				/*UserIdentifier pi = summary.getPrincipalInvestigator();
				logger.info("PrincipalInvestigator = "+pi);
				if(pi!=null){
					UserIdentifier mappedPi = new UserIdentifier();
					String piFName = pi.getFirstName();
					logger.info("PI First Name = "+piFName);
					if(piFName!=null && !"".equals(piFName))
					{
						mappedPi.setFirstName(piFName);
					}
					String piLName = pi.getLastName();
					logger.info("PI Last Name = "+piLName);
					if(piLName!=null && !"".equals(piLName))
					{
						mappedPi.setLastName(piLName);
					}
					GroupIdentifier pigi = pi.getGroupIdentifier();
					logger.info("PI Group Identifier = "+pigi);
					OrganizationIdentifier piorg = pi.getOrganizationIdentifier();
					logger.info("PI OrganizationIdentifier = "+piorg);
					String piLoginname = pi.getUserLoginName();
					logger.info("PI Login Name = "+piLoginname);
					if(piLoginname!=null && !"".equals(piLoginname))
					{
						mappedPi.setUserLoginName(piLoginname);
					}
					mappedStudySummary.setPrincipalInvestigator(mappedPi);
				}*/
				
				logger.info("Pricipal Investigator = "+prinInvestigator);
				if(prinInvestigator!=null){
					mappedStudySummary.setPrincipalInvestigator(prinInvestigator); //Get data from study team
				}
				
				String piOther = summary.getPrincipalInvestigatorOther();
				logger.info("PI Other = "+piOther);
				if(piOther!=null && !"".equals(piOther)){
					mappedStudySummary.setPrincipalInvestigatorOther(piOther);
				}
				
				//Study Coordinator
				/*UserIdentifier studyContact = summary.getStudyContact();
				logger.info("Study Contact = "+studyContact);
				if(studyContact!=null){
					UserIdentifier mappedStudyContact = new UserIdentifier();
					String studyContactFName = studyContact.getFirstName();
					logger.info("StudyContact First Name = "+studyContactFName);
					if(studyContactFName!=null && !"".equals(studyContactFName))
					{
						mappedStudyContact.setFirstName(studyContactFName);
					}
					String studyContactLName = pi.getLastName();
					logger.info("StudyContact Last Name = "+studyContactLName);
					if(studyContactLName!=null && !"".equals(studyContactLName))
					{
						mappedStudyContact.setLastName(studyContactLName);
					}
					GroupIdentifier studyContactgi = studyContact.getGroupIdentifier();
					logger.info("StudyContact Group Identifier = "+studyContactgi);
					OrganizationIdentifier studyContactorg = studyContact.getOrganizationIdentifier();
					logger.info("StudyContact OrganizationIdentifier = "+studyContactorg);
					String studyContactLoginname = studyContact.getUserLoginName();
					logger.info("StudyContact Login Name = "+studyContactLoginname);
					if(studyContactLoginname!=null && !"".equals(studyContactLoginname))
					{
						mappedStudyContact.setUserLoginName(studyContactLoginname);
					}
					mappedStudySummary.setStudyContact(mappedStudyContact);
				}*/
				
				logger.info("Study Coordinator = "+studyCoorinator);
				if(studyCoorinator!=null){
					mappedStudySummary.setStudyContact(studyCoorinator);//Get data from study team
				}
				
				
				/*logger.info("isPIMajorAuthor() = "+summary.isPIMajorAuthor());
				logger.info("isIsCtrpReportable = "+summary.isIsCtrpReportable());
				logger.info("isIsFdaRegulated() = "+summary.isIsFdaRegulated());
				logger.info("isIsInvestigatorHeldIndIde = "+summary.isIsInvestigatorHeldIndIde());
				
				
				logger.info("----------------------------------");
				logger.info("isIsStdDefPublic() = "+summary.isIsStdDefPublic());
				logger.info("isIsStdDetailPublic = "+summary.isIsStdDetailPublic());
				logger.info("isIsStdDesignPublic = "+summary.isIsStdDesignPublic());
				logger.info("isIsStdSponsorInfoPublic = "+summary.isIsStdSponsorInfoPublic());
				
				
				mappedStudySummary.setPIMajorAuthor(summary.isPIMajorAuthor());
				mappedStudySummary.setIsCtrpReportable(summary.isIsCtrpReportable());
				mappedStudySummary.setIsFdaRegulated(summary.isIsFdaRegulated());
				mappedStudySummary.setIsInvestigatorHeldIndIde(summary.isIsInvestigatorHeldIndIde());
				
				
				mappedStudySummary.setIsStdDefPublic(summary.isIsStdDefPublic());
				mappedStudySummary.setIsStdDetailPublic(summary.isIsStdDetailPublic());
				mappedStudySummary.setIsStdDesignPublic(summary.isIsStdDesignPublic());
				mappedStudySummary.setIsStdSponsorInfoPublic(summary.isIsStdSponsorInfoPublic());*/
				
				
				logger.info("Study Number = "+summary.getStudyNumber());
				mappedStudySummary.setStudyNumber(studyNumber);
				//mappedStudySummary.setStudyNumber(summary.getStudyNumber());
				//mappedStudySummary.setStudyNumber("Test Nov25-14");//Hardcoded Value
				
				String title = summary.getStudyTitle();
				logger.info("Study Title = "+title);
				if(title!=null && !"".equals(title)){
					mappedStudySummary.setStudyTitle(title);
				}
				
				String nctNumber = summary.getNctNumber();
				logger.info("NCT Number = "+nctNumber);
				if(nctNumber!=null && !"".equals(nctNumber)){
					mappedStudySummary.setNctNumber(nctNumber);
				}
				
				/*String nciTrialIdentifier = summary.getNciTrialIdentifier();
				logger.info("NciTrialIdentifier = "+nciTrialIdentifier);
				if(nciTrialIdentifier!=null && !"".equals(nciTrialIdentifier)){
					mappedStudySummary.setNciTrialIdentifier(nciTrialIdentifier);
				}*/
							
				
				String objective = summary.getObjective();
				logger.info("Study Objective = "+objective);
				if(objective!=null && !"".equals(objective)){
					mappedStudySummary.setObjective(objective);
				}
				/*String summaryText = summary.getSummaryText();
				logger.info("Study Summary = "+summaryText);
				if(summaryText!=null && !"".equals(summaryText)){
					mappedStudySummary.setSummaryText(summaryText);
				}
				String agentOrDevice = summary.getAgentDevice();
				logger.info("Study Agent/Device = "+agentOrDevice);
				if(agentOrDevice!=null && !"".equals(agentOrDevice)){
					mappedStudySummary.setAgentDevice(agentOrDevice);
				}
				int nationalSampleSize = summary.getNationalSampleSize();
				logger.info("National Sample Size = "+nationalSampleSize);
					mappedStudySummary.setNationalSampleSize(nationalSampleSize);
					
				Duration duration = summary.getStudyDuration();
				if(duration!=null){
					TimeUnits timeUnits = duration.getUnit();
					Integer value = duration.getValue();
					logger.info("Study Duration");
					logger.info("Time Units = "+timeUnits);
					logger.info("Value = "+value);
					mappedStudySummary.setStudyDuration(duration);
				}
				
				XMLGregorianCalendar estimatedBeginDate = summary.getEstimatedBeginDate();
				if(estimatedBeginDate!=null){
					logger.info("Estimated Begin Date");
					int month = estimatedBeginDate.getMonth();
					logger.info("Month = "+month);
					int day = estimatedBeginDate.getDay();
					logger.info("Day = "+day);
					int year = estimatedBeginDate.getYear();
					logger.info("Year = "+year);
					mappedStudySummary.setEstimatedBeginDate(estimatedBeginDate);
				}
				
				String sponsorIfOther = summary.getSponsorNameOther();
				logger.info("SponsorIfOther = "+sponsorIfOther);
				if(sponsorIfOther!=null && !"".equals(sponsorIfOther)){
					mappedStudySummary.setSponsorNameOther(sponsorIfOther);
				}
				
				String sponsorID = summary.getSponsorID();
				logger.info("Sponsor ID = "+sponsorID);
				if(sponsorID!=null && !"".equals(sponsorID)){
					mappedStudySummary.setSponsorID(sponsorID);
				}
				
				String sponsorContact = summary.getSponsorContact();
				logger.info("Sponsor Contact = "+sponsorContact);
				if(sponsorContact!=null && !"".equals(sponsorContact)){
					mappedStudySummary.setSponsorContact(sponsorContact);
				}
				
				String sponsorOtherInfo = summary.getSponsorOtherInfo();
				logger.info("Sponsor OtherInfo = "+sponsorOtherInfo);
				if(sponsorOtherInfo!=null && !"".equals(sponsorOtherInfo)){
					mappedStudySummary.setSponsorOtherInfo(sponsorOtherInfo);
				}
				
				String keywords = summary.getKeywords();
				logger.info("keywords = "+keywords);
				if(keywords!=null && !"".equals(keywords)){
					mappedStudySummary.setKeywords(keywords);
				}*/
				
				/*Code rZCode = summary.getRandomization();
				
				Code mappedRzCode = new Code();
				logger.info("Randomized Code = "+rZCode);
				if(rZCode != null){
					Boolean randomseMapRequire = Boolean.parseBoolean(prop.getProperty(orgName + "_mapping.randomization"));
					logger.info("Randomized Mapping Require = "+randomseMapRequire);
					if(randomseMapRequire){
						logger.info("Setting Randomize code with mapping");
						String pCode = phaseCode.getCode();
						//if()
						
					}else if(!randomseMapRequire){
						logger.info("Setting Randomize code with out mapping");
						mappedStudySummary.setRandomization(rZCode);
					}
				}*/
				
				//Division
				Code division = summary.getDivision();
				Code mappedDivision = null;
				if(division != null){
					String divisionCode = division.getCode();
					String divisionDesc = division.getDescription();
					String divisionType = division.getType();
					logger.info("Division codelst subtype = "+divisionCode);
					logger.info("Division codelst Desc = "+divisionDesc);
					logger.info("Division codelst Type = "+divisionType);
					mappedDivision = CodelstsCache.getMappedCode(requestMap,"division",divisionType,divisionCode,divisionDesc);
					mappedStudySummary.setDivision(mappedDivision);
				}
				
				//TherapeuticArea
				Code tArea = summary.getTherapeuticArea();
				Code mappedTArea = null;
				if(tArea != null){
					String tAreaCode = tArea.getCode();
					String tAreaDesc = tArea.getDescription();
					String tAreaType = tArea.getType();
					logger.info("Therapeutic codelst subtype = "+tAreaCode);
					logger.info("Therapeutic codelst Desc = "+tAreaDesc);
					logger.info("Therapeutic codelst Type = "+tAreaType);
					mappedTArea = CodelstsCache.getMappedCode(requestMap,"tArea",tAreaType,tAreaCode,tAreaDesc);
					mappedStudySummary.setTherapeuticArea(mappedTArea);
				}
				
		
				//Phase
				Code phase = summary.getPhase();
				Code mappedPhase = null;
				if(phase != null){
					String pCode = phase.getCode();
					String pDesc = phase.getDescription();
					String pType = phase.getType();
					logger.info("Phase codelst subtype = "+pCode);
					logger.info("Phase codelst Desc = "+pDesc);
					logger.info("Phase codelst Type = "+pType);
					mappedPhase = CodelstsCache.getMappedCode(requestMap,"phase",pType,pCode,pDesc);
					mappedStudySummary.setPhase(mappedPhase);
				}
				
				/*//ResearchType
				Code researchType = summary.getResearchType();
				Code mappedResearchType = null;
				if(researchType != null){
					String researchTypeCode = researchType.getCode();
					String researchTypeDesc = researchType.getDescription();
					String researchTypeType = researchType.getType();
					logger.info("ResearchType codelst subtype = "+researchTypeCode);
					logger.info("ResearchType codelst Desc = "+researchTypeDesc);
					logger.info("ResearchType codelst Type = "+researchTypeType);
					mappedResearchType = CodelstsCache.getMappedCode(requestMap,"researchType",researchTypeType,researchTypeCode,researchTypeDesc);
					mappedStudySummary.setResearchType(mappedResearchType);
				}
				
				//StudyScope
				Code studyScope = summary.getStudyScope();
				Code mappedStudyScope = null;
				if(studyScope != null){
					String studyScopeCode = studyScope.getCode();
					String studyScopeDesc = studyScope.getDescription();
					String studyScopeType = studyScope.getType();
					logger.info("StudyScope codelst subtype = "+studyScopeCode);
					logger.info("StudyScope codelst Desc = "+studyScopeDesc);
					logger.info("StudyScope codelst Type = "+studyScopeType);
					mappedStudyScope = CodelstsCache.getMappedCode(requestMap,"studyScope",studyScopeType,studyScopeCode,studyScopeDesc);
					mappedStudySummary.setStudyScope(mappedStudyScope);
				}
						
						
				//StudyType
				Code studyType = summary.getStudyType();
				Code mappedstudyType = null;
				if(studyType != null){
					String studyTypeCode = studyType.getCode();
					String studyTypeDesc = studyType.getDescription();
					String studyTypeType = studyType.getType();
					logger.info("StudyType codelst subtype = "+studyTypeCode);
					logger.info("StudyType codelst Desc = "+studyTypeDesc);
					logger.info("StudyType codelst Type = "+studyTypeType);
					mappedstudyType = CodelstsCache.getMappedCode(requestMap,"studyType",studyTypeType,studyTypeCode,studyTypeDesc);
					mappedStudySummary.setStudyType(mappedstudyType);
				}
						
				//Blinding
				Code blinding = summary.getBlinding();
				Code mappedBlinding = null;
				if(blinding != null){
					String blindingCode = blinding.getCode();
					String blindingDesc = blinding.getDescription();
					String blindingType = blinding.getType();
					logger.info("Blinding codelst subtype = "+blindingCode);
					logger.info("Blinding codelst Desc = "+blindingDesc);
					logger.info("Blinding codelst Type = "+blindingType);
					mappedBlinding = CodelstsCache.getMappedCode(requestMap,"blinding",blindingType,blindingCode,blindingDesc);
					mappedStudySummary.setBlinding(mappedBlinding);	
				}
						
						
				//Randomization
					
				Code randomization = summary.getRandomization();
				Code mappedRandomization = null;
				if(randomization != null){
					String rCode = randomization.getCode();
					String rDesc = randomization.getDescription();
					String rType = randomization.getType();
					logger.info("Randomization codelst subtype = "+rCode);
					logger.info("Randomization codelst Desc = "+rDesc);
					logger.info("Randomization codelst Type = "+rType);
					mappedRandomization = CodelstsCache.getMappedCode(requestMap,"randomization",rType,rCode,rDesc);
					mappedStudySummary.setRandomization(mappedRandomization);
				}
				
				//Disease Site
				List<Code> diseaseSites = summary.getDiseaseSites();
				List<Code> mappedDiseaseSites = new ArrayList<Code>();
				//if(!diseaseSites.isEmpty())
				for(Code diseaseSite : diseaseSites){
					if(diseaseSite!=null){
						String diseaseSiteCode = diseaseSite.getCode();
						String diseaseSiteDesc = diseaseSite.getDescription();
						String diseaseSiteType = diseaseSite.getType();
						logger.info("Disease Site codelst subtype = "+diseaseSiteCode);
						logger.info("Disease Site codelst Desc = "+diseaseSiteDesc);
						logger.info("Disease Site codelst Type = "+diseaseSiteType);
						Code mappedCode = mappedRandomization = CodelstsCache.getMappedCode(requestMap,"diseaseSite",diseaseSiteType,diseaseSiteCode,diseaseSiteDesc);
						mappedDiseaseSites.add(mappedCode);
						logger.info("------------------------");
					}
				}
				
				mappedStudySummary.getDiseaseSites().addAll(mappedDiseaseSites);
				
				//Specific Sites
				String specificSite1 = summary.getSpecificSite1();
				logger.info("specificSite1 = "+specificSite1);
				String specificSite2 = summary.getSpecificSite2();
				logger.info("specificSite2 = "+specificSite2);
				String specificSite3 = summary.getSpecificSite3();
				logger.info("specificSite3 = "+specificSite3);
				if(specificSite1!=null && !"".equals(specificSite1)){
					mappedStudySummary.setSpecificSite1(specificSite1);
				}
				if(specificSite2!=null && !"".equals(specificSite2)){
					mappedStudySummary.setSpecificSite2(specificSite2);
				}
				if(specificSite3!=null && !"".equals(specificSite3)){
					mappedStudySummary.setSpecificSite3(specificSite3);
				}
				
				
				//Sponsor Name
				Code sponsor = summary.getSponsorName();
				Code mappedSponsor = null;
				if(sponsor != null){
					String sponsorCode = sponsor.getCode();
					String sponsorDesc = sponsor.getDescription();
					String sponsorType = sponsor.getType();
					logger.info("sponsor codelst subtype = "+sponsorCode);
					logger.info("sponsor codelst Desc = "+sponsorDesc);
					logger.info("sponsor codelst Type = "+sponsorType);
					mappedSponsor = CodelstsCache.getMappedCode(requestMap,"sponsorName",sponsorType,sponsorCode,sponsorDesc);
					mappedStudySummary.setSponsorName(mappedSponsor);
				}
				*/				
				//Study Author
				logger.info("Study Author = "+dataManager);
				if(dataManager!=null){
					mappedStudySummary.setStudyAuthor(dataManager);//Get data from study team
				}
				
				/*//IND/IDE Data
				StudyINDIDEs studyINDIDEs = summary.getStudyINDIDEs();
				logger.info("StudyINDIDEs = "+studyINDIDEs);
				if(studyINDIDEs!=null){
					List<StudyINDIDEInfo> mappeIndList = getMappedIndIdeData(requestMap,summary,studyINDIDEs,orgName);
					logger.info("Mapped IND List = "+mappeIndList);
					
					StudyINDIDEs mappedStudyINDIDEs = new StudyINDIDEs();
						mappedStudyINDIDEs.getStudyINDIDEInfo().addAll(mappeIndList);
					
					mappedStudySummary.setStudyINDIDEs(mappedStudyINDIDEs);
				}
				
				//NIH Grant Information
				StudyNIHGrants studyNIHGrants = summary.getStudyNIHGrants();
				logger.info("StudyNIHGrants = "+studyNIHGrants);
				if(studyNIHGrants!=null){
					List<StudyNIHGrantInfo> mappedNihList = getMappedStudyNIHGrants(requestMap,summary,studyNIHGrants,orgName);
					logger.info("Mapped NIH List = "+mappedNihList);
					StudyNIHGrants mappedStudyNIHGrants = new StudyNIHGrants();
						mappedStudyNIHGrants.getStudyNIHGrantInfo().addAll(mappedNihList);
						
					mappedStudySummary.setStudyNIHGrants(mappedStudyNIHGrants);
				}
				*/
				List<NvPair> moreList = summary.getMoreStudyDetails();
				
				List<NvPair> mappedMoreList = getMappedMoreStudyDetails(summary,moreList,orgName);
				
				System.out.println("MappedMoreStudyList = "+mappedMoreList);
				System.out.println("MappedMoreStudyList Size = "+mappedMoreList.size());
				mappedStudySummary.getMoreStudyDetails().addAll(mappedMoreList);
				
				return mappedStudySummary;
	}
	
	//Method for getting MoreStudyDetails based on mapping configurations
	private List<NvPair> getMappedMoreStudyDetails(StudySummary summary,List<NvPair> moreList,String orgName){
		
		//More Study Details
		
		/*List<NvPair> moreList = summary.getMoreStudyDetails();
		logger.info("****More Study Details****");
		for (NvPair more : moreList) {
			String key = more.getKey();
			logger.info("Key = "+key);
			logger.info("Value Before typecasting = "+more.getValue());
			String value = (String) more.getValue();
			logger.info("Value = "+value);
		}
		
		NvPair ctxStudyNumber = new NvPair();
			ctxStudyNumber.setKey("ctx_study_num");
			ctxStudyNumber.setValue(studyIdentifier.getStudyNumber());
		
		mappedStudySummary.getMoreStudyDetails().add(ctxStudyNumber);*/
		
		
		NvPair nvPair = null;
		List<NvPair> mappedMoreList = new ArrayList<NvPair>();
		logger.info("****More Study Details****");
		for (NvPair more : moreList) {
			String key = more.getKey();
			logger.info("Key = "+key);
			logger.info("Value Before = "+more.getValue());
			String value = (String) more.getValue();
			logger.info("Value = "+value);
			if (prop.containsKey(orgName+"_"+key) && !"".equals(prop.getProperty(orgName+"_"+key).trim()) && !"".equals(value) && value!=null) {
				nvPair = new NvPair();
				String codelstSubtype = prop.getProperty(orgName+"_"+key).trim();
				logger.info("Mapped Codelst Subtype = "+codelstSubtype);
				logger.info("Mapped value = "+value);
				nvPair.setKey(codelstSubtype);
				nvPair.setValue(value);
				mappedMoreList.add(nvPair);
			}
		}
		
		NvPair ctxStudyNumber = null;
		if (prop.containsKey(orgName+"_CTXStudyNumber") && !"".equals(prop.getProperty(orgName+"_CTXStudyNumber").trim())) {
			ctxStudyNumber = new NvPair();
			String CTXStudyCodelstSubtype = prop.getProperty(orgName+"_CTXStudyNumber").trim();
			logger.info("Mapped CTX Study Number Codelst Subtype = "+CTXStudyCodelstSubtype);
			logger.info("StudyNumber as CTX Number = "+summary.getStudyNumber());
			ctxStudyNumber.setKey(CTXStudyCodelstSubtype);
			//ctxStudyNumber.setValue(studyIdentifier.getStudyNumber());
			ctxStudyNumber.setValue(summary.getStudyNumber());
			mappedMoreList.add(ctxStudyNumber);
		}
		
		return mappedMoreList;
	}
	
	private List<StudyINDIDEInfo> getMappedIndIdeData(Map<VelosKeys, Object> requestMap,StudySummary summary,StudyINDIDEs studyINDIDEs,String orgName){
		
		List<StudyINDIDEInfo> indList = studyINDIDEs.getStudyINDIDEInfo();
		
		List<StudyINDIDEInfo> mappeIndList = new ArrayList<StudyINDIDEInfo>();
		
		for(StudyINDIDEInfo ind : indList){
			
			StudyINDIDEInfo mappedInd = null;
			if(ind!=null){
				
				logger.info("---------------------------");
				mappedInd = new StudyINDIDEInfo();
				
				    //IND/IDE Types
					Integer indType = ind.getTypeINDIDE();
					logger.info("IND/IDE Type = "+indType);
					
					mappedInd.setTypeINDIDE(indType);
					
					//IND/IDE Number
					String INDNumber = ind.getNumberINDIDE();
					logger.info("IND/IDE Number = "+INDNumber);
					if(INDNumber!=null && !"".equals(INDNumber)){
						mappedInd.setNumberINDIDE(INDNumber);
					}
					
					//IND/IDE Grantor
					Code grantor = ind.getGrantor();
					Code mappedGrantor = null;
					if(grantor != null){
						String grantorCode = grantor.getCode();
						String grantorDesc = grantor.getDescription();
						String grantorType = grantor.getType();
						logger.info("Grantor codelst subtype = "+grantorCode);
						logger.info("Grantor codelst Desc = "+grantorDesc);
						logger.info("Grantor codelst Type = "+grantorType);
						mappedGrantor = CodelstsCache.getMappedCode(requestMap,"INDIDEGrantor",grantorType,grantorCode,grantorDesc);
						mappedInd.setGrantor(mappedGrantor);
					}
					
					//IND/IDE Holder Type
					Code holder = ind.getHolder();
					Code mappedholder = null;
					if(holder != null){
						String holderCode = holder.getCode();
						String holderDesc = holder.getDescription();
						String holderType = holder.getType();
						logger.info("Holder codelst subtype = "+holderCode);
						logger.info("Holder codelst Desc = "+holderDesc);
						logger.info("Holder codelst Type = "+holderType);
						mappedholder = CodelstsCache.getMappedCode(requestMap,"INDIDEHolder",holderType,holderCode,holderDesc);
						mappedInd.setHolder(mappedholder);
					}
					
					//NIH Institution, NCI Division/Program Code (If applicable)
					Code program = ind.getProgramCode();
					Code mappedProgram = null;
					if(program != null){
						String programCode = program.getCode();
						String programDesc = program.getDescription();
						String programType = program.getType();
						logger.info("Program Code codelst subtype = "+programCode);
						logger.info("Program Code codelst Desc = "+programDesc);
						logger.info("Program Code codelst Type = "+programType);
						mappedProgram = CodelstsCache.getMappedCode(requestMap,"INDProgramCode",programType,programCode,programDesc);
						mappedInd.setProgramCode(mappedProgram);
					}
					
					//Expanded Access
					Integer expandAccess = ind.getExpandAccess();
					logger.info("Expand Access = "+expandAccess);
					mappedInd.setExpandAccess(expandAccess);					
					
					//Expanded Access Type
					Code acesssType = ind.getAccessType();
					Code mappedAcesssType = null;
					if(acesssType != null){
						String acesssTypeCode = acesssType.getCode();
						String acesssTypeDesc = acesssType.getDescription();
						String acesssTypeType = acesssType.getType();
						logger.info("ExpandedAccessType codelst subtype = "+acesssTypeCode);
						logger.info("ExpandedAccessType codelst Desc = "+acesssTypeDesc);
						logger.info("ExpandedAccessType codelst Type = "+acesssTypeType);
						mappedAcesssType = CodelstsCache.getMappedCode(requestMap,"ExpandAcesssType",acesssTypeType,acesssTypeCode,acesssTypeDesc);
						mappedInd.setAccessType(mappedAcesssType);
					}
					
					//Exempt
					Integer exempt = ind.getExemptINDIDE();
					logger.info("Exempt = "+exempt);
					
					mappedInd.setExemptINDIDE(exempt);
			}
			mappeIndList.add(mappedInd);
		}
		return mappeIndList;
	}
	
	
	private List<StudyNIHGrantInfo> getMappedStudyNIHGrants(Map<VelosKeys, Object> requestMap,StudySummary summary,StudyNIHGrants studyNIHGrants, String orgName) {
		
		List<StudyNIHGrantInfo> nihList = studyNIHGrants.getStudyNIHGrantInfo();
		List<StudyNIHGrantInfo> mappeNihList = new ArrayList<StudyNIHGrantInfo>();
		for(StudyNIHGrantInfo nih : nihList){
			StudyNIHGrantInfo mappedNih = null;
			if(nih!=null){
				logger.info("---------------------------");
				mappedNih = new StudyNIHGrantInfo();
				Code fundMech = nih.getFundMech();
				Code mappedFundMech = null;
				if(fundMech != null){
					String fundMechCode = fundMech.getCode();
					String fundMechDesc = fundMech.getDescription();
					String fundMechType = fundMech.getType();
					logger.info("Funding Mechanism codelst subtype = "+fundMechCode);
					logger.info("Funding Mechanism codelst Desc = "+fundMechDesc);
					logger.info("Funding Mechanism codelst Type = "+fundMechType);
					mappedFundMech = CodelstsCache.getMappedCode(requestMap,"NIHFundMechanism",fundMechType,fundMechCode,fundMechDesc);
					mappedNih.setFundMech(mappedFundMech);
				}
				
				Code nihProgram = nih.getProgramCode();
				Code mappedNihProgram = null;
				if(nihProgram != null){
					String nihProgramCode = nihProgram.getCode();
					String nihProgramDesc = nihProgram.getDescription();
					String nihProgramType = nihProgram.getType();
					logger.info("NIH Program Code codelst subtype = "+nihProgramCode);
					logger.info("NIH Program Code codelst Desc = "+nihProgramDesc);
					logger.info("NIH Program Code codelst Type = "+nihProgramType);
					mappedNihProgram = CodelstsCache.getMappedCode(requestMap,"NIHProgramCode",nihProgramType,nihProgramCode,nihProgramDesc);
					mappedNih.setProgramCode(mappedNihProgram);
				}
				
				String nihGrantSerial = nih.getNihGrantSerial();
				logger.info("NIH Grant Serial = "+nihGrantSerial);
				if(nihGrantSerial!=null && !"".equals(nihGrantSerial)){
					mappedNih.setNihGrantSerial(nihGrantSerial);
				}
				
				Code nihInstCode = nih.getInstCode();
				Code mappedInstCode = null;
				if(nihInstCode != null){
					String instCode = nihInstCode.getCode();
					String instDesc = nihInstCode.getDescription();
					String instType = nihInstCode.getType();
					logger.info("NIH Institute codelst subtype = "+instCode);
					logger.info("NIH Institute codelst Desc = "+instDesc);
					logger.info("NIH Institute codelst Type = "+instType);
					mappedInstCode = CodelstsCache.getMappedCode(requestMap,"NIHInstituteCode",instType,instCode,instDesc);
					mappedNih.setInstCode(mappedInstCode);
				}
			}
			mappeNihList.add(mappedNih);
		}
		return mappeNihList;
	}
	
	
	public StudyStatus getMappedStudyStatus(Map<VelosKeys, Object> requestMap,UserIdentifier dataManager) throws Exception{
		StudyStatus mappedStudyStatus = new StudyStatus();
		UserIdentifier documentedBy = null;
		prop = (Properties)requestMap.get(ProtocolKeys.PropObj);
		Study study = (Study)requestMap.get(ProtocolKeys.StudyObj);
		String packageType = (String)requestMap.get(ProtocolKeys.PackageType);
		String orgName = ((String)requestMap.get(ProtocolKeys.OrganizationName)).trim();
    	logger.info("OrganizationName = "+orgName);
    	String stdStatus = ((String)requestMap.get(ProtocolKeys.StudyStatus)).trim();
    	logger.info("Study Status Changed = "+stdStatus);
    	OrganizationIdentifier mappedOrgIdentifier = new OrganizationIdentifier();
		mappedOrgIdentifier.setSiteName(orgName);
		logger.info("Documented By = "+dataManager);
		if(packageType.equals("StudyPackage")){
			if(dataManager!=null){
				mappedStudyStatus.setDocumentedBy(dataManager);
			}else if(dataManager==null){
				throw new Exception("DataManager Not Found");
			}
		}else if(packageType.equals("Amendments")){
			if (prop.containsKey(orgName + ".userID") && !"".equals(prop.getProperty(orgName + ".userID").trim())) {
				documentedBy = new UserIdentifier();
				String userName = prop.getProperty(orgName + ".userID").trim();
				documentedBy.setUserLoginName(userName);
			}
			mappedStudyStatus.setDocumentedBy(documentedBy);
		}
				
		mappedStudyStatus.setOrganizationId(mappedOrgIdentifier);
		String studyStatusError = "Study Status mapping not configured for participating site  "+orgName;;
		try{
			String statusCode = null;
			String statusDesc = null;
			String statustype = null;
			Code status = new Code();
			if(packageType.equals("StudyPackage")){
				statusCode = prop.getProperty(orgName+"_status.Code").trim();
				logger.info("Status Code = "+statusCode);
				statusDesc = prop.getProperty(orgName+"_status.Description").trim();
				logger.info("Status Desc = "+statusDesc);
				statustype = prop.getProperty(orgName+"_status.Type").trim();
				logger.info("Status Type = "+statustype);
			}else if(packageType.equals("Amendments")){
				studyStatusError = "Study Status Amendments mapping not configured for participating site  "+orgName;
				statusCode = prop.getProperty(orgName+"_AmndStudyStatus.Code").trim();
				logger.info("AmndStudyStatus SubType = "+statusCode);
				statusDesc = prop.getProperty(orgName+"_AmndStudyStatus.Description").trim();
				logger.info("AmndStudyStatus Desc = "+statusDesc);
				statustype = prop.getProperty(orgName+"_AmndStudyStatus.Type").trim();
				logger.info("AmndStudyStatus Type = "+statustype);
			}
				
			Code statusType = new Code();
			String statusTypeCode = prop.getProperty(orgName + "_statusType.Code").trim();
			String statusTypeDesc = prop.getProperty(orgName + "_statusType.Description").trim();
			String StatusTypeType = prop.getProperty(orgName + "_statusType.Type").trim();
			logger.info("Status Type Code = "+statusTypeCode);
			logger.info("Status Type Desc = "+statusTypeDesc);
			logger.info("Status Type Type = "+StatusTypeType);
			if("".equals(statusCode) || "".equals(statusDesc) || "".equals(statustype) || "".equals(statusTypeCode) || "".equals(statusTypeDesc) || "".equals(StatusTypeType)){
				throw new Exception(studyStatusError);
			}
			status.setCode(statusCode);
			status.setDescription(statusDesc);
			status.setType(statustype);
			mappedStudyStatus.setStatus(status);
			statusType.setCode(statusTypeCode);
			statusType.setDescription(statusTypeDesc);
			statusType.setType(StatusTypeType);
			mappedStudyStatus.setStatusType(statusType);
		}catch(Exception e){
			logger.fatal(studyStatusError);
			throw new Exception(studyStatusError);
		}
		
		/*String DATE_FORMAT_NOW = "yyyy-MM-dd";
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
		String stringDate = sdf.format(date);

		XMLGregorianCalendar result = StringToXMLGregorianCalendar
				.convertStringDateToXmlGregorianCalendar(stringDate,
						"yyyy-MM-dd", true);*/
		
		XMLGregorianCalendar validFromDate = null;
		XMLGregorianCalendar validUntilDate = null;
		XMLGregorianCalendar meetingDate = null;
		String notes = "";
		List<StudyStatus> studyStatuslist = study.getStudyStatuses().getStudyStatus();
		for(StudyStatus studyStatus : studyStatuslist ){
			Code code = studyStatus.getStatus();
			if(code!=null){
				if(stdStatus.equals(code.getDescription())){
					if(studyStatus.isCurrentForStudy()){
						mappedStudyStatus.setCurrentForStudy(studyStatus.isCurrentForStudy());
						logger.info("Is Current For Study = "+studyStatus.isCurrentForStudy());
						validFromDate = studyStatus.getValidFromDate();
						validUntilDate = studyStatus.getValidUntilDate();
						logger.info("validFromDate = "+validFromDate);
						logger.info("validUntilDate = "+validUntilDate);
						mappedStudyStatus.setValidFromDate(validFromDate);
						if(validUntilDate != null && !"".equals(validUntilDate)){
							mappedStudyStatus.setValidUntilDate(validUntilDate);
						}
						meetingDate = studyStatus.getMeetingDate();
						logger.info("meetingDate = "+meetingDate);
						if(meetingDate != null && !"".equals(meetingDate)){
							mappedStudyStatus.setMeetingDate(meetingDate);
						}
						notes = studyStatus.getNotes();
						logger.info("notes = "+notes);
						if(notes != null && !"".equals(notes)){
							mappedStudyStatus.setNotes(notes);
						}
						Code reviewBoard = studyStatus.getReviewBoard();
						Code mappedReviewBoard = null;
						if(reviewBoard != null && !"".equals(reviewBoard)){
								String reviewBoardSubType = reviewBoard.getCode();
								String reviewBoardDesc = reviewBoard.getDescription();
								String reviewBoardType = reviewBoard.getType();
								mappedReviewBoard = CodelstsCache.getMappedCode(requestMap,"reviewBoard",reviewBoardType,reviewBoardSubType,reviewBoardDesc);
								mappedStudyStatus.setReviewBoard(mappedReviewBoard);
						}
						Code outcome = studyStatus.getOutcome();
						Code mappedOutcome = null;
						if(outcome != null){
							String outcomeSubType = outcome.getCode();
							String outcomeDesc = outcome.getDescription();
							String outcomeType = outcome.getType();
							mappedOutcome = CodelstsCache.getMappedCode(requestMap,"outcome",outcomeType,outcomeSubType,outcomeDesc);
							mappedStudyStatus.setOutcome(mappedOutcome);
						}
						break;
					}
				}
			}
		}
		return mappedStudyStatus;
	}
	
}
