package com.velos.integration.espclient;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import com.velos.services.OperationException_Exception;

@WebService(targetNamespace = "http://velos.com/services/", name = "StudySEI")
public interface VelosEspSearchStudyEndpoint {
	
	@WebResult(name = "StudySearchResults", targetNamespace = "")
    @RequestWrapper(localName = "searchStudy", targetNamespace = "http://velos.com/services/", className = "com.velos.services.SearchStudy")
    @WebMethod
    @ResponseWrapper(localName = "searchStudyResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.SearchStudyResponse")
    public com.velos.services.StudySearchResults searchStudy(
        @WebParam(name = "StudySearch", targetNamespace = "")
        com.velos.services.StudySearch studySearch
    ) throws OperationException_Exception;
}