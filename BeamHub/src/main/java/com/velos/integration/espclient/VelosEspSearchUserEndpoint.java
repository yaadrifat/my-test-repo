package com.velos.integration.espclient;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import com.velos.services.OperationException_Exception;

 
@WebService(targetNamespace = "http://velos.com/services/", name = "UserSEI")
public interface VelosEspSearchUserEndpoint {
	 @WebResult(name = "UserSearchResults", targetNamespace = "")
	    @RequestWrapper(localName = "searchUser", targetNamespace = "http://velos.com/services/", className = "com.velos.services.SearchUser")
	    @WebMethod
	    @ResponseWrapper(localName = "searchUserResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.SearchUserResponse")
	    public com.velos.services.UserSearchResults searchUser(
	        @WebParam(name = "UserSearch", targetNamespace = "")
	        com.velos.services.UserSearch userSearch
	    ) throws OperationException_Exception;

}
