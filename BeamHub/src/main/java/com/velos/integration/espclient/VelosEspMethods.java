package com.velos.integration.espclient;

public enum VelosEspMethods {
	StudyGetStudySummary("getStudySummary"),
	StudyGetStudy("getStudy"),
	StudySearchOrgStudy("searchOrgStudy"),
	StudyCreateOrgStudy("createOrgStudy"),
	StudyCalGetStudyCalendar("getStudyCalendar"),
	StudyCalGetStudyCalendarList("getStudyCalendarList"),
	StudyCalCreateStudyCalendar("createStudyCalendar"),
	StudyGetDocumentedBy("getDocumentedBy"),
	StudyCalGetMilestones("getMilestones"),
	StudyCalCreateMilestones("createMilestones"),
	StudyPatAddStudyPatientStatus("addStudyPatientStatus"),
	StudyPatGetStudyPatients("getStudyPatients"),
	StudyPatEnrollPatientToStudy("enrollPatientToStudy"),
	StudyVersGetStudyVersList("getStudyVersionsList"),
	StudyVersCreateStudyVersions("createStudyVersions"),
	StudyGetStudyCalBudgets("getStudyCalBudgets"),
	StudyCreateStudyCalBudgets("createStudyCalBudgets"),
	PatDemogSearchPatient("searchPatient"),
	
	SysAdminGetObjectInfoFromOID("getObjectInfoFromOID"),
	
	PatientStudyGetPatientStudyInfo("getPatientStudyInfo"),
	PatientStudyGetStudyPatientStatusHistory("getStudyPatientStatusHistory"),
	StudyVersGetStudyVersAmndList("getStudyVersAmndList"),
	StudyVersCreateStudyVersAmendment("createStudyVersAmendment"),
	GetCodelsts("getCodelsts")
	;
	
	private String key;
	
	VelosEspMethods(String key) {
		this.key = key;
	}
	
	@Override
	public String toString() {
		return key;
	}
}
