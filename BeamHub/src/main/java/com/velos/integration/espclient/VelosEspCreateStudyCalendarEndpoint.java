package com.velos.integration.espclient;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import com.velos.services.OperationException_Exception;
import com.velos.services.OperationRolledBackException_Exception;

@WebService(targetNamespace = "http://velos.com/services/", name = "StudyCalendarSEI")
public interface VelosEspCreateStudyCalendarEndpoint {
    @WebResult(name = "Response", targetNamespace = "")
    @RequestWrapper(localName = "createStudyCalendar", targetNamespace = "http://velos.com/services/", className = "com.velos.services.CreateStudyCalendar")
    @WebMethod
    @ResponseWrapper(localName = "createStudyCalendarResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.CreateStudyCalendarResponse")
    public com.velos.services.ResponseHolder createStudyCalendar(
        @WebParam(name = "StudyCalendar", targetNamespace = "")
        com.velos.services.StudyCalendar studyCalendar
    ) throws OperationException_Exception, OperationRolledBackException_Exception;
}
