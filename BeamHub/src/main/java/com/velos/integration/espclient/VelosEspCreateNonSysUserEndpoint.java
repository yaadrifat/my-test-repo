package com.velos.integration.espclient;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;
import com.velos.services.OperationException_Exception;

@WebService(targetNamespace = "http://velos.com/services/", name = "UserSEI")
public interface VelosEspCreateNonSysUserEndpoint {
    @WebResult(name = "Response", targetNamespace = "")
    @RequestWrapper(localName = "createNonSystemUser", targetNamespace = "http://velos.com/services/", className = "com.velos.services.CreateNonSystemUser")
    @WebMethod
    @ResponseWrapper(localName = "createNonSystemUserResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.CreateNonSystemUserResponse")
    public com.velos.services.ResponseHolder createNonSystemUser(
        @WebParam(name = "NonSystemUser", targetNamespace = "")
        com.velos.services.NonSystemUser nonSystemUser
    ) throws OperationException_Exception;
}
