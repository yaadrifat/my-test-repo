package com.velos.integration.espclient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.camel.CamelContext;
import org.apache.camel.CamelExecutionException;
import org.apache.camel.ExchangePattern;
import org.apache.camel.ProducerTemplate;
import org.apache.cxf.message.MessageContentsList;
import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.velos.integration.dao.MessageDAO;
import com.velos.integration.mapping.EndpointKeys;
import com.velos.integration.mapping.ProtocolKeys;
import com.velos.integration.mapping.VelosKeys;
import com.velos.integration.mapping.VelosStudyCalendarMapper;
import com.velos.integration.mapping.VelosStudyMapper;
import com.velos.integration.notifications.EmailNotification;
import com.velos.services.CalendarIdentifier;
import com.velos.services.Codes;
import com.velos.services.CalendarSummary;
import com.velos.services.Code;
import com.velos.services.CompletedAction;
import com.velos.services.CrudAction;
import com.velos.services.GroupIdentifier;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.Issues;
import com.velos.services.NonSystemUser;
import com.velos.services.OperationException_Exception;
import com.velos.services.OperationRolledBackException_Exception;
import com.velos.services.OrganizationIdentifier;
import com.velos.services.ResponseHolder;
import com.velos.services.Results;
import com.velos.services.SimpleIdentifier;
import com.velos.services.Study;
import com.velos.services.StudyCalendar;
import com.velos.services.StudyCalendars;
import com.velos.services.StudyCalendarsList;
import com.velos.services.StudyIdentifier;
import com.velos.services.StudyPatientFormResponse;
import com.velos.services.StudyPatientFormResponses;
import com.velos.services.StudySearch;
import com.velos.services.StudySearchResults;
import com.velos.services.StudySummary;
import com.velos.services.StudyTeamMember;
import com.velos.services.StudyTeamMembers;
import com.velos.services.User;
import com.velos.services.UserIdentifier;
import com.velos.services.UserSearch;
import com.velos.services.UserSearchResults;
import com.velos.services.UserStatus;

//@Component
public class VelosEspClientCamel{

	private static Logger logger = Logger.getLogger(VelosEspClientCamel.class);
	
	private EmailNotification emailNotification;

	private MessageDAO messageDao;

	public EmailNotification getEmailNotification() {
		return emailNotification;
	}

	public void setEmailNotification(EmailNotification emailNotification) {
		this.emailNotification = emailNotification;
	}

	public MessageDAO getMessageDao() {
		return messageDao;
	}

	public void setMessageDao(MessageDAO messageDao) {
		this.messageDao = messageDao;
	}

	public void configureCamel() {
		if (this.context != null) { return; }
		logger.info("****Getting camel context and creating Beans");
		@SuppressWarnings("resource")
		ClassPathXmlApplicationContext springContext =
		new ClassPathXmlApplicationContext("camel-config.xml");
		this.setContext((CamelContext)springContext.getBean("camel"));		
	}

	protected CamelContext context;

	public CamelContext getContext() {
		return context;
	}

	public void setContext(CamelContext context) {
		this.context = context;
	}
	
	private List codeTypeList;

	public List getCodeTypeList() {
		return codeTypeList;
	}

	public void setCodeTypeList(List codeTypeList) {
		this.codeTypeList = codeTypeList;
	}

	public Map<VelosKeys, Object> handleRequest(VelosEspMethods method, Map<VelosKeys, Object> requestMap)  {
		if (requestMap == null) { return null; }
		//this.configureCamel();

		// Call Velos WS
		switch(method) {
		case StudyGetStudySummary:
			return callGetStudySummary(requestMap);
		case StudyGetStudy:
			return callGetStudy(requestMap);
		case StudySearchOrgStudy:
			return callSearchOrgStudy(requestMap);
		case StudyCreateOrgStudy:
			return callCreateOrgStudy(requestMap);
		case StudyCalGetStudyCalendarList:
			return callGetStudyCalendarList(requestMap);
		case StudyCalGetStudyCalendar:
			return callGetStudyCalendar(requestMap);
		case StudyCalCreateStudyCalendar:
			return callCreateOrgStudyCalendar(requestMap);
		case StudyGetDocumentedBy:
			return callGetDocumentedBy(requestMap);
		default:
			break;
		}
		return null;
	}

	public Map<VelosKeys, Object> callGetStudySummary(Map<VelosKeys, Object> requestMap) {
		StudyIdentifier studyIdentifier = new StudyIdentifier();
		studyIdentifier.setStudyNumber((String)requestMap.get(ProtocolKeys.StudyNumber));

		Map<VelosKeys, Object> dataMap = null;
		StudySummary studySummary = null;
		try {
			studySummary = getStudySummary(studyIdentifier);
		} catch (OperationException_Exception e) {
			//e.printStackTrace();
			logger.info("ERROR OCCURED");
			dataMap = new HashMap<VelosKeys, Object>();
			List<Issue> issueList = e.getFaultInfo().getIssues().getIssue();
			List<String> errorList = new ArrayList<String>();
			for(Issue issue : issueList){
				logger.info(issue.getType());
				logger.info(issue.getMessage());
				dataMap.put(ProtocolKeys.FaultString, issue.getMessage());
				errorList.add(issue.getMessage());
				logger.info("Got operaton exception with issue: "+issue.getMessage());
			}
			dataMap.put(ProtocolKeys.ErrorList, errorList);
			if (issueList == null) { return null; }
			return dataMap;
		} catch (Exception e) {
			e.printStackTrace();
			return dataMap;
		}

		if (dataMap == null){
			logger.info("Executing mapper");
			VelosStudyMapper mapper = new VelosStudyMapper((String)requestMap.get(EndpointKeys.Endpoint));
			return mapper.mapStudySummary(studySummary);
		}
		return dataMap;
	}

	//Method for getting study details from  ctxpress
	public Map<VelosKeys, Object> callGetStudy(Map<VelosKeys, Object> requestMap) {

		StudyIdentifier studyIdentifier = new StudyIdentifier();
		studyIdentifier.setStudyNumber((String)requestMap.get(ProtocolKeys.StudyNumber));

		Map<VelosKeys, Object> dataMap = null;
		Study study = null;
		try {
			study = getStudy(studyIdentifier);
			logger.info("Study = "+study);
		} catch (OperationException_Exception e) {
			//e.printStackTrace();
			logger.error("ERROR OCCURED GETTING STUDY");
			dataMap = new HashMap<VelosKeys, Object>();
			List<Issue> issueList = e.getFaultInfo().getIssues().getIssue();
			List<String> errorList = new ArrayList<String>();
			for(Issue issue : issueList){
				logger.error("Issue Type = "+issue.getType());
				logger.error("Message = "+issue.getMessage());
				dataMap.put(ProtocolKeys.FaultString, issue.getMessage());
				errorList.add("Issue Type = "+issue.getType()+" Message = "+issue.getMessage());
				//logger.info("Got operaton exception with issue: "+issue.getMessage());
			}
			dataMap.put(ProtocolKeys.ErrorList, errorList);
			if (issueList == null) { return null; }
			return dataMap;
		} catch (Exception e) {
			//e.printStackTrace();
			logger.error("Error :",e);
			return dataMap;
		}

		if (dataMap == null && study != null){
			logger.info("Executing mapper");
			VelosStudyMapper mapper = new VelosStudyMapper((String)requestMap.get(EndpointKeys.Endpoint));
			return mapper.mapStudyOrgList(study,requestMap);
		}
		return dataMap;
	}

	//Method for searching a study in participating Organizations
	public Map<VelosKeys, Object> callSearchOrgStudy(Map<VelosKeys, Object> requestMap) {

		StudyIdentifier studyIdentifier = new StudyIdentifier();
		studyIdentifier.setStudyNumber((String)requestMap.get(ProtocolKeys.StudyNumber));

		String orgName = ((String)requestMap.get(ProtocolKeys.OrganizationName)).trim();
		logger.info("OrganizationName = "+orgName);

		String studyNumber = (String)requestMap.get(ProtocolKeys.StudyNumber);
		logger.info("Study Number = "+studyNumber);
		Properties prop = (Properties)requestMap.get(ProtocolKeys.PropObj);
		String packageType = (String)requestMap.get(ProtocolKeys.PackageType);
		
		String ctxStudySubType = null;		
		Map<VelosKeys, Object> dataMap = null;
		Study study = null;
		StudySearchResults studySearchResults = null;
		String studyNumberInSummary = null;
		try {
			ctxStudySubType = prop.getProperty(orgName+"_CTXStudyNumber").trim();
			logger.info("CTX Study SubType = "+ctxStudySubType);
			
			if(ctxStudySubType!=null && !"".equals(ctxStudySubType)){
				StudySearch studySearch = new StudySearch();
				studySearch.setMoreStudyDetails(studyNumber);
				studySearch.setMsdCodeSubType(ctxStudySubType);
			
				//study = getOrgStudy(studyIdentifier,orgName);
				studySearchResults = searchOrgStudy(studySearch,orgName);
				logger.info("StudySearchResults = "+studySearchResults);
				List<StudySearch> studySearchList = studySearchResults.getStudySearch();
				for(StudySearch studyresult : studySearchList){
					studyNumberInSummary = studyresult.getStudyNumber();
					logger.info("Studies found in Participating site "+orgName);
					logger.info("Study Number = "+studyNumberInSummary);
				}
				logger.info("List = "+studySearchResults.getStudySearch());
			}else{
				throw new Exception();
			}
		} catch (OperationException_Exception e) {
			//e.printStackTrace();
			logger.error("Error occured Searching study in Participating site");
			dataMap = new HashMap<VelosKeys, Object>();
			List<Issue> issueList = e.getFaultInfo().getIssues().getIssue();
			List<String> errorList = new ArrayList<String>();
			for(Issue issue : issueList){
				IssueTypes issueTypes = issue.getType();
				String value = issueTypes.value();
				logger.info("Issue value = "+value);
				logger.info("Issue Type = "+issue.getType());
				logger.info("Message = "+issue.getMessage());
				dataMap.put(ProtocolKeys.FaultString, issue.getMessage());
				if(issue.getType().value().equals("STUDY_NOT_FOUND")){
					logger.info("Adding STUDY_NOT_FOUND to Map");
					dataMap.put(ProtocolKeys.STUDY_NOT_FOUND, "STUDY_NOT_FOUND in "+orgName);
					logger.info("Study Not Found in "+orgName);
					if(packageType.equals("Amendments")){
						String error = "Study Doesn't exist in "+orgName+".Amendments cannot be sent.";
						List<String> errorList2 = new ArrayList<String>();
						errorList2.add(error);
						emailNotification.sendNotification(requestMap,"Study Amendments fails for Study Number : ",orgName,"Amendment Versions","All","Failed",errorList);
						messageDao.saveMessageDetails(studyNumber,(String)requestMap.get(ProtocolKeys.BroadCastingSite),orgName, "Amendment Versions","All","Failed",error);
					}
				}
				errorList.add(issue.getMessage());
				//logger.info("Got operaton exception with issue: "+issue.getMessage());
			}
			dataMap.put(ProtocolKeys.ErrorList, errorList);
			if (issueList == null) { return null; }
			return dataMap;
		} catch (Exception e) {
			//e.printStackTrace();
			logger.error("Error :",e);
			if(ctxStudySubType== null || "".equals(ctxStudySubType)){
				String ctxError = "CTX Study Number mapping is not Configured for participating site "+orgName;
				logger.fatal(ctxError);
				messageDao.saveMessageDetails(studyNumber,(String)requestMap.get(ProtocolKeys.BroadCastingSite),orgName, "Study",studyNumber,"Failed",ctxError);
			}
			return dataMap;
		}

		logger.info(dataMap);
		logger.info(study);

		//if (dataMap == null && study != null ){
		if (dataMap == null && studySearchResults != null ){
			logger.info("Null map = "+dataMap);
			dataMap = new HashMap<VelosKeys, Object>();
			logger.error("Study Already exists in "+orgName);
			//Check for study number in MSD then send StudyAlreadyExists
			dataMap.put(ProtocolKeys.StudyAlreadyExists,"StudyAlreadyExists in "+orgName +"-sent as package");
			
			List<String> errorList = new ArrayList<String>();
			logger.error("Study Already exists-sent as package");
			errorList.add("Study Already exists-sent as package with studyNumber = "+studyNumberInSummary);
			if(packageType.equals("StudyPackage")){
				emailNotification.sendNotification(requestMap,"study creation fails for study : ",orgName,"Study",studyNumber,"Failed",errorList);
				saveDB(requestMap,orgName,"Study",studyNumber,"Failed",errorList);
			}else{
				saveDB(requestMap,orgName,"Amendment Versions",studyNumber,"Study Already exists",errorList);
			}
			dataMap.put(ProtocolKeys.ParticipatingSite_StudyNumber,studyNumberInSummary);
			logger.info(dataMap);
			return dataMap;
		}
		return dataMap;
	}

	//Method for creating a study in participating Organizations
	public Map<VelosKeys, Object> callCreateOrgStudy(Map<VelosKeys, Object> requestMap) {

		Study study = (Study)requestMap.get(ProtocolKeys.StudyObj);

		String studyNumber = study.getStudySummary().getStudyNumber();
		String bSite = (String)requestMap.get(ProtocolKeys.BroadCastingSite);

		String orgName = ((String)requestMap.get(ProtocolKeys.OrganizationName)).trim();
		logger.info("OrganizationName = "+orgName);

		OrganizationIdentifier organizationIdentifier = new OrganizationIdentifier();
		organizationIdentifier.setSiteName(orgName);

		GroupIdentifier groupIdentifier = new GroupIdentifier();
		groupIdentifier.setGroupName("");

		UserSearch userSearch = new UserSearch();
		userSearch.setOrganization(organizationIdentifier);
		userSearch.setPageNumber(1);
		userSearch.setPageSize(1000);
		userSearch.setGroup(groupIdentifier);

		String studyCreated = null;
		Map<VelosKeys, Object> dataMap = null;

		UserSearchResults userSearchResults = null;
		try {

			userSearchResults = searchUser(userSearch);
			logger.info("Study = "+study);
			logger.info("UserSearch Results = "+userSearchResults);

			userSearchResults.getTotalCount();
			Study mappedStudy = null;

			if (study != null ){
				VelosStudyMapper mapper = new VelosStudyMapper((String)requestMap.get(EndpointKeys.Endpoint));
				mappedStudy = mapper.getMappedOrgStudy(study,requestMap,userSearchResults);
				logger.info("Mapped Study value = "+mappedStudy);
			}
			
			List<User> studyTeamUsers = (List<User>)requestMap.get(ProtocolKeys.StudyTeamUsers);
			logger.info("studyTeamUsers = "+studyTeamUsers);
			Map<Integer,User> userMap = null;
			if(studyTeamUsers!=null && !studyTeamUsers.isEmpty()){
				userMap = createStudyTeamUsers(studyTeamUsers,requestMap);
				logger.info("Final User Map = "+userMap);
				Set<Integer> oldPKSet = userMap.keySet();
				Iterator<Integer> iterator = oldPKSet.iterator();
				while(iterator.hasNext()){
					Integer oldPk = iterator.next();
					logger.info("BroadCasting Site Pk  ="+oldPk);
					User finaluser = userMap.get(oldPk);
					logger.info("Partipating Site Pk = "+finaluser.getPK());
					logger.info("User FirstName = "+finaluser.getFirstName());
			    	logger.info("User Last name = "+finaluser.getLastName());
			    	logger.info("User Email = "+finaluser.getEmail());
					
				}
				
			}
			
			StudyTeamMembers studyTeamMembers = mappedStudy.getStudyTeamMembers();
			List<StudyTeamMember> studyTeamMemberList = studyTeamMembers.getStudyTeamMember();
			StudyTeamMembers finalStudyTeamMembers = new StudyTeamMembers();
			List<StudyTeamMember> finalStudyTeamMemberList = new ArrayList<StudyTeamMember>();
			for(StudyTeamMember studyTeamMember : studyTeamMemberList){
				UserIdentifier stui = studyTeamMember.getUserId();
				logger.info("stui = "+stui);
				Integer studyTeamPk = stui.getPK();
				logger.info("******Study Team/BroadCasting Site Pk = "+studyTeamPk);
				if(userMap.get(studyTeamPk)!=null){
					User finaluser = userMap.get(studyTeamPk);
					Integer finalPk = finaluser.getPK();
					logger.info("Final Pk = "+finalPk);
					stui.setPK(finalPk);
				}
				studyTeamMember.setUserId(stui);
				finalStudyTeamMemberList.add(studyTeamMember);
				
			}
			
			finalStudyTeamMembers.getStudyTeamMember().addAll(finalStudyTeamMemberList);
			mappedStudy.setStudyTeamMembers(finalStudyTeamMembers);

			ResponseHolder responseHolder = createOrgStudy(mappedStudy, true, orgName);
			logger.info("After Calling Study Create");
			Issues issues = responseHolder.getIssues();
			Results results = responseHolder.getResults();
			if(issues!=null && !"".equals(issues)){
				List<Issue> issueList = issues.getIssue();
				if(!issueList.isEmpty()){
					logger.info("Issues Found");
					logger.info("IssueList = "+issueList);
					for(Issue issue : issueList){
						logger.info("Issue = "+issue);
						logger.info("Issue Type = "+issue.getType());
						logger.info("Issue Message = "+issue.getMessage());
					}
				}
			}
			if(results!=null){
				logger.info("Study Create Results");
				List<CompletedAction> actionList = results.getResult();
				for(CompletedAction action : actionList){
					CrudAction crudAction = action.getAction();
					logger.info("Action = "+crudAction.value());
					SimpleIdentifier si = action.getObjectId();
					logger.info("OID = "+si.getOID());
					logger.info("Pk = "+si.getPK());
					logger.info("Object name = "+action.getObjectName());
					if((si instanceof StudyIdentifier)){
						logger.info("Returned Study Number");
						StudyIdentifier sid = (StudyIdentifier)si;
						if(sid!=null){
							studyCreated = sid.getStudyNumber();
							logger.debug("AutoGenerated/ParticipatingSite Study Number = "+studyCreated);
						}
					}
				}
				messageDao.saveMessageDetails(studyNumber,bSite,orgName, "Study",studyNumber,"Created", "Study Created = "+studyCreated);
			}
			logger.info("Study Created Successfully");
		} catch (OperationException_Exception e) {
			//e.printStackTrace();
			logger.info("ERROR OCCURED Creating Study in Participating org = "+orgName);
			dataMap = new HashMap<VelosKeys, Object>();
			List<Issue> issueList = e.getFaultInfo().getIssues().getIssue();
			List<String> errorList = new ArrayList<String>();
			for(Issue issue : issueList){
				logger.info("Issue Type = "+issue.getType());//This will give Study Already Exists Message Need to add to "dataMap"
				logger.info("Issue Message = "+issue.getMessage());
				dataMap.put(ProtocolKeys.FaultString, issue.getMessage());
				
				if(issue.getType().value().equals("STUDY_NUMBER_EXISTS")){
					
					//dataMap.put(ProtocolKeys.StudyAlreayExists, "Study Already Exists "+orgName);
					dataMap.put(ProtocolKeys.FaultString, "Study Already exists");
					logger.fatal("Study Already exists");
					errorList.add("Study Already exists");
				}
				if(issue.getType().value().equals("STUDY_NUMBER_NOT_FOUND")){
					String autogenError = "StudyNumber AutoGeneration is not enabled for Participating Site : "+orgName;
					logger.fatal("StudyNumber AutoGeneration is enabled at Interface end.");
					logger.fatal(autogenError);
					errorList.add(autogenError);
				}
				errorList.add(issue.getMessage());
				logger.error("Got operaton exception with issue: "+issue.getMessage());
			}
			dataMap.put(ProtocolKeys.ErrorList, errorList);
			if (issueList == null) { return null; }

			logger.info("Study Failed = "+studyNumber);
			logger.info("Study Creation Failed for organization  "+orgName +"="+studyNumber);

			emailNotification.sendNotification(requestMap,"study creation fails for study : ",orgName,"Study",studyNumber,"Failed",errorList);
			saveDB(requestMap,orgName,"Study",studyNumber,"Failed",errorList);

			return dataMap;
		} catch (Exception e) {
			logger.info("Data Map = "+dataMap);
			//e.printStackTrace();
			logger.error("Error :",e);
			if(e.getMessage().equals("DataManager Not Found")){
				String errorMsg = "Add a user with role DataManager to the StudyTeam for the participating Site "+orgName;
				List<String> errorList = new ArrayList<String>();
				errorList.add(errorMsg);
				emailNotification.sendNotification(requestMap,"study creation fails for study : ",orgName,"Study",studyNumber,"Failed",errorList);
				messageDao.saveMessageDetails(studyNumber,(String)requestMap.get(ProtocolKeys.BroadCastingSite),orgName, "Study",studyNumber,"Failed", errorMsg);
			}else{
				messageDao.saveMessageDetails(studyNumber,(String)requestMap.get(ProtocolKeys.BroadCastingSite),orgName, "Study",studyNumber,"Failed", e.getMessage());
			}
			return dataMap;
		}
		
		dataMap = new HashMap<VelosKeys, Object>();
		dataMap.put(ProtocolKeys.STUDY_CREATED,"study created");
		dataMap.put(ProtocolKeys.ParticipatingSite_StudyNumber, studyCreated);
		logger.info("Create org Map = "+dataMap);
		return dataMap;
	}

	//Method for getting email id for DocumentedBy user
	public Map<VelosKeys, Object> callGetDocumentedBy(Map<VelosKeys, Object> requestMap) {

		String studyNum = (String)requestMap.get(ProtocolKeys.StudyNumber);
				
		String firstName = ((String)requestMap.get(ProtocolKeys.DocumentedByFirstName)).trim();
		logger.info("firstName = "+firstName);

		String lastName = ((String)requestMap.get(ProtocolKeys.DocumentedByLastName)).trim();
		logger.info("lastName = "+lastName);
		
		String loginName = ((String)requestMap.get(ProtocolKeys.DocumentedByLoginName)).trim();
		logger.info("DocumnetedBy LoginName = "+loginName);

		GroupIdentifier groupIdentifier = new GroupIdentifier();
		groupIdentifier.setGroupName("");

		UserSearch userSearch = new UserSearch();
		userSearch.setFirstName(firstName);
		userSearch.setLastName(lastName);
		userSearch.setLoginName(loginName);
		userSearch.setPageNumber(1);
		userSearch.setPageSize(1000);
		userSearch.setGroup(groupIdentifier);

		Map<VelosKeys, Object> dataMap = null;

		UserSearchResults userSearchResults = null;
		try {

			userSearchResults = searchUser(userSearch);
			logger.info("UserSearch Results = "+userSearchResults);

			Long totalCount = userSearchResults.getTotalCount();
			logger.info("Total Count = "+totalCount);
			logger.info("Page Size = "+userSearchResults.getPageSize());

			String emailErrorMsg = null;
			if(totalCount == 0){
				emailErrorMsg = "No user found for the search criteria to get Email ID";
				logger.error(emailErrorMsg);
				messageDao.saveMessageDetails(studyNum,"null","null", "DocumentedByEmailID","null","Getting Email Failed", emailErrorMsg);
			}
			if(totalCount == 1){
				dataMap = new HashMap<VelosKeys, Object>();
				List<User> users = userSearchResults.getUser();
				for(User user:users){
					logger.info("*****Email Users*****");
					logger.info("user = "+user);
					logger.info("User Pk = "+user.getPK());
					logger.info("User FirstName = "+user.getFirstName());
					logger.info("User Last name = "+user.getLastName());
					logger.info("User Login = "+user.getUserLoginName());
					logger.info("User Org = "+user.getOrganization().getSiteName());
					String userMail = user.getEmail();
					logger.info("User Email = "+userMail);
					logger.info("***************************");
					if(!"".equals(userMail) && !"null".equals(userMail) && userMail!=null){
						dataMap.put(ProtocolKeys.EmailID,userMail);
					}
					else{
						emailErrorMsg = "There is no email id for the user DocumentedBy";
						logger.fatal(emailErrorMsg);
						messageDao.saveMessageDetails(studyNum,"null","null", "DocumentedByEmailID","null","Getting Email Failed", emailErrorMsg);
					}
					logger.info("DataMap = "+dataMap);
				}
			}
			if(totalCount >= 2){
				emailErrorMsg = "Multiple users found for the search criteria for DocumentedBy Users";
				logger.fatal(emailErrorMsg);
				messageDao.saveMessageDetails(studyNum,"null","null", "DocumentedByEmailID","null","Getting Email Failed", emailErrorMsg);
			}
			

		} catch (OperationException_Exception e) {
			//e.printStackTrace();
			logger.info("ERROR OCCURED Getting Email ID for Documented By");
			dataMap = new HashMap<VelosKeys, Object>();
			List<Issue> issueList = e.getFaultInfo().getIssues().getIssue();
			List<String> errorList = new ArrayList<String>();
			for(Issue issue : issueList){
				IssueTypes issueTypes = issue.getType();
				String value = issueTypes.value();
				logger.info("Issue value = "+value);
				logger.info("Issue Type = "+issue.getType());
				logger.info("Message = "+issue.getMessage());
				dataMap.put(ProtocolKeys.FaultString, issue.getMessage());

				errorList.add(issue.getMessage());
				logger.info("Got operaton exception with issue: "+issue.getMessage());
			}
			dataMap.put(ProtocolKeys.ErrorList, errorList);
			if (issueList == null) { return null; }
			return dataMap;
		} catch (Exception e) {
			//e.printStackTrace();
			logger.error("Error :",e);
			return dataMap;
		}

		logger.info("Email dataMap = "+dataMap);
		return dataMap;
	}


	//Method for getting list of study calendars
	public Map<VelosKeys, Object> callGetStudyCalendarList(Map<VelosKeys, Object> requestMap) {

		StudyIdentifier studyIdentifier = new StudyIdentifier();
		studyIdentifier.setStudyNumber((String)requestMap.get(ProtocolKeys.StudyNumber));

		Properties prop = (Properties)requestMap.get(ProtocolKeys.PropObj);
		String calStatusSubType = prop.getProperty("calendar.status.subtype").trim();
		String calStatusTypep = prop.getProperty("calendar.status.type").trim();
		logger.info("Cal Status SubType = "+calStatusSubType);
		logger.info("Cal Status Type = "+calStatusTypep);

		Map<VelosKeys, Object> dataMap = null;
		StudyCalendarsList studyCalendars = null;
		List<String> calendarsList = null;
		try {
			studyCalendars = getStudyCalendarList(studyIdentifier);
			logger.info("StudyCalendars = "+studyCalendars);
			if(studyCalendars!=null){
				calendarsList = new ArrayList<String>();
				List<StudyCalendars> studyCalList =	studyCalendars.getStudyCalendars();
				for(StudyCalendars studyCal : studyCalList){
					logger.info("-----------------------------------------------");
					String calName = studyCal.getCalendarName();
					logger.info("Cal Name = "+calName);
					Code calStatusCode = studyCal.getCalendarStatus();
					logger.info("Code = "+calStatusCode);
					if(calStatusCode!=null){
						String code = calStatusCode.getCode();
						logger.info("code value = "+code);
						String calStatus = calStatusCode.getDescription();
						logger.info("calStatus = "+calStatus);
						String calStatusType = calStatusCode.getType();
						logger.info("calStatusType = "+calStatusType);
						if(code.equals(calStatusSubType) && calStatusType.equals(calStatusTypep)){
							calendarsList.add(calName);
						}
						logger.info("-----------------------------------------------");
					}
				}
			}

		} catch (OperationException_Exception e) {
			//e.printStackTrace();
			logger.info("ERROR OCCURED GETTING STUDY CALENDAR LIST");
			dataMap = new HashMap<VelosKeys, Object>();
			List<Issue> issueList = e.getFaultInfo().getIssues().getIssue();
			List<String> errorList = new ArrayList<String>();
			for(Issue issue : issueList){
				logger.info("Issue Type = "+issue.getType());
				logger.info("Message = "+issue.getMessage());
				dataMap.put(ProtocolKeys.FaultString, issue.getMessage());
				errorList.add("Issue Type = "+issue.getType()+" Message = "+issue.getMessage());
				logger.info("Got operaton exception with issue: "+issue.getMessage());
			}
			dataMap.put(ProtocolKeys.ErrorList, errorList);
			if (issueList == null) { return null; }
			return dataMap;
		} catch (Exception e) {
			//e.printStackTrace();
			logger.error("Error :",e);
			return dataMap;
		}

		logger.info("Calendars Final List = "+calendarsList);
		if(calendarsList!=null && !calendarsList.isEmpty()){
			dataMap = new HashMap<VelosKeys, Object>();
			dataMap.put(ProtocolKeys.CalendarNameList,calendarsList);

		}else{
			logger.info("No calendars with the specified Status");
			dataMap = new HashMap<VelosKeys, Object>();
		}
		logger.info("Calendars List  DataMap = "+dataMap);
		return dataMap;
	}

	//Method for getting calendar data
	public Map<VelosKeys, Object> callGetStudyCalendar(Map<VelosKeys, Object> requestMap) {

		CalendarIdentifier calendarIdentifier = null ;

		StudyIdentifier studyIdentifier = new StudyIdentifier();
		studyIdentifier.setStudyNumber((String)requestMap.get(ProtocolKeys.StudyNumber));

		List<String> calNameList = (List<String>)requestMap.get(ProtocolKeys.CalendarNameList);
		logger.info("Calendars List to get data = "+calNameList);

		String gettingCal = null;
		Map<VelosKeys, Object> dataMap = null;
		StudyCalendar studyCal = null;
		List<StudyCalendar> calendarsList = new ArrayList<StudyCalendar>();

		try {
			for(String calName : calNameList){
				logger.info("Getting data for calendar = "+calName);
				gettingCal = calName;
				try{
					studyCal = getStudyCalendar(calendarIdentifier,studyIdentifier,calName);
					logger.info("StudyCalendar = "+studyCal);
					CalendarSummary calSum = studyCal.getCalendarSummary();
					String  Calename = calSum.getCalendarName();
					logger.info("calendar Name = "+Calename);
					String Caledesc = calSum.getCalendarDescription();
					logger.info("calendar desc = "+Caledesc);
					calendarsList.add(studyCal);
				}catch (OperationException_Exception e) {
					//e.printStackTrace();
					logger.info("ERROR OCCURED getting study calendar");
					dataMap = new HashMap<VelosKeys, Object>();
					List<Issue> issueList = e.getFaultInfo().getIssues().getIssue();
					List<String> errorList = new ArrayList<String>();
					for(Issue issue : issueList){
						logger.info(issue.getType());
						//logger.info(issue.getMessage());
						dataMap.put(ProtocolKeys.FaultString, issue.getMessage());
						errorList.add(issue.getMessage());
						logger.info("Got operaton exception with issue: "+issue.getMessage());
					}
					dataMap.put(ProtocolKeys.ErrorList, errorList);
					if (issueList == null) { return null; }
					logger.info("GetCalendar  Failed = "+gettingCal);

					saveDB(requestMap,"","Calendar",gettingCal,"Getting Calendar Failed",errorList);
					//return dataMap;
				}

			}
		} catch (Exception e) {
			//e.printStackTrace();
			logger.error("Error :",e);
			//return dataMap;
		}
		logger.info("Calendars = "+calendarsList);
		if(calendarsList!=null){
			dataMap = new HashMap<VelosKeys, Object>();
			dataMap.put(ProtocolKeys.CalObjList,calendarsList);
		}
		logger.info("CalObjList Map = "+dataMap);
		return dataMap;

	}


	public Map<VelosKeys, Object> callCreateOrgStudyCalendar(Map<VelosKeys, Object> requestMap) {

		String orgName = ((String)requestMap.get(ProtocolKeys.OrganizationName)).trim();
		logger.info("OrganizationName = "+orgName);

		String calCreating = null;
		@SuppressWarnings("unchecked")
		List<StudyCalendar> studyCalObjList = (List<StudyCalendar>)requestMap.get(ProtocolKeys.CalObjList);
		
		
		logger.info("StudyCal Object Final List = "+studyCalObjList);
		
		List<String> calFailedList = new ArrayList<String>();
		List<String> calCreatedList = new ArrayList<String>();
		Map<VelosKeys, Object> dataMap = null;
		try {

			VelosStudyCalendarMapper studyCalMapper = new VelosStudyCalendarMapper((String)requestMap.get(EndpointKeys.Endpoint));
			Map<VelosKeys, Object> mappedCals = studyCalMapper.mapOrgStudyCalList(studyCalObjList,requestMap);
			List<StudyCalendar> mappedCalObjList = (List<StudyCalendar>)mappedCals.get(ProtocolKeys.CalObjList);
			logger.info("Mapped CalObjList = "+mappedCalObjList);

			for(StudyCalendar studyCalendar : mappedCalObjList){

				String calName = studyCalendar.getCalendarSummary().getCalendarName();

				calCreating = calName;
				logger.info("****Creating Calendar = "+calName);
				try{
					ResponseHolder responseHolder = createOrgStudyCalendar(studyCalendar,orgName);
					logger.info("After Calling Create Org Study Calendar");
					Issues issues = responseHolder.getIssues();
					Results results = responseHolder.getResults();
					if(issues!=null && !"".equals(issues)){

						List<Issue> issueList = issues.getIssue();
						if(!issueList.isEmpty()){
							logger.info("Issues Found");

							logger.info("IssueList = "+issueList);
							for(Issue issue : issueList){
								logger.info("Issue = "+issue);
								logger.info("Issue Type = "+issue.getType());
								logger.info("Issue Message = "+issue.getMessage());
							}
						}
					}
					if(results!=null){
						logger.info("Study Calendar Create Results");
						List<CompletedAction> actionList = results.getResult();
						for(CompletedAction action : actionList){
							CrudAction crudAction = action.getAction();
							//logger.info("Action = "+crudAction.value());
							SimpleIdentifier si = action.getObjectId();
							//logger.info("OID = "+si.getOID());
							//logger.info("Pk = "+si.getPK());
							//logger.info("Object name = "+action.getObjectName());
						}
						String bSite = (String)requestMap.get(ProtocolKeys.BroadCastingSite);
						String studyNum = (String)requestMap.get(ProtocolKeys.StudyNumber);

						calCreatedList.add(orgName+"_"+calCreating);
						messageDao.saveMessageDetails(studyNum,bSite,orgName, "Calendar",calCreating,"Created", "Calendar Created");

					}

					logger.info("Study Calendar Created Successfully");
				}catch (OperationException_Exception e) {
					//e.printStackTrace();
					logger.info("ERROR OCCURED creating study calendar");
					dataMap = new HashMap<VelosKeys, Object>();
					List<Issue> issueList = e.getFaultInfo().getIssues().getIssue();
					List<String> errorList = new ArrayList<String>();
					for(Issue issue : issueList){
						logger.info(issue.getType());
						logger.info(issue.getMessage());
						dataMap.put(ProtocolKeys.FaultString, issue.getMessage());
						//errorList.add("Issue Type = "+issue.getType()+". Message = "+issue.getMessage());
						logger.info("Issue Type = "+issue.getType()+". Message = "+issue.getMessage());
						errorList.add(issue.getMessage());
						logger.info("Got operaton exception with issue: "+issue.getMessage());
					}
					dataMap.put(ProtocolKeys.ErrorList, errorList);
					if (issueList == null) { return null; }
					calFailedList.add(orgName+"_"+calCreating);
					logger.info("Calendar Failed = "+calCreating);
					logger.info("Calendar Creation Failed for organization  "+orgName +"="+calCreating);

					emailNotification.sendNotification(requestMap,calCreating+" creation fails for study : ",orgName,"Calendar",calCreating,"Failed",errorList);
					saveDB(requestMap,orgName,"Calendar",calCreating,"Failed",errorList);

				}finally{
					System.out.println("Finally");
				}
			}
		} catch (Exception e) {
			//e.printStackTrace();
			logger.error("Error :",e);
			messageDao.saveMessageDetails((String)requestMap.get(ProtocolKeys.StudyNumber),(String)requestMap.get(ProtocolKeys.BroadCastingSite),orgName, "Calendar",calCreating,"Failed", e.getMessage());
			return dataMap;
		}
		requestMap.put(ProtocolKeys.CALENDER_CREATED,"Calendar_Created");
		requestMap.put(ProtocolKeys.CalendarFailedList, calFailedList);
		requestMap.put(ProtocolKeys.CalendarCreatedList, calCreatedList);
		logger.info("Created all Study calendars");
		return dataMap;
	}

	public StudySummary getStudySummary(StudyIdentifier request)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList list = null;
		try{
			list = (MessageContentsList) producer.sendBody("cxf:bean:espGetSummaryEndpoint", 
					ExchangePattern.InOut, request);
		}catch(CamelExecutionException e) {
			logger.info(e.getCause());
			logger.info(e.getMessage());
			if (e.getCause() instanceof OperationException_Exception) {
				logger.info("throwing Exception");
				throw(OperationException_Exception)e.getCause();
			}
			e.printStackTrace();
			return null;
		}
		logger.info("Returned Summary");
		return (StudySummary) list.get(0);
	}

	public Study getStudy(StudyIdentifier request)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		logger.info("Getting study from ctxpress");
		MessageContentsList list = null;
		try{
			list = (MessageContentsList) producer.sendBody("cxf:bean:espGetStudyEndpoint", 
					ExchangePattern.InOut, request);
		}catch(CamelExecutionException e) {
			logger.info(e.getCause());
			logger.info(e.getMessage());
			if (e.getCause() instanceof OperationException_Exception) {
				logger.info("throwing Exception");
				throw(OperationException_Exception)e.getCause();
			}
			e.printStackTrace();
			return null;
		}
		logger.info("Returned Study");
		return (Study) list.get(0);
	}

	//Method for getting study details/searching study in participating Organization
	public Study getOrgStudy(StudyIdentifier request,String orgName)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		logger.info("Get study from "+orgName);
		MessageContentsList list = null;
		try{
			list = (MessageContentsList) producer.sendBody("cxf:bean:espGet"+orgName+"StudyEndpoint", 
					ExchangePattern.InOut, request);
		}catch(CamelExecutionException e) {
			logger.info(e.getCause());
			logger.info(e.getMessage());
			if (e.getCause() instanceof OperationException_Exception) {
				logger.info("throwing Exception");
				throw(OperationException_Exception)e.getCause();
			}
			e.printStackTrace();
			return null;
		}catch (Exception e) {
			//e.printStackTrace();
			logger.error("Error :",e);
			return null;
		}
		logger.info("Returned Organization Study");
		return (Study) list.get(0);
	}

	public ResponseHolder createStudy(Study study,boolean createNonSystemUsers)
			throws OperationException_Exception,OperationRolledBackException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList inpList = new MessageContentsList();
		inpList.add(study);
		inpList.add(createNonSystemUsers);
		MessageContentsList list = null;
		try{
			list = (MessageContentsList) producer.sendBody("cxf:bean:espCreateStudyEndpoint", 
					ExchangePattern.InOut,inpList);
		}catch(CamelExecutionException e) {
			logger.info(e.getCause());
			logger.info(e.getMessage());
			if (e.getCause() instanceof OperationException_Exception) {
				logger.info("throwing Exception");
				throw(OperationException_Exception)e.getCause();
			}
			e.printStackTrace();
			return null;
		}
		logger.info("Created Study");
		return (ResponseHolder) list.get(0);
	}

	//Method for creating  a study in participating Organization 
	public ResponseHolder createOrgStudy(Study study,boolean createNonSystemUsers,String orgName)
			throws OperationException_Exception,OperationRolledBackException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList inpList = new MessageContentsList();
		inpList.add(study);
		inpList.add(createNonSystemUsers);
		MessageContentsList list = null;
		try{
			list = (MessageContentsList) producer.sendBody(orgName+"_espCreateStudyEndpoint", 
					ExchangePattern.InOut,inpList);
		}catch(CamelExecutionException e) {
			logger.info(e.getCause());
			logger.info(e.getMessage());
			if (e.getCause() instanceof OperationException_Exception) {
				logger.info("throwing Exception");
				throw(OperationException_Exception)e.getCause();
			}
			e.printStackTrace();
			return null;
		}
		logger.info("Created Study in Org = "+orgName);
		return (ResponseHolder) list.get(0);
	}

	//Returns the list of all users based on the Organization
	public com.velos.services.UserSearchResults searchUser(UserSearch userSearch) throws OperationException_Exception{
		ProducerTemplate producer = context.createProducerTemplate();
		logger.info("Getting User from ctxpress");
		MessageContentsList list = null;
		try{
			list = (MessageContentsList) producer.sendBody("cxf:bean:espSearchUserEndpoint", 
					ExchangePattern.InOut, userSearch);
		}catch(CamelExecutionException e) {
			logger.info(e.getCause());
			logger.info(e.getMessage());
			if (e.getCause() instanceof OperationException_Exception) {
				logger.info("throwing Exception");
				throw(OperationException_Exception)e.getCause();
			}
			e.printStackTrace();
			return null;
		}
		logger.info("Returned Users based on organization");
		return (UserSearchResults) list.get(0);
	}

	//Method for getting all Calendars for a study
	public com.velos.services.StudyCalendarsList getStudyCalendarList(StudyIdentifier request) throws OperationException_Exception{
		ProducerTemplate producer = context.createProducerTemplate();
		logger.info("Getting list of calendars for a study from ctxpress");
		MessageContentsList list = null;
		try{
			list = (MessageContentsList) producer.sendBody("cxf:bean:espGetStudyCalendarListEndpoint", 
					ExchangePattern.InOut, request);
		}catch(CamelExecutionException e) {
			logger.info(e.getCause());
			logger.info(e.getMessage());
			if (e.getCause() instanceof OperationException_Exception) {
				logger.info("throwing Exception");
				throw(OperationException_Exception)e.getCause();
			}
			e.printStackTrace();
			return null;
		}
		logger.info("Returned All Study calendars list");
		return (StudyCalendarsList) list.get(0);

	}

	//Method for getting study calendar data
	public com.velos.services.StudyCalendar getStudyCalendar(CalendarIdentifier calendarIdentifier,StudyIdentifier studyIdentifier,String calendarName) throws OperationException_Exception{

		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList inpList = new MessageContentsList();
		inpList.add(calendarIdentifier);
		inpList.add(studyIdentifier);
		inpList.add(calendarName);
		MessageContentsList list = null;
		try{
			list = (MessageContentsList) producer.sendBody("cxf:bean:espGetStudyCalendarEndpoint", 
					ExchangePattern.InOut,inpList);
		}catch(CamelExecutionException e) {
			logger.info(e.getCause());
			logger.info(e.getMessage());
			if (e.getCause() instanceof OperationException_Exception) {
				logger.info("throwing Exception Getting Study Cal");
				throw(OperationException_Exception)e.getCause();
			}
			e.printStackTrace();
			return null;
		}
		logger.info("Returned Study Calendar");
		return (StudyCalendar) list.get(0);
	}

	//Method for creating a study calendar
	public com.velos.services.ResponseHolder createStudyCalendar(StudyCalendar studyCalendar) throws OperationException_Exception, OperationRolledBackException_Exception{
		ProducerTemplate producer = context.createProducerTemplate();
		logger.info("Creating  calendar for a study");
		MessageContentsList list = null;
		try{
			list = (MessageContentsList) producer.sendBody("cxf:bean:espCreateStudyCalendarEndpoint", 
					ExchangePattern.InOut, studyCalendar);
		}catch(CamelExecutionException e) {
			logger.info(e.getCause());
			logger.info(e.getMessage());
			if (e.getCause() instanceof OperationException_Exception) {
				logger.info("throwing Exception");
				throw(OperationException_Exception)e.getCause();
			}
			e.printStackTrace();
			return null;
		}
		logger.info("Created Study calendar");
		return (ResponseHolder) list.get(0);
	}


	//Method for creating  a studycalendar in participating Organization 
	public ResponseHolder createOrgStudyCalendar(StudyCalendar studyCalendar,String orgName)
			throws OperationException_Exception,OperationRolledBackException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		logger.info("Creating Study calendar for org = "+orgName);
		MessageContentsList list = null;
		try{
			list = (MessageContentsList) producer.sendBody(orgName+"_espCreateStudyCalendarEndpoint", 
					ExchangePattern.InOut, studyCalendar);
		}catch(CamelExecutionException e) {
			logger.info(e.getCause());
			logger.info(e.getMessage());
			if (e.getCause() instanceof OperationException_Exception) {
				logger.info("throwing Exception");
				throw(OperationException_Exception)e.getCause();
			}
			e.printStackTrace();
			return null;
		}
		logger.info("Created Study calendar in Org = "+orgName);
		return (ResponseHolder) list.get(0);
	}

	/* @Bean
	public WSS4JOutInterceptor wssInterceptor() {
		Properties prop = new Properties();
		try {
			prop.load(this.getClass().getClassLoader()
					.getResourceAsStream("config.properties"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Map<String, Object> props = new HashMap<String, Object>();
		props.put(WSHandlerConstants.ACTION, WSHandlerConstants.USERNAME_TOKEN);
		props.put(WSHandlerConstants.PASSWORD_TYPE, WSConstants.PW_TEXT);
		props.put(WSHandlerConstants.USER, prop.getProperty("velos.userID"));
		props.put(WSHandlerConstants.PW_CALLBACK_CLASS,
				SimpleAuthCallbackHandler.class.getName());
		WSS4JOutInterceptor wssBean = new WSS4JOutInterceptor(props);
		return wssBean;
	}
	 */

	public void saveDB(Map<VelosKeys, Object> requestMap,String participatingSite,String componentType,String componentName,String status,List<String> errorList){

		logger.info("saveDB()");
		String bSite = (String)requestMap.get(ProtocolKeys.BroadCastingSite);
		String studyNum = (String)requestMap.get(ProtocolKeys.StudyNumber);
		String errorMsg = "";
		StringBuffer sb = null;
		if(errorList!=null && !errorList.isEmpty()){
			sb = new StringBuffer();
			for(String error : errorList){
				sb.append(error);
				sb.append("\n");
			}
		}
		if(sb!=null){
			errorMsg = sb.toString();
		}
		messageDao.saveMessageDetails(studyNum,bSite,participatingSite, componentType,componentName,status,errorMsg);
		//messageDao.saveMessageDetails(studyNum,bSite,orgName, "Calendar",calCreating,"Failed", errorList.toString(), "Test User");
	}

	//Method for searching study
	public StudySearchResults searchStudy(StudySearch studySearch)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList list = null;
		try{
			list = (MessageContentsList) producer.sendBody("espSearchStudyEndpoint", 
					ExchangePattern.InOut, studySearch);
		}catch(CamelExecutionException e) {
			logger.info(e.getCause());
			logger.info(e.getMessage());
			if (e.getCause() instanceof OperationException_Exception) {
				logger.info("throwing Exception");
				throw(OperationException_Exception)e.getCause();
			}
			e.printStackTrace();
			return null;
		}catch (Exception e) {
			//e.printStackTrace();
			logger.error("Error :",e);
			return null;
		}
		logger.info("Search Study");
		return (StudySearchResults) list.get(0);
	}

	//Method for searching study in participating sites
	public StudySearchResults searchOrgStudy(StudySearch studySearch,String orgName)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList list = null;
		try{
			list = (MessageContentsList) producer.sendBody(orgName+"_espSearchStudyEndpoint", 
					ExchangePattern.InOut, studySearch);
		}catch(CamelExecutionException e) {
			logger.info(e.getCause());
			logger.info(e.getMessage());
			if (e.getCause() instanceof OperationException_Exception) {
				logger.info("throwing Exception");
				throw(OperationException_Exception)e.getCause();
			}
			e.printStackTrace();
			return null;
		}catch (Exception e) {
			//e.printStackTrace();
			logger.error("Error :",e);
			return null;
		}
		logger.info("Search Study");
		return (StudySearchResults) list.get(0);
	}
	
public Map<Integer,User> createStudyTeamUsers(List<User> studyTeamUsers,Map<VelosKeys, Object> requestMap){
	
	String studyNumber = (String)requestMap.get(ProtocolKeys.StudyNumber);
	logger.info("Study Number = "+studyNumber);
	
	String orgName = ((String)requestMap.get(ProtocolKeys.OrganizationName)).trim();
	logger.info("OrganizationName = "+orgName);
	
	String bSite = (String)requestMap.get(ProtocolKeys.BroadCastingSite);
	
	Map<Integer,User> userMap = new HashMap<Integer,User>();
	
		for(User user:studyTeamUsers){
			logger.info("*****Study Users*****");
	    	logger.info("user = "+user);
	    	Integer userPK = user.getPK();
	    	logger.info("User Pk = "+userPK);
	    	logger.info("User FirstName = "+user.getFirstName());
	    	logger.info("User Last name = "+user.getLastName());
	    	logger.info("User Login = "+user.getUserLoginName());
	    	logger.info("User Org = "+user.getOrganization().getSiteName());
	    	logger.info("User Email = "+user.getEmail());
	    	logger.info("***************************");
	    	
	    	OrganizationIdentifier organizationIdentifier = new OrganizationIdentifier();
			organizationIdentifier.setSiteName(orgName);
	    	
	    	GroupIdentifier groupIdentifier = new GroupIdentifier();
			groupIdentifier.setGroupName("");

			UserSearch userSearch = new UserSearch();
			String firstName = user.getFirstName();
			userSearch.setFirstName(firstName);
			String lastName = user.getLastName();
			userSearch.setLastName(lastName);
			String email = user.getEmail();
			userSearch.setEmail(email);
			userSearch.setOrganization(organizationIdentifier);
			userSearch.setPageNumber(1);
			userSearch.setPageSize(1000);
			userSearch.setGroup(groupIdentifier);
	    	
	    	    	
	    	UserSearchResults userSearchResults = null;
	    	int searchUserPk = 0;
			try {

				userSearchResults = searchOrgUser(userSearch,orgName);
				logger.info("Org UserSearch Results = "+userSearchResults);

				Long totalCount = userSearchResults.getTotalCount();
				logger.info("Total Count = "+totalCount);
				logger.info("Page Size = "+userSearchResults.getPageSize());

				String emailErrorMsg = null;
				if(totalCount == 0){
					emailErrorMsg = "No user found for the search criteria in participating site = "+orgName;
					logger.error(emailErrorMsg);
					messageDao.saveMessageDetails(studyNumber,bSite,orgName, "SearchUser","FirstName="+firstName+"\nLastName="+lastName+"\nEmail="+email,"UserNotFound", emailErrorMsg);
					
					int createUserPk = createOrgNonSysUser(user,requestMap);
					if(createUserPk!=0){
						user.setPK(createUserPk);
						userMap.put(userPK,user);
					}
					
				}
				if(totalCount == 1){
					List<User> searchUserList = userSearchResults.getUser();
					for(User searchUser : searchUserList){
						searchUserPk = searchUser.getPK();
					}
					emailErrorMsg = "User exists in participating site with pk = "+searchUserPk;
					logger.fatal(emailErrorMsg);
					messageDao.saveMessageDetails(studyNumber,bSite,orgName, "SearchUser","FirstName="+firstName+"\nLastName="+lastName+"\nEmail="+email,"UserAlreadyExists", emailErrorMsg);
					user.setPK(searchUserPk);
					
					logger.info("Final User withPK = "+user.getPK() );
			    	logger.info("User FirstName = "+user.getFirstName());
			    	logger.info("User Last name = "+user.getLastName());
			    	logger.info("User Email = "+user.getEmail());
					userMap.put(userPK, user);
				}
				if(totalCount >= 2){
					emailErrorMsg = "Multiple Users exists in participating site = "+orgName;
					logger.fatal(emailErrorMsg);
					messageDao.saveMessageDetails(studyNumber,bSite,orgName, "SearchUser","FirstName="+firstName+"\nLastName="+lastName+"\nEmail="+email,"Multiple Users Found", emailErrorMsg);
				}
				

			} catch (OperationException_Exception e) {
				//e.printStackTrace();
				logger.info("ERROR OCCURED Searching  User in participating site = "+orgName);
				List<Issue> issueList = e.getFaultInfo().getIssues().getIssue();
				List<String> errorList = new ArrayList<String>();
				for(Issue issue : issueList){
					IssueTypes issueTypes = issue.getType();
					String value = issueTypes.value();
					logger.info("Issue value = "+value);
					logger.info("Issue Type = "+issue.getType());
					logger.info("Message = "+issue.getMessage());
					errorList.add(issue.getMessage());
					logger.info("Got operaton exception with issue: "+issue.getMessage());
				}
							
			} catch (Exception e) {
				//e.printStackTrace();
				logger.error("Error :",e);
				
			}
		}
		
		return userMap;
	}
	
	//Method for searching users in participating sites
	public com.velos.services.UserSearchResults searchOrgUser(UserSearch userSearch,String orgName) throws OperationException_Exception{
		ProducerTemplate producer = context.createProducerTemplate();
		logger.info("Searching User in Site = "+orgName);
		MessageContentsList list = null;
		try{
			list = (MessageContentsList) producer.sendBody(orgName+"_espSearchUserEndpoint", 
					ExchangePattern.InOut, userSearch);
		}catch(CamelExecutionException e) {
			logger.info(e.getCause());
			logger.info(e.getMessage());
			if (e.getCause() instanceof OperationException_Exception) {
				logger.info("throwing Exception");
				throw(OperationException_Exception)e.getCause();
			}
			e.printStackTrace();
			return null;
		}
		logger.info("Returned Users based on organization");
		return (UserSearchResults) list.get(0);
	}
	
	//Method for creating NonSysUser in Participating site
	public int createOrgNonSysUser(User user,Map<VelosKeys, Object> requestMap){
		
		String studyNumber = (String)requestMap.get(ProtocolKeys.StudyNumber);
		logger.info("Study Number = "+studyNumber);
		
		String orgName = ((String)requestMap.get(ProtocolKeys.OrganizationName)).trim();
		logger.info("OrganizationName = "+orgName);
	
		int createUserPk = 0;
		
		try {
			
			OrganizationIdentifier organizationIdentifier = new OrganizationIdentifier();
			organizationIdentifier.setSiteName(orgName);
	    	
			NonSystemUser nonSystemUser = new NonSystemUser();

			nonSystemUser.setFirstName(user.getFirstName());
			nonSystemUser.setLastName(user.getLastName());
			nonSystemUser.setEmail(user.getEmail());
			nonSystemUser.setOrganization(organizationIdentifier);
			nonSystemUser.setUserStatus(UserStatus.ACTIVE);
			
			ResponseHolder responseHolder = createNonSysUser(nonSystemUser,orgName);
			
			Issues issues = responseHolder.getIssues();
			Results results = responseHolder.getResults();
			
			if(results!=null){
				logger.info("NonSysUser Create Results");
				List<CompletedAction> actionList = results.getResult();
				if(!actionList.isEmpty()){
					for(CompletedAction action : actionList){
						CrudAction crudAction = action.getAction();
						logger.info("Action = "+crudAction.value());
						SimpleIdentifier si = action.getObjectId();
						logger.info("OID = "+si.getOID());
						createUserPk = si.getPK();
						logger.info("Pk = "+createUserPk);
						logger.info("Object name = "+action.getObjectName());
						messageDao.saveMessageDetails(studyNumber,"null",orgName, "NonSystemUser","FirstName="+user.getFirstName()+"\nLastName="+user.getLastName()+"\nEmail="+user.getEmail(),"Created","User Created with pk = "+createUserPk);
					}
				}
			}
			if(issues!=null && !"".equals(issues)){

				List<Issue> issueList = issues.getIssue();
				List<String> errorList = new ArrayList<String>();
				if(!issueList.isEmpty()){
					logger.info("IssueList = "+issueList);
					for(Issue issue : issueList){
						logger.info("Issue = "+issue);
						logger.info("Issue Type = "+issue.getType());
						logger.info("Issue Message = "+issue.getMessage());
						errorList.add(issue.getMessage());
					}
				}
			}

		} catch (OperationException_Exception e) {
			//e.printStackTrace();
			logger.info("ERROR OCCURED Creating  User in participating site = "+orgName);
			List<Issue> issueList = e.getFaultInfo().getIssues().getIssue();
			List<String> errorList = new ArrayList<String>();
			for(Issue issue : issueList){
				IssueTypes issueTypes = issue.getType();
				String value = issueTypes.value();
				logger.info("Issue value = "+value);
				logger.info("Issue Type = "+issue.getType());
				logger.info("Message = "+issue.getMessage());
				errorList.add(issue.getMessage());
				logger.info("Got operaton exception with issue: "+issue.getMessage());
			}
						
		} catch (Exception e) {
			//e.printStackTrace();
			logger.error("Error :",e);
			
		}
		
		return createUserPk;
	}
	
	
	public ResponseHolder createNonSysUser(NonSystemUser nonSystemUser,String orgName)
			throws OperationException_Exception {
		logger.info("Creating User in Site = "+orgName);
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList list = null;
		try{
			list = (MessageContentsList) producer.sendBody(orgName+"_espCreateNonSysUserEndpoint", 
					ExchangePattern.InOut, nonSystemUser);
		}catch(CamelExecutionException e) {
			logger.info(e.getCause());
			logger.info(e.getMessage());
			if (e.getCause() instanceof OperationException_Exception) {
				logger.info("throwing Exception");
				throw(OperationException_Exception)e.getCause();
			}
			e.printStackTrace();
			return null;
		}catch (Exception e) {
			//e.printStackTrace();
			logger.error("Error :",e);
			return null;
		}
		
		return (ResponseHolder) list.get(0);
	}
	
	public Map<String, Object> callGetCodelsts(String orgName) {
		
		String codeType=null;
		Map<VelosKeys, Object> dataMap = null;
		Map<String,Object> codeMap = null;
		try{
						
			List<String> typeList = getCodeTypeList();
			logger.info("List = "+typeList);
			
			codeMap = new HashMap<String,Object>();
			logger.info("Getting codelsts for org = "+orgName);
			for(String type : typeList){
				try{
					codeType=type;
					logger.info("***************Type = "+type+"*****************");
					Codes codes = getCodelsts(orgName,type);
					if(codes!=null){
						List<Code> list = codes.getCodes();
						for(Code code : list){
							logger.info("Code SubType="+code.getCode()+" Code Desc="+code.getDescription());
							/*logger.info("Code Type = "+code.getType());
							logger.info("Code SubType = "+code.getCode());
							logger.info("Code Desc = "+code.getDescription());*/
						}
						codeMap.put(type, codes);
					}
				}catch (OperationException_Exception e) {
					logger.error("ERROR OCCURED getting Codelsts for "+orgName +"of Type = "+codeType);
					dataMap = new HashMap<VelosKeys, Object>();
					List<Issue> issueList = e.getFaultInfo().getIssues().getIssue();
					List<String> errorList = new ArrayList<String>();
					for(Issue issue : issueList){
						logger.info(issue.getType());
						logger.info(issue.getMessage());
						dataMap.put(ProtocolKeys.FaultString, issue.getMessage());
						logger.error("Issue Type = "+issue.getType()+". Message = "+issue.getMessage());
						errorList.add(issue.getMessage());
						logger.error("Got operaton exception with issue: "+issue.getMessage());
					}
					dataMap.put(ProtocolKeys.ErrorList, errorList);
					if (issueList == null) { return null; }
					return null;
				}
			}
		}catch (Exception e) {
			logger.error("Error :",e);
			return null;
		}
		logger.info("CodeMap = "+codeMap);
		return codeMap;
	}
	
	public Codes getCodelsts(String orgName,String codeType)
			throws OperationException_Exception {
	
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList list = null;
		try{
			list = (MessageContentsList) producer.sendBody(orgName+"_getCodelsts", 
					ExchangePattern.InOut, codeType);        
		}catch(CamelExecutionException e) {
				logger.error(e.getCause());
				logger.error(e.getMessage());
			if (e.getCause() instanceof OperationException_Exception) {
				throw(OperationException_Exception)e.getCause();
			}else{
				throw e;
			}
		}
		return (Codes) list.get(0);
	}
}
