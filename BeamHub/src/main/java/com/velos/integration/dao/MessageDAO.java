package com.velos.integration.dao;

import javax.sql.DataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;

public class MessageDAO {
	
	private static Logger logger = Logger.getLogger(MessageDAO.class);
	
	private DataSource eresDataSource;
	private JdbcTemplate jdbcTemplate;
	
	public void setEresDataSource(DataSource eresDataSource) {
		this.eresDataSource = eresDataSource;
		this.jdbcTemplate = new JdbcTemplate(eresDataSource);
		logger.info("*******Jdbc Connection created successfully");	}
	
	public boolean saveMessageDetails(String StudyNumber,String BroadCasting_Site,String Participating_Site,String Component_Type,String Component_Name,String Status,String response_message) {
				
		boolean isSuccess = false;
		try{
			String sql = "INSERT INTO BeamHub_AUDIT_DETAILS"
						+"(PK_MSG_AUDIT,STUDYNUMBER,BROADCASTING_SITE,PARTICIPATING_SITE,COMPONENT_TYPE,COMPONENT_NAME,STATUS,RESPONSE_MESSAGE,CREATION_DATE)"
						+" VALUES(nextval('seq_BeamHub_AUDIT_DETAILS'),?,?,?,?,?,?,?,current_timestamp)";

			jdbcTemplate.update(sql,new Object[]{StudyNumber,BroadCasting_Site,Participating_Site,Component_Type,Component_Name,Status,response_message});
			isSuccess = true;
		}catch(Exception e){
			logger.error("Database Error : ",e);
			isSuccess = false;
		}
		if(isSuccess){
			logger.info("//Successfully Saved in DB");
		}
		return isSuccess;
	}
	
}
