package com.velos.integration.core.messaging;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;

import org.apache.log4j.Logger;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;

import com.velos.integration.dao.MessageDAO;
import com.velos.integration.espclient.VelosSendStudyData;
import com.velos.integration.util.PropertiesUtil;

import java.io.StringReader;
import java.util.Properties;

 
public class XmlParser {
	
	private VelosSendStudyData velosSendStudyData;
	
	private static Logger logger = Logger.getLogger(XmlParser.class);
	
	public VelosSendStudyData getVelosSendStudyData() {
		return velosSendStudyData;
	}
	public void setVelosSendStudyData(VelosSendStudyData velosSendStudyData) {
		this.velosSendStudyData = velosSendStudyData;
	}
	
	private PropertiesUtil propertiesUtil;
	
	public PropertiesUtil getPropertiesUtil() {
		return propertiesUtil;
	}

	public void setPropertiesUtil(PropertiesUtil propertiesUtil) {
		this.propertiesUtil = propertiesUtil;
	}

	private MessageDAO messageDao;
	
	public MessageDAO getMessageDao() {
		return messageDao;
	}

	public void setMessageDao(MessageDAO messageDao) {
		this.messageDao = messageDao;
	}
	
	// Study trigger  Xml input 
	public  void studyXmlParser(String studyXml) {
		
		String studyNumber=null;
		Properties prop = propertiesUtil.getAllProperties();
		try{
			
			/*Properties prop = new Properties();
			
			prop.load(this.getClass().getClassLoader().getResourceAsStream("config.properties"));
			*/
			
			logger.info("Loading properties after request = "+prop.getProperty("study.pckg.status"));
			logger.info("***********studyXmlParser********************");
			
			DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			InputSource is = new InputSource();
				is.setCharacterStream(new StringReader(studyXml));

				Document doc = db.parse(is);
				NodeList nodes = doc.getElementsByTagName("changes");
				for (int i = 0; i < nodes.getLength(); i++) {
					Element element = (Element) nodes.item(i);

					NodeList name = element.getElementsByTagName("statusCodeDesc");
						Element line = (Element) name.item(0);
						String studyStatus=getCharacterDataFromElement(line);
		      	
					logger.info("Study Status=========>: " +studyStatus);
					
					NodeList title = element.getElementsByTagName("studyNumber");
						Element line1 = (Element) title.item(0);
						studyNumber=getCharacterDataFromElement(line1);
				      
					logger.info("Study Number=========>: " +studyNumber);
					if(studyStatus.equals(prop.getProperty("study.pckg.status").trim())){
						velosSendStudyData.sendStudyPackage(studyNumber,studyStatus,"StudyPackage");
					}else if(studyStatus.equals(prop.getProperty("study.amnd.doc.status").trim())){
						velosSendStudyData.sendStudyPackage(studyNumber,studyStatus,"Amendments");
					}else{
						String pkgStatus = "Incorrect Status to send Study Package or Amendment Documents";
						logger.error(pkgStatus);
						messageDao.saveMessageDetails(studyNumber,prop.getProperty("BroadCastingSite"),"All Participating sites", "All","All","Failed",pkgStatus);
					}
				}
		}catch(Exception e){
			//e.printStackTrace();
			logger.error("Error : ", e);
		}

	}
	public static String getCharacterDataFromElement(Element e) {
	    Node child = e.getFirstChild();
	    if (child instanceof CharacterData) {
	    	CharacterData cd = (CharacterData) child;
	    	return cd.getData();
	    }
	    return "";
	}
	
							
}
