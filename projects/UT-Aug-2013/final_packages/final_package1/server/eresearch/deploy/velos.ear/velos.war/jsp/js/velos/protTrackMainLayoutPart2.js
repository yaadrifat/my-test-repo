protTrackMainLayoutPart2JS={
		isPrintPreview: false,
		setDataReceived: {},
		dataReceived: {},
		showHideFields: {}
};

protTrackMainLayoutPart2JS.showHideFields = function(fld, action){
	switch (action){
	case 'show':
		if (fld){
			fld.style.display = "inline";
			if (!protTrackMainLayoutPart2JS.isPrintPreview){
				fld.parentNode.parentNode.style.display = "";
			} else {
				fld.parentNode.style.display = "";
			}
		}
		break;
	case 'hide':
		if (fld){
			fld.style.display = "none";
			if (!protTrackMainLayoutPart2JS.isPrintPreview){
				fld.parentNode.parentNode.style.display = "none";
			} else {
				fld.parentNode.style.display = "none";
			} 
		}
		break;
	default:
		break;
	}
};

protTrackMainLayoutPart2JS.setDataReceived = function(key, value){
	protTrackMainLayoutPart2JS.dataReceived[key] = value;
};

$j(document).ready(function(){
	formFieldMappingsJS.loadFormFieldMappings();

	var studyId = document.getElementsByName('studyId')[0].value;
	var researchTypeSubType;
	
	jQuery.ajax({
		type: "POST",
		url:"getStudySummaryFields.jsp",
		async:false,
		data: {"studyId": studyId},
		success: function(respStudy) {
			researchTypeSubType = respStudy.researchTypeSubType	
		}
	});
	
	var er_def_formstat = document.getElementById('er_def_formstat');
	protTrackMainLayoutPart2JS.isPrintPreview = (!er_def_formstat)? true : false;
	
	var ctrc_hpt_1572numberArr = [];
	var ctrc_hpt_fdasubmArr = [];
	var indx = 1;  //field id counter
	var arrIndx = 0; //array index counter
	
	if (protTrackMainLayoutPart2JS.isPrintPreview){
		while((formFieldMappingsJS.getColumnSysId('ctrc_hpt_1572number' + indx)) && (formFieldMappingsJS.getColumnSysId('ctrc_hpt_fdasubm' + indx))) {
			var ctrc_hpt_1572number = formFieldMappingsJS.getColumnSysId('ctrc_hpt_1572number' + indx);
			ctrc_hpt_1572numberArr[arrIndx] = document.getElementById(ctrc_hpt_1572number+'_span');
			
			var ctrc_hpt_fdasubm = formFieldMappingsJS.getColumnSysId('ctrc_hpt_fdasubm' + indx);
			ctrc_hpt_fdasubmArr[arrIndx] = document.getElementById(ctrc_hpt_fdasubm+'_span');
			++indx;
			++arrIndx;
		}
	} else {
		while((formFieldMappingsJS.getColumnSysId('ctrc_hpt_1572number' + indx)) && (formFieldMappingsJS.getColumnSysId('ctrc_hpt_fdasubm' + indx))) {
			var ctrc_hpt_1572number = formFieldMappingsJS.getColumnSysId('ctrc_hpt_1572number' + indx);
			ctrc_hpt_1572numberArr[arrIndx] = document.getElementById(ctrc_hpt_1572number);
			
			var ctrc_hpt_fdasubm = formFieldMappingsJS.getColumnSysId('ctrc_hpt_fdasubm' + indx);
			ctrc_hpt_fdasubmArr[arrIndx] = document.getElementById(ctrc_hpt_fdasubm);
			++indx;
			++arrIndx;
		}
	}

	if (researchTypeSubType == 'insti'){
		//show fields
		for(var count = 0; count < ctrc_hpt_1572numberArr.length; ++count){
			protTrackMainLayoutPart2JS.showHideFields(ctrc_hpt_1572numberArr[count], 'show');
			protTrackMainLayoutPart2JS.showHideFields(ctrc_hpt_fdasubmArr[count], 'show');
		}
	} else {
		//hide fields
		for(var count = 0; count < ctrc_hpt_1572numberArr.length; ++count){
			protTrackMainLayoutPart2JS.showHideFields(ctrc_hpt_1572numberArr[count], 'hide');
			protTrackMainLayoutPart2JS.showHideFields(ctrc_hpt_fdasubmArr[count], 'hide');
		}
	}
});

