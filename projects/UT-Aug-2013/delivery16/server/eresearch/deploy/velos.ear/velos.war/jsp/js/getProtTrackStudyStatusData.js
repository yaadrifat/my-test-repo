function getProtTrackStudyStatus_loadDetails(){
	if (document.getElementsByName("studyId")[0]){
		var studyId = (document.getElementsByName("studyId")[0]).value;	
		try {
			jQuery.ajax({
				type: "POST",
				url:"getStudyStatusData.jsp",
				data: {"studyId": studyId},
				success: function(resp) {
					var currentStudyStatId = resp.currentStudyStatId;
					var currentStudyStatus = resp.currentStudyStatus;

					jQuery.ajax({
						type: "POST",
						url:"getStudySummaryFields.jsp",
						data: {"studyId": studyId},
						success: function(resp) {
							if (resp.result < 0){
								$j("#fillform").prepend('<P>'+e.resultMsg+'</P>');
							}

							var studyStartDt = resp.studyStartDt;
							var studyEndDt = resp.studyEndDt;

							$j("#fillform").prepend('<br/><table class="outline" width="50%">'
									+'<tr><th width="30%">'+L_Current_Status+'</th>'
									+'<th width="30%">'+L_Std_StartDate+'</th>'
									+'<th width="30%">'+L_Std_EndDate+'</th>'
									+'</tr>'
									+'<tr class="browserEvenRow"><td>'+currentStudyStatus+'</td>'
									+'<td>'+studyStartDt+'</td>'
									+'<td>'+studyEndDt+'</td>'
									+'</tr>'
									+'</table><br/>'
							);
						}
					});
				}
			});			
		} catch (e){
			if (e.result < 0){
				$j("#fillform").prepend('<P>'+e.resultMsg+'</P>');
			}
		}

	}
}

function getProtTrackStudyStatus_loadPIDetails(){
	if (document.getElementsByName("studyId")[0]){
		var studyId = document.getElementsByName("studyId")[0].value;
		jQuery.ajax({
			type: "POST",
			url:"getStudyTeam.jsp",
			data : { 
				"studyId" :studyId,
				"role_subType": 'role_prin'
			},
	        success: function(resp) {
				var codeId = resp.teamRoleCodeId;
				if (codeId > 0 ){
		        	var arrFirstNames = resp.arrFirstNames;
					var arrLastNames = resp.arrLastNames;
					var codeDesc = resp.teamRoleCodeDesc;
		
					var ctrc_protocol_tracking_pi = document.getElementById(getColumnSysId('ctrc_protocol_tracking_pi'));
					if (ctrc_protocol_tracking_pi){				
						var list ='';
						for (var indx =0; indx < arrFirstNames.length; indx++){
							if (indx > 0) list += ',';
							list += '['+arrLastNames[indx] + ' ' + arrFirstNames[indx]+']';
						}
						
						var ctrc_protocol_tracking_pi_span = document.createElement("span");
						ctrc_protocol_tracking_pi_span.id = ctrc_protocol_tracking_pi.id+"_span";
						ctrc_protocol_tracking_pi_span.innerHTML = list;
						ctrc_protocol_tracking_pi.parentNode.appendChild(ctrc_protocol_tracking_pi_span);
					}
				}
			}
		});
	}
}

function getProtTrackStudyStatus_Init() {
	var ctrc_protocol_tracking_pi = document.getElementById(getColumnSysId('ctrc_protocol_tracking_pi'));
	if (ctrc_protocol_tracking_pi){
		ctrc_protocol_tracking_pi.style.display = "none";
		getProtTrackStudyStatus_loadPIDetails();
	}
	getProtTrackStudyStatus_loadDetails();
}

setTimeout(function(){ getProtTrackStudyStatus_Init();}, 1000);
