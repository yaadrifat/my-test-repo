var isCompProtInfoMandatory = false;

// --- Validation Check ---
function cdst_validation_check(){
	//Competing trials info fields will be mandatory if competing trials is marked Yes
	if (isCompProtInfoMandatory){
		var IDEAS_Study_Titles = document.getElementById(getColumnSysId('IDEAS_Study_Titles'));
		if (!(validate_col("List Study Titles",IDEAS_Study_Titles))) 
			return false;
		
		var IDEAS_Trial_Rationales = document.getElementsByName(getColumnSysId('IDEAS_Trial_Rationale'));
		if (!(validate_chk_radio("Rationale for Trial",IDEAS_Trial_Rationales))) 
			return false;
	}
	return true;
}

function cdst_loadTumorTypes(studyDisSites){
	var IDEAS_Internal_Tumor_Type_New = document.getElementById(getColumnSysId('IDEAS_Internal_Tumor_Type_New'));
	if (!IDEAS_Internal_Tumor_Type_New) return;
	var selTumorTypes = IDEAS_Internal_Tumor_Type_New.value;
	
	if (document.getElementsByName("studyId")[0]){
		var studyId = (document.getElementsByName("studyId")[0]).value;
		jQuery.ajax({
			type:"POST",
			url:"getCodeMultiSelectList.jsp",
			data : {
				listName: "listTumorTypes",
				codeType: "tumorType",
				filter: "customCol",
				customColValue: studyDisSites,
				selectedVals: selTumorTypes
			},
			success: function(resp) {
				var parentSpan = document.getElementById(IDEAS_Internal_Tumor_Type_New.id+"_span");
				var newElement = document.createElement("div");
				newElement.id = 'tumorTypesDiv';
				parentSpan.appendChild(newElement);
				
				parentSpan.innerHTML += "<BR>";
				parentSpan.innerHTML += resp;
				
				var listTumorTypes = document.getElementById('listTumorTypes');
				if (listTumorTypes){
					listTumorTypes.onchange = function(e){
						var tumorTypesDiv = document.getElementById('tumorTypesDiv');
						tumorTypesDiv.innerHTML ='';
						var IDEAS_Internal_Tumor_Type_New = document.getElementById(getColumnSysId('IDEAS_Internal_Tumor_Type_New'));
						IDEAS_Internal_Tumor_Type_New.value = '';
						for (var i = 0; i < this.options.length; i++){
							if (this.options[i].selected){
								if (IDEAS_Internal_Tumor_Type_New.value == ''){
									IDEAS_Internal_Tumor_Type_New.value += this.options[i].value;
									tumorTypesDiv.innerHTML += '['+this.options[i].text+']';
								}else {
									IDEAS_Internal_Tumor_Type_New.value += ','+this.options[i].value;
									tumorTypesDiv.innerHTML += ',['+this.options[i].text+']';
								}
							}
						}					
					}
					listTumorTypes.onchange();
				}
			}
		});
	}
}
function cdst_loadDiseaseSites(){
	if (document.getElementsByName("studyId")[0]){
		var studyId = (document.getElementsByName("studyId")[0]).value;
		jQuery.ajax({
			type: "POST",
			url:"getStudySummaryFields.jsp",
			data: {"studyId": studyId},
			success: function(resp) {
				var studyDisSites = resp.studyDisSiteSubTypes;

				if (studyDisSites){
					cdst_loadTumorTypes(studyDisSites);
				}
			}
		});
	}
}

function cdst_loadMandatoryFields(){
	if (document.getElementsByName("studyId")[0]){
		var studyId = (document.getElementsByName("studyId")[0]).value;
		var IDEAS_Competing_Protocols = document.getElementsByName(getColumnSysId('IDEAS_Competing_Protocols'));
		
		var IDEAS_Study_Titles = document.getElementById(getColumnSysId('IDEAS_Study_Titles'));
		var label = document.getElementById(IDEAS_Study_Titles.id+"_id");
		var newElement = document.createElement("font");
		newElement.id = 'IDEAS_Study_Titles_font';
		newElement.className = "mandatory";
		newElement.innerHTML="*";
		newElement.style.visibility = 'hidden';
		label.appendChild(newElement);
		
		label = document.getElementById(getColumnSysId('IDEAS_Trial_Rationale')+"_id");
		newElement = document.createElement("font");
		newElement.id = 'IDEAS_Trial_Rationale_font';
		newElement.className = "mandatory";
		newElement.innerHTML="*";
		newElement.style.visibility = 'hidden';
		label.appendChild(newElement);
		
		for (var i=0; i < IDEAS_Competing_Protocols.length; i++){
			if (IDEAS_Competing_Protocols[i].checked){ 
				if (IDEAS_Competing_Protocols[i].value == "Yes"){
					isCompProtInfoMandatory = true;
					document.getElementById('IDEAS_Study_Titles_font').style.visibility = 'visible';
					document.getElementById('IDEAS_Trial_Rationale_font').style.visibility = 'visible';
				} else {
					isCompProtInfoMandatory = false;
					document.getElementById('IDEAS_Study_Titles_font').style.visibility = 'hidden';
					document.getElementById('IDEAS_Trial_Rationale_font').style.visibility = 'hidden';
				}
			}
			IDEAS_Competing_Protocols[i].onclick = function(){
				if ('Yes' == this.value){
					isCompProtInfoMandatory = true;
					document.getElementById('IDEAS_Study_Titles_font').style.visibility = 'visible';
					document.getElementById('IDEAS_Trial_Rationale_font').style.visibility = 'visible';
				} else {
					isCompProtInfoMandatory = false;
					document.getElementById('IDEAS_Study_Titles_font').style.visibility = 'hidden';
					document.getElementById('IDEAS_Trial_Rationale_font').style.visibility = 'hidden';
				}
			};
		}
	}
}

function cdst_Init() {
	cdst_loadDiseaseSites();
	cdst_loadMandatoryFields();
	
	var IDEAS_Internal_Tumor_Type_New = document.getElementById(getColumnSysId('IDEAS_Internal_Tumor_Type_New'));
	if (!IDEAS_Internal_Tumor_Type_New) return;
	IDEAS_Internal_Tumor_Type_New.style.display="none";
}

var cdst_formOnSubmit;
function cdst_onload() {
	setTimeout(function(){ cdst_Init();}, 1000); 
    cdst_formOnSubmit = document.getElementById("fillform").onsubmit;
    document.getElementById("fillform").onsubmit="";
}
cdst_onload();
    
$j("#fillform").submit(function(e) {
	e.preventDefault();
	if (cdst_validation_check()){
		document.getElementById("fillform").onsubmit= cdst_formOnSubmit;
		var flag = document.getElementById("fillform").onsubmit();
		if(flag){
			document.getElementById("fillform").submit();
		} else {
			document.getElementById("fillform").onsubmit= "";
		}
	}
});