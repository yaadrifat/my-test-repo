This is a readMe for delivery #14 of custom project for University of Texas.
-------------------------------------------------------------------------------

This is a bug fix patch. Steps to deploy the patch are as follows:
1.	Copy the files mentioned below at correct locations.
a.	1 JSP file �
getStudyMoreDetails.jsp

This is a brand new file. It should be replaced in the following location -
eResearch_jboss510\server\eresearch\deploy\velos.ear\velos.war\jsp

c.	1 JS file �
getRegAffairsStudyData.js

This file is pre-existing. They should be replaced in the following location -
eResearch_jboss510\server\eresearch\deploy\velos.ear\velos.war\jsp\js\velos

2.	No server restart is required.
