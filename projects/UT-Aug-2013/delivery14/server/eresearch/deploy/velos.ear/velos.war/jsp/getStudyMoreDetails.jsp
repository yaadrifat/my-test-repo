<%@page import="com.velos.eres.business.study.impl.StudyBean"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.json.*"%>
<%@ page language = "java" import = "com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.esch.business.common.*,java.util.*"%>
<jsp:useBean id ="sessionmaint2" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="studyIdB" scope="session" class="com.velos.eres.web.studyId.StudyIdJB"/>
<jsp:useBean id="studyRightsB" scope="request" class="com.velos.eres.web.studyRights.StudyRightsJB"/>
<%
	HttpSession tSession2 = request.getSession(true);
	request.setCharacterEncoding("UTF-8");
	response.setContentType("application/json");
	JSONObject jsObj = new JSONObject();
	if (!sessionmaint2.isValidSession(tSession2)) {
		// Session is invalid; print an error and exit
		response.sendError(HttpServletResponse.SC_FORBIDDEN, "SESSION_TIME_OUT");
		jsObj.put("result", -1);
		jsObj.put("resultMsg", MC.M_UsrNot_LoggedIn);/*jsObj.put("resultMsg", "User is not logged in.");*****/
		out.println(jsObj.toString());
		return;
	}

   	if (sessionmaint2.isValidSession(tSession2)){
		int studyId = StringUtil.stringToNum((String) request.getParameter("studyId"));
		int userId = StringUtil.stringToNum((String) tSession2.getValue("userId"));
		int accId = StringUtil.stringToNum((String) tSession2.getValue("accountId"));
		
		if (!StringUtil.isAccessibleFor(studyRightsB.getStudyRightsForModule(studyId, userId, "STUDYSUM"), 'V')){
			response.sendError(HttpServletResponse.SC_FORBIDDEN, "ERROR");
			jsObj.put("result", -2);
			jsObj.put("resultMsg", MC.M_InsuffAccRgt_CnctSysAdmin);
			out.println(jsObj.toString());
			return;
		}
		
		String moreDetailType = request.getParameter("moreDetailType");
		if (StringUtil.isEmpty(moreDetailType)){
			return;
		}
		CodeDao cdDao = new CodeDao();
		int cdProtId = cdDao.getCodeId("studyidtype", moreDetailType);
		if (cdProtId <= 0){
			return;
		}

		int studyIdPK = studyIdB.findMoreStudyId(studyId, moreDetailType);
		if (studyIdPK > 0){
			studyIdB.setId(studyIdPK);
			studyIdB.getStudyIdDetails();
			
			String studyMoreDetail = studyIdB.getAlternateStudyId();
			jsObj.put("studyMoreDetail", studyMoreDetail);
		}
		out.println(jsObj.toString());

	} %>