var jsArrColumns = [];
var metadataReady = false;

function loadFormMetadata(){
	if (document.getElementsByName("formId")[0]){
		var formId = (document.getElementsByName("formId")[0]).value;
		jQuery.ajax({
			url:"getFormMetadata.jsp?formId=" + formId,
			async: true,
			cache: false,
			success: function(resp) {
				jsArrColumns = [];
				
				for (var i=0; i < (resp.arrColumns).length; i++){
					var jsObj = resp.arrColumns[i];
					jsArrColumns[jsObj.colUID]= jsObj;
				}
				metadataReady = true;
			}
		});
	}
}

function getColumnSysId(colUId){
	var colSysId = '';
	if(colUId){
		var jsObj = jsArrColumns[colUId];
		if (jsObj){
			colSysId = jsObj.colSysId;
		}
	}
	//alert(colUId+ ' '+colSysId);
	return colSysId;
}

//////******************************

jQuery(document).ready(function() {
	loadFormMetadata();
});