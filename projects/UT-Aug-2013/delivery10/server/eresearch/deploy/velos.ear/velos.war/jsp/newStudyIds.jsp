<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!-- saved from url=(0022)http://internet.e-mail -->
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML><HEAD><TITLE><%=LC.L_More_StdDets%><%--More <%=LC.Std_Study%> Details*****--%></TITLE>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

</HEAD>

<script>
function setValue(formobj,iElementId,cbcount) {
	var chkFld = formobj['alternateId'+iElementId];
	var value = chkFld.value;
	if (value=="Y") {
		chkFld.value="N";
		chkFld.checked=false;
	} else if ((value.length==0) || (value=="N"))  {
		chkFld.value="Y";
		chkFld.checked=true;
	} else { // <== there is some junk data in the DB column already
		chkFld.value="Y";
		chkFld.checked=true;
	}
}

function setDD(formobj)
{
	var optvalue=formobj.ddlist.value;
	if (optvalue.length>0)
	{
		var arrayofStrings=optvalue.split("||");
		if (arrayofStrings.length>1) {
			for (var j=0;j<arrayofStrings.length;j++)
			{
				var ddStr=arrayofStrings[j];
				var arrayofDD=ddStr.split(":");
				var ddcount=arrayofDD[0];
				var selvalue=arrayofDD[1];
				var ddFld = formobj['alternateId'+ddcount];
				if (ddFld && ddFld.options) {
					var opt = ddFld.options;
					for (var i=0;i<opt.length;i++){
						if (opt[i].value==selvalue){
							ddFld.selectedIndex=i ;
						}
					}
				}
			}
		} else {
			var ddStr=arrayofStrings[0];
			var arrayofDD=ddStr.split(":");
			var ddcount=arrayofDD[0];
			var selvalue=arrayofDD[1];
			var ddFld = formobj['alternateId'+ddcount];
			if (ddFld && ddFld.options) {
			 	var opt = ddFld.options; 
				if (opt == undefined)
			    	opt = ddFld.options; 
				for (var i=0;i<opt.length;i++){
					if (opt[i].value==selvalue){
						ddFld.selectedIndex=i ;
					}
				}
			}
		}// end else
	}//optvalue.length>0	
}


function validate(formobj) {

 	 if (!(validate_col('e-Signature',formobj.eSign))) return false

     if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
	   }

	return true;
}


</script>


<%
int ienet = 2;

String agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1)
     ienet = 0; //IE
    else
	ienet = 1;
	if(ienet == 0) {
%>
<body style="overflow:scroll;" onLoad="setDD(document.newstudyid)">
<%
	} else {
%>
<body onLoad="setDD(document.newstudyid)">
<%
	}
%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%>
<%@ page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.StringUtil"%>
<%@ page import="com.velos.eres.service.util.MC"%>


<jsp:useBean id="altId" scope="request" class="com.velos.eres.web.studyId.StudyIdJB"/>
<jsp:include page="include.jsp" flush="true"/>
<DIV class="popDefault" id="div1">

<%
	HttpSession tSession = request.getSession(true);
	 if (sessionmaint.isValidSession(tSession))
	{
	 %>
	<jsp:include page="sessionlogging.jsp" flush="true"/>

<%

	 String defUserGroup = (String) tSession.getAttribute("defUserGroup");

	 String accountId = (String) tSession.getValue("accountId");
	 String studyId = request.getParameter("studyId");
	 int studyRights = EJBUtil.stringToNum(request.getParameter("studyRights"));


	 //get study ids
	 StudyIdDao sidDao = new StudyIdDao();
	 sidDao = altId.getStudyIds(EJBUtil.stringToNum(studyId),defUserGroup);

 	 ArrayList studyIdType  = new ArrayList();
	 ArrayList studyIdTypesDesc = new ArrayList();
	 ArrayList id = new ArrayList();
	 ArrayList alternateId = new ArrayList();
 	 ArrayList recordType = new ArrayList();
	 ArrayList dispType=new ArrayList();
	 ArrayList dispData=new ArrayList();

	 id = sidDao.getId();
	 studyIdType =  sidDao.getStudyIdType();
	 studyIdTypesDesc = sidDao.getStudyIdTypesDesc();
	 alternateId = sidDao.getAlternateId();
	 recordType = sidDao.getRecordType ();
	 dispType=sidDao.getDispType();
	 dispData=sidDao.getDispData();


	 String strStudyIdTypesDesc ;
 	 String strAlternateId ;
  	 String strRecordType ;
  	 Integer intStudyIdType;
   	 Integer intId;
	 String disptype="";
	 String dispdata="";
	 String ddStr="";


%>
<Form  name="newstudyid" id ="newstudyidform" action="updatestudyid.jsp" method="post" onSubmit="if (validate(document.newstudyid)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}" >
<BR>
<P class = "sectionHeadings">
<%=LC.L_More_StdDets%><%--More <%=LC.Std_Study%> Details*****--%>
</P>
<table width="100%">
<!--<tr>
   <th width="45%" >Other <%=LC.Std_Study%> ID Type</th>
   <th width="55%" >Other <%=LC.Std_Study%> ID</th>
</tr> -->
<%
	int counter = 0,cbcount=0;
	for (counter = 0; counter <= studyIdType.size() -1 ; counter++)
		{
				strStudyIdTypesDesc = (String) studyIdTypesDesc.get(counter);
 	 			strAlternateId = (String) alternateId.get(counter);
 	 			intStudyIdType = (Integer) studyIdType.get(counter) ; 

 	 			disptype=(String) dispType.get(counter);
				dispdata=(String) dispData.get(counter);
				if (disptype==null) disptype="";
 	 			disptype=disptype.toLowerCase();

				if (dispdata==null) dispdata="";
				if (disptype.equals("dropdown")) {
					if (null != dispdata){
						dispdata = StringUtil.replaceAll(dispdata, "alternateId", "alternateId"+intStudyIdType);
					}
				}

 	 			if (strAlternateId == null)
 	 				strAlternateId = "";

  	 			strRecordType = (String) recordType.get(counter);
   	 			intId = (Integer) id.get(counter) ;

	%>

	<tr>
		     <td class=tdDefault width="45%" >
				<%= strStudyIdTypesDesc %>
			 </td>
		     <td class=tdDefault width="55%" >
				<% if (disptype.equals("chkbox")){
				cbcount=cbcount+1;
				%>
				<input type = "hidden" name = "alternateId<%=intStudyIdType%>" value = "<%=strAlternateId.trim()%>" size = "25" maxlength = "100" >
						<%	 if ((strAlternateId.trim()).equals("Y")){%>
				 <input type="checkbox" name="alternate" value="<%=strAlternateId.trim()%>" onClick="setValue(document.newstudyid,<%=intStudyIdType%>,<%=cbcount%>)" checked>
				  <% }else{%>
				  <input type="checkbox"  name="alternate" value="<%=strAlternateId.trim()%>" onClick="setValue(document.newstudyid,<%=intStudyIdType%>,<%=cbcount%>)">

				<%}} else if (disptype.equals("dropdown")) {
				if (ddStr.length()==0) ddStr=(intStudyIdType)+":"+strAlternateId;
					else ddStr=ddStr+"||"+(intStudyIdType)+":"+strAlternateId;
				%>
				  <%=dispdata%>
				<%} else if (disptype.equals("splfld")) {%> 
				  	<%=dispdata%>
				<%}else if (disptype.equals("date")) {%>
					<input name="alternateId<%=intStudyIdType%>" type="text" class="datefield" size="10" readOnly  value="<%=strAlternateId.trim()%>">
				<%}else if (disptype.equals("readonly-input")){%>
					<input type="text" class='readonly-input' readonly name="alternateId<%=intStudyIdType%>" value="<%=strAlternateId.trim()%>" size="25" maxlength="100"/>
				<%}else{%>
					<input type = "text" name = "alternateId<%=intStudyIdType%>" value = "<%=strAlternateId.trim()%>" size = "25" maxlength = "100" >
				<%}%>
				<input type = "hidden" name = "recordType<%=intStudyIdType%>" value = "<%=strRecordType %>" >
				<input type = "hidden" name = "id<%=intStudyIdType%>" value = "<%=intId%>" >
				<input type = "hidden" name = "studyIdType" value = "<%=intStudyIdType%>" >
				<input type = "hidden" name = "studyId" value = "<%=studyId%>" >
			 </td>
	</tr>

	<%
		}
	%>
	<input type="hidden" name="cbcount" value=<%=cbcount%>>
	<input type="hidden" name="ddlist" value="<%=ddStr%>">
	<%if (studyRights >= 6){%>
	 </table>
	<br>

	<jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="newstudyidform"/>
		<jsp:param name="showDiscard" value="N"/>
	</jsp:include>


<%}%>
	</form>
<%}else{%>
<jsp:include page="timeout_childwindow.jsp" flush="true"/>
<%}%>
</div>
<div class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</body>
</html>