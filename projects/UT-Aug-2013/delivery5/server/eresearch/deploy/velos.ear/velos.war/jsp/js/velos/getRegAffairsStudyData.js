function getRegAffairsStudyData_loadDetails(){
	if (document.getElementsByName("studyId")[0]){
		var studyId = (document.getElementsByName("studyId")[0]).value;
		try {
			jQuery.ajax({
				type: "POST",
				url:"getStudySummaryFields.jsp",
				data: {"studyId": studyId},
				success: function(resp) {
					var studyNumber = resp.studyNumber;
					var studyTitle = resp.studyTitle;
					
					var RA_Title = document.getElementById(getColumnSysId('RA_Title'));
					RA_Title.value = studyTitle;
					
					var RA_Title_span = document.createElement("span");
					RA_Title_span.id = RA_Title.id+"_span";
					RA_Title_span.innerHTML =studyTitle;
					RA_Title.parentNode.appendChild(RA_Title_span);
					
					var RA_Protocol_Short_Name = document.getElementById(getColumnSysId('RA_Protocol_Short_Name'));
					RA_Protocol_Short_Name.value = studyNumber;
					
					var RA_Protocol_Short_Name_span = document.createElement("span");
					RA_Protocol_Short_Name_span.id = RA_Title.id+"_span";
					RA_Protocol_Short_Name_span.innerHTML =studyNumber;
					RA_Protocol_Short_Name.parentNode.appendChild(RA_Protocol_Short_Name_span);
				}
			});
		} catch (e){
			if (e.result < 0){
				$j("#fillform").prepend('<P>'+e.resultMsg+'</P>');
			}
		}
	}
}

function getRegAffairsStudyData_Init() {
	var RA_Title = document.getElementById(getColumnSysId('RA_Title'));
	RA_Title.style.display="none";
	
	var RA_Protocol_Short_Name = document.getElementById(getColumnSysId('RA_Protocol_Short_Name'));
	RA_Protocol_Short_Name.style.display="none";
	getRegAffairsStudyData_loadDetails();
}

setTimeout(function(){ getRegAffairsStudyData_Init();}, 1000);
