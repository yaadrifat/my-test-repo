function getProtTrackStudyStatus_loadDetails(){
	if (document.getElementsByName("studyId")[0]){
		var studyId = (document.getElementsByName("studyId")[0]).value;	
		try {
			jQuery.ajax({
				type: "POST",
				url:"getStudyStatusData.jsp",
				data: {"studyId": studyId},
				success: function(resp) {
					var currentStudyStatId = resp.currentStudyStatId;
					var currentStudyStatus = resp.currentStudyStatus;

					jQuery.ajax({
						type: "POST",
						url:"getStudySummaryFields.jsp",
						data: {"studyId": studyId},
						success: function(resp) {
							if (resp.result < 0){
								$j("#fillform").prepend('<P>'+e.resultMsg+'</P>');
							}

							var studyStartDt = resp.studyStartDt;
							var studyEndDt = resp.studyEndDt;

							$j("#fillform").prepend('<br/><table class="outline" width="50%">'
									+'<tr><th width="30%">'+L_Current_Status+'</th>'
									+'<th width="30%">'+L_Std_StartDate+'</th>'
									+'<th width="30%">'+L_Std_EndDate+'</th>'
									+'</tr>'
									+'<tr class="browserEvenRow"><td>'+currentStudyStatus+'</td>'
									+'<td>'+studyStartDt+'</td>'
									+'<td>'+studyEndDt+'</td>'
									+'</tr>'
									+'</table><br/>'
							);
						}
					});
				}
			});			
		} catch (e){
			if (e.result < 0){
				$j("#fillform").prepend('<P>'+e.resultMsg+'</P>');
			}
		}

	}
}

function getProtTrackStudyStatus_Init() {
	getProtTrackStudyStatus_loadDetails();
}

setTimeout(function(){ getProtTrackStudyStatus_Init();}, 1000);
