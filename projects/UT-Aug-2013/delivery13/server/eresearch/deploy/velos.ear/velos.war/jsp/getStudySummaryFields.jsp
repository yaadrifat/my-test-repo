<%@page import="com.velos.eres.business.study.impl.StudyBean"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.json.*"%>
<%@ page language = "java" import = "com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.esch.business.common.*,java.util.*"%>
<jsp:useBean id ="sessionmaint2" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="studyB" scope="session" class="com.velos.eres.web.study.StudyJB"/>
<jsp:useBean id="studyRightsB" scope="request" class="com.velos.eres.web.studyRights.StudyRightsJB"/>
<%
	HttpSession tSession2 = request.getSession(true);
	request.setCharacterEncoding("UTF-8");
	response.setContentType("application/json");
	JSONObject jsObj = new JSONObject();
	if (!sessionmaint2.isValidSession(tSession2)) {
		// Session is invalid; print an error and exit
		response.sendError(HttpServletResponse.SC_FORBIDDEN, "SESSION_TIME_OUT");
		jsObj.put("result", -1);
		jsObj.put("resultMsg", MC.M_UsrNot_LoggedIn);/*jsObj.put("resultMsg", "User is not logged in.");*****/
		out.println(jsObj.toString());
		return;
	}

   	if (sessionmaint2.isValidSession(tSession2)){
		int studyId = StringUtil.stringToNum((String) request.getParameter("studyId"));
		int userId = StringUtil.stringToNum((String) tSession2.getValue("userId"));
		int accId = StringUtil.stringToNum((String) tSession2.getValue("accountId"));
		
		if (!StringUtil.isAccessibleFor(studyRightsB.getStudyRightsForModule(studyId, userId, "STUDYSUM"), 'V')){
			response.sendError(HttpServletResponse.SC_FORBIDDEN, "ERROR");
			jsObj.put("result", -2);
			jsObj.put("resultMsg", MC.M_InsuffAccRgt_CnctSysAdmin);
			out.println(jsObj.toString());
			return;
		}
		studyB.setId(studyId);
		studyB.getStudyDetails();
		
		String studyNumber = studyB.getStudyNumber();
		String title = studyB.getStudyTitle();

		jsObj.put("studyNumber", studyNumber);
		jsObj.put("studyTitle", title);

		CodeDao cdDao = new CodeDao();
		int tArea = StringUtil.stringToNum(studyB.getStudyTArea());
		if (tArea > 0){
			String tAreaDesc = cdDao.getCodeDescription(tArea);
			jsObj.put("studytAreaFK", tArea);
			jsObj.put("studytAreaDesc", tAreaDesc);
		}
		
		String studyStartDt = studyB.getStudyActBeginDate();
		String studyEndDt = studyB.getStudyEndDate();

		jsObj.put("studyStartDt", studyStartDt);
		jsObj.put("studyEndDt", studyEndDt);
		
		int resType = StringUtil.stringToNum(studyB.getStudyResType());
		if (resType > 0){
			cdDao = new CodeDao();
			if (cdDao.getCodeValuesById(resType)){
				jsObj.put("studyResTypeFK", resType);
				jsObj.put("studyResTypeSubType", (cdDao.getCSubType()).get(0));
				jsObj.put("studyResTypeDesc", (cdDao.getCDesc()).get(0));
			}
		}

		String diseaseSites = studyB.getDisSite();
		if (!StringUtil.isEmpty(diseaseSites)){
			cdDao = new CodeDao();
			cdDao.getCodeValuesByIds(diseaseSites);
			String tempDisStr = (cdDao.getCId()).toString();
			tempDisStr = tempDisStr.substring(tempDisStr.indexOf("[")+1, tempDisStr.lastIndexOf("]"));
			jsObj.put("studyDisSiteFKs", tempDisStr);

			tempDisStr = (cdDao.getCSubType()).toString();
			tempDisStr = tempDisStr.substring(tempDisStr.indexOf("[")+1, tempDisStr.lastIndexOf("]"));
			jsObj.put("studyDisSiteSubTypes", tempDisStr);

			tempDisStr = (cdDao.getCDesc()).toString();
			tempDisStr = tempDisStr.substring(tempDisStr.indexOf("[")+1, tempDisStr.lastIndexOf("]"));
			jsObj.put("studyDisSiteDescs", tempDisStr);
		}
		out.println(jsObj.toString());

	} %>