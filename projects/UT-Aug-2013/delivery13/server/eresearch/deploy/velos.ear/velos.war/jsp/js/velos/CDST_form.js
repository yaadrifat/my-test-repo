var isProtIDMandatory = false;
var isFundInfoMandatory = false; 
var isUnfundExpenseMandatory = false;
// --- Validation Check ---
function cdst_validation_check(){
	//Protocol ID field will be mandatory for specific research types 
	if (isProtIDMandatory){
		var Protocol_ID = document.getElementById(getColumnSysId('Protocol_ID'));
		if (!(validate_col("Protocol ID",Protocol_ID))) 
			return false;
	}
	
	//Funding Information fields will be mandatory for specific research types 
	if (isFundInfoMandatory){
		var IDEAS_Funding_Sources = document.getElementById(getColumnSysId('IDEAS_Funding_Sources'));
		if (!(validate_col("List sources of funding",IDEAS_Funding_Sources))) 
			return false;
		
		var IDEAS_Funding_Available = document.getElementById(getColumnSysId('IDEAS_Funding_Available'));
		if (!(validate_col("Amount of funding available",IDEAS_Funding_Available))) 
			return false;
		
		var IDEAS_Sufficient_Funds = document.getElementsByName(getColumnSysId('IDEAS_Sufficient_Funds'));
    	if (!(validate_chk_radio("Are there sufficient funds?",IDEAS_Sufficient_Funds))) 
			return false;
	}
	
	if (isUnfundExpenseMandatory){
		var IDEAS_Unfunded_Expenses = document.getElementById(getColumnSysId('IDEAS_Unfunded_Expenses'));
    	if (!(validate_col("Indicate how unfunded expenses will be paid for",IDEAS_Unfunded_Expenses))) 
			return false;
	}
}

function cdst_loadTumorTypes(studyDisSites){
	var IDEAS_Internal_Tumor_Type = document.getElementById(getColumnSysId('IDEAS_Internal_Tumor_Type'));
	if (!IDEAS_Internal_Tumor_Type) return;
	var selTumorTypes = IDEAS_Internal_Tumor_Type.value;
	
	if (document.getElementsByName("studyId")[0]){
		var studyId = (document.getElementsByName("studyId")[0]).value;
		jQuery.ajax({
			type:"POST",
			url:"getCodeMultiSelectList.jsp",
			data : {
				listName: "listTumorTypes",
				codeType: "tumorType",
				filter: "customCol",
				customColValue: studyDisSites,
				selectedVals: selTumorTypes
			},
			success: function(resp) {
				var parentSpan = document.getElementById(IDEAS_Internal_Tumor_Type.id+"_span");
				var newElement = document.createElement("div");
				newElement.id = 'tumorTypesDiv';
				parentSpan.appendChild(newElement);
				
				parentSpan.innerHTML += "<BR>";
				parentSpan.innerHTML += resp;
				
				var listTumorTypes = document.getElementById('listTumorTypes');
				if (listTumorTypes){
					listTumorTypes.onchange = function(e){
						var tumorTypesDiv = document.getElementById('tumorTypesDiv');
						tumorTypesDiv.innerHTML ='';
						var IDEAS_Internal_Tumor_Type = document.getElementById(getColumnSysId('IDEAS_Internal_Tumor_Type'));
						IDEAS_Internal_Tumor_Type.value = '';
						for (var i = 0; i < this.options.length; i++){
							if (this.options[i].selected){
								if (IDEAS_Internal_Tumor_Type.value == ''){
									IDEAS_Internal_Tumor_Type.value += this.options[i].value;
									tumorTypesDiv.innerHTML += '['+this.options[i].text+']';
								}else {
									IDEAS_Internal_Tumor_Type.value += ','+this.options[i].value;
									tumorTypesDiv.innerHTML += ',['+this.options[i].text+']';
								}
							}
						}					
					}
					listTumorTypes.onchange();
				}
			}
		});
	}
}
function cdst_loadDiseaseSites(){
	if (document.getElementsByName("studyId")[0]){
		var studyId = (document.getElementsByName("studyId")[0]).value;
		jQuery.ajax({
			type: "POST",
			url:"getStudySummaryFields.jsp",
			data: {"studyId": studyId},
			success: function(resp) {
				var studyDisSites = resp.studyDisSiteSubTypes;

				if (studyDisSites){
					cdst_loadTumorTypes(studyDisSites);
				}
			}
		});
	}
}

function cdst_Init() {
	cdst_loadDiseaseSites();
	var IDEAS_Internal_Tumor_Type = document.getElementById(getColumnSysId('IDEAS_Internal_Tumor_Type'));
	if (!IDEAS_Internal_Tumor_Type) return;
	IDEAS_Internal_Tumor_Type.style.display="none";
}

//function cdst_onload() {
    setTimeout(function(){ cdst_Init();}, 1000);  
//}
//cdst_onload();
