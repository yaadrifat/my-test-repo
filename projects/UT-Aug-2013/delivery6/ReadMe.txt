This is a readMe for delivery #6 of custom project for University of Texas.
-------------------------------------------------------------------------------

This is a bug fix patch. Steps to deploy the patch are as follows:
1.	Copy the files mentioned below at correct locations.
a.	1 JSP files �
studyformdetails.jsp

This is an existing file. It should be replaced in the following location -
eResearch_jboss510\server\eresearch\deploy\velos.ear\velos.war\jsp

c.	2 JS files �
getRegAffairsStudyData.js
getStudyTeamByRole.js

Both files are pre-existing. They should be replaced in the following location -
eResearch_jboss510\server\eresearch\deploy\velos.ear\velos.war\jsp\js\velos

2.	No server restart is required.
