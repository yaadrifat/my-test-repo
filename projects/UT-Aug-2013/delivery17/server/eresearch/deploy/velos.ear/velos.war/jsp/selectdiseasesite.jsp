<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%!
String TAreaDisSiteFlag = "TAreaDisSite_ON";
%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_DiseaseSite_Lookup%><%--Disease Site Lookup*****--%></title>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>



<SCRIPT Language="Javascript1.2">



	

function setSites(formobj) {

  var lenSites = 0;
  	from=formobj.from.value;
	datafld=formobj.datafld.value;
	dispfld=formobj.dispfld.value;
	lenSites = parseInt(formobj.len.value);

	var names = "";

	var ids = "";

	if(lenSites > 1) {

  

		for (var i = 0; i < lenSites; i++) {



			if(formobj.select[i].checked){

  

				names = names + formobj.siteName[i].value + ";";

				ids = ids + formobj.siteId[i].value + ",";

				

			}		

		}

	

	}

	if(lenSites==1) {



  		if(formobj.select.checked){  	

			names = names + formobj.siteName.value + ";";

			ids = ids + formobj.siteId.value + ",";		

		

		}		



	}





	

	names = names.substring(0,names.length-1);

	ids = ids.substring(0,ids.length-1);
	window.opener.document.forms[from].elements[datafld].value=ids;
	window.opener.document.forms[from].elements[dispfld].value=names;

		self.close();



}





</SCRIPT>







<jsp:useBean id="userB" scope="session" class="com.velos.eres.web.user.UserJB"/>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<%@page import="com.velos.eres.web.study.StudyJB"%>
<%@ page language = "java" import="com.velos.eres.business.group.*,com.velos.eres.service.util.LC,com.velos.eres.service.util.MC,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.*"%>





<% response.setHeader("Cache-Control", "max-age=1, must-revalidate");%>



<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">



</head>



<%

int ienet = 2;



String agent1 = request.getHeader("USER-AGENT");

   if (agent1 != null && agent1.indexOf("MSIE") != -1) 

     ienet = 0; //IE

    else

	ienet = 1;

	if(ienet == 0) {	

%>

<body style="overflow:scroll;">

<%

	} else {

%>

<body>

<%

	}

%>

<%HttpSession tSession = request.getSession(true);

  if (sessionmaint.isValidSession(tSession))

{
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<jsp:include page="include.jsp" flush="true"/>
<%
	String from=request.getParameter("from") ;
	String datafld=request.getParameter("datafld") ;
	String dispfld=request.getParameter("dispfld") ;
	
	int selectedSites=0;
	String names = "";
	String ids = "";
	ArrayList cId=null,cName=null;
	names = request.getParameter("names");
	names=(names==null)?"":(names);
	ids = request.getParameter("ids");
	while(names.indexOf('~') != -1){
	  	names = names.replace('~',' ');
	}

	String tArea = request.getParameter("tArea");
	String rownum=request.getParameter("rownum");
	String accountId = (String) tSession.getValue("accountId");
	String userJobType = "";
	String dJobType = "";
	String dAccSites ="";
	CodeDao cd = new CodeDao();

	if (TAreaDisSiteFlag.equals(LC.Config_TAreaDisSite_Switch)){
		if (!StringUtil.isEmpty(tArea)){
			CodeDao tAreaCD = new CodeDao();
			cd.getCodeValues("disease_site", tAreaCD.getCodeSubtype(StringUtil.stringToNum(tArea)), 1);
		}
	} else {
		cd.getCodeValues("disease_site");
	}
	cId=cd.getCId();
	cName=cd.getCDesc();
	String name="",id="";
	StringTokenizer namesSt=new StringTokenizer(names,";");
	StringTokenizer idsSt=new StringTokenizer(ids,",");
	selectedSites=idsSt.countTokens();
	
		

%>





<DIV class="popDefault"> 
<form name="selectSite" method="POST" id="selectSiteFrm" onSubmit="setSites(document.selectSite)">
<% if (selectedSites>0){%>
<table><tr><td><p class = "sectionHeadings" ><%=MC.M_Already_SelectedSites%><%--Already Selected Sites*****--%>:</p></td></tr></table>
<table width="100%">
		
      <tr class = "popupHeader"> 
         <th width="10%"><%=LC.L_Select%><%--Select*****--%></th>
         <th width="90%"><%=LC.L_Disease_Site%><%--Disease Site*****--%></th>
      </tr>
   <%for (int i=0;i<selectedSites;i++){
   	id=idsSt.nextToken();
	name=namesSt.nextToken();
   	if ((i%2)==0) {
	%>
      <tr class="browserEvenRow"> 
  <%
		}
	else {
  %>
      <tr class="browserOddRow"> 
   <%
		}
	%>
	<td width="10%"><input type="checkbox"  name="select" checked></td>
	<td width="90%"><%=name%></td>
	<input type="hidden" name="siteId" value="<%=id%>">
	<input type="hidden" name="siteName" value="<%=name%>">
	 </tr>
	 
<%}%>
</table>
<%}%>
	 <br><br>
		
	<table><tr><td><P class="defComments"> <%=MC.M_SelFrom_FlwList%><%--Please select from the following list*****--%>:</P></td></tr></table>	
	
	<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="N"/>
		<jsp:param name="formID" value="selectSiteFrm"/>
		<jsp:param name="showDiscard" value="Y"/>
	</jsp:include>

	 <br>
 <table width="100%">
      <tr class = "popupHeader"> 
         <th width="10%"><%=LC.L_Select%><%--Select*****--%></th>
         <th width="90%"><%=LC.L_Disease_Site%><%--Disease Site*****--%></th>
      </tr>
      <input type="hidden" name="len" value="<%=cId.size()+selectedSites%>">
      <input type="hidden" name="from" value="<%=from%>">
      <input type="hidden" name="datafld" value="<%=datafld%>">
      <input type="hidden" name="dispfld" value="<%=dispfld%>">
 <%for(int i=0;i<cId.size();i++){
  	id=EJBUtil.integerToString((Integer)cId.get(i));
	name=(String)cName.get(i);
 	if ((i%2)==0) {
	%>
      <tr class="browserEvenRow"> 
  <%
		}
	else {
  %>
      <tr class="browserOddRow"> 
   <%
		}
	%>
<td width="10%"><input type="checkbox" name="select"></td><td width="90%"><%=name%></td>
<input type="hidden" name="siteId" value="<%=id%>">
<input type="hidden" name="siteName" value="<%=name%>">

 </tr>	
 <%}%>
	</table>
	

<jsp:include page="submitBar.jsp" flush="true"> 
	<jsp:param name="displayESign" value="N"/>
	<jsp:param name="formID" value="selectSiteFrm"/>
	<jsp:param name="showDiscard" value="Y"/>
</jsp:include>

</form>
	<%
 }//end of if body for session

else{

%>

  <jsp:include page="timeout_childwindow.jsp" flush="true"/>

  <%

}


%>

</div>

<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>

</body>
</html>

