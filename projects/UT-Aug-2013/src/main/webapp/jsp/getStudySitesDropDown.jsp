<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.json.*"%>
<%@ page language = "java" import = "com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.esch.business.common.*,java.util.*"%>
<jsp:useBean id ="sessionmaint2" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="studySiteB" scope="session" class="com.velos.eres.web.studySite.StudySiteJB"/>
<%
	HttpSession tSession2 = request.getSession(true);
	request.setCharacterEncoding("UTF-8");
	response.setContentType("application/json");
	JSONObject jsObj = new JSONObject();
	if (!sessionmaint2.isValidSession(tSession2)) {
		// Session is invalid; print an error and exit
		response.sendError(HttpServletResponse.SC_FORBIDDEN, "SESSION_TIME_OUT");
		jsObj.put("result", -1);
		jsObj.put("resultMsg", MC.M_UsrNot_LoggedIn);/*jsObj.put("resultMsg", "User is not logged in.");*****/
		out.println(jsObj.toString());
		return;
	}

   	if (sessionmaint2.isValidSession(tSession2)){
		int studyId = StringUtil.stringToNum((String) request.getParameter("studyId"));
		int userId = StringUtil.stringToNum((String) tSession2.getValue("userId"));
		int accId = StringUtil.stringToNum((String) tSession2.getValue("accountId"));
		int selOrgId = StringUtil.stringToNum((String) request.getParameter("selOrgId"));
		Integer org ;
	
		if (selOrgId == 0){
			//Logged-in user's default organisation
			UserDao usrDao = new UserDao();
			selOrgId = StringUtil.stringToNum(usrDao.getUserDefaultSite(""+userId));
		}
		StudySiteDao ssDao = new StudySiteDao();
		ssDao	= studySiteB.getStudySiteTeamValues(studyId, 0, userId, accId);
		ArrayList arrSiteIds = ssDao.getSiteIds();
		ArrayList arrSiteNames = ssDao.getSiteNames();
		ArrayList arrSiteSampleSizes = ssDao.getLSampleSize();
		
		jsObj.put("arrSiteIds", arrSiteIds);
		jsObj.put("arrSiteNames", arrSiteNames);
		jsObj.put("arrSiteSampleSizes", arrSiteSampleSizes);
		
		StringBuffer sb = new StringBuffer();		
		sb.append("<SELECT id='accsites' NAME='accsites'>") ;
		sb.append("<OPTION value=''>"+LC.L_Select_AnOption+"</OPTION>");
		if (arrSiteIds.size() > 0){
			for (int counter = 0; counter <= arrSiteNames.size() -1 ; counter++){
				org = (Integer) arrSiteIds.get(counter);
				if(	org.intValue() == selOrgId)	{
					sb.append("<OPTION value = "+ org+" SELECTED>" + arrSiteNames.get(counter)+ "</OPTION>");
				}else{
					sb.append("<OPTION value = "+ org+">" + arrSiteNames.get(counter)+ "</OPTION>");
				}
			}
		}
		sb.append("</SELECT>");
		String ddString = "";
		ddString = sb.toString();
		jsObj.put("ddSites", sb);

		out.println(jsObj.toString());

	} %>