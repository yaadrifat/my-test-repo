<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language = "java" import = "com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.esch.business.common.*,java.util.*"%>

<jsp:useBean id ="sessionmaint2" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="ctrldao1" scope="request" class="com.velos.esch.business.common.EventdefDao"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>

<%

   HttpSession tSession2 = request.getSession(true);
   if (sessionmaint2.isValidSession(tSession2))
	{
	String codeType = request.getParameter("codeType");
	String listName = request.getParameter("listName");

	String selectedVals = request.getParameter("selectedVals");
	ArrayList selectedValsArray = new ArrayList();
	selectedValsArray = StringUtil.strArrToArrayList(selectedVals.split(","));

	String filter = request.getParameter("filter");
	String prop = request.getParameter("prop");
	if(prop == null) prop ="";
	String ddString = "";
	int length= 0;

	CodeDao cdList = new CodeDao();
	if ("customCol".equals(filter)){
		String customColValue = request.getParameter("customColValue");
		if (null == customColValue) customColValue = "";
		
		String [] strCustomColValues = customColValue.split(",");
		cdList.getCodeValues(codeType, strCustomColValues);	
	}

	ArrayList codeIds = cdList.getCId();
	ArrayList codeDescs = cdList.getCDesc();
	
	ddString = cdList.toMultiSelectList(listName,selectedValsArray,"");
%>
<%= ddString %>

<% } %>