var getStudyTeamRoleJS = {
	getStudyTeamByRole:{},
	getPIDetails:{},
	getPICellDetails:{},
	setMyDataInSpan: {}
};

getStudyTeamRoleJS.setMyDataInSpan = function (fld, data, spanId){
	if (!fld) { return; }
	fld.value = data;
	fld.readonly = true;
	fld.style.display ='none';

	var dataSpan = document.getElementById(spanId);
	if (!dataSpan){
		dataSpan = document.createElement('span');
		dataSpan.id = spanId;
		dataSpan.name = spanId;
		fld.parentNode.appendChild(dataSpan);
	}
	dataSpan.innerHTML = data;
};

getStudyTeamRoleJS.getStudyTeamByRole = function (roleSubType, roleFieldID){
	if (!roleSubType || roleSubType.length <= 0){
		return;
	}
	
	if (!roleFieldID || roleFieldID.length <= 0){
		return;
	}
	
	var roleField = document.getElementById(formFieldMappingsJS.getColumnSysId(roleFieldID));
	if (!roleField) return;
	
	getStudyTeamRoleJS.setMyDataInSpan(roleField, '', 'span'+roleSubType);

	if (document.getElementsByName("studyId")[0]){
		var studyId = document.getElementsByName("studyId")[0].value;
		jQuery.ajax({
			type: "POST",
			url:"getStudyTeam.jsp",
			data : { 
				"studyId" :studyId,
				"role_subType": roleSubType
			},
	        success: function(resp) {
				var codeId = resp.teamRoleCodeId;
	        	var arrFirstNames = resp.arrFirstNames;
				var arrLastNames = resp.arrLastNames;
				var codeDesc = resp.teamRoleCodeDesc;										
        		var teamMemNames = '';
	        	if (codeId > 0 ){
	        		for(var count = 0; count < arrFirstNames.length; count++){
	        			if (count > 0 ){
	        				teamMemNames += ',';
	        			}
	        			teamMemNames += '[' + arrLastNames[count] + ' ' + arrFirstNames[count] + ']';
	        		}
				}
	    		getStudyTeamRoleJS.setMyDataInSpan(roleField, teamMemNames, 'span'+roleSubType);
			}
		});
	}
}

getStudyTeamRoleJS.getPIDetails = function (){
	
	var ctrc_hpt_piname = document.getElementById(formFieldMappingsJS.getColumnSysId('ctrc_hpt_piname'));
	getStudyTeamRoleJS.setMyDataInSpan(ctrc_hpt_piname, '', 'spanPIName');

	var ctrc_hpt_officenumber = document.getElementById(formFieldMappingsJS.getColumnSysId('ctrc_hpt_officenumber'));
	getStudyTeamRoleJS.setMyDataInSpan(ctrc_hpt_officenumber, '', 'spanPIOfficePhone');
	
	if (document.getElementsByName("studyId")[0]){
		var studyId = document.getElementsByName("studyId")[0].value;
		jQuery.ajax({
			type: "POST",
			url:"getStudySummaryFields.jsp",
			data : { 
				"studyId" :studyId
			},
	        success: function(resp) {
	        	var primInvName = resp.primInvName;
	        	primInvName = (primInvName?primInvName:'');
	    		getStudyTeamRoleJS.setMyDataInSpan(ctrc_hpt_piname, '[' + primInvName + ']', 'spanPIName');

	    		var primInvPhone = resp.addOfficeNum;
	    		primInvPhone = (primInvPhone?primInvPhone:'');
	    		getStudyTeamRoleJS.setMyDataInSpan(ctrc_hpt_officenumber, primInvPhone, 'spanPIOfficePhone');

	    		getStudyTeamRoleJS.getPICellDetails(resp.primInvUserId);
			}
		});
	}
}

getStudyTeamRoleJS.getPICellDetails = function (primInv){
	var ctrc_hpt_cellnumber = document.getElementById(formFieldMappingsJS.getColumnSysId('ctrc_hpt_cellnumber'));
	getStudyTeamRoleJS.setMyDataInSpan(ctrc_hpt_cellnumber, '', 'spanPIMobilePhone');
	
	if (!primInv || (primInv.length == 0)) return;
	
	jQuery.ajax({
		type: "POST",
		url:"getUserMoreDetails.jsp",
		data : { 
			"userId" :primInv,
			"moreDetailTypes": "user_1"
		},
        success: function(resp) {
        	var mDetailJSArray = resp;
        	var primInvMobile = (mDetailJSArray[0]).userMoreDetail;
        	var primInvMobileNew = primInvMobile.replace(/[-]/g, "&minus;");
        	primInvMobileNew = (primInvMobileNew?primInvMobileNew:'');
        	
        	var ctrc_hpt_cellnumber = document.getElementById(formFieldMappingsJS.getColumnSysId('ctrc_hpt_cellnumber'));

        	getStudyTeamRoleJS.setMyDataInSpan(ctrc_hpt_cellnumber, primInvMobileNew, 'spanPIMobilePhone');
		}
	});
}
function getStudyTeamByRole_Init() {
	$j('#'+formFieldMappingsJS.getColumnSysId('ctrc_hpt_piname')).hide();
	$j('#'+formFieldMappingsJS.getColumnSysId('ctrc_hpt_officenumber')).hide();
	$j('#'+formFieldMappingsJS.getColumnSysId('ctrc_hpt_cellnumber')).hide();
	$j('#'+formFieldMappingsJS.getColumnSysId('ctrc_hptregaffairsstaff')).hide();
	$j('#'+formFieldMappingsJS.getColumnSysId('ctrc_hpt_rnsc')).hide();
	$j('#'+formFieldMappingsJS.getColumnSysId('ctrc_study_co')).hide();
	$j('#'+formFieldMappingsJS.getColumnSysId('ctrc_hpt_cra')).hide();
	
	getStudyTeamRoleJS.getStudyTeamByRole("role_regcon", 'ctrc_hptregaffairsstaff'); //Regulatory Contact
	getStudyTeamRoleJS.getStudyTeamByRole("ResNurse", 'ctrc_hpt_rnsc'); //Study Nurse
	getStudyTeamRoleJS.getStudyTeamByRole("role_coord", 'ctrc_study_co'); //Study Coordinator
	getStudyTeamRoleJS.getStudyTeamByRole("role_cra", 'ctrc_hpt_cra');  //CRA
	getStudyTeamRoleJS.getStudyTeamByRole("role_coinv", 'ctrc_hpt_copiname');  //Co-PI Name
	getStudyTeamRoleJS.getPIDetails();
}

$j(document).ready(function(){ 
	formFieldMappingsJS.loadFormFieldMappings();
	getStudyTeamByRole_Init();
});
