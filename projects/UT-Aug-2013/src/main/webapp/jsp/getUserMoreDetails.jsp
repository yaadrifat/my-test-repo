<%@page import="com.velos.eres.business.study.impl.StudyBean"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.json.*"%>
<%@ page language = "java" import = "com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.esch.business.common.*,java.util.*"%>
<%@ page language = "java" import = "com.velos.eres.web.moreDetails.MoreDetailsJB"%>
<jsp:useBean id ="sessionmaint2" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="studyIdB" scope="session" class="com.velos.eres.web.studyId.StudyIdJB"/>
<jsp:useBean id="grpRightsB" scope="request" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
<%
	HttpSession tSession2 = request.getSession(true);
	request.setCharacterEncoding("UTF-8");
	response.setContentType("application/json");
	JSONArray jsObjArray = new JSONArray();
	JSONObject jsObj = new JSONObject();
	if (!sessionmaint2.isValidSession(tSession2)) {
		// Session is invalid; print an error and exit
		response.sendError(HttpServletResponse.SC_FORBIDDEN, "SESSION_TIME_OUT");
		jsObj.put("result", -1);
		jsObj.put("resultMsg", MC.M_UsrNot_LoggedIn);/*jsObj.put("resultMsg", "User is not logged in.");*****/
		out.println(jsObj.toString());
		return;
	}

   	if (sessionmaint2.isValidSession(tSession2)){
		int eresearchUserId = StringUtil.stringToNum((String) request.getParameter("userId"));
		int userId = StringUtil.stringToNum((String) tSession2.getAttribute("userId"));
		int accId = StringUtil.stringToNum((String) tSession2.getAttribute("accountId"));
		String defUserGroup = (String) tSession2.getAttribute("defUserGroup");
	
		/*int grpRt = StringUtil.stringToNum(grpRightsB.getFtrRightsByValue("MUSERS"));
		if (!StringUtil.isAccessibleFor(grpRt, 'V')){
			response.sendError(HttpServletResponse.SC_FORBIDDEN, "ERROR");
			jsObj.put("result", -2);
			jsObj.put("resultMsg", MC.M_InsuffAccRgt_CnctSysAdmin);
			out.println(jsObj.toString());
			return;
		}*/
		
		String moreDetailTypes = request.getParameter("moreDetailTypes");
		String [] moreDetailsSubTypes = (StringUtil.isEmpty(moreDetailTypes))? 
				null : moreDetailTypes.split(",");
		if (null == moreDetailsSubTypes){
			return;
		}
		
		MoreDetailsJB mdJB = new MoreDetailsJB();
		MoreDetailsDao mdDao = new MoreDetailsDao();
		mdDao = mdJB.getMoreDetails(eresearchUserId,"user",defUserGroup);
		
		CodeDao cdDao = new CodeDao();
		/*//Following code is compatible with v9.3 compatible
		cdDao.getCodeValuesBySubTypes("studyidtype", moreDetailsSubTypes);
		ArrayList codePKs = cdDao.getCId();
		ArrayList codeSubTypes = cdDao.getCSubType();*/
		
		//Following code is compatible with v9.2 --BEGIN
		ArrayList codePKs = new ArrayList();
		ArrayList codeSubTypes = new ArrayList();
		
		for(int indx=0; indx < moreDetailsSubTypes.length; indx++){
			cdDao = new CodeDao();
			int codeId = cdDao.getCodeId("user", moreDetailsSubTypes[indx]);
			codePKs.add(codeId);
			codeSubTypes.add(moreDetailsSubTypes[indx]);
		}
		//--END
		
		
		int cdMSDetailLength = (null == codePKs)? 0 : (codePKs).size();
		if (cdMSDetailLength <= 0){
			return;
		}
		
		// Code added for the compatibility with v9.2
		ArrayList elementIds = mdDao.getMdElementIds();
		ArrayList elementValues = mdDao.getMdElementValues();
		CodeDao cdMoreDetailDao = null;
		int id = 0;

		for (int indx = 0; indx < cdMSDetailLength; indx++){
			jsObj = new JSONObject();
			
			String codeId = (codePKs.get(indx)).toString();
			String cdMSDetailsSubType = (codeSubTypes.get(indx)).toString();
			
			String userMoreDetail = "";
			int codeIndex = elementIds.indexOf(StringUtil.stringToNum(codeId));
			if (codeIndex<= -1){
				jsObj.put("userMoreDetailPK", codePKs.get(indx));
				jsObj.put("userMoreDetailSubType", cdMSDetailsSubType);
				jsObj.put("userMoreDetail", userMoreDetail);
			} else {
				userMoreDetail = (String)elementValues.get(codeIndex);
				jsObj.put("userMoreDetailPK", codePKs.get(indx));
				jsObj.put("userMoreDetailSubType", cdMSDetailsSubType);
				jsObj.put("userMoreDetail", userMoreDetail);
			}
			jsObjArray.put(indx,jsObj);
			jsObj = new JSONObject();
		}
		
		/*//Code compatible with v9.3
		ArrayList elementIds = mdDao.getMdElementIds();
		ArrayList elementCodeSubtypes = mdDao.getMdElementKeys(); //Code compatible with v9.3
		ArrayList elementValues = mdDao.getMdElementValues();
		CodeDao cdMoreDetailDao = null;
		int id = 0;

		for (int indx = 0; indx < cdMSDetailLength; indx++){
			jsObj = new JSONObject();
			String cdMSDetailsSubType = (codeSubTypes.get(indx)).toString();

			String userMoreDetail = "";
			int codeIndex = elementCodeSubtypes.indexOf(cdMSDetailsSubType);
			if (codeIndex<= -1){
				jsObj.put("userMoreDetailPK", codePKs.get(indx));
				jsObj.put("userMoreDetailSubType", cdMSDetailsSubType);
				jsObj.put("userMoreDetail", userMoreDetail);
			} else {
				userMoreDetail = (String)elementValues.get(codeIndex);
				jsObj.put("userMoreDetailPK", codePKs.get(indx));
				jsObj.put("userMoreDetailSubType", cdMSDetailsSubType);
				jsObj.put("userMoreDetail", userMoreDetail);
			}
			jsObjArray.put(indx,jsObj);
			jsObj = new JSONObject();
		}*/

		out.println(jsObjArray.toString());

	} %>