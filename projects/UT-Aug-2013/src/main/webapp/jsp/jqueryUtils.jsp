<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/><%@page import="com.velos.eres.service.util.*"%>
<link rel="stylesheet" href="js/jquery/demos/demos.css">
<link rel="stylesheet" href="js/jquery/themes/base/jquery.ui.all.css">	
<link href="js/jquery/themes/base/jquery.ui.autocomplete.css" rel="stylesheet" type="text/css"/>
<script src="js/velos/dateformatSetting.js"></script>
<script src="js/jquery/jquery-1.4.4.js"></script>
<script src="js/jquery/ui/jquery.ui.core.js"></script>
<script src="js/jquery/ui/jquery.ui.widget.js"></script>
<script src="js/jquery/ui/jquery.ui.datepicker.js"></script>
<script src="js/jquery/ui/jquery.ui.mouse.js"></script>
<script src="js/jquery/ui/jquery.ui.draggable.js"></script>
<script src="js/jquery/ui/jquery.ui.position.js"></script>
<script src="js/jquery/ui/jquery.ui.resizable.js"></script>
<script src="js/jquery/ui/jquery.ui.accordion.js"></script>
<script src="js/jquery/ui/jquery.ui.sortable.js"></script> <%--YK: Added for PCAL-20461 - For sortable utility--%>
<script src="js/jquery/ui/jquery.ui.dialog.js"></script>
<script src="js/jquery/ui/jquery.ui.button.js"></script>
<script src="js/jquery/ui/jquery.ui.tabs.js"></script>
<script type="text/javascript" src="js/mgPaneljs/dropmenu.js"></script> 
<script src="js/jquery/ui/jquery.ui.autocomplete.js"></script>
<script language="JavaScript" src="js/velos/velosConfig.js"></script>
<script>
/**
 * Converts project date format setting in to jquery dateformat setting.
 */ 
function  compiledFormat()
{
 var compiledDateFormat = calDateFormat;
	 compiledDateFormat = compiledDateFormat.replace("MMM","M");
	 compiledDateFormat = compiledDateFormat.replace("MM","mm");
	 compiledDateFormat = compiledDateFormat.replace("yyyy","yy");
	 return compiledDateFormat;		 
}
var jQueryDateFormat = compiledFormat() ;
/**
 * Forms the date range by taking upperbound and lowerbound from velosConfig.js. 
 */
var eResYearRange = "-"+YEAR_LOWER_BOUND+":"+"+"+YEAR_UPPER_BOUND ;
/**
 * Opens jquery datepicker based on selected theme and dateformat. 
 */
 $.noConflict();
 jQuery(document).ready(openCal);
var $j = jQuery;


function openCal() {
	var datefields = $j(".datefield");
	if (datefields && datefields.length && datefields.length > 0){
		for (var indx = 0; indx < datefields.length; indx++){
			var form = datefields[indx].form;
			var showDPicker = true;
			if (datefields[indx].readOnly){
				/* Datepicker is not displayed for Readonly date type form fields. 
				All other places in the application with Readonly date type fields, Datepicker will be displayed. */
				showDPicker = ($j(form).attr('name') == 'er_fillform1')? false : true;
			}
			if (datefields[indx].disabled){
				showDPicker = false;
			}
			if (showDPicker){
				$j(datefields[indx]).datepicker({
					changeMonth: true,
					changeYear: true,
					yearRange: eResYearRange,
					showButtonPanel: true,
					prevText: '',
					nextText: '',
					closeText: L_Clear,
					currentText: L_Today,
					dateFormat: jQueryDateFormat
				});
			}
		}
	}
}

function enableCal(dateFld) {
	if ($j(dateFld)){
		$j(dateFld).datepicker("enable");
	}
}

function disableCal(dateFld) {
	if ($j(dateFld)){	
		$j(dateFld).datepicker("disable");
	}
}
 jQuery.datepicker._generateHTML_Old = jQuery.datepicker._generateHTML;
 /**
 * Clear date functionality.
 **/
	jQuery.datepicker._generateHTML = function(inst) {
	    res = this._generateHTML_Old(inst);
	    res = res.replace("_hideDatepicker()","_clearDate('#"+inst.id+"')"); 
	    return res;
	}



	//Added by AK for INV9.1 enhancement(Auto complete feature).
	function openAuto(column,idColumn,data,columnName){
		jQuery( "#"+column ).autocomplete({
			autoFocus: true,
			extraParams: {
	        format: 'json' 
	        },
	        minLength:0,
	        change: function(event,ui) {
	        	validateAutoComplete(column,idColumn,data,columnName,true);
	        },        
	        select:function(event,ui) {
	        	jQuery("#"+idColumn).val(ui.item.id);
	        	jQuery("#"+column).val(ui.item.value);
	          },  
	        source: data
	    });
	    return true;

	} 

	//Ak:Added the autocomplete validation here
	validateAutoComplete = function (column,idColumn,data,columnName,autoCompFlag){
		var fieldValue=document.getElementById(column).value;
		var fieldId=document.getElementById(idColumn).value;
		
		
		if(fieldValue==null || fieldValue==""){
			document.getElementById(idColumn).value="";
			return true;
		}
		for (var j=0; j<data.length; j++) {
			if(data[j].value.toLowerCase()==fieldValue.toLowerCase()){
				document.getElementById(idColumn).value=data[j].id;
		        	return true;
		     }
	    }
		var paramArray = [columnName];
		if(autoCompFlag)
		alert(getLocalizedMessageString("M_NoSuch_PlsVldVal",paramArray));/*alert("No such "+columnName+" exists.Please enter a valid value.");*****/
		document.getElementById(idColumn).value="";
		document.getElementById(column).value="";
		
		
	}
	
</script>

<SCRIPT>
	$j(function() {
		$j( "#tabs" ).tabs();
		
		if ($j("input:submit")){
			$j( "input:submit").button();
		}
		if ($j("a:submit")){
			$j( "a:submit").button();
		}
		if ($j("button")){
			$j( "button").button();
		}
		//$( "a", ".demo" ).click(function() { return false; });
	});	
</SCRIPT>