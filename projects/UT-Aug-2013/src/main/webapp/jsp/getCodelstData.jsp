<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language = "java" import = "com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.esch.business.common.*,java.util.*"%>
<%@page import="org.json.*"%>
<jsp:useBean id ="sessionmaint2" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="ctrldao1" scope="request" class="com.velos.esch.business.common.EventdefDao"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>

<%

   HttpSession tSession2 = request.getSession(true);
   request.setCharacterEncoding("UTF-8");
   response.setContentType("application/json");
   
   if (sessionmaint2.isValidSession(tSession2))
	{
	String codeType = request.getParameter("codeType");

	String selectedVals = request.getParameter("selectedVals");
	ArrayList selectedValsArray = new ArrayList();
	selectedValsArray = StringUtil.strArrToArrayList(selectedVals.split(","));

	String filter = request.getParameter("filter");
	String prop = request.getParameter("prop");
	if(prop == null) prop ="";
	String ddString = "";
	int length= 0;

	CodeDao cdList = new CodeDao();
	if ("customCol".equals(filter)){
		String customColValue = request.getParameter("customColValue");
		if (null == customColValue) customColValue = "";
		
		String [] strCustomColValues = customColValue.split(",");
		cdList.getCodeValuesFilterCustom(codeType, strCustomColValues);	
	} else {
		String [] codelstPKs = (StringUtil.isEmpty(selectedVals))? 
				null : selectedVals.split(",");
		CodeDao cdDao = null;
		for(int indx=0; indx < codelstPKs.length; indx++){
			int codeId = StringUtil.stringToInteger(codelstPKs[indx]);
			cdList.setCId(codeId);
			cdDao = new CodeDao();
			cdList.setCDesc(cdDao.getCodeDescription(codeId));
		}
	}

	JSONObject jsObj = new JSONObject();
	ArrayList<Integer> codeIds = cdList.getCId();
	ArrayList<String> codeDescs = cdList.getCDesc();
	JSONArray codeItemArray = new JSONArray();
	for (Integer cId : codeIds) {
		codeItemArray.put(cId);
	}
	JSONArray codeItemDescArray = new JSONArray();
	for (String cDesc : codeDescs) {
		codeItemDescArray.put(cDesc);
	}
	jsObj.put("codeIds", codeItemArray);
	jsObj.put("codeDescs", codeItemDescArray);

	//jsObj.put("multiSelectListHTML", ddString);
	System.out.println("jsObj is "+jsObj.toString());
	out.println(jsObj.toString());
%>
<% } %>