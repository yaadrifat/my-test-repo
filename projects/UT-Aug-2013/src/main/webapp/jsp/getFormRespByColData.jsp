<%@page import="com.velos.eres.business.study.impl.StudyBean"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.json.*"%>
<%@ page language = "java" import = "com.velos.eres.service.util.*,com.velos.eres.business.common.*,java.util.*"%>
<jsp:useBean id ="sessionmaint2" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%
	HttpSession tSession2 = request.getSession(true);
	request.setCharacterEncoding("UTF-8");
	response.setContentType("application/json");
	JSONObject jsObj = new JSONObject();
	if (!sessionmaint2.isValidSession(tSession2)) {
		// Session is invalid; print an error and exit
		response.sendError(HttpServletResponse.SC_FORBIDDEN, "SESSION_TIME_OUT");
		jsObj.put("result", -1);
		jsObj.put("resultMsg", MC.M_UsrNot_LoggedIn);/*jsObj.put("resultMsg", "User is not logged in.");*****/
		out.println(jsObj.toString());
		return;
	}

   	if (sessionmaint2.isValidSession(tSession2)){
		int formId = StringUtil.stringToNum((String) request.getParameter("formId"));
		int userId = StringUtil.stringToNum((String) tSession2.getValue("userId"));
		int accId = StringUtil.stringToNum((String) tSession2.getValue("accountId"));

		ArrayList getColumns = new ArrayList();
		ArrayList matchColumns = new ArrayList();
		ArrayList matchWiths = new ArrayList();
		
		Enumeration paramNames = request.getParameterNames();
		while (paramNames.hasMoreElements())
		{
   			String param = (String) paramNames.nextElement();
   			if (param.startsWith("getColumn")) {
   				String getColumn = StringUtil.stripScript((String)request.getParameter(param));
   				getColumns.add(getColumn);
   			}
   			if (param.startsWith("matchColumn")) {
   				String matchColumn = StringUtil.stripScript((String)request.getParameter(param));
   				matchColumns.add(matchColumn);
   			}
   			if (param.startsWith("matchWith")) {
   				String matchWith = StringUtil.stripScript((String)request.getParameter(param));
   				matchWiths.add(matchWith);
   			}
		}

		FormCompareUtilityDao fcDao = new FormCompareUtilityDao();
		if (getColumns == null || getColumns.size() == 0) {
			//Number of filter columns and values do not match
			response.sendError(HttpServletResponse.SC_FORBIDDEN, "ERROR");
			jsObj.put("result", -1);
			jsObj.put("resultMsg", MC.Lkp_InvalidInputParam);
			out.println(jsObj.toString());
			return;
		}
		
		ArrayList matchColumnsMod = new ArrayList();
		if (matchColumns != null && matchWiths != null) {
			if (matchColumns.size() != matchWiths.size()) {
				//Number of filter columns and values do not match
				response.sendError(HttpServletResponse.SC_FORBIDDEN, "ERROR");
				jsObj.put("result", -1);
				jsObj.put("resultMsg", MC.Lkp_InvalidInputParam);
				out.println(jsObj.toString());
				return;
			}
		}

		JSONArray jsRecords = new JSONArray();
		HashMap<String, HashMap> records = new HashMap();
		FormResponseDao frDao = new FormResponseDao();
		records = frDao.getFormRespByColData(userId, formId, getColumns, matchColumns, matchWiths);

		for (String key : records.keySet()) {
		    System.out.println("Key = " + key);
	        HashMap valueHash = records.get(key);
	        
	        JSONObject jsRecObj = new JSONObject();
	        jsRecObj.put("pk", valueHash.get("PK_FORMSLINEAR"));
	        jsRecObj.put("rec", EJBUtil.hashMapToJSON(valueHash));
	        jsRecords.put(jsRecObj);
		}

		jsObj.put("reccount", records.size());
		jsObj.put("recArray", jsRecords);

		out.println(jsObj.toString());
	} %>