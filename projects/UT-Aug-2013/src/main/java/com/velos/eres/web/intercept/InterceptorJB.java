package com.velos.eres.web.intercept;

/**
 * This is the base class for a customization hook that can be called
 * from anywhere. Refer to updateFormData.jsp for an example to add a hook.
 */
public class InterceptorJB {
	public void handle(Object...args) {}
}
