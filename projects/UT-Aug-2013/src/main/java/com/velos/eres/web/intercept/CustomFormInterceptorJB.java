package com.velos.eres.web.intercept;

import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.FormCompareUtilityDao;
import com.velos.eres.service.util.SessionMaint;
import com.velos.eres.web.studyId.StudyIdJB;
import com.velos.esch.service.util.StringUtil;

public class CustomFormInterceptorJB extends InterceptorJB {
	@Override
	public void handle(Object... args) {
		System.out.println("hello world2!");
		if (args == null || args.length < 1) { return; }
		HttpServletRequest request = (HttpServletRequest)args[0];
		HttpSession tSession = request.getSession(true);
		
		SessionMaint sessionmaint = new SessionMaint();
		if (!sessionmaint.isValidSession(tSession)){
			return;
		}

		String userId = (String)tSession.getAttribute("userId");
		System.out.println("userId="+tSession.getAttribute("userId"));
		String studyId = request.getParameter("formStudy");
		if (StringUtil.isEmpty(studyId) || StringUtil.stringToNum(studyId) <= 0){
			return;
		}
		
		String formDispLocation = request.getParameter("formDispLocation");
		if (StringUtil.isEmpty(formDispLocation)){
			return;
		}
		System.out.println("formDispLocation="+formDispLocation);
		
		if ("S".equals(formDispLocation)){
			String formId = "";
			String formPullDown = request.getParameter("formPullDown");
			if (StringUtil.isEmpty(formPullDown)){
				return;
			} else {
			 	StringTokenizer strTokenizer = new StringTokenizer(formPullDown,"*");
	 	     	formId = strTokenizer.nextToken();
	 	     	//entryChar = strTokenizer.nextToken();
				//numEntries = EJBUtil.stringToNum(strTokenizer.nextToken());
			}
			System.out.println("formPullDown="+formPullDown);
			
			if (StringUtil.isEmpty(formId)){
				return;
			} else {
				int updateMDProtocolID = StringUtil.stringToNum(request.getParameter("updateMDProtocolID"));
				if (updateMDProtocolID > 0){
					FormCompareUtilityDao formCUDao = new FormCompareUtilityDao();
					formCUDao.setFormId(StringUtil.stringToNum(formId));
					formCUDao.getFormFieldMap();

					ArrayList arrColSysIds = formCUDao.getColSysIds();
					ArrayList arrColUIds = formCUDao.getColUIds();
					String colSystemId = "";

					for (int i =0; i < arrColUIds.size(); i++){
						if ("Protocol_ID".equals(arrColUIds.get(i))){
							colSystemId = (String) arrColSysIds.get(i);
							break;
						}
					}

					if (!StringUtil.isEmpty(colSystemId)){
						String colValue = request.getParameter(colSystemId);
						colValue = (StringUtil.isEmpty(colValue))? "" : colValue;

						CodeDao cdDao = new CodeDao();
						int cdProtId = cdDao.getCodeId("studyidtype", "studyidtype_267");
						if (cdProtId <= 0){
							return;
						}

						StudyIdJB studyIdB = new StudyIdJB();
						int ret = -1;
						int studyIdPK = studyIdB.findMoreStudyId(StringUtil.stringToNum(studyId), "studyidtype_267");
						if (studyIdPK > 0){
							studyIdB.setId(studyIdPK);
							studyIdB.getStudyIdDetails();
							
							studyIdB.setAlternateStudyId(colValue);
							studyIdB.setModifiedBy(userId);
							studyIdB.setIpAdd((String)tSession.getAttribute("ipAdd"));
							ret = studyIdB.updateStudyId();
						} else {
							studyIdB.setStudyId(studyId);
							studyIdB.setStudyIdType(""+cdProtId);
							studyIdB.setAlternateStudyId(colValue);
							studyIdB.setCreator(userId);
							studyIdB.setIpAdd((String)tSession.getAttribute("ipAdd"));
							ret = studyIdB.setStudyIdDetails();
						}

						if (ret == 0){
							System.out.println("Success");
						}
					}
				}
			}
		}
	}

}