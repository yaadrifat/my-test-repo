package com.velos.eres.web.intercept;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.velos.eres.business.common.CodeDao;
import com.velos.eres.service.util.SessionMaint;
import com.velos.eres.web.studyId.StudyIdJB;
import com.velos.esch.service.util.StringUtil;

public class CustomStudyInterceptorJB extends InterceptorJB {
	@Override
	public void handle(Object... args) {
		if (args == null || args.length < 1) { return; }
		HttpServletRequest request = (HttpServletRequest)args[0];
		HttpSession tSession = request.getSession(true);
		
		SessionMaint sessionmaint = new SessionMaint();
		if (!sessionmaint.isValidSession(tSession)){
			return;
		}

		String userId = (String)tSession.getAttribute("userId");
		String studyId = (String)tSession.getValue("studyId");
		if (StringUtil.isEmpty(studyId) || StringUtil.stringToNum(studyId) <= 0){
			return;
		}
		
		int studyResTypeFK = StringUtil.stringToNum(request.getParameter("studyResType"));
		if (studyResTypeFK <= 0){
			return;
		} else {
			CodeDao cdResType = new CodeDao();
			String studyResType = cdResType.getCodeSubtype(studyResTypeFK);
			if (!StringUtil.isEmpty(studyResType)){
				if ((!"coop".equals(studyResType)) && (!"other".equals(studyResType))){
					CodeDao cdDao = new CodeDao();
					int cdProtId = cdDao.getCodeId("studyidtype", "studyidtype_267");
					if (cdProtId <= 0){
						return;
					}
	
					StudyIdJB studyIdB = new StudyIdJB();
					int ret = -1;
					int studyIdPK = studyIdB.findMoreStudyId(StringUtil.stringToNum(studyId), "studyidtype_267");
					if (studyIdPK > 0){
						studyIdB.setId(studyIdPK);
						studyIdB.getStudyIdDetails();
						studyIdB.setModifiedBy(userId);
						studyIdB.setIpAdd((String)tSession.getAttribute("ipAdd"));
						studyIdB.setStudyIdDetails();

						ret = studyIdB.removeStudyId();
					}
	
					if (ret == 0){
						System.out.println("Success");
					}
				}
			}
		}
	}
}