<%@page import="com.velos.eres.business.study.impl.StudyBean"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.json.*"%>
<%@ page language = "java" import = "com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.esch.business.common.*,java.util.*"%>
<jsp:useBean id ="sessionmaint2" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="studyB" scope="session" class="com.velos.eres.web.study.StudyJB"/>
<%
	HttpSession tSession2 = request.getSession(true);
	request.setCharacterEncoding("UTF-8");
	response.setContentType("application/json");
	JSONObject jsObj = new JSONObject();
	if (!sessionmaint2.isValidSession(tSession2)) {
		// Session is invalid; print an error and exit
		response.sendError(HttpServletResponse.SC_FORBIDDEN, "SESSION_TIME_OUT");
		jsObj.put("result", -1);
		jsObj.put("resultMsg", MC.M_UsrNot_LoggedIn);/*jsObj.put("resultMsg", "User is not logged in.");*****/
		out.println(jsObj.toString());
		return;
	}

   	if (sessionmaint2.isValidSession(tSession2)){
		int studyId = StringUtil.stringToNum((String) request.getParameter("studyId"));
		int userId = StringUtil.stringToNum((String) tSession2.getValue("userId"));
		int accId = StringUtil.stringToNum((String) tSession2.getValue("accountId"));
		
		studyB.setId(studyId);
		studyB.getStudyDetails();
		
		String studyNumber = studyB.getStudyNumber();
		String title = studyB.getStudyTitle();

		jsObj.put("studyNumber", studyNumber);
		jsObj.put("studyTitle", title);

		String tArea = studyB.getStudyTArea();
		CodeDao cdDao = new CodeDao();
		String tAreaDesc = cdDao.getCodeDescription(StringUtil.stringToNum(tArea));

		jsObj.put("studytArea", tArea);
		jsObj.put("studytAreaDesc", tAreaDesc);

		out.println(jsObj.toString());

	} %>