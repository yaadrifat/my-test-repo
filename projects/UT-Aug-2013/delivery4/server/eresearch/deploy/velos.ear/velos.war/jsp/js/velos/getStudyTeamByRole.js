function getStudyTeamByRole(roleSubType){
	if (!roleSubType || roleSubType.length <= 0)
		return;
	
	if (document.getElementsByName("studyId")[0]){
		var studyId = document.getElementsByName("studyId")[0].value;
		jQuery.ajax({
			type: "POST",
			url:"getStudyTeam.jsp",
			data : { 
				"studyId" :studyId,
				"role_subType": roleSubType
			},
	        success: function(resp) {
				var codeId = resp.teamRoleCodeId;
				if (codeId > 0 ){
		        	var arrFirstNames = resp.arrFirstNames;
					var arrLastNames = resp.arrLastNames;
					var codeDesc = resp.teamRoleCodeDesc;
		
					var RA_PRC_Approval_Date_TR = document.getElementById(getColumnSysId('RA_PRC_Approval_Date')+"_span").parentNode.parentNode;
					var table = RA_PRC_Approval_Date_TR.parentNode;
					if (RA_PRC_Approval_Date_TR){				
						var list ='';
						for (var indx =0; indx < arrFirstNames.length; indx++){
							if (indx > 0) list += ',';
							list += '['+arrLastNames[indx] + ' ' + arrFirstNames[indx]+']';
						}
						
						var newRowElement = document.createElement("tr");
						newRowElement.id = roleSubType+'_TR';
						
						var newLabelElement = document.createElement("td");
						newLabelElement.innerHTML = codeDesc;
						newRowElement.appendChild(newLabelElement);
						
						var newDataElement = document.createElement("td");
						newDataElement.id = roleSubType+'_TD';
						newDataElement.innerHTML = list;
						newRowElement.appendChild(newDataElement);
	
						table.appendChild(newRowElement);
					}
				}
			}
		});
	}
}
        

function getStudyTeamByRole_Init() {
	getStudyTeamByRole("role_regcon");
	getStudyTeamByRole("ResNurse");
	getStudyTeamByRole("role_coord");
	getStudyTeamByRole("role_coinv");
	getStudyTeamByRole("role_cra");
}

setTimeout(function(){ getStudyTeamByRole_Init();}, 1000);
