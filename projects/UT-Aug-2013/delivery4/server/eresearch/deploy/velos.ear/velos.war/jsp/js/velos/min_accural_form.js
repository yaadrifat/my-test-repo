// --- Calculating Race ---
function cal_TotalRaceM(){
	var blkAfAmM = document.getElementById(getColumnSysId('blkAfAmM'));
	var amIndianM = document.getElementById(getColumnSysId('amIndianm'));
	var asianM = document.getElementById(getColumnSysId('asianM'));
	var natHawaiianM = document.getElementById(getColumnSysId('natHawaiianM'));
	var whiteM = document.getElementById(getColumnSysId('whiteM'));
	var multiRacialM = document.getElementById(getColumnSysId('multiRacialM'));
	var total_raceM = document.getElementById(getColumnSysId('total_raceM'));
	
	var Total = 0;
	if (blkAfAmM.value){
		Total += parseInt(blkAfAmM.value,10);
	}
	if (amIndianM.value){
		Total += parseInt(amIndianM.value,10);
	}
	if (asianM.value){
		Total += parseInt(asianM.value,10);
	}
	if (natHawaiianM.value){
		Total += parseInt(natHawaiianM.value,10);
	}
	if (whiteM.value){
		Total += parseInt(whiteM.value,10);
	}
	if (multiRacialM.value){
		Total += parseInt(multiRacialM.value,10);
	}
	total_raceM.value = Total;
	cal_PercentageRaceM();
}

function cal_TotalRaceF(){
	var blkAfAmF = document.getElementById(getColumnSysId('blkAfAmF'));
	var amIndianF = document.getElementById(getColumnSysId('amIndianF'));
	var asianF = document.getElementById(getColumnSysId('asianF'));
	var natHawaiianF = document.getElementById(getColumnSysId('natHawaiianF'));
	var whiteF = document.getElementById(getColumnSysId('whiteF'));
	var multiRacialF = document.getElementById(getColumnSysId('multiRacialF'));
	var total_raceF = document.getElementById(getColumnSysId('total_raceF'));
	var Total = 0;

	if (blkAfAmF.value){
		Total += parseInt(blkAfAmF.value,10);
	}
	if (amIndianF.value){
		Total += parseInt(amIndianF.value,10);
	}
	if (asianF.value){
		Total += parseInt(asianF.value,10);
	}
	if (natHawaiianF.value){
		Total += parseInt(natHawaiianF.value,10);
	}
	if (whiteF.value){
		Total += parseInt(whiteF.value,10);
	}
	if (multiRacialF.value){
		Total += parseInt(multiRacialF.value,10);
	}
	total_raceF.value = Total;
	cal_PercentageRaceF();
}

function getMePercentage(key){
	var element = document.getElementById(getColumnSysId(key));

	var orgLocalSampleSize = document.getElementById(getColumnSysId('orgLocalSampleSize'));
	var localSampleSize = parseInt(orgLocalSampleSize.value,10);

	var percent = 0;
	var subjectCount = 0;

	if (localSampleSize){
		if(element){
			var subjectCount = parseInt(element.value,10);
			if (!subjectCount)subjectCount = 0;
			percent = (subjectCount/localSampleSize)*100;
		}
		$j("#span_"+key).html("("+percent+"%)");
	} else 
		$j("#span_"+key).html("");
}

// ---Calculating Percentage --- 
function cal_PercentageRaceM(){
	getMePercentage("blkAfAmM");
	getMePercentage("amIndianm");
	getMePercentage("asianM");
	getMePercentage("natHawaiianM");
	getMePercentage("whiteM");
	getMePercentage("multiRacialM");
	getMePercentage("total_raceM");
}
function cal_PercentageRaceF(){
	getMePercentage("blkAfAmF");
	getMePercentage("amIndianF");
	getMePercentage("asianF");
	getMePercentage("natHawaiianF");
	getMePercentage("whiteF");
	getMePercentage("multiRacialF");
	getMePercentage("total_raceF");
}

//--- Calculating Ethnicity ---
function cal_TotalEthnicityM(){
	var hispM = document.getElementById(getColumnSysId('hispM'));
	var notHisM = document.getElementById(getColumnSysId('notHisM'));
	var total_ethnM = document.getElementById(getColumnSysId('total_ethnM'));

	var Total = 0;

	if (hispM.value){
		Total += parseInt(hispM.value,10);
	}
	if (notHisM.value){
		Total += parseInt(notHisM.value,10);
	}
	total_ethnM.value= Total;
	cal_PercentEthnicityM();
}

function cal_TotalEthnicityF(){
	var hispF = document.getElementById(getColumnSysId('hispF'));
	var notHisF = document.getElementById(getColumnSysId('notHisF'));
	var total_ethnF = document.getElementById(getColumnSysId('total_ethnF'));

	var Total = 0;
	if (hispF.value){
		Total += parseInt(hispF.value,10);
	}
	if (notHisF.value){
		Total += parseInt(notHisF.value,10);
	}
	total_ethnF.value= Total;
	cal_PercentEthnicityF();
}

function cal_PercentEthnicityM(){
	getMePercentage("hispM");
	getMePercentage("notHisM");
	getMePercentage("total_ethnM");
}

function cal_PercentEthnicityF(){
	getMePercentage("hispF");
	getMePercentage("notHisF");
	getMePercentage("total_ethnF");
}

function minAccrual_setCalculations(){
	//Organization ID 
	var orgID = document.getElementById(getColumnSysId('orgID'));
	if (!orgID) return;
	//Local Sample Size - orgLocalSampleSize
	var orgLocalSampleSize = document.getElementById(getColumnSysId('orgLocalSampleSize'));

    // ------ Race-------
    // Black or African American Male - blkAmM
    var blkAfAmM = document.getElementById(getColumnSysId('blkAfAmM'));
    $j("#"+blkAfAmM.id).parent().append("<span id='span_blkAfAmM' style='padding:2;'></span>");
    
	// Black or African American Female - blkAmF
    var blkAfAmF = document.getElementById(getColumnSysId('blkAfAmF'));
    $j("#"+blkAfAmF.id).parent().append("<span id='span_blkAfAmF' style='padding:2;'></span>");
    
	// American Indian or Alaskan Native Male - amIndianM
    var amIndianM = document.getElementById(getColumnSysId('amIndianm'));
    $j("#"+amIndianM.id).parent().append("<span id='span_amIndianm' style='padding:2;'></span>");
    
	// American Indian or Alaskan Native Female - amIndianF
    var amIndianF = document.getElementById(getColumnSysId('amIndianF'));
	$j("#"+amIndianF.id).parent().append("<span id='span_amIndianF' style='padding:2;'></span>");

	// Asian Male - asianM
	//var asianM = document.getElementById("fld52865072_32149_40957").value;
	var asianM = document.getElementById(getColumnSysId('asianM'));
	$j("#"+asianM.id).parent().append("<span id='span_asianM' style='padding:2;'></span>");

	// Asian Female - asianF
	var asianF = document.getElementById(getColumnSysId('asianF'));
	$j("#"+asianF.id).parent().append("<span id='span_asianF' style='padding:2;'></span>");
	
	// Native Hawaiian or Other Pacific Islander Male - natHawaiianM
	var natHawaiianM = document.getElementById(getColumnSysId('natHawaiianM'));
	$j("#"+natHawaiianM.id).parent().append("<span id='span_natHawaiianM' style='padding:2;'></span>");
	
	// Native Hawaiian or Other Pacific Islander Female - natHawaiianF
	var natHawaiianF = document.getElementById(getColumnSysId('natHawaiianF'));
	$j("#"+natHawaiianF.id).parent().append("<span id='span_natHawaiianF' style='padding:2;'></span>");
	
	// White Male - whiteM
	var whiteM = document.getElementById(getColumnSysId('whiteM'));
	$j("#"+whiteM.id).parent().append("<span id='span_whiteM' style='padding:2;'></span>");

	// White Female - whiteF
	var whiteF = document.getElementById(getColumnSysId('whiteF'));
	$j("#"+whiteF.id).parent().append("<span id='span_whiteF' style='padding:2;'></span>");
	
	// More Than One Race (Multiracial) Male - multiRacialM
	var multiRacialM = document.getElementById(getColumnSysId('multiRacialM'));
	$j("#"+multiRacialM.id).parent().append("<span id='span_multiRacialM' style='padding:2;'></span>");
	
	// More Than One Race (Multiracial) Female - multiRacialF
	var multiRacialF = document.getElementById(getColumnSysId('multiRacialF'));
	$j("#"+multiRacialF.id).parent().append("<span id='span_multiRacialF' style='padding:2;'></span>");
	
	// Total of Male Race - total_raceM
	var total_raceM = document.getElementById(getColumnSysId('total_raceM'));
	$j("#"+total_raceM.id).parent().append("<span id='span_total_raceM' style='padding:2;'></span>");
	
	// Total of Female Race - total_raceF
	var total_raceF = document.getElementById(getColumnSysId('total_raceF'));
	$j("#"+total_raceF.id).parent().append("<span id='span_total_raceF' style='padding:2;'></span>");
	
	// --- Race --- 
	$j("#"+blkAfAmM.id).change(function(event){
		//(blkAfAmM.onchange)();
		cal_TotalRaceM();
	});

	$j("#"+amIndianM.id).change(function(event){
		//(amIndianM.onchange)();
		cal_TotalRaceM();
	});
	
	$j("#"+asianM.id).change(function(event){
		//(asianM.onchange)();
		cal_TotalRaceM();
	});
	
	$j("#"+natHawaiianM.id).change(function(event){
		//(natHawaiianM.onchange)();
		cal_TotalRaceM();
	});
	
	$j("#"+whiteM.id).change(function(event){
		//(whiteM.onchange)();
		cal_TotalRaceM();
	});
	
	$j("#"+multiRacialM.id).change(function(event){
		//(multiRacialM.onchange)();
		cal_TotalRaceM();
	});
	
	$j("#"+total_raceM.id).change(function (event){
		//(total_raceM.onchange)();
		cal_TotalRaceM();
	});
	
	$j("#"+blkAfAmF.id).change(function(event){
		//(blkAfAmF.onchange)();
		cal_TotalRaceF();
	});
	
	$j("#"+amIndianF.id).change(function(event){
		//(amIndianF.onchange)();
		cal_TotalRaceF();
	});

	$j("#"+asianF.id).change(function(event){
		//(asianF.onchange)();
		cal_TotalRaceF();
	});
	
	$j("#"+natHawaiianF.id).change(function(event){
		//(natHawaiianF.onchange)();
		cal_TotalRaceF();
	});
	
	$j("#"+whiteF.id).change(function(event){
		//(whiteF.onchange)();
		cal_TotalRaceF();
	});
	
	$j("#"+multiRacialF.id).change(function(event){
		//(multiRacialF.onchange)();
		cal_TotalRaceF();
	});
	
	$j("#"+total_raceF.id).change(function(event){
		//(total_raceF.onchange)();
		cal_TotalRaceF();
	});
   
	//----Ethnicity------
	//Hispanic or Latino Male -hispM
	hispM = document.getElementById(getColumnSysId('hispM'));
	$j("#"+hispM.id).parent().append("<span id='span_hispM' style='padding:2;'></span>");

	//Hispanic or Latino Female - hisF
	hispF = document.getElementById(getColumnSysId('hispF'));
	$j("#"+hispF.id).parent().append("<span id='span_hispF' style='padding:2;'></span>");

    // Not Hispanic or Latino Male - notHisM
    notHisM = document.getElementById(getColumnSysId('notHisM'));
    $j("#"+notHisM.id).parent().append("<span id='span_notHisM' style='padding:2;'></span>");

    // Not Hispanic or Latino Female - notHisF
    notHisF = document.getElementById(getColumnSysId('notHisF'));
    $j("#"+notHisF.id).parent().append("<span id='span_notHisF' style='padding:2;'></span>");

    // Total for Male - total_ethnM
    total_ethnM = document.getElementById(getColumnSysId('total_ethnM'));
    $j("#"+total_ethnM.id).parent().append("<span id='span_total_ethnM' style='padding:2;'></span>");

    //Total for Female - total_ethnF
    total_ethnF = document.getElementById(getColumnSysId('total_ethnF'));
    $j("#"+total_ethnF.id).parent().append("<span id='span_total_ethnF' style='padding:2;'></span>");

	// --- Ethnicity ---
	$j("#"+hispM.id).change(function(event){
		//(hispM.onchange)();
		cal_TotalEthnicityM();
	});
	
	$j("#"+notHisM.id).change(function(event){
		//(notHisM.onchange)();
		cal_TotalEthnicityM();
	});

	$j("#"+total_ethnM.id).change(function(event){
		//(total_ethnM.onchange)();
		cal_TotalEthnicityM();
	});
	
	$j("#"+hispF.id).change(function(event){
		//(hispF.onchange)();
		cal_TotalEthnicityF();
	});
	
	$j("#"+notHisF.id).change(function(event){
		//(notHisF.onchange)();
		cal_TotalEthnicityF();
	});

	$j("#"+total_ethnF.id).change(function(event){
		//(total_ethnF.onchange)();
		cal_TotalEthnicityF();
	});
	
	// --- Validation Check Race ---
    /*total_raceM.onchange = minAccrual_validation_check;
	total_raceF.onchange = minAccrual_validation_check;
	total_ethnM.onchange = minAccrual_validation_check;
	total_ethnF.onchange = minAccrual_validation_check;*/
	
}

// --- Validation Check ---
function minAccrual_validation_check(){
	var total_raceM = document.getElementById(getColumnSysId('total_raceM'));
	var total_raceF = document.getElementById(getColumnSysId('total_raceF'));
	var total_ethnM = document.getElementById(getColumnSysId('total_ethnM'));
	var total_ethnF = document.getElementById(getColumnSysId('total_ethnF'));
	var orgLocalSampleSize = document.getElementById(getColumnSysId('orgLocalSampleSize'));
	var localSampleSize = parseInt(orgLocalSampleSize.value,10);
	var total = 0;

	if (total_raceM.value){
		total+=parseInt(total_raceM.value,10);
	}
	if (total_raceF.value){
		total+=parseInt(total_raceF.value,10);
	}
	if(total != localSampleSize){
		alert("Local Size Sample doesn't match with total number of subjects in Gender based Race distribution.");
		var blkAfAmM = document.getElementById(getColumnSysId('blkAfAmM'));
		blkAfAmM.focus();
		return false;
	}

	total = 0;
	if (total_ethnM.value){
		total+=parseInt(total_ethnM.value,10);
	}
	if (total_ethnF.value){
		total+=parseInt(total_ethnF.value,10);
	}
	if(total != localSampleSize){
		var hispM = document.getElementById(getColumnSysId('hispM'));
		hispM.focus();
		alert("Local Size Sample doesn't match with total number of subjects in Gender based Ethnicity distribution.");
		return false;
	}

	if(total_ethnM.value != total_raceM.value){
		var blkAfAmM = document.getElementById(getColumnSysId('blkAfAmM'));
		blkAfAmM.focus();
		alert("Total count of Male Subjects does not match for the Race and Ethnicity based distributions.");
		return false;
	} 
	if(total_ethnF.value != total_raceF.value){
		var blkAfAmF = document.getElementById(getColumnSysId('blkAfAmF'));
		blkAfAmF.focus();
		alert("Total count of Female Subjects does not match for the Race and Ethnicity based distributions.");
		return false;
	}
	return true;
}


//////******************************

function minAccrual_formatHTML(){
var trLength;
var length;
var trCount = 0;

var formelem;

formelem = document.getElementById("fillform");

		
var rcolors = new Array('#F9F9F9','EDF5FF');

//var rcolors = new Array('#C0C7CA','#AFDFF7');

	var tableElements = formelem.getElementsByTagName('table'), 
	tableLength = tableElements.length;
	
	while(tableLength--) {
		var el = tableElements[tableLength];
		//el.setAttribute("style","border:1px solid black");
		el.style.borderWidth = "1px";
		el.style.borderStyle = "solid";
		el.style.borderColor = "black";
	}
   		
	var elements = formelem.getElementsByTagName('input'), 
	length = elements.length;

	while(length--) {
		var el = elements[length];
		el.setAttribute("style","width: 120px; height:20px;  border-bottom:1px solid black; border-right:1px solid black;border-top:1px solid black; border-left:1px solid black;");
	}
	
   var elementsTR = formelem.getElementsByTagName('tr'), 
	

	trLength = elementsTR.length;
	var lastColor;
	var lastGreatgrandParentName;
	
	while(trLength--) {
		var el = elementsTR[trLength];
		if (el.style.display != "none"){
			if (!el.hasChildNodes())
			{
				delobjectParent = el.parentNode;
				delobjectParent.removeChild(el);
			}
			else
			{
				el.style.border = "1px";
				containsTables = el.getElementsByTagName('table');
			
				var greatgrandParentName = el.parentNode.parentNode.parentNode.nodeName; 

				if(containsTables.length == 0 ) {
					if (greatgrandParentName == "SPAN" && greatgrandParentName == lastGreatgrandParentName)					
					{
						el.style.backgroundColor = lastColor;
						el.setAttribute("style","background-color: "+lastColor);
					}
					else
					{
						var bgcol= rcolors[trCount % rcolors.length]; 
						el.style.backgroundColor = bgcol;
						lastColor = bgcol;
						trCount++;	
					}		
				}else 
				{
					el.style.backgroundColor = lastColor;
					el.setAttribute("style","background-color: "+lastColor);

					var childTableLen = containsTables.length;
					while(childTableLen--){
						var el = containsTables[childTableLen];
			    		el.style.border = "none";
					}
				}

				lastGreatgrandParentName = greatgrandParentName;

				containsTD = el.getElementsByTagName('TD');

				for (var kt=0; kt<containsTD.length; kt++) {
				
					if ( containsTD[kt].width == "" )
					{
						containsTD[kt].width = "20%";					
					}
					containsTD[kt].border = "1";
				}
			}
		}	
	}

	elementsTR[0].setAttribute("style","background-color:#F9F9F9;");

	var elements = formelem.getElementsByTagName('label'), 
	length = elements.length;

	while(length--) {
		var el = elements[length];
		//el.setAttribute("style","font-family:Verdana,Arial,Helvetica,sans-serif; color:black;font-weight:bold; font-size:11pt;");
	}

	var elements = formelem.getElementsByTagName('P'), 
	length = elements.length;

	while(length--) {
		var el = elements[length];
		
		if(length > 0)
		{
			el.setAttribute("style","height:24px; font-size:14pt;background-color:#27ACE4;color:white;font-weight:bold;");
		}
		else
		{
			el.setAttribute("style","height:40px; font-size:24pt;color:black;font-weight:bold;text-align:center;");
		}

	}
	
}

function includeJS(file){

 var script = document.createElement('script');
 script.src=file;
 script.type = 'text/css';
 
 document.getElementsByTagName('head').item(0).appendChild(script);
}

function minAccrual_loadStudySites(){
	var orgID = document.getElementById(getColumnSysId('orgID'));
	if (!orgID) return;
	var selOrg = orgID.value;
	
	if (document.getElementsByName("studyId")[0]){
		var studyId = (document.getElementsByName("studyId")[0]).value;
		jQuery.ajax({
			url:"getStudySitesDropDown.jsp?studyId=" + studyId + "&selOrgId="+selOrg,
			async: true,
			cache: false,
			success: function(resp) {
				arrSiteIds = resp.arrSiteIds;
				arrSiteNames = resp.arrSiteNames;
				arrSiteSampleSizes = resp.arrSiteSampleSizes;
				document.getElementById(orgID.id+"_span").innerHTML += resp.ddSites;
				
				var ddSites = document.getElementById('accsites');
				ddSites.onchange = function(e){
					var selIndex = this.selectedIndex;
					var orgID = document.getElementById(getColumnSysId('orgID'));
					orgID.value = arrSiteIds[selIndex-1];

					var orgLocalSampleSize = document.getElementById(getColumnSysId('orgLocalSampleSize'));
					var sampleSize = (this.value == '')? '': arrSiteSampleSizes[selIndex-1];
					sampleSize = (!sampleSize)? '' : sampleSize;
					orgLocalSampleSize.value = sampleSize;
					
					//(orgLocalSampleSize.onchange)();
					cal_TotalRaceM();
					cal_TotalRaceF();
					
					cal_TotalEthnicityM();
					cal_TotalEthnicityF();
				}
				ddSites.onchange();
			}
		});
	}
}
//////******************************

function minAccrual_Init() {
	$j('input').removeClass('numberfield');
	minAccrual_formatHTML();
	minAccrual_setCalculations();
	minAccrual_loadStudySites();
	var orgID = document.getElementById(getColumnSysId('orgID'));
	if (!orgID) return;
	orgID.style.display="none";
}
var minAccrual_formOnSubmit;

function minAccrual_onload() {
    setTimeout(function(){ minAccrual_Init();}, 1000);
    minAccrual_formOnSubmit = document.getElementById("fillform").onsubmit;
    document.getElementById("fillform").onsubmit="";
}
minAccrual_onload();

$j("#fillform").submit(function(e) {
	e.preventDefault();
	if (minAccrual_validation_check()){
		document.getElementById("fillform").onsubmit= minAccrual_formOnSubmit;
		var flag = document.getElementById("fillform").onsubmit();
		if(flag){
			document.getElementById("fillform").submit();
		} else {
			document.getElementById("fillform").onsubmit= "";
		}
	}
});