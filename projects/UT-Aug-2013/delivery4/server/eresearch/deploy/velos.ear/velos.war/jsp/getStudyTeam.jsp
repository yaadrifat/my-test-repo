<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.json.*"%>
<%@ page language = "java" import = "com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.esch.business.common.*,java.util.*,com.velos.eres.business.study.impl.*"%>
<jsp:useBean id ="sessionmaint2" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%
	HttpSession tSession2 = request.getSession(true);
	request.setCharacterEncoding("UTF-8");
	response.setContentType("application/json");
	JSONObject jsObj = new JSONObject();
	if (!sessionmaint2.isValidSession(tSession2)) {
		// Session is invalid; print an error and exit
		response.sendError(HttpServletResponse.SC_FORBIDDEN, "SESSION_TIME_OUT");
		jsObj.put("result", -1);
		jsObj.put("resultMsg", MC.M_UsrNot_LoggedIn);/*jsObj.put("resultMsg", "User is not logged in.");*****/
		out.println(jsObj.toString());
		return;
	}

   	if (sessionmaint2.isValidSession(tSession2)){
		int studyId = StringUtil.stringToNum((String) request.getParameter("studyId"));
	    int userId = StringUtil.stringToNum((String) tSession2.getValue("userId"));
	    int accId = StringUtil.stringToNum((String) tSession2.getValue("accountId"));
		String role_subType = (String) request.getParameter("role_subType");
		
		CodeDao cDao = new CodeDao();
		int codeId = cDao.getCodeId("role",role_subType);
		String codeDesc = (codeId > 0)? cDao.getCodeDescription() : "";
		
		jsObj.put("teamRoleCodeId", codeId);
        jsObj.put("teamRoleCodeDesc",codeDesc);

		TeamDao stDao = new TeamDao();
		stDao.getUserByStudyTeamRole(studyId, role_subType);
		ArrayList arrFirstNames = stDao.getTeamUserFirstNames();
		ArrayList arrLastNames = stDao.getTeamUserLastNames();

   		jsObj.put("arrFirstNames", arrFirstNames);
        jsObj.put("arrLastNames",arrLastNames);

		out.println(jsObj.toString());
   	} %>
