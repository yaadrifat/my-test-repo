function getCDSTDetails_loadCDSTDetails(){
	var CDS_Team = document.getElementById(getColumnSysId('CDS_Team'));
	if (document.getElementsByName("studyId")[0]){
		var studyId = (document.getElementsByName("studyId")[0]).value;	
		jQuery.ajax({
			type: "POST",
			url:"getStudySummaryFields.jsp",
			data: {"studyId": studyId},
			success: function(resp) {
				var studytArea = resp.studytArea;

				if (studytArea && refCDSTLookupFormId){
					var CDS_Team = document.getElementById(getColumnSysId('CDS_Team'));
					CDS_Team.value = studytArea;
					jQuery.ajax({
						type: "POST",
						url:"getFormRespByColData.jsp",
						data: {"formId": refCDSTLookupFormId,
							"getColumn1": "ctrc_cdst_leader_id",
							"getColumn2": "ctrc_cdst_leader",
							"matchColumn1": "ctrc_cdst_id",
							"matchWith1": studytArea
						},
						success: function(resp) {
							if (resp.recArray && resp.recArray.length > 0){
								var record = (resp.recArray[0]).rec;

								var CDST_Leader = document.getElementById(getColumnSysId('CDST_Leader'));
								CDST_Leader.value = record["ctrc_cdst_leader_id"];
								
								var CDST_Leader_span = document.getElementById(CDST_Leader.id+"_span");
								CDST_Leader_span.innerHTML += record["ctrc_cdst_leader"];
							}
						}
					});
					
					var studytAreaDesc = resp.studytAreaDesc;
					var studyTitle = resp.studyTitle;
					var CDS_Team_span = document.getElementById(getColumnSysId('CDS_Team')+"_span");
					if (CDS_Team_span)CDS_Team_span.innerHTML += studytAreaDesc;
				}
			}
		});
	}
}

function getCDSTDetails_Init() {
	var CDS_Team = document.getElementById(getColumnSysId('CDS_Team'));
 	CDS_Team.style.display="none";
	
	var CDST_Leader = document.getElementById(getColumnSysId('CDST_Leader'));
 	CDST_Leader.style.display="none";
	getCDSTDetails_loadCDSTDetails();
}

setTimeout(function(){ getCDSTDetails_Init();}, 1000);