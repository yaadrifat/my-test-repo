This is a readMe for delivery #19 of custom project for University of Texas.
-------------------------------------------------------------------------------

This is a bug fix patch. Steps to deploy the patch are as follows:
1. Copy the below mentioned files at correct location.
	a. 2 JS files�
		getProtTrackStudyStatusData.js
		getRegAffairsData.js

These may be existing files. If so, should be replaced in the following location -
eResearch_jboss510\server\eresearch\deploy\velos.ear\velos.war\jsp\js\velos

2. Delete below mentioned files from correct location.
	a. 2 JS files�
		getProtTrackStudyStatusData.js
		getRegAffairsData.js

The files were accidently delivered in a wrong location.
They should be deleted from the following location -
eResearch_jboss510\server\eresearch\deploy\velos.ear\velos.war\jsp\js
 
3. No server restart is required.
