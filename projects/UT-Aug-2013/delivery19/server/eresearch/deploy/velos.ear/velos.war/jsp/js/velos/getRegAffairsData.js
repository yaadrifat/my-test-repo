function getRegAffairsData_loadRegAffairsData(){
	if (document.getElementsByName("studyId")[0]){
		var studyId = (document.getElementsByName("studyId")[0]).value;
		if (studyId && refRegAffFormId){
			jQuery.ajax({
				type: "POST",
				url:"getFormRespByColData.jsp",
				data: {"formId": refRegAffFormId,
					"getColumn1": "RA_PRC_Approval_Date",
					"getColumn2": "RA_O_and_L_Approval_Date",
					"matchColumn1": "ID",
					"matchWith1": studyId
				},
				success: function(resp) {
					if (resp.recArray && resp.recArray.length > 0){
						var record = (resp.recArray[0]).rec;

						var ctrc_kac_PRCdate = document.getElementById(getColumnSysId('ctrc_kac_PRCdate'));
						ctrc_kac_PRCdate.value = record["RA_PRC_Approval_Date"];
						
						var ctrc_kac_oandldate = document.getElementById(getColumnSysId('ctrc_kac_oandldate'));
					 	ctrc_kac_oandldate.value = record["RA_O_and_L_Approval_Date"];
					}
				}
			});
		}
	}
}
//////******************************

function getRegAffairsData_Init() {
	getRegAffairsData_loadRegAffairsData();
}
window.onload = function() {
    setTimeout(function(){ getRegAffairsData_Init();}, 1000);
}