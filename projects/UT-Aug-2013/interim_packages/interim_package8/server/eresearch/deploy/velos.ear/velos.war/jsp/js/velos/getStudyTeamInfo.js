var getStudyTeamRoleJS = {
	getStudyTeamByRole:{}
};

getStudyTeamRoleJS.getStudyTeamByRole = function (roleSubType, roleFieldID){
	if (!roleSubType || roleSubType.length <= 0)
		return;
	
	if (document.getElementsByName("studyId")[0]){
		var studyId = document.getElementsByName("studyId")[0].value;
		jQuery.ajax({
			type: "POST",
			url:"getStudyTeam.jsp",
			data : { 
				"studyId" :studyId,
				"role_subType": roleSubType
			},
	        success: function(resp) {
				var codeId = resp.teamRoleCodeId;
				if (codeId > 0 ){
		        	var arrFirstNames = resp.arrFirstNames;
					var arrLastNames = resp.arrLastNames;
					var codeDesc = resp.teamRoleCodeDesc;										
					var roleField = document.getElementById(formFieldMappingsJS.getColumnSysId(roleFieldID));
					roleField.parentNode.parentNode.innerHTML = arrFirstNames[0] + ' ' + arrLastNames[0];
				}
			}
		});
	}
}

getStudyTeamRoleJS.getPIDetails = function (){
	var primInv;
	var primInvPhone;
	var primInvMobile;
	if (document.getElementsByName("studyId")[0]){
		var studyId = document.getElementsByName("studyId")[0].value;
		jQuery.ajax({
			type: "POST",
			url:"getStudySummaryFields.jsp",
			data : { 
				"studyId" :studyId,
			},
	        success: function(resp) {
	        	primInv = resp.primInv;
	    		var ctrc_hpt_piname = document.getElementById(formFieldMappingsJS.getColumnSysId('ctrc_hpt_piname'));
	    		ctrc_hpt_piname.parentNode.parentNode.innerHTML = primInv;
	    		
	    		primInvPhone = resp.addOfficeNum;
	    		var ctrc_hpt_officenumber = document.getElementById(formFieldMappingsJS.getColumnSysId('ctrc_hpt_officenumber'));
	    		ctrc_hpt_officenumber.parentNode.parentNode.innerHTML = primInvPhone;
	    		
	    		primInvMobile = resp.addMobile;
	    		var ctrc_hpt_cellnumber = document.getElementById(formFieldMappingsJS.getColumnSysId('ctrc_hpt_cellnumber'));
	    		ctrc_hpt_cellnumber.parentNode.parentNode.innerHTML = primInvMobile;
			}
		});
	}
}
        
function getStudyTeamByRole_Init() {
	getStudyTeamRoleJS.getStudyTeamByRole("role_regcon", 'ctrc_hptregaffairsstaff'); //Regulatory Contact
	getStudyTeamRoleJS.getStudyTeamByRole("ResNurse", 'ctrc_hpt_rnsc'); //Study Nurse
	getStudyTeamRoleJS.getStudyTeamByRole("role_coord", 'ctrc_study_co'); //Study Coordinator
	getStudyTeamRoleJS.getStudyTeamByRole("role_cra", 'ctrc_hpt_cra');  //CRA
	getStudyTeamRoleJS.getPIDetails();
}

$j(document).ready(function(){ 
	formFieldMappingsJS.loadFormFieldMappings();
	getStudyTeamByRole_Init();
});
