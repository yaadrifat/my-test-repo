<%@page import="com.velos.eres.business.study.impl.StudyBean"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.json.*"%>
<%@ page language = "java" import = "com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.esch.business.common.*,java.util.*"%>
<jsp:useBean id ="sessionmaint2" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="studyIdB" scope="session" class="com.velos.eres.web.studyId.StudyIdJB"/>
<jsp:useBean id="studyRightsB" scope="request" class="com.velos.eres.web.studyRights.StudyRightsJB"/>
<%
	HttpSession tSession2 = request.getSession(true);
	request.setCharacterEncoding("UTF-8");
	response.setContentType("application/json");
	JSONArray jsObjArray = new JSONArray();
	JSONObject jsObj = new JSONObject();
	if (!sessionmaint2.isValidSession(tSession2)) {
		// Session is invalid; print an error and exit
		response.sendError(HttpServletResponse.SC_FORBIDDEN, "SESSION_TIME_OUT");
		jsObj.put("result", -1);
		jsObj.put("resultMsg", MC.M_UsrNot_LoggedIn);/*jsObj.put("resultMsg", "User is not logged in.");*****/
		out.println(jsObj.toString());
		return;
	}

   	if (sessionmaint2.isValidSession(tSession2)){
		int studyId = StringUtil.stringToNum((String) request.getParameter("studyId"));
		int userId = StringUtil.stringToNum((String) tSession2.getAttribute("userId"));
		int accId = StringUtil.stringToNum((String) tSession2.getAttribute("accountId"));
		
		if (!StringUtil.isAccessibleFor(studyRightsB.getStudyRightsForModule(studyId, userId, "STUDYSUM"), 'V')){
			response.sendError(HttpServletResponse.SC_FORBIDDEN, "ERROR");
			jsObj.put("result", -2);
			jsObj.put("resultMsg", MC.M_InsuffAccRgt_CnctSysAdmin);
			out.println(jsObj.toString());
			return;
		}
		
		String moreDetailType = request.getParameter("moreDetailType");
		String moreDetailTypes = request.getParameter("moreDetailTypes");
		if (StringUtil.isEmpty(moreDetailType) && StringUtil.isEmpty(moreDetailTypes)){
			return;
		}
		String [] moreDetailsSubTypes = (StringUtil.isEmpty(moreDetailTypes))? 
				null : moreDetailTypes.split(",");
		if (null == moreDetailsSubTypes && !StringUtil.isEmpty(moreDetailType)){
			moreDetailsSubTypes = new String [1];
			moreDetailsSubTypes[0] = moreDetailType;
		} else {
			if (null == moreDetailsSubTypes && StringUtil.isEmpty(moreDetailType)){
				return;
			}
		}
		
		studyIdB.setId(studyId);
		studyIdB.getStudyIdDetails();
		
		CodeDao cdDao = new CodeDao();
		/*//Following code is compatible with v9.3 compatible
		cdDao.getCodeValuesBySubTypes("studyidtype", moreDetailsSubTypes);
		ArrayList codePKs = cdDao.getCId();
		ArrayList codeSubTypes = cdDao.getCSubType();*/
		
		//Following code is compatible with v9.2 --BEGIN
		ArrayList codePKs = new ArrayList();
		ArrayList codeSubTypes = new ArrayList();
		
		for(int indx=0; indx < moreDetailsSubTypes.length; indx++){
			cdDao = new CodeDao();
			int codeId = cdDao.getCodeId("studyidtype", moreDetailsSubTypes[indx]);
			codePKs.add(codeId);
			codeSubTypes.add(cdDao.getCodeSubtype(codeId));
		}
		//--END
		
		
		int cdMSDetailLength = (null == codePKs)? 0 : (codePKs).size();
		if (cdMSDetailLength <= 0){
			return;
		}

		for (int indx = 0; indx < cdMSDetailLength; indx++){
			String cdMSDetaislSubType = (codeSubTypes.get(indx)).toString();
			int studyIdPK = studyIdB.findMoreStudyId(studyId, cdMSDetaislSubType);
			if (studyIdPK > 0){
				studyIdB.setId(studyIdPK);
				studyIdB.getStudyIdDetails();
				
				String studyMoreDetail = studyIdB.getAlternateStudyId();
				jsObj.put("studyMoreDetailPK", codePKs.get(indx));
				jsObj.put("studyMoreDetailSubType", cdMSDetaislSubType);
				jsObj.put("studyMoreDetail", studyMoreDetail);
				
				if (!StringUtil.isEmpty(moreDetailType)){
					out.println(jsObj.toString());
					return;
				}
			}
			jsObjArray.put(indx,jsObj);
			jsObj = new JSONObject();
		}

		out.println(jsObjArray.toString());

	} %>