<%@page import="com.velos.eres.business.study.impl.StudyBean"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.json.*"%>
<%@ page language = "java" import = "com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.esch.business.common.*,java.util.*"%>
<jsp:useBean id ="sessionmaint2" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="studyB" scope="session" class="com.velos.eres.web.study.StudyJB"/>
<jsp:useBean id="studyRightsB" scope="request" class="com.velos.eres.web.studyRights.StudyRightsJB"/>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.velos.eres.web.address.AddressJB"%>
<%
	HttpSession tSession2 = request.getSession(true);
	request.setCharacterEncoding("UTF-8");
	response.setContentType("application/json");
	JSONObject jsObj = new JSONObject();
	if (!sessionmaint2.isValidSession(tSession2)) {
		// Session is invalid; print an error and exit
		response.sendError(HttpServletResponse.SC_FORBIDDEN, "SESSION_TIME_OUT");
		jsObj.put("result", -1);
		jsObj.put("resultMsg", MC.M_UsrNot_LoggedIn);/*jsObj.put("resultMsg", "User is not logged in.");*****/
		out.println(jsObj.toString());
		return;
	}

   	if (sessionmaint2.isValidSession(tSession2)){
		int studyId = StringUtil.stringToNum((String) request.getParameter("studyId"));
		int userId = StringUtil.stringToNum((String) tSession2.getValue("userId"));
		int accId = StringUtil.stringToNum((String) tSession2.getValue("accountId"));
		
		if (!StringUtil.isAccessibleFor(studyRightsB.getStudyRightsForModule(studyId, userId, "STUDYSUM"), 'V')){
			response.sendError(HttpServletResponse.SC_FORBIDDEN, "ERROR");
			jsObj.put("result", -2);
			jsObj.put("resultMsg", MC.M_InsuffAccRgt_CnctSysAdmin);
			out.println(jsObj.toString());
			return;
		}
		studyB.setId(studyId);
		studyB.getStudyDetails();
		
		String studyNumber = studyB.getStudyNumber();
		String title = studyB.getStudyTitle();
		//String studySponsorName = studyB.getStudySponsorName();
		String sponsorid = studyB.getStudySponsorIdInfo();
		String sponsor = studyB.getStudySponsor();
		String phase = studyB.getStudyPhase();
		String prod = studyB.getStudyProduct();
		String primInvUserId = studyB.getStudyPrimInv();
		UserJB userB = new UserJB();
		userB.setUserId(StringUtil.stringToNum(primInvUserId));
		userB.getUserDetails();
		String primInv = userB.getUserFirstName() + ' ' + userB.getUserLastName();
		AddressJB addressB = new AddressJB();
		addressB.setAddId(StringUtil.stringToNum(primInvUserId));
		addressB.getAddressDetails();
		String addOfficeNum = addressB.getAddPhone();
		String addMobile = addressB.getAddMobile();
		
		int fkCodeSponsor = StringUtil.stringToNum(studyB.getStudySponsorName());
		String studySponsorName = "";
		if (fkCodeSponsor > 0){
			CodeDao cdSponsorDao = new CodeDao();
			studySponsorName = cdSponsorDao.getCodeDescription(fkCodeSponsor);
		}
		
		jsObj.put("studyNumber", studyNumber);
		jsObj.put("studyTitle", title);
		jsObj.put("studySponsorName", studySponsorName);		
		jsObj.put("studySponsorId", sponsorid);
		jsObj.put("studySponsor", sponsor);
		jsObj.put("agentDevice", prod);
		jsObj.put("primInv", primInv);
		jsObj.put("addOfficeNum", addOfficeNum);
		jsObj.put("addMobile", addMobile);
		
		CodeDao cdDao = new CodeDao();
		int researchType = StringUtil.stringToNum(studyB.getStudyResType());
		if (researchType > 0){
			String researchTypeDesc = cdDao.getCodeDescription(researchType);
			cdDao = new CodeDao();
			jsObj.put("researchTypeFK", researchType);
			jsObj.put("researchTypeSubType", cdDao.getCodeSubtype(researchType));
			jsObj.put("researchTypeDesc", researchTypeDesc);
		}

		cdDao = new CodeDao();
		int tArea = StringUtil.stringToNum(studyB.getStudyTArea());
		if (tArea > 0){
			String tAreaDesc = cdDao.getCodeDescription(tArea);
			jsObj.put("studytAreaFK", tArea);
			jsObj.put("studytAreaDesc", tAreaDesc);
		}
		
		int phasePK = StringUtil.stringToNum(phase);
		if (phasePK > 0){
			cdDao = new CodeDao();
			String phaseDesc = cdDao.getCodeDescription(phasePK);
			jsObj.put("studyPhaseDesc", phaseDesc);
		}

		String studyStartDt = studyB.getStudyActBeginDate();
		String studyEndDt = studyB.getStudyEndDate();

		jsObj.put("studyStartDt", studyStartDt);
		jsObj.put("studyEndDt", studyEndDt);
		
		int resType = StringUtil.stringToNum(studyB.getStudyResType());
		if (resType > 0){
			cdDao = new CodeDao();
			if (cdDao.getCodeValuesById(resType)){
				jsObj.put("studyResTypeFK", resType);
				jsObj.put("studyResTypeSubType", (cdDao.getCSubType()).get(0));
				jsObj.put("studyResTypeDesc", (cdDao.getCDesc()).get(0));
			}
		}

		String diseaseSites = studyB.getDisSite();
		if (!StringUtil.isEmpty(diseaseSites)){
			cdDao = new CodeDao();
			cdDao.getCodeValuesByIds(diseaseSites);
			String tempDisStr = (cdDao.getCId()).toString();
			tempDisStr = tempDisStr.substring(tempDisStr.indexOf("[")+1, tempDisStr.lastIndexOf("]"));
			jsObj.put("studyDisSiteFKs", tempDisStr);

			tempDisStr = (cdDao.getCSubType()).toString();
			tempDisStr = tempDisStr.substring(tempDisStr.indexOf("[")+1, tempDisStr.lastIndexOf("]"));
			jsObj.put("studyDisSiteSubTypes", tempDisStr);

			tempDisStr = (cdDao.getCDesc()).toString();
			tempDisStr = tempDisStr.substring(tempDisStr.indexOf("[")+1, tempDisStr.lastIndexOf("]"));
			jsObj.put("studyDisSiteDescs", tempDisStr);
		}
		
		StudyINDIDEDAO studyIndIdeDAO = new StudyINDIDEDAO();
	 	int len = studyIndIdeDAO.getStudyIndIdeInfo(studyId);
	 	ArrayList indIdeType = new  ArrayList();
	 	ArrayList indIdeNum = new  ArrayList();
		if(len>0){
			indIdeType = studyIndIdeDAO.getTypeIndIdeList();
			indIdeNum = studyIndIdeDAO.getNumberIndIdeList();
		}

		jsObj.put("indIdeType", indIdeType);
		jsObj.put("indIdeNum", indIdeNum);

		out.println(jsObj.toString());

	} %>