getStudyInfoDialog_custom ={
	sSummaryRetrieved: false,
	MSDRetrieved: false,
	divDataReady: false,
	dataReceived: {},
	
	setDataReceived: {},
	getDataReceived: {},
	setDataForDialog: {},
	createDialog: {}
};

getStudyInfoDialog_custom.setDataReceived = function(key, value){
	getStudyInfoDialog_custom.dataReceived[key] = value;
};

getStudyInfoDialog_custom.getDataReceived = function(key){
	var value = getStudyInfoDialog_custom.dataReceived[key];
	value = (!value)? '-' : value;
	return value;
};


getStudyInfoDialog_custom.setDataForDialog = function (){
	if (!(getStudyInfoDialog_custom.sSummaryRetrieved && getStudyInfoDialog_custom.MSDRetrieved)){
		return; 
	}
		
	var displayDialogDiv = document.getElementById ('displayDialogDiv');
	
	var LabelArr = ['IRB Number', 'Short Title', 'Title', 'Sponsor Number', 'Sponsor Name', 'Agent/Device', 'Phase', 'Therapeutic Area/CDST', 'Internal Tumor Type', 'Accrual Start Date', 'Accrual End Date'];
	var keywordArr = ['irb_number', 'ST', 'title', 'id_sponsor', 'sponsor', 'agentDevice', 'phase', 'theraArea', 'intTumorType', 'accrStartDate', 'accrEndDate'];
	
	var displayDialogDivInHTML = '';
	displayDialogDivInHTML = '<b>Study Details:</b>';
	displayDialogDivInHTML += '<table border="solid 1">';	
	for (var indx=0; indx < keywordArr.length; indx++){
		displayDialogDivInHTML += '<TR align="left">';
		displayDialogDivInHTML += '<TH>';
		displayDialogDivInHTML += LabelArr[indx];
		displayDialogDivInHTML += '</TH>';
		displayDialogDivInHTML += '<TD>';
		displayDialogDivInHTML += getStudyInfoDialog_custom.getDataReceived(keywordArr[indx]);
		displayDialogDivInHTML += '</TD>';
		
		indx++;
		if (LabelArr[indx]){
			displayDialogDivInHTML += '<TH>';
			displayDialogDivInHTML += LabelArr[indx];
			displayDialogDivInHTML += '</TH>';
			displayDialogDivInHTML += '<TD>';
			displayDialogDivInHTML += getStudyInfoDialog_custom.getDataReceived(keywordArr[indx]);
			displayDialogDivInHTML += '</TD>';
			displayDialogDivInHTML += '</TR>';
		}
	}
	displayDialogDivInHTML += '</table>';
	
	displayDialogDivInHTML += '<br/><b>IND/IDE Details:</b>';
	displayDialogDivInHTML += '<table border="solid 1">';
	displayDialogDivInHTML += '<TR align="left">';
	displayDialogDivInHTML += '<TH>';
	displayDialogDivInHTML += 'IND/IDE Type';
	displayDialogDivInHTML += '</TH>';
	displayDialogDivInHTML += '<TH>';
	displayDialogDivInHTML += 'IND/IDE Number';
	displayDialogDivInHTML += '</TH>';
	displayDialogDivInHTML += '</TR>';

	var indIdeTypeArr = getStudyInfoDialog_custom.getDataReceived('indIdeType');
	var indIdeNumArr = getStudyInfoDialog_custom.getDataReceived('indIdeNum');
	
	for (var count=0; count < indIdeTypeArr.length; count++){
		displayDialogDivInHTML += '<TR>';
		displayDialogDivInHTML += '<TD>';
		displayDialogDivInHTML += (indIdeTypeArr[count])==1?'IND':'IDE';
		displayDialogDivInHTML += '</TD>';
		displayDialogDivInHTML += '<TD>';
		displayDialogDivInHTML += indIdeNumArr[count];
		displayDialogDivInHTML += '</TD>';
		displayDialogDivInHTML += '</TR>';
	}

	displayDialogDivInHTML += '</table>';
	
	displayDialogDiv.innerHTML = displayDialogDivInHTML;
	getStudyInfoDialog_custom.divDataReady = true;
};
getStudyInfoDialog_custom.createDialog = function (){
	if (!getStudyInfoDialog_custom.divDataReady){
		getStudyInfoDialog_custom.setDataForDialog();
	}
	
	$j('#displayDialogDiv').dialog( {
		height: 400,
		width: 500,
		title: 'Summary: ',
		resizable:false, 
		closeOnEscape: false,
		} 
	);
};

jQuery(document).ready( function(){
	var studyDataDiv = document.createElement('div');
	studyDataDiv.id ='studyDataDiv';
	studyDataDiv.name = 'studyDataDiv';
	var studytAreaFK;
	
	var irb_number; //from "More Study Details" 
	var ST; //from "More Study Details"
	var title; //from "Study Summary Page" 
	var id_sponsor; //Sponsor Number from "More Study Details" 
	//var indIde; //from "Study Summary Page"; probably COMMA SEPARATED 
	var indIdeType =[]; //IND/IDE type from "Study Summary Page"
	var indIdeNum =[]; //IND/IDE number from "Study Summary Page"
	var sponsor; //from "Study Summary Page" 
	var agentDevice; //from "Study Summary Page"
	var phase; //from "Study Summary Page" 
	var theraArea; //from "Study Summary Page" 
	var intTumorType; //from "Clinical Disease Site Team Form"
	var accrStartDate; //from "Study Status Tab"
	var accrEndDate; //from "Study Status Tab"
	
	var researchTypeSubType;
	
	var studyId = document.getElementsByName('studyId')[0].value;
	
	//Ajax for "Study Summary Page"
	jQuery.ajax({
		type: "POST",
		url:"getStudySummaryFields.jsp",
		async:false,
		data: {"studyId": studyId},
		success: function(respStudy) {
			getStudyInfoDialog_custom.setDataReceived('studyId', studyId);
			getStudyInfoDialog_custom.setDataReceived('studytAreaFK', respStudy.studytAreaFK);
			getStudyInfoDialog_custom.setDataReceived('sponsor', respStudy.studySponsor);
			getStudyInfoDialog_custom.setDataReceived('sponsorName', respStudy.studySponsorName);
			getStudyInfoDialog_custom.setDataReceived('sponsorid', respStudy.studySponsorId);
			getStudyInfoDialog_custom.setDataReceived('phase', respStudy.studyPhaseDesc);
			getStudyInfoDialog_custom.setDataReceived('accrual', respStudy.studyAccrual);
			getStudyInfoDialog_custom.setDataReceived('title', respStudy.studyTitle);
			getStudyInfoDialog_custom.setDataReceived('theraArea', respStudy.studytAreaDesc);
			getStudyInfoDialog_custom.setDataReceived('indIdeType', respStudy.indIdeType);
			getStudyInfoDialog_custom.setDataReceived('indIdeNum', respStudy.indIdeNum);			
			getStudyInfoDialog_custom.setDataReceived('agentDevice', respStudy.agentDevice);
			getStudyInfoDialog_custom.setDataReceived('researchTypeSubType', respStudy.researchTypeSubType);

			getStudyInfoDialog_custom.sSummaryRetrieved = true;
			
		//Ajax for "More Study Details"
		var moreDetailTypes='irb_number,ST,id_sponsor';
		jQuery.ajax({
			type: "POST",
			url:"getStudyMoreDetails.jsp",
			async:false,
			data: {"studyId": studyId,
				"moreDetailTypes": moreDetailTypes},
			success: function(resp) {
				var JSONArray = resp;
				for (var indx = 0; indx < JSONArray.length; indx++){
					var JSONObj = JSONArray[indx];
					switch(JSONObj['studyMoreDetailSubType']){
					case 'irb_number':
						getStudyInfoDialog_custom.setDataReceived('irb_number', JSONObj['studyMoreDetail']);
						break;
					case 'ST':
						getStudyInfoDialog_custom.setDataReceived('ST', JSONObj['studyMoreDetail']);
						break;
					case 'id_sponsor':
						getStudyInfoDialog_custom.setDataReceived('id_sponsor', JSONObj['studyMoreDetail']);
						break;
					default:
						break;
					}
				}
				getStudyInfoDialog_custom.MSDRetrieved = true;
			}
		});
	
		//Ajax for "Study Status Tab"
		jQuery.ajax({
			type: "POST",
			url:"getStudyStatusData.jsp",
			async:false,
			data: {"studyId": studyId,
				"studyStatusType": "active"
				},
			success: function(resp) {
				getStudyInfoDialog_custom.setDataReceived('accrStartDate', resp.activeStatusDate);
			}
		});
	
		jQuery.ajax({
			type: "POST",
			url:"getStudyStatusData.jsp",
			async:false,
			data: {"studyId": studyId,
				"studyStatusType": "studystat_80"//Enrollment Closed/UT
			},
			success: function(resp) {
				getStudyInfoDialog_custom.setDataReceived('accrEndDate', resp.closureStatusDate);
			}
		});
		
		//Ajax for "Clinical Disease Site Team Form"

		if (studyId && refCDSTFormId){
			jQuery.ajax({
				type: "POST",
				url:"getFormRespByColData.jsp",
				async:false,
				data: {"formId": refCDSTFormId,
					"getColumn1": "IDEAS_Internal_Tumor_Type_New",
					"matchColumn1": "ID",
					"matchWith1": studyId
				},
				success: function(resp) {
					if (resp.recArray && resp.recArray.length > 0){
						var record = (resp.recArray[0]).rec;
						
						//starting from here
						//var selTumorTypes = IDEAS_Internal_Tumor_Type_New.value;
						jQuery.ajax({
							type:"POST",
							url:"getCodelstData.jsp",
							async:false,
							data : {
								listName: "listTumorTypes",
								codeType: "tumorType",
								selectedVals: record["IDEAS_Internal_Tumor_Type_New"]
							},
							success: function(resp1) {
								//alert("In here!"+resp1);
								var codeIds = resp1.codeIds;
								var codeDescs = resp1.codeDescs;
								//alert("codeIds" + codeIds);
								//alert("codeDescs" + codeDescs);
								//Parsing logic
								//var codeDescParsed = StringUtil.stringToNum((String)codeDescs);
								//alert("codeDescParsed: " + codeDescParsed);
								//getStudyInfoDialog_custom.setDataReceived('intTumorType', parsedValuesFormattedAsHTML);
								getStudyInfoDialog_custom.setDataReceived('intTumorType', codeDescs);
							}
						});
						//till here
						
						}
					}
				});
			}
		}
	});
	
	var displayDialogDiv = document.createElement('div');
	displayDialogDiv.id = 'displayDialogDiv';
	displayDialogDiv.name = 'displayDialogDiv';
	displayDialogDiv.style.display = 'none';
	displayDialogDiv.innerHTML='Data Goes Here!';
	(document.getElementsByTagName("form")[0]).appendChild(displayDialogDiv);
	
	var btn=document.createElement("BUTTON");
	btn.id='newButton';
	(document.getElementsByTagName('form')[0]).appendChild(btn);
	$j('#newButton').html( "Summary");
	$j('#newButton').button();
	$j('#newButton').button("option", "type", "button");

	if (!btn.addEventListener) {
		btn.attachEvent('onclick', function(e){
			getStudyInfoDialog_custom.createDialog();
			$j('#displayDialogDiv').dialog("open");
			if (e.preventDefault){ e.preventDefault(); }
			$j('#displayDialogDiv').focus();
			return false;
		});
	} else {
		document.querySelector('#newButton').addEventListener('click', function(e){
			getStudyInfoDialog_custom.createDialog();
			$j('#displayDialogDiv').dialog("open");
			if (e.preventDefault){ e.preventDefault(); }
			$j('#displayDialogDiv').focus();
			return false;
		});
	}
});