protTrackMainLayoutPart2JS={
		setDataReceived: {},
		dataReceived: {},
		showHideFields: {}
};

protTrackMainLayoutPart2JS.showHideFields = function(fld, action){
	switch (action){
	case 'show':
		if (fld){
			fld.style.display = "inline";
			fld.parentNode.parentNode.style.display = "inline";
		}
		break;
	case 'hide':
		if (fld){
			fld.style.display = "none";
			fld.parentNode.parentNode.style.display = "none";
		}
		break;
	default:
		break;
	}
};

protTrackMainLayoutPart2JS.setDataReceived = function(key, value){
	protTrackMainLayoutPart2JS.dataReceived[key] = value;
};

$j(document).ready(function(){
	formFieldMappingsJS.loadFormFieldMappings();

	var studyId = document.getElementsByName('studyId')[0].value;
	var researchTypeSubType;
	
	jQuery.ajax({
		type: "POST",
		url:"getStudySummaryFields.jsp",
		async:false,
		data: {"studyId": studyId},
		success: function(respStudy) {
			researchTypeSubType = respStudy.researchTypeSubType	
		}
	});
	
	var ctrc_hpt_1572number = [];
	var ctrc_hpt_fdasubm = [];
	var indx = 1;  //field id counter
	var arrIndx = 0; //array index counter
	
	while((formFieldMappingsJS.getColumnSysId('ctrc_hpt_1572number' + indx)) && (formFieldMappingsJS.getColumnSysId('ctrc_hpt_fdasubm' + indx))) {
		ctrc_hpt_1572number[arrIndx] = document.getElementById(formFieldMappingsJS.getColumnSysId('ctrc_hpt_1572number' + indx));
		ctrc_hpt_fdasubm[arrIndx] = document.getElementById(formFieldMappingsJS.getColumnSysId('ctrc_hpt_fdasubm' + indx));
		++indx;
		++arrIndx;
	}

	if (researchTypeSubType == 'insti'){
		//show fields
		for(var count = 0; count < ctrc_hpt_1572number.length; ++count){
			protTrackMainLayoutPart2JS.showHideFields(ctrc_hpt_1572number[count], 'show');
			protTrackMainLayoutPart2JS.showHideFields(ctrc_hpt_fdasubm[count], 'show');
		}
	} else {
		//hide fields
		for(var count = 0; count < ctrc_hpt_1572number.length; ++count){
			protTrackMainLayoutPart2JS.showHideFields(ctrc_hpt_1572number[count], 'hide');
			protTrackMainLayoutPart2JS.showHideFields(ctrc_hpt_fdasubm[count], 'hide');
		}
	}
});

