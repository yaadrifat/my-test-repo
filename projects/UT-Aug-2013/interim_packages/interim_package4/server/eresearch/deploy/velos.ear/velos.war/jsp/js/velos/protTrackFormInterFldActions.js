protTrackFormInterFldActionsJS = {
	showHideFields: {}
};

protTrackFormInterFldActionsJS.showHideFields = function(fld, action){
	switch (action){
	case 'show':
		if (fld){
			fld.style.display = "inline";
			fld.parentNode.parentNode.parentNode.style.display = "inline";
		}
		break;
	case 'hide':
		if (fld){
			fld.style.display = "none";
			fld.parentNode.parentNode.parentNode.style.display = "none";
		}
		break;
	default:
		break;
	}
};

$j(document).ready(function(){
	formFieldMappingsJS.loadFormFieldMappings();
	
	var researchTypeSubType = getStudyInfoDialog_custom.getDataReceived('researchTypeSubType');
	while(!researchTypeSubType){
		researchTypeSubType = getStudyInfoDialog_custom.getDataReceived('researchTypeSubType');
	}

	var ctrc_hpt_datesub = document.getElementById(formFieldMappingsJS.getColumnSysId('ctrc_hpt_datesub'));
	var ctrc_hpt_initialapp = document.getElementById(formFieldMappingsJS.getColumnSysId('ctrc_hpt_initialapp'));
	var ctrc_hpt_annualreview = document.getElementById(formFieldMappingsJS.getColumnSysId(' ctrc_hpt_annualreview'));

	if ('insti' == researchTypeSubType){
		//show fields
		protTrackFormInterFldActionsJS.showHideFields(ctrc_hpt_datesub, 'show');
		protTrackFormInterFldActionsJS.showHideFields(ctrc_hpt_initialapp, 'show');
		protTrackFormInterFldActionsJS.showHideFields(ctrc_hpt_annualreview, 'show');
	} else {
		//hide fields
		protTrackFormInterFldActionsJS.showHideFields(ctrc_hpt_datesub, 'hide');
		protTrackFormInterFldActionsJS.showHideFields(ctrc_hpt_initialapp, 'hide');
		protTrackFormInterFldActionsJS.showHideFields(ctrc_hpt_annualreview, 'hide');
	}
});
