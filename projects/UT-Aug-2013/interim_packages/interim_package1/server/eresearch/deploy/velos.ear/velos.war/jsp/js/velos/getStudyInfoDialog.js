getStudySummaryDataJS_custom ={
	sSummaryRetrieved: false,
	MSDRetrieved: false,
	divDataReady: false,
	dataReceived: {},
	
	setDataReceived: {},
	getDataReceived: {},
	setDataForDialog: {},
	createDialog: {}
};

getStudySummaryDataJS_custom.setDataReceived = function(key, value){
	getStudySummaryDataJS_custom.dataReceived[key] = value;
};

getStudySummaryDataJS_custom.getDataReceived = function(key){
	var value = getStudySummaryDataJS_custom.dataReceived[key];
	value = (!value)? '-' : value;
	return value;
};


getStudySummaryDataJS_custom.setDataForDialog = function (){
	if (!(getStudySummaryDataJS_custom.sSummaryRetrieved && getStudySummaryDataJS_custom.MSDRetrieved)){
		return; 
	}
		
	var displayDialogDiv = document.getElementById ('displayDialogDiv');
	
	var LabelArr = ['Study ID', 'IRB Number', 'Short Title', 'Title', 'Sponsor Number', 'Sponsor Name', 'Agent/Device', 'Phase', 'Therapeutic Area', 'Internal Tumor Type', 'Accrual Start Date', 'Accrual End Date'];
	var keywordArr = ['studyId', 'irb_number', 'ST', 'title', 'id_sponsor', 'sponsor', 'agentDevice', 'phase', 'theraArea', 'intTumorType', 'accrStartDate', 'accrEndDate'];
	
	var displayDialogDivInHTML = '';
	displayDialogDivInHTML = '<b>Study Details:</b>';
	displayDialogDivInHTML += '<table border="solid 1">';	
	for (var indx=0; indx < keywordArr.length; indx++){
		displayDialogDivInHTML += '<TR align="left">';
		displayDialogDivInHTML += '<TH>';
		displayDialogDivInHTML += LabelArr[indx];
		displayDialogDivInHTML += '</TH>';
		displayDialogDivInHTML += '<TD>';
		displayDialogDivInHTML += getStudySummaryDataJS_custom.getDataReceived(keywordArr[indx]);
		displayDialogDivInHTML += '</TD>';
		
		indx++;
		displayDialogDivInHTML += '<TH>';
		displayDialogDivInHTML += LabelArr[indx];
		displayDialogDivInHTML += '</TH>';
		displayDialogDivInHTML += '<TD>';
		displayDialogDivInHTML += getStudySummaryDataJS_custom.getDataReceived(keywordArr[indx]);
		displayDialogDivInHTML += '</TD>';
		displayDialogDivInHTML += '</TR>';
	}
	displayDialogDivInHTML += '</table>';
	
	displayDialogDivInHTML += '<br/><b>IND/IDE Details:</b>';
	displayDialogDivInHTML += '<table border="solid 1">';
	displayDialogDivInHTML += '<TR align="left">';
	displayDialogDivInHTML += '<TH>';
	displayDialogDivInHTML += 'IND/IDE Type';
	displayDialogDivInHTML += '</TH>';
	displayDialogDivInHTML += '<TH>';
	displayDialogDivInHTML += 'IND/IDE Number';
	displayDialogDivInHTML += '</TH>';
	displayDialogDivInHTML += '</TR>';

	var indIdeTypeArr = getStudySummaryDataJS_custom.getDataReceived('indIdeType');
	var indIdeNumArr = getStudySummaryDataJS_custom.getDataReceived('indIdeNum');
	

	for (var count=0; count < indIdeTypeArr.length; count++){
		displayDialogDivInHTML += '<TR>';
		displayDialogDivInHTML += '<TD>';
		displayDialogDivInHTML += (indIdeTypeArr[count])==1?'IND':'IDE';
		displayDialogDivInHTML += '</TD>';
		displayDialogDivInHTML += '<TD>';
		displayDialogDivInHTML += indIdeNumArr[count]; //ternary operation
		displayDialogDivInHTML += '</TD>';
		displayDialogDivInHTML += '</TR>';
	}

	displayDialogDivInHTML += '</table>';
	
	displayDialogDiv.innerHTML = displayDialogDivInHTML;
	getStudySummaryDataJS_custom.divDataReady = true;
};
getStudySummaryDataJS_custom.createDialog = function (){
	if (!getStudySummaryDataJS_custom.divDataReady){
		getStudySummaryDataJS_custom.setDataForDialog();
	}
	
	$j('#displayDialogDiv').dialog( {
		height: 400,
		width: 500,
		title: 'Summary: ',
		resizable:false, 
		closeOnEscape: false,
		} 
	);
};

jQuery(document).ready( function(){
	//formFieldMappingsJS.loadFormFieldMappings();
	var studyDataDiv = document.createElement('div');
	studyDataDiv.id ='studyDataDiv';
	studyDataDiv.name = 'studyDataDiv';
	var studytAreaFK;
	
	var irb_number; //from "More Study Details" 
	var ST; //from "More Study Details"
	var title; //from "Study Summary Page" 
	var id_sponsor; //Sponsor Number from "More Study Details" 
	//var indIde; //from "Study Summary Page"; probably COMMA SEPARATED 
	var indIdeType =[]; //IND/IDE type from "Study Summary Page"
	var indIdeNum =[]; //IND/IDE number from "Study Summary Page"
	var sponsor; //from "Study Summary Page" 
	var agentDevice; //from "Study Summary Page"
	var phase; //from "Study Summary Page" 
	var theraArea; //from "Study Summary Page" 
	var intTumorType; //from "Clinical Disease Site Team Form"
	var accrStartDate; //from "Study Status Tab"
	var accrEndDate; //from "Study Status Tab"
	
	var studyId = document.getElementsByName('studyId')[0].value;
	
	//Ajax for "Study Summary Page"
	jQuery.ajax({
		type: "POST",
		url:"getStudySummaryFields.jsp",
		data: {"studyId": studyId},
		success: function(resp) {
			getStudySummaryDataJS_custom.setDataReceived('studyId', studyId);
			getStudySummaryDataJS_custom.setDataReceived('studytAreaFK', resp.studytAreaFK);
			getStudySummaryDataJS_custom.setDataReceived('sponsor', resp.studySponsor);
			getStudySummaryDataJS_custom.setDataReceived('sponsorName', resp.studySponsorName);
			getStudySummaryDataJS_custom.setDataReceived('sponsorid', resp.studySponsorId);
			getStudySummaryDataJS_custom.setDataReceived('phase', resp.studyPhaseDesc);
			getStudySummaryDataJS_custom.setDataReceived('accrual', resp.studyAccrual);
			getStudySummaryDataJS_custom.setDataReceived('title', resp.studyTitle);
			getStudySummaryDataJS_custom.setDataReceived('theraArea', resp.studytAreaDesc);
			getStudySummaryDataJS_custom.setDataReceived('indIdeType', resp.indIdeType);
			getStudySummaryDataJS_custom.setDataReceived('indIdeNum', resp.indIdeNum);			
			getStudySummaryDataJS_custom.setDataReceived('agentDevice', resp.agentDevice);
			
			getStudySummaryDataJS_custom.sSummaryRetrieved = true;
		}
	});
	
	//Ajax for "More Study Details"
	var moreDetailTypes='irb_number,ST,id_sponsor';
	jQuery.ajax({
		type: "POST",
		url:"getStudyMoreDetails.jsp",
		data: {"studyId": studyId,
			"moreDetailTypes": moreDetailTypes},
		success: function(resp) {
			var JSONArray = resp;
			for (var indx = 0; indx < JSONArray.length; indx++){
				var JSONObj = JSONArray[indx];
				switch(JSONObj['studyMoreDetailSubType']){
				case 'irb_number':
					getStudySummaryDataJS_custom.setDataReceived('irb_number', JSONObj['studyMoreDetail']);
					break;
				case 'ST':
					getStudySummaryDataJS_custom.setDataReceived('ST', JSONObj['studyMoreDetail']);
					break;
				case 'id_sponsor':
					getStudySummaryDataJS_custom.setDataReceived('id_sponsor', JSONObj['studyMoreDetail']);
					break;
				default:
					break;
				}
			}
			getStudySummaryDataJS_custom.MSDRetrieved = true;
		}
	});

	//Ajax for "Study Status Tab"
	jQuery.ajax({
		type: "POST",
		url:"getStudyStatusData.jsp",
		data: {"studyId": studyId,
			"studyStatusType": "active"
			},
		success: function(resp) {
			getStudySummaryDataJS_custom.setDataReceived('accrStartDate', resp.activeStatusDate);
		}
	});

	jQuery.ajax({
		type: "POST",
		url:"getStudyStatusData.jsp",
		data: {"studyId": studyId,
			"studyStatusType": "prmnt_cls"
		},
		success: function(resp) {
			getStudySummaryDataJS_custom.setDataReceived('accrEndDate', resp.closureStatusDate);
		}
	});
	
	//Ajax for "Clinical Disease Site Team Form"
	/*jQuery.ajax({
		type: "POST",
		url:"studyformdetails.jsp",
		data: {"studyId": studyId},
		success: function(resp) {
			//more code here
		}
	});*/
	
	var displayDialogDiv = document.createElement('div');
	displayDialogDiv.id = 'displayDialogDiv';
	displayDialogDiv.name = 'displayDialogDiv';
	displayDialogDiv.style.display = 'none';
	displayDialogDiv.innerHTML='Data Goes Here!';
	(document.getElementsByTagName("form")[0]).appendChild(displayDialogDiv);
	
	var btn=document.createElement("BUTTON");
	btn.id='newButton';
	(document.getElementsByTagName('form')[0]).appendChild(btn);
	$j('#newButton').html( "Summary");
	$j('#newButton').button();
	$j('#newButton').button("option", "type", "button");

	if (!newButton.addEventListener) {
		newButton.attachEvent('onclick', function(e){
			getStudySummaryDataJS_custom.createDialog();
			$j('#displayDialogDiv').dialog("open");
			if (e.preventDefault){ e.preventDefault(); }
			$j('#displayDialogDiv').focus();
			return false;
		});
	} else {
		document.querySelector('#newButton').addEventListener('click', function(e){
			getStudySummaryDataJS_custom.createDialog();
			$j('#displayDialogDiv').dialog("open");
			if (e.preventDefault){ e.preventDefault(); }
			$j('#displayDialogDiv').focus();
			return false;
		});
	}
});