<%@page import="com.velos.eres.business.study.impl.StudyBean"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.json.*"%>
<%@ page language = "java" import = "com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.esch.business.common.*,java.util.*"%>
<jsp:useBean id ="sessionmaint2" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="studyStatB" scope="request" class="com.velos.eres.web.studyStatus.StudyStatusJB"/>
<jsp:useBean id="studyRightsB" scope="request" class="com.velos.eres.web.studyRights.StudyRightsJB"/>
<%!
static final String CURRENT = "current";
static final String ACTIVE = "active";
static final String CLOSURE = "prmnt_cls";
%>
<%
	HttpSession tSession2 = request.getSession(true);
	request.setCharacterEncoding("UTF-8");
	response.setContentType("application/json");
	JSONObject jsObj = new JSONObject();
	if (!sessionmaint2.isValidSession(tSession2)) {
		// Session is invalid; print an error and exit
		response.sendError(HttpServletResponse.SC_FORBIDDEN, "SESSION_TIME_OUT");
		jsObj.put("result", -1);
		jsObj.put("resultMsg", MC.M_UsrNot_LoggedIn);/*jsObj.put("resultMsg", "User is not logged in.");*****/
		out.println(jsObj.toString());
		return;
	}

   	if (sessionmaint2.isValidSession(tSession2)){
		int studyId = StringUtil.stringToNum((String) request.getParameter("studyId"));
		int userId = StringUtil.stringToNum((String) tSession2.getAttribute("userId"));
		int accId = StringUtil.stringToNum((String) tSession2.getAttribute("accountId"));
		
		if (!StringUtil.isAccessibleFor(studyRightsB.getStudyRightsForModule(studyId, userId, "STUDYPTRACK"), 'V')){
			response.sendError(HttpServletResponse.SC_BAD_REQUEST, MC.M_InsuffAccRgt_CnctSysAdmin);
			jsObj.put("result", -2);
			jsObj.put("resultMsg", MC.M_InsuffAccRgt_CnctSysAdmin);
			out.println(jsObj.toString());
			return;
		}

		String studyStatusType = (String) request.getParameter("studyStatusType");
		studyStatusType = StringUtil.isEmpty(studyStatusType)? CURRENT : studyStatusType;

		int allOrg=0;
		if (CURRENT.equals(studyStatusType)){
			StudyStatusDao currStudyStatDao = studyStatB.getStudyStatusDesc(studyId, allOrg, userId, accId);
			ArrayList currentStats =  currStudyStatDao.getCurrentStats();
			ArrayList currStatusIdLst = currStudyStatDao.getIds();
			ArrayList currStatusLst = currStudyStatDao.getDescStudyStats();
	
			String currentStatId="";
			String currentVal="";
			String currentStatus ="";
			
			int indx = currentStats.indexOf("1");
			
			for (int i=0;i<currentStats.size();i++) {
				currentVal = (currentStats.get(i) == null)?"":(currentStats.get(i)).toString();
				if ( currentVal.equals("1")) {
					currentStatId = (String)currStatusIdLst.get(i).toString();
					currentStatus=((currStatusLst.get(i)) == null)?"-":(currStatusLst.get(i)).toString();
				}
			}
	
			jsObj.put("currentStudyStatId", currentStatId);
			jsObj.put("currentStudyStatus", currentStatus);
		} else if (ACTIVE.equals(studyStatusType)){
			int activeStatusPK = studyStatB.getFirstActiveEnrollPK(studyId);
			if (activeStatusPK <= 0){
				return;
			}
			
			studyStatB.setId(activeStatusPK);
			studyStatB.getStudyStatusDetails();
			
			System.out.println("Found this status "+ studyStatB.getId());
			String studyStatusDate =  studyStatB.getStatStartDate();
			
			int studyStatusCodeId = StringUtil.stringToNum(studyStatB.getStudyStatus());
			if (studyStatusCodeId <=0){
				return;
			}
			
			CodeDao cdStudyStatusDao = new CodeDao();
			String studyStatusDesc = cdStudyStatusDao.getCodeDescription( studyStatusCodeId);
			
			if (StringUtil.isEmpty(studyStatusDesc)){
				return;
			}
			
			jsObj.put("activeStudyStatId", activeStatusPK);
			jsObj.put("activeStudyStatus", studyStatusDesc);
			jsObj.put("activeStatusDate", studyStatusDate);
			
		} else if (CLOSURE.equals(studyStatusType)){
			/*CodeDao cdStudyStatusDao = new CodeDao();
			int cdClosureStatusId = cdStudyStatusDao.getCodeId("studystat", "prmnt_cls");

			StudyStatusDao closureStatDao = studyStatB.getStudyStatusDesc(studyId, allOrg, userId, accId);
			ArrayList cdClosureStatIds=  closureStatDao.getStudyStatus();
			ArrayList closureStatusIdLst = closureStatDao.getIds();
			ArrayList closureStatusDateLst = closureStatDao.getStudyStartDates();
			
			int closureStatusPK = 0;
			String closureStatId="";
			String closureStatus ="";
			String closureStatusDate ="";
			
			if (null == cdClosureStatIds || cdClosureStatIds.size() <= 0){
				return;
			}
			for (int i=0;i<closureStatusIdLst.size();i++) {
				String cdStudyStatusId = (cdClosureStatIds.get(i) == null)?"":(cdClosureStatIds.get(i)).toString();
				if ( cdStudyStatusId.equals(""+cdClosureStatusId)) {
					closureStatId = (String)closureStatusIdLst.get(i).toString();
					closureStatusDate=((closureStatusDateLst.get(i)) == null)?"-":(closureStatusDateLst.get(i)).toString();
					break;
				}
			}
			jsObj.put("closureStudyStatId", closureStatusPK);*/
			String closureStatusDate = studyStatB.getMinPermanentClosureDate(studyId);
			jsObj.put("closureStatusDate", closureStatusDate);
		}

		out.println(jsObj.toString());
	} %>