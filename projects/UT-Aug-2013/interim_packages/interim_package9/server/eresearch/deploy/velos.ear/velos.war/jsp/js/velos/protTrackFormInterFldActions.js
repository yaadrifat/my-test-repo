protTrackFormInterFldActionsJS = {
	isPrintPreview: false,
	showHideFields: {}
};

protTrackFormInterFldActionsJS.showHideFields = function(fld, action){
	switch (action){
	case 'show':
		if (fld){
			fld.style.display = "inline";
			fld.parentNode.parentNode.parentNode.style.display = "inline";
		}
		break;
	case 'hide':
		if (fld){
			fld.style.display = "none";
			fld.parentNode.parentNode.parentNode.style.display = "none";
		}
		break;
	default:
		break;
	}
};

$j(document).ready(function(){
	formFieldMappingsJS.loadFormFieldMappings();
	
	var researchTypeSubType = getStudyInfoDialog_custom.getDataReceived('researchTypeSubType');
	while(!researchTypeSubType){
		researchTypeSubType = getStudyInfoDialog_custom.getDataReceived('researchTypeSubType');
	}

	var er_def_formstat = document.getElementById('er_def_formstat');
	protTrackFormInterFldActionsJS.isPrintPreview = (!er_def_formstat)? true : false;
	
	var ctrc_hpt_datesub, ctrc_hpt_initialapp, ctrc_hpt_annualreview;
	if (protTrackFormInterFldActionsJS.isPrintPreview){
		ctrc_hpt_datesub = document.getElementById(formFieldMappingsJS.getColumnSysId('ctrc_hpt_datesub')+'_span');
		ctrc_hpt_initialapp = document.getElementById(formFieldMappingsJS.getColumnSysId('ctrc_hpt_initialapp')+'_span');
		ctrc_hpt_annualreview = document.getElementById(formFieldMappingsJS.getColumnSysId('ctrc_hpt_annualreview')+'_span');
	} else {
		ctrc_hpt_datesub = document.getElementById(formFieldMappingsJS.getColumnSysId('ctrc_hpt_datesub'));
		ctrc_hpt_initialapp = document.getElementById(formFieldMappingsJS.getColumnSysId('ctrc_hpt_initialapp'));
		ctrc_hpt_annualreview = document.getElementById(formFieldMappingsJS.getColumnSysId('ctrc_hpt_annualreview'));
	}

	if ('insti' == researchTypeSubType){
		//show fields
		protTrackFormInterFldActionsJS.showHideFields(ctrc_hpt_datesub, 'show');
		protTrackFormInterFldActionsJS.showHideFields(ctrc_hpt_initialapp, 'show');
		protTrackFormInterFldActionsJS.showHideFields(ctrc_hpt_annualreview, 'show');
	} else {
		//hide fields
		protTrackFormInterFldActionsJS.showHideFields(ctrc_hpt_datesub, 'hide');
		protTrackFormInterFldActionsJS.showHideFields(ctrc_hpt_initialapp, 'hide');
		protTrackFormInterFldActionsJS.showHideFields(ctrc_hpt_annualreview, 'hide');
	}
	
	var ctrc_hpt_officenumber = document.getElementById(formFieldMappingsJS.getColumnSysId('ctrc_hpt_officenumber'));
	var lookupClick = ""+ctrc_hpt_officenumber.id+"|acusr_phone";
	
	var docAnchors = document.getElementsByTagName('A');
	for (var indx = 0; indx < docAnchors.length; indx++){
		var achrClick = ""+docAnchors[indx].onclick;
		if (!achrClick) { continue; }
		
		if (achrClick.indexOf(lookupClick) >= 0 ){
			docAnchors[indx].style.display="none";
		}
	}
});
