package com.velos.webservice.test;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

public class ListByPI {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			
			//String wURL="https://vprrestservices.uthscsa.edu/listbypis.json";       //URL for Production environment
			
			String wURL="https://vprrestservices.uthscsa.edu/test/listbypis.json";  //URL for Staging environment
			
			//String wURL="https://vprrestservices.uthscsa.edu/dev/listbypis.json";  //URL for Test,QA & DEV environments
			
				
			URL url = new URL(wURL);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			
			//defronzo@uthscsa.edu,jeongw@uthscsa.edu,eng@uthscsa.edu,cohn@uthscsa.edu,brennera@uthscsa.edu,bowdenc@uthscsa.edu,anzueto@uthscsa.edu,baileys@uthscsa.edu.
			String eresearchUser="dpatil@ss.a";
			String input = "{\"PI\":\"defronzo@uthscsa.edu\",\"Key\":\"e8ef34f0-b1e1-4bf6-9e28-63a76776781e\",\"EresearchUser\":\""+eresearchUser+"\"}";
			


			OutputStream os = conn.getOutputStream();
			os.write(input.getBytes());
			os.flush();

			Scanner scanner;
			String result;
			if (conn.getResponseCode() != 200) {
				scanner = new Scanner(conn.getErrorStream());
				result = "Error From Server \n\n";
			} else {
				scanner = new Scanner(conn.getInputStream());
				result = "Response From Server \n\n";
			}
			scanner.useDelimiter("\\Z");
			//System.out.println("***************"+result + scanner.next());//comment this or below
			
			String json=scanner.next(); //comment this or above
			
       System.out.println("*******json**************=\n"+json);			
			scanner.close();
			conn.disconnect();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

