package com.velos.controller;

import java.io.File;

import javax.mail.PasswordAuthentication;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Properties;
import java.util.Scanner;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jettison.json.JSONObject;

import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.web.study.UthscaStudyJB;

/**
 * Servlet implementation class HomeController
 */
public class HomeController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public HomeController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void service(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("application/json");
		try {
			String opCode = request.getParameter("opCode");
			Properties prop=new Properties();

			String eHome = EJBUtil.getEnvVariable("ERES_HOME");
			eHome = (eHome == null) ? "" : eHome;
			if (eHome.trim().equals("%ERES_HOME%"))
				eHome = System.getProperty("ERES_HOME");
			eHome = eHome.trim();
			String  fileName = eHome + "email.properties";

			File f=new File(fileName);
			FileInputStream inputStream=new FileInputStream(f);
			prop.load(inputStream);
			
			if(opCode!=null && opCode.trim().equals("1")){
				
				URL url = new URL(prop.getProperty("listbypi"));
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setDoOutput(true);
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Content-Type", "application/json");

				String studyNumber = request.getParameter("studyNumber");
				String logonName = request.getParameter("logonName");
				UthscaStudyJB studyJB= new UthscaStudyJB();
				String prinvemail = studyJB.getPrincipalInvestigatorEMail(studyNumber);
				String erUserEmail = studyJB.getEresearchUserEmail(logonName);
				System.out.println("logonName = " + logonName);
				
				//String input = "{\"PI\":\"defronzo@uthscsa.edu\",\"Key\":\"e8ef34f0-b1e1-4bf6-9e28-63a76776781e\",\"EresearchUser\":\"abcd@uthscsa.edu\"}";

				String input = "{\"PI\":\""+prinvemail+"\",\"Key\":\"e8ef34f0-b1e1-4bf6-9e28-63a76776781e\",\"EresearchUser\":\""+erUserEmail+"\"}";

				//Tested
				//String input = "{\"PI\":\"defronzo@uthscsa.edu\",\"Key\":\"e8ef34f0-b1e1-4bf6-9e28-63a76776781e\",\"EresearchUser\":\""+erUserEmail+"\"}";
				 System.out.println("**********Input Values of ListByPI WebService for StudyNumber=" + studyNumber + "*************");
	             System.out.println("PI = " + prinvemail);
	             System.out.println("EresearchUser = " + erUserEmail);
	             System.out.println("****************************************");
				
				OutputStream os = conn.getOutputStream();
				os.write(input.getBytes());
				os.flush();

				Scanner scanner;
				String result;
				if (conn.getResponseCode() != 200) {
					scanner = new Scanner(conn.getErrorStream());
					result = "Error From Server \n\n";
				} else {
					scanner = new Scanner(conn.getInputStream());
					result = "Response From Server \n\n";
				}
				scanner.useDelimiter("\\Z");

				String json=scanner.next();
				System.out.println("**********Result of ListByPI WebService for StudyNumber=" + studyNumber + "*************\n" + json);
				response.getWriter().write(json);
				scanner.close();
				conn.disconnect();
			}else if(opCode!=null && opCode.trim().equals("2")){

				URL url = new URL(prop.getProperty("generateid"));
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setDoOutput(true);
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Content-Type", "application/json");

				String studyNumber = request.getParameter("studyNumber");
				String logonName = request.getParameter("logonName");
				String studyTitle = request.getParameter("studyTitle");
				String studySummary = request.getParameter("studySummary");
				String studyId = request.getParameter("studyId");
				System.out.println("logonName = " + logonName);
				
				studyTitle =  URLEncoder.encode(studyTitle,"UTF-8");
				studySummary = URLEncoder.encode(studySummary,"UTF-8");
				
				UthscaStudyJB studyJB= new UthscaStudyJB();
				String prinvemail = studyJB.getPrincipalInvestigatorEMail(studyNumber);
				String erUserEmail = studyJB.getEresearchUserEmail(logonName);
				String enrollmentStatus = studyJB.getStudyEnrollmentStatus(studyNumber);
				
				
				//String input = "{\"PI\":\"france@uthscsa.edu\",\"Key\":\"e8ef34f0-b1e1-4bf6-9e28-63a76776781e\",\"ProtocolTitle\":\" A Euglycemic Insulin Clamp Study in Type 2 Diabetic Patients with Oral Insulin\",\"StudyNumber\":\"113\",\"EnrollmentStatus\":\"IRB-Active\",\"ProtocolDescription\":\"Human Study Requiring Consent\",\"EresearchUser\":\"sam@uthscsa.edu\",\"EresearchPK\":\"18578\"}";

				String input = "{\"PI\":\""+prinvemail+"\",\"Key\":\"e8ef34f0-b1e1-4bf6-9e28-63a76776781e\",\"ProtocolTitle\":\""+studyTitle+"\",\"StudyNumber\":\""+studyNumber+"\",\"EnrollmentStatus\":\""+enrollmentStatus+"\",\"ProtocolDescription\":\""+studySummary+"\",\"EresearchUser\":\""+erUserEmail+"\",\"EresearchPK\":\""+studyId+"\"}";
				
				//Tested
				//String input = "{\"PI\":\"france@uthscsa.edu\",\"Key\":\"e8ef34f0-b1e1-4bf6-9e28-63a76776781e\",\"ProtocolTitle\":\""+studyTitle+"\",\"StudyNumber\":\""+studyNumber+"\",\"EnrollmentStatus\":\""+enrollmentStatus+"\",\"ProtocolDescription\":\""+studySummary+"\",\"EresearchUser\":\""+erUserEmail+"\",\"EresearchPK\":\""+studyId+"\"}";
				 System.out.println("**********Input Values of GenerateID WebService for StudyNumber=" + studyNumber + "*************");
	             System.out.println("PI = " + prinvemail);
	             System.out.println("ProtocolTitle = " + studyTitle);
	             System.out.println("StudyNumber = " + studyNumber);
	             System.out.println("EnrollmentStatus = " + enrollmentStatus);
	             System.out.println("ProtocolDescription = " + studySummary);
	             System.out.println("EresearchUser = " + erUserEmail);
	             System.out.println("EresearchPK = " + studyId);
	             System.out.println("****************************************");
				OutputStream os = conn.getOutputStream();
				os.write(input.getBytes());
				os.flush();

				Scanner scanner;
				String result;
				if (conn.getResponseCode() != 200) {
					scanner = new Scanner(conn.getErrorStream());
					result = "Error From Server \n\n";
				} else {
					scanner = new Scanner(conn.getInputStream());
					result = "Response From Server \n\n";
				}
				scanner.useDelimiter("\\Z");

				String json=scanner.next(); 
				System.out.println("**********Result of GenerateID WebService for StudyNumber=" + studyNumber + "*************\n" + json);
				response.getWriter().write(json);
				scanner.close();
				conn.disconnect();
			}else if(opCode!=null && opCode.trim().equals("3")){
				
				String studyNumber = request.getParameter("studyNumber");

				String orcaId = request.getParameter("orcaId");
				
				URL url = new URL(prop.getProperty("updatebyid"));
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setDoOutput(true);
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Content-Type", "application/json");


				//String input = "{\"Key\":\"e8ef34f0-b1e1-4bf6-9e28-63a76776781e\",\"ORCA_ID\":\"25559\",\"StudyNumber\":\"115\"}";

				//Tested
				//String input = "{\"Key\":\"e8ef34f0-b1e1-4bf6-9e28-63a76776781e\",\"ORCA_ID\":\""+orcaId+"\",\"StudyNumber\":\""+studyNumber+"\"}";
				
				String input = "{\"Key\":\"e8ef34f0-b1e1-4bf6-9e28-63a76776781e\",\"ORCA_ID\":\""+orcaId+"\",\"StudyNumber\":\""+studyNumber+"\",\"Action\":\"associate\",\"XML\":\"\"}";
				System.out.println("**********Input Values of UpdateByID WebService for StudyNumber=" + studyNumber + "*************");
                System.out.println("ORCA_ID = " + orcaId);
                System.out.println("StudyNumber = " + studyNumber);
                System.out.println("****************************************");
				OutputStream os = conn.getOutputStream();
				os.write(input.getBytes());
				os.flush();

				Scanner scanner;
				String result;
				if (conn.getResponseCode() != 200) {
					scanner = new Scanner(conn.getErrorStream());
					result = "Error From Server \n\n";
				} else {
					scanner = new Scanner(conn.getInputStream());
					result = "Response From Server \n\n";
				}
				scanner.useDelimiter("\\Z");

				String json=scanner.next(); 
				System.out.println("**********Result of UpdateByID WebService for StudyNumber=" + studyNumber + "*************\n" + json);
				response.getWriter().write(json);
				scanner.close();
				conn.disconnect();

			}else if(opCode!=null && opCode.trim().equals("4")){

				try{
					String successMsg = "";			
					String studyNumber = request.getParameter("studyNumber");
					String protocolTitle = request.getParameter("studyTitle");
					String orcaId = request.getParameter("orcaId");
					String irbNumber = request.getParameter("irbNumber");
					String logonName = request.getParameter("logonName");
					
					UthscaStudyJB studyJB= new UthscaStudyJB();
					String erUserEmail = studyJB.getEresearchUserEmail(logonName);
					String piName = studyJB.getPIName(studyNumber);
					String userName = studyJB.getEresearchUserName(logonName);
					
					

					// Recipient's email ID needs to be mentioned.
					String to = prop.getProperty("mail.to");
					//String cc=prop.getProperty("mail.cc");
					// Sender's email ID needs to be mentioned
					final String from= prop.getProperty("mail.from");
					final String password= prop.getProperty("mail.passwd");
					
					String host =prop.getProperty("mail.host");
					String port =prop.getProperty("mail.port");
					String authentication=prop.getProperty("mail.smtp.auth");
					
					Properties properties = System.getProperties();
					properties.put("mail.smtp.host", host);
					properties.put("mail.smtp.port", port);
					//Session session = Session.getDefaultInstance(properties);
					properties.put("mail.smtp.auth",authentication);
					properties.setProperty("mail.debug", "true");
					
					properties.put("mail.smtp.socketFactory.port", "465");
					properties.put("mail.smtp.socketFactory.class",	"javax.net.ssl.SSLSocketFactory");
			 
					Session session = Session.getDefaultInstance(properties,
						new javax.mail.Authenticator() {
							protected PasswordAuthentication getPasswordAuthentication() {
								return new PasswordAuthentication(from,password);
							}
						});
					
									
					MimeMessage message = new MimeMessage(session);
					message.setFrom(new InternetAddress(from));
					message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(to));
					
					String subject1=prop.getProperty("mail.subject1");
					String subject2=prop.getProperty("mail.subject2");
										
					//message.setSubject(subject1+"   "+studyNumber+"   "+subject2+"   "+irbNumber);

					
					StringBuilder subject = new StringBuilder();
					subject.append(subject1);
					subject.append("   ");
					subject.append(studyNumber);
					subject.append("   ");
					subject.append(subject2);
					subject.append("   ");
					subject.append(irbNumber);

					message.setSubject(subject.toString());

					String body1=prop.getProperty("mail.body1");
					String body2=prop.getProperty("mail.body2");
					String body3=prop.getProperty("mail.body3");
					String body4=prop.getProperty("mail.body4");
					String body5=prop.getProperty("mail.body5");
					String body6=prop.getProperty("mail.body6");
					String body7=prop.getProperty("mail.body7");
					String body8=prop.getProperty("mail.body8");
					String body9=prop.getProperty("mail.body9");
					String body10=prop.getProperty("mail.body10");
					
					//message.setText(body1+"\n"+userName+" & "+erUserEmail+"\n"+body2+"\n"+body3+"  "+piName+"\n"+body4+"  "+studyNumber+"\n"+body5+"  "+protocolTitle+"\n"+body6+"  "+irbNumber+"\n"+body7+"  "+orcaId);

					StringBuilder msgText = new StringBuilder();
					msgText.append(body1);
					msgText.append("\n");
					msgText.append("      "+userName+"  "+erUserEmail);
					msgText.append("\n");
					msgText.append("\n");
					msgText.append(body2);
					msgText.append("\n");
					msgText.append("      "+body3);
					msgText.append(" ");
					msgText.append(piName);
					msgText.append("\n");
					msgText.append("      "+body4);
					msgText.append(" ");
					msgText.append(studyNumber);
					msgText.append("\n");
					msgText.append("      "+body5);
					msgText.append(" ");
					msgText.append(protocolTitle);
					msgText.append("\n");
					msgText.append("      "+body6);
					msgText.append(" ");
					msgText.append(irbNumber);
					msgText.append("\n");
					msgText.append("      "+body7);
					msgText.append(" ");
					msgText.append(orcaId);
					msgText.append("\n");
					msgText.append("\n");
					msgText.append(body8);
					msgText.append("\n");
					msgText.append("      "+body9);
					msgText.append("\n");
					msgText.append("      "+body10);

					message.setText(msgText.toString());
					
					Transport.send(message);
					
					JSONObject msg = new JSONObject();
					successMsg = "success"; 
					msg.put("Msg", successMsg);
					response.getWriter().write(msg.toString());		
				}
				catch(Exception e){
					e.printStackTrace();
					JSONObject msg = new JSONObject();
					String successMsg = "failure"; 
					msg.put("Msg", successMsg);
					response.getWriter().write(msg.toString());	
				}

			}
		
		} catch (Exception e) {
			
			e.printStackTrace();
		}
	}

}
