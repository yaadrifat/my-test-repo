
package com.velos.services.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import javax.xml.namespace.QName;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="StudySummary")

/**
 * Model class for fields in the Study Sumamary
 */
public class StudySummary
    extends ServiceObject 
    implements Serializable
{
	/**
     * 
     */
    private static final long serialVersionUID = -356950809796989200L;

    public StudySummary(){
        
    }

    /** constant defining the name of the velos tabespace for more study details */
	public static QName MORE_STUDY_DETAILS_NS = 
			new QName(
					"www.velos.com",
					"MoreStudyDetails",
			"Map");


	/** Theraputic Area **/
    protected Code therapeuticArea;

    protected UserIdentifier studyAuthor;

    protected UserIdentifier principalInvestigator;

    protected String principalInvestigatorOther;

    protected UserIdentifier studyContact;
    

    protected String studyNumber;

    protected String studyTitle;

    protected String objective;

    protected String summaryText;

    protected String agentDevice;

    protected Code division;

    protected List<Code> diseaseSites;

    protected String specificSite1;

    protected String specificSite2;

    protected String specificSite3;
   
    protected Integer nationalSampleSize;

    protected Duration studyDuration;

    protected Date estimatedBeginDate;

    protected Code phase;

    protected Code researchType;

    protected Code studyScope;

    protected Code studyType;
 
    //KM-#5383
    protected StudyIdentifier studyLinkedTo;

    protected Code blinding;

    protected Code randomization;

    protected Code sponsorName;
 
    protected String sponsorContact;

    protected String sponsorNameOther;

    protected String sponsorID;

    protected String sponsorOtherInfo;
  
    protected String keywords;

    protected List<NVPair> moreStudyDetails;

    protected boolean isPIMajorAuthor;

    protected String studyIndIdeNum;
    //virendra
    protected Boolean isStdDefPublic;
    
    protected Boolean isStdDetailPublic;
    
    protected Boolean isStdDesignPublic;
    
    protected Boolean isStdSponsorInfoPublic;
    
    //virendra:for #5246 
    protected Boolean isInvestigatorHeldIndIde;
    
    protected Boolean isCtrpReportable;
    
    protected Boolean isFdaRegulated;
    
    /**
     * Sets the disease sites for this Study.
     * @param diseaseSites
     */
	public void setDiseaseSites(List<Code> diseaseSites) {
		this.diseaseSites = diseaseSites;
	}


    /**
     * Returns the therapeutic area for this study
     * @return
     */
	@NotNull
	@Valid
    public Code getTherapeuticArea() {
        return therapeuticArea;
    }

    /**
     * Sets the therapeutic area for this study.
     * @param value
     */
    public void setTherapeuticArea(Code value) {
        this.therapeuticArea = value;
    }

    
    /**
     * Gets the study author for this study.
     * @return
     */
    @Valid
    @NotNull
    public UserIdentifier getStudyAuthor() {
        return studyAuthor;
    }

    /**
     * Sets the study author for this study.
     * @param value
     */
    public void setStudyAuthor(UserIdentifier value) {
        this.studyAuthor = value;
    }

    /**
     * Gets the principal investigator for this study.
     * 
     * @return
     */
    public UserIdentifier getPrincipalInvestigator() {
        return principalInvestigator;
    }

    /**
     * Sets the principal investigator for this study. Though they are logically-related,
     * this is different from a Principal Investigator stored in the study team
     * as a {@link StudyTeamMember}.
     * @param value
     */
    public void setPrincipalInvestigator(UserIdentifier value) {
        this.principalInvestigator = value;
    }

    /**
     * Returns the free text field other principal investigator
     * @return
     */
    public String getPrincipalInvestigatorOther() {
        return principalInvestigatorOther;
    }

    /**
     * Sets the free text field other principal investigator
     * @return
     */
    public void setPrincipalInvestigatorOther(String value) {
        this.principalInvestigatorOther = value;
    }

    /**
     * Sets the principal investigator for this study. Though they are logically-related,
     * this is different from a Data Manager stored in the study team
     * as a {@link StudyTeamMember}.
     * @param value
     */
    public UserIdentifier getStudyContact() {
        return studyContact;
    }

    /**
     * Gets the principal investigator for this study. Though they are logically-related,
     * this is different from a Data Manager stored in the study team
     * as a {@link StudyTeamMember}.
     * @param value
     */
    public void setStudyContact(UserIdentifier value) {
        this.studyContact = value;
    }

	/**
	 * Gets the study number for this study.
	 * @return
	 */
    @NotNull
    @Size(min = 1, max = 100)
    public String getStudyNumber() {
        return studyNumber;
    }

    /**
    * Sets the study number for this study.
    * @param value
    */
    public void setStudyNumber(String value) {
        this.studyNumber = value;
    }
    
    /**
     * Gets the study title. 
     * @return
     */
    @NotNull
    @Size(min=0, max=1000)
    public String getStudyTitle() {
        return studyTitle;
    }

    /**
     * Sets the study title. 
     * @param value
     */
    public void setStudyTitle(String value) {
        this.studyTitle = value;
    }

    /**
     * Gets the object for the study.
     * @return
     */
    public String getObjective() {
        return objective;
    }

    /**
     * Sets the objective for the study.
     * @param value
     */
    public void setObjective(String value) {
        this.objective = value;
    }

    /**
     * Gets the summary text field for the study
     * @return
     */
    public String getSummaryText() {
        return summaryText;
    }

    /**
     * Returns the summary text field for the study.
     * @param value
     */
    public void setSummaryText(String value) {
        this.summaryText = value;
    }

    /**
     * Returns the agent device field for the study.
     * @return
     */
    @Size(max=100)
    public String getAgentDevice() {
        return agentDevice;
    }

    /**
     * Sets the agent device field for the study. 
     * @param value
     */
    public void setAgentDevice(String value) {
        this.agentDevice = value;
    }

    /**
     * Returns the division code for the study
     * @return
     */
    @Valid
    public Code getDivision() {
        return division;
    }

    /**
     * Sets the division code for the study
     * @param value
     */
    public void setDivision(Code value) {
        this.division = value;
    }

    /**
     * Returns the list of disease sites for the study.
     * @return
     */
    @Valid
    public List<Code> getDiseaseSites() {
        if (diseaseSites == null) {
            diseaseSites = new ArrayList<Code>();
        }
        return this.diseaseSites;
    }

    /**
     * Gets the specific-site1 field.
     * @return
     */
    @Size(max=2000)
    public String getSpecificSite1() {
        return specificSite1;
    }

    /**
     * Sets the specific site 1 field
     * @param value
     */
    public void setSpecificSite1(String value) {
        this.specificSite1 = value;
    }

    /**
     * Gets the specific site 2 field.
     * @return
     */
    @Size(max=2000)
    public String getSpecificSite2() {
        return specificSite2;
    }

    /**
     * Sets the speicfic site 2 field
     * @param value
     */
    public void setSpecificSite2(String value) {
        this.specificSite2 = value;
    }

    /**
     * Gets speicific site 3 field.
     * @return
     */
    @Size(max=2000)
    public String getSpecificSite3() {
        return specificSite3;
    }

    /**
     * Sets specific site 3 field
     * @param value
     */
    public void setSpecificSite3(String value) {
        this.specificSite3 = value;
    }

    /**
     * Returns the national sample size field.
     * @return
     */
    @Max(999999999)
    public Integer getNationalSampleSize() {
        return nationalSampleSize;
    }

    /**
     * Sets the national sample size field.
     * @param value
     */
    public void setNationalSampleSize(Integer value) {
        this.nationalSampleSize = value;
    }

    /**
     * Gets study duration.
     * @return
     */
    public Duration getStudyDuration() {
        return studyDuration;
    }

    /**
     * Sets study duration.
     * @param value
     */
    public void setStudyDuration(Duration value) {
        this.studyDuration = value;
    }

    /**
     * Get estimated begin date.
     * @return
     */
    public Date getEstimatedBeginDate() {
        return estimatedBeginDate;
    }

    /**
     * Sets estimated begin date
     * @param value
     */
    public void setEstimatedBeginDate(Date value) {
        this.estimatedBeginDate = value;
    }

	/**
	 * Gets study phase
	 * @return
	 */
    @NotNull
    @Valid
    public Code getPhase() {
        return phase;
    }

    /**
     * Sets study phase
     * @param value
     */
    public void setPhase(Code value) {
        this.phase = value;
    }

    /**
     * Gets research type
     * @return
     */
    @Valid
    public Code getResearchType() {
        return researchType;
    }

    /**
     * Sets research type
     * @param value
     */
    public void setResearchType(Code value) {
        this.researchType = value;
    }

    /**
     * Gets study scope
     * @return
     */
    @Valid
    public Code getStudyScope() {
        return studyScope;
    }

	/**
	 * Sets study scope.
	 * @param value
	 */
    public void setStudyScope(Code value) {
        this.studyScope = value;
    }

    /**
     * Gets study type
     * @return
     */
    @Valid
    public Code getStudyType() {
        return studyType;
    }

    /**
     * Sets study type
     * @param value
     */
    public void setStudyType(Code value) {
        this.studyType = value;
    }

    /**
     * Gets study linked to
     * @return
     */
    //KM-#5383
    public StudyIdentifier getStudyLinkedTo() {
        return studyLinkedTo;
    }

    /**
     * Sets study linked to
     * @param value
     */
    public void setStudyLinkedTo(StudyIdentifier value) {
        this.studyLinkedTo = value;
    }

    /**
     * Gets study Blinding
     * @return
     */
    @Valid
    public Code getBlinding() {
        return blinding;
    }

    /**
     * Sets study blinding
     * @param value
     */
    public void setBlinding(Code value) {
        this.blinding = value;
    }

    /**
     * Gets study randomization
     * @return
     */
    @Valid
    public Code getRandomization() {
        return randomization;
    }

    /**
     * Sets study randomization
     * @param value
     */
    public void setRandomization(Code value) {
        this.randomization = value;
    }

    /**
     * Gets sponsor name
     * @return
     */
    public Code getSponsorName() {
        return sponsorName;
    }

    /**
     * Sets sponsor name
     * @param value
     */
    public void setSponsorName(Code value) {
        this.sponsorName = value;
    }

    /**
     * Gets sponsor contact
     * @return
     */
    @Size(max=2000)
    public String getSponsorContact() {
        return sponsorContact;
    }

    /**
     * Sets sponsor contact
     * @param value
     */
    public void setSponsorContact(String value) {
        this.sponsorContact = value;
    }

    /**
     * Gets sponsor other name
     * @return
     */
    public String getSponsorNameOther() {
        return sponsorNameOther;
    }

    /**
     * Sets sponsor other name
     * @param value
     */
    public void setSponsorNameOther(String value) {
        this.sponsorNameOther = value;
    }

    /**
     * Gets sponsor ID
     * @return
     */
    @Size(max=50)
    public String getSponsorID() {
        return sponsorID;
    }

    /**
     * Sets sponsor ID
     * @param value
     */
    public void setSponsorID(String value) {
        this.sponsorID = value;
    }

    /**
     * Gets sponsor other info
     * @return
     */
    @Size(max=2000)
    public String getSponsorOtherInfo() {
        return sponsorOtherInfo;
    }

    /**
     * Sets sponsor other info
     * @param value
     */
    public void setSponsorOtherInfo(String value) {
        this.sponsorOtherInfo = value;
    }

    /**
     * Gets keywords
     * @return
     */
    @Size(max=500)
    public String getKeywords() {
        return keywords;
    }

    /**
     * Sets keywords
     * @param value
     */
    public void setKeywords(String value) {
        this.keywords = value;
    }

    /**
     * Gets more study details
     * @return
     */
    public List<NVPair> getMoreStudyDetails() {
        return moreStudyDetails;
    }

    /**
     * Sets more study details
     * @param value
     */
    public void setMoreStudyDetails(List<NVPair> value) {
        this.moreStudyDetails = value;
    }

    /**
     * Gets the Is PI Major Author field
     * @return
     */
	public boolean isPIMajorAuthor() {
		return isPIMajorAuthor;
	}

	/**
	 * Sets the Is PI Major Author field
	 * @param isPIMajorAuthor
	 */
	public void setPIMajorAuthor(boolean isPIMajorAuthor) {
		this.isPIMajorAuthor = isPIMajorAuthor;
	}

	/**
	 * Gets the IND/NDE number field
	 * @return
	 */
	public String getStudyIndIdeNum() {
		return studyIndIdeNum;
	}

	/**
	 * Sets the IND/NDE number field
	 * @param studyIndIdeNum
	 */
	public void setStudyIndIdeNum(String studyIndIdeNum) {
		this.studyIndIdeNum = studyIndIdeNum;
	}


	/**
	 * Gets the IND/NDE number field
	 * @return
	 */
	public Boolean getIsStdDefPublic() {
		return isStdDefPublic;
	}

	/**
	 * Sets the Is Study Definition Public field
	 * @param isStdDefPublic
	 */
	public void setIsStdDefPublic(Boolean isStdDefPublic) {
		this.isStdDefPublic = isStdDefPublic;
	}

	/**
	 * Gets the Is Study Detail Public field
	 * @return
	 */
	public Boolean getIsStdDetailPublic() {
		return isStdDetailPublic;
	}

	/**
	 * Sets the Is Study Detail Public field
	 * @param isStdDetailPublic
	 */
	public void setIsStdDetailPublic(Boolean isStdDetailPublic) {
		this.isStdDetailPublic = isStdDetailPublic;
	}

	/**
	 * Gets the Is Study Design Public field
	 * @return
	 */
	public Boolean getIsStdDesignPublic() {
		return isStdDesignPublic;
	}
	
	/**
	 * Sets the Is Study Design Public field
	 * @param isStdDesignPublic
	 */
	public void setIsStdDesignPublic(Boolean isStdDesignPublic) {
		this.isStdDesignPublic = isStdDesignPublic;
	}

	/**
	 * Gets the Is Study Sponsor Info Public field
	 * @return
	 */
	public Boolean getIsStdSponsorInfoPublic() {
		return isStdSponsorInfoPublic;
	}

	/**
	 * Sets the Is Study Sponsor Info Public field
	 *
	 * @param isStdSponsorInfoPublic
	 */
	public void setIsStdSponsorInfoPublic(Boolean isStdSponsorInfoPublic) {
		this.isStdSponsorInfoPublic = isStdSponsorInfoPublic;
	}

	/**
	 * Gets the PI held IND/NDE field
	 * @return
	 */
	public Boolean getIsInvestigatorHeldIndIde() {
		return isInvestigatorHeldIndIde;
	}

	/**
	 * Sets the PI held IND/NDE field
	 * @param isInvestigatorHeldIndIde
	 */
	public void setIsInvestigatorHeldIndIde(Boolean isInvestigatorHeldIndIde) {
		this.isInvestigatorHeldIndIde = isInvestigatorHeldIndIde;
	}
	
	public Boolean getIsCtrpReportable() {
		return isCtrpReportable;
	}


	public void setIsCtrpReportable(Boolean isCtrpReportable) {
		this.isCtrpReportable = isCtrpReportable;
	}


	public Boolean getIsFdaRegulated() {
		return isFdaRegulated;
	}


	public void setIsFdaRegulated(Boolean isFdaRegulated) {
		this.isFdaRegulated = isFdaRegulated;
	}


	//virendra;Fixed#6121, overloading setParentId for studyIdentifier
    public StudyIdentifier getParentIdentifier() {
        if (super.parentIdentifier == null || 
                super.parentIdentifier.getId().size() < 1) { return null; }
        for (SimpleIdentifier myId : super.parentIdentifier.getId()) {
            if (myId instanceof StudyIdentifier) {
                return (StudyIdentifier)myId;
            }
        }
        return null;
    }
    
    public void setParentIdentifier(StudyIdentifier studyIdentifer) {
        List<SimpleIdentifier> idList = new ArrayList<SimpleIdentifier>();
        idList.add(studyIdentifer);
        super.parentIdentifier = new ParentIdentifier();
        super.parentIdentifier.setId(idList);
    }
	
}
