package com.velos.eres.web.study;


import com.velos.eres.service.studyAgent.UthscaStudyAgent;
import com.velos.eres.service.util.UthscaEJBUtil;

public class UthscaStudyJB {
	
	public String getPrincipalInvestigatorEMail(String studyNumber) throws Exception{
    	UthscaStudyAgent studyAgent = UthscaEJBUtil.getUthscaStudyAgentHome();
    	String email = studyAgent.getPrincipalInvestigatorEmail(studyNumber);
    	return email;
    }
	
	public String getEresearchUserEmail(String userName) throws Exception {
		UthscaStudyAgent studyAgent = UthscaEJBUtil.getUthscaStudyAgentHome();
		String email = studyAgent.getEresearchUserEmail(userName);
		return email;
	}

	public String getStudyEnrollmentStatus(String studyNumber) throws Exception{
		UthscaStudyAgent studyAgent = UthscaEJBUtil.getUthscaStudyAgentHome();
		String enrollmentStatus = studyAgent.getStudyEnrollmentStatus(studyNumber);
		return enrollmentStatus;
	}
    
	public String getStudyStatusID(String desc) throws Exception{
		UthscaStudyAgent studyAgent = UthscaEJBUtil.getUthscaStudyAgentHome();
		String studyStatusId = studyAgent.getStudyStatusID(desc);
		return studyStatusId;
	}
	
	public String getStudyStatusType(String key) throws Exception{
		UthscaStudyAgent studyAgent = UthscaEJBUtil.getUthscaStudyAgentHome();
		String studyStatusType = studyAgent.getStudyStatusType(key);
		return studyStatusType;
	}
	
	public String getStudyStatusTypeID(String desc) throws Exception{
		UthscaStudyAgent studyAgent = UthscaEJBUtil.getUthscaStudyAgentHome();
		String studyStatusTypeId = studyAgent.getStudyStatusTypeID(desc);
		return studyStatusTypeId;
	}
	
	public String getCurrentStudyStatusId() throws Exception{
		UthscaStudyAgent studyAgent = UthscaEJBUtil.getUthscaStudyAgentHome();
		String currentStatusId = studyAgent.getCurrentStudyStatusId();
		return currentStatusId;
	}
	public String 	getSiteID(String siteName) throws Exception{
		UthscaStudyAgent studyAgent = UthscaEJBUtil.getUthscaStudyAgentHome();
		String siteID = studyAgent.getSiteID(siteName);
		return siteID;
	}
	public String 	updateStudyTitle(String protocolTitle,int studyId,String ipAdd,String modifiedBy) throws Exception{
		UthscaStudyAgent studyAgent = UthscaEJBUtil.getUthscaStudyAgentHome();
		String status = studyAgent.updateStudyTitle(protocolTitle, studyId, ipAdd, modifiedBy);
		return status;
	}
	public String getPIName(String studyNumber)throws Exception{
    	UthscaStudyAgent studyAgent = UthscaEJBUtil.getUthscaStudyAgentHome();
    	String piName = studyAgent.getPIName(studyNumber);
    	return piName;
    }
	public String getEresearchUserName(String logonName) throws Exception {
		UthscaStudyAgent studyAgent = UthscaEJBUtil.getUthscaStudyAgentHome();
		String userName = studyAgent.getEresearchUserName(logonName);
		return userName;
	}
	
}
