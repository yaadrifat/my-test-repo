package com.velos.eres.service.studyAgent;

import javax.ejb.Remote;

@Remote
public interface UthscaStudyAgent {
	
	public String getPrincipalInvestigatorEmail(String studyNumber) throws Exception;
	public String getEresearchUserEmail(String userName) throws Exception;
	public String getStudyEnrollmentStatus(String studyNumber) throws Exception;
	public String getStudyStatusID(String desc) throws Exception;
	public String getStudyStatusType(String key) throws Exception;
	public String getStudyStatusTypeID(String desc) throws Exception;
	public String getCurrentStudyStatusId() throws Exception;
	public String getSiteID(String siteName) throws Exception;
	public String updateStudyTitle(String protocolTitle, int studyId, String ipAdd, String modifiedBy) throws Exception;
	public String getPIName(String studyNumber) throws Exception;
	public String getEresearchUserName(String logonName) throws Exception;
}
