package com.velos.eres.service.util;


public class UthscaJNDINames {
	
	public final static String prefix = "velos/";
	public final static String suffix = "/remote";
	
	public final static String UthscaStudyAgentHome = prefix+"UthscaStudyAgentBean"+suffix;

}
