package com.velos.eres.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
public class UthscaStudyDao extends CommonDAO implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String getPrincipalInvestigatorEmail(String studyNumber) throws Exception{
    	Connection conn = null;
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	String email = null;
    	try{
    		conn = getConnection();
	    	String sSQL = "select add_email from er_add where pk_add=(select fk_peradd from er_user"+
	    					" where PK_USER=(select study_prinv from er_study where study_number=?))";
    		Rlog.debug("study","UthscsaStudyDao.getPrincipalInvestigatorEmail sql:"+ sSQL);
	    	ps = conn.prepareStatement(sSQL);
	    	ps.setString(1, studyNumber);
	    	rs = ps.executeQuery();
	    	if(rs.next()){
	    		email = rs.getString("add_email");
	    	}
	    	
    	}catch(Exception e){
    		Rlog.fatal("study","UthscsaStudyDao.getPrincipalInvestigatorEmail EXCEPTION IN FETCHING FROM ADD table"+ e);
    	}finally{
    		try {
    			if (rs != null)
    				rs.close();
    		} catch (Exception e) {
    		}
    		try {
    			if (ps != null)
    				ps.close();
    		} catch (Exception e) {
    		}
    		try {
    			if (conn != null)
    				conn.close();
    		} catch (Exception e) {
    		}

    	}
    	return email;
    }
	
	 public String getEresearchUserEmail(String userName) throws Exception{
	    	Connection conn = null;
	    	PreparedStatement ps = null;
	    	ResultSet rs = null;
	    	String email = null;
	    	try{
	    		conn = getConnection();
	    		String sSQL = "select add_email from er_add where pk_add=(select fk_peradd from er_user where usr_logname=?)";
	    		Rlog.debug("study","UthscsaStudyDao.getEresearchUserEmail sql:"+ sSQL);
	    		ps = conn.prepareStatement(sSQL);
	    		ps.setString(1, userName);
	    		rs = ps.executeQuery();
	    		if(rs.next()){
	    			email = rs.getString("add_email");
	    		}
	    	}catch(Exception e){
	    		 Rlog.fatal("study","UthscsaStudyDao.getEresearchUserEmail EXCEPTION IN FETCHING FROM ADD table"+ e);
	    	}finally{
	    		try {
	    			if (rs != null)
	    				rs.close();
	    		} catch (Exception e) {
	    		}
	    		try {
	    			if (ps != null)
	    				ps.close();
	    		} catch (Exception e) {
	    		}
	    		try {
	    			if (conn != null)
	    				conn.close();
	    		} catch (Exception e) {
	    		}

	    	}
	    	return email;
	    }
	    
	    public String getStudyEnrollmentStatus(String studyNumber) throws Exception{
	    	Connection conn = null;
	    	PreparedStatement ps = null;
	    	ResultSet rs = null;
	    	String enrollmentStatus = null;
	    	try{
	    		conn = getConnection();
	    		String sSQL = "select codelst_desc from er_codelst where pk_codelst=(select fk_codelst_studystat from er_studystat where pk_studystat=(select max(pk_studystat) from er_studystat where fk_study=(select pk_study from er_study where STUDY_NUMBER=?) AND status_type=(select pk_codelst from er_codelst where codelst_desc='Study Enrollment/UT')))";
	    		Rlog.debug("study","UthscsaStudyDao.getStudyEnrollmentStatus sql:"+ sSQL);
	    		ps = conn.prepareStatement(sSQL);
	    		ps.setString(1, studyNumber);
	    		rs = ps.executeQuery();
	    		if(rs.next()){
	    			enrollmentStatus = rs.getString("Codelst_desc");
	    		}
	    	}catch(Exception e){
	    		Rlog.fatal("study","UthscsaStudyDao.getStudyEnrollmentStatus EXCEPTION IN FETCHING FROM codelst table"+ e);
	    		}finally{
	    		try {
	    			if (rs != null)
	    				rs.close();
	    		} catch (Exception e) {
	    		}
	    		try {
	    			if (ps != null)
	    				ps.close();
	    		} catch (Exception e) {
	    		}
	    		try {
	    			if (conn != null)
	    				conn.close();
	    		} catch (Exception e) {
	    		}

	    	}
	    	return enrollmentStatus;
	    }
	    
	    public String getStudyStatusID(String desc) throws Exception {
	    	Connection conn = null;
	    	PreparedStatement ps = null;
	    	ResultSet rs = null;
	    	String studyStatusId = null;
	    	try{
	    		conn = getConnection();
	    		String SQL = "select pk_codelst from ER_CODELST where codelst_desc=? and codelst_type='studystat'";
	    		Rlog.debug("study","UthscsaStudyDao.getStudyStatusID sql:"+ SQL);
	    		ps = conn.prepareStatement(SQL);
	    		ps.setString(1, desc);
	    		rs = ps.executeQuery();
	    		if(rs.next()){
	    			studyStatusId = rs.getString("pk_codelst");
	    		}
	    	}catch(Exception e){
	    		Rlog.fatal("study","UthscsaStudyDao.getStudyStatusID EXCEPTION IN FETCHING FROM codelst table"+ e);
	    		}finally{
	    		try {
	    			if (rs != null)
	    				rs.close();
	    		} catch (Exception e) {
	    		}
	    		try {
	    			if (ps != null)
	    				ps.close();
	    		} catch (Exception e) {
	    		}
	    		try {
	    			if (conn != null)
	    				conn.close();
	    		} catch (Exception e) {
	    		}

	    	}
	    	
	    	return studyStatusId;
	    }
	    
	    public String getStudyStatusType(String key) throws Exception{
	    	Connection conn = null;
	    	PreparedStatement ps = null;
	    	ResultSet rs = null;
	    	String studyStatusType = null;
	    	try{
	    		conn = getConnection();
	    		String SQL = "select codelst_custom_col1 from ER_CODELST where pk_codelst=?";
	    		Rlog.debug("study","UthscsaStudyDao.getStudyStatusType sql:"+ SQL);
	    		ps = conn.prepareStatement(SQL);
	    		ps.setString(1, key);
	    		rs = ps.executeQuery();
	    		if(rs.next()){
	    			studyStatusType = rs.getString("codelst_custom_col1");
	    		}
	    	}catch(Exception e){
	    		Rlog.fatal("study","UthscsaStudyDao.getStudyStatusType EXCEPTION IN FETCHING FROM codelst table"+ e);
	    		}finally{
	    		try {
	    			if (rs != null)
	    				rs.close();
	    		} catch (Exception e) {
	    		}
	    		try {
	    			if (ps != null)
	    				ps.close();
	    		} catch (Exception e) {
	    		}
	    		try {
	    			if (conn != null)
	    				conn.close();
	    		} catch (Exception e) {
	    		}
	    		
	    	}
	    	return studyStatusType;
	    }
	    
	    
		public String getStudyStatusTypeID(String desc) throws Exception{
	    	Connection conn = null;
	    	PreparedStatement ps = null;
	    	ResultSet rs = null;
	    	String studyStatusTypeId = null;
	    	try{
	    		conn = getConnection();
	    		String SQL = "select pk_codelst from er_codelst where codelst_subtyp=?";
	    		Rlog.debug("study","UthscsaStudyDao.getStudyStatusTypeID sql:"+ SQL);
	    		ps = conn.prepareStatement(SQL);
	    		ps.setString(1, desc);
	    		rs = ps.executeQuery();
	    		if(rs.next()){
	    			studyStatusTypeId = rs.getString("pk_codelst");
	    		}
	    	}catch(Exception e){
	    		Rlog.fatal("study","UthscsaStudyDao.getStudyStatusTypeID EXCEPTION IN FETCHING FROM codelst table"+ e);
	    	}finally{
	    		try {
	    			if (rs != null)
	    				rs.close();
	    		} catch (Exception e) {
	    		}
	    		try {
	    			if (ps != null)
	    				ps.close();
	    		} catch (Exception e) {
	    		}
	    		try {
	    			if (conn != null)
	    				conn.close();
	    		} catch (Exception e) {
	    		}
	    		
	    	}
	    	return studyStatusTypeId;
		}
		
		public String getCurrentStudyStatusId() throws Exception{
	    	Connection conn = null;
	    	PreparedStatement ps = null;
	    	ResultSet rs = null;
	    	String currentStatusId = null;
			try
			{
				conn = getConnection();
				String SQL = "select max(pk_studystat) current_status_id from er_studystat where current_stat=1";
	    		Rlog.debug("study","UthscsaStudyDao.getCurrentStudyStatusId sql:"+ SQL);
				ps = conn.prepareStatement(SQL);
				rs = ps.executeQuery();
				if(rs.next()){
					currentStatusId = rs.getString("current_status_id");
				}
			}catch(Exception e){
				Rlog.fatal("study","UthscsaStudyDao.getCurrentStudyStatusId EXCEPTION IN FETCHING FROM studystat table"+ e);
			}finally{
	    		try {
	    			if (rs != null)
	    				rs.close();
	    		} catch (Exception e) {
	    		}
	    		try {
	    			if (ps != null)
	    				ps.close();
	    		} catch (Exception e) {
	    		}
	    		try {
	    			if (conn != null)
	    				conn.close();
	    		} catch (Exception e) {
	    		}
	    		
	    	}
			return currentStatusId;
		}
	    
		public String getSiteID(String siteName) throws Exception{
	    	Connection conn = null;
	    	PreparedStatement ps = null;
	    	ResultSet rs = null;
	    	String siteID = null;
			try
			{
				conn = getConnection();
				String SQL = "select pk_site from er_site where site_name=?";
				Rlog.debug("study","UthscsaStudyDao.getSiteID sql:"+ SQL);
	    		ps = conn.prepareStatement(SQL);
	    		ps.setString(1, siteName);
	    		rs = ps.executeQuery();
	    		if(rs.next()){
	    			siteID = rs.getString("pk_site");
	    		}
			}catch(Exception e){
				Rlog.fatal("study","UthscsaStudyDao.getSiteID EXCEPTION IN FETCHING FROM studystat table"+ e);
			}finally{
	    		try {
	    			if (rs != null)
	    				rs.close();
	    		} catch (Exception e) {
	    		}
	    		try {
	    			if (ps != null)
	    				ps.close();
	    		} catch (Exception e) {
	    		}
	    		try {
	    			if (conn != null)
	    				conn.close();
	    		} catch (Exception e) {
	    		}
	    		
	    	}
			return siteID;
		}
		public String updateStudyTitle(String protocolTitle, int studyId, String ipAdd, String modifiedBy) throws Exception{
	    	Connection conn = null;
	    	PreparedStatement ps = null;
	    	int rows=0;
	    	String status = null;
			try
			{
				conn = getConnection();
				String SQL = "update er_study set study_title=?,ip_add=?,last_modified_by=? where pk_study=?";
				Rlog.debug("study","UthscsaStudyDao.updateStudyTitle sql:"+ SQL);
	    		ps = conn.prepareStatement(SQL);
	    		ps.setString(1, protocolTitle);
	    		ps.setString(2, ipAdd);
	    		ps.setString(3, modifiedBy);
	    		ps.setInt(4, studyId);
	    		
	    		rows = ps.executeUpdate();
	    		if(rows==1){
	    			status = "success";
	    		}
	    		else
	    			status = "failure";
			}catch(Exception e){
				status = "failure";
				Rlog.fatal("study","UthscsaStudyDao.updateStudyTitle EXCEPTION IN Updating  studytitle"+ e);
			}finally{
	    		try {
	    			if (ps != null)
	    				ps.close();
	    		} catch (Exception e) {
	    		}
	    		try {
	    			if (conn != null)
	    				conn.close();
	    		} catch (Exception e) {
	    		}
	    		
	    	}
			return status;
		}

		public String getPIName(String studyNumber) throws Exception{
	    	Connection conn = null;
	    	PreparedStatement ps = null;
	    	ResultSet rs = null;
	    	String piName = null;
	    	String firstName = null;
	    	String middleName = null;
	    	String lastName = null;
	    	try{
	    		conn = getConnection();
		    	String sSQL = "select usr_firstname,usr_midname,usr_lastname from er_user where PK_USER=(select study_prinv from er_study where study_number=?)";
	    		Rlog.debug("study","UthscsaStudyDao.getPIName sql:"+ sSQL);
		    	ps = conn.prepareStatement(sSQL);
		    	ps.setString(1, studyNumber);
		    	rs = ps.executeQuery();
		    	if(rs.next()){
		    		firstName = rs.getString("usr_firstname");
		    		middleName = rs.getString("usr_midname");
		    		lastName = rs.getString("usr_lastname");
		    	}
		    	
		    	piName = getName(firstName,middleName,lastName);
	    	}catch(Exception e){
	    		Rlog.fatal("study","UthscsaStudyDao.getPIName EXCEPTION IN FETCHING FROM USER table"+ e);
	    	}finally{
	    		try {
	    			if (rs != null)
	    				rs.close();
	    		} catch (Exception e) {
	    		}
	    		try {
	    			if (ps != null)
	    				ps.close();
	    		} catch (Exception e) {
	    		}
	    		try {
	    			if (conn != null)
	    				conn.close();
	    		} catch (Exception e) {
	    		}

	    	}
	    	return piName;
	    }

		public String getEresearchUserName(String logonName) throws Exception{
	    	Connection conn = null;
	    	PreparedStatement ps = null;
	    	ResultSet rs = null;
	    	String userName = null;
	    	String firstName = null;
	    	String middleName = null;
	    	String lastName = null;
	    	try{
	    		conn = getConnection();
	    		String sSQL = "select usr_firstname,usr_midname,usr_lastname from er_user where usr_logname=?";
	    		Rlog.debug("study","UthscsaStudyDao.getEresearchUserName sql:"+ sSQL);
	    		ps = conn.prepareStatement(sSQL);
	    		ps.setString(1, logonName);
	    		rs = ps.executeQuery();
	    		if(rs.next()){
	    			firstName = rs.getString("usr_firstname");
		    		middleName = rs.getString("usr_midname");
		    		lastName = rs.getString("usr_lastname");
	    		}
	    				    	
		    	userName = getName(firstName,middleName,lastName);
	    	}catch(Exception e){
	    		 Rlog.fatal("study","UthscsaStudyDao.getEresearchUserName EXCEPTION IN FETCHING FROM USER table"+ e);
	    	}finally{
	    		try {
	    			if (rs != null)
	    				rs.close();
	    		} catch (Exception e) {
	    		}
	    		try {
	    			if (ps != null)
	    				ps.close();
	    		} catch (Exception e) {
	    		}
	    		try {
	    			if (conn != null)
	    				conn.close();
	    		} catch (Exception e) {
	    		}

	    	}
	    	return userName;
	    }
		public String getName(String firstName,String middleName,String lastName){
			String name = null;
			if(!StringUtil.isEmpty(firstName) && !StringUtil.isEmpty(middleName) && !StringUtil.isEmpty(lastName)){
				name=firstName+" "+middleName+" "+lastName;
	    	}else if(StringUtil.isEmpty(firstName) && !StringUtil.isEmpty(middleName) && !StringUtil.isEmpty(lastName)){
	    		name=middleName+" "+lastName;
	    	}else if(!StringUtil.isEmpty(firstName) && StringUtil.isEmpty(middleName) && !StringUtil.isEmpty(lastName)){
	    		name=firstName+" "+lastName;
	    	}else if(!StringUtil.isEmpty(firstName) && !StringUtil.isEmpty(middleName) && StringUtil.isEmpty(lastName)){
	    		name=firstName+" "+middleName;
	    	}else if(StringUtil.isEmpty(firstName) && StringUtil.isEmpty(middleName) && !StringUtil.isEmpty(lastName)){
	    		name=lastName;
	    	}else if(StringUtil.isEmpty(firstName) && !StringUtil.isEmpty(middleName) && StringUtil.isEmpty(lastName)){
	    		name=middleName;
	    	}else if(!StringUtil.isEmpty(firstName) && StringUtil.isEmpty(middleName) && StringUtil.isEmpty(lastName)){
	    		name=firstName;
	    	}
			return name;

		}

}
