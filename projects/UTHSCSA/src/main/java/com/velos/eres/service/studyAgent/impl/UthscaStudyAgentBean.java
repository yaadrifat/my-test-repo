package com.velos.eres.service.studyAgent.impl;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import com.velos.eres.business.common.UthscaStudyDao;
import com.velos.eres.service.studyAgent.UthscaStudyAgent;

@Stateless
@Remote( { UthscaStudyAgent.class })
public class UthscaStudyAgentBean implements UthscaStudyAgent {

	public String getPrincipalInvestigatorEmail(String studyNumber) throws Exception {
		UthscaStudyDao studyDao = new UthscaStudyDao();
		return studyDao.getPrincipalInvestigatorEmail(studyNumber);
	}
	
	public String getEresearchUserEmail(String userName) throws Exception {
		UthscaStudyDao studyDao = new UthscaStudyDao();
		String email = studyDao.getEresearchUserEmail(userName);
		return email;
	}
	
	public String getStudyEnrollmentStatus(String studyNumber) throws Exception{
		UthscaStudyDao studyDao = new UthscaStudyDao();
		String enrollmentStatus = studyDao.getStudyEnrollmentStatus(studyNumber);
		return enrollmentStatus;
	}

	public String getStudyStatusID(String desc) throws Exception {
		UthscaStudyDao studyDao = new UthscaStudyDao();
		String studyStatusID = studyDao.getStudyStatusID(desc);
		return studyStatusID;
	}
	
	public String getStudyStatusType(String key) throws Exception{
		UthscaStudyDao studyDao = new UthscaStudyDao();
		String studyStatusType = studyDao.getStudyStatusType(key);
		return studyStatusType;
	}
	
	public String getStudyStatusTypeID(String desc) throws Exception{
		UthscaStudyDao studyDao = new UthscaStudyDao();
		String studyStatusTypeId = studyDao.getStudyStatusTypeID(desc);
		return studyStatusTypeId;
	}
	public String getCurrentStudyStatusId() throws Exception{
		UthscaStudyDao studyDao = new UthscaStudyDao();
		String currentStatusID = studyDao.getCurrentStudyStatusId();
		return currentStatusID;
	}
	public String getSiteID(String siteName) throws Exception{
		UthscaStudyDao studyDao = new UthscaStudyDao();
		String siteID = studyDao.getSiteID(siteName);
		return siteID;
	}
	public String updateStudyTitle(String protocolTitle, int studyId, String ipAdd, String modifiedBy) throws Exception{
		UthscaStudyDao studyDao = new UthscaStudyDao();
		String status = studyDao.updateStudyTitle(protocolTitle,  studyId,  ipAdd,  modifiedBy);
		return status;
	}
	public String getPIName(String studyNumber) throws Exception {
		UthscaStudyDao studyDao = new UthscaStudyDao();
		return studyDao.getPIName(studyNumber);
	}
	public String getEresearchUserName(String logonName) throws Exception {
		UthscaStudyDao studyDao = new UthscaStudyDao();
		String userName = studyDao.getEresearchUserName(logonName);
		return userName;
	}
}
