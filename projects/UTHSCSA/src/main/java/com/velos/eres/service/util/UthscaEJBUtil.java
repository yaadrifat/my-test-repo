package com.velos.eres.service.util;


import javax.naming.InitialContext;
import com.velos.eres.service.studyAgent.UthscaStudyAgent;

public class UthscaEJBUtil {
	
    public static UthscaStudyAgent getUthscaStudyAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            // System.out.println((String)initial.lookup("SiteAgentBean"));
            return (UthscaStudyAgent) initial.lookup(UthscaJNDINames.UthscaStudyAgentHome);
        } catch (Exception e) {
            Rlog.fatal("common", "gen ex=" + e);
            return null;
        }
    }
    
}
