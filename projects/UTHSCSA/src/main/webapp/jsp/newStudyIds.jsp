<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!-- saved from url=(0022)http://internet.e-mail -->
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder"%><%--rajasekhar--%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML><HEAD><TITLE><%=LC.L_More_StdDets%><%--More <%=LC.Std_Study%> Details*****--%></TITLE>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<%-- rajasekhar starts --%>
<script src="js/jquery/jquery-1.4.4.js"></script>
<script src="js/jquery/jquery-ui.js"></script>
<link rel="stylesheet" href="styles/jquery-ui.css">
<style type="text/css">

.ut-button{
   background: #AA202E;
   padding: 5px 8px;
   color: #FFFFFF;
   text-decoration: none;
   vertical-align: middle;
   position: relative;
   margin-right: .1em; 
   cursor: pointer;
   text-align: center;
   border: none; 
   font-family: Arial,Helvetica,sans-serif;
   font-size: 12px;
}
.ut-button:hover {
   background: #AA202E;
}
.ut-button:active {
   background: #AA202E;
}
.ut-button:focus {
   background: #AA202E;
}
</style>
<%-- rajasekhar ends --%>
</HEAD>

<script>
<%--rajasekhar starts--%>
jQuery(document).ready(function(){
	getListByPI();
});

function updateById(){
	var b=true;
	var IRBNumber = jQuery('#IRBNumber').val();
	var studyNo = jQuery('#studyNumber').val();
	var orcaId = getORCAId(studyNo);
	if(orcaId!='' && orcaId!=undefined){
		jQuery.ajax({
			url : "../HomeController",
			data : {'opCode':'3','studyNumber':studyNo,'orcaId':orcaId},
			type : "post",
			dataType : "json",
			async: false,
			success : function(data) {
				var statusCode='';
				var text='';
				var s =data;
				for (var obj in s){
					if(obj=='Response'){
						 statusCode=s.Response[0].Status["StatusCode"];
						 text=s.Response[0].Status["Text"];
					}
					else{
						statusCode=s.Result[0].Status["StatusCode"];
						text=s.Result[0].Status["Text"];
					}
				}

				// statusCode='200';
				if(statusCode=='200'){
					b=true;
				}
				else {
					displayError(statusCode,text);
					
					b=false;
				}
			}
		});
	}
	
	if(!jQuery('#IRBNumber').is('[readonly]') && IRBNumber!='' && (orcaId=='' || orcaId==undefined)){
		alert('IRB Number entered is wrong Please Enter correct IRB Number...');
		clearData();
		b = false;
	}
	return b;
}

function getORCAId(studyNo){
	var orcaId = '';
	var IRBNumber = jQuery('#IRBNumber').val();
	var logonName = jQuery('#logonName').val();
	if(IRBNumber!=''){
		if (!jQuery('#IRBNumber').is('[readonly]') ){
			var length = IRBNumber.indexOf("-");
			var protocolId = IRBNumber.substring(0,length);
			var protTitle = IRBNumber.substring(length+1,IRBNumber.length);
			jQuery.ajax({
				url : "../HomeController",
				data : {'opCode':'1','studyNumber':studyNo,'logonName':logonName},
				type : "post",
				async: false,
				dataType : "json",
				success : function(data) {
					var s =data;
					var l=s.Result.length;
					var statusCode=s.Result[0].Status["StatusCode"];
					//var statusCode=400;
					var text=s.Result[0].Status["Text"];
					if(statusCode=='200'){
						for(var i=1;i<s.Result.length;i++){
							var protocolIdVal = s.Result[i].ListByPI.ProtocolID;
							var protocolTitle =  s.Result[i].ListByPI.ProtocolTitle;
							var protocolTitle1 = protocolTitle.substring(0,protTitle.length);
							if(protocolId==protocolIdVal && protocolTitle1==protTitle){
								orcaId = s.Result[i].ListByPI.ORCA_ID;
								//var protocolDescription = s.Result[i].ListByPI.ProtocolDescription;
								var protocolTypeCode = s.Result[i].ListByPI.ProtocolTypeCode;
								var protocolTypeDescr = s.Result[i].ListByPI.ProtocolTypeDescr;
								var irbProtocolStatus = s.Result[i].ListByPI.IRBProtocolStatus;
								var irbProtocolStatusEffDate = s.Result[i].ListByPI.IRBProtocolStatusEffDate;
								var hscProtocolStatus = s.Result[i].ListByPI.HSCProtocolStatus;
								var hscProtocolStatusEffDate = s.Result[i].ListByPI.HSCProtocolStatusEffDate;
								var enrollmentStatus = s.Result[i].ListByPI.EnrollmentStatus;
								var irbExpirationDate = s.Result[i].ListByPI.IRBExpirationDate;
								var siteName = s.Result[i].ListByPI.studyOrganization.siteName;
								
								
								//var protocolTitleVal=protocolTitle.substring(0,100);
								
								
								jQuery("#ORCAID").val(orcaId);
								jQuery("#ShortIRBNumber").val(protocolIdVal);
								//jQuery("[id='Short IRB Number']").val(protocolIdVal);
								//jQuery('#ProtocolTitle').val(protocolTitleVal);
								jQuery('#ProtocolTitle').val(protocolTitle);
								//jQuery('#ProtocolDescription').val(protocolDescription);
								jQuery('#ProtocolTypeCode').val(protocolTypeCode);
								jQuery('#ProtocolTypeDescr').val(protocolTypeDescr);
								jQuery('#IRBProtocolStatus').val(irbProtocolStatus);
								jQuery('#IRBProtocolStatusEffDate').val(irbProtocolStatusEffDate);
								jQuery('#HSCProtocolStatus').val(hscProtocolStatus);
								jQuery('#HSCProtocolStatusEffDate').val(hscProtocolStatusEffDate);
								jQuery('#IRBExpirationDate').val(irbExpirationDate);
								jQuery('#SiteName').val(siteName);
								
								
								break;
							}
						}
					}/*else if(statusCode=='403'){
						displayError(statusCode,text);
					}else if(statusCode=='500'){
						displayError(statusCode,text);
					}*/else{
						displayError(statusCode,text);
					}
				}
			});
		}
	}
return orcaId;
}

function createNewId(esign){
  var studyNumber = jQuery('#studyNumber').val();
  var studyTitle = jQuery('#studyTitle').val();
  var studySummary = jQuery('#studySummary').val();
  var studyId = jQuery('#studyId').val();
  var logonName = jQuery('#logonName').val();
		
	jQuery.ajax({
		url : "../HomeController",
		data : {'opCode':'2','studyNumber':studyNumber,'studyTitle':studyTitle,'studySummary':studySummary,'studyId':studyId,'logonName':logonName},
		type : "post",
		async: false,
		dataType : "json",
		success : function(data) {
			var s =data;
			var l=s.Result.length;
			var statusCode=s.Result[0].Status["StatusCode"];
			//var statusCode='200';
			var text=s.Result[0].Status["Text"];
	
			if(statusCode=='200'){
				var orcaid = s.Result[1].ListByPI.ORCA_ID;
				var protocolId = s.Result[1].ListByPI.ProtocolID;
				var protocolTitle = s.Result[1].ListByPI.ProtocolTitle;
				//var protocolDescription = s.Result[1].ListByPI.ProtocolDescription;
				var protocolTypeCode = s.Result[1].ListByPI.ProtocolTypeCode;
				var protocolTypeDescr = s.Result[1].ListByPI.ProtocolTypeDescr;
				var irbProtocolStatus = s.Result[1].ListByPI.IRBProtocolStatus;
				var irbProtocolStatusEffDate = s.Result[1].ListByPI.IRBProtocolStatusEffDate;
				var hscProtocolStatus = s.Result[1].ListByPI.HSCProtocolStatus;
				var hscProtocolStatusEffDate = s.Result[1].ListByPI.HSCProtocolStatusEffDate;
				var irbExpirationDate = s.Result[1].ListByPI.IRBExpirationDate;
				var siteName = s.Result[1].ListByPI.studyOrganization.siteName;
				
				
				protocolTitle=decodeURIComponent( protocolTitle.replace(/\+/g, '%20') );
				//protocolDescription=decodeURIComponent( protocolDescription.replace(/\+/g, '%20') );
				
			
				
				var result = protocolId+'-'+protocolTitle;
				var irbNumber = result.substring(0,25);
				//var protocolTitleVal=protocolTitle.substring(0,90);
				//protocolDescription = protocolDescription.substring(0,90);
				
				jQuery('#IRBNumber').val(irbNumber);
				jQuery("#ORCAID").val(orcaid);
				jQuery("#ShortIRBNumber").val(protocolId);
				//jQuery('#ProtocolTitle').val(protocolTitleVal);
				jQuery('#ProtocolTitle').val(protocolTitle);
				//jQuery('#ProtocolDescription').val(protocolDescription);
				jQuery('#ProtocolTypeCode').val(protocolTypeCode);
				jQuery('#ProtocolTypeDescr').val(protocolTypeDescr);
				jQuery('#IRBProtocolStatus').val(irbProtocolStatus);
				jQuery('#IRBProtocolStatusEffDate').val(irbProtocolStatusEffDate);
				jQuery('#IRBExpirationDate').val(irbExpirationDate);
				jQuery('#HSCProtocolStatus').val(hscProtocolStatus);
				jQuery('#HSCProtocolStatusEffDate').val(hscProtocolStatusEffDate);
				jQuery('#SiteName').val(siteName);
				
				
				
				jQuery('#IRBNumber').attr('readonly', true);
				//rk starts
				jQuery('#method').val('createnew');
				document.newstudyid.eSign.value=esign;
				//rk ends
				jQuery('#newstudyidform').submit();		
				
				
				//alert('New ORCA ID is received');
			}/*else if(statusCode=='500'){
		 		displayError(statusCode,text);
		
			}*/else{
		 		displayError(statusCode,text);
			}
		}
	});
	
}

function reqModification(){
	var studyNumber = jQuery('#studyNumber').val();
	var studyTitle = jQuery('#studyTitle').val();
	var orcaId = jQuery('#ORCAID').val();
	var irbNumber = jQuery('#IRBNumber').val();
	var logonName = jQuery('#logonName').val();
		
	if (confirm("Are you sure you want to submit!") == true) {
		jQuery.ajax({
			url : "../HomeController",
			data : {'opCode':'4','studyNumber':studyNumber,'studyTitle':studyTitle,'orcaId':orcaId,'irbNumber':irbNumber,'logonName':logonName},
			type : "post",
			async: false,
			dataType : "json",
			success : function(data) {
				 var msg = data.Msg;
				// alert(msg);
				 if(msg=='success'){
					 alert('Mail sent successfully');
				 }else{
					 alert('Mail send failed');
				 }
			}
		});
	}
}

function getListByPI(){
	var studyNo = jQuery('#studyNumber').val();
	var resArr = [];
	var IRBNumber = jQuery('#IRBNumber').val();
		jQuery('#ORCAID').attr('readonly', true);
		jQuery('#ShortIRBNumber').attr('readonly', true);
		//jQuery('#ProtocolTitle').attr('readonly', true);
		//jQuery('#ProtocolDescription').attr('readonly', true);
		jQuery('#ProtocolTypeCode').attr('readonly', true);
		jQuery('#ProtocolTypeDescr').attr('readonly', true);
	
	var logonName = jQuery('#logonName').val();
	if(IRBNumber==null || IRBNumber==''){
		jQuery.ajax({
			url : "../HomeController",
			data : {'opCode':'1','studyNumber':studyNo,'logonName':logonName},
			type : "post",
			dataType : "json",
			success : function(data) {
				var s =data;
				var l=s.Result.length;
				var statusCode=s.Result[0].Status["StatusCode"];
				//var statusCode='403';
				var text=s.Result[0].Status["Text"];
				if(statusCode=='200'){
					for(var i=1;i<s.Result.length;i++){
						var id = s.Result[i].ListByPI.ProtocolID;
						var desc = s.Result[i].ListByPI.ProtocolTitle;
						var result = id+'-'+desc;
						resArr[i-1] = result.substring(0,25);
					}
					
					jQuery("#IRBNumber").autocomplete({     
						source :resArr
					});
				}/*else if(statusCode=='403'){
					displayError(statusCode,text);
				}else if(statusCode=='500'){
					displayError(statusCode,text);
				}*/else{
					displayError(statusCode,text);
				}
			}
		});
	}
	
}
function displayError(statusCode,text){
	jQuery( "<div>Error:"+statusCode+"-"+text+"</div>" ).dialog({
		autoOpen: true, 
		modal:true,
		resizable:false, 
		closeOnEscape: false,
		draggable:false,
		buttons: {
			Continue: function() {
				//jQuery('#IRBNumber').val('');
				jQuery(this).dialog("close");
			}
		 },
		title: "ORCA Web Service Error",
		position: {
			my: "center",
			at: "center",
			of:window
		},
		close: clearData
});
}
function clearData(){
	jQuery('#IRBNumber').val('');
	jQuery("#ORCAID").val('');
	jQuery("#ShortIRBNumber").val('');
	jQuery('#ProtocolTitle').val('');
	//jQuery('#ProtocolDescription').val('');
	jQuery('#ProtocolTypeCode').val('');
	jQuery('#ProtocolTypeDescr').val('');
	jQuery('#IRBProtocolStatus').val('');
	jQuery('#IRBProtocolStatusEffDate').val('');
	jQuery('#HSCProtocolStatus').val('');
	jQuery('#HSCProtocolStatusEffDate').val('');
	jQuery('#IRBExpirationDate').val('');
}
<%--rajasekhar ends--%>


function setValue(formobj,iElementId,cbcount) {
	var chkFld = formobj['alternateId'+iElementId];
	var value = chkFld.value;
	if (value=="Y") {
		chkFld.value="N";
		chkFld.checked=false;
	} else if ((value.length==0) || (value=="N"))  {
		chkFld.value="Y";
		chkFld.checked=true;
	} else { // <== there is some junk data in the DB column already
		chkFld.value="Y";
		chkFld.checked=true;
	}
}

function setDD(formobj)
{
	var optvalue=formobj.ddlist.value;
	if (optvalue.length>0)
	{
		var arrayofStrings=optvalue.split("||");
		if (arrayofStrings.length>1) {
			for (var j=0;j<arrayofStrings.length;j++)
			{
				var ddStr=arrayofStrings[j];
				var arrayofDD=ddStr.split(":");
				var ddcount=arrayofDD[0];
				var selvalue=arrayofDD[1];
				var ddFld = formobj['alternateId'+ddcount];
				if (ddFld && ddFld.options) {
					var opt = ddFld.options;
					for (var i=0;i<opt.length;i++){
						if (opt[i].value==selvalue){
							ddFld.selectedIndex=i ;
						}
					}
				}
			}
		} else {
			var ddStr=arrayofStrings[0];
			var arrayofDD=ddStr.split(":");
			var ddcount=arrayofDD[0];
			var selvalue=arrayofDD[1];
			var ddFld = formobj['alternateId'+ddcount];
			if (ddFld && ddFld.options) {
			 	var opt = ddFld.options; 
				if (opt == undefined)
			    	opt = ddFld.options; 
				for (var i=0;i<opt.length;i++){
					if (opt[i].value==selvalue){
						ddFld.selectedIndex=i ;
					}
				}
			}
		}// end else
	}//optvalue.length>0	
}


function validate(formobj) {

 	 if (!(validate_col('e-Signature',formobj.eSign))) return false

     if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
	   }
//rajasekhar starts
	//return true;
 var method=jQuery('#method').val();
			
			if(method!='createnew' || method==null || method==''){
				if(document.getElementById("eSignMessage").innerHTML != "<%=LC.L_Valid_Esign%>") {
				alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
				formobj.eSign.focus();
				return false;
				}
			}
			
			var isSubmit = updateById();
			if(isSubmit){
				return true;
			}else{
				return false;
			}
			//rajasekhar ends
			
}


</script>


<%
int ienet = 2;

String agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1)
     ienet = 0; //IE
    else
	ienet = 1;
	if(ienet == 0) {
%>
<body style="overflow:scroll;" onLoad="setDD(document.newstudyid)">
<%
	} else {
%>
<body onLoad="setDD(document.newstudyid)">
<%
	}
%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%>
<%@ page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.StringUtil"%>
<%@ page import="com.velos.eres.service.util.MC"%>
<!-- rk starts -->
<jsp:include page="eSignDialogBox.jsp"></jsp:include>
<!-- rk ends -->

<jsp:useBean id="altId" scope="request" class="com.velos.eres.web.studyId.StudyIdJB"/>
<jsp:include page="include.jsp" flush="true"/>
<DIV class="popDefault" id="div1">

<%
	HttpSession tSession = request.getSession(true);
	 if (sessionmaint.isValidSession(tSession))
	{
	 %>
	<jsp:include page="sessionlogging.jsp" flush="true"/>

<%
	 String defUserGroup = (String) tSession.getAttribute("defUserGroup");

	 String accountId = (String) tSession.getValue("accountId");
	 String studyId = request.getParameter("studyId");
	 int studyRights = EJBUtil.stringToNum(request.getParameter("studyRights"));
	 
	  //rajasekhar starts
	 String loginName =(String) tSession.getValue("loginname");
	 String studyNo = request.getParameter("studyNumber");
	 String studyTitle = request.getParameter("studyTitle");
	 String studySummary = request.getParameter("studySummary");
	 //studySummary=URLDecoder.decode(studySummary,"UTF-8");
	 // System.out.println("Decoded studySummary=="+studySummary);
	 //rajasekhar ends

	 //get study ids
	 StudyIdDao sidDao = new StudyIdDao();
	 sidDao = altId.getStudyIds(EJBUtil.stringToNum(studyId),defUserGroup);

 	 ArrayList studyIdType  = new ArrayList();
	 ArrayList studyIdTypesDesc = new ArrayList();
	 ArrayList id = new ArrayList();
	 ArrayList alternateId = new ArrayList();
 	 ArrayList recordType = new ArrayList();
	 ArrayList dispType=new ArrayList();
	 ArrayList dispData=new ArrayList();

	 id = sidDao.getId();
	 studyIdType =  sidDao.getStudyIdType();
	 studyIdTypesDesc = sidDao.getStudyIdTypesDesc();
	 alternateId = sidDao.getAlternateId();
	 recordType = sidDao.getRecordType ();
	 dispType=sidDao.getDispType();
	 dispData=sidDao.getDispData();

	  //rajasekhar starts
	 String strStudyId="";
	 //rajasekhar ends
	 String strStudyIdTypesDesc ;
 	 String strAlternateId ;
  	 String strRecordType ;
  	 Integer intStudyIdType;
   	 Integer intId;
	 String disptype="";
	 String dispdata="";
	 String ddStr="";


%>
<Form  name="newstudyid" id ="newstudyidform" action="updatestudyid.jsp" method="post" onSubmit="if (validate(document.newstudyid)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}" >
<BR>
<P class = "sectionHeadings">
<%=LC.L_More_StdDets%><%--More <%=LC.Std_Study%> Details*****--%>
</P>
<table width="100%">
<!-- rajasekhar starts -->
<input type="hidden" id="studyNumber" name="studyNumber" value="<%=studyNo%>"/>
<input type="hidden" id="studyTitle" name="studyTitle" value="<%=studyTitle%>"/>
<input type="hidden" id="studySummary" name="studySummary" value="<%=studySummary%>"/>
<input type="hidden" id="studyId" name="studyId" value="<%=studyId%>"/>
<input type="hidden" id="logonName" name="logonName" value="<%=loginName%>"/>
<input type="hidden" id="ProtocolTitle" name="ProtocolTitle" value=""/>
<input type="hidden" id="IRBProtocolStatus" name="IRBProtocolStatus" value=""/>
<input type="hidden" id="IRBProtocolStatusEffDate" name="IRBProtocolStatusEffDate" value=""/>
<input type="hidden" id="HSCProtocolStatus" name="HSCProtocolStatus" value=""/>
<input type="hidden" id="HSCProtocolStatusEffDate" name="HSCProtocolStatusEffDate" value=""/>
<input type="hidden" id="IRBExpirationDate" name="IRBExpirationDate" value=""/>
<input type="hidden" id="SiteName" name="SiteName" value=""/>
<!-- rk starts -->
<input type="hidden" id="method" name="method" value=""/>
<!-- rk ends -->
<!-- rajasekhar ends -->
<!--<tr>
   <th width="45%" >Other <%=LC.Std_Study%> ID Type</th>
   <th width="55%" >Other <%=LC.Std_Study%> ID</th>
</tr> -->
<%
	int counter = 0,cbcount=0;
	for (counter = 0; counter <= studyIdType.size() -1 ; counter++)
		{
				strStudyIdTypesDesc = (String) studyIdTypesDesc.get(counter);
			//rajasekhar starts
			strStudyId = (String) studyIdTypesDesc.get(counter);
			if (strStudyId.trim().equals("IRB Number")) {
			strStudyId = "IRBNumber";
			}
			if (strStudyId.trim().equals("ORCA ID")) {
			strStudyId = "ORCAID";
			}
			if (strStudyId.trim().equals("Short IRB Number")) {
			strStudyId = "ShortIRBNumber";
			}
			if (strStudyId.trim().equals("Protocol Type Code")) {
			strStudyId = "ProtocolTypeCode";
			}
			if (strStudyId.trim().equals("Protocol Type Descr")) {
			strStudyId = "ProtocolTypeDescr";
			}
			//rajasekhar ends
 	 			strAlternateId = (String) alternateId.get(counter);
 	 			intStudyIdType = (Integer) studyIdType.get(counter) ; 

 	 			disptype=(String) dispType.get(counter);
				dispdata=(String) dispData.get(counter);
				if (disptype==null) disptype="";
 	 			disptype=disptype.toLowerCase();

				if (dispdata==null) dispdata="";
				if (disptype.equals("dropdown")) {
					if (null != dispdata){
						dispdata = StringUtil.replaceAll(dispdata, "alternateId", "alternateId"+intStudyIdType);
					}
				}

 	 			if (strAlternateId == null)
 	 				strAlternateId = "";

  	 			strRecordType = (String) recordType.get(counter);
   	 			intId = (Integer) id.get(counter) ;

	%>

	<tr>
		     <!-- rajasekhar starts -->
			<!-- <td class=tdDefault width="45%" > -->
			<td class=tdDefault width="40%">
			<!-- rajasekhar ends -->
				<%= strStudyIdTypesDesc %>
			 </td>
			<!-- rajasekhar starts -->
			<!-- <td class=tdDefault width="55%" > -->
			<td class=tdDefault width="30%">
			<!-- rajasekhar ends -->
				<% if (disptype.equals("chkbox")){
				cbcount=cbcount+1;
				%>
				<input type = "hidden" name = "alternateId<%=intStudyIdType%>" value = "<%=strAlternateId.trim()%>" size = "25" maxlength = "100" >
						<%	 if ((strAlternateId.trim()).equals("Y")){%>
				 <input type="checkbox" name="alternate" value="<%=strAlternateId.trim()%>" onClick="setValue(document.newstudyid,<%=intStudyIdType%>,<%=cbcount%>)" checked>
				  <% }else{%>
				  <input type="checkbox"  name="alternate" value="<%=strAlternateId.trim()%>" onClick="setValue(document.newstudyid,<%=intStudyIdType%>,<%=cbcount%>)">

				<%}} else if (disptype.equals("dropdown")) {
				if (ddStr.length()==0) ddStr=(intStudyIdType)+":"+strAlternateId;
					else ddStr=ddStr+"||"+(intStudyIdType)+":"+strAlternateId;
				%>
				  <%=dispdata%>
				<%} else if (disptype.equals("splfld")) {%> 
				  	<%=dispdata%>
				<%}else if (disptype.equals("date")) {%>
					<input name="alternateId<%=intStudyIdType%>" type="text" class="datefield" size="10" readOnly  value="<%=strAlternateId.trim()%>">
				<%}else if (disptype.equals("readonly-input")){%>
					<input type="text" class='readonly-input' readonly name="alternateId<%=intStudyIdType%>" value="<%=strAlternateId.trim()%>" size="25" maxlength="100"/>
				<%}else{%>
					 <!-- rajasekhar starts -->
			<%-- 	<input type = "text" name = "alternateId<%=intStudyIdType%>" value = "<%=strAlternateId.trim()%>" size = "25" maxlength = "100" >
					</input> <%
			--%>
			<%
				if (strStudyId != null && strStudyId.trim().equals("IRBNumber") && strAlternateId != null && !strAlternateId.trim().equals("")) {
			%>
					<input type="text" class='readonly-input' readonly id="<%=strStudyId%>" name="alternateId<%=intStudyIdType%>" value="<%=strAlternateId.trim()%>" size="25" maxlength="100">
					</input> <%
				} else {
%>
					<input type="text" id="<%=strStudyId%>" name="alternateId<%=intStudyIdType%>" value="<%=strAlternateId.trim()%>" size="25" maxlength="100">
					</input> <%
				}
%> <%
	}
//rajasekhar ends
%>
				<input type = "hidden" name = "recordType<%=intStudyIdType%>" value = "<%=strRecordType %>" >
				<input type = "hidden" name = "id<%=intStudyIdType%>" value = "<%=intId%>" >
				<input type = "hidden" name = "studyIdType" value = "<%=intStudyIdType%>" >
				<input type = "hidden" name = "studyId" value = "<%=studyId%>" >
			 </td>
 <!-- rajasekhar starts -->
			<%
				if (strStudyId != null && strStudyId.trim().equals("IRBNumber")) {
						if (strAlternateId == null || strAlternateId.trim().equals("")) {
			%>
						<td><input type="button" id="createNew" class="ut-button" value="Create New"/>
						</td>
			<%
						} else {
			%>
							<td>
								<input type="button" id="requestModification" class="ut-button" value="Disconnect IRB Number" onclick="reqModification()"/></td>
			<%
						}
				} else {
			%>
			<td></td>
			<%
				}
			%>
			<!-- rajasekhar ends -->
	</tr>

	<%
		}
	%>
	<input type="hidden" name="cbcount" value=<%=cbcount%>>
	<input type="hidden" name="ddlist" value="<%=ddStr%>">
	<%if (studyRights >= 6){%>
	 </table>
	<br>

	<jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="newstudyidform"/>
		<jsp:param name="showDiscard" value="N"/>
	</jsp:include>


<%}%>
	</form>
<%}else{%>
<jsp:include page="timeout_childwindow.jsp" flush="true"/>
<%}%>
</div>
<div class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</body>
</html>
