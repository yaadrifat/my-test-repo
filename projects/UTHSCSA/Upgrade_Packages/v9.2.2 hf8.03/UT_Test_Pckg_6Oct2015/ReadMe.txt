/////*********UTHSCSA ORCA IRB Interface Deployment Package for v9.2.2 build #693f.08 Oct 6,2015**********////
======================================================================================================
Steps for customizations deployment:
1. Stop the Velos eResearch application server.

2. Copy the files mentioned below at correct locations.

	a.Properties File
		1.email.properties(1 new)
		This  file should be copied at the following location-
		eResearch_jboss510/conf
		
	b.New Class Files(8 new class Files,1 existing)	
         
		1.HomeController.class
		eResearch_jboss510/server/eresearch/deploy/velos.ear/velos-main.jar/com/velos/controller

		2.UthscaJNDINames.class
		eResearch_jboss510/server/eresearch/deploy/velos.ear/velos-main.jar/com/velos/eres/service/util

		3.UthscaEJBUtil.class
		eResearch_jboss510/server/eresearch/deploy/velos.ear/velos-main.jar/com/velos/eres/service/util
				
		4.UthscaStudyJB.class
		eResearch_jboss510/server/eresearch/deploy/velos.ear/velos-main.jar/com/velos/eres/web/study

		5.UthscaStudyAgent.class
		eResearch_jboss510/server/eresearch/deploy/velos.ear/velos-ejb-eres.jar/com/velos/eres/service/studyAgent

		6.UthscaStudyAgentBean.class
		eResearch_jboss510/server/eresearch/deploy/velos.ear/velos-ejb-eres.jar/com/velos/eres/service/studyAgent/impl

		7.UthscaStudyDao.class
		eResearch_jboss510/server/eresearch/deploy/velos.ear/velos-ejb-eres.jar/com/velos/eres/business/common

		8.HomeController$1.class
		eResearch_jboss510/server/eresearch/deploy/velos.ear/velos-main.jar/com/velos/controller

		9.StudySummary.class(Existing and it should be replaced in 2 locations)
			
		eResearch_jboss510/server/eresearch/deploy/velos.ear/velos-common.jar/com/velos/services/model
		eResearch_jboss510/server/eresearch/deploy/velos.ear/velos-ejb-eres.jar/com/velos/services/model


	c.JSP Files(3 Existing,1 New)

		1.newStudyIds.jsp
		2.study.jsp
		3.updatestudyid.jsp
		4.eSignDialogBox.jsp(New)
			
		These 4 JSP files should be replaced at the following location-
		eResearch_jboss510/server/eresearch/deploy/velos.ear/velos.war/jsp

	d.web.xml(Existing)	
		This Xml file should be replaced at the following location-
		eResearch_jboss510/server/eresearch/deploy/velos.ear/velos.war/WEB-INF

	e.Js Files(1 New,2 Existing)
		1.jquery-ui.js(New)
		This Js file should be copied at the following location-
		eResearch_jboss510/server/eresearch/deploy/velos.ear/velos.war/jsp/js/jquery

		2.jquery.ui.dialog.js
		This Js file should be replaced at the following location-
		eResearch_jboss510/server/eresearch/deploy/velos.ear/velos.war/jsp/js/jquery/ui

		3.ajaxengine.js
			eResearch_jboss510/server/eresearch/deploy/velos.ear/velos.war/jsp/js/velos/ajaxengine.js

	f.CSS file(New)
		1.jquery-ui.css
		This css file should be copied at the following location-
		eResearch_jboss510/server/eresearch/deploy/velos.ear/velos.war/jsp/styles

3. Clear the contents for the folders tmp and work under
	eResearch_jboss510/server/eresearch

4. DB Script files:
	
	Eres :
		1.morestudydetails.sql
	Run the script file in eres schema.
5. Start the application server.

=============================================================================================================
