<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>


    <meta http-equiv="content-type" content="text/html; charset=utf-8">
<title></title>

<style type="text/css">
/*margin and padding on body element
  can introduce errors in determining
  element position and are not recommended;
  we turn them off as a foundation for YUI
  CSS treatments. */
body {
	margin:0;
	padding:0;
}
</style>

<link rel="stylesheet" type="text/css" href="js/yui/build/container/assets/skins/sam/button.css" />
<link rel="stylesheet" type="text/css" href="js/yui/build/container/assets/skins/sam/container.css"> 

<script type="text/javascript" src="js/yui/build/yahoo-dom-event/yahoo-dom-event.js"></script>
<script type="text/javascript" src="js/yui/build/connection/connection-min.js"></script>
<script type="text/javascript" src="js/yui/build/element/element-min.js"></script>
<script type="text/javascript" src="js/yui/build/button/button-min.js"></script>
<script type="text/javascript" src="js/yui/build/dragdrop/dragdrop-min.js"></script>
<script type="text/javascript" src="js/yui/build/container/container-min.js"></script>

<!--begin custom header content for this example-->
<script type="text/javascript">
document.documentElement.className = "yui-pe";
</script>

<style type="text/css">
#example {
    height:30em;
}

label { 
    display:block;
    float:left;
    width:45%;
    clear:left;
}

.clear {
    clear:both;
}

#resp {
    margin:10px;
    padding:5px;
    border:1px solid #ccc;
    background:#fff;
}

#resp li {
    font-family:monospace
}

.yui-pe .yui-pe-content {
    display:none;
}
</style>

<!--end custom header content for this example-->

</head>

<body class="yui-skin-sam">

<script>
YAHOO.namespace("example.container");

YAHOO.util.Event.onDOMReady(function () {
	
	// Define various event handlers for Dialog
	var handleSubmit = function() {
		var msg = jQuery('#eSignMessage1').text();
		
		if(msg==''){
			alert('Please enter e-Signature');
			return false;
		}
		
		if(msg!='Valid e-Sign'){
			alert('Incorrect e-Signature. Please enter again.');
			return false;
		} 
		//this.submit();
		var eSign = jQuery('#eSign1').val();
		createNewId(eSign);
	};
	var handleCancel = function() {
		this.cancel();
	};
	var handleSuccess = function(o) {
		//var eSign = jQuery('#eSign1').val();
		//createNewId(eSign);
	};
	var handleFailure = function(o) {
		//alert("Submission failed: " + o.status);
	};

    // Remove progressively enhanced content class, just before creating the module
    YAHOO.util.Dom.removeClass("dialog1", "yui-pe-content");

	// Instantiate the Dialog
	YAHOO.example.container.dialog1 = new YAHOO.widget.Dialog("dialog1", 
							{ width : "35em",
							  fixedcenter : true,
							  visible : false, 
							  constraintoviewport : true,
							  buttons : [ { text:"Save", handler:handleSubmit },
								      { text:"Cancel", handler:handleCancel } ]
							});

	// Wire up the success and failure handlers
	YAHOO.example.container.dialog1.callback = { success: handleSuccess,
						     failure: handleFailure };
	
	// Render the Dialog
	YAHOO.example.container.dialog1.render();

	YAHOO.util.Event.addListener("createNew", "click", YAHOO.example.container.dialog1.show, YAHOO.example.container.dialog1, true);
});

</script>

<div id="dialog1" class="yui-pe-content">
<div class="hd">Confirm Create New ORCA ID</div>
<div class="bd">
<p align="center">
	Do you want to proceed with Create New ORCA ID?<br/>
	Please enter eSignature and click Save to confirm.
</p> 
	
<form name="eSignForm" id="eSignForm" method="POST" action="">
<table>
	<tbody>
		<tr>
		<td width="10%"></td>
		<td width="45%" align="right"><span id="eSignMessage1" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;"></span></td>
		<td width="15%" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;">e-Signature<font class="Mandatory">*</font>&nbsp;</td>
		<td><input autocomplete="off" name="eSign1" id="eSign1" maxlength="8" onkeyup="ajaxvalidate('misc:'+this.id,4,'eSignMessage1','Valid e-Sign','Invalid e-Sign','sessUserId')" type="password">&nbsp;</td>
		</tr>
	</tbody>
</table>
</form>
</div>
</div>
</body>
</html>
