/////*********UTHSCSA ORCA IRB Interface Staging Deployment Package for v9.2.2 build #693f.08 Oct 27,2015**********////
======================================================================================================
Steps for customizations deployment:
1. Stop the Velos eResearch application server.

2. Copy the files by getting from the package deployed in Test env.
   
3. Replace email.properties file.

4. Clear the contents for the folders tmp and work under
	eResearch_jboss510/server/eresearch

5. Start the application server.

=============================================================================================================
