<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="include.jsp" flush="true"/>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_StdGtSumm%><%--Study >> Summary*****--%></title>
<%!
String TAreaDisSiteFlag = "TAreaDisSite_ON";
%>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.StringUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB" %>
<%@ page import="java.net.URLEncoder"%> <%--rajasekhar--%>
<!-- KM : import the objects for customized field -->
<%@ page import="com.velos.eres.web.user.ConfigFacade,com.velos.eres.web.user.ConfigObject, com.velos.eres.web.user.ConfigDetailsObject,com.velos.eres.business.common.CommonDAO"%>
<%@ page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC,com.velos.eres.business.studyNIHGrant.impl.StudyNIHGrantBean" %>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="studyAssocB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="accountB" scope="page" class="com.velos.eres.web.account.AccountJB" />
<jsp:useBean id="studyNIHGrantB"  scope="request" class="com.velos.eres.web.studyNIHGrant.StudyNIHGrantJB"/>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!--<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<META HTTP-EQUIV="Expires" CONTENT="-1">-->
</head>
<SCRIPT language="Javascript1.2">
	var screenWidth = screen.width;
	var screenHeight = screen.height;
	function openLSampleSize(studyId, mode)	{
		if (mode == 'N') {
			alert("<%=MC.M_SbmtStdFirst_ForSampleSize%>");/*alert("Please Submit the Study first for entering Local Sample Size");*****/
			return false;
		}else{
			windowName = window.open("LSampleSizePopUp.jsp?studyId="+studyId,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=600,height=500 top=50,left=100 0, ");
			windowName.focus();
		}
	}
	//Added by Manimaran for September Enhancement2 to select the ICD codes.
	function openWinICD(codestr,lkpId,lkpColumn){	    windowName=window.open("getlookup.jsp?viewId="+lkpId+"&form=study&dfilter=&keyword="+codestr+"|"+lkpColumn+"","Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=700,height=550 top=100,left=100");
	    windowName.focus();
	}
	function openLookup(acc){
	 dfilter = "study";
	 windowName = window.open("getlookup.jsp?viewId=&viewName=Study Summary&form=study&dfilter="+dfilter+"&keyword=studyAssocNum|VELSTUDY_NUM~studyAssoc|VELPK","Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=700,height=550 top=100,left=100 0, ")
	 windowName.focus();
	}


	function openLookupSpnsr(formobj) {
		formobj.target="Lookup";
		formobj.method="post";
		formobj.action="multilookup.jsp?viewId=6011&form=study&seperator=;"+
	                  "&dfilter=codelst_type='sponsor'&keyword=sponsor1|CODEDESC~sponsorpk|CODEPK|[VELHIDE]&maxselect=1";

		formWin =open('donotdelete.html','Lookup','resizable=1,status=0, width=850,height=550 top=100,left=100,menubar=no,scrollbars=1');

		if (formWin && !formWin.closed) formWin.focus();
		formobj.submit();
		formobj.target="_self";
		formobj.action="updateNewStudy.jsp";
		void(0);
	}



	function selectDisease(formobj){
		var tArea = formobj.studyTArea.value;
		var name=formobj.disSitename.value;
		var id=formobj.disSiteid.value;
		windowName = window.open("selectdiseasesite.jsp?from=study&datafld=disSiteid&dispfld=disSitename&names="+name +"&ids="+id+"&tArea="+tArea,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=800,height=600 top=50,left=100 0, ")
		windowName.focus();
	}

	function setValue(formobj){
	value=formobj.author.value;
	if (value=="Y"){
		formobj.cbauth.checked=false;
		formobj.author.value="";
	}
	if ((value.length==0) || (value=="N")){
		formobj.cbauth.checked=true;
		formobj.author.value="Y";
	}

}


	function  validate(formobj,autogen,accId) {

		 if (document.getElementById('mandstudyent')) { //KM-3655
			if (!(validate_col('Study Entered By',formobj.dataManagerName))) return false
		 }

		 var mode = formobj.mode.value;

		 //KM: Modified for Edit mode.
		 /* Changes By Sudhir on 05-Apr-2012 for Bug #9121*/
		 /* Bug#11005 24-Aug-2012 -Sudhir*/
		 if (autogen != 1 || mode == 'M'){  //KM-0702		 
		   if (document.getElementById('mandsnumber')) {
		  	 if (!(validate_col('Study Number',formobj.studyNumber))) return false
		   }
         }
		//Modified to not allowed the special character "," in Study Number
		 if(formobj.studyNumber.value.indexOf(",") != -1 ){
			var paramArray = [","];
			alert(getLocalizedMessageString("M_SplCharNotAllowed", paramArray));/*alert("Special Character(,) not allowed for this Field");*****/
		 	formobj.studyNumber.focus();
			return false;
		 }   
		 /* Changes By Sudhir on 05-Apr-2012 for Bug #9121*/
		 if (document.getElementById('mandtarea')) {
			if (!(validate_col('Therapeutic Area',formobj.studyTArea))) return false
		 }

		 if (document.getElementById('mandphase')) {
			if (!(validate_col('Study Phase',formobj.studyPhase))) return false
		 }

		 if (document.getElementById('mandtitle')) {
		     if (!(validate_col('Title',formobj.studyTitle))) return false
		 }


		if (!(validate_col('e-Sign',formobj.eSign))) return false

		/*if (!(isInteger(formobj.studySize.value))){
	 		alert("Invalid Study Size");
			formobj.studySize.focus();
            return false;
		}*/

		/////KM


		 if (document.getElementById('pgcustomstudyentby')) {
				 if (!(validate_col('Study Entered By',formobj.dataManagerName))) return false
		 }


		 //Prinicipal Investigator mandatory/non-mand implementation
		 if (document.getElementById('pgcustompi')) {
				 if (!(validate_col('pinvestigator',formobj.prinInvName))) return false
		 }

		 //if other mandatory/non-mand implementation
		 if (document.getElementById('pgcustomifotheruser')) {
			 if (!(validate_col('prinvOther',formobj.prinvOther))) return false
		 }


		 if (document.getElementById('pgcustomstudycont')) {
				 if (!(validate_col('Study Contact',formobj.studycoName))) return false
		 }


		 /* Bug#11005 24-Aug-2012 -Sudhir*/
		if (autogen != 1){
	    	if (!(validate_col('Study Number',formobj.studyNumber))) return false
	    }


		 if (autogen != 1 || mode == 'M' ){ //KM:31July08
  		   if (document.getElementById('pgcustomstudynum')) {
				  if (!(validate_col('Study Number',formobj.studyNumber))) return false
		   }
	     }



		 if (document.getElementById('pgcustomstudytitle')) {
				  if (!(validate_col('Title',formobj.studyTitle))) return false
		 }



		 if (document.getElementById('pgcustomobjective')) {
				  if (!(validate_col('Objective',formobj.studyObjective))) return false
		 }


		 if (document.getElementById('pgcustomsummary')) {
				  if (!(validate_col('Summary',formobj.studySummary))) return false
		 }

		 if (document.getElementById('pgcustomagent')) {
				  if (!(validate_col('Agent',formobj.studyProduct))) return false
		 }

		 if (document.getElementById('pgcustomdivision')) {
				  if (!(validate_col('Division',formobj.studyDivision))) return false
		 }


		if (document.getElementById('pgcustomtarea')) {
				 if (!(validate_col('Therapeutic Area',formobj.studyTArea))) return false
		}


		if (document.getElementById('pgcustomdissite')) {
				 if (!(validate_col('Disease Site',formobj.disSitename))) return false
		 }


		if (document.getElementById('pgcustomspecsite')) {
				 if (!(validate_col('Specific Site1',formobj.ICDcode1))) return false
		 }

		if (document.getElementById('pgcustomspecsite')) {
				 if (!(validate_col('Specific Site2',formobj.ICDcode2))) return false
		 }

		if (document.getElementById('pgcustomnss')) {
				 if (!(validate_col('National Sample Size',formobj.nStudySize))) return false
		 }

		if (document.getElementById('pgcustomnss')) {
				 if (!(validate_col('National Sample Size',formobj.nStudySize))) return false
		 }

		if (document.getElementById('pgcustomestdt')) {
				 if (!(validate_col('Estimated Begin Date',formobj.studyEstBgnDate))) return false
		 }


		if (document.getElementById('pgcustomphase')) {
				 if (!(validate_col('Study Phase',formobj.studyPhase))) return false
		 }

		if (document.getElementById('pgcustomresearchtype')) {
				 if (!(validate_col('Research Type',formobj.studyResType))) return false
		 }

		if (document.getElementById('pgcustomscope')) {
				 if (!(validate_col('Study Scope',formobj.studyScope))) return false
		 }


		if (document.getElementById('pgcustomtype')) {
				 if (!(validate_col('Study Type',formobj.studyType))) return false
		 }

		if (document.getElementById('pgcustomstdlinkedto')) {
				 if (!(validate_col('Study LinkedTo',formobj.studyAssocNum))) return false
		 }

		if (document.getElementById('pgcustomblinding')) {
				 if (!(validate_col('Blinding',formobj.studyBlinding))) return false
		 }


		if (document.getElementById('pgcustomrandom')) {
				 if (!(validate_col('Randomization',formobj.studyRandom))) return false
		 }

		if (document.getElementById('pgcustomspname')) {
				 if (!(validate_col('Sponsor Name',formobj.sponsor))) return false
		 }

		if (document.getElementById('pgcustomspname1')) {
				 if (!(validate_col('Sponsor Name',formobj.sponsor1))) return false
		 }

		if (document.getElementById('pgcustomothsponsor')) {
				 if (!(validate_col('If other',formobj.studySponsor))) return false
		 }

		if (document.getElementById('pgcustomsponsorid')) {
				 if (!(validate_col('Sponsor ID',formobj.studySponsorIdInfo))) return false
		 }

		if (document.getElementById('pgcustomcontact')) {
				 if (!(validate_col('Contact',formobj.studyContact))) return false
		 }

		if (document.getElementById('pgcustomothinfo')) {
				 if (!(validate_col('Other Information',formobj.studyOtherInfo))) return false
		 }

		if (document.getElementById('pgcustomkeywords')) {
				 if (!(validate_col('Keywords',formobj.studyKeywrds))) return false
		 }

		if (document.getElementById('pgcustomduration')) {
				 if (!(validate_col('Study duration',formobj.studyDuration))) return false
		 }

		if (document.getElementById('pgcustomstudyncitrialidentifier')) {
			 if (!(validate_col('NCI Trial Identifier',formobj.NCITrialIdentifier))) return false
	 	}

		if (document.getElementById('pgcustomstudynctnumber')) {
			 if (!(validate_col('NCT Number',formobj.nctNumber))) return false
	 }
		 
		//Added by Yogendra to fix Bug#10071.
		if (document.getElementById('prinvIfOther')){
			if(formobj.prinvOther.disabled != true) {
				if(document.getElementById('prinvIfOther').value.length>100){
					alert("<%=MC.M_PIOther_Size%>");/*alert("Maximum limit of characters that can be entered in the principal investigator IF OTHER field is 100");*****/
					formobj.prinvOther.focus();
					return false;
			 	}
		 	}
		}
		////KM

		//Added by Manimaran to fix the Bug.2658
		if (formobj.studyNumber) {
  		   if (checkChar(formobj.studyNumber.value,'<') ) {
		      //alert("Special character '<' is not allowed in Study Number");
			  alert("<%=MC.M_SplChar_LtNtAlwdStd%>");/*alert("Special character '<' is not allowed in <%=LC.Std_Study%> Number");*****/
	          formobj.studyNumber.focus();
		   return false;
	    }
		}
		
		if(!jQueryAjaxStudyNum(formobj,accId)){
			alert(M_StdNumExst);
			formobj.studyNumber.focus();
			return false;
		}
				
		if (!(isInteger(formobj.nStudySize.value))){
	 		alert("<%=LC.L_Invalid_SampleSize%>");/*alert("Invalid Sample Size");*****/
			formobj.nStudySize.focus();
            return false;
		}
		if (!(isInteger(formobj.studyDuration.value))){
	 		alert("<%=LC.L_Invalid_Duration%>");/*alert("Invalid Duration");*****/
			formobj.studyDuration.focus()
            return false;
		}

		//Added by Manimaran to fix the issue 3721
		if (document.getElementById('stdDuration')) {
		if ((formobj.studyDuration.value != 0) && (formobj.durationUnit.value == '')) {
			if(formobj.durationUnit.disabled != true) {
			  alert("<%=MC.M_Selc_StdDurUnit%>");/*alert("Please select <%=LC.Std_Study%> Duration unit");*****/
			  formobj.durationUnit.focus();
			  return false;
			}
		}
		}

		//Added for CTRP-22471 : Raviesh
		var nctNumVal = document.getElementById('nctNumber').value;
		if(!(nctNumVal=='' || nctNumVal==null)){
			var ar=new Array();
			var nctNumberVal = document.getElementById('nctNumber').value
			ar=nctNumberVal.split(" ");			
			var isMatch = nctNumberVal.length == 11 && nctNumberVal.substr(0, 3) == "NCT" && ar.length ==1 && !(isNaN(nctNumberVal.substr(4, 10)));
				if(!(isMatch && allSpclcharcheck(nctNumberVal,formobj.nctNumber))){
					alert("<%=MC.M_NctNumber_Format%>");
					formobj.nctNumber.focus();
					return false;
				}
		}

		if (document.getElementById('durunit')) {
		if ((formobj.durationUnit.value != '') && (formobj.studyDuration.value == '')) {
			if(formobj.studyDuration.disabled != true ) {
			  alert("<%=MC.M_Etr_StdDurVal%>");/*alert("Please enter <%=LC.Std_Study%> Duration value");*****/
			  formobj.studyDuration.focus();
			  return false;
			}
		}
		}

		//if (!(validate_col('Study Entered By',formobj.dataManagerName))) return false---Commented by Manimaran temporarily


			if(isNaN(formobj.eSign.value) == true) {
				alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
				formobj.eSign.focus();
				return false;
			}
 
		// Enhancemnet CTRP-20527  : AGodara
			if(formobj.cbIndIdeGrid != null && formobj.cbIndIdeGrid.checked != null && formobj.cbIndIdeGrid.checked){
				if(!validateIndIdeGrid(formobj)){
					return false;
				}
			}
		//Ak:Added for enhancemnet CTRP-20527_1
			if(formobj.nihGrantChk.checked != null && formobj.nihGrantChk.checked){
				if(!validateNIHGrantGrid(formobj)){
					return false;
				}
			}

		// Bug#8098 Fixed : Raviesh
			if(document.getElementById("eSignMessage").innerHTML != "<%=LC.L_Valid_Esign%>") {
				alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
				formobj.eSign.focus();
				return false;
			}
			

	    }


	function openwin() {
		window.open("","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=600,height=450")
		;}

	<%--rajasekhar starts--%>
	<%--function openwinstudyid(studyid, mode, studyRights)--%>
	function openwinstudyid(studyid, mode, studyRights,studyNumber,studyTitle,summary)
	<%--rajasekhar ends--%>
	{
		if (mode == 'M'){
			<%--rajasekhar starts--%>
			<%--windowname=window.open("newStudyIds.jsp?studyId=" + studyid+"&studyRights="+studyRights ,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=650,height=550 top=120,left=200 0, ");--%>
			windowname=window.open("newStudyIds.jsp?studyId=" + studyid+"&studyRights="+studyRights+"&studyNumber="+studyNumber+"&studyTitle="+studyTitle+"&studySummary="+summary ,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=650,height=550 top=120,left=200 0, ");
			<%--rajasekhar ends--%>
			windowname.focus();
		} else if (mode == 'N')	{
			alert("<%=MC.M_SbmtStdFirst_ForMoreStdDet%>");/*alert("Please Submit the <%=LC.Std_Study%> first for entering More <%=LC.Std_Study%> Details");*****/
			return false;
		}
	}

	function callAjaxGetTareaDD(formobj){

	   selval = formobj.studyDivision.value;

 	   new VELOS.ajaxObject("getCodeDropDown.jsp", {
   		urlData:"selectedVal="+selval + "&ddName=studyTArea&codeType=tarea&ddType=child" ,
		   reqType:"POST",
		   outputElement: "span_tarea" }

   ).startRequest();

}
	// Bug #8090  Usign AJAX call to check for duplicate study Number
	function jQueryAjaxStudyNum(formobj,accId){

		var result;
		var studyNum = formobj.studyNumber.value;
		var studyNumberOld = formobj.studyNumberOld.value;

		if(studyNumberOld==null || studyNum!=studyNumberOld){
			studyNum = encodeURIComponent(studyNum);
		$j.ajax({		  
	   		url: "../validate?module=study:studyNumberId&dataset="+accId+':'+studyNum ,
	   		dataType: "xml",
	   		async: false,
			success: function(o) {
						var r = $j(o).find('valid').text();
						if(r.indexOf("false")>=0){
	   						result = false;
   						}else {
	   						result = true;
	   					}
					}
	    });
		return result;
	}else{
		return true;
	}
}   


	function fnAddNciNctRow(){
		   if ($j("#ctrp").is(':checked'))
		     $j("#NCIRow").show();
		   else
		     $j("#NCIRow").hide();
	}


	$j(window).load(function () {
			if ($j("#ctrp").is(':checked'))
				     $j("#NCIRow").show();
			
			
		});

</script>

<SCRIPT langauge="Javascript1.2">
	function openwin1(frm) {
		windowName = window.open("usersearchdetails.jsp?fname=&lname=&from="+ frm,"TEST","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200");
		windowName.focus();
	}
</SCRIPT>

<% String src;
src= request.getParameter("srcmenu");
String selectedTab = request.getParameter("selectedTab");
String mode ="";
mode = request.getParameter("mode");
String from = "default";
int studyId = 0;
if (mode.equals("M")) {
    	studyId = EJBUtil.stringToNum(request.getParameter("studyId"));
	}
%>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<jsp:include page="ui-include.jsp" flush="true"></jsp:include>

<body style="overflow:hidden;">

<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
	<script language="JavaScript" src="overlib.js" type="text/javascript"><!-- overLIB (c) Erik Bosrup --></script>
	<script language="JavaScript" src="overlib_shadow.js" type="text/javascript"><!-- overLIB (c) Erik Bosrup --></script>
	<script language="JavaScript" src="overlib_hideform.js" type="text/javascript"><!-- overLIB (c) Erik Bosrup --></script>
	
<DIV class="BrowserTopn" id="divTab">
	<jsp:include page="studytabs.jsp" flush="true">
	<jsp:param name="from" value="<%=from%>"/>
	<jsp:param name="selectedTab" value="<%=selectedTab%>"/>
	<jsp:param name="studyId" value="<%=studyId%>"/>
	</jsp:include>
</DIV>

<%
String stid= request.getParameter("studyId");
if(stid==null || stid==""){%>
<!-- Bug#15375 Fix for 1360*768 resolution- Tarun Kumar -->
<SCRIPT LANGUAGE="JavaScript">
	if(screenWidth>1280 || screenHeight>1024)
		document.write('<DIV class="BrowserBotN BrowserBotN_S_1" id="div1"  style="overflow-x:hidden; height:77%;">')
	else
		document.write('<DIV class="BrowserBotN BrowserBotN_S_1" id="div1"  style="overflow-x:hidden;">')
</SCRIPT>
	<%} else {%>
	<SCRIPT LANGUAGE="JavaScript">
	if(screenWidth>1280 || screenHeight>1024)
		document.write('<DIV class="BrowserBotN BrowserBotN_S_3" id="div1" style="overflow-x:hidden; height:77%;" >')
	else
		document.write('<DIV class="BrowserBotN BrowserBotN_S_3" id="div1" style="overflow-x:hidden;" >')
</SCRIPT>
<%}

  	boolean hasAccess = false;
	HttpSession tSession = request.getSession(true);
	String uName = (String) tSession.getValue("userName");
	String userIdFromSession = (String) tSession.getValue("userId");
	String acc = (String) tSession.getValue("accountId");
	if (sessionmaint.isValidSession(tSession))
	{
	  	String defUserGroup = (String) tSession.getAttribute("defUserGroup");

		int accId = EJBUtil.stringToNum(acc);
		int pageRight = 0;
		int stdRight =0;
		int grpRight = 0;
		
		//copy a new study is controlled by Manage Protocols new group right
		GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
   		grpRight = Integer.parseInt(grpRights.getFtrRightsByValue("NPROTOCOL"));

		ArrayList tId ;
		tId = new ArrayList();

		StudyDao stDao=new StudyDao();
		CtrlDao ctrlValue=new CtrlDao();//km
		CodeDao cdTArea = new CodeDao();
		CodeDao cdPhase = new CodeDao();
		CodeDao cdResType = new CodeDao();
		CodeDao cdBlinding = new CodeDao();
		CodeDao cdRandom = new CodeDao();
		CodeDao cdType = new CodeDao();
		CodeDao cdScope=new CodeDao();
		CodeDao cdDivision = new CodeDao();
		CodeDao cdSponsorName = new CodeDao();//gopu
		String studyNumber = "" ;
		String studyTitle = "" ;
		
		String nciTrialIdentifier = "";
		String nctNumber = "";

		String studyObjective = "" ;
		String studySummary = "" ;
		String tempStudyTitle = "" ; //rajasekhar
		String tempStudySummary="";//rajasekhar
		String studyProduct = "" ;
		String studyDivision = "";
		String studyTArea = "" ;
		String studySize = "0" ;
		String studyDuration = "0" ;
		String studyDurationUnit = "" ;
		String studyEstBgnDate = "" ;
		String studyPhase = "" ;
		String studyResType = "" ;
		String studyType = "" ;
		String studyBlinding = "" ;
		String studyRandom = "" ;
		String studySponsorId = "" ;
		String studySponsorName = ""; //gopu
		String studySponsorIdInfo=""; //gopu
		String studyContactId = "" ;
		String studyOtherInfo = "" ;
		String studyPartCenters = "" ;
		String studyKeywrds = "" ;
		String studyAuthor = "" ;
		String studyAuthorName = "" ;
		String studyPrinv = "" ;
		String studyCo="";
		String studyCoName="";
		String studyPrinvName = "" ;
		String majAuthor="";
		String studyScope="";
		String studyAssoc="";
		String studyAssocNum="";
		String disSiteid="";
		String disSitename="";
		String studyPubCol = "00000";
		String studyVerParent = "";
		String studyInvFlag = "";
		String studyInvNum = "";
		String prinvOther="",nStudySize="0";
		String lastName  ="";
		String firstName = "";
		String ICDCode1 = "";//km
		String ICDCode2 = "";
		String studyDiv = "";

		//KM
		String disableStr ="";
		String readOnlyStr ="";
		String ctrpRepFlag =""; //CTRP-20527 22-Nov-2011 @Ankit
		String fdaRegStdFlag =""; //INF-22247 27-Feb-2012 @Ankit
		/*String ICDCode3 = "";*/

		StringBuffer studyContact = new StringBuffer();
		StringBuffer studySponsor = new StringBuffer();
		StringBuffer dataManager = new StringBuffer();
		StringBuffer durationUnit = new StringBuffer();
		int counter = 0;
		int userId = 0;
		int siteId = 0;
		SiteDao siteDao = new SiteDao();
		// Added Column customization
		ArrayList cValue = new ArrayList();
		ArrayList cCustom1 = new ArrayList();
		String lkpId = "";
		String lkpColumn = "";
		
		ctrlValue.getControlValues("icd");//km
		cValue=ctrlValue.getCValue();
		cCustom1=ctrlValue.getCCustom1();
		if((cValue!=null && cValue.size()>0) && (cCustom1!=null && cCustom1.size()>0))
		{
			lkpId = (String)cValue.get(0);
			lkpColumn = (String)cCustom1.get(0);
		}
		
		
		//Added for INF-22311 : Raviesh
		String publicVsNonpublicStr = "";
		String fdaRegulatedStr = "";
		String ctrpReportableStr = "";
		publicVsNonpublicStr = MC.M_PublicVsNonPublicContent;
		fdaRegulatedStr = MC.M_FDARegulatedStudyContent;
		ctrpReportableStr = MC.M_CTRPReportableContent;


		//Added by Manimaran for the Enh.#GL9

		ConfigFacade cFacade=ConfigFacade.getConfigFacade();
		HashMap hashPgCustFld = new HashMap();

		ConfigDetailsObject cdoPgField = cFacade.populateObject(Integer.parseInt(acc), "studysummary");

		if (cdoPgField.getPcfField()!= null && cdoPgField.getPcfField().size() > 0) {
			for (int i=0;i<cdoPgField.getPcfField().size();i++){
				hashPgCustFld.put((String)cdoPgField.getPcfField().get(i), String.valueOf(i));

	       }
	    }


		//Added by Manimaran for the Enh.#GL7.
		accountB.setAccId(EJBUtil.stringToNum(acc));
		accountB.getAccountDetails();
		String autoGenStudy = accountB.getAutoGenStudy();
		if (autoGenStudy == null)
			autoGenStudy ="0";
			if (mode.equals("M"))
			{
				String roleCodePk ="";

				TeamDao teamDao = new TeamDao();
				teamDao.getTeamRights(studyId,EJBUtil.stringToNum(userIdFromSession));
				tId = teamDao.getTeamIds();

				if (tId.size() <=0)
				{
					pageRight  = 0;
					StudyRightsJB stdRightstemp = new StudyRightsJB();
					tSession.putValue("studyRights",stdRightstemp);
					tSession.putValue("studyRoleCodePk",roleCodePk);

				} else {
					studyId = EJBUtil.stringToNum(request.getParameter("studyId"));

					stdRights.setId(EJBUtil.stringToNum(tId.get(0).toString()));

					ArrayList teamRights ;
					teamRights  = new ArrayList();
					teamRights = teamDao.getTeamRights();

					ArrayList arRoles = new ArrayList();
					arRoles = teamDao.getTeamRoleIds();

					if (arRoles != null && arRoles.size() >0 )
					{
						roleCodePk = (String) arRoles.get(0);

						if (StringUtil.isEmpty(roleCodePk))
						{
							roleCodePk="";
						}
					}
					else
					{
						roleCodePk ="";
					}


					stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
					stdRights.loadStudyRights();

					tSession.putValue("studyRights",stdRights);
					tSession.putValue("studyRoleCodePk",roleCodePk);


					if ((stdRights.getFtrRights().size()) == 0){
					 	stdRight= 0;
					}else{
						stdRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYSUM"));
					}
				}
				stdRights =(StudyRightsJB) tSession.getValue("studyRights");

				if (tId.size() >0)
					pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYSUM"));
					String studyT = "";

					studyB.setId(studyId);
					studyB.getStudyDetails();
					studyNumber = studyB.getStudyNumber();
					if (studyNumber == null ) studyNumber ="";

					studyVerParent = studyB.getStudyVersionParent();
					studyDiv = studyB.getStudyDivision();
					studyInvFlag = studyB.getStudyInvIndIdeFlag();
					studyInvNum = studyB.getStudyIndIdeNum();
					ctrpRepFlag = studyB.getStudyCtrpReportable();
					fdaRegStdFlag = studyB.getFdaRegulatedStudy();
					
					if (studyVerParent == null)	studyVerParent = "";
					if (StringUtil.isEmpty(studyDiv))
					{
						studyDiv = "";
					}

					if (studyInvFlag == null) studyInvFlag = "";
					if (studyInvNum == null) studyInvNum = "";
					if (ctrpRepFlag == null) ctrpRepFlag = "";
					if (fdaRegStdFlag == null) fdaRegStdFlag = "";
					tSession.putValue("studyId",request.getParameter("studyId"));
					tSession.putValue("studyNo",studyNumber);
					tSession.putValue("studyVerParent",studyVerParent);

					studyT = studyB.getStudyTArea();

					if (studyT == null)
						studyT = "";
					 	tSession.putValue("studyTArea",studyT);
					} else {
						tSession.putValue("studyId","");
						tSession.putValue("studyNo","");
						tSession.putValue("StudyRights",null);
				   		pageRight = grpRight;
					}

					if (((mode.equals("M") && pageRight >=4) || (mode.equals("N") && (pageRight == 5 || pageRight == 7 )) ) && ((mode.equals("M") && grpRight >=4) || (mode.equals("N") && (grpRight == 5 || grpRight == 7 ))))
					{
						if ( ("M".equals(mode) && pageRight > 5) ||
						     ("N".equals(mode) && (pageRight == 5 || pageRight == 7) ) )
						{
						 	hasAccess = true;
						}
						//cdTArea.getCodeValues("tarea");

						cdTArea.getCodeValuesForCustom1("tarea",studyDiv);
						cdTArea.setCType("tarea");
				        cdTArea.setForGroup(defUserGroup);

						cdDivision.getCodeValues("study_division");
						cdDivision.setCType("study_division");
				        cdDivision.setForGroup(defUserGroup);

						cdPhase.getCodeValues("phase");
						cdPhase.setCType("phase");
				        cdPhase.setForGroup(defUserGroup);

						cdResType.getCodeValues("research_type");
						cdResType.setCType("research_type");
				        cdResType.setForGroup(defUserGroup);

						cdBlinding.getCodeValues("blinding");
						cdBlinding.setCType("blinding");
				        cdBlinding.setForGroup(defUserGroup);

						cdRandom.getCodeValues("randomization");
						cdRandom.setCType("randomization");
				        cdRandom.setForGroup(defUserGroup);

						cdType.getCodeValues("study_type");
						cdType.setCType("study_type");
				        cdType.setForGroup(defUserGroup);

						cdScope.getCodeValues("studyscope");
						cdScope.setCType("studyscope");
				        cdScope.setForGroup(defUserGroup);

						cdSponsorName.getCodeValues("sponsor"); //gopu
						cdSponsorName.setCType("sponsor");
				        cdSponsorName.setForGroup(defUserGroup);

				        String divAtt ="";
						String tareaAtt ="";
						String phaseAtt ="";
						String researchAtt ="";
						String scopeAtt ="";
						String typeAtt ="";
						String blindingAtt ="";
						String randomAtt = "";
						String spnameAtt = "";

					    if (hashPgCustFld.containsKey("division")) {
							int fldNumDivision = Integer.parseInt((String)hashPgCustFld.get("division"));
							divAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumDivision));
							if(divAtt == null) divAtt ="";
					    }

						if (hashPgCustFld.containsKey("therapeuticarea")) {
							int fldNumTarea = Integer.parseInt((String)hashPgCustFld.get("therapeuticarea"));
							tareaAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumTarea));
							if(tareaAtt == null) tareaAtt ="";
					    }

						if (hashPgCustFld.containsKey("stdphase")) {
							int fldNumPhase = Integer.parseInt((String)hashPgCustFld.get("stdphase"));
							phaseAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumPhase));
							if (phaseAtt == null) phaseAtt ="";
					    }


						if (hashPgCustFld.containsKey("researchtype")) {
							int fldNumResearch = Integer.parseInt((String)hashPgCustFld.get("researchtype"));
							researchAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumResearch));
							if (researchAtt == null) researchAtt ="";
					    }

						if (hashPgCustFld.containsKey("studyscope")) {
							int fldNumScope = Integer.parseInt((String)hashPgCustFld.get("studyscope"));
							scopeAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumScope));
							if (scopeAtt == null) scopeAtt ="";
					    }


						if (hashPgCustFld.containsKey("studytype")) {
							int fldNumType = Integer.parseInt((String)hashPgCustFld.get("studytype"));
							typeAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumType));
							if (typeAtt == null) typeAtt ="";
					    }


						if (hashPgCustFld.containsKey("blinding")) {
							int fldNumBlinding = Integer.parseInt((String)hashPgCustFld.get("blinding"));
							blindingAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumBlinding));
							if (blindingAtt == null) blindingAtt ="";
					    }


						if (hashPgCustFld.containsKey("randomization")) {
							int fldNumRandom = Integer.parseInt((String)hashPgCustFld.get("randomization"));
							randomAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumRandom));
							if (randomAtt == null) randomAtt ="";
					    }


						if (hashPgCustFld.containsKey("sponsorname")) {
							int fldNumSpname = Integer.parseInt((String)hashPgCustFld.get("sponsorname"));
							spnameAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumSpname));
							if (spnameAtt == null) spnameAtt ="";
					    }

						if (mode.equals("M")) {
%>
<%--
				String studyT = "";
				studyB.setId(studyId);
				studyB.getStudyDetails();
				studyNumber = studyB.getStudyNumber();
				studyVerParent = studyB.getStudyVersionParent();

				if (studyVerParent == null)
					studyVerParent = "";

				tSession.putValue("studyId",request.getParameter("studyId"));
				tSession.putValue("studyNo",studyNumber);

				tSession.putValue("studyVerParent",studyVerParent);

				studyT = studyB.getStudyTArea();
				studyDiv = studyB.getStudyDivision();
				studyInvFlag = studyB.getStudyInvIndIdeFlag();
				studyInvNum = studyB.getStudyIndIdeNum();

				if (studyT == null)
					studyT = "";
				if(studyDiv == null)
					studyDiv  = "";
				if(studyInvNum == null)
					studyInvNum = "";
				tSession.putValue("studyTArea",studyT);
--%>
<%
						//stdRights.setId(studyId);
							studyTitle = studyB.getStudyTitle();
							tempStudyTitle = URLEncoder.encode(studyTitle,"UTF-8");//rajasekhar
							if(studyTitle == null) studyTitle = ""; //KM
							
							
							//nciTrialIdentifier Added for CTRP-22471 : Raviesh							
							nciTrialIdentifier = studyB.getNciTrialIdentifier();
							if(nciTrialIdentifier == null) nciTrialIdentifier = "";
							
							//nctNumber Added for CTRP-22471 : Raviesh
							nctNumber = studyB.getNctNumber();
							if(nctNumber == null) nctNumber = "";
							

							//	studyTitle = StringUtil.htmlDecode(studyTitle);

							studyObjective = studyB.getStudyObjective();
							studyObjective = (   studyObjective  == null      )?"":(  studyObjective ) ;

							studySummary = studyB.getStudySummary();
							tempStudySummary = URLEncoder.encode(studySummary,"UTF-8");//rajasekhar
							//studySummary = studySummary.replaceAll("[\\t\\n\\r]"," ");//rajasekhar-
							studySummary = (   studySummary  == null      )?"":(  studySummary ) ;

							studyProduct = studyB.getStudyProduct();
							studyProduct = (   studyProduct  == null      )?"":(  studyProduct ) ;

							//studySize = studyB.getStudySize();
							//studySize = (   studySize  == null      )?"":(  studySize ) ;
							nStudySize=studyB.getStudyNSamplSize();
							nStudySize=(nStudySize==null)?"":nStudySize;
							studyDuration = studyB.getStudyDuration();
							studyDuration=(studyDuration==null)?"":studyDuration;
							studyDuration=(studyDuration.equals("0"))?"":studyDuration;

							studyDurationUnit = studyB.getStudyDurationUnit();
							studyEstBgnDate = studyB.getStudyEstBeginDate();

							studyOtherInfo = studyB.getStudyInfo();
							studyOtherInfo = (   studyOtherInfo  == null      )?"":(  studyOtherInfo ) ;

							//studyPartCenters = studyB.getStudyPartCenters();
							//studyPartCenters = (   studyPartCenters  == null      )?"":(  studyPartCenters ) ;

							studyKeywrds = studyB.getStudyKeywords();
							studyKeywrds = (   studyKeywrds  == null      )?"":(  studyKeywrds ) ;

							studySponsorId = studyB.getStudySponsor();
							studySponsorId = (   studySponsorId  == null      )?"":(  studySponsorId ) ;

							//gopu
							studySponsorName = studyB.getStudySponsorName();
							studySponsorName = (   studySponsorName  == null      )?"":(  studySponsorName ) ;

							studySponsorIdInfo = studyB.getStudySponsorIdInfo();
							studySponsorIdInfo = (   studySponsorIdInfo  == null      )?"":(  studySponsorIdInfo ) ;

							studyContactId = studyB.getStudyContact();
							studyContactId = (   studyContactId  == null      )?"":(  studyContactId ) ;

							studyAuthor = studyB.getStudyAuthor();
							if(studyAuthor == null)	studyAuthor = "";

							studyPrinv = studyB.getStudyPrimInv();
							if(studyPrinv==null) studyPrinv="";

							//Study Coordinator
							studyCo=studyB.getStudyCoordinator();
							studyCo=(studyCo==null)?"":studyCo;

							prinvOther=studyB.getStudyOtherPrinv();
							if (prinvOther==null) prinvOther="";
							majAuthor=studyB.getMajAuthor();
							majAuthor=(majAuthor==null)?"":(majAuthor);

							studyAssoc=studyB.getStudyAssoc();
							studyAssoc=(studyAssoc==null)?"":(studyAssoc);

							disSiteid=studyB.getDisSite();
							disSiteid=(disSiteid==null)?"":(disSiteid);
							disSitename=stDao.getDiseaseSite(EJBUtil.integerToString(new Integer(studyId)));
							disSitename=(disSitename==null)?"":(disSitename);
							tSession.putValue("studyDataMgr",studyAuthor);
							ICDCode1=studyB.getStudyICDCode1();//km
							ICDCode1=(ICDCode1==null)?"":(ICDCode1);
							ICDCode2=studyB.getStudyICDCode2();
							ICDCode2=(ICDCode2==null)?"":(ICDCode2);
							/*ICDCode3=studyB.getStudyICDCode3();
							ICDCode3=(ICDCode3==null)?"":(ICDCode3);*/

							userB.setUserId(EJBUtil.stringToNum(studyAuthor));
							userB.getUserDetails();
							firstName  = userB.getUserFirstName();
							lastName = userB.getUserLastName();
							if(firstName == null)
								firstName  = "";
							if(lastName == null)
								lastName  = "";

							studyAuthorName = firstName+" " +lastName ;

							//if (! EJBUtil.isEmpty(studyPrinv) )
							if(studyPrinv.length()==0)
								studyPrinvName="";
							else {
								userB.setUserId(EJBUtil.stringToNum(studyPrinv));
								userB.getUserDetails();
								studyPrinvName =  userB.getUserFirstName() + " " +userB.getUserLastName();
							}

							//if (! EJBUtil.isEmpty(studyPrinv) )
							if(studyCo.length()==0)
								studyCoName="";
							else {
								userB.setUserId(EJBUtil.stringToNum(studyCo));
								userB.getUserDetails();
								studyCoName =  userB.getUserFirstName() + " " +userB.getUserLastName();
							}

							studyPubCol = studyB.getStudyPubCol();

							if (tareaAtt.equals("1") || tareaAtt.equals("2"))
								studyTArea = cdTArea.toPullDown("studyTArea",EJBUtil.stringToNum(studyB.getStudyTArea()) ,"disabled");
							else
								studyTArea = cdTArea.toPullDown("studyTArea",EJBUtil.stringToNum(studyB.getStudyTArea()));

							if (divAtt.equals("1") || divAtt.equals("2"))
								studyDivision = cdDivision.toPullDown("studyDivision",EJBUtil.stringToNum(studyDiv), " onChange=callAjaxGetTareaDD(document.study); disabled");
							else
							    studyDivision = cdDivision.toPullDown("studyDivision",EJBUtil.stringToNum(studyDiv), " onChange=callAjaxGetTareaDD(document.study);");

							if (phaseAtt.equals("1") || phaseAtt.equals("2"))
								studyPhase = cdPhase.toPullDown("studyPhase",EJBUtil.stringToNum(studyB.getStudyPhase()),"disabled");
							else
								studyPhase = cdPhase.toPullDown("studyPhase",EJBUtil.stringToNum(studyB.getStudyPhase()));

							if (researchAtt.equals("1") || researchAtt.equals("2"))
								studyResType = cdResType.toPullDown("studyResType",EJBUtil.stringToNum(studyB.getStudyResType()),"disabled");
							else
								studyResType = cdResType.toPullDown("studyResType",EJBUtil.stringToNum(studyB.getStudyResType()));

							if (blindingAtt.equals("1") || blindingAtt.equals("2"))
								studyBlinding = cdBlinding.toPullDown("studyBlinding",EJBUtil.stringToNum(studyB.getStudyBlinding()),"disabled");
							else
								studyBlinding = cdBlinding.toPullDown("studyBlinding",EJBUtil.stringToNum(studyB.getStudyBlinding()));

							if (randomAtt.equals("1") || randomAtt.equals("2"))
								studyRandom = cdRandom.toPullDown("studyRandom",EJBUtil.stringToNum(studyB.getStudyRandom()),"disabled");
							else
								studyRandom = cdRandom.toPullDown("studyRandom",EJBUtil.stringToNum(studyB.getStudyRandom()));

							if (spnameAtt.equals("1") || spnameAtt.equals("2"))
								studySponsorName = cdSponsorName.toPullDown("sponsor",EJBUtil.stringToNum(studyB.getStudySponsorName()),"disabled");
							else
								studySponsorName = cdSponsorName.toPullDown("sponsor",EJBUtil.stringToNum(studyB.getStudySponsorName()),true);

							if (typeAtt.equals("1") || typeAtt.equals("2"))
								studyType = cdType.toPullDown("studyType",EJBUtil.stringToNum(studyB.getStudyType()),"disabled");
							else
								studyType = cdType.toPullDown("studyType",EJBUtil.stringToNum(studyB.getStudyType()));

							if (scopeAtt.equals("1") || scopeAtt.equals("2"))
								studyScope=cdScope.toPullDown("studyScope",EJBUtil.stringToNum(studyB.getStudyScope()),"disabled");
							else
								studyScope=cdScope.toPullDown("studyScope",EJBUtil.stringToNum(studyB.getStudyScope()));

							if (studyAssoc.length()>0){
								studyAssocB.setId(EJBUtil.stringToNum(studyAssoc));
								studyAssocB.getStudyDetails();
								studyAssocNum = studyAssocB.getStudyNumber();
							}
					   	} else {
							if (tareaAtt.equals("1") || tareaAtt.equals("2"))
								studyTArea = cdTArea.toPullDown("studyTArea","disabled");
							else
								studyTArea = cdTArea.toPullDown("studyTArea");

							if (divAtt.equals("1") || divAtt.equals("2"))
								studyDivision = cdDivision.toPullDown("studyDivision",0, " onChange=callAjaxGetTareaDD(document.study); disabled");
							else
							    studyDivision = cdDivision.toPullDown("studyDivision",0, " onChange=callAjaxGetTareaDD(document.study);");

							if (phaseAtt.equals("1") || phaseAtt.equals("2"))
								studyPhase = cdPhase.toPullDown("studyPhase","disabled");
							else
							    studyPhase = cdPhase.toPullDown("studyPhase");

							if (researchAtt.equals("1") || researchAtt.equals("2"))
								studyResType = cdResType.toPullDown("studyResType","disabled");
							else
								studyResType = cdResType.toPullDown("studyResType");

							if (blindingAtt.equals("1") || blindingAtt.equals("2"))
								studyBlinding = cdBlinding.toPullDown("studyBlinding","disabled");
							else
								studyBlinding = cdBlinding.toPullDown("studyBlinding");

							if (randomAtt.equals("1") || randomAtt.equals("2"))
								studyRandom = cdRandom.toPullDown("studyRandom","disabled");
							else
								studyRandom = cdRandom.toPullDown("studyRandom");

							if (spnameAtt.equals("1") || spnameAtt.equals("2"))
								studySponsorName = cdSponsorName.toPullDown("sponsor","disabled");
							else
								studySponsorName = cdSponsorName.toPullDown("sponsor");

							if (typeAtt.equals("1") || typeAtt.equals("2"))
								studyType = cdType.toPullDown("studyType","disabled");
							else
								studyType = cdType.toPullDown("studyType");

							if (scopeAtt.equals("1") || scopeAtt.equals("2"))
								studyScope=cdScope.toPullDown("studyScope","disabled");
							else
								studyScope=cdScope.toPullDown("studyScope");

							studyAuthor = userIdFromSession;
							userB.setUserId(EJBUtil.stringToNum(studyAuthor));
							userB.getUserDetails();
							firstName  = userB.getUserFirstName();
							lastName = userB.getUserLastName();
							if(firstName == null)
								firstName  = "";
							if(lastName == null)
								lastName  = "";
							studyAuthorName = firstName+" " + lastName;

							//studyPrinv = userIdFromSession;
							//studyPrinvName = userB.getUserLastName() + ", " + userB.getUserFirstName();
						}
						char[] stdSecRights= null;
						if (studyPubCol == null) studyPubCol="1111";
						stdSecRights= studyPubCol.toCharArray();
						int totSec = studyPubCol.length();
						int secCount=0;

						from = "default";
			%>
<div style="width:99%">
			<Form name="study" id="studyForm" method="post" action="updateNewStudy.jsp"  onSubmit="if (validate(document.study,<%=autoGenStudy%>,<%=accId%>)==false) {setValidateFlag('false');return false;} else {setValidateFlag('true'); return true;}">
			    <input type="hidden" name="srcmenu" size = 20  value = <%=src%> >
			    <input type="hidden" name="selectedTab" size = 20  value = <%=selectedTab%> >
			    <input type="hidden" name="totPubSections" size = 20  value=4>
			    <input type="hidden" name="delStrs" >

		    <%if (mode.equals("M"))
			{%>
			    <input type="hidden" name="studyId" size = 20  value = <%=studyId%> >
				<input type="hidden" name="studyVerParent" size = 20  value = <%=studyVerParent%>>
				
		 <% } %>
				<input type="hidden" name="mode" size="20"  value = <%=mode%> >
			    <P class="sectionHeadingsFrm"><%=LC.L_Std_Info%><%--Study Information*****--%></P>
			    <table width="100%" cellspacing="2" cellpadding="2" border="0" class="basetbl">
					<tr>
						<td width="40%">
							<table>
								<tr>									
									<td colspan =2>
										<A href="copystudy.jsp?srcmenu=<%=src%>&studymode=<%=mode%>&fromstudyId=<%=studyId%>&fromMode=initial" onClick="return f_check_perm(<%=grpRight%>,'N')"><%=MC.M_CpyExtg_Std%><%--COPY AN EXISTING <%=LC.Std_Study_Upper%>*****--%></A>
									</td>
								</tr>		
				
								<tr>
								<%
				
								if (hashPgCustFld.containsKey("studyentby")) {
				
								int fldNumStudyBy = Integer.parseInt((String)hashPgCustFld.get("studyentby"));
								String studyByMand = ((String)cdoPgField.getPcfMandatory().get(fldNumStudyBy));
								String studyByLable = ((String)cdoPgField.getPcfLabel().get(fldNumStudyBy));
								String studyByAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumStudyBy));
				
								disableStr ="";
								readOnlyStr ="";
				
								if(studyByAtt == null) studyByAtt ="";
								if(studyByMand == null) studyByMand ="";
				
								if(!studyByAtt.equals("0")) {
				
								if(studyByLable !=null){
								%>
								<td width="20%">
								 <%=studyByLable%> 
								<%} else {%> <td width="20%">
								<!--b>Study Entered By</b-->
								<%=LC.L_Std_EnteredBy%><%--<%=LC.Std_Study%> Entered By*****--%>
								<% }
				
				
							   if (studyByMand.equals("1")) {
								%>
							   <FONT class="Mandatory" id="pgcustomstudyentby">* </FONT>
							   <% }
							   %>
				
							   <%if(studyByAtt.equals("1")) {
								   disableStr = "disabled class='readonly-input'"; }
							   %>
							  </td>
							  <td width="30%">
							  <input type=hidden name="dataManager" value='<%=studyAuthor%>' <%=disableStr%> >
							  <input type=text name="dataManagerName" value="<%=studyAuthorName%>" readonly <%=disableStr%>>
				
							  <%if(!studyByAtt.equals("1") && !studyByAtt.equals("2")) { %>
							  <A HREF=# onClick=openwin1('study') ><%=LC.L_Select_User%><%--SELECT USER*****--%></A>
							  <%}%>
				
							  </td>
				
								<% } else { %>
							   <input type=hidden name="dataManager" value='<%=studyAuthor%>'>   <!-- KM- for PK value in case of hidden-->
				
								<%}}  else {%>
				
								<td width="20%"><%=LC.L_Std_EnteredBy%><%--<%=LC.Std_Study%> Entered By*****--%> <FONT class="Mandatory" id="mandstudyent">* </FONT>
										</td>
										<td width="30%">
											<input type=hidden name="dataManager" value='<%=studyAuthor%>'>
											<input type=text name="dataManagerName" value="<%=studyAuthorName%>" readonly>
											<A HREF=# onClick=openwin1('study') ><%=LC.L_Select_User%><%--SELECT USER*****--%></A>
										</td>
				
							 <%}%>
							</tr>
				
				
								<tr>
								<%
								if (hashPgCustFld.containsKey("pinvestigator")) {
				
								int fldNumPi = Integer.parseInt((String)hashPgCustFld.get("pinvestigator"));
								String pIMand = ((String)cdoPgField.getPcfMandatory().get(fldNumPi));
								String pILable = ((String)cdoPgField.getPcfLabel().get(fldNumPi));
								String pIAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumPi));
				
								disableStr ="";
								readOnlyStr ="";
				
								if(pIAtt == null) pIAtt ="";
								if(pIMand == null) pIMand ="";
				
								if(!pIAtt.equals("0")) {
				
								if(pILable !=null){
								%>
								<td width="20%">
								 <%=pILable%> 
								<%} else {%> <td width="20%">
								<%=LC.L_Principal_Investigator%><%--Principal Investigator*****--%>
								<% }
				
				
							   if (pIMand.equals("1")) {
								%>
							   <FONT class="Mandatory" id="pgcustompi">* </FONT>
							   <% }
							   %>
				
							   <%if(pIAtt.equals("1")) {
								   disableStr = "disabled class='readonly-input'"; }
				
							   %>
							  </td>
							  <td width="30%">
							  <input type=hidden name="prinInv" value='<%=studyPrinv%>'>
							  <input type=text name="prinInvName" value="<%=studyPrinvName%>" readonly <%=disableStr%>>
				
							   <%if(!pIAtt.equals("1") && !pIAtt.equals("2")) {%>
							  <A HREF=# onClick=openwin1('studyinv') ><%=LC.L_Select_User%><%--SELECT USER*****--%></A>
							   <%}%>
							  </td>
				
								<% } else { %>
								 <input type=hidden name="prinInv" value='<%=studyPrinv%>'>
				
								<% } }  else {%>
				
								<td width="20%"> <%=LC.L_Principal_Investigator%><%--Principal Investigator*****--%>
								 </td>
								 <td width="30%">
											<input type=hidden name="prinInv" value='<%=studyPrinv%>'>
											<input type=text name="prinInvName" value="<%=studyPrinvName%>" readonly>
											<A HREF=# onClick=openwin1('studyinv') ><%=LC.L_Select_User%><%--SELECT USER*****--%></A>
				
							 <%}%>
							 </tr>
				
				
								<tr>
								<%
								if (hashPgCustFld.containsKey("ifotheruser")) {
				
								int fldNumOtherUser = Integer.parseInt((String)hashPgCustFld.get("ifotheruser"));
								String othUserMand = ((String)cdoPgField.getPcfMandatory().get(fldNumOtherUser));
								String othUserLable = ((String)cdoPgField.getPcfLabel().get(fldNumOtherUser));
								String othUserAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumOtherUser));
				
								disableStr ="";
								readOnlyStr ="";
				
								if(othUserAtt == null) othUserAtt ="";
								if(othUserMand == null) othUserMand ="";
				
								if(!othUserAtt.equals("0")) {
				
								if(othUserLable !=null){
								%>
								<td width="20%">
								<%=othUserLable%>
								<%} else {%> <td width="20%">
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_If_Other%><%--IF OTHER*****--%>
								<% }
				
				
							   if (othUserMand.equals("1")) {
								%>
							   <FONT class="Mandatory" id="pgcustomifotheruser">* </FONT>
							   <% }
							   %>
				
							   <%if(othUserAtt.equals("1")) {
								   disableStr = "disabled class='readonly-input'"; }
								 else if (othUserAtt.equals("2") ) {
								   readOnlyStr = "readonly"; }
							   %>
							  </td>
							  <td width="30%"> <input type="text" name="prinvOther" id="prinvIfOther" value="<%=prinvOther%>"  <%=disableStr%>  <%=readOnlyStr%>  ></td>
				
								<% } else { %>
								<input type="hidden" name="prinvOther" id="prinvIfOther"  value="<%=prinvOther%>" >
				
								<% } }  else {%>
				
								<td width="20%"> <%=LC.L_If_Other%><%--IF OTHER*****--%></td>
									<td width="30%"><input type="text" name="prinvOther" id="prinvIfOther" value="<%=prinvOther%>"></td>
							 <%}%>
							 </tr>
				
				
								<tr>
								<%
				
								if (hashPgCustFld.containsKey("studycontact")) {
				
								int fldNumStudyCont = Integer.parseInt((String)hashPgCustFld.get("studycontact"));
								String StudyContMand = ((String)cdoPgField.getPcfMandatory().get(fldNumStudyCont));
								String StudyContLable = ((String)cdoPgField.getPcfLabel().get(fldNumStudyCont));
								String StudyContAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumStudyCont));
				
								disableStr ="";
								readOnlyStr ="";
				
								if(StudyContAtt == null) StudyContAtt ="";
								if(StudyContMand == null) StudyContMand ="";
				
								if(!StudyContAtt.equals("0")) {
				
								if(StudyContLable !=null){
								%>
								<td width="20%">
								 <%=StudyContLable%> 
								<%} else {%> <td width="20%">
								<%=LC.L_Study_Contact%><%--Study Contact*****--%> 
								<% }
				
							   if (StudyContMand.equals("1")) {
								%>
							   <FONT class="Mandatory" id="pgcustomstudycont">* </FONT>
							   <% }
							   %>
				
							   <%if(StudyContAtt.equals("1")) {
								   disableStr = "disabled class='readonly-input'"; }
							   %>
							  </td>
							  <td width="30%">
									<input type=hidden name="studyco" value='<%=studyCo%>'>
									<input type=text name="studycoName" value="<%=studyCoName%>" readonly  <%=disableStr%> >
				
									 <%if(!StudyContAtt.equals("1") && !StudyContAtt.equals("2")) {%>
									<A HREF=# onClick=openwin1('studyco') ><%=LC.L_Select_User%><%--SELECT USER*****--%></A>
									<%}%>
				
				
							  </td>
				
								<% } else {%>
								<input type=hidden name="studyco" value='<%=studyCo%>'>
								<% }}  else {%>
				
								<td width="20%"> <%=LC.L_Study_Contact%><%--Study Contact*****--%> </td>
								<td width="30%">
									<input type=hidden name="studyco" value='<%=studyCo%>'>
									<input type=text name="studycoName" value="<%=studyCoName%>" readonly>
									<A HREF=# onClick=openwin1('studyco') ><%=LC.L_Select_User%><%--SELECT USER*****--%></A>
								</td>
							 <%}%>
							</tr>
							</table>
							
					</td>
					<td width="40%">
						<table width="80%">
							<tr>
						<td>
					<%if (majAuthor.equals("Y")){%>
					<input type="checkbox" name="cbauth" onClick="setValue(document.study)" checked>
					<%}else{%>
					<input type="checkbox" name="cbauth" onClick="setValue(document.study)">
					<%}%>
					<%=MC.M_PInvestigator_MajorAuth%><%--Principal Investigator was a major author/initiator of this <%=LC.Std_Study_Lower%>?*****--%></td>
					<input type="hidden" name="author" value="<%=majAuthor%>">

					</tr>
					</tr>

					<tr >
						<!-- CTRP-20527 22-Nov-2011 @Ankit -->
						<td>
							<input type="checkbox" name="ctrp" id="ctrp" <%if(ctrpRepFlag.equals("1")){%>checked onclick="return false" onkeydown="return false"<%}%><%else{%>onclick="fnAddNciNctRow();"<%}%>></input>
							<%=LC.L_Ctrp_Reportable%>
							&nbsp; <A href="#"><img src="../images/jpg/help.jpg" border="0" onmouseover="return overlib('<%=StringUtil.escapeSpecialCharJS(ctrpReportableStr)%>',CAPTION,'<%=LC.L_Ctrp_Reportable%> <%=LC.L_Study%>',LEFT, ABOVE);"	onmouseout="return nd();" alt=""></img></A>
						</td>
					</tr>
					<tr >
						<!-- INF-22247 27-Feb-2012 @Ankit -->
						<td>
						<%if(fdaRegStdFlag.equals("1")){%>
							&nbsp;<%=LC.L_StudyMarkedAsFDARegulated%>
							<input type="hidden" name="fda" value="on" >
						<%}else{%>
							<input type="checkbox" name="fda">
							<%=LC.L_FdaRegulatedStudy%>
							&nbsp; <A href="#">
							<img src="../images/jpg/help.jpg" border="0" onmouseover="return overlib('<%=StringUtil.escapeSpecialCharJS(fdaRegulatedStr)%>',CAPTION,'<%=LC.L_FdaRegulatedStudy%>',LEFT, ABOVE);" onmouseout="return nd();" alt=""></img></A>
						<%}%>
						</td>
					</tr>					
						</table>
					</td>
				</tr>
				</table>
				
			   <!-- Fixed Bug#7882 : Raviesh -->
			   <table width="100%" cellspacing="2" cellpadding="2" border="0" class="basetbl midAlign">
					<tr>					
						<td colspan=2 align="left">
						<br/>
						<!-- Modified for Bug#7962 : Raviesh -->
						<!-- Modified for Bug#7969 : Akshi -->
							<jsp:include page="ctrpDraftSectINDIDE.jsp">
								<jsp:param value="<%=studyId%>" name="studyId"/>
								<jsp:param value="<%=studyInvNum%>" name="studyInvNum"/>
								<jsp:param value="<%=studyInvFlag%>" name="studyInvFlag"/>
								<jsp:param value="editable" name="mode"/>
							</jsp:include>					
						</td>
				    </tr>				
				</table>
							<P class = "sectionHeadingsFrm"> <%=LC.L_Std_Definition%><%--<%=LC.Std_Study%> Definition*****--%> </P>
			    <table width="100%" cellspacing="2" cellpadding="2" border="0" class="basetbl midAlign" >



				<!--tr>
						<% if (autoGenStudy.equals("1")){ %>
						<td width="20%"> Study Number  <FONT class="Mandatory">* </FONT> (system-generated) </td>
						<%}else { %>
				        <td width="20%"> Study Number <FONT class="Mandatory">* </FONT> </td>
				        <%} %>
				        <td>
				       <!-- <input type="text" name="studyNumber" size = 15 MAXLENGTH = 20 value="<%=studyNumber%>" dojoType="ValidationTextBox" required="true" >-->
   				        <!--input type="text" name="studyNumber" size = 40 MAXLENGTH = 100 value="<%=studyNumber%>" dojoType="ValidationTextBox" required="true" >
    					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<A href="#" onClick="openwinstudyid('<%=studyId%>','<%=mode%>',<%=stdRight%>);"><img src="../images/jpg/preview.gif" border="0"> MORE STUDY DETAILS</A>
				        </td>
				</tr-->

				
			<tr>
				<%

				if (hashPgCustFld.containsKey("studynumber")) {

				int fldNumStudyNum = Integer.parseInt((String)hashPgCustFld.get("studynumber"));
				String StudyNumMand = ((String)cdoPgField.getPcfMandatory().get(fldNumStudyNum));
				String StudyNumLable = ((String)cdoPgField.getPcfLabel().get(fldNumStudyNum));
				String StudyNumAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumStudyNum));

				disableStr ="";
				readOnlyStr ="";

				if(StudyNumAtt == null) StudyNumAtt ="";
  				if(StudyNumMand == null) StudyNumMand ="";

				if(!StudyNumAtt.equals("0")) {

				if(StudyNumLable !=null){
				%>
				<td width="20%">  <%=StudyNumLable%> 
				<%} else {%> <td width="20%">
				  <%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%>
				<% }

			   if (StudyNumMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomstudynum">* </FONT>
		 	   <% }
				//KM:31July08
				if (autoGenStudy.equals("1") && mode.equals("N")) {
			   %>
			   (system-generated)
			   <%} if(StudyNumAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }
		         else if (StudyNumAtt.equals("2") ) {
			       readOnlyStr = "readonly"; }
  		       %>
 			  </td>
			  <td>
			  			<!-- Changes By Sudhir on 05-Apr-2012 for Bug #9121-->
   				        <input type="text" name="studyNumber" size = 40 MAXLENGTH = 100   <%=disableStr%> <%=readOnlyStr%>  value="<%=studyNumber%>" >
   				        <!-- Changes By Sudhir on 05-Apr-2012 for Bug #9121-->
   				        <input type="hidden" name="studyNumberOld" value="<%=studyNumber%>">
					
					<%-- rajasekhar starts --%>
    					<%--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<A href="#" onClick="openwinstudyid('<%=studyId%>','<%=mode%>',<%=stdRight%>);"><img src="../jsp/images/moreInfo.png" border="0" title="<%=LC.L_More_StdDets%>"></A>--%>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<A href="#" onClick="openwinstudyid('<%=studyId%>','<%=mode%>',<%=stdRight%>,'<%=studyNumber%>','<%=tempStudyTitle%>','<%=tempStudySummary%>');"><img src="../jsp/images/moreInfo.png" border="0" title="<%=LC.L_More_StdDets%>"></A>
					<%--rajasekhar ends --%>			        
					</td>

				<% } else { %>

				 <input type="hidden" name="studyNumber" size = 40 MAXLENGTH = 100   value="<%=studyNumber%>" >
				 <input type="hidden" name="studyNumberOld" value="<%=studyNumber%>">
				<% }}  else {%>

				<%

				//Modified by Manimaran on July31,08
				if (autoGenStudy.equals("1")){ %>

						<td width="20%"> <%=LC.L_Study_Number%><%--Study Number*****--%>  <FONT class="Mandatory" id="mandsnumber">* </FONT> <% if (mode.equals("N")) { %> (<%=LC.L_SysHypenGen%><%--system-generated*****--%>) <%}%> </td>
						<%}else { %>
				        <td width="20%"> <%=LC.L_Study_Number%><%--Study Number*****--%> <FONT class="Mandatory" id="mandsnumber">* </FONT> </td>
				        <%} %>
				        <td>
				       <!-- <input type="text" name="studyNumber" size = 15 MAXLENGTH = 20 value="<%=studyNumber%>" dojoType="ValidationTextBox" required="true" >-->
				       <!-- Changes By Sudhir on 05-Apr-2012 for Bug #9121-->
   				        <input type="text" name="studyNumber" size = 40 MAXLENGTH = 100 value="<%=studyNumber%>" >
   				        <!-- Changes By Sudhir on 05-Apr-2012 for Bug #9121-->
   				        <input type="hidden" name="studyNumberOld" value="<%=studyNumber%>">
					<%-- rajasekhar starts --%>
    					<%--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<A href="#" onClick="openwinstudyid('<%=studyId%>','<%=mode%>',<%=stdRight%>);"><img src="../jsp/images/moreInfo.png" border="0" title="<%=LC.L_More_StdDets%>"></A>--%>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<A href="#" onClick="openwinstudyid('<%=studyId%>','<%=mode%>',<%=stdRight%>,'<%=studyNumber%>','<%=tempStudyTitle%>','<%=tempStudySummary%>');"><img src="../jsp/images/moreInfo.png" border="0" title="<%=LC.L_More_StdDets%>"></A>					
					<%-- rajasekhar ends --%>
 		         </td>
			 <%}%>
				</tr>


				<tr>
				<%
				if (hashPgCustFld.containsKey("studytitle")) {

				int fldNumStudyTitle = Integer.parseInt((String)hashPgCustFld.get("studytitle"));
				String stdTitleMand = ((String)cdoPgField.getPcfMandatory().get(fldNumStudyTitle));
				String stdTitleLable = ((String)cdoPgField.getPcfLabel().get(fldNumStudyTitle));
				String stdTitleAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumStudyTitle));

				disableStr ="";
				readOnlyStr ="";

				if(stdTitleAtt == null) stdTitleAtt ="";
  				if(stdTitleMand == null) stdTitleMand ="";

				if(!stdTitleAtt.equals("0")) {

				if(stdTitleLable !=null){
				%>
				<td width="20%">
				 <%=stdTitleLable%>
				<%} else {%> <td width="20%">
				<%=LC.L_Title%><%--Title*****--%>
				<% }


			   if (stdTitleMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomstudytitle">* </FONT>
		 	   <% }
			   %>

			   <%if(stdTitleAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }
		         else if (stdTitleAtt.equals("2") ) {
			       readOnlyStr = "readonly"; }
  		       %>
 			  </td>

              <td> <TextArea name="studyTitle" rows=2 cols=93 MAXLENGTH = 1000  <%=disableStr%>  <%=readOnlyStr%> > <%=studyTitle%> </TextArea> </td>

			  <% } else { %>
			   <TextArea name="studyTitle"  Style = "visibility:hidden"  rows=2 cols=93 MAXLENGTH = 1000> <%=studyTitle%> </TextArea>

			 <% }}  else {%>

				<td width="20%"><%=LC.L_Title%><%--Title*****--%> <FONT class="Mandatory" id="mandtitle">* </FONT> </td>
			        <td>
				        <TextArea name="studyTitle" rows=2 cols=93 MAXLENGTH = 1000  ><%=studyTitle%></TextArea>
			        </td>
			 <%}%>
			 </tr>



				<tr>
				<%
				if (hashPgCustFld.containsKey("objective")) {

				int fldNumObjective = Integer.parseInt((String)hashPgCustFld.get("objective"));
				String objectiveMand = ((String)cdoPgField.getPcfMandatory().get(fldNumObjective));
				String objectiveLable = ((String)cdoPgField.getPcfLabel().get(fldNumObjective));
				String objectiveAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumObjective));

				disableStr ="";
				readOnlyStr ="";

				if(objectiveAtt == null) objectiveAtt ="";
  				if(objectiveMand == null) objectiveMand ="";

				if(!objectiveAtt.equals("0")) {

				if(objectiveLable !=null){
				%>
				<td width="20%">
				 <%=objectiveLable%>
				<%} else {%> <td width="20%">
				<%=LC.L_Objective%><%--Objective*****--%>
				<% }


			   if (objectiveMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomobjective">* </FONT>
		 	   <% }
			   %>

			   <%if(objectiveAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }
		         else if (objectiveAtt.equals("2") ) {
			       readOnlyStr = "readonly"; }
  		       %>
 			  </td>

			  <td>  <TextArea name="studyObjective" rows=3 cols=93  <%=disableStr%>  <%=readOnlyStr%> ><%=studyObjective%></TextArea>  </td>

			  <% } else { %>

			  <TextArea name="studyObjective" rows=3  Style = "visibility:hidden" cols=93 > <%=studyObjective%> </TextArea>

			  <% } }  else {%>

				<td width="20%"><%=LC.L_Objective%><%--Objective*****--%></td>
			     <td>
						<!--Modified by Manimaran to fix the Bug2741 -->
					    <TextArea name="studyObjective" rows=3 cols=93  ><%=studyObjective%></TextArea>
	 	        </td>
			 <%}%>
			 </tr>


				<tr>
				<%
				if (hashPgCustFld.containsKey("summary")) {

				int fldNumSummary = Integer.parseInt((String)hashPgCustFld.get("summary"));
				String summaryMand = ((String)cdoPgField.getPcfMandatory().get(fldNumSummary));
				String summaryLable = ((String)cdoPgField.getPcfLabel().get(fldNumSummary));
				String summaryAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumSummary));

				disableStr ="";
				readOnlyStr ="";

				if(summaryAtt == null) summaryAtt ="";
  				if(summaryMand == null) summaryMand ="";

				if(!summaryAtt.equals("0")) {

				if(summaryLable !=null){
				%>
				<td width="20%">
				 <%=summaryLable%>
				<%} else {%> <td width="20%">
				<%=LC.L_Summary%><%--Summary*****--%>
				<% }


			   if (summaryMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomsummary">* </FONT>
		 	   <% }
			   %>

			   <%if(summaryAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }
		         else if (summaryAtt.equals("2") ) {
			       readOnlyStr = "readonly"; }
  		       %>
 			  </td>

			  <td> 	<TextArea name="studySummary" rows=3 cols=93  <%=disableStr%>  <%=readOnlyStr%> ><%=studySummary%></TextArea>  </td>

			  <% } else { %>

			  <TextArea name="studySummary" rows=3 Style = "visibility:hidden" cols=93 ><%=studySummary%></TextArea>

			  <% }}  else {%>

			     <td width="20%"><%=LC.L_Summary%><%--Summary*****--%></td>
		          <td>
				    <!--Modified by Manimaran to fix the Bug2741 -->
					<TextArea name="studySummary" rows=3 cols=93><%=studySummary%></TextArea>
			      </td>
			 <%}%>
			 </tr>
			 
			 
			 
			 <%--[Start] CTRP-22471 : Rows added for storing NCI Trial Identifier and NCT Number : Raviesh --%>
			 
			 <tr id="NCIRow" style="display: none;">
				<%
				if (hashPgCustFld.containsKey("NCITrialIdentifier")) {

				int fldNumNCITrialIdentifier = Integer.parseInt((String)hashPgCustFld.get("NCITrialIdentifier"));
				String stdNCITrialIdentifierMand = ((String)cdoPgField.getPcfMandatory().get(fldNumNCITrialIdentifier));
				String stdNCITrialIdentifierLabel = ((String)cdoPgField.getPcfLabel().get(fldNumNCITrialIdentifier));
				String stdNCITrialIdentifierAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumNCITrialIdentifier));

				disableStr ="";
				readOnlyStr ="";

				if(stdNCITrialIdentifierAtt == null) stdNCITrialIdentifierAtt ="";
  				if(stdNCITrialIdentifierMand == null) stdNCITrialIdentifierMand ="";

				if(!stdNCITrialIdentifierAtt.equals("0")) {

				if(stdNCITrialIdentifierLabel !=null){
				%>
				<td width="20%">
				 <%=stdNCITrialIdentifierLabel%>
				<%} else {%> <td width="20%">
				<%=LC.CTRP_DraftNCITrialId%><%--NCI Trial Identifier*****--%>
				<% }


			   if (stdNCITrialIdentifierMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomstudyncitrialidentifier">* </FONT>
		 	   <% }
			   %>

			   <%if(stdNCITrialIdentifierAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }
		         else if (stdNCITrialIdentifierAtt.equals("2") ) {
			       readOnlyStr = "readonly"; }
  		       %>
 			  </td>

              <td>
             	 <input type="text" name="NCITrialIdentifier" size = 40 MAXLENGTH = 35   <%=disableStr%> <%=readOnlyStr%>  value="<%=nciTrialIdentifier%>" />
              </td>

			  <% } else { %>
			  <input type="text" name="NCITrialIdentifier" style="visibility: hidden;" size = 40 MAXLENGTH = 35 value="<%=nciTrialIdentifier%>" />

			 <% }}  else {%>

				<td width="20%"><%=LC.CTRP_DraftNCITrialId%><%--NCI Trial Identifier*****--%></td>
			        <td>
			        <input type="text" name="NCITrialIdentifier" size = 40 MAXLENGTH = 35 value="<%=nciTrialIdentifier%>" />
			        </td>
			 <%}%>
			 </tr>
			 

			<tr>
				<%
				if (hashPgCustFld.containsKey("NCTNUmber")) {

				int fldNumNctNumber = Integer.parseInt((String)hashPgCustFld.get("NCTNUmber"));
				String stdNctNumberMand = ((String)cdoPgField.getPcfMandatory().get(fldNumNctNumber));
				String stdNctNumberLabel = ((String)cdoPgField.getPcfLabel().get(fldNumNctNumber));
				String stdNctNumberAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumNctNumber));

				disableStr ="";
				readOnlyStr ="";

				if(stdNctNumberAtt == null) stdNctNumberAtt ="";
  				if(stdNctNumberMand == null) stdNctNumberMand ="";

				if(!stdNctNumberAtt.equals("0")) {

				if(stdNctNumberLabel !=null){
				%>
				<td width="20%">
				 <%=stdNctNumberLabel%>
				<%} else {%> <td width="20%">
				<%=LC.CTRP_DraftNCTNumber%><%--NCT Number*****--%>
				<% }


			   if (stdNctNumberMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomstudynctnumber">* </FONT>
		 	   <% }
			   %>

			   <%if(stdNctNumberAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }
		         else if (stdNctNumberAtt.equals("2") ) {
			       readOnlyStr = "readonly"; }
  		       %>
 			  </td>

              <td>
              	<input type="text" name="nctNumber" id="nctNumber" size = 40 MAXLENGTH = 100 <%=disableStr%>  <%=readOnlyStr%> value="<%=nctNumber%>" />
              </td>

			  <% } else { %>
				<input type="text" name="nctNumber" id="nctNumber" style="visibility: hidden;" size = 40 MAXLENGTH = 100 value="<%=nctNumber%>" />
			 <% }}  else {%>

				<td width="20%"><%=LC.CTRP_DraftNCTNumber%><%--NCT Number*****--%></td>
			        <td>
			        	<input type="text" name="nctNumber" id="nctNumber" size = 40 MAXLENGTH = 100 value="<%=nctNumber%>" />
			        </td>
			 <%}%>
			 </tr>
			 
			 <%--[END] CTRP-22471 : Rows added for storing NCI Trial Identifier and NCT Number : Raviesh --%>
			 
			 
			 
			 
				 <tr>
				        <td colspan=2 align="center"> <%=MC.M_DoYouWant_InfoToPublic%><%--Do you want information in this section to be available to the public?*****--%>
					&nbsp;&nbsp;
					        <% if (stdSecRights[secCount] == '1') { %>
						        <input type="Radio" name="studyPubFlag1" value='1' Checked><%=LC.L_Yes%><%--Yes*****--%>
						        <input type="Radio" name="studyPubFlag1" value='0' ><%=LC.L_No%><%--No*****--%>
					        <%} else if (stdSecRights[secCount] == '0') {
					        %>
						        <input type="Radio" name="studyPubFlag1" value='1'><%=LC.L_Yes%><%--Yes*****--%>
						        <input type="Radio" name="studyPubFlag1" value='0' Checked><%=LC.L_No%><%--No*****--%>
					       <%} else {%>
						        <input type="Radio" name="studyPubFlag1" value='1'><%=LC.L_Yes%><%--Yes*****--%>
						        <input type="Radio" name="studyPubFlag1" value='0'><%=LC.L_No%><%--No*****--%>
					        <%} secCount++; %>
					        <input type=hidden name=studyPubFlag1 value=''>
							&nbsp; <A href="#"><img src="../images/jpg/help.jpg" border="0"	onmouseover="return overlib('<%=StringUtil.escapeSpecialCharJS(publicVsNonpublicStr)%>',CAPTION,'<%=LC.L_PublicNonPublicInfo%>',LEFT, ABOVE);" onmouseout="return nd();" alt=""></img></A>
						</td>
					</tr>
			    </table>

				<P class = "sectionHeadingsFrm"> <%=LC.L_Study_Details%><%--Study Details*****--%> </P>
				<table width="100%" cellspacing="2" cellpadding="2" border="0" class="basetbl midAlign">


				<tr>
				<%
				if (hashPgCustFld.containsKey("agentordevice")) {

				int fldNumAgentDev = Integer.parseInt((String)hashPgCustFld.get("agentordevice"));
				String agentMand = ((String)cdoPgField.getPcfMandatory().get(fldNumAgentDev));
				String agentLable = ((String)cdoPgField.getPcfLabel().get(fldNumAgentDev));
				String agentAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumAgentDev));

				disableStr ="";
				readOnlyStr ="";

				if(agentAtt == null) agentAtt ="";
  				if(agentMand == null) agentMand ="";

				if(!agentAtt.equals("0")) {

				if(agentLable !=null){
				%>
				<td width="20%">
				<%=agentLable%>
				<%} else {%> <td width="20%">
				 <%=LC.L_AgentOrDevice%><%--Agent/Device*****--%>
				<% }


			   if (agentMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomagent">* </FONT>
		 	   <% }
			   %>

			   <%if(agentAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }
		         else if (agentAtt.equals("2") ) {
			       readOnlyStr = "readonly"; }
  		       %>
 			  </td>

			  <td>  <input type="text" name="studyProduct" size = 40 MAXLENGTH = 100  <%=disableStr%>  <%=readOnlyStr%>   value="<%=studyProduct%>" >    </td>

			  <% }  else { %>

			  <input type="hidden" name="studyProduct" size = 40 MAXLENGTH = 100  value="<%=studyProduct%>" >

			 <% } }  else {%>

			 <td width="20%"> <%=LC.L_AgentOrDevice%><%--Agent/Device*****--%>  </td>
			 <td>
			       <input type="text" name="studyProduct" size = 40 MAXLENGTH = 100 value="<%=studyProduct%>" >
			 </td>
			 <%}%>
			 </tr>



		<tr>

       <%if (hashPgCustFld.containsKey("division")) {
			int fldNumDiv = Integer.parseInt((String)hashPgCustFld.get("division"));
			String divMand = ((String)cdoPgField.getPcfMandatory().get(fldNumDiv));
			String divLable = ((String)cdoPgField.getPcfLabel().get(fldNumDiv));
			divAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumDiv));

			if(divAtt == null) divAtt ="";
			if(divMand == null) divMand ="";

			if(!divAtt.equals("0")) {
			if(divLable !=null){
			%><td width="20%">
			<%=divLable%>
			<%} else {%> <td width="20%">
			<%=LC.L_Division%><%--Division*****--%>
			<%}

			if (divMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomdivision">* </FONT>
			<% }
		 %>
      </td>
      <td>
		<%=studyDivision%>
		<%if(divAtt.equals("1")) {%>
			<input type="hidden" name="studyDivision" value=""> 
		<%} else if(divAtt.equals("2")) {%><!--KM-->
	 		<input type="hidden" name="studyDivision" value="<%=studyDiv%>">
		 <%}%>
     </td>

	 <%} else { %>

	 <input type="hidden" name="studyDivision" value=<%=studyDiv%> >

	 <%}} else {%>
	  <td width="20%"> <%=LC.L_Division%><%--Division*****--%> </td>
	  <td> <%=studyDivision%> </td>

	 <% }%>

     </tr>



     <tr>

	 <%     if (hashPgCustFld.containsKey("therapeuticarea")) {
			int fldNumTarea = Integer.parseInt((String)hashPgCustFld.get("therapeuticarea"));
			String tareaMand = ((String)cdoPgField.getPcfMandatory().get(fldNumTarea));
			String tareaLable = ((String)cdoPgField.getPcfLabel().get(fldNumTarea));
			tareaAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumTarea));

			if(tareaAtt == null) tareaAtt ="";
			if(tareaMand == null) tareaMand ="";

			if(!tareaAtt.equals("0")) {
			if(tareaLable !=null){
			%><td width="20%">
			<%=tareaLable%>
			<%} else {%> <td width="20%">
			<%=LC.L_Therapeutic_Area%><%--Therapeutic Area*****--%>
			<%}

			if (tareaMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomtarea">* </FONT>
			<% }
	  %>
      </td>
      <td>
      	 <span id = "span_tarea"> <%=studyTArea%> </span>
		 <%if(tareaAtt.equals("1")) {%>
		 	<input type="hidden" name="studyTArea" value="">
		 <%} else if(tareaAtt.equals("2")){%>
		 	<input type="hidden" name="studyTArea" value="<%=studyB.getStudyTArea()%>">
		 <%}%>
      </td>

	  <%} else if(tareaAtt.equals("0")) {%>

	  <input type="hidden" name="studyTArea" value="<%=studyB.getStudyTArea()%>"  > <!--KM-->

	 <%}} else {%>
	   <td width="20%"> <%=LC.L_Therapeutic_Area%><%--Therapeutic Area*****--%> <FONT class="Mandatory" id="mandtarea">* </FONT> </td>
	   <td><span id = "span_tarea"> <%=studyTArea%> </span></td>
	 <% }%>

	 </tr>


				<tr>
				<%
				if (hashPgCustFld.containsKey("diseasesite")) {

				int fldNumDiseaseSite = Integer.parseInt((String)hashPgCustFld.get("diseasesite"));
				String disSiteMand = ((String)cdoPgField.getPcfMandatory().get(fldNumDiseaseSite));
				String disSiteLable = ((String)cdoPgField.getPcfLabel().get(fldNumDiseaseSite));
				String disSiteAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumDiseaseSite));

				disableStr ="";
				readOnlyStr ="";

				if(disSiteAtt == null) disSiteAtt ="";
  				if(disSiteMand == null) disSiteMand ="";

				if(!disSiteAtt.equals("0")) {

				if(disSiteLable !=null){
				%>
				<td width="20%">
				<%=disSiteLable%>
				<%} else {%> <td width="20%">
				 <%=LC.L_Disease_Site%><%--Disease Site*****--%>
				<% }


			   if (disSiteMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomdissite">* </FONT>
		 	   <% }
			   %>

			   <%if(disSiteAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }

  		       %>
 			  </td>


			  <td><input type="text" name="disSitename" size="20" value="<%=disSitename%>" readonly   <%=disableStr%>  >&nbsp;&nbsp;

			  <%if(!disSiteAtt.equals("1") && !disSiteAtt.equals("2")) { %>
			  <a href="#" onClick="selectDisease(document.study)"><%=LC.L_Select_DiseaseSites%><%--SELECT DISEASE SITE(S)*****--%></a>  </td>
			  <input type="hidden" name="disSiteid" value="<%=disSiteid%>">

			  <% }} else { %>
			 <input type="hidden" name="disSiteid" value="<%=disSiteid%>">

			 <% } }  else { %>

			  <td width="20%"> <%=LC.L_Disease_Site%><%--Disease Site*****--%> </td>
			  <td><input type="text" name="disSitename" size="20" value="<%=disSitename%>" readonly>&nbsp;&nbsp;<a href="#" onClick="selectDisease(document.study)"><%=LC.L_Select_DiseaseSites%><%--SELECT DISEASE SITE(S)*****--%></a>  </td>
			 	<input type="hidden" name="disSiteid" value="<%=disSiteid%>">
			 <%}%>
			 </tr>



			<tr>
				<%
				if (hashPgCustFld.containsKey("specificsites")) {

				int fldNumSpecsites = Integer.parseInt((String)hashPgCustFld.get("specificsites"));
				String specSiteMand = ((String)cdoPgField.getPcfMandatory().get(fldNumSpecsites));
				String specSiteLable = ((String)cdoPgField.getPcfLabel().get(fldNumSpecsites));
				String specSiteAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumSpecsites));

				disableStr ="";
				readOnlyStr ="";

				if(specSiteAtt == null) specSiteAtt ="";
  				if(specSiteMand == null) specSiteMand ="";

				if(!specSiteAtt.equals("0")) {

				if(specSiteLable !=null){
				%>
				<td width="20%">
				<%=specSiteLable%>
				<%} else {%> <td width="20%">
				 <%=LC.L_Specific_Sites%><%--Specific Sites*****--%>
				<% }


			   if (specSiteMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomspecsite">* </FONT>
		 	   <% }
			   %>

			   <%if(specSiteAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }
 		       %>
 			  </td>

				<td><input type="text" name="ICDcode1" size="20" value="<%=ICDCode1%>" readonly  <%=disableStr%> >&nbsp;&nbsp;


				<%if(!specSiteAtt.equals("1") && !specSiteAtt.equals("2")) {%>
				<a href="#" onClick="openWinICD('ICDcode1','<%=lkpId%>','<%=lkpColumn%>')"><%=LC.L_Select_Site1%><%--SELECT SITE 1*****--%></a>
   			    <%}%>


			   </td></tr>
			   <tr>
					<td width="20%">&nbsp;</td>
				<td><input type="text" name="ICDcode2" size="20"
					value="<%=ICDCode2%>" readonly  <%=disableStr%> >&nbsp;&nbsp;

				<%if(!specSiteAtt.equals("1") && !specSiteAtt.equals("2")) {%>
				<a href="#" onClick="openWinICD('ICDcode2','<%=lkpId%>','<%=lkpColumn%>')"><%=LC.L_Select_Site2%><%--SELECT SITE 2*****--%></a></td>
				 <%}%>


			  <% } else { %>
			  <input type="hidden" name="ICDcode1" size="20" value="<%=ICDCode1%>" >
			  <input type="hidden" name="ICDcode2" size="20" value="<%=ICDCode2%>" >

			 <% }}  else {%>

					  <!--Added by Manimaran for September Enhancement2 to select the ICD codes.-->
					<td width="20%"> <%=LC.L_Specific_Sites%><%--Specific Sites*****--%> </td>
					<td><input type="text" name="ICDcode1" size="20" value="<%=ICDCode1%>" readonly>&nbsp;&nbsp;<a href="#" onClick="openWinICD('ICDcode1','<%=lkpId%>','<%=lkpColumn%>')"><%=LC.L_Select_Site1%><%--SELECT SITE 1*****--%></a>
				    </td></tr>
			 <tr>

					<td width="20%">&nbsp;</td>
					<td><input type="text" name="ICDcode2" size="20"
					value="<%=ICDCode2%>" readonly>&nbsp;&nbsp;<a href="#" onClick="openWinICD('ICDcode2','<%=lkpId%>','<%=lkpColumn%>')"><%=LC.L_Select_Site2%><%--SELECT SITE 2*****--%></a></td>

			 <%}%>
			 </tr>

			 <!--<tr>
				    	<td width="200">ICD-9 Code</td>
				        <td><input type="text" name="ICDcode1" size="10" " >
				        <input type="text" name="ICDcode2" size="10" " >
				        <input type="text" name="ICDcode3" size="10" " >
				        <a href="#" onClick="">Select ICD-9</a></td>
				    </tr>-->
				    <!--<tr>
						<td width="20%">Local Sample Size </td>
				        <td>
					        <input type="text" name="studySize" size = 15 value="<%=studySize%>" >
				        </td>
   	         </tr>-->



			<tr>
				<%
				if (hashPgCustFld.containsKey("nss")) {

				int fldNumNss = Integer.parseInt((String)hashPgCustFld.get("nss"));
				String nssMand = ((String)cdoPgField.getPcfMandatory().get(fldNumNss));
				String nssLable = ((String)cdoPgField.getPcfLabel().get(fldNumNss));
				String nssAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumNss));

				disableStr ="";
				readOnlyStr ="";

				if(nssAtt == null) nssAtt ="";
  				if(nssMand == null) nssMand ="";

				if(!nssAtt.equals("0")) {

				if(nssLable !=null){
				%>
				<td width="20%">
				<%=nssLable%>
				<%} else {%> <td width="20%">
				<%=LC.L_National_SampleSize%><%--National Sample Size*****--%>
				<% }


			   if (nssMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomnss">* </FONT>
		 	   <% }
			   %>

			   <%if(nssAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }
		         else if (nssAtt.equals("2") ) {
			       readOnlyStr = "readonly"; }
  		       %>
 			  </td>


			  <td>  <input type="text" name="nStudySize" size = 20 maxlength="10" value="<%=nStudySize%>"  <%=readOnlyStr%> <%=disableStr%> >&nbsp; <%-- YK BUG#5564 --%>

		      <%if(!nssAtt.equals("1") && !nssAtt.equals("2")) {%>
			  <A href="#" onClick="openLSampleSize('<%=studyId%>','<%=mode%>');"><%=LC.L_Local_SampleSize%><%--LOCAL SAMPLE SIZE*****--%></A></td>
			  <%}%>

			  <% } else {  %>

			  <input type="hidden" name="nStudySize" size = 20 value="<%=nStudySize%>" >

			  <% }}  else {%>

			    <td width="20%"><%=LC.L_National_SampleSize%><%--National Sample Size*****--%> </td>
				<td>
					        <input type="text" name="nStudySize" size = 20 maxlength="10" value="<%=nStudySize%>" >&nbsp; <%-- YK BUG#5564 --%>
				        <A href="#" onClick="openLSampleSize('<%=studyId%>','<%=mode%>');"><%=LC.L_Local_SampleSize%><%--LOCAL SAMPLE SIZE*****--%></A></td>
			 <%}%>
			 </tr>



				<%

				String durationAtt = "";
				if (hashPgCustFld.containsKey("studyduration")) {
				int fldNumDuration = Integer.parseInt((String)hashPgCustFld.get("studyduration"));
				durationAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumDuration));
				if (durationAtt == null) durationAtt ="";
				}

				%>

				 <%
					if(durationAtt.equals("1"))
				 		durationUnit.append("<SELECT NAME=durationUnit disabled id=durunit>") ;
				    else
				 		durationUnit.append("<SELECT NAME=durationUnit id=durunit>") ;


					if(studyDurationUnit != null && (!studyDurationUnit.equals("null")) ){

						if (studyDurationUnit.equals("days")) {
							durationUnit.append("<OPTION value=days SELECTED>"+LC.L_Days_Lower/*days*****/+"</OPTION>");
						} else {
							durationUnit.append("<OPTION value = days>"+LC.L_Days_Lower/*days*****/+"</OPTION>");
						}
						if (studyDurationUnit.equals("weeks")) {
							durationUnit.append("<OPTION value = weeks SELECTED>"+LC.L_Weeks_Lower/*weeks*****/+"</OPTION>");
						} else {
							durationUnit.append("<OPTION value = weeks>"+LC.L_Weeks_Lower/*weeks*****/+"</OPTION>");
						}
						if (studyDurationUnit.equals("months")) {
							durationUnit.append("<OPTION value =months SELECTED>"+LC.L_Months_Lower/*months*****/+"</OPTION>");
						} else {
							durationUnit.append("<OPTION value = months>"+LC.L_Months_Lower/*months*****/+"</OPTION>");
						}
						if (studyDurationUnit.equals("years")) {
							durationUnit.append("<OPTION value = years SELECTED>"+LC.L_Years_Lower/*years*****/+"</OPTION>");
						} else {
							durationUnit.append("<OPTION value = years>"+LC.L_Years_Lower/*years*****/+"</OPTION>");
						}
						if (studyDurationUnit.equals("")) {
							durationUnit.append("<OPTION value='' SELECTED> "+LC.L_Select_AnOption/*Select an Option*****/+" </OPTION>");
						}
					} else {
						durationUnit.append("<OPTION value = days>"+LC.L_Days_Lower/*days*****/+"</OPTION>");
						durationUnit.append("<OPTION value = weeks>"+LC.L_Weeks_Lower/*weeks*****/+"</OPTION>");
						durationUnit.append("<OPTION value = months>"+LC.L_Months_Lower/*months*****/+"</OPTION>");
						durationUnit.append("<OPTION value = years>"+LC.L_Years_Lower/*years*****/+"</OPTION>");
						durationUnit.append("<OPTION value='' SELECTED> "+LC.L_Select_AnOption/*Select an Option*****/+" </OPTION>");
					}
					durationUnit.append("</SELECT>");
					%>



					<tr>
				<%
				if (hashPgCustFld.containsKey("studyduration")) {

				int fldNumDuration = Integer.parseInt((String)hashPgCustFld.get("studyduration"));
				String durationMand = ((String)cdoPgField.getPcfMandatory().get(fldNumDuration));
				String durationLable = ((String)cdoPgField.getPcfLabel().get(fldNumDuration));
				durationAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumDuration));

				disableStr ="";
				readOnlyStr ="";

				if(durationAtt == null) durationAtt ="";
  				if(durationMand == null) durationMand ="";

				if(!durationAtt.equals("0")) {

				if(durationLable !=null){
				%>
				<td width="20%">
				<%=durationLable%>
				<%} else {%> <td width="20%">
				 <%=LC.L_Study_Duration%><%--<%=LC.Std_Study%> Duration*****--%>
				<% }


			   if (durationMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomduration">* </FONT>
		 	   <% }
			   %>

			   <%if(durationAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }
		         else if (durationAtt.equals("2") ) {
			       readOnlyStr = "readonly"; }
  		       %>
 			  </td>


			  <td>
 	          <input type="text" name="studyDuration" id="stdDuration" size = 20 MAXLENGTH = 10    <%=disableStr%>  <%=readOnlyStr%>   value="<%=studyDuration%>" >
			  <%=durationUnit%>
			  </td>

			  <% } else { %>

	  	      <input type="hidden" name="studyDuration" size = 20 MAXLENGTH = 10  value="<%=studyDuration%>" >
			  <input type="hidden" name="durationUnit"  value= "<%=studyDurationUnit%>" >


			 <% }}  else {%>

			   <td width="20%"> <%=LC.L_Study_Duration%><%--<%=LC.Std_Study%> Duration*****--%> </td>
			   <td>
				        <input type="text" name="studyDuration" id="stdDuration" size = 20 MAXLENGTH = 10 value="<%=studyDuration%>" >
				        <%=durationUnit%>
				</td>
			 <%}%>
			 </tr>


				<tr>
				<%
				if (hashPgCustFld.containsKey("estimatedbegindt")) {

				int fldNumEstDt = Integer.parseInt((String)hashPgCustFld.get("estimatedbegindt"));
				String estDtMand = ((String)cdoPgField.getPcfMandatory().get(fldNumEstDt));
				String estDtLable = ((String)cdoPgField.getPcfLabel().get(fldNumEstDt));
				String estDtAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumEstDt));

				disableStr ="";
				readOnlyStr ="";

				if(estDtAtt == null) estDtAtt ="";
  				if(estDtMand == null) estDtMand ="";

				if(!estDtAtt.equals("0")) {

				if(estDtLable !=null){
				%>
				<td width="20%">
				<%=estDtLable%>
				<%} else {%> <td width="20%">
				 <%=LC.L_Estimated_BeginDate%><%--Estimated Begin Date*****--%>
				<% }


			   if (estDtMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomestdt">* </FONT>
		 	   <% }
			   %>

			   <%if(estDtAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }
		         else if (estDtAtt.equals("2") ) {
			       readOnlyStr = "readonly"; }
  		       %>
 			</td>
			<%-- INF-20084 Datepicker-- AGodara --%>
			<td colspan=1>
			<%if (StringUtil.isEmpty(estDtAtt)){ %>
				<input type="text" id="studyEstBgnDate" name="studyEstBgnDate" class="datefield" size = 20 MAXLENGTH = 20 value ="<%=studyEstBgnDate%>">
			<%} else if(estDtAtt.equals("1") || estDtAtt.equals("2")) {%>
				<input type="text" id="studyEstBgnDate" name="studyEstBgnDate" <%=disableStr%> <%=readOnlyStr%> size = 20 MAXLENGTH = 20 value="<%=studyEstBgnDate%>">
			<%}%>
			</td>
		  <%}else { %>
				<input type="hidden" id="studyEstBgnDate" name="studyEstBgnDate" size = 20  MAXLENGTH = 20 value="<%=studyEstBgnDate%>">
		<%}}else{%>
				<td width="20%"> <%=LC.L_Estimated_BeginDate%><%--Estimated Begin Date*****--%></td>
			<td colspan=1>
				<input type="text" id="studyEstBgnDate" name="studyEstBgnDate" class="datefield" size = 20 MAXLENGTH = 20 value="<%=studyEstBgnDate%>" READONLY>
			</td>
		<%}%>
		 </tr>



<!--					<tr height="10">
					    <td></td>
						<td>
							<A href=# onClick="return fnShowCalendar(document.study.studyEstBgnDate)"><input type="image" src="./images/calendar.jpg" align="absmiddle" border="0"></A>
					    </td>
					</tr> -->
					<tr>
						<td colspan=2 align="center"> <%=MC.M_DoYouWant_InfoToPublic%><%--Do you want information in this section to be available to the public?*****--%>
					&nbsp;&nbsp;
					        <% if (stdSecRights[secCount] == '1') { %>
						        <input type="Radio" name="studyPubFlag2" value='1' Checked><%=LC.L_Yes%><%--Yes*****--%>
						        <input type="Radio" name="studyPubFlag2" value='0'><%=LC.L_No%><%--No*****--%>
					        <%} else if (stdSecRights[secCount] == '0') {  %>
						        <input type="Radio" name="studyPubFlag2" value='1'><%=LC.L_Yes%><%--Yes*****--%>
						        <input type="Radio" name="studyPubFlag2" value='0' Checked><%=LC.L_No%><%--No*****--%>
					       <% } else {%>
						        <input type="Radio" name="studyPubFlag2" value='1'><%=LC.L_Yes%><%--Yes*****--%>
					    	    <input type="Radio" name="studyPubFlag2" value='0'><%=LC.L_No%><%--No*****--%>
					        <%} secCount++; %>
      					        <input type=hidden name=studyPubFlag2 value=''>
							&nbsp; <A href="#"><img src="../images/jpg/help.jpg" border="0"	onmouseover="return overlib('<%=StringUtil.escapeSpecialCharJS(publicVsNonpublicStr)%>',CAPTION,'<%=LC.L_PublicNonPublicInfo%>',LEFT, ABOVE);" onmouseout="return nd();" alt=""></img></A>
						</td>
			      	</tr>
	    		</table>
    			<P class = "sectionHeadingsFrm"> <%=LC.L_Study_Design%><%--<%=LC.Std_Study%> Design*****--%> </P>
			    <table width="100%" cellspacing="2" cellpadding="2" border="0" class="basetbl midAlign">


				<tr>

			   <%if (hashPgCustFld.containsKey("stdphase")) {
					int fldNumPhase = Integer.parseInt((String)hashPgCustFld.get("stdphase"));
					String phaseMand = ((String)cdoPgField.getPcfMandatory().get(fldNumPhase));
					String phaseLable = ((String)cdoPgField.getPcfLabel().get(fldNumPhase));
					phaseAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumPhase));

					if(phaseAtt == null) phaseAtt ="";
					if(phaseMand == null) phaseMand ="";

					if(!phaseAtt.equals("0")) {
					if(phaseLable !=null){
					%><td width="20%">
					<%=phaseLable%>
					<%} else {%> <td width="20%">
					<%=LC.L_Phase%><%--Phase*****--%>
					<%}

					if (phaseMand.equals("1")) {
						%>
					   <FONT class="Mandatory" id="pgcustomphase">* </FONT>
					<% }
				   %>
			    </td>
			   <td>
				<%=studyPhase%>
				<%if(phaseAtt.equals("1")) {%>
					<input type="hidden" name="studyPhase" value="">
				<%} else if(phaseAtt.equals("2")) {%>
					<input type="hidden" name="studyPhase" value="<%=studyB.getStudyPhase()%>">
				<%}%>
			   </td>

			   <%} else {%>

			  <input type="hidden" name="studyPhase" value="<%=studyB.getStudyPhase()%>">


			  <%}} else {%>
				 <td width="20%"> <%=LC.L_Phase%><%--Phase*****--%> <FONT class="Mandatory" id="mandphase">* </FONT> </td>
				 <td> <%=studyPhase%> </td>
			  <% }%>

			  </tr>


		<tr>

        <%if (hashPgCustFld.containsKey("researchtype")) {
	  		int fldNumResearch = Integer.parseInt((String)hashPgCustFld.get("researchtype"));
			String researchMand = ((String)cdoPgField.getPcfMandatory().get(fldNumResearch));
			String researchLable = ((String)cdoPgField.getPcfLabel().get(fldNumResearch));
			researchAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumResearch));

			if(researchAtt == null) researchAtt ="";
			if(researchMand == null) researchMand ="";

			if(!researchAtt.equals("0")) {
			if(researchLable !=null){
			%><td width="20%">
			<%=researchLable%>
			<%} else {%> <td width="20%">
			<%=LC.L_Research_Type%><%--Research Type*****--%>
			<%}

			if (researchMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomresearchtype">* </FONT>
			<% }
		 %>
        </td>
        <td>
		  <%=studyResType%>
		  <%if(researchAtt.equals("1")) {%>
		  	<input type="hidden" name="studyResType" value="">
		  <%} else if(researchAtt.equals("2")) {%>
			<input type="hidden" name="studyResType" value="<%=studyB.getStudyResType()%>"> 
		  <%}%>
        </td>

		 <%} else  {%>

		 <input type="hidden" name="studyResType" value="<%=studyB.getStudyResType()%>"> <!--KM-->


		 <%}} else {%>
		 <td width="20%"> <%=LC.L_Research_Type%><%--Research Type*****--%> </td>
		 <td> <%=studyResType%> </td>

		 <% }%>

		 </tr>


		<tr>

        <%if (hashPgCustFld.containsKey("studyscope")) {
	  		int fldNumScope = Integer.parseInt((String)hashPgCustFld.get("studyscope"));
			String scopeMand = ((String)cdoPgField.getPcfMandatory().get(fldNumScope));
			String scopeLable = ((String)cdoPgField.getPcfLabel().get(fldNumScope));
			scopeAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumScope));

			if(scopeAtt == null) scopeAtt ="";
			if(scopeMand == null) scopeMand ="";

			if(!scopeAtt.equals("0")) {
			if(scopeLable !=null){
			%><td width="20%">
			<%=scopeLable%>
			<%} else {%> <td width="20%">
			<%=LC.L_Study_Scope%><%--<%=LC.Std_Study%> Scope*****--%>
			<%}

			if (scopeMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomscope">* </FONT>
			<% }
		 %>
        </td>
        <td>
		  <%=studyScope%>
		  <%if(scopeAtt.equals("1")) {%>
		  	<input type="hidden" name="studyScope" value="">
		  <%} else if(scopeAtt.equals("2")) {%>
		  	<input type="hidden" name="studyScope" value=<%=studyB.getStudyScope()%> >
		  <%} %>
        </td>

		 <%} else if(scopeAtt.equals("0")) {%>

		 <input type="hidden" name="studyScope" value=<%=studyB.getStudyScope()%> > <!--KM-->


		 <%}} else {%>
		 <td width="20%"> <%=LC.L_Study_Scope%><%--<%=LC.Std_Study%> Scope*****--%></td>
		 <td><%=studyScope%></td>
		 <% }%>

		 </tr>



		<tr>

        <%if (hashPgCustFld.containsKey("studytype")) {
	  		int fldNumType = Integer.parseInt((String)hashPgCustFld.get("studytype"));
			String typeMand = ((String)cdoPgField.getPcfMandatory().get(fldNumType));
			String typeLable = ((String)cdoPgField.getPcfLabel().get(fldNumType));
			typeAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumType));

			if(typeAtt == null) typeAtt ="";
			if(typeMand == null) typeMand ="";

			if(!typeAtt.equals("0")) {
			if(typeLable !=null){
			%><td width="20%">
			<%=typeLable%>
			<%} else {%> <td width="20%">
			<%=LC.L_Study_Type%><%--<%=LC.Std_Study%> Type*****--%>
			<%}

			if (typeMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomtype">* </FONT>
			<% }
		 %>
        </td>
        <td>
		  <%=studyType%>
		  <%if(typeAtt.equals("1")) {%>
		  	<input type="hidden" name="studyType" value="">
		  <%} else if(typeAtt.equals("2")) {%>
		  	<input type="hidden" name="studyType" value="<%=studyB.getStudyType()%>" >
		  <%} %>
        </td>

		 <%} else {%>

		 <input type="hidden" name="studyType" value="<%=studyB.getStudyType()%>" >

		 <%}} else {%>
		<td width="20%"> <%=LC.L_Study_Type%><%--<%=LC.Std_Study%> Type*****--%> </td>
		<td> <%=studyType%> </td>
		 <% }%>

		 </tr>



				<tr>
				<%
				if (hashPgCustFld.containsKey("studylinkedto")) {

				int fldNumStdLinkedTo = Integer.parseInt((String)hashPgCustFld.get("studylinkedto"));
				String stdLinkedToMand = ((String)cdoPgField.getPcfMandatory().get(fldNumStdLinkedTo));
				String stdLinkedToLable = ((String)cdoPgField.getPcfLabel().get(fldNumStdLinkedTo));
				String stdLinkedToAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumStdLinkedTo));

				disableStr ="";
				readOnlyStr ="";

				if(stdLinkedToAtt == null) stdLinkedToAtt ="";
  				if(stdLinkedToMand == null) stdLinkedToMand ="";

				if(!stdLinkedToAtt.equals("0")) {

				if(stdLinkedToLable !=null){
				%>
				<td width="20%">
				<%=stdLinkedToLable%>
				<%} else {%> <td width="20%">
				 <%=LC.L_Std_Linked_To%><%--<%=LC.Std_Study%> Linked To*****--%>
				<% }


			   if (stdLinkedToMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomstdlinkedto">* </FONT>
		 	   <% }
			   %>

			   <%if(stdLinkedToAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }

  		       %>
 			  </td>


			  <td><Input type="text" name="studyAssocNum" size="10" value="<%=studyAssocNum%>" readonly  <%=disableStr%> >

			  <%if(!stdLinkedToAtt.equals("1") && !stdLinkedToAtt.equals("2")) {%>
			  <A href=# onClick="openLookup(<%=accId%>)"><%=LC.L_Select%><%--Select*****--%></A></td>
			  <Input type="hidden" name="studyAssoc" size="20" value="<%=studyAssoc%>">

			  <% }} else { %>
			  <Input type="hidden" name="studyAssocNum" size="10" value="<%=studyAssocNum%>" >
			  <Input type="hidden" name="studyAssoc" size="20" value="<%=studyAssoc%>">

			 <% } }  else {%>

				<td width="20%"><%=LC.L_Std_Linked_To%><%--<%=LC.Std_Study%> Linked To*****--%></td><td><Input type="text" name="studyAssocNum" size="10" value="<%=studyAssocNum%>" readonly><A href=# onClick="openLookup(<%=accId%>)"><%=LC.L_Select%><%--Select*****--%></A></td>
				<Input type="hidden" name="studyAssoc" size="20" value="<%=studyAssoc%>">

			 <%}%>
			 </tr>




        <tr>

        <%if (hashPgCustFld.containsKey("blinding")) {
	  		int fldNumBlinding = Integer.parseInt((String)hashPgCustFld.get("blinding"));
			String blindingMand = ((String)cdoPgField.getPcfMandatory().get(fldNumBlinding));
			String blindingLable = ((String)cdoPgField.getPcfLabel().get(fldNumBlinding));
			blindingAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumBlinding));

			if(blindingAtt == null) blindingAtt ="";
			if(blindingMand == null) blindingMand ="";

			if(!blindingAtt.equals("0")) {
			if(blindingLable !=null){
			%><td width="20%">
			<%=blindingLable%>
			<%} else {%> <td width="20%">
			<%=LC.L_Blinding%><%--Blinding*****--%>
			<%}

			if (blindingMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomblinding">* </FONT>
			<% }
		 %>
        </td>
        <td>
		   <%=studyBlinding%>
		   <%if(blindingAtt.equals("1")) {%>
		   	<input type="hidden" name="studyBlinding" value="">
		   <%} else if (blindingAtt.equals("2")){%>
		   	<input type="hidden" name="studyBlinding" value=<%=studyB.getStudyBlinding()%> >
		   <%} %>
        </td>

		 <%} else  {%>

		 <input type="hidden" name="studyBlinding" value=<%=studyB.getStudyBlinding()%> >

		 <%}} else {%>
		<td width="20%"> <%=LC.L_Blinding%><%--Blinding*****--%> </td>
		<td> <%=studyBlinding%> </td>
		 <% }%>

		 </tr>




		 <tr>

        <%if (hashPgCustFld.containsKey("randomization")) {
	  		int fldNumRandom = Integer.parseInt((String)hashPgCustFld.get("randomization"));
			String randomMand = ((String)cdoPgField.getPcfMandatory().get(fldNumRandom));
			String randomLable = ((String)cdoPgField.getPcfLabel().get(fldNumRandom));
			randomAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumRandom));

			if(randomAtt == null) randomAtt ="";
			if(randomMand == null) randomMand ="";

			if(!randomAtt.equals("0")) {
			if(randomLable !=null){
			%><td width="20%">
			<%=randomLable%>
			<%} else {%> <td width="20%">
			<%=LC.L_Randomization%><%--Randomization*****--%>
			<%}

			if (randomMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomrandom">* </FONT>
			<% }
		 %>
        </td>
        <td>
		    <%=studyRandom%>
		    <%if(randomAtt.equals("1")) {%>
		    	<input type="hidden" name="studyRandom" value="">
		    <%} else if (randomAtt.equals("2")){%>
		    	<input type="hidden" name="studyRandom" value="<%=studyB.getStudyRandom()%>" >
		    <%} %>
        </td>

		 <%} else  {%>

		 <input type="hidden" name="studyRandom" value="<%=studyB.getStudyRandom()%>" >

		 <%}} else {%>
		<td width="20%"> <%=LC.L_Randomization%><%--Randomization*****--%> </td>
		<td> <%=studyRandom%> </td>
		 <% }%>

		 </tr>



				        <td colspan=2 align="center"> <%=MC.M_DoYouWant_InfoToPublic%><%--Do you want information in this section to be available to the public?*****--%>
					&nbsp;&nbsp;
					        <% if (stdSecRights[secCount] == '1') { %>
						        <input type="Radio" name="studyPubFlag3" value='1' Checked><%=LC.L_Yes%><%--Yes*****--%>
						        <input type="Radio" name="studyPubFlag3" value='0'><%=LC.L_No%><%--No*****--%>
					        <%} else if (stdSecRights[secCount] == '0') {  %>
						        <input type="Radio" name="studyPubFlag3" value='1'><%=LC.L_Yes%><%--Yes*****--%>
						        <input type="Radio" name="studyPubFlag3" value='0' Checked><%=LC.L_No%><%--No*****--%>
					       <% } else {%>
						        <input type="Radio" name="studyPubFlag3" value='1'><%=LC.L_Yes%><%--Yes*****--%>
					    	    <input type="Radio" name="studyPubFlag3" value='0'><%=LC.L_No%><%--No*****--%>
					        <%} secCount++; %>
					        	<input type=hidden name=studyPubFlag3 value=''>
							&nbsp; <A href="#"><img src="../images/jpg/help.jpg" border="0"	onmouseover="return overlib('<%=StringUtil.escapeSpecialCharJS(publicVsNonpublicStr)%>',CAPTION,'<%=LC.L_PublicNonPublicInfo%>',LEFT, ABOVE);" onmouseout="return nd();" alt=""></img></A>
						</td>
					</tr>
				</table>
			    <P class = "sectionHeadingsFrm"> <%=LC.L_Sponsor_Information%><%--Sponsor Information*****--%> </P>
			    <%
				siteDao.getSiteValues(EJBUtil.stringToNum(acc));
				counter = 0;
				%>
			    <table width="100%" cellspacing="2" cellpadding="2" border="0" class="basetbl midAlign">
			<tr>

		<%


		//JM: 20Apr2010: Enh:SW-FIN6

		String sponsorNameId = "";
		sponsorNameId = studyB.getStudySponsorName();
		sponsorNameId=(sponsorNameId==null)?"":sponsorNameId;


		int fldSpnameLkp = 0;
		String spnameLkpMand = "";
		String spnameLkpLabel = "";
		String spnameLkpAtt = "";




		if (hashPgCustFld.containsKey("sponsorlookup")) {
	  		fldSpnameLkp = Integer.parseInt((String)hashPgCustFld.get("sponsorlookup"));
			spnameLkpAtt = ((String)cdoPgField.getPcfAttribute().get(fldSpnameLkp));
			if(spnameLkpAtt == null) spnameLkpAtt ="";
		}





		if (hashPgCustFld.containsKey("sponsorname")) {
	  		int fldNumSpname = Integer.parseInt((String)hashPgCustFld.get("sponsorname"));
			String spnameMand = ((String)cdoPgField.getPcfMandatory().get(fldNumSpname));
			String spnameLable = ((String)cdoPgField.getPcfLabel().get(fldNumSpname));
			spnameAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumSpname));


			if(spnameAtt == null) spnameAtt ="";
			if(spnameMand == null) spnameMand ="";

			if(!spnameAtt.equals("0")) {
				if(spnameLable !=null){
				%><td width="20%">
				 <%=spnameLable%> 
				<%} else {%> <td width="20%">
				 <%=LC.L_Sponsor_Name%><%--Sponsor Name*****--%> 
				<%}

				if (spnameMand.equals("1")) {
					%>
				   <FONT class="Mandatory" id="pgcustomspname">* </FONT>
				<%}%>
         		</td>
		        <td>
				   <%=studySponsorName%>
				   <%if(spnameAtt.equals("1")) {%>
				   		<input type="hidden" name="sponsor" value="">
				   <%} else if (randomAtt.equals("2")){%>
				    	<input type="hidden" name="sponsor" value="<%=studyB.getStudySponsorName()%>" >
				   <%} %>
		        </td>
		        <input type="hidden" name="fromSponsor" value="DD">

			<%}else{%>

				<input type="hidden" name="sponsor" value='<%=sponsorNameId%>' >

		 	<%}

		} else {%>
			<td width="20%"> <%=LC.L_Sponsor_Name%><%--Sponsor Name*****--%>  </td>
			<td> <%=studySponsorName%></td>
			<input type="hidden" name="fromSponsor" value="DD">
		<%
		}%>

	   </tr>



	<tr><%




		//JM: 20Apr2010: Enh:SW-FIN6: to add the Sponosor look up
		if ( (spnameAtt.equals("")||spnameAtt.equals("1")||spnameAtt.equals("2")) && (spnameLkpAtt.equals("1") || spnameLkpAtt.equals("")|| spnameLkpAtt.equals("2"))) {

			//Do not show sponsor look up ..........

		}else {


			//show sponsor look up ..........

			if (hashPgCustFld.containsKey("sponsorlookup")) {

				spnameLkpMand = ((String)cdoPgField.getPcfMandatory().get(fldSpnameLkp));
				spnameLkpLabel = ((String)cdoPgField.getPcfLabel().get(fldSpnameLkp));

				if(spnameLkpAtt == null) spnameLkpAtt ="";
				if(spnameLkpMand == null) spnameLkpMand ="";

				if(!spnameLkpAtt.equals("0")) {
					if(spnameLkpLabel !=null){%>
						<td width="20%">
						 <%=spnameLkpLabel%>
					<%
					} else {%>
						<td width="20%">
						 <%=LC.L_Sponsor_Name%><%--Sponsor Name*****--%> 
					<%
					}

					if (spnameLkpMand.equals("1")) {%>
					   <FONT class="Mandatory" id="pgcustomspname1">* </FONT>
					<%
					}
				  	%>
		         		</td>
		           <td>
				   <input type="text" name="sponsor1"   value='<%=(sponsorNameId.length()>0)?cdSponsorName.getCodeDesc(EJBUtil.stringToNum(sponsorNameId)):""%>' readonly>
				   <%
				   if(!spnameLkpAtt.equals("1") && !spnameLkpAtt.equals("2")) {%>
						<A href="#" onClick="openLookupSpnsr(document.study)"><%=LC.L_Select_Sponsor%><%--Select Sponsor*****--%> </A>
						<td><input type="hidden" name="sponsorpk" value='<%=sponsorNameId%>'></td>
		   		   <%
		   		   }
		   		   if( spnameLkpAtt.equals("1") || spnameLkpAtt.equals("2")) {%>
		   		   		<input type="hidden" name="sponsorpk" value='<%=sponsorNameId%>'> <%}%>
		           </td>
		           <input type="hidden" name="fromSponsor" value="LKP">

			 	<%
			 	}else{%>
			 		<td><input type="hidden" name="sponsorpk" value='<%=sponsorNameId%>' ></td>

			 	<%
			 	}

			} else {%>
				<td width="20%"> <%=LC.L_Sponsor_Name%><%--Sponsor Name*****--%>  </td>
				<td>
				<input type="text" name="sponsor1"   value='<%=(sponsorNameId.length()>0)?cdSponsorName.getCodeDesc(EJBUtil.stringToNum(sponsorNameId)):""%>' readonly>
				<A href="#" onClick="openLookupSpnsr(document.study)"><%=LC.L_Select_Sponsor%><%--Select Sponsor*****--%></A>
				<input type="hidden" name="sponsorpk" value='<%=sponsorNameId%>'>
				<input type="hidden" name="fromSponsor" value="LKP">
				</td>
			<%
			}

		}//end of else part
		%>

		</tr>



				<tr>
				<%
				if (hashPgCustFld.containsKey("ifothersponsor")) {

				int fldNumOthSponsor = Integer.parseInt((String)hashPgCustFld.get("ifothersponsor"));
				String othSponsorMand = ((String)cdoPgField.getPcfMandatory().get(fldNumOthSponsor));
				String othSponsorLable = ((String)cdoPgField.getPcfLabel().get(fldNumOthSponsor));
				String othSponsorAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumOthSponsor));

				disableStr ="";
				readOnlyStr ="";

				if(othSponsorAtt == null) othSponsorAtt ="";
  				if(othSponsorMand == null) othSponsorMand ="";

				if(!othSponsorAtt.equals("0")) {

				if(othSponsorLable !=null){
				%>
				<td width="20%">
				<%=othSponsorLable%>
				<%} else {%> <td width="20%">
				 <%=LC.L_If_Other%><%--If Other*****--%>
				<% }


			   if (othSponsorMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomothsponsor">* </FONT>
		 	   <% }
			   %>

			   <%if(othSponsorAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }
		         else if (othSponsorAtt.equals("2") ) {
			       readOnlyStr = "readonly"; }
  		       %>
 			  </td>

			   <td>
				<Input type=text name="studySponsor"  <%=disableStr%> <%=readOnlyStr%>  value="<%=studySponsorId%>"  >
			   </td>

			  <% } else { %>

			  <Input type=hidden name="studySponsor"   value="<%=studySponsorId%>"  >

 			  <% } }  else {%>

				 <td width="20%"> <%=LC.L_If_Other%><%--If Other*****--%> </td>
    				<td>
						<Input type=text name="studySponsor" value="<%=studySponsorId%>" >
				 </td>

			 <%}%>
			 </tr>




				<tr>
				<%
				if (hashPgCustFld.containsKey("sponsorid")) {

				int fldNumSponsorId = Integer.parseInt((String)hashPgCustFld.get("sponsorid"));
				String sponsorIdMand = ((String)cdoPgField.getPcfMandatory().get(fldNumSponsorId));
				String sponsorIdLable = ((String)cdoPgField.getPcfLabel().get(fldNumSponsorId));
				String sponsorIdAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumSponsorId));

				disableStr ="";
				readOnlyStr ="";

				if(sponsorIdAtt == null) sponsorIdAtt ="";
  				if(sponsorIdMand == null) sponsorIdMand ="";

				if(!sponsorIdAtt.equals("0")) {

				if(sponsorIdLable !=null){
				%>
				<td width="20%">
				<%=sponsorIdLable%>
				<%} else {%> <td width="20%">
				 <%=LC.L_Sponsor_Id%><%--Sponsor ID*****--%>
				<% }


			   if (sponsorIdMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomsponsorid">* </FONT>
		 	   <% }
			   %>

			   <%if(sponsorIdAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }
		         else if (sponsorIdAtt.equals("2") ) {
			       readOnlyStr = "readonly"; }
  		       %>
 			  </td>

			   <td>  <input type="text" name="studySponsorIdInfo"  maxlength="50"  <%=disableStr%> <%=readOnlyStr%> value="<%=studySponsorIdInfo%>">  </td>

			  <% } else { %>

			   <input type="hidden" name="studySponsorIdInfo"  maxlength="50"  value="<%=studySponsorIdInfo%>">

			  <% } }  else {%>

				 <td width="20%"> <%=LC.L_Sponsor_Id%><%--Sponsor ID*****--%></td>
						<td>   <!--Modified by Manimaran based on Code review -->
							<input type="text" name="studySponsorIdInfo"  maxlength="50" value="<%=studySponsorIdInfo%>">
						</td>

			 <%}%>
			 </tr>



			<tr>
				<%
				if (hashPgCustFld.containsKey("contact")) {

				int fldNumContact = Integer.parseInt((String)hashPgCustFld.get("contact"));
				String contactMand = ((String)cdoPgField.getPcfMandatory().get(fldNumContact));
				String contactLable = ((String)cdoPgField.getPcfLabel().get(fldNumContact));
				String contactAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumContact));

				disableStr ="";
				readOnlyStr ="";

				if(contactAtt == null) contactAtt ="";
  				if(contactMand == null) contactMand ="";

				if(!contactAtt.equals("0")) {

				if(contactLable !=null){
				%>
				<td width="20%">
				<%=contactLable%>
				<%} else {%> <td width="20%">
				<%=LC.L_Contact%><%--Contact*****--%>
				<% }


			   if (contactMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomcontact">* </FONT>
		 	   <% }
			   %>

			   <%if(contactAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }
		         else if (contactAtt.equals("2") ) {
			       readOnlyStr = "readonly"; }
  		       %>
 			  </td>

			  <td> 	<Input type=text name="studyContact" value="<%=studyContactId%>" <%=disableStr%> <%=readOnlyStr%> >   </td>

			  <% } else { %>

			  <Input type=hidden name="studyContact" value="<%=studyContactId%>" >


			 <% } }  else {%>

				 <td width="20%"> <%=LC.L_Contact%><%--Contact*****--%> </td>
				  <td>
							<Input type=text name="studyContact" value="<%=studyContactId%>" >
				  </td>

			 <%}%>
			 </tr>



			<tr>
				<%
				if (hashPgCustFld.containsKey("othinfo")) {

				int fldNumOthInfo = Integer.parseInt((String)hashPgCustFld.get("othinfo"));
				String othInfoMand = ((String)cdoPgField.getPcfMandatory().get(fldNumOthInfo));
				String othInfoLable = ((String)cdoPgField.getPcfLabel().get(fldNumOthInfo));
				String othInfoAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumOthInfo));

				disableStr ="";
				readOnlyStr ="";

				if(othInfoAtt == null) othInfoAtt ="";
  				if(othInfoMand == null) othInfoMand ="";

				if(!othInfoAtt.equals("0")) {

				if(othInfoLable !=null){
				%>
				<td width="20%">
				<%=othInfoLable%>
				<%} else {%> <td width="20%">
				<%=LC.L_Other_Information%><%--Other Information*****--%>
				<% }


			   if (othInfoMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomothinfo">* </FONT>
		 	   <% }
			   %>

			   <%if(othInfoAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }
		         else if (othInfoAtt.equals("2") ) {
			       readOnlyStr = "readonly"; }
  		       %>
 			  </td>

			  <td> 	<TextArea name="studyOtherInfo" rows=3 cols=50 MAXLENGTH = 2000 <%=disableStr%> <%=readOnlyStr%>  ><%=studyOtherInfo%> </TextArea>	     </td>


			  <% } else { %>
			  <TextArea name="studyOtherInfo" rows=3 cols=50 Style = "visibility:hidden" MAXLENGTH = 2000  ><%=studyOtherInfo%> </TextArea>

			  <% } }  else {%>

				<td width="20%"> <%=LC.L_Other_Information%><%--Other Information*****--%> </td>
			        <td>
							<TextArea name="studyOtherInfo"  rows=3 cols=50 MAXLENGTH = 2000 ><%=studyOtherInfo%></TextArea>
			    </td>

			 <%}%>
			 </tr>
			 <tr>					
					<td colspan=2 align="left">
					<br/>
					<!-- Modified for Bug#7962 : Raviesh -->	
					<!-- Modified for Bug#7969 : Akshi -->					   
								<jsp:include page="ctrpDraftSectNihGrant.jsp" flush="true">
									<jsp:param name="pageMode" value="E"/>
			  						<jsp:param name="studyId" value="<%=studyId%>"/> 				
								</jsp:include>	
					</td>			   			
					
			</tr>

					<tr>
				        <td colspan=2  rowspan=3 align="center"> <%=MC.M_DoYouWant_InfoToPublic%><%--Do you want information in this section to be available to the public?*****--%>
					&nbsp;&nbsp;
					        <% if (stdSecRights[secCount] == '1') {%>
						        <input type="Radio" name="studyPubFlag4" value='1' Checked><%=LC.L_Yes%><%--Yes*****--%>
						        <input type="Radio" name="studyPubFlag4" value='0'><%=LC.L_No%><%--No*****--%>
					        <%} else if (stdSecRights[secCount] == '0') {%>
						        <input type="Radio" name="studyPubFlag4" value='1'><%=LC.L_Yes%><%--Yes*****--%>
						        <input type="Radio" name="studyPubFlag4" value='0' Checked><%=LC.L_No%><%--No*****--%>
					       <% } else {%>
						        <input type="Radio" name="studyPubFlag4" value='1'><%=LC.L_Yes%><%--Yes*****--%>
					    	    <input type="Radio" name="studyPubFlag4" value='0'><%=LC.L_No%><%--No*****--%>
					        <%} secCount++; %>
   					        <input type=hidden name=studyPubFlag4 value=''>
							&nbsp; <A href="#"><img src="../images/jpg/help.jpg" border="0" onmouseover="return overlib('<%=StringUtil.escapeSpecialCharJS(publicVsNonpublicStr)%>',CAPTION,'<%=LC.L_PublicNonPublicInfo%>',LEFT, ABOVE);" onmouseout="return nd();" alt=""></img></A>
						</td>
					</tr>
		    	</table>

						<!--
						<P class = "sectionHeadings"> Participating Centers </P> -->
				<P class = "sectionHeadingsFrm"> <%=LC.L_Keywords%><%--Keywords*****--%></P>
				<table width="80%" cellspacing="2" cellpadding="2" border="0" class="basetbl midAlign">
	            	<!-- <tr>
	              		<td width="300"> Participating Centers </td>
	              		<td>
	                		<TextArea name="studyPartCenters" rows=3 cols=50 MAXLENGTH = 1000 ><%=studyPartCenters%></TextArea>
	              		</td>
	            	</tr>
					<tr>
	              		<td width="500" colspan=2> Do you want information in this section to be available to the public?
						</td>
					</tr>
			        <tr>
				        <td colspan=2>  -->
	              			<% //if (stdSecRights[secCount] == '1') {//Public %>
	              				<!-- <input type="Radio" name="studyPubFlag5" value='1' Checked>Yes
				            <%//} else if (stdSecRights[secCount] == '0') { //Non Public %>
					            <!-- <input type="Radio" name="studyPubFlag5" value='1' >Yes
					            <input type="Radio" name="studyPubFlag5" value='0' Checked>No  -->
				            <% //} else {%>
							    <!-- <input type="Radio" name="studyPubFlag5" value='1'>Yes
					            <input type="Radio" name="studyPubFlag5" value='0'>No  -->
				            <%//} secCount++;%>
	            			<!-- &nbsp; <A href="pubvsnonpub.htm" target="Information" onClick="openwin()">What is Public vs Non Public information?</A>
						</td>
					</tr> -->

			  		<!--<tr><td height =20></td>
					</tr>-->



				<tr>
				<%
				if (hashPgCustFld.containsKey("keywords")) {

				int fldNumKeywords = Integer.parseInt((String)hashPgCustFld.get("keywords"));
				String keywordsMand = ((String)cdoPgField.getPcfMandatory().get(fldNumKeywords));
				String keywordsLable = ((String)cdoPgField.getPcfLabel().get(fldNumKeywords));
				String keywordsAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumKeywords));

				disableStr ="";
				readOnlyStr ="";

				if(keywordsAtt == null) keywordsAtt ="";
  				if(keywordsMand == null) keywordsMand ="";

				if(!keywordsAtt.equals("0")) {

				if(keywordsLable !=null){
				%>
				<td width="25%">
				<%=keywordsLable%>
				<%} else {%> <td width="25%">
				<%=LC.L_Keywords%><%--Keywords*****--%>
				<% }


			   if (keywordsMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomkeywords">* </FONT>
		 	   <% }
			   %>

			   <%if(keywordsAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }
		         else if (keywordsAtt.equals("2") ) {
			       readOnlyStr = "readonly"; }
  		       %>
 			  </td>

			  <td>  <TextArea name="studyKeywrds" rows=2 cols = 50  MAXLENGTH = 500 <%=disableStr%> <%=readOnlyStr%> ><%=studyKeywrds%></TextArea>    </td>


			  <% } else { %>
			  <TextArea name="studyKeywrds" rows=2 cols = 50  Style = "visibility:hidden"  MAXLENGTH = 500  >	<%=studyKeywrds%> </TextArea>


			 <% } }  else {%>

				<td width="25%"> <%=LC.L_Keywords%><%--Keywords*****--%> </td>
	        	<td>
				     <TextArea name="studyKeywrds" rows=2 cols = 50  MAXLENGTH = 500><%=studyKeywrds%></TextArea>
			    </td>

			 <%}%>
			 </tr>


				<tr>
			            <td></td>
	        			<td ><p class="defComments"><%=MC.M_TipEtrSpfcSrch_SeprComma%><%--Tip: Enter specific words, which will help to search this trial. For Example leukemia,. You can add more than one keyword separated by a ',' (comma)*****--%>
						 </p></td>
			        </tr>
		        </table>
    			

    			<%
    			if (hasAccess)
								{%>

								<jsp:include page="submitBar.jsp" flush="true">
									<jsp:param name="displayESign" value="Y"/>
									<jsp:param name="formID" value="studyForm"/>
									<jsp:param name="showDiscard" value="N"/>
								</jsp:include>

						<% }%>
				 			</Form>
				 			</div>
				<%if (TAreaDisSiteFlag.equals(LC.Config_TAreaDisSite_Switch)){%>
					<script>
						var tAreaFld = document.getElementsByName("studyTArea")[0];
						if (tAreaFld){
							tAreaFld.onchange = function(e){
								var disSitenameFld = document.getElementsByName("disSitename")[0];
								disSitenameFld.value = '';
								var disSiteIdFld = document.getElementsByName("disSiteid")[0];
								disSiteIdFld.value = '';
							};
						}
					</script>
				<%}%>
		<%
		}else {
		//KM-#4099. Though the tab is hidden, we should not show blank page
		%>
			 <jsp:include page="accessdenied.jsp" flush="true"/>
			<%
		}
	}//end of if body for session
   	else
	{
	%>

	<jsp:include page="studytabs.jsp" flush="true">
	<jsp:param name="from" value='<%=from%>'/>
	<jsp:param name="selectedTab" value="<%=selectedTab%>"/>
	<jsp:param name="studyId" value="<%=studyId%>"/>
	</jsp:include>

		<jsp:include page="timeout.html" flush="true"/>
	<%
	}
	%>

	<div class = "myHomebottomPanel">
	    <jsp:include page="bottompanel.jsp" flush="true"/>
	</div>

</DIV>

<div class ="mainMenu" id="emenu">
  	<jsp:include page="getmenu.jsp" flush="true"/>
</div>


</body>

</html>

