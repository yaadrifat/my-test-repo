function includeJS(file)
{

  var script  = document.createElement('script');
  script.src  = file;
  script.type = 'text/javascript';
  script.defer = true;

  document.getElementsByTagName('head').item(0).appendChild(script);

}

/* include any js files here */

// Introduced for localization
includeJS('js/velos/velosUtil.js');
dojo.require("dojo.io.*");
dojo.require("dojo.event.*");
//dojo.require("dojo.widget.validate");

dojo.require("dojo.lfx.*");

var modField ="";
var msgTargets="";
var prevValue="";
var validateFlag="true";
var validationStat=0;
var disableSubmit="true";
var validationPassClass="";
var validationFailClass="";
var LYahoo=L_Yahoo;
var ProcYour_Req=M_ProcYour_Req;
var Saving_Data=L_Saving_Data;
var ErrSvg_Data=L_ErrSvg_Data;
var Invalid_Esign=L_Invalid_Esign;
var Valid_Esign=L_Valid_Esign;
var Total_Chars=L_Total_Chars;
var Total_Allowed=L_Total_Allowed;
var Data_SavedSucc=M_Data_SavedSucc;

VELOS=YAHOO.namespace("velos");
VELOS.gIndex=0;
VELOS.generateIndex=function()
{
 VELOS.gIndex=VELOS.gIndex + 1 ;
 return VELOS.gIndex;
}

				
var $E = YAHOO.util.Event,
$D = YAHOO.util.Dom,
$ = $D.get;

// Variable to capture if the control should be returned if no value change occured for target
//field. This will help to avoid unneccesary database hits
var valChangeReturn=1;

function ajaxvalidate(name,minLen,msgTarget,passMsg,failMsg,addFldStr,msgMode) {
    var target=""; 
	var dataset="";
	var targetValue="";
	msgTargets= msgTarget;
	if ((typeof(msgMode)=="undefined") && (msgMode==null))
	msgMode="";
	if (minLen.length==0) minLen=0;
	//if (!target) target = dojo.byId(name);
	
	var arrayOfComps=name.split(":");
    var module=arrayOfComps[0];
    modField=arrayOfComps[1];
	if (!target) target = document.getElementById(modField);
	
	if (!(typeof(target)=="undefined") && (target!=null)){
	if (target.value.length==0)
	{	 
		//enableSubmit("true");
		setMessageUsingInline("",modField,msgTargets,"","");
	 	prevValue=target.value;
	 	return;
	}
	if (minLen > 0){
		if ((target.value.length<minLen)&& (minLen>0) )
		{ 
			//enableSubmit("false");
			var paramArray = [minLen];
			setMessageUsingInline("false",modField,msgTargets,"",getLocalizedMessageString("M_MinLen_Is",paramArray));
		    /*setMessageUsingInline("false",modField,msgTarget,"","Minimum length is "+minLen);*****/
		   	prevValue=target.value;
		   	return;
		}
	}
	targetValue=target.value;
	}
	else {
	targetValue="";
	}
	
	//Set Default Messages if blank
	//122806 - Disabled for now. May enabled this in future
	/*if (passMsg.length==0)
	  passMsg="Validation Passed";
	if (failMsg.length==0)
	    failMsg="Validation Failed";*/
		
    
	 //var userId=dojo.byId("userId");
	   //Create Dataset to send to Validation controller
	   
   if (addFldStr.length>0)
   {
   	var tempFld=null;
   addFldArray=addFldStr.split(":");
   for (var i=0; i < addFldArray.length; i++) 
   {
   
    
   	tempFld=document.getElementById(addFldArray[i]);
	
	
	if (((typeof(tempFld)!="undefined")) && (tempFld!=null)) 
	{
	
		if (dataset.length>0)
		
		dataset=dataset+":"+tempFld.value;
		else
		{
		if (tempFld.value.length==0)
		dataset="null";
		else
		dataset=tempFld.value;
		}
	} else
	{
	 if (dataset.length>0) 
	 dataset=dataset+":"+addFldArray[i].replace('CONSTANT|','');
	 else 
	 dataset=addFldArray[i].replace('CONSTANT|','');
	} 
   }
    
   
   dataset=replaceSubstring(dataset,"null","");
   }
	
	if ((prevValue==targetValue) && (valChangeReturn==1))
	{
	   return ;	
	} else
	{
	valChangeReturn==1;	
	}
	if(name=="misc:eSigns" || name=="misc:eSignss" || name=="misc:eSign1")
		name="misc:eSign";
   //End Dataset Creation
        // Use dojo.io.bind() for remoting
    dojo.io.bind({
		   //Request Type
		   method : 'POST',
          // URL -  destination you want to send your request
          url: "../validate?module="+name+"&dataset="+dataset+"&id="+escape(targetValue),
          sync : true,
          // Callback function that will be invoked asynchronously
          load: function(type, data, evt){
		  	processRequest(data,modField,msgTargets,passMsg,failMsg,msgMode);
			
			},
         
          // Error handler function that will be invoked in case of an error
          error: function(type, error){ 
		  //alert("Critical Validation Error.Please contact Velos Support with this message."); 
		  },
         
          // Backward and forward button handling
          //backButton: function() { alert("back button pressed"); },
          //forwardButton: function() { alert("forward button pressed");},
         
          // Type of data you want to receive
          mimetype: "text/xml"         
    });     
	prevValue=targetValue; 		
	   
}



function processRequest(data,name,msgTarget,passMsg,failMsg,msgMode) {
            var message = data.getElementsByTagName("valid")[0].childNodes[0].nodeValue;
            var msgContent="";
            if (data.getElementsByTagName("valid")[0].childNodes[1])
            msgContent=data.getElementsByTagName("valid")[0].childNodes[1].childNodes[0].nodeValue;
			if (msgMode=="alert")
			setMessageAlert(message,name,msgTarget,passMsg,failMsg);
			else if (msgMode=="confirm")
			setMessageConfirmBox(message,name,msgTarget,passMsg,failMsg);
			else
			{
			if ((message=="false") && (failMsg.length==0))  failMsg=msgContent;
			if ((message=="true") && (passMsg.length==0))  passMsg=msgContent;
						 
		    setMessageUsingInline(message,name,msgTarget,passMsg,failMsg);
		    }

/*Disable the toggling of enable/diable of button for now.Enter key does not work on a page after aa image is disabled and then enabled.*/
		    
//		    enableSubmit(message);

}

function enableSubmit(flag)
{
	var submitBtn = document.getElementById("submit_btn");
	if (!(typeof(submitBtn)=="undefined") && (submitBtn!=null)) 
	{
		if (flag == "false") 
		{
		
		  submitBtn.disabled = true;
		  submitBtn.src="../images/jpg/Submit_D.gif";
		  if (jQuery && jQuery("#submit_btn")) {
			  jQuery("#submit_btn").button("disable");
		  }
		} else 
		{
		   submitBtn.src="../images/jpg/Submit.gif";
		   submitBtn.disabled = false;
		   if (jQuery && jQuery("#submit_btn")) {
			   jQuery("#submit_btn").button("enable");
		   }
		}
	}
}
function setMessage(msgTarget,message,className){
	
	var mdiv = dojo.byId(msgTarget);
	
	if (!(typeof(mdiv)=="undefined") && (mdiv!=null))
	{
		mdiv.className=className;
		mdiv.innerHTML=message;
	} 
	     
	
}

function setMessageUsingInline(message,name,msgTarget,passMsg,failMsg) {
    //mdiv = document.getElementById("userIdMessage");
//	alert(getValidationPassClass());
	var divValid=false;
	var mdiv = dojo.byId(msgTarget);
	if (mdiv) { 
	     divValid=true;
	    } else
	    {
	     var el=dojo.byId('div_msg');
	     if (!el) {
	     el=document.createElement('div');
			el.id='div_msg';
			//el.className="validation-fail"
			//el.innerHTML = failMsg;
			
			}// end for (!el)
			
			$D.insertAfter(el,name);
			mdiv=el;
			divValid=true;
			
	    }
	     
		 
	 if (message == "false") {
         if (divValid){ 
         if (failMsg.length>0){
	   mdiv.className="validation-fail";
	   mdiv.innerHTML = failMsg;
	   }
	   else
       		{
       		mdiv.className="hide";
       		mdiv.innerHTML="";
       		}
	   }
	   if (!(typeof(dojo.byId(name))=="undefined") && (dojo.byId(name)!=null))
	   dojo.byId(name).className="validation-failed";
	   }
	     else if (message == "true") {
		if (divValid){
		    if (passMsg.length>0) {
		  	mdiv.className=getValidationPassClass();
       		mdiv.innerHTML = passMsg
       		}
       		else
       		{
       		mdiv.className="hide";
       		mdiv.innerHTML="";
       		}
	   }
	   if (!(typeof(dojo.byId(name))=="undefined") && (dojo.byId(name)!=null))
	    dojo.byId(name).className="";
	   
    }
	else{
		if (divValid){
		  	mdiv.className="hide";
       		mdiv.innerHTML = "";
			}
			if (!(typeof(dojo.byId(name))=="undefined") && (dojo.byId(name)!=null))
			dojo.byId(name).className="";
	}
	
}
function setMessageConfirmBox(message,name,msgTarget,passMsg,failMsg) {
    //mdiv = document.getElementById("userIdMessage");
	
	 if (message == "false") {
         
	   
	  if (confirm(failMsg)) { 
	  validationStat=1;
//	  alert("After Success"+validationStat);
	   return true;
	   }
	   else
	   { 
//	   alert("Set Validationstat");
	   validationStat=-1;
//	   alert("After Set Validationstat"+validationStat);
	   return false;
	   }
	   
	   
	   }
	
	
}

function setMessageAlert(message,name,msgTarget,passMsg,failMsg) {
    //mdiv = document.getElementById("userIdMessage");
	
	 if (message == "false") {
      alert(failsMsg);
	  return false;
	   
	   }
	
	
}


 function setMessageUsingDOM(message) {
    // var userMessageElement = document.getElementById("userIdMessage");
    var userMessageElement =dojo.byId("ajaxmsg"); 
     var messageText;
	  if (message == "false") {
         userMessageElement.style.color = "red";
         userMessageElement.innerHTML=Invalid_Esign;/*userMessageElement.innerHTML="Invalid e-sign";*****/
		 
     } else {
         userMessageElement.style.color = "green";
         userMessageElement.innerHTML=Valid_Esign;/*userMessageElement.innerHTML="Valid e-sign";*****/
     }
	 }
	
	 
 function setToolTip()
	 {
	 	var connectFld="misc:eSign";
	 	tooltip = dojo.widget.createWidget("tooltip", {caption:'testing3',connectId:connectFld}); 
		  tooltip.setContent('<Font size="5"><A href=http://ww.yahoo.com">'+LYahoo+'</A></font>');/*tooltip.setContent('<Font size="5"><A href=http://ww.yahoo.com">yahoo</A></font>');*****/
		  document.body.appendChild(tooltip.domNode);
	 }
function preSubmit()
{ 
//	alert(getDisableSubmit());	    
    if (getDisableSubmit()!='false')
	{ 
	//alert("validateFlag"+validateFlag);
	if (validateFlag!='false')
	{ 
		 
	var submitBtn = document.getElementById("submit_btn");
	if (!(typeof(submitBtn)=="undefined") && (submitBtn!=null))
	{  
	      
		 submitBtn.disabled = true;
		  submitBtn.src="../images/jpg/Submit_D.gif";	
	}
	}
	}
	//alert("submitBtn.disabled -"+document.getElementById("submit_btn").disabled + " and submitBtn.src- "+document.getElementById("submit_btn").src);
	setValidateFlag('true');

}	
function onKeyCounter(name,MAXCHAR,msgTarget)
{
  var fieldName=dojo.byId(name); 
   if (fieldName.value.length>MAXCHAR) { 
    setMessageUsingInline("false",name,msgTarget,"",Total_Chars+":"+fieldName.value.length+" "+Total_Allowed+":"+MAXCHAR);/*setMessageUsingInline("false",name,msgTarget,"","Total Chars:"+fieldName.value.length+" Total Allowed:"+MAXCHAR);*****/
	} else
	setMessageUsingInline("true",name,msgTarget,Total_Chars+":"+fieldName.value.length+" "+Total_Allowed+":"+MAXCHAR,"");/*setMessageUsingInline("true",name,msgTarget,"Total Chars:"+fieldName.value.length+" Total Allowed:"+MAXCHAR,"");*****/
 
} 
function linkFormSubmit(formName)
{	
	if (!(typeof(document.getElementById(formName))=="undefined") && (document.getElementById(formName)!=null) )
	{ 
	dojo.event.connect(dojo.byId(formName),"onsubmit",preSubmit);
	
	}
	
}
function setValidateFlag(flag)
{
	validateFlag=flag;
}

function getValidateFlag()
{
	return validateFlag;
}

/*
 * Flag to capture if submit button should be disable or not
 */
function setDisableSubmit(flag)
{
	disableSubmit=flag;
}

function getDisableSubmit()
{
	return disableSubmit;
}

/**
 * @param {Object} flag - Flag to turn Indicator On/Off
 * @param {Object} msg  - Message to display when Toggle is on.
 */
function toggleNotification(flag,msg)
{
   var notifyElement=dojo.byId("notification");
   
   if (!(typeof(notifyElement)=="undefined") && (notifyElement!=null)) 
   {
   	notifyElement.className="notification";	    
   	  if (flag.toLowerCase()=="on")	
	 { 
		notifyElement.style.visibility="visible";
		if (msg.length==0)
		notifyElement.innerHTML="<img src='../images/indicator.gif' align='absmiddle'>&nbsp;"+ProcYour_Req+"...";/*notifyElement.innerHTML="<img src='../images/indicator.gif' align='absmiddle'>&nbsp;Processing Your Request...";*****/
		else
		notifyElement.innerHTML="<img src='../images/indicator.gif' align='absmiddle'>&nbsp;"+msg;
		
		
	 }
	 else 
	 {
    notifyElement.innerHTML="";
	notifyElement.className='';
	notifyElement.style.visibility="hidden";
 	
	 }	
   }
}

function linkForSilentSubmit(formId,msgTarget,message,passMsg,failMsg)
{
	if (message.length==0)
	 message=Saving_Data ;/*message="Saving data.." ;*****/
	if (passMsg.length==0)
	passMsg=Data_SavedSucc;/*passMsg="Data Saved Successfully";*****/
	if (failMsg.length==0)
	passMsg=ErrSvg_Data;/*passMsg="Error Saving Data";*****/
	var x = new dojo.io.FormBind({
	formNode: document.getElementById(formId),
load: function(type, data, e) {
		// do something
	setMessageUsingInline('true','',msgTarget,passMsg,'');
	postProcess();	
	},
error: function(type, error){
	 	setMessageUsingInline('false','',msgTarget,'',failMsg);
		setValidationFailClass('');
		toggleNotification("off","");
	 }	
	
});
}
 
function postProcess()
{
	
	toggleNotification("off","");	
	resetColumns(); 
}

function resetColumns()
{
	//This is hardcoded for now. A correct implementation would take all the fields that need to be
	//refreshed and reset them
	tempFld=document.getElementById(modField);//("eSign")
	tempFldMessage=document.getElementById(msgTargets);//("eSignMessage")
	if (!(typeof(tempFld)=="undefined") && (tempFld!=null) )
	{
	tempFld.value="";
	tempFld.className="";
	}
	
	if (!(typeof(tempFldMessage)=="undefined") && (tempFldMessage!=null) )
		{
		tempFldMessage.innerHTML="";
		tempFldMessage.className="";
		}
		
		
			
	
	
}

/*
 * What value should be used for formatting changes to Display Message
 */
function getValidationPassClass()
{
	if (validationPassClass.length==0) {
	    validationPassClass="validation-pass";
		}
	return validationPassClass;	
}

function getValidationFailClass()
{
	if (validationFailClass.length==0)
	{
	    validationFailClass="validation-fail";
	}
		return validationFailClass;
}
function setValidationPassClass(name)
{
	validationPassClass=name;
	
}

function setValidationFailClass(name)
{
	validationFailClass=name;
}

 
//dojo.widget.validate.ValidationTextbox.prototype.validColor="white";


 

	
	
