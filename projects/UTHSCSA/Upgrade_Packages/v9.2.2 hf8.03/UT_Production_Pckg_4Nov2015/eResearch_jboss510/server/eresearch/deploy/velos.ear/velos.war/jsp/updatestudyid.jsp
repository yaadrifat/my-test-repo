<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=MC.M_Updt_OtherStdIds%><%--Update Other <%=LC.Std_Study%> IDs*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<%@ page import="com.velos.eres.service.util.LC"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="altId" scope="request"  class="com.velos.eres.web.studyId.StudyIdJB"/>

<%-- rajasekhar starts --%>
<jsp:useBean id="studyStatB" scope="request" class="com.velos.eres.web.studyStatus.StudyStatusJB"/>
<jsp:useBean id="studyJB" scope="request" class="com.velos.eres.web.study.UthscaStudyJB"/>
<jsp:useBean id="userJB" scope="request" class="com.velos.eres.web.user.UserJB"/>
			
<%@ page language = "java" import = "java.text.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.studyId.impl.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.*,com.velos.eres.web.user.*"%>
<%-- rajasekhar ends --%>

<body>
<DIV class="popDefault">

<%
	//rk starts
	String method = null;
	//rk ends
	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))
	{

%>

		<jsp:include page="sessionlogging.jsp" flush="true"/>
 <%
		// rajasekhar starts 
		//rk starts
		method = request.getParameter("method");
		//rk ends
		String studyNumber = request.getParameter("studyNumber");
		String studyTitle = request.getParameter("studyTitle");
		String studySummary = request.getParameter("studySummary");
		// rajasekhar ends 

		String eSign= request.getParameter("eSign");
    	String oldESign = (String) tSession.getValue("eSign");

    	 String defUserGroup = (String) tSession.getAttribute("defUserGroup");

	   String irbProtocolStatus = ""; //rajasekhar
	   if(oldESign.equals(eSign))
	   {


			int errorCode=0;
			int rows = 0;

			String recordType="" ;
			String creator="";
			String ipAdd="";
			String accountId="";
			String studyId="";
			String protocolTitle=""; //rajasekhar

			creator=(String) tSession.getAttribute("userId");
			ipAdd=(String)tSession.getAttribute("ipAdd");

			studyId = request.getParameter("studyId");

			ArrayList arRecordType = new ArrayList();
			ArrayList arAlternateId = new ArrayList();
			ArrayList arAlternateValues = new ArrayList();
			ArrayList arId = new ArrayList();
			//String[] arElementId = request.getParameterValues("elementId");

			Enumeration params = request.getParameterNames();
			while (params.hasMoreElements()) {
			    String param = (String)params.nextElement();
			    if (param.startsWith("alternateId")) {
			    	String paramVal = request.getParameter(param);
			    	if (null != paramVal){
			    		String mdId = param.substring(param.indexOf("alternateId")+("alternateId".length()));
			    		arAlternateId.add(mdId);
			    		arAlternateValues.add(paramVal);
			    		paramVal = request.getParameter("recordType"+mdId);
				    	if (null != paramVal){
				    		arRecordType.add(paramVal);
				    	}
				    	paramVal = request.getParameter("id"+mdId);
				    	if (null != paramVal){
				    		arId.add(paramVal);
				    	}
			      	}
			    }
			
			// rajasekhar starts 
			 String siteId="";
			String siteName="";
			
			if(param.equals("ProtocolTitle")){
			protocolTitle = request.getParameter("ProtocolTitle");
			
			if(!StringUtil.isEmpty(protocolTitle) && !(method.trim().equals("createnew"))){
			int studyIdn = StringUtil.stringToNum(studyId);
			
			String status = studyJB.updateStudyTitle(protocolTitle,studyIdn, ipAdd, creator);
			}
			
			}
			
			if(param.equals("IRBProtocolStatus")){
			
			
			irbProtocolStatus=request.getParameter("IRBProtocolStatus");
			String statusEffDate=request.getParameter("IRBProtocolStatusEffDate");
			String irbExpirationDate =request.getParameter("IRBExpirationDate");
			siteName=request.getParameter("SiteName");
			siteId=studyJB.getSiteID(siteName);
			
			
			if(statusEffDate==null){
			statusEffDate="";
			}else if(statusEffDate!=null && !(statusEffDate.equals(""))){
			SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");
			Date date = dt.parse(statusEffDate);
			
			SimpleDateFormat dt1 = new SimpleDateFormat("MM/dd/yyyy");
			statusEffDate=dt1.format(date);
			
			}
			
			if(irbExpirationDate==null){
			irbExpirationDate = "";
			}else if(irbExpirationDate!=null && !irbExpirationDate.trim().equals("")){
			SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");
			Date date = dt.parse(irbExpirationDate);
			
			SimpleDateFormat dt1 = new SimpleDateFormat("MM/dd/yyyy");
			irbExpirationDate=dt1.format(date);
			}
			
			if(irbProtocolStatus!=null && !irbProtocolStatus.equals(""))
			{
			String s1=irbProtocolStatus.substring(0,3);
			String s2=irbProtocolStatus.substring(6,irbProtocolStatus.length());
			
			irbProtocolStatus=s1+" - "+s2+"/UT";
			//IRB – Approved IRB - Approved/UT
			// String studyStatusId= studyJB.getStudyStatusID(irbProtocolStatus);
			String studyStatusId= studyJB.getStudyStatusID(irbProtocolStatus);
			
			
			String statusType = studyJB.getStudyStatusType(studyStatusId);
			String studyStatusTypeId= studyJB.getStudyStatusTypeID(statusType);
			int currentStatusId = Integer.parseInt(studyJB.getCurrentStudyStatusId());
			
			studyStatB.setStatDocUser(creator);
			studyStatB.setStatStartDate(statusEffDate);
			studyStatB.setStudyStatus(studyStatusId);
			studyStatB.setStudyStatusType(studyStatusTypeId);
			studyStatB.setStatStudy(studyId);
			studyStatB.setSiteId(siteId);
			studyStatB.setStatValidDate(irbExpirationDate);
			studyStatB.setCreator(creator);
			studyStatB.setIpAdd(ipAdd);
			studyStatB.setCurrentStat("1");
			studyStatB.setStudyStatusDetails();
			studyStatB.setId(currentStatusId);
			studyStatB.getStudyStatusDetails();
			studyStatB.setCurrentStat("0");
			studyStatB.setModifiedBy(creator);
			
			int ret = studyStatB.updateStudyStatus();
			}
			}
			if(param.equals("HSCProtocolStatus")){
			
			
			String hscProtocolStatus=request.getParameter("HSCProtocolStatus");
			String hscstatusEffDate=request.getParameter("HSCProtocolStatusEffDate");
			siteName=request.getParameter("SiteName");
			siteId=studyJB.getSiteID(siteName);
			
			
			if(hscstatusEffDate==null){
			hscstatusEffDate="";
			}else if(hscstatusEffDate!=null && !(hscstatusEffDate.equals(""))){
			SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");
			Date date = dt.parse(hscstatusEffDate);
			
			SimpleDateFormat dt1 = new SimpleDateFormat("MM/dd/yyyy");
			hscstatusEffDate=dt1.format(date);
			}
			
			
			if(hscProtocolStatus!=null && !hscProtocolStatus.equals(""))
			{
			String s3=hscProtocolStatus.substring(0,3);
			
			String s4=hscProtocolStatus.substring(6,hscProtocolStatus.length());
			
			hscProtocolStatus=s3+" - "+s4;
			
			String studyStatusId= studyJB.getStudyStatusID(hscProtocolStatus);
			// String studyStatusId= studyJB.getStudyStatusID("HSC - Active");
			
			
			String statusType = studyJB.getStudyStatusType(studyStatusId);
			
			String studyStatusTypeId= studyJB.getStudyStatusTypeID(statusType);
			
			int currentStatusId = Integer.parseInt(studyJB.getCurrentStudyStatusId());
			
			studyStatB.setStatDocUser(creator);
			studyStatB.setStatStartDate(hscstatusEffDate);
			studyStatB.setStudyStatus(studyStatusId);
			studyStatB.setStudyStatusType(studyStatusTypeId);
			studyStatB.setStatStudy(studyId);
			studyStatB.setSiteId(siteId);
			studyStatB.setCreator(creator);
			studyStatB.setIpAdd(ipAdd);
			studyStatB.setCurrentStat("1");
			studyStatB.setStudyStatusDetails();
			studyStatB.setId(currentStatusId);
			studyStatB.getStudyStatusDetails();
			studyStatB.setCurrentStat("0");
			studyStatB.setModifiedBy(creator);
			int ret = studyStatB.updateStudyStatus();
			}
			}

			//rajasekhar ends 
			}



			StudyIdBean sskMain = new StudyIdBean();

			ArrayList alElementData = arAlternateValues;

				int len = alElementData.size();

				for ( int j = 0 ; j< len ; j++)
				{
					StudyIdBean sskTemp = new StudyIdBean();

					if   ( ( !(EJBUtil.isEmpty ((String)arAlternateValues.get(j))) &&  "N".equals((String)arRecordType.get(j))) || "M".equals((String)arRecordType.get(j)))
					{
						sskTemp.setId(EJBUtil.stringToNum((String)arId.get(j)));
						sskTemp.setStudyId(studyId) ;
						sskTemp.setStudyIdType((String)arAlternateId.get(j));
						sskTemp.setAlternateStudyId((String)arAlternateValues.get(j));
						sskTemp.setRecordType((String)arRecordType.get(j));
						if ("N".equals((String)arRecordType.get(j)))
						{
							sskTemp.setCreator(creator);
						}else
						{
							sskTemp.setModifiedBy(creator);
						}
						sskTemp.setIpAdd(ipAdd);
						sskMain.setStudyIdBeans(sskTemp);
					}
				}

			errorCode = altId.createMultipleStudyIds(sskMain,defUserGroup);

		%>
		<br><br><br><br><br><br><br>
		<table "width = 500" align = "center">
			<tr>
				<td width = 500>



		<%


		if ( errorCode == -2  )
		{
%>
			<p class = "successfulmsg" >
	 			<%=MC.M_DataNotSvd_Succ%><%--Data was not saved successfully*****--%>
			</p>

<%

		} //end of if for Error Code

		else

		{
%>

		<p class = "successfulmsg" >
			<%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%>
		</p>

		</td>
		</tr>
		</table>
		<script>
			//window.opener.location.reload();
			setTimeout("self.close()",1000);
		</script>
		<!-- rk starts -->
		<%if(method!=null && method.trim().equals("createnew")){ %>
		<META HTTP-EQUIV="refresh" CONTENT="0; URL=newStudyIds.jsp?studyId=<%=studyId%>&studyRights=6&studyNumber=<%=studyNumber%>&studyTitle=<%=studyTitle%>&studySummary=<%=studySummary%>">
		<%}%>
		<!-- rk ends -->
<%
		}//end of else for Error Code Msg Display
	}//end of if old esign
	else{
%>
		<jsp:include page="incorrectesign.jsp" flush="true"/>
<%
			}//end of else of incorrect of esign
     }//end of if body for session

	else
	{

%>
		  <jsp:include page="timeout.html" flush="true"/>
<%

} //end of else of the Session


%>

</body>

</html>

