set define off;

declare
v_exists NUMBER := 0;
v_nextVal NUMBER := 0;

v_PK_WORKFLOW NUMBER := 0;
v_PK_ACTIVITY NUMBER := 0;
v_PK_WORKFLOWACTIVITY NUMBER := 0;

v_PK_WFForm := NUMBER := [pk_form];
begin
	v_PK_WORKFLOW := 0;
	SELECT count(*) INTO v_exists FROM WORKFLOW WHERE WORKFLOW_TYPE = 'StudyCentricPatEnrollUPenn';
	
	IF (v_exists = 0) THEN
		v_PK_WORKFLOW := SQ_WORKFLOW.NEXTVAL();

		Insert into WORKFLOW (PK_WORKFLOW,WORKFLOW_NAME,WORKFLOW_DESC,WORKFLOW_TYPE) 
		values (v_PK_WORKFLOW,'Study Centric Enrollment Workflow','Study Centric Enrollment Workflow','StudyCentricPatEnrollUPenn');
		COMMIT;
	ELSE 
		SELECT PK_WORKFLOW INTO v_PK_WORKFLOW FROM WORKFLOW WHERE WORKFLOW_TYPE = 'StudyCentricPatEnrollUPenn';
	END IF;
	---------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------
	
	v_PK_ACTIVITY := 0;
	SELECT count(*) INTO v_exists FROM ACTIVITY WHERE ACTIVITY_NAME = 'PatientRecordCreated';
	
	IF (v_exists = 0) THEN
		v_PK_ACTIVITY := SQ_ACTIVITY.NEXTVAL();
		
		Insert into ACTIVITY (PK_ACTIVITY,ACTIVITY_NAME,ACTIVITY_DESC) 
		values (v_PK_ACTIVITY,'PatientRecordCreated','An Activity to indicate that Patient record is created');
		COMMIT;
	ELSE
		SELECT PK_ACTIVITY INTO v_PK_ACTIVITY FROM ACTIVITY WHERE ACTIVITY_NAME = 'PatientRecordCreated';
	END IF;
	
	v_PK_WORKFLOWACTIVITY := 0;
	IF (v_PK_ACTIVITY > 0) THEN
		SELECT COUNT(*) INTO v_exists FROM WORKFLOW_ACTIVITY WHERE FK_WORKFLOWID = v_PK_WORKFLOW AND FK_ACTIVITYID = v_PK_ACTIVITY;
	
		IF (v_exists = 0) THEN
			v_PK_WORKFLOWACTIVITY := SQ_WORKFLOWACTIVITY.NEXTVAL();
			
			Insert into WORKFLOW_ACTIVITY (PK_WORKFLOWACTIVITY,WA_NAME,FK_WORKFLOWID,FK_ACTIVITYID,WA_SEQUENCE,WA_TERMINATEFLAG,WA_DISPLAYQUERY) 
			values (v_PK_WORKFLOWACTIVITY,'Patient Created',v_PK_WORKFLOW,v_PK_ACTIVITY,1,0,'SELECT COUNT(*) cnt from EPAT.PERSON where PK_PERSON=(SELECT FK_PER FROM ER_PATPROT WHERE PK_PATPROT = :1)');
			COMMIT;
		END IF;
	END IF;
	---------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------
	
	v_PK_ACTIVITY := 0;
	SELECT count(*) INTO v_exists FROM ACTIVITY WHERE ACTIVITY_NAME = 'PatientDemogUpdated';

	IF (v_exists = 0) THEN
		v_PK_ACTIVITY := SQ_ACTIVITY.NEXTVAL();
		
		Insert into ACTIVITY (PK_ACTIVITY,ACTIVITY_NAME,ACTIVITY_DESC) 
		values (v_PK_ACTIVITY,'PatientDemogUpdated','An Activity to indicate that Patient Demographics record is updated');
		COMMIT;
	ELSE
		SELECT PK_ACTIVITY INTO v_PK_ACTIVITY FROM ACTIVITY WHERE ACTIVITY_NAME = 'PatientDemogUpdated';
	END IF;
	
	v_PK_WORKFLOWACTIVITY := 0;
	IF (v_PK_ACTIVITY > 0) THEN
		SELECT COUNT(*) INTO v_exists FROM WORKFLOW_ACTIVITY WHERE FK_WORKFLOWID = v_PK_WORKFLOW AND FK_ACTIVITYID = v_PK_ACTIVITY;
	
		IF (v_exists = 0) THEN
			v_PK_WORKFLOWACTIVITY := SQ_WORKFLOWACTIVITY.NEXTVAL();
			
			Insert into WORKFLOW_ACTIVITY (PK_WORKFLOWACTIVITY,WA_NAME,FK_WORKFLOWID,FK_ACTIVITYID,WA_SEQUENCE,WA_TERMINATEFLAG,WA_DISPLAYQUERY) 
			values (v_PK_WORKFLOWACTIVITY,'Patient Demographics',v_PK_WORKFLOW,v_PK_ACTIVITY,2,0,'SELECT COUNT(*) cnt from EPAT.PERSON where PK_PERSON= :3 AND (PERSON_COUNTRY IS NOT NULL AND FK_CODELST_RACE IS NOT NULL AND FK_CODELST_ETHNICITY IS NOT NULL AND PERSON_ADD_RACE IS NOT NULL)');
			COMMIT;
		END IF;
	END IF;
	---------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------
	
	v_PK_ACTIVITY := 0;
	SELECT count(*) INTO v_exists FROM ACTIVITY WHERE ACTIVITY_NAME = 'PatientEnrolled';

	IF (v_exists = 0) THEN
		v_PK_ACTIVITY := SQ_ACTIVITY.NEXTVAL();
		
		Insert into ACTIVITY (PK_ACTIVITY,ACTIVITY_NAME,ACTIVITY_DESC) 
		values (v_PK_ACTIVITY,'PatientEnrolled','An Activity to indicate that Patient is enrolled on a Study');
		COMMIT;
	ELSE
		SELECT PK_ACTIVITY INTO v_PK_ACTIVITY FROM ACTIVITY WHERE ACTIVITY_NAME = 'PatientEnrolled';
	END IF;
	
	v_PK_WORKFLOWACTIVITY := 0;
	IF (v_PK_ACTIVITY > 0) THEN
		SELECT COUNT(*) INTO v_exists FROM WORKFLOW_ACTIVITY WHERE FK_WORKFLOWID = v_PK_WORKFLOW AND FK_ACTIVITYID = v_PK_ACTIVITY;
		
		IF (v_exists = 0) THEN
			v_PK_WORKFLOWACTIVITY := SQ_WORKFLOWACTIVITY.NEXTVAL();
			
			Insert into WORKFLOW_ACTIVITY (PK_WORKFLOWACTIVITY,WA_NAME,FK_WORKFLOWID,FK_ACTIVITYID,WA_SEQUENCE,WA_TERMINATEFLAG,WA_DISPLAYQUERY) 
			values (v_PK_WORKFLOWACTIVITY,'Subject Enrolled',v_PK_WORKFLOW,v_PK_ACTIVITY,3,0,'SELECT COUNT(*) cnt from ERES.ER_PATPROT pp, ER_PATSTUDYSTAT pss WHERE PK_PATPROT=:1 AND pp.FK_PER = pss.FK_PER AND pp.FK_STUDY = pss.FK_STUDY AND FK_CODELST_STAT IN (select pk_codelst from er_codelst where codelst_type=''patStatus'' and codelst_subtyp = ''enrolled'')');
			COMMIT;
		END IF;
	END IF;
	---------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------
	
	v_PK_ACTIVITY := 0;
	SELECT count(*) INTO v_exists FROM ACTIVITY WHERE ACTIVITY_NAME = 'PatStudyFormResponseAdded';

	IF (v_exists = 0) THEN
		v_PK_ACTIVITY := SQ_ACTIVITY.NEXTVAL();
		
		Insert into ACTIVITY (PK_ACTIVITY,ACTIVITY_NAME,ACTIVITY_DESC) 
		values (v_PK_ACTIVITY,'PatStudyFormResponseAdded','An Activity to indicate that Pat Study Form Response record is ceated or updated');
		COMMIT;
	ELSE
		SELECT PK_ACTIVITY INTO v_PK_ACTIVITY FROM ACTIVITY WHERE ACTIVITY_NAME = 'PatStudyFormResponseAdded';
	END IF;
	
	v_PK_WORKFLOWACTIVITY := 0;
	IF (v_PK_ACTIVITY > 0) THEN
		SELECT COUNT(*) INTO v_exists FROM WORKFLOW_ACTIVITY WHERE FK_WORKFLOWID = v_PK_WORKFLOW AND FK_ACTIVITYID = v_PK_ACTIVITY;
		
		IF (v_PK_WORKFLOWACTIVITY = 0) THEN
			v_PK_WORKFLOWACTIVITY := SQ_WORKFLOWACTIVITY.NEXTVAL();
			
			Insert into WORKFLOW_ACTIVITY (PK_WORKFLOWACTIVITY,WA_NAME,FK_WORKFLOWID,FK_ACTIVITYID,WA_SEQUENCE,WA_TERMINATEFLAG,WA_DISPLAYQUERY) 
			values (v_PK_WORKFLOWACTIVITY,'More Subject Details',v_PK_WORKFLOW,v_PK_ACTIVITY,4,0,'SELECT COUNT(*) cnt from er_formslinear where fk_form = '|| TO_CHAR(v_PK_WFForm) ||' and id = :3 and fk_patprot in (select pk_patprot from er_patprot where fk_study = :2 and fk_per = :3)');		
			COMMIT;
		END IF;
	END IF;
	---------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------
end;
/


