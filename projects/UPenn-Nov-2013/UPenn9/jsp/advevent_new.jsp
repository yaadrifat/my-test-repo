<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="adveventB" scope="request" class="com.velos.esch.web.advEve.AdvEveJB"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="codeLst" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="ulinkB" scope="request" class="com.velos.eres.web.ulink.ULinkJB"/>
<jsp:useBean id="userSiteB" scope="page" class="com.velos.eres.web.userSite.UserSiteJB" />
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="eventAssocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="patEnrollB" scope="request" class="com.velos.eres.web.patProt.PatProtJB"/>
<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="patB" scope="request" class="com.velos.eres.web.patStudyStat.PatStudyStatJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import = "com.velos.eres.business.section.*,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.service.util.DateUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.esch.business.common.*,com.velos.eres.web.user.UserJB,com.velos.eres.business.common.*,com.velos.esch.business.common.*"%>
<%@ page import="com.velos.eres.web.user.ConfigFacade,com.velos.eres.web.user.ConfigDetailsObject"%>

<%if(false){ //request.getParameter("includeMode")==null%>
	<meta http-equiv="Refresh" content="0; URL=">

<% } %>

  <%
  HttpSession tSession = request.getSession(false);
   if (sessionmaint.isValidSession(tSession)){
	   
	   String src = request.getParameter("srcmenu");
	   String selectedTab = request.getParameter("selectedTab");
	   String mode = request.getParameter("mode");
	   String pkey = request.getParameter("pkey");
	   String studyId = (String) tSession.getAttribute("studyId");
	   
		int pageRight = 7;
		int orgRight = 0;
		String userIdFromSession = (String) tSession.getAttribute("userId");
		//GET STUDY TEAM RIGHTS
		TeamDao teamDao = new TeamDao();
		teamDao.getTeamRights(StringUtil.stringToNum(studyId),StringUtil.stringToNum(userIdFromSession));
		ArrayList tId = teamDao.getTeamIds();
		if (tId.size() == 0) {
			pageRight=0 ;
	    }else {
				stdRights.setId(StringUtil.stringToNum(tId.get(0).toString()));
	
			 	ArrayList teamRights ;
				teamRights  = new ArrayList();
				teamRights = teamDao.getTeamRights();
	
				stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
				stdRights.loadStudyRights();
	
	    	if ((stdRights.getFtrRights().size()) == 0){
	    	 	pageRight= 0;
	    	}else{
	    		pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYMPAT"));
	    	}
	    }
	String death = "";
	String meddracode="";
	String advDictionary="";
	String descripton="";
	String startDt ="";
	String stopDt = "";
	String treatment = "";
	String enteredBy="";
	String reportedBy="";


	String outcomeString="";
	String outcomeDt ="";
	String addInfoString="";
	String notes ="";
	String outcomeNotes="";
	String codelstId="";
	String outcomeActionM="";
	String severity = "";
	String sysaff = "";
	String drugdroc = "";
	String recoverydesc = "";
	String outactiondesc = "";
	String advNotifyDate = "";
	String advNotifyCodeId = "";
	String advNotifyString = "";
	
	String aeCategory = "";
	String aeToxicity = "";
	String aeToxicityDesc = "";
	String aeGradeDesc = "";
	
	String adveventId= request.getParameter("adveventId");
	char strIndex='0';
	String status="";
	String enteredByName="";
	ArrayList outcomeIds=null;
	ArrayList addInfoIds = null;
	ArrayList advNotifyCodeIds = null;
	String outcomeId="";
	String addInfoId = "";
	String protocolId = "";
	String statDesc=request.getParameter("statDesc");
	String statid=request.getParameter("statid");
	String accId = (String) tSession.getAttribute("accountId");
	String studyVer = request.getParameter("studyVer");
	String studyNum = request.getParameter("studyNum");
	String enrollId =(String) tSession.getAttribute("enrollId");
	String patientId = "";
	String patProtId=(String) request.getParameter("patProtId");
	String study = (String) tSession.getAttribute("studyId");
	String eventId=(String) request.getParameter("eventId");
	String eventName=request.getParameter("eventName");
	String visit=request.getParameter("visit");
	String patStatSubType = "";
	String patStudyStat =  "";
	String reportedByName = "";
	String discoveryDt = "";
	String loggedDt = "";
	String linkedTo = "";
	String linkedToId = "";
	String linkedToName = "";

	int patStudyStatpk = 0;
	patEnrollB.setPatProtId(StringUtil.stringToNum(enrollId));
	patEnrollB.getPatProtDetails();
	protocolId =patEnrollB.getPatProtProtocolId();
	String protName = "";
	if(protocolId != null) {
		eventAssocB.setEvent_id(StringUtil.stringToNum(protocolId));
		eventAssocB.getEventAssocDetails();
		protName = 	eventAssocB.getName();
	}

	String studyTitle = "";
	String studyNumber = "";
	studyB.setId(StringUtil.stringToNum(studyId));

	studyB.getStudyDetails();
	studyTitle = studyB.getStudyTitle();
	studyNumber = studyB.getStudyNumber();
	int fdaStudy = ("1".equals(studyB.getFdaRegulatedStudy()))? 1:0;

	String studyAdvlkpVer = "0";
	String studyAdvlkpVerNumber = "0";

	//get study lookup view for adverse events
	studyAdvlkpVer = studyB.getStudyAdvlkpVer();

		if (StringUtil.isEmpty(studyAdvlkpVer))
		{
			studyAdvlkpVer  = "0";
		}
		else
		{
			studyAdvlkpVerNumber = studyB.getLkpTypeVersionForView(StringUtil.stringToNum(studyAdvlkpVer) );
		}


		if (StringUtil.isEmpty(studyAdvlkpVerNumber))
		{
			studyAdvlkpVerNumber  = "0";
		}


   //KM: added 05/10/06 May - June requirement PS1


	int index=-1;
    String studyLkpDict = studyB.getStudyAdvlkpVer();
    if (StringUtil.isEmpty(studyLkpDict))
		studyLkpDict = "0";
   int studyLkpDictId=StringUtil.stringToNum(studyLkpDict);
		LookupDao lookupDao = new LookupDao();
		lookupDao = studyB.getAllViewsForLkpType(StringUtil.stringToNum(accId) ,"NCI");
		ArrayList viewIds = lookupDao.getLViewIds();
		ArrayList viewNames = lookupDao.getLViewNames();
		String settingValue="";

	 if (studyLkpDictId==0)
	 {
		settingValue="";
	 }
	 else
	 {
	   index=viewIds.indexOf(new Integer(studyLkpDict));
	   settingValue=(String)viewNames.get(index);
	  }

	  if (StringUtil.isEmpty(settingValue))
	  {
	  	settingValue = LC.L_Free_TextEntry;/*settingValue = "Free Text Entry";*****/
	  }
   //advDictionary=settingValue;

	int siteId = 0;
	String siteName = "";

	String advName = "";
	String grade = "";
	String gradeText="";
	
	//boolean genSchedule = true;

	SchCodeDao cd = new SchCodeDao();

	//SchCodeDao cd_severe = new SchCodeDao();

	//SchCodeDao cd_sysaff = new SchCodeDao();

	SchCodeDao cd_drugproc = new SchCodeDao();
	SchCodeDao cd_recoverydesc = new SchCodeDao();

	SchCodeDao cd_actiondesc = new SchCodeDao(); //chn


	String dCur = "";



	String dCur_drugproc = "";
	String dCur_recoverydesc = "";
	String dFormStatus = "";
	String advFormStatus = "";
	String dCur_actiondesc= ""; //chn


	String disableStr ="disabled class='readonly-input'";
	String readOnlyStr ="";

	//cd_severe.getCodeValues("adve_severity");
	//cd_sysaff.getCodeValues("adve_bdsystem");

	cd_drugproc.getCodeValues("adve_relation");
	cd_recoverydesc.getCodeValues("adve_recovery");
	cd_actiondesc.getCodeValues("outaction"); //chn



	ConfigFacade cFacade=ConfigFacade.getConfigFacade();
	HashMap hashPgCustFld = new HashMap();

	ConfigDetailsObject cdoPgField = cFacade.populateObject(Integer.parseInt(accId), "adverseevent");

	if (cdoPgField.getPcfField()!= null && cdoPgField.getPcfField().size() > 0) {
		for (int i=0;i<cdoPgField.getPcfField().size();i++){
			hashPgCustFld.put((String)cdoPgField.getPcfField().get(i), String.valueOf(i));
	    }
	}



	 String attrAtt = "";
	 String actAtt = "";
	 String recAtt = "";
	 String formStatAtt ="";
	 String typeAtt = "";

     if (hashPgCustFld.containsKey("attribution")) {
			int fldNumAttr = Integer.parseInt((String)hashPgCustFld.get("attribution"));
			attrAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumAttr));
			if(attrAtt == null) attrAtt ="";
      }


	  if (hashPgCustFld.containsKey("action")) {
			int fldNumAct = Integer.parseInt((String)hashPgCustFld.get("action"));
			actAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumAct));
			if(actAtt == null) actAtt ="";
      }

	  if (hashPgCustFld.containsKey("recoverydesc")) {
			int fldNumRec = Integer.parseInt((String)hashPgCustFld.get("recoverydesc"));
			recAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumRec));
			if(recAtt == null) recAtt ="";
      }


	  if (hashPgCustFld.containsKey("formstatus")) {
			int fldNumFormStat = Integer.parseInt((String)hashPgCustFld.get("formstatus"));
			formStatAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumFormStat));
			if(formStatAtt == null) formStatAtt ="";
      }


	  if (hashPgCustFld.containsKey("aetype")) {
			int fldNumType = Integer.parseInt((String)hashPgCustFld.get("aetype"));
			typeAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumType));
			if(typeAtt == null) typeAtt ="";
      }






	if(mode.equals("N"))
	{
		cd.getCodeValues("adve_type");

		//dCur =  cd.toPullDown("adve_type",0);

		if (typeAtt.equals("1") || typeAtt.equals("2"))
			dCur =  cd.toPullDown("adve_type",0,false);
		else
			dCur =  cd.toPullDown("adve_type",0);

		//reset cd
		cd.resetObject();

		cd.getCodeValues("fillformstat");

		if (formStatAtt.equals("1") || formStatAtt.equals("2"))
		   dFormStatus =  cd.toPullDown("formStatus",0,false);
		else
			dFormStatus =  cd.toPullDown("formStatus",0);


		//dCur_severe =  cd_severe.toPullDown("adve_severity",0);

		//dCur_sysaff =  cd_sysaff.toPullDown("adve_bdsystem",0);

		//dCur_drugproc =  cd_drugproc.toPullDown("adve_relation",0);

		if (attrAtt.equals("1") || attrAtt.equals("2"))
			dCur_drugproc =  cd_drugproc.toPullDown("adve_relation",0,false);
		else
			dCur_drugproc =  cd_drugproc.toPullDown("adve_relation",0);


		if (recAtt.equals("1") || recAtt.equals("2"))
			dCur_recoverydesc =  cd_recoverydesc.toPullDown("adve_recovery",0,false);
		else
			dCur_recoverydesc =  cd_recoverydesc.toPullDown("adve_recovery",0);

		if (actAtt.equals("1") || actAtt.equals("2"))
			dCur_actiondesc =  cd_actiondesc.toPullDown("outaction",0,false); //chn
		else
			dCur_actiondesc =  cd_actiondesc.toPullDown("outaction",0);


		String uName = (String) tSession.getAttribute("userName");
		enteredByName = uName;
		enteredBy=userIdFromSession;
		//reportedByName = uName;
		//reportedBy=userIdFromSession;

	}else	{

		//get current patient status and its subtype
		patStatSubType = patB.getPatStudyCurrentStatusWithCodeSubType(studyId, pkey);
		patStudyStat = patB.getPatStudyStat();
		patStudyStatpk = patB.getId();
		statid = patStudyStat ;
		if (StringUtil.isEmpty(patStatSubType))
			patStatSubType= "";

	///////////////////////////////////

		//dCur_actiondesc =  cd_actiondesc.toPullDown("outaction",0);

		if (actAtt.equals("1") || actAtt.equals("2"))
			dCur_actiondesc =  cd_actiondesc.toPullDown("outaction",0,false);
		else
			dCur_actiondesc =  cd_actiondesc.toPullDown("outaction",0);


		adveventB.setAdvEveId(StringUtil.stringToNum(adveventId));
		adveventB.getAdvEveDetails();

		codelstId=adveventB.getAdvEveCodelstAeTypeId();
		//severity=adveventB.getAdvEveSeverity();

		//sysaff=adveventB.getAdvEveBdsystemAff();

		drugdroc=adveventB.getAdvEveRelationship();

		recoverydesc=adveventB.getAdvEveRecoveryDesc();
		// Added by Gopu dated on 01/20/05
		outactiondesc = adveventB.getFkOutcomeAction();


//KM: added 05/10/06 May - June requirement PS1
		meddracode=adveventB.getAdvEveMedDRA();
		if(meddracode==null)
		{ meddracode="";}


		advDictionary=adveventB.getAdvEveDictionary();
		if (advDictionary==null)
		{ advDictionary="";}


		descripton=adveventB.getAdvEveDesc();
		if (descripton==null){ descripton=""; }

		treatment=adveventB.getAdvEveTreatment();
		if (treatment==null){ treatment=""; }


		startDt = adveventB.getAdvEveStDate();
		stopDt =  adveventB.getAdvEveEndDate();
	   	enteredBy= adveventB.getAdvEveEnterBy();
		reportedBy =adveventB.getAdvEveReportedBy();
		discoveryDt = adveventB.getAdvEveDiscvryDate(); ;
	        loggedDt = adveventB.getAdvEveLoggedDate();
		linkedToName = adveventB.getAdvEveLinkedTo();


		if(linkedToName == null) {
		  linkedToName = "";


		}


		outcomeString=adveventB.getAdvEveOutType();
		outcomeDt =adveventB.getAdvEveOutDate();

		outcomeNotes= adveventB.getAdvEveOutNotes();
		outcomeNotes = (   outcomeNotes  == null      )?"":(  outcomeNotes ) ;

		
		
		
		addInfoString=adveventB.getAdvEveAddInfo();

		notes=adveventB.getAdvEveNotes();
		notes = (   notes  == null      )?"":(  notes ) ;

		//get grade and adv event name

		grade = adveventB.getAdvEveGrade();
		// Added by Gopu dated on 01/20/05
		outcomeActionM = adveventB.getFkOutcomeAction();
		if (grade==null) grade="";
		advName = adveventB.getAdvEveName();
		if (advName==null) advName= "";

		if (grade.equals("-1") )
			grade = "";
		
		if ( ! EJBUtil.isEmpty(grade) )
		{
			gradeText =  LC.L_Grade+" : " +  grade + " " + advName ;/*gradeText =  "Grade : " +  grade + " " + advName ;*****/
		}
		else
		{
			gradeText =  advName;
		}
		
		
		
		
		advFormStatus = adveventB.getFormStatus();

		cd.getCodeValues("adve_type");

		//dCur =  cd.toPullDown("adve_type",StringUtil.stringToNum(codelstId));
		if (typeAtt.equals("1") || typeAtt.equals("2"))
			dCur =  cd.toPullDown("adve_type",StringUtil.stringToNum(codelstId),false);
		else
			dCur =  cd.toPullDown("adve_type",StringUtil.stringToNum(codelstId));

		//reset cd
		cd.resetObject();

		cd.getCodeValues("fillformstat");

		if (formStatAtt.equals("1") || formStatAtt.equals("2"))
		   dFormStatus =  cd.toPullDown("formStatus",StringUtil.stringToNum(advFormStatus),false);
		else
		   dFormStatus =  cd.toPullDown("formStatus",StringUtil.stringToNum(advFormStatus));



		//dCur_severe =  cd_severe.toPullDown("adve_severity",StringUtil.stringToNum(severity));

		//dCur_sysaff =  cd_sysaff.toPullDown("adve_bdsystem",StringUtil.stringToNum(sysaff));


		//dCur_drugproc =  cd_drugproc.toPullDown("adve_relation",StringUtil.stringToNum(drugdroc));

		if (attrAtt.equals("1") || attrAtt.equals("2"))
			dCur_drugproc =  cd_drugproc.toPullDown("adve_relation",StringUtil.stringToNum(drugdroc),false);
		else
			dCur_drugproc =  cd_drugproc.toPullDown("adve_relation",StringUtil.stringToNum(drugdroc));


		if (recAtt.equals("1") || recAtt.equals("2"))
 	 		dCur_recoverydesc =  cd_recoverydesc.toPullDown("adve_recovery",StringUtil.stringToNum(recoverydesc),false);
		else
			dCur_recoverydesc =  cd_recoverydesc.toPullDown("adve_recovery",StringUtil.stringToNum(recoverydesc));


		//dCur_actiondesc = cd_actiondesc.toPullDown("outaction", StringUtil.stringToNum(outactiondesc));

		if (actAtt.equals("1") || actAtt.equals("2"))
			dCur_actiondesc = cd_actiondesc.toPullDown("outaction", StringUtil.stringToNum(outactiondesc),false);
		else
			dCur_actiondesc = cd_actiondesc.toPullDown("outaction", StringUtil.stringToNum(outactiondesc));


		userB.setUserId(StringUtil.stringToNum(enteredBy));
	        userB.getUserDetails();
	        enteredByName = userB.getUserFirstName() + " " + userB.getUserLastName();

		if(reportedBy != null){
		userB.setUserId(StringUtil.stringToNum(reportedBy));
	        userB.getUserDetails();
		reportedByName = userB.getUserFirstName() + " " + userB.getUserLastName();
		}
		
		aeCategory = adveventB.getAeCategory();
		if (aeCategory==null) aeCategory="";
		
		aeToxicity = adveventB.getAeToxicity();
		if (aeToxicity==null) aeToxicity="";
		
		
		aeToxicityDesc = adveventB.getAeToxicityDesc();
		if (aeToxicityDesc==null) aeToxicityDesc="";
		
		
		aeGradeDesc = adveventB.getAeGradeDesc();
		if (aeGradeDesc==null) aeGradeDesc="";
		
		
	}
	String uName = (String) tSession.getAttribute("userName");
%>
<%
	int personPK = 0;
	personPK = StringUtil.stringToNum(pkey);
	person.setPersonPKId(personPK);
	person.getPersonDetails();
	patientId = person.getPersonPId();
	siteId = StringUtil.stringToNum(person.getPersonLocation());
	orgRight = userSiteB.getMaxRightForStudyPatient(StringUtil.stringToNum(userIdFromSession), personPK , StringUtil.stringToNum(studyId) );
	if (orgRight > 0){
		System.out.println("patient adverse new orgRight" + orgRight);
		orgRight = 7;
	}
%>
			<table width="100%" cellspacing="2" cellpadding="2" border="0" class="basetbl midAlign">
			       
			       <!-- The field would be cleared when a value is select from dictionary-->
			       <input type="hidden" id="dictValSel" name="dictValSel" value=" " />
			      
			       <%
			       if(mode.equals("M")){
			       %>
			        <tr>

			    	<td class="tdDefault" width="20%">
				        <%=LC.L_AE_RES_ID%><%--Response ID*****--%>
					</td>
					<td class="tdDefault" width="80%">
					<input type="text"  size="4" name="Response ID"  value='<%=adveventId%>' readonly>
					</td>
				</tr>
				<%
				}
				%>



				<tr>
				 <%if (hashPgCustFld.containsKey("aetype")) {
						int fldNumType = Integer.parseInt((String)hashPgCustFld.get("aetype"));
						String typeMand = ((String)cdoPgField.getPcfMandatory().get(fldNumType));
						String typeLable = ((String)cdoPgField.getPcfLabel().get(fldNumType));
						typeAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumType));

						if(typeAtt == null) typeAtt ="";
						if(typeMand == null) typeMand ="";

						if(!typeAtt.equals("0")) {
						if(typeLable !=null){
						%> <td class="tdDefault" width="20%">
						 <%=typeLable%> 
						<%} else {%> <td>
						  <%=LC.L_AdverseEvent_Type%><%--Adverse Event Type*****--%> 
						<%}

						if (typeMand.equals("1")) {
							%>
						   <FONT class="Mandatory" id="pgcustomtype">* </FONT>
						<% } %>

				</td>
				<td width="80%">
				<%=dCur%>
				<%if(typeAtt.equals("1")) {%>
					<input type="hidden" name="adve_type" value="">
				<%} else if(typeAtt.equals("2")) {%>
					<input type="hidden" name="adve_type" value="<%=codelstId%>">
				<%}%>
				</td>
				<%} else if(typeAtt.equals("0")) {%>
				<input type="hidden" name="adve_type" value="<%=codelstId%>">
				<%}} else {%>

					<td class="tdDefault" width="20%">
				       <%=LC.L_AdverseEvent_Type%><%--Adverse Event Type*****--%> <FONT class="Mandatory" id ="mandae">* </FONT>
					</td>
					<td width="80%">	<%=dCur%></td>
				<%}%>
			  </tr>
			
			<tr id="nciDictRow"><td class="tdDefault">&nbsp;</td>
				<td class="tdDefault"><A href="#" id="dictionaryLink" onClick="advEventNewFunctions.openwindowdict(<%=studyAdvlkpVer%>,<%=studyAdvlkpVerNumber%>,document.adverseEventScreenForm);"><%=LC.L_Select_FromDict%><%--Select from Dictionary*****--%></A>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<A href="#" onClick="advEventNewFunctions.openwindowcalc('<%=studyAdvlkpVerNumber%>',<%=pkey%>,document.adverseEventScreenForm);"><%=LC.L_Calculate%><%--Calculate*****--%></A>
					<input type="hidden" value="<%=studyAdvlkpVerNumber%>" id="studyAdvlkpVerNumber"/>
					<input type="hidden" value="<%=studyAdvlkpVer%>" id="studyAdvlkpVer"/>
				</td>
			</tr>
			
			<% if(hashPgCustFld.containsKey("category")){
					int fldNum = Integer.parseInt((String)hashPgCustFld.get("category"));
					String fldMand = ((String)cdoPgField.getPcfMandatory().get(fldNum));
					String fldLable = ((String)cdoPgField.getPcfLabel().get(fldNum));
					String fldAttr = ((String)cdoPgField.getPcfAttribute().get(fldNum));
					
					if(fldAttr.equals("0")){%>
						<input type ="hidden" maxLength="500" name="aeCategory" id="aeCategory" value= '<%=aeCategory%>' />
					<%} else if(fldAttr.equals("1")){
						if(fldLable !=null){%>
						<tr><td class="tdDefault" ><%=fldLable%> 
							<%} else {%> 
						<tr><td class="tdDefault" ><%=LC.L_AE_CATEGORY%>
						<%} 
						
						if(fldMand.equals("1")) {%>
						   <FONT class="Mandatory" id="pgcustomaecategory">* </FONT>
						<%}%>
						</td>
						<td>						
							<input maxLength="200" READONLY size="50" name="aeCategory" id="aeCategory" value = '<%=aeCategory%>' <%=disableStr%> />
						</td>
						<tr>
					<% }else if(fldAttr.equals("2")){
						
						if(fldLable !=null){%>
						<tr><td class="tdDefault" ><%=fldLable%> 
							<%} else {%> 
						<tr><td class="tdDefault" ><%=LC.L_AE_CATEGORY%>
							<%}
						
						if(fldMand.equals("1")) {%>
						   <FONT class="Mandatory" id="pgcustomaecategory">* </FONT>
						<%}%>
						</td>
						<td>						
							<input maxLength="200" size="50" name="aeCategory" id="aeCategory" value = '<%=aeCategory%>' readonly/>
						</td>
						</tr>
					<%}else{
					
					if(fldLable !=null){%>
						<tr><td class="tdDefault" ><%=fldLable%> 
							<%} else {%> 
						<tr><td class="tdDefault" ><%=LC.L_AE_CATEGORY%>
						<%} 
					
					if(fldMand.equals("1")) {%>
						   <FONT class="Mandatory" id="pgcustomaecategory">* </FONT>
					<%}%>
					</td>
					<td><input maxLength="200" size="50" name="aeCategory" id="aeCategory" value = '<%=aeCategory%>'/><td>
					</tr>
					<% }
			} else { %>
			<tr>
				<td class="tdDefault" width="20%"><%=LC.L_AE_CATEGORY %></td>
				<td width="80%" class="tdDefault">
					<input readonly maxLength="500" size="50" name="aeCategory" id="aeCategory" value= '<%=aeCategory%>' />
				</td>
			</tr>				
			<%} %>
			
			<% if(hashPgCustFld.containsKey("advname")){
					int fldNum = Integer.parseInt((String)hashPgCustFld.get("advname"));
					String fldMand = ((String)cdoPgField.getPcfMandatory().get(fldNum));
					String fldLable = ((String)cdoPgField.getPcfLabel().get(fldNum));
					String fldAttr = ((String)cdoPgField.getPcfAttribute().get(fldNum));
					
					if(fldAttr.equals("0")){%>
						<input type ="hidden" maxLength="500" name="advName" id="advName" value= '<%=advName%>' />
					<%} else if(fldAttr.equals("1")){
						if(fldLable !=null){%>
						<tr><td class="tdDefault" ><%=fldLable%> 
							<%} else {%> 
						<tr><td class="tdDefault" ><%=LC.L_Adv_EvtName%>
						<%} 
						
						if(fldMand.equals("1")) {%>
						   <FONT class="Mandatory" id="pagecustomadvname">* </FONT>
						<%}%>
						</td>
						<td>						
							<input maxLength="500" READONLY size="50" name="advName" id="advName" value = '<%=advName%>' <%=disableStr%> />
						</td>
						<tr>
					<% }else if(fldAttr.equals("2")){
						
						if(fldLable !=null){%>
						<tr><td class="tdDefault" ><%=fldLable%> 
							<%} else {%> 
						<tr><td class="tdDefault" ><%=LC.L_Adv_EvtName%>
							<%}
						
						if(fldMand.equals("1")) {%>
						   <FONT class="Mandatory" id="pagecustomadvname">* </FONT>
						<%}%>
						</td>
						<td>						
							<input maxLength="500" size="50" name="advName" id="advName" value = '<%=advName%>' readonly/>
						</td>
						</tr>
					<%}else{
					
					if(fldLable !=null){%>
						<tr><td class="tdDefault" ><%=fldLable%> 
							<%} else {%> 
						<tr><td class="tdDefault" ><%=LC.L_Adv_EvtName%>
						<%} 
					
					if(fldMand.equals("1")) {%>
						   <FONT class="Mandatory" id="pagecustomadvname">* </FONT>
					<%}%>
					</td>
					<td><input maxLength="500" size="50" name="advName" id="advName" value = '<%=advName%>'/><td>
					</tr>
					<% }
			} else { %>
			
			<tr>
				<td class="tdDefault" width="20%" id="advNameLabel"><%=LC.L_Adv_EvtName %></td>
				<td width="80%" class="tdDefault">
					<input readonly maxLength="500" size="50" name="advName" id="advName" value= '<%=advName%>' />
				</td>
			</tr>
			<%} %>
			
			
			<% if(hashPgCustFld.containsKey("toxicity")){
					int fldNum = Integer.parseInt((String)hashPgCustFld.get("toxicity"));
					String fldMand = ((String)cdoPgField.getPcfMandatory().get(fldNum));
					String fldLable = ((String)cdoPgField.getPcfLabel().get(fldNum));
					String fldAttr = ((String)cdoPgField.getPcfAttribute().get(fldNum));
					
					if(fldAttr.equals("0")){%>
						<input type ="hidden" maxLength="500" name="aeToxicity" id="aeToxicity" value= '<%=aeToxicity%>' />
					<%} else if(fldAttr.equals("1")){
						if(fldLable !=null){%>
						<tr><td class="tdDefault" ><%=fldLable%> 
							<%} else {%> 
						<tr><td class="tdDefault" ><%=LC.L_Toxicity%>
						<%} 
						
						if(fldMand.equals("1")) {%>
						   <FONT class="Mandatory" id="pagecustomaetoxicity">* </FONT>
						<%}%>
						</td>
						<td>						
							<input maxLength="500" READONLY size="50" name="aeToxicity" id="aeToxicity" value = '<%=aeToxicity%>' <%=disableStr%> />
						</td>
						<tr>
					<% }else if(fldAttr.equals("2")){
						
						if(fldLable !=null){%>
						<tr><td class="tdDefault" ><%=fldLable%> 
							<%} else {%> 
						<tr><td class="tdDefault" ><%=LC.L_Toxicity%>
							<%}
						
						if(fldMand.equals("1")) {%>
						   <FONT class="Mandatory" id="pagecustomaetoxicity">* </FONT>
						<%}%>
						</td>
						<td>						
							<input maxLength="500" size="50" name="aeToxicity" id="aeToxicity" value = '<%=aeToxicity%>' readonly/>
						</td>
						</tr>
					<%}else{
					
					if(fldLable !=null){%>
						<tr><td class="tdDefault" ><%=fldLable%> 
							<%} else {%> 
						<tr><td class="tdDefault" ><%=LC.L_Toxicity%>
						<%} 
					
					if(fldMand.equals("1")) {%>
						   <FONT class="Mandatory" id="pagecustomaetoxicity">* </FONT>
					<%}%>
					</td>
					<td><input maxLength="500" size="50" name="aeToxicity" id="aeToxicity" value = '<%=aeToxicity%>'/><td>
					</tr>
					<% }
			} else { %>
			
			<tr>
				<td class="tdDefault" width="20%"><%=LC.L_Toxicity %></td>
				<td width="80%" class="tdDefault">
					<input readonly maxLength="500" size="50" name="aeToxicity" id="aeToxicity" value= '<%=aeToxicity%>' />
				</td>
			</tr>
			<%} %>
			
			<% if(hashPgCustFld.containsKey("toxicitydesc")){
					int fldNum = Integer.parseInt((String)hashPgCustFld.get("toxicitydesc"));
					String fldMand = ((String)cdoPgField.getPcfMandatory().get(fldNum));
					String fldLable = ((String)cdoPgField.getPcfLabel().get(fldNum));
					String fldAttr = ((String)cdoPgField.getPcfAttribute().get(fldNum));
					
					if(fldAttr.equals("0")){%>
						<input type ="hidden" maxLength="4000" id="aeToxicityDesc" name="aeToxicityDesc" value= '<%=aeToxicityDesc%>' />
					<%} else if(fldAttr.equals("1")){
						if(fldLable !=null){%>
						<tr><td class="tdDefault" ><%=fldLable%> 
							<%} else {%> 
						<tr><td class="tdDefault" ><%=LC.L_AE_TOXICITY_DESC%>
						<%} 
						
						if(fldMand.equals("1")) {%>
						   <FONT class="Mandatory" id="pagecustomaetoxicitydesc">* </FONT>
						<%}%>
						</td>
						<td>						
							<input maxLength="4000" READONLY size="50" id="aeToxicityDesc" name="aeToxicityDesc" value = '<%=aeToxicityDesc%>' <%=disableStr%> />
						</td>
						<tr>
					<% }else if(fldAttr.equals("2")){
						
						if(fldLable !=null){%>
						<tr><td class="tdDefault" ><%=fldLable%> 
							<%} else {%> 
						<tr><td class="tdDefault" ><%=LC.L_AE_TOXICITY_DESC%>
							<%}
						
						if(fldMand.equals("1")) {%>
						   <FONT class="Mandatory" id="pagecustomaetoxicitydesc">* </FONT>
						<%}%>
						</td>
						<td>						
							<input maxLength="4000" size="50" name="aeToxicityDesc" id="aeToxicityDesc" value = '<%=aeToxicityDesc%>' readonly/>
						</td>
						</tr>
					<%}else{
					
					if(fldLable !=null){%>
						<tr><td class="tdDefault" ><%=fldLable%> 
							<%} else {%> 
						<tr><td class="tdDefault" ><%=LC.L_AE_TOXICITY_DESC%>
						<%} 
					
					if(fldMand.equals("1")) {%>
						   <FONT class="Mandatory" id="pagecustomaetoxicitydesc">* </FONT>
					<%}%>
					</td>
					<td><input maxLength="4000" size="50" name="aeToxicityDesc" id="aeToxicityDesc" value = '<%=aeToxicityDesc%>'/><td>
					</tr>
					<% }
			} else { %>
				
			<tr>
				<td class="tdDefault" width="20%"><%=LC.L_AE_TOXICITY_DESC %></td>
				<td width="80%" class="tdDefault">
					<input class="readonly" readonly maxLength="4000" size="50" name="aeToxicityDesc" id="aeToxicityDesc" value="<%=aeToxicityDesc%>"/>
				</td>
			</tr>
			<%} %>
			
			
			<% if(hashPgCustFld.containsKey("severity")){
					int fldNum = Integer.parseInt((String)hashPgCustFld.get("severity"));
					String fldMand = ((String)cdoPgField.getPcfMandatory().get(fldNum));
					String fldLable = ((String)cdoPgField.getPcfLabel().get(fldNum));
					String fldAttr = ((String)cdoPgField.getPcfAttribute().get(fldNum));
					
					if(fldAttr.equals("0")){%>
						<input type ="hidden" maxLength="255" name="grade" id="grade" value= '<%=grade%>' />
					<%} else if(fldAttr.equals("1")){
						if(fldLable !=null){%>
						<tr><td class="tdDefault" ><%=fldLable%> 
							<%} else {%> 
						<tr><td class="tdDefault" ><%=LC.L_Adv_EvtGrade%>
						<%} 
						
						if(fldMand.equals("1")) {%>
						   <FONT class="Mandatory" id="pagecustomgrade">* </FONT>
						<%}%>
						</td>
						<td>						
							<input maxLength="255" READONLY size="50" name="grade" id="grade" value = '<%=grade%>' <%=disableStr%> />
						</td>
						<tr>
					<% }else if(fldAttr.equals("2")){
						
						if(fldLable !=null){%>
						<tr><td class="tdDefault" ><%=fldLable%> 
							<%} else {%> 
						<tr><td class="tdDefault" ><%=LC.L_Adv_EvtGrade%>
							<%}
						
						if(fldMand.equals("1")) {%>
						   <FONT class="Mandatory" id="pagecustomgrade">* </FONT>
						<%}%>
						</td>
						<td>						
							<input maxLength="255" size="50" name="grade" id="grade" value = '<%=grade%>' readonly/>
						</td>
						</tr>
					<%}else{
					
					if(fldLable !=null){%>
						<tr><td class="tdDefault" ><%=fldLable%> 
							<%} else {%> 
						<tr><td class="tdDefault" ><%=LC.L_Adv_EvtGrade%>
						<%} 
					
					if(fldMand.equals("1")) {%>
						   <FONT class="Mandatory" id="pagecustomgrade">* </FONT>
					<%}%>
					</td>
					<td><input maxLength="255" size="50" name="grade" id="grade" value = '<%=grade%>'/><td>
					</tr>
					<% }
			} else { %>
			<tr>
				<td class="tdDefault" width="20%"><%=LC.L_Adv_EvtGrade %></td>
				<td width="80%" class="tdDefault">
					<input readonly maxLength="255" name="grade" id="grade" size="50" value= '<%=grade%>' />
				</td>
			</tr>
			<%} %>
			
			
			<% if(hashPgCustFld.containsKey("severitydesc")){
					int fldNum = Integer.parseInt((String)hashPgCustFld.get("severitydesc"));
					String fldMand = ((String)cdoPgField.getPcfMandatory().get(fldNum));
					String fldLable = ((String)cdoPgField.getPcfLabel().get(fldNum));
					String fldAttr = ((String)cdoPgField.getPcfAttribute().get(fldNum));
					
					if(fldAttr.equals("0")){%>
						<input type ="hidden" maxLength="4000" name="aeGradeDesc" id="aeGradeDesc" value= '<%=aeGradeDesc%>' />
					<%} else if(fldAttr.equals("1")){
						if(fldLable !=null){%>
						<tr><td class="tdDefault" ><%=fldLable%> 
							<%} else {%> 
						<tr><td class="tdDefault" ><%=LC.L_AE_GRADE_DESC%>
						<%} 
						
						if(fldMand.equals("1")) {%>
						   <FONT class="Mandatory" id="pagecustomaegradedesc">* </FONT>
						<%}%>
						</td>
						<td>						
							<input maxLength="4000" READONLY size="50" name="aeGradeDesc" id="aeGradeDesc" value = '<%=aeGradeDesc%>' <%=disableStr%> />
						</td>
						<tr>
					<% }else if(fldAttr.equals("2")){
						
						if(fldLable !=null){%>
						<tr><td class="tdDefault" ><%=fldLable%> 
							<%} else {%> 
						<tr><td class="tdDefault" ><%=LC.L_AE_GRADE_DESC%>
							<%}
						
						if(fldMand.equals("1")) {%>
						   <FONT class="Mandatory" id="pagecustomaegradedesc">* </FONT>
						<%}%>
						</td>
						<td>						
							<input maxLength="4000" size="50" name="aeGradeDesc" id="aeGradeDesc" value = '<%=aeGradeDesc%>' readonly/>
						</td>
						</tr>
					<%}else{
					
					if(fldLable !=null){%>
						<tr><td class="tdDefault" ><%=fldLable%> 
							<%} else {%> 
						<tr><td class="tdDefault" ><%=LC.L_AE_GRADE_DESC%>
						<%} 
					
					if(fldMand.equals("1")) {%>
						   <FONT class="Mandatory" id="pagecustomaegradedesc">* </FONT>
					<%}%>
					</td>
					<td><input maxLength="4000" size="50" name="aeGradeDesc" id="aeGradeDesc" value = '<%=aeGradeDesc%>'/><td>
					</tr>
					<% }
			} else { %>
			
			<tr>
				<td class="tdDefault" width="20%"><%=LC.L_AE_GRADE_DESC %></td>
				<td class="tdDefault" width="80%">
					<input readonly maxLength="4000" name="aeGradeDesc" id="aeGradeDesc" size="50" value="<%=aeGradeDesc%>"/>
				</td>
			</tr>
			<%} %>

	<tr>
		<%if (hashPgCustFld.containsKey("meddracode")) {

			int fldNumMeddra = Integer.parseInt((String)hashPgCustFld.get("meddracode"));
			String meddraMand = ((String)cdoPgField.getPcfMandatory().get(fldNumMeddra));
			String meddraLable = ((String)cdoPgField.getPcfLabel().get(fldNumMeddra));
			String meddraAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumMeddra));

			disableStr ="";
			readOnlyStr ="";
			if(meddraAtt == null) meddraAtt ="";
			if(meddraMand == null) meddraMand ="";


			if(!meddraAtt.equals("0")) {
			if(meddraLable !=null){
			%> <td class="tdDefault"  width="20%">
			&nbsp;&nbsp;&nbsp;&nbsp; <%=meddraLable%>
			<%} else {%> <td class="tdDefault" width="20%">
			&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Meddra_code%><%--MedDRA code*****--%>
			<%}

			if (meddraMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustommeddra">* </FONT>
			<% }
		 %>

		  <%if(meddraAtt.equals("1")) {
			 disableStr = "disabled class='readonly-input'"; }
		     else if (meddraAtt.equals("2") ) {
			 readOnlyStr = "readonly"; }
		 %>



	  </td>
	  <td class="tdDefault">
		<input type="text" maxLength="30"  size="50" name="MedDRAcode"  value='<%=meddracode%>' <%=disableStr%> <%=readOnlyStr%> >
	  </td>
    	<% } else { %>

	   <input type="hidden" maxLength="30"  size="50" name="MedDRAcode"  value='<%=meddracode%>' >

	  <% }}

	  else {
		 %>
		 <!--   //KM: added 05/10/06 May - June requirement PS1 -->
		<td class="tdDefault"  width="20%">&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Meddra_code%><%--MedDRA code*****--%> </td>
		<td class="tdDefault">
		<input type="text" maxLength="30"  size="50" name="MedDRAcode" readonly value='<%=meddracode%>' />
		</td>

     <%}%>

	</tr>




<tr>
		<%if (hashPgCustFld.containsKey("dictionary")) {

			int fldNumDict = Integer.parseInt((String)hashPgCustFld.get("dictionary"));
			String dictMand = ((String)cdoPgField.getPcfMandatory().get(fldNumDict));
			String dictLable = ((String)cdoPgField.getPcfLabel().get(fldNumDict));
			String dictAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumDict));

			disableStr ="";
			readOnlyStr ="";
			if(dictAtt == null) dictAtt ="";
			if(dictMand == null) dictMand ="";


			if(!dictAtt.equals("0")) {
			if(dictLable !=null){
			%> <td class="tdDefault"  width="20%">
			&nbsp;&nbsp;&nbsp;&nbsp; <%=dictLable%>
			<%} else {%> <td class="tdDefault" width="20%">
			&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Dictionary%><%--Dictionary*****--%>
			<%}

			if (dictMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomdict">* </FONT>
			<% }
		 %>

		  <%if(dictAtt.equals("1")) {
			 disableStr = "disabled class='readonly-input'"; }
		     else if (dictAtt.equals("2") ) {
			 readOnlyStr = "readonly"; }
		 %>

	  </td>
	  <td class="tdDefault">
		<input type="text" maxLength="30"  size="50" name="advDictionary"  value='<%=advDictionary%>' readonly <%=disableStr%> >

		<%
		if (!mode.equals("N")) {
			if (patStatSubType.equals("lockdown")) { pageRight=4;}
		}
		%>



	 </td>
    	<% } else { %>

	   <input type="hidden" maxLength="30"  size="50" name="advDictionary"  value='<%=advDictionary%>' >

	  <% }}

	  else {  %>

		<%
				if(mode.equals("N")) { %>


					<td class="tdDefault"  width="20%">&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Dictionary%><%--Dictionary*****--%> </td>
					<td class="tdDefault">
					<input type="text" maxLength="30"  size="50" name="advDictionary"  value='<%=advDictionary%>' readonly>
					</td>

				<%} else { %>

					<td class="tdDefault"  width="20%">&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Dictionary%><%--Dictionary*****--%> </td>
					<td class="tdDefault">
					<input type="text" maxLength="30"  size="50" name="advDictionary"  value='<%=advDictionary%>' readonly>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<%if (patStatSubType.equals("lockdown")) { pageRight=4;} %>
					</td>

				<% } %>


     <%}%>

	</tr>
	<tr>
        <%if (hashPgCustFld.containsKey("description")) {
			int fldNumDesc = Integer.parseInt((String)hashPgCustFld.get("description"));
			String descMand = ((String)cdoPgField.getPcfMandatory().get(fldNumDesc));
			String descLable = ((String)cdoPgField.getPcfLabel().get(fldNumDesc));
			String descAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumDesc));

			disableStr ="";
			readOnlyStr ="";
			if(descAtt == null) descAtt ="";
			if(descMand == null) descMand ="";


			if(!descAtt.equals("0")) {
			if(descLable !=null){
			%><td class="tdDefault" width="20%"> &nbsp;&nbsp;&nbsp;&nbsp;
			<%=descLable%>
			<%} else {%> <td>
			 &nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_OTHER_DESC%><%--Description*****--%>
			<%}

			if (descMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomdesc">* </FONT>
			<% }
		 %>

		  <%if(descAtt.equals("1")) {
			 disableStr = "disabled class='readonly-input'"; }
		     else if (descAtt.equals("2") ) {
			 readOnlyStr = "readonly"; }
		 %>



		</td>
		<td class="tdDefault">
			<TEXTAREA id=descripton name=descripton  rows=3 cols=38  <%=disableStr%>  <%=readOnlyStr%> ><%=descripton%></TEXTAREA>
			<br><font class="Mandatory"><div id="charlimitinfodescripton"><%=MC.M_Limit2000_CharLeft %></div></font>
		</td>
			<% } else { %>

		 <TEXTAREA id=descripton name=descripton  Style = "visibility:hidden"  rows=3 cols=38> <%=descripton%></TEXTAREA>
		 <br><font class="Mandatory"><div id="charlimitinfodescripton"><%=MC.M_Limit2000_CharLeft %></div></font>

		 <% }} else {
			 %>
		<td class="tdDefault"  width="20%">&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_OTHER_DESC%><%--Description*****--%> </td>
		<td class="tdDefault">
				<TEXTAREA id=descripton name=descripton  rows=3 cols=38><%=descripton%></TEXTAREA>
				<br><font class="Mandatory"><div id="charlimitinfodescripton"><%=MC.M_Limit2000_CharLeft %></div></font>

		</td>
	  <%}%>
  	  </tr>
<tr>
		<%
			String courseAtt ="";
			if (hashPgCustFld.containsKey("treatmentcourse")) {

			int fldNumCourse = Integer.parseInt((String)hashPgCustFld.get("treatmentcourse"));
			String courseMand = ((String)cdoPgField.getPcfMandatory().get(fldNumCourse));
			String courseLable = ((String)cdoPgField.getPcfLabel().get(fldNumCourse));
			courseAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumCourse));

			disableStr ="";
			readOnlyStr ="";
			if(courseAtt == null) courseAtt ="";
			if(courseMand == null) courseMand ="";

			if(!courseAtt.equals("0")) {
			if(courseLable !=null){
			%> <td class="tdDefault" >
			&nbsp;&nbsp;&nbsp;&nbsp; <%=courseLable%>
			<%} else {%> <td class="tdDefault">
			&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Treatment_Course%><%--Treatment Course*****--%>
			<%}

			if (courseMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomcourse">* </FONT>
			<% }
		 %>

		  <%if(courseAtt.equals("1")) {
			 disableStr = "disabled class='readonly-input'"; }
		     else if (courseAtt.equals("2") ) {
			 readOnlyStr = "readonly"; }
		 %>



	  </td>
	  <td class="tdDefault">
		<input type="text" name="treatment" maxlength="150" size="50" value="<%=treatment%>" <%=disableStr%> <%=readOnlyStr%>>
	  </td>
    	<% } else { %>

	  <input type="hidden" name="treatment" maxlength="150" size="50" value="<%=treatment%>">

	  <% }}

	  else {
		 %>
		<td class="tdDefault"  width="20%">&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Treatment_Course%><%--Treatment Course*****--%></td>
		<td class="tdDefault">
			<input type="text" name="treatment" maxlength="150" size="50" value="<%=treatment%>">
		</td>

      <%}%>

	  </tr>

	   <% if(!courseAtt.equals("0")) { %>
	  <tr>
	  <td></td>
		<td id="tdComments">(<%=MC.M_CycleNum_CourseId%><%--e.g. cycle number, course ID etc.*****--%>)</td>
	  </tr>
	  <%}%>

		<tr>
		<%

				if (hashPgCustFld.containsKey("startdate")) {

				int fldNumStartDt = Integer.parseInt((String)hashPgCustFld.get("startdate"));
				String startDtMand = ((String)cdoPgField.getPcfMandatory().get(fldNumStartDt));
				String startDtLable = ((String)cdoPgField.getPcfLabel().get(fldNumStartDt));
				String startDtAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumStartDt));

				disableStr ="";
				readOnlyStr ="";

				if(startDtAtt == null) startDtAtt ="";
  				if(startDtMand == null) startDtMand ="";

				if(!startDtAtt.equals("0")) {

				if(startDtLable !=null){
				%>
				<td class="tdDefault" width="20%">
				 <%=startDtLable%>  
				<%} else {%> <td width="20%">
				   <%=LC.L_Start_Date%><%--Start Date*****--%> 
				<% }


			   if (startDtMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomstdt">* </FONT>
		 	   <% }
			   %>

			   <%if(startDtAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }
				 else if (startDtAtt.equals("2") ) {
					 readOnlyStr = "readonly"; }

  		       %>


 			   </td>
			  <td class="tdDefault">
				<%-- INF-20084 Datepicker-- AGodara --%>
				<%if (StringUtil.isEmpty(startDtAtt)) { %>
			   		<INPUT type="text" name="startDt" class="cusdatefield" size=10  value="<%=startDt%>">
			   	<%} else if (startDtAtt.equals("1") || startDtAtt.equals("2")) { %>
			   		<INPUT type="text" name="startDt" size=10 <%=readOnlyStr%> <%=disableStr%>  value="<%=startDt%>"  >
			   	<%}%>
			 </td>
			<%}else{ %>
					<INPUT type="hidden" name="startDt" class="datefield" size=10  value="<%=startDt%>" >
		<%}}else {%>
					<td class="tdDefault"><%=LC.L_Start_Date%><%--Start Date*****--%> <FONT class="Mandatory" id="mandstdate">* </FONT></td>
					<td class="tdDefault"><INPUT type=text readonly name=startDt class="cusdatefield" size=10  value=<%=startDt%>></td>
		<%}%>
			</tr>
		<tr>

		 <%
				if (hashPgCustFld.containsKey("stopdate")) {

				int fldNumStopDt = Integer.parseInt((String)hashPgCustFld.get("stopdate"));
				String stopDtMand = ((String)cdoPgField.getPcfMandatory().get(fldNumStopDt));
				String stopDtLable = ((String)cdoPgField.getPcfLabel().get(fldNumStopDt));
				String stopDtAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumStopDt));

				disableStr ="";
				readOnlyStr ="";

				if(stopDtAtt == null) stopDtAtt ="";
  				if(stopDtMand == null) stopDtMand ="";

				if(!stopDtAtt.equals("0")) {

				if(stopDtLable !=null){
				%>
				<td class="tdDefault" width="20%">&nbsp;&nbsp;&nbsp;&nbsp;
				 <%=stopDtLable%>
				<%} else {%> <td width="20%">
				  &nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Stop_Date%><%--Stop Date*****--%> 
				<% }


			   if (stopDtMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomstopdt">* </FONT>
		 	   <% }
			   %>

			   <%if(stopDtAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }
				 else if (stopDtAtt.equals("2") ) {
					 readOnlyStr = "readonly"; }

  		       %>


 			   </td>
			  <td class="tdDefault">
				<%-- INF-20084 Datepicker-- AGodara --%>
			 	<%if (StringUtil.isEmpty(stopDtAtt)) { %>
			   		<INPUT type="text" name="stopDt" class="cusdatefield" size=10  value="<%=stopDt%>">
			   	<%} else if(stopDtAtt.equals("1") || stopDtAtt.equals("2")) { %>
			 		<INPUT type="text" name="stopDt" size=10 <%=readOnlyStr%> <%=disableStr%> value="<%=stopDt%>"  >
			   	<%}%>
			  </td>
			<%}else{ %>
					<INPUT type="hidden" name="stopDt" class="cusdatefield" size=10  value="<%=stopDt%>" >
		<%}}else{%>
					<td class="tdDefault">&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Stop_Date%><%--Stop Date*****--%></td>
					<td class="tdDefault"><INPUT type=text readonly name=stopDt class="cusdatefield" size=10  value=<%=stopDt%>></td>
		<%}%>
			</tr>


			<tr>

		 <%
				if (hashPgCustFld.containsKey("discoverydate")) {

				int fldNumDiscDt = Integer.parseInt((String)hashPgCustFld.get("discoverydate"));
				String discDtMand = ((String)cdoPgField.getPcfMandatory().get(fldNumDiscDt));
				String discDtLable = ((String)cdoPgField.getPcfLabel().get(fldNumDiscDt));
				String discDtAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumDiscDt));

				disableStr ="";
				readOnlyStr ="";

				if(discDtAtt == null) discDtAtt ="";
  				if(discDtMand == null) discDtMand ="";

				if(!discDtAtt.equals("0")) {

				if(discDtLable !=null){
				%>
				<td class="tdDefault" width="20%">&nbsp;&nbsp;&nbsp;&nbsp;
				 <%=discDtLable%>
				<%} else {%> <td>
				  &nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Ae_DiscoveryDate%><%--AE Discovery Date*****--%> 
				<% }


			   if (discDtMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomdiscdt">* </FONT>
		 	   <% }
			   %>

			   <%if(discDtAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }
				 else if (discDtAtt.equals("2") ) {
					 readOnlyStr = "readonly"; }

  		       %>

 			   </td>
			  <td class="tdDefault" width="20%">
				<%-- INF-20084 Datepicker-- AGodara --%>
				<%if (StringUtil.isEmpty(discDtAtt)) { %>
			   		<INPUT type="text" name="aediscoveryDt" class="datefield" size="10"  value="<%=discoveryDt%>">
			   	<%} else if(discDtAtt.equals("1") || discDtAtt.equals("2")) { %>
			 		<INPUT type="text" name="aediscoveryDt" size=10 <%=readOnlyStr%> <%=disableStr%> value="<%=discoveryDt%>" >
			   	<%} %>
			  </td>
			<%}else{ %>
				<INPUT type="hidden" name="aediscoveryDt" class="datefield" size=10  value="<%=discoveryDt%>" >
		<%}}else{%>
				<td class="tdDefault" width="20%">&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Ae_DiscoveryDate%><%--AE Discovery Date*****--%>  </FONT></td>
				<td class="tdDefault"><INPUT type=text readonly name=aediscoveryDt class="datefield" size=10  value=<%=discoveryDt%>></td>
		<%}%>
			</tr>
			<tr>

		 <%
				if (hashPgCustFld.containsKey("loggeddate")) {

				int fldNumLogDt = Integer.parseInt((String)hashPgCustFld.get("loggeddate"));
				String logDtMand = ((String)cdoPgField.getPcfMandatory().get(fldNumLogDt));
				String logDtLable = ((String)cdoPgField.getPcfLabel().get(fldNumLogDt));
				String logDtAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumLogDt));

				disableStr ="";
				readOnlyStr ="";

				if(logDtAtt == null) logDtAtt ="";
  				if(logDtMand == null) logDtMand ="";

				if(!logDtAtt.equals("0")) {

				if(logDtLable !=null){
				%>
				<td class="tdDefault" width="20%">&nbsp;&nbsp;&nbsp;&nbsp;
				 <%=logDtLable%>
				<%} else {%> <td>
				  &nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Ae_LoggedDate%><%--AE Logged Date*****--%> 
				<% }


			   if (logDtMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomlogdt">* </FONT>
		 	   <% }
			   %>

			   <%if(logDtAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }
				 else if (logDtAtt.equals("2") ) {
					 readOnlyStr = "readonly"; }

  		       %>

 			   </td>
			  <td class="tdDefault">
				<%-- INF-20084 Datepicker-- AGodara --%>
			 	<%if (StringUtil.isEmpty(logDtAtt)) { %>
			   		<INPUT type="text" name="aeloggedDt" class="datefield" size=10  value="<%=loggedDt%>">
			   	<%} else if(logDtAtt.equals("1") || logDtAtt.equals("2")) { %>
					<INPUT type="text" name="aeloggedDt" size=10 <%=readOnlyStr%> <%=disableStr%> value="<%=loggedDt%>" >
			   	<%} %>
			  </td>
			<%}else{%>
				 <INPUT type="hidden" name="aeloggedDt" class="datefield" size=10  value="<%=loggedDt%>">
		<%}}else{%>
				<td class="tdDefault" width="20%">&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Ae_LoggedDate%><%--AE Logged Date*****--%>  </FONT></td>
				<td class="tdDefault" width="80%"><INPUT type=text name=aeloggedDt readonly class="datefield" size=10  value=<%=loggedDt%>></td>
		<%}%>
			</tr>
			<tr>
		 <%
				if (hashPgCustFld.containsKey("enteredby")) {

				int fldNumEntby = Integer.parseInt((String)hashPgCustFld.get("enteredby"));
				String entMand = ((String)cdoPgField.getPcfMandatory().get(fldNumEntby));
				String entLable = ((String)cdoPgField.getPcfLabel().get(fldNumEntby));
				String entAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumEntby));

				disableStr ="";
				readOnlyStr ="";
				%>
					<input name="death" type=hidden value=<%=death%>>
					<input name="eventId" type=hidden value=<%=eventId %>>
					<input name="pkey" type=hidden value=<%=pkey%>>
					<input name="eventName" type=hidden value="<%=eventName%>">
					<input name="visit" type=hidden value=<%=visit%>>
					<input name="srcmenu" type=hidden value=<%=src%>>
					<input name="adveventId" type=hidden value=<%=adveventId%>>
					<input id="mode" name="mode" type=hidden value=<%=mode%>>
					<input id="advMode" name="advMode" type=hidden value=<%=mode%>>
					<input type="hidden" name=studyId value=<%=studyId%>>
					<input type="hidden" name=statDesc value=<%=statDesc%>>
					<input type="hidden" name=statid value=<%=statid%>>
					<input type="hidden" name=patProtId value=<%=patProtId%>>
					<input type=hidden name=studyVer value=<%=studyVer%>>
					<input type=hidden name=studyNum value=<%=studyNum%>>
					<input type=hidden name=patientCode value=<%=patientId%>>
				<%
				if(entAtt == null) entAtt ="";
  				if(entMand == null) entMand ="";

				if(!entAtt.equals("0")) {

				if(entLable !=null){
				%>
				<td width="20%">
				 <%=entLable%> 
				<%} else {%> <td width="20%">
				<%=LC.L_Entered_By%><%--Entered By*****--%> 
				<% }


			   if (entMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustoment">* </FONT>
		 	   <% }
			   %>

			   <%if(entAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }
				 else if (entAtt.equals("2") ) {
					 readOnlyStr = "readonly"; }

  		       %>

 			   </td>
			  <td>


			 <input type=hidden name=enteredBy <%=disableStr%> value=<%=enteredBy%> >
			 <input type=text name=enteredByName readonly value="<%=enteredByName%>" <%=disableStr%>>


			 <%if(!entAtt.equals("1") && !entAtt.equals("2")) { %>
			 <A HREF=# onClick="advEventNewFunctions.openwin1('entrBy')" ><%=LC.L_Select_User%><%--Select User*****--%></A>
			  <%}%>

			  </td>

			<%} else { %>

			<input type=hidden name=enteredBy value=<%=enteredBy%>>
			<input type=hidden name=enteredByName readonly value="<%=enteredByName%>">

			<% }}  else {%>
			<td width="20%"><%=LC.L_Entered_By%><%--Entered By*****--%> <FONT class="Mandatory" id="mandent">* </FONT>
					</td>
					<td>
					<input name="death" type=hidden value=<%=death%>>
					<input name="eventId" type=hidden value=<%=eventId %>>
					<input name="pkey" type=hidden value=<%=pkey%>>
					<input name="eventName" type=hidden value="<%=eventName%>">
					<input name="visit" type=hidden value=<%=visit%>>
					<input name="srcmenu" type=hidden value=<%=src%>>
					<input name="adveventId" type=hidden value=<%=adveventId%>>
					<input id="mode" name="mode" type=hidden value=<%=mode%>>
					<input id="advMode" name="advMode" type=hidden value=<%=mode%>>
					<input type=hidden name=enteredBy value=<%=enteredBy%>>
					<input type="hidden" name=studyId value=<%=studyId%>>
					<input type="hidden" name=statDesc value=<%=statDesc%>>
					<input type="hidden" name=statid value=<%=statid%>>
					<input type="hidden" name=patProtId value=<%=patProtId%>>
					<input type=hidden name=studyVer value=<%=studyVer%>>
					<input type=hidden name=studyNum value=<%=studyNum%>>
					<input type=hidden name=patientCode value=<%=patientId%>>
					<input type=text name=enteredByName readonly value="<%=enteredByName%>">
					<A HREF=# onClick="advEventNewFunctions.openwin1('entrBy')" ><%=LC.L_Select_User%><%--Select User*****--%></A>
				</td>

			 <%}%>
			</tr>


		 <tr>
		 <%
				if (hashPgCustFld.containsKey("reportedby")) {

				int fldNumRepby = Integer.parseInt((String)hashPgCustFld.get("reportedby"));
				String repMand = ((String)cdoPgField.getPcfMandatory().get(fldNumRepby));
				String repLable = ((String)cdoPgField.getPcfLabel().get(fldNumRepby));
				String repAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumRepby));

				disableStr ="";
				readOnlyStr ="";
				%>

				<input name="death"   type=hidden value=<%=death%>>
				<input name="eventId" type=hidden value=<%=eventId %>>
				<input name="pkey"    type=hidden value=<%=pkey%>>
				<input name="eventName" type=hidden value="<%=eventName%>">
				<input name="visit" type=hidden value=<%=visit%>>
				<input name="srcmenu" type=hidden value=<%=src%>>
				<input name="adveventId" type=hidden value=<%=adveventId%>>
				<input id="mode" name="mode" type=hidden value=<%=mode%>>
				<input id="advMode" name="advMode" type=hidden value=<%=mode%>>
				<input type="hidden" name=studyId value=<%=studyId%>>
				<input type="hidden" name=statDesc value=<%=statDesc%>>
				<input type="hidden" name=statid value=<%=statid%>>
				<input type="hidden" name=patProtId value=<%=patProtId%>>
				<input type= hidden   name=studyVer value=<%=studyVer%>>
				<input type= hidden   name=studyNum value=<%=studyNum%>>
				<input type= hidden   name=patientCode value=<%=patientId%>>




				<%
				if(repAtt == null) repAtt ="";
  				if(repMand == null) repMand ="";

				if(!repAtt.equals("0")) {

				if(repLable !=null){
				%>
				<td width="20%">
				&nbsp;&nbsp;&nbsp;&nbsp; <%=repLable%>
				<%} else {%> <td width="20%">
				&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Rpted_By%><%--Reported By*****--%>
				<% }


			   if (repMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomrep">* </FONT>
		 	   <% }
			   %>

			   <%if(repAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }
				 else if (repAtt.equals("2") ) {
					 readOnlyStr = "readonly"; }

  		       %>

 			   </td>
			  <td>

			 <input type=hidden name=reportedBy <%=disableStr%> value=<%=reportedBy%>>
			 <input type= text name=reportedByName readonly value="<%=reportedByName%>" <%=disableStr%>>

			 <%if(!repAtt.equals("1") && !repAtt.equals("2")) { %>
			 <A HREF=# onClick="advEventNewFunctions.openwin1('repBy')" ><%=LC.L_Select_User%><%--Select User*****--%></A>
			  <%}%>

			  </td>

			<%} else { %>

			<input type=hidden name=reportedBy value=<%=reportedBy%>>
			<input type= hidden name=reportedByName readonly value="<%=reportedByName%> ">
			<% }}  else {%>
					<td width="20%">&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Rpted_By%><%--Reported By*****--%>
					</td>
					<td>
					<input name="death"   type=hidden value=<%=death%>>
					<input name="eventId" type=hidden value=<%=eventId %>>
					<input name="pkey"    type=hidden value=<%=pkey%>>
					<input name="eventName" type=hidden value="<%=eventName%>">
			        <input name="visit" type=hidden value=<%=visit%>>
			        <input name="srcmenu" type=hidden value=<%=src%>>
			        <input name="adveventId" type=hidden value=<%=adveventId%>>
			        <input id="mode" name="mode" type=hidden value=<%=mode%>>

					<input type=hidden name=reportedBy value=<%=reportedBy%>>
					<input type="hidden" name=studyId value=<%=studyId%>>
					<input type="hidden" name=statDesc value=<%=statDesc%>>
					<input type="hidden" name=statid value=<%=statid%>>
					<input type="hidden" name=patProtId value=<%=patProtId%>>
					<input type= hidden   name=studyVer value=<%=studyVer%>>
					<input type= hidden   name=studyNum value=<%=studyNum%>>
					<input type= hidden   name=patientCode value=<%=patientId%>>
					<input type= text name=reportedByName readonly value="<%=reportedByName%>">
						<A HREF=# onClick="advEventNewFunctions.openwin1('repBy')" ><%=LC.L_Select_User%><%--Select User*****--%></A>
					</td>
			 <%}%>
			</tr>



	 <tr>
	 <%if (hashPgCustFld.containsKey("attribution")) {
			int fldNumAttr = Integer.parseInt((String)hashPgCustFld.get("attribution"));
			String attrMand = ((String)cdoPgField.getPcfMandatory().get(fldNumAttr));
			String attrLable = ((String)cdoPgField.getPcfLabel().get(fldNumAttr));
			attrAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumAttr));

			if(attrAtt == null) attrAtt ="";
			if(attrMand == null) attrMand ="";


			if(!attrAtt.equals("0")) {
			if(attrLable !=null){
			%> <td class="tdDefault"  width="20%">
			 <%=attrLable%> 
			<%} else {%> <td width="20%">
			 <%=LC.L_Attribution%><%--Attribution*****--%>
			<%}

			if (attrMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomattr">* </FONT>
			<% } %>

    </td>
    <td>

	 <%=dCur_drugproc%>
	 <%if(attrAtt.equals("2")) {%>
	 	<input type="hidden" name="adve_relation" value="<%=drugdroc%>">
	 <%}%>
    </td>
	<%} else if(attrAtt.equals("0")) {%>

	<input type="hidden" name="adve_relation" value ="<%=drugdroc%>" >
	<%}} else {%>
    <td class="tdDefault" width="20%" >
	<%=LC.L_Attribution%><%--Attribution*****--%>   <!-- Relationship to the study drug and/or procedure -->
	</td>
	<td>	<%=dCur_drugproc%> </td>
    <%}%>
  </tr>



		 <%
			if(linkedToName == null){
			  linkedToName = "";
			}
		 %>


		<tr>

		 <%
				if (hashPgCustFld.containsKey("linkedto")) {

				int fldNumLink = Integer.parseInt((String)hashPgCustFld.get("linkedto"));
				String linkMand = ((String)cdoPgField.getPcfMandatory().get(fldNumLink));
				String linkLable = ((String)cdoPgField.getPcfLabel().get(fldNumLink));
				String linkAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumLink));

				disableStr ="";
				readOnlyStr ="";

				if(linkAtt == null) linkAtt ="";
  				if(linkMand == null) linkMand ="";

				if(!linkAtt.equals("0")) {

				if(linkLable !=null){
				%>
				<td width="20%">
				 <%=linkLable%>
				<%} else {%> <td width="20%">
				  <%=LC.L_Linked_To%><%--Linked To*****--%>
				<% }


			   if (linkMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomlink">* </FONT>
		 	   <% }
			   %>

			   <%if(linkAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }
				 else if (linkAtt.equals("2") ) {
					 readOnlyStr = "readonly"; }

  		       %>

 			   </td>
			  <td class="tdDefault">

			 <input type= hidden name=linkedToId readonly value="<%=linkedToId%>">
			 <input type= text name=linkedToName readonly value="<%=linkedToName%>" <%=readOnlyStr%> <%=disableStr%> >


			 <%if(!linkAtt.equals("1") && !linkAtt.equals("2")) { %>
			 <A HREF=# onClick="advEventNewFunctions.openAdverseEvent(<%=studyId%>,<%=personPK%>,'<%=src%>')" ><%=LC.L_Select_AdvEvts%><%--Select Adverse Events*****--%></A>
			  <%}%>

			  </td>

			<%} else { %>

			 <input type= hidden name=linkedToName readonly value="<%=linkedToName%>">
			<% }}  else {%>
			<td><%=LC.L_Linked_To%><%--Linked To*****--%>
			</td>
			<td>

			<input type= hidden name=linkedToId readonly value="<%=linkedToId%>">
			<input type= text name=linkedToName readonly value="<%=linkedToName%>">
			<A HREF=# onClick="advEventNewFunctions.openAdverseEvent(<%=studyId%>,<%=personPK%>,'<%=src%>')" ><%=LC.L_Select_AdvEvts%><%--Select Adverse Events*****--%></A>
			</td>
			 <%}%>
			</tr>

				<tr>
					<td COLSPAN=2>
						<p class = "sectionHeadings" > <%=LC.L_Outcome_Info%><%--Outcome Information*****--%></p>
					</td>
				</tr>


				<!-- startkm -->


				<%SchCodeDao outcome = new SchCodeDao();
				outcome.getCodeValues("outcome");
				ArrayList codelstDescs=outcome.getCDesc();
				outcomeIds=outcome.getCId();
				ArrayList outcomeActions=outcome.getCDesc();
				ArrayList codeLstSubTypes = outcome.getCSubType();
				String outcomeAction="";
				String codelstdesc="";
				String codeLstSubTyp="";
				int length=codelstDescs.size();

				ArrayList advInfoOutIds =null;
				ArrayList advInfoCodeLstIds =null;
				ArrayList advInfoCodeVals =null;
				
				String advInfoOutId =null;
				if(mode.equals("M")){
				EventInfoDao eventInfoDao = new EventInfoDao();
				eventInfoDao = eventdefB.getAdvInfo(StringUtil.stringToNum(adveventId),"O") ;
				advInfoOutIds =eventInfoDao.getAdvInfoIds();
				advInfoCodeLstIds = eventInfoDao.getAdvInfoCodelstInfoIds();
				advInfoCodeVals = eventInfoDao.getAdvInfoValues();
				}

				%>
			   <input  type="hidden" name="outcomeString">

				<%

				if (hashPgCustFld.containsKey("outcometype")) {

					int fldNumOutType = Integer.parseInt((String)hashPgCustFld.get("outcometype"));
					String outTypeMand = ((String)cdoPgField.getPcfMandatory().get(fldNumOutType));
					String outTypeLable = ((String)cdoPgField.getPcfLabel().get(fldNumOutType));
					String outTypeAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumOutType));

					disableStr ="";
					readOnlyStr ="";
					if(outTypeAtt == null) outTypeAtt ="";
					if(outTypeMand == null) outTypeMand ="";

					if(!outTypeAtt.equals("0")) {

					 for (int i=0;i<length; i++) {
						codelstdesc=(String)  codelstDescs.get(i);
						outcomeId=(String) outcomeIds.get(i).toString();
						codeLstSubTyp = (String) codeLstSubTypes.get(i);
						strIndex = '0';
						status="";
						advInfoOutId=null;
						
						if(mode.equals("M")){
							
							// Checking the codelist Ids aganist Ids stored in database 
							for(int j=0;j<advInfoCodeLstIds.size();j++){
								if(advInfoCodeLstIds.get(j).toString().equals(outcomeId) ){
									advInfoOutId = advInfoOutIds.get(j).toString();
									if( advInfoCodeVals.get(j).toString().equals("1")){
										status="Checked=true";
										strIndex = '1';
										break;
									}
								}
							}
							
						}

						
					%><tr><td width="20%">
					<% if (i==0) {

									if(outTypeLable !=null) { %>
									   <%=outTypeLable%>
								<%	}
									else {
										out.println(LC.L_Outcome_Type/*"Outcome Type"*****/); }


							if (outTypeMand.equals("1")) {
					%>

					   <FONT class="Mandatory" id="pgcustomouttype">* </FONT>

					<% }

					}

					%>

					 <%if(outTypeAtt.equals("1")) {
						 disableStr = "disabled class='readonly-input'"; }
						 else if (outTypeAtt.equals("2") ) {
						 readOnlyStr = "disabled"; }
					 %>


						</td>
						<td>
						   <input type="checkbox" data-subType="<%=codeLstSubTyp %>" <%=disableStr%>  name="outcomeType"    <%=readOnlyStr%> value=<%=outcomeId%>  <%=status%> onclick="advEventNewFunctions.checkoutcome(document.adverseEventScreenForm,'<%=codeLstSubTyp %>',<%=length%>)"> <%=codelstdesc%>
							<% if (codeLstSubTyp.equalsIgnoreCase("al_death")) {%>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <%=LC.L_Date%><%--Date*****--%> &nbsp;
								<%if(outTypeAtt.equals("2")) { %>
									<INPUT type=text name="outcomeDt" readonly size=10  value="<%=outcomeDt%>">
							   	<%}else if (outTypeAtt.equals("1")){%>
							   		<INPUT type=text name="outcomeDt" <%=disableStr%> size=10  value="<%=outcomeDt%>">
							   	<%}
							}%>
						</td>
					</tr>
					<input type="hidden" name=outcomeLen value=<%=length%>>
					<input type="hidden" name=outcomeId value=<%=outcomeId%>>
					<input type="hidden" name=advInfoOutId value=<%=advInfoOutId%>>
				<%}%>

				<% } else { %>


				<% for (int i=0;i<length; i++) {
					codelstdesc=(String)  codelstDescs.get(i);
					outcomeId=(String) outcomeIds.get(i).toString();
					codeLstSubTyp = (String) codeLstSubTypes.get(i);
					strIndex = '0';
					status="";
					advInfoOutId=null;
					
					if(mode.equals("M")){
						
						// Checking the codelist Ids aganist Ids stored in database 
						for(int j=0;j<advInfoCodeLstIds.size();j++){
							if(advInfoCodeLstIds.get(j).toString().equals(outcomeId) ){
								advInfoOutId = advInfoOutIds.get(j).toString();
								if( advInfoCodeVals.get(j).toString().equals("1")){
									status="Checked=true";
									strIndex = '1';
									break;
								}
							}
						}
					}
					%>

						   <input type="checkbox" data-subType="<%=codeLstSubTyp %>" name="outcomeType"  Style = "visibility:hidden" value=<%=outcomeId%>  <%=status%> onclick="advEventNewFunctions.checkoutcome(document.adverseEventScreenForm,'<%=codeLstSubTyp %>',<%=length%>)">
							<% if (codeLstSubTyp.equalsIgnoreCase("al_death")) {%>
								 <INPUT type=hidden name=outcomeDt  size=10  value=<%=outcomeDt%>>

							<%}%>

					<input type="hidden" name=outcomeLen value=<%=length%>>
					<input type="hidden" name=outcomeId value=<%=outcomeId%>>
					<input type="hidden" name=advInfoOutId value=<%=advInfoOutId%>>
				<%}%>


				<% } } else { %>


				<% for (int i=0;i<length; i++) {
					
					codelstdesc=(String)  codelstDescs.get(i);
					outcomeId=(String) outcomeIds.get(i).toString();
					codeLstSubTyp = (String) codeLstSubTypes.get(i);
					
					strIndex = '0';
					advInfoOutId=null;
					status="";
					if(mode.equals("M")){
						
						// Checking the codelist Ids aganist Ids stored in database 
						for(int j=0;j<advInfoCodeLstIds.size();j++){
							if(advInfoCodeLstIds.get(j).toString().equals(outcomeId) ){
								advInfoOutId = advInfoOutIds.get(j).toString();
								if( advInfoCodeVals.get(j).toString().equals("1")){
									status="Checked=true";
									strIndex = '1';
									break;
								}
							}
						}
						
					}%>

					<tr>
						<td width="20%">
							<% if (i==0) {
								out.println(LC.L_Outcome_Type/*"Outcome Type"*****/);
							}
							%>
						</td>
						<td>
						   <input type="checkbox" data-subType="<%=codeLstSubTyp %>" name="outcomeType" value=<%=outcomeId%>  <%=status%> onclick="advEventNewFunctions.checkoutcome(document.adverseEventScreenForm,'<%=codeLstSubTyp %>',<%=length%>)"> <%=codelstdesc%>
							<% if (codeLstSubTyp.equalsIgnoreCase("al_death")) {%>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <%=LC.L_Date%><%--Date*****--%> &nbsp; <INPUT type=text readonly name="outcomeDt" class="datefield" size=10  value="<%=outcomeDt%>">
							<%}%>
						</td>
					</tr>
					<input type="hidden" name=outcomeLen value=<%=length%>>
					<input type="hidden" name=outcomeId value=<%=outcomeId%>>
					<input type="hidden" name=advInfoOutId value=<%=advInfoOutId%>>
				<%} }%>

				<tr>
					<td> &nbsp;
					</td>
					<td> &nbsp;
					</td>
				</tr>
				<tr>
				 <%if (hashPgCustFld.containsKey("action")) {
						int fldNumAct = Integer.parseInt((String)hashPgCustFld.get("action"));
						String actMand = ((String)cdoPgField.getPcfMandatory().get(fldNumAct));
						String actLable = ((String)cdoPgField.getPcfLabel().get(fldNumAct));
						actAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumAct));

						if(actAtt == null) actAtt ="";
						if(actMand == null) actMand ="";

						if(!actAtt.equals("0")) {
						if(actLable !=null){
						%> <td class="tdDefault" >
						<%=actLable%>
						<%} else {%> <td>
						 <%=LC.L_Action%><%--Action*****--%>
						<%}

						if (actMand.equals("1")) {
							%>
						   <FONT class="Mandatory" id="pgcustomaction">* </FONT>
						<% } %>

				</td>
				<td>

				<%=dCur_actiondesc%>
				<%if(actAtt.equals("2")) {%>
					<input type="hidden" name="outaction" value="<%=outactiondesc%>">
				<%}%>
				</td>
				<%} else if(actAtt.equals("0")) {%>

				<input type="hidden" name="outaction" value ="<%=outactiondesc%>" >
				<%}} else {%>
				<td class="tdDefault"><%=LC.L_Action%><%--Action*****--%>
				</td>
				<td><%=dCur_actiondesc%></td>
				<%}%>
			  </tr>

			<tr>
				 <%if (hashPgCustFld.containsKey("recoverydesc")) {
						int fldNumRecover = Integer.parseInt((String)hashPgCustFld.get("recoverydesc"));
						String recMand = ((String)cdoPgField.getPcfMandatory().get(fldNumRecover));
						String recLable = ((String)cdoPgField.getPcfLabel().get(fldNumRecover));
						recAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumRecover));

						if(recAtt == null) recAtt ="";
						if(recMand == null) recMand ="";

						if(!recAtt.equals("0")) {
						if(recLable !=null){
						%> <td>
						<%=recLable%>
						<%} else {%> <td>
						   <%=LC.L_Recovery_Description%><%--Recovery Description*****--%>
						<%}

						if (recMand.equals("1")) {
							%>
						   <FONT class="Mandatory" id="pgcustomrec">* </FONT>
						<% } %>

				</td>
				<td>

				<%=dCur_recoverydesc%>
				<%if(recAtt.equals("2")) {%>
					<input type="hidden" name="adve_recovery" value="<%=recoverydesc%>">
				<%}%>
				</td>
				<%} else if(recAtt.equals("0")) {%>

				<input type="hidden" name="adve_recovery" value ="<%=recoverydesc%>" >
				<%}} else {%>
				<td>
				     <%=LC.L_Recovery_Description%><%--Recovery Description*****--%>
				</td>
				<td>	<%=dCur_recoverydesc%> </td>
				<%}%>
			  </tr>
		<tr>
		<%if (hashPgCustFld.containsKey("outcomenotes")) {

			int fldNumOutcome = Integer.parseInt((String)hashPgCustFld.get("outcomenotes"));
			String outcomeMand = ((String)cdoPgField.getPcfMandatory().get(fldNumOutcome));
			String outcomeLable = ((String)cdoPgField.getPcfLabel().get(fldNumOutcome));
			String outcomeAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumOutcome));

			disableStr ="";
			readOnlyStr ="";
			if(outcomeAtt == null) outcomeAtt ="";
			if(outcomeMand == null) outcomeMand ="";

			if(!outcomeAtt.equals("0")) {
			if(outcomeLable !=null){
			%> <td>
			 <%=outcomeLable%>
			<%} else {%> <td>
			<%=LC.L_Outcome_Notes%><%--Outcome Notes*****--%>
			<%}

			if (outcomeMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomoutcome">* </FONT>
			<% }
		 %>

		  <%if(outcomeAtt.equals("1")) {
			 disableStr = "disabled class='readonly-input'"; }
		     else if (outcomeAtt.equals("2") ) {
			 readOnlyStr = "readonly"; }
		 %>



	  </td>
	  <td>
	   <textarea id="outcomeNotes" name="outcomeNotes" <%=disableStr%> <%=readOnlyStr%> rows="3" cols="60" MAXLENGTH="4000" ><%=outcomeNotes%></textarea>
	   <br><font class="Mandatory"><div id="charlimitinfooutcomeNotes"><%=MC.M_Limit4000_CharLeft %></div></font>
	  </td>
    	<% } else { %>

	  <INPUT type="hidden" name="outcomeNotes" value="<%=outcomeNotes%>">

	  <% }}

	  else {
		 %>
		<td>
		<%=LC.L_Outcome_Notes%><%--Outcome Notes*****--%>
		</td>
		<td><textarea id="outcomeNotes" name="outcomeNotes" rows="3" cols="60" MAXLENGTH="4000" ><%=outcomeNotes%></textarea>
		<br><font class="Mandatory"><div id="charlimitinfooutcomeNotes"><%=MC.M_Limit4000_CharLeft %></div></font> 
		</td>

      <%}%>

	  </tr>


		<%if (hashPgCustFld.containsKey("additionalinfo")) {


			SchCodeDao addInfo= new SchCodeDao();
			addInfo.getCodeValues("adve_info");
			codelstDescs=addInfo.getCDesc();
			codeLstSubTypes = addInfo.getCSubType();
			addInfoIds=addInfo.getCId();
			codelstdesc="";
			length=codelstDescs.size();
			ArrayList advInfoAddIds =null;
			ArrayList addInfoCdLstIds =null;
			ArrayList addInfoVals =null;
			String advInfoAddId =null;
			if(mode.equals("M")){
			EventInfoDao eventInfoDao = new EventInfoDao();
			eventInfoDao = eventdefB.getAdvInfo(StringUtil.stringToNum(adveventId),"A") ;
			advInfoAddIds =eventInfoDao.getAdvInfoIds();
			addInfoCdLstIds = eventInfoDao.getAdvInfoCodelstInfoIds();
			addInfoVals = eventInfoDao.getAdvInfoValues();

			}

			int fldNumaddl = Integer.parseInt((String)hashPgCustFld.get("additionalinfo"));
			String addlMand = ((String)cdoPgField.getPcfMandatory().get(fldNumaddl));
			String addlLable = ((String)cdoPgField.getPcfLabel().get(fldNumaddl));
			String addlAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumaddl));

			disableStr ="";
			readOnlyStr ="";
			if(addlAtt == null) addlAtt ="";
			if(addlMand == null) addlMand ="";

			if(!addlAtt.equals("0")) {
			if(addlLable !=null){
			%> <tr>
			<td COLSPAN=2> <br>
			<p class = "sectionHeadings" >
			 <%=addlLable%>  <% if (addlMand.equals("1")) { %>
			   <FONT class="Mandatory" id="pgcustomaddl">* </FONT>
			<% } %>  <p>
			<%} else {%> <tr> <td COLSPAN=2> <br>
			<p class = "sectionHeadings" > <%=LC.L_Addl_Info%><%--Additional Information*****--%>   <%if (addlMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomaddl">* </FONT>
			<% } %> </p>
			<%} %>


		  <%if(addlAtt.equals("1")) {
			 disableStr = "disabled class='readonly-input'"; }
		     else if (addlAtt.equals("2") ) {
			 readOnlyStr = "disabled"; }
		 %>

	  </td>


	  <td>&nbsp;</td>
	  </tr>

		<input type="hidden" name="addInfoString">
		<% for (int i=0;i<length; i++) {
			codelstdesc=(String)  codelstDescs.get(i);
			addInfoId=(String) addInfoIds.get(i).toString();
			codeLstSubTyp = (String)codeLstSubTypes.get(i);
			strIndex='0';
			advInfoAddId=null;
			status="";
				
			if(mode.equals("M")){
				
				// Checking the codelist Ids aganist Ids stored in database 
				for(int j=0;j<addInfoCdLstIds.size();j++){
					if(addInfoCdLstIds.get(j).toString().equals(addInfoId) ){
						advInfoAddId = advInfoAddIds.get(j).toString();
						if( addInfoVals.get(j).toString().equals("1")){
							status="Checked=true";
							strIndex = '1';
							break;
						}
					}
				}
				// Need to remove orphan Ids
			}
				
				%>


			<tr>
					<td>
					<input type="checkbox"   data-subType="<%=codeLstSubTyp %>" <%=disableStr%>  <%=readOnlyStr%> name="addInfo" <%=status%> > <%=codelstdesc%>
					</td>
					<td>&nbsp;</td>
					</tr>
					<input type="hidden" name=addInfoLen value=<%=length%>>
					<input type="hidden" name=addInfoId value=<%=addInfoId%>>
					<input type="hidden" name=advInfoAddId value=<%=advInfoAddId%>>

				<%}%>

			<td>&nbsp </td><td>&nbsp </td>


    	<% } else {

			%>
		   <input type="hidden" name="addInfoString">
			<%
			for (int i=0;i<length; i++) {
			codelstdesc=(String)  codelstDescs.get(i);
			addInfoId=(String) addInfoIds.get(i).toString();
			codeLstSubTyp = (String)codeLstSubTypes.get(i);
			strIndex='0';
			advInfoAddId = null;
			status="";
							
			if(mode.equals("M")){
				// Checking the codelist Ids aganist Ids stored in database 
				for(int j=0;j<addInfoCdLstIds.size();j++){
					if(addInfoCdLstIds.get(j).toString().equals(addInfoId) ){
						advInfoAddId = advInfoAddIds.get(j).toString();
						if( addInfoVals.get(j).toString().equals("1")){
							status="Checked=true";
							strIndex = '1';
							break;
						}
					}
				}
				// Need to remove orphan Ids
			}

		%>

	  <input type="checkbox" data-subType="<%=codeLstSubTyp %>" Style = "visibility:hidden"  name="addInfo" <%=status%> >

	  <input type="hidden" name=addInfoLen value=<%=length%>>
	  <input type="hidden" name=addInfoId value=<%=addInfoId%>>
	  <input type="hidden" name=advInfoAddId value=<%=advInfoAddId%>>

	  <% }}}

	  else {
		 %>
		<tr>
			<td COLSPAN=2>
				<p class = "sectionHeadings" ><%=LC.L_Addl_Info%><%--Additional Information*****--%></p>
			</td>
		</tr>


				<% SchCodeDao addInfo= new SchCodeDao();
				addInfo.getCodeValues("adve_info");
				codelstDescs=addInfo.getCDesc();
				codeLstSubTypes = addInfo.getCSubType();
				addInfoIds=addInfo.getCId();
				codelstdesc="";
				length=codelstDescs.size();
				ArrayList advInfoAddIds =null;
				
				ArrayList addInfoCdLstIds =null;
				ArrayList addInfoVals =null;
				
				String advInfoAddId =null;
				if(mode.equals("M")){
				EventInfoDao eventInfoDao = new EventInfoDao();
				eventInfoDao = eventdefB.getAdvInfo(StringUtil.stringToNum(adveventId),"A") ;
				advInfoAddIds =eventInfoDao.getAdvInfoIds();
				addInfoCdLstIds = eventInfoDao.getAdvInfoCodelstInfoIds();
				addInfoVals = eventInfoDao.getAdvInfoValues();
				}
				%>
				<input type="hidden" name="addInfoString">
				<% for (int i=0;i<length; i++) {
					codelstdesc=(String)  codelstDescs.get(i);
					addInfoId=(String) addInfoIds.get(i).toString();
					codeLstSubTyp = (String)codeLstSubTypes.get(i);
					strIndex = '0';
					status="";
					advInfoAddId=null;
									
					/* if(mode.equals("M")){
						if (i < advInfoAddIds.size())
						{
							advInfoAddId=(String) advInfoAddIds.get(i).toString();
							strIndex=(null==addInfoString)?0:addInfoString.charAt(i);
						}
						else
						{
							advInfoAddId = "";
							strIndex = '0';
						}

					if(strIndex=='1')
						{status="Checked=true" ;}
					else{status="" ;}
					} */
					
					if(mode.equals("M")){
						// Checking the codelist Ids aganist Ids stored in database 
						for(int j=0;j<addInfoCdLstIds.size();j++){
							if(addInfoCdLstIds.get(j).toString().equals(addInfoId) ){
								advInfoAddId = advInfoAddIds.get(j).toString();
								if( addInfoVals.get(j).toString().equals("1")){
									status="Checked=true";
									strIndex = '1';
									break;
								}
							}
						}
						// Need to remove orphan Ids
					}
					%>
					<tr>
						<td>
							<input type="checkbox" data-subType="<%=codeLstSubTyp %>" name="addInfo" <%=status%> > <%=codelstdesc%>
						</td>
						<td>&nbsp;</td>
					</tr>
					<input type="hidden" name=addInfoLen value=<%=length%>>
					<input type="hidden" name=addInfoId value=<%=addInfoId%>>
					<input type="hidden" name=advInfoAddId value=<%=advInfoAddId%>>
				<%}%>

				<td>&nbsp;</td><td>&nbsp;</td>

      <%}%>


		<%SchCodeDao advNotify= new SchCodeDao();
				String notifyDate = "";
				advNotify.getCodeValues("adve_notify");
				codelstDescs=advNotify.getCDesc();
				codeLstSubTypes = advNotify.getCSubType();
				advNotifyCodeIds=advNotify.getCId();
				codelstdesc="";


				ArrayList advNotifyIds =null;
            	ArrayList advNotifyAdverseIds =null;
            	ArrayList advNotifyCodelstNotTypeIds =null;
            	ArrayList advNotifyDates =null;
            	ArrayList advNotifyNotes =null;
            	ArrayList advNotifyValues =null;


				String advNotifyId =null;
            	String advNotifyAdverseId =null;
            	String advNotifyCodelstNotTypeId =null;
            	String advNotifyDt =null;
            	String advNotifyNote =null;
            	String advNotifyValue =null;

            	length=codelstDescs.size();
    			//Starting to work on notify
            	if(mode.equals("M")){

				EventInfoDao eventInfoDao = new EventInfoDao();
				eventInfoDao = eventdefB.getAdvNotify(StringUtil.stringToNum(adveventId)) ;


            	advNotifyIds =eventInfoDao.getAdvNotifyIds();
            	advNotifyAdverseIds =eventInfoDao.getAdvNotifyAdverseIds();
            	advNotifyCodelstNotTypeIds =eventInfoDao.getAdvNotifyCodelstNotTypeIds();

            	advNotifyDates =eventInfoDao.getAdvNotifyDates();
            	advNotifyNotes =eventInfoDao.getAdvNotifyNotes();
				advNotifyValues =eventInfoDao.getAdvNotifyValues();
				
				// Still needs to iterate through all codeLst options
				//length=advNotifyIds.size();
				}
            	
				%>
				<input type="hidden" name="advNotifyString">


		<%if (hashPgCustFld.containsKey("followingnotified")) {

			int fldNumNotified = Integer.parseInt((String)hashPgCustFld.get("followingnotified"));
			String notifiedMand = ((String)cdoPgField.getPcfMandatory().get(fldNumNotified));
			String notifiedLable = ((String)cdoPgField.getPcfLabel().get(fldNumNotified));
			String notifiedAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumNotified));

			disableStr ="";
			readOnlyStr ="";
			if(notifiedAtt == null) notifiedAtt ="";
			if(notifiedMand == null) notifiedMand ="";

			if(!notifiedAtt.equals("0")) {

			if(notifiedLable !=null) {
			%>
			<tr> <td colspan="2">  <%=notifiedLable%>:    <% if (notifiedMand.equals("1")) {%> <FONT class="Mandatory" id="pgcustomnotified">* </FONT> <%}%>      </td></tr>
			<% } else { %>
			<tr> <td colspan="2"> <%=MC.M_Following_WereNotified%><%--The Following were Notified*****--%>:   <% if (notifiedMand.equals("1")) {%> <FONT class="Mandatory" id="pgcustomnotified">* </FONT> <%}%>   </td></tr>

			<%
			 }
			   if(notifiedAtt.equals("1")) {
					 disableStr = "disabled class='readonly-input'"; }
			   else if (notifiedAtt.equals("2") ) {
					 readOnlyStr = "disabled"; }
			 %>



			<% for (int i=0;i<length; i++) {
/* 
				notifyDate = "notifyDate"+i;

				if(mode.equals("N")){
					codelstdesc=(String)  codelstDescs.get(i);
					advNotifyCodeId=(String) advNotifyCodeIds.get(i).toString();


					status="" ;


				} else {

					codelstdesc=(String)  codelstDescs.get(i);
					advNotifyCodeId=(String) advNotifyCodelstNotTypeIds.get(i).toString();
					advNotifyId=(String) advNotifyIds.get(i).toString();
					advNotifyDate=((advNotifyDates.get(i)) == null)?"":(advNotifyDates.get(i)).toString();
					advNotifyValue = (String)advNotifyValues.get(i);



					if(advNotifyValue.equals("1")){
					status="Checked=true" ;
					}else{
					status="" ;
					}

					} */
					

				notifyDate = "notifyDate"+i;
				advNotifyCodeId=(String) advNotifyCodeIds.get(i).toString();
				codelstdesc=(String)  codelstDescs.get(i);
				codeLstSubTyp = (String)codeLstSubTypes.get(i);
				status="" ;
				status="";
				advNotifyId=null;
				advNotifyDate="";
				
				if(mode.equals("M")){
					// Checking the codelist Ids aganist Ids stored in database 
					for(int j=0;j<advNotifyCodelstNotTypeIds.size();j++){
						if(advNotifyCodelstNotTypeIds.get(j).toString().equals(advNotifyCodeId) ){
							
							advNotifyId = advNotifyIds.get(j).toString();
							advNotifyDate=((advNotifyDates.get(j)) == null)?"":(advNotifyDates.get(j)).toString();
							
							if( advNotifyValues.get(j).toString().equals("1")){
								status="Checked=true";
								break;
							}
						}
					}
					// Need to remove orphan Ids
				}

					

					%>
					<tr>
						<td>
							<input type="checkbox" data-subType="<%=codeLstSubTyp %>" <%=disableStr%>  <%=readOnlyStr%> name="advNotify" <%=status%> > <%=codelstdesc%>
						</td>
						<td class="tdDefault"><%=LC.L_Date%><%--Date*****--%>&nbsp;&nbsp;
						<%-- INF-20084 Datepicker-- AGodara --%>
						<%if (StringUtil.isEmpty(notifiedAtt)) { %>
			   				<INPUT type="text" name="<%=notifyDate%>" class="datefield" id="<%=notifyDate%>" size=10  value="<%=advNotifyDate%>">
			   			<%} else if(notifiedAtt.equals("2")) { %>
							<INPUT type=text name="<%=notifyDate%>" readonly size=10 id="<%=notifyDate%>"  value="<%=advNotifyDate%>" >
						<%} else if(notifiedAtt.equals("1")){ %>
							<INPUT type=text name="<%=notifyDate%>" <%=disableStr%> size=10 id="<%=notifyDate%>"  value="<%=advNotifyDate%>" >
						<%}%>
						
					<input type="hidden" name=advNotifyLen value=<%=length%>>
					<input type="hidden" name=advNotifyCodeId value=<%=advNotifyCodeId%>>
					<input type="hidden" name=advNotifyId value=<%=advNotifyId%>>
					<input type="hidden" name=advNotifyDate value=<%=advNotifyDate%>>
					</td>
						
				</tr>
					
				<%}%>

				<%} else { %>

				<% for (int i=0;i<length; i++) {
/* 
				notifyDate = "notifyDate"+i;

				if(mode.equals("N")){
					codelstdesc=(String)  codelstDescs.get(i);
					advNotifyCodeId=(String) advNotifyCodeIds.get(i).toString();


					status="" ;


				} else {

					codelstdesc=(String)  codelstDescs.get(i);
					advNotifyCodeId=(String) advNotifyCodelstNotTypeIds.get(i).toString();
					advNotifyId=(String) advNotifyIds.get(i).toString();
					advNotifyDate=((advNotifyDates.get(i)) == null)?"":(advNotifyDates.get(i)).toString();
					advNotifyValue = (String)advNotifyValues.get(i);



					if(advNotifyValue.equals("1")){
					status="Checked=true" ;
					}else{
					status="" ;
					}

					} */


					notifyDate = "notifyDate"+i;
					advNotifyCodeId=(String) advNotifyCodeIds.get(i).toString();
					codelstdesc=(String)  codelstDescs.get(i);
					codeLstSubTyp = (String)codeLstSubTypes.get(i);
					status="" ;
					advNotifyId=null;
					advNotifyDate="";
					
					if(mode.equals("M")){
						// Checking the codelist Ids aganist Ids stored in database 
						for(int j=0;j<advNotifyCodelstNotTypeIds.size();j++){
							if(advNotifyCodelstNotTypeIds.get(j).toString().equals(advNotifyCodeId) ){
								
								advNotifyId = advNotifyIds.get(j).toString();
								advNotifyDate=((advNotifyDates.get(j)) == null)?"":(advNotifyDates.get(j)).toString();
								
								if( advNotifyValues.get(j).toString().equals("1")){
									status="Checked=true";
									break;
								}
							}
						}
						// Need to remove orphan Ids
					}

					
					
					%>
					<input type="checkbox" data-subType="<%=codeLstSubTyp %>" name="advNotify" Style = "visibility:hidden" <%=status%> >
					<INPUT type="hidden" name="<%=notifyDate%>" size=10 READONLY value="<%=advNotifyDate%>">

					<input type="hidden" name=advNotifyLen value=<%=length%>>
					<input type="hidden" name=advNotifyCodeId value=<%=advNotifyCodeId%>>
					<input type="hidden" name=advNotifyId value=<%=advNotifyId%>>
					<input type="hidden" name=advNotifyDate value=<%=advNotifyDate%>>
				<%}%>


				<% } } else {%>

				<tr><td colspan="2"><%=MC.M_Following_WereNotified%><%--The Following were Notified*****--%>: <FONT id="pgcustomnotified"></FONT>  </td></tr>

				<% for (int i=0;i<length; i++) {

				notifyDate = "notifyDate"+i;
				advNotifyCodeId=(String) advNotifyCodeIds.get(i).toString();
				codelstdesc=(String)  codelstDescs.get(i);
				codeLstSubTyp = (String)codeLstSubTypes.get(i);
				status="" ;
				status="";
				advNotifyId=null;
				advNotifyDate="";
				
				if(mode.equals("M")){
					/* codelstdesc=(String)  codelstDescs.get(i);
					advNotifyCodeId=(String) advNotifyCodelstNotTypeIds.get(i).toString();
					advNotifyId=(String) advNotifyIds.get(i).toString();
					advNotifyDate=((advNotifyDates.get(i)) == null)?"":(advNotifyDates.get(i)).toString();
					advNotifyValue = (String)advNotifyValues.get(i);
					if(advNotifyValue.equals("1")){
					status="Checked=true" ;
					}else{
					status="" ;
					} */
					// Checking the codelist Ids aganist Ids stored in database 
					for(int j=0;j<advNotifyCodelstNotTypeIds.size();j++){
						if(advNotifyCodelstNotTypeIds.get(j).toString().equals(advNotifyCodeId) ){
							
							advNotifyId = advNotifyIds.get(j).toString();
							advNotifyDate=((advNotifyDates.get(j)) == null)?"":(advNotifyDates.get(j)).toString();
							
							if( advNotifyValues.get(j).toString().equals("1")){
								status="Checked=true";
								break;
							}
						}
					}
					// Need to remove orphan Ids
				}

					%>
					<tr>
						<td>
							<input type="checkbox" data-subType="<%=codeLstSubTyp %>" name="advNotify" <%=status%> > <%=codelstdesc%>
						</td>
						<td class="tdDefault"><%=LC.L_Date%><%--Date*****--%>&nbsp;&nbsp;
						<INPUT type=text class="datefield" name="<%=notifyDate%>" readonly size=10 id="<%=notifyDate%>" value="<%=advNotifyDate%>" ></td>
					</tr>
					<input type="hidden" name=advNotifyLen value=<%=length%>>
					<input type="hidden" name=advNotifyCodeId value=<%=advNotifyCodeId%>>
					<input type="hidden" name=advNotifyId value=<%=advNotifyId%>>
					<input type="hidden" name=advNotifyDate value=<%=advNotifyDate%>>
				<%}%>

				<%}%>
		<tr>
        <%if (hashPgCustFld.containsKey("notes")) {
			int fldNumNotes = Integer.parseInt((String)hashPgCustFld.get("notes"));
			String notesMand = ((String)cdoPgField.getPcfMandatory().get(fldNumNotes));
			String notesLable = ((String)cdoPgField.getPcfLabel().get(fldNumNotes));
			String notesAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumNotes));

			disableStr ="";
			readOnlyStr ="";
			if(notesAtt == null) notesAtt ="";
			if(notesMand == null) notesMand ="";


			if(!notesAtt.equals("0")) {
			if(notesLable !=null){
			%><td class="tdDefault" width="15%"> 
			<%=notesLable%> 
			<%} else {%> <td>
			  <%=LC.L_Notes%><%--Notes*****--%>   <!--KM-->
			<%}

			if (notesMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomnotes">* </FONT>
			<% }
		 %>

		  <%if(notesAtt.equals("1")) {
			 disableStr = "disabled class='readonly-input'"; }
		     else if (notesAtt.equals("2") ) {
			 readOnlyStr = "readonly"; }
		 %>



		</td>
		<td class="tdDefault">
			<TEXTAREA id=notes name=notes rows=3 cols=60 <%=disableStr%>  <%=readOnlyStr%> ><%=notes%></TEXTAREA>
			<br><font class="Mandatory"><div id="charlimitinfonotes"><%=MC.M_Limit4000_CharLeft %></div></font>
		</td>
			<% } else { %>

		 <TEXTAREA id=notes name=notes rows=3 Style = "visibility:hidden" cols=60 ><%=notes%></TEXTAREA>
		 <br><font class="Mandatory"><div id="charlimitinfonotes"><%=MC.M_Limit4000_CharLeft %></div></font>

		 <% }} else {
			 %>
		<td class="tdDefault" width="15%"><%=LC.L_Notes%><%--Notes*****--%> </td>
		<td class="tdDefault">

				<TEXTAREA id=notes name=notes rows=3 cols=60><%=notes%></TEXTAREA>
				<br><font class="Mandatory"><div id="charlimitinfonotes"><%=MC.M_Limit4000_CharLeft %></div></font>
		</td>
		 <%}%>
  	    </tr>
		<% if ("M".equals(mode)){%>
		<tr>
	      <td width="20%"><%=LC.L_ReasonForChangeFDA%>
	      	<%if (fdaStudy == 1){%>&nbsp;<FONT class="Mandatory">* </FONT><%} %>
	      </td>
		  <td width="80%"><textarea id="remarks" name="remarks" rows="3" cols="60" MAXLENGTH="4000"></textarea>
		  <br><font class="Mandatory"><div id="charlimitinforemarks"><%=MC.M_Limit4000_CharLeft %></div></font></td>
	  	</tr>
	  	<%} %>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>



			</table>


				<table width="100%" cellspacing="0" cellpadding="0" bgcolor="#cccccc" class= "basetbl midAlign">
							<%
				//Modified by Manimaran for November Enhancement PS4.
			if ((mode.equals("M") && (pageRight >= 6) && (orgRight >= 6) && (!patStatSubType.equals("lockdown")) ) || (mode.equals("N") && (pageRight == 5  || pageRight == 7 || orgRight == 7) ))
				{
				%>
					<!--tr>
						<td class="tdDefault" width="12%">Form Status</td>
						<td width="13%"><%= dFormStatus %></td-->

				<tr valign="middle">
				 <%if (hashPgCustFld.containsKey("formstatus")) {
						int fldNumFormStat  = Integer.parseInt((String)hashPgCustFld.get("formstatus"));
						String formStatMand = ((String)cdoPgField.getPcfMandatory().get(fldNumFormStat));
						String formStatLable = ((String)cdoPgField.getPcfLabel().get(fldNumFormStat));
						formStatAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumFormStat));

						if(formStatAtt == null) formStatAtt ="";
						if(formStatMand == null) formStatMand ="";

						if(!formStatAtt.equals("0")) {
						if(formStatLable !=null){
						%> <td class="tdDefault" width="12%" >
						<%=formStatLable%>
						<%} else {%> <td class="tdDefault" width="12%" >
						 <%=LC.L_AE_STATUS%><%--Form Status*****--%>
						<%}

					if (formStatMand.equals("1")) {
						%>
					   <FONT class="Mandatory" id="pgcustomformstat">* </FONT>
					<% } %>

				</td>
				<td width="13%">
					<%= dFormStatus %>
					<%if(formStatAtt.equals("2")) {%>
						<input type="hidden" name="formStatus" value="<%=advFormStatus%>">
					<%}%>
				</td>
				<%} else if(formStatAtt.equals("0")) {%>

				<input type="hidden" name="formStatus" value ="<%=advFormStatus%>" >
				<%}} else {%>
				<td class="tdDefault" width="12%"><%=LC.L_AE_STATUS%><%--Form Status*****--%></td>
				<td width="13%"><%= dFormStatus %></td>

				<%}%>
				<%if ("L2_ON".equals(LC.L_Auth2_Switch)){%>
				<td>&nbsp;</td>
		  		<td width="12%"><%=LC.L_UserName%>&nbsp;<FONT class="Mandatory">* </FONT></td>
		  		<td>
					<input type="password" id="userLogName" name="userLogName" value=""
				  	onkeyup="ajaxvalidate('misc:'+this.id,-1,'userLogNameMessage','<%=MC.M_Valid_UserName%>','<%=MC.M_Invalid_UserName %>','sessUserId')"/>
				  	<span id="userLogNameMessage"></span>
		  		</td>
		  		<%}%>
				<input type="hidden" id="fdaStudy" name="fdaStudy" value="<%=fdaStudy%>">
				<input type= "hidden"  id="currentDictSetting" name="currentDictSetting" value="<%=settingValue%>">
			   </tr>
				<%
				}
				else
				{%>
				<tr>
				<td></td>
				<td></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				</tr>
				<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				</tr>
			 <%}	%>
			</table>
 		<DIV class="myLinksmgpat">
			<%
			UsrLinkDao usrLinkDao = ulinkB.getULinkValuesByAccountId(StringUtil.stringToNum(accId),"lnk_adv");
		   	ArrayList lnksIds = usrLinkDao.getLnksIds();
			ArrayList lnksUris = usrLinkDao.getLnksUris();
			ArrayList lnksDescs = usrLinkDao.getLnksDescs();
			ArrayList lnksGrps = usrLinkDao.getLnksGrpNames();
			int len = lnksIds.size();
			int counter = 0;
	   		String lnkUri = "";
			String lnkDesc = "";
   			String oldGrp = "";
	   		String lnkGrp = "";

			%>

			<DIV class="accLinksmgpat">
					<TABLE width="100%" cellspacing="0" cellpadding="3" border="1" border="black" height="150" bgcolor="#000066">
						<tr><th width="80%" align="left" height="20"><%=LC.L_Important_Links%><%--Important Links*****--%></th></tr>
<!--    				        <td width=19 height=20> <img src= "../images/link_upleft.jpg" height=20>
    				        </td>
    				        <td width="130" height=20 >
    				            <P class = "sectionHeadings"> Important Links </P>
    				        </td>
    						<td width=22>&nbsp</td>
    				        <td  width=18 height=20 align="right"> <img src= "../images/link_upright.jpg" width=23 height=20>
    				        </td>
<th colspan="2">Important Links</th>-->
<!--    			        <th width="20%" align="center">&nbsp;</th> -->

    			        <%
    				    for(counter = 0;counter<len;counter++)
    					{
    						lnkUri=((lnksUris.get(counter))==null)?"-":(lnksUris.get(counter)).toString();
    						lnkDesc=((lnksDescs.get(counter))==null)?"-":(lnksDescs.get(counter)).toString();
    						lnkGrp = ((lnksGrps.get(counter))==null)?"-":(lnksGrps.get(counter)).toString();
    						%>
    				        <% if ( ! lnkGrp.equals(oldGrp)){ %>
									<% if (counter != 0) {%>
										<tr><td colspan="2">&nbsp;</td></tr>
									<% } %>
								<tr > <td colspan="2">  <%= lnkGrp %>  </td></tr>
							<%}else{%>
<!--						    <td colspan="2">&nbsp;</td></tr> -->
    						<%}%>
    				        	<tr>
									<td colspan ="2"> <A href="<%= lnkUri%>" target="Information" onClick="advEventNewFunctions.openWin()" ><%= lnkDesc%></A>   </td>
	    				        </tr>
    				        <%
    						oldGrp = lnkGrp;
    			 		}
				if (counter==0){
				%>
					<tr><td colspan="2">&nbsp;</td></tr>

				<%}%>
					</table>
		</div>
		</div>
		<div id='progressDialog' style="display:none;" ><br><p class="sectionHeadings" align="center"> <%=LC.L_Saving_Data %> </p></div>
		
	<%	} 	//end of if session times out%>
