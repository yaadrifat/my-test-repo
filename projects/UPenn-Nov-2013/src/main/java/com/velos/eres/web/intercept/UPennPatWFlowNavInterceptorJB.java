package com.velos.eres.web.intercept;

import javax.servlet.http.HttpSession;

import com.velos.eres.gems.business.WorkflowInstance;
import com.velos.eres.gems.business.WorkflowInstanceJB;
import com.velos.eres.service.util.CFG;
import com.velos.eres.web.patProt.PatProtJB;
import com.velos.esch.service.util.StringUtil;

public class UPennPatWFlowNavInterceptorJB extends InterceptorJB {
	public void handle(Object...args){
		if (null == args || null == args[0]) return;
		HttpSession tSession = (HttpSession) args[0];
		
		String callingPage = (String) args[1];
		
		String DUMMY_WFSWITCH ="PatEnrollWf_ON";
		String DUMMY_WorkflowType ="[Workflow_PatEnroll_WfType]";

		String personPK = (String)tSession.getAttribute("personPK");
		String patientCode = (String)tSession.getAttribute("patientCode");
		String studyId = (String)tSession.getAttribute("studyId");
		
		PatProtJB patEnrollB =  new PatProtJB();
		patEnrollB.findCurrentPatProtDetails(StringUtil.stringToNum(studyId),StringUtil.stringToNum(personPK));
		int patProtPk = patEnrollB.getPatProtId();

		if ("Y".equals(CFG.Workflows_Enabled)) {
			if (DUMMY_WFSWITCH.equals(CFG.Workflow_PatEnroll_WfSwitch) && !DUMMY_WorkflowType.equals(CFG.Workflow_PatEnroll_WfType)){			
				WorkflowInstanceJB workflowInstanceJB = new WorkflowInstanceJB();
				WorkflowInstance workflowInstance = new WorkflowInstance();
	
				workflowInstance = workflowInstanceJB.getWorkflowInstanceStatus(""+patProtPk, CFG.Workflow_PatEnroll_WfType);
				
				Integer workflowInstanceId = 0;
				workflowInstanceId = workflowInstance.getPkWfInstanceId();

				if (workflowInstanceId == null || workflowInstanceId == 0) {
					return;
				} else {
					Integer workflowInstanceStatus =  workflowInstance.getWfIStatusFlag();
					workflowInstanceStatus = (null == workflowInstanceStatus)? 0 : workflowInstanceStatus;
					
					if (workflowInstanceStatus == 0){
						//Navigate to next page in workflow
						if ("patientDemogScreen".equals(callingPage)){
							if (patProtPk > 0 && !StringUtil.isEmpty(studyId)) {
								args[1] = "enrollpatient.jsp?srcmenu=tdMenuBarItem5&selectedTab=2&mode=M&page=patientEnroll&studyId="+studyId+"&studyVer=null"
										+"&patientCode="+patientCode+"&pkey="+personPK +"&patProtId="+patProtPk;
							} else {
								args[1] = "";
							}
						} else if ("patStudyStatusScreen".equals(callingPage)){
							if (patProtPk > 0 && !StringUtil.isEmpty(studyId)) {
								args[1] = "formfilledstdpatbrowser.jsp?mode=M&srcmenu=tdmenubaritem5&selectedTab=8&calledFrom=S"
										+ "&pkey="+personPK+"&patProtId="+patProtPk+"&studyId="+studyId+"&patientCode="+patientCode;
							} else {
								args[1] = "";
							}
						}
					} else {
						//Navigate to next "out-of-the-box product" page
						if ("patientDemogScreen".equals(callingPage)){
							args[1] = "patientDemogScreen.jsp?srcmenu=tdmenubaritem5&selectedTab=1&mode=M&includeMode=N&page=patient&patientCode="+patientCode+"&pkey="+personPK;
						} else if ("patStudyStatusScreen".equals(callingPage)){
							args[1] = "enrollpatient.jsp?srcmenu=tdMenuBarItem5&selectedTab=2&mode=M&page=patientEnroll&studyId="+studyId+"&studyVer=null"
									+"&patientCode="+patientCode+"&pkey="+personPK +"&patProtId="+patProtPk;
						}
					}
				}
			}
		}
	}
}
