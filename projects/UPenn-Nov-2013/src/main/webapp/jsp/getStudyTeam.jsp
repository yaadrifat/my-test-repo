<%@page import="com.velos.eres.web.moreDetails.MoreDetailsJB"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.json.*"%>
<%@ page language = "java" import = "com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.esch.business.common.*,java.util.*,com.velos.eres.business.study.impl.*"%>
<jsp:useBean id ="sessionmaint2" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%
	HttpSession tSession2 = request.getSession(true);
	request.setCharacterEncoding("UTF-8");
	response.setContentType("application/json");
	JSONObject jsObj = new JSONObject();
	if (!sessionmaint2.isValidSession(tSession2)) {
		// Session is invalid; print an error and exit
		response.sendError(HttpServletResponse.SC_FORBIDDEN, "SESSION_TIME_OUT");
		jsObj.put("result", -1);
		jsObj.put("resultMsg", MC.M_UsrNot_LoggedIn);/*jsObj.put("resultMsg", "User is not logged in.");*****/
		out.println(jsObj.toString());
		return;
	}

   	if (sessionmaint2.isValidSession(tSession2)){
		int studyId = StringUtil.stringToNum((String) request.getParameter("studyId"));
	    int userId = StringUtil.stringToNum((String) tSession2.getAttribute("userId"));
	    int accId = StringUtil.stringToNum((String) tSession2.getAttribute("accountId"));
	    String defUserGroup = (String) tSession2.getAttribute("defUserGroup");

		String roleSubTypeList = (String) request.getParameter("role_subType");
		
		CodeDao cDao = new CodeDao();
		String[] roleSubTypes = roleSubTypeList.split(",");

		int codeId = 0;
		CodeDao cdTeamRoles = new CodeDao();
		if (roleSubTypes.length == 1){
			codeId = cdTeamRoles.getCodeId("role", roleSubTypeList);
			String codeDesc = (codeId > 0)? cDao.getCodeDescription() : "";
			
			jsObj.put("teamRoleCodeId", codeId);
	        jsObj.put("teamRoleCodeDesc",codeDesc);
		}

		TeamDao stDao = new TeamDao();
		stDao.getUserByStudyTeamRole(studyId, roleSubTypeList);
		ArrayList arrUserIds = stDao.getUserIds();
		jsObj.put("arrUserIds", arrUserIds);

		ArrayList arrUserMoreDetails = new ArrayList();
		MoreDetailsDao userDetailsDao = new MoreDetailsDao();
		JSONObject jsObjMDetails = null;

		for (int indx = 0; indx < arrUserIds.size(); indx++){
			int teamUserId = ((Integer)arrUserIds.get(indx)).intValue();
			
			userDetailsDao = new MoreDetailsDao();
			userDetailsDao.getMoreDetails(teamUserId, "user", defUserGroup);

			jsObjMDetails = new JSONObject();
			jsObjMDetails.put("userMDSubTypes", userDetailsDao.getMdElementKeys());
			jsObjMDetails.put("userMDData", userDetailsDao.getMdElementValues());

			arrUserMoreDetails.add(jsObjMDetails);
		}
		jsObj.put("arrUserMoreDetails", arrUserMoreDetails);

		ArrayList arrFirstNames = stDao.getTeamUserFirstNames();
		ArrayList arrLastNames = stDao.getTeamUserLastNames();
		ArrayList arrTeamRoles = stDao.getTeamRoles();

		jsObj.put("arrFirstNames", arrFirstNames);
        jsObj.put("arrLastNames",arrLastNames);
        jsObj.put("arrRoles",arrTeamRoles);

        ArrayList arrUserPhones = stDao.getUserPhone();
        ArrayList arrUserEmails = stDao.getUserEmail();

        jsObj.put("arrUserPhones",arrUserPhones);
        jsObj.put("arrUserEmails",arrUserEmails);
        
		out.println(jsObj.toString());
   	} %>
