<%@page import="com.velos.eres.business.study.impl.StudyBean"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.json.*"%>
<%@ page language = "java" import = "com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.esch.business.common.*,java.util.*"%>
<jsp:useBean id ="sessionmaint2" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="studyStatB" scope="request" class="com.velos.eres.web.studyStatus.StudyStatusJB"/>
<jsp:useBean id="studyRightsB" scope="request" class="com.velos.eres.web.studyRights.StudyRightsJB"/>
<%
	HttpSession tSession2 = request.getSession(true);
	request.setCharacterEncoding("UTF-8");
	response.setContentType("application/json");
	JSONObject jsObj = new JSONObject();
	if (!sessionmaint2.isValidSession(tSession2)) {
		// Session is invalid; print an error and exit
		response.sendError(HttpServletResponse.SC_FORBIDDEN, "SESSION_TIME_OUT");
		jsObj.put("result", -1);
		jsObj.put("resultMsg", MC.M_UsrNot_LoggedIn);/*jsObj.put("resultMsg", "User is not logged in.");*****/
		out.println(jsObj.toString());
		return;
	}

   	if (sessionmaint2.isValidSession(tSession2)){
		int studyId = StringUtil.stringToNum((String) request.getParameter("studyId"));
		int userId = StringUtil.stringToNum((String) tSession2.getValue("userId"));
		int accId = StringUtil.stringToNum((String) tSession2.getValue("accountId"));
		
		if (!StringUtil.isAccessibleFor(studyRightsB.getStudyRightsForModule(studyId, userId, "STUDYPTRACK"), 'V')){
			response.sendError(HttpServletResponse.SC_BAD_REQUEST, MC.M_InsuffAccRgt_CnctSysAdmin);
			jsObj.put("result", -2);
			jsObj.put("resultMsg", MC.M_InsuffAccRgt_CnctSysAdmin);
			out.println(jsObj.toString());
			return;
		}

		int allOrg=0;
		StudyStatusDao currStudyStatDao = studyStatB.getStudyStatusDesc(studyId, allOrg, userId, accId);
		ArrayList currentStats =  currStudyStatDao.getCurrentStats();
		ArrayList currStatusIdLst = currStudyStatDao.getIds();
		ArrayList currStatusLst = currStudyStatDao.getDescStudyStats();
		ArrayList currStatStartDateLst = currStudyStatDao.getStatStartDates();

		String currentStatId="";
		String currentVal="";
		String currentStatus ="";
		String currentStatusStartDate = "";
				
				
		int indx = currentStats.indexOf("1");
		
		for (int i=0;i<currentStats.size();i++) {
			currentVal = (currentStats.get(i) == null)?"":(currentStats.get(i)).toString();
			if ( currentVal.equals("1")) {
				currentStatId = (String)currStatusIdLst.get(i).toString();
				currentStatus=((currStatusLst.get(i)) == null)?"-":(currStatusLst.get(i)).toString();
				currentStatusStartDate=((currStatStartDateLst.get(i)) == null)?"-":(currStatStartDateLst.get(i)).toString();
				currentStatusStartDate = DateUtil.format2DateFormat(currentStatusStartDate);
				currentStatusStartDate = "[" + currentStatusStartDate.substring(0, currentStatusStartDate.indexOf(" ")) + "]";
				//currentStatusStartDate = currentStatusStartDate.trim();
				break;
			}
		}

		jsObj.put("currentStudyStatId", currentStatId);
		jsObj.put("currentStudyStatus", currentStatus);
		jsObj.put("currentStudyStatStartDate", currentStatusStartDate);

		out.println(jsObj.toString());

	} %>