var patientDemogScreenJS = {
	applySecurity: {},
	applySecurityToField: {},
	validate: {}
};

patientDemogScreenJS.validate = function(){
	return true;
};

patientDemogScreenJS.applySecurityToField = function (fieldId){
	var fld = document.getElementById(fieldId);
	if (fld){
		switch (fieldId){
		case 'patid':
			if (!patientDemogScreenFunctions.patientScrnIsAdmin){
				fld.readOnly = true;
				fld.setAttribute("class", "readonly-input");
			}
			break;
		default :
			break;
		}
	}
};
patientDemogScreenJS.applySecurity = function (){
	var securityType = patientDetailsFunctions.entryMode;

	if (securityType == 'M'){
		//Patient ID
		patientDemogScreenJS.applySecurityToField('patid');
	}
};

$j(document).ready(function (){
	patientDemogScreenJS.applySecurity();
});