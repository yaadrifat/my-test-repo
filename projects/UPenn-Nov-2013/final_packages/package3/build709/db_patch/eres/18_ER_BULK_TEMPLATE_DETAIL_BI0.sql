CREATE  OR REPLACE TRIGGER ER_BULK_TEMPLATE_DETAIL_BI0 BEFORE INSERT ON ER_BULK_TEMPLATE_DETAIL        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
usr := 'New User' ;
 
 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'ER_BULK_TEMPLATE_DETAIL',erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'CREATED_ON',TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'CREATOR', usr );
       Audit_Trail.column_insert (raid, 'FILE_FIELD_COLNUM', :NEW.FILE_FIELD_COLNUM);
       Audit_Trail.column_insert (raid, 'FILE_FIELD_NAME', :NEW.FILE_FIELD_NAME);
       Audit_Trail.column_insert (raid, 'FK_BULK_ENTITY_DETAIL', :NEW.FK_BULK_ENTITY_DETAIL);
       Audit_Trail.column_insert (raid, 'FK_BULK_TEMPLATE_MASTER', :NEW.FK_BULK_TEMPLATE_MASTER);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE',TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'PK_BULK_TEMPLATE_DETAIL', :NEW.PK_BULK_TEMPLATE_DETAIL);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
COMMIT;
END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,308,18,'18_ER_BULK_TEMPLATE_DETAIL_BI0.sql',sysdate,'v9.3.0 #709');

commit;

