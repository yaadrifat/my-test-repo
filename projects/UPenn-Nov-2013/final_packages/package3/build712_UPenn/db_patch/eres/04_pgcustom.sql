set define off;

declare
v_exists NUMBER := 0;
v_fk_account NUMBER := [fk_account];
v_pk_pagecustom NUMBER := 0;
begin	
	select count(pk_pagecustom) into v_exists from er_pagecustom where PAGECUSTOM_PAGE = 'studysummary' and fk_account = v_fk_account;
	if (v_exists > 0) then
		select pk_pagecustom into v_pk_pagecustom from er_pagecustom where PAGECUSTOM_PAGE = 'studysummary' and fk_account = v_fk_account;
	
		INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
		VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_pk_pagecustom, 'pinvestigator', null, 1, null);
		commit;
	end if;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,311,4,'04_pgcustom.sql',sysdate,'v9.3.0 #712');

commit;

