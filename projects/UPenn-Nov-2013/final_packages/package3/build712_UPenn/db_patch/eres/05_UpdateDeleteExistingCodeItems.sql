set define off;

UPDATE ER_CODELST SET CODELST_DESC='Alaskan Native' WHERE CODELST_TYPE = 'race' AND CODELST_SUBTYP='race_AlaskNativ';

COMMIT;

declare		
v_CodeExists NUMBER := 0;
begin

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'race' AND CODELST_SUBTYP = 'race_AsiaPac';	
	IF (v_CodeExists = 0) THEN INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'race', 'race_AsiaPac', 'Asian or Pacific Islander');
	END IF;	
	UPDATE ER_CODELST SET CODELST_CUSTOM_COL1='race_prim', CODELST_HIDE='Y' WHERE CODELST_TYPE = 'race' AND CODELST_SUBTYP='race_AsiaPac';
	COMMIT;
	
end;
/


declare		
v_AsianCodeExists NUMBER := 0;
v_AsiaPacCodeExists NUMBER := 0;
begin

	SELECT PK_CODELST INTO v_AsianCodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'race' AND CODELST_SUBTYP = 'race_asian';
	
	SELECT PK_CODELST INTO v_AsiaPacCodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'race' AND CODELST_SUBTYP = 'race_AsiaPac';	
	
	if (v_AsianCodeExists > 0 AND v_AsiaPacCodeExists > 0) then
		update epat.person set fk_codelst_race = v_AsiaPacCodeExists where fk_codelst_race = v_AsianCodeExists;
		commit;
		
		update epat.person set person_add_race = v_AsiaPacCodeExists where person_add_race like v_AsianCodeExists;
		commit;
		
		update epat.person set person_add_race = replace(person_add_race, v_AsianCodeExists||',', v_AsiaPacCodeExists||',') where person_add_race like v_AsianCodeExists||',%';
		commit;
	end if;
end;
/

UPDATE ER_CODELST SET CODELST_DESC='Asian' WHERE CODELST_TYPE = 'race' AND CODELST_SUBTYP='race_asian';

COMMIT;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,311,5,'05_UpdateDeleteExistingCodeItems.sql',sysdate,'v9.3.0 #712');

commit;