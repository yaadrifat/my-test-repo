/////*********This readMe is a consolidated readMe for builds v9.3.0 build #694 - #697**********////

IMP: Please follow the instructions mentioned in the docuement named "Velos eResearch Study Screen Deployment Instructions (UPenn)" in the doc folder.

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Please follow these instructions for following 4 JARs- 
jmimemagic-0.1.0.jar
mime-util-2.1.1.jar
opencsv-2.3.jar
xstream-1.4.4.jar

1. Remove the jar files from: 
	server\eresearch\deploy\velos.ear\velos.war\WEB-INF\lib
	
2. Files are provided in encloded "BulkUploadJars" folder.

3. Place the newly provided JARs under:
	server\eresearch\lib

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/////*********This readMe is for build697_UPenn **********////
1. Please remove all the configurations from lableBundle_custom.properties file for the following key. This configuration is no more supported.
Config_ProgMembershipLkpId

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
