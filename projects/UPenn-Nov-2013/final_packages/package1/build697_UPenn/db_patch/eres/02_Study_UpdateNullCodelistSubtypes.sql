set define off;

update er_codelst set codelst_subtyp = 'phaseIII' where codelst_type = 'phase' and (codelst_desc = 'Phase III' or codelst_desc = 'III');
update er_codelst set codelst_subtyp = 'phaseI/II' where codelst_type = 'phase' and (codelst_desc = 'Phase I/II' or codelst_desc like '%I/II');
update er_codelst set codelst_subtyp = 'phaseIV' where codelst_type = 'phase' and (codelst_desc = 'Phase IV' or codelst_desc = 'IV');
update er_codelst set codelst_subtyp = 'phaseIV/V' where codelst_type = 'phase' and (codelst_desc = 'Phase IV/V' or codelst_desc like '%IV/V');
update er_codelst set codelst_subtyp = 'phaseII' where codelst_type = 'phase' and (codelst_desc = 'Phase II' or codelst_desc = 'II');
update er_codelst set codelst_subtyp = 'phaseII/III' where codelst_type = 'phase' and (codelst_desc = 'Phase II/III' or codelst_desc like '%II/III');
update er_codelst set codelst_subtyp = 'phaseI' where codelst_type = 'phase' and (codelst_desc = 'Phase I' or codelst_desc = 'I');
commit;

update er_codelst set codelst_subtyp = 'HIV_Related' where codelst_type = 'tarea' and codelst_desc = 'HIV-related';
update er_codelst set codelst_subtyp = 'neurologic' where codelst_type = 'tarea' and codelst_desc = 'Neurologic';
update er_codelst set codelst_subtyp = 'analDysplasia' where codelst_type = 'tarea' and codelst_desc = 'Anal Dysplasia';
update er_codelst set codelst_subtyp = 'gastrointestinl' where codelst_type = 'tarea' and codelst_desc = 'Gastrointestinal';
update er_codelst set codelst_subtyp = 'urology' where codelst_type = 'tarea' and (codelst_desc = 'Urology' or codelst_desc = 'Urologic');
update er_codelst set codelst_subtyp = 'thracic' where codelst_type = 'tarea' and (codelst_desc = 'Thoracic (Lung)' or codelst_desc = 'Thoracic Oncology');
commit;

update er_codelst set codelst_subtyp = 'bgABPositive', codelst_desc = 'AB+' where codelst_type = 'bloodgr' and (codelst_desc = 'AB+' or codelst_desc = 'AB');
update er_codelst set codelst_subtyp = 'bgABNegative' where codelst_type = 'bloodgr' and codelst_desc = 'AB-';
update er_codelst set codelst_subtyp = 'bgONegative' where codelst_type = 'bloodgr' and codelst_desc = 'O-';
commit;

update er_codelst set codelst_subtyp = 'bindingDouble' where codelst_type = 'blinding' and codelst_desc = 'Double';
commit;

update er_codelst set codelst_subtyp = 'nonRandomized' where codelst_type = 'randomization' and codelst_desc = 'Non - Randomized';
update er_codelst set codelst_subtyp = 'noneRandomized' where codelst_type = 'randomization' and codelst_desc = 'None';
commit;

--update er_codelst set codelst_subtyp = 'graduate' where codelst_type = 'education' and codelst_desc = 'Graduate';
--update er_codelst set codelst_subtyp = 'postGraduate' where codelst_type = 'education' and codelst_desc = 'Post Graduate';
--commit;

update er_codelst set codelst_subtyp = 'outcomeSuspend' where codelst_type = 'studystat_out' and codelst_desc = 'Suspended';
update er_codelst set codelst_subtyp = 'outcomeApproved' where codelst_type = 'studystat_out' and codelst_desc = 'Approved';
update er_codelst set codelst_subtyp = 'outcomeDeclined' where codelst_type = 'studystat_out' and codelst_desc = 'Declined';
commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,296,2,'02_Study_UpdateNullCodelistSubtypes.sql',sysdate,'v9.3.0 #697');

commit;