--Hot Fix #1
set define off;

declare
v_PK_PAGECUSTOM NUMBER := 0;
v_FK_ACCOUNT NUMBER := [fk_account];
begin
	SELECT PK_PAGECUSTOM INTO v_PK_PAGECUSTOM FROM ER_PAGECUSTOM WHERE FK_ACCOUNT = v_FK_ACCOUNT AND PAGECUSTOM_PAGE = 'studysummary';
	
	UPDATE ER_PAGECUSTOMFLDS SET PAGECUSTOMFLDS_ATTRIBUTE = '1', PAGECUSTOMFLDS_LABEL = 'Sponsor Name' WHERE FK_PAGECUSTOM = v_PK_PAGECUSTOM AND PAGECUSTOMFLDS_FIELD = 'ifothersponsor';
	
	UPDATE ER_PAGECUSTOMFLDS SET PAGECUSTOMFLDS_ATTRIBUTE = '0', PAGECUSTOMFLDS_LABEL = NULL WHERE FK_PAGECUSTOM = v_PK_PAGECUSTOM AND PAGECUSTOMFLDS_FIELD = 'sponsorlookup';
	
	COMMIT;
END;
/

Update er_codelst set codelst_custom_col ='nonIndustrial', CODELST_SUBTYP ='other', CODELST_DESC ='Externally Peer-Reviewed' where 
CODELST_TYPE= 'research_type' AND CODELST_DESC like 'Other Externally Peer Reviewed';

UPDATE ER_CODELST SET CODELST_DESC='Externally Peer-Reviewed' WHERE CODELST_TYPE = 'research_type' AND CODELST_SUBTYP='other';

COMMIT;

declare
v_CodeExists NUMBER := 0;
begin
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'INDIDEGrantor' AND CODELST_SUBTYP = 'notRecorded';	
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'INDIDEGrantor', 'notRecorded', 'Not Recorded');
	END IF;
	
	UPDATE ER_CODELST SET  CODELST_CUSTOM_COL='IND,IDE' WHERE CODELST_TYPE = 'INDIDEGrantor' AND CODELST_SUBTYP='notRecorded';
	
	commit;
end;
/

UPDATE ER_CODELST SET CODELST_HIDE='Y' WHERE CODELST_TYPE = 'INDIDEGrantor' AND CODELST_SUBTYP='CDRH';
UPDATE ER_CODELST SET CODELST_HIDE='Y' WHERE CODELST_TYPE = 'INDIDEGrantor' AND CODELST_SUBTYP='CDER';
UPDATE ER_CODELST SET CODELST_HIDE='Y' WHERE CODELST_TYPE = 'INDIDEGrantor' AND CODELST_SUBTYP='CBER';
UPDATE ER_CODELST SET CODELST_HIDE='Y' WHERE CODELST_TYPE = 'INDIDEHolder' AND CODELST_SUBTYP='investigator';
UPDATE ER_CODELST SET CODELST_HIDE='Y' WHERE CODELST_TYPE = 'INDIDEHolder' AND CODELST_SUBTYP='organization';
UPDATE ER_CODELST SET CODELST_HIDE='Y' WHERE CODELST_TYPE = 'INDIDEHolder' AND CODELST_SUBTYP='industry';
UPDATE ER_CODELST SET CODELST_HIDE='Y' WHERE CODELST_TYPE = 'INDIDEHolder' AND CODELST_SUBTYP='NIH';
UPDATE ER_CODELST SET CODELST_HIDE='Y' WHERE CODELST_TYPE = 'INDIDEHolder' AND CODELST_SUBTYP='NCI';
UPDATE ER_CODELST SET CODELST_HIDE='Y' WHERE CODELST_TYPE = 'INDIDEHolder' AND CODELST_SUBTYP='PI';

commit;

declare
v_CodeExists NUMBER := 0;
begin
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'INDIDEHolder' AND CODELST_SUBTYP = 'notRecorded';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'INDIDEHolder', 'notRecorded', 'Not Recorded');
	END IF;

	COMMIT;
end;
/

-- Hot Fix #2
UPDATE ER_PAGECUSTOMFLDS SET PAGECUSTOMFLDS_ATTRIBUTE = '0', PAGECUSTOMFLDS_LABEL = NULL, PAGECUSTOMFLDS_FIELD = 'sponsorlookup' WHERE PAGECUSTOMFLDS_FIELD = 'Sponsorlookup';

COMMIT;

UPDATE ER_CODELST SET CODELST_CUSTOM_COL = 'dropdown' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP ='PR_COOPGROUP_DD';
UPDATE ER_CODELST SET CODELST_CUSTOM_COL = 'dropdown' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP ='PR_CONSORTIUM';

COMMIT;

UPDATE ER_CODELST SET 
CODELST_CUSTOM_COL1='[{data:"CACTIS", display:"CACTIS"},{data:"CAMRIS", display:"CAMRIS"},{data:"CISC", display:"CISC"},{data:"DOD", display:"DOD"},{data:"FDA", display:"FDA"},{data:"IBC", display:"IBC"},{data:"IBS", display:"IBS"},{data:"IDS", display:"IDS"},{data:"IRB", display:"IRB"},{data:"PERIOP", display:"PERIOP"},{data:"PET", display:"PET"},{data:"RDC", display:"RDC"},{data:"SPONSOR", display:"SPONSOR"},{data:"TCRC", display:"TCRC"},{data:"NATIONL", display:"NATIONAL#"}]' 
WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='OTH_REVIEW_GRP';

COMMIT;

declare
v_CodeExists NUMBER := 0;
begin
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'role' AND CODELST_SUBTYP = 'role_contact1';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'role', 'role_contact1', 'Contact 1');
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'role' AND CODELST_SUBTYP = 'role_contact2';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'role', 'role_contact2', 'Contact 2');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'role' AND CODELST_SUBTYP = 'role_contact3';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'role', 'role_contact3', 'Contact 3');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'role' AND CODELST_SUBTYP = 'role_contact4';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'role', 'role_contact4', 'Contact 4');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'role' AND CODELST_SUBTYP = 'role_contact5';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'role', 'role_contact5', 'Contact 5');
	END IF;
end;
/

declare
v_roleExisits NUMBER := 0;
v_roleCoordPK NUMBER := 0;
v_roleDmPK NUMBER := 0;
begin
	select pk_codelst INTO v_roleCoordPK from er_codelst where codelst_subtyp = 'role_coord';
	if (v_roleCoordPK > 0)  then
		select count(*) into v_roleExisits from er_studyteam where FK_CODELST_TMROLE = v_roleCoordPK;
	
		if (v_roleExisits > 0) then
			select pk_codelst INTO v_roleDmPK from er_codelst where codelst_subtyp = 'role_dm';

			if (v_roleDmPK > 0) then 
				update er_studyteam set FK_CODELST_TMROLE = v_roleDmPK WHERE FK_CODELST_TMROLE = v_roleCoordPK;
				commit;
			end if;
		end if;
	end if;
end;
/

UPDATE ER_CODELST SET  CODELST_DESC='Data Manager', CODELST_HIDE= 'Y' WHERE CODELST_TYPE = 'role' AND CODELST_SUBTYP='role_coord';

commit;

--Hot Fix #3
declare
v_PK_PAGECUSTOM NUMBER := 0;
v_FK_ACCOUNT NUMBER := [fk_account];
begin
	SELECT PK_PAGECUSTOM INTO v_PK_PAGECUSTOM FROM ER_PAGECUSTOM WHERE FK_ACCOUNT = v_FK_ACCOUNT AND PAGECUSTOM_PAGE = 'studysummary';
	
	UPDATE ER_PAGECUSTOMFLDS SET PAGECUSTOMFLDS_ATTRIBUTE = '2', PAGECUSTOMFLDS_LABEL = 'Sponsor Name' WHERE FK_PAGECUSTOM = v_PK_PAGECUSTOM AND PAGECUSTOMFLDS_FIELD = 'ifothersponsor';

	COMMIT;
END;
/

update er_codelst set codelst_custom_col = 'checkbox' where codelst_type = 'studyidtype' and codelst_custom_col = 'chkbox';

commit;

--Hot Fix #4
--No db changes

--Hot Fix #5
--No db changes

--Hot Fix #6
--No db changes

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,296,10,'10_Study_Misc_Patch1_6.sql',sysdate,'v9.3.0 #697');

commit;