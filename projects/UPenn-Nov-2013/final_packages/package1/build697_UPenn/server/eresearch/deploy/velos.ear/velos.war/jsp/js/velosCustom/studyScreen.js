var studyScreenJS = {
	piProgMembershipLkpId: 0,
	sponsorLkpId: 0,
	sponsorLkpKeywordStr: '', //'studySponsor|[Configure custom column keyword Here]',

	validate: {},
	customValidateMoreStudyDetails: {},
	applySecurityToField: {},
	applySecurity: {},
	openLookup: {},

	/*Functions for creating UI elements*/
	createUIElements: {},
	addStudyStatusSection: {},
	addPersonnelSection: {},
	/**/
	
	/*Function to add listeners*/
	addListeners: {},
	/**/

	/*Load data*/
	loadStudyStatus: {},
	loadStudyTeam: {},
	/**/

	/*Function for inter-field actions*/
	runInterFieldActions: {},
	runAllInterFieldActions: {},
	OTH_REVIEW_GRP_checkBoxChange: {},
	OTH_REVIEW_GRP_checkCheckBox: {},
	chkCTSRMCReviewd_checkBoxChange: {},
	/**/

	requestUPCC: {},
	registerCoop: {},
	
	onClickStudyPILink: {},
	onClickSponsorLink: {},
	onClickUpdateStudyStatusLink: {},
	onClickUpdateStudyTeamLink: {}
};

studyScreenJS.validate = function(){
	if(!studyScreenJS.customValidateMoreStudyDetails()){
		return false;
	}
	
	return true;
};

studyScreenJS.customValidateMoreStudyDetails = function(){
	var studyResType = document.getElementById('studyResType');
	var studyResTypeValue = studyResType.options[studyResType.selectedIndex].innerHTML;
	if (studyResTypeValue == 'Industrial'){
		if (!(validate_col("National Protocol Number", moreStudyDetFunctions.getMoreStudyDetField("PR_NATIONL_NUM"))))
			return false;
	}
	if (studyResTypeValue == 'National'){
		if (!(validate_col("National", moreStudyDetFunctions.getMoreStudyDetField("PR_COOPGROUP_DD"))))
			return false;
	}

	var PENN_GOAL_LOW = moreStudyDetFunctions.getMoreStudyDetField("PENN_GOAL_LOW");
	if (PENN_GOAL_LOW){
		if (!(validate_col("Center Accrual Goal - Low", PENN_GOAL_LOW)))
			return false;
		
		if (isInteger(PENN_GOAL_LOW.value) == false) {
			alert("Please enter a valid Number");  
			PENN_GOAL_LOW.focus();
			return false;
		}
	}

	var PENN_GOAL_HIGH = moreStudyDetFunctions.getMoreStudyDetField("PENN_GOAL_HIGH");
	if (PENN_GOAL_HIGH){
		
		if (isInteger(PENN_GOAL_HIGH.value) == false) {
			alert("Please enter a valid Number");  
			PENN_GOAL_HIGH.focus();
			return false;
		}
	}

	if (!(validate_col("Specific Funding Source", moreStudyDetFunctions.getMoreStudyDetField("PR_GRANTEE"))))
		return false;

	return true;
};


studyScreenJS.applySecurityToField = function (fieldId){
	var fld = document.getElementById(fieldId);
	if (fld){
		switch (fieldId){
		case 'upccButton':
		case 'chkCTSRMCReviewd':
			if (!studyScreenFunctions.studyScrnIsAdmin){
				fld.disabled = true;
			}
			break;
		case 'nctNumber':
		case 'studyNumber':
		case 'studyTitle':
			if (!studyScreenFunctions.studyScrnIsAdmin){
				fld.readOnly = true;
				fld.setAttribute("class", "readonly-input");
			}
			break;
		default :
			var PR_STUDY_RISK = moreStudyDetFunctions.getMoreStudyDetField('PR_STUDY_RISK');
			if (fld == PR_STUDY_RISK){
				if (!studyScreenFunctions.studyScrnIsAdmin){
					$j('#'+ fld.id +' option:not(:selected)').attr('disabled', true);
					fld.setAttribute("class", "readonly-input");
				}
			} else {
				if (studyScreenFunctions.studyScrnIsAdmin){
					fld.style.display = 'inline';
					fld.parentNode.parentNode.style.display = 'inline';
				} else {
					fld.style.display = 'none';
					fld.parentNode.parentNode.style.display = 'none';
				}
			}
			break;
		}
	}
};

studyScreenJS.createUIElements = function(){
	//----------------//
	if (document.getElementById('studyNumber_Label')){
		var studyNumberTR=document.getElementById('studyNumber_Label').parentNode.parentNode.parentNode.insertRow(0);
		var studyNumberTD = document.createElement('td');
		studyNumberTR.appendChild(studyNumberTD);

		if (studyScreenFunctions.studyScrnIsAdmin){
			var upccButton = document.createElement('button');
			upccButton.id = 'upccButton';
			upccButton.name = 'upccButton';
			upccButton.innerHTML = 'Generate UPCC#';
			studyNumberTD.appendChild(upccButton);
			$j('#upccButton').button();
		}
		
		var chkCTSRMCReviewdTD = document.createElement('td');
		var chkCTSRMCReviewd = document.createElement('input');
		chkCTSRMCReviewd.id = 'chkCTSRMCReviewd';
		chkCTSRMCReviewd.name = 'chkCTSRMCReviewd';
		chkCTSRMCReviewd.type= 'checkbox';
		if (!studyScreenFunctions.studyScrnIsAdmin){
			chkCTSRMCReviewd.disabled = true;
		}
		chkCTSRMCReviewdTD.appendChild(chkCTSRMCReviewd);
		chkCTSRMCReviewdTD.innerHTML += "CTSRMC Reviewed";

		studyNumberTR.appendChild(chkCTSRMCReviewdTD);
	}
	//----------------//

	var studyNumber = document.getElementById("studyNumber");
	if (studyNumber){
		var coopButton = document.createElement('button');
		coopButton.id = 'coopButton';
		coopButton.name = 'coopButton';
		studyNumber.parentNode.appendChild(coopButton);
		$j('#coopButton').button();
		$j('#coopButton').button("option", "label", "Registering a Cooperative Group study? Click Here!");
	
		if (!coopButton.addEventListener) {
			coopButton.attachEvent('onclick', studyScreenJS.registerCoop);
		} else {
			document.querySelector('#coopButton').addEventListener('click', studyScreenJS.registerCoop);
		}
	}

	var ifothersponsor = document.getElementById("ifothersponsor");
	if (ifothersponsor){
		var sponsorAnchor = document.createElement('A');
		sponsorAnchor.id = 'sponsorAnchor';
		sponsorAnchor.name = 'sponsorAnchor';
		sponsorAnchor.href = '#';
		sponsorAnchor.innerHTML = "Select Sponsor";
		ifothersponsor.parentNode.appendChild(sponsorAnchor);
		
		if (!sponsorAnchor.addEventListener) {
			sponsorAnchor.attachEvent('onclick', studyScreenJS.onClickSponsorLink);
		} else {
			document.querySelector('#sponsorAnchor').addEventListener('click', studyScreenJS.onClickSponsorLink);
		}
	}
	//----------------//
	if (studyFunctions.entryMode == 'M'){
		studyScreenJS.addStudyStatusSection();
		studyScreenJS.addPersonnelSection();
	}
	//----------------//
};

studyScreenJS.addStudyStatusSection = function(){
	var studySection2 = document.getElementById('studySection2');
	var studySection3 = document.createElement('div');
	studySection3.name = 'studySection';
	studySection3.id = 'studySection3';
	studySection3.className="portlet portletstatus ui-widget ui-widget-border ui-helper-clearfix ui-corner-all";
	studySection3.innerHTML = '<div id="studyTab3content" onclick="toggleDiv(\'studyTab3\');" class="portlet-header portletstatus ui-widget ui-widget-header ui-widget-content ui-helper-clearfix ui-corner-all" style="background-color:rgb(140, 130, 130)">'
		+ '<span class="ui-icon ui-icon-triangle-1-s"></span>'
		+ 'Study Status'
		+ '</div>'
		+'<div id="studyTab3"></div>';
	studySection2.parentNode.appendChild(studySection3);
	
	var studyTab3 = document.getElementById('studyTab3');
	var studyStatusTable = document.createElement('table');
	studyStatusTable.setAttribute('class','basetbl');
	studyStatusTable.style.width="100%";
	studyTab3.appendChild(studyStatusTable);
    
	var studyStatusTR = studyStatusTable.insertRow(0);

	var studyStatusLabelTD = studyStatusTR.insertCell(0);
	studyStatusLabelTD.width="20%";
	studyStatusLabelTD.innerHTML = "Protocol Status";
	
	var studyStatusTD = studyStatusTR.insertCell(1);
	
	var studyStatusSpan = document.createElement('span');
	studyStatusSpan.id = 'studyStatusSpan';
	studyStatusSpan.name = 'studyStatusSpan';
	studyStatusSpan.align = "left";
	studyStatusSpan.innerHTML = '';
	studyStatusTD.appendChild(studyStatusSpan);

	studyStatusTD.innerHTML += "&nbsp;&nbsp;";

	var studyStatusAnchor = document.createElement('a');
	studyStatusAnchor.id = 'studyStatusAnchor';
	studyStatusAnchor.name = 'studyStatusAnchor';
	studyStatusAnchor.href = '#';
	studyStatusAnchor.innerHTML = 'Update Status';
	studyStatusTD.appendChild(studyStatusAnchor);
	
	if (!studyStatusAnchor.addEventListener) {
		studyStatusAnchor.attachEvent('onclick', studyScreenJS.onClickUpdateStudyStatusLink);
	} else {
		document.querySelector('#studyStatusAnchor').addEventListener('click', studyScreenJS.onClickUpdateStudyStatusLink);
	}
};

studyScreenJS.addPersonnelSection = function(){
	var studySection3 = (document.getElementById('studySection3'))? document.getElementById('studySection3') : document.getElementById('studySection2');
	var studySection4 = document.createElement('div');
	studySection4.id = 'studySection4';
	studySection4.name = 'studySection';
	studySection4.className="portlet portletstatus ui-widget ui-widget-border ui-helper-clearfix ui-corner-all";
	studySection3.parentNode.appendChild(studySection4);
	studySection4.innerHTML = '<div id="studyTab4content" onclick="toggleDiv(\'studyTab4\');" class="portlet-header portletstatus ui-widget ui-widget-header ui-widget-content ui-helper-clearfix ui-corner-all" style="background-color:rgb(140, 130, 130)">'
		+ '<span class="ui-icon ui-icon-triangle-1-s"></span>'
		+ 'Personnel (Quick Contacts)'
		+ '</div>'
		+'<div id="studyTab4"></div>';
	
	var studyTab4 = document.getElementById('studyTab4');
	var studyStatusTable = document.createElement('table');
	studyStatusTable.setAttribute('class','basetbl');
	studyStatusTable.style.width="100%";
	studyTab4.appendChild(studyStatusTable);

	var studyStatusTR = studyStatusTable.insertRow(0); 

	var studyContactsTD = studyStatusTR.insertCell(0);
	var studyContactsAnchor = document.createElement('a');
	studyContactsAnchor.id = 'studyContactsAnchor';
	studyContactsAnchor.name = 'studyContactsAnchor';
	studyContactsAnchor.href = '#';
	studyContactsAnchor.innerHTML = 'Update Study Contacts';
	studyContactsTD.appendChild(studyContactsAnchor);
	
	if (!studyContactsAnchor.addEventListener) {
		studyContactsAnchor.attachEvent('onclick', studyScreenJS.onClickUpdateStudyTeamLink);
	} else {
		document.querySelector('#studyContactsAnchor').addEventListener('click', studyScreenJS.onClickUpdateStudyTeamLink);
	}
};


studyScreenJS.openLookup = function (viewId, keyword){
	if (!viewId || viewId <= 0) return;
	var dfilter = '';
	var formName = studyFunctions.formObj.name;
	var windowName = window.open("getlookup.jsp?viewId="+viewId+"&form="+formName+"&dfilter=&keyword="+keyword,"Information",
			"toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=700,height=550 top=100,left=100 0, ");
	windowName.focus();
};

studyScreenJS.onClickStudyPILink = function(e){
	e.preventDefault();

	var viewId = studyScreenJS.piProgMembershipLkpId;
	var eleProgMembership = moreStudyDetFunctions.getMoreStudyDetCodePK("PROG_MEMBERSHIP");
	var keywordStr = 'prinInv|pk_user~prinInvName|uname~alternateId'+eleProgMembership+'|progMembership';
	studyScreenJS.openLookup(viewId, keywordStr);
};

studyScreenJS.onClickSponsorLink = function(e){
	// Please configure studyScreenJS.sponsorLkpId and studyScreenJS.sponsorLkpKeywordStr at the top.
	var viewId = studyScreenJS.sponsorLkpId;
	var keywordStr = studyScreenJS.sponsorLkpKeywordStr;

	if (viewId < 1 || (!keywordStr || keywordStr.length < 0)) {
		alert('Sponsor lookup is not configured.\nPlease contact your System Administrator.');
		return;
	}
	studyScreenJS.openLookup(viewId, keywordStr);
};

studyScreenJS.addListeners = function(){
	var studyinv_selectLink = $j('#studyinv_selectLink');
	if (studyinv_selectLink){
		if (studyScreenJS.piProgMembershipLkpId > 0){
			$j("#studyinv_selectLink").die('click');
			$j("#studyinv_selectLink").unbind("click");
			$j('#studyinv_selectLink').click(studyScreenJS.onClickStudyPILink);
		}
	}

	var Oth_Review_Grp = moreStudyDetFunctions.getMoreStudyDetField("OTH_REVIEW_GRP");
	if (Oth_Review_Grp){
		var Oth_Review_Grp_Checks = document.getElementsByName('alternateId'+moreStudyDetFunctions.getMoreStudyDetCodePK("OTH_REVIEW_GRP")+'Checks');

		for (var j=0; j < Oth_Review_Grp_Checks.length; j++){
			$j('#' + Oth_Review_Grp_Checks[j].id).live('change', studyScreenJS.OTH_REVIEW_GRP_checkBoxChange);
			$j('#' + Oth_Review_Grp_Checks[j].id).live('click', studyScreenJS.OTH_REVIEW_GRP_checkBoxChange);
			studyScreenJS.runInterFieldActions("OTH_REVIEW_GRP", Oth_Review_Grp_Checks[j]);
		}
	}
	
	$j('#upccButton').live('click', studyScreenJS.requestUPCC);
	
	$j('#chkCTSRMCReviewd').live('change', studyScreenJS.chkCTSRMCReviewd_checkBoxChange);

	//Site
	$j('#disSitename').live('focus', function(e){
		studyScreenJS.runInterFieldActions("disSitename");
	});
	
	//Study Source
	$j('#studyResType').live('change', function(e){
		studyScreenJS.runInterFieldActions("studyResType");
	});
	
	//Penn Initiated Multi-Site
	$j('#studyScope').live('change', function(e){
		studyScreenJS.runInterFieldActions("studyScope");
	});
};

studyScreenJS.loadStudyStatus = function(){
	if (studyFunctions.studyId > 0){
		try {
			jQuery.ajax({
				type: "POST",
				url:"getStudyStatusData.jsp",
				data: {"studyId": studyFunctions.studyId},
				success: function(resp) {
					var currentStudyStatId = resp.currentStudyStatId;
					var currentStudyStatus = resp.currentStudyStatus;
					if (!currentStudyStatus) return;
					var currentStatusStartDate = resp.currentStudyStatStartDate;

					$j('#studyStatusSpan').html(currentStudyStatus + "&nbsp;&nbsp;" + currentStatusStartDate);
				}
			});
		} catch (e){
			if (e.result < 0){
				$j("#fillform").prepend('<P>'+e.resultMsg+'</P>');
			}
		}
	}
};

studyScreenJS.loadStudyTeam = function(){
	if (!studyScreenFunctions.studyScrnTeamRoles && (studyScreenFunctions.studyScrnTeamRoles.length <= 0)){ return; }

	if (studyFunctions.studyId > 0){
		jQuery.ajax({
			type: "POST",
			url:"getStudyTeam.jsp",
			data : { 
				"studyId" :studyFunctions.studyId,
				"role_subType": studyScreenFunctions.studyScrnTeamRoles
			},
	        success: function(resp) {
				var arrRoles = resp.arrRoles;

				if (arrRoles && arrRoles.length > 0 ){
		        	var arrUserIds = resp.arrUserIds;
		        	var arrUserMoreDetails = resp.arrUserMoreDetails;

		        	var arrFirstNames = resp.arrFirstNames;
					var arrLastNames = resp.arrLastNames;
					var arrUserPhones = resp.arrUserPhones;
					var arrUserEmails = resp.arrUserEmails;
	
					var studyTab4 = document.getElementById('studyTab4');
					var studyTeamTable = document.createElement('table');
					studyTeamTable.id = 'studyTeamTable';
					$j('#studyTeamTable').addClass('basetbl midAlign');
					studyTeamTable.style.border="1";
					studyTeamTable.style.width="99%";
					studyTab4.appendChild(studyTeamTable);

					var newTHead = studyTeamTable.createTHead();
					var studyTeamTR = newTHead.insertRow(-1);

					var columnHeaders = ['Personnel Type', 'First Name', 'Last Name', 'Degree', 'Phone', 'Email', 'Investigator No.'];
					for (var colIndx = 0; colIndx < columnHeaders.length; colIndx++){
						var studyContactsTD = document.createElement("TH");
						studyContactsTD.width="15%";
						studyContactsTD.innerHTML = columnHeaders[colIndx];
						studyTeamTR.appendChild(studyContactsTD);	
					}

					for (var indx =0; indx < arrFirstNames.length; indx++){
						var cellCount = 0;
						studyTeamTR = studyTeamTable.insertRow(indx+1);
						studyTeamTR.id = 'studyTeamTR'+indx;
						if (indx%2 == 0)
							$j('#studyTeamTR'+indx).addClass('browserEvenRow');
						else
							$j('#studyTeamTR'+indx).addClass('browserOddRow');
							
						studyContactsTD = studyTeamTR.insertCell(cellCount++);
						studyContactsTD.innerHTML = arrRoles[indx];

						studyContactsTD = studyTeamTR.insertCell(cellCount++);
						studyContactsTD.innerHTML = arrFirstNames[indx];

						studyContactsTD = studyTeamTR.insertCell(cellCount++);
						studyContactsTD.innerHTML = arrLastNames[indx];

						var jsObj = arrUserMoreDetails[indx];
						var degreeIndx = (jsObj.userMDSubTypes).indexOf('degree');
						var degree = (jsObj.userMDData[degreeIndx])? jsObj.userMDData[degreeIndx]: '-';
						degree = (degree.length > 0)? degree : '-';
						studyContactsTD = studyTeamTR.insertCell(cellCount++);
						studyContactsTD.innerHTML = degree;

						var phone = (arrUserPhones[indx])? arrUserPhones[indx] : '-';
						studyContactsTD = studyTeamTR.insertCell(cellCount++);
						studyContactsTD.innerHTML = phone;
						
						var email = (arrUserEmails[indx])? arrUserEmails[indx] : '-';
						studyContactsTD = studyTeamTR.insertCell(cellCount++);
						studyContactsTD.innerHTML = email;

						var invNoIndx = (jsObj.userMDSubTypes).indexOf('investigatorNo');
						var investigatorNo = (jsObj.userMDData[invNoIndx])? jsObj.userMDData[invNoIndx]: '-';
						investigatorNo = (investigatorNo.length > 0)? investigatorNo : '-';
						studyContactsTD = studyTeamTR.insertCell(cellCount++);
						studyContactsTD.innerHTML = investigatorNo;
					}
				}
			}
		});
	}
};

studyScreenJS.runInterFieldActions = function(fieldId, fld){
	switch(fieldId){
		case "COOP_TRIAL":
			var COOP_TRIAL = moreStudyDetFunctions.getMoreStudyDetField("COOP_TRIAL");
			if (!COOP_TRIAL) return;
	
			var fldValue = COOP_TRIAL.value;
			if ('Y' == fldValue){
				$j('#studyNumber_Label').html('Cooperative Group#');
				//Change Button Text
				$j('#coopButton').button("option", "label", "Click here to request a UPCC#.");
			} else {
				$j('#studyNumber_Label').html('UPCC#');
			}
			break;
		case "CTSRMC_REVIEWED":
			var CTSRMC_REVIEWED = moreStudyDetFunctions.getMoreStudyDetField("CTSRMC_REVIEWED");
			if (!CTSRMC_REVIEWED) return;
			var chkCTSRMCReviewd = document.getElementById('chkCTSRMCReviewd');
			if (!chkCTSRMCReviewd) return;

			var fldValue = CTSRMC_REVIEWED.value;
			if ('Y' == fldValue){
				chkCTSRMCReviewd.checked = true;
			} else {
				chkCTSRMCReviewd.checked = false;
			}
			break;
		case "chkCTSRMCReviewd":
			if (!fld) return;
			var CTSRMC_REVIEWED = moreStudyDetFunctions.getMoreStudyDetField("CTSRMC_REVIEWED");
			if (!CTSRMC_REVIEWED) return;

			if (fld.checked)
				CTSRMC_REVIEWED.value = 'Y';
			else 
				CTSRMC_REVIEWED.value = 'N';
			break;
		case "OTH_REVIEW_GRP":
			if (!fld) return;
			var checkValue = fld.value;
			if (!checkValue) return;
			
			if (checkValue == "NATIONL" && (!fld.checked)){
				var studyResType = document.getElementById('studyResType');
				var studyResTypeValue = studyResType.options[studyResType.selectedIndex].innerHTML;
				
				if (studyResTypeValue == 'Industrial' || studyResTypeValue == 'Externally Peer-Reviewed'){
					fld.checked = true;
				}
			}

			var Othe_Review_Grp_No = moreStudyDetFunctions.getMoreStudyDetField("PR_"+checkValue+"_NUM");
			var Othe_Review_Grp_Date = moreStudyDetFunctions.getMoreStudyDetField("PR_"+checkValue+"_DATE");
			if (fld.checked){
				(Othe_Review_Grp_No.parentNode.parentNode).style.display="";
				Othe_Review_Grp_No.style.display="block";
				(Othe_Review_Grp_Date.parentNode.parentNode).style.display="";
				Othe_Review_Grp_Date.style.display="block";
			} else {
				(Othe_Review_Grp_No.parentNode.parentNode).style.display="none";
				Othe_Review_Grp_No.value = '';
				Othe_Review_Grp_No.style.display="none";
				(Othe_Review_Grp_Date.parentNode.parentNode).style.display="none";
				Othe_Review_Grp_Date.value = '';
				Othe_Review_Grp_Date.style.display="none";
			}
			break;
		case "studyResType"://Study Source
			var studyResType = document.getElementById('studyResType');
			var studyResTypeValue = studyResType.options[studyResType.selectedIndex].innerHTML;

			if (studyResTypeValue != 'National'){
				//Cooperative Trial Registered
				var COOP_TRIAL = moreStudyDetFunctions.getMoreStudyDetField('COOP_TRIAL');
				if (COOP_TRIAL && $j('#'+ COOP_TRIAL.id).val() == 'Y'){
					alert("While registering a Cooperative Group trial, 'Study Source' value should be 'National'");
					$j("#studyResType option").each(function () {
				        if ($j(this).html() == "National") {
				            $j(this).attr("selected", "selected");
				            studyScreenJS.runInterFieldActions("studyResType");
				            return;
				        }
					});
					return;
				}
			}

			var PR_COOPGROUP_DD = moreStudyDetFunctions.getMoreStudyDetField("PR_COOPGROUP_DD");
			if (studyResTypeValue == 'National'){
				(PR_COOPGROUP_DD.parentNode.parentNode).style.display="";
				PR_COOPGROUP_DD.style.display="block";
			} else {
				(PR_COOPGROUP_DD.parentNode.parentNode).style.display="none";
				PR_COOPGROUP_DD.value = '';
				PR_COOPGROUP_DD.style.display="none";
			}

			var PR_CONSORTIUM = moreStudyDetFunctions.getMoreStudyDetField("PR_CONSORTIUM");
			if (studyResTypeValue == 'Externally Peer-Reviewed'){
				(PR_CONSORTIUM.parentNode.parentNode).style.display="";
				PR_CONSORTIUM.style.display="block";
			} else {
				(PR_CONSORTIUM.parentNode.parentNode).style.display="none";
				PR_CONSORTIUM.value = '';
				PR_CONSORTIUM.style.display="none";
			}
			
			var PR_NATIONL_NUM_MAND = document.getElementById('PR_NATIONL_NUM_MAND');
			if (studyResTypeValue == 'Industrial'){
				studyScreenJS.OTH_REVIEW_GRP_checkCheckBox("NATIONL", true);
				PR_NATIONL_NUM_MAND.style.display="inline";
			} else if (studyResTypeValue == 'Externally Peer-Reviewed'){
				studyScreenJS.OTH_REVIEW_GRP_checkCheckBox("NATIONL", true);
				PR_NATIONL_NUM_MAND.style.display="none";
			} else {
				studyScreenJS.OTH_REVIEW_GRP_checkCheckBox("NATIONL", false);
				PR_NATIONL_NUM_MAND.style.display="none";
			}

			break;
		case "disSitename"://Disease Site
			//if (studyFunctions.entryMode != 'M') return;
			var disSitename = document.getElementById('disSitename');
			var PR_DISEASE_STAT = moreStudyDetFunctions.getMoreStudyDetField("PR_DISEASE_STAT");
			var HORM_REC_STATUS = moreStudyDetFunctions.getMoreStudyDetField("HORM_REC_STATUS");
			var HER2_NEU_STATUS = moreStudyDetFunctions.getMoreStudyDetField("HER2_NEU_STATUS");

			if (disSitename.value == 'Breast'){
				(PR_DISEASE_STAT.parentNode.parentNode).style.display="inline";
				PR_DISEASE_STAT.style.display="block";
				
				(HORM_REC_STATUS.parentNode.parentNode).style.display="inline";
				HORM_REC_STATUS.style.display="block";
				
				(HER2_NEU_STATUS.parentNode.parentNode).style.display="inline";
				HER2_NEU_STATUS.style.display="block";
			} else {
				(PR_DISEASE_STAT.parentNode.parentNode).style.display="none";
				PR_DISEASE_STAT.value = '';
				PR_DISEASE_STAT.style.display="none";
				
				(HORM_REC_STATUS.parentNode.parentNode).style.display="none";
				HORM_REC_STATUS.value = '';
				HORM_REC_STATUS.style.display="none";
				
				(HER2_NEU_STATUS.parentNode.parentNode).style.display="none";
				HER2_NEU_STATUS.value = '';
				HER2_NEU_STATUS.style.display="none";
			}
			break;
		case "studyScope": //Penn Initiated Multi-Site 
			//if (studyFunctions.entryMode != 'M') return;
			var studyScope = document.getElementById('studyScope');
			var studyScopeValue = studyScope.options[studyScope.selectedIndex].innerHTML;
			var PENN_INIT_SITE = moreStudyDetFunctions.getMoreStudyDetField("PENN_INIT_SITE");

			if (studyScopeValue == 'Yes'){
				(PENN_INIT_SITE.parentNode.parentNode).style.display="inline";
				PENN_INIT_SITE.style.display="inline";
			} else {
				(PENN_INIT_SITE.parentNode.parentNode).style.display="none";
				PENN_INIT_SITE.value = '';
				PENN_INIT_SITE.style.display="none";
			}
			break;
		default:
			break
	}
};

studyScreenJS.runAllInterFieldActions = function(){
	studyScreenJS.runInterFieldActions("COOP_TRIAL");
	studyScreenJS.runInterFieldActions("disSitename");
	studyScreenJS.runInterFieldActions("studyResType");
	studyScreenJS.runInterFieldActions("studyScope");
	studyScreenJS.runInterFieldActions("CTSRMC_REVIEWED");
};

studyScreenJS.OTH_REVIEW_GRP_checkBoxChange = function(e){
	studyScreenJS.runInterFieldActions("OTH_REVIEW_GRP", this);
};

studyScreenJS.OTH_REVIEW_GRP_checkCheckBox = function(checkValue, boolChecked){
	var Oth_Review_Grp = moreStudyDetFunctions.getMoreStudyDetField("OTH_REVIEW_GRP");
	if (Oth_Review_Grp){
		var Oth_Review_Grp_Checks = document.getElementsByName('alternateId'+moreStudyDetFunctions.getMoreStudyDetCodePK("OTH_REVIEW_GRP")+'Checks');

		for (var j=0; j < Oth_Review_Grp_Checks.length; j++){
			if (Oth_Review_Grp_Checks[j].value == checkValue){
				Oth_Review_Grp_Checks[j].checked = boolChecked;
				studyScreenJS.runInterFieldActions("OTH_REVIEW_GRP", Oth_Review_Grp_Checks[j]);
				break;
			}
		}
	}
};

studyScreenJS.chkCTSRMCReviewd_checkBoxChange = function (e){
	if (studyScreenFunctions.studyScrnIsAdmin){
		studyScreenJS.runInterFieldActions("chkCTSRMCReviewd", this);
	}
};

studyScreenJS.requestUPCC = function (e){
	if (e.preventDefault){ e.preventDefault(); }
	$j('#studyNumber').focus();
	return false;
};

studyScreenJS.registerCoop = function(e){
	if (e.preventDefault){ e.preventDefault(); }

	//Cooperative Trial Registered
	var COOP_TRIAL = moreStudyDetFunctions.getMoreStudyDetField('COOP_TRIAL');
	var labelText = $j('#studyNumber_Label').html();
	if (labelText == 'Cooperative Group#'){
		//Show Generate UPCC# Button
		if (document.getElementById('upccButton')){
			document.getElementById('upccButton').style.display = 'inline';
		}
		
		//Change Label
		$j('#studyNumber_Label').html('UPCC#');
		
		//Change Study Number Value 
		if (studyFunctions.entryMode == 'N'){
			$j('#studyNumber').val("REQUESTING A UPCC");
		}
		
		//Change Button Text
		$j('#coopButton').button("option", "label", "Registering a Cooperative Group study? Click Here!");

		if (COOP_TRIAL){
			$j('#'+COOP_TRIAL.id).val("N");
		}
	} else {
		//Hide Generate UPCC# Button
		if (document.getElementById('upccButton')){
			document.getElementById('upccButton').style.display = 'none';
		}

		//Change Label
		$j('#studyNumber_Label').html('Cooperative Group#');
		
		//Change Study Number Value
		if (studyFunctions.entryMode == 'N'){
			$j('#studyNumber').val("");
		}
		
		//Change Button Text
		$j('#coopButton').button("option", "label", "Click here to request a UPCC#.");

		if (COOP_TRIAL){
			$j('#'+COOP_TRIAL.id).val("Y");
		}

		$j("#studyResType option").each(function () {
	        if ($j(this).html() == "National") {
	            $j(this).attr("selected", "selected");
	            studyScreenJS.runInterFieldActions("studyResType");
	        }
		});
	}
	$j('#studyNumber').focus();
	return false;
};

studyScreenJS.onClickUpdateStudyStatusLink = function (){
	if (confirm('WARNING \nAny unsaved information on this page will be lost. \nClick OK to continue to the Study Status area. \nClick Cancel to stay on this page.')){
		$("studyScreenForm").onSubmit=null;
		$("studyScreenForm").action ="studystatusbrowser.jsp?srcmenu=tdmenubaritem3&selectedTab=3&studyId="+studyFunctions.studyId;
		$("studyScreenForm").submit();
	}
};

studyScreenJS.onClickUpdateStudyTeamLink = function (){
	if (confirm('WARNING \nAny unsaved information on this page will be lost. \nClick OK to continue to the Study Team area. \nClick Cancel to stay on this page.')){
		$("studyScreenForm").onSubmit=null;
		$("studyScreenForm").action ="teamBrowser.jsp?srcmenu=tdmenubaritem3&selectedTab=5&studyId="+studyFunctions.studyId;
		$("studyScreenForm").submit();
	}
};

studyScreenJS.applySecurity = function (){
	var securityType = studyFunctions.entryMode;

	//Generate UPCC# Button
	studyScreenJS.applySecurityToField('upccButton');
	//CTSRMC Reviewed
	studyScreenJS.applySecurityToField('chkCTSRMCReviewd');

	if (securityType == 'M'){
		//UPCC#
		studyScreenJS.applySecurityToField('studyNumber');
	}

	//NCT Code
	studyScreenJS.applySecurityToField('nctNumber');
	//Study Risk
	var PR_STUDY_RISK = moreStudyDetFunctions.getMoreStudyDetField('PR_STUDY_RISK');
	studyScreenJS.applySecurityToField(PR_STUDY_RISK.id);

	if (securityType == 'M'){
		//Study Title
		studyScreenJS.applySecurityToField('studyTitle');
	}
};

$j(document).ready(function() {
	if (studyFunctions.entryMode == 'N'){
		//Setting default values
		$j('#studyNumber').val("REQUESTING A UPCC");
		$j('#durunit').val('months');

		var cc = "Abramson Cancer Center";
		$j("#studyDivision option:contains(Abramson Cancer Center)").attr('selected', 'selected');
		$j('#studyDivision').change();
	}

	$j('#nssLink').hide();

	$j("#durunit option[value='days']").remove();
	$j("#durunit option[value='years']").remove();
	$j("#durunit option[value='weeks']").remove();

	studyScreenJS.createUIElements();
	studyScreenJS.addListeners();
	studyScreenJS.loadStudyStatus();
	studyScreenJS.loadStudyTeam();

	studyScreenJS.runAllInterFieldActions();
	studyScreenJS.applySecurity();
});