<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.web.userReportLog.UserReportLogJB"%>
<%@page import="com.velos.eres.web.userReportLog.UserReportLogJBHelper"%>
<%@page import="java.text.SimpleDateFormat,java.util.*,com.velos.eres.service.util.*" %>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<META http-equiv="Content-Type" content="text/html; charset=UTF-8">	
<%
	HttpSession tSession = request.getSession(false);
	if (sessionmaint.isValidSession(tSession)){
		UserReportLogJBHelper userReportLogJBHelper = null;
		UserReportLogJB userReportLogJB = null;
		String queryString	= null ;
		String requestURL 	= null ;
		String repTitle 	= null;
		Date accessDateTime	= null;
		String reportCat	= null;
		String urlPath 		= null ;
		/*Now fetch the data from session and request*/
		int userId 			= EJBUtil.stringToNum((String) (tSession.getValue("userId")));
		String ipAdd 		= (String)tSession.getValue("ipAdd");
		Integer accountId 	= StringUtil.stringToInteger((tSession.getValue("accountId")==null) ? "0" : (String)tSession.getValue("accountId"));
		Integer repId 		= StringUtil.stringToInteger((request.getParameter("repId")==null) ? "0" : request.getParameter("repId"));
		String repXml 		= request.getParameter("repXml");
		String fileName 	= request.getParameter("fileName");
		String fileDnPath 	= StringUtil.decodeString(request.getParameter("filePath"));
		String moduleName 	= request.getParameter("moduleName");
		//moduleName 			= UserReportLogJBHelper.moduleName(moduleName);
		String downloadFormat = request.getParameter("downloadFormat");
		Integer downloadFlag  = StringUtil.stringToInteger(request.getParameter("downloadFlag"));
		String tableName=null;
		if(null != tSession.getAttribute("reportCat"))
			reportCat = tSession.getAttribute("reportCat").toString();
		
		requestURL 	= UserReportLogJBHelper.getRequestURL(moduleName, request);
		queryString = UserReportLogJBHelper.getQueryString(moduleName, request);
		requestURL  = ( requestURL == null  ) ? "" : requestURL;
		queryString = ( queryString == null ) ? "" : queryString;
		
		/* Current System date and time */
		accessDateTime  = new Date(DateUtil.getCurrentDateTime());
		if(!(moduleName.equalsIgnoreCase(LC.L_Rpt_Central)) && 
				(reportCat!=null &&  reportCat.equalsIgnoreCase("ad-hoc queries"))){
			tSession.removeAttribute("reportCat");
			reportCat = null;
		}
		if(null==tableName && repId!=0)tableName = UserReportLogJBHelper.getTableName(moduleName,reportCat);
		if(null!=request.getParameter("reportInvoice") && moduleName.equalsIgnoreCase(LC.L_Invoice)){
			tableName="ER_REPORT";
			moduleName ="";
		}
		/*Now call the helper method for further processing the data.*/
		userReportLogJBHelper = new UserReportLogJBHelper();
		if(repId!=0)
			repTitle = userReportLogJBHelper.getReportTitle(moduleName,reportCat,accountId,repId);
		
		if(moduleName.equalsIgnoreCase("studyBudget"))moduleName=LC.L_Budget;
		if(null!=request.getParameter("reportInvoice")){
			moduleName=LC.L_Invoice;
			if(StringUtil.stringToInteger(request.getParameter("successflag"))!=0){
				downloadFlag= new Integer(1);
				downloadFormat=LC.L_Printer_FriendlyFormat;
			}
		}
		if(null!=request.getParameter("previewReportName") && repId==0){
			repTitle = request.getParameter("previewReportName");
		}
		moduleName 			= UserReportLogJBHelper.moduleName(moduleName);
		userReportLogJB = userReportLogJBHelper.setUserReportLogJBData(userId,repId,repTitle,ipAdd,requestURL,
				accessDateTime,queryString,moduleName,fileName,fileDnPath,repXml,downloadFlag,downloadFormat,tableName);
		userReportLogJBHelper.setUserReportLogDetails(userReportLogJB);
	}
%>