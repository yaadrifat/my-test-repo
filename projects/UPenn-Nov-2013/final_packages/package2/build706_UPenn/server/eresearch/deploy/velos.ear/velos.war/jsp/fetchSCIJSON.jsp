<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList,java.util.HashMap,org.json.*"%>
<%@page import="com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.*"%>
<%@page import="com.velos.esch.business.common.SubCostItemDao"%>
<%@page import="com.velos.esch.business.common.EventdefDao"%>
<%@page import="com.velos.esch.business.common.EventAssocDao"%>


<%@page import="com.velos.esch.business.common.SchCodeDao"%>
<%@page import="com.velos.eres.business.common.CodeDao"%>
<%@page import="com.velos.esch.business.common.SubCostItemDao"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="subCostItemB" scope="request" class="com.velos.esch.web.subCostItem.SubCostItemJB"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="protVisitB" scope="request" class="com.velos.esch.web.protvisit.ProtVisitJB"/>
<jsp:useBean id="visitdao" scope="request" class="com.velos.esch.business.common.ProtVisitDao"/>

<%
    request.setCharacterEncoding("UTF-8");
    response.setContentType("application/json");
    // response.setContentType("text/html");

	HttpSession tSession = request.getSession(true);
    JSONObject jsObj = new JSONObject();
    if (!sessionmaint.isValidSession(tSession)) {
        // Session is invalid; print an error and exit
        response.sendError(HttpServletResponse.SC_FORBIDDEN, "SESSION_TIME_OUT");
        jsObj.put("error", new Integer(-1));
        jsObj.put("errorMsg", MC.M_UsrNot_LoggedIn);/*jsObj.put("errorMsg", "User is not logged in.");*****/
	    out.println(jsObj.toString());
        return;
    }
    
	String protocolId = request.getParameter("protocolId");
	if(protocolId == "" || protocolId == null || protocolId.equals("null") || protocolId.equals("")) {
	    // A valid protocol ID is required; print an error and exit
        jsObj.put("error", new Integer(-2));
        jsObj.put("errorMsg", MC.M_PcolId_Invalid);/*jsObj.put("errorMsg", "Protocol ID is invalid.");*****/
	    out.println(jsObj.toString());
	    return;
	}
	
    // Define page-wide variables here
    StringBuffer sb = new StringBuffer();
    String calledFrom = request.getParameter("calledFrom");
    boolean isLibCal = false;
    boolean isStudyCal = false;
    String protocolName = null;
    String calStatus = null;
    int errorCode = 0;
    ArrayList itemIds = null, itemNames = null, itemCategory = null,
    	itemCosts = null, itemUnits = null;
    ArrayList itemVisitIds = null;
    ArrayList itemOfflines = null;
    
    if ("P".equals(calledFrom) || "L".equals(calledFrom) ) {
        isLibCal = true;
    } else if ("S".equals(calledFrom)) {
        isStudyCal = true;
    } else {
        jsObj.put("error", new Integer(-3));
        jsObj.put("errorMsg", MC.M_CalType_Miss);/*jsObj.put("errorMsg", "Calendar Type is missing.");*****/
	    out.println(jsObj.toString());
        return;
    }
    
   	SubCostItemDao sciDao = subCostItemB.getProtSelectedAndGroupedItems(EJBUtil.stringToNum(protocolId));
	itemIds = sciDao.getItemIds();
	itemVisitIds = sciDao.getVisitIds();
	itemNames = sciDao.getItemNames();
	itemCategory = sciDao.getItemCategory();
	itemCosts = sciDao.getItemCosts();
	itemUnits = sciDao.getItemUnits();
	//itemOfflines = assocdao.getOfflineFlags(); // Unique to event_assoc, not in event_def

   	 String calStatusPk = "";
	 SchCodeDao scho = new SchCodeDao();
   	 if (isLibCal) {
   		eventdefB.setEvent_id(EJBUtil.stringToNum(protocolId));
   		eventdefB.getEventdefDetails();
   		protocolName = eventdefB.getName();
   		
		//KM-#DFin9
		calStatusPk = eventdefB.getStatCode();
		calStatus = scho.getCodeSubtype(EJBUtil.stringToNum(calStatusPk)).trim();


   	 }else if (isStudyCal) {
		eventassocB.setEvent_id(EJBUtil.stringToNum(protocolId));
    	eventassocB.getEventAssocDetails();
    	protocolName = eventassocB.getName();
    	
		//KM-#DFin9
		calStatusPk = eventassocB.getStatCode();
		calStatus = scho.getCodeSubtype(EJBUtil.stringToNum(calStatusPk)).trim();

     }
   	
    ArrayList visitIds = null;
	ArrayList visitNames = null;
	ArrayList visitDisplacements = null;
	String visitId = null;
	String visitName = null;
	
	visitdao = protVisitB.getProtocolVisits(EJBUtil.stringToNum(protocolId));
	visitIds = visitdao.getVisit_ids();
	visitNames = visitdao.getNames();
	visitDisplacements = visitdao.getDisplacements();

    int noOfItems = itemIds == null ? 0 : itemIds.size();
    if (protocolName == null) { protocolName = ""; }
    int noOfVisits = visitIds == null ? 0 : visitIds.size();
    
    
    SchCodeDao schcdao = new SchCodeDao();
    schcdao.getCodeValues("category");
    
    jsObj.put("error", errorCode);
    jsObj.put("errorMsg", "");
    jsObj.put("noOfItems", noOfItems);
    
    JSONArray jsItems = new JSONArray();
    for (int iX=0; iX<itemIds.size(); iX++) {
        JSONObject jsObj1 = new JSONObject();
        jsObj1.put(String.valueOf(itemIds.get(iX)), itemNames.get(iX));
        jsItems.put(jsObj1);
    }
    jsObj.put("items", jsItems);
    jsObj.put("protocolName", protocolName);
    jsObj.put("calStatus", calStatus);
    jsObj.put("noOfVisits", noOfVisits);
    jsObj.put("costCategoryId",schcdao.getCId());
    jsObj.put("costCategory",schcdao.getCDesc());
    
    JSONArray jsColArray = new JSONArray();
    {
    	JSONObject jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "itemSeq");
    	jsObjTemp1.put("label", "itemSeq");
    	jsColArray.put(jsObjTemp1);
    	
    	// action icons
    	jsObjTemp1 = new JSONObject();
	   	jsObjTemp1.put("key", "delete");
	   	jsObjTemp1.put("label", LC.L_Delete);/*jsObjTemp1.put("label", "Delete");*****/
    	jsObjTemp1.put("action", "delete");
    	jsColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "costCat");
    	jsObjTemp1.put("label", LC.L_Cost_Cat);/*jsObjTemp1.put("label", "Cost Category");*****/
    	jsColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "costCatId");
    	jsObjTemp1.put("label",LC.L_CostCat_ID);/*jsObjTemp1.put("label", "Cost Category Id");*****/ 	
    	jsColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "checkItem");
    	jsObjTemp1.put("label", "");
    	jsColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "itemId");
    	jsObjTemp1.put("label",LC.L_ItemID);/*jsObjTemp1.put("label", "itemId");*****/
    	jsColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "item");
    	jsObjTemp1.put("label",LC.L_CostItem_Name+" <FONT class='mandatory'><sup>*</sup></FONT>");/*jsObjTemp1.put("label", "Cost Item Name <FONT class='mandatory'><sup>*</sup></FONT>");*****/
    	jsObjTemp1.put("type", "varchar");  	
    	jsColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "cost");
    	jsObjTemp1.put("label", LC.L_Unit_Cost);/*jsObjTemp1.put("label", "Unit Cost");*****/
    	jsObjTemp1.put("type", "number");
    	jsColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "units");
    	jsObjTemp1.put("label", LC.L_Number_OfUnits);/*jsObjTemp1.put("label", "Number of Units");*****/
    	jsObjTemp1.put("type", "integer");
    	jsColArray.put(jsObjTemp1);
    }
    JSONArray jsDisplacements = new JSONArray();
    for (int iX=0; iX<noOfVisits; iX++) {
        JSONObject jsObj1 = new JSONObject();
        jsObj1.put("key", "v"+String.valueOf(visitIds.get(iX)));
        jsObj1.put("value", visitDisplacements.get(iX));
        jsDisplacements.put(jsObj1);
        JSONObject jsObjTemp = new JSONObject();
        jsObjTemp.put("key", "v"+(visitIds.get(iX)));
        StringBuffer sb1 = new StringBuffer((String)visitNames.get(iX));
        sb1.append(" <input type='checkbox' ");
        sb1.append("name='").append("all_v").append(visitIds.get(iX)).append("'");
        sb1.append("id='").append("all_v").append(visitIds.get(iX)).append("'");
        sb1.append(" onclick='VELOS.subjectGrid.visitAll(\"v").append(visitIds.get(iX)).append("\");'");
        sb1.append("> ");
        jsObjTemp.put("label", sb1.toString());
        jsObjTemp.put("seq", "v"+(iX+1));
        jsColArray.put(jsObjTemp);
    }
    jsObj.put("displacements", jsDisplacements);
    jsObj.put("colArray", jsColArray);
    
    if (itemOfflines != null) {
        JSONArray offlineKeys = new JSONArray();
        JSONArray offlineValues = new JSONArray();
        for (int iX=0; iX<itemOfflines.size(); iX++) {
        	if (((Integer)itemIds.get(iX)).intValue() == 0
        	        || ((Integer)itemVisitIds.get(iX)).intValue() == 0
        	        || itemOfflines.get(iX) == null) { continue; }
            JSONObject jsObjTemp1 = new JSONObject();
        	jsObjTemp1.put("key", "e"+itemIds.get(iX)+"v"+itemVisitIds.get(iX));
        	offlineKeys.put(jsObjTemp1);
            JSONObject jsObjTemp2 = new JSONObject();
        	jsObjTemp2.put("value", itemOfflines.get(iX));
        	offlineValues.put(jsObjTemp2);
        }
        jsObj.put("offlineKeys", offlineKeys);
        jsObj.put("offlineValues", offlineValues);
    }
    
	ArrayList dataList = new ArrayList();
	int itemSeq =0;
    for (int iX=0; iX<itemIds.size(); iX++) {
        if (((Integer)itemIds.get(iX)).intValue() < 1) { continue; }
        if (dataList.size() == 0) {
            HashMap map1 = new HashMap();
            itemSeq++;
            map1.put("itemSeq", ""+itemSeq);
            
            int categoryId=0;          
            categoryId = EJBUtil.stringToNum((String)itemCategory.get(iX));
            String ddCategory = schcdao.toPullDown("costCat"+itemIds.get(iX), categoryId);
            //String ddCategory = schcdao.toJSON();
            ddCategory= schcdao.getCodeDescription(categoryId);
            map1.put("costCat", ddCategory);
            map1.put("costCatId", ""+categoryId);
            
            String itemName = (String)itemNames.get(iX);
            
          	//replace less than and greater than signs
            if (itemName != null){
            	itemName = itemName.replaceAll("[&]","&amp;")
            		.replaceAll("[<]","&lt;")
            		.replaceAll("[>]","&gt;");
            }
            
            map1.put("item", itemName);
            map1.put("itemId", itemIds.get(iX));
            
            map1.put("cost", itemCosts.get(iX));
            map1.put("units", itemUnits.get(iX));
            map1.put("v"+itemVisitIds.get(iX), "Y");    			
    			
            dataList.add(map1);
        } else {
            HashMap map1 = (HashMap)dataList.get(dataList.size()-1);
            if (((Integer)map1.get("itemId")).intValue() == ((Integer)itemIds.get(iX)).intValue()) {
                map1.put("v"+itemVisitIds.get(iX), "Y");
                dataList.set(dataList.size()-1, map1);
            } else {
                HashMap map2 = new HashMap();
                itemSeq++;
                map2.put("itemSeq", ""+itemSeq);
                
                int categoryId=0;          
                categoryId = EJBUtil.stringToNum((String)itemCategory.get(iX));
                String ddCategory = schcdao.toPullDown("costCat"+itemIds.get(iX), categoryId);
                //String ddCategory = schcdao.toJSON();
                ddCategory= schcdao.getCodeDescription(categoryId);
                map2.put("costCat", ddCategory);
                map2.put("costCatId", ""+categoryId);
                
                String itemName = (String)itemNames.get(iX);
                
              	//replace less than and greater than signs
                if (itemName != null){
                	itemName = itemName.replaceAll("[&]","&amp;")
                		.replaceAll("[<]","&lt;")
                		.replaceAll("[>]","&gt;");
                }
              	
                map2.put("item", itemName);
                map2.put("itemId", itemIds.get(iX));
                
                map2.put("cost", itemCosts.get(iX));
                map2.put("units", itemUnits.get(iX));
                map2.put("v"+itemVisitIds.get(iX), "Y");
                
                dataList.add(map2);
            }
        }
    }
    jsObj.put("itemCount",itemSeq);
    //dataList = eventassocB.sortListByKeyword(dataList, "item");
    
    JSONArray jsItemVisits = new JSONArray();
    for (int iX=0; iX<dataList.size(); iX++) {
        jsItemVisits.put(EJBUtil.hashMapToJSON((HashMap)dataList.get(iX)));
    }
    jsObj.put("dataArray", jsItemVisits);
    out.println(jsObj.toString());
    //System.out.println(jsObj.toString());
    // <html><head></head><body></body></html>
%>

<%! // Define Java functions here
private String createCheckbox(String itemId, String visitId, boolean checked) {
	StringBuffer sb = new StringBuffer();
	sb.append("<input type='checkbox' ");
	sb.append(" name='e").append(itemId).append("v").append(visitId).append("' ");
	sb.append(" id='e").append(itemId).append("v").append(visitId).append("' ");
	if (checked) {
		sb.append(" "+LC.L_Checked+" ");/*sb.append(" checked ");*****/
	}
	sb.append(" >");
	return sb.toString();
}
%>
