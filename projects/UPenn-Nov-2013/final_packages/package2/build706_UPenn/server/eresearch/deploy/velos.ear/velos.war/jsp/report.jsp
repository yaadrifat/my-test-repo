<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title><%=LC.L_Rpt_Output%><%--Report Output*****--%></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
body, layer { color: 000000; background-color: FFFFFF; }
body, td, center, p, div { font-family: arial; font-size: 10pt }
b { font-family: arial }
.sp { line-height: 2pt; }
.heading { font-weight: bold; font-family: arial; font-size: 10pt;}
.small { font-size: 8pt; font-family: arial }
.welcome { font-size: 12pt; font-family: arial; color: 000099;font-weight:bold }
.large { font-size: 12pt; }
.margin { margin-top: 2px; margin-bottom: 2px; }
.exc { padding-bottom: 3; padding-left: 3; padding-right: 3; padding-top: 5; }
.bluehr {color:"#330099"}
.evenrow{background-color:"#CCCCCC"}
.oddrow{background-color:"#FFFFFF"}
.labelheading { font-size: 11pt; font-family: arial; color: 000000 ;font-weight:bold;background-color:"#CCCCCC" }
</style>
</head>


<body style="overflow:scroll;">
<jsp:useBean id="reportsB" scope="session" class="com.velos.eres.web.reports.ReportsJB"/>
<jsp:useBean id="schReportsB" scope="session" class="com.velos.eres.web.eschReports.EschReportsJB"/>




<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import="com.velos.eres.web.reports.ReportsDao,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*"%>
<%
HttpSession tSession = request.getSession(true);
StringBuffer output = new StringBuffer();
boolean noDataFlag = false;
if (sessionmaint.isValidSession(tSession))

{
	String studyNo = (String) tSession.getValue("studyNo");

	int reportNumber = EJBUtil.stringToNum(request.getParameter("reportNumber"));
	String filterType = request.getParameter("filterType");
	String month = request.getParameter("month");
	String monthString = null;
	String year = null;
	if(filterType == null) filterType = "";
	if(filterType.equals("3")) {
		year = request.getParameter("year1");
		switch(EJBUtil.stringToNum(month)) {
			case(1):
				monthString = LC.L_January/*"January"*****/;
				break;
			case(2):
				monthString = LC.L_February/*"February"*****/;
				break;
			case(3):
				monthString = LC.L_March/*"March"*****/;
				break;
			case(4):
				monthString = LC.L_April/*"April"*****/;
				break;
			case(5):
				monthString = LC.L_May/*"May"*****/;
				break;
			case(6):
				monthString = LC.L_June/*"June"*****/;
				break;
			case(7):
				monthString = LC.L_July/*"July"*****/;
				break;
			case(8):
				monthString = LC.L_August/*"August"*****/;
				break;
			case(9):
				monthString = LC.L_September/*"September"*****/;
				break;
			case(10):
				monthString = LC.L_October/*"October"*****/;
				break;
			case(11):
				monthString = LC.L_November/*"November"*****/;
				break;
			case(12):
				monthString = LC.L_December/*"December"*****/;
				break;
		}
	} else {
		year = request.getParameter("year");
	}

	String dateFrom = request.getParameter("dateFrom");
	String dateTo = request.getParameter("dateTo");
	String id = request.getParameter("id");
	String accId = request.getParameter("accId");
	String val = request.getParameter("val");
	String protId=request.getParameter("protId");

//out.println("value is" + protId);


//	reportNumber = 18;  //uncommented to test
	int orderBy = 2;
	String order;
	String repHeading = MC.M_AllPat_AssocCst/*"All "+LC.Pat_Patients+ " Associated Cost"*****/;
	if(reportNumber==18)
	{
		order=request.getParameter("dispStyle");


		if(order.equals("P"))
		{
			orderBy = 1; //
		}
		if(!(id.equals("")))
		{
			orderBy = 3;
			repHeading = MC.M_PatAssoc_CstEvt/*LC.Pat_Patients+" Associated Cost (Done Events)"*****/;
		}
	}
//	String filterType = null;
//	String year = null;
//	String month = null;
//	String dateFrom = null;
//	String dateTo = null;
//	String id = "123";


ReportsDao reportsDao=new ReportsDao();
EschReportsDao schreportsDao =new EschReportsDao();
EschCostReportDao schcostreportDao =new EschCostReportDao();

if(reportNumber==17){
schreportsDao=schReportsB.fetchEschReportsData(reportNumber, filterType, year,    month, dateFrom, dateTo, protId , accId);
}
else if(reportNumber==18){
//out.println("inside 18");
Integer pId = new Integer(protId);
	if(orderBy == 3)
	{
		Integer patID = new Integer(id);
		schcostreportDao=schReportsB.getCostReport(patID.intValue(), 		pId.intValue(),orderBy);

	}
	else
	{
		schcostreportDao=schReportsB.getCostReport(reportNumber, pId.intValue(),orderBy);
	}
}

else
{
 reportsDao = reportsB.fetchReportsData(reportNumber, filterType, year, month, dateFrom, dateTo, id, accId);
}








output.append("<BR>");
//output.append("<H1 align='center'>");
//output.append("Velos eResearch Report");
//output.append("</H1>");


	ArrayList userLastNames;
	ArrayList userFirstNames;
	ArrayList organizationNames;
	ArrayList userJobTypes;
	ArrayList studyNumbers;
	ArrayList studyTitles;
	ArrayList tareas;
	ArrayList userTeamRoles;
	ArrayList userWrkExps;
	ArrayList userPhaseInvs;
	ArrayList userSpls;
	ArrayList userAdds;
	ArrayList userCitys;
	ArrayList userStates;
	ArrayList userZips;
	ArrayList userCountrys;
	ArrayList userPhones;
	ArrayList userEmails;
	ArrayList siteAdds;
	ArrayList siteCitys;
	ArrayList siteStates;
	ArrayList siteZips;
	ArrayList siteCountrys;
	ArrayList sitePhones;
	ArrayList siteEmails;
	ArrayList studyAuthors;
	ArrayList partCenters;
	ArrayList studyDataMgrs;
	ArrayList studyStats;
	ArrayList studyStatDates;
	ArrayList sectionNames;
	ArrayList sectionContents;
	ArrayList viewMods;
	ArrayList studyIds;
////scheduling reports..

ArrayList eventIds;
ArrayList eventNames;
ArrayList eventDurations;
ArrayList fuzzyPeriods;
ArrayList eventDisplacements;
ArrayList eventCosts;
ArrayList eventDocs;
ArrayList eventUsers;
ArrayList eventResources;





	String userLastName = null;
	String userFirstName = null;
	String organizationName = null;
	String userJobType = null;
	String studyNumber = null;
	String studyTitle = null;
	String tarea = null;
	String userTeamRole = null;
	String userWrkExp = null;
	String userPhaseInv = null;
	String userSpl = null;
	String userAdd = null;
	String userCity = null;
	String userState = null;
	String userZip = null;
	String userCountry = null;
	String userPhone = null;
	String userEmail = null;
	String siteAdd = null;
	String siteCity = null;
	String siteState = null;
	String siteZip = null;
	String siteCountry = null;
	String sitePhone = null;
	String siteEmail = null;
	String studyAuthor = null;
	String partCenter = null;
	String studyDataMgr = null;
	String studyStat = null;
	String studyStatDate = null;
	String sectionName = null;
	String sectionContent = null;
	String viewMod = null;
	String studyId = null;

////scheduling reports
String eventId;
String eventName;
String eventDuration;
String fuzzyPeriod;
String eventDisplacement;
String eventCost;
String eventDoc;
String eventUser;
String eventResource;
String protName;
String protDuration;



	String oldStudyId = null;

	int counter = 0;
	int rows = 0;
	int i = 0;

	switch(reportNumber) {
		case(1):
			userLastNames = reportsDao.getUserLastNames();
			userFirstNames = reportsDao.getUserFirstNames();
			userPhones = reportsDao.getUserPhones();
			userEmails = reportsDao.getUserEmails();
			organizationNames = reportsDao.getOrganizationNames();
			userJobTypes = reportsDao.getUserJobTypes();
			rows = reportsDao.getCRows();


output.append("<Form name='report' method=\"POST\">");
output.append("  <table width='100%' cellspacing='0' cellpadding='0' border=0 >");
output.append("    <tr > ");
output.append("      <td width = '100%'> ");
output.append("        <P class = 'welcome' align='left'> "+LC.L_All_AccUsers/*All Account Users*****/+" </P>");
output.append("      </td>");
output.append("    </tr>");
output.append("  </table>");
output.append("<hr size=5 class='bluehr'>");
			if(rows > 0) {


output.append("<table width='100%' cellspacing='0' cellpadding='0' border=0>");
output.append("  <tr> ");
output.append("    <th width='25%' class= 'heading'> "+LC.L_User_Name/*User Name*****/+" </th>");
output.append("    <th width='15%' class= 'heading'> "+LC.L_User_Phone/*User Phone*****/+" </th>");
output.append("    <th width='15%' class= 'heading'> "+LC.L_User_Email/*User Email*****/+" </th>");
output.append("    <th width='25%' class= 'heading'> "+LC.L_Organization/*Organization*****/+" </th>");
output.append("    <th width='20%' class= 'heading'> "+LC.L_Job_Type/*Job Type*****/+" </th>");
output.append("  </tr>");
			for(i = 0 ; i < rows ; i++)
			   {
				userLastName = ((userLastNames.get(i)) == null)?"-":userLastNames.get(i).toString();
				userFirstName = ((userFirstNames.get(i)) == null)?"-":userFirstNames.get(i).toString();
				userPhone = ((userPhones.get(i)) == null)?"-":userPhones.get(i).toString();
				userEmail = ((userEmails.get(i)) == null)?"-":userEmails.get(i).toString();
				organizationName = ((organizationNames.get(i)) == null)?"-":organizationNames.get(i).toString();
				userJobType = ((userJobTypes.get(i)) == null)?"-":userJobTypes.get(i).toString();
if((i%2) == 0)
{
output.append("<tr class='evenrow'>");
}
else
{
output.append("<tr  class='oddrow'>");
}

output.append(" <td width ='25%' align='left'>" + userLastName +",&nbsp;" + userFirstName + " </td>");
output.append(" <td width='15%' align='left'> " + userPhone + "</td>");
output.append(" <td width='15%' align='left'> " + userEmail + "</td>");
output.append(" <td width='25%' align='left'> " + organizationName + "</td>");
output.append(" <td width ='20%' align='left'>" + userJobType + "</td>");
output.append("</tr>");

			   } //end of for loop
output.append("</table>");
output.append("<hr size=5 class='bluehr'>");
			} else { //else for the if checking the value of rows
				noDataFlag = true;
			}

			break;
		case(2):
			userLastNames = reportsDao.getUserLastNames();
			userFirstNames = reportsDao.getUserFirstNames();
			organizationNames = reportsDao.getOrganizationNames();
			userJobTypes = reportsDao.getUserJobTypes();
			userWrkExps = reportsDao.getUserWrkExps();
			userPhaseInvs = reportsDao.getUserPhaseInvs();
			userSpls = reportsDao.getUserSpls();
			userAdds = reportsDao.getUserAdds();
			userCitys = reportsDao.getUserCitys();
			userStates = reportsDao.getUserStates();
			userZips = reportsDao.getUserZips();
			userCountrys = reportsDao.getUserCountrys();
			userPhones = reportsDao.getUserPhones();
			userEmails = reportsDao.getUserEmails();
			siteAdds = reportsDao.getSiteAdds();
			siteCitys = reportsDao.getSiteCitys();
			siteStates = reportsDao.getSiteStates();
			siteZips = reportsDao.getSiteZips();
			siteCountrys = reportsDao.getSiteCountrys();
			sitePhones = reportsDao.getSitePhones();
			siteEmails = reportsDao.getSiteEmails();
			rows = reportsDao.getCRows();


output.append("<Form name='report' method=\"POST\">");
output.append("  <table width='100%' cellspacing='0' cellpadding='0' border=0 >");
output.append("    <tr > ");
output.append("      <td width = '100%'> ");
output.append("        <P class = 'welcome' align='left'> "+LC.L_User_Profile/*User Profile*****/+" </P>");
output.append("      </td>");
output.append("    </tr>");
output.append("  </table>");
output.append("<hr size=5 class='bluehr'>");
			if(rows > 0) {
			   userLastName = ((userLastNames.get(0)) == null)?"-":userLastNames.get(0).toString();
			   userFirstName = ((userFirstNames.get(0)) == null)?"-":userFirstNames.get(0).toString();
			   organizationName = ((organizationNames.get(0)) == null)?"-":organizationNames.get(0).toString();
			   userJobType = ((userJobTypes.get(0)) == null)?"-":userJobTypes.get(0).toString();
			   userWrkExp = ((userWrkExps.get(0)) == null)?"-":userWrkExps.get(0).toString();
			   userPhaseInv = ((userPhaseInvs.get(0)) == null)?"-":userPhaseInvs.get(0).toString();
			   userSpl = ((userSpls.get(0)) == null)?"-":userSpls.get(0).toString();
			   userAdd = ((userAdds.get(0)) == null)?"-":userAdds.get(0).toString();
			   userCity = ((userCitys.get(0)) == null)?"-":userCitys.get(0).toString();
			   userState = ((userStates.get(0)) == null)?"-":userStates.get(0).toString();
			   userZip = ((userZips.get(0)) == null)?"-":userZips.get(0).toString();
			   userCountry = ((userCountrys.get(0)) == null)?"-":userCountrys.get(0).toString();
			   userPhone = ((userPhones.get(0)) == null)?"-":userPhones.get(0).toString();
			   userEmail = ((userEmails.get(0)) == null)?"-":userEmails.get(0).toString();
			   siteAdd = ((siteAdds.get(0)) == null)?"-":siteAdds.get(0).toString();
			   siteCity = ((siteCitys.get(0)) == null)?"-":siteCitys.get(0).toString();
			   siteState = ((siteStates.get(0)) == null)?"-":siteStates.get(0).toString();
			   siteZip = ((siteZips.get(0)) == null)?"-":siteZips.get(0).toString();
			   siteCountry = ((siteCountrys.get(0)) == null)?"-":siteCountrys.get(0).toString();
			   sitePhone = ((sitePhones.get(0)) == null)?"-":sitePhones.get(0).toString();
			   siteEmail = ((siteEmails.get(0)) == null)?"-":siteEmails.get(0).toString();

output.append("<table width='100%' cellspacing='0' cellpadding='0' border=0>");
output.append("  <tr> ");
output.append("    <td width='100%' class= 'labelheading'>"+LC.L_Member_Info/*Member Information*****/+"</td>");
output.append("  </tr>");
output.append("  </table>");
output.append("<hr size=2 class='bluehr'>");

output.append("<table width='100%' cellspacing='0' cellpadding='0' border=0>");
output.append("  <tr> ");
output.append("    <td width='40%' class= 'heading'>"+LC.L_Name/*Name****/+" </td>");
output.append("    <td width='60%'>" + userLastName + ",&nbsp;" + userFirstName + "</td>");
output.append("  </tr>");
output.append("  <tr>");
output.append("    <td  class= 'heading'> "+MC.M_ClinicalRes_Experience/*Clinical Research Experience (in years)*****/+"</td>");
output.append("    <td>" + userWrkExp + "</td>");
output.append("  </tr>");
output.append("  <tr>");
output.append("    <td class= 'heading'> "+MC.M_Involvement_InResPhases/*Involvement in Research Phases*****/+" </td>");
output.append("    <td>" + userPhaseInv + "</td>");
output.append("  </tr>");
output.append("  <tr>");
output.append("    <td class= 'heading'> "+LC.L_Job_Type/*Job Type*****/+" </td>");
output.append("    <td>" + userJobType + "</td>");
output.append("  </tr>");
output.append("  <tr>");
output.append("    <td class= 'heading'> "+LC.L_Speciality/*Speciality*****/+" </td>");
output.append("    <td> " + userSpl + "</td>");
output.append("  </tr>");
output.append("  </table>");

output.append("  <br>");

output.append("<table width='100%' cellspacing='0' cellpadding='0' border=0>");
output.append("  <tr> ");
output.append("    <td width='100%' class= 'labelheading'>"+LC.L_Contact_Info/*Contact Information*****/+"</td>");
output.append("  </tr>");
output.append("  </table>");
output.append("<hr size=2 class='bluehr'>");
output.append("  </table>");

output.append("<table width='100%' cellspacing='0' cellpadding='0' border=0>");
output.append("  <tr>");
output.append("    <td class= 'heading' width='40%'> "+LC.L_Address/*Address*****/+" </td>");
output.append("    <td> " + userAdd + "</td>");
output.append("  </tr>");
output.append("  <tr>");
output.append("    <td class= 'heading'> "+LC.L_City/*City*****/+" </td>");
output.append("    <td>" + userCity + "</td>");
output.append("  </tr>");
output.append("  <tr >");
output.append("    <td class= 'heading'> "+LC.L_State/*State*****/+" </td>");
output.append("    <td>" + userState + "</td>");
output.append("  </tr>");
output.append("  <tr >");
output.append("    <td class= 'heading'> "+LC.L_Zip/*Zip*****/+" </td>");
output.append("    <td>" + userZip + "</td>");
output.append("  </tr>");
output.append("  <tr>");
output.append("    <td class= 'heading'> "+LC.L_Country/*Country*****/+" </td>");
output.append("    <td>" + userCountry + "</td>");
output.append("  </tr>");
output.append("  <tr>");
output.append("    <td class= 'heading'> "+LC.L_Phone_Number/*Phone Number*****/+" </td>");
output.append("    <td>" + userPhone + "</td>");
output.append("  </tr>");
output.append("  <tr>");
output.append("    <td class= 'heading'> "+LC.L_Email/*Email*****/+" </td>");
output.append("    <td>" + userEmail + "</td>");
output.append("  </tr>");
output.append("  </table>");

output.append("  <br>");

output.append("<table width='100%' cellspacing='0' cellpadding='0' border=0>");
output.append("  <tr> ");
output.append("    <td width='100%' class= 'labelheading'>"+LC.L_Org_Info/*Organization Information*****/+"</td>");
output.append("  </tr>");
output.append("  </table>");
output.append("<hr size=2 class='bluehr'>");
output.append("  </table>");

output.append("<table width='100%' cellspacing='0' cellpadding='0' border=0>");
output.append("  <tr>");
output.append("    <td class= 'heading' width='40%'> "+LC.L_Name/*Name****/+" </td>");
output.append("    <td>" + organizationName + "</td>");
output.append("  </tr>");
output.append("  <tr>");
output.append("    <td class= 'heading'>"+LC.L_Address/*Address*****/+" </td>");
output.append("    <td>" + siteAdd + "</td>");
output.append("  </tr>");
output.append("  <tr>");
output.append("    <td class= 'heading'> "+LC.L_City/*City*****/+" </td>");
output.append("    <td>" + siteCity + "</td>");
output.append("  </tr>");
output.append("  <tr >");
output.append("    <td class= 'heading'> "+LC.L_State/*State*****/+" </td>");
output.append("    <td>" + siteState + "</td>");
output.append("  </tr>");
output.append("  <tr>");
output.append("    <td class= 'heading'> "+LC.L_Zip/*Zip*****/+" </td>");
output.append("    <td>" + siteZip + "</td>");
output.append("  </tr>");
output.append("  <tr>");
output.append("    <td class= 'heading'> "+LC.L_Country/*Country*****/+" </td>");
output.append("    <td>" + siteCountry + "</td>");
output.append("  </tr>");
output.append("  <tr>");
output.append("    <td class= 'heading'> "+LC.L_Phone/*Phone*****/+" </td>");
output.append("    <td>" + sitePhone + "</td>");
output.append("  </tr>");
output.append("  <tr>");
output.append("    <td class= 'heading'> "+LC.L_Email/*Email*****/+" </td>");
output.append("    <td>" + siteEmail + "</td>");
output.append("  </tr>");
output.append("</table>");
output.append("<hr size=5 class='bluehr'>");
				}
				break;
			case(3):
				studyNumbers = reportsDao.getStudyNumbers();
				studyTitles = reportsDao.getStudyTitles();
				tareas = reportsDao.getTareas();
				studyDataMgrs = reportsDao.getStudyDataMgrs();

				rows = reportsDao.getCRows();



output.append("<Form name='report' method=\"POST\">");
output.append("  <table width='100%' cellspacing='0' cellpadding='0' border=0 >");
output.append("    <tr > ");
output.append("      <td width = '30%'> ");
output.append("        <P class = 'welcome' align='left'> "+LC.L_Active_Pcols/*Active Protocols*****/+" </P>");
output.append("      </td>");

				if(!(filterType.equals("1"))) {
output.append("    <tr class='welcome' align = 'right'>");
output.append("    <td class = 'welcome'>"+LC.L_Selected_Filter/*Selected Filter*****/+": ");
					if(filterType.equals("2")) {
output.append("	"+LC.L_For_Year/*For Year*****/+" " + year);
					} else if(filterType.equals("3")){
output.append("	"+LC.L_For/*For*****/+" " + monthString + ", " + year);
					} else {
output.append("	"+LC.L_From/*From*****/+" " + dateFrom + " "+LC.L_To_Lower/*to*****/+" " + dateTo);
					}
output.append("   </B> </td>");
output.append("    </tr>");
				}
output.append("  </table>");
output.append("<hr size=5 class='bluehr'>");

				if(rows > 0) {

output.append("<table width='100%' cellspacing='0' cellpadding='0' border=0>");
output.append("  <tr> ");
output.append("    <th width='15%' align='center' class= 'heading'> "+LC.L_Number/*Number*****/+" </th>");
output.append("    <th width='50%' align='center' class= 'heading'> "+LC.L_Title/*Title*****/+" </th>");
output.append("    <th width='20%' align='center' class= 'heading'> "+LC.L_Therapeutic_Area/*Therapeutic Area*****/+" </th>");
output.append("    <th width='15%' align='center' class= 'heading'> "+LC.L_Author/*Author*****/+" </th>");
output.append("  </tr>");

				for(i = 0 ; i < rows ; i++)
				   {
					studyNumber = ((studyNumbers.get(i)) == null)?"-":studyNumbers.get(i).toString();
					studyTitle = ((studyTitles.get(i)) == null)?"-":studyTitles.get(i).toString();
					tarea = ((tareas.get(i)) == null)?"-":tareas.get(i).toString();
					studyDataMgr = ((studyDataMgrs.get(i)) == null)?"-":studyDataMgrs.get(i).toString();

if((i%2) == 0)
{
output.append("<tr class='evenrow'>");
}
else
{
output.append("<tr  class='oddrow'>");
}

output.append(" <td width ='15%' align='left'> " + studyNumber + "</td>");
output.append(" <td width='50%' align='left'> " + studyTitle + "</td>");
output.append(" <td width ='20%' align='left'> " + tarea + " </td>");
output.append(" <td width ='15%' align='left'> " + studyDataMgr + "</td>");
output.append("</tr>");


				   } //end of for loop

output.append("</table>");
output.append("<hr size=5 class='bluehr'>");

				} else {
					noDataFlag = true;
				}

				break;
			case(4):
				studyNumbers = reportsDao.getStudyNumbers();
				studyTitles = reportsDao.getStudyTitles();
				tareas = reportsDao.getTareas();
				studyAuthors = reportsDao.getStudyAuthors();
				rows = reportsDao.getCRows();

output.append("<Form name='report' method=\"POST\">");
output.append("  <table width='100%' cellspacing='0' cellpadding='0' border=0 >");
output.append("    <tr > ");
output.append("      <td width = '100%' class = 'welcome' align='left'> ");
output.append("        "+LC.L_Public_Pcol/*Public Protocols"*****/);
output.append("      </td>");
output.append("    </tr>");
output.append("  </table>");
output.append("<hr size=5 class='bluehr'>");
  				if(rows > 0) {

output.append("<table width='100%' cellspacing='0' cellpadding='0' border=0>");
output.append("  <tr> ");
output.append("    <th width='15%' align='center' class = 'heading'> "+LC.L_Number/*Number*****/+" </th>");
output.append("    <th width='50%' align='center' class = 'heading'> "+LC.L_Title/*Title*****/+" </th>");
output.append("    <th width='20%' align='center' class = 'heading'> "+LC.L_Therapeutic_Area/*Therapeutic Area*****/+" </th>");
output.append("    <th width='15%' align='center' class = 'heading'> "+LC.L_Data_Manager/*Data Manager*****/+" </th>");
output.append("  </tr>");

				for(i = 0 ; i < rows ; i++)
				   {
					studyNumber = ((studyNumbers.get(i)) == null)?"-":studyNumbers.get(i).toString();
					studyTitle = ((studyTitles.get(i)) == null)?"-":studyTitles.get(i).toString();
					tarea = ((tareas.get(i)) == null)?"-":tareas.get(i).toString();
					studyAuthor = ((studyAuthors.get(i)) == null)?"-":studyAuthors.get(i).toString();

if((i%2) == 0)
{
output.append("<tr class='evenrow'>");
}
else
{
output.append("<tr  class='oddrow'>");
}

output.append(" <td width ='15%' align='left'> " + studyNumber + "</td>");
output.append(" <td width='50%' align='left'> " + studyTitle + "</td>");
output.append(" <td width ='20%' align='left'> " + tarea + " </td>");
output.append(" <td width ='15%' align='left'> " + studyAuthor + "</td>");
output.append("</tr>");


				   } //end of for loop

output.append("</table>");
output.append("<hr size=5 class='bluehr'>");

				} else {
					noDataFlag = true;
				}

				break;
			case(5):

				userLastNames = reportsDao.getUserLastNames();
				userFirstNames = reportsDao.getUserFirstNames();
				studyNumbers = reportsDao.getStudyNumbers();
				studyTitles = reportsDao.getStudyTitles();
				tareas = reportsDao.getTareas();
				userTeamRoles = reportsDao.getUserTeamRoles();

				rows = reportsDao.getCRows();

output.append("<Form name='report' method=\"POST\">");
output.append("  <table width='100%' cellspacing='0' cellpadding='0' border=0 >");
output.append("    <tr > ");
output.append("      <td width = '40%'> ");
output.append("        <P class = 'welcome'  align='left'> "+LC.L_Pcol_ByUsr/*Protocols By User*****/+": "+ val+"</P>");
output.append("      </td>");

if(!(filterType.equals("1"))) {

output.append("    <td class='welcome' align='right'>"+LC.L_Selected_Filter/*Selected Filter*****/+":");
					if(filterType.equals("2")) {
output.append("	"+LC.L_For_Year/*For Year*****/+" " + year);
					} else if(filterType.equals("3")){
output.append("	"+LC.L_For/*For*****/+" " + monthString + ", " + year);
					} else {
output.append("	"+LC.L_From/*From*****/+" " + dateFrom + " "+LC.L_To_Lower/*to*****/+" " + dateTo);
					}
output.append("   </B> </td>");
output.append("    </tr>");
				}
output.append("  </table>");
output.append("<hr size=5 class='bluehr'>");

if(rows > 0) {

output.append("<table width='100%' cellspacing='0' cellpadding='0' border=0>");
output.append("  <tr> ");
output.append("    <th width='15%' class= 'heading'> "+LC.L_Number/*Number*****/+" </th>");
output.append("    <th width='50%' class= 'heading'> "+LC.L_Title/*Title*****/+" </th>");
output.append("    <th width='20%' class= 'heading'> "+LC.L_Therapeutic_Area/*Therapeutic Area*****/+" </th>");
output.append("    <th width='15%' class= 'heading'> "+LC.L_Team_Role/*Team Role*****/+" </th>");
output.append("  </tr>");

				for(i = 0 ; i < rows ; i++)
				   {
					studyNumber = ((studyNumbers.get(i)) == null)?"-":studyNumbers.get(i).toString();
					studyTitle = ((studyTitles.get(i)) == null)?"-":studyTitles.get(i).toString();
					tarea = ((tareas.get(i)) == null)?"-":tareas.get(i).toString();
					userTeamRole = ((userTeamRoles.get(i)) == null)?"-":userTeamRoles.get(i).toString();

if((i%2) == 0)
{
output.append("<tr class='evenrow'>");
}
else
{
output.append("<tr  class='oddrow'>");
}

output.append(" <td width ='20%' align='left'> " + studyNumber + "</td>");
output.append(" <td width='40%' align='left'> " + studyTitle + "</td>");
output.append(" <td width ='20%' align='left'> " + tarea + " </td>");
output.append(" <td width ='20%' align='left'> " + userTeamRole + "</td>");
output.append("</tr>");


			   } //end of for loop

output.append("</table>");
output.append("<hr size=5 class='bluehr'>");
				} else {
					noDataFlag = true;
				}// end of if for checking the rows
				break;
			case(6):
				studyNumbers = reportsDao.getStudyNumbers();
				studyTitles = reportsDao.getStudyTitles();
				tareas = reportsDao.getTareas();
				partCenters = reportsDao.getPartCenters();
				studyAuthors = reportsDao.getStudyAuthors();

				rows = reportsDao.getCRows();



output.append("<Form name='report' method=\"POST\">");
output.append("  <table width='100%' cellspacing='0' cellpadding='0' border=0 >");
output.append("    <tr > ");
output.append("      <td width = '40%'> ");
output.append("        <P class = 'welcome' align='left'> "+MC.M_Pcol_ByThpticArea/*Protocols by Therapeutic Area*****/+": " +val+ " </P>");
output.append("      </td>");
				if(!(filterType.equals("1"))) {

output.append("    <td class = 'welcome' align = 'right'>"+LC.L_Selected_Filter/*Selected Filter*****/+": ");
					if(filterType.equals("2")) {
output.append("	"+LC.L_For_Year/*For Year*****/+" " + year);
					} else if(filterType.equals("3")){
output.append("	"+LC.L_For/*For*****/+" " + monthString + ", " + year);
					} else {
output.append("	"+LC.L_From/*From*****/+" " + dateFrom + " "+LC.L_To_Lower/*to*****/+" " + dateTo);
					}
output.append("   </td>");
output.append("    </tr>");
				}

output.append("  </table>");
output.append("<hr size=5 class='bluehr'>");

				if(rows > 0) {


output.append("<table width='100%' cellspacing='0' cellpadding='0' border=0>");
output.append("  <tr> ");
output.append("    <th width='15%' align='center' class= 'heading'> "+LC.L_Study_Number/*Study Number*****/+" </th>");
output.append("    <th width='40%' align='center' class= 'heading'> "+LC.L_Study_Title/*Study Title*****/+" </th>");
output.append("    <th width='20%' align='center' class= 'heading'> "+LC.L_Std_DataManager/*Study Data Manager*****/+" </th>");
output.append("    <th width='25%' align='center' class= 'heading'> "+LC.L_Organization/*Organization*****/+" </th>");
output.append("  </tr>");



				for(i = 0 ; i < rows ; i++)
				   {
					studyNumber = ((studyNumbers.get(i)) == null)?"-":studyNumbers.get(i).toString();
					studyTitle = ((studyTitles.get(i)) == null)?"-":studyTitles.get(i).toString();
					partCenter = ((partCenters.get(i)) == null)?"-":partCenters.get(i).toString();
					studyAuthor = ((studyAuthors.get(i)) == null)?"-":studyAuthors.get(i).toString();


if((i%2) == 0)
{
output.append("<tr class='evenrow'>");
}
else
{
output.append("<tr  class='oddrow'>");
}
output.append(" <td width ='15%' align='left'> " + studyNumber + "</td>");
output.append(" <td width='40%' align='left'> " + studyTitle + "</td>");
output.append(" <td width ='20%' align='left'> " + studyAuthor + "</td>");
output.append(" <td width ='25%' align='left'> " + partCenter + " </td>");
output.append("</tr>");
				   } //end of for loop
output.append("</table>");
output.append("<hr size=5 class='bluehr'>");
				} else {
					noDataFlag = true;
				}// end of if for checking the rows
				break;
			case(7):
				userLastNames = reportsDao.getUserLastNames();
				userFirstNames = reportsDao.getUserFirstNames();
				studyIds = reportsDao.getStudyIds();
				studyTitles = reportsDao.getStudyTitles();
				studyNumbers = reportsDao.getStudyNumbers();
				studyStats = reportsDao.getStudyStats();
				tareas = reportsDao.getTareas();
				userTeamRoles = reportsDao.getUserTeamRoles();


				rows = reportsDao.getCRows();




output.append("<Form name='report' method=\"POST\">");
output.append("  <table width='100%' cellspacing='0' cellpadding='0' border=0 >");
output.append("    <tr> ");
output.append("      <td width = '50%' align='left'> ");
output.append("        <P class = 'welcome'> "+MC.M_Pcol_ByOrg/*Protocols by Organization*****/+" </P>");
output.append("      </td>");
output.append("    <td align = 'right' class='welcome'>");
output.append(" "+LC.L_Organization_Name/*Organization Name*****/+": " + val);
output.append("    </td>");
output.append("    </tr>");
output.append("  </table>");

output.append("<hr size=5 class='bluehr'>");

				if(rows > 0){


output.append("<table width='100%' cellspacing='0' cellpadding='0' border=0>");
output.append("  <tr> ");
output.append("    <th width='15%' align='center' class= 'heading'> "+LC.L_Number/*Number*****/+" </th>");
output.append("    <th width='30%' align='center' class= 'heading'> "+LC.L_Title/*Title*****/+" </th>");
output.append("    <th width='10%' align='center' class= 'heading'> "+LC.L_Status/*Status*****/+" </th>");
output.append("    <th width='20%' align='center' class= 'heading'> "+LC.L_Therapeutic_Area/*Therapeutic Area*****/+" </th>");
output.append("    <th width='15%' align='center' class= 'heading'> "+LC.L_Std_Team/*Study Team*****/+" </th>");
output.append("    <th width='10%' align='center' class= 'heading'> "+LC.L_Role/*Role*****/+" </th>");
output.append("  </tr>");
				for(i = 0 ; i < rows ; i++)
				   {
					userLastName = ((userLastNames.get(i)) == null)?"-":userLastNames.get(i).toString();
					userFirstName = ((userFirstNames.get(i)) == null)?"-":userFirstNames.get(i).toString();
					studyId = studyIds.get(i).toString();
					studyNumber = ((studyNumbers.get(i)) == null)?"-":studyNumbers.get(i).toString();
					studyTitle = ((studyTitles.get(i)) == null)?"-":studyTitles.get(i).toString();
					studyStat = ((studyStats.get(i)) == null)?"-":studyStats.get(i).toString();
					tarea = ((tareas.get(i)) == null)?"-":tareas.get(i).toString();
					userTeamRole = ((userTeamRoles.get(i)) == null)?"-":userTeamRoles.get(i).toString();


if((i%2) == 0)
{
output.append("<tr class='evenrow'>");
}
else
{
output.append("<tr  class='oddrow'>");
}

if(studyId.equals(oldStudyId)) {
   output.append(" <td width ='15%' align='left'> </td>");
   output.append(" <td width='30%' align='left'> </td>");
   output.append(" <td width='10%' align='left'> </td>");
   output.append(" <td width ='20%' align='left'> </td>");
} else {
   output.append(" <td width ='15%' align='left'> " + studyNumber + "</td>");
   output.append(" <td width='30%' align='left'> " + studyTitle + "</td>");
   output.append(" <td width='10%' align='left'> " + studyStat + "</td>");
   output.append(" <td width ='20%' align='left'> " + tarea + " </td>");
   oldStudyId = studyId;
}
output.append(" <td width ='15%' align='left'> " + userLastName + ",&nbsp;" + userFirstName + "</td>");
output.append(" <td width='10%' align='left'> " + userTeamRole + "</td>");
output.append("</tr>");


				   } //end of for loop

output.append("</table>");
output.append("<hr size=5 class='bluehr'>");

				} else {
					noDataFlag = true;
				}
				break;
			case(8):

output.append(MC.M_RptYet_NotReady/*"This report is not yet ready"*****/);
				break;
			case(9):
				sectionNames = reportsDao.getSectionNames();
				sectionContents = reportsDao.getSectionContents();

				rows = reportsDao.getCRows();




output.append("<Form name='report' method=\"POST\">");
output.append("  <table width='100%' cellspacing='0' cellpadding='0' border=0 >");
output.append("    <tr > ");
output.append("      <td width = '100%'> ");
output.append("        <P class = 'sectionHeadings' align='center'> "+LC.L_Pcol_Summary/*Protocol Summary*****/+" </P>");
output.append("      </td>");
output.append("    </tr>");
output.append("    <tr>");
output.append("	<td>");
output.append("	<BR>");
output.append("	</td>");
output.append("    </tr>");
output.append("    <tr>");
output.append("    <td><b>");
output.append("	"+MC.M_For_StdNum/*For Study Number*****/+" " + val);
output.append("    </b></td>");
output.append("    </tr>");
output.append("  </table>");
output.append("<BR>");

output.append("<table width='100%' cellspacing='0' cellpadding='0' border=0>");
output.append("  <tr width='100%'> ");
output.append("	    <th width='30%' align='left'> "+LC.L_Sec_Name/*Section Name*****/+" </th>");
output.append("	    <th width='2%' align='left'> </th>");
output.append("	    <th width='68%' align='left'> "+LC.L_Sec_Contents/*Section Contents*****/+" </th>");
output.append("  </tr>");


				for(i = 0 ; i < rows ; i++)
				   {
					sectionName = ((sectionNames.get(i)) == null)?"-":sectionNames.get(i).toString();
					sectionContent = ((sectionContents.get(i)) == null)?"-":sectionContents.get(i).toString();


output.append("  <tr> ");
output.append("    <td width='30%' align='left'> " + sectionName + " </td>");
output.append("    <td width='2%' align='left'> </td>");
output.append("    <td width='68%' align='left'> " + sectionContent + " </td>");
output.append("  </tr>");


				   } //end of for loop

output.append("</table>");

				break;
			case(10):
				userLastNames = reportsDao.getUserLastNames();
				userFirstNames = reportsDao.getUserFirstNames();
				userTeamRoles = reportsDao.getUserTeamRoles();
				userPhones = reportsDao.getUserPhones();
				userEmails = reportsDao.getUserEmails();
				organizationNames = reportsDao.getOrganizationNames();

				rows = reportsDao.getCRows();




output.append("<Form name='report' method=\"POST\">");
output.append("  <table width='100%' cellspacing='0' cellpadding='0' border=0 >");
output.append("    <tr > ");
output.append("      <td width = '30%'> ");
output.append("        <P class = 'welcome' align='left'> "+LC.L_Std_Team/*Study Team*****/+" </P>");
output.append("      </td>");
output.append("    <td class='welcome' align='right'>");
output.append("	"+MC.M_For_StdNum/*For Study Number*****/+": " + val);
output.append("    </td>");
output.append("    </tr>");
output.append("  </table>");
output.append("<hr size=5 class='bluehr'>");
				if(rows > 0) {

output.append("<table width='100%' cellspacing='0' cellpadding='0' border=0>");
output.append("  <tr> ");
output.append("    <th width='20%' align='left'> "+LC.L_Name/*Name****/+" </th>");
output.append("    <th width='20%' align='left'> "+LC.L_Organization_Name/*Organization Name*****/+" </th>");
output.append("    <th width='20%' align='left'> "+LC.L_Phone/*Phone*****/+" </th>");
output.append("    <th width='20%' align='left'> "+LC.L_Email/*Email*****/+" </th>");
output.append("    <th width='20%' align='left'> "+LC.L_Role/*Role*****/+" </th>");
output.append("  </tr>");


				for(i = 0 ; i < rows ; i++)
				   {
					userLastName = ((userLastNames.get(i)) == null)?"-":userLastNames.get(i).toString();
					userFirstName = ((userFirstNames.get(i)) == null)?"-":userFirstNames.get(i).toString();
					userTeamRole = ((userTeamRoles.get(i)) == null)?"-":userTeamRoles.get(i).toString();
					organizationName = ((organizationNames.get(i)) == null)?"-":organizationNames.get(i).toString();
					userPhone = ((userPhones.get(i)) == null)?"-":userPhones.get(i).toString();
					userEmail = ((userEmails.get(i)) == null)?"-":userEmails.get(i).toString();


if((i%2) == 0)
{
output.append("<tr class='evenrow'>");
}
else
{
output.append("<tr  class='oddrow'>");
}

output.append(" <td width ='20%' align='left'> " + userLastName + ",&nbsp;" + userFirstName + " </td>");
output.append(" <td width ='20%' align='left'> " + organizationName +" </td>");
output.append(" <td width='20%' align='left'> " + userPhone + " </td>");
output.append(" <td width ='20%' align='left'> " + userEmail + " </td>");
output.append(" <td width ='20%' align='left'> " + userTeamRole + "</td>");
output.append("</tr>");


				   } //end of for loop

output.append("</table>");
output.append("<hr size=5 class='bluehr'>");
				} else {
					noDataFlag = true;
				}
				break;
			case(11):
				userLastNames = reportsDao.getUserLastNames();
				userFirstNames = reportsDao.getUserFirstNames();
				userTeamRoles = reportsDao.getUserTeamRoles();
				organizationNames = reportsDao.getOrganizationNames();
				userPhones = reportsDao.getUserPhones();
				userEmails = reportsDao.getUserEmails();
				viewMods = reportsDao.getViewMods();

				rows = reportsDao.getCRows();


output.append("<Form name='report' method=\"POST\">");
output.append("  <table width='100%' cellspacing='0' cellpadding='0' border=0 >");
output.append("    <tr > ");
output.append("      <td width = '100%'> ");
output.append("        <P class = 'sectionHeadings' align='center'> "+MC.M_Assign_ExtrUsr/*Assigned External Users*****/+" </P>");
output.append("      </td>");
output.append("    </tr>");
output.append("    <tr>");
output.append("	<td>");
output.append("	<BR>");
output.append("	</td>");
output.append("    </tr>");
output.append("    <tr>");
output.append("    <td><b>");
output.append("	"+MC.M_For_StdNum/*For Study Number*****/+" " + val);
output.append("    </b></td>");
output.append("    </tr>");
output.append("  </table>");

				if(rows > 0) {


output.append("<table width='100%' cellspacing='0' cellpadding='0' border=0>");
output.append("  <tr> ");
output.append("    <th width='20%' align='left'> "+LC.L_User_Name/*User Name*****/+" </th>");
output.append("    <th width='20%' align='left'> "+LC.L_Team_Role/*Team Role*****/+" </th>");
output.append("    <th width='25%' align='left'> "+LC.L_Organization_Name/*Organization Name*****/+" </th>");
output.append("    <th width='10%' align='left'> "+LC.L_Phone/*Phone*****/+" </th>");
output.append("    <th width='10%' align='left'> "+LC.L_Email/*Email*****/+" </th>");
output.append("    <th width='15%' align='left'> "+LC.L_Assigned_Right/*Assigned Right*****/+" </th>");
output.append("  </tr>");


				for(i = 0 ; i < rows ; i++)
				   {
					userLastName = ((userLastNames.get(i)) == null)?"-":userLastNames.get(i).toString();
					userFirstName = ((userFirstNames.get(i)) == null)?"-":userFirstNames.get(i).toString();
					userTeamRole = ((userTeamRoles.get(i)) == null)?"-":userTeamRoles.get(i).toString();
					organizationName = ((organizationNames.get(i)) == null)?"-":organizationNames.get(i).toString();
					userPhone = ((userPhones.get(i)) == null)?"-":userPhones.get(i).toString();
					userEmail = ((userEmails.get(i)) == null)?"-":userEmails.get(i).toString();
					viewMod = ((viewMods.get(i)).equals("V"))?"View":"Modify";


output.append("<tr>");
output.append(" <td width ='20%' align='left'> " + userLastName + ",&nbsp;" + userFirstName + " </td>");
output.append(" <td width='20%' align='left'> " + userTeamRole + " </td>");
output.append(" <td width='25%' align='left'> " + organizationName + " </td>");
output.append(" <td width='10%' align='left'> " + userPhone + " </td>");
output.append(" <td width='10%' align='left'> " + userEmail + " </td>");
output.append(" <td width='15%' align='left'> " + viewMod + " </td>");
output.append("</tr>");


				   } //end of for loop

output.append("</table>");
				} else {
					noDataFlag = true;
				}
				break;
			case(12):
				studyStats = reportsDao.getStudyStats();
				studyStatDates = reportsDao.getStudyStatDates();

				rows = reportsDao.getCRows();




output.append("<Form name='report' method=\"POST\">");
output.append("  <table width='100%' cellspacing='0' cellpadding='0' border=0 >");
output.append("    <tr > ");
output.append("      <td width = '30%'> ");
output.append("        <P class = 'welcome' align='left'> "+LC.L_Protocol_Tracking/*Protocol Tracking*****/+" </P>");
output.append("      </td>");
output.append("    <td class='welcome' align='right'>");
output.append("	"+MC.M_For_StdNum/*For Study Number*****/+": " + val);
output.append(" </td>");
output.append("    </tr>");
output.append("  </table>");
output.append("<hr size=5 class='bluehr'>");
				if(rows > 0) {


output.append("<table width='100%' cellspacing='0' cellpadding='0' border=0>");
output.append("  <tr> ");
output.append("    <th width='40%' align='center' class= 'heading'> "+LC.L_Status/*Status*****/+" </th>");
output.append("    <th width='60%' align='center' class= 'heading'> "+LC.L_Date/*Date*****/+" </th>");
output.append("  </tr>");


				for(i = 0 ; i < rows ; i++)
				   {
					studyStat = ((studyStats.get(i)) == null)?"-":studyStats.get(i).toString();
					studyStatDate = ((studyStatDates.get(i)) == null)?"-":studyStatDates.get(i).toString();


if((i%2) == 0)
{
output.append("<tr class='evenrow'>");
}
else
{
output.append("<tr  class='oddrow'>");
}

output.append(" <td width ='40%' align='left'> " + studyStat + " </td>");
output.append(" <td width='60%' align='left'> " + studyStatDate + " </td>");
output.append("</tr>");


				   } //end of for loop

output.append("</table>");
output.append("<hr size=5 class='bluehr'>");
				} else {
					noDataFlag = true;
				}

				break;
	case(17): 			//meant for protocol template
				eventIds = schreportsDao.getEventIds();
				eventNames = schreportsDao.getNames();
				eventDurations = schreportsDao.getDurations();
				fuzzyPeriods = schreportsDao.getFuzzyPeriods();
				eventDisplacements = schreportsDao.getDisplacements();
				eventCosts= schreportsDao.getCosts();
				eventDocs= schreportsDao.getDocNames();
				eventUsers= schreportsDao.getUsers();
				eventResources = schreportsDao.getResources();
				protName=schreportsDao.getProtName();
				protDuration=schreportsDao.getProtDuration();
				int rowCount=1;
				Integer duration=new Integer(protDuration);
								if(eventNames.size()==0){
					rows=0;}
					else{
					rows=eventNames.size();
					}


             output.append("  <table width='100%' cellspacing='0' cellpadding='0' border=0 >");
             output.append("    <tr > ");
             output.append("      <td width = '30%'> ");
             output.append("        <P class = 'welcome' align='left'> "+LC.L_Pcol_CalTemplate/*Protocol Calendar Template*****/+" </P>");
             output.append("      </td>");
		 output.append("    </tr>");
             output.append("  </table>");
      	 output.append("<hr size=5 class='bluehr'>");


             output.append("<Form name='report' method=\"POST\">");
             output.append("  <table width='100%' cellspacing='0' cellpadding='0' border=0 >");
             output.append("    <tr > ");
             output.append("      <td width = '30%'> ");
             output.append("        <P class = 'welcome' align='left'> "+MC.M_For_StdNum/*For Study Number*****/+": " + val + "</P>");
             output.append("      </td>");
             output.append("    <td class='welcome' align='right'>");
             output.append("	"+LC.L_Pcol_CalTitle/*Protocol Calender Title*****/+": " + protName);
             output.append("    </td>");
             output.append("    </tr>");
             output.append("  </table>");
             //output.append("<hr size=5 class='bluehr'>");
             //output.append("	For Study Number&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp &nbsp&nbsp  " + request.getParameter("studyPk") + "&nbsp&nbsp&nbsp&nbsp&nbsp Protocol Calender Title :&nbsp&nbsp &nbsp&nbsp " + request.getParameter("selProtocol"));
		output.append("<br></br>");

				if(rows > 0) {


output.append("<table width='100%' cellspacing='0' cellpadding='3' border=0>");
output.append("  <tr> ");
output.append("    <th width='05%' align='center'> "+LC.L_Week/*Week*****/+" </th>");
output.append("    <th width='05%' align='center'> "+LC.L_Day/*Day*****/+" </th>");
output.append("    <th width='20%' align='center'> "+LC.L_Event/*Event*****/+"</th>");
output.append("    <th width='05%' align='center'> "+LC.L_Duration/*Duration*****/+" </th>");
output.append("    <th width='10%' align='center'> "+LC.L_Fuzzy_Period/*Fuzzy Period*****/+" </th>");
output.append("    <th width='10%' align='center'> "+LC.L_Associated_Costs/*Associated Costs*****/+" </th>");
output.append("    <th width='20%' align='center'> "+LC.L_Associated_Resources/*Associated Resources*****/+"</th>");
output.append("    <th width='20%' align='center'> "+LC.L_Associated_Forms_S/*Associated Forms(s)*****/+" </th>");
output.append("  </tr>");
output.append("<tr bgColor=#fafad2> <td > "+LC.L_Week1/*week1*****/+" </td><td> </td> <td> </td> <td> </td> <td> </td> <td> </td> <td> </td> <td> </td>  </tr> ");


				for(i = 0 ; i < rows ; i++)
				   {


eventId= ((eventIds.get(i)) == null)?"-":eventIds.get(i).toString();
eventName = ((eventNames.get(i)) == null)?"-":eventNames.get(i).toString();
eventDuration = ((eventDurations.get(i)) == null)?"-":eventDurations.get(i).toString();


fuzzyPeriod = ((fuzzyPeriods.get(i)) == null)?"-":fuzzyPeriods.get(i).toString();

if(fuzzyPeriod.startsWith("+"))
fuzzyPeriod=fuzzyPeriod.substring(1);

if(fuzzyPeriod.startsWith("-"))
fuzzyPeriod=fuzzyPeriod.substring(1);


eventDisplacement = ((eventDisplacements.get(i)) == null)?"-":eventDisplacements.get(i).toString();
eventCost = ((eventCosts.get(i)) == null)?"-  ":eventCosts.get(i).toString();
eventDoc = ((eventDocs.get(i)) == null)?"- ":eventDocs.get(i).toString();
eventUser = ((eventUsers.get(i)) == null)?"-":eventUsers.get(i).toString();
eventResource = ((eventResources.get(i)) == null)?"-":eventResources.get(i).toString();



if(eventUser.equals("-"))
{
eventUser="";
eventUser=eventUser+eventResource;
}
else
{
eventUser=eventUser+ ", " +eventResource;
}
eventDuration+=LC.L_Day/*"day"*****/;


output.append("<tr>");
output.append(" <td width='05%' align='center'> " + "" + " </td>");
Integer temp=new Integer(eventDisplacement);
int printval=temp.intValue();
if(printval%7==0)
{
printval=7;
}
else
{
printval=printval%7;
}
output.append(" <td width='05%' align='center' valign=top> " + printval +"</td>");

output.append(" <td width='20%' align='center' valign=top> " + eventName + " </td>");
output.append(" <td width='05%' align='center' valign=top> " + eventDuration+ " </td>");
output.append(" <td width='10%' align='center' valign=top> " + fuzzyPeriod+ " </td>");
output.append(" <td width='10%' align='center' valign=top> " + eventCost + " </td>");
output.append(" <td width='20%' align='center' valign=top> " + eventUser +" </td>");
output.append(" <td width='20%' align='center' valign=top> " + eventDoc + " </td>");
output.append("</tr>");
Integer displacement=new Integer(eventDisplacement);

rowCount=displacement.intValue();

	if((rowCount%7==0)&& (rowCount!=duration.intValue()))
			{
			output.append("<tr bgColor=#fafad2 ><td> "+LC.L_Week/*Week*****/+" " + (rowCount/7+1) + " </td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>");
		}


if(i<(rows-1))
{
	Integer first=new Integer(eventDisplacement);
	Integer second=new Integer(eventDisplacements.get(i+1).toString());

	int diff=second.intValue()-first.intValue();

		 if(diff>1)
		  {
			for(int k=1;k<diff;k++)
			{
			output.append("<tr>");
			output.append(" <td width='05%' align='center'> " + "" + " </td>");
			output.append(" <td width='05%' align='center'> " + (rowCount%7 + 1) + " 			</td>");
			output.append(" <td width='20%' align='center'> " + "-" + " </td>");
			output.append(" <td width='05%' align='center'> " + "-"+ " 			</td>");
			output.append(" <td width='20%' align='center'> " + "-" + " </td>");
			output.append(" <td width='40%' align='center'> " + "-" + " </td>");
			output.append(" <td width='45%' align='center'> " + "-" +" </td>");
			output.append(" <td width='50%' align='center'> " + "-" + " </td>");
			output.append("</tr>");
			rowCount++;
			if((rowCount%7)==0)
			{
			output.append("<tr bgColor=#fafad2 ><td > "+LC.L_Week/*Week*****/+" "+ (rowCount/7+1) + "</td><td> </td> <td> </td> <td> </td> <td> </td> <td> </td> <td> </td> <td> </td> </tr> ");
		}


			}//end of for
		  } //end of if
} //end of if


} //end of for loop

/////////////////add rest of rows.....

int test=duration.intValue()-rowCount;
	for(int p=1;p<=test;p++)
			{
			output.append("<tr>");
			output.append(" <td width='05%' align='center'> " + "" + " </td>");
			output.append(" <td width='05%' align='center'> " + (rowCount%7 + 1) + " 			</td>");
			output.append(" <td width='20%' align='center'> " + "-" + " </td>");
			output.append(" <td width='05%' align='center'> " + "-"+ " 			</td>");
			output.append(" <td width='20%' align='center'> " + "-" + " </td>");
			output.append(" <td width='40%' align='center'> " + "-" + " </td>");
			output.append(" <td width='45%' align='center'> " + "-" +" </td>");
			output.append(" <td width='50%' align='center'> " + "-" + " 			</td>");
			output.append("</tr>");
			rowCount++;
			if(((rowCount%7)==0)&&(duration.intValue()!=rowCount))
			{
			output.append("<tr bgColor=#fafad2 ><td > "+LC.L_Week/*Week*****/+" "+  ((rowCount/7)+1) + "</td><td> </td> <td> </td> <td> </td> <td> </td> <td> </td> <td> </td> <td> </td> </tr> ");
		}


			}//end of for
/////////////////////////////end of insertions.








output.append("</table>");
				} else {
					noDataFlag = true;
				}
				break;

	case(18): 			//meant for protocol template

			if(schcostreportDao.descriptions.size()==0){
					rows=0;}
					else{
					rows=schcostreportDao.descriptions.size();
					}
//rows=1;

             output.append("<Form name='report' method=\"POST\">");
             output.append("  <table width='100%' cellspacing='0' cellpadding='0' border=0 >");
             output.append("    <tr > ");
             output.append("      <td width = '30%' colspan=2> ");
             output.append("        <P class = 'welcome' align='left'> " +repHeading );
             output.append("<hr size=5 class='bluehr'></P>");
             output.append("      </td>");
             output.append("    <tr><td class='heading' align='left'>");
             output.append("	"+LC.L_Study_Number/*Study Number*****/+": " + studyNo);
             output.append("    </td>");
			 output.append("    <td class='heading' align='right'>");
             output.append("	"+LC.L_Protocol_CalTitle/*Protocol Calendar Title*****/+": " + request.getParameter("selProtocol"));
			 output.append("    </td>");
			 output.append("    </tr>");
             output.append("  </table> <BR>");

            /* output.append("<Form name='report'>");
            output.append("  <table width='100%' cellspacing='0' cellpadding='0' border=0 >");
            output.append("    <tr > ");
            output.append("      <td width = '100%'> ");
            output.append("        <P class = 'sectionHeadings' align='center'> Protocol Calender Template </P>");
            output.append("      </td>");
            output.append("    </tr>");
            output.append("    <tr>");
            output.append("	<td>");
            output.append("	<BR>");
            output.append("	</td>");
            output.append("    </tr>");
            output.append("    <tr>");
            output.append("    <td><b>");
            output.append("	For Study Number&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp &nbsp&nbsp  " + request.getParameter("studyPk") + "&nbsp&nbsp&nbsp&nbsp&nbsp Protocol Calender Title :&nbsp&nbsp &nbsp&nbsp " + request.getParameter("selProtocol"));
            output.append("    </b></td>");
            output.append("    </tr>");
            output.append("  </table>");
			*/

if(rows > 0) {


output.append("<table width='100%' cellspacing='0' cellpadding='0' border=0>");
output.append("  <tr> ");
if(orderBy == 2)
{
	output.append("    <th width='05%' align='center'> "+LC.L_Patient_Id/*Patient ID*****/+" </th>");
}

output.append("    <th width='15%' align='center'> "+LC.L_Event/*Event*****/+" </th>");
output.append("    <th width='05%' align='center'> "+LC.L_Month/*Month*****/+" </th>");
output.append("    <th width='20%' align='center'> "+LC.L_Actual_Date/*Actual Date*****/+"</th>");
output.append("    <th width='20%' align='center'> "+LC.L_Scheduled_Date/*Scheduled Date*****/+"</th>");
output.append("    <th width='20%' align='center'> "+LC.L_Associated_Costs/*Associated Costs*****/+" </th>");
output.append("    <th width='20%' align='center'> "+LC.L_Cost_Description/*Cost Description*****/+"</th>");
output.append("  </tr><tr height=10></tr>");
String patientPrev="";
String monthPrev="";
if(rows>0)
{
	patientPrev=(String)schcostreportDao.patient_ids.get(0);
	monthPrev=(String)schcostreportDao.months.get(0);


}

int flg=0;
float subTotal=0;
float grandTotal = 0;
for(i = 0 ; i < rows ; i++)
{
if(orderBy == 1 || orderBy == 3)
{
	if(i==0)
	{
	//bgColor=#fafad2
		output.append("<tr class='evenrow'>");
		output.append(" <td  width='20%' align='left' colspan=6> " + LC.L_Patient_Id_Upper/*"PATIENT ID*****/+": "+ 			(String)schcostreportDao.per_codes.get(i) + " </td>");
		output.append("</tr>");


	}
	if(!(patientPrev.equals((String)schcostreportDao.patient_ids.get(i))))
	{
		output.append("<tr>");
		output.append("<td> </td><td> </td><td> </td><td> </td><td width='20%' align='center'> <B>" + LC.L_Sub_Total/*"Sub Total*****/+": </B>"+"$"+subTotal + " </td>");
		output.append("</tr>");
		if(i<schcostreportDao.sub_totals.size())
		{
			subTotal=((Float)schcostreportDao.sub_totals.get(i)).floatValue();
		}

		output.append("<tr class='evenrow' >");
		output.append(" <td width='20%' align='left' colspan=6> " + LC.L_Patient_Id_Upper/*"PATIENT ID*****/+": "+ 			(String)schcostreportDao.per_codes.get(i) + " </td>");
		output.append("</tr>");
		patientPrev=(String)schcostreportDao.patient_ids.get(i);
		flg=1;

	}
	else
	{
		if(i<schcostreportDao.sub_totals.size())
		{
			subTotal +=  ((Float)schcostreportDao.sub_totals.get(i)).floatValue();
		}
//		patientPrev=(String)schcostreportDao.patient_ids.get(i);
		flg=0;

	}
}

if(orderBy == 2)
{
	if(i==0)
	{
		output.append("<tr class='evenrow'>");
		output.append(" <td  width='20%' align='left' colspan=6> " + LC.L_Month/*"Month*****/+" : "+ 			(String)schcostreportDao.months.get(i) + " </td>");
		output.append("</tr>");


	}
	if(!(monthPrev.equals((String)schcostreportDao.months.get(i))))
	{
		output.append("<tr>");
		output.append("<td> </td><td> </td><td> </td><td> </td><td> </td> <td width='20%' align='center'> <B>" + LC.L_Sub_Total/*"Sub Total*****/+": </B>"+"$"+subTotal + " </td>");
		output.append("</tr>");
		if(i<schcostreportDao.sub_totals.size())
		{
			subTotal=((Float)schcostreportDao.sub_totals.get(i)).floatValue();
		}

		output.append("<tr class='evenrow' >");
		output.append(" <td width='20%' align='left' colspan=6> " + LC.L_Month/*"Month*****/+" : "+ 			(String)schcostreportDao.months.get(i) + " </td>");
		output.append("</tr>");
		monthPrev=(String)schcostreportDao.months.get(i);
		flg=1;

	}
	else
	{
		if(i<schcostreportDao.sub_totals.size())
		{
			subTotal +=  ((Float)schcostreportDao.sub_totals.get(i)).floatValue();
		}
		flg=0;

	}
}



output.append("<tr class='oddrow' >");
if(orderBy == 2)
{
	output.append(" <td width='20%' align='center' > " + 		(String)schcostreportDao.per_codes.get(i) + " </td>");

}
String dt = (String)schcostreportDao.event_exeons.get(i);
String actualDt = dt.toString().substring(5,7) + "/" + dt.toString().substring(8,10) + "/" + dt.toString().substring(0,4);
dt = (String)schcostreportDao.start_date_times.get(i);
String schDt = dt.toString().substring(5,7) + "/" + dt.toString().substring(8,10) + "/" + dt.toString().substring(0,4);

output.append(" <td width='05%' align='center' > " + (String)schcostreportDao.descriptions.get(i) + " </td>");

output.append(" <td width='05%' align='center' > " + (String)schcostreportDao.months.get(i) + " </td>");

output.append(" <td width='20%' align='center' > " + actualDt + " </td>");
output.append(" <td width='20%' align='center' > " + schDt + " </td>");
if(i<schcostreportDao.eventcost_values1.size())
{
	output.append(" <td width='05%' align='center' > " + (String)schcostreportDao.eventcost_values1.get(i)+ " </td>");
}
else
{
	output.append(" <td width='05%' align='center' > " + "---" + " </td>");
}

if(i<schcostreportDao.eventcost_descs1.size())
{
	output.append(" <td width='20%' align='center' > " + (String)schcostreportDao.eventcost_descs1.get(i)+ " </td>");
}
else
{
	output.append(" <td width='05%' align='center' > " + "---" + " </td>");
}

output.append("</tr>");
//subTotal +=  ((Float)schcostreportDao.sub_totals.get(i)).floatValue();
if(i<schcostreportDao.sub_totals.size())
{
	grandTotal +=  ((Float)schcostreportDao.sub_totals.get(i)).floatValue();
}

if(i ==(rows-1))
{
	if(orderBy == 1 || orderBy == 3)
	{

		output.append("<tr >");
		output.append("<td> </td><td> </td><td> </td><td> </td> <td width='20%' align='center'><B> " + LC.L_Sub_Total/*"Sub Total*****/+": </B>"+"$"+subTotal + " </td>");
		output.append("</tr>");
		subTotal = 0;
		flg=0;



		output.append("<tr >");
		output.append("<td> </td><td> </td><td> </td><td> </td> <td width='05%' align='center'> <B>" + LC.L_Grand_Total/*"Grand Total*****/+":</B> "+ 			grandTotal + " </td>");
		output.append("</tr>");
	}
	else
	{
		output.append("<tr>");
		output.append("<td> </td><td> </td><td> </td><td> </td><td> </td> <td width='20%' 		align='center'> <B>" + LC.L_Sub_Total/*"Sub Total*****/+":</B> "+"$"+subTotal + " </td>");
		output.append("</tr>");
		subTotal = 0;
		flg=0;



		output.append("<tr >");
		output.append("<td> </td><td> </td><td> </td><td> </td><td> </td> <td width='05%' 		align='center'> <B>" + LC.L_Grand_Total/*"Grand Total*****/+": </B>"+ grandTotal + " </td>");
		output.append("</tr>");

	}

}


				   } //end of for loop

output.append("</table>");
				} else {
					noDataFlag = true;
				}
				break;

              		default:
	} //end of switch statement
} else {   //end of if body for session
%>
<jsp:include page='timeout.html' flush='true'/>
<%
}
output.append("</form>");
if(noDataFlag) {

output.append("<BR><BR><BR><BR><BR><BR>");
output.append("<table width='100%'>");
output.append("<tr width='100%'>");
output.append("<td align='center' width='100%'>");
output.append("<B>"+MC.M_NoDataAval_ForSelOpt/*No data is available for the selected option(s)*****/+"</B>");
output.append("</td>");
output.append("</tr>");
output.append("</table>");

}

%>
<%

String contents1, contents2, contents3, Str, styleTag;
contents1 = null;
contents2 = null;
contents3 = null;
Str = null;
styleTag = null;
Str = output.toString();


styleTag = "<style> body, layer { color: 000000; background-color: FFFFFF; } body, td, center, p, div { font-family: arial; font-size: 10pt } " +
" b { font-family: arial } .sp { line-height: 2pt; } .heading { font-weight: bold; font-family: arial; font-size: 10pt;} " +
".small { font-size: 8pt; font-family: arial } .welcome { font-size: 12pt; font-family: arial; color: 000099;font-weight:bold }" +
".large { font-size: 12pt; } .margin { margin-top: 2px; margin-bottom: 2px; }" +
".exc { padding-bottom: 3; padding-left: 3; padding-right: 3; padding-top: 5; }" +
".bluehr {color:#330099} .evenrow{background-color:#CCCCCC} .oddrow{background-color:#FFFFFF}" +
" .labelheading { font-size: 11pt; font-family: arial; color: 000000 ;font-weight:bold;background-color:#CCCCCC }" +
"</style>";

ReportIO.STYLE_TAG = styleTag;
contents1 = ReportIO.saveReportToDoc(Str,"doc","reportword");
contents2 = ReportIO.saveReportToDoc(Str,"xls","reportexcel");
contents3 = ReportIO.saveReportToDoc(Str,"htm","reporthtm");



if(!noDataFlag) {

%>
<br>
<form METHOD="POST">
<table width='100%' class="evenrow">
<tr>
<%/* SV, 8/24/04, fix for bug 1673, _new is not a keyword, _blank is and it opens new window instance each time which is what we want for reports.*/%>


<td> <%=MC.M_Download_ReportIn%><%--Download the report in*****--%>: <A href="<%=contents1%>" target="_blank"><%=LC.L_Word_Format%><%--Word Format*****--%></A>&nbsp;<A href="<%=contents2%>" target="_blank"><%=LC.L_Excel_Format%><%--Excel Format*****--%></A> &nbsp;<A href="<%=contents3%>" target="_blank"><%=LC.L_Printer_FriendlyFormat%><%--Printer Friendly Format*****--%></A>
</td>
<td>
<!--<input type=button value='Print' onClick='window.print()'>-->
</td>
</table>
</form>

<%
} //end of if for noDataFlag
%>

<%=output.toString()%>
</body>

</html>
