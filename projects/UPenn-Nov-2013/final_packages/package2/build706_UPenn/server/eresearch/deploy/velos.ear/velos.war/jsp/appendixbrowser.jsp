<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/> 
<HTML>
<HEAD>
<%
boolean isIrb = "irb_upload_tab".equals(request.getParameter("selectedTab")) ? true : false; 
if (isIrb) {
%>
<title><%=MC.M_ResCompApp_UploadDoc %><%-- Research Compliance >> New Application >> Upload Documents*****--%></title>
<% } else { %>
<TITLE><%=LC.L_Study_Appendix %><%-- <%=LC.Std_Study%> >> Appendix*****--%></TITLE>
<% } %>

 

</HEAD>

<% String src;

src= request.getParameter("srcmenu");
String from = "appendixver";

%>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include> 

<SCRIPT language="JavaScript1.1">

var screenWidth = screen.width;
var screenHeight = screen.height;

	function openWin() {

      windowName = window.open("","Information","toolbar=yes,location=yes,scrollbars=yes,resizable=yes,menubar=yes,width=600,height=300")
	windowName.focus();

;}


function fdownload(formobj,pk,filename,dnldurl)
{
	formobj.file.value = filename;
	formobj.pkValue.value = pk ;
	formobj.dnldurl.value=dnldurl;
	formobj.moduleName.value="Study";
	formobj.action="postFileDownload.jsp";
	//formobj.action=dnldurl;
	
	formobj.target = "_filedownload";
	formobj.method = "POST";
	formobj.submit();
}

</SCRIPT> 



<SCRIPT language="JavaScript1.1">

	function openWin1() {

      windowName=window.open("#","Information","toolbar=no,location=no,scrollbars=yes,resizable=yes,menubar=no,width=600,height=300")
	windowName.focus();

;}

</SCRIPT>

 

<SCRIPT language="javascript">

function confirmBox(fileName,pgRight) {
	if (f_check_perm(pgRight,'E') == true) {
		var paramArray = [decodeString(fileName)];
		if (confirm(getLocalizedMessageString("L_Del_FrmApdx",paramArray))) {/*if (confirm("Delete " + decodeString(fileName) + " from Appendix?")) {*****/
		    return true;
		} else	{
			return false;
		}
	} else {
		return false;
	}		
}

</SCRIPT> 

  
<body>
<jsp:useBean id="studyVerB" scope="request" class="com.velos.eres.web.studyVer.StudyVerJB"/>
<jsp:useBean id="appendixB" scope="request" class="com.velos.eres.web.appendix.AppendixJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="statHistoryB" scope="request" class="com.velos.eres.web.statusHistory.StatusHistoryJB"/>
<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.web.studyRights.StudyRightsJB"%>
<BR>
<DIV class="BrowserTopn" id="div1"> 

  <%

   HttpSession tSession = request.getSession(true); 

   if (sessionmaint.isValidSession(tSession))

	{
       String includeTabsJsp = isIrb ? "irbnewtabs.jsp" : "studytabs.jsp";
	%>
		  <jsp:include page="<%=includeTabsJsp%>" flush="true">
		  <jsp:param name="from" value="<%=from%>"/> 
	  </jsp:include>
	</DIV>
<!-- Bug#15424 Fix for 1360*768 resolution- Tarun Kumar -->	
<SCRIPT LANGUAGE="JavaScript">
	if(screenWidth>1280 || screenHeight>1024)
		document.write('<DIV class="BrowserBotN  BrowserBotN_S_3" id = "div1" style = "height:76%;">')
	else
		document.write('<DIV class="BrowserBotN  BrowserBotN_S_3" id = "div1">')
</SCRIPT>


		<%

	String uName = (String) tSession.getValue("userName");

	String sessStudyId = (String) tSession.getValue("studyId");

	String studyNo = (String) tSession.getValue("studyNo");
	int studyVerId = EJBUtil.stringToNum(request.getParameter("studyVerId"));
	studyVerB.setStudyVerId(studyVerId);
	studyVerB.getStudyVerDetails();
	String verStatus = "";//studyVerB.getStudyVerStatus();
	String verNumber = studyVerB.getStudyVerNumber();


	verStatus = statHistoryB.getLatestStatus(studyVerId,"er_studyver");
	if(verStatus == null)
		verStatus = "W";

	String tab="";

      tab = request.getParameter("selectedTab");

	String space = request.getParameter("outOfSpace");
	space = (space==null)?"":space.toString();

	String incorrectFile = request.getParameter("incorrectFile");
	incorrectFile = (incorrectFile==null)?"":incorrectFile.toString();		
	if(space.equals("1")) {
%>
<br><br><br><br>
<table width="100%" cellspacing="0" cellpadding="0" border="0" class="basetbl">
<tr>
<td align=center>
<p class = "sectionHeadings">
<%=MC.M_UploadSpace_ContAdmin%><%-- The space allotted to you for file uploading has exhausted. Please contact Velos Administrator.*****--%> 
</p>
</td>
</tr>

<tr height=20></tr>
<tr>
<td align=center>

		<button onclick="window.history.go(-2);"><%=LC.L_Back%></button>
</td>		
</tr>		
</table>		


<%		
	} else if(incorrectFile.equals("1")) {
%>
<br><br><br><br>
<table width="100%" cellspacing="0" cellpadding="0" border="0" class="basetbl">
<tr>
<td align=center>

<p class = "sectionHeadings">

<%=MC.M_FileUpldEmpty_IncorrPath%><%-- The file that you are trying to upload is either empty or the path specified is incorrect.*****--%>  

</p>
</td>
</tr>

<tr height=20></tr>
<tr>
<td align=center>

		<button onclick="window.history.go(-1);"><%=LC.L_Back%></button>
</td>		
</tr>		
</table>		


<%		
	} else { //else of if for incorrectFile.equals("1")


	if(sessStudyId == "" || sessStudyId == null) {

%>
  <jsp:include page="studyDoesNotExist.jsp" flush="true"/>
  <%

   } else {   		
	   int studyId = EJBUtil.stringToNum(sessStudyId);
   	   StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");	 
	   int pageRight =0;	
   	   if ((stdRights.getFtrRights().size()) == 0){
	 	pageRight= 0;
	   }else{
		pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYAPNDX"));
   	   }
    	   if (pageRight > 0 )
   	   {

		int stId=EJBUtil.stringToNum(sessStudyId);
		AppendixDao appendixDao =new AppendixDao();
		appendixDao=appendixB.getByStudyIdAndType(studyVerId, "url");

		ArrayList appendixIds = appendixDao.getAppendixIds(); 
		ArrayList appendixDescriptions = appendixDao.getAppendixDescriptions();
		ArrayList appendixTypes = appendixDao.getAppendixTypes();
		ArrayList appendixFile_Uris = appendixDao.getAppendixFile_Uris();
		
		ArrayList appendixFiles = appendixDao.getAppendixFiles();

		ArrayList appendixStudyIds = appendixDao.getAppendixStudyIds();
		
		ArrayList appendixStudyVers = appendixDao.getAppendixStudyVers();
		
		ArrayList appendixPubFlags =appendixDao.getAppendixPubFlags();
		


		String appendixId=null;

		int appId;
	
		String appendixDescription=null;

		String appendixType=null;

		String appendixFile_Uri = null;

		String appendixFile = null;

		String appendixStudy = null;
		
		String appendixStudyVer = null;		

		String appendixPubFlag = null;	

		int len = appendixIds.size();
	   	int counter = 0;
		Object temp;
		
	%>
	<p class="defComments"><%=LC.L_Version_Number%><%-- Version Number*****--%>: <%=verNumber%> &nbsp;&nbsp;
	<%				
	 if (verStatus.equals("F")) {
	 %>
      <FONT class="Mandatory"><%=MC.M_CntChg_InFreezeVer%><%-- You cannot make any changes in a Freeze Version.*****--%></Font>
	 <%}%>
	</p>
  <Form name="apendixbrowser" method="post" action="" onsubmit="">
    <table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl midalign" >
      <tr > 
        <td width = "100%"> 
          <P class = "defComments"><%=MC.M_PcolAppx_AsFollows%><%-- Protocol Appendix is as follows*****--%>:</P>
        </td>
      </tr>
      <tr> 
        <th width="100%" > <%=LC.L_My_Links%><%-- My Links*****--%> </th>
      </tr>
      <tr height=5> 
        <td> </td>
      </tr>
      <tr> 
        <td> 
          <P class = "defComments"> <%=MC.M_YouCanList_ImplLnkUrl%><%-- You can list your important links. Give full 
            path of your URLs.*****--%> </P>
        </td>
      </tr>
	  <% if (!(verStatus.equals("F"))) {%>
      <tr> 
        <td>
          <P class = "defComments"> 
		  <A href="appendix_url.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&studyVerId=<%=studyVerId%>" onClick="return f_check_perm(<%=pageRight%>,'N')"><%=MC.M_ClickHere_AddNewLink%><%--CLICK HERE</A> to add a new Link.*****--%> 
		  </P>		  
        </td>
      </tr>
	  <%}%>
      <tr> 
        <td> 
          <P class = "defComments"> <%=MC.M_EditLnkDet_ClkNameOpen%><%-- Select [Edit] against the name of a link to 
            edit its details. Click on the name of the link to open it in a new 
            browser window.*****--%> </P>
        </td>
      </tr>
      <tr height=5> 
        <td> </td>
      </tr>
    </table>
    <table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl outline midalign">
	<th width="30%"><%=LC.L_Url_Upper%><%-- URL*****--%></th>
	<th width="60%"><%=LC.L_Description%><%-- Description*****--%></th>
	<th width="5%"><%=LC.L_Edit%><%-- Edit*****--%></th>
	<th width="5%"><%=LC.L_Delete%><%-- Delete*****--%></th>
      <%

	for(counter = 0;counter<len;counter++)
	{	
		temp=appendixIds.get(counter);			

		if (!(temp ==null))

			appendixId=temp.toString();

			appId=EJBUtil.stringToNum(appendixId); 	

		temp=appendixDescriptions.get(counter);			

		if (!(temp ==null))

			appendixDescription=temp.toString();

		temp=appendixFile_Uris.get(counter);	

		if (!(temp ==null))

			appendixFile_Uri = temp.toString();

		temp=appendixStudyVers.get(counter);

		if (!(temp ==null))	

			appendixStudyVer = temp.toString();

		temp=appendixPubFlags.get(counter);

		if (!(temp ==null))	

			appendixPubFlag = temp.toString(); 			 

	

		if ((counter%2)==0) {

		%>
      <tr class="browserEvenRow"> 
        <%

			}

		else{

		%>
      <tr class="browserOddRow"> 
        <%

			}

		%>
        <td width="30%"> 
		<A href="<%=appendixFile_Uri%>" target="Information" onClick="openWin()"><%=appendixFile_Uri%></A>		 
        </td>
		<td width="60%"><%=appendixDescription%></td>
        <td width="5%">
 	    <% if (!(verStatus.equals("F"))) {%> 
			<A href="appendix_url_edit.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&appId=<%=appId%>&studyVerId=<%=studyVerId%>" onClick="return f_check_perm(<%=pageRight%>,'E')"><img src="./images/edit.gif" title="<%=LC.L_Edit%>" border="0" /></A>
		<%}%> 
        </td>
	
		 <td width="5%" align="center">
	    <% if (!(verStatus.equals("F"))) {%>
	     <!-- Changed By PK Bug #4017 --> 
		 <A href="appendix_file_delete.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&appId=<%=appId%>&studyVerId=<%=studyVerId%>" onClick="return confirmBox('<%= StringUtil.encodeString(appendixFile_Uri) %>',<%=pageRight%>);" ><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0" /></A>
		<%}%> 
        </td>
      </tr>
      <%			

		

		}

	%>
    </table>
    <%

	AppendixDao appendixDao1 =new AppendixDao();

	appendixDao1=appendixB.getByStudyIdAndType(studyVerId, "file");

	ArrayList appendixIds1 = appendixDao1.getAppendixIds(); 

	ArrayList appendixDescriptions1 = appendixDao1.getAppendixDescriptions();


	ArrayList appendixTypes1 = appendixDao1.getAppendixTypes();

	ArrayList appendixFile_Uris1 = appendixDao1.getAppendixFile_Uris();
	
	ArrayList appendixFiles1 = appendixDao1.getAppendixFiles();
	

	ArrayList appendixStudyIds1 = appendixDao1.getAppendixStudyIds();

	ArrayList appendixStudyVers1 = appendixDao1.getAppendixStudyVers();
	
	ArrayList appendixPubFlags1 =appendixDao1.getAppendixPubFlags();



	String appendixId1=null;

	String appendixDescription1=null;

	String appendixType1=null;

	String appendixFile_Uri1 = null;

	String appendixFile1 = null;

	String appendixStudy1 = null;
	String appendixStudyVer1 = null;
	String appendixPubFlag1 = null;	

	

	int len1 = appendixIds1.size();



   	int counter1 = 0;



	Object temp1;

%>
    <BR>
    <table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl outline midalign" >
      <tr> 
        <th width="100%" > <%=LC.L_My_Files%><%-- My Files*****--%> </th>
      </tr>
      <tr height=5> 
        <td> </td>
      </tr>
      <tr> 
        <td> 
          <P class = "defComments"> <%=MC.M_AttachDocu_WithPcol%><%-- You can attach your important documents/forms 
            with this protocol.*****--%> </P>
        </td>
      </tr>
	    <% if (!(verStatus.equals("F"))) {%>
      <tr> 
        <td> 
          <P class = "defComments"> 
		  <A href="appendix_file.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&studyVerId=<%=studyVerId%>" onClick="return f_check_perm(<%=pageRight%>,'N')"><%=MC.M_ClickHere_UploadNewFile%><%--CLICK HERE </A> to upload a new File.*****--%> 
		  </P>
        </td>
      </tr>
	  <%}%>
      <tr> 
        <td> 
          <P class = "defComments"> <%=MC.M_SelDel_RemAppx_ClkFile%><%-- Select [Delete] against the name of a file 
            to remove it from appendix. Click on the file name to view it in a 
            new browser window.*****--%> </P>
        </td>
      </tr>
    </table>
    <table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl outline midalign">
	<th width="30%"><%=LC.L_File_Name%><%-- File name*****--%></th>
	<th width="60%"><%=LC.L_Description%><%-- Description*****--%></th>
	<th width="5%"><%=LC.L_Edit%><%-- Edit*****--%></th>
	<th width="5%"><%=LC.L_Delete%><%-- Delete*****--%></th>
      <%

	for(counter1 = 0;counter1<len1;counter1++)

	{	

		temp1=appendixIds1.get(counter1);			

		if (!(temp1 ==null))

			appendixId1=temp1.toString();

			appId=EJBUtil.stringToNum(appendixId1);	

		temp1=appendixDescriptions1.get(counter1);			

		if (!(temp1 ==null))

			appendixDescription1=temp1.toString();

		temp1=appendixFile_Uris1.get(counter1);	

		if (!(temp1 ==null))

			appendixFile_Uri1 = temp1.toString();

		temp1=appendixFiles1.get(counter1);	

		if (!(temp1 ==null))

			appendixFile1 = temp1.toString();	

		temp1=appendixStudyVers1.get(counter1);

		if (!(temp1 ==null))	

			appendixStudyVer1 = temp1.toString();

		temp1=appendixPubFlags1.get(counter1);

		if (!(temp1 ==null))	

			appendixPubFlag1 = temp1.toString(); 			 

	

		if ((counter1%2)==0) {

		%>
      <tr class="browserEvenRow"> 
        <%

			}

		else{

		%>
      <tr class="browserOddRow"> 
        <%

			}

		%>
        <td width=30%> 
          <% 	String dnld;
	com.aithent.file.uploadDownload.Configuration.readSettings("eres");
	com.aithent.file.uploadDownload.Configuration.readUploadDownloadParam(com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD + "fileUploadDownload.xml", "study");		  
	dnld=com.aithent.file.uploadDownload.Configuration.DOWNLOADSERVLET ;
	//SV, 7/27/04, fix for bug #1201, where a file with embedded # sign will cause browser to look for a HTML section tag. 
	//Fix: Encode the file name here and servlets decodes it later. 
	String modDnld = dnld + "?file=" + StringUtil.encodeString(appendixFile_Uri1) ;	
	%>

          <A href="#" onClick="fdownload(document.apendixbrowser,<%=appId%>,'<%=appendixFile_Uri1%>','<%=dnld%>');return false;" >
          
            
          
          <%=appendixFile_Uri1%>
           
          </A> </td>
		<td width="60%"><%=appendixDescription1%></td>
  		<td width=5%>
		  <% if (!(verStatus.equals("F"))) {%> 
		<A href="appendix_file_edit.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&appndxId=<%=appId%>&studyVerId=<%=studyVerId%>" onClick="return f_check_perm(<%=pageRight%>,'E')" ><img src="./images/edit.gif" title="<%=LC.L_Edit %>" border="0"/></A>
		<%}%> 
        </td>  
        <td width=5%  align="center">
		  <% if (!(verStatus.equals("F"))) {%>
		 <!-- BUG #4844 Fixed By Prabhat --> 
		<A href="appendix_file_delete.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&appId=<%=appId%>&studyVerId=<%=studyVerId%>" onClick="return confirmBox('<%=appendixFile_Uri1%>',<%=pageRight%>);" ><img src="./images/delete.gif" title="<%=LC.L_Delete %>" border="0"/></A>
		 <%}%>
        </td>
      </tr>
      <%			
		}
	%>
    </table>
    <input type="hidden" name="studyId" value=<%=sessStudyId%>>
    
    
    <input type="hidden" name="tableName" value="ER_STUDYAPNDX">
    <input type="hidden" name="columnName" value="STUDYAPNDX_FILEOBJ">
    <input type="hidden" name="pkColumnName" value="PK_STUDYAPNDX">
    <input type="hidden" name="module" value="study">
    <input type="hidden" name="db" value="eres">
    <input type="hidden" name="pkValue" value="">
    <input type="hidden" name="file" value="">
    <input type="hidden" name="dnldurl" value="">
    <input type="hidden" name="moduleName" value="">
    
    
  </FORM>
<%
	} //end of if body for page right
	else
	{
%>
	  <jsp:include page="accessdenied.jsp" flush="true"/>
 <%	} //end of else body for page right

}//end of else
}//end of if for space.equals("1")
}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<div class ="mainMenu" id="emenu"> 
  	<jsp:include page="getmenu.jsp" flush="true"/>
</div>
</BODY>



</HTML>

<%--dnld ="http://deepali:8080/appendixweb/servlet/Download/" + appendixFile1+"";JNDINames.ERESEARCH_CONF_PATH--%>
