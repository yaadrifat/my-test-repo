<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<BODY>
<jsp:useBean id ="userSiteB" scope="request" class="com.velos.eres.web.userSite.UserSiteJB"/>
<jsp:useBean id ="stdSiteB" scope="request" class="com.velos.eres.web.stdSiteRights.StdSiteRightsJB"/>
<jsp:useBean id ="userB" scope="page" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.common.UserSiteDao,java.util.*,com.velos.eres.service.util.*"%>
<%

int ret = 0;
String rights[] = null;
String pkUserSites[] = null;
int totrows =0; 

String eSign = request.getParameter("eSign");
String calledFrom  = request.getParameter("calledFrom");
if(calledFrom == null)
	calledFrom = "";

HttpSession tSession = request.getSession(true); 
if (sessionmaint.isValidSession(tSession))  {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%   
  	String oldESign = (String) tSession.getValue("eSign");
	if(!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} else {
	
 String ipAdd = (String) tSession.getValue("ipAdd");
 int usr = EJBUtil.stringToNum(request.getParameter("userId"));
 String userSiteFlag = request.getParameter("siteFlag");
 String userIdSess = (String) tSession.getValue("userId");
 
 String cdrpfRight = request.getParameter("cdrpfRight");
 
 if(!calledFrom.equals("S")){
 userB.setUserId(usr);
 userB.getUserDetails();
 userB.setUserSiteFlag(userSiteFlag);
 userB.updateUser();
 }
 totrows = Integer.parseInt(request.getParameter("totalrows")); //Total Rows
if (totrows > 1){
	pkUserSites = request.getParameterValues("pkUserSite");
	rights = request.getParameterValues("rights");
}else {
    pkUserSites = new String[1];
    rights = new String[1];
	pkUserSites[0] = request.getParameter("pkUserSite");
	rights[0] = request.getParameter("rights");
}

//pass array of pkUserSites and array of corresponding rights

	if(calledFrom.equals("S")){    
			ret = stdSiteB.updateStudySite(pkUserSites,null, rights,0,usr,ipAdd,userIdSess); 
	}
	else{
 	    //KM-#3634
		ret = userSiteB.updateUserSite(pkUserSites,rights,EJBUtil.stringToNum(userIdSess),usr,ipAdd); 
	}
%>
<br>
<br>
<br>
<br>
<br>
<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%> </p>

<%if (cdrpfRight.compareTo("1") == 0){ %>
<META HTTP-EQUIV=Refresh CONTENT="2; URL=manageUserSitesGrps.jsp?userId=<%=usr%>" >
<%}else{%>
<script>
setTimeout("self.close()",1000); //wait for sometime for showing  msg then close
</script>
<%}
}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
<%
}
%>

</BODY>

</HTML>





