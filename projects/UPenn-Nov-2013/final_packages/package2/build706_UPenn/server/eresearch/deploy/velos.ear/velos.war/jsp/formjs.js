var IncorrEsign_EtrAgain = M_IncorrEsign_EtrAgain;
var IncorrUsrName_EtrAgain = M_IncorrUsrName_EtrAgain;
var NoRgtTo_SetStatus = M_NoRgtTo_SetStatus;
var Sbmt_CurrFrmFirst=M_Sbmt_CurrFrmFirst;
var Disab_InPrvMode=M_Disab_InPrvMode;
var FrmStatNotAllow_WantProc=M_FrmStatNotAllow_WantProc;
var calledFromPreview = false;
var calledFromSchedule = false; //for CRF forms
var targetVar = "";
var v_displayvalflag = 1;
 
var fkStorageForSpecimen = "";

var fk_sch_events1 = ''; //for CRF forms

  	function updateField2(fld1,fld1Val,fld2,fld2Val)
	{
		var fld1text ;
		if (fld1.type == 'select-one')
		{
			fld1text = fld1.options[fld1.selectedIndex].text;
		}
		else if (fld1.type == 'text')
		{
			fld1text = fld1.value;
		}	
		if (fld1text == fld1Val) 
		{
			if (fld2.type == 'select-one')
			{			
						for (i=0;i<fld2.options.length;i++) 
							{
								if (fld2.options[i].text ==	fld2Val) 
								{
									break;
								}
							fld2.selectedIndex = i+1;
						 	}
		 	}
			else
			{
				fld2.value =  fld2Val;
			}
		}	
		else
			{
				fld2.value =  '';
			}	
	}

	function changeFieldState(fld1,conditionType,fld1Val,readOnlyfld2,fldstate)
		{
		var changeState = false;
		var fld1text ;
		if (fld1.type == 'select-one')
		{
			fld1text = fld1.options[fld1.selectedIndex].text;
		}
		else if (fld1.type == 'text')
		{
			fld1text = fld1.value;
		}	
		



		if (conditionType == 'velEquals') 
		 {
			if (fld1text == fld1Val) 
			{
				changeState = true;
			}
		 }
		 else if (conditionType == 'velNotEquals')
		{
			if (fld1text != fld1Val) 
			{
				changeState = true;
			}
		}
		 else if (conditionType == 'velGreaterThan')
		{
			if(isDecimal(fld1text) && isDecimal(fld1Val))
			{ 
				if (parseFloat(fld1text) > parseFloat(fld1Val)) 
				{
					changeState = true;
				}
			}	
		}
		 else if (conditionType == 'velLessThan')
		{
			if(isDecimal(fld1text) && isDecimal(fld1Val))
			{ 
				if (parseFloat(fld1text) < parseFloat(fld1Val)) 
				{
					changeState = true;
				}
			}	
		}
		 else if (conditionType == 'velLessThanEquals')
		{
			if(isDecimal(fld1text) && isDecimal(fld1Val))
			{ 
				if (parseFloat(fld1text) <= parseFloat(fld1Val)) 
				{
					changeState = true;
				}
			}	
		}
		 else if (conditionType == 'velGreaterThanEquals')
		{
			if(isDecimal(fld1text) && isDecimal(fld1Val))
			{ 
				if (parseFloat(fld1text) >= parseFloat(fld1Val)) 
				{
					changeState = true;
				}
			}	
		}
		if (changeState)
		{
			if (fldstate == 'readonly')
			{
				readOnlyfld2.readOnly =  true;
				//done by sonika to change the class
				readOnlyfld2.className='inpGrey';
			}
			else if	(fldstate == 'disabled')
			{
				readOnlyfld2.disabled =  true;
				readOnlyfld2.className='inpGrey';				
			}	
		} 
		else
		{
			if (fldstate == 'readonly')
			{
				readOnlyfld2.readOnly =  false;
				readOnlyfld2.className='inpDefault';				
			}
			else if	(fldstate == 'disabled')
			{
				readOnlyfld2.disabled =  false;
				readOnlyfld2.className='inpDefault';				
			}	
		}	
	}	
	function calculateValue(fld1,fld2,fldResult,fldOperator)
	{
	 if (!isWhitespace(fld1.value) && !isWhitespace(fld2.value ))
	 {
		if(isDecimal(fld1.value ) && isDecimal(fld2.value ))
			{
				if (fldOperator == 'velAdd') //Add
				{
					fldResult.value = parseFloat(fld1.value) + parseFloat(fld2.value);
				}	
				else if (fldOperator == 'velSub') //Subtract
				{
					fldResult.value = parseFloat(fld1.value) -  parseFloat(fld2.value);
				}
				else if (fldOperator == 'velDiv') //Divide
				{
					fldResult.value = parseFloat(fld1.value) /  parseFloat(fld2.value);
				}
				else if (fldOperator == 'velMult') //Multiply
				{
					fldResult.value = parseFloat(fld1.value) *  parseFloat(fld2.value);
				}
				else if (fldOperator == 'velAvg') //Average
				{
					fldResult.value = (parseFloat(fld1.value) +  parseFloat(fld2.value))/2;
				}				
			}
	  }			
	}

            
	function addMultipleFlds(arFlds, fldResult )
	{
		var val;
		var fld;
		var result =parseFloat(0);
	 	
		for (var i=0; i<arFlds.length; i++)
		 {

 			fld = arFlds[i];

			val = fld.value;

	
			if(!isWhitespace(val) && !isWhitespace(val ) && isDecimal(val ) )
			{
				result = parseFloat(result) + parseFloat(val);
			}
			else
			{
				result = parseFloat(result) + 0 ;
			}
	 	  }
		
		  fldResult.value = roundANumber(result,3);	
		
	}	  	

	function multiplyMultipleFlds(arFlds,fldResult)
	{
		var val;
		var fld;
		var result =parseFloat(1);
	 	
		for (var i=0; i<arFlds.length; i++)
		 {
 			fld = arFlds[i];
			val = fld.value;
		
			if(!isWhitespace(val) && !isWhitespace(val ) && isDecimal(val ) )
			{
				result = parseFloat(result) *  parseFloat(val);
			}
			else
			{
				result = parseFloat(result) *  1 ;
			}
	 	  }
		 fldResult.value = result;
	}	  	

	function checkInterfieldConditionsForPrint(fld1, conditionType, fld1ValArr) {
		var changeState = false;
		if (conditionType == 'velEquals') {
			if (fld1ValArr && fld1ValArr.length) {
				for (var iX=0; iX<fld1ValArr.length; iX++) {
					if (fld1 == fld1ValArr[iX]) { changeState = true; break; }
					else {
						try {
							var myDigitGroupSymbol = $j.formatCurrency.regions[appNumberLocale].digitGroupSymbol;
							if (isNaN(Number(fld1ValArr[iX].replace(myDigitGroupSymbol, "")))) { continue; }
							if (Number(fld1.replace(myDigitGroupSymbol, "")) == 
									Number(fld1ValArr[iX].replace(myDigitGroupSymbol, ""))) {
								changeState = true;
								break;
							}
						} catch(e) {}
					}
				}
			}
		} else if (conditionType == 'velNotEquals') {
			changeState = true;
			if (fld1ValArr && fld1ValArr.length) {
				for (var iX=0; iX<fld1ValArr.length; iX++) {
					if (fld1 == fld1ValArr[iX]) { changeState = false; }
				}
			}
		} else if (conditionType == 'velGreaterThan') {
			if (fld1ValArr && fld1ValArr.length) {
				for (var iX=0; iX<fld1ValArr.length; iX++) {
					var fld1val = fld1ValArr[iX];
					if(isDecimal(fld1) && isDecimal(fld1val)) { 
						if (parseFloat(fld1) > parseFloat(fld1val)) {
							changeState = true;
						}
					}
				}
			}
		} else if (conditionType == 'velLessThan') {
			if (fld1ValArr && fld1ValArr.length) {
				for (var iX=0; iX<fld1ValArr.length; iX++) {
					var fld1val = fld1ValArr[iX];
					if(isDecimal(fld1) && isDecimal(fld1val)) { 
						if (parseFloat(fld1) < parseFloat(fld1val)) {
							changeState = true;
						}
					}
				}
			}
		} else if (conditionType == 'velLessThanEquals') {
			if (fld1ValArr && fld1ValArr.length) {
				for (var iX=0; iX<fld1ValArr.length; iX++) {
					var fld1val = fld1ValArr[iX];
					if(isDecimal(fld1) && isDecimal(fld1val)) { 
						if (parseFloat(fld1) <= parseFloat(fld1val)) {
							changeState = true;
						}
					}
				}
			}
		} else if (conditionType == 'velGreaterThanEquals') {
			if (fld1ValArr && fld1ValArr.length) {
				for (var iX=0; iX<fld1ValArr.length; iX++) {
					var fld1val = fld1ValArr[iX];
					if(isDecimal(fld1) && isDecimal(fld1val)) { 
						if (parseFloat(fld1) >= parseFloat(fld1val)) {
							changeState = true;
						}
					}
				}
			}
		} else if (conditionType == 'velIsNull') {
			if (!fld1 || fld1.length == 0) { changeState = true; }
		} else if (conditionType == 'velIsNotNull') {
			if (fld1 && fld1.length > 0) { changeState = true; }
		}
		return changeState;
	}
	
	function changeFieldStateMultiplePrint(fld1, conditionType, fld1ValArr, readOnlyfld2Arr, fldstate) {
		var formElements = document.getElementsByTagName('input');
		if (formElements) {
			for(var iX=0; iX<formElements.length; iX++) {
				formElements[iX].readOnly = true;
				formElements[iX].disabled = true;
			}
		}
		
		var changeState = false;
		if (fld1) {
			fld1 = fld1.replace(/^undefined/, "");
		}
		if (fld1 && fld1.indexOf("|") > -1) {
			var fld1Array = fld1.split("|");
			for (var fldIndex=0; fldIndex<fld1Array.length; fldIndex++) {
				changeState = checkInterfieldConditionsForPrint(fld1Array[fldIndex], conditionType, fld1ValArr);
				if (changeState) { break; }
			}
		} else {
			changeState = checkInterfieldConditionsForPrint(fld1, conditionType, fld1ValArr);
		}

		if (!changeState) { return; }
		for (var iX=0; iX<readOnlyfld2Arr.length; iX++) {
			var fld = readOnlyfld2Arr[iX];
			var labelObj = document.getElementById(fld.name + "_id");
			var spanObj = document.getElementById(fld.name + "_span");
			if	(fldstate == 'disabled') {
				if (labelObj) {
					labelObj.style.visibility = "visible";
				}
				if (spanObj) {
					spanObj.style.visibility = "visible";
					spanObj.style.backgroundColor = "lightGray";
					if (spanObj.innerHTML) {
						spanObj.innerHTML = spanObj.innerHTML.replace("<U><U>", "<font color='gray'>");
						spanObj.innerHTML = spanObj.innerHTML.replace("</U></U>", "</font>");
					}
				}
			} else if (fldstate == 'hidden') {
				if (labelObj) {
					labelObj.style.visibility = "hidden";
				}
				if (spanObj) {
					spanObj.style.visibility = "hidden";
				}
			}
		}
	}

	function changeFieldStateMultiple(fld1,conditionType,fld1ValArr,readOnlyfld2Arr,fldstate)
		{
		
		//alert("v_displayvalflag" +v_displayvalflag);
		var changeState = false;
		var fld1text = "" ;
		var fld1val ;
		var fldLength = 0;
		var labelObj= "";
		var datePicObj  = ""


		if (fld1.type == 'select-one')
		{
			if (v_displayvalflag == 1)
			{
				fld1text = fld1.options[fld1.selectedIndex].text;
			}
			else
			{
				fld1text = fld1.options[fld1.selectedIndex].value;
			}	
		}
		else if (fld1.type == undefined)
		{
					
			fldLength = fld1.length;
		}	
		else
		{
			fld1text = fld1.value;
		}
		
		if (fld1.type == 'checkbox' || fld1.type == 'radio')
		{
			fldLength =  fld1.length;

			if (fldLength == undefined)
			{
				fldLength = 0;

				if (fld1.checked)
				{
					if (v_displayvalflag == 1)
					{
						fld1text = fld1.id;
					}else
						{
							fld1text = fld1.value;
						}	
				}
				else
				{
					fld1text = '';
				}

			}
		}



		if (conditionType == 'velEquals') 
		 {
			//convert fldtext to lower case
	
			fld1text = fld1text.toLowerCase();


		  for (var i=0; i<fld1ValArr.length; i++)
		    {
 			fld1val = fld1ValArr[i];

			fld1val = fld1val.toLowerCase();

			if (fldLength == 0)
			{

				if (fld1text == fld1val) 
				{	
					changeState = true;
				} else {
					try {
						if (!isNaN(Number(fld1text)) && !isNaN(fld1val) && 
								(Number(fld1text) == Number(fld1val))) {
							changeState = true;
						} else {
							var myDigitGroupSymbol = $j.formatCurrency.regions[appNumberLocale].digitGroupSymbol;
							var fld1textcleaned = fld1text.replace(myDigitGroupSymbol, "");
							var fld1valcleaned = fld1val.replace(myDigitGroupSymbol, "");
							if (!isNaN(fld1textcleaned) && !isNaN(fld1valcleaned) &&
									Number(fld1textcleaned) == Number(fld1valcleaned)) {
								changeState = true;
							}
						}
					} catch(e) {}
				}
			}
			else
			{
				for (var k = 0; k < fldLength; k++)
				{
										
					if (fld1[k].checked)
					{
						if (v_displayvalflag == 1)
						{
							fld1text = fld1[k].id;
						}
						else
						{
							fld1text = fld1[k].value;
						}	
						
						fld1text = fld1text.toLowerCase();
															
						if (fld1text == fld1val) 
						{	
							changeState = true;
						}
					}
				}
				

			}
			

		   }	
		 }
		 else if (conditionType == 'velNotEquals')
		{
		  changeState = true;
			
			//convert fldtext to lower case
	
			fld1text = fld1text.toLowerCase();


	
		  for (var i=0; i<fld1ValArr.length; i++)
		    {
 			fld1val = fld1ValArr[i];
			fld1val = fld1val.toLowerCase();

			if (fldLength == 0)
			{
				if (fld1text == fld1val) 
				{	
					changeState = false;
				}
			}
			else
			{
				for (var k = 0; k < fldLength; k++)
				{
										
					if (fld1[k].checked)
					{
						if (v_displayvalflag == 1)
						{
							fld1text = fld1[k].id;
						}
						else
						{
							fld1text = fld1[k].value;
						}	
						//convert fldtext to lower case
	
						fld1text = fld1text.toLowerCase();


															
						if (fld1text == fld1val) 
						{	
							changeState = false;
						}
					}
				}
				

			}
	

	 	   }

		}
		 else if (conditionType == 'velGreaterThan')
		{
			for (var i=0; i<fld1ValArr.length; i++)
		    	{
 				fld1val = fld1ValArr[i];
				if (fldLength == 0)
				{
				if(isDecimal(fld1text) && isDecimal(fld1val))
					{ 
						if (parseFloat(fld1text) > parseFloat(fld1val)) 
						{
						changeState = true;
						}
					}	
				}
				else
				{
					for (var k = 0; k < fldLength; k++)
					{
											
						if (fld1[k].checked)
						{
							if (v_displayvalflag == 1)
							{
								fld1text = fld1[k].id;
							}
							else
							{
								fld1text = fld1[k].value;
							}	
							 

							if(isDecimal(fld1text) && isDecimal(fld1val))
							{ 
								if (parseFloat(fld1text) > parseFloat(fld1val)) 
								{
								changeState = true;
								}
							}																	
						}
					}
				

				}
		
			}
		}
		 else if (conditionType == 'velLessThan')
		{
		 for (var i=0; i<fld1ValArr.length; i++)
		    {
 			fld1val = fld1ValArr[i];
			if (fldLength == 0)
			{
				if(isDecimal(fld1text) && isDecimal(fld1val))
				{ 
					if (parseFloat(fld1text) < parseFloat(fld1val)) 
					{
						changeState = true;
					}
				}	
			}
			else
				{
					for (var k = 0; k < fldLength; k++)
					{
											
						if (fld1[k].checked)
						{
							if (v_displayvalflag == 1)
							{
								fld1text = fld1[k].id;
							}
							else
							{
								fld1text = fld1[k].value;
							}

							if(isDecimal(fld1text) && isDecimal(fld1val))
							{ 
								if (parseFloat(fld1text) < parseFloat(fld1val)) 
								{
									changeState = true;
								}
							}																	
						}
					}
				

				}
		
		  }
		}
		 else if (conditionType == 'velLessThanEquals')
		{
		 for (var i=0; i<fld1ValArr.length; i++)
		    {
 			fld1val = fld1ValArr[i];
			
			if (fldLength == 0)
			{
				if(isDecimal(fld1text) && isDecimal(fld1val))
				{ 
					if (parseFloat(fld1text) <= parseFloat(fld1val)) 
					{
						changeState = true;
					}
				}		
			}
			else
				{
					for (var k = 0; k < fldLength; k++)
					{
											
						if (fld1[k].checked)
						{
							if (v_displayvalflag == 1)
							{
								fld1text = fld1[k].id;
							}
							else
							{
								fld1text = fld1[k].value;
							}


							if(isDecimal(fld1text) && isDecimal(fld1val))
							{ 
							if (parseFloat(fld1text) <= parseFloat(fld1val)) 
								{
								changeState = true;
								}
							}																	
						}
					}
				

				}

		   }
		}
		 else if (conditionType == 'velGreaterThanEquals')
		{
		  for (var i=0; i<fld1ValArr.length; i++)
		    {
 			fld1val = fld1ValArr[i];
			if (fldLength == 0)
			{
	
				if(isDecimal(fld1text) && isDecimal(fld1val))
				{ 
					if (parseFloat(fld1text) >= parseFloat(fld1val)) 
					{
						changeState = true;
					}
				}	
			}
			else
				{
					for (var k = 0; k < fldLength; k++)
					{
											
						if (fld1[k].checked)
						{
								if (v_displayvalflag == 1)
							{
								fld1text = fld1[k].id;
							}
							else
							{
								fld1text = fld1[k].value;
							}


							if(isDecimal(fld1text) && isDecimal(fld1val))
							{ 
								if (parseFloat(fld1text) >= parseFloat(fld1val)) 
								{
								changeState = true;
								}
							}																	
						}
					}
				

				}

		    }	
		} else if (conditionType == 'velIsNull') 
		 {
			//convert fldtext to lower case
	
			fld1text = fld1text.toLowerCase();
			 
            if ( ( fldLength ==0 && isEmpty(fld1text) )  || (fld1.type == 'select-one' && fld1text == 'select an option' ))
            {
              changeState = true;
            }  
            else if (fldLength > 0 && fld1.type != 'select-one')
            {
            	 
            	 var chkcount=0;
            	 
            	for (var k = 0; k < fldLength; k++)
				{
																				
					if (fld1[k].checked)
					{
						chkcount = chkcount+1;
					}
						
            	}
            	
            	if (chkcount ==0)
					{
						changeState = true;
					}
					
            }
		   	
		 }else if (conditionType == 'velIsNotNull') 
		 {
			//convert fldtext to lower case
	
			fld1text = fld1text.toLowerCase();
			 
			 
			var chkcount=0;
			 
            
            if (fldLength ==0 && !isEmpty(fld1text) && fld1.type != 'select-one')
            {
              changeState = true;
            }
            else  if (fld1.type == 'select-one' && fld1text != 'select an option' )
            {
           
              changeState = true;
            }
            else if (fldLength > 0  )
            {
            	for (var k = 0; k < fldLength; k++)
				{
					
										
					if (fld1[k].checked)
					{
						chkcount = chkcount+1;
					}
					
					 
					
            	}
            	
            	if (chkcount >0)
					{
						changeState = true;
					}
            
            }

		   	
		 }
		

		if (changeState)
		{
			
		  for (var i=0; i<readOnlyfld2Arr.length; i++)
		    {
 			fld = readOnlyfld2Arr[i];
			
		
			if (fldstate == 'readonly')
			{

				if (fld.length == undefined || fld.type == 'select-one')	
				{
					var cName = fld.className;
					if ((cName.length != 0) && cName.indexOf('datefield') >= 0 ){
						disableCal(fld);
					}
					fld.readOnly =  true;

				}
				else
				{
				
					for (var k=0;k<fld.length;k++)
					{
						var cName = fld[k].className;
						if ((cName.length != 0) && cName.indexOf('datefield') >= 0 ){
							disableCal(fld[k]);
						}
						fld[k].readOnly =  true;

						
					}
	
				}



			}
			else if	(fldstate == 'disabled')
			{
				/* for checkbox target*/
				
				if (fld.length == undefined || fld.type == 'select-one')	
				{		
					fld.disabled =  true;
					var cName = fld.className;
					if ((cName.length != 0) && cName.indexOf('datefield') >= 0 ){
						fld.className= 'datefield inpGrey';
					}else
						fld.className='inpGrey';				
				}
				else
				{

					for (var k=0;k<fld.length;k++)
					{
						fld[k].disabled =  true;
						var cName = fld[k].className;
						if ((cName.length != 0) && cName.indexOf('datefield') >= 0)
							fld[k].className= 'datefield inpGrey';
						else
							fld[k].className='inpGrey';
					}
	
				}


			}	//for disabled
				else if	(fldstate == 'hidden')
			{
				/* for checkbox target*/
				
				if (fld.length == undefined)	
				{		
					fld.disabled =  true;
					var cName = fld.className;
					if ((cName.length != 0) && cName.indexOf('datefield') >= 0 ){
						fld.className= 'datefield inpGrey';
					}else
						fld.className='inpGrey';		
					
					labelObj = document.getElementById(fld.name + "_id");
					//datePicObj = document.getElementById(fld.name + "_date_A");
					
					if (fld.type == 'textarea'){
			 			spanObj = document.getElementById(fld.name);
					}else{
						spanObj = document.getElementById(fld.name + "_span");
					} 
				}
				else
				{
					 if (fld.length >=1)
					 {
						if(fld.type == 'select-one'){
							labelObj = document.getElementById(fld.name + "_id");
							
							spanObj = document.getElementById(fld.name + "_span");
						}else{
							for (var k=0;k<fld.length;k++)
							{		
								fld[k].disabled =  true;
								var cName = fld[k].className;
								if ((cName.length != 0) && cName.indexOf('datefield') >= 0 ){
									fld[k].className= 'datefield inpGrey';
								}else
									fld[k].className='inpGrey';
							}
							labelObj = document.getElementById(fld[0].name + "_id");
							
							spanObj = document.getElementById(fld[0].name + "_span");
						}
					}else{
						labelObj = document.getElementById(fld.name + "_id");
						
						spanObj = document.getElementById(fld.name + "_span");
					}	
					 
										
				  
	
				}
				
				if (labelObj !=undefined)
				{
					labelObj.style.display = "none"; 
				
				}
				
				if (spanObj !=undefined)
				{
					spanObj.style.display = "none"; 
				
				}


			}	//for hidden
		   }	

		} 
		else
		{
		  for (var i=0; i<readOnlyfld2Arr.length; i++)
		    {
 			fld = readOnlyfld2Arr[i];

			if (fldstate == 'readonly' )
			{

				if (fld.length == undefined || fld.type == 'select-one')	
				{
					var cName = fld.className;
					if ((cName.length != 0) && cName.indexOf('datefield') >= 0 ){
						enableCal(fld);
					}
					fld.readOnly =  false;

				}
				else
				{
				  for (var k=0;k<fld.length;k++)
				  {
				    var cName = fld[k].className;
					if ((cName.length != 0) && cName.indexOf('datefield') >= 0 ){
						enableCal(fld[k]);
					}
					fld[k].readOnly =  false;

				   }
	
				}
				
			}
			else if	(fldstate == 'disabled')
			{
				if (fld.length == undefined || fld.type == 'select-one')	
				{
					fld.disabled =  false;
					var cName = fld.className;
					if ((cName.length != 0) && cName.indexOf('datefield') >= 0) 
						fld.className= 'datefield';
					else
						fld.className='inpDefault';				
				}
				else
				{
				
					for (var k=0;k<fld.length;k++)
					{
						fld[k].disabled =  false;
						var cName = fld[k].className;
						if ((cName.length != 0) && cName.indexOf('datefield') >= 0) 
							fld[k].className= 'datefield';
						else
							fld[k].className='inpDefault';				
						
					}
	
				}

			} //for disabled
         else if	(fldstate == 'hidden')
			{
				if (fld.length == undefined)	
				{
					fld.disabled =  false;
					var cName = fld.className;
					if ((cName.length != 0) && cName.indexOf('datefield') >= 0) 
						fld.className= 'datefield';
					else
						fld.className='inpDefault';
					
					labelObj = document.getElementById(fld.name + "_id");
					//datePicObj = document.getElementById(fld.name + "_date_A");
					
					if (fld.type == 'textarea'){
			 			spanObj = document.getElementById(fld.name);
					}else{
						spanObj = document.getElementById(fld.name + "_span");
					}		
				}
				else
				{
					 if (fld.length >=1)
					 {
					 
						if(fld.type == 'select-one'){
							labelObj = document.getElementById(fld.name + "_id");
							
							spanObj = document.getElementById(fld.name + "_span");
						}else{
							
					 		for (var k=0;k<fld.length;k++)
							{
								fld[k].disabled =  false;
								var cName = fld[k].className;
								if ((cName.length != 0) && cName.indexOf('datefield') >= 0) 
									fld[k].className= 'datefield';
								else
									fld[k].className='inpDefault';
								
							}
							labelObj = document.getElementById(fld[0].name + "_id");
							
							spanObj = document.getElementById(fld[0].name + "_span");
						}
					}else{
						labelObj = document.getElementById(fld.name + "_id");
						
						spanObj = document.getElementById(fld.name + "_span");
					}
	
				}
				
				if (labelObj !=undefined)
				{
					labelObj.style.display = "block"; 
				
				}
				
				if (spanObj !=undefined)
				{
					spanObj.style.display = "block"; 
				
				}

			}	//hidden		
				
		   }	
		}	
		
		// For Bug#17234
		if (typeof openCal !== typeof undefined) {  // Safety check to make sure formjs.js can resolve the function
			openCal();  // Defined in jqueryUtils.jsp
		}
	}	


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	function autoPopulateMultiple(fld1,conditionType,fld1ValArr,targetfld2Arr,targetfldValues)
		{
		var populate = false;
		var fld1text = "" ;
		var fld1val ;

		if (fld1.type == 'select-one')
		{
			fld1text = fld1.options[fld1.selectedIndex].text;
		}
		else if (fld1.type == 'text')
		{
			fld1text = fld1.value;
		}	

		if (conditionType == 'velEquals') 
		 {
		  for (var i=0; i<fld1ValArr.length; i++)
		    {
 			fld1val = fld1ValArr[i];
			
			
			if (fld1text == fld1val) 
			{
				populate = true;
			}
			

		   }	
		 }
		 else if (conditionType == 'velNotEquals')
		{
		  populate = true;
	
		  for (var i=0; i<fld1ValArr.length; i++)
		    {
 			fld1val = fld1ValArr[i];
	
			if (fld1text == fld1val) 
			{	
				populate = false;
			}
	 	   }

		}
		 else if (conditionType == 'velGreaterThan')
		{
			for (var i=0; i<fld1ValArr.length; i++)
		    	{
 				fld1val = fld1ValArr[i];
	
				if(isDecimal(fld1text) && isDecimal(fld1val))
				{ 
					if (parseFloat(fld1text) > parseFloat(fld1val)) 
					{
					populate = true;
					}
				}	
			}
		}
		 else if (conditionType == 'velLessThan')
		{
		 for (var i=0; i<fld1ValArr.length; i++)
		    {
 			fld1val = fld1ValArr[i];

			if(isDecimal(fld1text) && isDecimal(fld1val))
			{ 
				if (parseFloat(fld1text) < parseFloat(fld1val)) 
				{
					populate = true;
				}
			}	
		  }
		}
		 else if (conditionType == 'velLessThanEquals')
		{
		 for (var i=0; i<fld1ValArr.length; i++)
		    {
 			fld1val = fld1ValArr[i];

			if(isDecimal(fld1text) && isDecimal(fld1val))
			{ 
				if (parseFloat(fld1text) <= parseFloat(fld1val)) 
				{
					populate = true;
				}
			}	
		   }
		}
		 else if (conditionType == 'velGreaterThanEquals')
		{
		  for (var i=0; i<fld1ValArr.length; i++)
		    {
 			fld1val = fld1ValArr[i];
	
			if(isDecimal(fld1text) && isDecimal(fld1val))
			{ 
				if (parseFloat(fld1text) >= parseFloat(fld1val)) 
				{
					populate = true;
				}
			}	
		    }	
		}


		if (populate)
		{
			
		  for (var i=0; i<targetfld2Arr.length; i++)
		    {
 			fld = targetfld2Arr[i];

			fld.value =  targetfldValues[i];
			
			
		   }	

		} 
		else
		{
		  for (var i=0; i<targetfld2Arr.length; i++)
		    {
 			fld = targetfld2Arr[i];
			fld.value = '';
		   }	
		}	
	}	
//////////////////////////////////////////////////////////////////

	function addMultipleFldsMultipleTarget(arFlds, arfldResult )
	{
		var val;
		var fld;
		var result =parseFloat(0);
	 	var gotAnyValue = false;

		for (var i=0; i<arFlds.length; i++)
		 {

 			fld = arFlds[i];

			val = fld.value;

			
			if(!isWhitespace(val) && !isNaN(parseFloat(val)) && isDecimal(val ) )
			{
				result = parseFloat(result) + parseFloat(val);
				gotAnyValue = true;	
			}
			
	 	  }

		if (!gotAnyValue)
		{
		   result = '';
		}

		
		if (isNaN(result))
		{
			result = '';
		}
		
		if (!isNaN(result) && isDecimal(result))
		{
		 	result = roundANumber(result,3);
		 
		}
		
		for (var i=0; i < arfldResult.length; i++)
		 {
		  
		  fld = arfldResult[i];	
		  fld.value = result;
		}
	}	  	
////////////////////////////////////////////////////////////////////////////////

            
	function multiplyMultipleFldMultipleTarget(arFlds,arrFldResult)
	{
		var val;
		var fld;
		var result =parseFloat(1);
		var gotAnyValue = false;
	 	
		for (var i=0; i<arFlds.length; i++)
		 {
 			fld = arFlds[i];
			val = fld.value;
		
			if(!isWhitespace(val) && !isNaN(parseFloat(val)) && isDecimal(val ) )
			{
				result = parseFloat(result) *  parseFloat(val);
				gotAnyValue = true;
			}
			
	 	  }

		if (!gotAnyValue)
		{
		   result = '';
		}
		if (isNaN(result))
		{
			result = '';
		}

	   if (!isNaN(result) && isDecimal(result))
		{
		 	result = roundANumber(result,3);
		 
		}
		
			for (var i=0; i<arrFldResult.length; i++)
			 {
 				fld = arrFldResult[i];
			   	fld.value = result;
			 }
		

	}	  	

//////////////////////////////////////////////////////////////////

	function subtractMultipleFldsMultipleTarget(arFlds, arfldResult )
	{
		var val;
		var fld;
		var result =parseFloat(0);
	 	var gotAnyValue = false;
	 	var fldtype = "";
	 	var olddate;

		for (var i=0; i<arFlds.length; i++)
		 {

 			fld = arFlds[i];

			val = fld.value;
			
			
			
			fldtype = fld.getAttribute("flddatatype");
			
			if (fldtype == undefined)
			{
				fldtype = "";
			}

	
			if(!isWhitespace(val) && !isNaN(parseFloat(val)) && isDecimal(val ) && fldtype!='D')
			{
				
			  	if ( i == 0)
				{	
 					result = parseFloat(val);		
				}
				else
				{
 					result = parseFloat(result) - parseFloat(val);		
				}

			gotAnyValue = true;		

			} else if ( !isWhitespace(val) && fldtype=='D' )
			{
				
				if ( i == 0)
				{	
 					result = 0;		
 					olddate = fld;
 					 
 					
				}
				else
				{
					if(!isWhitespace(fld.value) && (fld.value != '') && olddate!= undefined)
					{
 						result = result - getDatediff(olddate, fld);
 						olddate = fld;
 					}		
 					 		
 					 
				}
				gotAnyValue = true;	
				result = Math.abs(result);
				
			}
			
	 	  
		 }
	   if (!gotAnyValue)
		{
		   result = '';
		}
		if (isNaN(result))
		{
			result = '';
		}

		if (!isNaN(result) && isDecimal(result))
		{
		 	result = roundANumber(result,3);
		 
		}
		
		for (var i=0; i < arfldResult.length; i++)
		 {
		  
		  fld = arfldResult[i];	
		  fld.value = result;
		}
	}	  	
////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////

	function divideMultipleFldsMultipleTarget(arFlds, arfldResult )
	{
		var val;
		var fld;
		var result =parseFloat(0);
	 	var gotAnyValue = false;

		for (var i=0; i<arFlds.length; i++)
		 {

 			fld = arFlds[i];

			val = fld.value;

	
			if(!isWhitespace(val) && !isNaN(parseFloat(val)) && isDecimal(val ) )
			{
				
			  	if ( i == 0)
				{	
 					result = parseFloat(val);		
				}
				else
				{
 					result = parseFloat(result) / parseFloat(val);		
				}

			gotAnyValue = true;		

			}
	 	  
		 }
		
		if (!gotAnyValue)
		{
		   result = '';
		}
		if (isNaN(result))
		{
			result = '';
		}

		if (!isNaN(result) && isDecimal(result))
		{
		 	result = roundANumber(result,3);
		 
		}
		
		for (var i=0; i < arfldResult.length; i++)
		 {
		  
		  fld = arfldResult[i];	
		  fld.value = result;
		}
	}	  	
////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////

	function averageMultipleFldsMultipleTarget(arFlds, arfldResult )
	{
		var val;
		var fld;
		var result =parseFloat(0);
	 	var gotAnyValue = false;
		var valuecount = 0;

		for (var i=0; i<arFlds.length; i++)
		 {

 			fld = arFlds[i];

			val = fld.value;

	
			if(!isWhitespace(val) && !isNaN(parseFloat(val)) && isDecimal(val ) )
			{
				
		  		result = parseFloat(result) + parseFloat(val);	
				gotAnyValue = true;		
				valuecount = valuecount + 1;

			}
	 	  
		 }
		
		if (!gotAnyValue)
		{
		   result = '';
		}
		else //take average
		{
			result = result/valuecount ;
		}
		
		if (isNaN(result))
		{
			result = '';
		}

		if (!isNaN(result) && isDecimal(result))
		{
		 	result = roundANumber(result,3);
		 
		}
				
		for (var i=0; i < arfldResult.length; i++)
		 {
		  
		  fld = arfldResult[i];	
		  fld.value = result;
		}
	}	  	

  
  function openChildForm (linkedForm,formEntryType,formDisplayType,formobj){
  if (typeof(formobj.formFilledFormId)=="undefined") {
  /*alert("Please Submit the Current Form First");*****/
    alert(Sbmt_CurrFrmFirst);
  return false;
  }
  else
  {
  openlinkedForm (linkedForm,formEntryType,formDisplayType,formobj);
  }
  
  
  
  }

/*function to open linked forms from form preview*/
	function openlinkedForm (linkedForm,formEntryType,formDisplayType,formobj)
	{
		var windowName ;
		var windowURL;
		var targetStr = "";
		
		targetStr = targetVar;
		 
		if (targetStr != "_self")
		{
			targetStr = "LinkForm";
		}
		
		if (calledFromPreview)
		{
			/*alert("Disabled in Preview mode");*****/
			  alert(Disab_InPrvMode);
			return;
		}
 	
		
		windowURL = getFormBrowserURL (linkedForm,formEntryType,formDisplayType,formobj);
						
		if (windowURL.length > 0)
		{
			// Bug #7044
			if (formDisplayType == 'SP' || formDisplayType == 'PS' || formDisplayType == 'PR' ){
				windowName = window.open(windowURL, 
						targetStr,'toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=825,height=600,top=90,left=150');
			}else{
				windowName = window.open(windowURL, 
							targetStr,'toolbar=no,resizable=no,scrollbars=yes,menubar=no,status=yes,width=825,height=600,top=90,left=150'); 
			}
		windowName.focus();
		}
	
	}
	
//////////////////
	/*function to open linked forms from form preview*/
	function openlinkedForm (linkedForm,formEntryType,formDisplayType,formobj,calledFrom,moreParam)
	{
		var windowName ;
		var windowURL;
		var targetStr = "";
		
		targetStr = targetVar;
		 
		if (targetStr != "_self")
		{
			targetStr = "LinkForm";
		}
		
		if (calledFromPreview)
		{
			/*alert("Disabled in Preview mode");*****/
			  alert(Disab_InPrvMode);
			return;
		}
 	
		
		windowURL = getFormBrowserURL (linkedForm,formEntryType,formDisplayType,formobj);
		if(calledFrom=="patientPortalSch" || calledFrom=="patientPortal"){
			windowURL=windowURL+"&patientForm="+calledFrom+moreParam;
		}
						
		if (windowURL.length > 0)
		{
			// Bug #7044
			if (formDisplayType == 'SP' || formDisplayType == 'PS' || formDisplayType == 'PR' ){
				windowName = window.open(windowURL, 
						targetStr,'toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=1050,height=600,top=90,left=150');
			}else{
				windowName = window.open(windowURL, 
							targetStr,'toolbar=no,resizable=no,scrollbars=yes,menubar=no,status=yes,width=1050,height=600,top=90,left=150'); 
			}
		windowName.focus();
		}
	
	}
//////////////////

/*function to open linked forms from form preview*/
	function getFormBrowserURL (linkedForm,formEntryType,formDisplayType,formobj)
	{
		var formstr;
		var url = "";
		
		var formStudy = "";
		var formPatient = "";
		var formPatprot = "";
			
 		responseStr="";
 					
		if (typeof(formobj.formFilledFormId)!="undefined") {
		responseStr="responseId="+formobj.formFilledFormId.value;
		}			
		if (typeof(formobj.formId)!="undefined") {
		if (responseStr.length>0) {
		responseStr=responseStr+"&parentId="+formobj.formId.value;
		}else
		{
		responseStr="parentId="+formobj.formId.value;
		}
		
		}
 						
		formstr	= linkedForm + "*" + formEntryType;			
		

		if (formDisplayType == 'A')
		{
			url = 'formfilledaccountbrowser.jsp?calledFromForm=formpreview&formPullDown=' + formstr+"&"+responseStr ;
		}
		if (formDisplayType == 'S' || formDisplayType == 'SA')
		{
			//get studyId
			formStudy = formobj.formStudy.value;
			url = 'formfilledstudybrowser.jsp?calledFromForm=formpreview&formPullDown=' + formstr + '&studyId='+formStudy+"&"+responseStr ;
		}
		
		if (formDisplayType == 'PA')
		{
			formPatient = formobj.formPatient.value;
			url = 'formfilledpatbrowser.jsp?calledFromForm=formpreview&formPullDown=' + formstr + '&pkey='+formPatient+"&"+responseStr ;
		}

		if (formDisplayType == 'SP' || formDisplayType == 'PS' || formDisplayType == 'PR' )
		{
			formPatient = formobj.formPatient.value;
			formPatprot = formobj.formPatprot.value;
			formStudy = formobj.patStudyId.value;
			
			url = 'formfilledstdpatbrowser.jsp?calledFromForm=formpreview&formPullDown=' + formstr + '&pkey=' + formPatient +'&patProtId=' + formPatprot + '&studyId=' + formStudy+"&"+responseStr + "&formDispType="+formDisplayType;

			url = url + '&fk_sch_events1=' + fk_sch_events1 + '&fkStorageForSpecimen=' + fkStorageForSpecimen;
			
			
			
		}
		
		return url;				
		
	}

//////////////////
// function to open patient specific AdHoc query from forms. Opened from a hard coded link in patient specific forms. 
//Will filter the 	adHoc report for the selected patient 
 function openPatAdHoc(linkStr,formobj)
 {
   var vlinkStr;
   var patId;
  
   patId = formobj.formPatient.value;
   vlinkStr = linkStr + "&formPatient=" + patId;
 
   windowName = window.open(linkStr + "&formPatient=" + patId, "AdHocPreview","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=700,height=550 top=100,left=100");  
   windowName.focus();
 }
 
 function ShowLockDownMessage()
 { 
 	
	
	 if (document.getElementById("formRights")==null) return;
 	if (document.getElementById("formRights").value<6)
	{ 
	  
		if (document.getElementById("er_def_formstat").value==document.getElementById("lockDownStatus").value) {
		/*alert("You do not have appropriate rights to set this status.");*****/
		alert(NoRgtTo_SetStatus);
			document.getElementById("er_def_formstat").value=document.getElementById("currentStatus").value;
		return false;
		}
	}
 	
 	if (document.getElementById("er_def_formstat").value==document.getElementById("lockDownStatus").value)
	{
		/*if (confirm('This Form Status will not allow any further changes to this form response. Do you want to proceed?'))*****/
		if (confirm(FrmStatNotAllow_WantProc))
		
		{
		document.getElementById("currentStatus").value=document.getElementById("er_def_formstat").value;	
		return true;
		}
		else{
			document.getElementById("er_def_formstat").value=document.getElementById("currentStatus").value;
		return false;
		}
		
	} 
	else{
		
		document.getElementById("currentStatus").value=document.getElementById("er_def_formstat").value;
		
	}
 }
 
 function getWhereClauseForFormsLookup(formtype, accId,userId,uniqueFilterString,study,pat)
 {
  var whereClause="";
  return whereClause;
 }
 
 
 /*function to open linked forms from form preview*/
	function openNewForm(linkedForm,formEntryType,formDisplayType,formobj)
	{
		var formstr;
		var windowURL = "";
		var windowName ;
		var formStudy = "";
		var formPatient = "";
		var formPatprot = "";
		
			
		formstr	= linkedForm + "*" + formEntryType;			

		
		if (formDisplayType == 'PA')
		{
			formPatient = formobj.formPatient.value;
			windowURL = 'patformdetails.jsp?formId='+ linkedForm + '&formFillDt=ALL&entryChar='+formEntryType+'&mode=N&formDispLocation=PA&calledFromForm=formpreview&formPullDown=' + formstr + '&pkey='+formPatient;
		}

		if (formDisplayType == 'SP' || formDisplayType == 'PS' || formDisplayType == 'PR' )
		{
			formPatient = formobj.formPatient.value;
			formPatprot = formobj.formPatprot.value;
			formStudy = formobj.patStudyId.value;
		
			windowURL = 'patstudyformdetails.jsp?formId='+ linkedForm +
			'&mode=N&formDispLocation=SP&calledFromForm=formpreview&calledFrom=S&entryChar='+formEntryType+'&formFillDt=ALL&formPullDown=' + 
			 formstr + '&pkey=' +
			 formPatient +'&patProtId=' + formPatprot + '&studyId=' + formStudy;
			
			windowURL = windowURL + '&schevent=' + fk_sch_events1+ '&fkStorageForSpecimen=' + fkStorageForSpecimen;
			
		}				
				
		if (windowURL.length > 0)
		{

		windowName = window.open(windowURL, 
		'_self','toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=700,height=550 top=100,left=100'); 

		windowName.focus();
		}
	
	}
	
function openPrintWinCommon(formId,filledFormId,linkFrom,studyId,pkey,patProtId,patientCode,protocolId,formLibVerNumber) 
{
    
    
   if (linkFrom=="PA") linkFrom = "P";
   if (linkFrom=="SA") linkFrom = "S";
   if (linkFrom=="SP") linkFrom = "P";            
   if (linkFrom=="PR") linkFrom = "P";
   if (linkFrom=="PS") linkFrom = "P";
   
     
   
  windowName=window.open("formprint.jsp?formId="+formId+"&filledFormId="+filledFormId+"&linkFrom="+linkFrom+
	  "&studyId="+studyId+"&formLibVerNumber="+formLibVerNumber+"&pkey="+pkey+"&patProtId="+patProtId+"&patientCode="+patientCode+"&protocolId="+protocolId,"Information1","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
   windowName.focus();
}
	
	
/*function getAjaxOutput(filename,params,targetout)
		{
			 
			  new VELOS.ajaxObject(filename, {
		   			urlData:params ,
				   reqType:"POST",
				   outputElement: targetout} 
		   
		   ).startRequest(); 
		
		}*/
			
			
	function openWindowCommon(windowURL,target_)
	{
		var windowName = "";
		windowName = window.open(windowURL, 
		target_,'toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=700,height=550 top=100,left=100'); 

		windowName.focus();
		
	}		
	
	
	
	function  openReportCommon(formobj,reportId,reportName,repFilledFormId,act){
		
		openReportCommonWithMode(formobj,reportId,reportName,repFilledFormId,act,'');
		
	}
	
	
	function  openReportCommonWithMode(formobj,reportId,reportName,repFilledFormId,act,displayMode){
	
		formobj.repId.value = reportId;
		formobj.repName.value = reportName;
		formobj.repFilledFormId.value = repFilledFormId;
		formobj.filterType.value = "A";
		
		formobj.action="repRetrieve.jsp?displayMode="+displayMode;
		formobj.target="_new";
		formobj.submit();
		formobj.action=act;

	}
	
	
function openLocationLookupForSpecimen(formobj) {			
 	windowName= window.open("searchLocation.jsp?gridFor=ST&form="+formobj.name+"&locationName=storageLoc&coordx=strCoordinateX&coordy=strCoordinateY&storagepk=mainFKStorage","Grid","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=900,height=650");
	
	windowName.focus();	
}

function openLocationLookup(formobj,gridFor) {			
 	windowName= window.open("searchLocation.jsp?gridFor="+gridFor+"&form="+formobj.name+"&locationName=storageLoc&coordx=strCoordinateX&coordy=strCoordinateY&storagepk=mainFKStorage","Grid","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=900,height=650");
	windowName.focus();	
}

function triggerFormAction(e)
{
var fldId=$E.getTarget(e).id;
ajaxvalidate('form~pfldValidate:'+fldId,0,'','','','formId:CONSTANT|'+fldId+':studyId:pkey:sessAccId'); 
}

//if true, by pass all validations and let the form save with incomplete status
function byPassValidationsForIncomplete(formobj)
{
 
	if (formobj.incompleteStatusValue.value == formobj.er_def_formstat.value)
	{
	 
		
		formobj.override_count.value = "0";
		formobj.action = "updateFormData.jsp";
		formobj.target="_self";

		var datevalue ;
	
		datevalue=formobj.er_def_date_01.value;
		
	 	if (isWhitespace(datevalue) == true || datevalue==''||datevalue==null )
	 	 {
	 	 	formobj.er_def_date_01.value= fnGetFullCurrentDate();
		  	
		  }
		  		
		return true;
	}
	else
	{
		return false;
	}
}

function validateFormESign(formobj)
{
	if (document.getElementById('userLogName')){
		if (!(validate_col( 'User Name',formobj.userLogName))){
			 return false; 
		}
		var userLogNameValue = formobj.userLogName.value;
		if(!userLogNameValue || userLogNameValue.length == 0) {
			/*alert("Incorrect User Name. Please enter again"); *****/
			alert(IncorrUsrName_EtrAgain);
        	formobj.userLogName.focus();
        	return false;
		}
	}

	if (!(validate_col( 'e-Signature',formobj.eSign))){
		return false;
	}
	  
	if(isNaN(formobj.eSign.value) == true) {
		/*alert("Incorrect e-Signature. Please enter again"); *****/
		alert(IncorrEsign_EtrAgain);
		formobj.eSign.focus();  return false;
	};
	return true;
}

function validateFormStatus(formobj)
{
	if (  ! (validate_col( 'Form Status',formobj.er_def_formstat)     )   ) 
	{
		return false; 
	}
	{
		return true;
	}	
}