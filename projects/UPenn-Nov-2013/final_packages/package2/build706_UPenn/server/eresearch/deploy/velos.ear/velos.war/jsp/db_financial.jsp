
<jsp:include page="localization.jsp" flush="true"/>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:include page="ui-include.jsp" flush="true"></jsp:include>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB"/><!--km-->
<jsp:useBean id="groupB" scope="page" class="com.velos.eres.web.group.GroupJB"/><!--km-->
<%@ page language = "java" import = "com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.EJBUtil,java.util.*,com.velos.eres.service.util.BrowserRows,com.velos.eres.service.util.Configuration,java.sql.*,com.velos.eres.business.common.*,com.velos.eres.service.util.StringUtil,com.velos.eres.service.util.*"%>

<title><%=LC.L_Fin_Dboard%><%--Financial Dashboard*****--%></title>
<link href="./js/tree/xmlTree.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="./js/chart/EJSChart.js"> </script>
<script type="text/javascript" src="./js/tree/xmlTree.js"></script>
<script>
var gChart;
function getElementsById(sId){
	var outArray = new Array();	
	if(typeof(sId)!='string' || !sId)
	{
		return outArray;
	};
	
	if(document.evaluate)
	{
		var xpathString = "//*[@id='" + sId.toString() + "']"
		var xpathResult = document.evaluate(xpathString, document, null, 0, null);
		while ((outArray[outArray.length] = xpathResult.iterateNext())) { }
		outArray.pop();
	}
	else if(document.all)
	{
		
		for(var i=0,j=document.all[sId].length;i<j;i++){
			outArray[i] = document.all[sId][i];
		}
		
	}else if(document.getElementsByTagName)
	{
	
		var aEl = document.getElementsByTagName( '*' );	
		for(var i=0,j=aEl.length;i<j;i+=1){
		
			if(aEl[i].id == sId )
			{
				outArray.push(aEl[i]);
			};
		};	
		
	};
	
	return outArray;
 }

function callAjaxGetFinancialData(nodeNo, snodeNo, linkVal){
	//if (parseInt(linkVal) == 0) return;
	var ao=new VELOS.ajaxObject("ajaxFinancialDataFetch.jsp", {
		urlData:"nodeNo="+nodeNo + "&snodeNo="+snodeNo ,
		reqType:"POST",
		outputElement: "Browser" 
		} 
	);
	ao.startRequest();
}

function showChart(num, name){
	var divChart = document.getElementById("onlyChart"+num);
	var dTable = document.getElementById('dataT');
	dTable.innerHTML ="";

	var tempDiv;
	for (var i=0; i<10; i++){
		tempDiv=document.getElementById("onlyChart"+i);
		tempDiv.style.display = "none";
	}
	divChart.style.display = "block";
	
	switch (num){
	case 0:
		//Financial Summary
		if (document.getElementById('foreCast7')!= undefined){
			callAjaxGetFinancialData(0,0, document.getElementById('budget_today').value);
			var totalAlerts = parseInt(document.getElementById('achNInvCount').value,10) + parseInt(document.getElementById('achPartInvCount').value,10)+ parseInt(document.getElementById('achOverInvCount').value,10);
			var totalDue = parseInt(document.getElementById('due_last_7d').value,10)+ parseInt(document.getElementById('due_last_15d').value,10)+ parseInt(document.getElementById('due_last_30d').value,10)+ parseInt(document.getElementById('due_gt30d').value,10);
			dTable.innerHTML = '<table width="100%" class="outline"><tr>'
			+ '	<td>'
			+ '		<table width="100%">'
			+ '			<tr>'
			+ " 			<th><%=LC.L_Todays_Summary%> <%--Today's Summary*****--%> </th>"
			+ '				<th align="center"> <%=LC.L_Count%><%--Count*****--%> </th>'
			+ '			</tr>'
			+ '			<tr class="browserOddRow">'
			+ '				<td><%=LC.L_Budgets_Modified%><%--Budgets Modified*****--%></td>'
			+ '				<td align="center">'+ document.getElementById('budget_today').value+'</td>'
			+ '			</tr>'
			+ '			<tr class="browserEvenRow">'
			+ '				<td><%=LC.L_Milestones_Achieved%><%--Milestones Achieved*****--%></td>'
			+ '				<td align="center">'+ document.getElementById('achieve_Today').value+'</td>'
			+ '			</tr>'
			+ '			<tr class="browserOddRow">'
			+ '				<td><%=LC.L_Invs_Created%> <%--Invoices Created*****--%></td>'
			+ '				<td align="center">'+ document.getElementById('invoice_today').value+'</td>'
			+ '			</tr>'
			+ '			<tr class="browserEvenRow">'
			+ '				<td><%=LC.L_Invoices_Due%><%--Invoices Due*****--%></td>'
			+ '				<td align="center">'+ document.getElementById('due_today').value+'</td>'
			+ '			</tr>'
			+ '			<tr class="browserOddRow">'
			+ '				<td><%=LC.L_Payments%><%--Payments*****--%></td>'
			+ '				<td align="center">'+ document.getElementById('payment_today').value+'</td>'
			+ '			</tr>'
			+ '		</table>'
			+ '	</td>'
			+ '</tr><tr>'
			+ '	<td>'
			+ '		<table width="100%">'
			+ '			<tr>'
			+ '				<th><%=LC.L_Alerts%><%--Alerts*****--%></th>'
			+ '				<th align="center"> <%=LC.L_Count%><%--Count*****--%> </th>'
			+ '			</tr>'
			+ '			<tr class="browserOddRow">'
			+ '				<td><%=LC.L_Receivable_Alert%><%--Receivable Alert*****--%></td>'
			+ '				<td align="center">'+ totalAlerts +'</td>'
			+ '			</tr>'
			+ '			<tr class="browserEvenRow">'
			+ '				<td><%=MC.M_InvPast_Due%><%--Invoices Past Due*****--%></td>'
			+ '				<td align="center">'+ totalDue +'</td>'
			+ '			</tr>'
			+ '		</table>'
			+ '	</td>'
			+ '</tr></table>';
		}
		break;
	case 1:
		//By Modification Date
		if (document.getElementById('budget_today')!= undefined){
			callAjaxGetFinancialData(1,0, document.getElementById('budget_today').value);
			if ((document.getElementById('budget_today').value != 0 || document.getElementById('budget_last_7d').value != 0) ||(document.getElementById('budget_last_15d').value != 0 || document.getElementById('budget_last_30d').value != 0)){
				dTable.innerHTML = '<table width ="100%">'
				+ '	<tr>'
				+ '		<th> <%=LC.L_Budgets_Modified%><%--Budgets Modified*****--%> </th>'
				+ '		<th align="center"> <%=LC.L_Count%><%--Count*****--%> </th>'
				+ '	</tr>'
				+ '	<tr class="browserOddRow">'
				+ '		<td><%=LC.L_Today%><%--Today*****--%></td>'
				+ '		<td align="center">'+ document.getElementById('budget_today').value +'</td>'
				+ '	</tr>'
				+ '	<tr class="browserEvenRow">'
				+ '		<td><%=LC.L_1_7Days%><%--1-7 Days*****--%></td>'
				+ '		<td align="center">'+ document.getElementById('budget_last_7d').value +'</td>'
				+ '	</tr>'
				+ '	<tr class="browserOddRow">'
				+ '		<td><%=LC.L_8_15Days%><%--8-15 Days*****--%></td>'
				+ '		<td align="center">'+ document.getElementById('budget_last_15d').value +'</td>'
				+ '	</tr>'
				+ '	<tr class="browserEvenRow">'
				+ '		<td><%=LC.L_16_30Days%><%--16-30 Days*****--%></td>'
				+ '		<td align="center">'+ document.getElementById('budget_last_30d').value +'</td>'
				+ '	</tr>'
				+ '</table>';
			}
		}
		break;
	case 2:
		//By Status
		if (document.getElementById('budget_W')!= undefined){
			callAjaxGetFinancialData(1,1, document.getElementById('budget_W').value);
			if (document.getElementById('budget_W').value != 0 || document.getElementById('budget_F').value!=0 || document.getElementById('budget_T').value !=0){
				dTable.innerHTML = '<table width ="100%">'
				+'	<tr>'
				+'		<table>'
				+'			<tr>'
				+'				<th> <%=LC.L_Status%><%--Status*****--%> </th>'
				+'				<th align="center"> <%=LC.L_Count%><%--Count*****--%> </th>'
				+'			</tr>'
				+'			<tr class="browserOddRow">'
				+'				<td><%=LC.L_Work_InProgress%><%--Work In Progress*****--%></td>'
				+'				<td align="center">'+ document.getElementById('budget_W').value +'</td>'
				+'			</tr>'
				+'			<tr class="browserEvenRow">'
				+'				<td><%=LC.L_Freeze%><%--Freeze*****--%></td>'
				+'				<td align="center">'+ document.getElementById('budget_F').value +'</td>'
				+'			</tr>'
				+'			<tr class="browserOddRow">'
				+'				<td><%=LC.L_Bgt_Template%><%--Budget Template*****--%></td>'
				+'				<td align="center">'+ document.getElementById('budget_T').value +'</td>'
				+'			</tr>'
				+'		</table>'
				+'	</tr><BR/>'
				+'</table>';
			}
		}
		break;
	case 3:
		//Achievements
		if (document.getElementById('Year_current')!= undefined){
			callAjaxGetFinancialData(2,0, document.getElementById('Year_current').value);
		}
		break;
	case 4:
		//Forecasting
		if (document.getElementById('Year_currt')!= undefined){
			callAjaxGetFinancialData(2,1, document.getElementById('Year_current').value);
		}
		break;
	case 5:
		//Receivables
		if (document.getElementById('achNInvCount')!= undefined){
			callAjaxGetFinancialData(2,2, document.getElementById('achNInvCount').value);
			dTable.innerHTML = '<table width ="100%"><tr>'
			+ '	<th> <%=LC.L_Status%><%--Status*****--%> </th>'
			+ '	<th align="center"> <%=LC.L_Count%><%--Count*****--%> </th>'
			+ '</tr>'
			+ '<tr class="browserEvenRow">'
			+ '	<td><%=LC.L_Achv_NtInvoiced%><%--Achieved/ Not Invoiced*****--%></td>'
			+ '	<td align="center">'+ document.getElementById('achNInvCount').value +'</td>'
			+ '</tr>'
			+ '<tr class="browserOddRow">'
			+ '	<td><%=LC.L_Achv_PartInvoiced%><%--Achieved/ Partially Invoiced*****--%></td>'
			+ '	<td align="center">'+ document.getElementById('achPartInvCount').value +'</td>'
			+ '</tr>'
			+ '<tr class="browserEvenRow">'
			+ '	<td><%=LC.L_Ach_OverInvoiced%><%--Achieved/ Over Invoiced*****--%></td>'
			+ '	<td align="center">'+ document.getElementById('achOverInvCount').value +'</td>'
			+ '</tr></table>';
		}
		break;
	case 6:
		//By Invoice Date
		if (document.getElementById('invoice_today')!= undefined){
			callAjaxGetFinancialData(3,0, document.getElementById('invoice_today').value);
			if ((document.getElementById('invoice_today').value != 0 || document.getElementById('invoice_last_7d').value != 0) ||(document.getElementById('invoice_last_15d').value != 0 || document.getElementById('invoice_last_30d').value != 0)){
				dTable.innerHTML = '<table width ="100%">'
				+ '	<tr>'
				+ '		<th> <%=LC.L_Invs_Created%> <%--Invoices Created*****--%> </th>'
				+ '		<th align="center"> <%=LC.L_Count%><%--Count*****--%> </th>'
				+ '	</tr>'
				+ '	<tr class="browserOddRow">'
				+ '		<td><%=LC.L_Today%><%--Today*****--%></td>'
				+ '		<td align="center">'+ document.getElementById('invoice_today').value +'</td>'
				+ '	</tr>'
				+ '	<tr class="browserEvenRow">'
				+ '		<td<%=LC.L_1_7Days%><%--1-7 Days*****--%></td>'
				+ '		<td align="center">'+ document.getElementById('invoice_last_7d').value +'</td>'
				+ '	</tr>'
				+ '	<tr class="browserOddRow">'
				+ '		<td><%=LC.L_8_15Days%><%--8-15 Days*****--%></td>'
				+ '		<td align="center">'+ document.getElementById('invoice_last_15d').value +'</td>'
				+ '	</tr>'
				+ '	<tr class="browserEvenRow">'
				+ '		<td><%=LC.L_16_30Days%><%--16-30 Days*****--%></td>'
				+ '		<td align="center">'+ document.getElementById('invoice_last_30d').value +'</td>'
				+ '	</tr>'
				+ '</table>';
			}
		}
		break;
	case 7:
		//Past due
		if (document.getElementById('due_today')!= undefined){
			callAjaxGetFinancialData(3,1, document.getElementById('due_today').value);
			if (document.getElementById('due_last_7d').value!=0 || document.getElementById('due_last_15d').value !=0 || document.getElementById('due_last_30d').value !=0 || document.getElementById('due_gt30d').value !=0){		
				dTable.innerHTML = '<table width ="100%">'
				+'	<tr>'
				+'		<table >'
				+'			<tr>'
				+'				<th> <%=LC.L_Past_DueFor%><%--Past Due For*****--%> </th>'
				+'				<th align="center"> <%=LC.L_Count%><%--Count*****--%> </th>'
				+'			</tr>'
				+'			<tr class="browserOddRow">'
				+'				<td><%=LC.L_1_7Days%><%--1-7 Days*****--%></td>'
				+'				<td align="center">'+ document.getElementById('due_last_7d').value +'</td>'
				+'			</tr>'
				+'			<tr class="browserEvenRow">'
				+'				<td><%=LC.L_8_15Days%><%--8-15 Days*****--%></td>'
				+'				<td align="center">'+ document.getElementById('due_last_15d').value +'</td>'
				+'			</tr>'
				+'			<tr class="browserOddRow">'
				+'				<td><%=LC.L_16_30Days%><%--16-30 Days*****--%></td>'
				+'				<td align="center">'+ document.getElementById('due_last_30d').value +'</td>'
				+'			</tr>'
				+'			<tr class="browserEvenRow">'
				+'				<td><%=MC.M_GtThan_30Days%><%--Greater Than 30 Days*****--%></td>'
				+'				<td align="center">'+ document.getElementById('due_gt30d').value +'</td>'
				+'			</tr>'
				+'		</table>'
				+'	</tr><BR/>'
				+'</table>';
			}
		}
		break;
	case 8:
		//By Payment Date
		if (document.getElementById('payment_today')!= undefined){
			callAjaxGetFinancialData(4,0, document.getElementById('payment_today').value);
			if ((document.getElementById('payment_today').value != 0 || document.getElementById('payment_last_7d').value != 0) ||(document.getElementById('payment_last_15d').value != 0 || document.getElementById('payment_last_30d').value != 0)){
				dTable.innerHTML = '<table width ="100%">'
				+ '	<tr>'
				+ '		<th> <%=LC.L_Payments_Mode%><%--Payments Made*****--%> </th>'
				+ '		<th align="center"> <%=LC.L_Count%><%--Count*****--%> </th>'
				+ '	</tr>'
				+ '	<tr class="browserOddRow">'
				+ '		<td><%=LC.L_Today%><%--Today*****--%></td>'
				+ '		<td align="center">'+ document.getElementById('payment_today').value +'</td>'
				+ '	</tr>'
				+ '	<tr class="browserEvenRow">'
				+ '		<td><%=LC.L_1_7Days%><%--1-7 Days*****--%></td>'
				+ '		<td align="center">'+ document.getElementById('payment_last_7d').value +'</td>'
				+ '	</tr>'
				+ '	<tr class="browserOddRow">'
				+ '		<td><%=LC.L_8_15Days%><%--8-15 Days*****--%></td>'
				+ '		<td align="center">'+ document.getElementById('payment_last_15d').value +'</td>'
				+ '	</tr>'
				+ '	<tr class="browserEvenRow">'
				+ '		<td><%=LC.L_16_30Days%><%--16-30 Days*****--%></td>'
				+ '		<td align="center">'+ document.getElementById('payment_last_30d').value +'</td>'
				+ '	</tr>'
				+ '</table>';
			}
		}
		break;
	case 9:
		//By amount
		if (document.getElementById('payment_today')!= undefined){
			callAjaxGetFinancialData(4,1, document.getElementById('amount_today').value);
			if ((document.getElementById('amount_today').value > 0 || document.getElementById('amount_last_7d').value > 0) ||(document.getElementById('amount_last_15d').value > 0 || document.getElementById('amount_last_30d').value > 0)){
				dTable.innerHTML = '<table width ="100%">'
				+ '	<tr>'
				+ '		<th> <%=LC.L_Payments_Mode%><%--Payments Made*****--%> </th>'
				+ '		<th align="center"> <%=LC.L_Amount%><%--Amount*****--%> </th>'
				+ '	</tr>'
				+ '	<tr class="browserOddRow">'
				+ '		<td><%=LC.L_Today%><%--Today*****--%></td>'
				+ '		<td align="center">'+ document.getElementById('amount_today').value +'</td>'
				+ '	</tr>'
				+ '	<tr class="browserEvenRow">'
				+ '		<td><%=LC.L_1_7Days%><%--1-7 Days*****--%></td>'
				+ '		<td align="center">'+ document.getElementById('amount_last_7d').value +'</td>'
				+ '	</tr>'
				+ '	<tr class="browserOddRow">'
				+ '		<td><%=LC.L_8_15Days%><%--8-15 Days*****--%></td>'
				+ '		<td align="center">'+ document.getElementById('amount_last_15d').value +'</td>'
				+ '	</tr>'
				+ '	<tr class="browserEvenRow">'
				+ '		<td><%=LC.L_16_30Days%><%--16-30 Days*****--%></td>'
				+ '		<td align="center">'+ document.getElementById('amount_last_30d').value +'</td>'
				+ '	</tr>'
				+ '</table>';
			}
		}
		break;
	}
	return;
}

function createChart(num, name){
	var chart;
	
	/*if (gChart!= undefined){
		alert('I am here');
		gChart.remove();
	}
	if (isNaN(gChart)){
		gChart = new EJSC.Chart("onlyChart",{
			show_legend: false,
			allow_zoom: false,
		 	show_mouse_position: false
		}); 
	}*/
	//divChart.parentNode.removeChild(divChart);
	//var divParent = document.getElementById("onlyChartParent");
	//divParent.innerHTML = '<div id="onlyChart" class="chart" style="border-width: 1px; border-color: #0000ff; border-style: solid; width: 450px; height: 200px; display:none"></div>';
	var divChart = document.getElementById("onlyChart"+num);
	
	switch (num){
	case 0:
		//Financial Summary
		if (document.getElementById('foreCast7')!= undefined){
			if (document.getElementById('foreCast7').value != "'00/00'|0"){
				chart = new EJSC.Chart("onlyChart"+num,{
					show_legend: false,
					allow_zoom: false,
					show_mouse_position: false,
					show_hints: true, 
					title: '<%=LC.L_Fin_Summary%><%--Financial Summary*****--%> '
				}); 
				//gChart = chart;
				chart.addSeries(new EJSC.LineSeries( 
					new EJSC.CSVStringDataHandler(document.getElementById('foreCast7').value), 
					{ title: "<%=MC.M_DueNxt_7Days%><%--Due Next 7 Days*****--%>" } 
				));
			}else{
				divChart.innerHTML ="<b><%=MC.M_NoDataDboard_FinSumm%><%--No data for dashboard- Financial Summary*****--%></b>";
			}
		}else{
			divChart.innerHTML ="<b><%=MC.M_FethDataDboard_FinSumm%><%--Fetching Data for dashboard- Financial Summary*****--%> ....</b>";
		}
		break;
	case 1:
		//By Modification Date
		if (document.getElementById('budget_today')!= undefined){
			if ((document.getElementById('budget_today').value != 0 || document.getElementById('budget_last_7d').value != 0) ||(document.getElementById('budget_last_15d').value != 0 || document.getElementById('budget_last_30d').value != 0)){
				//Set Y-axis Xtreme
				var yXtreme=0;
				if (parseInt(document.getElementById('budget_today').value,10) > yXtreme){
					yXtreme = parseInt(document.getElementById('budget_today').value,10);
				}
				if (parseInt(document.getElementById('budget_last_7d').value,10) > yXtreme){
					yXtreme = parseInt(document.getElementById('budget_last_7d').value,10);
				}
				if (parseInt(document.getElementById('budget_last_15d').value,10) > yXtreme){
					yXtreme = parseInt(document.getElementById('budget_last_15d').value,10);
				}
				if (parseInt(document.getElementById('budget_last_30d').value,10) > yXtreme){
					yXtreme = parseInt(document.getElementById('budget_last_30d').value,10);
				}
				var paramArray = [name];
				var chart = new EJSC.Chart("onlyChart"+num, { 
					show_legend: false,
					allow_zoom: false,
					show_mouse_position: false,
					title: getLocalizedMessageString("L_Bgts",paramArray)/*Budgets {0}*****/,
					x_axis_size: 24 
				}); 
				//gChart = chart;
				chart.setYExtremes(0, yXtreme);
				chart.addSeries(new EJSC.BarSeries( 
					new EJSC.ArrayDataHandler([ 
				    ['<%=LC.L_Today%><%--Today*****--%>',document.getElementById('budget_today').value],
				    ['<%=LC.L_1_7Days%><%--1-7 Days*****--%>',document.getElementById('budget_last_7d').value],
				    ['<%=LC.L_8_15Days%><%--8-15 Days*****--%>',document.getElementById('budget_last_15d').value],
				    ['<%=LC.L_16_30Days%><%--16-30 Days*****--%>',document.getElementById('budget_last_30d').value]
				   ]), 
				   { 
				      groupedBars: false, 
				      color: 'rgb(200,0,0)', 
				      opacity: 0,
				      title:getLocalizedMessageString("L_Bgts",paramArray)/*Budgets {0}*****/
				    } 
				    ) 
				); 
			}else{
				var paramArray = [name];
				divChart.innerHTML =getLocalizedMessageString("M_NoDataDboard_Bgt",paramArray)/*<b>No data for dashboard- Budgets {0}</b>*****/;
			}
		}else{
			var paramArray = [name];
			divChart.innerHTML =getLocalizedMessageString("M_FetchData_DboardBgt",paramArray)/*Fetching Data for dashboard- Budgets {0}*****/;
		}
		break;
	case 2:
		//By Status
		if (document.getElementById('budget_W')!= undefined){
			if (document.getElementById('budget_W').value != 0 || document.getElementById('budget_F').value!=0 || document.getElementById('budget_T').value !=0){
				 var paramArray = [name];
				chart = new EJSC.Chart("onlyChart"+num, { 
				    show_legend: false,
				    allow_zoom: false,
				    show_mouse_position: false,
				    show_hints: true, 
				    title: getLocalizedMessageString("L_Bgts",paramArray)/*Budgets {0}*****/
				}); 
				//gChart = chart;
				chart.addSeries(new EJSC.PieSeries( 
				    new EJSC.CSVStringDataHandler( document.getElementById('budget_W').value + '|"<%=LC.L_Work_InProgress%><%--Work In Progress--%>",'
				    + document.getElementById('budget_F').value + '|"<%=LC.L_Freeze%><%--Freeze*****--%>",'
				    + document.getElementById('budget_T').value + '|"<%=LC.L_Bgt_Template%><%--Budget Template*****--%>"'),
				  	{ 
				      defaultColors: [ 
				          'rgb(35,107,142)',  //SteelBlue 
				          'rgb(112,219,147)',  //AquaMarine 
				          'rgb(255,140,0)'  //DarkOrange 
				      ],
				      title:getLocalizedMessageString("L_Bgts",paramArray)/*Budgets {0}*****/
				    } 
				));
			} else {
				var paramArray = [name];
				divChart.innerHTML =getLocalizedMessageString("M_NoDataDboard_Bgt",paramArray)/*<b>No data for dashboard- Budgets {0}</b>*****/;
			}
		}else{
			var paramArray = [name];
			divChart.innerHTML =getLocalizedMessageString("M_FetchData_DboardBgt",paramArray)/*Fetching Data for dashboard- Budgets {0}*****/;
		}
		break;
	case 3:
		//Achievements
		if (document.getElementById('Year_current')!= undefined){
			if (document.getElementById('Year_current').value != "" || document.getElementById('Year_last1').value != "" || document.getElementById('Year_last2').value != "" || document.getElementById('Year_last3').value != "" || document.getElementById('Year_last4').value != ""){
				var Year = parseInt(document.getElementById('ThisYear').value,10);
				var paramArray1 = [Year];
				var paramArray2 = [(Year-1)];
				var paramArray3=  [(Year-2)];
			    var paramArray4 = [(Year-3)];
			    var paramArray5 = [(Year-4)];
				var chart = new EJSC.Chart("onlyChart"+num,{
					show_legend: true,
				    allow_zoom: false,
				    show_mouse_position: false,
				    title: '<%=LC.L_Achievements%><%--Achievements*****--%> '
				}); 
				//gChart = chart;
				chart.addSeries(new EJSC.LineSeries( 
					new EJSC.CSVStringDataHandler(document.getElementById('Year_current').value), 
				      {
				    title: getLocalizedMessageString("L_Year_Hyp",paramArray1)/*Year - Year*****/
					} 
				));
				chart.addSeries(new EJSC.LineSeries( 
					new EJSC.CSVStringDataHandler(document.getElementById('Year_last1').value), 
				      {
					     title: getLocalizedMessageString("L_Year_Hyp",paramArray2)/*Year - (Year-1)*****/
					} 
				));
				chart.addSeries(new EJSC.LineSeries( 
					new EJSC.CSVStringDataHandler(document.getElementById('Year_last2').value), 
				    {
					     title: getLocalizedMessageString("L_Year_Hyp",paramArray3)/*Year - (Year-2)*****/
					} 
				));
				chart.addSeries(new EJSC.LineSeries( 
					new EJSC.CSVStringDataHandler(document.getElementById('Year_last3').value), 
				    				   {
					     title: getLocalizedMessageString("L_Year_Hyp",paramArray4)/*Year - (Year-3)*****/
					} 
				));
				chart.addSeries(new EJSC.LineSeries( 
					new EJSC.CSVStringDataHandler(document.getElementById('Year_last4').value), 
				      {
					     title: getLocalizedMessageString("L_Year_Hyp",paramArray5)/*Year - (Year-4)*****/
					} 
				));
			}else{
				divChart.innerHTML ="<b><%=MC.M_NoDataFor_DboardAchv%><%--No data for dashboard- Achievements*****--%></b>";
			}
		}else{
			divChart.innerHTML ="<b><%=MC.M_FtchData_DboardAchv%><%--Fetching Data for dashboard- Achievements*****--%> ....</b>";
		}
		break;
	case 4:
		//Forecasting
		if (document.getElementById('Year_currt')!= undefined){
			if (document.getElementById('Year_currt').value != "" || document.getElementById('Year_next1').value != "" || document.getElementById('Year_next2').value != "" || document.getElementById('Year_next3').value != "" || document.getElementById('Year_next4').value != ""){
				var Year = parseInt(document.getElementById('ThisYear').value,10);
				var paramArray1 = [Year];
				var paramArray2 = [(Year+1)];
				var paramArray3=  [(Year+2)];
			    var paramArray4 = [(Year+3)];
			    var paramArray5 = [(Year+4)];
				chart = new EJSC.Chart("onlyChart"+num,{
					show_legend: true,
				    allow_zoom: false,
				    show_mouse_position: false,
				    title: '<%=LC.L_Forecasting%><%--Forecasting*****--%> '
				}); 
				//gChart = chart;
				chart.addSeries(new EJSC.LineSeries( 
					new EJSC.CSVStringDataHandler(document.getElementById('Year_currt').value), 
				    {
				    title: getLocalizedMessageString("L_Year_Hyp",paramArray1)/*Year - Year*****/
					} 
				));
				chart.addSeries(new EJSC.LineSeries( 
					new EJSC.CSVStringDataHandler(document.getElementById('Year_next1').value), 
				        {
					     title: getLocalizedMessageString("L_Year_Hyp",paramArray2)/*Year - (Year+1)*****/
					} 
				));
				chart.addSeries(new EJSC.LineSeries( 
					new EJSC.CSVStringDataHandler(document.getElementById('Year_next2').value), 
				       {
					     title: getLocalizedMessageString("L_Year_Hyp",paramArray3)/*Year - (Year+2)*****/
					} 
				));
				chart.addSeries(new EJSC.LineSeries( 
					new EJSC.CSVStringDataHandler(document.getElementById('Year_next3').value), 
				      {
					     title: getLocalizedMessageString("L_Year_Hyp",paramArray4)/*Year - (Year+3)*****/
					} 
				));
				chart.addSeries(new EJSC.LineSeries( 
					new EJSC.CSVStringDataHandler(document.getElementById('Year_next4').value), 
				       {
					     title: getLocalizedMessageString("L_Year_Hyp",paramArray5)/*Year - (Year+4)*****/
					} 
				));
			}else{
				divChart.innerHTML ="<b><%=MC.M_NoData_DboardFcst%><%--No data for dashboard- Forecasting*****--%></b>";
			}
		}else{
			divChart.innerHTML ="<b><%=MC.M_FtchData_DboardFcst%><%--Fetching Data for dashboard- Forecasting*****--%> ....</b>";
		}
		break;
	case 5:
		//Receivables
		if (document.getElementById('achNInvCount')!= undefined){
			chart = new EJSC.Chart("onlyChart"+num, { 
			    show_legend: false,
			    allow_zoom: false,
				show_mouse_position: false,
			    title: name
			}); 
			//gChart = chart;
			chart.addSeries(new EJSC.PieSeries( 
			    new EJSC.ArrayDataHandler([[document.getElementById('achNInvCount').value,"<%=LC.L_Achv_NtInvoiced%><%--Achieved/ Not Invoiced*****--%>"],
			    [document.getElementById('achPartInvCount').value,"<%=LC.L_Achv_PartInvoiced%><%--Achieved/ Partially Invoiced*****--%>"],
			    [document.getElementById('achOverInvCount').value,"<%=LC.L_Ach_OverInvoiced%><%--Achieved/ Over Invoiced*****--%>"]] 
			  ),
			  { 
			      defaultColors: [ 
			          'rgb(255,255,0)',  //Yellow 
			          'rgb(112,219,147)',  //AquaMarine 
			          'rgb(255,140,0)',  //DarkOrange 
			          'rgb(178,34,34)'  //FireBrick 
			      ],
			      title: name
			    } 
			));
		}else{
			var paramArray = [name];
			divChart.innerHTML =getLocalizedMessageString("M_FetchDboard",paramArray)/*<b>Fetching Data for dashboard {0} ....</b>*****/;
		}
		break;
	case 6:
		//By Invoice Date
		if (document.getElementById('invoice_today')!= undefined){
			if ((document.getElementById('invoice_today').value != 0 || document.getElementById('invoice_last_7d').value != 0) ||(document.getElementById('invoice_last_15d').value != 0 || document.getElementById('invoice_last_30d').value != 0)){
				//Set Y-axis Xtreme
				var yXtreme=0;
				var paramArray = [name];
				if (parseInt(document.getElementById('invoice_today').value,10) > yXtreme){
					yXtreme = parseInt(document.getElementById('invoice_today').value,10);
				}
				if (parseInt(document.getElementById('invoice_last_7d').value,10) > yXtreme){
					yXtreme = parseInt(document.getElementById('invoice_last_7d').value,10);
				}
				if (parseInt(document.getElementById('invoice_last_15d').value,10) > yXtreme){
					yXtreme = parseInt(document.getElementById('invoice_last_15d').value,10);
				}
				if (parseInt(document.getElementById('invoice_last_30d').value,10) > yXtreme){
					yXtreme = parseInt(document.getElementById('invoice_last_30d').value,10);
				}
				
				chart = new EJSC.Chart("onlyChart"+num, { 
				    show_legend: false,
				    allow_zoom: false,
				    show_mouse_position: false,
				    title: getLocalizedMessageString("L_Invoices_Dyn",paramArray)/*Invoices {0}*****/,
				    x_axis_size: 24 
				}); 
				//gChart = chart;
				chart.setYExtremes(0, yXtreme);
				chart.addSeries(new EJSC.BarSeries( 
					new EJSC.ArrayDataHandler([ 
				    ['<%=LC.L_Today%><%--Today*****--%>',document.getElementById('invoice_today').value],
				    ['<%=LC.L_1_7Days%><%--1-7 Days*****--%>',document.getElementById('invoice_last_7d').value],
				    ['<%=LC.L_8_15Days%><%--8-15 Days*****--%>',document.getElementById('invoice_last_15d').value],
				    ['<%=LC.L_16_30Days%><%--16-30 Days*****--%>',document.getElementById('invoice_last_30d').value]
				   ]), 
				   { 
				      groupedBars: false, 
				      color: 'rgb(200,0,0)', 
				      opacity: 0,
				      title:getLocalizedMessageString("L_Invoices_Dyn",paramArray)/*Invoices {0}*****/
				    } 
				    ) 
				); 
			}else{
				var paramArray = [name];
				divChart.innerHTML =getLocalizedMessageString("M_NoDataDboard_Inv",paramArray)/*<b>No data for dashboard- Invoices {0}</b>*****/;
			}
		}else{
			var paramArray = [name];
			divChart.innerHTML =getLocalizedMessageString("M_FetchDboard_Inv",paramArray)/*<b>Fetching Data for dashboard- Invoices {0} ....</b>*****/;
		}
		break;
	case 7:
		//Past due
		if (document.getElementById('due_today')!= undefined){
			if (document.getElementById('due_last_7d').value!=0 || document.getElementById('due_last_15d').value !=0 || document.getElementById('due_last_30d').value !=0 || document.getElementById('due_gt30d').value !=0){
				var paramArray = [name];
				chart = new EJSC.Chart("onlyChart"+num, { 
				    show_legend: false,
				    allow_zoom: false,
				    show_mouse_position: false,
				    title: getLocalizedMessageString("L_Invoices_Dyn",paramArray)/*Invoices {0}*****/
				}); 
				//gChart = chart;
				chart.addSeries(new EJSC.PieSeries( 
				    new EJSC.CSVStringDataHandler( document.getElementById('due_last_7d').value + '|"<%=LC.L_1_7Days%><%--1-7 Days*****--%>",'
				    + document.getElementById('due_last_15d').value + '|"<%=LC.L_8_15Days%><%--8-15 Days*****--%>",'
				    + document.getElementById('due_last_30d').value + '|"<%=LC.L_16_30Days%><%--16-30 Days*****--%>",'
				    + document.getElementById('due_gt30d').value + '|"<%=MC.M_GtThan_30Days%><%--Greater Than 30 Days*****--%>"'),
				  { 
				      defaultColors: [ 
				          'rgb(255,255,0)',  //Yellow 
				          'rgb(35,107,142)',  //SteelBlue 
				          'rgb(112,219,147)',  //AquaMarine 
				          'rgb(255,140,0)',  //DarkOrange 
				          'rgb(153,50,204)',  //DarkOrchid 
				          'rgb(178,34,34)'  //FireBrick 
				      ],
				      title: name
				    } 
				));
			} else {
				var paramArray = [name];
				divChart.innerHTML =getLocalizedMessageString("M_NoDataDboard_Inv",paramArray)/*<b>No data for dashboard- Invoices {0}</b>*****/;
			}
		}else{
			var paramArray = [name];
			divChart.innerHTML =getLocalizedMessageString("M_FetchDboard_Inv",paramArray)/*<b>Fetching Data for dashboard- Invoices {0} ....</b>*****/;
		}
		break;
	case 8:
		//By Payment Date
		if (document.getElementById('payment_today')!= undefined){
			if ((document.getElementById('payment_today').value != 0 || document.getElementById('payment_last_7d').value != 0) ||(document.getElementById('payment_last_15d').value != 0 || document.getElementById('payment_last_30d').value != 0)){
				//Set Y-axis Xtreme
				var paramArray = [name];
				var yXtreme=0;
				if (parseInt(document.getElementById('payment_today').value,10) > yXtreme){
					yXtreme = parseInt(document.getElementById('payment_today').value,10);
				}
				if (parseInt(document.getElementById('payment_last_7d').value,10) > yXtreme){
					yXtreme = parseInt(document.getElementById('payment_last_7d').value,10);
				}
				if (parseInt(document.getElementById('payment_last_15d').value,10) > yXtreme){
					yXtreme = parseInt(document.getElementById('payment_last_15d').value,10);
				}
				if (parseInt(document.getElementById('payment_last_30d').value,10) > yXtreme){
					yXtreme = parseInt(document.getElementById('payment_last_30d').value,10);
				}
				chart = new EJSC.Chart("onlyChart"+num, { 
				    show_legend: false,
				    allow_zoom: false,
				    show_mouse_position: false,
				    title:  getLocalizedMessageString("L_Payments_Dyn",paramArray)/*Payments {0}*****/,
				    x_axis_size: 24 
				}); 
				//gChart = chart;
				chart.setYExtremes(0, yXtreme);
				chart.addSeries(new EJSC.BarSeries( 
					new EJSC.ArrayDataHandler([ 
				    ['<%=LC.L_Today%><%--Today*****--%>',document.getElementById('payment_today').value],
				    ['<%=LC.L_1_7Days%><%--1-7 Days*****--%>',document.getElementById('payment_last_7d').value],
				    ['<%=LC.L_8_15Days%><%--8-15 Days*****--%>',document.getElementById('payment_last_15d').value],
				    ['<%=LC.L_16_30Days%><%--16-30 Days*****--%>',document.getElementById('payment_last_30d').value]
				   ]), 
				   { 
				      groupedBars: false, 
				      color: 'rgb(200,0,0)', 
				      opacity: 0,
				      title: getLocalizedMessageString("L_Payments_Dyn",paramArray)/*Payments {0}*****/
				    } 
				    ) 
				); 
			}else{
				var paramArray = [name];
				divChart.innerHTML =getLocalizedMessageString("M_NoDataDboard_Payment",paramArray)/*<b>No data for dashboard- Payments {0}</b>*****/;
			}
		}else{
			var paramArray = [name];
			divChart.innerHTML =getLocalizedMessageString("M_FetchDboard_Payment",paramArray)/*<b>Fetching Data for dashboard- Payments {0} ....</b>*****/;
		}
		break;
	case 9:
		//By amount
		if (document.getElementById('payment_today')!= undefined){
			if ((document.getElementById('amount_today').value > 0 || document.getElementById('amount_last_7d').value > 0) ||(document.getElementById('amount_last_15d').value > 0 || document.getElementById('amount_last_30d').value > 0)){
				//Set Y-axis Xtreme
				var yXtreme=0;
				var paramArray = [name];
				if (parseFloat(document.getElementById('amount_today').value) > yXtreme){
					yXtreme = parseFloat(document.getElementById('amount_today').value);
				}
				if (parseFloat(document.getElementById('amount_last_7d').value) > yXtreme){
					yXtreme = parseFloat(document.getElementById('amount_last_7d').value);
				}
				if (parseFloat(document.getElementById('amount_last_15d').value) > yXtreme){
					yXtreme = parseFloat(document.getElementById('amount_last_15d').value);
				}
				if (parseFloat(document.getElementById('amount_last_30d').value) > yXtreme){
					yXtreme = parseFloat(document.getElementById('amount_last_30d').value);
				}
				
				chart = new EJSC.Chart("onlyChart"+num, { 
				    show_legend: false,
				    allow_zoom: false,
				    show_mouse_position: false,
				    title:  getLocalizedMessageString("L_Payments_Dyn",paramArray)/*Payments {0}*****/,
				    x_axis_size: 24 
				}); 
				//gChart = chart;
				chart.setYExtremes(0, yXtreme);
				chart.addSeries(new EJSC.BarSeries( 
					new EJSC.ArrayDataHandler([ 
				    ['<%=LC.L_Today%><%--Today*****--%>',document.getElementById('amount_today').value],
				    ['<%=LC.L_1_7Days%><%--1-7 Days*****--%>',document.getElementById('amount_last_7d').value],
				    ['<%=LC.L_8_15Days%><%--8-15 Days*****--%>',document.getElementById('amount_last_15d').value],
				    ['<%=LC.L_16_30Days%><%--16-30 Days*****--%>',document.getElementById('amount_last_30d').value]
				   ]), 
				   { 
				      groupedBars: false, 
				      color: 'rgb(35,107,142)', 
				      opacity: 0,
				      title: getLocalizedMessageString("L_Payments_Dyn",paramArray)/*Payments {0}*****/
				    } 
				    ) 
				); 
			}else{
				var paramArray = [name];
				divChart.innerHTML =getLocalizedMessageString("M_NoDataDboard_Payment",paramArray)/*<b>No data for dashboard- Payments {0}</b>*****/;
			}
		}else{
			var paramArray = [name];
			divChart.innerHTML =getLocalizedMessageString("M_FetchDboard_Payment",paramArray)/*<b>Fetching Data for dashboard- Payments {0} ....</b>*****/;
		}
		break;
	}
	return;
}
function toggleBranch(arrayID, index){
	var linkArray = getElementsById(arrayID);
	for(var i = 0; i < linkArray.length; i++){
		if (i != index){
			linkArray[i].style.display = "none";
		} else {
			if (linkArray[i].style.display == "block"){
				linkArray[i].style.display = "none";
			} else{
				linkArray[i].style.display = "block";
				linkArray[i].focus;
			}
		}
	}
}
function confirmBox(name){
	var paramArray = [name];
    if (confirm(getLocalizedMessageString("M_Want_DelStd",paramArray))) {/* if (confirm("Do you want to delete <%=LC.Std_Study%> "+ name+"?" )) {*****/
		return true;
	}  else {
		return false;
	}
}//km

function opensvwin(id) {
     windowName = window.open("studyview.jsp?studyVPk="+ id,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=500,height=400")
	 windowName.focus();
;}
</script>

<body>
<%
	//Declarations
	String src="tdMenuBarItem3";
	String studySql;
	String countSql;

	long rowsPerPage=0;
	long totalPages=0;	
	long rowsReturned = 0;
	long showPages = 0;
	long totalRows = 0;	 
	long firstRec = 0;
	long lastRec = 0;	 
	int openCount = 0;
	int resolvedCount = 0;
	int reopenCount = 0;
	int closedCount = 0;
	int enrolled = 0;

	String studyNumber = null;
	String studyTitle = null;
	String studyStatus = null;
	String studyActualDt = null;
	String studyIRB = null;
	String studyIRB2 = null;
	String studySite = null;
	String studyPI = null;

	int studyId=0;
	boolean hasMore = false;
	boolean hasPrevious = false;
	CodeDao cd = new CodeDao();
	String codeLst= cd.getValForSpecificSubType();
	HttpSession tSession = request.getSession(true); 
	String userId= (String) tSession.getValue("userId");
	
	String pagenum = "";
	int curPage = 0;
	String orderBy = "";
	orderBy = request.getParameter("orderBy");

	String orderType = "";
	orderType = request.getParameter("orderType");

	BrowserRows pr;
	BrowserRows br;
	
	long startPage = 1;
	long cntr = 0;
	String pagenumView = request.getParameter("pageView");
	if (pagenumView == null) {pagenumView = "1";}
	int curPageView = EJBUtil.stringToNum(pagenumView);
	
	long startPageView = 1;
	long cntrView = 0;
	String defGroup;
	String groupName;
	String rightSeq;
	
	String orderByView = "";
	orderByView = request.getParameter("orderByView");

	String orderTypeView = "";
	orderTypeView = request.getParameter("orderTypeView");
	
	int usrId;
	CodeDao cd1 = new CodeDao();

	ArrayList aRightSeq ;
	int iRightSeq ;
	
	int pageRight = 0;
	usrId = EJBUtil.stringToNum(userId);
	userB.setUserId(usrId);
	userB.getUserDetails();
	String defaultGrp=userB.getUserGrpDefault();
	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("NPROTOCOL"));
	String accountId= (String) tSession.getValue("accountId");
%>
<%
	if (sessionmaint.isValidSession(tSession)) {
		if (pageRight > 0 )	{
			String foreCast7="'00/00'|0";
			int achieve_Today =0;
			
			String queryCount = "SELECT COUNT(*) as achToday "
			+ "FROM erv_mileachieved a, ER_STUDY b "
			+ "WHERE pk_study = fk_study "
			+ "AND (EXISTS (SELECT * FROM ER_STUDYTEAM bb "
			+ "WHERE pk_study = bb.fk_study AND bb.fk_user = "+userId+" AND NVL(studyteam_status,'Active') = 'Active') "
			+ "OR Pkg_Phi.F_Is_Superuser("+userId+", a.fk_study) = 1) "
			+ "AND TO_CHAR(ach_date, 'mm/dd/yyyy') = TO_CHAR(SYSDATE, 'mm/dd/yyyy')";
			
			BrowserRows query = new BrowserRows();
			query.getPageRows(1,10,queryCount,50,"select 1 from dual","1","asc");
			achieve_Today = EJBUtil.stringToNum(query.getBValues(1,"achToday"));
			
			queryCount = "SELECT "
			+ " ','''|| SUBSTR(TO_CHAR(TO_DATE(SYSDATE+1),'mm/dd/yyyy'),1,5) || '''|' || COUNT(CASE WHEN TO_DATE(actual_schdate) = TO_DATE(SYSDATE+1) THEN 1 END) ||"
			+ " ','''|| SUBSTR(TO_CHAR(TO_DATE(SYSDATE+2),'mm/dd/yyyy'),1,5) || '''|' || COUNT(CASE WHEN TO_DATE(actual_schdate) = TO_DATE(SYSDATE+2) THEN 1 END) ||"
			+ " ','''|| SUBSTR(TO_CHAR(TO_DATE(SYSDATE+3),'mm/dd/yyyy'),1,5) || '''|' || COUNT(CASE WHEN TO_DATE(actual_schdate) = TO_DATE(SYSDATE+3) THEN 1 END) ||"
			+ " ','''|| SUBSTR(TO_CHAR(TO_DATE(SYSDATE+4),'mm/dd/yyyy'),1,5) || '''|' || COUNT(CASE WHEN TO_DATE(actual_schdate) = TO_DATE(SYSDATE+4) THEN 1 END) ||"
			+ " ','''|| SUBSTR(TO_CHAR(TO_DATE(SYSDATE+5),'mm/dd/yyyy'),1,5) || '''|' || COUNT(CASE WHEN TO_DATE(actual_schdate) = TO_DATE(SYSDATE+5) THEN 1 END) ||"
			+ " ','''|| SUBSTR(TO_CHAR(TO_DATE(SYSDATE+6),'mm/dd/yyyy'),1,5) || '''|' || COUNT(CASE WHEN TO_DATE(actual_schdate) = TO_DATE(SYSDATE+6) THEN 1 END) ||"
			+ " ','''|| SUBSTR(TO_CHAR(TO_DATE(SYSDATE+7),'mm/dd/yyyy'),1,5) || '''|' || COUNT(CASE WHEN TO_DATE(actual_schdate) = TO_DATE(SYSDATE+7) THEN 1 END) as fCast,"
			+ " COUNT(CASE WHEN TO_DATE(actual_schdate) between TO_DATE(SYSDATE+1) and TO_DATE(SYSDATE+7) THEN 1 END) AS cnt" 
			+ " FROM erv_milestones_4forecast a, sch_events1 y, ER_STUDY z "
			+ " WHERE pk_study = a.FK_STUDY "
			+ " AND (EXISTS (SELECT * FROM ER_STUDYTEAM bb WHERE pk_study = bb.FK_STUDY AND bb.fk_user = "+ userId +" AND NVL(studyteam_status,'Active') = 'Active') "
			+ " OR Pkg_Phi.F_Is_Superuser("+ userId +", a.fk_study) = 1) "
			+ " AND (visit IN(SELECT milestone_visit FROM ER_MILESTONE x WHERE a.pk_milestone = x.pk_milestone) OR (a.fk_eventassoc = fk_assoc)) "
			+ " AND a.fk_cal = TO_NUMBER(session_id) "
			+ " AND (study_prinv IN ( SELECT pk_user FROM ER_USER WHERE fk_account=("+ accountId +")) OR study_prinv IS NULL) "
			+ " AND TO_DATE(actual_schdate) BETWEEN TO_DATE(SYSDATE+1) AND TO_DATE(SYSDATE+7) ";
		
			query = new BrowserRows();
			query.getPageRows(1,10,queryCount,50,"select 1 from dual","1","asc");
		
			if (EJBUtil.stringToNum(query.getBValues(1,"cnt")) != 0){
				foreCast7 = foreCast7 + query.getBValues(1,"fCast");
			}
			//foreCast7 = "'00/00'|0,'04/17'|3,'04/18'|2,'04/19'|7,'04/20'|0,'04/21'|1,'04/22'|4,'04/23'|0";
		
		%>
		<Input type="hidden" id="foreCast7" name="foreCast7" value="<%=foreCast7%>">
		<Input type="hidden" id="achieve_Today" name="achieve_Today" value="<%=achieve_Today%>">
		<%
			int budget_today = 0;
			int budget_last_7d = 0;
			int budget_last_15d = 0;
			int budget_last_30d = 0;
			int budget_gt30d = 0;
		
			String budgetSql ="select pk_budget, budget_name, "
			+ "CASE "
			+ "	WHEN (a.created_on IS NOT NULL AND a.last_modified_date IS NOT NULL) AND TRUNC(a.created_on) <= TRUNC(a.last_modified_date)THEN TRUNC(a.last_modified_date)  "
			+ "	WHEN (a.created_on IS NOT NULL AND a.last_modified_date IS NOT NULL) AND TRUNC(a.last_modified_date) < TRUNC(a.created_on) THEN TRUNC(a.created_on) "
			+ "	WHEN (a.created_on IS NULL AND a.last_modified_date IS NOT NULL) THEN TRUNC(a.last_modified_date) "
			//+ "	WHEN (a.created_on IS NOT NULL AND a.last_modified_date IS NULL) THEN TRUNC(a.created_on)  "
			+ "END AS moddate "
   	   		+ "from sch_budget a, er_study b, er_site c " 
			+ "where ((a.pk_budget in (select d.fk_budget from sch_bgtusers d " 
			+ "Where d.fk_user = " + userId+ " ))  OR " 
			+" (SELECT COUNT(*) FROM ER_GRPS WHERE pk_grp="+ defaultGrp  
			+" AND (grp_supbud_flag=1  AND (grp_supbud_rights IS NOT NULL OR to_number(grp_supbud_rights)>0)))>0 " 
			+"  )  AND a.fk_account="+ accountId +"	and " 
			+ "a.FK_SITE=c.PK_SITE(+) ";
				
			budgetSql  = budgetSql + " and  budget_calendar is null ";
			budgetSql  = budgetSql + " and  a.FK_STUDY=b.PK_STUDY(+) ";							
			budgetSql  = budgetSql + " and  nvl(a.budget_delflag,'Z') <> 'Y' ";
		
			queryCount="SELECT COUNT(CASE WHEN moddate = TRUNC(SYSDATE) THEN 1 END) AS budget_today,"
			+ " COUNT(CASE WHEN (moddate < TRUNC(SYSDATE) AND moddate >= TRUNC(SYSDATE)-7) THEN 1 END) AS budget_last_7d,"
			+ " COUNT(CASE WHEN (moddate < TRUNC(SYSDATE)-7 AND moddate >= TRUNC(SYSDATE)-15) THEN 1 END) AS budget_last_15d,"
			+ " COUNT(CASE WHEN (moddate < TRUNC(SYSDATE)-15 AND moddate >= TRUNC(SYSDATE)-30) THEN 1 END) AS budget_last_30d,"
			+ " COUNT(CASE WHEN (moddate < TRUNC(SYSDATE)-30) THEN 1 END) AS budget_gt30d "
			+ "FROM ( " 
			+ " select * from ("+budgetSql+" AND "
			+ " ( (((SELECT (settings_value) FROM ER_SETTINGS WHERE settings_modname=4 AND settings_modnum=" + defaultGrp  
			+ " AND settings_keyword = 'BUD_TAREA') LIKE '%'||TO_CHAR(b.fk_codelst_tarea)||'%') AND (fk_codelst_tarea IS NOT NULL)) "
			+ " OR	 (((SELECT (settings_value) FROM ER_SETTINGS WHERE settings_modname=4 AND settings_modnum=" + defaultGrp 
			+ " AND settings_keyword = 'BUD_DIVISION') LIKE '%'||TO_CHAR(b.study_division)||'%') AND (b.study_division IS NOT NULL)) "
			+ " OR "  
			+ " (1=(SELECT  Pkg_Util.f_compare('SELECT (settings_value) FROM ER_SETTINGS WHERE settings_modname=4 AND settings_modnum="+defaultGrp 
			+ " AND settings_keyword = ''BUD_DISSITE''', b.study_disease_site,',',',','SQL','') FROM dual) AND (study_disease_site IS NOT NULL) ) "
			+ " OR (0=(SELECT COUNT(*) FROM ER_SETTINGS WHERE settings_modname=4 AND settings_modnum="+defaultGrp
			+" AND settings_keyword IN ( 'BUD_TAREA','BUD_DISSITE','BUD_DIVISION')) ) " 
			+ " OR ((a.pk_budget in (select d.fk_budget from sch_bgtusers d " 
			+ " Where d.fk_user = " + userId+ " ))))  )"
			+ ") WHERE (TRUNC(moddate) > TRUNC(SYSDATE) -30 OR TRUNC(moddate) > TRUNC(SYSDATE)-30) ";
			//System.out.println(queryCount);
			query = new BrowserRows();
			query.getPageRows(1,10,queryCount,1,"select 1 from dual","1","asc");
		
			budget_today = EJBUtil.stringToNum(query.getBValues(1,"budget_today"));
			budget_last_7d = EJBUtil.stringToNum(query.getBValues(1,"budget_last_7d"));
			budget_last_15d = EJBUtil.stringToNum(query.getBValues(1,"budget_last_15d"));
			budget_last_30d = EJBUtil.stringToNum(query.getBValues(1,"budget_last_30d"));
			budget_gt30d = EJBUtil.stringToNum(query.getBValues(1,"budget_gt30d"));
		%>
		<Input type="hidden" id="budget_today" name="budget_today" value="<%=budget_today%>">
		<Input type="hidden" id="budget_last_7d" name="budget_last_7d" value="<%=budget_last_7d%>">
		<Input type="hidden" id="budget_last_15d" name="budget_last_15d" value="<%=budget_last_15d%>">
		<Input type="hidden" id="budget_last_30d" name="budget_last_30d" value="<%=budget_last_30d%>">
		<Input type="hidden" id="budget_gt30d" name="budget_gt30d" value="<%=budget_gt30d%>">
		<%
			int budget_W = 0;
			int budget_F = 0;
			int budget_T = 0;	
			
			budgetSql ="select pk_budget, budget_name, budget_status "
			+ "from sch_budget a, er_study b, er_site c " 
			+ "where ((a.pk_budget in (select d.fk_budget from sch_bgtusers d " 
			+ "Where d.fk_user = " + userId+ " ))  OR " 
			+" (SELECT COUNT(*) FROM ER_GRPS WHERE pk_grp="+ defaultGrp  
			+" AND (grp_supbud_flag=1  AND (grp_supbud_rights IS NOT NULL OR to_number(grp_supbud_rights)>0)))>0 " 
			+"  )  AND a.fk_account="+ accountId +"	and " 
			+ "a.FK_SITE=c.PK_SITE(+) ";
				
			budgetSql  = budgetSql + " and  budget_calendar is null ";
			budgetSql  = budgetSql + " and  a.FK_STUDY=b.PK_STUDY(+) ";							
			budgetSql  = budgetSql + " and  nvl(a.budget_delflag,'Z') <> 'Y' ";
		
			queryCount="SELECT SUM(DECODE(budget_status,'W',1,0)) AS W_count, SUM(DECODE(budget_status,'F',1,0)) AS F_count, "
			+ "SUM(DECODE(budget_status,'T',1,0)) AS T_count "
			+ "FROM ( "
			+ " select * from ("+budgetSql+" AND "
			+ " ( (((SELECT (settings_value) FROM ER_SETTINGS WHERE settings_modname=4 AND settings_modnum=" + defaultGrp  
			+ " AND settings_keyword = 'BUD_TAREA') LIKE '%'||TO_CHAR(b.fk_codelst_tarea)||'%') AND (fk_codelst_tarea IS NOT NULL)) "
			+ " OR	 (((SELECT (settings_value) FROM ER_SETTINGS WHERE settings_modname=4 AND settings_modnum=" + defaultGrp 
			+ " AND settings_keyword = 'BUD_DIVISION') LIKE '%'||TO_CHAR(b.study_division)||'%') AND (b.study_division IS NOT NULL)) "
			+ " OR "  
			+ " (1=(SELECT  Pkg_Util.f_compare('SELECT (settings_value) FROM ER_SETTINGS WHERE settings_modname=4 AND settings_modnum="+defaultGrp 
			+ " AND settings_keyword = ''BUD_DISSITE''', b.study_disease_site,',',',','SQL','') FROM dual) AND (study_disease_site IS NOT NULL) ) "
			+ " OR (0=(SELECT COUNT(*) FROM ER_SETTINGS WHERE settings_modname=4 AND settings_modnum="+defaultGrp
			+" AND settings_keyword IN ( 'BUD_TAREA','BUD_DISSITE','BUD_DIVISION')) ) " 
			+ " OR ((a.pk_budget in (select d.fk_budget from sch_bgtusers d " 
			+ " Where d.fk_user = " + userId+ " ))))  )"
			+ ")  ";
			
			query = new BrowserRows();
			query.getPageRows(1,10,queryCount,50,"select 1 from dual","1","asc");
		
			budget_W = EJBUtil.stringToNum(query.getBValues(1,"W_count"));
			budget_F = EJBUtil.stringToNum(query.getBValues(1,"F_count"));
			budget_T = EJBUtil.stringToNum(query.getBValues(1,"T_count"));
		%>
		<Input type="hidden" id="budget_W" name="budget_W" value="<%=budget_W%>">
		<Input type="hidden" id="budget_F" name="budget_F" value="<%=budget_F%>">
		<Input type="hidden" id="budget_T" name="budget_T" value="<%=budget_T%>">
		<%
			String Year_current ="";
			String Year_last1 ="";
			String Year_last2 ="";
			String Year_last3 ="";
			String Year_last4 ="";
		
			int no = 0;
			int ThisYear = 0;
			
			queryCount = "SELECT no,yyyy,jan_cnt,feb_cnt,mar_cnt,apr_cnt,may_cnt,jun_cnt,jul_cnt,aug_cnt,sep_cnt,oct_cnt,nov_cnt,dec_cnt"
			+ " FROM ("
			+ " SELECT 1 AS no,SUBSTR(TO_CHAR(SYSDATE,'mm/dd/yyyy'),7,10) AS yyyy,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='01' THEN 1 END) AS jan_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='02' THEN 1 END) AS feb_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='03' THEN 1 END) AS mar_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='04' THEN 1 END) AS apr_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='05' THEN 1 END) AS may_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='06' THEN 1 END) AS jun_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='07' THEN 1 END) AS jul_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='08' THEN 1 END) AS aug_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='09' THEN 1 END) AS sep_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='10' THEN 1 END) AS oct_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='11' THEN 1 END) AS nov_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='12' THEN 1 END) AS dec_cnt"
			+ " FROM erv_mileachieved a,ER_STUDY b"
			+ " WHERE pk_study=fk_study"
			+ " AND (EXISTS (SELECT * FROM ER_STUDYTEAM bb WHERE pk_study=bb.fk_study AND bb.fk_user ="+ userId + " AND NVL(studyteam_status,'Active')='Active')"
			+ " OR Pkg_Phi.F_Is_Superuser("+ userId + ",a.fk_study)=1)"
			+ " AND SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),7,10)=SUBSTR(TO_CHAR(SYSDATE,'mm/dd/yyyy'),7,10)"
			+ " UNION"
			+ " SELECT 2,SUBSTR(TO_CHAR(SYSDATE-365,'mm/dd/yyyy'),7,10) AS yyyy,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='01' THEN 1 END) AS jan_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='02' THEN 1 END) AS feb_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='03' THEN 1 END) AS mar_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='04' THEN 1 END) AS apr_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='05' THEN 1 END) AS may_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='06' THEN 1 END) AS jun_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='07' THEN 1 END) AS jul_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='08' THEN 1 END) AS aug_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='09' THEN 1 END) AS sep_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='10' THEN 1 END) AS oct_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='11' THEN 1 END) AS nov_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='12' THEN 1 END) AS dec_cnt"
			+ " FROM erv_mileachieved a,ER_STUDY b"
			+ " WHERE pk_study=fk_study"
			+ " AND (EXISTS (SELECT * FROM ER_STUDYTEAM bb WHERE pk_study=bb.fk_study AND bb.fk_user ="+ userId + " AND NVL(studyteam_status,'Active')='Active')"
			+ " OR Pkg_Phi.F_Is_Superuser("+ userId + ",a.fk_study)=1)"
			+ " AND SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),7,10)=SUBSTR(TO_CHAR(SYSDATE-365,'mm/dd/yyyy'),7,10)"
			+ " UNION"
			+ " SELECT 3,SUBSTR(TO_CHAR(SYSDATE-(365*2),'mm/dd/yyyy'),7,10) AS yyyy,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='01' THEN 1 END) AS jan_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='02' THEN 1 END) AS feb_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='03' THEN 1 END) AS mar_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='04' THEN 1 END) AS apr_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='05' THEN 1 END) AS may_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='06' THEN 1 END) AS jun_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='07' THEN 1 END) AS jul_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='08' THEN 1 END) AS aug_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='09' THEN 1 END) AS sep_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='10' THEN 1 END) AS oct_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='11' THEN 1 END) AS nov_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='12' THEN 1 END) AS dec_cnt"
			+ " FROM erv_mileachieved a,ER_STUDY b"
			+ " WHERE pk_study=fk_study"
			+ " AND (EXISTS (SELECT * FROM ER_STUDYTEAM bb WHERE pk_study=bb.fk_study AND bb.fk_user ="+ userId + " AND NVL(studyteam_status,'Active')='Active')"
			+ " OR Pkg_Phi.F_Is_Superuser("+ userId + ",a.fk_study)=1)"
			+ " AND SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),7,10)=SUBSTR(TO_CHAR(SYSDATE-(365*2),'mm/dd/yyyy'),7,10)"
			+ " UNION"
			+ " SELECT 4,SUBSTR(TO_CHAR(SYSDATE-(365*3),'mm/dd/yyyy'),7,10) AS yyyy,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='01' THEN 1 END) AS jan_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='02' THEN 1 END) AS feb_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='03' THEN 1 END) AS mar_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='04' THEN 1 END) AS apr_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='05' THEN 1 END) AS may_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='06' THEN 1 END) AS jun_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='07' THEN 1 END) AS jul_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='08' THEN 1 END) AS aug_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='09' THEN 1 END) AS sep_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='10' THEN 1 END) AS oct_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='11' THEN 1 END) AS nov_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='12' THEN 1 END) AS dec_cnt"
			+ " FROM erv_mileachieved a,ER_STUDY b"
			+ " WHERE pk_study=fk_study"
			+ " AND (EXISTS (SELECT * FROM ER_STUDYTEAM bb WHERE pk_study=bb.fk_study AND bb.fk_user ="+ userId + " AND NVL(studyteam_status,'Active')='Active')"
			+ " OR Pkg_Phi.F_Is_Superuser("+ userId + ",a.fk_study)=1)"
			+ " AND SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),7,10)=SUBSTR(TO_CHAR(SYSDATE-(365*3),'mm/dd/yyyy'),7,10)"
			+ " UNION"
			+ " SELECT 5,SUBSTR(TO_CHAR(SYSDATE-(365*4),'mm/dd/yyyy'),7,10) AS yyyy,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='01' THEN 1 END) AS jan_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='02' THEN 1 END) AS feb_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='03' THEN 1 END) AS mar_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='04' THEN 1 END) AS apr_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='05' THEN 1 END) AS may_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='06' THEN 1 END) AS jun_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='07' THEN 1 END) AS jul_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='08' THEN 1 END) AS aug_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='09' THEN 1 END) AS sep_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='10' THEN 1 END) AS oct_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='11' THEN 1 END) AS nov_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),1,2)='12' THEN 1 END) AS dec_cnt"
			+ " FROM erv_mileachieved a,ER_STUDY b"
			+ " WHERE pk_study=fk_study"
			+ " AND (EXISTS (SELECT * FROM ER_STUDYTEAM bb WHERE pk_study=bb.fk_study AND bb.fk_user ="+ userId + " AND NVL(studyteam_status,'Active')='Active')"
			+ " OR Pkg_Phi.F_Is_Superuser("+ userId + ",a.fk_study)=1)"
			+ " AND SUBSTR(TO_CHAR(ach_date,'mm/dd/yyyy'),7,10)=SUBSTR(TO_CHAR(SYSDATE-(365*4),'mm/dd/yyyy'),7,10)"
			+ " ) ORDER BY no";
		
			//out.println(queryCount);
			query=new BrowserRows();
			query.getPageRows(1,10,queryCount,50,"select 5 from dual","1","asc");
			
			int counter=1;
			int yearCnt=1;
			int yyyy=0;
			String Data="";
			
			if (query.getRowReturned()!= 0){ 
				ThisYear=EJBUtil.stringToNum(query.getBValues(1,"yyyy"));
				
				for(counter=1; counter<=query.getRowReturned();counter++){				
					yyyy=EJBUtil.stringToNum(query.getBValues(counter,"yyyy"));
					while (yearCnt != (ThisYear-yyyy)+1){
						Data =LC.L_Month/*Month*****/+"|0,"+LC.L_Jan/*Jan*****/+"|0,"+LC.L_Feb/*Feb*****/+"|0,"+LC.L_Mar/*Mar*******/+"|0,"+LC.L_Apr/*Apr*****/+"|0,"+LC.L_May/*May*****/+"|0,"+LC.L_Jun/*Jun*****/+"|0,"+LC.L_Jul/*Jul*****/+"|0,"+LC.L_Aug/*Aug*****/+"|0,"+LC.L_Sep/*Sep*****/+"|0,"+LC.L_Oct/*Oct*****/+"|0,"+LC.L_Nov/*Nov*****/+"|0,"+LC.L_Dec/*Dec*****/+"|0";
						switch(yearCnt){
							case 1: Year_current = Data; break;
							case 2: Year_last1 = Data; break;
							case 3: Year_last2 = Data; break;
							case 4: Year_last3 = Data; break;
							case 5: Year_last4 = Data; break;
						}				
						yearCnt++;
					}
					if (yearCnt == (ThisYear-yyyy)+1){
						Data =LC.L_Month/*Month*****/+"|0,"+LC.L_Jan/*Jan*****/+"|" + query.getBValues(counter,"jan_cnt")
						+ ","+LC.L_Feb/*Feb*****/+"|" + query.getBValues(counter,"feb_cnt")
						+ ","+LC.L_Mar/*Mar*****/+"|" + query.getBValues(counter,"mar_cnt") 
						+ ","+LC.L_Apr/*Apr*****/+"|" + query.getBValues(counter,"apr_cnt") 
						+ ","+LC.L_May/*May*****/+"|" + query.getBValues(counter,"may_cnt") 
						+ ","+LC.L_Jun/*Jun*****/+"|" + query.getBValues(counter,"jun_cnt") 
						+ ","+LC.L_Jul/*Jul******/+"|" + query.getBValues(counter,"jul_cnt") 
						+ ","+LC.L_Aug/*Aug******/+"|" + query.getBValues(counter,"aug_cnt") 
						+ ","+LC.L_Sep/*Sep******/+"|" + query.getBValues(counter,"sep_cnt") 
						+ ","+LC.L_Oct/*Oct*******/+"|" + query.getBValues(counter,"oct_cnt") 
						+ ","+LC.L_Nov/*Nov*****/+"|" + query.getBValues(counter,"nov_cnt") 
						+ ","+LC.L_Dec/*Dec******/+"|" + query.getBValues(counter,"dec_cnt") ;
					
						switch(yearCnt){
							case 1: Year_current = Data; break;
							case 2: Year_last1 = Data; break;
							case 3: Year_last2 = Data; break;
							case 4: Year_last3 = Data; break;
							case 5: Year_last4 = Data; break;
						}
						yearCnt++;
					}
				}
				
				while (yearCnt <= 5){
					Data =LC.L_Month/*Month*****/+"|0,"+LC.L_Jan/*Jan*****/+"|0,"+LC.L_Feb/*Feb*****/+"|0,"+LC.L_Mar/*Mar*******/+"|0,"+LC.L_Apr/*Apr*****/+"|0,"+LC.L_May/*May*****/+"|0,"+LC.L_Jun/*Jun*****/+"|0,"+LC.L_Jul/*Jul*****/+"|0,"+LC.L_Aug/*Aug*****/+"|0,"+LC.L_Sep/*Sep*****/+"|0,"+LC.L_Oct/*Oct*****/+"|0,"+LC.L_Nov/*Nov*****/+"|0,"+LC.L_Dec/*Dec*****/+"|0";
					switch(yearCnt){
						case 1: Year_current = Data; break;
						case 2: Year_last1 = Data; break;
						case 3: Year_last2 = Data; break;
						case 4: Year_last3 = Data; break;
						case 5: Year_last4 = Data; break;
					}
					yearCnt++;
				}
			}
		%>
		<Input type="hidden" id="ThisYear" name="ThisYear" value="<%=ThisYear%>">
		<Input type="hidden" id="Year_current" name="Year_current" value="<%=Year_current%>">
		<Input type="hidden" id="Year_last1" name="Year_last1" value="<%=Year_last1%>">
		<Input type="hidden" id="Year_last2" name="Year_last2" value="<%=Year_last2%>">
		<Input type="hidden" id="Year_last3" name="Year_last3" value="<%=Year_last3%>">
		<Input type="hidden" id="Year_last4" name="Year_last4" value="<%=Year_last4%>">
		<%
			String Year_currt ="";
			String Year_next1 ="";
			String Year_next2 ="";
			String Year_next3 ="";
			String Year_next4 ="";
		
			yyyy=0;
			Data="";
			
			queryCount="SELECT yyyy,jan_cnt,feb_cnt,mar_cnt,apr_cnt,may_cnt,jun_cnt,jul_cnt,aug_cnt,sep_cnt,oct_cnt,nov_cnt,dec_cnt"
			+ " FROM ("
			+ " SELECT SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),7,10) AS yyyy,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='01' THEN 1 END) AS jan_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='02' THEN 1 END) AS feb_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='03' THEN 1 END) AS mar_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='04' THEN 1 END) AS apr_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='05' THEN 1 END) AS may_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='06' THEN 1 END) AS jun_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='07' THEN 1 END) AS jul_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='08' THEN 1 END) AS aug_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='09' THEN 1 END) AS sep_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='10' THEN 1 END) AS oct_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='11' THEN 1 END) AS nov_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='12' THEN 1 END) AS dec_cnt"
			+ " FROM erv_milestones_4forecast a,sch_events1 y,ER_STUDY z"
			+ " WHERE pk_study=a.FK_STUDY"
			+ " AND (EXISTS (SELECT * FROM ER_STUDYTEAM bb WHERE pk_study=bb.FK_STUDY AND bb.fk_user="+userId+ " AND NVL(studyteam_status,'Active')='Active')"
			+ " OR Pkg_Phi.F_Is_Superuser("+ userId + ",a.fk_study)=1)"
			+ " AND (visit IN(SELECT milestone_visit FROM ER_MILESTONE x WHERE a.pk_milestone=x.pk_milestone) OR (a.fk_eventassoc=fk_assoc))"
			+ " AND a.fk_cal=TO_NUMBER(session_id)"
			//+ " AND a.fk_site IN(SELECT xx.fk_site FROM ER_USERSITE xx WHERE xx.fk_user="+ userId + " )"
			+ " AND (study_prinv IN (SELECT pk_user FROM ER_USER WHERE fk_account= "+accountId+ ") OR study_prinv IS NULL)"
			+ " AND f_to_number(SUBSTR(TO_CHAR(actual_schdate, 'mm/dd/yyyy'),7,10)) between"
			+ " f_to_number(SUBSTR(TO_CHAR(SYSDATE,'mm/dd/yyyy'),7,10)) AND f_to_number(SUBSTR(TO_CHAR(SYSDATE+(365*4),'mm/dd/yyyy'),7,10))"
			+ " group by SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),7,10)) ";
			
				
			/*queryCount="SELECT no,yyyy,jan_cnt,feb_cnt,mar_cnt,apr_cnt,may_cnt,jun_cnt,jul_cnt,aug_cnt,sep_cnt,oct_cnt,nov_cnt,dec_cnt"
			+ " FROM ("
			+ " SELECT 1 AS no,SUBSTR(TO_CHAR(SYSDATE,'mm/dd/yyyy'),7,10) AS yyyy,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='01' THEN 1 END) AS jan_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='02' THEN 1 END) AS feb_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='03' THEN 1 END) AS mar_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='04' THEN 1 END) AS apr_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='05' THEN 1 END) AS may_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='06' THEN 1 END) AS jun_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='07' THEN 1 END) AS jul_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='08' THEN 1 END) AS aug_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='09' THEN 1 END) AS sep_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='10' THEN 1 END) AS oct_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='11' THEN 1 END) AS nov_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='12' THEN 1 END) AS dec_cnt"
			+ " FROM erv_milestones_4forecast a,sch_events1 y,ER_STUDY z"
			+ " WHERE pk_study=a.FK_STUDY"
			+ " AND (EXISTS (SELECT * FROM ER_STUDYTEAM bb WHERE pk_study=bb.FK_STUDY AND bb.fk_user="+userId+ " AND NVL(studyteam_status,'Active')='Active')"
			+ " OR Pkg_Phi.F_Is_Superuser("+ userId + ",a.fk_study)=1)"
			+ " AND (visit IN(SELECT milestone_visit FROM ER_MILESTONE x WHERE a.pk_milestone=x.pk_milestone) OR (a.fk_eventassoc=fk_assoc))"
			+ " AND a.fk_cal=TO_NUMBER(session_id)"
			//+ " AND a.fk_site IN(SELECT xx.fk_site FROM ER_USERSITE xx WHERE xx.fk_user="+ userId + " )"
			+ " AND (study_prinv IN (SELECT pk_user FROM ER_USER WHERE fk_account= "+accountId+ ") OR study_prinv IS NULL)"
			+ " AND SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),7,10)=SUBSTR(TO_CHAR(SYSDATE,'mm/dd/yyyy'),7,10)"
			+ " UNION"
			+ " SELECT 2 AS no,SUBSTR(TO_CHAR(SYSDATE+365,'mm/dd/yyyy'),7,10) AS yyyy,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='01' THEN 1 END) AS jan_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='02' THEN 1 END) AS feb_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='03' THEN 1 END) AS mar_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='04' THEN 1 END) AS apr_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='05' THEN 1 END) AS may_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='06' THEN 1 END) AS jun_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='07' THEN 1 END) AS jul_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='08' THEN 1 END) AS aug_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='09' THEN 1 END) AS sep_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='10' THEN 1 END) AS oct_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='11' THEN 1 END) AS nov_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='12' THEN 1 END) AS dec_cnt"
			+ " FROM erv_milestones_4forecast a,sch_events1 y,ER_STUDY z"
			+ " WHERE pk_study=a.FK_STUDY"
			+ " AND (EXISTS (SELECT * FROM ER_STUDYTEAM bb WHERE pk_study=bb.FK_STUDY AND bb.fk_user="+userId+ " AND NVL(studyteam_status,'Active')='Active')"
			+ " OR Pkg_Phi.F_Is_Superuser("+ userId + ",a.fk_study)=1)"
			+ " AND (visit IN(SELECT milestone_visit FROM ER_MILESTONE x WHERE a.pk_milestone=x.pk_milestone) OR (a.fk_eventassoc=fk_assoc))"
			+ " AND a.fk_cal=TO_NUMBER(session_id)"
			//+ " AND a.fk_site IN(SELECT xx.fk_site FROM ER_USERSITE xx WHERE xx.fk_user="+ userId + " )"
			+ " AND (study_prinv IN (SELECT pk_user FROM ER_USER WHERE fk_account= "+accountId+ ") OR study_prinv IS NULL)"
			+ " AND SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),7,10)=SUBSTR(TO_CHAR(SYSDATE+365,'mm/dd/yyyy'),7,10)"
			+ " UNION"
			+ " SELECT 3 AS no,SUBSTR(TO_CHAR(SYSDATE+(365*2),'mm/dd/yyyy'),7,10) AS yyyy,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='01' THEN 1 END) AS jan_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='02' THEN 1 END) AS feb_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='03' THEN 1 END) AS mar_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='04' THEN 1 END) AS apr_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='05' THEN 1 END) AS may_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='06' THEN 1 END) AS jun_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='07' THEN 1 END) AS jul_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='08' THEN 1 END) AS aug_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='09' THEN 1 END) AS sep_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='10' THEN 1 END) AS oct_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='11' THEN 1 END) AS nov_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='12' THEN 1 END) AS dec_cnt"
			+ " FROM erv_milestones_4forecast a,sch_events1 y,ER_STUDY z"
			+ " WHERE pk_study=a.FK_STUDY"
			+ " AND (EXISTS (SELECT * FROM ER_STUDYTEAM bb WHERE pk_study=bb.FK_STUDY AND bb.fk_user="+userId+ " AND NVL(studyteam_status,'Active')='Active')"
			+ " OR Pkg_Phi.F_Is_Superuser("+ userId + ",a.fk_study)=1)"
			+ " AND (visit IN(SELECT milestone_visit FROM ER_MILESTONE x WHERE a.pk_milestone=x.pk_milestone) OR (a.fk_eventassoc=fk_assoc))"
			+ " AND a.fk_cal=TO_NUMBER(session_id)"
			//+ " AND a.fk_site IN(SELECT xx.fk_site FROM ER_USERSITE xx WHERE xx.fk_user="+ userId + " )"
			+ " AND (study_prinv IN (SELECT pk_user FROM ER_USER WHERE fk_account= "+accountId+ ") OR study_prinv IS NULL)"
			+ " AND SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),7,10)=SUBSTR(TO_CHAR(SYSDATE+(365*2),'mm/dd/yyyy'),7,10)"
			+ " UNION"
			+ " SELECT 4 AS no,SUBSTR(TO_CHAR(SYSDATE+(365*3),'mm/dd/yyyy'),7,10) AS yyyy,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='01' THEN 1 END) AS jan_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='02' THEN 1 END) AS feb_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='03' THEN 1 END) AS mar_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='04' THEN 1 END) AS apr_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='05' THEN 1 END) AS may_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='06' THEN 1 END) AS jun_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='07' THEN 1 END) AS jul_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='08' THEN 1 END) AS aug_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='09' THEN 1 END) AS sep_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='10' THEN 1 END) AS oct_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='11' THEN 1 END) AS nov_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='12' THEN 1 END) AS dec_cnt"
			+ " FROM erv_milestones_4forecast a,sch_events1 y,ER_STUDY z"
			+ " WHERE pk_study=a.FK_STUDY"
			+ " AND (EXISTS (SELECT * FROM ER_STUDYTEAM bb WHERE pk_study=bb.FK_STUDY AND bb.fk_user="+userId+ " AND NVL(studyteam_status,'Active')='Active')"
			+ " OR Pkg_Phi.F_Is_Superuser("+ userId + ",a.fk_study)=1)"
			+ " AND (visit IN(SELECT milestone_visit FROM ER_MILESTONE x WHERE a.pk_milestone=x.pk_milestone) OR (a.fk_eventassoc=fk_assoc))"
			+ " AND a.fk_cal=TO_NUMBER(session_id)"
			//+ " AND a.fk_site IN(SELECT xx.fk_site FROM ER_USERSITE xx WHERE xx.fk_user="+ userId + " )"
			+ " AND (study_prinv IN (SELECT pk_user FROM ER_USER WHERE fk_account= "+accountId+ ") OR study_prinv IS NULL)"
			+ " AND SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),7,10)=SUBSTR(TO_CHAR(SYSDATE+(365*3),'mm/dd/yyyy'),7,10)"
			+ " UNION"
			+ " SELECT 5 AS no,SUBSTR(TO_CHAR(SYSDATE+(365*4),'mm/dd/yyyy'),7,10) AS yyyy,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='01' THEN 1 END) AS jan_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='02' THEN 1 END) AS feb_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='03' THEN 1 END) AS mar_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='04' THEN 1 END) AS apr_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='05' THEN 1 END) AS may_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='06' THEN 1 END) AS jun_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='07' THEN 1 END) AS jul_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='08' THEN 1 END) AS aug_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='09' THEN 1 END) AS sep_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='10' THEN 1 END) AS oct_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='11' THEN 1 END) AS nov_cnt,"
			+ " COUNT(CASE WHEN SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),1,2)='12' THEN 1 END) AS dec_cnt"
			+ " FROM erv_milestones_4forecast a,sch_events1 y,ER_STUDY z"
			+ " WHERE pk_study=a.FK_STUDY"
			+ " AND (EXISTS (SELECT * FROM ER_STUDYTEAM bb WHERE pk_study=bb.FK_STUDY AND bb.fk_user="+userId+ " AND NVL(studyteam_status,'Active')='Active')"
			+ " OR Pkg_Phi.F_Is_Superuser("+ userId + ",a.fk_study)=1)"
			+ " AND (visit IN(SELECT milestone_visit FROM ER_MILESTONE x WHERE a.pk_milestone=x.pk_milestone) OR (a.fk_eventassoc=fk_assoc))"
			+ " AND a.fk_cal=TO_NUMBER(session_id)"
			//+ " AND a.fk_site IN(SELECT xx.fk_site FROM ER_USERSITE xx WHERE xx.fk_user="+ userId + " ) "
			+ " AND (study_prinv IN (SELECT pk_user FROM ER_USER WHERE fk_account= "+accountId+ ") OR study_prinv IS NULL)"
			+ " AND SUBSTR(TO_CHAR(actual_schdate,'mm/dd/yyyy'),7,10)=SUBSTR(TO_CHAR(SYSDATE+(365*4),'mm/dd/yyyy'),7,10)"
			+ " ) ";*/
			
			query=new BrowserRows();
			query.getPageRows(1,10,queryCount,50,"select 5 from dual","1","asc");
		
			counter=1;
			yearCnt=1;
			
			if (query.getRowReturned()!= 0){ 
				for(counter=1; counter<=query.getRowReturned();counter++){
					yyyy=EJBUtil.stringToNum(query.getBValues(counter,"yyyy"));
					while (yearCnt != (yyyy-ThisYear)+1){
						Data =LC.L_Month/*Month*****/+"|0,"+LC.L_Jan/*Jan*****/+"|0,"+LC.L_Feb/*Feb*****/+"|0,"+LC.L_Mar/*Mar*******/+"|0,"+LC.L_Apr/*Apr*****/+"|0,"+LC.L_May/*May*****/+"|0,"+LC.L_Jun/*Jun*****/+"|0,"+LC.L_Jul/*Jul*****/+"|0,"+LC.L_Aug/*Aug*****/+"|0,"+LC.L_Sep/*Sep*****/+"|0,"+LC.L_Oct/*Oct*****/+"|0,"+LC.L_Nov/*Nov*****/+"|0,"+LC.L_Dec/*Dec*****/+"|0";
						switch(yearCnt){
							case 1: Year_currt = Data; break;
							case 2: Year_next1 = Data; break;
							case 3: Year_next2 = Data; break;
							case 4: Year_next3 = Data; break;
							case 5: Year_next4 = Data; break;
						}				
						yearCnt++;
					}
					if (yearCnt == (yyyy-ThisYear)+1){
						Data =LC.L_Month/*Month*****/+"|0,"+LC.L_Jan/*Jan*****/+"|" + query.getBValues(counter,"jan_cnt")
						+ ","+LC.L_Feb/*Feb*****/+"|" + query.getBValues(counter,"feb_cnt")
						+ ","+LC.L_Mar/*Mar*****/+"|" + query.getBValues(counter,"mar_cnt") 
						+ ","+LC.L_Apr/*Apr*****/+"|" + query.getBValues(counter,"apr_cnt") 
						+ ","+LC.L_May/*May*****/+"|" + query.getBValues(counter,"may_cnt") 
						+ ","+LC.L_Jun/*Jun*****/+"|" + query.getBValues(counter,"jun_cnt") 
						+ ","+LC.L_Jul/*Jul******/+"|" + query.getBValues(counter,"jul_cnt") 
						+ ","+LC.L_Aug/*Aug******/+"|" + query.getBValues(counter,"aug_cnt") 
						+ ","+LC.L_Sep/*Sep******/+"|" + query.getBValues(counter,"sep_cnt") 
						+ ","+LC.L_Oct/*Oct*******/+"|" + query.getBValues(counter,"oct_cnt") 
						+ ","+LC.L_Nov/*Nov*****/+"|" + query.getBValues(counter,"nov_cnt") 
						+ ","+LC.L_Dec/*Dec******/+"|" + query.getBValues(counter,"dec_cnt") ;
					
						switch(yearCnt){
							case 1: Year_currt = Data; break;
							case 2: Year_next1 = Data; break;
							case 3: Year_next2 = Data; break;
							case 4: Year_next3 = Data; break;
							case 5: Year_next4 = Data; break;
						}
						yearCnt++;
					}
				}
				
				while (yearCnt <= 5){
					Data =LC.L_Month+"|0,"+LC.L_Jan+"|0,"+LC.L_Feb+"|0,"+LC.L_Mar+"|0,"+LC.L_Apr+"|0,"+LC.L_May+"|0,"+LC.L_Jun+"|0,"+LC.L_Jul+"|0,"+LC.L_Aug+"|0,"+LC.L_Sep+"|0,"+LC.L_Oct+"|0,"+LC.L_Nov+"|0,"+LC.L_Dec+"|0";
					/*Data ="Month|0,Jan|0,Feb|0,Mar|0,Apr|0,May|0,Jun|0,Jul|0,Aug|0,Sep|0,Oct|0,Nov|0,Dec|0";*****/
					switch(yearCnt){
						case 1: Year_currt = Data; break;
						case 2: Year_next1 = Data; break;
						case 3: Year_next2 = Data; break;
						case 4: Year_next3 = Data; break;
						case 5: Year_next4 = Data; break;
					}
					yearCnt++;
				}
			}
		%>
		<Input type="hidden" id="Year_currt" name="Year_currt" value="<%=Year_currt%>">
		<Input type="hidden" id="Year_next1" name="Year_next1" value="<%=Year_next1%>">
		<Input type="hidden" id="Year_next2" name="Year_next2" value="<%=Year_next2%>">
		<Input type="hidden" id="Year_next3" name="Year_next3" value="<%=Year_next3%>">
		<Input type="hidden" id="Year_next4" name="Year_next4" value="<%=Year_next4%>">
		<%
			int achNInvCount = 0;
			int achPartInvCount = 0;
			int achOverInvCount = 0;
			int achPaidCount = 0;
		
			/*queryCount = "SELECT 1 AS dum,SUM(aa) AS achNInvCount,SUM(bb) AS achPartInvCount,SUM(cc) AS achOverInvCount "
			+ "FROM( "
			+ "SELECT pk_study,study_number,"
			+ "SUM(CASE WHEN (amount_achieved != amount_paid) AND (amount_invoiced = 0) THEN 1 END) AS aa,"
			+ "SUM(CASE WHEN (amount_achieved != amount_paid) AND (amount_invoiced != 0) AND (amount_achieved > amount_invoiced) THEN 1 END) AS bb,"
			+ "SUM(CASE WHEN (amount_achieved != amount_paid) AND (amount_invoiced != 0) AND (amount_invoiced > amount_achieved) THEN 1 END) AS cc "
			+ "FROM ( "
			+ "SELECT pk_study,study_number,"
			+ "to_number(TRUNC(DECODE(milestone_count,0,(T.achcount * milestone_amount) ,1,(T.achcount * milestone_amount ) ,(FLOOR( ABS(T.achcount /milestone_count )) * milestone_amount )),2)) AS amount_achieved,"
			+ "to_number(DECODE(milestone_paytype ,(SELECT pk_codelst FROM ER_CODELST WHERE trim(codelst_type) = 'milepaytype' AND codelst_subtyp = 'rec'), Pkg_Milestone_New.getMileInvoiceAmount(m.pk_milestone,'01/01/1900','01/01/3000') ,0 )) amount_invoiced,"
			+ "to_number(NVL(Pkg_Milestone_New. getMilePaymentAmount(m.pk_milestone,'01/01/1900','01/01/3000','payrec') ,0)) amount_received,"
			+ "to_number(NVL(Pkg_Milestone_New.getMilePaymentAmount(m.pk_milestone,'01/01/1900','01/01/3000','paymade') ,0)) amount_paid "
			+ "FROM ER_STUDY s,"
			+ "(SELECT FK_MILESTONE,1 AS achcount FROM ERV_MILEACHIEVED a WHERE a.fk_account = "+ accountId + " AND ACH_DATE >= TO_DATE (' 01-Jan-1900') AND ACH_DATE <= TO_DATE (' 01-Jan-3000') AND (study_prinv IN ( SELECT pk_user FROM ER_USER WHERE fk_account= "+ accountId + ") OR study_prinv IS NULL) AND DECODE(MILESTONE_TYPE,'SM',1,(Pkg_User.f_chk_right_for_studysite(fk_study,"+ userId + ",a.fk_site)) ) > 0 ORDER BY fk_milestone ) T,"
			+ "ER_MILESTONE m "
			+ "WHERE T.fk_milestone = pk_milestone AND m.fk_study = pk_study AND s.fk_account = "+ accountId + " "
			+ "ORDER BY study_number,pk_milestone "
			+ ") GROUP BY pk_study,study_number "
			+ ") ORDER BY DUM asc";*/
			
			queryCount = "SELECT 1 AS dum,SUM(aa) AS achNInvCount,SUM(bb) AS achPartInvCount,SUM(cc) AS achOverInvCount "
			+ "FROM( "
			+ "SELECT pk_study,study_number,aa, bb,cc "
			+ "FROM ( "
			+ "SELECT pk_study,study_number,"
			+ "SUM(msNotInvoiced) AS aa,"
			+ "SUM(CASE WHEN (amount_invoiced != 0) AND (amount_achieved > amount_invoiced) THEN msInvoiced END) AS bb,"
			+ "SUM(CASE WHEN (amount_invoiced != 0) AND (amount_invoiced > amount_achieved) THEN msInvoiced END) AS cc "
			+ "FROM ( "
			+ "SELECT pk_study,study_number, pk_milestone,amount_achieved * SUM(msInvoiced) AS amount_achieved,amount_invoiced,amount_received,SUM(msInvoiced) AS msInvoiced,SUM(msNotInvoiced) AS msNotInvoiced "
			+ "FROM ( "
			+ "SELECT pk_study, study_number, m.pk_milestone, "
			+ "TO_NUMBER(TRUNC(DECODE(milestone_count,0,(T.achcount * milestone_amount) ,1,(T.achcount * milestone_amount ) ,(FLOOR( ABS(T.achcount /milestone_count )) * milestone_amount )),2)) AS amount_achieved, "
			+ "(SELECT SUM(idet.amount_invoiced) FROM ER_INVOICE_DETAIL idet WHERE idet.fk_milestone = m.pk_milestone AND idet.fk_mileachieved = T.pk_mileachieved AND idet.DETAIL_TYPE = 'D' GROUP BY idet.fk_milestone) amount_invoiced, "
			+ "TO_NUMBER(NVL(Pkg_Milestone_New. getMilePaymentAmount(m.pk_milestone,PKG_DATEUTIL.f_get_null_date_str,PKG_DATEUTIL.f_get_future_null_date_str,'payrec') ,0)) amount_received, "
			+ "CASE WHEN (SELECT NVL(SUM(idet.amount_invoiced),0) FROM ER_INVOICE_DETAIL idet WHERE idet.fk_milestone = m.pk_milestone AND idet.fk_mileachieved = T.pk_mileachieved AND idet.DETAIL_TYPE = 'D')>0 THEN 1 ELSE 0 END AS msInvoiced, "
			+ "CASE WHEN (SELECT NVL(SUM(idet.amount_invoiced),0) FROM ER_INVOICE_DETAIL idet WHERE idet.fk_milestone = m.pk_milestone AND idet.fk_mileachieved = T.pk_mileachieved AND idet.DETAIL_TYPE = 'D')=0 THEN 1 ELSE 0 END AS msNotInvoiced "
			+ "FROM ER_STUDY s, "
			+ "(SELECT a.pk_mileachieved, a.FK_MILESTONE, 1 AS achcount FROM ERV_MILEACHIEVED a WHERE a.fk_account = "+ accountId + " AND ACH_DATE >= TO_DATE (' 01-Jan-1900') AND ACH_DATE <= TO_DATE (' 01-Jan-3000') AND (study_prinv IN ( SELECT pk_user FROM ER_USER WHERE fk_account= "+ accountId + " ) OR study_prinv IS NULL) AND DECODE(MILESTONE_TYPE,'SM',1, (Pkg_User.f_chk_right_for_studysite(fk_study,"+ userId + ",a.fk_site)) ) > 0 ORDER BY fk_milestone ) T, "
			+ "ER_MILESTONE m "
			+ "WHERE T.fk_milestone = m.pk_milestone AND m.fk_study = pk_study AND s.fk_account = "+ accountId + " "
			+ "AND (pk_study IN (SELECT h.fk_study FROM er_studyteam h WHERE pk_study = h.fk_study AND h.fk_user = "+ userId + " AND NVL(studyteam_status,'Active') = 'Active')"
			+ " OR Pkg_Phi.F_Is_Superuser("+ userId + ", pk_study) = 1) "
			+ "ORDER BY study_number,pk_milestone "
			+ ")GROUP BY pk_study, study_number, pk_milestone, amount_achieved,amount_invoiced,amount_received,msInvoiced "
			+ ") "
			+ "GROUP BY pk_study, study_number "
			+ ")"
			+ "WHERE "
			+ "(aa != 0 AND aa IS NOT NULL) OR (bb != 0 AND bb IS NOT NULL) OR (cc != 0 AND cc IS NOT NULL) "
			+ ") ORDER BY DUM ASC ";
			countSql = "select 1 from dual";
			
			query = new BrowserRows();
			query.getPageRows(1,100,queryCount,50,countSql,"2","asc");
		
			if (query.getRowReturned()!= 0){
				achNInvCount = EJBUtil.stringToNum(query.getBValues(1,"achNInvCount"));
				achPartInvCount = EJBUtil.stringToNum(query.getBValues(1,"achPartInvCount"));
				achOverInvCount = EJBUtil.stringToNum(query.getBValues(1,"achOverInvCount"));
			}
		%>
		<Input type="hidden" id="achNInvCount" name="achNInvCount" value="<%=achNInvCount%>">
		<Input type="hidden" id="achPartInvCount" name="achPartInvCount" value="<%=achPartInvCount%>">
		<Input type="hidden" id="achOverInvCount" name="achOverInvCount" value="<%=achOverInvCount%>">
		<Input type="hidden" id="achPaidCount" name="achPaidCount" value="<%=achPaidCount%>">
		<%
			int invoice_today = 0;
			int invoice_last_7d = 0;
			int invoice_last_15d = 0;
			int invoice_last_30d = 0;
			int invoice_gt30d = 0;
			
			queryCount = "SELECT COUNT(CASE WHEN inv_date = TRUNC(SYSDATE) THEN 1 END) AS invoice_today,"
			+ " COUNT(CASE WHEN (inv_date < TRUNC(SYSDATE) AND inv_date >= TRUNC(SYSDATE) -7) THEN 1 END) AS invoice_last_7d,"
			+ " COUNT(CASE WHEN (inv_date < TRUNC(SYSDATE) -7 AND inv_date >= TRUNC(SYSDATE) -15) THEN 1 END) AS invoice_last_15d,"
			+ " COUNT(CASE WHEN (inv_date < TRUNC(SYSDATE) -15 AND inv_date >= TRUNC(SYSDATE) -30) THEN 1 END) AS invoice_last_30d,"
			+ " COUNT(CASE WHEN (inv_date < TRUNC(SYSDATE) -30) THEN 1 END) AS invoice_gt30d"
			+ " FROM"
			+ " (SELECT TO_DATE(TRUNC(a.inv_date)) AS inv_date "
			+ " FROM ER_INVOICE a, ER_STUDY b"
			+ " WHERE pk_study = fk_study"
			+ " AND (EXISTS (SELECT * FROM ER_STUDYTEAM bb WHERE pk_study = bb.FK_STUDY AND bb.fk_user = "+userId+ " AND NVL(studyteam_status,'Active') = 'Active') "
			+ " OR Pkg_Phi.F_Is_Superuser("+ userId + ", a.fk_study) = 1) "
			+ " AND fk_account = "+accountId+ ""
			+ " AND (inv_date <= TRUNC(SYSDATE) AND inv_date >= TRUNC(SYSDATE) -30)"
			+ ")";
			
			query = new BrowserRows();
			query.getPageRows(1,10,queryCount,50,"select 1 from dual","1","asc");
			
			invoice_today = EJBUtil.stringToNum(query.getBValues(1,"invoice_today"));
			invoice_last_7d = EJBUtil.stringToNum(query.getBValues(1,"invoice_last_7d"));
			invoice_last_15d = EJBUtil.stringToNum(query.getBValues(1,"invoice_last_15d"));
			invoice_last_30d = EJBUtil.stringToNum(query.getBValues(1,"invoice_last_30d"));
			invoice_gt30d = EJBUtil.stringToNum(query.getBValues(1,"invoice_gt30d"));
		%>
		<Input type="hidden" id="invoice_today" name="invoice_today" value="<%=invoice_today%>">
		<Input type="hidden" id="invoice_last_7d" name="invoice_last_7d" value="<%=invoice_last_7d%>">
		<Input type="hidden" id="invoice_last_15d" name="invoice_last_15d" value="<%=invoice_last_15d%>">
		<Input type="hidden" id="invoice_last_30d" name="invoice_last_30d" value="<%=invoice_last_30d%>">
		<Input type="hidden" id="invoice_gt30d" name="invoice_gt30d" value="<%=invoice_gt30d%>">
		<%
			int due_today = 0;
			int due_last_7d = 0;
			int due_last_15d = 0;
			int due_last_30d = 0;
			int due_gt30d = 0;
			
			queryCount = "SELECT "
			+ " COUNT(CASE WHEN (TO_DATE(due_by) = TRUNC(SYSDATE)) THEN 1 END) AS due_today,"
			+ " COUNT(CASE WHEN (TO_DATE(due_by) < TRUNC(SYSDATE) AND TO_DATE(due_by) >= TRUNC(SYSDATE) -7) THEN 1 END) AS due_last_7d, "
			+ " COUNT(CASE WHEN (TO_DATE(due_by) < TRUNC(SYSDATE) -7 AND TO_DATE(due_by) >= TRUNC(SYSDATE) -15) THEN 1 END) AS due_last_15d,"
			+ " COUNT(CASE WHEN (TO_DATE(due_by) < TRUNC(SYSDATE) -15 AND TO_DATE(due_by) >= TRUNC(SYSDATE) -30) THEN 1 END) AS due_last_30d,"
			+ " COUNT(CASE WHEN (TO_DATE(due_by) < TRUNC(SYSDATE) -30) THEN 1 END) AS due_gt30d "
			+ " FROM "
			+ " (SELECT CASE WHEN f_to_number(inv_payment_dueby) IS NULL THEN inv_date "
			+ " ELSE inv_date + f_to_number(inv_payment_dueby) "
			+ " END AS due_by "
			+ " FROM ER_INVOICE a, ER_STUDY b "
			+ " WHERE pk_study = fk_study "
			+ " AND (EXISTS (SELECT * FROM ER_STUDYTEAM bb WHERE pk_study = bb.FK_STUDY AND bb.fk_user = "+userId+ " AND NVL(studyteam_status,'Active') = 'Active') "
			+ " OR Pkg_Phi.F_Is_Superuser("+ userId + ", a.fk_study) = 1) "
			+ " AND fk_account = "+accountId+ ")";
			
			query = new BrowserRows();
			query.getPageRows(1,10,queryCount,50,"select 1 from dual","1","asc");
			
			due_today = EJBUtil.stringToNum(query.getBValues(1,"due_today"));
			due_last_7d = EJBUtil.stringToNum(query.getBValues(1,"due_last_7d"));
			due_last_15d = EJBUtil.stringToNum(query.getBValues(1,"due_last_15d"));
			due_last_30d = EJBUtil.stringToNum(query.getBValues(1,"due_last_30d"));
			due_gt30d = EJBUtil.stringToNum(query.getBValues(1,"due_gt30d"));
		%>
		<Input type="hidden" id="due_today" name="due_today" value="<%=due_today%>">
		<Input type="hidden" id="due_last_7d" name="due_last_7d" value="<%=due_last_7d%>">
		<Input type="hidden" id="due_last_15d" name="due_last_15d" value="<%=due_last_15d%>">
		<Input type="hidden" id="due_last_30d" name="due_last_30d" value="<%=due_last_30d%>">
		<Input type="hidden" id="due_gt30d" name="due_gt30d" value="<%=due_gt30d%>">
		<%
			int payment_today = 0;
			int payment_last_7d = 0;
			int payment_last_15d = 0;
			int payment_last_30d = 0;
			int payment_gt30d = 0;
			
			queryCount = "SELECT COUNT(CASE WHEN milepayment_date = TRUNC(SYSDATE) THEN 1 END) AS payment_today,"
			+ " COUNT(CASE WHEN (milepayment_date < TRUNC(SYSDATE) AND milepayment_date >= TRUNC(SYSDATE) -7) THEN 1 END) AS payment_last_7d,"
			+ " COUNT(CASE WHEN (milepayment_date < TRUNC(SYSDATE) -7 AND milepayment_date >= TRUNC(SYSDATE) -15) THEN 1 END) AS payment_last_15d,"
			+ " COUNT(CASE WHEN (milepayment_date < TRUNC(SYSDATE) -15 AND milepayment_date >= TRUNC(SYSDATE) -30) THEN 1 END) AS payment_last_30d,"
			+ " COUNT(CASE WHEN (milepayment_date < TRUNC(SYSDATE) -30) THEN 1 END) AS payment_gt30d"
			+ " FROM"
			+ " (SELECT TO_DATE(TRUNC(a.milepayment_date)) AS milepayment_date "
			+ " FROM ER_MILEPAYMENT a, ER_STUDY b"
			+ " WHERE pk_study = fk_study"
			+ " AND (EXISTS (SELECT * FROM ER_STUDYTEAM bb WHERE pk_study = bb.FK_STUDY AND bb.fk_user = "+userId+ " AND NVL(studyteam_status,'Active') = 'Active') "
			+ " OR Pkg_Phi.F_Is_Superuser("+ userId + ", a.fk_study) = 1) "
			+ " AND fk_account = "+accountId+ " "
			+ " AND (to_date(milepayment_date) <= TRUNC(SYSDATE) AND to_date(milepayment_date) >= TRUNC(SYSDATE) -30) "
			+ " AND milepayment_delflag != 'Y')";
			
			query = new BrowserRows();
			query.getPageRows(1,10,queryCount,50,"select 1 from dual","1","asc");
			
			payment_today = EJBUtil.stringToNum(query.getBValues(1,"payment_today"));
			payment_last_7d = EJBUtil.stringToNum(query.getBValues(1,"payment_last_7d"));
			payment_last_15d = EJBUtil.stringToNum(query.getBValues(1,"payment_last_15d"));
			payment_last_30d = EJBUtil.stringToNum(query.getBValues(1,"payment_last_30d"));
			payment_gt30d = EJBUtil.stringToNum(query.getBValues(1,"payment_gt30d"));
		%>
		<Input type="hidden" id="payment_today" name="payment_today" value="<%=payment_today%>">
		<Input type="hidden" id="payment_last_7d" name="payment_last_7d" value="<%=payment_last_7d%>">
		<Input type="hidden" id="payment_last_15d" name="payment_last_15d" value="<%=payment_last_15d%>">
		<Input type="hidden" id="payment_last_30d" name="payment_last_30d" value="<%=payment_last_30d%>">
		<Input type="hidden" id="payment_gt30d" name="payment_gt30d" value="<%=payment_gt30d%>">
		<%
			float amount_today = 0;
			float amount_last_7d = 0;
			float amount_last_15d = 0;
			float amount_last_30d = 0;
			float amount_gt30d = 0;
			
			queryCount = "SELECT SUM(CASE WHEN milepayment_date = TRUNC(SYSDATE) THEN milepayment_amt END) AS amount_today,"
			+ " SUM(CASE WHEN (milepayment_date < TRUNC(SYSDATE) AND milepayment_date >= TRUNC(SYSDATE) -7) THEN milepayment_amt END) AS amount_last_7d,"
			+ " SUM(CASE WHEN (milepayment_date < TRUNC(SYSDATE) -7 AND milepayment_date >= TRUNC(SYSDATE) -15) THEN milepayment_amt END) AS amount_last_15d,"
			+ " SUM(CASE WHEN (milepayment_date < TRUNC(SYSDATE) -15 AND milepayment_date >= TRUNC(SYSDATE) -30) THEN milepayment_amt END) AS amount_last_30d,"
			+ " SUM(CASE WHEN (milepayment_date < TRUNC(SYSDATE) -30) THEN milepayment_amt END) AS amount_gt30d"
			+ " FROM"
			+ " (SELECT TO_DATE(TRUNC(a.milepayment_date)) AS milepayment_date, milepayment_amt "
			+ " FROM ER_MILEPAYMENT a, ER_STUDY b"
			+ " WHERE pk_study = fk_study"
			+ " AND (EXISTS (SELECT * FROM ER_STUDYTEAM bb WHERE pk_study = bb.FK_STUDY AND bb.fk_user = "+userId+ " AND NVL(studyteam_status,'Active') = 'Active') "
			+ " OR Pkg_Phi.F_Is_Superuser("+ userId + ", a.fk_study) = 1) "
			+ " AND fk_account = "+accountId+ " "
			+ " AND (to_date(milepayment_date) <= TRUNC(SYSDATE) AND to_date(milepayment_date) >= TRUNC(SYSDATE) -30) "
			+ " AND milepayment_delflag != 'Y')";
			
			query = new BrowserRows();
			query.getPageRows(1,10,queryCount,50,"select 1 from dual","1","asc");
			
			amount_today = EJBUtil.stringToFloat(query.getBValues(1,"amount_today"));
			amount_last_7d = EJBUtil.stringToFloat(query.getBValues(1,"amount_last_7d"));
			amount_last_15d = EJBUtil.stringToFloat(query.getBValues(1,"amount_last_15d"));
			amount_last_30d = EJBUtil.stringToFloat(query.getBValues(1,"amount_last_30d"));
			amount_gt30d = EJBUtil.stringToFloat(query.getBValues(1,"amount_gt30d"));
		%>
		<Input type="hidden" id="amount_today" name="amount_today" value="<%=amount_today%>">
		<Input type="hidden" id="amount_last_7d" name="amount_last_7d" value="<%=amount_last_7d%>">
		<Input type="hidden" id="amount_last_15d" name="amount_last_15d" value="<%=amount_last_15d%>">
		<Input type="hidden" id="amount_last_30d" name="amount_last_30d" value="<%=amount_last_30d%>">
		<Input type="hidden" id="amount_gt30d" name="amount_gt30d" value="<%=amount_gt30d%>">

		<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
		<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
		<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
		<Form name="finDashboardpg" method=post onsubmit="" action="db_financial.jsp?">
		<!-- New Dashlet BEGIN-->

		<div class="tmpHeight"></div>
		<DIV id="charts" class="chartDisplay midalign tmpHeight">
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
				<td width="1%"></td>
				<td width="15%" class="lhsFont tmpHeight" valign="top">
					<div id="tree" >
						<span class="trigger" onClick="showChart(0,'Financial Summary');">
							<img src="./js/tree/closed.gif" id="IFSummary" > <%=LC.L_Fin_Summary%><%--Financial Summary*****--%><br>
						</span>
						<span class="trigger" onClick="showBranch('Budget');">
							<img src="./js/tree/closed.gif" id="IBudget"> <%=LC.L_Budgets%><%--Budgets*****--%><br>
						</span>
						<span class="branch" id="Budget" >
							<img src="./js/tree/doc.gif"><a href="#" onClick="showChart(1,'By Modification Date' );"><%=LC.L_By_ModificationDate%><%--By Modification Date*****--%></a>
							<br/>
							<img src="./js/tree/doc.gif"><a href="#" onClick="showChart(2,'By Status' );"><%=LC.L_By_Status%><%--By Status*****--%></a>
							<br/>
						</span>
						<span class="trigger" onClick="showBranch('Milestone');">
							<img src="./js/tree/closed.gif" id="IMilestone"> <%=LC.L_Milestones%><%--Milestones*****--%><br>
						</span>
						<span class="branch" id="Milestone" >
							<img src="./js/tree/doc.gif"><a href="#" onClick="showChart(3,'Achievements' );"><%=LC.L_Achievements%><%--Achievements*****--%></a>
							<br/>
							<img src="./js/tree/doc.gif"><a href="#" onClick="showChart(4,'Forecasting' );"><%=LC.L_Forecasting%><%--Forecasting*****--%></a>
							<br/>
							<img src="./js/tree/doc.gif"><a href="#" onClick="showChart(5,'Receivable Alerts' );"><%=LC.L_Receivables%><%--Receivables*****--%></a>
							<br/>
						</span>
						<span class="trigger" onClick="showBranch('Invoice');">
							<img src="./js/tree/closed.gif" id="IInvoice"> <%=LC.L_Invoices%><%--Invoices*****--%><br>
						</span>
						<span class="branch" id="Invoice" >
							<img src="./js/tree/doc.gif"><a href="#" onClick="showChart(6,'By Invoice Date' );"><%=LC.L_By_InvDate%><%--By Invoice Date*****--%></a>
							<br/>
							<img src="./js/tree/doc.gif"><a href="#" onClick="showChart(7,'Past Due' );"><%=LC.L_Past_Due%><%--Past Due*****--%></a>
							<br/>
						</span>
						<span class="trigger" onClick="showBranch('Payment');">
							<img src="./js/tree/closed.gif" id="IPayment"> <%=LC.L_Payments%><%--Payments--%><br>
						</span>
						<span class="branch" id="Payment" >
							<img src="./js/tree/doc.gif"><a href="#" onClick="showChart(8,'By Payment Date' );"><%=LC.L_By_PaymentDate%><%--By Payment Date*****--%></a>
							<br/>
							<img src="./js/tree/doc.gif"><a href="#" onClick="showChart(9,'By Amount' );"><%=LC.L_By_Amt%><%--By Amount*****--%></a>
							<br/>
						</span>
					</div> 
				</td>
				<td width="25%" align="left">	
				<span style="height: 230px;"></span>				
					<span id="onlyChartParent" style="height: 230px;">
						<div id="onlyChart0" name="onlyChart" class="chart" style="border-width: 1px; border-color: #A80101; border-style: solid; width: 460px; height: 210px; display:none"></div>
						<div id="onlyChart1" name="onlyChart" class="chart" style="border-width: 1px; border-color: #A80101; border-style: solid; width: 460px; height: 210px; display:none"></div>
						<div id="onlyChart2" name="onlyChart" class="chart" style="border-width: 1px; border-color: #A80101; border-style: solid; width: 460px; height: 210px; display:none"></div>
						<div id="onlyChart3" name="onlyChart" class="chart" style="border-width: 1px; border-color: #A80101; border-style: solid; width: 460px; height: 210px; display:none"></div>
						<div id="onlyChart4" name="onlyChart" class="chart" style="border-width: 1px; border-color: #A80101; border-style: solid; width: 460px; height: 210px; display:none"></div>
						<div id="onlyChart5" name="onlyChart" class="chart" style="border-width: 1px; border-color: #A80101; border-style: solid; width: 460px; height: 210px; display:none"></div>
						<div id="onlyChart6" name="onlyChart" class="chart" style="border-width: 1px; border-color: #A80101; border-style: solid; width: 460px; height: 210px; display:none"></div>
						<div id="onlyChart7" name="onlyChart" class="chart" style="border-width: 1px; border-color: #A80101; border-style: solid; width: 460px; height: 210px; display:none"></div>
						<div id="onlyChart8" name="onlyChart" class="chart" style="border-width: 1px; border-color: #A80101; border-style: solid; width: 460px; height: 210px; display:none"></div>
						<div id="onlyChart9" name="onlyChart" class="chart" style="border-width: 1px; border-color: #A80101; border-style: solid; width: 460px; height: 210px; display:none"></div>
					</span>
				</td>				
				<td width="35%" align="left" valign="top">
					<div id="dataT"></div>
				</td></tr>
			</table>
		</DIV>
		<!-- New Dashlet END -->
		<div id="Browser">
			<jsp:include page="ajaxFinancialDataFetch.jsp?nodeNo=0&snodeNo=0" flush="true"/>
		</div>
		<script>
			createChart(0,'Financial Summary' ); showChart(0,'Financial Summary');
			createChart(1,'By Modification Date' );
			createChart(2,'By Status' );
			createChart(3,'Achievements' );
			createChart(4,'Forecasting' );
			createChart(5,'Receivable Alerts' );
			createChart(6,'By Invoice Date' );
			createChart(7,'Past Due' );
			createChart(8,'By Payment Date' );
			createChart(9,'By Amount' );
		</script>
	</Form>
<%} //end of if body for page right
		else{%>
			<jsp:include page="accessdenied.jsp" flush="true"/>
		<%} //end of else body for page right
	} //end of if body for session
	else {%>
		<jsp:include page="timeout.html" flush="true"/>
	<%}%>
</body>
</html>
