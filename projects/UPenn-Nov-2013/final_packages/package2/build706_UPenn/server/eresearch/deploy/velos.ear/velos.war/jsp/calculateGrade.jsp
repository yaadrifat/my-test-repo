<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<TITLE><%=LC.L_Calculation_Results%><%--Calculation Results*****--%></TITLE>
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<script>
	function SendBack(grade,event,from,formobj)
	{
	 var pform = formobj.parentform.value;
	 var gradefld = formobj.gradefld.value;
 	 var dispfield = formobj.dispfield.value;
 	 var advnamefld = formobj.advnamefld.value;
 	 
	 //check if the colname has an index in it
	 //if it has then separate the index from the colname
	 firstPos = gradefld.indexOf("[");	 
	 if (firstPos > 0) {
	 	 secPos = gradefld.lastIndexOf("]");
	 	 idx = gradefld.substring(firstPos+1,secPos);
		 gradefld = gradefld.substring(0,firstPos);
	 }else {
	 	 idx = -1;
	 } 	
			
 	 firstPos = advnamefld.indexOf("[");	 
	 if (firstPos > 0) {
	 	 secPos = advnamefld.lastIndexOf("]");
	 	 idx = advnamefld.substring(firstPos+1,secPos);
		 advnamefld = advnamefld.substring(0,firstPos);
	 }else {
	 	 idx = -1;
	 } 	

   
 	 firstPos = dispfield.indexOf("[");	 
	 if (firstPos > 0) {
	 	 secPos = dispfield.lastIndexOf("]");
	 	 idx = dispfield.substring(firstPos+1,secPos);
		 dispfield = dispfield.substring(0,firstPos);
	 }else {
	 	 idx = -1;
	 } 		  
	 
	
	//set the values depending on whether multiple elements of the same name exist in the form 
	 if (idx >=0) { 
	  	 window.opener.document.forms[pform].elements[gradefld][idx].value=grade;
	 } else {
	 	 window.opener.document.forms[pform].elements[gradefld].value=grade;
	 } 
	 
	 if (idx >=0) {
	   	 window.opener.document.forms[pform].elements[advnamefld][idx].value=event;
	 } else {
	   	 window.opener.document.forms[pform].elements[advnamefld].value=event;	 
	 } 
   	 		
	 this.close();
	}

 
</script>



<BODY>
<jsp:useBean id="advB" scope="request" class="com.velos.esch.web.advEve.AdvEveJB"/>
<%@ page language = "java" import = "com.velos.esch.service.util.EJBUtil,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>


<%
 HttpSession tSession = request.getSession(true); 
 if (sessionmaint.isValidSession(tSession))
 {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
	//String eSign = request.getParameter("eSign");
	//String oldESign = (String) tSession.getValue("eSign");
	
	String ipAdd = (String) tSession.getValue("ipAdd");

	//if(!(oldESign.equals(eSign)))  
	//{
	//} 
	//else {



	String toxicityGroup = request.getParameter("dToxicityGroup");
	
	String labDate =request.getParameter("labDate");
	String labResult = request.getParameter("labResult");
	String labUnit = request.getParameter("labUnit");	
	String labSite =request.getParameter("labSite");
	String labName = request.getParameter("labName"); //SV, 8/5		
	String labLLN = request.getParameter("otherLLN");
	String labULN = request.getParameter("otherULN");

	String nciVersion =request.getParameter("nciVersion");
	String patient = request.getParameter("patient");

	String parentform = request.getParameter("parentform");
	String gradefld = request.getParameter("gradefld");
	String advnamefld =	request.getParameter("advnamefld");
	String dispfield =	request.getParameter("dispfield");

	

	int userRangeFlag = 0;


			
	if ( labLLN.trim().equals("") && labULN.trim().equals("") )
		userRangeFlag = 0 ;
	else
		userRangeFlag = 1;


	NCILibDao nciLDao = new NCILibDao();

	nciLDao = advB.getToxicityGrade(EJBUtil.stringToNum(toxicityGroup), labDate, labUnit,  EJBUtil.stringToNum(labSite), labResult, EJBUtil.stringToNum(labLLN), EJBUtil.stringToNum(labULN), userRangeFlag, nciVersion, EJBUtil.stringToNum(patient));

	ArrayList toxicityName = nciLDao.getToxicityName();
	ArrayList toxicityGrade = nciLDao.getToxicityGrade();
	ArrayList toxicityGradeDesc = nciLDao.getToxicityGradeDesc();
	String toxName = ""; 
	String toxGrade = "";
	int calcGrade = -1 ;
	
	String styleText = "";

	int llnUsed = -10000000 ;
	int ulnUsed = -10000000 ;
	
	if (toxicityName.size() > 0)
		toxName = (String) toxicityName.get(0);
	
	calcGrade = nciLDao.getCalculatedToxicityGrade();

	llnUsed = nciLDao.getLlnUsed();
	ulnUsed = nciLDao.getUlnUsed();
	
	if (EJBUtil.stringToNum(labSite) == 0)
		labName = LC.L_Not_Specified/*"Not Specified"*****/; //SV,8/5, changed labSite->labName	
	
%>


<BR>	
	<P class="sectionHeadings"><%=LC.L_Calculation_Results%><%--Calculation Results*****--%></P>
<form name="calcGrade" method="POST">
   	<INPUT type=hidden name= parentform value = <%=parentform%>>		
   	<INPUT type=hidden name= gradefld value = <%=gradefld%>>		
   	<INPUT type=hidden name= dispfield value = <%=dispfield%>>				   
   	<INPUT type=hidden name= advnamefld value = <%=advnamefld%>>				   
		
	<table class="basetbl" width="100%">
		<tr>
			<td  colspan = 2 >
				<b><%=toxName%>	</b>
			</td>
			<td  width = 20%>
				<b><%=LC.L_Lab_Facility%><%--Lab Facility*****--%></><b>
			</td>
			<td  width = 40%>
				<!--SV,8/5, changed to labName-->
				<%=labName%> 	
			</td>
		</tr>
		<tr>
		 <td width = 20%>
				<b><%=LC.L_Lab_Date%><%--Lab Date*****--%></b>
			</td>
			<td width = 20%>
				<%=labDate%>	
			</td>
		 <td width = 20%>
				<b><%=LC.L_Lab_Result%><%--Lab Result*****--%></b>
			</td>
			<td  width = 40%>
				<%=labResult%>&nbsp;<%=labUnit%>	
			</td>
		</tr>
		<tr>
		    <td  width = 20%>
				<b><%=LC.L_Lln_Upper%><%--LLN*****--%></b>
			</td>
			<td width = 20%>
				<%=llnUsed%>
			</td>
			<td  width = 40%>
			<b><%=LC.L_Uln_Upper%><%--ULN*****--%></b>
			</td>
			<td  width = 20%>
				<%=ulnUsed%>	
			</td>	
		</tr>
	</table>
				<BR>		
<table class="browserDefault basetbl" width="100%" border=0>
	<%			
		int counter = 0;
	
		for (counter = 0; counter <= toxicityName.size() -1 ; counter++)
		{
			toxGrade = (String) toxicityGrade.get(counter);
			if ( Integer.parseInt(toxGrade) == calcGrade )
				styleText = " style = \"border-style: solid; border-width: 3; border-color:#FF0000\" ";
			else
				styleText = "";
			
			if ((counter%2) == 0) { %>
 				 <tr class="browserEvenRow"> 
        	<%
				}else{
			  %>
 			    <tr class="browserOddRow"> 
	         <% }	
		%>

			<td width="15%" <%=styleText%>>
			<% Object[] arguments = {toxGrade}; %><%=VelosResourceBundle.getLabelString("L_Grade_Dyna",arguments) %>
			<%-- <%=LC.L_Grade%>	<%=toxGrade%> --%> 	

			</td>
			
			<td width="80%" <%=styleText%>>	
			   <%= toxicityGradeDesc.get(counter)%>
			</td>
			
			<td width="10%" <%=styleText%>>
				<A href="#" Onclick="SendBack('<%=toxGrade%>','<%=toxName%>','adv', document.calcGrade )"><%=LC.L_Select%><%--Select*****--%></A>
			</td>     	
	 	</tr>		
		
	<%	
		}
		
	%>
		
		
	</table>	
</form>	

<%
	//}        //end of if for eSign
} //end of if for session
else{%>
<jsp:include page="timeout_childwindow.jsp" flush="true"/> 
<%
}%>





</BODY>
</HTML>
