<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- 
/**
* Auther : Yogesh Kumar
* Purpose: To display new Visits for Changing Sequence
* Date	 : 13-Sep-2011 
*/
 -->

<html>
<head>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="protVisitB" scope="request" class="com.velos.esch.web.protvisit.ProtVisitJB"/>

<%@ page language = "java" import ="com.velos.eres.business.group.*,java.net.URLEncoder,com.velos.eres.business.common.*,
com.velos.esch.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.StringUtil,
com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB, 
com.velos.esch.web.eventdef.EventdefJB, com.velos.esch.web.eventassoc.EventAssocJB, com.velos.eres.service.util.LC, com.velos.eres.service.util.MC"%>

<jsp:useBean id="eventlistdao" scope="request" class="com.velos.esch.business.common.EventdefDao"/>
<jsp:useBean id="assoclistdao" scope="request" class="com.velos.esch.business.common.EventAssocDao"/>
<jsp:useBean id="visitdao" scope="request" class="com.velos.esch.business.common.ProtVisitDao"/>

<%@page import="java.util.ArrayList"%>
<jsp:include page="localization.jsp" flush="true"/>
<Link Rel=STYLESHEET HREF='./styles/yuilookandfeel/yuilookandfeel.css' type=text/css>
<link Rel="stylesheet" HREF="./styles/velos.css" type="text/css" >

<%
String src= StringUtil.htmlEncodeXss(request.getParameter("srcmenu"));
%>

</head>
<script>
var changesMadeToVisit = false; 
jQuery.noConflict(); /*stops conflicting code with jQuery syntax*/

jQuery(function() {
//*YK: Fixed Bug# 7278*/
if(jQuery("#insertFormContent"))
	{
		jQuery("#insertFormContent").html("");
	}
	if (jQuery("button")){
		jQuery( "button").button();
	}
	/*Open/Closes Visit Panel*/
	jQuery( "#VisitAccordian" ).accordion({
		autoHeight: false, collapsible: true, active: false, fillSpace: false, animated: false
	});
	
});
</script>
<title>Modify Sequence and Display</title>
<body class="yui-skin-sam yui-dt yui-dt-liner" style="overflow-y:hidden">
<%
   HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession)){
		String protocolName = "";
		String calledFrom = request.getParameter("calledFrom");
		
		String mode = request.getParameter("mode");
	    String protocolId = request.getParameter("protocolId");
	    SchCodeDao scho = new SchCodeDao();
	  
	    String visitNameId = request.getParameter("visitList");
		if (visitNameId==null) visitNameId = "";
	    String calStatus = request.getParameter("calStatus");
	    String calAssoc=request.getParameter("calassoc");
		calAssoc=(calAssoc==null)?"":calAssoc;
		
	    String visitSel=request.getParameter("visitSel");
	    visitSel=(visitSel==null)?"ALL":visitSel;
		
		
	    String calStatusPk = "";
		String calStatDesc = "";
		
		String evtName = "";
		evtName =request.getParameter("searchEvt");
		evtName=(evtName==null)?"":evtName;
		
		if (StringUtil.isEmpty(protocolName) == true) {	
		 if (calledFrom.equals("P")  || calledFrom.equals("L"))
			{ // display the calendar name
			   eventdefB.setEvent_id(EJBUtil.stringToNum(protocolId));
			   eventdefB.getEventdefDetails();
			   protocolName = eventdefB.getName();
			   calStatusPk = eventdefB.getStatCode();
			   calStatus = scho.getCodeSubtype(EJBUtil.stringToNum(calStatusPk)).trim();
			   calStatDesc = scho.getCodeDescription(EJBUtil.stringToNum(calStatusPk));
			}
			else if (calledFrom.equals("S"))
			{  //called From Study
		       // display the calendar name
			   eventassocB.setEvent_id(EJBUtil.stringToNum(protocolId));
			   eventassocB.getEventAssocDetails();
			   protocolName = eventassocB.getName();
			   calStatusPk = eventassocB.getStatCode();
			   calStatus = scho.getCodeSubtype(EJBUtil.stringToNum(calStatusPk)).trim();
			   calStatDesc = scho.getCodeDescription(EJBUtil.stringToNum(calStatusPk));
			}
		   }
	  	ArrayList visitIds = null;
		ArrayList visitNames = null;
		ArrayList visitDisplacements = null;
		
		visitdao = protVisitB.getProtocolVisits(EJBUtil.stringToNum(protocolId));
		visitIds = visitdao.getVisit_ids();
		visitNames = visitdao.getNames();
		visitDisplacements = visitdao.getDisplacements();
		
		if (protocolName == null) { protocolName = ""; }
	    int noOfVisits = visitIds == null ? 0 : visitIds.size();
	    String offLineFlag ="";
		String hideFlag ="";
	
	  
	%>

<script type="text/javascript">
//YK: Added for PCAL-20461 - Adds Sortable functionality to Event, under Visit
jQuery(function() {
    <% for (int iX=0; iX<noOfVisits; iX++) {%>
    jQuery( "#<%=visitIds.get(iX)%> tbody" ).sortable({
    	items: "tr:not(.noEvent)",
    	axis: "y" , scroll: false,
    	placeholder: "ui-state-highlight",
    	forcePlaceholderSize: true,
    	beforeStop: function( event, ui ) {return nd();}
    	
	});
   <%}%>

   
});

var visitID="";
var visitIDe="";
var visitName="";
var jSonEvents="";
//YK: Added for PCAL-20461 - Loads Events under Visit
function loadEventsVisit(visitid, calledFrom, protocolId,calStatus,reload) {	
changesMadeToVisit = false;
var needToCall = false;
//if(visitID!="")
//{
	//checkChanges(visitID);
	//if(sequenceShuffleData!="[]" || hideEventData!="[]" )
	//{ 	
//		alert("Please save the change before moving forward.");
		//return false;
	//}
//}
//YK: Fixed Bug#7284*/
if(visitIDe!=""){
jQuery('#eventsDisplay'+visitIDe).html("<tr><td><p><%=LC.L_Loading%>...<img src=\"../images/jpg/loading_pg.gif\" /></p></td></tr>"); // Wipe out last data for performance
}
var myObj = jQuery('#eventsDisplay'+visitid);
if (myObj[0] && myObj[0].innerHTML.indexOf('<%=LC.L_Loading%>...') > -1) {
	needToCall = true;
} else {
	jQuery('#eventsDisplay'+visitid).html("<tr><td><p><%=LC.L_Loading%>...<img src=\"../images/jpg/loading_pg.gif\" /></p></td></tr>"); // Wipe out last data for performance
	needToCall = false;
}
visitID="";
visitName="";
jSonEvents="";
if(reload=="true")
{
	needToCall = true;
}
	if (needToCall) {
	var urlCall="manageVisitEvents.jsp?calledFrom="+calledFrom+"&protocolId="+protocolId+"&visitList="+visitid+"&calStatus="+calStatus+"";
	jQuery.ajax({
			url : urlCall,
			cache: false,
			success : function (data) {
			jQuery('#eventsDisplay'+visitid).html(data);
			loadEventsInVisits(visitid);
			}
			});
	
	}
	visitIDe=visitid;
}
//YK: Added for PCAL-20461 - Creates a JSON Array when Events are loaded.
function loadEventsInVisits(visitid) {
var dataJson="";
var indexNo=0;
if(jSonEvents.length>0)
{
	jSonEvents="";
}
 if(visitid!=visitID)
	{
	visitID=visitid;
	
	jSonEvents ="[";
	 <% for (int iX=0; iX<noOfVisits; iX++) {%>
	if(visitid == <%=visitIds.get(iX)%>)
	{
		jQuery("#<%=visitIds.get(iX).toString()%> tbody tr:not(:last)").each(function() {
			visitName="<%=(String)visitNames.get(iX)%>";
			indexNo++;
		    jSonEvents +="{";
		    jSonEvents +="\"index\":\""+indexNo+"\",";
            jSonEvents +="\"visitid\":\""+visitid+"\",";
    		var trValue =jQuery(this).html();
		    jQuery(this).find("td").each(function(index){
			 
	           	var tdValue =jQuery(this).html();
	        
	            if(tdValue!="")
    	    	{
	            	if(index==0)
	    	    		{
	    	    			jSonEvents +="\"eventId\":\""+tdValue+"\",";
	    	    		}
	    	    	else if (index==1)	
    	    		{
    	    			jSonEvents +="\"seq\":\""+tdValue+"\",";
    	     		}
	    	    	else if (index==2)	
    	    		{
						/* Bug#9012 16-May-2012 -Sudhir*/
	    	    		tdValue = tdValue.replace(/[^a-zA-Z 0-9]+/g, "");
    	    			jSonEvents +="\"name\":\""+tdValue+"\",";
    	     		}
	    	    	else 
	    	    	{
	    	    	
	    	    		 if(tdValue.indexOf("hidden")!=-1)
	    	    			{
		    	    		 if(tdValue.indexOf("unchecked")==-1)
		    	    			{
		    	    			 jSonEvents +="\"hidden\":\"1\"";
	    	    				}
	    	    			 else
	    	    				{ 
	    	    				 jSonEvents +="\"hidden\":\"0\"";
	    	    				}
	    	    			}
	    	    		 else
    	    				{ 
    	    				jSonEvents +="\"hidden\":\"disabled\"";
    	    				}
	    	    		}
    	    	   }
    	    	});
	     	jSonEvents +="},";
	   
    	 });
	 }
	   <%}%>
	  jSonEvents +="]";
	  jSonEvents  = jSonEvents.replace('},]','}]');
 }else{
	 visitID="";
	 visitName="";
	 }
}
var jSonEventsSeq="";
var visitSeqName="";
//YK: Added for PCAL-20461 - Creates JSON Array when we Save the changes or before closing the dialog window o check for changes. 
function changeEventsSequenceInVisits(visitid) {
	var dataJson="";
	var indexNo=0;
		jSonEventsSeq ="[";
		 <% for (int iX=0; iX<noOfVisits; iX++) {%>
		if(visitid == <%=visitIds.get(iX)%>)
		{
			jQuery("#<%=visitIds.get(iX).toString()%> tbody tr:not(:last)").each(function() {
				visitSeqName="<%=(String)visitNames.get(iX)%>";
				indexNo++;
			    jSonEventsSeq +="{";
			    jSonEventsSeq +="\"index\":\""+indexNo+"\",";
	            jSonEventsSeq +="\"visitid\":\""+visitid+"\",";
				var trValue =jQuery(this).html();
			    jQuery(this).find("td").each(function(index){
			   	var tdValue =jQuery(this).html();
 	            if(tdValue!="")
	    	    	{
 	             	if(index==0)
		    	    		{
 	    		    			jSonEventsSeq +="\"eventId\":\""+tdValue+"\",";
		    	    		}
		    	    	else if (index==1)	
	    	    		{
		        			jSonEventsSeq +="\"seq\":\""+tdValue+"\",";
	    	     		}
		    	    	else if (index==2)	
		    	    		{
		    	    			/* Bug#9012 16-May-2012 -Sudhir*/
		    	    			tdValue = tdValue.replace(/[^a-zA-Z 0-9]+/g, "");
		    	    			jSonEventsSeq +="\"name\":\""+tdValue+"\",";
		    	     		}
		    	    	else 
		    	    		{
		    	    	 if(tdValue.indexOf("hidden")!=-1)
		    	    			{
			    	    		 if(tdValue.indexOf("unchecked")==-1)
			    	    			{
				    	    			jSonEventsSeq +="\"hidden\":\"1\"";
		    	    				}
		    	    			 else
		    	    				{ 
		    	    				 	jSonEventsSeq +="\"hidden\":\"0\"";
		    	    				}
		    	    			}
		    	    		 else
	    	    				{ 
	    	    				 	jSonEventsSeq +="\"hidden\":\"disabled\"";
	    	    				}
		    	    		}
	    	    	   }
	    	    	});
 	     	jSonEventsSeq +="},";
 	   
	    	 });
		 }
		   <%}%>
		  jSonEventsSeq +="]";
		  jSonEventsSeq  = jSonEventsSeq.replace('},]','}]');
	
}

var sequenceShuffleData="";
var hideEventData="";
//YK: Added for PCAL-20461 - Checks of any thing has been modified.
function checkChanges(visitid)
{
	changeEventsSequenceInVisits(visitid);
	if((jSonEvents == "" || jSonEvents.indexOf(M_NoEvt_InThisVisit)>0) 
		&& (jSonEventsSeq == "" || jSonEventsSeq.indexOf(M_NoEvt_InThisVisit)>0))
	{
		jSonEvents ="";
		jSonEventsSeq = "";
	}
	if(jSonEventsSeq !="[]" && jSonEventsSeq !="")
	{
		changesMadeToVisit = true;
    	//YK: Added for PCAL-20461 - Creates JSON Array for Sequence Changes
		sequenceShuffleData="[";
		//YK: Added for PCAL-20461 -Creates JSON Array for Hide/Unhide Changes
		hideEventData="[";
		var eventJsonData = jQuery.parseJSON(jSonEvents);
		var eventSeqJson  = jQuery.parseJSON(jSonEventsSeq);
		jQuery.each(eventJsonData , function(key, value) { 
			var indexEvent=value.index;
			var eventNo=value.seq;
			var eventHidden=value.hidden;
			var eventId=value.eventId;
			jQuery.each(eventSeqJson , function(key, value) {
				var indexSeqEvent=value.index;
				var eventSeqNo=value.seq;
				var eventSeqId=value.eventId;
				var evntSeqHid=value.hidden;
				
				if(indexSeqEvent == indexEvent && eventSeqNo!=eventNo ) 
				  {
					sequenceShuffleData +="{";
					sequenceShuffleData +="\"eventID\":\""+eventSeqId+"\",";
					sequenceShuffleData +="\"eventSeqNew\":\""+eventNo+"\"";
					sequenceShuffleData +="},";
				  }
				if(evntSeqHid!=eventHidden &&  eventId==eventSeqId) 
				  {
					hideEventData +="{";
					hideEventData +="\"eventID\":\""+eventSeqId+"\",";
					hideEventData +="\"eventHiddenNew\":\""+value.hidden+"\"";
					hideEventData +="},";
				  }
			
				  //dataJson +='EventID : ' + value.eventId + ' , Sequence : '+value.seq +' , EventName : ' +value.name+' , EventHidden : '+value.hidden +'<BR>'; 
			  });
			
		  });
		sequenceShuffleData +="]";
		sequenceShuffleData  = sequenceShuffleData.replace('},]','}]');
		hideEventData +="]";
		hideEventData  = hideEventData.replace('},]','}]');
	}
	
}

//YK: Added for PCAL-20461 - Updated the changes in Database
function updateEventSequence(visitid,calledFrom,calStatus,protocolId,calAssoc)
{
	//YK: Added for PCAL-20461 - Checks for changes
	checkChanges(visitid);
	if((sequenceShuffleData!="" && sequenceShuffleData!="[]" )|| (hideEventData!="" &&  hideEventData!="[]" ))
		{
		//Yogendra Pratap : Modified for removing eSign for PCAL 22169 : Date-12 March 2012 
		
			var urlCall="updateSequenceAndDisplay.jsp?sequenceShuffleData="+sequenceShuffleData+"&hideEventData="+hideEventData+"&calledFrom="+calledFrom+"&calStatus="+calStatus+"&protocolId="+protocolId+"&calAssoc="+calAssoc+"";
			openMessageDialog('progressMsg');
			jQuery.ajax({
					url : urlCall,
					success : function (data) {
					if(data.indexOf("Successful")>0)
					{
					 closeMessageDialog('progressMsg');	
					 openMessageDialog('successMsg');
					 window.setTimeout('closeMessageDialog("successMsg")', 1000);
					}else{
						closeMessageDialog('progressMsg');
						openMessageDialog('failureMsg');
						window.setTimeout('closeMessageDialog("failureMsg")', 1500);
						}
					
					loadEventsVisit(visitid, calledFrom, protocolId,calStatus,'true');
					//Yogendra Pratap : Modified for removing eSign for PCAL 22169 : Date-12 March 2012
					jSonEventsSeq="";
					sequenceShuffleData="";
					hideEventData="";
			    	}
					});
		}else{
			changesMadeToVisit = false;
			alert(M_ThereNo_ChgToSave);
			return false;
		}
		
}
//YK: Added for PCAL-20461 - Identify the Event if it has to hidden or unhide
function checkUncheck(checkBoxId)
{
	var hiddenValueCheckBox="chkHid"+checkBoxId;
	var checkBocvalue="chk"+checkBoxId;
	if(document.getElementById(checkBocvalue).checked)
	{
		document.getElementById(hiddenValueCheckBox).value="checked";
	}else{
		document.getElementById(hiddenValueCheckBox).value="unchecked";
	}
	
}
//YK: Added for PCAL-20461 - Opens the Dialog for Result Window
function openMessageDialog(divId)
{
	//YK: Fixed Bug#7416
	var messageHgth = navigator.userAgent.indexOf("MSIE") != -1 ? 150 : 80;
	jQuery("#"+divId).dialog({
		height: messageHgth,width: 500,position: 'center',resizable: false,modal: true,autoOpen: false
    }).siblings('.ui-dialog-titlebar').remove();
	jQuery("#"+divId).dialog("open");
	
}
//YK: Added for PCAL-20461 - Closes the Result window Dialog
function closeMessageDialog(divId)
{
	jQuery("#"+divId).dialog("close");
}

//YK: Added for PCAL-20461 - Check for changes before Clsoing the Dialog
function checkBeforeClose()
{
	if(visitID!="")
	{
		checkChanges(visitID);
		if (!changesMadeToVisit)
			return true;
		if((sequenceShuffleData!="" && sequenceShuffleData!="[]" )|| (hideEventData!="" &&  hideEventData!="[]" ))
		{ 	
			var r=confirm("<%=MC.M_SaveBeforeMovingFwd%>");
			if (r)
			  {
				return false;
			  }
		}
	}
	return true;
	}

//YK:Added for Enhancement Bug #7417
function selectAll(visitID)
{
	var containerId='eventsDisplay'+visitID;
	var container = document.getElementById(containerId);
	var itemsLength = container.getElementsByTagName('input').length;
	var selectCheckbox= 'selectAll'+visitID;
	var selectId= document.getElementById(selectCheckbox);
	
	for(var iY = 0; iY < itemsLength; iY++) {
	
		    if(container.getElementsByTagName('input').item(iY).type=="checkbox")
		    {
			    if(selectId.checked)
		    	container.getElementsByTagName('input').item(iY).checked=true;
			    else
			    container.getElementsByTagName('input').item(iY).checked=false;
				    
			 }
		    if(container.getElementsByTagName('input').item(iY).type=="hidden")
		    {
		    	if(selectId.checked)
		    	container.getElementsByTagName('input').item(iY).value='checked';
		    	else
		    	container.getElementsByTagName('input').item(iY).value='unchecked';
			    	
			 }
			
	
     }
}
	

</script>
	
	<table width="98%">
	<tr>
		<td width="40%">
		<p class="defComments">&nbsp;&nbsp;<%=LC.L_Calendar%>: <B>
			<%=protocolName%>
		</B>
	&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Cal_Status%>: <B><%=calStatDesc%></B></p></td>
	</tr>
	<tr>
		<td width="40%" align="center">
		<br>
		<p class="defComments"><b><%=MC.M_PlsVisitPanel_SeqEvt%>.<br>
		<%=MC.M_DragEvt_ChgSeq%>. <%=MC.M_ChkBoxHide_UnhideEvt%>.
		</b></p>
		<font class="Mandatory"><b><%=MC.M_EvntVisitSeqSave%></b></font>
		<br>
		</td>
	</tr>
	<tr>
	<td>
	<div id="VisitAccordian">
	<%
	  for (int iX=0; iX<noOfVisits; iX++) {
		  if(visitSel.equals("ALL"))
	       {//YK: Added for PCAL-20461 - Creates Visit Panel for All visits
	       %>
	   		<h3 onclick="loadEventsVisit('<%=visitIds.get(iX)%>','<%=calledFrom%>','<%=protocolId%>','<%=calStatus%>','false')"><a href="javascript:void(0);" ><%=(String)visitNames.get(iX)%></a></h3>
			<div >
			<table id="<%=visitIds.get(iX).toString()%>"  width="100%" border="0" cellspacing="0" cellpadding="0" class="tablebdr" >
			<thead>
			<tr class="noEvent">
			<th width="5%" class="normalcol"  height="20" style="display: none;">&nbsp;</td>
			<th width="5%" class="normalcol"  height="20"><%=LC.L_Sequence%></td>
			<th width="50%" class="normalcol" ><%=LC.L_Event_Name%></td>
			<th width="10%" class="normalcol" ><%=LC.L_Hidden%> <input type="checkbox" id="selectAll<%=visitIds.get(iX).toString()%>" name="selectAll<%=visitIds.get(iX).toString()%>" <%if(!calStatus.equals("O")){%>DISABLED<% }%> onclick="selectAll('<%=visitIds.get(iX).toString()%>');"/> </td>
			</tr>
			</thead>
			<tbody id="eventsDisplay<%=visitIds.get(iX)%>" onmouseover="this.style.cursor='move';">
			<tr><td colspan="4"><p><%=LC.L_Loading%>...<img class="asIsImage" src="../images/jpg/loading_pg.gif" /></p></td></tr>
			</tbody>	
			</table>
			<table   width="100%" border="0" style="margin-bottom: 3px; margin-top: 3px;" cellspacing="0" cellpadding="0"  style="vertical-align: top;">
			<tr class="noEvent" >
				<td colspan="3" align="right">
				<button id="saveEventData" name="saveEventData" type="button" onclick="updateEventSequence('<%=visitIds.get(iX)%>','<%=calledFrom%>','<%=calStatus%>','<%=protocolId%>','<%=calAssoc%>');" ><%=LC.L_Save%><%--Save--%></button>
				</td>
			</tr>
			</table>
		    </div>
			
		   <%}else{ //YK: Added for PCAL-20461 - Creates Visit Panel when called for individual Visit
		   if(visitSel.equals(visitIds.get(iX).toString()))
		   {%>
	   		<h3 onclick="loadEventsVisit('<%=visitIds.get(iX)%>','<%=calledFrom%>','<%=protocolId%>','<%=calStatus%>','false')"><a href="javascript:void(0);"><%=(String)visitNames.get(iX)%></a></h3>
			<div >
			<table id="<%=visitIds.get(iX).toString()%>"  width="100%" border="0" cellspacing="0" cellpadding="0" class="tablebdr" >
			<thead>
			<tr class="noEvent">
			<td width="5%"  class="normalcolNo" height="20" style="display: none;">&nbsp;</td>
			<td width="5%"  class="normalcolNo" height="20"><%=LC.L_Sequence%></td>
			<td class="normalcol" width="50%"><%=LC.L_Event_Name%></td>
			<td class="normalcol" width="10%"><%=LC.L_Hidden%> <input type="checkbox" id="selectAll<%=visitIds.get(iX).toString()%>" name="selectAll<%=visitIds.get(iX).toString()%>" <%if(!calStatus.equals("O")){%>DISABLED<% }%> onclick="selectAll('<%=visitIds.get(iX).toString()%>')"/></td>
			</tr>
			</thead>
			<tbody id="eventsDisplay<%=visitIds.get(iX)%>" onmouseover="this.style.cursor='move';">
			<tr><td colspan="4"><p><%=LC.L_Loading%>...<img class="asIsImage" src="../images/jpg/loading_pg.gif" /></p></td></tr>
			</tbody>	
			</table>
			<table   width="100%" style="margin-bottom: 3px; margin-top: 3px;" border="0" cellspacing="0" cellpadding="0"  style="vertical-align: top;">
			<tr class="noEvent">
				<td colspan="3" align="right">
				<button id="saveEventData" name="saveEventData" type="button" onclick="updateEventSequence('<%=visitIds.get(iX)%>','<%=calledFrom%>','<%=calStatus%>','<%=protocolId%>','<%=calAssoc%>');" ><%=LC.L_Save%><%--Save--%></button>
				</td>
			</tr>
			</table>
		    </div>
			
		   <%}
		   }
			   
		   
	    }
	%>
	</div>
	<%--//YK: Added for PCAL-20461 - Dialog for Loading.... Window --%>
	<div id='progressMsg' style="display:none;"><br><p class="sectionHeadings" align="center"><%=LC.L_Please_Wait%>... <img class="asIsImage"  src="../images/jpg/loading_pg.gif" /></p></div>
	<%--//YK: Added for PCAL-20461 - Dialog for Success Message Window--%>
	<div id='successMsg'  style="display:none;"><br><p class="sectionHeadings" align="center"><%=MC.M_Data_SavedSucc%></p></div>
	<%--//YK: Added for PCAL-20461 - Dialog for Failure message window--%>
	<div id='failureMsg'  style="display:none;"><br><p class="sectionHeadings" align="center"><%=MC.M_Changes_CntSvd%> </p></div>
	</td>
	</tr>
	</table>
	<%-- Yogendra Pratap : Modified for removing eSign for PCAL 22169 : Date-12 March 2012--%>
	<%
	
	
}//end of if body for session
	else{%> 
	 <jsp:include page="timeout.html" flush="true" /> 
<%}%>



</body>
</html>
