<!--
This JSP will be included in gadgetHome.jsp. All non-local javascript variables and functions 
will be global in the mother JSP. Therefore, make sure to suffix all non-local variables and functions
with the gadget ID.
 -->

<%@page import="com.velos.eres.service.util.VelosResourceBundle"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC,com.velos.eres.service.util.Security"%> 
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<%@page import="java.text.DecimalFormat" %>
<%
if (!sessionmaint.isValidSession(request.getSession(false))) {
	return;
}
boolean isIE = false;
HttpSession tSession = request.getSession(true); 
String keySessId = "";
if (sessionmaint.isValidSession(tSession)) {
	String sessId = tSession.getId();
	if (sessId.length()>8) { sessId = sessId.substring(0,8); }
	sessId = Security.encrypt(sessId);
	char[] chs = sessId.toCharArray();
	StringBuffer sb = new StringBuffer();
	DecimalFormat df = new DecimalFormat("000");
	for (int iX=0; iX<chs.length; iX++) {
	    sb.append(df.format((int)chs[iX]));
	}
	keySessId = sb.toString();
	try { isIE = request.getHeader("USER-AGENT").toUpperCase().indexOf("MSIE") != -1; } catch(Exception e) {}
}
String src;
src= request.getParameter("srcmenu");
String selectedTab = request.getParameter("selectedTab");
%>

<script>
var L_Study_Title = '<%=LC.L_Study_Title%>';
var L_Std_Admin = '<%=LC.L_Std_Admin%>';
var L_PatMgmt_EnrlPat = '<%=LC.L_PatMgmt_EnrlPat%>';
var L_Budgets = '<%=LC.L_Budgets%>';
var L_Calendars = '<%=LC.L_Calendars%>';
</script>

<!-- Data-specific style definitions -->
<style>
.yui-dt-col-invoiceable > div.yui-dt-liner { text-align:right; }
.yui-dt-col-invoiced > div.yui-dt-liner { text-align:right; }
.yui-dt-col-uninvoiced > div.yui-dt-liner { text-align:right; }
.yui-dt-col-collected > div.yui-dt-liner { text-align:right; }
.yui-dt-col-outstanding > div.yui-dt-liner { text-align:right; }
.yui-dt-col-receipts > div.yui-dt-liner { text-align:right; }
.yui-dt-col-disbursements > div.yui-dt-liner { text-align:right; }
.yui-dt-col-netCash > div.yui-dt-liner { text-align:right; }
</style>
<script>
//Declare all functions here
var gadgetMyStudies = {
	settings: {
		addStudyCriteria: {},
		allStudyData: [],
		changeStudyCriteria: {},
		data: {},
		formatMyStudiesHTML: {},
		setAutoComplete: {},
		screenAction: {},
		openSettings: {},
		getAllStudies: {},
		getSettings: {},
		updateSettings: {},
		submitSettings: {},
		cancelSettings: {},
		clearSettings: {},
		openDeleteDialogStudy: {},
		deleteStudy: {}
	},
	data: {},
	reloadGrid: {},
	formatCells: {},
	screenAction: {},
	clearErrMsg: {},
	getMyStudies: {},
	beforeSubmitStudy:{},
	beforeSubmitPatient: {},
	beforeSubmitPatientStudy: {}
};

gadgetMyStudies.formatCells = function(el, oRecord, oColumn, oData) {
	if (oData != null){
		var key = oColumn.key;

		switch (key){
		case 'quickAccess':
			var inHTML = '';
			var studyId = oRecord.getData('fk_study');

			inHTML = '<table width="100%" cellpadding="0" cellspacing="0" style="border:none" align="center" border="0">'
				+ '<tr>'
				+ '<td width="25%" style="border: 0px">';
				
			var studyIcon = oRecord.getData('studyIcon');
			if (studyIcon == 1){
				inHTML += '<a href="study.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&studyId='+studyId+'"><img class="studyMenuPop" id="'+studyId+'" src="../images/jpg/study.gif" title="'+L_Std_Admin+'" border=0 ></a>';
			}
						
			inHTML += '</td>'
				+ '<td width="25%" style="border: 0px">';
				
			var patientIcon = oRecord.getData('patientIcon');
			if (patientIcon == 1){
				inHTML += '<a href="studypatients.jsp?srcmenu=tdmenubaritem5&srcmenu=&patid=&patstatus=&selectedTab=2&studyId='+studyId+'"><img src="../images/jpg/patient.gif" title="'+L_PatMgmt_EnrlPat+'" border=0 ></a>';
			}
			inHTML += '</td>'
				+ '<td width="25%" style="border: 0px">';
			
			var budgetIcon = oRecord.getData('budgetIcon');
			if (budgetIcon == 1){
				inHTML += '<a href="budgetbrowserpg.jsp?srcmenu=tdmenubaritem6&calledFrom=study&studyId='+studyId+'"><img src="../images/jpg/Budget.gif" title="'+L_Budgets+'" border=0 ></a>';
			}
			inHTML += '</td>'
				+ '<td width="25%" style="border: 0px">';

			var calendarIcon = oRecord.getData('calendarIcon');
			if (calendarIcon == 1){
				inHTML += '<a href="studyprotocols.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=7&studyId='+studyId+'"><img src="../images/jpg/Calendar.gif" title="'+L_Calendars+'" border=0 ></a>';
			}
			inHTML += '</td>'
				+ '</tr></table>';
			
			el.innerHTML = inHTML;
			break;
		case 'studyNumber':
			var studyNumber = oRecord.getData('studyNumber');
			el.innerHTML = (studyNumber.length > 15)? studyNumber.substring(0, 12) + "&nbsp;<img class='asIsImage' " 
				+ "onmouseover='return overlib(\""+studyNumber+"\",CAPTION,\""+L_Study_Number+"\");' "
				+ "onmouseout='return nd();' " 
				+ "src='./images/More.png' border='0' complete='complete'/>" : studyNumber;
			break;
		case 'studyTitle':
			var studyTitle = escapeSingleQuoteEtc(oData);
			var inHTML = '';

			if (oData.length > 40){
				inHTML = '<table border="0" cellpadding="0" cellspacing="0" style="border:none" width="100%"><tr>'
					+ '<td style="border-width: 0;">'+htmlEncode((oData.substring(0,40)))+'&nbsp;'
					+ '<IMG src ="./images/More.png" class="asIsImage" border="0" '
					+ 'onMouseOver="return overlib(\''+htmlEncode(studyTitle)+'\',CAPTION,\''+L_Study_Title+'\');" '
					+ 'onMouseOut="return nd();"/></td>'
					+ '</tr></table>';
			} else {
				inHTML = htmlEncode(studyTitle);
			}
			el.innerHTML = inHTML;
			break;
		case 'studyStatus':
			el.innerHTML = (oData);
			break;
		default:
			el.innerHTML = htmlEncode(escapeSingleQuoteEtc(oData));
			break;
		}
	}
};

// Implementation starts here

gadgetMyStudies.settings.updateSettings = function() {
	var j = {};
	j.id = 'gadgetMyStudies';
	j.displayOpt = -1;
	var displayOptionsList = document.getElementsByName("gadgetMyStudiesJB.displayOption");
	for (var i = 0; i < displayOptionsList.length; i++){
		if (displayOptionsList[i].checked){
			j.displayOpt = displayOptionsList[i].value;
		}
	}
	
	if (j.displayOpt == 0){
		j.studyIds = '[]';
	}
	if (j.displayOpt == 1){
		j.studyIds = '['+jQuery.trim($('gadgetMyStudies_studyIds').value)+']';
	}
	return j;
}
gadgetMyStudies.settings.submitSettings = function() {
	gadgetMyStudies.settings.updateSettings();
	var myGadgetId = 'gadgetMyStudies';
	setSettingsGlobal(myGadgetId);
	<%if(isIE){%>
		if (navigator.userAgent.indexOf("MSIE 8") > -1) {
			onMouseleaveToolTip(e);
		} else{ 
			onMouseleaveToolTip();
		}
	<%} else {%>
		onMouseleaveToolTip();
	<%}%>
	$j("#gadgetMyStudies_settings").slideUp(200).fadeOut(200);
	setTimeout(function() { 
		reloadGadgetScreenGlobal(myGadgetId);
		gadgetMyStudies.screenAction();
	}, 150);
	
	return true;
}
gadgetMyStudies.settings.clearSettings = function() {
	if ($('gadgetMyStudies_studyIds')) { $j('#gadgetMyStudies_studyIds').val(""); }
	if ($('gadgetMyStudies_reallyDeleteHTML')) { $('gadgetMyStudies_reallyDeleteHTML').innerHTML = ''; }
	gadgetMyStudies.clearErrMsg();
};
gadgetMyStudies.clearErrMsg = function() {
	if ($('gadgetMyStudies_errMsg_studyIds')) { $('gadgetMyStudies_errMsg_studyIds').innerHTML = ''; }
};

gadgetMyStudies.settings.getAllStudies = function(){
	jQuery.ajax({
		url:'gadgetMyStudies_getStudies',
		async: true,
		cache: false,
		success: function(resp) {
			resetSessionWarningOnTimer();
			gadgetMyStudies.settings.data.allArray = resp.array;
			gadgetMyStudies.settings.setAutoComplete();
		}
	});
};
gadgetMyStudies.settings.getSettings = function(o) {
	try {
		var j = jQuery.parseJSON(o.responseText);
		jQuery.ajax({
    		url:'gadgetMyStudies_getMyStudies',
    		async: true,
    		cache: false,
    		success: function(resp) {
    			resetSessionWarningOnTimer();
    			gadgetMyStudies.settings.data.displayOption = resp.displayOpt;
    	    	//j.gadgetMyStudies.displayOption = resp.displayOpt;
    	    	
    			var displayOption = resp.displayOpt;
    			var displayOptionsList = document.getElementsByName("gadgetMyStudiesJB.displayOption");
    	    	for (var i = 0 ; i < displayOptionsList.length; i++){
    	    		if (displayOption == displayOptionsList[i].value){
    	    			displayOptionsList[i].checked = true;
    	    		} else {
        	    		displayOptionsList[i].checked = false;
    	    		}
    	    	}
          		
    	    	$j('#gadgetMyStudies_studyIds').val("");
    	    	
    			if (displayOption <= 0){
    				gadgetMyStudies.settings.data.myArray = null;
    				
           			$('selectStudiesRow').style.display = "none";
           			$('selectedStudiesRow').style.display = "none";
    			} 
    			if (displayOption == 1){
    				gadgetMyStudies.settings.data.myArray = resp.array;
    				
	    			if (gadgetMyStudies.settings.data.myArray) {
	    				for(var iX=0; iX<gadgetMyStudies.settings.data.myArray.length; iX++) {
	    					if (iX > 5) { break; }
	    					var dataJSON = gadgetMyStudies.settings.data.myArray[iX];
	   						if ($('gadgetMyStudies_studyIds').value == '')
	   							$('gadgetMyStudies_studyIds').value += dataJSON.studyId;
	   						else
	   							$('gadgetMyStudies_studyIds').value += ','+ dataJSON.studyId;
	    				}
	    			}
	    			
	       			$('selectStudiesRow').style.display ="table-row";
	       			$('selectedStudiesRow').style.display ="table-row";
    			}
    			
    			//j.gadgetMyStudies.studyIds = $('gadgetMyStudies_studyIds').value;
    			gadgetMyStudies.settings.formatMyStudiesHTML();
    			gadgetMyStudies.settings.screenAction();
    		}
    	});

	} catch(e) {
		$j('#gadgetMyStudies_studyIds').val("");
		gadgetMyStudies.settings.formatMyStudiesHTML();
		gadgetMyStudies.settings.screenAction();
	};
	gadgetMyStudies.settings.getAllStudies();
};

gadgetMyStudies.settings.formatMyStudiesHTML = function () {
	var str1 = '<table width="100%" cellspacing="0">';

	if (gadgetMyStudies.settings.data.myArray){
		for(var iX=0; iX<gadgetMyStudies.settings.data.myArray.length; iX++) {
			if (iX > 5) { break; }
			var dataJSON = gadgetMyStudies.settings.data.myArray[iX];
			var studyNumber = dataJSON.studyNumber;
			studyNumber = (studyNumber.length > 15)? studyNumber.substring(0, 12) + "&nbsp;<img class='asIsImage' " 
				+ "onmouseover='return overlib(\""+studyNumber+"\",CAPTION,\""+L_Study_Number+"\");' "
				+ "onmouseout='return nd();' " 
				+ "src='./images/More.png' border='0' complete='complete'/>" : studyNumber;

			str1 += "<tr id='gadgetMyStudies_myStudy-row-"+iX+"'><td>";
			str1 += "<a target=\"_blank\" id=\"gadgetMyStudies_myStudy"+iX+"\"";
			str1 += " href=\""+dataJSON.studyId+"\"";
			str1 += " title=\""+dataJSON.studyId+"\"></a>";
			str1 += studyNumber;
			str1 += "</td><td nowrap='nowrap' align='right'>";
			str1 += "<a style=\"opacity:0; filter:alpha(opacity=0);\" href=\"javascript:gadgetMyStudies.settings.openDeleteDialogStudy("+(iX+1);
			str1 += ",'";
			str1 += dataJSON.studyId;
			str1 += "');\" id=\"gadgetMyStudies_delMyStudy"+iX+"\">";
			str1 += "<img id='gadgetMyStudies_myStudy-del-img-"+iX;
			str1 += "' class='headerImage' style='border:none; opacity:0; filter:alpha(opacity=0);' title='";
			str1 += "Delete selection #".replace("\'", "&#39;")+(iX+1);
			str1 += "' src='./images/delete.gif' border=0 />";
			str1 += "</a>";
			str1 += "</td></tr>";
		}
	} else {
		str1 += '<tr><td><i>'+'No studies selected'+'</i></td></tr>';
	}
	str1 += '</table>';
	$('gadgetMyStudies_myStudiesHTML').innerHTML = str1;
}
gadgetMyStudies.settings.setAutoComplete = function (){
	gadgetMyStudies.settings.allStudyData = [];
	if (gadgetMyStudies.settings.data.allArray){	
		for(var iY=0; iY<gadgetMyStudies.settings.data.allArray.length; iY++) {
			var dataJSON = gadgetMyStudies.settings.data.allArray[iY];
			var j = {};
			j["id"] = dataJSON.allStudyId; 
			j["value"] = dataJSON.allStudyNumber;
			gadgetMyStudies.settings.allStudyData[iY] = j;
		}
	}
	YUI().use('node', 'event-mouseenter', function(Y) {
		if ($('gadgetMyStudies_study')){
			Y.one('#gadgetMyStudies_study').on('focus', 
					openAuto('gadgetMyStudies_study','gadgetMyStudies_studyId',gadgetMyStudies.settings.allStudyData,'<%=LC.L_Study%>',
						'<%=VelosResourceBundle.getMessageString("M_EnterParamAndSelect",LC.L_Study_Number)%>'));
			Y.one('#gadgetMyStudies_study').on('click', 
				openAuto('gadgetMyStudies_study','gadgetMyStudies_studyId',gadgetMyStudies.settings.allStudyData,'<%=LC.L_Study%>',
						'<%=VelosResourceBundle.getMessageString("M_EnterParamAndSelect",LC.L_Study_Number)%>'));
		}
	});
}

gadgetMyStudies.settings.addStudyCriteria = function (){
	var existingStudies = $('gadgetMyStudies_studyIds').value;
	existingStudies = existingStudies.replace(/\s/g , "");
	var addedStudy = $('gadgetMyStudies_studyId').value;
	addedStudy = addedStudy.replace(/\s/g , "");

	if (addedStudy && addedStudy != ''){	
		if (existingStudies == '')
			$('gadgetMyStudies_studyIds').value += addedStudy;
		else{
			if (existingStudies == addedStudy || (existingStudies.indexOf(','+addedStudy) > 0))
				return;
			else
				$('gadgetMyStudies_studyIds').value += ','+ addedStudy;
		}
		$('gadgetMyStudies_studyId').value ='';

		//gadgetMyStudies.settings.updateSettings();
		gadgetMyStudies.settings.submitSettings();
		setTimeout(function() { gadgetMyStudies.settings.openSettings()}, 250);
	}
}

gadgetMyStudies.settings.changeStudyCriteria = function (){
	var displayOption;
	var displayOptionsList = document.getElementsByName("gadgetMyStudiesJB.displayOption");

	for (var i = 0; i < displayOptionsList.length; i++){
		if (displayOptionsList[i].checked){
			displayOption = displayOptionsList[i].value;
		}
	}

	if (displayOption == 0){
		$('selectStudiesRow').style.display = "none";
		$('selectedStudiesRow').style.display = "none";
	} 
	if (displayOption == 1){
		var numStudies = gadgetMyStudies.getMyStudies(); // This way settings are also loaded
		$('selectStudiesRow').style.display ="table-row";
		$('selectedStudiesRow').style.display ="table-row";
	}

	gadgetMyStudies.settings.submitSettings();
	if (displayOption == 0){ return; }
	setTimeout(function() { gadgetMyStudies.settings.openSettings()}, 250);
}

gadgetMyStudies.settings.screenAction = function() {
	YUI({
	    gallery: 'gallery-2012.06.20-20-07'
	}).use('node', 'event-mouseenter', 'gallery-placeholder', function(Y) {
		if (Y.one('#gadgetMyStudies_study')) {
			Y.one('#gadgetMyStudies_study').plug(Y.Plugin.Placeholder, {
				text : '<%=VelosResourceBundle.getMessageString("M_EnterParamAndSelect",LC.L_Study_Number)%>',
				hideOnFocus : true
			});
		}
		var onMouseoverDelMyStudy = function(e) {
			var trNum = -1;
			if (e.target.get('tagName') == 'TD') {
				var pos1 = 'gadgetMyStudies_myStudy-row-'.length;
				trNum = e.target.get('parentNode').get('id').substring(pos1,pos1+1);
			} else if (e.target.get('tagName') == 'A') {
				var pos1 = 'gadgetMyStudies_myStudy-row-'.length;
				trNum = e.target.get('parentNode').get('parentNode').get('id').substring(pos1,pos1+1);
			} else if (e.target.get('tagName') == 'IMG') {
				var pos1 = 'gadgetMyStudies_myStudy-del-img-'.length;
				trNum = e.target.get('id').substring(pos1,pos1+1);
			}
			if (trNum == -1 || !Y.one('#gadgetMyStudies_myStudy-del-img-'+trNum)) { return; }
			Y.one('#gadgetMyStudies_myStudy-del-img-'+trNum).setStyle('opacity', '1');
			Y.one('#gadgetMyStudies_myStudy-del-img-'+trNum).get('parentNode').setStyle('opacity', '1');
			$j('#gadgetMyStudies_myStudy-row-'+trNum).addClass('gdt-highlighted-row');
		}
		var onMouseoutDelMyStudy = function(e) {
			var trNum = -1;
			if (e.target.get('tagName') == 'TD') {
				var pos1 = 'gadgetMyStudies_myStudy-row-'.length;
				trNum = e.target.get('parentNode').get('id').substring(pos1,pos1+1);
			} else if (e.target.get('tagName') == 'A') {
				var pos1 = 'gadgetMyStudies_myStudy-row-'.length;
				trNum = e.target.get('parentNode').get('parentNode').get('id').substring(pos1,pos1+1);
			} else if (e.target.get('tagName') == 'IMG') {
				var pos1 = 'gadgetMyStudies_myStudy-del-img-'.length;
				trNum = e.target.get('id').substring(pos1,pos1+1);
			}
			if (trNum == -1 || !Y.one('#gadgetMyStudies_myStudy-del-img-'+trNum)) { return; }
			Y.one('#gadgetMyStudies_myStudy-del-img-'+trNum).setStyle('opacity', '0');
			Y.one('#gadgetMyStudies_myStudy-del-img-'+trNum).get('parentNode').setStyle('opacity', '0');
			$j('#gadgetMyStudies_myStudy-row-'+trNum).removeClass('gdt-highlighted-row');
		}
		for(var iX=0; iX<5; iX++) {
			if (!Y.one('#gadgetMyStudies_myStudy-row-'+iX)) { continue; }
			Y.one('#gadgetMyStudies_myStudy-row-'+iX).on('mousemove', onMouseoverDelMyStudy);
			Y.one('#gadgetMyStudies_myStudy-row-'+iX).on('mouseenter', onMouseoverDelMyStudy);
			Y.one('#gadgetMyStudies_myStudy-row-'+iX).on('mouseover', onMouseoverDelMyStudy);
			Y.one('#gadgetMyStudies_myStudy-row-'+iX).on('mouseleave', onMouseoutDelMyStudy);
			Y.one('#gadgetMyStudies_myStudy-row-'+iX).on('mouseout', onMouseoutDelMyStudy);
			if (!Y.one('#gadgetMyStudies_delMyStudy'+iX)) { continue; }
			Y.one('#gadgetMyStudies_delMyStudy'+iX).setStyle('opacity', '0');
			Y.one('#gadgetMyStudies_delMyStudy'+iX).setStyle('opacity', '0');
		}
		if ($('gadgetMyStudies_study')){
			Y.one('#gadgetMyStudies_study').on('focus', 
				openAuto('gadgetMyStudies_study','gadgetMyStudies_studyId',gadgetMyStudies.settings.allStudyData,'<%=LC.L_Study%>',
						'<%=VelosResourceBundle.getMessageString("M_EnterParamAndSelect",LC.L_Study_Number)%>'));
			Y.one('#gadgetMyStudies_study').on('click', 
				openAuto('gadgetMyStudies_study','gadgetMyStudies_studyId',gadgetMyStudies.settings.allStudyData,'<%=LC.L_Study%>',
						'<%=VelosResourceBundle.getMessageString("M_EnterParamAndSelect",LC.L_Study_Number)%>'));
		}
	});
}
gadgetMyStudies.settings.openDeleteDialogStudy = function(recNum,studyId) {
	$j("#gadgetMyStudies_reallyDeleteHTML").fadeIn(400);
	$("gadgetMyStudies_reallyDeleteHTML").innerHTML = '<table class="gdt-table" style="width:100%;">'
			+ '<tr><td>Really delete selection #'+recNum+'?'+'</td></tr>'
			+ '<tr><td align="center"><button type="button"' 
			+ ' id="gadgetMyStudies_settings_delStudy" class="gadgetMyStudies_settings_delStudy">Yes</button>&nbsp;&nbsp;'
			+ '<button type="button" id="gadgetMyStudies_settings_delStudyCancel"'
			+ ' class="gadgetMyStudies_settings_delStudyCancel">No</button>';
	$j("#gadgetMyStudies_settings_delStudy").button();
	$j("#gadgetMyStudies_settings_delStudy").click(function() {
		$j("#gadgetMyStudies_reallyDeleteHTML").innerHTML = '';
		gadgetMyStudies.settings.deleteStudy(studyId);
	});
	$j("#gadgetMyStudies_settings_delStudyCancel").button();
	$j("#gadgetMyStudies_settings_delStudyCancel").click(function() {
		$j("#gadgetMyStudies_reallyDeleteHTML").fadeOut(200);
	});
}
gadgetMyStudies.settings.deleteStudy = function(studyId) {
	var thisJ = {}
	$j('#gadgetMyStudies_studyIds').val("");
	try {
		for(var iX=0; iX<gadgetMyStudies.settings.data.myArray.length; iX++) {
			if (iX > 5) { break; }
			var dataJSON = gadgetMyStudies.settings.data.myArray[iX];
			if (dataJSON.studyId != studyId){
				if ($('gadgetMyStudies_studyIds').value == '')
					$('gadgetMyStudies_studyIds').value += dataJSON.studyId;
				else
					$('gadgetMyStudies_studyIds').value += ','+ dataJSON.studyId;
			}
		}
		//gadgetMyStudies.settings.updateSettings();
		gadgetMyStudies.settings.submitSettings();
		setTimeout(function() { gadgetMyStudies.settings.openSettings()}, 250);
	} catch(e) {}
}
gadgetMyStudies.settings.openSettings = function() {
	$j("#gadgetMyStudies_settings").slideDown(200).fadeIn(200);
	YUI({
	    gallery: 'gallery-2012.06.20-20-07'
	}).use('gallery-placeholder', 'node', 'io', function(Y) {
		//Y.one('#gadgetMyStudies_saveButton').on('click', gadgetMyStudies.settings.submitSettings);
    	Y.io('gadgetCookie.jsp', {
        	method: 'POST',
        	data: 'type=typeGetSettings',
        	sync: true,
        	on: {
            	success: function (tranId, o) {
            		resetSessionWarningOnTimer();
            		gadgetMyStudies.settings.getSettings(o);

            		var displayOptionsList = document.getElementsByName("gadgetMyStudiesJB.displayOption");
                	for (var i = 0; i < displayOptionsList.length; i++){
                		Y.one('#'+displayOptionsList[i].id).on('click', gadgetMyStudies.settings.changeStudyCriteria);
                	}
                	
                },
                failure: function (tranId, o) {
                }
            }
        });
   		
    	if ($('gadgetMyStudies_addStudyButton')){
    		Y.one('#gadgetMyStudies_addStudyButton').on('doubleclick', function(){return false;});
    		Y.one('#gadgetMyStudies_addStudyButton').on('click', gadgetMyStudies.settings.addStudyCriteria);
    	}
	});
}
gadgetMyStudies.settings.cancelSettings = function() {
	gadgetMyStudies.settings.clearSettings();
	$j("#gadgetMyStudies_settings").slideUp(200).fadeOut(200);
}

gadgetMyStudies.screenAction = function() {
	YUI({
	    gallery: 'gallery-2012.06.20-20-07'
	}).use('gallery-placeholder', 'node', function(Y) {
		var addToolTipToButton = function(buttonId) {
			Y.one(buttonId).on('mousemove', onMousemoveToolTip);
			Y.one(buttonId).on('mouseleave', onMouseleaveToolTip);
			Y.one(buttonId).on('mouseout', onMouseleaveToolTip);
		};

		if ($('gadgetMyStudies_addStudyButton')){
			addToolTipToButton('#gadgetMyStudies_addStudyButton');
		}
		Y.one('#gadgetMyStudies_studySearch').plug(Y.Plugin.Placeholder, {
			text : "<%=MC.M_StdTitle_Kword%>",
			hideOnFocus : true
		});
		
		addToolTipToButton('#gadgetMyStudies_studySearchButton');
		addToolTipToButton('#gadgetMyStudies_studyAdvancedSearchButton');
		
		$j("#gadgetMyStudies_studySearchButton").button();
		$j("#gadgetMyStudies_studyAdvancedSearchButton").button();

		$j("#gadgetMyStudies_studySearchButton").click(function() {
			gadgetMyStudies.submitStudy();
		});
		$j("#gadgetMyStudies_studyAdvancedSearchButton").click(function() {
			gadgetMyStudies.submitStudyAdvanced();
		});
	});

	if ($('gadgetMyStudies_addStudyButton')){
		$j("#gadgetMyStudies_addStudyButton").button();
	}
	//$j("#gadgetMyStudies_saveButton").button();
	$j("#gadgetMyStudies_cancelButton").button();
	$j("#gadgetMyStudies_cancelButton").click(function() {
		gadgetMyStudies.settings.cancelSettings();
	});
	
	gadgetMyStudies.reloadGrid();
	
	var displayOption = gadgetMyStudies.settings.data.displayOption;    	
	if (displayOption == 0){
		$('selectStudiesRow').style.display = "none";
		$('selectedStudiesRow').style.display = "none";
	} else {
		$('selectStudiesRow').style.display ="table-row";
		$('selectedStudiesRow').style.display ="table-row";
	}
	
	gadgetMyStudies.settings.formatMyStudiesHTML();
	gadgetMyStudies.settings.screenAction();
};

gadgetMyStudies.getMyStudies = function() {
	var numStudies = 0;
	try {
		jQuery.ajax({
    		url:'gadgetMyStudies_getMyStudies',
    		async: false,
    		cache: false,
    		success: function(resp) {
    			resetSessionWarningOnTimer();
    			if (resp.array)
    				numStudies = resp.array.length;

    			gadgetMyStudies.settings.data.displayOption = resp.displayOpt;
    			$j('#gadgetMyStudies_studyIds').val("");

    			if (resp.displayOpt == 0){
    				gadgetMyStudies.settings.data.myArray = null;
    			} 
    			if (resp.displayOpt == 1){
    				gadgetMyStudies.settings.data.myArray = resp.array;
    				if (gadgetMyStudies.settings.data.myArray) {
	    				for(var iX=0; iX<gadgetMyStudies.settings.data.myArray.length; iX++) {
	    					if (iX > 5) { break; }
	    					var dataJSON = gadgetMyStudies.settings.data.myArray[iX];
	   						if ($('gadgetMyStudies_studyIds').value == '')
	   							$('gadgetMyStudies_studyIds').value += dataJSON.studyId;
	   						else
	   							$('gadgetMyStudies_studyIds').value += ','+ dataJSON.studyId;
	    				}
	    			}
    			}
				gadgetMyStudies.settings.formatMyStudiesHTML();
				gadgetMyStudies.settings.screenAction();
    		}
    	});
	} catch(e) { alert(e.message); }
	return numStudies;
};

gadgetMyStudies.reloadGrid = function() {
	var numStudies = gadgetMyStudies.getMyStudies(); // This way settings are also loaded
	var displayOption = gadgetMyStudies.settings.data.displayOption;
	if (displayOption < 0 || (displayOption == 1 && numStudies < 1)) {
		$('gadgetMyStudies_myStudiesGrid').innerHTML = '<p><i>'+'<%=MC.M_PlsUseSet_ToAddStudies%>'+'</i></p>';
		return;
	}
	if (displayOption > -1){
		try {
			$("gadgetMyStudies_myStudiesGrid").innerHTML = '<%=LC.L_Loading%>...'
				+'<img class="asIsImage" src="../images/jpg/loading_pg.gif" />';
			jQuery.ajax({
				url:'gadgetMyStudies_getMyStudiesData',
				async: true,
				cache: false,
				success: function(resp) {
					resetSessionWarningOnTimer();
					try {
						gadgetMyStudies.data.gridData = {
							colArray: resp.colArray,
							dataArray: resp.dataArray
						};
						var loadDataTableForBilling = function() {
							// Sepcify column settings
							var colArray = YAHOO.lang.JSON.parse(resp.colArray);
							var configureColArray = function () {
								var tmpColArray = [];
								for (var iX=0; iX<colArray.length; iX++) {
									if (colArray[iX].key == 'studyId') { continue; }
									if (colArray[iX].key == 'studyIcon'
										|| colArray[iX].key == 'patientIcon'
										|| colArray[iX].key == 'budgetIcon'
										|| colArray[iX].key == 'calendarIcon')
									{ continue; }
									tmpColArray.push(colArray[iX]);
								}
								colArray = tmpColArray;
								
								for (var iX=0; iX<colArray.length; iX++) {
									colArray[iX].resizeable = true;
									if (colArray[iX].key != 'quickAccess')
										colArray[iX].sortable = true;
									else 
										colArray[iX].width = 110;
									colArray[iX].formatter = gadgetMyStudies.formatCells;
								}
							}(); // Execute configureColArray()
							
							// Get data
							var dataArray = YAHOO.lang.JSON.parse(resp.dataArray);

							var myDataSource = new YAHOO.util.DataSource(dataArray);		
							myDataSource.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
							myDataSource.responseSchema = {
								fields: resp.fieldsArray
							};
							
							// Create datatable
							var myDataTable = new YAHOO.widget.DataTable(
								"gadgetMyStudies_myStudiesGrid", 
								colArray, myDataSource, 
								{ 
									width: (document.getElementById("list1").clientWidth-17) +"px",
									scrollable: true
									//sortedBy:{key:'studyNumber', dir:'asc'}
								});

							var fnMouseOver = function(oArgs) {
							    var target = oArgs.target;
								var column = this.getColumn(target);
								var oRecord = this.getRecord(target);

								if(column.key!='studyNumber' && column.key!='studyStatus'){
									return nd();
								}
								if(column.key=='studyNumber'){
									var studyId = oRecord.getData('fk_study');
									return fnGetStudyMouseOver(studyId);
								}
							};
							var fnMouseOut = function(oArgs) {
								return nd();
							};
							myDataTable.subscribe("mousemoveEvent", fnMouseOver);
							myDataTable.subscribe("cellMouseoverEvent", fnMouseOver);
							myDataTable.subscribe("cellMouseoutEvent", fnMouseOut);
						}(); // Execute loadDataTableForBilling()
					} catch(e) { alert(e.message); }
				} // End of success
			}); // End of jQuery.ajax
		} catch(e) {
			alert(getLocalizedMessageString("M_CommErr_CnctVelosSupp",{}));
		}
	}
};

gadgetMyStudies.beforeSubmitStudy = function(formobj) {
	try {
		if (!$j("#gadgetMyStudies_studySearch").hasClass('yui3-placeholder-text')) {
			formobj.searchCriteria.value = formobj.gadgetMyStudies_studySearch.value;
		}
	} catch(e) {
		// console.log('In gadgetMyStudies.beforeSubmitStudy:'+e);
	}
}
gadgetMyStudies.submitStudy = function() {
	try {
		if (!$j("#gadgetMyStudies_studySearch").hasClass('yui3-placeholder-text')) {
			var mySearchCriteria = $j("#gadgetMyStudies_formStudy").children('#searchCriteria');
			if (mySearchCriteria && mySearchCriteria[0]) {
				mySearchCriteria[0].value = $j("#gadgetMyStudies_studySearch").val();
			}
		}
		$("gadgetMyStudies_formStudy").action = "studybrowserpg.jsp";
		$("gadgetMyStudies_formStudy").method = "POST";
		$("gadgetMyStudies_formStudy").submit();
	} catch(e) {
		// console.log('In gadgetMyStudies.submitStudy:'+e);
	}
}
gadgetMyStudies.submitStudyAdvanced = function() {
	try {
		if (!$j("#gadgetMyStudies_studySearch").hasClass('yui3-placeholder-text')) {
			var mySearchCriteria = $j("#gadgetMyStudies_formStudy").children('#searchCriteria');
			if (mySearchCriteria && mySearchCriteria[0]) {
				mySearchCriteria[0].value = $j("#gadgetMyStudies_studySearch").val();
			}
		}
		$("gadgetMyStudies_formStudy").action = "advStudysearchpg.jsp";
		$("gadgetMyStudies_formStudy").method = "POST";
		$("gadgetMyStudies_formStudy").submit();
	} catch(e) {
		// console.log('In gadgetMyStudies.submitStudyAdvanced:'+e);
	}
}

clearerRegistry['gadgetMyStudies'] = gadgetMyStudies.settings.clearSettings;
//validatorRegistry['gadgetMyStudies'] = gadgetMyStudies.validate;
settingsUpdatorRegistry['gadgetMyStudies'] = gadgetMyStudies.settings.updateSettings;
settingsGetterRegistry['gadgetMyStudies'] = gadgetMyStudies.settings.getSettings;
settingsOpenerRegistry['gadgetMyStudies'] = gadgetMyStudies.settings.openSettings;
screenActionRegistry['gadgetMyStudies'] = gadgetMyStudies.screenAction;
</script>

<%-- Start of Export modal dialog --%>
<div id="gadgetMyStudiesExportDiv" title="" class="" style="display:none; top: 0px;">
	<div id='gadgetMyStudiesExport_EmbedDataHere'></div>
</div>
<%-- End of Export modal dialog --%>

<script>
	//defaultHeight used for the modal dialog
	var defaultHeight = navigator.userAgent.indexOf("MSIE") != -1 ? 550 : 650;
	var calcHeight = defaultHeight;
	$j("#gadgetMyStudiesExportDiv").dialog( { height: calcHeight, width: 850, modal: true, 
		resizable: true, autoOpen: false, closeText: '',
		//dragStart: function(event, ui) { calcHeight = $j("#gadgetMyStudiesExportDiv").dialog( "option", "height" ); },
		//dragStop: function(event, ui) { $j("#gadgetMyStudiesExportDiv").dialog( "option", "height", calcHeight ); },
		beforeClose: function(event, ui) {},
		buttons: {
	      "<%=LC.L_Close%>": function() {
	      	$j("#gadgetMyStudiesExportDiv").dialog("close");
	      }
	  	}
	  } 
	);
</script>