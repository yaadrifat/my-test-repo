<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<jsp:include page="popupJS.js" flush="true"/>
<script>

	function confirmCheck(formobj) {
			formobj.action="updatepatstatus.jsp";
			formobj.flag.value = "1";
    		formobj.submit();
	}

</script>

</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>

<jsp:useBean id="patB" scope="request" class="com.velos.eres.web.patStudyStat.PatStudyStatJB"/>
<jsp:useBean id="prevPatStatB" scope="request" class="com.velos.eres.web.patStudyStat.PatStudyStatJB"/>
<jsp:useBean id="personB" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="patBOld" scope="request" class="com.velos.eres.web.patStudyStat.PatStudyStatJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="CommonB" scope="page" class="com.velos.eres.web.common.CommonJB" />
<jsp:useBean id="StudyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="SiteB" scope="page" class="com.velos.eres.web.site.SiteJB" />
<jsp:useBean id="ssJB" scope="page" class="com.velos.eres.web.studySite.StudySiteJB" />
<%@page import="com.velos.eres.audit.web.AuditRowEresJB,com.velos.epat.audit.web.AuditRowEpatJB"%>
<%@ page language = "java" import = "com.velos.eres.business.patProt.impl.PatProtBean,com.velos.eres.business.common.*,java.util.*, com.velos.eres.service.util.*"%>
<%@page import="com.velos.eres.web.intercept.InterceptorJB" %>
<BR>
<DIV class="popDefault" id="div1">
<%


	HttpSession tSession = request.getSession(true);
	String notes = "";
	String stDate = "";
	String curStatusId = "";
	String prevStatusId = "";
	String prevStat = "";
	String studyId = "";
	String status = "";
	String patientId = "";
	String statReason = "";
	String siteId = "";
	String selStatSubType = "";
	String othDthCause = request.getParameter("othDthCause");
	String othDthCauseRel = request.getParameter("othDthCauseRel");

	String presurvival = request.getParameter("presurvival");
	String currentStat=request.getParameter("currStat");//KM
	String currentStatId=request.getParameter("currentStatId");//KM

	int ret = 0;
	String msg = "";
	String src = "";
	String from="" ;
	String selectedTab="" ;

	String screenNumber = "";
	String screenedBy = "";
	String screeningOutcome = "";
	String infConsentVerNumber = "";
	String followUpDate = "";
    PatProtBean psk = new PatProtBean();

    String patAssignedTo = "";
	String patPhysician = "";
	String patTreatLoc = "";
	String patOrg = "";
	String dGrphReportable = "";
	String patDiseaseCode = ""; // CTRP-21708-05012013 @Ankit
	String patOtherDisCode = "";
    String patMoreDisCode1 = "";
	String patMoreDisCode2 = "";
	String anatomicSite = "";
	String evalFlag = "";
	String evalStat = "";
	String inevalStat = "";
	String survival = "";
	String dthCause = "";
	String dDthRelStd = "";
	String patStudyId = "";
	String patRandomizationNumber = "";
	String patEnrollmentDate = "";
	String patEnrolledBy = "";
	String  enrollingSite = "";

	String deathDate = "";
	SettingsDao settingsdao = new SettingsDao();
	ArrayList arSettingsValue = new ArrayList();
	String formatString = "";
	String idNumberFormat = "";
	String idNumberFormat_val = "";

	String firstKeyword = "";
	String secondKeyword = "",forwardPage="";
	int curEnrCount = 0;
	String organization = "";
	String patientDefSite = "";


    from=request.getParameter("from");
	 patientDefSite = request.getParameter("patientDefSite");

	if (from==null) from ="" ;
    if (from.equals("studypatients")) {
    	selectedTab=request.getParameter("selectedTab");
    }

    src = request.getParameter("srcmenu");
    int fdaStudy = ("1".equals(request.getParameter("fdaStudy")))? 1:0;
    String remarks = request.getParameter("remarks");
	int userId = StringUtil.stringToNum((String)tSession.getAttribute("userId"));

	if (sessionmaint.isValidSession(tSession))
	{
%>
	<jsp:include page="sessionlogging.jsp" flush="true"/>

<%
	     String oldESign = (String) tSession.getAttribute("eSign");
		 String eSign = request.getParameter("eSign");

		if(!(oldESign.equals(eSign))) {

	%>
	   <jsp:include page="incorrectesign.jsp" flush="true"/>
	<%
		} else
		{

	     	String ipAdd = (String) tSession.getAttribute("ipAdd");
			int personPK = StringUtil.stringToNum(request.getParameter("pkey"));
	     	String usr = null;
	     	String patStudyIdAutoStr  = "";

	     	String oldPatStudyId = request.getParameter("oldPatStudyId");

			usr = (String) tSession.getAttribute("userId");
			studyId = request.getParameter("studyId");
			siteId = request.getParameter("siteId");
			notes = request.getParameter("Notes");
			stDate = request.getParameter("StatusDate");
			prevStat = request.getParameter("prevStat");

			status = request.getParameter("patstatus");

			patientId = request.getParameter("patientId");
			statReason = request.getParameter("patstatreason");

			if(statReason.equals("null")) statReason ="";

			infConsentVerNumber = request.getParameter("infConsentVerNumber");

			followUpDate = request.getParameter("followUpDate");

			screenNumber  = request.getParameter("patScreenNumber");
			screenedBy  = request.getParameter("patScreenedBy");
			screeningOutcome  = request.getParameter("patScreenOutcome");
			selStatSubType = request.getParameter("selStatSubType");

			patPhysician = request.getParameter("patPhysician");
			patAssignedTo = request.getParameter("patAssignTo");
			patTreatLoc = request.getParameter("patTreatLoc");
			patOrg = request.getParameter("patorg");//JM
			patDiseaseCode = request.getParameter("patDiseaseCode");
			dGrphReportable = request.getParameter("dGrphReportable");//JM
			patMoreDisCode1 = request.getParameter("patMoreDisCode1");
			patMoreDisCode2 = request.getParameter("patMoreDisCode2");
			patOtherDisCode = request.getParameter("patOtherDisCode");
			anatomicSite = request.getParameter("anaSiteid");
			anatomicSite = anatomicSite.replace(';',',');
			//KM-3715
			if(patOrg == null || patOrg.equals("null"))
				patOrg ="";
			if(patDiseaseCode == null || patDiseaseCode.equals("null"))
				patDiseaseCode ="";
			if(dGrphReportable == null || dGrphReportable.equals("null"))
				dGrphReportable ="0";
			if(dGrphReportable.equals("on"))
				dGrphReportable ="1";
				
			if(patOtherDisCode == null || patOtherDisCode.equals("null"))
				patOtherDisCode ="";
			if(patMoreDisCode1 == null || patMoreDisCode1.equals("null"))
				patMoreDisCode1 ="";
			if(patMoreDisCode2 == null || patMoreDisCode2.equals("null"))
				patMoreDisCode2 ="";
			if(anatomicSite == null || anatomicSite.equals("null"))
				anatomicSite ="";

			evalFlag = request.getParameter("evalFlag");
			evalStat = request.getParameter("evalStat");
			inevalStat = request.getParameter("inevalStat");
			survival = request.getParameter("survival");
			dthCause = request.getParameter("dthCause");
			dDthRelStd = request.getParameter("dDthRelStd");

			patStudyId = request.getParameter("patStudyId");
            //KN
			if (!StringUtil.isEmpty(patStudyId))
				patStudyId = patStudyId.trim();

			if (patStudyId == null) //KM
				 patStudyId = "";

			patStudyIdAutoStr = request.getParameter("patStudyIdAuto");

			patRandomizationNumber  = request.getParameter("patRandomizationNumber");
			patEnrollmentDate   = request.getParameter("StatusDate");
			patEnrolledBy   = request.getParameter("patEnrollBy");
			enrollingSite   = request.getParameter("enrollingSite");


			deathDate = request.getParameter("deathDate");
			String changeStatusMode = request.getParameter("changeStatusMode");
			//Added by Manimaran to check Patient study ID being entered is a duplicate
			String flag = request.getParameter("flag");
			flag = (flag == null)?"" :flag;
			%>
			<Form id ="patstatus" name ="patstatus" method ="post"  action = "updatepatstatus.jsp" onsubmit ="">

	     	<input type="hidden" name="pkey" size = 20  value = <%=personPK%> >
			<input type="hidden" name="studyId" size = 20  value = <%=studyId%> >
			<input type="hidden" name="siteId" size = 20  value = <%=siteId%> >
			<input type="hidden" name="Notes" size = 20  value = "<%=notes%>" >
			<input type="hidden" name="StatusDate" size = 20  value = <%=stDate%> >
			<input type="hidden" name="prevStat" size = 20  value = <%=prevStat%> >
			<input type="hidden" name="patstatus" size = 20  value = <%=status%> >
			<input type="hidden" name="patientId" size = 20  value = <%=patientId%> >
			<input type="hidden" name="patstatreason" size = 20  value = <%=statReason%> >
			<input type="hidden" name="infConsentVerNumber" size = 20  value = <%=infConsentVerNumber%> >
			<input type="hidden" name="followUpDate" size = 20  value = <%=followUpDate%> >
			<input type="hidden" name="patScreenNumber" size = 20  value = <%=screenNumber%> >
			<input type="hidden" name="patScreenedBy" size = 20  value = <%=screenedBy%> >
			<input type="hidden" name="patScreenOutcome" size = 20  value = <%=screeningOutcome%> >
			<input type="hidden" name="selStatSubType" size = 20  value = <%=selStatSubType%> >


			<input type="hidden" name="patPhysician" size = 20  value = <%=patPhysician%> >
			<input type="hidden" name="patAssignTo" size = 20  value = <%=patAssignedTo%> >
			<input type="hidden" name="patTreatLoc" size = 20  value = <%=patTreatLoc%> >
			<input type="hidden" name="patorg" size = 20  value = <%=patOrg%> >
			<input type="hidden" name="patDiseaseCode" size = 20  value = <%=patDiseaseCode%> >
			<input type="hidden" name="dGrphReportable" size = 20  value = <%=dGrphReportable%> >
			
			<input type="hidden" name="patMoreDisCode1" size = 50  value = <%=patMoreDisCode1%> >
			<input type="hidden" name="patMoreDisCode2" size = 50  value = <%=patMoreDisCode2%> >
			<input type="hidden" name="patOtherDisCode" size = 50  value = <%=patOtherDisCode%> >
			<input type="hidden" name="anaSiteid" size = 50  value = <%=anatomicSite%> >
			<input type="hidden" name="evalFlag" size = 20  value = <%=evalFlag%> >
			<input type="hidden" name="evalStat" size = 20  value = <%=evalStat%> >
			<input type="hidden" name="inevalStat" size = 20  value = <%=inevalStat%> >
			<input type="hidden" name="survival" size = 20  value = <%=survival%> >
			<input type="hidden" name="dthCause" size = 20  value = <%=dthCause%> >
			<input type="hidden" name="dDthRelStd" size = 20  value = <%=dDthRelStd%> >
			<input type="hidden" name="patStudyId" size = 20  value = "<%=patStudyId%>"	 >
			<input type="hidden" name="patStudyIdAuto" size = 20  value = <%=patStudyIdAutoStr%> >
			<input type="hidden" name="patRandomizationNumber" size = 20  value = <%=patRandomizationNumber%> >
			<input type="hidden" name="StatusDate" size = 20  value = <%=patEnrollmentDate%> >
			<input type="hidden" name="patEnrollBy" size = 20  value = <%=patEnrolledBy%> >
			<input type="hidden" name="enrollingSite" size = 20  value = <%=enrollingSite%> >
			<input type="hidden" name="deathDate" size = 20  value = <%=deathDate%> >
			<input type="hidden" name="changeStatusMode" size = 20  value = <%=changeStatusMode%> >


			<input type="hidden" name="eSign" size = 20  value = <%=eSign%> >
			<input type="hidden" name="othDthCause" size = 20  value = <%=othDthCause%> >
			<input type="hidden" name="othDthCauseRel" size = 20  value = <%=othDthCauseRel%> >
			<input type="hidden" name="presurvival" size = 20  value = <%=presurvival%> >
			<input type="hidden" name="currStat" size = 20  value = <%=currentStat%> >
			<input type="hidden" name="currentStatId" size = 20  value = <%=currentStatId%> >
			<input type="hidden" name="patientDefSite" size = 20  value = <%=patientDefSite%> >
			<input type="hidden" name="from" size = 20  value = <%=from%> >
			<input type="hidden" name="selectedTab" size = 20  value = <%=selectedTab%> >
			<input type="hidden" name="srcmenu" size = 20  value = <%=src%> >
			<input type="hidden" name="flag" size =20 value =<%=flag%> >
			<input type="hidden" name="oldPatStudyId" size =20 value ="<%=oldPatStudyId%>" >


			<%
		int count = 0;
		if(!(oldPatStudyId.toLowerCase()).equals(patStudyId.toLowerCase())) {
		     count = patB.getCountPatStudyId(StringUtil.stringToNum(studyId),patStudyId.trim());
		}
		if (count > 0 && flag.equals("")) {
			%>
			<br><br><br><br><br><br><br>
				<TABLE width="500" border = "0">
			 	<tr>
					<td align="center">
			     	<P class = "successfulmsg">
			     		 <%=MC.M_PatStdId_Dupli %><%-- The <%=LC.Pat_Patient%> <%=LC.Std_Study%> Id you have entered is duplicate. Do you want to continue ?*****--%>
					<br><br>
				  <A onclick="return confirmCheck(document.patstatus);"><input type="button" name="ok" value="<%=LC.L_Yes%><%--Yes--%> " border="0" align="left"/></A>
		         <input type="button" name="cancel" value="<%=LC.L_No%><%--No--%>" onClick="window.history.back()">
				 	</P>
				</td>
			   </tr>
			 </table>

		<%
		return;
		}


		   if (StringUtil.isEmpty(patStudyIdAutoStr))
		   {
		   	patStudyIdAutoStr = "";
		   }

			//generate the patStudyID
				if (patStudyIdAutoStr.equals("1"))
			 	{ //generate patient study id
			 		personB.setPersonPKId(personPK);
     				personB.getPersonDetails();

     				organization = personB.getPersonLocation();

			 			settingsdao = CommonB.retrieveSettings(StringUtil.stringToNum(studyId),3,"PATSTUDY_ID_FORMAT")	;
							if (settingsdao != null)
							{
								arSettingsValue = settingsdao.getSettingValue();
								if (arSettingsValue != null && arSettingsValue.size() > 0)
								{
									formatString = (String) arSettingsValue.get(0);
									if (StringUtil.isEmpty(formatString))
									{
										idNumberFormat = "";
										idNumberFormat_val = "";
									}
									else
									{
										String[] st  = StringUtil.chopChop(formatString,'-');

											if (st.length >= 3)
											{
												firstKeyword = st[0];
												secondKeyword = st[1];
												String[] numformat  = StringUtil.chopChop(st[2],'|');
												if (numformat.length == 2){
													idNumberFormat_val = numformat[0];
													idNumberFormat = numformat[1];
												} else {
													idNumberFormat_val = "";
													idNumberFormat = numformat[0];
												}
											}

											if (StringUtil.isEmpty(idNumberFormat))
											{
												idNumberFormat = "";
												idNumberFormat_val = "";
											}

										if (firstKeyword.equals("STUDY") || secondKeyword.equals("STUDY"))
										{
											StudyB.setId(StringUtil.stringToNum(studyId));
											StudyB.getStudyDetails();
										}
										if (firstKeyword.equals("SITEID") || secondKeyword.equals("SITEID"))
										{
											SiteB.setSiteId(StringUtil.stringToNum(organization));
											SiteB.getSiteDetails();
										}

										if (firstKeyword.equals("STUDY"))
										{firstKeyword = StudyB.getStudyNumber();}
										 else if (firstKeyword.equals("SITEID"))
										{ firstKeyword = SiteB.getSiteIdentifier();}
										 else {	firstKeyword =""; }

										if (StringUtil.isEmpty(firstKeyword ))
										{ firstKeyword = "";}

										if (secondKeyword.equals("STUDY"))
										{ secondKeyword = StudyB.getStudyNumber(); }
										else if (secondKeyword.equals("SITEID"))
										{ secondKeyword = SiteB.getSiteIdentifier();
										} else
										{
										secondKeyword ="";
										}
										if (StringUtil.isEmpty(secondKeyword ))
										{
											secondKeyword = "";
										}
										//include hyphens
										if (! StringUtil.isEmpty(firstKeyword))
										{
											firstKeyword  = firstKeyword + "-";
										}
										if (! StringUtil.isEmpty(secondKeyword))
										{
											secondKeyword  = secondKeyword + "-";
										}
										if (! StringUtil.isEmpty(idNumberFormat_val))
										{
											idNumberFormat_val  = idNumberFormat_val + "-";
										}


									}

								}
							} // end of if for settings dao
					//get the next enrollment number for the study -site of the patient
					ssJB.setStudyId(studyId);
					ssJB.setSiteId(organization);
					ssJB.findBeanByStudySite();

					curEnrCount = StringUtil.stringToNum( ssJB.getCurrentSitePatientCount() );
					curEnrCount = curEnrCount + 1;

					//generate patStudyId

					patStudyId = StringUtil.getFormattedString((firstKeyword +secondKeyword+idNumberFormat_val),"",idNumberFormat,curEnrCount);

					//cut the string to 20
					if 	(patStudyId .length() > 20)
					{
						patStudyId = patStudyId.substring(( patStudyId.length() - 20) );
					}

			 	} //




			if(changeStatusMode.equals("no"))
			{
				prevStatusId = ""+prevStat;
				patB.setId(StringUtil.stringToNum(prevStat));
				patB.getPatStudyStatDetails();
				psk = patB.getPatProtObj();
				psk.setModifiedBy(usr);

			}else
			{
				if (! StringUtil.isEmpty(prevStat))
				{

				patBOld.setId(StringUtil.stringToNum(prevStat));
				patBOld.getPatStudyStatDetails();
				psk = patBOld.getPatProtObj();
				}

				psk.setCreator(usr);

			}

				patB.setNotes(notes);
				patB.setPatientId(patientId);
				patB.setStartDate(stDate);

				if(changeStatusMode.equals("yes"))
				    patB.setPatStudyStat(status);

				patB.setStudyId(studyId);
		   		if (StringUtil.isEmpty(statReason))
     				statReason = null;
     			patB.setPatStudyStatReason(statReason);
				patB.setIpAdd(ipAdd);


				//set standard enrollment details on psk Object

				psk.setTreatmentLoc(patTreatLoc);
				psk.setIpAdd(ipAdd);
				psk.setpatOrg(patOrg);//JM: 07Aug06
				psk.setdGrphReportable(dGrphReportable);
				psk.setPatDiseaseCode(patDiseaseCode);
				psk.setPatMoreDisCode1(patMoreDisCode1);
				psk.setPatMoreDisCode2(patMoreDisCode2);
				psk.setPatOtherDisCode(patOtherDisCode);
				psk.setAnatomicSite(anatomicSite);
				psk.setPatProtPhysician(patPhysician);
				psk.setAssignTo(patAssignedTo);
				psk.setPatProtStudyId(studyId);
				psk.setPatProtPersonId(patientId);
				psk.setPatStudyId(patStudyId);
				psk.setPtstEvalFlag(evalFlag);
				psk.setPtstEval(evalStat);
				psk.setPtstInEval(inevalStat);
				psk.setPtstSurvival(survival);

				psk.setPtstDthStdRel(dDthRelStd);
				if(othDthCauseRel!=null )
				psk.setPtstDthStdRelOther(othDthCauseRel);
				else
				psk.setPtstDthStdRelOther("");
				psk.setPtstDeathDate(deathDate);

				//set to default site
				if (StringUtil.isEmpty(psk.getEnrollingSite()) )
				{
					psk.setEnrollingSite(patientDefSite);
				}

				psk.setEnrollingSite(enrollingSite);


				if (StringUtil.isEmpty(psk.getPatProtStat()) )
				{
					psk.setPatProtStat("1");
				}


				//set enrolled status specific enrollment details
				if (selStatSubType.equals("enrolled"))
				{
					psk.setPatProtRandomNumber(patRandomizationNumber);
					psk.updatePatProtEnrolDt(patEnrollmentDate);
					psk.setPatProtUserId(patEnrolledBy);


				}

				if (selStatSubType.equals("infConsent"))
				{
					//psk.setPtstConsentVer(infConsentVerNumber);
					//JM:
					patB.setPtstConsentVer(infConsentVerNumber);

				}
				if (selStatSubType.equals("followup"))
				{

					//psk.setPtstNextFlwup(followUpDate);
					//JM:
					patB.setPtstNextFlwup(followUpDate);
				}


				if (selStatSubType.equals("screening"))
				{

					patB.setScreenNumber(screenNumber);
					patB.setScreenedBy(screenedBy);
					patB.setScreeningOutcome(screeningOutcome);
				}

				//set Enrollment details object
				patB.setPatProtObj(psk);

				if (changeStatusMode.equals("no"))
			   	{
			   		patB.setModifiedBy(usr);
					patB.setCurrentStat(currentStat);//KM
					ret = patB.updatePatStudyStat();

					//Added by Manimaran for September Enhancement S8.
					if (currentStat.equals("1") && (StringUtil.stringToNum(prevStat)!=StringUtil.stringToNum(currentStatId))) {
						prevPatStatB.setId(StringUtil.stringToNum(currentStatId));
						prevPatStatB.getPatStudyStatDetails();
						prevPatStatB.setCurrentStat("0");
						prevPatStatB.setModifiedBy(usr);//JM: 09Jul2008, #3529
						prevPatStatB.updatePatStudyStat();
					}
					if (currentStat.equals("1")){
						curStatusId = currentStatId;
					}
					if ("no".equals(changeStatusMode)){
						if (!StringUtil.isEmpty(remarks)){
							AuditRowEresJB auditJB = new AuditRowEresJB();
							auditJB.setReasonForChangeOfPatientStudyStat(patB.getId(), userId, remarks);
						}
					}

			   	}
			   	else
				{
					patB.setCreator(usr);
					patB.setCurrentStat(currentStat);//KM
					ret = patB.setPatStudyStatDetails();
					prevStatusId = ""+ret;
					
					//Added by Manimaran based on code review
					if (currentStat.equals("1")  &&  ret != -4 ) { //KM
				   		if (!StringUtil.isEmpty(currentStatId) && (StringUtil.stringToNum(currentStatId) > 0)){
				   			prevPatStatB.setId(StringUtil.stringToNum(currentStatId));
				   			prevPatStatB.getPatStudyStatDetails();
				   			prevPatStatB.setCurrentStat("0");
				   			prevPatStatB.setModifiedBy(usr);//JM: 09Jul2008, #3529
				   			prevPatStatB.updatePatStudyStat();
				   		}
						curStatusId = ""+ret;
					}

				}

    	personB.setPersonPKId(personPK);
     	personB.getPersonDetails();
    	personB.setPatDthCause(dthCause);
		if(othDthCause!=null )
		personB.setDthCauseOther(othDthCause);
		else
		personB.setDthCauseOther("");

		CodeDao cd = new CodeDao();
		CodeDao cd1 = new CodeDao();
		int  deadStatPk = cd.getCodeId("ptst_survival","dead");
		String  perDeadStat = String.valueOf(cd1.getCodeId("patient_status","dead"));

		// Update death date in patient demographics when survival status is changed to dead

		if(StringUtil.stringToNum(presurvival)!= StringUtil.stringToNum(survival) && StringUtil.stringToNum(survival)==deadStatPk)
		{
		 personB.setPersonStatus(perDeadStat);
		  personB.setPersonDeathDate(deathDate);
		}
		personB.setModifiedBy(usr);//JM: 26OCT2009: #4087

		AuditRowEpatJB auditEpatJB = new AuditRowEpatJB();
		int lastRaid = auditEpatJB.findLatestRaidForPatient(personB.getPersonPKId());

		personB.updatePerson();

				if ("no".equals(changeStatusMode)){
					if (!StringUtil.isEmpty(remarks)){
						auditEpatJB.setReasonForChangeOfPatient(personB.getPersonPKId(), userId, remarks, lastRaid);
					}
				}

				if (!changeStatusMode.equals("no") && ret == -3)
				{
					msg = MC.M_EnrlStatPat_BackDiffStat;/*msg = "'Enrolled' status for this "+LC.Pat_Patient_Lower+" already exists. Please click on 'Back' and Enter a different status.";*****/
				}
				if (!changeStatusMode.equals("no") && ret == -4)
				{
					msg = MC.M_PatNotEnrl_EtrStat;/*msg = "The "+LC.Pat_Patient_Lower+" is not currently enrolled. To enter this status, please enter an 'Enrolled' status first.";*****/
				}
				if (ret >=0)
				{
					if ("no".equals(changeStatusMode)){
						if (!StringUtil.isEmpty(remarks)){
							AuditRowEresJB auditJB = new AuditRowEresJB();
							auditJB.setReasonForChangeOfPatProt(psk.getPatProtId(), userId, remarks);
							auditJB.setReasonForChangeOfPatientStudyStat(patB.getId(), userId, remarks);
						}
					}
					msg = MC.M_Data_SvdSucc;/*msg = "Data was saved successfully";*****/
          	     }
          	   	if (ret == -2 || ret == -1)
          	   {
          	   		msg = MC.M_DataNotSvd_Succ;/*msg = "Data was not saved successfully";*****/
	    	   }
	    	   %>
			  <br><br><br><br><br><br><br>
				<TABLE width="500" border = "0">
			 	<tr>
					<td align="center">
			     	<P class = "successfulmsg">
			     		 <%=msg%>
				 	</P>
				</td>
			   </tr>
			 </table>
				<%
					if (!changeStatusMode.equals("no") && (ret == -3 || ret == -4 ))
					{

				 %>
			   <TABLE width="550" border = "0">
				<tr>
					<td align=center>
					<!--img src="../images/jpg/Back.gif" align="absmiddle" onclick="window.history.back(); return false;" border="0">
					</img-->

					<a href="patstudystatus.jsp?pkey=<%=personPK%>&studyId=<%=studyId%>&changeStatusMode=yes&statid=<%=prevStat%>" type="submit">  <!--KM-July29 -->
					<%=LC.L_Back%></a>

					</td>
				</tr>
   				</table>
 	 		 	<%
					}
					else
					{
				%>
				<%
	 	    	 String addOneMoreStat = request.getParameter("addOneMoreStat"); 
	 	    	 if (StringUtil.stringToNum(addOneMoreStat) > 0){
	 	    		%>
			 		<script>
			 	    	window.opener.location.reload();
				 	   	$("patstatus").onSubmit=null;
						$("patstatus").action = "patstudystatus.jsp?patStudiesEnrollingOrg=<%=enrollingSite%>&studyId=<%=studyId%>&changeStatusMode=yes&statid=<%=prevStatusId%>&pkey=<%=personPK%>&currentStatId=<%=curStatusId%>&oldPatStudyId=<%=oldPatStudyId%>";
						$("patstatus").submit();
						$("patstatus").action = '#';
					</script>
		     		<%
	 	    	 } else {%>
	 	    	 	<%
				    	// -- Add a hook for customization for after a patient demog update succeeds
			 			String DEFAULT_DUMMY_CLASS = "[Config_PatWflowNav_Interceptor_Class]";
			 			if (DEFAULT_DUMMY_CLASS.equals(LC.Config_PatWflowNav_Interceptor_Class)){
			 				%>
			 				<script>
			 	    	 		window.opener.location.reload();
			 	    	 		setTimeout("self.close()",1000);
			 				</script>
			 				<%
	 	    	 		}else if (!StringUtil.isEmpty(LC.Config_PatWflowNav_Interceptor_Class) &&
		 						!DEFAULT_DUMMY_CLASS.equals(LC.Config_PatWflowNav_Interceptor_Class)) {
			 				try {
			 					Class clazz = Class.forName(LC.Config_PatWflowNav_Interceptor_Class);
			 					Object obj = clazz.newInstance();
			 					Object[] args = new Object[2];
			 					args[0] = (Object)tSession;
			 					args[1] = "patStudyStatusScreen";
			 					((InterceptorJB) obj).handle(args);

			 					String nextURL = (String) args[1];
			 					if (!StringUtil.isEmpty(nextURL)){%>
				 				<script>			
				 					window.opener.location.href = "<%=(String) args[1]%>";
				 					setTimeout("self.close()",1000);
								</script>
								<%} else {%>
								<script>
				 	    	 		window.opener.location.reload();
				 	    	 		setTimeout("self.close()",1000);
				 				</script>
								<%} %>
				 			<%} catch(Exception e) {
				 				e.printStackTrace();
				 			}
			 			}
			 			// -- End of hook
			 	     %>
				<%} %>
	     		<%
	     			}
	}
%>
</Form>
<%
}//end of if body for session

else

{

%>

 <jsp:include page="timeout_childwindow.jsp" flush="true"/>

  <%

}

%>

</DIV>
</BODY>
</HTML>


