<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@page import="java.sql.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*,java.util.StringTokenizer"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="specimenJB" scope="request" class="com.velos.eres.web.specimen.SpecimenJB" />

<%
	HttpSession tSession = request.getSession(true);
	String specIDval="",specId="";
	if (sessionmaint.isValidSession(tSession))
	{	
		String TableStart="<table border='1' width='100%' height='100%' style='border-collapse: collapse;'>";
		TableStart=TableStart+"<tr><td width='293px' valign='top' style='word-wrap:break-word; display:block;'><br><b><font face='Verdana' size='2'>";
		String TableEnd = "</font></b><br></td></tr></table>";
		String pk_storage_id = request.getParameter("strg_pk");
		
		StringTokenizer st = new StringTokenizer(pk_storage_id, ",");
		while (st.hasMoreElements())
		{
			specIDval = String.valueOf(st.nextElement());
			specimenJB.setPkSpecimen(EJBUtil.stringToNum(specIDval));
 			specimenJB.getSpecimenDetails();
			specId=specimenJB.getSpecimenId();
			TableStart = TableStart + "<a href='specimendetails.jsp?mode=M&pkId="+specIDval+"'); style='text-decoration: none'>"+specId+ "</a><br>";
		}
		out.print("$"+TableStart+TableEnd);
	}
	else
	{
		%><jsp:include page="timeout.html" flush="true"/><%
	} //end of else body for session time out
%>

