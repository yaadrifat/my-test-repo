<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Data_SafetyMonitoring%><%--Data Safety Monitoring*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT language="Javascript1.1">

function fOpenWindow(type,section,frmobj){
	width=300;
	height= 300;
	if (type == "Y"){
		width=200;
		height=70;
	}
	if (type == "D"){
		width=520;
		height=320;
	}
	if (type == "M"){
		width=335;
		height=70;
	}		
	frmobj.year.value ="";
	frmobj.month.value ="";
	frmobj.dateFrom.value ="";
	frmobj.dateTo.value ="";

	frmobj.range.value = "";
	if (type == 'A'){
		frmobj.range.value = "All"
	}
	if (type != 'A'){
		windowName=window.open("selectDateRange.jsp?parm="+type +"&section=" +section,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=" +width +",height="+height)
		windowName.focus();
	}
}


function fValidate(frmobj){
	
	reportChecked=false;
	for(i=0;i<frmobj.reportName.length;i++){
		sel = frmobj.reportName[i].checked;
		if (frmobj.reportName[i].checked){
	   		reportChecked=true;
		}
	}
	if (reportChecked==false) {
		alert("<%=MC.M_Selc_Rpt%>");/*alert("Please select a Report");*****/
		return false;
	}

	reportNo = frmobj.repId.value;

	switch (reportNo) {
		case "82": //Active/Enrolling Protocols
				dateChecked=false;
				if (frmobj.range.value != ""){
	   				dateChecked=true;
				}
				if (dateChecked==false) {
					alert("<%=MC.M_Selc_DtRange%>");/*alert("Please select a Date Range");*****/
					return false;
				}
				break;
		
		case "79": //Study Enrollment (All Sites)
		case "80": //Study Enrollment by Race/Gender -- Not in use
		case "116": //Study Enrollment by Race/Gender
		case "81": //Adverse Event Tracking
				if (frmobj.studyPk.value == "") {
					alert("<%=MC.M_Selc_Std%>");/*alert("Please select a Study");*****/
					return false;
				}		

				dateChecked=false;
				if (frmobj.range.value != ""){
	   				dateChecked=true;
				}
				if (dateChecked==false) {
					alert("<%=MC.M_Selc_DtRange%>");/*alert("Please select a Date Range");*****/
					return false;
				}
	   			break;
									
		case "83": //Study Enrollment (Site Specific)
		case "84": //Protocol Compliance		
		case "85": //Adverse Events by Study
		case "115": //Adverse Events Summary
		case "86": //Deaths by Study
				if (frmobj.studyPk.value == "") {
					alert("<%=MC.M_Selc_Std%>");/*alert("Please select a Study");*****/
					return false;
				}
				
				if (frmobj.selOrg.value == "None") {
					alert("<%=MC.M_PlsSelectOrg%>");/*alert("Please select Organization");*****/
					return false;
				}
												
				dateChecked=false;	   		
				if (frmobj.range.value != ""){
	   				dateChecked=true;
				}
				if (dateChecked==false) {
					alert("<%=MC.M_Selc_DtRange%>");/*alert("Please select a Date Range");*****/
					return false;
				}
	   			break;
		
		case "87": //Protocols by Organization		
				if (frmobj.selOrg.value == "None") {
					alert("<%=MC.M_PlsSelectOrg%>");/*alert("Please select Organization");*****/
					return false;
				}
												
				dateChecked=false;	   		
				if (frmobj.range.value != ""){
	   				dateChecked=true;
				}
				if (dateChecked==false) {
					alert("<%=MC.M_Selc_DtRange%>");/*alert("Please select a Date Range");*****/
					return false;
				}
	   			break;
		case "91": //Summary 3 - Accrual to Therapeutic Protocols
				dateChecked=false;
				if (frmobj.range.value != ""){
	   				dateChecked=true;
				}
				if (dateChecked==false) {
					alert("<%=MC.M_Selc_DtRange%>");/*alert("Please select a Date Range");*****/
					return false;
				}
				break;

		case "92": //Summary 4 - Clinical Research Protocols
				dateChecked=false;
				if (frmobj.range.value != ""){
	   				dateChecked=true;
				}
				if (dateChecked==false) {
					alert("<%=MC.M_Selc_DtRange%>");/*alert("Please select a Date Range");*****/
					return false;
				}
				break;
		case "113": //Summary 4 - Clinical Research Protocols Multi-Site
				dateChecked=false;
				if (frmobj.range.value != ""){
	   				dateChecked=true;
				}
				if (dateChecked==false) {
					alert("<%=MC.M_Selc_DtRange%>");/*alert("Please select a Date Range");*****/
					return false;
				}
				break;
		case "117": //Enrollment Summary by Race/gender/ethnicity
		case "114": //NCI Summary 3 new
				dateChecked=false;
				if (frmobj.range.value != ""){
	   				dateChecked=true;
				}
				if (dateChecked==false) {
					alert("<%=MC.M_Selc_DtRange%>");/*alert("Please select a Date Range");*****/
					return false;
				}
				break;
		case "5000": //External Adverse Event Log Report
			if (frmobj.studyPk.value == "") {
				alert("<%=MC.M_Selc_Std%>");/*alert("Please select a Study");*****/
				return false;
			}		

			if (frmobj.selUser.value == "None") {
				alert("<%=MC.M_Selc_User%>");/*alert("Please select a User");*****/
				return false;
			}


			break;
		case "98": //Study Dump
		case "103": //Study Dump
//				dateChecked=false;
//				if (frmobj.range.value != ""){
//	   				dateChecked=true;
//				}
//				if (dateChecked==false) {
//					alert("Please select a Date Range");
//					return false;
//				}
				break;

	}

}

function fSetId(ddtype,frmobj){
	if (ddtype == "report") {//report Id and name are concatenated by %.Need to separate them
		for(i=0;i<frmobj.reportName.length;i++)	{
			if (frmobj.reportName[i].checked){
				lsReport = frmobj.reportName[i].value;	
				ind = lsReport.indexOf("%");
				frmobj.repId.value = lsReport.substring(0,ind);	
				frmobj.repName.value = lsReport.substring(ind+1,lsReport.length);
				break;
			}
		}
	}

	if (ddtype == "org") {				
		frmobj.orgId.value = "";
		frmobj.selOrg.value = "None";
		frmobj.val.value = "";
		if (!(frmobj.repId.value==87)) { 
			if (frmobj.studyPk.value == "") {
				alert("<%=MC.M_Selc_Std%>");/*alert("Please select a Study");*****/
				return false;
			}
		}		
		openwindow(frmobj);
	}
	
	if (ddtype == "study1") {	
		i=frmobj.study1.options.selectedIndex;		
		frmobj.val.value = frmobj.study1.options[i].text;	
		frmobj.studyPk.value = frmobj.study1.options[i].value;	
		frmobj.selOrg.value="None";
		frmobj.orgId.value=null;		
	}
}

function openwindow(frmobj) {
	if (frmobj.repId.value==87) {
		studyPk='';
	} else {
		studyPk =frmobj.studyPk.value;
	}
    windowName=window.open("sitepopup.jsp?studyPk="+studyPk,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=300,height=300");
	windowName.focus();
}

</SCRIPT>


<SCRIPT language="JavaScript1.1">
function openwin12() {
    windowName=window.open("usersearchdetails.jsp?fname=&lname=&genOpenerFormName='reports'&genOpenerUserNameFld='selUser'&genOpenerUserIdFld='id'","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200");
	windowName.focus();
	}
</SCRIPT>

</head>

<% String src="";
src= request.getParameter("srcmenu");
%>	

<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   


<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="repdao" scope="request" class="com.velos.eres.business.common.ReportDaoNew"/>
<jsp:useBean id="repdao1" scope="request" class="com.velos.eres.business.common.ReportDaoNew"/>
<jsp:useBean id="repdao2" scope="request" class="com.velos.eres.business.common.ReportDaoNew"/>
<jsp:useBean id="ctrlD" scope="request" class="com.velos.eres.business.common.CtrlDao"/>

<%@ page language = "java" import = "com.velos.eres.business.section.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.business.common.CtrlDao"%>

<body>
<br>
 
<DIV class="browserDefault" id="div1">
<P class="sectionHeadings"><%=LC.L_Data_SafetyMonitoring%><%--Data Safety Monitoring*****--%> </P>
<%

   HttpSession tSession = request.getSession(true); 
   if (sessionmaint.isValidSession(tSession))   {
   
   //get DSM module account right 
	String modRight = (String) tSession.getValue("modRight");
	int modlen = modRight.length();
	CtrlDao acmod = new CtrlDao();
	acmod.getControlValues("module");
	ArrayList acmodfeature =  acmod.getCValue();
	ArrayList acmodftrSeq = acmod.getCSeq();
	int dsmSeq = acmodfeature.indexOf("MODDSM");
	dsmSeq	= ((Integer) acmodftrSeq.get(dsmSeq)).intValue();
	char dsmAppRight = modRight.charAt(dsmSeq - 1);
	
	//get DSM group right
    int pageRight = 0;
    GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");		

     pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("DSM"));
	  
  if (String.valueOf(dsmAppRight).compareTo("1") == 0 && pageRight > 0 ){  
	String uName =(String) tSession.getValue("userName");	 
	String userId = (String) tSession.getValue("userId");
	
	int acc = EJBUtil.stringToNum((String) (tSession.getValue("accountId")));

	int counter=0;
	int studyId=0;
	StringBuffer study=new StringBuffer();	
	StudyDao studyDao = new StudyDao();	
	studyDao.getStudyValuesForUsers(userId);
	
	study.append("<SELECT NAME=study1 onChange=fSetId('study1',document.reports)>") ;
	for (counter = 0; counter <= (studyDao.getStudyNumbers()).size() -1 ; counter++){
		studyId = ((Integer)((studyDao.getStudyIds()).get(counter))).intValue();
		study.append("<OPTION value = "+ studyId +">" + (studyDao.getStudyNumbers()).get(counter)+ "</OPTION>");
	}
	study.append("<OPTION value=''  selected>"+LC.L_Select_AnOption/*Select an Option*****/+"</OPTION>");
	study.append("</SELECT>");
	
	Calendar cal = Calendar.getInstance();
	int currYear = cal.get(cal.YEAR);

	String filterText="";

	String ver = "";

	CtrlDao ctrl = new CtrlDao();
	ctrl.getControlValues("allsite_rep");

	String catId = (String) ctrl.getCValue().get(0);

	repdao.getAllRep(EJBUtil.stringToNum(catId),acc); //All Site Reports
	ArrayList repIdsAllSites= repdao.getPkReport(); 
	ArrayList namesAllSites= repdao.getRepName();
   	ArrayList descAllSites= repdao.getRepDesc();
	ArrayList repFilterAllSites = repdao.getRepFilters();

	CtrlDao ctrl1 = new CtrlDao();
	ctrl1.getControlValues("site_rep");
	catId = (String) ctrl1.getCValue().get(0);
	repdao1.getAllRep(EJBUtil.stringToNum(catId),acc); //Site Specific Reports
	ArrayList repIdsOneSites= repdao1.getPkReport(); 
	ArrayList namesOneSites= repdao1.getRepName();
   	ArrayList descOneSites= repdao1.getRepDesc();
	ArrayList repFilterOneSites = repdao1.getRepFilters();

// Rajeev - 12/23/03 - Begin
	CtrlDao ctrl2 = new CtrlDao();
	ctrl2.getControlValues("nci_rep");
	catId = (String) ctrl2.getCValue().get(0);
	repdao2.getAllRep(EJBUtil.stringToNum(catId),acc); //NCI Reports
	ArrayList repIdsNci= repdao2.getPkReport(); 
	ArrayList namesNci= repdao2.getRepName();
   	ArrayList descNci= repdao2.getRepDesc();
	ArrayList repFilterNci = repdao2.getRepFilters();
	int repIdNci=0; 
	String nameNci="";
	int lenNci = repIdsNci.size() ;

// Rajeev - 12/23/03 - End


	int repIdAllSite=0; 
	String nameAllSite="";
	int repIdOneSite=0; 
	String nameOneSite="";

	int lenAllSite = repIdsAllSites.size() ;   
	int lenOneSite = repIdsOneSites.size() ;
  
%>

<Form Name="reports" method="post" action="repRetrieve.jsp" target="new" onSubmit="return fValidate(document.reports)">   
<input type="hidden" name="srcmenu" value='<%=src%>'>
<Input type="hidden" name="studyPk">
<input type="hidden" name="orgId" value=>

<input type=hidden name=year>
<input type=hidden name=month>
<input type=hidden name=year1>
<input type=hidden name=dateFrom>
<input type=hidden name=dateTo>

<table width="100%">	
<tr>
	<th width="35%"><Font class="reportText"><%=LC.L_Report%><%--Report*****--%></font></th>
	<th width="25%"><Font class="reportText"><%=LC.L_Required_Filters%><%--Required Filters*****--%></font></th>
	<th width="40%"><Font class="reportText"><%=LC.L_Available_Filters%><%--Available Filters*****--%></font></th>
 </tr>
 </table>
<table width="100%">
	<tr>
	<td width="60%">
		<table>

         <tr>
         	 <td colspan=3>
         	<p class = "reportHeadings"><BR><%=LC.L_All_SitesRpts%><%--All Sites Reports*****--%></p>
         	</td>	
         	</tr>

		<%
		for(counter = 0;counter<lenAllSite;counter++)
		{
			repIdAllSite = ((Integer)repIdsAllSites.get(counter)).intValue();
			nameAllSite=((namesAllSites.get(counter)) == null)?"-":(namesAllSites.get(counter)).toString();

			if ((counter%2)==0) {
		  	%>
      			<tr class="browserEvenRow"> 
        	<%
			}else{ %>
      			<tr class="browserOddRow"> 
        	<%}%>
         	<td width="35%">
			<Input Type="radio" name="reportName" value="<%=repIdAllSite%>%<%=nameAllSite%>" onClick=fSetId('report',document.reports)> <%=nameAllSite%>
    		</td>
			<td width="25%"><%=repFilterAllSites.get(counter)%></td>
		</tr>			
		<%}	%>
		
         <tr>
       	 <td colspan=3>
        	<p class = "reportHeadings"><BR><%=LC.L_SiteSpec_Rpt%><%--Site Specific Reports*****--%></p>
       	</td>	
       	</tr>
		
		<%
		for(counter = 0;counter<lenOneSite;counter++)
		{
			repIdOneSite = ((Integer)repIdsOneSites.get(counter)).intValue();
			nameOneSite=((namesOneSites.get(counter)) == null)?"-":(namesOneSites.get(counter)).toString();
			if ((counter%2)==0) {
		  	%>
      			<tr class="browserEvenRow"> 
        	<%
			}else{ %>
      			<tr class="browserOddRow"> 
        	<%
			}
         	%><td width="35%">
			<Input Type="radio" name="reportName"  value="<%=repIdOneSite%>%<%=nameOneSite%>" onClick=fSetId('report',document.reports)> <%=nameOneSite%>
			</td>
			<td width="25%"><%=repFilterOneSites.get(counter)%></td>
		</tr>
         	<%}%>

<!-- Rajeev - 12/23/03 - Begin -->
         <tr>
       	 <td colspan=3>
        	<p class = "reportHeadings"><BR><%=LC.L_NCI_Rpts%><%--NCI Reports*****--%></p>
       	</td>	
       	</tr>
		
		<%
		for(counter = 0;counter<lenNci;counter++)
		{
			repIdNci = ((Integer)repIdsNci.get(counter)).intValue();
			nameNci=((namesNci.get(counter)) == null)?"-":(namesNci.get(counter)).toString();
			if ((counter%2)==0) {
		  	%>
      			<tr class="browserEvenRow"> 
        	<%
			}else{ %>
      			<tr class="browserOddRow"> 
        	<%
			}
         	%><td width="35%">
			<Input Type="radio" name="reportName"  value="<%=repIdNci%>%<%=nameNci%>" onClick=fSetId('report',document.reports)> <%=nameNci%>
			</td>
			<td width="25%"><%=repFilterNci.get(counter)%></td>
		</tr>
         	<%}%>
<!-- Rajeev - 12/23/03 - End -->


         </table>
</td>

 <td width="40%" valign="top">
	<table><tr>
         	<td><br><br><Font class=reportText><%=LC.L_Date_Filter%><%--Date Filter*****--%>:</Font></td></tr>
			<tr>
			<td colspan="2">
         	<Input type="radio" name="filterType" checked value="1" onClick="fOpenWindow('A','1', document.reports)"> <%=LC.L_All%><%--All*****--%>
         	<Input type="radio" name="filterType" value="2" onClick="fOpenWindow('Y','1', document.reports)"> <%=LC.L_Year%><%--Year*****--%>
         	<Input type="radio" name="filterType" value="3" onClick="fOpenWindow('M','1', document.reports)"> <%=LC.L_Month%><%--Month*****--%>
         	<Input type="radio" name="filterType" value="4" onClick="fOpenWindow('D','1', document.reports)"> <%=LC.L_Date_Range%><%--Date Range*****--%> <br>
         	<input type=text name=range size=30 READONLY value="All">
         	</td>
			</tr>
			 <tr>
	<td colspan="2"><br><br><Font class=reportText><%=LC.L_Addl_Filters%><%--Additional Filters*****--%>:</Font><BR></td>
</tr>
		<tr>
	       	<td><Font class=comments><%=LC.L_Select_Study%><%--Select Study*****--%>:</Font></td>
	       	<td> <%=study%></td>
		</tr>

		<tr>
	       	<td><FONT class = "comments"><A href="#" onclick="fSetId('org',document.reports)"><%=LC.L_Select_Org%><%--Select Organization*****--%></A> </FONT> </td>
	       	<td><Input type="text" name="selOrg" size="20" READONLY value="None"></td>
		</tr>

		<tr>
         	<td valign="middle">
         		<Font class = "reportText"><A href="#" onclick="return openwin12()"><%=LC.L_Select_User%><%--Select User*****--%>:</A> </font>
			</td>
			<td align="bottom">
         		<Input type=text name=selUser size=20 READONLY value=None>
         	 </td>
		</tr>

		<tr>
			<td colspan="2" align="center">
			<br><br>
<!--			<A onClick = "return fValidate(document.reports)" href="#"><input type="image" src="../images/jpg/displayreport.jpg" align="absmiddle" border=0></A>-->
				<input type="image" src="../images/jpg/displayreport.jpg" align="absmiddle" border=0>

			</td>
		</tr>

         </table>
</td>
</table>

<Input type = hidden  name=id value="">
<Input type = hidden  name="val" >
<Input type=hidden name="repId">
<Input type=hidden name="repName">
<Input type=hidden name="rangeType">
<Input Type=hidden name="reportNumber">
<Input type=hidden name="filterText" value = "<%=filterText%>">

</form>

  <%

	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right


} //end of if session times out
else{
%>
	<jsp:include page="timeout.html" flush="true"/> 

<%
} 
%>

<div>
<jsp:include page="bottompanel.jsp" flush="true"/> 

</div>

</div>

<DIV class="mainMenu" id = "emenu">
  <jsp:include page="getmenu.jsp" flush="true"/>   
</DIV>
</body>
</html>

