<!-- 
This JSP will be included in gadgetHome.jsp. All non-local javascript variables and functions 
will be global in the mother JSP. Therefore, make sure to suffix all non-local variables and functions
with the gadget ID.
 -->

<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%> 
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%
if (!sessionmaint.isValidSession(request.getSession(false))) {
	return;
}
%>
<script>
//Declare all functions here
var gadgetPatSchedules = {
	settings: {
		allStudyData: [],
		data: {},
		formatMyStudiesHTML: {},
		setAutoComplete: {},
		screenAction: {},
		openSettings: {},
		getAllStudies: {},
		getSettings: {},
		updateSettings: {},
		submitSettings: {},
		cancelSettings: {},
		clearSettings: {},
		delMyStudy: {},
		deleteStudy: {}
	},
	data: {},
	screenAction: {},
	clearErrMsg: {},
	reloadPatSchedules: {},
	changeVisitFilter: {}
};
//Implement the functions below
gadgetPatSchedules.settings.updateSettings = function() {
	var settingData = gadgetPatSchedules.settings.data;
	var j = {};
	j.id = 'gadgetPatSchedules';
	j.visitFilter = jQuery.trim($('gadgetPatSchedulesJB.settingsVisitFilterMenu').value);
	settingData.visitFilter = j.visitFilter;
	//j.studyIds = '['+jQuery.trim($('gadgetPatSchedulesJB.settingsStudyIds').value)+']';
	//settingData.studyIds = j.studyIds;
	var radios = document.getElementsByName('gadgetPatSchedulesJB.settingsSortBy');
	var radioChecked;
	for (var i=0; i<radios.length; i++){
		if (radios[i].checked){
			radioChecked = radios[i].value;
			switch (radioChecked){
				case "1":
					j.sortBy = "patientStudyId";
					break;
				case "2":
					j.sortBy = "studyNumber";
					break;
				default:
					j.sortBy = "";
					break;
			}
			i = radios.length;
			break;
		}
	}
	if (!radioChecked){
		alert('Please select "Sort By"');
		return;
	}
	settingData.sortBy = j.sortBy;
	
	var dir = jQuery.trim($('gadgetPatSchedulesJB.settingsSortDirMenu').value);
	j.sortDir = (dir == "1")? "asc" : "desc";
	settingData.sortDir = j.sortDir;
	return j;
}
gadgetPatSchedules.settings.submitSettings = function() {
	//gadgetPatSchedules.settings.updateSettings();
	
	var myGadgetId = 'gadgetPatSchedules';
	setSettingsGlobal(myGadgetId);
	$j("#gadgetPatSchedules_settings").slideUp(200).fadeOut(200);
	setTimeout(function() { 
		reloadGadgetScreenGlobal(myGadgetId);
		gadgetPatSchedules.screenAction();
	}, 150);
	
	return true;
}
gadgetPatSchedules.settings.clearSettings = function() {
	if ($('gadgetPatSchedulesJB.settingsStudyIds'))
		$('gadgetPatSchedulesJB.settingsStudyIds').value = '';
	gadgetPatSchedules.clearErrMsg();
};
gadgetPatSchedules.clearErrMsg = function() {
	if ($('gadgetPatSchedules_errMsg_studyIds')) { $('gadgetPatSchedules_errMsg_studyIds').innerHTML = ''; }
};
gadgetPatSchedules.settings.getAllStudies = function(){
	jQuery.ajax({
		url:'gadgetPatSchedules_getStudies',
		async: true,
		cache: false,
		success: function(resp) {
			resetSessionWarningOnTimer();
			gadgetPatSchedules.settings.data.allStudyArray = resp.array;
			gadgetPatSchedules.settings.setAutoComplete();
		}
	});
}
gadgetPatSchedules.settings.getSettings = function(o) {
	try {
		var j = jQuery.parseJSON(o.responseText);
		var settingData = gadgetPatSchedules.settings.data;
		settingData.visitFilter = j.gadgetPatSchedules.visitFilter;
		
		var sortBy = j.gadgetPatSchedules.sortBy;
		if (!sortBy || sortBy=="") sortBy = "patientStudyId";
		settingData.sortBy = sortBy;

		var sortDir = j.gadgetPatSchedules.sortDir;
		if (!sortDir || sortDir=="") sortDir = "asc";
		settingData.sortDir = sortDir;

		var studyIdsStr = j.gadgetPatSchedules.studyIds;
		studyIdsStr=studyIdsStr.substring(studyIdsStr.indexOf("[")+1, studyIdsStr.indexOf("]"));
		if ($('gadgetPatSchedulesJB.settingsStudyIds'))
			$j('#gadgetPatSchedulesJB.settingsStudyIds').val(studyIdsStr);
		
		/*jQuery.ajax({
    		url:'gadgetPatSchedules_getMyStudies',
    		async: true,
    		cache: false,
    		success: function(resp) {
    			resetSessionWarningOnTimer();
    			gadgetPatSchedules.settings.data.myStudyArray = resp.array;
    			gadgetPatSchedules.settings.formatMyStudiesHTML();
    			gadgetPatSchedules.settings.screenAction();
    		}
    	});*/
	} catch(e) {};
	//gadgetPatSchedules.settings.getAllStudies();
};
gadgetPatSchedules.settings.formatMyStudiesHTML = function () {
	var str1 = '<table width="100%" >';

	if (gadgetPatSchedules.settings.data.myStudyArray){
		for(var iX=0; iX<gadgetPatSchedules.settings.data.myStudyArray.length; iX++) {
			if (iX > 5) { break; }
			var dataJSON = gadgetPatSchedules.settings.data.myStudyArray[iX];
			str1 += "<tr id='gadgetPatSchedules_myStudy-row-"+iX+"'><td>";
			str1 += "<a target=\"_blank\" id=\"gadgetPatSchedules_myStudy"+iX+"\"";
			str1 += " href=\""+dataJSON.studyId+"\"";
			str1 += " title=\""+dataJSON.studyId+"\"></a>";
			str1 += dataJSON.studyNumber;
			str1 += "</td><td nowrap='nowrap' align='right'>";
			str1 += "<a style=\"opacity:0; filter:alpha(opacity=0);\" href=\"javascript:gadgetPatSchedules.settings.delMyStudy('";
			str1 += dataJSON.studyId;
			str1 += "');\" id=\"gadgetPatSchedules_delMyStudy"+iX+"\">";
			str1 += "<img id='gadgetPatSchedules_myStudy-del-img-"+iX;
			str1 += "' class='headerImage' style='border:none; opacity:0; filter:alpha(opacity=0);' title='";
			str1 += "Delete Study #".replace("\'", "&#39;")+(iX+1);
			str1 += "' src='./images/delete.gif' border=0 />";
			str1 += "</a>";
			str1 += "</td></tr>";
		}
	}
	str1 += '</table>';
	$('gadgetPatSchedules_myStudiesHTML').innerHTML = str1;
}
gadgetPatSchedules.settings.setAutoComplete = function (){
	gadgetPatSchedules.settings.allStudyData = [];
	if (gadgetPatSchedules.settings.data.allStudyArray){	
		for(var iY=0; iY<gadgetPatSchedules.settings.data.allStudyArray.length; iY++) {
			var dataJSON = gadgetPatSchedules.settings.data.allStudyArray[iY];
			var j = {};
			j["id"] = dataJSON.allStudyId; 
			j["value"] = dataJSON.allStudyNumber;
			gadgetPatSchedules.settings.allStudyData[iY] = j;
		}
	}
	if ($j('#gadgetPatSchedules_study')){
		$j('#gadgetPatSchedules_study').focus(function(){ 
				openAuto('gadgetPatSchedules_study','gadgetPatSchedules_studyId',gadgetPatSchedules.settings.allStudyData,'Study');
		});
		$j('#gadgetPatSchedules_study').focus(function(){  
			openAuto('gadgetPatSchedules_study','gadgetPatSchedules_studyId',gadgetPatSchedules.settings.allStudyData,'Study');
		});
	}
};

gadgetPatSchedules.settings.addStudyCriteria = function (){
	if ($('gadgetPatSchedules_studyId').value && $('gadgetPatSchedules_studyId').value != ''){
		if ($('gadgetPatSchedulesJB.settingsStudyIds').value == '')
			$('gadgetPatSchedulesJB.settingsStudyIds').value += $('gadgetPatSchedules_studyId').value;
		else
			$('gadgetPatSchedulesJB.settingsStudyIds').value += ','+ $('gadgetPatSchedules_studyId').value;
		
		gadgetPatSchedules.settings.submitSettings();
		setTimeout(function() { gadgetPatSchedules.settings.openSettings()}, 250);
	}
};

gadgetPatSchedules.settings.screenAction = function() {
	YUI().use('node', 'event-mouseenter', function(Y) {
		var onMouseoverDelMyStudy = function(e) {
			var trNum = -1;
			if (e.target.get('tagName') == 'TD') {
				var pos1 = 'gadgetPatSchedules_myStudy-row-'.length;
				trNum = e.target.get('parentNode').get('id').substring(pos1,pos1+1);
			} else if (e.target.get('tagName') == 'A') {
				var pos1 = 'gadgetPatSchedules_myStudy-row-'.length;
				trNum = e.target.get('parentNode').get('parentNode').get('id').substring(pos1,pos1+1);
			} else if (e.target.get('tagName') == 'IMG') {
				var pos1 = 'gadgetPatSchedules_myStudy-del-img-'.length;
				trNum = e.target.get('id').substring(pos1,pos1+1);
			}
			if (trNum == -1 || !Y.one('#gadgetPatSchedules_myStudy-del-img-'+trNum)) { return; }
			Y.one('#gadgetPatSchedules_myStudy-del-img-'+trNum).setStyle('opacity', '1');
			Y.one('#gadgetPatSchedules_myStudy-del-img-'+trNum).get('parentNode').setStyle('opacity', '1');
			$j('#gadgetPatSchedules_myStudy-row-'+trNum).addClass('gdt-highlighted-row');
		}
		var onMouseoutDelMyStudy = function(e) {
			var trNum = -1;
			if (e.target.get('tagName') == 'TD') {
				var pos1 = 'gadgetPatSchedules_myStudy-row-'.length;
				trNum = e.target.get('parentNode').get('id').substring(pos1,pos1+1);
			} else if (e.target.get('tagName') == 'A') {
				var pos1 = 'gadgetPatSchedules_myStudy-row-'.length;
				trNum = e.target.get('parentNode').get('parentNode').get('id').substring(pos1,pos1+1);
			} else if (e.target.get('tagName') == 'IMG') {
				var pos1 = 'gadgetPatSchedules_myStudy-del-img-'.length;
				trNum = e.target.get('id').substring(pos1,pos1+1);
			}
			if (trNum == -1 || !Y.one('#gadgetPatSchedules_myStudy-del-img-'+trNum)) { return; }
			Y.one('#gadgetPatSchedules_myStudy-del-img-'+trNum).setStyle('opacity', '0');
			Y.one('#gadgetPatSchedules_myStudy-del-img-'+trNum).get('parentNode').setStyle('opacity', '0');
			$j('#gadgetPatSchedules_myStudy-row-'+trNum).removeClass('gdt-highlighted-row');
		}
		for(var iX=0; iX<5; iX++) {
			if (!Y.one('#gadgetPatSchedules_myStudy-row-'+iX)) { continue; }
			Y.one('#gadgetPatSchedules_myStudy-row-'+iX).on('mousemove', onMouseoverDelMyStudy);
			Y.one('#gadgetPatSchedules_myStudy-row-'+iX).on('mouseenter', onMouseoverDelMyStudy);
			Y.one('#gadgetPatSchedules_myStudy-row-'+iX).on('mouseover', onMouseoverDelMyStudy);
			Y.one('#gadgetPatSchedules_myStudy-row-'+iX).on('mouseleave', onMouseoutDelMyStudy);
			Y.one('#gadgetPatSchedules_myStudy-row-'+iX).on('mouseout', onMouseoutDelMyStudy);
			if (!Y.one('#gadgetPatSchedules_delMyStudy'+iX)) { continue; }
			Y.one('#gadgetPatSchedules_delMyStudy'+iX).setStyle('opacity', '0');
			Y.one('#gadgetPatSchedules_delMyStudy'+iX).setStyle('opacity', '0');
		}
		if ($j('#gadgetPatSchedules_study')){
			$j('#gadgetPatSchedules_study').focus( function(){
				openAuto('gadgetPatSchedules_study','gadgetPatSchedules_studyId',gadgetPatSchedules.settings.allStudyData,'Study');
			});
			$j('#gadgetPatSchedules_study').focus( function(){
				openAuto('gadgetPatSchedules_study','gadgetPatSchedules_studyId',gadgetPatSchedules.settings.allStudyData,'Study');
			});
		}
	});
};
gadgetPatSchedules.settings.delMyStudy = function(studyId) {
	gadgetPatSchedules.settings.deleteStudy(studyId);
};
gadgetPatSchedules.settings.deleteStudy = function(studyId) {
	$('gadgetPatSchedulesJB.settingsStudyIds').value = '';
	try {
		for(var iX=0; iX<gadgetPatSchedules.settings.data.myStudyArray.length; iX++) {
			if (iX > 5) { break; }
			var dataJSON = gadgetPatSchedules.settings.data.myStudyArray[iX];
			if (dataJSON.studyId != studyId){
				if ($('gadgetPatSchedulesJB.settingsStudyIds').value == '')
					$('gadgetPatSchedulesJB.settingsStudyIds').value += dataJSON.studyId;
				else
					$('gadgetPatSchedulesJB.settingsStudyIds').value += ','+ dataJSON.studyId;
			}
		}
		gadgetPatSchedules.settings.submitSettings();
		setTimeout(function() { gadgetPatSchedules.settings.openSettings()}, 250);
	} catch(e) {}
}
gadgetPatSchedules.settings.openSettings = function() {
	$j("#gadgetPatSchedules_settings").slideDown(200).fadeIn(200);
	YUI({
	    gallery: 'gallery-2012.06.20-20-07'
	}).use('gallery-placeholder', 'node', 'io', function(Y) {
		Y.one('#gadgetPatSchedules_saveButton').on('click', gadgetPatSchedules.settings.submitSettings);
    	Y.io('gadgetCookie.jsp', {
        	method: 'POST',
        	data: 'type=typeGetSettings',
        	sync: true,
        	on: {
            	success: function (tranId, o) {
            		resetSessionWarningOnTimer();
            		gadgetPatSchedules.settings.getSettings(o);
                },
                failure: function (tranId, o) {}
            }
        });

    	gadgetPatSchedules.settings.getAllStudies();

    	if ($('gadgetPatSchedules_addStudyButton')){
    		Y.one('#gadgetPatSchedules_addStudyButton').on('click', gadgetPatSchedules.settings.addStudyCriteria);
    	}
	});
}
gadgetPatSchedules.settings.cancelSettings = function() {
	gadgetPatSchedules.settings.clearSettings();
	$j("#gadgetPatSchedules_settings").slideUp(200).fadeOut(200);
}

gadgetPatSchedules.reloadPatSchedules = function() {
	try {
		var visitFilter;
		if ($j('#gadgetPatSchedulesJB.visitFilterMenu')){
			visitFilter = $('gadgetPatSchedulesJB.visitFilterMenu').value;
		}

		if (!gadgetPatSchedules.settings.data.sortBy){
			var radios = document.getElementsByName('gadgetPatSchedulesJB.settingsSortBy');
			var radioChecked;
			for (var i=0; i<radios.length; i++){
				if (radios[i].checked){
					radioChecked = radios[i].value;
					switch (radioChecked){
						case "1":
							gadgetPatSchedules.settings.data.sortBy = "patientStudyId";
							break;
						case "2":
							gadgetPatSchedules.settings.data.sortBy = "studyNumber";
							break;
						default:
							gadgetPatSchedules.settings.data.sortBy = "";
							break;
					}
					i = radios.length;
					break;
				}
			}
		}

		if (!gadgetPatSchedules.settings.data.sortDir){
			var dir = jQuery.trim($('gadgetPatSchedulesJB.settingsSortDirMenu').value);
			gadgetPatSchedules.settings.data.sortDir = (dir == "1")? "asc" : "desc";
		}

		if ($('gadgetPatSchedules_moreLinkTr'))
        	$('gadgetPatSchedules_moreLinkTr').style.display = 'none';

		$("gadgetPatSchedules_myPatSchedulesGrid").innerHTML = '<%=LC.L_Loading%>... '
			+ '<img class="asIsImage" src="../images/jpg/loading_pg.gif" />';

    	jQuery.ajax({
    		url:'gadgetPatSchedules_getPatSchedules?visitFilter='+visitFilter,
    		async: true,
    		cache: false,
    		success: function(resp) {
    			resetSessionWarningOnTimer();
    			gadgetPatSchedules.data.showMoreLink= resp.showMoreLink;
    			gadgetPatSchedules.data.gridData = {
    				colArray: resp.colArray,
    				dataArray: resp.dataArray
    			};
    			YAHOO.example.gadgetPatSchedulesGrid = function() {
    	    		var args = {
    	    			sortBy: gadgetPatSchedules.settings.data.sortBy,
    	    			sortDir: gadgetPatSchedules.settings.data.sortDir,
    	    			respJ: gadgetPatSchedules.data.gridData,
    	    			dataTable: "gadgetPatSchedules_myPatSchedulesGrid"
    	    		};
    	    	
    	    		myGadgetPatSchedulesGrid = VELOS.gadgetPatSchedulesGrid(args);
    	        }();
    	        var showMoreLink = resp.showMoreLink;
    	        var showMoreLinkdisplay = 'none';
    	        var str1 ='';
    	        
    	        if (showMoreLink == "true"){
    	        	str1 = '<table width="100%" cellspacing="0">';
    	        	str1 += "<tr style='background:none'><td align='right'><a href='allSchedules.jsp?srcmenu=tdmenubaritem5&selectedTab=3&openMode=F&filterNextVisit="+visitFilter+"'";
    	    		str1 += " title='<%=LC.L_More_PatSchedules%>'><%=LC.L_More%></a>";
    	    		str1 += "</td></tr>";
	    	    	str1 += '</table>';

	    	    	showMoreLinkdisplay = 'block';
    	        }
    	        if ($('gadgetPatSchedules_morePatSchHTML'))
	        		$('gadgetPatSchedules_morePatSchHTML').innerHTML = str1;
    	        if ($('gadgetPatSchedules_moreLinkTr'))
    	        	$('gadgetPatSchedules_moreLinkTr').style.display = showMoreLinkdisplay;
    		}
    	});   	
    	
	} catch(e) {}
};

gadgetPatSchedules.screenAction = function() {
	YUI({
	    gallery: 'gallery-2012.06.20-20-07'
	}).use('gallery-placeholder', 'node', function(Y) {
		var addToolTipToButton = function(buttonId) {
			Y.one(buttonId).on('mousemove', onMousemoveToolTip);
			Y.one(buttonId).on('mouseleave', onMouseleaveToolTip);
			Y.one(buttonId).on('mouseout', onMouseleaveToolTip);
		};
		
		addToolTipToButton('#gadgetPatSchedules_saveButton');
	});

	if ($('gadgetPatSchedules_addStudyButton')){
		$j("#gadgetPatSchedules_addStudyButton").button();
	}
	$j("#gadgetPatSchedules_saveButton").button();
	$j("#gadgetPatSchedules_cancelButton").button();
	$j("#gadgetPatSchedules_cancelButton").click(function() {
		gadgetPatSchedules.settings.cancelSettings();
	});
	
	if ($('gadgetPatSchedulesJB.visitFilterMenu')){
		YUI().use('node', 'event-mouseenter', function(Y) {
			Y.one('#gadgetPatSchedulesJB\\.visitFilterMenu').on('change', gadgetPatSchedules.reloadPatSchedules);
		});
	}
	gadgetPatSchedules.reloadPatSchedules();    
};

clearerRegistry['gadgetPatSchedules'] = gadgetPatSchedules.settings.clearSettings;
//validatorRegistry['gadgetPatSchedules'] = gadgetPatSchedules.validate;
settingsUpdatorRegistry['gadgetPatSchedules'] = gadgetPatSchedules.settings.updateSettings;
settingsGetterRegistry['gadgetPatSchedules'] = gadgetPatSchedules.settings.getSettings;
settingsOpenerRegistry['gadgetPatSchedules'] = gadgetPatSchedules.settings.openSettings;
screenActionRegistry['gadgetPatSchedules'] = gadgetPatSchedules.screenAction;
</script>