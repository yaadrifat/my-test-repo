<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=LC.L_Update_MstoneStatus%><%--Update MileStone Status*****--%></title>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
}
</SCRIPT>

</head>

<%@ page language = "java" import="java.util.*,com.velos.eres.service.util.*"%>
<jsp:useBean id="milestoneB" scope="request" class="com.velos.eres.web.milestone.MilestoneJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/> 
<%@ page language = "java" import="com.velos.eres.web.milestone.MilestoneJB, com.velos.eres.business.common.*"%>

<jsp:include page="include.jsp" flush="true"/>  


<% String src;
	src= request.getParameter("srcmenu");
	
  String changeStatusTo = "";
		changeStatusTo = request.getParameter("changeStatusTo");
		
  //KM-#D-FIN7 	
  CodeDao cdStat = new CodeDao();
  int wipPk = cdStat.getCodeId("milestone_stat","WIP");
  String wipPkStr = wipPk +"";

  String wipDesc = cdStat.getCodeDescription(wipPk);

  int actPk = cdStat.getCodeId("milestone_stat","A");
  String actPkStr = actPk + "";

  String actDesc = cdStat.getCodeDescription(actPk);
  
  if (StringUtil.isEmpty(changeStatusTo ))	 
  {
  	changeStatusTo =wipPkStr;
  }
  
String changeToDesc="";
  
if ( changeStatusTo.equals(wipPkStr))	changeToDesc = wipDesc;
if (changeStatusTo.equals(actPkStr)) 	changeToDesc = actDesc;
	 
  
%>


<BODY> 
<br>

<DIV class="popDefault" id="div1">
<% 
String milestoneId= "";
String selectedTab = request.getParameter("selectedTab");
		
HttpSession tSession = request.getSession(true); 
 if (sessionmaint.isValidSession(tSession))	{

	milestoneId= request.getParameter("milestoneId");	
	String studyId = request.getParameter("studyId");
	 


	int ret=0;
		
	String editMode=request.getParameter("editMode");
	
	if (editMode == null) {
		editMode="final";
%>
	<FORM name="milestoneupdate" id="editmilestat" method="post" action="editmilestonestatus.jsp" onSubmit="if (validate(document.milestoneupdate)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	
	<br><br>
	<!--  Modified By Parminder Singh Bug#10127-->
	<p class="defComments"><%Object[] arguments = {changeToDesc}; %>
	    <%=VelosResourceBundle.getMessageString("M_MstoneRule_Update",arguments)%><%--Note: The Milestone Rules with existing status as '<%=changeToDesc%>' will not be updated by this action*****--%></p>
	<TABLE width="98%" cellspacing="0" cellpadding="0" >
		<tr><td align="center">
		<b><%=MC.M_Etr_Esign_ToProc%><%--Please enter e-Signature to proceed*****--%></b>
		</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>
	
		</table>
			
<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="editmilestat"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>

	<input type="hidden" name="editMode" value="<%=editMode%>">
	<input type="hidden" name="srcmenu" value="<%=src%>">
	<input type="hidden" name="milestoneId" value="<%=milestoneId%>">
	<input type="hidden" name="studyId" value="<%=studyId%>">  
	<input type="hidden" name="selectedTab" value="<%=selectedTab%>">
	<input type="hidden" name="changeStatusTo" value="<%=changeStatusTo%>">	
 	

	</FORM>
<%
	} else {%>
   <%

			String eSign = request.getParameter("eSign");	
            milestoneId= request.getParameter("milestoneId");			
            studyId=request.getParameter("studyId");			
            selectedTab=request.getParameter("selectedTab");		
            src=	request.getParameter("srcmenu");		
            editMode=null;
			String oldESign = (String) tSession.getValue("eSign");
			
			changeStatusTo = request.getParameter("changeStatusTo");
	
			  if (StringUtil.isEmpty(changeStatusTo ))	
			  {
			  	changeStatusTo ="0";
			  }

			if(!oldESign.equals(eSign)) {
%>        
	<br> <br> 	<br> <br> 	<br> <br> 	<br> <br> 	<br> <br> 

 <table width=100%>

 

<tr>

<td align=center>

<p class = "sectionHeadings">

<%=MC.M_EtrWrongEsign_ClkBack%><%--You have entered a wrong e-Signature. Please click on Back button to enter again.*****--%>

</p>

</td>

</tr>

<tr height=20></tr>

<tr>

<td align=center>

</td>		

</tr>		

</table>		
   <table>	<tr><td width="85%"></td><td>&nbsp;&nbsp;
<button type="button" tabindex=2 onClick="history.go(-1);"><%=LC.L_Back%></button>	
</td></tr>	</table>

        
 		  
         
<%
			} else {
	
				
			StringTokenizer mileIds=new StringTokenizer(milestoneId,",");
			int numIds=mileIds.countTokens();
			String[] strArrMileIds = new String[numIds] ;
				
			for(int cnt=0;cnt<numIds;cnt++){
				
				strArrMileIds[cnt] = mileIds.nextToken();
				
				
				MilestoneJB mlJB= new MilestoneJB();	
				
					
				mlJB.setId(EJBUtil.stringToNum(strArrMileIds[cnt]));					
				mlJB.getMilestoneDetails();
				
				String oldStatus="";
				
				oldStatus = mlJB.getMilestoneStatFK();
				
				if (StringUtil.isEmpty(oldStatus))
				{
					oldStatus="0";
				}
				
						 
				if (!oldStatus.equals(changeStatusTo) )
				{
					mlJB.setMilestoneStatFK(changeStatusTo);
					ret = mlJB.updateMilestone();		
				}
			}		
			

  
			if (ret==-2) {%>
				<br><br><br><br><br> <p class = "sectionHeadings" align = center>  <%=MC.M_MstoneStat_NotUpdt%><%--Milestone Status not Updated*****--%> </p>			
			<%}else { %>
				<br><br><br><br><br> <p class = "sectionHeadings" align = center> <%=LC.L_Mstone_StatusUpdated%><%--Milestone Status Updated*****--%> </p>
				<script>
					window.opener.location.reload();
					setTimeout("self.close()",1000);
				</script>

		<%

			}
			
	
			} //end esign
			
			

	} //end of editMode
	
  }//end of if body for session 
else { %>
 <jsp:include page="timeout.html" flush="true"/> 
 <% } %> 
	
<div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

</DIV>  



</body>
</HTML>


