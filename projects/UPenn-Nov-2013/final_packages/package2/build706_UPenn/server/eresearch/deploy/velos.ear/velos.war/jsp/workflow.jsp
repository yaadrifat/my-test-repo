<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*,com.velos.eres.service.util.*"%>
<%@ page import="com.velos.eres.gems.business.*"%>
<%
	String entityId = (String)request.getParameter("entityId");
	String workflow_type = (String)request.getParameter("workflow_type");
	String moreParams = (String)request.getParameter("params");

	if ("Y".equals(CFG.Workflows_Enabled)) {
		WorkflowJB workflowJB = new WorkflowJB();
		Workflow wf = new Workflow();
		wf = workflowJB.findWorkflowByType(workflow_type);
		
		if (null == wf) {return;}
		
		WorkflowInstanceJB workflowInstanceJB = new WorkflowInstanceJB();
		Integer result = 0;
		String nextDestination = "";
		WorkflowInstance workflowInstance = new WorkflowInstance();

		workflowInstance = workflowInstanceJB.getWorkflowInstanceStatus(entityId, workflow_type);
		
		Integer workflowInstanceId = 0;
		workflowInstanceId = workflowInstance.getPkWfinstanceid();
		
		Integer workflowInstanceStatus = 0;
		workflowInstanceStatus = workflowInstance.getWiStatusflag();
		workflowInstanceStatus = (null == workflowInstanceStatus)? 0 : workflowInstanceStatus;

		if (workflowInstanceId == null || workflowInstanceId == 0) {
			workflowInstanceId = workflowInstanceJB.startWorkflowInstance(entityId, workflow_type);

			workflowInstanceJB.updateWorkflowInstance(entityId, workflow_type, moreParams);

			if (workflowInstanceId != null || workflowInstanceId != 0) {
				nextDestination = workflowInstanceJB.fetchNextTask(entityId, workflow_type);
			}
		} else {
			workflowInstanceJB.updateWorkflowInstance(entityId, workflow_type, moreParams);
			nextDestination = workflowInstanceJB.fetchNextTask(entityId, workflow_type);
		}

		ArrayList workflowTasksList = new ArrayList();
		workflowTasksList = workflowInstanceJB.getWorkflowInstanceTasks(workflowInstanceId);

		workflowInstanceId = workflowInstanceId;
		%>
<%-- Start of Task modal dialog --%>
<div id="workflowTasksDiv" title="<%=wf.getWorkflowName()%>" class="" style="display:none; top: 0px;">
	<div id='workflowTasks_EmbedDataHere'>
		<table class="basetbl outline midalign">
			<tr>
	        	<%--<th>Task #</th> --%>
	        	<th><%=LC.L_Task_Name%></th>
	        	<th><%=LC.L_Complete_QuestionMark%></th>
	        </tr>
		<%
		String trClassName = "";
		String taskCompleteFlag = "0";
		HashMap<String, Object> record = new HashMap<String, Object>();
		for (int indx = 0; indx < workflowTasksList.size(); indx++){
	        record = (HashMap<String, Object>) workflowTasksList.get(indx);
	        trClassName = ((indx%2) == 0)? "reportEvenRow" : "reportOddRow";
	        %>
	        <tr class="<%=trClassName%>">
	        	<%--<td><%=record.get("PK_TASKID")%></td> --%>
	        	<td><%=record.get("WA_NAME")%></td>
	        	<td>
	        	<%
		        	taskCompleteFlag = (String)record.get("TASK_COMPLETEFLAG");
		        	if ("1".equals(taskCompleteFlag)){
		        		%>
		        		<img class="headerImage" align="bottom" src="images/greencheck.png" border="0">
		        		<%
		        	} else {
	        		%>
	        		<img class="headerImage" align="bottom" src="images/exclamation.png" border="0">
	        		<%
		        	}
		        %>
	        	</td>
	        </tr>
	        <%
	    }
		%>
		</table>
	</div>
</div>
<%-- End of Task modal dialog --%>
<script>
var workflowFunctions = {
	createWorkflowDialog: {},
	openWorkflowDialog: {}
};

workflowFunctions.createWorkflowDialog = function (){
	$j('#workflowTasksDiv').dialog({
		minHeight: 200,
		maxHeight: 300,
		maxWidth: 250,
		minWidth: 200,
		autoOpen: false,
		closeText:''
	}).dialog("widget").position({
		my: 'left', 
		at: 'right',
		of: $j('#showWorkflowTasksLink')
	});
};

workflowFunctions.openWorkflowDialog = function (){
	if (!$j("#workflowTasksDiv").dialog("isOpen")){
		$j('#workflowTasksDiv').dialog('open')
		.dialog("widget").position({
			my: 'left',
			at: 'right',
			of: $j('#showWorkflowTasksLink')
		});
	}
};

$j(document).ready(function() {
	workflowFunctions.createWorkflowDialog();
	$j('#showWorkflowTasksLink').click(workflowFunctions.openWorkflowDialog);
	<%if(workflowInstanceStatus.equals(1)) {%>
		$j('#showWorkflowTasksLink').html('<img class="headerImage" align="bottom" src="images/greencheck.png" border="0">');
	<%} else {%>
		workflowFunctions.openWorkflowDialog();
	<%}%>
});

</script>

<%}%>