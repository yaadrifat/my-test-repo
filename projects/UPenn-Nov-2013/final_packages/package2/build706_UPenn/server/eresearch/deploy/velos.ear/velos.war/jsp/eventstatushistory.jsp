<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<title><%=LC.L_Evt_StatusHistory%><%--Event Status History*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<%@page import="com.velos.eres.service.util.StringUtil" %>

<%@ page import="com.velos.eres.business.common.*, com.velos.eres.service.util.*,com.velos.esch.business.common.EventdefDao"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>

<script>
 function confirmBox(name,pageRight,value){
	 var paramArray = [name];
      if (confirm(getLocalizedMessageString("M_Want_DelEvtStat",paramArray)))/*if (confirm("Do you want to delete the Event Status "+ name+"?" ))*****/ {
          if(f_check_perm(pageRight,value)==true) {

	        	return true;
	}else{
	  	return false;
		}
      }
      else
	  	return false;

 }
</script>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id ="eventDefB" scope="session" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="codeLst" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<%@ page language = "java" import = "com.velos.eres.business.section.*,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.DateUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.esch.business.common.*"%>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>


<% String src;

src= request.getParameter("srcmenu");

		String selectedTab =  request.getParameter("selectedTab");

%>

<jsp:include page="panel.jsp" flush="true">

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>



<body>
<DIV class="browserDefault" id="div1">
  <%
	String eventStat = "";
	String eventStatDate = "";
	String eventStatEndDate = "";
	String eventStatNote = "";
	String eventStatAuthor = "";



	EventdefDao eventDefDao = new EventdefDao();
	int len = 0;
	int startDateLen = 0;
	int endDateLen = 0;
	String studyNum = "";

	HttpSession tSession = request.getSession(true);

	if (sessionmaint.isValidSession(tSession))

	{
		String eventId = request.getParameter("eventId");
		String uName = (String) tSession.getValue("userName");
		studyNum = StringUtil.decodeString(request.getParameter("studyNum"));
		String pkey = request.getParameter("pkey");
		Calendar cal1 = new GregorianCalendar();

		String calAssoc=request.getParameter("calassoc");
		calAssoc=(calAssoc==null)?"":calAssoc;

   			int personPK = 0;
   			String patientId = "";
   			String dob = "";
   			String gender = "";
   			String genderId = "";
   			String yob = "";
   			String age ="";
		int patientMob=0;
		int patientDob=0;
		int patientYob=0;
		int patientAge=0;
		int sysYear=0;
		int sysMonth=0;
		int sysDate=0;
		int noOfMonths=0;
		int noOfYears=0;
		int noOfDays=0;
		String deathDate = "";

	person.setPersonPKId(EJBUtil.stringToNum(pkey));
	if (EJBUtil.stringToNum(pkey)>0) {
	person.getPersonDetails();
	patientId = person.getPersonPId();
	genderId = person.getPersonGender();
	dob = person.getPersonDob();
	deathDate = person.getPersonDeathDate();

	gender = codeLst.getCodeDescription(EJBUtil.stringToNum(genderId));
	if (gender==null) { gender=""; }

	// added Gopu on 18th March for fixing the bug No.2063
	if (!(dob==null) && !(dob.equals(""))) {
              java.util.Date dtDob = DateUtil.stringToDate(dob);
			  patientYob = dtDob.getYear()+1900;
			  patientMob = dtDob.getMonth()+1;
			  patientDob = dtDob.getDate();

			   if (! StringUtil.isEmpty(deathDate)) // if patient is dead, calculate age till his dod
			  {
	              java.util.Date dtDeath = DateUtil.stringToDate(deathDate);
	              sysYear = dtDeath.getYear()+1900;
	              sysMonth = dtDeath.getMonth()+1;
	              sysDate = dtDeath.getDate();

			  }
			  else
			  {
				  sysMonth=cal1.get(Calendar.MONTH)+1;
				  sysDate=cal1.get(Calendar.DATE);
				  sysYear=cal1.get(Calendar.YEAR);
			  }


			  if (sysYear==patientYob)
	              patientAge=0;
	          if (sysYear > patientYob)
	          {
				  patientAge=sysYear-patientYob;
                  if(patientMob > sysMonth)
		             patientAge--;
                  if(patientMob==sysMonth && patientDob>sysDate)
			         patientAge--;
	          }
    		  if(patientAge!=0){
    			  Object[] arguments = {String.valueOf(patientAge)};
    			  age = VelosResourceBundle.getLabelString("L_Spc_Years",arguments);  
    		  }
			  if(patientAge==0)
			  {
				  if(patientMob <= sysMonth && sysDate>=patientDob)
					  noOfMonths=sysMonth-patientMob;
				  else if(patientMob<sysMonth && patientDob>sysDate)
					  noOfMonths=(sysMonth-patientMob)-1;
				  else if (patientMob>=sysMonth&&patientDob<=sysDate)
				  {
					  noOfMonths=(patientMob-sysMonth);
					  noOfMonths=12-(noOfMonths);
				  }
				  else if(patientMob>=sysMonth&&patientDob>sysDate)
				  {
					  noOfMonths=patientMob-sysMonth;
					  noOfMonths=11-(noOfMonths);
				  }
				  Object[] arguments1 = {String.valueOf(noOfMonths)};
				  age=VelosResourceBundle.getLabelString("L_Spc_Months",arguments1);
			  }
			  if(patientAge==0 && noOfMonths==0)
			  {
				  if(patientDob<=sysDate)
					  noOfDays=(sysDate-patientDob)+1;
				  else
				  {
					  noOfDays=sysDate-patientDob;
					  if (patientMob==1)
						  noOfDays=31-(-noOfDays);
					  if (patientMob==2)
					  {
						  noOfDays=28-(-noOfDays);
						  if(sysYear%4==0 && (sysYear % 100 != 0 || sysYear % 400 == 0))
							  noOfDays=29-(-noOfDays);
					  }
					  if (patientMob==3)
						  noOfDays=31-(-noOfDays);
					  if (patientMob==4)
						  noOfDays=30-(-noOfDays);
					  if (patientMob==5)
						  noOfDays=31-(-noOfDays);
					  if (patientMob==6)
						  noOfDays=30-(-noOfDays);
					  if (patientMob==7)
						  noOfDays=31-(-noOfDays);
					  if (patientMob==8)
						  noOfDays=31-(-noOfDays);
					  if (patientMob==9)
						  noOfDays=30-(-noOfDays);
					  if (patientMob==10)
						  noOfDays=31-(-noOfDays);
					  if (patientMob==11)
						  noOfDays=30-(-noOfDays);
					  if (patientMob==12)
						  noOfDays=31-(-noOfDays);
					   noOfDays=noOfDays+1;
				  }
				  Object[] arguments2 = {String.valueOf(noOfDays)};
				  age= VelosResourceBundle.getLabelString("L_Spc_Days",arguments2);
			  }
			  //yob = dob.substring(6,10);
		      //age = cal1.get(Calendar.YEAR) - EJBUtil.stringToNum(yob);
	}
	}

	//KM-14Sep09
	String org_id = request.getParameter("org_id");
	String calledFrom = request.getParameter("calledFrom");
	String eventName ="";

	 if (calledFrom.equals("P")){

  		  	eventDefB.setEvent_id(EJBUtil.stringToNum(org_id));
			eventDefB.getEventdefDetails();
		    eventName = eventDefB.getName(); 
	 }else{	

		   eventassocB.setEvent_id(EJBUtil.stringToNum(org_id));
		   eventassocB.getEventAssocDetails();
		   eventName = eventassocB.getName(); 
	 }
	
	

	String visit = request.getParameter("visit");
	String visitName=request.getParameter("visitName");

	visitName=(visitName==null)?"":visitName;
	visitName =  StringUtil.decodeString(visitName)	;

	String patProtId = request.getParameter("patProtId");

	String fromDelPage = request.getParameter("fromDelPage");
	if (fromDelPage==null) fromDelPage="";


	String study = request.getParameter("studyId");
	if (study==null) study="";

	String calId = request.getParameter("calId");
	if (calId==null) calId="";



	String calledFromPage = request.getParameter("calledFromPage");
	if (calledFromPage==null) calledFromPage="";


	String nextpage = request.getParameter("nextpage");
	//if (nextpage==null) nextpage="1";
	if (StringUtil.isEmpty(nextpage)){
		nextpage = "1";
	}

	String usr = (String) tSession.getValue("userId");


		int pageRight = 0;
		 TeamDao teamDao = new TeamDao();
		    teamDao.getTeamRights(EJBUtil.stringToNum(study),EJBUtil.stringToNum(usr));
		    ArrayList tId = teamDao.getTeamIds();
		    int patdetright = 0;
		    if (tId.size() == 0)
			{
		    	pageRight=0 ;
		    }
			else
			{
		    	stdRights.setId(EJBUtil.stringToNum(tId.get(0).toString()));
		    	 	ArrayList teamRights ;
								teamRights  = new ArrayList();
								teamRights = teamDao.getTeamRights();

								stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
								stdRights.loadStudyRights();

		    	tSession.setAttribute("studyRights",stdRights);
		    	if ((stdRights.getFtrRights().size()) == 0)
				{
		    	 	pageRight= 0;
		    		patdetright = 0;
		    	}
				else
				{
		    		pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYMPAT"));
		    		patdetright = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYVPDET"));
		    		tSession.setAttribute("studyManagePat",new Integer(pageRight));
		    		tSession.setAttribute("studyViewPatDet",new Integer(patdetright));
		    	}
		    }
		int idLen = 0;
		int bal = 0;

		idLen = eventId.length();
		bal =10 - idLen;
		String zeroStr = "";

		for (int i=0;i < bal;i++)
		{
			zeroStr = zeroStr + "0";
		}
		eventId = zeroStr + eventId;

		eventDefDao = eventDefB.getEventStatusHistory(eventId);

		ArrayList eventStats = new ArrayList();
		ArrayList eventStatDates = new ArrayList();
		ArrayList eventStatEndDates = new ArrayList();
		ArrayList eventStatNotes = new ArrayList();
		ArrayList eventStatAuthors = new ArrayList();

		ArrayList subTypes = new ArrayList();
		ArrayList eventStatIds = new ArrayList();
		
		ArrayList isDefault = new ArrayList();




		eventStats = eventDefDao.getEventStats();
		eventStatDates = eventDefDao.getEventStatDates();
        eventStatEndDates = eventDefDao.getEventStatEndDates();
        eventStatNotes = eventDefDao.getEventStatNotes();
        eventStatAuthors = eventDefDao.getEventStatAuthors();

		subTypes = eventDefDao.getSubTypes();
		eventStatIds = eventDefDao.getEventStatIds();
		//Rohit Bug # 5079
		isDefault = eventDefDao.getIsDefault();

	String  subType = "";
	String  eventStatId = "";
	String  is_default = "";



		len= eventStats.size();

	if ((pageRight >=6) || ((pageRight == 5 || pageRight == 7 )) )

	{

%>
  <br>
  <P class = "userName"> <%= uName %> </P>

  <P class="sectionHeadings"> <%=LC.L_Evt_StatusHistory%><%--Event Status History*****--%> </P>

<Form name="eventstat" method="post" action="" onsubmit="">
    <input type="hidden" name="srcmenu" value='<%=src%>'>
    <input type="hidden" name="selectedTab" value='<%=selectedTab%>'>
    <input type="hidden" name="patProtId" value='<%=patProtId%>'>

     <input type="hidden" name="studyId" value='<%=study%>'>
     <input type="hidden" name="calId" value='<%=calId%>'>
     <input type="hidden" name="calledFromPage" value='<%=calledFromPage%>'>
     <input type="hidden" name="nextpage" value='<%=nextpage%>'>




   <table width="100%" cellspacing="0" cellpadding="0" border=0 >
   <tr>
	  <td>
         <P ><%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%>: &nbsp;&nbsp;<%=studyNum%></P>
     </td>
    </tr>
	<tr >
		<td height=10>
		</td>
	</tr>

   </table>

    <%if  (EJBUtil.stringToNum(pkey)>0){%>
<table width=100%>
 <tr>
 	<!--modified on 040405 for fixing the Bug # 2063 -->

 	<td class=tdDefault width=20%>	<B> <%=LC.L_Patient_Id%><%--<%=LC.Pat_Patient%> Id*****--%>: <%=patientId%> </B>	</td>
		<%if(!(dob== null) && !(dob.equals(""))){%>
		    		<td class=tdDefault width=20%>	<B> <%=LC.L_Age%><%--Age*****--%>: <%=age%> </B>	</td>
			<%} else { %>
		    	<td width=15%>
		    		<td class=tdDefault width=20%>	<B> <%=LC.L_Age%><%--Age*****--%>: - </B>	</td>
    			</td>
			<%}%>


	<td class=tdDefault width=20%>	<B><%=LC.L_Gender%><%--Gender*****--%>: <%=gender%> </B>	</td>
	<td class=browserEvenRow width=20%>	</td>
 </tr>
</table>
<%} %>
<table width=100%>
<tr>
	<td class=tdDefault width = 20% ><%=LC.L_Patient_Visit%><%--<%=LC.Pat_Patient%> Visit*****--%>: <%=visitName%></td>
</tr>
<tr>
<td class=tdDefault><%=LC.L_Event_Name%><%--Event Name*****--%>: <%=eventName%></td>
</tr>
    <tr >
      <td width = "70%">
        <P class = "defComments"><%=MC.M_ListDisp_EvtStatHist%><%--The list below displays the event''s status history*****--%>:</P>
      </td>
    </tr>
</table>
<table width="100%" >
       <tr>
	   		<th width="20%"> <%=LC.L_Status%><%--Status*****--%> </th>
        	<th width="15%"> <%=LC.L_Start_Date%><%--Start Date*****--%></th>
	        <th width="15%"> <%=LC.L_End_Date%><%--End Date*****--%> </th>
	        <th width="30%"> <%=LC.L_Notes%><%--Notes*****--%> </th>
			<th width="20%"> <%=LC.L_Entered_By%><%--Entered By*****--%> </th>
			<th width="20%"> <%=LC.L_Delete%><%--Delete*****--%> </th>

      </tr>
       <%

    for(int counter = 0;counter<len;counter++)
	{
		eventStatDate = ((eventStatDates.get(counter)) == null)?"-":(eventStatDates.get(counter)).toString();

		 startDateLen = eventStatDate.length();

		if (startDateLen > 1) {
			eventStatDate = DateUtil.dateToString(java.sql.Date.valueOf(eventStatDate.substring(0,10)));
		}

		eventStatEndDate=((eventStatEndDates.get(counter)) == null)?"-":(eventStatEndDates.get(counter)).toString();

		endDateLen=eventStatEndDate.length();

		if (endDateLen > 1) {
		eventStatEndDate = DateUtil.dateToString(java.sql.Date.valueOf(eventStatEndDate.substring(0,10)));
		}
		eventStat=((eventStats.get(counter)) == null)?"-":(eventStats.get(counter)).toString();
		eventStatNote=((eventStatNotes.get(counter)) == null)?"-":(eventStatNotes.get(counter)).toString();
		eventStatAuthor=((eventStatAuthors.get(counter)) == null)?"-":(eventStatAuthors.get(counter)).toString();


		subType=((subTypes.get(counter)) == null)?"-":(subTypes.get(counter)).toString();
		eventStatId=((eventStatIds.get(counter)) == null)?"-":(eventStatIds.get(counter)).toString();
		is_default=((isDefault.get(counter)) == null)?"-":(isDefault.get(counter)).toString();


		if ((counter%2)==0) {

  %>
      <tr class="browserEvenRow">
        <%

		}

		else{

  %>
      <tr class="browserOddRow">
        <%

		}

  %>
        <td align=center><%=eventStat%> </td>
     	<td align=center><%=eventStatDate%></td>
     	<td align=center ><%=eventStatEndDate%></td>
     	<td align=left><%=eventStatNote%></td>
        <td align=center><%=eventStatAuthor%> </td>
        <td align=center>


        <% 

		//KM-14Sep09
		//Rohit Bug # 5079 modified the if condition
		if (!((subType.trim().equalsIgnoreCase("ev_notdone")) && (is_default.equals("1") )) ) {
		//KM-#3818	
		%>
        <A href="deleventstatus.jsp?eventStatId=<%=eventStatId%>&availableSch=<%=patProtId%>&srcmenu=<%=src%>&pkey=<%=pkey%>&visit=<%=visit%>&visitName=<%=StringUtil.encodeString(visitName)%>&eventId=<%=eventId%>&studyNum=<%=StringUtil.encodeString(studyNum)%>&studyId=<%=study%>&calId=<%=calId%>&calledFromPage=<%=calledFromPage%>&nextpage=<%=nextpage%>&org_id=<%=org_id%>&calledFrom=<%=calledFrom%>"
		onClick="return confirmBox('<%=eventStat%>',<%=pageRight%>,'E')"> <img src="./images/delete.gif" border="0"/>
		<%}%>
        </td>
		</tr>
      <%

		}

%>
<tr>

 <td align=center colspan=5>

	<!--
	//JM: 16Jul2008: #3586
	<A href = "javascript:window.history.back();" type="submit"><%=LC.L_Back%></A>
	-->
	<%if (calledFromPage.equals("patientschedule")){ %>

<A href ="patientschedule.jsp?srcmenu=<%=src%>&nextpage=<%=nextpage%>&selectedTab=3&mode=M&page=patientEnroll&availableSch=<%=patProtId%>&generate=N&pkey=<%=pkey%>&studyNum=<%=studyNum%>" type="submit"><%=LC.L_Back%></A>
	<%}else if (calledFromPage.equals("studyadmincal")){%>

		<A href ="studyadmincal.jsp?srcmenu=<%=src%>&nextpage=<%=nextpage%>&selectedTab=10&studyId=<%=study%>&calId=<%=calId%>" type="submit"><%=LC.L_Back%></A>
	<%}%>





</td>

</tr>
    </table>
  </Form>

  <%

	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>

<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/>
</div>

</body>

</html>

