<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.velos.eres.service.util.LC" %>
<%@ page import="com.velos.eres.service.util.StringUtil" %>
<%@ page import="com.velos.eres.service.util.SessionMaint" %>

<%--Include JSPs --%>
<jsp:include page="jqueryUtils.jsp"></jsp:include>
<jsp:include page="velos_includes.jsp"></jsp:include>
<jsp:include page="advEventNewInclude.jsp" flush="true"/>
<%----%>

<%!
static final String studyScreen_AdminGrps = "[Config_StudyScreen_AdminGrps]";
static final String studyScreen_TeamRoles = "[Config_StudyScreen_TeamRoles]";
%>

<%
SessionMaint sessionmaint = new SessionMaint();
HttpSession tSession = request.getSession(true);
if (!sessionmaint.isValidSession(tSession))
	return;

String defUserGroup = (tSession.getAttribute("defUserGroup")).toString();

%>
<div id="submitFailedDialog" title="<%=LC.L_Error%>" class="comments" style="display:none; overflow:auto">
	<table align="center">
		<tr><td height="20px">&nbsp;</td></tr>
		<tr align="center">
			<td id="errorMessageTD"></td>
		</tr>
		<tr><td height="20px">&nbsp;</td></tr>
		<tr align="center">
			<td>
				<button onclick='$j("#submitFailedDialog").dialog("close");'><%=LC.L_Close%></button>
			</td>
		</tr>
	</table>
</div>


<script>
var aeScreenFunctions = {
	aeScrnIsAdmin: {},
	validateAEScreen: {},
	openMessageDialog: {},
	defUserGroup:<%=defUserGroup %>,
	aeScrnAdminGrps:"<%=((studyScreen_AdminGrps.equals(LC.Config_StudyScreen_AdminGrps)) ? 
		"" : LC.Config_StudyScreen_AdminGrps)%>",
		aeScrnTeamRoles: "<%=((studyScreen_TeamRoles.equals(LC.Config_StudyScreen_TeamRoles))? 
			"" : LC.Config_StudyScreen_TeamRoles)%>"
};

{
var grp = ''+aeScreenFunctions.defUserGroup;
var aeScrnAdminGrps = (aeScreenFunctions.aeScrnAdminGrps).split(",");

aeScreenFunctions.aeScrnIsAdmin = false;

if (aeScrnAdminGrps.length == 1){
	if (grp == aeScrnAdminGrps){
		aeScreenFunctions.aeScrnIsAdmin = true;
	}
} else {
	for (var indx=0; indx < aeScrnAdminGrps.length; indx++){
		if (grp == aeScrnAdminGrps[indx].trim()){
			aeScreenFunctions.aeScrnIsAdmin = true;
			break;
		}
	}
}

}

//Genric function to open a dialog with supplied Id
aeScreenFunctions.openMessageDialog = function openMessageDialog(divId)
{
	var messageHgth = navigator.userAgent.indexOf("MSIE") != -1 ? 160 : 90;
	jQuery("#"+divId).dialog({
		height: messageHgth,maxWidth: 250,position: 'center',resizable: false,modal: true,autoOpen: false
    }).siblings('.ui-dialog-titlebar').remove();
	jQuery("#"+divId).dialog("open");
	jQuery('.ui-widget-overlay').css('background', 'white');
	
	
}

	
 
</script>

<script type="text/javascript">
var isValidatedForm = false;

aeScreenFunctions.validateAEScreen = function() {
	
	

	if (aeScreen && aeScreen.validate){

		if(!aeScreen.overrideValidation){
			//Call all the validate functions one-by-one
			if(!advEventNewFunctions.validate(adverseEventScreenForm)){
				return false;
			}
			
			if (!moreDetailsFunctions.validate(adverseEventScreenForm)){ 
				return false;
			}
			
			if (!aeScreen.validate()){
				return false;
			}
		}else{
			alert('overirding validation');
		}
	}else{
		//Call all the validate functions one-by-one
		if(!advEventNewFunctions.validate(adverseEventScreenForm)){
			return false;
		}
		
		if (!moreDetailsFunctions.validate(adverseEventScreenForm)){ 
			return false;
		}
	}
	
	
	if (isValidatedForm) { return true; }
	isValidatedForm = true;
	
	aeScreenFunctions.openMessageDialog('progressDialog');
	
	
	$j.post('updateAdverseEventScreen',
			$j('#adverseEventScreenForm').serialize(),
			function(data) {
			
				var errorMap = data.errorMap;
				var hasErrors = false;
				for (var key in errorMap) {
					hasErrors = true;
					isValidatedForm = false;
					$j('#errorMessageTD').html(errorMap[key]);
					break;
				}
				if (hasErrors) {
					
					$j('#progressDialog').dialog( "destroy" );
					
					$j('#submitFailedDialog').dialog({
						modal:true,
						closeText: '',
						close: function() {
							$j("#submitFailedDialog" ).dialog("destroy");
						}
					});
				} else {
					$("adverseEventScreenForm").onSubmit=null;
					$("adverseEventScreenForm").action ="genericMessage.jsp?messageKey=M_Data_SvdSucc&id=8";
					$("adverseEventScreenForm").submit();
					$("adverseEventScreenForm").action = '#';
					$("adverseEventScreenForm").onSubmit = "return aeScreenFunctions.validateAEScreen();";
				}
			}
		);
		return false;
};
</script>