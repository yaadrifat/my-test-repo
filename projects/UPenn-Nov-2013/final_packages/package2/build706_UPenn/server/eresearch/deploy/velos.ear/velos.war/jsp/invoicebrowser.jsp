<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<link rel="stylesheet" href="js/jquery/themes/base/jquery.ui.all.css">
<title><%=LC.L_Milestone_Invoicing%><%--Milestone >> Invoicing*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

 


<% String src;
src= request.getParameter("srcmenu");
%>

<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   

<script language="Javascript">
function key(e) {
	if(!$j("#submit_btn").show()) $j("#closeBttn").click();
	//$j('#dialogDelete').dialog('close');} 
	//$j('#dialogDelete').hide();
	else{
		 if (e.keyCode == 13) {
		   if(fnClickOnce(e)){
			   $j("#submit_btn").click();
			}
		 }
	}
}
</script>
<body onkeypress="javascript:key(event);">
<!-- Bug NO: 3976 fixed by PK -->
<div id="overDiv" style="position:relative; visibility:hidden; z-index:99999;"> </div> 
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="invB" scope="page" class="com.velos.eres.web.invoice.InvoiceJB" />
<jsp:useBean id="stdRightsB" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />

<%@ page language = "java" import = "com.velos.eres.business.common.InvoiceDao,com.velos.eres.business.common.TeamDao,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.*,com.velos.esch.business.common.*"%>

<%
 String selectedTab = request.getParameter("selectedTab"); 
 String study = request.getParameter("studyId");
 
%>  

  <DIV  class="BrowserTopN" id="div1"> 
	  <jsp:include page="milestonetabs.jsp" flush="true">
		<jsp:param name="studyId" value="<%=study%>"/>
		<jsp:param name="selectedTab" value="<%=selectedTab%>"/>
	  </jsp:include>
	</div>


<DIV class="BrowserBotN BrowserBotN_M_2" id="div1"> 
  <%
   HttpSession tSession = request.getSession(true);
   if (sessionmaint.isValidSession(tSession))
	{
    int pageRight= 0;	 	  
    int mileRight= 0;	 	 	 
	String userIdFromSession = (String) tSession.getValue("userId");	
    int studyId = EJBUtil.stringToNum(study);
    
	
	ArrayList teamId ;
	teamId = new ArrayList();
	TeamDao teamDao = new TeamDao();
	teamDao.getTeamRights(studyId,EJBUtil.stringToNum(userIdFromSession));
	teamId = teamDao.getTeamIds();
	

	if (teamId.size() <=0)
	{	
		mileRight  = 0;
		StudyRightsJB stdRightstemp = new StudyRightsJB();
	}
	if (teamId.size() == 0) { 
		mileRight=0 ;
	}else {
		stdRightsB.setId(EJBUtil.stringToNum(teamId.get(0).toString()));
		 
			ArrayList teamRights ;
			teamRights  = new ArrayList();
			teamRights = teamDao.getTeamRights();
				 
			stdRightsB.setSuperRightsStringForStudy((String)teamRights.get(0));
			stdRightsB.loadStudyRights();
		 
		
		if ((stdRightsB.getFtrRights().size()) == 0){
			mileRight = 0;		
		}else{
			mileRight = Integer.parseInt(stdRightsB.getFtrRightsByValue("MILESTONES"));			
		}		
	}
	tSession.setAttribute("mileRight",(new Integer(mileRight)).toString());
	
	pageRight = mileRight;
	
 	if (pageRight > 0)
	{
	%>
	  <jsp:include page="invbrowsercommon.jsp" flush="true">
		<jsp:param name="studyId" value="<%=study%>"/>
		<jsp:param name="pageRight" value="<%=pageRight%>"/>
		<jsp:param name="selectedTab" value="<%=selectedTab%>"/>
		<jsp:param name="srcmenu" value="<%=src%>"/>
	  </jsp:include>
	  
	<%
		
	} //end of if body for page right

else

{

%>

  <jsp:include page="accessdenied.jsp" flush="true"/>
  
  <%

} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<div class ="mainMenu" id="emenu"> 
  	<jsp:include page="getmenu.jsp" flush="true"/>
</div>
</body>

</html>

