<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<title><%=MC.M_GiveCopy_OfPcol%><%--Give a Copy of the Protocol*****--%></title>



<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.business.common.*" %>





<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>

<SCRIPT language="JavaScript1.1">
function openwin1() {
      windowName=window.open("usersearchdetails.jsp?fname=&lname=&from=teamBrowser","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=500,height=400")
	windowName.focus();
;}

function openwin2() {
      windowName=window.open("userSearchByPin.jsp?userPin=&from=teamBrowser","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=500,height=400")
	windowName.focus();
;}

function opensvwin(id) {
       windowName = window.open("studyview.jsp?studyVPk="+ id,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=500,height=400")
	 windowName.focus();
;}

function openwinsnap() {
    windowName=window.open("snapshottext.htm","Information","toolbar=no,scrollbars=yes,resizable=no,menubar=no,status=yes,width=500,height=400")
	windowName.focus();
;}

</SCRIPT>

<SCRIPT>
function fcheck() {

	formobj=document.user
	
      if(document.user.userName.value == "-") {
	alert("<%=MC.M_Selc_UsrFirst%>");/*alert("Please select a user first");*****/
	document.user.userName.focus();


	 if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   	}

        return false;
	}
;}
</SCRIPT>


<% String src;
String from = "summary";
src= request.getParameter("srcmenu");

%>



<jsp:include page="panel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>   



<body>
<jsp:useBean id="teamB" scope="session" class="com.velos.eres.web.team.TeamJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.team.*,com.velos.eres.business.common.*,com.velos.eres.service.util.*,java.util.*,com.velos.eres.service.util.EJBUtil"%>
<DIV class="browserDefault" id="div1"> 
  <jsp:include page="studytabs.jsp" flush="true">
  <jsp:param name="from" value="<%=from%>"/> 
   </jsp:include>
  <%

   HttpSession tSession = request.getSession(true); 

   if (sessionmaint.isValidSession(tSession))

	{

	String uName = (String) tSession.getValue("userName");

	String study = (String) tSession.getValue("studyId");

	String acc = (String) tSession.getValue("accountId");

	String rights = request.getParameter("right");

	String tab = request.getParameter("selectedTab");
	
	String user = request.getParameter("user");
	
	String userName = request.getParameter("userName");

	userName=(userName==null)?"-":userName.toString();
	
	String userId = request.getParameter("userId");

	userId=(userId==null)?"-":userId.toString();


    int pageRight = EJBUtil.stringToNum(rights);

    StudyViewDao svdMain = new StudyViewDao();
    int pkStudyView = 0;
	String svStr = "";
	String strUpdate = "";

 if (pageRight > 0 )

	{

	   int studyId = 0;   

	   studyId = EJBUtil.stringToNum(study);
	   
	   pkStudyView =  svdMain.getStudyViewIdByStudy(studyId);
	   
	   if (pkStudyView > 0)
	   {
		 strUpdate = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<A href='updatesnapshot.jsp?srcmenu="+src+"&calledfrom=T&studyId=" + studyId + "&selectedTab="+tab+"&right="+pageRight+"&user="+user+"'>"+LC.L_Update_Snapshot/*Update Snapshot*****/+"</A>";	   	
	 	  svStr = "<p class = 'defComments'>"+LC.L_Click_On/*Click on*****/+" &nbsp;<A HREF = '#' onClick='opensvwin(" + pkStudyView  + ")'><IMG src ='../images/jpg/snapshot.jpg' width='28' height = '18' border=0></A>&nbsp;"+MC.M_ViewCur_StdSnapshot/*to view the current status of this Study's Snapshot*****/+" &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<A href = '#' onClick=openwinsnap()>"+MC.M_What_IsSnapshot/*What is a Snapshot*****/ +"</A>" + strUpdate+"</p>";
  		}  
		 else
		 {
		  svStr = "<P class = 'defComments'>"+MC.M_NoStdSnapshot_ViewAces/*At present this study does not have a snapshot. When you assign 'View' access to an Internal or External user, a snapshot will be created automatically.*****/+"<br><A href = '#' onClick=openwinsnap()>"+MC.M_What_IsSnapshot/*What is a Snapshot*****/+" </A> </p>";
		 }
		 

	if(user.equals("I")) {
%>
  <br>
  <P class = "defComments"><%=MC.M_IfUsrPcolCopy_SelUsrFrm%><%--If the user to be given a protocol copy belongs to the same account as you, you may select the user from the account user list.*****--%>
  </P>
  <%=svStr%>
   
   <A HREF=# onClick=openwin1() ><%=LC.L_Select_User%><%--Select User*****--%></A>
<%
	} else {

%>
<br>
  <P class = "defComments"><%=MC.M_IfCopyNotSame_NeedUnqId%><%--If the user to be given a protocol copy does not belong to the same account as you, you will need to know their unique identifying User ID for selecting a user.*****--%>
  </P>
  <%=svStr%>

  <A HREF=# onClick=openwin2() ><%=MC.M_Identify_ExtAccUser%><%--Identify External Account User*****--%></A>

<%
	}
%>


  <Form  name="user" method="post" action="assignStudyCopy.jsp">
	<input type=hidden name=userId >
	<input type=hidden name=srcmenu value=<%=src%> >
	<input type=hidden name=selectedTab value=<%=tab%> >

    <table class = tableDefault width="98%" cellspacing="0" cellpadding="0" border=0 >
      <tr> 
	<td>
        <P class = "defComments"> <%=LC.L_Selected_User%><%--Selected User*****--%></P>
	</td>
      <td>
		<input type=text name=userName value="-" readonly>
	</td>
	<td class=tdDefault>
		<input type=radio name=perGranted value="V" checked >  <%=LC.L_View%><%--View*****--%></input>
		<input type=radio name=perGranted value="M" > <%=LC.L_Modify%><%--Modify*****--%> </input>
	</td>


	
      </tr>
    </table>
	<table class = tableDefault width="98%" cellspacing="0" cellpadding="0" border=0>
	<tr>
	   <td><%=LC.L_Esignature%><%--e-Signature*****--%> <FONT class="Mandatory">* </FONT>
	   </td>
	   <td>
		<input type="password" name="eSign" maxlength="8" autocomplete="off">
		</td><td>
		<input type="image" src="../images/jpg/Submit.gif" onClick = "return fcheck()" align="absmiddle" border="0">

	   </td>
	   <td>
			&nbsp;
	   </td>
	   </tr>
	  <tr>
	   <td>
			&nbsp;
	   </td>
	   <td>
<!--	<input type="image" src="../images/jpg/Submit.gif" onClick = "return fcheck()" align="absmiddle" border="0">-->
	<!--	<input type=submit name=submit value=Submit> -->
	</td>
	   <td>
			&nbsp;
	   </td>
	</tr>
	</table>

  </Form>
  <%
	
	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<DIV class="mainMenu" id = "emenu"> 
  <jsp:include page="getmenu.jsp" flush="true"/>
</DIV>
</body>

</html>
