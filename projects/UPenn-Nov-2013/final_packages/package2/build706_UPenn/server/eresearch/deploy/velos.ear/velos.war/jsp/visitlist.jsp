<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=LC.L_Define_Visits%><%--Define Visits*****--%></title>



<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>



<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">



</head>



<SCRIPT language="javascript">



function confirmBox(eventName,pgRight, calStatus) {

	if ( (calStatus == 'F') || (calStatus == 'A')) {
		alert("<%=MC.M_CntDelVisits_InFrozenCal%>");/*alert("Cannot Delete Visits in a Frozen/Active calendar");*****/
		return false;
	}

	if (f_check_perm(pgRight,'E') == true) {

		var paramArray = [eventName];
		if (confirm(getLocalizedMessageString("L_Del_FromPcol",paramArray))) {/*if (confirm("Delete " + eventName + " from Protocol?")) {*****/

		    return true;

		} else {

			return false;

		}

	} else { 

		return false;

	}			

}

function openForEditVisit(formobj, link) {
          
          if(formobj.calStatus.value == 'D'){
	    alert("<%=MC.M_CalDeac_VisitCntAdded%>");/*alert("Calendar Status is 'Deactivated' Visit cannot be Added or Modified");*****/
	    return false;
	}
	if ((formobj.calStatus.value == 'F') || (formobj.calStatus.value == 'A')) {
		alert("<%=MC.M_CntEdtFroz_ActCal%>");/*alert("Cannot edit Visit in Frozen/Active calendar");*****/
		return false;
	}
	
	
	return openwindow(formobj, link);
		
}

function openForNewVisit(formobj, link) {
        
	 if(formobj.calStatus.value == 'D'){
		 alert("<%=MC.M_CalDeac_VisitCntAdded%>");/*alert("Calendar Status is 'Deactivated' Visit cannot be Added or Modified");*****/
	    return false;
	 }
	 
		if ((formobj.calStatus.value == 'F') || (formobj.calStatus.value == 'A')) {
		alert("<%=MC.M_CntEdtNew_VstFrozCal%>");/*alert("Cannot add new Visits to frozen calendar");*****/
		return false;
	}
	
	return openwindow(formobj, link);
}

function openwindow(formobj,link) {



	if (document.all) {


	}

	else

	{


	}	


	lmode = formobj.mode.value;


	lduration = formobj.duration.value;

	lprotocolId = formobj.protocolId.value;

	lsrcmenu = formobj.srcmenu.value;

	lcalledFrom = formobj.calledFrom.value;

	lcalStatus = formobj.calStatus.value;

    lcalltime="New" ;
	//******************************************************************//

	//	            Vishal-07/19/2002									//

	//			To resize the selectevent new window.                   //

	//*****************************************************************//	

  //windowName=window.open("selectevent.jsp?catId="+lcatId+"&catName="+lcatName+"&mode="+lmode+"&srcmenu="+lsrcmenu+"&duration="+lduration+"&protocolId="+lprotocolId+"&calledFrom="+lcalledFrom+"&calStatus="+lcalStatus+"&cost="+lcost,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=500,height=400");
    
																																				
//    windowName=window.open(link+"&catId="+lcatId+"&catName="+lcatName,"Information","screenX=" + xOffset +",screenY="+yOffset + ",toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=700,height=550");
    windowName=window.open(link,"Information","toolbar=no,resizable=yes,scrollbars=no,menubar=no,status=yes,width=750,height=350 top=100,left=150 ");
    	windowName.focus();

    //End-V



}

</SCRIPT> 







<% String src;

src= request.getParameter("srcmenu");

%>



<jsp:include page="panel.jsp" flush="true"> 



<jsp:param name="src" value="<%=src%>"/>

</jsp:include>   






<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id="protVisitB" scope="request" class="com.velos.esch.web.protvisit.ProtVisitJB"/>



<%@ page language = "java" import = "com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.StringUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB"%><%@page import="com.velos.eres.service.util.MC"%><%@page import="com.velos.eres.service.util.LC"%>
<jsp:useBean id="visitdao" scope="request" class="com.velos.esch.business.common.ProtVisitDao"/>
<jsp:useBean id="ctrldao" scope="request" class="com.velos.esch.business.common.ProtVisitDao"/>

<body>



<% int pageRight = 0;
	int visitId = 0;
	String protocolId = request.getParameter("protocolId");
        String name = "" ;
	String description = "";
        HttpSession tSession = request.getSession(true); 

 if (sessionmaint.isValidSession(tSession))
 {



   int accId = EJBUtil.stringToNum((String) (tSession.getValue("accountId")));
   
  
   String eventId="";
   String eventName="";
   String eventNameHide="";
   String calledFrom="";
   String calStatus="";
   String catId="";
   ArrayList monthsArr = null ;
   ArrayList weeksArr = null ;
   ArrayList daysArr = null ; 
   
   //SV, 10/28/04, added to display (insert after) interval for dependent visits.
   ArrayList insertAfterVisitIdArr = null ; 
   ArrayList insertAfterIntervalArr = null ; 
   ArrayList insertAfterIntervalUnitArr = null ; 
   
   
   String intervalString = "";
   
   
   
   String monthString = "";
   String weekString = "";
   String dayString = ""; 
   String tempStr2 = "";
   String tempStr1= "";

   String attributeString = "";
   String insertAfterString = "";
		
	

   int counter = 0;

   Integer id;

   String uName = (String) tSession.getValue("userName");

   String duration= "";
	
	String newDuration = "";
	
	newDuration =  (String) tSession.getAttribute("newduration");
	
	
	if (StringUtil.isEmpty(newDuration))
	{
		duration =  request.getParameter("duration"); 
	}
	else
	{
		duration = newDuration ; 
	}
	
   String displayType= request.getParameter("displayType");
   String displayDur= request.getParameter("displayDur");
   String pageNo= request.getParameter("pageNo");

   protocolId= request.getParameter("protocolId");

   calledFrom= request.getParameter("calledFrom");

   String mode= request.getParameter("mode");

   calStatus = request.getParameter("calStatus");
   
  

   String refresh_flag= request.getParameter("refresh_flag");

   String calAssoc=request.getParameter("calassoc");
   calAssoc=(calAssoc==null)?"":calAssoc;		

   
   String selectedTab = request.getParameter("selectedTab");
   System.out.println("selected Tab.."+selectedTab);
   ArrayList visitIds= null; 
   ArrayList visitnos= null; 

   ArrayList names= null;

   ArrayList descriptions= null;

   ArrayList event_types= null;

   ArrayList eventCosts= null;
   int max_visit_no = 0;
   
  //  salil 
	if (calledFrom.equals("S")) {

   	   	   StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");	 

	  	   if ((stdRights.getFtrRights().size()) == 0){

			 	pageRight= 0;

		   }else{

				pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));

	   	   }	

//		} else if ((calledFrom.equals("P")) || (calledFrom.equals("L"))) {
		} else if (calledFrom.equals("P")) {
			GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		

			pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));

		}


  // end  by salil

//SV, 9/22/04, select all the visit records for the protocol_id passed in.
	visitdao = protVisitB.getProtocolVisits(EJBUtil.stringToNum(protocolId));
	visitIds = visitdao.getVisit_ids();
	visitnos = visitdao.getVisit_nos(); //SV, 10/28/04
	names = visitdao.getNames();
	
	descriptions = visitdao.getDescriptions();

// we need to store the months, weeks and days, as the duration can be entered in many ways:
// for e.g., 32 day interval can be entered as 1 Month(30 days), 2 days or as 
// 4 weeks and 4 days. To maintain the original entry, we will store and display what was entered.
 
	monthsArr = visitdao.getMonths();
	weeksArr = visitdao.getWeeks();
	daysArr = 	visitdao.getDays();
	max_visit_no = visitdao.getMaxVisitNo(); 


   insertAfterVisitIdArr = visitdao.getinsertAfterVisitId() ; 
   insertAfterIntervalArr = visitdao.getInsertAfterInterval() ; 
   insertAfterIntervalUnitArr = visitdao.getInsertAfterIntervalUnit() ; 
   



%>

<DIV class="tabDefTop" id="div1"> 

<% if (pageRight > 0 )

	{

 %>



<%

   if(mode.equals("N")) {

%>


<P class="sectionHeadings"> <%=LC.L_PcolCal_New%><%--Protocol Calendar >> New*****--%> </P>

<%

   } else if(mode.equals("M")) {

%>

<P class="sectionHeadings"> <%=LC.L_PcolCal_Modify%><%--Protocol Calendar >> Modify*****--%> </P>

<%

   }

%>


<jsp:include page="protocoltabs.jsp" flush="true"> 
<jsp:param name="selectedTab" value="<%=selectedTab%>"/>
</jsp:include>   

</div>
<DIV class="tabDefBot" id="div2"> 

  <table width="100%" cellspacing="0" cellpadding="0" border="0" >
    <tr> 
      <td width = "80%"> 
        <P class = "defComments"><%=MC.M_VisitsCurDef_ForCal%><%--Visits currently defined for this calendar*****--%></P>
      </td>
      <% String link="visitdetail.jsp?srcmenu=" + src + "&eventId=" + eventId + "&duration=" + duration + "&protocolId="+protocolId+ "&calledFrom=" + calledFrom + "&mode=N&calStatus=" + calStatus + "&fromPage=eventbrowser&from=initial&tableName=sch_protocol_visit&max_visit_no="+max_visit_no+"&calassoc="+calAssoc;%>
      <td width="20%"><A href="#" onclick="openForNewVisit(document.definedVisits,'<%=link%>')"><%=MC.M_Define_NewVst%><%--Define a New Visit*****--%></A></td>
    </tr>

  </table>



  

<%-- <Form name="definedVisits" method="post" action="displayDOW.jsp?selectedTab=4&displayType=D&displayDur=3&pageNo=1" onsubmit=""> --%>
<%-- changed display type to be V. per Rehan, when we come from defined visits screen, assume defined visits view in schedule events page --%>
 <Form name="definedVisits" method="post" action="displayDOW.jsp?selectedTab=4&mode=<%=mode%>&duration=<%=duration%>&calledFrom=<%=calledFrom%>&protocolId=<%=protocolId%>&srcmenu=<%=src%>&calStatus=<%=calStatus%>&pageNo=1&displayType=V&headingNo=1&displayDur=3&pageRight=<%=pageRight%>&refresh_flag=<%=refresh_flag%>&calassoc=<%=calAssoc%>" onsubmit="">



<table width="100%">

      <tr> 

       <td width="50%"> 
	</td><td width=50%> 

	   
  </td></tr>

  </Table>

  

    <input type="hidden" name="srcmenu" value="<%=src%>">

    <Input name="duration" type=hidden value=<%=duration%>>

    <Input name="protocolId" type=hidden value=<%=protocolId%>>

    <Input name="calledFrom" type=hidden value=<%=calledFrom%>>

    <Input name="mode" type=hidden value=<%=mode%>>

    <Input name="calStatus" type=hidden value=<%=calStatus%>>
    
    <Input name="calassoc" type=hidden value=<%=calAssoc%>>



    <table width="100%" >

      <tr> 
      	<th width="5%"> <%=LC. L_Seq_No%><%--Seq No.*****--%></th>
	<th width="20%"> <%=LC.L_Visit_Name%><%--Visit Name*****--%> </th>
	<th width="30%"> <%=LC.L_Description%><%--Description*****--%> </th>
	<th width="30%"> <%=LC.L_Interval%><%--Interval*****--%> </th>
	<!-- th width="10%"> Delete </th-->

      </tr>
<%   int len = visitIds.size() ; 

   for (int i=0;i<visitIds.size();i++) 
   {	
   		intervalString = "";
   		// SV, 10/28/04, if the user entered this as a relative visit, show that.
   		if  (    ((Integer)insertAfterIntervalArr.get(i)).intValue()  > 0) { 
			String unitStr = "";   			
			if (insertAfterIntervalUnitArr.get(i).toString().equals("D")) {
				unitStr = LC.L_Days/*"Days"*****/;
			}
			else if (insertAfterIntervalUnitArr.get(i).toString().equals("W")) {
				unitStr = LC.L_Weeks/*"Weeks"*****/;
			}	
			else if (insertAfterIntervalUnitArr.get(i).toString().equals("M")) {
				unitStr = LC.L_Months/*"Months"*****/;
			}
			else {
				unitStr = "";
			}
			int index = 0;
			//insertAfter is essentially the visit id' of the parent. get it's index, then get the name
			index = visitIds.indexOf(insertAfterVisitIdArr.get(i));
			if (index > -1) {
	   			intervalString = insertAfterIntervalArr.get(i).toString() + " " + unitStr + " "+LC.L_AfterVst_Time/*after visit name*****/+":" + names.get(index) ;
	   		}
   		}
   		else {	
				if (    ((Integer)monthsArr.get(i)).intValue()  > 0) { 
					monthString = LC.L_Months/*"Months"*****/+" "+ monthsArr.get(i) ;
				}
				else {
					monthString = "";
				}
					
				intervalString = monthString;

				if (    ((Integer)weeksArr.get(i)).intValue()  > 0) {
					weekString = LC.L_Weeks/*"Weeks"*****/+" "+ weeksArr.get(i)   ;
				}
				else { 
					weekString = "";
				}

				if (intervalString.length() == 0)
					intervalString = weekString;
				else if (weekString.length() > 0)
					intervalString += "," + weekString;
				
				if (    ((Integer)daysArr.get(i)).intValue() != 0)
					dayString = LC.L_Days/*"Days"*****/+(Integer)daysArr.get(i) ;
				else
					dayString = "";
					
				if (intervalString.length() == 0) 
					intervalString = dayString;
				else if (dayString.length() > 0)
					intervalString +=  "," + dayString;
		}
	if ((i%2) == 0) {
%>
      <tr class="browserEvenRow"> 

<%	}

	else {

%>
      <tr class="browserOddRow"> 
<%}%>
<%--SV, 10/29 reverted to seq no in both here and detail screen. Seems like visit_no lost its purpose and may have to dropped out	--%>
<%--changed i+1 to visitnos	--%>
	<td><%=i+1%></td>
<td>  <% String edit_link="visitdetail.jsp?srcmenu=" + src + "&eventId=" + eventId + "&duration=" + duration + "&protocolId="+protocolId+ "&calledFrom=" + calledFrom + "&mode=M&calStatus=" + calStatus + "&visitId=" + visitIds.get(i) + "&fromPage=eventbrowser&from=initial&tableName=sch_protocol_visit"+"&seq_no="+ (i+1)+"&calassoc="+calAssoc ;%>
<A href="#" onClick="openForEditVisit(document.definedVisits,'<%=edit_link%>')" > <%=names.get(i)%> </A></td>
	<td><%=(descriptions.get(i) == null)?"":descriptions.get(i)%></td>
	<td><%=intervalString%></td>

	<td>
	<A href="visitdelete.jsp?srcmenu=<%=src%>&visitId=<%=visitIds.get(i)%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&fromPage=eventbrowser&from=initial&catId=<%=catId%>&tableName=sch_protocol_visit&calassoc=<%=calAssoc%>" onclick="return confirmBox('<%=names.get(i)%>',<%=pageRight%>, '<%=calStatus%>')"><img src="./images/delete.gif" border="0" align="left"/></A>
	<input type="hidden" name="max_visit_no" value="<%=max_visit_no%>">
	
<%}//for%>	
    </table>

  <br>

  <br>

  <table width="100%" cellspacing="0" cellpadding="0">

  <tr>

      <td align=center> 

<!--	SV, 10/25/04, cal-enh-??	<input type="image" src="../images/jpg/Back.gif" align="absmiddle" onclick="window.history.back(); return false;" border="0"> -->
		<A href="eventbrowser.jsp?selectedTab=2&mode=<%=mode%>&duration=<%=duration%>&calledFrom=<%=calledFrom%>&protocolId=<%=protocolId%>&srcmenu=<%=src%>&calStatus=<%=calStatus%>&pageNo=<%=pageNo%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&calassoc=<%=calAssoc%>" type="submit"><%=LC.L_Back%></A>

      </td>

      <td> 

		<button type="submit"><%=LC.L_Next%></button>

      </td> 

      </tr>

  </table>


  </Form>

 <%//SV, 9/15/04, cal-enh-04 %> 
<jsp:include page="paramHide.jsp" flush="true"/>			


 <%

	} //end of if body for page right

else

{

%>

  <jsp:include page="accessdenied.jsp" flush="true"/>

 <%

 } //end of else body for page right
}//end of if body for session

else

{

%>

  <jsp:include page="timeout.html" flush="true"/>

<%

}

%>

  <div> 

    <jsp:include page="bottompanel.jsp" flush="true"/>

  </div>

</div>

<div class ="mainMenu" id = "emenu"> 

  <jsp:include page="getmenu.jsp" flush="true"/>

</div>



</BODY>



</HTML>

