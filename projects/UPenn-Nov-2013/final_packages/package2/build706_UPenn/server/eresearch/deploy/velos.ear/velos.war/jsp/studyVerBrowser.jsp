<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<%
boolean isIrb = "irb_upload_tab".equals(request.getParameter("selectedTab")) ? true : false;
String includeTabsJsp = isIrb ? "irbnewtabs.jsp" : "studytabs.jsp";
if (isIrb) {
%>
<title><%=MC.M_ResComp_NewUpldDocu%><%--Research Compliance >> New Application >> Upload Documents*****--%></title>
<% } else { %>
<title><%=LC.L_Std_Ver%><%--<%=LC.L_Study%> >> Versions*****--%></title>
<% } %>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<% String src;
src= request.getParameter("srcmenu");
%>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<SCRIPT language="javascript">
var windowName;
var screenWidth = screen.width;
var screenHeight = screen.height;

function setOrder(formobj,orderBy)
{

	if (formobj.orderType.value=="desc") {
		formobj.orderType.value= "asc";
	} else 	if (formobj.orderType.value=="asc") {
		formobj.orderType.value= "desc";
	}

	formobj.orderBy.value= orderBy;


	formobj.submit();
}

function closeChildWindow() {
	if (windowName != null) {
		windowName.close();
	}
}

function confirmBox(ver,pgRight,num) {
	if (windowName != null) {
		windowName.close();
	}

	if (f_check_perm(pgRight,'E') == true) {
		if (num == 1)
		{
			//Added by Ganapathy on 04-15-05
			//alert("The default version can not be deleted.");
			alert("<%=MC.M_ThisVerStd_YouNotDel%>");/*alert("This is the only version left for this <%=LC.Std_Study_Lower%>. You can not delete this version.");*****/
			return false;
		}
		var paramArray = [ver];
		msg=getLocalizedMessageString("L_Del_StdVer",paramArray);/*msg="Delete <%=LC.L_Study%> Version " + ver+ "?";*****/
		if (confirm(msg))
		{
    		return true;
		}
		else
		{
			return false;
		}
	} else {
		return false;
	}
}


function openWin(pgRight,mode,src,tab,studyVerId)
{	if (f_check_perm(pgRight,'E') == true) {
		windowName= window.open("studyver.jsp?mode=" + mode + "&srcmenu=" + src + "&selectedTab=" + tab + "&studyVerId=" +  studyVerId,"information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=500,height=300");
		windowName.focus();
	}
}
function openNewVersionWin(pgRight,mode,src,tab,studyId)
{	if (f_check_perm(pgRight,'N') == true) {
		windowName= window.open("studyver.jsp?mode=" + mode + "&srcmenu=" + src + "&selectedTab=" + tab + "&studyId=" +  studyId,"information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=500,height=300");
		windowName.focus();
	}
}

function openNewMultiVersionWin(pgRight,pageRightApndx,mode,src,tab,studyId)
{	if (f_check_perm(pgRight,'N') == true && f_check_perm(pageRightApndx,'N') == true) {
		windowName= window.open("appendix_file_multi.jsp?mode=" + mode + "&srcmenu=" + src + "&selectedTab=" + tab + "&studyId=" +  studyId,"information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=1150,height=400");
		windowName.focus();
	}
}

function openWinStatus(pgRight,mode,studyVerId,verNumber,statusId)
{
	var checkType;

	if (mode=='N')
		checkType = 'N';
	else
		checkType = 'E';

	var otherParam;
	<% String sectionHeading = isIrb ? LC.L_Status_Dets/*"Status Details"*****/ : MC.M_MngPcolVer_StatDets/*"Manage Protocols >> Versions >> Status Details"*****/; %>
	otherParam = "&moduleTable=er_studyver&statusCodeType=versionStatus&sectionHeading=<%=sectionHeading%>&statusId=" + statusId;
		  if (f_check_perm(pgRight,checkType) == true) {
		//JM: 031006: if condition changed
		if (verNumber!='')
		windowName= window.open("editstatus.jsp?mode=" + mode + "&modulePk=" +  studyVerId + "&verNumber=" + verNumber + otherParam,"statusdetails","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=500,height=300");
		else
		windowName= window.open("editstatus.jsp?mode=" + mode + "&modulePk=" +  studyVerId + otherParam,"statusdetails","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=500,height=300");
		windowName.focus();
	}
}

</SCRIPT>
<body>

<div id="overDiv" style="position:absolute;visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

<%
	String from = "version";
	String tab = request.getParameter("selectedTab");

	String studyIdForTabs = "";
 	studyIdForTabs = request.getParameter("studyId");

%>
<DIV class="BrowserTopn" id = "divtab">
<jsp:include page="<%=includeTabsJsp%>" flush="true">
				<jsp:param name="from" value="<%=from%>"/>
				<jsp:param name="selectedTab" value="<%=tab%>"/>
				<jsp:param name="studyId" value="<%=studyIdForTabs%>"/>
</jsp:include>
</DIV>

<%
HttpSession tSession = request.getSession(true);


%>
<%
String stid= request.getParameter("studyId");
if(stid==null || stid==""){%>
<SCRIPT LANGUAGE="JavaScript">
	if(screenWidth>1280 || screenHeight>1024)
		document.write('<DIV class="BrowserBotN BrowserBotN_S_1" id="div1"  style="height:75%;">')
	else
		document.write('<DIV class="BrowserBotN BrowserBotN_S_1" id="div1">')
</SCRIPT>
	<%} else {%>
<SCRIPT LANGUAGE="JavaScript">
	if(screenWidth>1280 || screenHeight>1024)
		document.write('<DIV class="BrowserBotN BrowserBotN_S_3" id="div1" style="height:75%;">')
	else
		document.write('<DIV class="BrowserBotN BrowserBotN_S_3" id="div1">')
</SCRIPT>
<%}%>
	<jsp:useBean id="studyVerB" scope="request" class="com.velos.eres.web.studyVer.StudyVerJB"/>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<jsp:useBean id="appendixB" scope="request" class="com.velos.eres.web.appendix.AppendixJB"/>
	<%@ page language = "java" import = "com.velos.eres.business.ulink.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.service.util.EJBUtil"%>
	<%

	if (sessionmaint.isValidSession(tSession))
	{

		String studyId = (String) tSession.getValue("studyId");
		String usrId ;
		usrId = (String) tSession.getValue("userId");

		String uName = (String) tSession.getValue("userName");
		String defUserGroup = (String) tSession.getAttribute("defUserGroup");

		int pageRight = 0;
		int pageRightApndx = 0;

        if(studyId == "" || studyId == null) {
	%>
	  <jsp:include page="studyDoesNotExist.jsp" flush="true"/>
	  <%
	  } else {

	    	StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");
			if ((stdRights.getFtrRights().size()) == 0){
		 		pageRight= 0;
			}else{
				pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYVER"));
				pageRightApndx = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYAPNDX"));
	   		}
		// change by salil  moving the closing bracket of else
	     ///change by salil on 15-sep-2003

		// int viewVer=0;
		 //int  secRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYSEC"));
		// int  apndxRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYAPNDX"));

		// if(secRight >=4 && apndxRight >=4){ viewVer = 4;}

	   if ( pageRight > 0 )
		{



		//JM: 11/17/2005
			String orderType = "";
			orderType = request.getParameter("orderType");
			//if (orderType==null) orderType="";

			if (EJBUtil.isEmpty(orderType))
			{
			orderType = "desc";
			}

			String orderBy = "";
			orderBy = request.getParameter("orderBy");
			if (orderBy==null)		orderBy="";
			if (EJBUtil.isEmpty(orderBy))
			orderBy = "PK_STUDYVER";






			String ddStudyvercat = "" ;
			CodeDao cd1=new CodeDao();
			cd1.getCodeValues("studyvercat");
			cd1.setCType("studyvercat");
	        cd1.setForGroup(defUserGroup);


			String studyvercat=request.getParameter("dStudyvercat");
			if (studyvercat==null) studyvercat="";


			if (studyvercat.equals("")){
			ddStudyvercat=cd1.toPullDown("dStudyvercat");
			}
			else{
			ddStudyvercat=cd1.toPullDown("dStudyvercat",EJBUtil.stringToNum(studyvercat),true);
			}

			String ddStudyvertype = "" ;
			CodeDao cd2=new CodeDao();
			cd2.getCodeValues("studyvertype");

			cd2.setCType("studyvertype");
	        cd2.setForGroup(defUserGroup);

			String studyvertype=request.getParameter("dStudyvertype");
			if (studyvertype==null) studyvertype="";

			if (studyvertype.equals("")){
			ddStudyvertype=cd2.toPullDown("dStudyvertype");
			}
			else{
			ddStudyvertype=cd2.toPullDown("dStudyvertype",EJBUtil.stringToNum(studyvertype),true);
			}


			String ddVersionStatus = "" ;
			CodeDao cd3=new CodeDao();
			cd3.getCodeValues("versionStatus");
			cd3.setCType("versionStatus");
	        cd3.setForGroup(defUserGroup);


			String versionStatus=request.getParameter("dVersionStatus");
			if (versionStatus==null) versionStatus="";

			if (versionStatus.equals("")){
			ddVersionStatus=cd3.toPullDown("dVersionStatus");
			}
			else{
			ddVersionStatus=cd3.toPullDown("dVersionStatus",EJBUtil.stringToNum(versionStatus),true);
			}

			String	dStudyvercat =	request.getParameter("dStudyvercat");
			String dStudyvertype =  request.getParameter("dStudyvertype");
			String dVersionStatus = request.getParameter("dVersionStatus");







			String studyVerNumber = request.getParameter("studyVerNumber");
			if (studyVerNumber==null) studyVerNumber="";

			String verCat = request.getParameter("dStudyvercat");
			if (verCat==null) verCat="";
			String verType = request.getParameter("dStudyvertype");
			if (verType==null) verType="";

			String verStat = request.getParameter("dVersionStatus");
			if (verStat==null) verStat="";


	%>

		<Form name="studyverbrowserSearh" method="post" action="studyVerBrowser.jsp" onsubmit="">
				<Input type="hidden" name="selectedTab" value="<%=tab%>">

<div class="tmpHeight"></div>
<table  width="99%" border="0" cellspacing="0" cellpadding="0" class="basetbl outline midAlign">
	<tr height="20">
	<td colspan="9"> <b> <%=LC.L_Search_By%><%--Search By*****--%></b></td>
	</tr>
	<tr >
	<!--Mukul: To fix Bug 4274  -->
	<td width="10%" >  <%=LC.L_Version%><%--Version*****--%> &#35; : </td>
	<td width="10%" >	<% if (studyVerNumber.equals("")) {%>
					<Input type=text name="studyVerNumber" size=15 value="">
					<%}else{%>
					<Input type=text name="studyVerNumber" size=15 value="<%=studyVerNumber%>">
					<%}%>
	</td>
	<td width="10%" align="right"> <%=LC.L_Category%><%--Category*****--%>:&nbsp;   </td>
	<td width="15%">     <%=ddStudyvercat%>  </td>

	<td width="10%" align="right"> <%=LC.L_Type%><%--Type*****--%>:&nbsp; </td>
	<td width="15%" >          <%=ddStudyvertype%>
	</td>
	<td width="10%" align="right"> <%=LC.L_Status%><%--Status*****--%>:&nbsp;</td>
	<td width="15%" >  <%=ddVersionStatus%></td>
	<td width="10%" valign="bottom">
					<button type="submit"><%=LC.L_Search%></button>
	      			</td>

				</tr>
				</table>

				</form>


	<%
//+++++++++++++++++++++++++++++++++++++JM pagination+++++++++++++++++++++++++++++++++++++++++++++//


		int curPage = 0;
		long startPage = 1;
		String stPage;
		long cntr = 0;
		String pagenum  = request.getParameter("page");


		if (pagenum == null)
		{
			pagenum = "1";
		}

		curPage = EJBUtil.stringToNum(pagenum);

		String count1 = "";
		String count2 = "";
		String countSql = "";
		String formSql = "";


		String sWhere = "";
        String vcWhere = "";
        String vtWhere = "";
        String vstWhere = "";
        String obWhere = "";
        String otWhere = "";


        StringBuffer sqlBuffer = new StringBuffer();
       // int intStudyId =EJBUtil.stringToNum(studyId);





	 	String mysqlSelect = "SELECT PK_STUDYVER,"
                    + " (select count(*) from er_studysec where fk_studyver = pk_studyver ) secCount, "
                    + " (select count(*) from er_studyapndx where fk_studyver = pk_studyver) apndxCount, "
                    + " FK_STUDY,"
                    + " STUDYVER_NUMBER,studyver_date,TO_CHAR(STUDYVER_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) as STUDYVER_DATE_STR,"
                    + " (select codelst_desc from er_codelst where pk_codelst=a.STUDYVER_CATEGORY) as STUDYVER_CATEGORY,"
                    + " (select codelst_desc from er_codelst where pk_codelst=a.STUDYVER_TYPE) as STUDYVER_TYPE,"
                    + " STUDYVER_NOTES,"
                    + " CREATOR,"
                    + " IP_ADD, CODELST_DESC STATUS_CODEDESC, CODELST_SUBTYP STATUS_CODESUBTYP,PK_STATUS "
                    + " FROM ER_STUDYVER a , ( Select CODELST_DESC, CODELST_SUBTYP, PK_STATUS,STATUS_MODPK, FK_CODELST_STAT  from "
                    + " er_studyver i, ER_STATUS_HISTORY, er_codelst c Where i.fk_study = "+EJBUtil.stringToNum(studyId)
                    + " and i.PK_STUDYVER =  STATUS_MODPK and STATUS_MODTABLE =  'er_studyver'  "
                    + " and STATUS_END_DATE is null and c.pk_codelst = FK_CODELST_STAT ) stat "
                    + " WHERE FK_STUDY = " + EJBUtil.stringToNum(studyId)
                    + " and STATUS_MODPK (+) = pk_studyver " ;


				if ((studyVerNumber!= null) && (! studyVerNumber.trim().equals("") && ! studyVerNumber.trim().equals("null")))
				{
					sWhere = " and upper(a.STUDYVER_NUMBER) like upper('%";
					sWhere+=studyVerNumber.trim();
					sWhere+="%')";
				}

				vcWhere = " and a.STUDYVER_CATEGORY not in (select fk_codelst from er_codelst_hide where fk_grp = " +defUserGroup +" and codelst_type='studyvercat' and codelst_hide_table  = 1)";

				if ((verCat!= null) && (! verCat.trim().equals("") && ! verCat.trim().equals("null")))
				{
					vcWhere = vcWhere  + " and upper(a.STUDYVER_CATEGORY) = upper(";
					vcWhere+=verCat;
					vcWhere+=")";
				}



				vtWhere = " and ( a.STUDYVER_TYPE is null or a.STUDYVER_TYPE not in (select fk_codelst from er_codelst_hide where fk_grp = " +defUserGroup +" and codelst_type='studyvertype' and codelst_hide_table  = 1) ) ";

				if ((verType!= null) && (! verType.trim().equals("") && ! verType.trim().equals("null")))
				{
					vtWhere = vtWhere + " and a.STUDYVER_TYPE = UPPER(" ;
					vtWhere+=verType;
					vtWhere+=")";
				}

				vstWhere = " and FK_CODELST_STAT not in (select fk_codelst from er_codelst_hide where fk_grp = " +defUserGroup +" and codelst_type='versionStatus' and codelst_hide_table  = 1)";

				if ((verStat!= null) && (! verStat.trim().equals("") && ! verStat.trim().equals("null")))
				{
					vstWhere = vstWhere + " and FK_CODELST_STAT = UPPER(" ;
					vstWhere+=verStat;
					vstWhere+=")";
				}
				if ((orderBy!= null) && (! orderBy.trim().equals("") && ! orderBy.trim().equals("null")))
				{
					obWhere = " order by " +orderBy+ " " + orderType;

				}
			//	else{
			//
			//		obWhere = " order by PK_STUDYVER DESC ";
			//	}

				sqlBuffer.append(mysqlSelect);

	            if(sWhere!=null){
	            	sqlBuffer.append(sWhere);
	            }
	            if(vcWhere !=null){
	            	sqlBuffer.append(vcWhere );
	            }
	            if(vtWhere!=null){
	            	sqlBuffer.append(vtWhere);
	            }
	            if(vstWhere !=null){
	            	sqlBuffer.append(vstWhere );
	            }
	            if(orderBy!=null){
	            	sqlBuffer.append(obWhere);
	            }


	            formSql=sqlBuffer.toString();
	            //out.println(formSql);


				count1 = "select count(*) from  ( " ;
				count2 = ")"  ;
				countSql = count1 + formSql + count2 ;


				long rowsPerPage=0;
				long totalPages=0;
				long rowsReturned = 0;
				long showPages = 0;

			    boolean hasMore = false;
			    boolean hasPrevious = false;
			    long firstRec = 0;
			    long lastRec = 0;
			    long totalRows = 0;

			    String type ="" ;
			    String type1="";

			    rowsPerPage =  Configuration.MOREBROWSERROWS ;
			    totalPages =Configuration.PAGEPERBROWSER ;



		        BrowserRows br = new BrowserRows();
		        br.getPageRows(curPage,rowsPerPage,formSql ,totalPages,countSql,orderBy,orderType);
		   	    rowsReturned = br.getRowReturned();
			    showPages = br.getShowPages();
			    startPage = br.getStartPage();
			    hasMore = br.getHasMore();
			    hasPrevious = br.getHasPrevious();
			    totalRows = br.getTotalRows();
			    firstRec = br.getFirstRec();
			    lastRec = br.getLastRec();

				String verStatus = null;
				String popCrfString = "";
				AppendixDao appendixDao =new AppendixDao();
				AppendixDao appendixDao1 =new AppendixDao();
				int counter = 0;
//+++++++++++++++++++++++++++++++++++++JM pagination+++++++++++++++++++++++++++++++++++++++++++++//


			%>


<div class="tmpHeight"></div>
			<table width="100%" cellspacing="0" cellpadding="0" border=0 >
			<tr >
			<td>&nbsp;</td>
				<td width = "50%" class="lhsFont">
					<P class = "sectionheadings"> <%=MC.M_AssocVer_DocuList%><%--Associated Versions/Documents Listed Below*****--%></P>
			    </td>
			    <td align="center" width="25%">
<A href=# onclick="openNewVersionWin(<%=pageRight%>,'N','<%=src%>','<%=tab%>','<%=studyId%>')"><%=LC.L_Add_NewVersion_Upper%><%--ADD NEW VERSION*****--%></A>
                </td><td width="25%">&nbsp;
<A href=# onclick="openNewMultiVersionWin(<%=pageRight%>,<%=pageRightApndx%>,'N','<%=src%>','<%=tab%>','<%=studyId%>')"><%=MC.M_AddNew_VerOrDocu_Upper%><%--ADD NEW VERSION/DOCUMENT*****--%></A>
                </td>

		    </tr>
			</table>
	<Form name="studyverbrowser" method="post" action="studyVerBrowser.jsp" onsubmit="">
				<Input type="hidden" name="selectedTab" value="<%=tab%>">
				<Input type="hidden" name="orderType" value="<%=orderType%>">
				<Input type="hidden" name="orderBy" value="<%=orderBy%>">
				<Input type="hidden" name="page" value="<%=pagenum%>">
				<Input type="hidden" name="mode" value="M">
				<input type="hidden" name="rowsReturned" value=<%=rowsReturned%>>
				<input type="hidden" name="verCat" value=<%=verCat%>>
				<input type="hidden" name="verType" value=<%=verType%>>
				<input type="hidden" name="verStat" value=<%=verStat%>>


				<input type="hidden" name="dStudyvercat" value=<%=dStudyvercat%>>
				<input type="hidden" name="dStudyvertype" value=<%=dStudyvertype%>>
				<input type="hidden" name="dVersionStatus" value=<%=dVersionStatus%>>
				<input type="hidden" name="studyVerNumber" value=<%=studyVerNumber%>>



				<table class="outline midAlign" width="99%" cellpadding="0" cellspacing="0" >
					<tr>
						<th width="10%" onClick="setOrder(document.studyverbrowser,'lower(STUDYVER_NUMBER)')" ><%=LC.L_Version%><%--Version*****--%> &#35; &loz;</th>	<!--Mukul: To fix Bug 4274  -->
						<th width="12%" onClick="setOrder(document.studyverbrowser,'STUDYVER_DATE')"> <%=LC.L_Version_Date%><%--Version Date*****--%> &loz;</th>
						<th width="15%" onClick="setOrder(document.studyverbrowser,'lower(STUDYVER_CATEGORY)')"> <%=LC.L_Category%><%--Category*****--%> &loz;</th>
						<th width="10%" onClick="setOrder(document.studyverbrowser,'lower(STUDYVER_TYPE)')"> <%=LC.L_Type%><%--Type*****--%> &loz; </th>
						<th width="12%" onClick="setOrder(document.studyverbrowser,'lower(SECCOUNT)')"> <%=LC.L_Section%><%--Section*****--%> &loz;</th>
						<th width="15%" onClick="setOrder(document.studyverbrowser,'lower(APNDXCOUNT)')"> <%=LC.L_Appendix%><%--Appendix*****--%> &loz; </th>
						<th width="15%" onClick="setOrder(document.studyverbrowser,'lower(STATUS_CODEDESC)')"> <%=LC.L_Version_Status%><%--Version Status*****--%> &loz; </th>
						<th width="5%"><%=LC.L_Delete%><%--Delete*****--%></th>
						<th width="4%"><%=LC.L_Copy%><%--Copy*****--%></th>
					</tr>
					<%




int i = 0;


	for(i = 1 ; i <=rowsReturned ; i++){



			int studyVerId = EJBUtil.stringToNum(br.getBValues(i,"PK_STUDYVER"));
			String studyVerStudyId = br.getBValues(i,"FK_STUDY");//
						  studyVerStudyId=(studyVerStudyId==null)?"":studyVerStudyId;
			String verNumber = br.getBValues(i,"STUDYVER_NUMBER");
						  verNumber=(verNumber==null)?"":verNumber;
			String statusSubType = br.getBValues(i,"STATUS_CODESUBTYP");
						  statusSubType=(statusSubType==null)?"":statusSubType;
			String statusDesc = br.getBValues(i,"STATUS_CODEDESC");
						  statusDesc=(statusDesc==null)?"":statusDesc;
			String statusPk = br.getBValues(i,"PK_STATUS");
						  statusPk=(statusPk==null)?"":statusPk;
			String studyVerNote = br.getBValues(i,"STUDYVER_NOTES");//
						  studyVerNote=(studyVerNote==null)?"":studyVerNote;
			int    secCount = EJBUtil.stringToNum(br.getBValues(i,"secCount"));
			int    apndxCount = EJBUtil.stringToNum(br.getBValues(i,"apndxCount"));


			String studyVerDate	  =	br.getBValues(i,"STUDYVER_DATE_str");
						  studyVerDate=(studyVerDate==null)?"-":studyVerDate;
			String studyVerCat = br.getBValues(i,"STUDYVER_CATEGORY");
						  studyVerCat=(studyVerCat==null)?"-":studyVerCat;
			String studyVerType = br.getBValues(i,"STUDYVER_TYPE");
						  studyVerType=(studyVerType==null)?"-":studyVerType;



		    appendixDao=appendixB.getByStudyIdAndType(studyVerId, "url");
			ArrayList appendixFile_Uris = appendixDao.getAppendixFile_Uris();


			appendixDao1=appendixB.getByStudyIdAndType(studyVerId, "file");
			ArrayList appendixFile_Uris1 = appendixDao1.getAppendixFile_Uris();

			int uriLen = appendixFile_Uris.size();
			int uriLen1 = appendixFile_Uris1.size();
			    popCrfString = "";
			    popCrfString = popCrfString+"<table width=300px cellspacing=0 cellpadding 0>";	

				if(uriLen==0)
						{
							popCrfString = popCrfString + "<tr><td colspan=2 align=left><B>"+LC.L_NoUrls_Attch+"</B><br></td><tr>";/*popCrfString = popCrfString + "<B>No URLs attached</B>";*****/
						}
				else{
					popCrfString = popCrfString + "<tr><td colspan=2 align=left>&#9679;&nbsp;<B>"+LC.L_My_Urls+"</B><br></td><tr>";/*popCrfString = popCrfString + "<LI><B>My URLs</B><UL>";*****/
						for (int k=0;k<uriLen;k++) {
							popCrfString = popCrfString + "<tr><td align=right width=15px >&nbsp;&nbsp;&nbsp;&#9675;</td><td align=left>" + appendixFile_Uris.get(k) + "</td></tr>";
						}
 	    			}
					if(uriLen1==0)
						{
						if(uriLen!=0)
								popCrfString = popCrfString + "" ;
							popCrfString = popCrfString + "<tr><td colspan=2 align=left><br><B>"+MC.M_NoFilesAttached+"</B><br></td><tr>";/*popCrfString = popCrfString + "<br><B>No Files attached</B>";*****/
						}
						else{
							if(uriLen!=0)
								popCrfString = popCrfString + "" ;

				popCrfString = popCrfString + "<tr><td colspan=2 align=left>&#9679;&nbsp;<B>"+LC.L_My_Files+"</B><br></td><tr>" ;/*popCrfString = popCrfString + "<LI><B>My Files</B><UL>" ;*****/
				for(int k1=0;k1<uriLen1;k1++){
					popCrfString = popCrfString + "<tr><td align=right width=15px >&nbsp;&nbsp;&nbsp;&#9675;</td><td align=left>" + appendixFile_Uris1.get(k1) + "</td></tr>";
				}
						}
				popCrfString = popCrfString+"</table>";
				//popCrfString = popCrfString + "</UL></UL>";




						if(statusSubType.equals("F")) {
							verStatus = "Freeze";
						} else
						{
						 verStatus = "";
						}


						if ((i%2)==0) {
						%>
							<tr class="browserEvenRow">
				        <%
						}
						else{
						%>
						    <tr class="browserOddRow">
				        <%
						}
						%>

						<td width="150"><A href="#" onclick="openWin(<%=pageRight%>,'M','<%=src%>','<%=tab%>','<%=studyVerId%>')" ><%= verNumber%></A>
						</td>
						<td width="150"><%=studyVerDate%>
						</td>

						<td width="150"><%=studyVerCat%>
						</td>

						<td width="150"><%=studyVerType%>
						</td>


        				<td width="150"> <A href = "sectionBrowserNew.jsp?mode=M&srcmenu=<%=src%>&selectedTab=<%=tab%>&studyVerId=<%=studyVerId%>"> 
        				<%Object[] arguments1 = {secCount}; %>
	    				<%=VelosResourceBundle.getLabelString("L_Sec_Dyn",arguments1)%><%--Sections (<%=secCount%>)*****--%></A>
				        </td>

        				<td width="150"> <A href = "appendixbrowser.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&studyVerId=<%=studyVerId%>" onmouseover="return overlib('<%=popCrfString%>',CAPTION,'<%=LC.L_Attachments%><%-- Attachments*****--%>');" onmouseout="return nd();">
        				<%Object[] arguments2 = {apndxCount}; %>
	    				<%=VelosResourceBundle.getLabelString("L_Attch_Dyn",arguments2)%><%--Attachments (<%=apndxCount%>)*****--%> </A>
				        </td>
						<td>

						<!--//JM: 031006: verNumber not passing from the link-->
						<!--A href="#" onclick="openWinStatus(<%--=pageRight--%>,'M','<%--=studyVerId--%>','<%--=verNumber--%>','<%--=statusPk--%>')"><%--= statusDesc--%></A-->
						<A href="#" onclick="openWinStatus(<%=pageRight%>,'M','<%=studyVerId%>','','<%=statusPk%>')"><%= statusDesc%></A>
						<% // Added by Ganapathy on 04-15-05
							if(verStatus.equals("Freeze"))
							{}
						 else  {%>

						 <!--<A href="#" onclick="openWinStatus(<%--=pageRight--%>,'N','<%--=studyVerId--%>', '<%--=StringUtil.encodeString(verNumber)--%>','0')">C</A>-->
						  <A href="#" onclick="openWinStatus(<%=pageRight%>,'N','<%=studyVerId%>', '','0')"><img border="0" title="<%=LC.L_Edit%>" alt="<%=LC.L_Edit%>" src="./images/edit.gif" ><%//=LC.L_Edit %><%-- Edit*****--%><%//=LC.L_C%><%--C*****--%></A>
						 <% }%>
					<!--//JM: 031006: StringUtil.encodeString() method applied on version number-->
					<A href="showVersionHistory.jsp?modulePk=<%=studyVerId%>&pageRight=<%=pageRight%>&srcmenu=<%=src%>&selectedTab=<%=tab%>&verNumber=<%=StringUtil.encodeString(verNumber)%>&page=<%=page%>&from=verHistory&fromjsp=showVersionHistory.jsp"><img border="0" title="<%=LC.L_History%>" alt="<%=LC.L_History%>" src="./images/History.gif" ><%--H*****--%></A>

						</td>
						<td align="center"><!--Change by Ganapathy on 04-15-05
						//JM: 12Dec2006, Changed
						-->
						<!--<A href="studyversiondelete.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&studyVerId=<%=studyVerId%>&delMode=null" onClick="return confirmBox('<%=verNumber%>',<%=pageRight%>,<%=counter%>);" ><img src="./images/delete.gif" border="0"/></A>-->
						<A href="studyversiondelete.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&studyVerId=<%=studyVerId%>&delMode=null&from=<%=from%>" onClick="return confirmBox('<%=verNumber%>',<%=pageRight%>,<%=rowsReturned%>);" ><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0" /></A>
						</td>
						<td align="center"><A href="copystudyversion.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&studyVerId=<%=studyVerId%>&copyMode=" onClick="closeChildWindow();return f_check_perm(<%=pageRight%>,'N');"><img border="0" title="<%=LC.L_Copy%>" src="../images/copy.png"/></A>
						</td>

					</tr>
				    <%

			 		} //end of for loop

					%>

				</table>
<!--/////////////////////////JM pagination///////////////////////////////////////////////////-->
		<table >
		<tr><td>
		<% if (rowsReturned > 0)
		{ Object[] arguments = {firstRec,lastRec,totalRows};
		%>
					<font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%></font><%-- <font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s)</font>*****--%>
		<%} else {%>
			<font class="recNumber"> <%=MC.M_NoSrchResMatch_Criteria%><%--No search results matching given criteria found*****--%></font>
		<%}%>
		</td></tr>
		</table>


		<table align=center>
			<tr>
				<%	if (curPage==1) startPage=1;

				for ( int count = 1; count <= showPages;count++)
				{

		   				cntr = (startPage - 1) + count;

		   				if ((count == 1) && (hasPrevious))
						{
				%>
								<td colspan = 2>
								<!-- km-to fix Bug 2442 -->
							  	<A href="studyVerBrowser.jsp?srcmenu=<%=src%>&page=<%=cntr-1%>&studyVerNumber=<%=studyVerNumber%>&dStudyvercat=<%=dStudyvercat%>&dStudyvertype=<%=dStudyvertype%>&dVersionStatus=<%=dVersionStatus%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&selectedTab=<%=tab%>&mode=\"M\"">< <%=LC.L_Previous%><%--Previous*****--%> <%=totalPages%> > </A>
								&nbsp;&nbsp;&nbsp;&nbsp;
								</td>
				<%
						}
				%>
					<td>
				<%
		 		 		if (curPage  == cntr)
				 		{
			    %>
								<FONT class = "pageNumber"><%= cntr %></Font>
		       	<%		}else
		       			{
		       	%>
								<A href="studyVerBrowser.jsp?srcmenu=<%=src%>&page=<%=cntr%>&studyVerNumber=<%=studyVerNumber%>&dStudyvercat=<%=dStudyvercat%>&dStudyvertype=<%=dStudyvertype%>&dVersionStatus=<%=dVersionStatus%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&selectedTab=<%=tab%>&mode=\"M\""><%= cntr%></A>
		        <%		}
		        %>
					</td>
				<%
				}

				if (hasMore)
				{
				%>
	   				<td colspan = 3 align = center>&nbsp;&nbsp;&nbsp;&nbsp;
				    <A href="studyVerBrowser.jsp?srcmenu=<%=src%>&page=<%=cntr+1%>&studyVerNumber=<%=studyVerNumber%>&dStudyvercat=<%=dStudyvercat%>&dStudyvertype=<%=dStudyvertype%>&dVersionStatus=<%=dVersionStatus%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&selectedTab=<%=tab%>&mode=\"M\"">< <%=LC.L_Next%><%--Next*****--%> <%=totalPages%>></A>
					</td>
				<%
				}
				%>

	   		</tr>
	    </table>

<!--///////////////////////////JM pagination/////////////////////////////////////////////////-->


	</Form>
<%
		} //end of if body for page right
		else
		{
		%>
			<jsp:include page="accessdenied.jsp" flush="true"/>
		<%
		} //end of else body for page right
		}// end of else of study check
	}//end of if body for session
	else
	{
	%>
		<jsp:include page="timeout.html" flush="true"/>
	<%
	}
	%>
	<div>
    	<jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
</div>
<div class ="mainMenu" id = "emenu" >
  <jsp:include page="getmenu.jsp" flush="true"/>
</div>
</body>

</html>

