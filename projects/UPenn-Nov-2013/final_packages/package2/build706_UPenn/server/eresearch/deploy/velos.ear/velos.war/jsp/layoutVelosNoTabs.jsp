<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@page import="com.velos.eres.service.util.StringUtil"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="-1">
<meta http-equiv="Cache-Control" content="no-cache">
<title><tiles:insertAttribute name="title" /></title>
	<div id="nav" style="width:99%;">
	    <tiles:insertAttribute name="localization" />
	    <tiles:insertAttribute name="panel" />
	</div>
	<div id="commoninc">
		<tiles:insertAttribute	name="velos_includes" />
	</div>
<style type="text/css">
ul.errorMessage { list-style-type:none; }
ul.errorMessage li { color:red; height:1.5em; text-align:right; }
span.errorMessage { color:red; height:1.5em; text-align:right; }
li.yui3-dd-draggable { list-style-type:none; }
li.yui3-dd-drop { list-style-type:none; }
li.item { list-style-type:none; }
</style>
<script>
if (navigator.userAgent.indexOf("MSIE") > -1) {
	if (navigator.userAgent.indexOf("MSIE 10") < 0
			&& $j('link[href="./styles/ie_1024.css"]')) {
		$j('link[href="./styles/ie_1024.css"]').remove();
	}
	if (navigator.userAgent.indexOf("MSIE 8") > -1) {
		$j('<link rel="stylesheet" type="text/css" href="styles/ie_1024.css" >').appendTo("head");
	}
}
$j(document).ready(function(){
	if ("Y" == "<%=StringUtil.htmlEncodeXss(request.getParameter("noScroll"))%>") {
		$j("div.BrowserBotN").css({overflow:"hidden"});
	}
	$j("div.BrowserTopN").css({height:"91%"});
});
</script>
<s:head /> 
</head>
<body style="overflow:hidden; width:100%">
	<div class="BrowserTopN BrowserTopN_RR_1" style="width:99.5%; overflow:auto" onmouseout='return nd();'>
		<div id="body" onmouseout='return nd();'>
			<tiles:insertAttribute	name="body" />
		</div>
		<div id="footer" onmouseover='return nd();' onmouseout='return nd();'>
			<tiles:insertAttribute	name="footer" />
		</div>
	</div>
</body>
</html>
