<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>
<title><%=LC.L_Mstone_Defn%><%--Milestone Definition*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>


</HEAD>

<script>

function setParam(str,formobj){
	        ripLocaleFromAll();
			formobj.subVal.value=str;
		if (!(test(formobj))) {
			 applyLocaleToAll();
			 return false;
			   }
//		 setVals(formobj.subVal.value,formobj);
		formobj.action = formobj.action + "?addFlag=Add"
		formobj.submit();
	}

function setVals(str,formobj)
{
	formobj.action = formobj.action + "?addFlag=" + str;
	if (test(formobj)== false) {
		return false;
	}
return true;
}


function test(formobj) {

    milestoneType = formobj.milestoneType.value;

	var exist = 0;
	//KM -#5116- Modified for Patient,Study and Additonal milestone status validation.
	for (counter = 0; counter<5; counter++)
	{
		if (milestoneType != 'AM') {
			if (formobj.dStatus[counter].value != '') {
				 exist = 1;
			}
		}
		else
		{
			if (fnTrimSpaces(formobj.description[counter].value) != '') { //KM-#3301
				exist = 1;
			}
		}
		//KM-07Jun10-#4962
		if (formobj.milestoneStat[counter].value != '') {
			exist = 1;
		}

		if (formobj.count[counter].value != '') {
			exist = 1;
		}
	}

	if(exist == 0){

			if (milestoneType == 'PM')
			{
				alert("<%=MC.M_Etr_MandantoryFlds%>");/*alert("Please enter data in all mandatory fields");*****/
				formobj.count[0].focus();
			}
			else

			if(milestoneType == 'SM')
			{
				alert("<%=MC.M_Etr_MandantoryFlds%>");/*alert("Please enter data in all mandatory fields");*****/
				formobj.dStatus[0].focus();
			}
			else if(milestoneType == 'AM')
			{
				alert("<%=MC.M_Etr_MandantoryFlds%>");/*alert("Please enter data in all mandatory fields");*****/
				formobj.description[0].focus();
			}

		formobj.subVal.value="Submit";
		return false;
	}


	for (counter = 0;counter < 5;counter ++)
	{
		//KM -#5116- Modified for Patient,Study and Additonal milestone status validation.
		if (milestoneType == 'AM')
		{
			if ((validate_col_no_alert('Milestone Status',formobj.milestoneStat[counter])) && (! validate_col_no_alert('Description',formobj.description[counter])))
	        {
				alert("<%=MC.M_EtrDesc_ToProc%>");/*alert("Please enter Description to proceed further");*****/
				formobj.description[counter].focus();
				return false;
			}

			if ((validate_col_no_alert('Description',formobj.description[counter])) && (! validate_col_no_alert('Milestone Status',formobj.milestoneStat[counter])))
	        {
				alert("<%=MC.M_SelMstoneStat_ToProc%>");/*alert("Please select Milestone Status to proceed further");*****/
				formobj.milestoneStat[counter].focus();
				return false;
			}
		}
		else if (milestoneType == 'SM')
		{
			if ((validate_col_no_alert('Milestone Status',formobj.milestoneStat[counter])) && (! validate_col_no_alert('<%=LC.Std_Study%> Status',formobj.dStatus[counter])))
	        {
				alert("<%=MC.M_SelStdStat_ToProc%>");/*alert("Please select <%=LC.Std_Study%> Status to proceed further");*****/
				formobj.dStatus[counter].focus();
				return false;
			}

			if ((validate_col_no_alert('<%=LC.Std_Study%> Status',formobj.dStatus[counter])) && (! validate_col_no_alert('Milestone Status',formobj.milestoneStat[counter])))
	        {
				alert("<%=MC.M_SelMstoneStat_ToProc%>");/*alert("Please select Milestone Status to proceed further");*****/
				formobj.milestoneStat[counter].focus();
				return false;
			}

		}

		else if ( milestoneType== 'PM' )
		{
			if ((validate_col_no_alert('<%=LC.Pat_Patient%> Status',formobj.dStatus[counter])) && (! validate_col_no_alert('Count',formobj.count[counter])))
	        {
				alert("<%=MC.M_EtrCnt_ToProc%>");/*alert("Please enter count to proceed further");*****/
				formobj.count[counter].focus();
				return false;
			}

			if ((validate_col_no_alert('Milestone Status',formobj.milestoneStat[counter])) && (! validate_col_no_alert('Count',formobj.count[counter])))
	        {
				alert("<%=MC.M_EtrCnt_ToProc%>");/*alert("Please enter count to proceed further");*****/
				formobj.count[counter].focus();
				return false;
			}

			if ((validate_col_no_alert('Count',formobj.count[counter])) && (!validate_col_no_alert('<%=LC.Pat_Patient%> Status',formobj.dStatus[counter])))
	        {
				alert("<%=MC.M_SelPatStat_ToProc%>");/*alert("Please select <%=LC.Pat_Patient%> Status to proceed further");*****/
				formobj.dStatus[counter].focus();
				return false;
			}

			if ((validate_col_no_alert('Count',formobj.count[counter])) && (!validate_col_no_alert('Milestone Status',formobj.milestoneStat[counter])))
	        {
				alert("<%=MC.M_SelMstoneStat_ToProc%>");/*alert("Please select Milestone Status to proceed further");*****/
				formobj.milestoneStat[counter].focus();
				return false;
			}

			if ( formobj.count[counter].value.indexOf(' ') >= 0 )
			{
			  alert("<%=MC.M_NumVal_ForCnt%>");/*alert("Please enter a numeric value for Count.");*****/
			  formobj.count[counter].focus();
			  formobj.subVal.value="Submit";
			  return false;
			}

    		if ( !isInteger(formobj.count[counter].value) )
			{
				if (!isNegNum(formobj.count[counter].value))
				{
					formobj.count[counter].focus();
					alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid Number");*****/
					return false;
				}
				else
				{
					if (formobj.count[counter].value == '-0')
					{
						formobj.count[counter].focus();
						alert("<%=MC.M_InvalidNegVal_EtrVldCnt%>");/*alert("Invalid negative value. Please enter a valid 'Count'");*****/
						return false;
					}

					if (parseInt(formobj.count[counter].value) < -1)
					{
						formobj.count[counter].focus();
						alert("<%=MC.M_CntLess_EtrValidCount%>");/*alert("The count cannot be less than -1. Please enter a valid 'Count'");*****/
						return false;
					}
				}
			}

			  var index = formobj.count[counter].value.indexOf(".");
		  	  if (index != -1) {
		    		alert("<%=MC.M_DecimalNot_InCountFld%>");/*alert("Decimal value not allowed in the Count field.");*****/
					//formobj.count[counter].focus();
					formobj.subVal.value="Submit";
					return false;
		    	}
			}

		 	//if (formobj.amount[counter].value != '' && isNaN(formobj.amount[counter].value) == true) {
			//   	alert("Please enter a numeric value for Amount.");
			//	formobj.amount[counter].focus();
			//	return false;
			//}


			//var index2 = formobj.amountNum[counter].value.indexOf(".");
		  	  //if (index2 != -1) {
		    		//alert("<%=MC.M_DecimalNotAlw_Fld%>");/*alert("Decimal value not allowed in this field. Please enter the decimal part in the appropriate field");*****/
					//formobj.amountNum[counter].focus();
					//formobj.subVal.value="Submit";
					//return false;
		    	//}

		if (isNaN(formobj.amountNum[counter].value) == true && isNegNum(formobj.amountNum[counter].value) == false) {
			alert("<%=MC.M_EtrNum_AmtVal%>");/*alert("Please enter a numeric value for Amount.");*****/
			formobj.amountNum[counter].focus();
			formobj.subVal.value="Submit";
			return false;
   		}
		
		if (/^\d{0,11}?$/.test(formobj.amountNum[counter].value) == false && !isEmpty(formobj.amountNum[counter].value)) {
			alert("<%=MC.M_ValEtrFwg_AmtFmt11Dgt%>");/*alert("The value entered does not pass the following criteria:\n \n -'Amount' should be a number.\n -'Amount' should conform to format - ###########.## \n    i.e. max 11 digits followed by max 2 decimal places.\n \nPlease enter a valid value.");*****/
			formobj.amountNum[counter].focus();
			formobj.subVal.value="Submit";
			return false;
		}
		
		//if (isNaN(formobj.amountFrac[counter].value) == true ) {
			//alert("<%=MC.M_EtrNum_AmtVal%>");/*alert("Please enter a numeric value for Amount.");*****/
			//formobj.amountFrac[counter].focus();
			//formobj.subVal.value="Submit";
			//return false;
    	//}

		if (/^\d{0,2}?$/.test(formobj.amountFrac[counter].value) == false && !isEmpty(formobj.amountFrac[counter].value)) {
			alert("<%=MC.M_ValEtrFwg_AmtFmt11Dgt%>");/*alert("The value entered does not pass the following criteria:\n \n -'Amount' should be a number.\n -'Amount' should conform to format - ###########.## \n    i.e. max 11 digits followed by max 2 decimal places.\n \nPlease enter a valid value.");*****/
			formobj.amountFrac[counter].focus();
			formobj.subVal.value="Submit";
			return false;
    	}


   	 	formobj.amount[counter].value = formobj.amountNum[counter].value + "." + formobj.amountFrac[counter].value;

		/*if(formobj.amount[counter].value != "."){
	 		if (!(isDecimal(formobj.amount[counter].value))){
	 			alert("Invalid Amount");
				formobj.amountNum[counter].focus();
				formobj.subVal.value="Submit";
		 	 	return false;
		 	}
		}*/

   	 //Added by Ashu for BUG#5403
   	 if(milestoneType == 'SM'){
 		 if( (     (!validate_col_no_alert('Study Status',formobj.dStatus[counter])) 
 	    	   &&  (!validate_col_no_alert('Milestone Status',formobj.milestoneStat[counter]))
 	    	 ) && (
      	   	             (validate_col_no_alert('Amount',formobj.amountNum[counter]))
      	   	    	   ||(validate_col_no_alert('Payment For',formobj.dpayFor[counter]))
      	   	    	  // ||(validate_col_no_alert('Amount Fraction',formobj.amountFrac[counter]))
      	   	    	   ||(validate_col_no_alert('Limit',formobj.mLimit[counter]))
      	   	      )
      	  )
	     {
 			alert("<%=MC.M_Etr_MandantoryFlds%>");/*alert("Please enter data in all mandatory fields");*****/
			 formobj.dStatus[counter].focus();
		     return false;
 	   	}
	}
	//AK:Validate Additional milestones 
   	 else if (milestoneType == 'AM'){
 		 if(  (   (!validate_col_no_alert('Description',formobj.description[counter])) 
 	    	   && (!validate_col_no_alert('Milestone Status',formobj.milestoneStat[counter]))
 	    	  ) &&  	  
	    	   (         (validate_col_no_alert('Amount',formobj.amountNum[counter]))
	    			 //  ||(validate_col_no_alert('Amount Fraction',formobj.amountFrac[counter]))
	      	    	   ||(validate_col_no_alert('Payment For',formobj.dpayFor[counter]))
	      	   )
	      	)   
	        {
 			alert("<%=MC.M_Etr_MandantoryFlds%>");/*alert("Please enter data in all mandatory fields");*****/
			 formobj.description[counter].focus();
		     return false;
 	     	}
	}
   //AK:Validate  Patient Milestones
	 else if (milestoneType == 'PM'){
 		 if(  (   (!validate_col_no_alert('Patient Count',formobj.count[counter])) 	
 			   && (!validate_col_no_alert('Patient Status',formobj.dStatus[counter])) 
 	    	   && (!validate_col_no_alert('Milestone Status',formobj.milestoneStat[counter]))
 	    	  ) &&  	  
	    	   (         (validate_col_no_alert('Amount',formobj.amountNum[counter]))
      	   	    	   ||(validate_col_no_alert('Payment For',formobj.dpayFor[counter]))
      	   	    	  // ||(validate_col_no_alert('Amount Fraction',formobj.amountFrac[counter]))
      	   	    	   ||(validate_col_no_alert('Limit',formobj.mLimit[counter]))
	      	   )
	      	)   
	        {
 			alert("<%=MC.M_Etr_MandantoryFlds%>");/*alert("Please enter data in all mandatory fields");*****/
			 formobj.count[counter].focus();
		     return false;
 	     	}
	}
 	
 

		if(milestoneType != 'AM') {
			if (! isInteger(formobj.mLimit[counter].value))
			{
				formobj.mLimit[counter].focus();
				alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid Number");*****/
			 	return false;
			}
			
			// Fin-22373 Date: 26-Sep-2012 Ankit
			if ((validate_col_no_alert('<%=LC.L_Date_From%>',formobj.dateFrom[counter])) && ( validate_col_no_alert('<%=LC.L_Date_To%>',formobj.dateTo[counter])))
	        {
				if(CompareDates(formobj.dateFrom[counter].value,formobj.dateTo[counter].value)){
					alert("<%=MC.M_DtToNotLessThan_DtFrom%>")
					formobj.dateTo[counter].focus();
					return false;
				}
	        }
		}


    }

		//	if (!(validate_col('e-Signature',formobj.eSign))) return false;
		//    if(isNaN(formobj.eSign.value) == true) {
		//		alert("Incorrect e-Signature. Please enter again");
		//		formobj.eSign.focus();
		//		formobj.subVal.value="Submit";
		//		return false;
		//	   }
		//return setVals(formobj.subVal.value,formobj);
	return true;
}



</script>

<BODY>

<DIV >

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.esch.business.common.*"%>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="studyBN" scope="request" class="com.velos.eres.web.study.StudyJB" />
<jsp:include page="include.jsp" flush="true"/>
<%
	HttpSession tSession = request.getSession(true);
  String studyId = "";
  String mode = "";
  String milestoneType = "";
	 if (sessionmaint.isValidSession(tSession))
	{
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
		//String mode = request.getParameter("mode");
		studyId = request.getParameter("studyId");
		milestoneType = request.getParameter("milestoneType");

		if (StringUtil.isEmpty(milestoneType))
		{
			milestoneType = "PM";
		}

	}
	%>



<%
int totalrows = 5;
String patCount="";

String dCur = "";
String amount="";

CodeDao codeLst = new CodeDao();
CodeDao codeLstStatus = new CodeDao();
CodeDao codeLstPayType = new CodeDao();
CodeDao codeLstPayFor = new CodeDao();
CodeDao codeMsStat = new CodeDao();

codeLst.getCodeValues("PM");


	String studyCurrency = "";

	ArrayList curr = new ArrayList();
	String currency = "";
	String acc = (String) tSession.getValue("accountId");
	int accId = 0;
	String dStatus = "";
	String dpayType = "";
	String dpayFor = "";
	String statusHeader = "";


	studyBN.setId(EJBUtil.stringToNum(studyId));
   	studyBN.getStudyDetails();
   	studyCurrency = studyBN.getStudyCurrency();

	SchCodeDao cdDesc = new SchCodeDao();
	cdDesc.getCodeValuesById(EJBUtil.stringToNum(studyCurrency));
	curr =  cdDesc.getCSubType();

	if (curr.size() > 0) {
		currency=(((curr.get(0)) ==null)?"-":(curr.get(0)).toString());

	}
	if (StringUtil.isEmpty(currency))
		{
			currency = "$";
		}

	// get patient status dropdown (for the account)

		accId = EJBUtil.stringToNum(acc);

		if (milestoneType.equals("PM"))
		{
			codeLstStatus.getCodeValues("patStatus",accId);
			statusHeader = LC.L_Patient_Status/*LC.Pat_Patient+" Status"*****/;

		}
		else
		{
			codeLstStatus.getCodeValues("studystat");
			statusHeader = LC.L_Study_Status/*LC.Std_Study+ " " + "Status"*****/;
			if (milestoneType.equals("AM")) //KM
				statusHeader = LC.L_Additional/*"Additional"*****/;

		}

		dStatus = codeLstStatus.toPullDown("dStatus");


		//JM: 30Dec2009: #3357
		codeLstPayType.getCodeValues("milepaytype");

	    //retrieve 'Receivable' by default
		int default_sel = codeLstPayType.getCodeId("milepaytype", "rec");

		dpayType = EJBUtil.createPullDownWithStrNoSelectIfHidden("dpayType",""+default_sel,codeLstPayType.getCId(),codeLstPayType.getCDesc(), codeLstPayType.getCodeHide());

		codeLstPayFor.getCodeValues("milePayfor");
		dpayFor = codeLstPayFor.toPullDown("dpayFor");



%>
<P class="sectionHeadings"><%Object[] arguments1 = {statusHeader}; %><%=VelosResourceBundle.getLabelString("L_Mstone_Mstone",arguments1)%><%--Milestones >> <%=statusHeader%> Milestones*****--%></P>
<Form name="PatientStatus" id="patstatmilefrm" action="updatepatstatmilestone.jsp" method="post" onSubmit="ripLocaleFromAll(); if (setVals('Submit',document.PatientStatus)== false) { setValidateFlag('false'); applyLocaleToAll(); return false; } else { setValidateFlag('true'); return true;}" >

<table  width="100%" border="0">
<tr>
<%
if (milestoneType.equals("PM"))
{%>
	<th class="headernoColor"  width=10% ><%=LC.L_Pat_Count%><%--<%=LC.Pat_Patient%> Count*****--%><FONT class="Mandatory">* </FONT></th>

<% } %>

	<% if (milestoneType.equals("AM")){	%>
	<th class="headernoColor"  width=15% > <%=LC.L_Description%><%--Description*****--%> <FONT class="Mandatory">* </FONT></th>
	<%} else {%>
	<th class="headernoColor"  width=15% ><%=statusHeader%> <FONT class="Mandatory">* </FONT></th>
	<%}Object[] arguments = {currency};%>
	<th class="headernoColor" width=15% ><%=VelosResourceBundle.getLabelString("L_Amount_In",arguments)%><%--Amount (in <%=currency%>)*****--%></th>
	<% if (!milestoneType.equals("AM")){	%>
	<th class="headernoColor" width=5% ><%=LC.L_Limit%><%--Limit*****--%></th>
	<%}%>
	<th class="headernoColor" width=5% ><%=LC.L_Payment_Type%><%--Payment Type*****--%></th>
	<th class="headernoColor" width=15% ><%=LC.L_Payment_For%><%--Payment For*****--%></th>
	<th class="headernoColor" width=15% ><%=LC.L_Mstone_Status%><%--Milestone Status*****--%> <FONT class="Mandatory">* </FONT></th>
	
	<%-- Fin-22373 Date: 26-Sep-2012 Ankit --%>
	<%if(!milestoneType.equals("AM")) {%>
	<th class="headernoColor" width=15% ><%=LC.L_Date_From%><%--Date From*****--%> </th>
	<th class="headernoColor" width=15% ><%=LC.L_Date_To%><%--Date To*****--%> </th>
	<%}%>
</tr>

<tr>
<%
if (milestoneType.equals("PM"))
{%>
	<td><hr class="thinLine"></td>
<% } %>

	<td><hr class="thinLine"></td>
	<td><hr class="thinLine"></td>
	<td><hr class="thinLine"></td>
	<td><hr class="thinLine"></td>
	<td><hr class="thinLine"></td>
	<td><hr class="thinLine"></td>
	<%if(!milestoneType.equals("AM")) {%>
	<td><hr class="thinLine"></td>
	<td><hr class="thinLine"></td>
	<%}%>
</tr>
<%

		//KM-D-FIN7
		String userIdFromSession = (String) tSession.getValue("userId");
		String roleCodePk="";

	    if (EJBUtil.stringToNum(studyId) > 0)
		{
			ArrayList tId = new ArrayList();

			TeamDao teamRoleDao = new TeamDao();
			teamRoleDao.getTeamRights(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(userIdFromSession));
			tId = teamRoleDao.getTeamIds();

			if (tId != null && tId.size() > 0)
			{
				ArrayList arRoles = new ArrayList();
				arRoles = teamRoleDao.getTeamRoleIds();

				if (arRoles != null && arRoles.size() >0 )
				{
					roleCodePk = (String) arRoles.get(0);

					if (StringUtil.isEmpty(roleCodePk))
					{
						roleCodePk="";
					}
				}
				else
				{
					roleCodePk ="";
				}

			}
			else
			{
				roleCodePk ="";
			}
		} else
			{
			  roleCodePk ="";
			}

		codeMsStat.getCodeValuesForStudyRole("milestone_stat",roleCodePk);


for(int i=0;i<totalrows;i++){
 %>

 <tr>

	 <%
		if (milestoneType.equals("PM"))
		{%>
	<td align=center>
		<input type=text name=count size=5 maxlength=10 class="leftAlign numberfield" data-unitsymbol="" data-formatas="number" value=<%=patCount%>>
	</td>
	  <% }
	  else
	  {
	  %>
	  		<input type=hidden name=count size=5 maxlength=10 class="leftAlign">
	  <%
	  } %>

	<td >
	<%

	//Modified by Manimaran for the Enh.#FIN10

	if(milestoneType.equals("AM")) {%>
	<input type="text" name="description" size="45" value="" maxlength=4000> <!-- KM -->
	<%}else{%>
	<%=dStatus%>
	<%}%>
	</td>

	<td align=center>
	<INPUT NAME="amount" TYPE="hidden" value="<%=amount%>" SIZE=20>
	<input type=text name=amountNum size=16 maxlength=16 class="leftAlign numberfield" data-unitsymbol="" data-formatas="currency">
     <!--  <B>.</B>
	<input type=text name=amountFrac size=2 maxlength=2  class="leftAlign" > -->
	 </td>
	<%if(!milestoneType.equals("AM")) {%>
	<td><input type=text name="mLimit" size=3 maxlength=10  class="leftAlign numberfield" data-unitsymbol="" data-formatas="number" ></td>
	<%}%>
		<td >
	<%=dpayType%>
	</td>
	<!--<td align=center>
		<input type=text name="paymentDueIn" size=5 maxlength=10 class="leftAlign" >
		<Select name="paymentDueUnit">
			<option value="D" SELECTED>Day(s)</option>
			<option value="W" >Week(s)</option>
			<option value="M" >Month(s)</option>
			<option value="Y" >Year(s)</option>
		</Select>
	</td> -->
		<td >
	<%=dpayFor%>
	</td>

<%
//KM-01Jun10-#4962
String msStat = codeMsStat.toPullDown("milestoneStat");
%>
	<td align = center> <%=msStat%> </td>
	
	<%if(!milestoneType.equals("AM")) {%>
		<td align = center><input type="text" name="dateFrom" class="datefield" size ="12" MAXLENGTH ="8" value='' ></td>
		<td align = center><input type="text" name="dateTo" class="datefield" size ="12" MAXLENGTH ="8" value='' ></td>
	<%}%>

</tr>


 <%
}
%>

<INPUT NAME="mode" TYPE=hidden  value=<%=mode%>>
<INPUT NAME="studyId" TYPE=hidden  value=<%=studyId%>>
<INPUT NAME="milestoneType" TYPE=hidden  value=<%=milestoneType%>>
</TABLE>
<br>
<table width="100%" cellspacing="0" cellpadding="0">
	<tr bgcolor="<%=StringUtil.eSignBgcolor%>" align="center">
		<td align="center">
			<A type="submit" href="#" onClick="return setParam('Add',document.PatientStatus);"><%=LC.L_Submit_AddAnother%></A>
		</td>
		<td bgcolor="<%=StringUtil.eSignBgcolor%>" align="center">
			<jsp:include page="submitBar.jsp" flush="true">
				<jsp:param name="displayESign" value="N"/>
				<jsp:param name="formID" value="patstatmilefrm"/>
				<jsp:param name="showDiscard" value="N"/>
				<jsp:param name="noBR" value="Y"/>
			</jsp:include>
		</td>
	</tr>
</table>

   <input type="hidden" name="subVal" value="Sumbit">
</Form>

</DIV>

<div class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</BODY>


