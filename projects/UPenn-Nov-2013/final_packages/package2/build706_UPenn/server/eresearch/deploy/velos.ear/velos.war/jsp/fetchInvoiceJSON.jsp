<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page	import="com.velos.eres.service.util.*,java.util.ArrayList,java.util.HashMap,org.json.*,com.velos.eres.service.util.EJBUtil,com.velos.esch.business.common.*,com.velos.eres.service.util.StringUtil,
com.velos.eres.business.common.MileAchievedDao, com.velos.eres.business.common.MilestoneDao,com.velos.eres.service.util.EJBUtil"%>
<%@page import="java.util.Hashtable,org.json.JSONArray"%>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
<%@page import="java.util.Collections"%>

<%@page import="com.velos.eres.business.common.PatientDao"%><jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint" />
<jsp:useBean id="InvB" scope="request" class="com.velos.eres.web.invoice.InvoiceJB"/>
<%
	request.setCharacterEncoding("UTF-8");
	response.setContentType("application/json");
	HttpSession tSession = request.getSession(true);

	JSONObject jsObj = new JSONObject();
	if (!sessionmaint.isValidSession(tSession)) {
		response.sendError(HttpServletResponse.SC_FORBIDDEN,
				"SESSION_TIME_OUT");

		jsObj.put("error", new Integer(-1));
		jsObj.put("errorMsg", MC.M_UsrNot_LoggedIn/*User is not logged in.*****/);
		out.println(jsObj.toString());
		return;
	}
	//Validate the study ID
	String studyId = request.getParameter("studyId");

	if (studyId == "" || studyId == null
			|| studyId.equals("null") || studyId.equals("")) {
		// A valid study ID is required; print an error and exit
		jsObj.put("error", new Integer(-2));
		jsObj.put("errorMsg", MC.M_PcolId_Invalid/*Study ID is invalid.*****/);
		out.println(jsObj.toString());
		return;
	}
	
	SchCodeDao schDao = new SchCodeDao();
	schDao.getCodeValues("coverage_type");
	ArrayList covSubTypeList = schDao.getCSubType();
	ArrayList covDescList = schDao.getCDesc();
	ArrayList<String> covCodeHide=schDao.getCodeHide();
	StringBuffer sbHeader = new StringBuffer();
	StringBuffer sbData = new StringBuffer();
	for (int iX=0; iX<covSubTypeList.size(); iX++) {
		String covSub1 = (String)covSubTypeList.get(iX);
	    String covDesc1 = (String)covDescList.get(iX);
	    String covIsHidden = covCodeHide.get(iX);
	    if(StringUtil.isEmpty(covIsHidden))covIsHidden="N";
	    
		if (covSub1 == null || covDesc1 == null) { continue; }
	    covSub1 = covSub1.replaceAll("'", "\\\\'");
	    covDesc1 = covDesc1.replaceAll("'", "\\\\'");
	  	//sbHeader.append("<b>").append(covSub1).append("</b>").append("=").append(covDesc1).append("<br/>");
		if(covIsHidden.equalsIgnoreCase("Y")){
			sbHeader.append("<b>").append(covSub1).append("</b>").append("=").append(covDesc1).append(" ["+LC.L_Hidden+"]").append("<br/>");
		}else{
			sbHeader.append("<b>").append(covSub1).append("</b>").append("=").append(covDesc1).append("<br/>");
		}
		
		covSub1 = (String)covSubTypeList.get(iX);
	    covDesc1 = (String)covDescList.get(iX);
	    
	    covSub1 = covSub1.replaceAll("'", "\\\'");
	    covDesc1 = covDesc1.replaceAll("'", "\\\'");
	  //sbData.append("<b>").append(covSub1).append("</b>").append("=").append(covDesc1).append("<br/>");
	    if(covIsHidden.equalsIgnoreCase("Y")){
	    	sbData.append("<b>").append(covSub1).append("</b>").append("=").append(covDesc1).append(" ["+LC.L_Hidden+"]").append("<br/>");
	    }else{
		sbData.append("<b>").append(covSub1).append("</b>").append("=").append(covDesc1).append("<br/>");
	    }
		
	}
	String covTypeLegend = sbData.toString();
	jsObj.put("covTypeLegend", covTypeLegend);
	
	covTypeLegend = sbHeader.toString();

	String milestoneReceivableStatus = request.getParameter("milestoneReceivableStatus");
	String dateRangeType = request.getParameter("dateRangeType");
	String dtFilterDateFrom = request.getParameter("dtFilterDateFrom");
	String dtFilterDateTo = request.getParameter("dtFilterDateTo");
	String dSites = request.getParameter("dSites");
	String coverType = request.getParameter("coverType");
	String yearFilter= request.getParameter("yearFilter");
	String monthFilterYear= request.getParameter("monthFilterYear");
	String monthFilter= request.getParameter("monthFilter");
	if (StringUtil.isEmpty(coverType) || "null".equals(coverType)){
		coverType = null;
	}

	ArrayList ddArMileAmount = new ArrayList();
	ArrayList ddArPrevInvoicedAmounts = new ArrayList();
	
	//List of Datatable data
	ArrayList dataList = new ArrayList();

	String[] arMilestoneType =  new String[5];
	arMilestoneType[0] = "EM";
	arMilestoneType[1] = "VM";
	arMilestoneType[2] = "PM";
	arMilestoneType[3] = "SM";
	arMilestoneType[4] = "AM";
	
	String[] arMilestoneTypeHeader =  new String[5];
	arMilestoneTypeHeader[0] = LC.L_Event;/*Event*/
	arMilestoneTypeHeader[1] = LC.L_Visit;/*Visit*/
	arMilestoneTypeHeader[2] = LC.L_Patient_Status;/*Patient Status*/	
	arMilestoneTypeHeader[3] = LC.L_Study_Status;/*Study Status*/
	arMilestoneTypeHeader[4] = LC.L_Additional;/*Additional*/

	String displayMilestoneHeader ="";
	//double totalMileAmount =0.0, totalPrevInvoicedAmount=0.0, totalInvoiceAmount=0.0;
	
	PatientDao pdao = new PatientDao();
	int minPHIRight = 0;
	String maskValues="****";
	String userId = (String)tSession.getAttribute("userId");
	userB.setUserId(EJBUtil.stringToNum(userId));

	userB.getUserDetails();

	String defGroup = userB.getUserGrpDefault();
	
	int grpId=EJBUtil.stringToNum(defGroup);
	
		for (int t = 0; t <arMilestoneType.length;t++){
	
		MileAchievedDao mileDao = new MileAchievedDao();
 		if(arMilestoneType[t].equalsIgnoreCase("EM")){
 			mileDao = InvB.getAchievedMilestonesEM(studyId,yearFilter,monthFilterYear,monthFilter, dtFilterDateFrom,dtFilterDateTo, milestoneReceivableStatus,dSites, coverType,dateRangeType);
 		}else{
 			if (arMilestoneType[t].equalsIgnoreCase("AM")){
 	 			mileDao = InvB.getAchievedMilestonesAM(studyId,yearFilter,monthFilterYear,monthFilter, dtFilterDateFrom,dtFilterDateTo, milestoneReceivableStatus,dSites, coverType,dateRangeType);
 			}else{	
 				mileDao = InvB.getAchievedMilestonesNew(studyId,yearFilter,monthFilterYear,monthFilter, dtFilterDateFrom,dtFilterDateTo, milestoneReceivableStatus,arMilestoneType[t],dSites,dateRangeType);
 			}
 		}
		
		MilestoneDao milestoneDao = new MilestoneDao();
		milestoneDao = mileDao.getMilestoneDao();
		ArrayList arMilestoneIds = new ArrayList();
		arMilestoneIds = milestoneDao.getMilestoneIds();
		ArrayList arMilestoneRuleDescs = new  ArrayList();
		arMilestoneRuleDescs = milestoneDao.getMilestoneRuleDescs();
		ArrayList arMilestoneAchievedCounts = new ArrayList();
		arMilestoneAchievedCounts = milestoneDao.getMilestoneAchievedCounts();
		ArrayList arCountPerMilestone = new ArrayList();
		arCountPerMilestone= milestoneDao.getMilestoneCounts();
		ArrayList arMileAmounts = new ArrayList();
		arMileAmounts =  milestoneDao.getMilestoneAmounts();
		ArrayList arMileTotalInvoicedAmounts = new ArrayList();
		arMileTotalInvoicedAmounts = milestoneDao.getTotalInvoicedAmounts();
		ArrayList milestoneHoldbacks = new ArrayList();
		milestoneHoldbacks = milestoneDao.getHoldBack();
		int milecount=0;
	
		if (arMilestoneIds != null)
		{
			milecount = arMilestoneIds.size();
		}	
		int totalmilecount = milecount;
		
		String mileDesc="", mileStoneId="", mileAchCount="";
		int countPerMilestone =0, mileDetailCount=0;
		double mileAmount = 0.00, mileAmountForGroupMilestones=0.0, tempMileAmtForGrpMilestones=0.0, calcAmount=0.0, holdback=0.00,
		totalMilePrevInvoicedAmount=0.0, mileDetailAmountsPrevInvoiced = 0.00;
	
		// for each milestone, get the milestone achieved details
		for (int i =0; i < milecount ; i++){
			mileDesc = (String ) arMilestoneRuleDescs.get(i);
			mileStoneId = String.valueOf(((Integer)arMilestoneIds.get(i)).intValue());
	
			MileAchievedDao ach = new MileAchievedDao();
			ArrayList patients = new ArrayList();
			ArrayList ids = new ArrayList();
			ArrayList patientCodes = null;
			ArrayList patStudyIds = null;
			ArrayList calendars = null;
			ArrayList visits = null;
			ArrayList paymentTypes = new ArrayList();
			ArrayList paymentFors = new ArrayList();
			ArrayList coverTypes = null;
			ArrayList achievedOn = new ArrayList();
			ArrayList arMileDetailAmountsPrevInvoiced = new ArrayList();
			ArrayList enrollingSite = new ArrayList();
			ArrayList patFirstName = new ArrayList();
			ArrayList patLastName = new ArrayList();
	
			countPerMilestone =  EJBUtil.stringToNum((String) arCountPerMilestone.get(i));
			mileAchCount = (String) arMilestoneAchievedCounts.get(i);
			mileAmount = Double.parseDouble((String) arMileAmounts.get(i));
			if(milestoneHoldbacks.get(i)==null){
				holdback = 0.00;
			}
			else{
			holdback = (Float) milestoneHoldbacks.get(i);
			//holdback = Double.parseDouble(""+NumberUtil.roundOffNo(holdback));	
			}
			
			if (!ddArMileAmount.contains(""+mileAmount)){
				ddArMileAmount.add(""+mileAmount);
			}
			//totalMileAmount += mileAmount;
			
			Hashtable htAchieved = new Hashtable();
			htAchieved = mileDao.getHtAchieved();
			totalMilePrevInvoicedAmount = 0;
			if (countPerMilestone > 1)
			{
		 		mileAmountForGroupMilestones = mileAmount/countPerMilestone;
		 	}else{
		 		mileAmountForGroupMilestones = mileAmount;
		 	}
			tempMileAmtForGrpMilestones = mileAmountForGroupMilestones;
			//mileAmountForGroupMilestones = Double.parseDouble(""+NumberUtil.roundOffNo(mileAmountForGroupMilestones));					
	
			if (htAchieved.containsKey(mileStoneId)){
				ach = (MileAchievedDao) htAchieved.get(mileStoneId);
				ids = ach.getId();
				patients = ach.getPatient();
				mileDetailCount = ids.size();
				patientCodes = ach.getPatientCode();
				patStudyIds = ach.getPatientStudyIds();
				calendars = ach.getCalDescs();
				visits = ach.getVisitNames();
				paymentTypes = ach.getMilestonePaymentTypes();
				coverTypes = ach.getCoverAnalysis();
				paymentFors = ach.getMilestonePaymentFors();
				calcAmount = EJBUtil.stringToNum(mileAchCount) * mileAmount;
				achievedOn = ach.getAchievedOn();
				enrollingSite = ach.getEnrollingSite();
				patFirstName = ach.getPatFirstName();
				patLastName = ach.getPatLastName();
				//arMileDetailAmountsPrevInvoiced = ach.getAmountInvoiced();
				//System.out.println("PSINGH>>>>>>>>>>>>>>1111 "+ach.getAmountInvoiced()+" "+ach.getAmountHoldback());
			} else {
				mileAchCount = "0";
				mileDetailCount = 0;
				calcAmount = mileAmount;
			}
			
			
			String strTotalPrevInvAmount = "";
			strTotalPrevInvAmount = (String)arMileTotalInvoicedAmounts.get(i);
			
			if (! StringUtil.isEmpty(strTotalPrevInvAmount)){
				 totalMilePrevInvoicedAmount = Double.parseDouble(strTotalPrevInvAmount);
			} else {
				totalMilePrevInvoicedAmount = 0;
			}
			
			totalMilePrevInvoicedAmount = Math.rint(totalMilePrevInvoicedAmount * 100)/100;
			//totalPrevInvoicedAmount += totalMilePrevInvoicedAmount;
			
			for (int k = 0; k < mileDetailCount; k++){
				int count=1;
				if(holdback>0 && mileAmountForGroupMilestones>0){
				count=2; 
				}
			for(int rows=0;rows<count;rows++){
				mileAmountForGroupMilestones=tempMileAmtForGrpMilestones;
				if (htAchieved.containsKey(mileStoneId)){
					if(rows==1)
					{
						arMileDetailAmountsPrevInvoiced = ach.getAmountHoldback();
					}
					else
					{
						arMileDetailAmountsPrevInvoiced = ach.getAmountInvoiced();
					}
				}
				mileDetailAmountsPrevInvoiced = Double.parseDouble((String) arMileDetailAmountsPrevInvoiced.get(k));
				mileDetailAmountsPrevInvoiced = Double.parseDouble(""+NumberUtil.roundOffNo(mileDetailAmountsPrevInvoiced));
				
				if (!ddArPrevInvoicedAmounts.contains(""+mileDetailAmountsPrevInvoiced)){
					ddArPrevInvoicedAmounts.add(""+mileDetailAmountsPrevInvoiced);
				}
				
				int mileAchieveId = StringUtil.stringToNum(""+ids.get(k));

				HashMap visitMap = new HashMap();
				visitMap.put("milestoneId",mileStoneId);
				visitMap.put("mileAchieveId",""+mileAchieveId);
				visitMap.put("patientID",""+patients.get(k));
				String patientPK = (String)patients.get(k);
				if(rows==1){
					visitMap.put("mileType",LC.L_Holdback_Amt+"- "+arMilestoneTypeHeader[t]);
					mileAmountForGroupMilestones=mileAmountForGroupMilestones*(holdback/100);
					mileAmountForGroupMilestones = Double.parseDouble(""+NumberUtil.roundOffNo(mileAmountForGroupMilestones));
				}
				else{
					visitMap.put("mileType",arMilestoneTypeHeader[t]);
					mileAmountForGroupMilestones=mileAmountForGroupMilestones*(1-holdback/100);
					mileAmountForGroupMilestones = Double.parseDouble(""+NumberUtil.roundOffNo(mileAmountForGroupMilestones));
				}
				minPHIRight =  pdao.getViewPatientPHIRight(EJBUtil.stringToNum(userId),grpId,EJBUtil.stringToNum(patientPK));
				
				String enrollingSit = (String)enrollingSite.get(k);
				if (StringUtil.isEmpty(enrollingSit)){
					visitMap.put("enrollSite", "-");
				}else{
					visitMap.put("enrollSite", enrollingSite.get(k));
				}
				
				
				String patFirstNam = (String)patFirstName.get(k);
				
					if (StringUtil.isEmpty(patFirstNam)){
						visitMap.put("patientFirstName", "-");
					}else{
						if(minPHIRight>=4){
							visitMap.put("patientFirstName", patFirstName.get(k));
						}else{
							visitMap.put("patientFirstName", maskValues);
						}
					}
				
				
					String patLastNam = (String)patLastName.get(k);
					if (StringUtil.isEmpty(patLastNam)){
						visitMap.put("patientLastName", "-");
					}else{
						if(minPHIRight>=4){
								visitMap.put("patientLastName", patLastName.get(k));
						}else{
							visitMap.put("patientLastName", maskValues);
						}
					}
					if (null == patientCodes)
						visitMap.put("patientCode", "-");
					else{
						String patientCode = (String)patientCodes.get(k);
						if (StringUtil.isEmpty(patientCode)){
							visitMap.put("patientCode", "-");
						}
						else{
							if(minPHIRight>=4){
										visitMap.put("patientCode", patientCodes.get(k));
							}else{
								visitMap.put("patientCode", maskValues);
							}
						}
					}
				
				if (null == patStudyIds)
					visitMap.put("patStudyId", "-");
				else
					visitMap.put("patStudyId", patStudyIds.get(k));
				
				if (null == calendars)
					visitMap.put("calendar", "-");
				else
					visitMap.put("calendar", calendars.get(k));
				
				if (null == visits)
					visitMap.put("visit", "-");
				else
					visitMap.put("visit", visits.get(k));
				
				visitMap.put("paymentType", paymentTypes.get(k));
				visitMap.put("paymentFor", paymentFors.get(k));

				String cT = (String)coverTypes.get(k);
				if (null != cT && !"-".equals(cT)){
					cT = "<a href=\"javascript:void(0)\">"+cT+"</a>";
				}
				visitMap.put("coverageType",cT);
				visitMap.put("holdback",holdback);
				visitMap.put("descService", mileDesc + "<br>"+ LC.L_Amount +": "+mileAmount);
				
				if (-1 <= countPerMilestone && countPerMilestone <= 1){
					visitMap.put("quantity", "1");					
				}else{
					visitMap.put("quantity", "1/"+countPerMilestone);
				}

				visitMap.put("prevInvoicedAmount", mileDetailAmountsPrevInvoiced);
				visitMap.put("mileAmount", mileAmountForGroupMilestones);
				
				
				String calcImage = "<div align='center'><img src='../images/jpg/Budget.gif' title='"+LC.L_Calculate+"' border='0'"
					+ " name='calculate_e"+mileAchieveId+"' id='calculate_e"+mileAchieveId+"'/></div>";
				visitMap.put("calculate", calcImage);
				
				String eraseImage = "<img src='../images/jpg//Erase.gif' title='"+LC.L_Erase+"' border='0'>";
				visitMap.put("erase", eraseImage);
				dataList.add(visitMap);
				}
			}
		}	// for milestone types
	}
	
	//End Data here
	JSONArray jsMileAchieveData = new JSONArray();
	for (int iX = 0; iX < dataList.size(); iX++) {
		jsMileAchieveData.put(EJBUtil.hashMapToJSON((HashMap) dataList.get(iX)));
	}
	
	/*jsObj.put("totalMileAmount",""+NumberUtil.roundOffNo(totalMileAmount));
	jsObj.put("totalPrevInvoicedAmount",NumberUtil.roundOffNo(totalPrevInvoicedAmount));
	jsObj.put("totalInvoiceAmount",NumberUtil.roundOffNo(totalInvoiceAmount));*/

	String ddStr = "";
	ArrayList fieldsList = new ArrayList();
	JSONArray jsColArray = new JSONArray();
	{
		JSONObject jsObjTemp1 = new JSONObject();
		jsObjTemp1.put("key", "milestoneId");
		jsObjTemp1.put("label","Milestone ID");
		jsObjTemp1.put("hidden","true");
		fieldsList.add("milestoneId");
		jsColArray.put(jsObjTemp1);	
		
		jsObjTemp1 = new JSONObject();
		jsObjTemp1.put("key", "mileAchieveId");
		jsObjTemp1.put("label","Milestone Achievement ID");
		jsObjTemp1.put("hidden","true");
		fieldsList.add("mileAchieveId");
		jsColArray.put(jsObjTemp1);	
		jsObjTemp1 = new JSONObject();
		jsObjTemp1.put("key", "patientID");
		jsObjTemp1.put("label","patients ID");
		jsObjTemp1.put("hidden","true");
		fieldsList.add("patientID");
		jsColArray.put(jsObjTemp1);	
		
		jsObjTemp1 = new JSONObject();
		jsObjTemp1.put("key", "mileType");
		jsObjTemp1.put("label",LC.L_Milestone_Type);
		fieldsList.add("mileType");
		jsColArray.put(jsObjTemp1);
		
		jsObjTemp1 = new JSONObject();
		jsObjTemp1.put("key", "enrollSite");
		jsObjTemp1.put("label",LC.L_Enrolling_Site);
		fieldsList.add("enrollSite");
		jsColArray.put(jsObjTemp1);
		
		jsObjTemp1 = new JSONObject();
		jsObjTemp1.put("key", "patientCode");
		jsObjTemp1.put("label","<a href=\"javascript:void(0)\""
				+" onmouseover=\"return overlib('"+LC.L_pat_Or_Stat_Date+"',CAPTION,'');\""
				+" onmouseout=\"return nd();\">"+LC.L_more_period+"</a>");
		fieldsList.add("patientCode");
		jsColArray.put(jsObjTemp1);
		
		jsObjTemp1 = new JSONObject();
		jsObjTemp1.put("key", "patientFirstName");
		jsObjTemp1.put("label",LC.L_Patient_FirstName);
		fieldsList.add("patientFirstName");
		jsColArray.put(jsObjTemp1);
		
		jsObjTemp1 = new JSONObject();
		jsObjTemp1.put("key", "patientLastName");
		jsObjTemp1.put("label",LC.L_Patient_LastName);
		fieldsList.add("patientLastName");
		jsColArray.put(jsObjTemp1);
		
		jsObjTemp1 = new JSONObject();
		jsObjTemp1.put("key", "patStudyId");
		jsObjTemp1.put("label",LC.L_Patient_StudyId);
		fieldsList.add("patientCode");
		jsColArray.put(jsObjTemp1);
		
		jsObjTemp1 = new JSONObject();
		jsObjTemp1.put("key", "calendar");
		jsObjTemp1.put("label",LC.L_Cal_Name);
		fieldsList.add("calendar");
		jsColArray.put(jsObjTemp1);
		
		jsObjTemp1 = new JSONObject();
		jsObjTemp1.put("key", "visit");
		jsObjTemp1.put("label",LC.L_Visit_Name);
		fieldsList.add("visit");
		jsColArray.put(jsObjTemp1);

		jsObjTemp1 = new JSONObject();
		jsObjTemp1.put("key", "paymentType");
		jsObjTemp1.put("label",LC.L_Payment_Type);
		fieldsList.add("paymentType");
		jsColArray.put(jsObjTemp1);
		
		jsObjTemp1 = new JSONObject();
		jsObjTemp1.put("key", "paymentFor");
		jsObjTemp1.put("label",LC.L_Payment_For);
		fieldsList.add("paymentFor");
		jsColArray.put(jsObjTemp1);
		
		jsObjTemp1 = new JSONObject();
		jsObjTemp1.put("key", "coverageType");
		jsObjTemp1.put("label","<a href=\"javascript:void(0)\""
			+" onmouseover=\"return overlib('"+covTypeLegend+"',CAPTION,'"+LC.L_Coverage_TypeLegend+"');\""
			+" onmouseout=\"return nd();\">"+LC.L_Coverage_Analysis+"</a>");
		fieldsList.add("coverageType");
		jsColArray.put(jsObjTemp1);
		
		jsObjTemp1 = new JSONObject();
		jsObjTemp1.put("key", "holdback");
		jsObjTemp1.put("label",LC.L_Holdback_Amt);
		fieldsList.add("holdback");
		jsColArray.put(jsObjTemp1);
		
		jsObjTemp1 = new JSONObject();
		jsObjTemp1.put("key", "descService");
		jsObjTemp1.put("label",LC.L_Desc_of_Serv);
		fieldsList.add("descService");
		jsColArray.put(jsObjTemp1);
		
		jsObjTemp1 = new JSONObject();
	   	jsObjTemp1.put("key", "quantity");
    	jsObjTemp1.put("label", LC.L_Quantity);
    	fieldsList.add("quantity");
    	jsColArray.put(jsObjTemp1); 

    	jsObjTemp1 = new JSONObject();
	   	jsObjTemp1.put("key", "mileAmount");
    	jsObjTemp1.put("label", LC.L_Mstone_Amt);
		jsObjTemp1.put("type", "number");
		fieldsList.add("mileAmount");
    	jsColArray.put(jsObjTemp1);
		
    	jsObjTemp1 = new JSONObject();
		jsObjTemp1.put("key", "prevInvoicedAmount");
		jsObjTemp1.put("label",LC.L_InvoicedToDate);
		jsObjTemp1.put("type", "number");
		fieldsList.add("prevInvoicedAmount");
		jsColArray.put(jsObjTemp1);
		
    	jsObjTemp1 = new JSONObject();
		jsObjTemp1.put("key", "invoiceAmount");
		jsObjTemp1.put("label",	LC.L_Inv_Amt);
		jsObjTemp1.put("type",	"number");
		fieldsList.add("invoiceAmount");
		jsColArray.put(jsObjTemp1);

		jsObjTemp1 = new JSONObject();
	   	jsObjTemp1.put("key", "calculate");
    	jsObjTemp1.put("label", "<img src='../images/jpg/Budget.gif' title='"+ LC.L_Calculate_All +"' border='0' name='calculateAll' id='calculateAll' onclick='return VELOS.generateInvoiceGrid.calculateAll();'/> ");
    	fieldsList.add("calculate");
    	jsObjTemp1.put("action", "calculate");
    	jsColArray.put(jsObjTemp1);
    	
		jsObjTemp1 = new JSONObject();
	   	jsObjTemp1.put("key", "erase");
    	jsObjTemp1.put("label", "<img src='../images/jpg/Erase.gif' title='"+ LC.L_Erase_All +"' border='0' name='eraseAll' id='eraseAll' onclick='return VELOS.generateInvoiceGrid.eraseAll();'/> ");
    	fieldsList.add("erase");
    	jsObjTemp1.put("action", "erase");
    	jsColArray.put(jsObjTemp1);
	}
	
	jsObj.put("dataArray", jsMileAchieveData);
	jsObj.put("colArray", jsColArray);

	out.println(jsObj.toString());
	
%>

