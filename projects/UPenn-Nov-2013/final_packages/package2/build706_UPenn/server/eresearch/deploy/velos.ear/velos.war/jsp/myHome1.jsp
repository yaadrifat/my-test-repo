<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
	<HTML>
	<HEAD>
	<Link Rel=STYLESHEET HREF="common.css" type=text/css>
	</HEAD>
	
	<jsp:include page="myhomepanel.jsp" flush="true"/>   
	
	<SCRIPT>
	function openuserwin(userId) {
	      window.open("viewuser.jsp?userId=" + userId,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=500,height=400")
	;}
	
	function opentextwin(msgText) {
	    
	      window.open("viewtext.jsp?msgtext=" + msgText,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=500,height=400")
	;}
	
	</SCRIPT> 
	
	
	<BODY>
	<jsp:useBean id="msgcntr" scope="session" class="com.velos.eres.web.msgcntr.MsgcntrJB"/>
	<jsp:useBean id="grpRights" scope="session" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	
	<%@ page language = "java" import = "com.velos.eres.business.study.*,com.velos.eres.web.user.UserJB,com.velos.eres.business.common.*,com.velos.eres.service.util.*,java.util.*,com.velos.eres.business.ulink.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.business.common.MsgCntrDao"%>
	
	<DIV class="formDefault" id="div1" style="border-style:none">
	
	<%HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession))
	{
	
	 UserJB user = (UserJB) tSession.getValue("currentUser");
	 int usrId=0;   
	 usrId = user.getUserId();
	 
	 String uName = (String) tSession.getValue("userName");
	
	%>
	
	<div id = "myName">
	<P><%= uName %></p>
	</div>
	
	<%grpRights.setId(Integer.parseInt(user.getUserGrpDefault()));%>
	<%grpRights.getGrpRightsDetails();%>
	
	<P class="sectionHeadings"> <%=MC.M_MyEres_Hpage%><%--My eResearch >> HomePage*****--%></P> <BR>
	
	<div id = "myStudies" style="border-style:none">
	<br>
	<br>
	<P class="defComments"><i><%=MC.M_HaveAccsFlw_StdPcols%><%--You have access to following Study Protocols*****--%>:</i></p>
	
	<%! UserStudiesStateKeeper usk = null;
		ArrayList title;ArrayList num;ArrayList study; StringBuffer studyList;
		int counter = 0; int len = 0;String pagename = "study.jsp";
	%>
	
	<%
		title = new ArrayList();
		study = new ArrayList();
		num = new ArrayList();
		studyList = new StringBuffer();
	
		user.setUserId(usrId);
		usk = user.getUserStudies();
		
		title = usk.getUTitles();
		study = usk.getUStudies();
		num = usk.getUStudyNumbers();
		len = study.size();
		
	
		studyList.append("<Table border = 0>");
	
		for(counter = 0;counter<len;counter++)
		{
			studyList.append("<TR><TD>");	 
			//studyList.append("<A HREF = "+ pagename + "?id=" + study.get(counter) +"> [" + num.get(counter) + "] "+title.get(counter) +"</A>");	 
			studyList.append("<A HREF = "+ pagename + "?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&studyId=" + study.get(counter) +"> [" + num.get(counter) + "] "+title.get(counter) +"</A>");	 
			studyList.append("</TR></TD>");	 
		}			
		studyList.append("</Table>");	
		out.print(studyList.toString());	
		if (len <=0 )
		{
		%>
		
		<P class="blackComments"><%=MC.M_DoNotHaveAces_ToStd%><%--You do not have access to any Study*****--%> </P>
		
		<%
		}
	%>
	
	<% java.util.ArrayList ids=msgcntr.getMsgIds(); %>
	
	<% String userName="";
	   String studyName = "";
	   String studyId="";	
	   String userX="";
	   String reqType="";
	   String permission="";
	   String msgcntrId="";	
	   String pageStatus=request.getParameter("type");
	   String perGranted="";
	   String msgText="";
	   String subText="";
	   String subUser="";
	
	   if (pageStatus == null) pageStatus = "U";	
	%>
	<Input type="hidden" name="type" value="U">
	
	<P class="defComments"><i><%=LC.L_Msg_Center%><%--Message Center*****--%>:</i> 
	<a href="myHome.jsp?type=U"> <%=LC.L_Unread%><%--Unread*****--%></A>
	&nbsp  
	<a href="myHome.jsp?type=R"> <%=LC.L_Read%><%--Read*****--%></A>
	&nbsp  
	<a href="myHome.jsp?type=A"> <%=LC.L_Acknowledgements%><%--Acknowledgements*****--%></A>
	</P>
	<%MsgCntrDao ctrldao = new MsgCntrDao();
	  ctrldao= msgcntr.getMsgCntrVelosUser(usrId,pageStatus); 
	%> 
	
	<% ArrayList texts=ctrldao.getMsgcntrTexts() ; %>
	<% ArrayList username=ctrldao.getUserFroms() ; %>
	<% ArrayList studyname=ctrldao.getStudyNames() ; %>
	<% ArrayList reqtype=ctrldao.getMsgcntrReqTypes() ; %>
	<% ArrayList studyid=ctrldao.getMsgcntrStudyIds(); %>
	<% ArrayList userx=ctrldao.getMsgcntrFromUserIds(); %>
	<% ArrayList msgcntrid=ctrldao.getMsgcntrIds(); %>
	<% ArrayList Granted=ctrldao.getPermissions(); %>
	
	<% if(pageStatus.equals("R"))
	{
	%>
	
	<P class="defComments"><i><%=LC.L_Read_Messages%><%--Read Messages*****--%></i></P>
	<%}%>
	
	<% if(pageStatus.equals("U"))
	{
	%>
	<P class="defComments"><i><%=LC.L_Unread_Msgs%><%--UnRead Messages*****--%></i></P>
	<%}%>
	
	<% if(pageStatus.equals("A"))
	{
	%>
	<P class="defComments"><i><%=LC.L_Acknowledgements%><%--Acknowledgements*****--%></i></P>
	<%}%>
	
	<Form Name="msgRights" method="post" action="updatemsgcntr.jsp">
	
	<table width="100%" >
	 <tr>
	   <th width="20%">
		<%=LC.L_Name%><%--Name*****--%>
	   </th>
	   <th width="10%">
		<%=LC.L_Study%><%--Study*****--%>
	   </th>
	   <th width="10%">
		<%=LC.L_Text%><%--Text*****--%>
	   </th>
	   <th width="10%">
		<%=LC.L_Request%><%--Request*****--%>
	   </th>   
	   <th width="50%">
		<%=LC.L_Permission_Granted%><%--Permission Granted*****--%>
	   </th>
	 </tr>
	
	<input name=count type=hidden value=<%=texts.size()%>>
	<input name=srcmenu type=hidden value="tdMenuBarItem3">
	
	 <%
	
	for(int i=0; i< texts.size(); i++) 
	{	
		reqType=((reqtype.get(i)) == null)?"-":(reqtype.get(i)).toString();
		reqType=reqType.trim();
		//if(!reqType.equalsIgnoreCase("a"))
		//{
		if(reqType.equalsIgnoreCase("v")){
			permission=LC.L_View/*"View"*****/;
		}
				
		if(reqType.equalsIgnoreCase("m")){	
			permission=LC.L_Modify/*Modify*****/;
	      }		
	
		userName= ((username.get(i)) == null)?"-":(username.get(i)).toString();
		studyName=((studyname.get(i)) == null)?"-":(studyname.get(i)).toString();
		//studyId=((studyid.get(i)) == null)?"-":(studyid.get(i)).toString();		
		perGranted=((Granted.get(i)) == null)?"-":(Granted.get(i)).toString();
		studyId=((studyid.get(i)) == null)?"-":(studyid.get(i)).toString();
		userX=((userx.get(i)) == null)?"-":(userx.get(i)).toString();
		msgText=((texts.get(i)) == null)?"-":(texts.get(i)).toString();
	
		//out.print(userX);
		msgcntrId=((msgcntrid.get(i)) == null)?"-":(msgcntrid.get(i)).toString();
		out.print(studyId);
		out.print("msg" +msgcntrId);
	
	%>
		<input	type= hidden value="<%=msgcntrId%>" name=id<%=i%> >
		<input	type= hidden value="<%=studyId%>" name=studyid<%=i%> >
		<input	type= hidden value="<%=userX%>" name=userX<%=i%> >
	
		
	<%	if ((i%2)==0) {
	  %>
			<tr  id="msgcenterEvenRow">	
	  <%
			}
			else{
	  %>
			
				<tr id="msgcenterOddRow">	
	  <%
			}
	  %>
			<td >
			
			<% if(userName.length()>10){
	    		subUser=userName.substring(0,10);
	    		subUser+="..";
	    		}
	    	%>
			
				<a href="#" onclick="openuserwin(<%=userX%>)"><%=subUser %>
				
			
			</td>
			<td >
				<%= studyName %>
			</td>
			<td >
			<% if (msgText.length() >3){
			subText=msgText.substring(0,3);
			subText+="..";
			}%>		
			<a href="#" onclick="opentextwin('<%=msgText%>')"><%=subText %>
				
			</td>		
			<td>
				<%= permission %>
			</td>
	
	<% if(pageStatus.equals("U"))
	{
	%>
			<td>		
			
			<Input type="radio" value ="view" name=right<%=i%> ><%=LC.L_View%><%--View*****--%>
			<Input type="radio" value ="modify" name=right<%=i%>><%=LC.L_Modify%><%--Modify*****--%>
			<Input type="radio" value="deny" name=right<%=i%>><%=LC.L_Deny%><%--Deny*****--%>
			
			
			</td>				
	<%}else{%>
	
			<td>		
				<% if(perGranted.equalsIgnoreCase("v")){
				perGranted=LC.L_View/*View*****/;
				}
				if(perGranted.equalsIgnoreCase("m")){	
				perGranted=LC.L_Modify/*Modify*****/;
	      		}		
	      		if(perGranted.equalsIgnoreCase("d")){	
				perGranted=LC.L_Deny/*Deny*****/;
	      		}		%>
				
				<%= perGranted %>
			</td>				
	<%}%>
			</tr>
	<%
	}
	%>
	
	<% if(pageStatus.equals("U") && texts.size() > 0 )
	{
	%>
	<tr><td colspan=4><Input type="submit" value="<%=LC.L_Submit%><%--Submit*****--%>" name="Submit"></td></tr>
	<%}%>
	</table>
	
	&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
	&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
	</form>
	
	
	</DIV>
	
	
	<%HttpSession thisSession = request.getSession(true); %>
	
	<%--out.print(thisSession);--%>
	
	<%thisSession.putValue("GRights",grpRights);%>
	
	<jsp:useBean id="ulinkB" scope="request" class="com.velos.eres.web.ulink.ULinkJB"/>
	
	<%
	     UsrLinkDao usrLinkDao = ulinkB.getULinkValuesByUserId(usrId);
	   
	   ArrayList lnksIds = usrLinkDao.getLnksIds(); 
	   ArrayList lnksUris = usrLinkDao.getLnksUris();
	   ArrayList lnksDescs = usrLinkDao.getLnksDescs();
	   ArrayList lnksGrps = usrLinkDao.getLnksGrpNames();
	
	
	   String lnkUri = null;
	   String lnkDesc = null;
	   String oldGrp = null;	
	   String lnkGrp = null;		
	
	
	   int len = lnksIds.size();
	   int counter = 0;
	%>
	
	
	<DIV id="myLinks">
	
	<Form name="usrlnkbrowser" method="post" action="" onsubmit="">
	
	<table width="190" cellspacing="0" cellpadding="0" border=0 >
	
	 <tr >
	    <td width=19 height=20>
	     <img src= "../images/link_upleft.jpg" height=20>
	    </td>
	    <td width="130" height=20>
		<P class = "sectionHeadings">
			<%=LC.L_My_Links%><%--My Links*****--%>
		</P>
	    </td>
	    <td width=22>
		<A href="ulinkBrowser.jsp?user=<%=usrId%>&srcmenu="><%=LC.L_Edit%><%--Edit*****--%></A>
	    </td>
	    <td  width=19 height=20 align="right">
		<img src= "../images/link_upright.jpg" width=23 height=20>
	    </td>
	 </tr>
	
	 <%
	    for(counter = 0;counter<len;counter++)
		{		
			lnkUri=((lnksUris.get(counter))==null)?"-":(lnksUris.get(counter)).toString();
			lnkDesc=((lnksDescs.get(counter))==null)?"-":(lnksDescs.get(counter)).toString();
	lnkGrp = ((lnksGrps.get(counter))==null)?"-":(lnksGrps.get(counter)).toString();
	
	
	%>
	
	   	<% if ( ! lnkGrp.equals(oldGrp)){ %>
			<tr height = 15><td width=19> </td> 
		<td width="130">	 </td>
		<td width=22> </td>
		 <td  width=18 > </td>
		</tr>              
	        	<tr >	
	   		 <td width=19>
	   		 </td>
			 <td width="130">
			  <%= lnkGrp %>
			 </td>
			 <td width=22>
			 </td>
			 <td  width=18 >
			 </td>
			</tr>              
	
	
		 <%}%>
	
	
	
			<tr>	
	   		 <td width=19>
	   		 </td>
			 <td width="130">
			  <A href="<%= lnkUri%>" target="_new"><%= lnkDesc%></A>
			 </td>
			 <td width=22>
			 </td>
			 <td  width=18 >
			 </td>
			</tr>              
	
	 <%
		oldGrp = lnkGrp;
	 		}
	%>
	
	 <tr>
	 <td width=19 height=20 align="bottom">
	   <img src= "../images/link_lowleft.jpg" align="absbottom">
	 </td>
	 <td width="130" height=20></td>
	 <td width=22 height=20></td>
	 <td width=18 height=20 align="right">
	   <img src= "../images/link_lowright.jpg" align="absbottom" height=21>
	 </td>
	 </tr>
	</table>
	</Form>
	
	</div>
	
	<%
	} //end of body for session 
	else
	{
	%>
	
	<jsp:include page="timeout.html" flush="true"/> 
	<%
	}
	%>
	<div class = "myHomebottomPanel" style="border-style:none" align=bottmom>
	<jsp:include page="bottompanel.jsp" flush="true"> 
	</jsp:include>   
	</div>
	
	</div>
	
	<div class ="mainMenu" id = "emenu">
	<jsp:include page="menus.htm" flush="true"/>   
	</div>
	
	</BODY>
	</HTML>
