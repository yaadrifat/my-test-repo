<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/><%@page import="com.velos.eres.service.util.*"%>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="myhomeresolution.js"></SCRIPT>
<SCRIPT>
	function openwin() {
      window.open("","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=500,height=400")
;}
</SCRIPT>  


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<body id="bd" onLoad="load()" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0">
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%

int ienet = 2;

String agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1) 
     ienet = 0; //IE
    else
	ienet = 1;
 		String scHeight = "";
		String scWidth = "";
	
	HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession))
	{	
		scHeight= (String) tSession.getValue("appScHeight");
		scWidth = (String) tSession.getValue("appScWidth");
 
	}
		
		if (scHeight == "" || scHeight == null)
		{
			scHeight = "600";
			scWidth = "800";	
		}
		

%>
	<table class=tableDefault width="546" cellspacing="0" cellpadding="0" border="0">
		<tr> 
		<td id=topleftCurve class=tdDefault width="52"><img src="../images/jpg/top_left_curve.jpg" width="52" 	height="32"></td>
		<%
		if (ienet==0) {
		%>
	    	<td valign=TOP><img id="hometop" src="../images/jpg/topline.jpg" width="658" height="5" align="top"></td>
		<%}
		else
		{
			if ( Integer.parseInt(scWidth) > 1024)
			{
			%>
			
			
	    	<td valign="TOP"><img id="hometop" src="../images/jpg/topline.jpg" width="858" height="5" align="top"></td>
			
		<%
			}
			else if ( Integer.parseInt(scWidth) > 800)
			{
			%>
			
			
	    	<td valign="TOP"><img id="hometop" src="../images/jpg/topline.jpg" width="658" height="5" align="top"></td>
			
		<%
			}
			else
			{
			%>
				
	    	<td valign=TOP><img id="hometop" src="../images/jpg/topline.jpg" width="658" height="5" align="top"></td> 
			
			<%
			
			}
		
		}
		%>
 		</tr>
	</table>
	<table class=leftPanelVLine width="22" cellspacing="0" cellpadding="0" border="0">
		<tr> 
	    <td width="21">&nbsp;</td>
	  <td width="1" >
 	   <%
	  			if ( Integer.parseInt(scHeight) > 768)
			{
		%>
			  <img id=homeright src="../images/jpg/vline.jpg" width="2" height="640"></td>
		
			<%
			}
			else if ( Integer.parseInt(scHeight) > 600)
			{
		%>
			  <img id=homeright src="../images/jpg/vline.jpg" width="2" height="569"></td>
		
			<%
			}
			else
			{
			%>
<%-- fix for bug #1357 - increase the length of the side bar --%>			
			  <img id=homeright src="../images/jpg/vline.jpg" width="2" height="469"></td>
			  
			<%
			
			}
			%>	  
		
		</tr>
	</table>
<table class=tableDefault width="75%" cellspacing="0" cellpadding="0" border="0">
  <tr> 
    <td class=tDDefault > 
      <p align="left"><img id=bottomcurve src="../images/jpg/test.jpg"></p>
    </td>
  </tr>
</table>
<form name="panel" method="POST">
  <% String srcMenu;
	srcMenu= request.getParameter("src");
			if (sessionmaint.isValidSession(tSession))
			{	
				if (srcMenu == null)
				{
				srcMenu = "";
				}
			}	
			else
			{
			 srcMenu = "";
			}

out.print("<Input type=\"hidden\" name=\"src\" value=\"" +srcMenu +"\">");
%>
</form>
<!--<DIV CLASS=topRightPanel id=topRightDiv> -->
<%
 tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession))
	{

   		int i = 2;

		String agent_panel = request.getHeader("USER-AGENT");

	if((agent_panel.indexOf("MSIE") != -1)  && 	(agent_panel.indexOf("Mac") != -1))
{
%>	
<DIV CLASS=topRightPanel id=topRightDiv style="margin-top:-654;"> 
<% }else {
 %>	
 	<DIV CLASS=topRightPanel id=topRightDiv >
  <%}}%> 
	
  <form name="form1" method="POST" action="studySearchAfterLogin.jsp">
    <table cellspacing="0" cellpadding="0" border="0">
<!--      <tr> 
        <td class=tdDefault width="49" height="80" ROWSPAN = 4><img src="../images/jpg/top_right_curve.jpg"></td>
        <td width="203" VALIGN = TOP height=20>&nbsp;</td>
      </tr>

	  <tr><td>&nbsp;</td></tr>
	  <tr><td>&nbsp;</td></tr>
      <tr > 
        <td  width="200" height="7"> 
          <img src="../images/jpg/topline.jpg" width="245" height="7" align="absbottom">
        </td>
      </tr>
-->
<%
	String accName = (String) tSession.getValue("accName");
	  accName=(accName==null)?"default":accName;

	if ((accName == "default") && (ienet == 0)) accName = accName + "_ie";

	String logoName = "../images/acclogo/account_" + accName + ".jpg";

%>

<% if (tSession.getValue("accName") != "default") {

 if (ienet == 0) {%>
	<tr><td width="254" height="8" valign=TOP><img src="../images/jpg/topline.jpg" width="291" height="8" align="top"></td></tr>
<% }else{ %>
	<tr><td width="254" height="5" valign=TOP><img src="../images/jpg/topline.jpg" width="291" height="5" align="top"></td></tr>
<%}}%>

	<tr><td><img src=<%= logoName%> width="291" height="80" align="absbottom"></td></tr> 

    </table>
    </table>
  </form>
</DIV>
<DIV class="logo" ID="vlogo"> 
  <table width=100% border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="155" valign="top"><A href="http://www.velos.com"> <img src="./images/vlogo.gif"></A></td>
	  <td align="left">
	  
		<% 
		if (sessionmaint.isValidSession(tSession))
		{
	  		String uName = (String) tSession.getValue("userName");

			//VA- check if the dynamic report modile is exited, the
			// clean the seesion hashmap from session 
			String sess=(String)request.getParameter("sess");
			if (sess==null)  tSession.removeAttribute("attributes");
			//end VA
			
			%>
			<font  family="arial" color="red" size="4pt" weight="bold"><%Object[] arguments1 = {uName}; %>
	    <%=VelosResourceBundle.getLabelString("L_Welcm",arguments1)%><%-- Welcome<%= uName %>*****--%> </font>
			<div id="test" class="plwt" >
			<P class = "plwait" id="plwait" align="abscenter"><%=MC.M_Wait_PageLoading %><%-- Please wait, page loading*****--%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img valign="bottom" id="imgwait" src="../images/jpg/pleasewait.gif"></img></p>
			</div>

		<%
		}
		%>
	  </td>
    </tr>
  </table>
</DIV>

</body>
