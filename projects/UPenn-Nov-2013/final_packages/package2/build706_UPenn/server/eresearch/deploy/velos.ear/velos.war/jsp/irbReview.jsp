<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html> 
<head> 
<title><%=LC.L_Review_AndForm%><%--Review and Form*****--%></title> 

<%@ page language = "java" import="com.velos.eres.service.util.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.web.appendix.AppendixJB,com.velos.eres.web.user.UserJB" %>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="lnkformB" scope="request" class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>

			 
<script type="text/javascript">
function reloadOpener() {
    if (window.opener != null && window.opener.location != null && window.opener.location != undefined) {
	    window.opener.location.reload();
	    // setTimeout("self.close()",1000);
    }
}

function viewDocument(formobj){
		formobj.submit();
}

function openPopup(formobj) {
	if (formobj.popup.checked) {
	    windowName = window.open("irbReviewPopup.jsp?studyId="+formobj.studyId.value+
	    	    "&selType="+formobj.selType.value+ "&study_acc_form_right="+formobj.study_acc_form_right.value+ "&study_team_form_access_right="+formobj.study_team_form_access_right.value+"&appSubmissionType="+formobj.appSubmissionType.value,
	    	    "reviewWin","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=no,width=600,height=400,left=10,top=10");
	    if (!windowName) { windowName.focus(); }
	} else {
		formobj.submit();
	}
} 

</script> 
</head> 
   	<jsp:include page="include.jsp" flush="true"/>
   	<jsp:include page="ui-include.jsp" flush="true"/>
<style>
	html, body { overflow:hidden; }
</style>
<body> 
<%
	HttpSession tSession = request.getSession(true);
	
	if (sessionmaint.isValidSession(tSession))
	{
	
					String defUserGroup = (String) tSession.getAttribute("defUserGroup");
				
					String ddStudyvercat="";
					CodeDao cdCat=new CodeDao();
					cdCat.getCodeValues("studyvercat");
					cdCat.setCType("studyvercat");
			        cdCat.setForGroup(defUserGroup);
			
					
					String studyvercat=request.getParameter("selType");
					if (studyvercat==null) studyvercat="";		
					
					String studyId = StringUtil.htmlEncodeXss(request.getParameter("studyId"));
					
					ddStudyvercat=cdCat.toPullDown("selType",EJBUtil.stringToNum(studyvercat)," onChange='viewDocument(document.rev);' ");
					// ddStudyvercat=cdCat.toPullDown("selType",EJBUtil.stringToNum(studyvercat)," onChange='' ");
					
					String tabsubtype =StringUtil.htmlEncodeXss(request.getParameter("tabsubtype"));
					String appSubmissionType =StringUtil.htmlEncodeXss(request.getParameter("appSubmissionType"));

 
					String submissionPK = StringUtil.htmlEncodeXss(request.getParameter("submissionPK"));
					String submissionBoardPK = StringUtil.htmlEncodeXss(request.getParameter("submissionBoardPK"));
					
					//bypass rights
					int study_acc_form_right = 7;
					int study_team_form_access_right = 7;
					
					String accId=(String)tSession.getAttribute("accountId");
					 
			 	    String userId = (String) tSession.getValue("userId");
				    UserJB userB = (UserJB) tSession.getValue("currentUser");
			   	    String siteId = userB.getUserSiteId();
			   	    
			   	    String tabSubTypeForApplicationForm ="";
			   	    String formSubmissionType = "";
			   	    ArrayList arrFrmIds = new ArrayList();
			   	    ArrayList arrFrmNames = new ArrayList();
			   	    
			   	    StringBuffer sbForm = new StringBuffer();
			   		String formOptionsString = "";    
			   		String viewCompletedFormsOptionsString = "";
			   	    
			   	    if (appSubmissionType.equals("new_app"))
			   	    {
			   	    	tabSubTypeForApplicationForm = "irb_form_tab";
			   	    }
			   	    else
			   	    {
			   	    	tabSubTypeForApplicationForm = "irb_ongoing_menu";
			   	    }
				 
				 	// Add link for all 'Checklist' forms
		 		 
			 		 int idxOption = -1;
			 		 viewCompletedFormsOptionsString = "<OPTION value='AllCheck*F'>"+LC.L_ViewChk_Forms/*View Checklist Forms*****/+"</OPTION>";		
			 		 idxOption = ddStudyvercat.indexOf("<OPTION");
			 		 
		 		 	if (idxOption > -1)
		 		 	{
		 		 		ddStudyvercat = ddStudyvercat.substring(0,idxOption) + viewCompletedFormsOptionsString + ddStudyvercat.substring(idxOption);
		 		 	}	
		 		 		 
					ArrayList arTabs = new ArrayList();
					arTabs.add("irb_new_tab");
					arTabs.add("irb_assigned_tab");
					arTabs.add("irb_compl_tab");
					arTabs.add("irb_post_tab");
					arTabs.add("irb_pend_tab");
					arTabs.add("irb_ongoing_menu");
					arTabs.add("irb_revfull_tab");
					arTabs.add("irb_revanc_tab");
					arTabs.add("irb_revexem_tab");
					arTabs.add("irb_revexp_tab");
					arTabs.add("irb_meeting");
					 
					//arFormTypes are linked with tab types
					String[] arFormTypes = { "irb_check1","irb_check2","irb_check3","irb_check5","","irb_sub","irb_check4", "irb_check4","irb_check4","irb_check4","irb_meeting"};
					String formCategory = "";
					int idxForm = -1;
					if (! StringUtil.isEmpty(tabsubtype))
					{
						idxForm = arTabs.indexOf(tabsubtype);
						if (idxForm > -1)
						{
							formCategory = arFormTypes[idxForm];
						}
					}

					formSubmissionType = EIRBDao.getIRBFormSubmissionTypeSQL(tabSubTypeForApplicationForm,"irb_sub",appSubmissionType);	 						

					LinkedFormsDao lnkFrmDao = new LinkedFormsDao();
		 			
		 			lnkFrmDao = lnkformB.getStudyForms(EJBUtil.stringToNum(accId),EJBUtil.stringToNum(studyId), 
		 				EJBUtil.stringToNum(userId), EJBUtil.stringToNum(siteId), 
		         study_acc_form_right,  study_team_form_access_right, 
		         true, formSubmissionType, "irb_sub");
	 
                 if (lnkFrmDao != null)
                 {
                 	arrFrmIds = lnkFrmDao.getFormId();		 
		 			arrFrmNames = lnkFrmDao.getFormName();
		 		 }
		 		 
		 		 if (arrFrmIds != null && arrFrmIds.size() > 0)
		 		 {
		 		 	 
		 		 	for (int k=0;k< arrFrmIds.size() ;k++)
		 		 	{
		 		 		sbForm.append("<OPTION ").append(k==0 ? "SELECTED" : "")
		 		 		.append(" value=\""+arrFrmIds.get(k)+"*F\">"+ arrFrmNames.get(k)+"</OPTION>");
		 		 	}	
		 		 	formOptionsString  = sbForm.toString();
		 		 	
		 		 	
					
		 		 	if (! StringUtil.isEmpty(formOptionsString))
		 		 	{
		 		 		//append this string to the documents dropdown
		 		 		
		 		 		int idx = -1;
		 		 		
		 		 		idx = ddStudyvercat.indexOf("<OPTION");
		 		 		
		 		 	 
		 		 		if (idx > -1)
		 		 		{
		 		 			ddStudyvercat = ddStudyvercat.substring(0,idx) + formOptionsString + ddStudyvercat.substring(idx);
		 		 		}		
		 		 	}
		 		 }
		 		 
		 		 
		 		 		
		 		 
		 		 
		 		 boolean isReviewArea = false;
                 if (tabsubtype != null && tabsubtype.startsWith("irb_rev")) { isReviewArea = true; }
				
					%>
				<table border="1" width="100%" height="100%" cellpadding="3px" cellspacing="3px">
                <div id="ddOpt" style="font-family:Verdana,Arial,Helvetica,sans-serif;font-size:12">
                <tr><td>
				<form name="rev" style="margin:0px" action="previewIRBDocs.jsp" method="post" target="preview"> 
				 
  				  <input name="studyId" value="<%=studyId%>" type="hidden" >
				  <input name="appSubmissionType" value="<%=appSubmissionType%>" type="hidden" >
				  
				  <input name="study_acc_form_right" value="<%=study_acc_form_right%>" type="hidden" >
				  <input name="study_team_form_access_right" value="<%=study_team_form_access_right%>" type="hidden" >
				  
 				   <%=ddStudyvercat%>
                  <input type="checkbox" name="popup"><%=MC.M_OpenIn_NewWindow%><%--Open in a New Window*****--%></input>
                  <button type="button"  name="goButton" onClick="return openPopup(document.rev);" value="<%=LC.L_Go%><%--Go*****--%>"><%=LC.L_Go%></button>
				</form>
                </td><td>
                <%
				formSubmissionType = EIRBDao.getIRBFormSubmissionTypeSQL(tabsubtype,formCategory,appSubmissionType);	 	
                %>
                <jsp:include page="studyFormDD.jsp" flush="true"> 
               	<jsp:param name="studyId" value="<%=studyId%>"/>
             	<jsp:param name="submissionType" value="<%=formSubmissionType%>"/>			
            	<jsp:param name="formCategory" value="<%=formCategory%>"/>
            	<jsp:param name="submissionPK" value="<%=submissionPK%>"/>
             	<jsp:param name="submissionBoardPK" value="<%=submissionBoardPK%>"/>
            	<jsp:param name="target" value="irbf"/>	
            	<jsp:param name="study_acc_form_right" value="7"/>	
            	<jsp:param name="study_team_form_access_right" value="7"/>	
                </jsp:include>   
                </td>
                </tr>
                </div> 
				<% String formHeight = isReviewArea ? "70%" : "95%"; %>
				<tr height="<%=formHeight%>"> 
				 
				<td id="previewTD" valign="top" width="50%"> 
					
					<iframe name="preview" src="previewIRBDocs.jsp" width="100%" height="100%" frameborder="1" scrolling="yes" allowautotransparency=true>
						
						</iframe>
						
				 </td> 
				 	
				<td id="reviewTD" valign="top" width="50%"> 
					<iframe name="review" src="irbReviewForm.jsp?studyId=<%=studyId%>&tabsubtype=<%=tabsubtype%>&appSubmissionType=<%=appSubmissionType%>&submissionPK=<%=submissionPK%>&submissionBoardPK=<%=submissionBoardPK%>" 
						width="100%" height="100%" frameborder="1" scrolling="no" allowautotransparency=true>
						</iframe>
						
				 </td> 				
				</tr>
                <% if (isReviewArea) { %>
					 <tr height="25%">
					 	<td colspan=2 valign="top" >
					 
							<iframe name="proviso" src="irbProvisos.jsp?studyId=<%=studyId%>&tabsubtype=<%=tabsubtype%>&appSubmissionType=<%=appSubmissionType%>&submissionPK=<%=submissionPK%>&submissionBoardPK=<%=submissionBoardPK%>" 
						width="100%" height="100%" frameborder="1" scrolling="yes" allowautotransparency=true>
						</iframe>
											
						</td>
					 </tr> 
                <% }  %>
				</table> 

	<script>
		document.rev.selType.selectedIndex=0;
		
		viewDocument(document.rev);	
	</script>
	<% }
		else
		{
		%>
			<jsp:include page="timeout.html" flush="true"/>
		<%
		}
		%>
		
	
	  <div class = "myHomebottomPanel"> 
	    <jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
</body> 
</html>