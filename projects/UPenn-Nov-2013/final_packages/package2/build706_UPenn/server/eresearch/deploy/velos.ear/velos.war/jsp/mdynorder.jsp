<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title><%=LC.L_AdhocQry_Sort%><%--Ad-Hoc Query >> Sort*****--%> </title>
	<%@ page language = "java" import = "java.util.*,com.velos.eres.service.util.*,com.velos.eres.web.dynrep.holder.*,com.velos.eres.business.common.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB"%>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<jsp:useBean id="dynrepB" scope="page" class="com.velos.eres.web.dynrep.DynRepJB" />
	<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
	<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script>
		
			window.name = "dynorder";
var formWin = null;

<%-- agodara 08Aug11: Removed for Bug #6728, already using Whichcss_skin.js in panel.jsp --%>
	
function setOrder(formobj,mode){
totcount=formobj.totcount.value;
var orderStr="";
var index="";
var dataOrder="";


if (totcount==1)
{
	fldName=formobj.fldNameOrder.value;
	fldOrder=formobj.fldOrder.value;
	if ((fldName.length>0) && (fldOrder.length>0)) 
	{
		orderStr="Order by " + fldName+ " " + fldOrder;
		dataOrder = fldName+"[$]"+fldOrder;
	}	

} else if (totcount>0)
{
	for (i=0;i<totcount;i++){
		fldName=formobj.fldNameOrder[i].value;
		index=fldName.indexOf("|");
		if (index>=0){
			fldName=fldName.substring(0,index);
		 }

	   fldOrder=formobj.fldOrder[i].value;
		//create a string to store in the datbase and will be pasrsed in modify mode to display on screen
		if ((fldName.length>0) && (fldOrder.length>0))
		{
		  if (dataOrder.length==0) dataOrder=fldName+"[$]"+fldOrder;
		  else dataOrder=dataOrder+"[$$]"+fldName+"[$]"+fldOrder;
		}
		if ((fldName.length>0) && (fldOrder.length>0))
		{ 
			if (orderStr.length==0) orderStr="Order by " + fldName+ " " + fldOrder;
			else if (orderStr.length>0) orderStr=orderStr+", " + fldName+ " " + fldOrder;
		} // if
	}//end for

} //end else
orderStr=replaceSubstring(orderStr,"|",",");
formobj.order.value=orderStr;
formobj.dataOrder.value=dataOrder;
formobj.mode.value=mode;
if (mode=="formchange")
     formobj.action="mdynorder.jsp";
     
      formobj.submit();
      formobj.action="mdynrep.jsp";
}


</script>
	


</head>
<% String src,sql="",tempStr="",sqlStr="",fltrName="",fltrId="",prevId="";
   int strlen=0,firstpos=-1,secondpos=-1,index=-1;
src= request.getParameter("srcmenu");
System.out.println(src);
	
	
%>
<jsp:include page="panel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>
<jsp:param name="sess" value="keep"/>

</jsp:include>   
<body style="overflow:scroll;">
<br>
<DIV id="div1">  
<%

	String mode = request.getParameter("mode");
	String dataOrder="",dynType="";
	String selectedTab = request.getParameter("selectedTab");
	ArrayList sessCol=new ArrayList();
	HashMap attributes = new HashMap();
	ArrayList sessColName=new ArrayList();
	ArrayList sessColType = new ArrayList();
	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))
	{
	String[] scriteria=null,fldData=null,fldSelect= null;
	String selValue="",choppedFilter="",formId="";
	ArrayList selectedfld=null;
	ArrayList dataList=new ArrayList() ;
	ArrayList tempList=new ArrayList();
	ArrayList startBracList=new ArrayList();
	ArrayList endBracList=new ArrayList();
	ArrayList scriteriaList=new ArrayList();
	ArrayList fldDataList=new ArrayList();
	HashMap FltrAttr = new HashMap();
	HashMap FldAttr= new HashMap();
	HashMap TypeAttr= new HashMap();
	HashMap SortAttr= new HashMap();
	String[] fldOrder=null,fldOrderBy=null;
	selValue=request.getParameter("prevFilter");
	
	if (selValue==null) selValue="0";
	String[] fldName=request.getParameterValues("fldLocal");
	String[] fldCol=request.getParameterValues("fldLocalCol");
	String[] fldNameSelect=null;
	String[] extend=null;
	ArrayList fldNameSelList=new ArrayList();
	ArrayList extendList=new ArrayList();
	ArrayList excludeList=new ArrayList();
	ArrayList fltrDtIdList=new ArrayList();
	String[] startBracket=request.getParameterValues("startBracket");
	String[] endBracket=request.getParameterValues("endBracket");
	String[] exclude = request.getParameterValues("exclude");
	String[] fltrDtId= request.getParameterValues("fltrDtId");
	String filter=request.getParameter("filter");
	String direction=request.getParameter("direction");
	direction=(direction==null)?"":direction;
	fltrName=request.getParameter("fltrName");
	choppedFilter=request.getParameter("choppedFilter");
	choppedFilter=(choppedFilter==null)?"":choppedFilter;
	if (fltrName==null) fltrName="";
	if (filter==null) filter="";
	fltrId=request.getParameter("prevFilter");
	if (fltrId==null) fltrId="0";
	if (fltrDtId!=null)
	fltrDtIdList=EJBUtil.strArrToArrayList(fltrDtId);
	if (startBracket!=null)
	startBracList=EJBUtil.strArrToArrayList(startBracket);
	if (endBracket!=null)
	endBracList=EJBUtil.strArrToArrayList(endBracket);
	fldNameSelect=request.getParameterValues("fldName");
	if (fldNameSelect!=null)
	fldNameSelList=EJBUtil.strArrToArrayList(fldNameSelect);
	scriteria=request.getParameterValues("scriteria");
	if (scriteria!=null)
	scriteriaList=EJBUtil.strArrToArrayList(scriteria);
	fldData=request.getParameterValues("fltData");
	if (fldData!=null) 
	fldDataList=EJBUtil.strArrToArrayList(fldData);
	extend=request.getParameterValues("extend");
	if (extend!=null)
	extendList=EJBUtil.strArrToArrayList(extend);
	System.out.println("fldNameSelect"+fldNameSelect);
	if (exclude!=null)
	excludeList=EJBUtil.strArrToArrayList(exclude);
	//Preprocess excludeList,startBracList,endBracList to add -1 for chgeckboxes not selected,"" is stored for rows for unchecled chkbox
	if (fldNameSelList!=null){
	for(int z=0;z<fldNameSelList.size();z++){
	if (excludeList.indexOf(EJBUtil.integerToString(new Integer(z)))>=0){}
	else{
	excludeList.add(z,"");
	}
	if (startBracList.indexOf(EJBUtil.integerToString(new Integer(z)))>=0){}
	else{
	startBracList.add(z,"");
	}
	if (endBracList.indexOf(EJBUtil.integerToString(new Integer(z)))>=0){}
	else{
	endBracList.add(z,"");
	}
	
	}//end for 
	}//end if
	//end
	
	ReportHolder container=(ReportHolder)tSession.getAttribute("reportHolder");
		if (container==null){%>
	<META HTTP-EQUIV=Refresh CONTENT="0; URL=dynrepbrowse.jsp?srcmenu=<%=src%>">
	
	<%}else {
	
	if (mode.equals("next")) 
	{
		String defDateFilterType = ""; String defDateFilterDateFrom = ""; String defDateFilterDateTo = ""; 
		FilterContainer fltrContainer=container.getFilterObject((selValue));
		defDateFilterType = request.getParameter("dateRangeType");
	
		if (StringUtil.isEmpty(defDateFilterType))
		defDateFilterType = "";
		
		defDateFilterDateFrom  = request.getParameter("dtFilterDateFrom");
	
		if (StringUtil.isEmpty(defDateFilterDateFrom))
		defDateFilterDateFrom = "";
	
		defDateFilterDateTo  = request.getParameter("dtFilterDateTo");
	
		if (StringUtil.isEmpty(defDateFilterDateTo))
		defDateFilterDateTo = "";
	
		
		
		if ((fltrContainer==null) && (fldNameSelList.size()>0) && (! StringUtil.isEmpty(fltrName)) )
		{
			
			if (EJBUtil.isEmpty(selValue) || selValue.equals("0") )
			{
				fltrContainer=container.newFilterObject(fltrName);
				fltrContainer.reset();
				fltrContainer.setFilterMode("N");
				
				
				if (! StringUtil.isEmpty(fltrName))
				{
					container.setFilterIds(fltrName);
					container.setFilterNames(fltrName);
				}	
			}
			else
			{
				fltrContainer=container.newFilterObject(selValue);
				fltrContainer.reset();
				fltrContainer.setFilterMode("M");
				
			}	
		  }	else if ((fltrContainer != null) && (fldNameSelList.size()>0) && (! StringUtil.isEmpty(fltrName)) )
			{
				fltrContainer.reset();
				fltrContainer.setFilterMode("M");
				
			}
		
								
		if ( (fltrContainer != null) )
		{
				fltrContainer.setFilterDateRangeType(defDateFilterType); 
				fltrContainer.setFilterDateRangeTo(defDateFilterDateTo); 
				fltrContainer.setFilterDateRangeFrom(defDateFilterDateFrom); 
				
				if (fltrDtIdList!=null)
				{
				if (fltrDtIdList.size()>0) fltrContainer.setFilterDbId(fltrDtIdList);
				}
				if (excludeList!=null) {
				if (excludeList.size()>0) fltrContainer.setFilterExclude(excludeList);
				}
				if (startBracList!=null){
				if (startBracList.size()>0) fltrContainer.setFilterBbrac(startBracList);
				}
				if (fltrName.length()>0) fltrContainer.setFilterName(fltrName);
				if ((fltrId.length()>0)) fltrContainer.setFilterId(fltrId);
				if (endBracList!=null){ 
				if (endBracList.size()>0) fltrContainer.setFilterEbrac(endBracList);
				}
				if (fldNameSelList!=null){
				if (fldNameSelList.size()>0)	fltrContainer.setFilterCols(fldNameSelList);
				}
				if (scriteriaList!=null) {
				if (scriteriaList.size()>0) fltrContainer.setFilterQualifier(scriteriaList);
				}
				if (fldDataList!=null){
				if (fldDataList.size()>0)  fltrContainer.setFilterData(fldDataList);
				}
				if (extendList!=null) {
				if (extendList.size()>0) fltrContainer.setFilterOper(extendList);
				}
				
				//if (fldName!=null) FltrAttr.put("fldName",fldName);
				//if (fldCol!=null) FltrAttr.put("fldCol",fldCol);
				
				//if (filter.length()>0)	FltrAttr.put("filter",filter);
				fltrContainer.setFilterStr(filter);
				
				
				
				String[] tempArray=StringUtil.strSplit(choppedFilter,"[VELFILTERSEP]",false);
				String [] processArray;
				for (int i=0;i<tempArray.length;i++)
				{
				processArray=StringUtil.strSplit(tempArray[i],"[VELSEP]",false);
				
				  fltrContainer.setFormIds(processArray[0]);
				  fltrContainer.setChoppedFilters(processArray[1]);
				
				}
		} //fltrContainer !=null	
		container.Display();
 
	} 
	String selForm=request.getParameter("selForm");
	selForm=(selForm==null)?"":selForm;
	String prevSelForm=request.getParameter("prevSel");
	prevSelForm=(prevSelForm==null)?"":prevSelForm;
	
	ArrayList formIdList=container.getFormIds();
	ArrayList formNameList=container.getFormNames();
	
	if (selForm.length()==0)
	 selForm=(String)formIdList.get(0);
	 
	 StringBuffer formDD=new StringBuffer("<select name='selForm' onchange=setOrder(document.dynorder,'formchange')>");
	 for (int i=0;i<formIdList.size();i++)
	 {
	   String selId=(String)formIdList.get(i);
	   if (selId.equals(selForm))
	   {
	    formDD.append("<option value="+selId+" selected>"+formNameList.get(i)+"</option>");
	   }
	   else
	   {
	    formDD.append("<option value="+selId+">"+formNameList.get(i)+"</option>");
	   }
	 }
	 ArrayList fldNameList=new ArrayList(),
	           fldOrderList=new ArrayList(); 
	  String[] fldOrderToObject=request.getParameterValues("fldNameOrder");
	  String[] fldOrderByToObject=request.getParameterValues("fldOrder");
	 dataOrder=request.getParameter("dataOrder");
	 dataOrder=(dataOrder==null)?"":dataOrder;
	 
	 String order=request.getParameter("order");
	 order=(order==null)?"":order;
		 
	 if (fldOrderToObject!=null)
	    fldNameList=EJBUtil.strArrToArrayList(fldOrderToObject);
	 if (fldOrderByToObject!=null)
	  fldOrderList=EJBUtil.strArrToArrayList(fldOrderByToObject);
	 
	SortContainer sortContainer=container.getSortObject(prevSelForm);
	if ((sortContainer==null) && (fldNameList.size()>0))
	{
	 sortContainer=container.newSortObject(prevSelForm);
	}
	
	if (sortContainer!=null)
	{
	sortContainer.reset();
	sortContainer.setSortFields(fldNameList);
	sortContainer.setSortOrderBy(fldOrderList);
	sortContainer.setSortStr(dataOrder);
	System.out.println("dataOrder11" +dataOrder);
	
	}
	
	tSession.setAttribute("reportHolder",container);
	
	container.Display();
	
	formId=container.getFormIdStr();
	if (formId==null) formId="";
	
	dynType=container.getReportType();
	if (dynType==null) dynType="";
	
	System.out.println("selForm!!!!" + selForm);
	
	sortContainer=container.getSortObject(selForm);
	
	if (sortContainer!=null)
	{
		System.out.println("got sort object!!!!" );
		
		dataOrder=sortContainer.getSortStr();
		
		System.out.println("got sort object!!!!" + dataOrder);
		
		dataOrder=(dataOrder==null)?"":dataOrder;
		
		
		fldOrder=(String[]) sortContainer.getSortFields()
		.toArray(new String[sortContainer.getSortFields().size()]);
		
		
		fldOrderBy=(String[])sortContainer.getSortOrderBy()
		 .toArray(new String[sortContainer.getSortOrderBy().size()]);
				 
		if ((fldOrder==null || fldOrder.length == 0) && (dataOrder.length()>0)){
		ArrayList tempName=new ArrayList();
		ArrayList tempOrder=new ArrayList();
		//[$$] is replaced with ~ because of imporoper token results from tokenzer using [$$]
		dataOrder=StringUtil.replaceAll(dataOrder,"[$$]","~");
		StringTokenizer st= new StringTokenizer(dataOrder,"~");
		System.out.println("&&&&&&&&&&&&&&&&&dataOrder&&&&&&&&&&&&&"+dataOrder);
		StringTokenizer stTemp=null;
		while (st.hasMoreTokens()){
		tempStr=(String)st.nextToken();
		System.out.println("tempStr"+tempStr);
		System.out.println(tempStr.substring(0,tempStr.indexOf("[$]")));
		System.out.println(tempStr.substring((tempStr.indexOf("[$]"))+3));
		if ((tempStr.trim()).length()>3){
		System.out.println("inhere"+tempStr.length());
		tempName.add(tempStr.substring(0,tempStr.indexOf("[$]")));
		tempOrder.add(tempStr.substring((tempStr.indexOf("[$]"))+3));
		}
		
		}
		fldOrder=(( String [] ) tempName.toArray ( new String [ tempName.size() ] ));
		fldOrderBy=(( String [] ) tempOrder.toArray ( new String [ tempOrder.size() ] ));
		if ((order.length()==0) && (dataOrder.length()>0)) {
		order =" Order By " + StringUtil.replaceAll(dataOrder,"[$]"," ");
		order=StringUtil.replaceAll(order,"[$$]",",");
		
		System.out.println("fldOrder"+fldOrder);
		System.out.println("fldOrderBy"+fldOrderBy);
		
		
		}
		
		}
	} // end(sortContainer!=null)
	FieldContainer fldContainer=container.getFldObject(selForm);
	if (fldContainer!=null)
	{
	sessCol=fldContainer.getFieldColId();
	sessColName=fldContainer.getFieldName();
	sessColType = fldContainer.getFieldType();
	}
	
	
	int counter=0;
	String name="";
	String fldType="";
	StringBuffer fldDD=new StringBuffer();
	String formName=container.getFormNameStr();
	 if( formName==null) formName="";
	 formName = StringUtil.replace(formName,"{VELSEP}",";");
	%>

<form name="dynorder" action="dynrep.jsp" method="post">

<div class="tabDefTopN" id="divTab">


<table align="center" border="0" cellpadding="0" cellspacing="0" width="98%">

<tr bgcolor="#dcdcdc" align="center">
<%if (container.getFormIdStr()!=null){%>
<td><A href="mdynreptype.jsp?mode=link&srcmenu=<%=src%>&dynType=<%=dynType%>"><%=LC.L_Select_RptType%><%--Select Report Type*****--%></A></td>
<%}else{%>
<td><%=LC.L_Select_RptType%><%--Select Report Type*****--%></td>
<%}%>
<td>>></td>
<%if ((container.getFldObjects()).size()>0){%>
<td><A href="mdynselectflds.jsp?mode=link&srcmenu=<%=src%>"><%=LC.L_Select_Flds%><%--Select Fields*****--%></A></td>

<%}else{%>
<td><%=LC.L_Select_Flds%><%--Select Fields*****--%></td>
<%}%>
<td>>></td>
<%if ((container.getFilterObjects()).size()>0){%>
<td><A href="mdynfilter.jsp?mode=link&srcmenu=<%=src%>"><%=LC.L_Filter_Criteria%><%--Filter Criteria*****--%></A></td>

<%}else{%>
<td><%=LC.L_Filter_Criteria%><%--Filter Criteria*****--%></td>
<%}%>
<td>>></td>
<%if ((container.getSortObjects()).size()>0){%>
<td><A href="mdynorder.jsp?mode=link&srcmenu=<%=src%>"><font color="red"><%=LC.L_Sort_Criteria%><%--Sort Criteria*****--%></font></A></td>

<%}else{%>
<td><font color="red"><%=LC.L_Sort_Criteria%><%--Sort Criteria*****--%></font></td>
<%}%>
<td>>></td>
<%if (container.getReportName().length()>0){%>
<td><A href="mdynrep.jsp?mode=link&srcmenu=<%=src%>"><%=LC.L_Preview_Save%><%--Preview & Save*****--%></A></td>

<%}else{%>
<td><%=LC.L_Preview_Save%><%--Preview & Save*****--%></td>
<%}%>

<td width="25%" align="right">
<button type="submit" onClick ="return setOrder(document.dynorder,'next');"><%=LC.L_Next%></button> 		
</td>
</tr>
</table>
	
</div>

<DIV class="tabDefBotN" id="div1">
<input type="hidden" name="formId" value="<%=formId%>")>
<input type="hidden" name="prevSel" value="<%=selForm%>")>
<input type="hidden" name="totcount" value="<%=sessCol.size()%>">
<input type="hidden" name="mode" value="<%=mode%>">
<input type="hidden" name="order" value="">
<input type="hidden" name="dataOrder" value="<%=dataOrder%>">
<input type="hidden" name="filter" value="<%=filter%>">
<input type="hidden" name="srcmenu" value="<%=src%>">

<!-- <P class="sectionHeadings">Selected Form: <%=formName%> </P> -->
<BR>

<table class="tableDefault" width="100%" border="1" align="center">
<tr><td colspan="2" align="center"><%=MC.M_Sel_ToSpecifyCriteria%><%--Select a Form or Table to specify Sort Criteria*****--%>: <%=formDD%> </td></tr>
<tr>
<th width="35%"><%=LC.L_Select_Fld%><%--Select Field*****--%></th>
<th width="35%"><%=LC.L_Sort_Order%><%--Sort Order*****--%></th>

</tr>

<%
for (int i=0;i<(sessCol.size());i++){
	
  if ((i%2)==0)
  {%>
  <tr>
   <%}else{ %>
   <tr>
   <%}%>
   
   <td width="15%" align="center"><%
   if (sessCol!=null){
   
   %>
	<SELECT NAME='fldNameOrder'> ;
	<OPTION value='' ><%=LC.L_Select_AnOption%><%--Select an Option*****--%></OPTION>
	<%
		
	for (counter = 0; counter < (sessCol.size()) ; counter++){
		name =(String)sessCol.get(counter);
		if (name==null) name="";
			
		fldType = (String)sessColType.get(counter);
		if (fldType==null) fldType="";
		
		
		/*if (name.length()>=32) name=name.substring(0,30);
		if (name.indexOf("?")>=0) 
		name=name.replace('?','q');
		if (name.indexOf("\\")>=0)
		name=name.replace('\\','S');*/
		if (name.length()>0)
		{
			index=name.indexOf("|");
		
			if (index>=0){
			name=name.substring(0,index);
			}
		
			if (fldType.equals("ET"))
			{
				name  = "lower(" + name +")" ;
			}
			
			if (! selForm.startsWith("C"))	
			{
				if (fldType.equals("EN"))
				{
					name  = "pkg_util.f_to_number(" + name +")" ;
				}else if (fldType.equals("ED"))
				{
					if ( (! (name.trim()).equalsIgnoreCase("created_on")) && (! (name.trim()).equalsIgnoreCase("last_modified_date")))
					{
						name  = "F_TO_DATE("+name+")" ;
					}
				}
			}				
					
		
		if ((fldOrder!=null) && (i<fldOrder.length)){
						
	       	if (name.equals(fldOrder[i])){		%>
		<OPTION value='<%=name%>' selected><%=sessColName.get(counter)%></OPTION>
		<%}else{%>
		<OPTION value='<%=name%>'><%=sessColName.get(counter)%></OPTION>
		<%}}else{%> 		
		<OPTION value='<%=name%>'><%=sessColName.get(counter)%></OPTION>
			<%}
			}
		}%> 
		
		</SELECT>
	<%}%>
	</td>
   <td width="10%" align="center"><select size="1" name="fldOrder">
   <option value=""><%=LC.L_Select_AnOption%><%--Select an Option*****--%></option>   
   <option value="Asc" <% if (fldOrderBy!=null){if (i<fldOrderBy.length) {System.out.println(fldOrderBy[i]); if (fldOrderBy[i].equals("Asc")){%>selected<%}}}%>><%=LC.L_Ascending%><%--Ascending*****--%></option>
   <option value="Desc" <%if (fldOrderBy!=null){ if (i<fldOrderBy.length){if (fldOrderBy[i].equals("Desc")){%>selected<%}}}%>><%=LC.L_Descending%><%--Descending*****--%></option>
</select></td>
 </tr>
<%}%>
</table>
<!--<table width="100%" cellspacing="0" cellpadding="0">
      <td width="20%"> 
	   
		
      </td> 

      <td> 
	<input type="image" src="../images/jpg/Next.gif" align="absmiddle"  border="0" onClick ="return setOrder(document.dynorder,'next');">		
<A href=""><img src="../images/jpg/Next.gif" align="absmiddle" border="0"></img></A>
      </td> 
      </tr>
  </table>-->
</DIV>
  <input type="hidden" name="sess" value="keep">
</form>
<%
}//end for attributes==null
} else {  //else of if body for session

%>
  <jsp:include page="timeout.html" flush="true"/>

  <%
}


%>
 <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</DIV>
  <div class ="mainMenu" id = "emenu">
  <jsp:include page="getmenu.jsp" flush="true"/>
  </div>


</body>
</html>
