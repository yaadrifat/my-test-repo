<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=MC.M_Mstone_PmentDets%><%--Milestone >> Payment Details*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
</head>

<SCRIPT>
function  validate(formobj){
	
	
	
	//JM: 01.30.2006 
	if(formobj.dpayCode.value=='') {
   
		alert("<%=MC.M_PlsSel_PmentType%>");/*alert("Please select the Payment Type");*****/
		formobj.dpayCode.focus();		
		return false;
		
	}

	//if (!(validate_col('Type',formobj.dpayCode))) return false
	
	
	
	
	
	
	//if (!(validate_col('e-Signature',formobj.amount))) return false
	if (!(validate_col('Payment Date',formobj.date))) return false

	//if(formobj.amountNum.value=="" && formobj.amountFrac.value=="")
	//{
	//	alert("<%=MC.M_Etr_AmtData%>");/*alert("Please enter data in Amount");*****/
	//	return false;
	//}
	if(formobj.amountNum.value=="")
	{
		alert("<%=MC.M_Etr_AmtData%>");/*alert("Please enter data in Amount");*****/
		return false;
	}
	//For Fixing the Bug# 13699
	if(formobj.amountNum.value.length > 12){
		alert("<%=MC.M_Max_AmtLength%>");/*alert("Maximum length of Amount field is 12 digits.");*****/
		return false;
	}
	//	if (!(validate_col('e-Signature',formobj.eSign))) return false

	
	if(isNaN(formobj.amountNum.value) == true) {
		alert("<%=MC.M_EtrNum_AmtVal%>");/*alert("Please enter a numeric value for Amount.");*****/
		formobj.amountNum.focus();
		return false;
   }
   
    //var index2 = formobj.amountNum.value.indexOf(".");
    //	  if (index2 != -1) {
    // 		alert("<%=MC.M_DecimalNotAlw_Fld%>");/*alert("Decimal value not allowed in this field. Please enter the decimal part in the appropriate field");*****/
	//		formobj.amountNum.focus();
			 		
	//		return false;
    //	}
	    	
   	
	//if(isNaN(formobj.amountFrac.value) == true) {
	//	alert("<%=MC.M_EtrNum_AmtVal%>");/*alert("Please enter a numeric value for Amount.");*****/
	//	formobj.amountFrac.focus();
	//		return false;
    //  }
   
   
    // formobj.amount.value = formobj.amountNum.value + "." + formobj.amountFrac.value;
    formobj.amount.value = formobj.amountNum.value;
	
	 /*if (!(isDecimal(formobj.amount.value))){
	 		alert("Invalid Amount");
			formobj.amountNum.focus();
		 	 return false;
		 } */
   	
//	if(isNaN(formobj.eSign.value) == true) {
//		alert("Incorrect e-Signature. Please enter again");
//		formobj.eSign.focus();
//		return false;
 //  }
   
   
   
}
</SCRIPT>



<% String srcmenu;
srcmenu= request.getParameter("srcmenu");
%>

<body >

<br>
<jsp:useBean id="milepaymentB" scope="request" class="com.velos.eres.web.milepayment.MilepaymentJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="stdRightsB" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />

<%@ page language = "java" import = "com.velos.eres.business.common.*,com.velos.eres.service.util.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.DateUtil,com.velos.esch.business.common.*"%>
<jsp:include page="include.jsp" flush="true"/>
<%
 String selectedTab = request.getParameter("selectedTab"); 
 String study = request.getParameter("studyId");
%>  

<DIV class="popDefault" id="div1"> 
  
	
  <%
   HttpSession tSession = request.getSession(true);
   if (sessionmaint.isValidSession(tSession))
	{	

	
String pageRight= "";	 
 int pgRight = 0;
 
 
	pageRight = (String) tSession.getAttribute("mileRight");	
  pgRight = EJBUtil.stringToNum(pageRight);
  
  if (pgRight > 0 ){
	
	  CodeDao codeLstPayType = new CodeDao();
    String description = "";
    String date = "";
    String amount = "";
	String amountNumeric = "";
	String amountFractional = "";
    String mode = "";
	int decPos = 0;
	String milepaymentId = "";
    String userIdFromSession = (String) tSession.getAttribute("userId");	
    int studyId = EJBUtil.stringToNum(study);
    String dpayCode = "";

    String ddpayCode ="";//JM:
    String paymentCode = "";
    
	String studyCurrency = "";
	ArrayList curr = new ArrayList();
	String currency = "";		
	String comments = "";

	studyB.setId(EJBUtil.stringToNum(study));
  		studyB.getStudyDetails();
  		studyCurrency = studyB.getStudyCurrency();	
	SchCodeDao cdDesc = new SchCodeDao();
	cdDesc.getCodeValuesById(EJBUtil.stringToNum(studyCurrency));
	curr =  cdDesc.getCSubType();
	currency=(((curr.get(0)) ==null)?"-":(curr.get(0)).toString());		
	
	codeLstPayType.getCodeValues("paymentCat");
	
    mode = request.getParameter("mode");
     
    
    if(mode.equals("M")){  
	
	milepaymentId = request.getParameter("milepaymentId");
	milepaymentB.setId(EJBUtil.stringToNum(milepaymentId));
	milepaymentB.getMilepaymentDetails();
	
	description = milepaymentB.getMilepaymentDesc();
	description = (   description  == null      )?"":(  description ) ;	
	
	date = milepaymentB.getMilepaymentDate();
	amount = milepaymentB.getMilepaymentAmount();
	paymentCode = milepaymentB.getMilepaymentType();	
	
	
	comments = milepaymentB.getMilepaymentComments();

	//KM-#3890
	if (comments == null)
		comments ="";

	if (amount == null){
		amount = "0.00";
	}
	
	/*decPos = amount.indexOf(".");
   	if(decPos == -1) decPos = amount.length();
  	amountNumeric = amount.substring(0,decPos);
   	if(decPos != amount.length()) decPos++;
   	amountFractional = amount.substring(decPos);*/
   	

	ddpayCode = codeLstPayType.toPullDown("dpayCode",EJBUtil.stringToNum(paymentCode));			 
    }
    
    
    
    if (mode.equals("N")){
    
	ddpayCode = codeLstPayType.toPullDown("dpayCode");
	
	}
	  	
%>
<Form name="payments" id="milepayments" method="post" action="updatemilepayments.jsp" onsubmit="ripLocaleFromAll(); if (validate(document.payments)== false) { setValidateFlag('false'); applyLocaleToAll(); return false; } else { setValidateFlag('true'); return true;}">
  <Input type="hidden" name="milepaymentId" value="<%=milepaymentId%>">  
   
   
 
 
 
 <table width="70%">
  <tr>
  	<td width="15%"> <%=LC.L_Type%><%--Type*****--%> <FONT class="Mandatory">*</FONT></td>
	<td align=left colspan=2 >
		<%=ddpayCode%>
	</td>
  </tr>
 
  <tr>
  	<td width="15%"> <%=LC.L_Description%><%--Description*****--%> </td>
	<td align=left colspan=2 >
		<input type=text name=description size=50 maxlength=250 class="leftAlign" value="<%=description%>">
	</td>
  </tr>

  <tr>
  	<td><%=LC.L_Payment_Date%><%--Payment Date*****--%> <FONT class="Mandatory">*</FONT></td>
		<td width="10%" align=left>
<%-- INF-20084 Datepicker-- AGodara --%>		
			<input type=text name=date size=10  class="leftAlign datefield" value="<%=date%>" READONLY>
		</td>
    </tr>

  <tr>
  <td> <%Object[] arguments1 = {currency}; %><%=VelosResourceBundle.getLabelString("L_Amount_In",arguments1)%><%--Amount (in <%=currency%>)*****--%><FONT class="Mandatory">* </FONT>  </td>
 <td align=left>
 	<INPUT NAME="amount" TYPE="hidden" value="<%=amount%>" SIZE=20>
	<input type=text data-unitsymbol="" data-formatas="currency" name=amountNum size=14 maxlength=14  class="leftAlign numberfield" value=<%=amount%>>
 </td>
 <!-- 
  <td>
   .
	<input type=text name=amountFrac size=3 maxlength=2  class="leftAlign" value=<%=amountFractional%>>
 </td> -->
 
 </tr>
 
   <tr>
  	<td width="15%"> <%=LC.L_Comments%><%--Comments*****--%></td>
	<td align=left colspan=2 >
		<textarea name="comments" rows=4 cols=40><%=comments%></textarea>
	</td>
  </tr>
 
 
 </table> 
 
 <jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="N"/>
		<jsp:param name="formID" value="milepayments"/>
		<jsp:param name="showDiscard" value="N"/>
 </jsp:include>

<input type=hidden name=addFlag>    
   
<Input type="hidden" name="studyId" value="<%=studyId%>">
<Input type="hidden" name="mode" value="<%=mode%>">
<Input type="hidden" name="srcmenu" value="<%=srcmenu%>">
<Input type="hidden" name="selectedTab" value="<%=selectedTab%>">
    

  </Form>

<%
}


else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right



}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>

<%--
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<div class ="mainMenu" id="emenu"> 
  	<jsp:include page="getmenu.jsp" flush="true"/>
</div>
--%>

<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</body>

</html>

