<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>

<% 
String catLibType = request.getParameter("type");
%>

<title>
<% if (catLibType.equals("C")) 
{%>
<%=LC.L_Fld_Cat%><%--Field Category*****--%>
<%} else if (catLibType.equals("L")){%> 	  
<%=LC.L_Cal_Type%><%--Calendar Type*****--%>
<%}else{ %>
<%=LC.L_Form_Type%><%--Form Type*****--%>
<%}%>

</title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:include page="include.jsp" flush="true"/>
</head>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
 <SCRIPT Language="javascript">

 function  validate(formobj){
 
    if (!(validate_col('Category Name',formobj.categoryName))) return false
    //if (!(validate_col('e-Signature',formobj.eSign))) return false

	var valFlag = allfldquotecheck(formobj);
	if(valFlag!=undefined && !valFlag==true){
		return false;
	}

	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   	}	

    if(document.getElementById("eSignMessage").innerHTML != "<%=LC.L_Valid_Esign%>") {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
	}

   }

</SCRIPT> 

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id="fieldcategoryB" scope="request" class="com.velos.eres.web.catLib.CatLibJB"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,java.util.*,java.io.*,org.w3c.dom.*"%><%@page import="com.velos.eres.service.util.*"%>


<% String src;

src= request.getParameter("srcmenu");

%>
<body>

<DIV class="popDefault" id="div1"> 

  <%

	HttpSession tSession = request.getSession(true); 
	String categoryName ="";
	String categoryDesc ="";
	 String cCalledFrom = "";
	String mode=request.getParameter("mode");
	//Modified By Parminder Singh Bug#10693
	cCalledFrom = ((request.getParameter("cCalledFrom")) == null)?"P":request.getParameter("cCalledFrom");
	int pageRight = 0;	
	
	int catLibId=0;
	
	if (sessionmaint.isValidSession(tSession))
	{
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
	<jsp:include page="include.jsp" flush="true"/>
<%		
	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
	if (catLibType.equals("C")) 
	{
		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MFLDLIB"));
	} else if (catLibType.equals("T")) 
	{
 		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MFRMLIB"));
	} else if (catLibType.equals("L"))
	{
	 pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));
	}
	

	if (pageRight >=4) 

	{
		
		String txtname="";
		String txtdesc="";

		if (mode.equals("M"))
		{
			catLibId= EJBUtil.stringToNum(request.getParameter("catLibId"));		
			fieldcategoryB.setCatLibId(catLibId);
			fieldcategoryB.getCatLibDetails();
			txtname=fieldcategoryB.getCatLibName();
			txtdesc=fieldcategoryB.getCatLibDesc();
		} //mode=M
		txtname=(txtname==null)?"":txtname;
		txtdesc=(txtdesc==null)?"":txtdesc;
		
%>
 
 <table width="100%" cellspacing="0" cellpadding="0" border="0" class="basetbl"><tr><td>
  <P class="sectionHeadings">  
	  <%if (catLibType.equals("C")){%>
	      <%=MC.M_FldLib_Cat%><%--Field Library >> Category*****--%> 
	      <% } else if (catLibType.equals("L")){ %>
	      <%=MC.M_CalLib_CalType%><%--Calendar Library >> Calendar Type*****--%>
	  <%} else {%>
		  <%=MC.M_FrmLib_FrmType%><%--Form Library >> Form Type*****--%>
      <%}%>
	  </P>
  

  <P class="defComments_txt"> 
  <%if (catLibType.equals("C")){%> 	  
	  <%=MC.M_EtrFlw_CatDet%><%--Please enter the following Category Details*****--%>
  <%}else if (catLibType.equals("L")) {%>   
    <%=MC.M_EtrFlw_CalTypDet%><%--Please enter the following Calendar Type Details*****--%>
  <%}else {%>
	  <%=MC.M_EtrFlw_FrmTypDet%><%--Please enter the following Form Type Details*****--%>
  <%}%>	  
  </P>
</td></tr></table>
 
  <Form name="fieldcategory" id="fieldcatform" method="post" action="updatefieldcategory.jsp" onSubmit="if (validate(document.fieldcategory)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}" >


  	  
  <table width="100%" cellspacing="2" cellpadding="0" border="0" class="basetbl">
      <tr> 
        <td width="20%">
        <%if (catLibType.equals("C")){%> 	   
		    <%=LC.L_Cat_Name%><%--Category Name*****--%>
		 <% }else if (catLibType.equals("L")) {%>   
		   <%=LC.L_Cal_Type%><%--Calendar Type*****--%>
        <%}else{%>
		   <%=LC.L_Form_Type%><%--Form Type*****--%>
	    <%}%>
        <FONT class="Mandatory">* </FONT> </td>
        <td width="30%"> 
          <input type="text" name="categoryName" size = 40 MAXLENGTH = 100 value="<%=StringUtil.htmlEncodeXss( txtname ) %>">
        </td>
      </tr>
      <tr> 
        <td width="30%">
	    <%if (catLibType.equals("C")){%> 
	       <%=LC.L_Cat_Desc%><%--Category Description*****--%>
	     <%}else if (catLibType.equals("L")) {%>   
	      <%=LC.L_Cat_Desc%><%--Type Description*****--%>
		<%}else{%>
		<%=LC.L_Cat_Desc%><%--Type Description*****--%>
        <%}%>
  </td>
        <td width="20%"> 
	 <input type="text" name="categoryDesc" size = 40 MAXLENGTH = 255 value="<%=StringUtil.htmlEncodeXss(txtdesc)%>">
        </td>

	  <td>
	  <input name="mode" type=hidden value=<%=mode%>></td>
	  <input name="catLibId" type=hidden value=<%=catLibId%>>
	  <input name="type" type="hidden" value=<%=catLibType%>>	  	  
	  <!-- Modified By Parminder singh Bug#10693-->
	  <input name="cCalledFrom" type="hidden" value=<%=cCalledFrom%>>	
	  <td>
  </tr>
  
  <br>
	  <tr> <td colspan="4">
<%if((pageRight == 5 && mode.equals("N")) || (pageRight == 6 && mode.equals("M")) || pageRight == 7){%>

	 
	  <jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="fieldcatform"/>
			<jsp:param name="showDiscard" value="N"/>
	  </jsp:include>	 -
	 <%--  <table align="center"><tr>
	  <td align="center">
				<button type="submit" id="submit_btn" ondblclick="return false"><%=LC.L_Submit%></button>
			</td> 
	  </tr></table>--%>
	  
	  <%}%>
	</td> </tr>
</table>


</Form>
  <%

	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
 <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

</body>

</html>


