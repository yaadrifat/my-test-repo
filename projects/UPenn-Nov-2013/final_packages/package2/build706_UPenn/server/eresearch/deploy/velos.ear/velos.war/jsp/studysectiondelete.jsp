<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%
boolean isIrb = "irb_upload_tab".equals(request.getParameter("selectedTab")) ? true : false;
String includeTabsJsp = isIrb ? "irbnewtabs.jsp" : "studytabs.jsp";
if (isIrb) {
%>
<title><%=MC.M_ResComp_NewUpldDocu%><%--Research Compliance >> New Application >> Upload Documents*****--%></title>
<% } else { %>
<title><%=LC.L_Sec_Del%><%--Section Delete*****--%></title>
<% } %>

<%-- agodara 08Aug11: Removed for Bug #6728, already using Whichcss_skin.js in panel.jsp --%>

<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
}
</SCRIPT>

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<%@ page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC,com.aithent.audittrail.reports.AuditUtils"%>
<%@ page language = "java" import="java.util.*,com.velos.eres.service.util.EJBUtil"%>
<jsp:useBean id="sectionB" scope="request" class="com.velos.eres.web.section.SectionJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<% String src;
src= request.getParameter("srcmenu");
%>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<BODY>
<br>
<%
String from = "sectionver";
%>

<DIV class="formDefault" id="div1">
  <jsp:include page="<%=includeTabsJsp%>" flush="true">
  <jsp:param name="from" value="<%=from%>"/>
  </jsp:include>

<%
	String studySectionId= "";
	String selectedTab="";
	String studyVerId = "";

HttpSession tSession = request.getSession(true);
 if (sessionmaint.isValidSession(tSession))	{
		studySectionId= request.getParameter("studySectionId");
		selectedTab=request.getParameter("selectedTab");
		studyVerId = request.getParameter("studyVerId");

		int ret=0;
		String delMode=request.getParameter("delMode");

		if (delMode.equals("null")) {
			delMode="final";
%>

	<FORM name="studysectiondelete" id="stdSecDelFrm" method="post" action="studysectiondelete.jsp" onSubmit="if (validate(document.studysectiondelete)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

	<P class="defComments"><%=MC.M_EsignToProc_WithStdDel%><%--Please enter e-Signature to proceed with <%=LC.L_Study%> Section Delete*****--%></P>


	<jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="stdSecDelFrm"/>
		<jsp:param name="showDiscard" value="N"/>
	</jsp:include>



 	 <input type="hidden" name="delMode" value="<%=delMode%>">
  	 <input type="hidden" name="srcmenu" value="<%=src%>">
  	 <input type="hidden" name="studySectionId" value="<%=studySectionId%>">
   	 <input type="hidden" name="selectedTab" value="<%=selectedTab%>">
   	 <input type="hidden" name="studyVerId" value="<%=studyVerId%>">

	</FORM>
<%
	} else {
			String eSign = request.getParameter("eSign");
			String oldESign = (String) tSession.getValue("eSign");
			if(!oldESign.equals(eSign)) {
%>
 		  <jsp:include page="incorrectesign.jsp" flush="true"/>
<%
			} else {
			// Modified for INF-18183 ::: AGodara 
			ret = sectionB.studySectionDelete(EJBUtil.stringToNum(studySectionId),AuditUtils.createArgs(tSession,"",LC.L_Study));     // Fixed Bug#7604 : Raviesh
			if (ret==-2) {%>
				<br><br><br> <p class = "successfulmsg" align = center> <%=MC.M_Data_NotRemSucc%><%--Data not removed successfully*****--%> </p>
			<%}else { %>
				<br><br><br><br><br> <p class = "successfulmsg" align = center> <%=MC.M_Data_RemSucc%><%--Data removed successfully*****--%> </p>
				<META HTTP-EQUIV=Refresh CONTENT="1; URL=sectionBrowserNew.jsp?mode=M&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&studyVerId=<%=studyVerId%>">

			<%}
			} //end esign
	} //end of delMode
  }//end of if body for session
else { %>
 <jsp:include page="timeout.html" flush="true"/>
 <% } %>

 <div class = "myHomebottomPanel">
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

</DIV>

<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/>
</div>

</body>
</HTML>


