<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>
<title><%=LC.L_Pcol_Cal%><%--Protocol calendars*****--%></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<% String src;

src= request.getParameter("srcmenu");

%>


<body>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="objShareB" scope="request" class="com.velos.eres.web.objectShare.ObjectShareJB"/>

<%@ page language = "java" import = "com.velos.eres.business.study.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%><%@page import="com.velos.eres.service.util.*"%>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<DIV class="popDefault" id="div1"> 

<%
String eventId="";
String msg="";
String userId="";
String sharedWith="";
	String sharedWithIds="";
	String objNumber ="";
	String fkObj = "";

	String eSign = request.getParameter("eSign"); // Amar

HttpSession tSession = request.getSession(true); 
if (sessionmaint.isValidSession(tSession))
 {
 %>
   <jsp:include page="sessionlogging.jsp" flush="true"/> 

 <!--Added By Amarnadh for issue # 3145 -->
  <%
	 String oldESign = (String) tSession.getValue("eSign");
	 if(!oldESign.equals(eSign))
	 {
	%>
		 <jsp:include page="incorrectesign.jsp" flush="true"/>
	<%
	 }
	else
	 {

	String studyId = (String) tSession.getValue("studyId");
	userId = (String) tSession.getValue("userId");
	String ipAdd = (String) tSession.getValue("ipAdd");
	String mode=request.getParameter("mode");
	mode=(mode==null)?"":mode;
	
	String name=StringUtil.stripScript(request.getParameter("name"));
	name=(name==null)?"":name;
	
	String desc=request.getParameter("desc");
	desc=(desc==null)?"":desc;
	
	String type=request.getParameter("caltype");
	type=(type==null)?"":type;
	
	
	
	int ret = 0;
	eventId=request.getParameter("eventid"); 
	sharedWith="";
	sharedWithIds="";
	objNumber =request.getParameter("objNumber");
	fkObj = request.getParameter("fkObj");
	sharedWith=request.getParameter("rbSharedWith");
	sharedWith = (sharedWith == null)?"":sharedWith;
	sharedWithIds="";

	if(sharedWith.equals("P"))
	{
		sharedWithIds=request.getParameter("selUsrId");
	}
	else if(sharedWith.equals("A"))
	{
		sharedWithIds=request.getParameter("selAccId");
	}
	else if(sharedWith.equals("G"))
	{
		sharedWithIds=request.getParameter("selGrpIds");
	}
	else if(sharedWith.equals("S"))
	{
		sharedWithIds=request.getParameter("selStudyIds");
	}
	else if(sharedWith.equals("O"))
	{
	 sharedWithIds=request.getParameter("selOrgIds");
	}	
%>
	
<%
	ret=eventdefB.copyProtToLib(eventId,studyId,name,desc,type,sharedWith,userId,ipAdd);
%>
<BR>
<BR>


<%
	if (ret== -1 ) {
		Object[] arguments1 = {name};
		msg = VelosResourceBundle.getMessageString("M_CalNameDupli_BackBtnToChgNew",arguments1); /*"The Calendar Name "{0}" already exists. Click on the "Back" button below to enter a unique Calendar Name." ;*****/
%>

  <p class="defComments" align="center">
  	<Font class="mandatory">
  		<%=msg%>
  	</Font>
  </p>
  <br>
  <table width="100%" cellspacing="0" cellpadding="0">
  <tr>
      <td align=center> 
		<button onclick="window.history.back(); return false;"><%=LC.L_Back%></button> 
      </td> 
      </tr>
  </table>
<%
   }	
	else {
				objShareB.setIpAdd(ipAdd);
	         	objShareB.setCreator(userId);
	         	objShareB.setObjNumber("3");//number of module
	         	objShareB.setFkObj(new Integer(ret).toString());// formId, CalId, reportId
	         	objShareB.setObjSharedId(sharedWithIds);// shared ids
	         	objShareB.setObjSharedType(sharedWith);// U-user id,A -account id,S - study id,G - group id,O - organization id 
//System.out.println("sharedWith="+sharedWith+" sharedWithIds="+sharedWithIds);	         		
	         	objShareB.setRecordType(mode);
	         	objShareB.setModifiedBy(userId);	
	         	ret = objShareB.setObjectShareDetails();
		msg = MC.M_Data_SavedSucc/*"Data saved successfully"*****/;
%>
  	 <BR>
  	 <BR>
     <BR>
  	 <BR>
  	  <p class = "successfulmsg" align = center > <%=msg%> </p>
	  <script>
			window.opener.location.reload();
			setTimeout("self.close()",1000);
	  </script>	  
	
  
<%
	 }
}//end of if body for session
 }
else

{
%>
  <jsp:include page="timeout.html" flush="true"/>
  
<%

}
 
%>

</div>


</BODY>

</HTML>
