<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML> 

<HEAD>
	<LINK REL="SHORTCUT ICON" HREF="./favicon.ico">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<TITLE> <%=LC.L_Reg_Complete%><%--Registration Complete*****--%> </TITLE>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

</HEAD>
<link rel="stylesheet" href="styles/velos_style.css" type="text/css">
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff">
<jsp:useBean id="signupB" scope="request" class="com.velos.eres.web.accountWrapper.AccountWrapperJB"/>
<jsp:useBean id="userB" scope="request" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id="addressB" scope="request" class="com.velos.eres.web.address.AddressJB"/>
<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,com.velos.eres.service.util.*,java.lang.*,java.util.*,java.text.*"%>



<%--
	com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*
--%>
<%

	



	String ipAdd = request.getRemoteAddr();

	String loginName = request.getParameter("loginName");
	String email= request.getParameter("email");
	String notes= request.getParameter("notes");
	String smtpHost = "";
	String domain = "";
	String messageTo = "";
	String userEmail = "";
	String messageFrom = "";
	String messageText = "";
	String messageSubject = "";
	String messageHeader = "";
	String messageFooter = "";		
	String completeMessage = "";
	
	int ctrlrows = 0;
	int rows = 0;
	
	 String userMailStatus = "";
 int metaTime = 1;


%>

	




	<table border="0" cellspacing="0" cellpadding="0" width="770">

	 <!-- HEADER OBJECT START-->
	<tr>
	  <td colspan="14"><a href="http://www.velos.com"><img src="./images/toplogo.jpg" width="770" height="60" alt="<%=LC.L_Velos_Eres %><%-- Velos eResearch*****--%>" border="0"></a></td>
	</tr>
	<tr>
	  	  <td align="right"><img src="./images/toplefcurve.gif" width="18" height="16"></td> 
          <td><img src="./images/vspacer.gif" width="201" height="16" border="0"></td> 
          <td><img src="./images/topmidcurve.gif" width="30" height="16" border="0"></td>
          <td width="85" align="center" bgcolor="9999FF"><a href="index.html" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_5,250,77);" ><img name="Home" src="./images/home.gif" height="16" border="0" alt="<%=LC.L_Home %><%-- Home*****--%>"></a></td>
          <td><img src="./images/mspacer.gif" width="2" height="16" border="0"></td>
          <td width="85" align="center" bgcolor="9999FF"><a href="ataglance.html" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_0,335,77);" ><img name="About" src="./images/about.gif" height="16" border="0" alt="<%=LC.L_About %><%-- About*****--%>"></a></td>
          <td><img src="./images/mspacer.gif" width="2" height="16" border="0"></td>
          <td width="85" align="center" bgcolor="9999FF"><a href="products.html" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_1,420,77);" ><img name="vpopmenu_r2_c3" src="./images/solutions.gif" height="16" border="0" alt="<%=LC.L_Solutions %><%-- Solutions*****--%>"></a></td>
          <td><img src="./images/mspacer.gif" width="2" height="16" border="0"></td>
          <td width="85" align="center" bgcolor="9999FF"><a href="demo.html" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_2,515,77);" ><img name="vpopmenu_r2_c4" src="./images/demo.gif" height="16" border="0" alt="<%=LC.L_Demo %><%-- Demo*****--%>"></a></td>
          <td><img src="./images/mspacer.gif" width="2" height="16" border="0"></td>
          <td width="85" align="center" bgcolor="9999FF"><a href="contact.html" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_3,597,77);" ><img name="vpopmenu_r2_c6" src="./images/contact.gif" height="16" border="0" alt="<%=LC.L_Contact %><%-- Contact*****--%>"></a></td>
          <td><img src="./images/mspacer.gif" width="2" height="16" border="0"></td>
          <td width="85" align="center" bgcolor="9999FF"><a href="register.jsp" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_4,683,77);" ><img name="vpopmenu_r2_c7" src="./images/register.gif" height="16" border="0" alt="<%=LC.L_Register %><%-- Register*****--%>"></td>
     </tr>
	 </table>
	 <!-- HEADER OBJECT END -->


	 
	   <table width="770" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
	   		  <tr>

		   	   <!-- LIBRARY OBJECT START-->
	  	      <td width="1" bgcolor="9999ff"><img src="./images/sspacer.gif" width="1">				  
		      </td>
		   	   <!-- LIBRARY OBJECT END-->

			   <td>
			   <table cellspacing="0" cellpadding="5">
			   <tr>
			  	  <td width="125" valign="top" background="./images/leftsidebg.gif">
				  </td>
			  	  <td align="center" width="750">
				  <p> <%
				  int output=0;
				  String printout="";
				output=	userB.findUser(loginName);
				if(output == -1){
				 printout=MC.M_YouEtr_InvalidUname/*" You have entered Invalid User Name"*****/; 
				}
				else{
			      int userPerAddressId;
				userPerAddressId=EJBUtil.stringToNum(userB.getUserPerAddressId());
				addressB.setAddId(userPerAddressId);
				addressB.getAddressDetails();
				String emailID="";
				emailID=addressB.getAddEmail();
				if(emailID.equals(email)){
				 printout=MC.M_NotifyCustSupp_PwdEmailed/*"We have sent a notification to Customer Support. Your password will be emailed to you after verification."*****/;  
				   
					CtrlDao userCtrl = new CtrlDao();	
					userCtrl.getControlValues("eresuser");
					rows = userCtrl.getCRows();
					if (rows > 0)
					   {
					   	messageFrom = (String) userCtrl.getCDesc().get(0);
					   }  	 
					Object[] arguments = {loginName};
					messageSubject = VelosResourceBundle.getMessageString("M_FgotPwd_UsrNme",arguments);/*messageSubject = "Forgot Password: User name -> " + loginName;*****/	
					messageHeader =LC.L_Fgot_Pwd;/*"\n Forgot password: \n\n" ;*****/ 

					/*messageText = "Hi Velos eResearch customer support,\n I forgot my password, please send me a new password. \n\n Login ID:" + loginName + "\n\n Email address:" + email + "\n\n Notes:" + notes + " \n\n\nThanks " + loginName;*****/
					Object[] arguments1 = {loginName,email,notes,loginName};
					messageText =VelosResourceBundle.getMessageString("M_VelosNewPwd_Thanks",arguments1); 
					completeMessage = messageHeader  + messageText + messageFooter ;
			try{
					 VMailer vm = new VMailer();
                     VMailMessage msgObj = new VMailMessage();
                    				
                	 msgObj.setMessageFrom(emailID);
                	 msgObj.setMessageFromDescription(MC.M_VeResCustSrv);/*msgObj.setMessageFromDescription("Velos eResearch Customer Services");*****/
                	  msgObj.setMessageTo(messageFrom);
                	 msgObj.setMessageSubject(messageSubject);
                	 msgObj.setMessageSentDate(new Date());
                	 msgObj.setMessageText(completeMessage);
                	 vm.setVMailMessage(msgObj);
                	 userMailStatus = vm.sendMail(); 			
				  }
				 catch(Exception  m)
				  {
		    		 userMailStatus = m.toString();
				  }
/////////////////////////////////////////////////////// end of mail data
				 }
				 else{
				 
				  printout=MC.M_YouEtrd_IncrtEmail/*"Your have entered Incorrect Email Id "*****/;
				  
				  }}%>
				  				  
				  	 <br><br><br><br><br><br><br><br><br><br><br>
					<%
                        if ( EJBUtil.isEmpty(userMailStatus) )
                        {
                        	metaTime = 10;
                      	
                        %>
                      	<p class = "sectionHeadings" align = center> <%=printout%> </p>
                      
                       <%
                      	 } else
                      	{
                      	 	metaTime = 15;
                           %>
                      		<p class = "redMessage" align = center> <%=MC.M_ErrSending_Notfic%><%-- There was an error in sending notification to Customer Support.<br>
                      		Please contact Customer Support with the following message*****--%>:<BR><%=userMailStatus%></p>
                      
                      	 <%
                        		}
                      	%>				  	 
					 
					 <br><br><br><br><br><br><br><br><br><br><br> 
				  </p>
		   	   <META HTTP-EQUIV=Refresh CONTENT="<%=metaTime%>; URL=index.html">
				  </td>
			   </tr>
				  </table>
				  </td>
		   	   <!-- LIBRARY OBJECT START-->
	  	      <td width="1" bgcolor="9999ff"><img src="./images/sspacer.gif" width="1">				  
		      </td>
		   	   <!-- LIBRARY OBJECT END-->
			  </tr>
	   </table>
	   

	   
	   
	  <!-- FOOTER OBJECT START-->
	   <table width="770" cellspacing="0" cellpadding="0" border="0">
	   		  <tr>
			  	  <td rowspan="2"><img src="images/copyright.gif" width="307" height="32" border="0" alt="<%=LC.L_Velos_Copyright %><%-- Velos copyright*****--%>"></td>
          		  <td colspan="6" bgcolor="#FFFFFF"><img src="./images/spacer.gif" width="308" height="16" border="0"></td>
	  	  		  <td align="right"><img src="./images/botrigcurve.gif" width="18" height="16"></td> 
			  </tr>

		<tr>
          <td bgcolor="9999ff"><img src="./images/sspacer.gif" width="8" height="16" border="0"></td>
		  <td><a href="termsofservice.html"><img name="Terms of service" src="./images/termsofservice.gif" width="150" height="16" border="0" alt="<%=LC.L_Terms_OfService %><%-- Terms of Service*****--%>"></a></td>
          <td bgcolor="9999ff"><img src="./images/sspacer.gif" width="1" height="16" border="0"></td>
          <td><img src="./images/mspacer.gif" width="2" height="16" border="0"></td>
          <td><a href="privacypolicy.html"><img name="Privacy policy" src="./images/privacypolicy.gif" width="150" height="16" border="0" alt="<%=LC.L_Privacy_Policy %><%-- Privacy policy*****--%>"></a></td>
          <td><img src="./images/mspacer.gif" width="2" height="16" border="0"></td>
          <td><a href="sitemap.html"><img name="Site map" src="./images/sitemap.gif" width="150" height="16" border="0" alt="<%=LC.L_Sitemap %><%-- Sitemap*****--%>"></a></td>
		</tr>
	   </table>
	   <table width="770" cellspacing="0" cellpadding="0" border="0">
        <tr>
          <td colspan="7"><img src="./images/botbar.gif" width="770" height="8" border="0"></td>
        </tr>
	   </table>
	 <!-- FOOTER OBJECT END-->
	   
	
	
	   
	   




</body>
</HTML>
 








