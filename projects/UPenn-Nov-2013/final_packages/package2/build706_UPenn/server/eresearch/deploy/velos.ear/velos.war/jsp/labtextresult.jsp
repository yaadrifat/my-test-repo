<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!-- saved from url=(0022)http://internet.e-mail -->
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML><HEAD><TITLE><%=LC.L_Long_Results%><%--Long Results*****--%></TITLE>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

</HEAD>

<script>

function validate(formobj) {

    count=formobj.count.value;
    totcount=formobj.totcount.value;
if (totcount==1){
	window.opener.document.patlab.longresult.value=formobj.longresult.value;
}else {
	window.opener.document.patlab.longresult[count].value=formobj.longresult.value;
}
	this.close();
}
 
 
</script>

<BODY>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@page import="com.velos.eres.service.util.*"%>
	<!--<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%>-->

<jsp:include page="include.jsp" flush="true"/>
<%
	HttpSession tSession = request.getSession(true); 
	 if (sessionmaint.isValidSession(tSession))
	{
		String count=request.getParameter("count");
		String totcount=request.getParameter("totcount");
			
	 %>


<Form class=formDefault id="labtxtresult" name="result" action="" method="post" onSubmit="if (validate(document.result)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}" >


	<BR>
	<input type="hidden" name="count" value="<%=count%>">
				<input type="hidden" name="totcount" value="<%=totcount%>">
	<table >
	<tr>

	<td class=tdDefault> <%=LC.L_Long_Results%><%--Long Results*****--%> </td>
	<td><textarea rows="10" name="longresult" cols="59"></textarea></td>
	</tr>
	
	   </table>		
	<br>
	<jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="N"/>
			<jsp:param name="formID" value="labtxtresult"/>
			<jsp:param name="showDiscard" value="N"/>
	</jsp:include>
		   <script>document.result.longresult.value=window.opener.document.patlab.hidelongresult.value</script>
	</form>
<%}else{%>
<jsp:include page="timeout_childwindow.jsp" flush="true"/> 
<%}%>

<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
