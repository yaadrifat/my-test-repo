<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=MC.M_Save_FrmToLib%><%-- Save Form to Library*****--%></title>

<%@ page import="java.util.*,java.io.*,com.velos.eres.service.util.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:useBean id="catLibJB" scope="request"  class="com.velos.eres.web.catLib.CatLibJB"/>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

</head>

<SCRIPT Language="javascript">


function checkPermission(pageRight)
{
   return f_check_perm(pageRight,'N');
}

 function  validate(formobj){

  // modified code for fixing the defect 1931, 2868 KN
   // modified code for fixing the defect #3006
	var val = formobj.name.value ;
	var formStatus = formobj.formStatus.value;
	/* Bug#10963 09-Aug-2012 -Sudhir*/
	//if (!(allsplcharcheck(formobj.name.value))) return false;
	if(val.indexOf("\"")!=-1)
	  {/*alert("Special Character(\") not allowed for this Field");*****/
		var paramArray = ["\""];
		alert(getLocalizedMessageString("M_SplCharNotAllowed", paramArray));
		return false;
		}

	 if (!(validate_col('Form Name',formobj.name))) return false;

		if (!(validate_col('Form Type',formobj.formType))) return false;


		if(formStatus != "W"){
			if (!(validate_col('e-Signature',formobj.eSign))) return false;
		

	    	 if(isNaN(formobj.eSign.value) == true) {
	    		 alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
					formobj.eSign.focus();
					return false;
			}
		}
 	}
</SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<!-- Modified By Amarnadh for issue #3219-->
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.service.util.EJBUtil"%>

<body>
<DIV class="popDefault">

<%

	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))
	{
		String uName = (String) tSession.getValue("userName");
	//  Modified By Amarnadh
	//	String name=request.getParameter("name");
		String name=StringUtil.decodeString(request.getParameter("name"));
		name=(name==null)?"":name;

		String accId=(String)tSession.getAttribute("accountId");
		int iaccId=EJBUtil.stringToNum(accId);
		String formLibId = request.getParameter("formLibId");
		String formStatus = request.getParameter("formStatusInt");
		int formStatusInt = EJBUtil.stringToNum(request.getParameter("formStatusInt"));
		CodeDao cd = new CodeDao();
		String frmStat =cd.getCodeSubtype(formStatusInt);
		ArrayList formTypeIds = new ArrayList();
		ArrayList formTypeNames = new ArrayList();
		int formTypeId=0;
		String catLibType = "T";
		String formTypeStr="";
		int forLastAll = -1; //Don't pass 0 because it will create a DD entry for 'ALL' ;
		CatLibDao catLibDao = new CatLibDao();
		catLibDao= catLibJB.getCategoriesWithAllOption(iaccId,catLibType,forLastAll);
		formTypeIds = catLibDao.getCatLibIds();
		formTypeNames= catLibDao.getCatLibNames();
		formTypeStr=EJBUtil.createPullDown("formType", formTypeId, formTypeIds, formTypeNames);

%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<jsp:include page="include.jsp" flush="true"/>
<Form name="protocolForm" id="protForm" method="post" action="saveformtolibrary.jsp" onsubmit="if (validate(document.protocolForm)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

   <input type ="hidden" name="selectedForms" value="<%=formLibId%>">
   <input type="hidden" name="formStatus" value="<%=frmStat%>">
   <table width="100%" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20%" ><%=LC.L_Frm_Name%><%-- Form Name*****--%> <FONT class="Mandatory" >* </FONT> </td>
        <td width="90%">
           <input type="text" name="name" size = 30 MAXLENGTH = 50 value="<%=name%>">
        </td>
      </tr>

	 <td width="20%" ><%=LC.L_Form_Type%><%-- Form Type*****--%><FONT class="Mandatory" >* </FONT> </td>
        <td width="90%">
			<%=formTypeStr%>
        </td>
</tr>
</table>

	<%	if(frmStat.equals("W")){	%>
	
				<table align="center"><tr>
	  <td align="center"><br />
				<button type="submit" id="submit_btn" ondblclick="return false"><%=LC.L_Submit%></button>
			</td> 
	  </tr></table>

<% }else{ %>
		<br />
		<jsp:include page="submitBar.jsp" flush="true">
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="protForm"/>
			<jsp:param name="showDiscard" value="N"/>
		</jsp:include>
			
<%} %>
  </Form>
<%
}
else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div class = "myHomebottomPanel">
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
</body>

</html>


