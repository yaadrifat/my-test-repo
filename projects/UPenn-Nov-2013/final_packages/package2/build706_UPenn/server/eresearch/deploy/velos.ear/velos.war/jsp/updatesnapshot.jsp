<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<TITLE> <%=LC.L_Update_StdSnapshot%><%--update Study Snapshot*****--%> </TITLE>
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
<body>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.service.util.*,java.lang.*,java.util.*,java.text.*"%>

<%

   HttpSession tSession = request.getSession(true);

   if (sessionmaint.isValidSession(tSession))

	{

%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%

	String src = request.getParameter("srcmenu");

	String calledfrom = request.getParameter("calledfrom");
	String study = request.getParameter("studyId");

	String tab = "";
	String right = "";
	String userType = "";

	if (calledfrom.equals("T"))
	{
		tab =  request.getParameter("selectedTab");
		right =  request.getParameter("right");
		userType =   request.getParameter("user");
	}

	//to update snapshot of the study
	%>
	<jsp:include page="getsnapshot.jsp" flush="true">

	<jsp:param name="studyId" value="<%=study%>"/>
	<jsp:param name="refreshFlag" value="T"/>

	</jsp:include>

	<%

%>
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
<p class = "successfulmsg" align = center> <%=MC.M_StdSnapshot_UpdtSucc%><%--The <%=LC.Std_Study%> Snapshot was updated successfully*****--%></p>

<%
	if (calledfrom.equals("H"))
	{
%>
	<META HTTP-EQUIV=Refresh CONTENT="2; URL=myHome.jsp?srcmenu=<%=src%>">
<%
	}
	else if (calledfrom.equals("T"))
	{
%>
	<META HTTP-EQUIV=Refresh CONTENT="2; URL=copy_stdy_for_user.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&right=<%=right%>&user=<%=userType%>">
<%
	}
	else if (calledfrom.equals("S"))
	{
%>
	<META HTTP-EQUIV=Refresh CONTENT="2; URL=userStudies.jsp?srcmenu=<%=src%>">
<%
	}

//end of if for eSign check
}//end of if body for session

else

{

%>
<jsp:include page="timeout.html" flush="true"/>
<%

}

%>
</body>
</HTML>
