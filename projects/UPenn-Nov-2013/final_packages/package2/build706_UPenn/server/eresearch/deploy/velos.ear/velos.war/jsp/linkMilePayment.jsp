<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>

<%@page import="com.velos.eres.web.milestone.MilestoneJB"%>

<%@page import="java.math.BigDecimal"%><HTML>
<HEAD>
<title><%=LC.L_Payment_Dets%><%--Payment Details*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
	<%-- Nicholas : Start --%>
	<%--<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>--%>
	<%-- Nicholas : End --%>
<SCRIPT LANGUAGE="JavaScript" src="calendar.js"></SCRIPT>
<script src="js/jquery/jquery-1.4.4.js"></script>
<SCRIPT  Language="JavaScript1.2">
$(window).load(function(){
	jQuery("html").css("overflow","auto");
	jQuery("BODY").css("overflow","auto");
});	
	function setSelected(formobj,counter)
	{

		var milestones;
		var selectedCount;

		milestones = parseInt(formobj.totalmilecount.value);
		selectedCount = parseInt(formobj.selectedCount.value);

		if (milestones == 1)
		{
			if (formobj.selectMilestone.checked)
			{
				formobj.isChecked.value="1";
				selectedCount = selectedCount + 1;
			}
			else
			{
				formobj.isChecked.value="0";
				selectedCount = selectedCount - 1;
			}
		}
		else
		{
			if (formobj.selectMilestone[counter].checked)
			{
				formobj.isChecked[counter].value="1";
				selectedCount = selectedCount + 1;
			}
			else
			{
				formobj.isChecked[counter].value="0";
				selectedCount = selectedCount - 1;
			}

		}

		if (selectedCount < 0)
		{
			selectedCount = 0;
		}

		formobj.selectedCount.value = selectedCount ;

	}


  function validate(formobj)
  {
		var ct;
		var checktemp;
		var isDecOrNumber;
		var i;
		var mileStoneId;
		var mileDetailCount;
		var d;
		var isChecked;
		var coltemp ;

		var selcount;

  	  	selcount = parseInt(formobj.selectedCount.value );

  	  	if (selcount <= 0)
  	  	{
  	  		alert("<%=MC.M_SelcAtleast_OneMstone%>");/*alert("Please select atleast one Milestone");*****/
  	  		return false;
  	  	}

		ct = parseInt(formobj.totalmilecount.value);// total number of milestones

		//Modified For Bug#10167 : Raviesh
		if (ct == 1)
		{
			isChecked = parseInt(formobj.isChecked.value);
			if (isChecked == 1)
			{
					if (!(isDecimalOrNumberValAndNotBlank(formobj.applyAmount.value) && isNegNum(formobj.applyAmount.value)))     //Akshi:Fixed bug#7887
						{
							alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid Number");*****/
							formobj.applyAmount.focus();
							return false;
						}
						
					// detail records
					mileStoneId	= formobj.linktoId.value;
					mileDetailCount = formobj.mileDetailCount.value;

					if (mileDetailCount == 1)
					{
						 eval("coltemp = formobj.applyAmount" + mileStoneId );
						 
						 eval("isDecOrNumber = isDecimalOrNumberValAndNotBlank(coltemp.value)" );
						 eval("isNeg = isNegNum(coltemp.value)");

							if (!(isDecOrNumber && isNeg))     //Akshi:Fixed bug#7887
							{
								alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid Number");*****/
								coltemp.focus();
								return false;
							}
							checktemp= "formobj.applyAmounttoHoldBack"+mileStoneId;
							if(checktemp.value != undefined || checktemp.value != null){
							eval("coltemp = formobj.applyAmounttoHoldBack" + mileStoneId );
							 
							 eval("isDecOrNumber = isDecimalOrNumberValAndNotBlank(coltemp.value)" );
							 eval("isNeg = isNegNum(coltemp.value)");

								if (!(isDecOrNumber && isNeg))     //Akshi:Fixed bug#7887
								{
									alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid Number");*****/
									coltemp.focus();
									return false;
								}
							}
					}
					else if (mileDetailCount > 1)
					{
						for ( d=0;d<mileDetailCount;d++)
						{
						    eval("coltemp = formobj.applyAmount" + mileStoneId + "["+d+"]");
							eval("isDecOrNumber = isDecimalOrNumberValAndNotBlank(coltemp.value)" );
							eval("isNeg = isNegNum(coltemp.value)");

							if (!(isDecOrNumber) && isNeg)     //Akshi:Fixed bug#7887
							{
								alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid Number");*****/
								coltemp.focus();
								return false;
							}
							checktemp= "formobj.applyAmounttoHoldBack"+mileStoneId;
							if(checktemp.value != undefined || checktemp.value != null){
							eval("coltemp = formobj.applyAmounttoHoldBack" + mileStoneId + "["+d+"]");
							eval("isDecOrNumber = isDecimalOrNumberValAndNotBlank(coltemp.value)" );
							eval("isNeg = isNegNum(coltemp.value)");

							if (!(isDecOrNumber) && isNeg)     //Akshi:Fixed bug#7887
							{
								alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid Number");*****/
								coltemp.focus();
								return false;
							}
							}
						}
					}
			} // for if checked ==1
		} // one milestone
		else if(ct > 1)
		{

		  for ( i=0; i < ct; i++)
			{

			isChecked = parseInt(formobj.isChecked[i].value);
			if (isChecked == 1) //validate only if milestone is selected
			{

				 	if (!(isDecimalOrNumberValAndNotBlank(formobj.applyAmount[i].value) && isNegNum(formobj.applyAmount[i].value)))         //Akshi:Fixed bug#7887
				 	{
					 	formobj.applyAmount[i].focus();
					 	alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid Number");*****/
						return false;
					}

					// for detail
					mileStoneId	= formobj.linktoId[i].value;
					mileDetailCount = formobj.mileDetailCount[i].value;



					if (mileDetailCount == 1)
					{
						 eval(" coltemp = formobj.applyAmount" + mileStoneId );
						 eval("isDecOrNumber = isDecimalOrNumberValAndNotBlank(coltemp.value)" );
						 eval("isNeg = isNegNum(coltemp.value)");

							if (!(isDecOrNumber && isNeg))        //Akshi:Fixed bug#7887
							{
								alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid Number");*****/
								coltemp.focus();
								return false;
							}
						checktemp= "formobj.applyAmounttoHoldBack"+mileStoneId;
						if(checktemp.value != undefined || checktemp.value != null){
						  eval(" coltemp = formobj.applyAmounttoHoldBack" + mileStoneId );
						  eval("isDecOrNumber = isDecimalOrNumberValAndNotBlank(coltemp.value)" );
						  eval("isNeg = isNegNum(coltemp.value)");

								if (!(isDecOrNumber && isNeg))        //Akshi:Fixed bug#7887
								{
									alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid Number");*****/
									coltemp.focus();
									return false;
								}
						}
		  			 }
					else if (mileDetailCount > 1)
						{
							for ( d=0;d<mileDetailCount;d++)
							{
								eval("coltemp = formobj.applyAmount" + mileStoneId + "["+d+"]");
								eval("isDecOrNumber = isDecimalOrNumberValAndNotBlank(coltemp.value)" );
								eval("isNeg = isNegNum(coltemp.value)");

								if (!(isDecOrNumber && isNeg))      //Akshi:Fixed bug#7887
								{
									alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid Number");*****/
									coltemp.focus();
									return false;
								}
							checktemp= "formobj.applyAmounttoHoldBack"+mileStoneId+ "["+d+"]";
							if(checktemp.value != undefined || checktemp.value != null){
								eval("coltemp = formobj.applyAmounttoHoldBack" + mileStoneId + "["+d+"]");
								eval("isDecOrNumber = isDecimalOrNumberValAndNotBlank(coltemp.value)" );
								eval("isNeg = isNegNum(coltemp.value)");

								if (!(isDecOrNumber && isNeg))      //Akshi:Fixed bug#7887
								{
									alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid Number");*****/
									coltemp.focus();
									return false;
								}
								}
							}
						}
				} // if isChecked==1
			 } // end of for
		}
		//End of Modification for Bug#10167

	  if (!(validate_col('e-Signature',formobj.eSign))) return false
	   if(isNaN(formobj.eSign.value) == true)
	   {
		   alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/

		formobj.eSign.focus();
		return false;

		  }
	  return true;
  }


</SCRIPT>

</HEAD>


<BODY>
  <jsp:useBean id="InvB" scope="request" class="com.velos.eres.web.invoice.InvoiceJB"/>
  <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
  <jsp:useBean id="PayB" scope="request" class="com.velos.eres.web.milepayment.PaymentDetailJB"/>
  <jsp:useBean id="MileJB" scope="request" class="com.velos.eres.web.milestone.MilestoneJB"/>

  <%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*"%>
  <jsp:include page="include.jsp" flush="true"/>
	<%-- Nicholas : Start --%>
	<jsp:include page="skinChoser.jsp" flush="true"/>
	<%-- Nicholas : End --%>
  <%



	String eSign = request.getParameter("eSign");
	String mode = request.getParameter("mode");

	String studyId = request.getParameter("studyId");
	String paymentPk = request.getParameter("paymentPk");
	String pageRight = request.getParameter("pR");
	String mileStoneIdStr = request.getParameter("mileStoneId");
	String mileType = "";
	if(mileStoneIdStr!=null)
	{
	MilestoneJB mileJB = new MilestoneJB();
	mileJB.setId(Integer.parseInt(mileStoneIdStr));
	mileJB.getMilestoneDetails();
	mileType = mileJB.getMilestoneType();
	}
	int mileIdParam = 0;
	mileIdParam = EJBUtil.stringToNum(mileStoneIdStr);


HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession))

   {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
   	String oldESign = (String) tSession.getValue("eSign");
	String eventId = (String) tSession.getValue("eventId");

	String ipAdd = (String) tSession.getValue("ipAdd");
	String usr = (String) tSession.getValue("userId");



%>

<%
if (StringUtil.isEmpty(mode))
{
	mode = "N";
}



%>

<SCRIPT>
//	fclose_to_role();
</SCRIPT>
 <!-- Get the achieved milestones for invoice -->
 <%
 int totalmilecount  = 0;
 int totalCounter = 0;


 	%>

 	<DIV class="popDefault"  >

  <jsp:include page="paymenttabs.jsp" flush="true">
	<jsp:param name="studyId" value="<%=studyId%>"/>
	<jsp:param name="selectedTab" value="2"/>
	<jsp:param name="paymentPk" value="<%=paymentPk%>"/>
	<jsp:param name="pR" value="<%=pageRight%>"/>
  </jsp:include>


	 	<form name="miledet" id="linkMilePayment" action="saveMilePaymentLink.jsp" method="post" onSubmit="ripLocaleFromAll(); if (validate(document.miledet)== false) { setValidateFlag('false'); applyLocaleToAll();return false; } else { setValidateFlag('true'); return true;}" >
		<input type="hidden" name="studyId" value="<%=studyId%>"/>
		<input type="hidden" name="paymentPk" value="<%=paymentPk%>"/>
		<input type="hidden" name="pR" value="<%=pageRight%>"/>

        <!--Fixed Bug#6937 : Raviesh-->
	 		<table width="100%" cellspacing="0" cellpadding="0" border="0" class="outline midAlign">
			<tr>
	        <th><%=LC.L_Select%><%--Select*****--%></th>
	        <th><%=LC.L_Milestone%><%--Milestone*****--%></th>
	        <th width="20%"><%=LC.L_Count%><%--Count*****--%> </th>
			<th width="20%"><%=LC.L_Holdback_Amt%><%--Holdback Amount*****--%></th>
	        <th width="20%"><%=LC.L_Mstone_Amt%><%--Milestone Amount*****--%></th>
	        <th width="20%"><%=MC.M_Applied_AmtToDate%><%--Applied Amount To Date*****--%></th>
	        <th width="80%"><%=MC.M_AddlAmt_ForPay%><%--Additional Applied Amount from this payment*****--%></th>
	      </tr>

 	<%
 		// iterate through 4 milestone types

		String[] arMilestoneType = null;
		String[] arMilestoneTypeHeader = null;

		if (mileIdParam <= 0)
		{
	 		//KM-to fix the issue-3328
			arMilestoneType =  new String[5];
	 		arMilestoneType[0] = "PM";
	 		arMilestoneType[1] = "VM";
	 		arMilestoneType[2] = "EM";
	 		arMilestoneType[3] = "SM";
	 		arMilestoneType[4] = "AM";//KM


	 		arMilestoneTypeHeader =  new String[5];
	 		arMilestoneTypeHeader[0] = LC.L_Pat_StatusMstones/*LC.Pat_Patient+" Status Milestones"*****/;
	 		arMilestoneTypeHeader[1] = LC.L_Visit_Mstones/*"Visit Milestones"*****/;
	 		arMilestoneTypeHeader[2] = LC.L_Evt_Mstones/*"Event Milestones"*****/;
	 		arMilestoneTypeHeader[3] = LC.L_Std_Mstones/*LC.Std_Study+" Milestones"*****/;
	 		arMilestoneTypeHeader[4] = LC.L_Addl_Mstones/*"Additional Milestones"*****/;
	 	} else
	 	{
	 		arMilestoneType =  new String[1];
	 		arMilestoneType[0] = LC.L_Dummy/*"dummy"*****/;

	 		 arMilestoneTypeHeader =  new String[1];
	 		arMilestoneTypeHeader[0] = LC.L_Milestone/*"Milestone"*****/+":";


	 	}


 		String displayMilestoneHeader ="";


 		// get previous data totals/or previous data entered for the same payment and invoice
 		PaymentDetailsDao pdao = new PaymentDetailsDao();
 		PaymentDetailsDao pdaoLevel1 = new PaymentDetailsDao();
 		int idxLevel1 = -1;
 		int idxLevel2 = -1;

 		boolean notReconciled = false;
 		int alreadyEnteredCount = 0;
 		String prevAppliedAmountTotal = "0.00";
 		float holdback=0;
 		double holdBackAppliedDueToDate=0f;
 		String prevAmount = "0.00";
 		double prevHoldbackAmt=0f;
 		String oldPk = "";
 		BigDecimal holdBackAppliedDueToDate1 = new BigDecimal("0.0");
		BigDecimal prevHoldbackAmt1 = new BigDecimal("0.0");
 		for (int t = 0; t <arMilestoneType.length;t++)
 		{


 			%>

 			<tr >
		        <td colspan = 7><p class="sectionHeadings"><%=arMilestoneTypeHeader[t]%></p></td>
		     </tr>

		 	<%
	 		MileAchievedDao mile = new MileAchievedDao();

	 		MilestoneDao milestoneDao = new MilestoneDao();
	 		ArrayList arMilestoneIds = new ArrayList();
	 		ArrayList arMilestoneRuleDescs = new ArrayList();

	 		ArrayList arMilestoneAchievedCounts = new ArrayList();
	 		ArrayList arCountPerMilestone = new ArrayList();
	 		ArrayList arMileAmounts = new ArrayList();
	 		ArrayList<Float> arLevel1HoldBack=new ArrayList<Float>();



	 		String mileStoneId = "";
	 		Hashtable htAchieved = new Hashtable();

	 		int milecount = 0;
	 		int mileDetailCount = 0;
	 		String mileAchCount = "0";
	 		String mileDesc = "";
		  int countPerMilestone  = 0;

		   double calcAmount = 0.00;
		   double mileAmount = 0.00;
		   double mileAmountForGroupMilestones = 0.00;
		   double mileDetailAmountsPrevInvoiced = 0.00;  		
		   double totalMilePrevInvoicedAmount = 0.00;
		   BigDecimal calcAmounts = new BigDecimal("0.0");
		   BigDecimal mileAmt = new BigDecimal("0.0");
	 		 if (mileIdParam <= 0)
				{
	 				mile = InvB.getAchievedMilestonesForUser(studyId, "","", "",arMilestoneType[t],usr);
				}
				else
				{
	 				mile = MileJB.getAchievedMilestones(mileIdParam,usr);
				}

	 		milestoneDao = mile.getMilestoneDao();
	 		htAchieved = mile.getHtAchieved();

	 		arMilestoneIds = milestoneDao.getMilestoneIds();
	 		arMilestoneRuleDescs = milestoneDao.getMilestoneRuleDescs();
	 		arMilestoneAchievedCounts = milestoneDao.getMilestoneAchievedCounts();
	 		arCountPerMilestone  = milestoneDao.getMilestoneCounts();
	 		arMileAmounts =  milestoneDao.getMilestoneAmounts();
	 		arLevel1HoldBack=milestoneDao.getHoldBack();



	 		if (arMilestoneIds != null)
	 		{
	 			milecount = arMilestoneIds.size();

	 		}
	 		else
	 		{
		 		milecount  = 0;
	 		}

	 		totalmilecount = totalmilecount + milecount;

	 		// for each milestone, get the milestone achieved details

	 		for (int i =0; i < milecount ; i++)
	 		{
	 				ArrayList arLevel1 = new ArrayList();
			 		ArrayList arLevel1Amount = new ArrayList();
			 		ArrayList arLevel1PrevAppliedTotal= new ArrayList();
			 		ArrayList arLevel1OldPk= new ArrayList();
			 		ArrayList arLevel1MileAchvd= new ArrayList();
			 		ArrayList<Float> holdBackAppliedDueToDateList=new ArrayList<Float>();
			 		ArrayList<Double> arPrevtotalAmt=new ArrayList<Double>();


					mileDesc = (String ) arMilestoneRuleDescs.get(i);
	 				mileStoneId = String.valueOf(((Integer)arMilestoneIds.get(i)).intValue());

				//old data

			 		pdao = PayB.getPaymentDetailsWithTotals(EJBUtil.stringToNum(paymentPk),EJBUtil.stringToNum(mileStoneId),"M");

			 		alreadyEnteredCount = pdao.getPaymentDetailRowCount();

			 		notReconciled = true;
			 		
			 		if (alreadyEnteredCount <= 0) //there was no payment data added for this invoice and payment, get any previous totals (all payments)
			 		{

			 	 		pdao = PayB.getPaymentDetailsWithTotals(0,EJBUtil.stringToNum(mileStoneId),"M");
			 			alreadyEnteredCount = pdao.getPaymentDetailRowCount();

			 		}

					prevAppliedAmountTotal = "0.00";
					prevAmount = "0.00";
					prevHoldbackAmt=0f;
					holdback=0;
					oldPk = "";

					holdBackAppliedDueToDate=0;
			 		if (alreadyEnteredCount > 0) // get data
			 		{
			 			//The control will be here if a payment is reconciled with any milestone/invoice/bugdet
			 			notReconciled = false;
			 			
			 			//get Main record

			 			prevAppliedAmountTotal =  (String )(pdao.getAppliedTotal()).get(0);
			 			holdBackAppliedDueToDate=pdao.getHoldBackAppliedDueToDateList().get(0);
			 			prevAmount = (String ) (pdao.getAmount()).get(0);
			 			prevHoldbackAmt=pdao.getPrevHoldBackAmount().get(0);
			 			if (StringUtil.isEmpty(prevAmount))
			 			{
			 				prevAmount = "0.00";

			 			}
			 			oldPk = (String ) (pdao.getId()).get(0);

			 			pdaoLevel1 = pdao.getArLevel1();

			 			arLevel1 = pdaoLevel1.getLinkToLevel1();
			 	 		arLevel1Amount = pdaoLevel1.getAmount();
			 	 		arPrevtotalAmt=	pdaoLevel1.getPrevHoldBackAmount();	
			 	 		arLevel1PrevAppliedTotal= pdaoLevel1.getAppliedTotal();
			 	 		arLevel1OldPk = pdaoLevel1.getId();
			 	 		arLevel1MileAchvd = pdaoLevel1.getLinkMileAchieved();
			 	 		holdBackAppliedDueToDateList=pdaoLevel1.getHoldBackAppliedDueToDateList();
			 	 		
			 		}



	 		////////////////



				MileAchievedDao ach = new MileAchievedDao();
				ArrayList patients = new ArrayList();
				ArrayList ids = new ArrayList();
				ArrayList patientCodes = new ArrayList();
				ArrayList achievedOn = new ArrayList();


				countPerMilestone =  EJBUtil.stringToNum((String) arCountPerMilestone.get(i));
				mileAchCount = (String) arMilestoneAchievedCounts.get(i);
				mileAmount = Double.parseDouble((String) arMileAmounts.get(i));
				if(arLevel1HoldBack!=null && arLevel1HoldBack.size()>0)
				holdback=	arLevel1HoldBack.get(i);
				if(Integer.parseInt(mileAchCount)>0)
				{
				if (countPerMilestone > 1)
				{
			 		mileAmountForGroupMilestones = mileAmount/countPerMilestone;
			 	}

				if (htAchieved.containsKey(mileStoneId))
				{


					ach = (MileAchievedDao) htAchieved.get(mileStoneId);
					ids = ach.getId();
					patients = ach.getPatient();
					mileDetailCount = ids.size();
					patientCodes = ach.getPatientCode();
					calcAmount = EJBUtil.stringToNum(mileAchCount) * mileAmount;
					calcAmounts=new BigDecimal(calcAmount);
					achievedOn = ach.getAchievedOn();


				}
				else
				{
					mileAchCount = "0";
					mileDetailCount = 0;
					calcAmount = 0.00;
					calcAmounts=new BigDecimal(calcAmount);
				}



				totalMilePrevInvoicedAmount = Math.rint(totalMilePrevInvoicedAmount * 100)/100;

				if(mileDesc.length() > 500) { //KM
					mileDesc = mileDesc.substring(0,500)+"...";
				}


	 			%>
	 			<tr>
		 			<td>
					<% if (mileIdParam <= 0)
					{ %>					
					<%
					String styleStr ="";
					if ((notReconciled && !isAccessibleFor(EJBUtil.stringToNum(pageRight),'N')) ||
					((!notReconciled) && !isAccessibleFor(EJBUtil.stringToNum(pageRight),'E'))){
						styleStr = "visibility:hidden";
					}
					%>
					
		 			<input type="checkbox" style="<%=styleStr %>" 
		 			onClick= "setSelected(document.miledet,'<%=totalCounter%>')" name="selectMilestone" value="<%=mileStoneId%>"/>
		 			<input type="hidden" name="isChecked" value="0"/>
		 			<% } else { %> <input type="hidden" name="isChecked" value="1"/> <% }%>
		 				 			</td>
		 			<td><%=mileDesc %></td>
		 			<td align="center"><%=mileAchCount%></td>
		 			<td align="center"><%=mileDetailCount==0?PayB.getHoldBackPercentage(holdback):""%></td>
		 			<td><span class="numberfield" data-unitsymbol="" data-formatas="currency"><%=calcAmounts%></span></td>
		 			<td><span class="numberfield" data-unitsymbol="" data-formatas="currency"><%=prevAppliedAmountTotal%></span></td>
		 			<!-- Mukul: BUG 4020 NumberUtil.roundOffNo() function used to roundoff number upto two decimal places -->

				 	<td>
				 		<%
						String styleStr ="";
						if ((notReconciled && !isAccessibleFor(EJBUtil.stringToNum(pageRight),'N')) ||
						((!notReconciled) && !isAccessibleFor(EJBUtil.stringToNum(pageRight),'E'))){
							styleStr = "READONLY";
						} %>
				 		<input type="text" <%=styleStr%> name="applyAmount" class="numberfield" data-unitsymbol="" data-formatas="currency" value="<%=prevAmount%>" <%=(styleStr!=null && styleStr!="")?"onfocus=\"this.blur();\"":""%>/>
				 		<input type= "hidden"  name="linktoId"  value="<%=mileStoneId%>" />
				 		<input type= "hidden"  name="countPerMilestone"  value="<%=countPerMilestone%>" />
				 		<input type= "hidden" class="numberfield" data-unitsymbol="" data-formatas="currency" name="mileAmountDue"  value="<%=calcAmount%>" />
				 		<input type= "hidden"  name="mileAchCount"  value="<%=mileAchCount%>" />

				 		<input type= "hidden" class="numberfield" data-unitsymbol="" data-formatas="currency" name="origmileAmount"  value="<%=mileAmount%>" />
			 			<input type= "hidden"  name="mileDetailCount" value="<%=mileDetailCount%>" />
			 			<input type= "hidden"  class="numberfield" data-unitsymbol="" data-formatas="currency" name="mileAmountForGroupMilestones"  value="<%=mileAmountForGroupMilestones%>" />
			 			<input type="hidden" name="linkType"  value="M" />
			 			<input type="hidden" name="oldPk"  value="<%=oldPk%>" />


				 	</td>

	 			</tr>

	 			<%

	 				for (int k = 0; k < mileDetailCount; k++)
	 				{
	 					// find data in level1 arrayLists
				 		  if (arLevel1 != null)
				 		  {
				 		    //modified by sonia, search for pk_mileachieved instead of patient
					 		  idxLevel1 = arLevel1MileAchvd.indexOf((String)ids.get(k));
				 		     if((arMilestoneType[t].equals("AM") && alreadyEnteredCount > 0) || mileType.equals("AM"))
				 		     {
				 		    	idxLevel1 = 1;
				 		     }

					 		  if (idxLevel1 >= 0)
					 		  {
					 			prevAmount = (String) arLevel1Amount.get(idxLevel1);
					 			prevHoldbackAmt=arPrevtotalAmt.get(idxLevel1); 
					  			oldPk = (String ) arLevel1OldPk.get(idxLevel1);
				  				prevAppliedAmountTotal = "0.00";
					  			prevAppliedAmountTotal =  (String ) arLevel1PrevAppliedTotal.get(idxLevel1); 
					  			holdBackAppliedDueToDate=holdBackAppliedDueToDateList.get(idxLevel1);
					 		  }
					 		  else
					 		  {
					 			 prevAmount = "0.00";
					 			 oldPk  = "0";
					 			prevAppliedAmountTotal = "0.00";
					 		  }
				 		  }


	 				%>
	 				<input type="hidden" name="level1Id<%=mileStoneId%>"  value="<%= (String)patients.get(k)%>" />
	 				<input type="hidden" name="linkType<%=mileStoneId%>"  value="P" />
	 				<input type="hidden" name="oldPk<%=mileStoneId%>"  value="<%=oldPk%>" />





		 				<%
						if(k%2==0){
					%>
						<TR class="browserEvenRow">
					<%
					}else{
					%>
						<TR class="browserOddRow">
					<%
					}
					%>
					 <td>&nbsp;</td>
		 			<td>&nbsp;</td>
		 			<td>&nbsp;&nbsp;&nbsp;<%= (String) patientCodes.get(k)%></td>
		 			<td align="center"><%= PayB.getHoldBackPercentage(holdback)%>
		 			<input type="hidden" name="level1ACHId<%=mileStoneId%>"  value="<%= (String)ids.get(k)%>" />
		 			</td>
		 			<td>
		 			<%
				 		if (countPerMilestone <= 1)
				 		{	
				 			mileAmt = new BigDecimal(PayB.getMilestoneAmountByType(holdback,mileAmount,PayB.ACTUAL_MILESTONE_AMOUNT));
				 		%>
				 			<span class="numberfield" data-unitsymbol="" data-formatas="currency"><%=mileAmt%></span>
				 			<input type="hidden" name="mileDetailAmount<%=mileStoneId%>" class="numberfield" data-unitsymbol="" data-formatas="currency" value="<%=mileAmount%>" />

				 		<%
				 		}
				 		else {
				 			mileAmt =new BigDecimal(PayB.getMilestoneAmountByType(holdback,mileAmountForGroupMilestones,PayB.ACTUAL_MILESTONE_AMOUNT));%>
					 		<span class="numberfield" data-unitsymbol="" data-formatas="currency"><%=mileAmt%></span>
					 		<input type="hidden" name="mileDetailAmount<%=mileStoneId%>" class="numberfield" data-unitsymbol="" data-formatas="currency" value="<%=mileAmountForGroupMilestones%>" />

				 		<%}

				 	%>
				 	
				 		</td>
		 			<td><span class="numberfield" data-unitsymbol="" data-formatas="currency"><%=prevAppliedAmountTotal%></span></td>
				 	<td><input type="text" <%=styleStr%> name="applyAmount<%=mileStoneId%>" class="numberfield" data-unitsymbol="" data-formatas="currency" value="<%=prevAmount%>" /></td>
		 			
		 			</tr>
					<!-- if holdback is greater than zero then need to ceate a new row to show the  holback row -->					
					
					<%String classForRow=k%2==0?"browserEvenRow":"browserOddRow";
					if(PayB.isHoldBack(holdback)){ %>
												
						<TR class="<%=classForRow%>">
							<td>&nbsp;</td><td align="center"><%=PayB.HOLDBACK_NOTATION%></td>
		 					<td>&nbsp;&nbsp;&nbsp;<%= (String) patientCodes.get(k)%></td>
		 					<td align="center"><%= PayB.getHoldBackPercentage(holdback)%></td>
		 					<%if (countPerMilestone <= 1)
				 			{mileAmt = new BigDecimal(PayB.getMilestoneAmountByType(holdback,mileAmount,PayB.HOLDBACK_AMOUNT));
				 			%>
							<td><span class="numberfield" data-unitsymbol="" data-formatas="currency"><%=mileAmt%></span>
							<input type="hidden" name="mileAmountToHoldBack<%=mileStoneId%>" class="numberfield" data-unitsymbol="" data-formatas="currency" value="<%=mileAmount%>" /></td>
							<%	} else { mileAmt = new BigDecimal(PayB.getMilestoneAmountByType(holdback,mileAmountForGroupMilestones,PayB.HOLDBACK_AMOUNT));%>
							<td><span class="numberfield" data-unitsymbol="" data-formatas="currency"><%=mileAmt%></span>
							<input type="hidden" name="mileAmountToHoldBack<%=mileStoneId%>" class="numberfield" data-unitsymbol="" data-formatas="currency" value="<%=mileAmountForGroupMilestones%>" /></td>
							<%} holdBackAppliedDueToDate1 = new BigDecimal(holdBackAppliedDueToDate);
							prevHoldbackAmt1 = new BigDecimal(prevHoldbackAmt);%>
				 			
						<td><span class="numberfield" data-unitsymbol="" data-formatas="currency"><%=holdBackAppliedDueToDate1%></span></td>
				 		<td><input type="text" <%=styleStr%> name="applyAmounttoHoldBack<%=mileStoneId%>" class="numberfield" data-unitsymbol="" data-formatas="currency" value="<%=prevHoldbackAmt1%>" /></td>
				 		<input type="hidden" name="holdback<%=mileStoneId%>"  value="true" /></td>
						</TR>
					
					<% }else {%>
					<!-- need to create flag for the milestone not having holdback set as false  -->
						<input type="hidden" name="holdback<%=mileStoneId%>"  value="false" /></td>
					<%} %>
	 				<%

	 				}

	 			totalCounter = totalCounter + 1;
	 			// end of for
				}
				else
				{%>
					<input type="hidden" name="selectMilestone" value="<%=mileStoneId%>"/>
					<input type="hidden" name="isChecked" value="0"/>
					<input type= "hidden"  name="mileAchCount"  value="<%=mileAchCount%>" />
					<input type= "hidden"  name="linktoId"  value="<%=mileStoneId%>" />
				 	<input type= "hidden"  name="countPerMilestone"  value="<%=countPerMilestone%>" />
				 	<input type= "hidden" class="numberfield" data-unitsymbol="" data-formatas="currency" name="mileAmountDue"  value="<%=calcAmount%>" />
					<input type="hidden"  name="applyAmount" class="numberfield" data-unitsymbol="" data-formatas="currency" value="<%=prevAmount%>" />
					<input type= "hidden" class="numberfield" data-unitsymbol="" data-formatas="currency" name="origmileAmount"  value="<%=mileAmount%>" />
			 		<input type= "hidden"  name="mileDetailCount" value="<%=mileDetailCount%>" />
			 		<input type= "hidden"  class="numberfield" data-unitsymbol="" data-formatas="currency" name="mileAmountForGroupMilestones"  value="<%=mileAmountForGroupMilestones%>" />
			 		<input type="hidden" name="linkType"  value="M" />
			 		<input type="hidden" name="oldPk"  value="<%=oldPk%>" />
				<%  totalCounter = totalCounter + 1;}
			}
	 	}	// for milestone types


 %>
 </table>

<P>
<br>
<input type= "hidden"  name="totalmilecount" class="numberfield" data-unitsymbol="" data-formatas="" value="<%=totalmilecount%>" />
</P>


<% String showSubmit ="";
if (EJBUtil.isEmpty(mileStoneIdStr)){
	//KM-#5405
	if(isAccessibleFor(EJBUtil.stringToNum(pageRight),'N'))
	{ showSubmit ="Y";} else {showSubmit="N";} 
}else{
	//Edit Mode
	if(isAccessibleFor(EJBUtil.stringToNum(pageRight),'E'))
	{ showSubmit ="Y";} else {showSubmit="N";}
}
%>

<%if (showSubmit.equals("Y")){ %>
<jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="linkMilePayment"/>
		<jsp:param name="showDiscard" value="N"/>
		<jsp:param name="showSubmit" value="<%=showSubmit%>"/>
</jsp:include>
<%} %>


<% if (mileIdParam <= 0)
		{ %>
			<input type="hidden" name="selectedCount" maxlength="8" value="0">
		<% } else { %>
			<input type="hidden" name="selectedCount" maxlength="8" value="1">
		<% } %>

</form>
</div>
<%




}//end of if body for session

else
{
%>
  <jsp:include page="timeout_childwindow.jsp" flush="true"/>
  <%
}
%>

</BODY>

<div class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</HTML>
<%!
private boolean isAccessibleFor(int pageRight, char type) {
    switch(type) {
    case 'V': return ((pageRight & 0x0004) >> 2) == 1;
    case 'E': return ((pageRight & 0x0002) >> 1) == 1;
    case 'N': return  (pageRight & 0x0001) == 1;
    }
    return false;
}
%>