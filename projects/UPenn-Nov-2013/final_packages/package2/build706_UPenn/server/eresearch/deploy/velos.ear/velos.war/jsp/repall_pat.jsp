<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<title><%=LC.L_My_Rpts%><%--My Reports*****--%></title>



<%@ page import="java.util.*,java.io.*,org.w3c.dom.*"%>
<%@ page import="com.velos.eres.service.util.*"%>



<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">



<SCRIPT language="Javascript">





function fOpenWindow(type,section,frmobj){

	if (section==2) { //uncheck the upper filter range if lower range is selected

		for(i=0;i<frmobj.filterType.length;i++)	{

			frmobj.filterType[i].checked=false;

		}

	}



	if (section==1) { //uncheck the lower filter range if upper range is selected

		for(i=0;i<frmobj.filterType2.length;i++)	{

			frmobj.filterType2[i].checked=false;

		}

	}



	width=300;

	height= 300;

	if (type == "Y"){

		width=200;

		height=70;

	}

	if (type == "D"){

		width=520;

		height=320;

	}

	if (type == "M"){

		width=335;

		height=70;

	}

	frmobj.year.value ="";

	frmobj.year1.value ="";

	frmobj.month.value ="";

	frmobj.dateFrom.value ="";

	frmobj.dateTo.value ="";

	for (cnt=0;cnt <2;cnt++){

		frmobj.range[cnt].value ="";

	}

	frmobj.range[section - 1].value ="";

	if (type == 'A'){

			frmobj.range[section - 1].value = "All"

	}

	 if (type != 'A'){

		windowName=window.open("selectDateRange.jsp?parm="+type +"&section=" +section,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=" +width +",height="+height)

		windowName.focus();

	}

}





function fValidate(frmobj){

	reportChecked=false;

	for(i=0;i<frmobj.reportName.length;i++){

		sel = frmobj.reportName[i].checked;

		if (frmobj.reportName[i].checked){

	   		reportChecked=true;

		}

	}

	if (reportChecked==false) {

		alert("<%=MC.M_Selc_Rpt%>");/*alert("Please select a Report");*****/

		return false;

	}



	frmobj.action = "repRetrieve.jsp";



	reportNo = frmobj.repId.value;

	switch (reportNo) {



		case "10": //Patient Schedule

		case "11": //Patient Events Completed

		case "12": //Patient Events To Be Done

		case "65": //Patient Event/CRF Status

		case "66": //Patient CRF Tracking

					dateChecked=false;

		   			if (fnTrimSpaces(frmobj.id.value) == "") {

   						alert("<%=MC.M_Selc_Pat%>");/*alert("Please select a <%=LC.Pat_Patient%>");*****/

		   				return false;

   					}

		   			if (fnTrimSpaces(frmobj.studyPk2.value) == "") {

   						alert("<%=MC.M_Selc_Std%>");/*alert("Please select a <%=LC.Std_Study%>");*****/

		   				return false;

   					}

					if (frmobj.range[1].value != ""){

		   				dateChecked=true;

					}

					if (dateChecked==false) {

						alert("<%=MC.M_Selc_DtRange%>");/*alert("Please select a Date Range");*****/

						return false;

					}



		   			break;



		case "13": //Patients Event List

		case "16": //All Patient Enrolled to a study

			dateChecked =false;

			if (fnTrimSpaces(frmobj.studyPk.value) == "") {

				alert("<%=MC.M_Selc_Std%>");/*alert("Please select a <%=LC.Std_Study%>");*****/

				return false;

			}

			if (frmobj.range[0].value != ""){

   				dateChecked=true;

			}

			if (dateChecked==false) {

				alert("<%=MC.M_Selc_DtRange%>");/*alert("Please select a Date Range");*****/

				return false;

			}



			break;

		case "45": //All Patient Associated Cost

			dateChecked =false;

			if (fnTrimSpaces(frmobj.protId.value) == "") {

				alert("<%=MC.M_Selc_PcolCal%>");/*alert("Please select a Protocol Calendar");*****/

				return false;

			}

			if (frmobj.range[0].value != ""){

   				dateChecked=true;

			}

			if (dateChecked==false) {

				alert("<%=MC.M_Selc_DtRange%>");/*alert("Please select a Date Range");*****/

				return false;

			}



			break;



		case "47": //Patient Associated Cost

			dateChecked =false;

   			if (fnTrimSpaces(frmobj.id.value) == "") {

   				alert("<%=MC.M_Selc_Pat%>");/*alert("Please select a <%=LC.Pat_Patient%>");*****/

   				return false;

			}

			if (fnTrimSpaces(frmobj.studyPk2.value) == "") {

				alert("<%=MC.M_Selc_Std%>");/*alert("Please select a <%=LC.Std_Study%>");*****/

				return false;

			}

			if (fnTrimSpaces(frmobj.protId2.value) == "") {

				alert("<%=MC.M_Selc_PcolCal%>");/*alert("Please select a Protocol Calendar");*****/

				return false;

			}

			if (frmobj.range[1].value != ""){

   				dateChecked=true;

			}

			if (dateChecked==false) {

				alert("<%=MC.M_Selc_DtRange%>");/*alert("Please select a Date Range");*****/

				return false;

			}



			break;

	}



/*		for(i=0;i<document.reports.filterType.length;i++)	{

			if (document.reports.filterType[i].checked){



			if (document.reports.filterType[i].value == "4") {

				if (document.reports.dateFrom.value == "") {

					alert("Please select From Date");

					return false;

				}

				if (document.reports.dateTo.value == "") {

					alert("Please select To Date");

					return false;

				}



			}

			}

		}*/



	frmobj.submit();

}



function fSetIdNew(ddtype,frmobj){

	if (ddtype == "patient") {

		frmobj.id.value = "";

		frmobj.selPatient.value = "None";

		frmobj.val.value = "";

		openwindow();

	}



	if (ddtype == "study1") {

		frmobj.selProtocol.value = "None";

		frmobj.protId.value = "";

		i=frmobj.sId1.options.selectedIndex;

		frmobj.studyPk.value = frmobj.sId1.options[i].value;



	}

	if (ddtype == "study2") {

		frmobj.selProtocol2.value = "None";

		frmobj.protId2.value = "";

		i=frmobj.sId2.options.selectedIndex;

		frmobj.studyPk2.value = frmobj.sId2.options[i].value;

	}



	if (ddtype == "report") {//report Id and name are concatenated by %.Need to separate them

		for(i=0;i<frmobj.reportName.length;i++)	{

			if (frmobj.reportName[i].checked){

				lsReport = frmobj.reportName[i].value;

				ind = lsReport.indexOf("%");

				frmobj.repId.value = lsReport.substring(0,ind);

				frmobj.repName.value = lsReport.substring(ind+1,lsReport.length);

				break;

			}

		}

	}



	if (ddtype == "daoreport") {//reports created thru dao

		for(i=0;i<frmobj.reportName.length;i++)	{

			if (frmobj.reportName[i].checked){

				lsReport = frmobj.reportName[i].value;

				frmobj.repId.value = frmobj.reportName[i].value;

				frmobj.reportNumber.value = frmobj.reportName[i].value;

				break;

			}

		}

	}



}



</SCRIPT>



<SCRIPT language="JavaScript1.1">

function openwindow() {

    windowName=window.open("patientsearch.jsp?openMode=P&srcmenu=&patid=&patstatus=","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=500,height=400");

	windowName.focus();

}



function openProtWindow(param,frmobj) {

	if (param == 'all') { //all patient reports section

		lstudyPk = frmobj.studyPk.value;

	}else if (param == 'single') {

		lstudyPk = frmobj.studyPk2.value;

	}

	if (fnTrimSpaces(lstudyPk) == "") {

		alert("<%=MC.M_Selc_Std%>");/*alert("Please select a <%=LC.Std_Study%>");*****/

			return false;

	}

	windowName=window.open("protocolPopup.jsp?studyPk="+lstudyPk+"&reportType="+param,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=500,height=400");

	windowName.focus();

}





</SCRIPT>



</head>



<% String src="";

src= request.getParameter("srcmenu");

%>



<jsp:include page="panel.jsp" flush="true">

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>





<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id="repdao" scope="request" class="com.velos.eres.business.common.ReportDaoNew"/>

<jsp:useBean id="repdao1" scope="request" class="com.velos.eres.business.common.ReportDaoNew"/>

<jsp:useBean id="grpRights" scope="request" class="com.velos.eres.web.grpRights.GrpRightsJB"/>

<jsp:useBean id="ctrlD" scope="request" class="com.velos.eres.business.common.CtrlDao"/>



<%@ page language = "java" import = "com.velos.eres.business.section.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%>



<body>

<br>



<DIV class="formDefault" id="div1">



<%



   HttpSession tSession = request.getSession(true);



   if (sessionmaint.isValidSession(tSession))

   {

	String uName =(String) tSession.getValue("userName");

	String userId = (String) tSession.getValue("userId");



	int acc = EJBUtil.stringToNum((String) (tSession.getValue("accountId")));

	String tab = request.getParameter("selectedTab");



	String modRight = (String) tSession.getValue("modRight");



	int modlen = modRight.length();



	ctrlD.getControlValues("module");

	int ctrlrows = ctrlD.getCRows();



	ArrayList feature =  ctrlD.getCValue();

	ArrayList ftrDesc = ctrlD.getCDesc();

	ArrayList ftrSeq = ctrlD.getCSeq();

	ArrayList ftrRight = new ArrayList();

	String strR;



	ArrayList ctrlValue = new ArrayList();



	for (int counter = 0; counter <= (modlen - 1);counter ++)

	{

		strR = String.valueOf(modRight.charAt(counter));

		ftrRight.add(strR);

	}



	grpRights.setGrSeq(ftrSeq);

    grpRights.setFtrRights(ftrRight);

	grpRights.setGrValue(feature);

	grpRights.setGrDesc(ftrDesc);





	int eptRight = 0;

	eptRight  = Integer.parseInt(grpRights.getFtrRightsByValue("MODEPT"));



	Calendar cal = Calendar.getInstance();

	int currYear = cal.get(cal.YEAR);



	String filterText="";



	int counter=0;

	int studyId=0;

	StringBuffer study1=new StringBuffer();

	StringBuffer study2=new StringBuffer();

	StudyDao studyDao = new StudyDao();

	studyDao.getReportStudyValuesForUsers(userId);

	study1.append("<SELECT NAME=sId1 onChange=fSetIdNew('study1',document.reports)>") ;

	study2.append("<SELECT NAME=sId2 onChange=fSetIdNew('study2',document.reports)>") ;

	for (counter = 0; counter <= (studyDao.getStudyNumbers()).size() -1 ; counter++)

		{




		studyId = ((Integer)((studyDao.getStudyIds()).get(counter))).intValue();

		study1.append("<OPTION value = "+ studyId +">" + (studyDao.getStudyNumbers()).get(counter)+"</OPTION>");

		study2.append("<OPTION value = "+ studyId +">" + (studyDao.getStudyNumbers()).get(counter)+"</OPTION>");



		}



	study1.append("<OPTION value=''  selected>"+LC.L_Select_AnOption/*Select an Option*****/+"</OPTION>");

	study1.append("</SELECT>");



	study2.append("<OPTION value=''  selected>"+LC.L_Select_AnOption/*Select an Option*****/+"</OPTION>");

	study2.append("</SELECT>");



	CtrlDao ctrl = new CtrlDao();

	ctrl.getControlValues("allpat");

	String catId = (String) ctrl.getCValue().get(0);



	repdao.getAllRep(EJBUtil.stringToNum(catId),acc); //All Patient Reports

	ArrayList repIdsAllPatients= repdao.getPkReport();

	ArrayList namesAllPatients= repdao.getRepName();

   	ArrayList descAllPatients= repdao.getRepDesc();



	CtrlDao ctrl1 = new CtrlDao();

	ctrl1.getControlValues("patspecific");

	catId = (String) ctrl1.getCValue().get(0);

	repdao1.getAllRep(EJBUtil.stringToNum(catId),acc); //Patient Specific Reports

	ArrayList repIdsOnePatients= repdao1.getPkReport();

	ArrayList namesOnePatients= repdao1.getRepName();

   	ArrayList descOnePatients= repdao1.getRepDesc();



	int repIdAllPatient=0;

	String nameAllPatient="";

	int repIdOnePatient=0;

	String nameOnePatient="";



	int lenAllPatient = repIdsAllPatients.size() ;

	int lenOnePatient = repIdsOnePatients.size() ;

%>
	<%/* SV, 8/24/04, fix for bug 1673, _new is not a keyword, _blank is and it opens new window instance each time which is what we want for reports.*/%>


<Form Name="reports" method="post" action="repRetrieve.jsp" target="_blank" >



	<P class = "userName"><%= uName %></P>

	<BR>

	<jsp:include page="reporttabs.jsp" flush="true"/>



	<input type="hidden" name="srcmenu" value='<%=src%>'>

	<input type="hidden" name="selectedTab" value='<%=tab%>'>



	<br>



<table width="100%" cellspacing="0" cellpadding="0" border=0 >

<tr>

 <td colspan = 3>

<p class = "defComments"><%=MC.M_SelRpt_ListBelow%><%--Select a report from the list below, specify appropriate filters and click on the 'Display' button.*****--%> </p>

<BR>

</td>

</tr>



<tr>

 <td width=40%>

	<Font class=numberFonts > 1.</font><Font class=reportText ><I><%=MC.M_Selc_YourRptType%><%--Select your report type*****--%> </I></font>

</td>

 <td width=40%>

	<Font class=numberFonts >2.</font> <Font class=reportText ><I><%=MC.M_Selc_YourRptFilters%><%--Select your report filters*****--%></I></font>

</td>

 <td width=20%>

	<Font class=numberFonts >3.</font> <Font class=reportText ><I><%=LC.L_View_YourRpt%><%--View your report*****--%></I></font>

</td>

</tr>

</Table>



<HR class=blue>

<input type=hidden name=year>

<input type=hidden name=month>

<input type=hidden name=year1>

<input type=hidden name=dateFrom>

<input type=hidden name=dateTo>





<Table border=0 width=100%>

<tr>

	 <td colspan=3>

				<p class = "reportHeadings"><%=LC.L_All_PatsRpts%><%--All <%=LC.Pat_Patients%> Reports*****--%></p>

			</td>

		</tr>



		<tr height=7>

			<td>&nbsp;</td>

		</tr>



		<tr>

			<td width=40% valign=top>

		<%

		for(counter = 0;counter<lenAllPatient;counter++)

		{

			repIdAllPatient = ((Integer)repIdsAllPatients.get(counter)).intValue();

			nameAllPatient=((namesAllPatients.get(counter)) == null)?"-":(namesAllPatients.get(counter)).toString();

			%>

				<Input Type="radio" name="reportName" value="<%=repIdAllPatient%>%<%=nameAllPatient%>" onClick=fSetIdNew('report',document.reports)> <%=nameAllPatient%>

			<BR>

			<%

		}

		%>

		</td>

			<td WIDTH=50%>

				<FONT class = "comments"><%=MC.M_Selc_DispStyleAs%><%--Select display style as*****--%> :</FONT>

					<SELECT name=dispStyle>

					<OPTION value = 'P'><%=LC.L_Disp_ByPat%><%--Display By <%=LC.Pat_Patient%>*****--%> </OPTION>

					<OPTION value = 'D'><%=MC.M_Disp_DateRange%><%--Display By Date Range*****--%> </OPTION>

					</SELECT>

				 <br>



			     <FONT class="comments"> <%=LC.L_Select_Study%><%--Select <%=LC.Std_Study%>*****--%>: &nbsp;<%=study1%> </FONT>



				<br>

					<FONT class = "comments"><A HREF=# onClick="openProtWindow('all',document.reports)"><%=LC.L_Select_PcolCal%><%--Select Protocol Calendar*****--%>:</A> </FONT>

					<Input type=text name=selProtocol size=20 READONLY value=None>

				<br>

			   	<Input type="radio" name="filterType" value="1" onClick="fOpenWindow('A','1',document.reports)"> <%=LC.L_All%><%--All*****--%>

			   	<Input type="radio" name="filterType" value="2" onClick="fOpenWindow('Y','1',document.reports)"> <%=LC.L_Year%><%--Year*****--%>

			   	<Input type="radio" name="filterType" value="3" onClick="fOpenWindow('M','1',document.reports)"> <%=LC.L_Month%><%--Month*****--%>

			   	<Input type="radio" name="filterType" value="4" onClick="fOpenWindow('D','1',document.reports)"> <%=LC.L_Date_Range%><%--Date Range*****--%>

				 <BR><Font class=comments><%=LC.L_Selected_DateFilter%><%--Selected Date Filter*****--%>:</Font>

				  	<input type=text name=range size=40 READONLY>

			</td>

		<td width=10%></td>

	</tr>

</table>



<HR class=blue>

<Br>

<table border=0 width=100%>

	<tr>

	 <td colspan=3>

				<P class="reportHeadings"><%=LC.L_Pat_SpecificRpts%><%--<%=LC.Pat_Patient%> Specific Reports*****--%></P>

			</td>

		</tr>

		<tr height=7>

			<td>&nbsp;</td>

		</tr>

	</tr>

	<tr>

	<td width=40% valign=top>

		<%

		for(counter = 0;counter<lenOnePatient;counter++)

		{

			repIdOnePatient = ((Integer)repIdsOnePatients.get(counter)).intValue();

			nameOnePatient=((namesOnePatients.get(counter)) == null)?"-":(namesOnePatients.get(counter)).toString();

		%>



		<%

		if(eptRight==0){

			if ((repIdOnePatient!=65) && (repIdOnePatient!=66)) {

		%>

			<Input Type="radio" name="reportName"  value="<%=repIdOnePatient%>%<%=nameOnePatient%>" onClick=fSetIdNew('report',document.reports)> <%=nameOnePatient%>

			<%}%>

		<%} else {%>

					<Input Type="radio" name="reportName"  value="<%=repIdOnePatient%>%<%=nameOnePatient%>" onClick=fSetIdNew('report',document.reports)> <%=nameOnePatient%>

		<%}%>

	<br>

		<%

		}

		%>

	</td>

	<td width=50%>

		<FONT class = "comments"><%=MC.M_ForPat_SpecificRpts%><%--For <%=LC.Pat_Patient_Lower%> specific reports,*****--%>

		<A href="#" onclick="fSetIdNew('patient',document.reports)"><%=LC.L_Select_APat%><%--Select a <%=LC.Pat_Patient%>*****--%></A> <Br>

		<%=MC.M_Cur_SelcPatIs%><%--Currently the selected <%=LC.Pat_Patient_Lower%> is*****--%>: </FONT>



		<Input type=text name=selPatient size=20 READONLY value=None>

		<br>

		<FONT class = "comments">	<%=LC.L_Select_Study%><%--Select <%=LC.Std_Study%>*****--%>: &nbsp;<%=study2%> </FONT>

		<br>

		<FONT class = "comments"><A HREF=# onClick="openProtWindow('single',document.reports)"><%=LC.L_Select_PcolCal%><%--Select Protocol Calendar*****--%>:</A></FONT>

		<Input type=text name=selProtocol2 size=20 READONLY value=None>

		<br>

		<Input type="radio" name="filterType2" value="1"  onClick="fOpenWindow('A','2',document.reports)"> <%=LC.L_All%><%--All*****--%>

		<Input type="radio" name="filterType2" value="2" onClick="fOpenWindow('Y','2',document.reports)"> <%=LC.L_Year%><%--Year*****--%>

		<Input type="radio" name="filterType2" value="3" onClick="fOpenWindow('M','2',document.reports)"> <%=LC.L_Month%><%--Month*****--%>

		<Input type="radio" name="filterType2" value="4" onClick="fOpenWindow('D','2',document.reports)"> <%=LC.L_Date_Range%><%--Date Range*****--%>

		<BR><Font class=comments><%=LC.L_Selected_DateFilter%><%--Selected Date Filter*****--%>:</Font>

		<input type=text name=range size=40 READONLY>

	</td>



	<td width=10%></td>

	<tr>

	</table>



<Input type = hidden  name=id value="">

<Input type = hidden  name="val" >

<Input type = hidden  name=protId value="">

<Input type = hidden  name=protId2 value="">

<Input type=hidden name="studyPk">

<Input type=hidden name="studyPk2">

<Input type=hidden name="repId">

<Input type=hidden name="repName">

<Input type=hidden name="rangeType">

<Input Type=hidden name="reportNumber">



<Input type=hidden name="filterText" value = "<%=filterText%>">



<table width="100%" cellspacing="0" cellpadding="0" border=0 >

<tr>

	<td colspan=2 align=right>

		<A onClick = "return fValidate(document.reports)" href="#"><input type="image" src="../images/jpg/Display.gif" align="absmiddle" border=0></A>

	</td>

</tr>

</table>



</form>

<%} //end of if session times out

else{

%>

	<jsp:include page="timeout.html" flush="true">

	</jsp:include>

<%

}

%>



<div>

<jsp:include page="bottompanel.jsp" flush="true">

</jsp:include>

</div>



</div>



<DIV class="mainMenu" id = "emenu">

  <jsp:include page="getmenu.jsp" flush="true"/>

</DIV>

</body>

</html>





