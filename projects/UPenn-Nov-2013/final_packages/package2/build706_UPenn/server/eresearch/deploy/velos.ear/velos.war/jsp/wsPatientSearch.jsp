<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<%/*
This JSP is a customization of the the standard patient search JSP. We recognize the need
to move from a hard-coded set of search fields to a more customizable set. This JSP
is an intermediate step for UTSW to get to that point. The fields are hard-coded
based on UTSW's requirements, but it uses the more dynamic call in com.velos.remoteservice.
*/%>

<head>

<jsp:include page="include.jsp"/>
<jsp:include page="ui-include.jsp"/>

<style type="text/css">
<!--

body{
	overflow: auto;
}
#mrn_search {

  float: left;

}

tr.searchRow{
	background-color: #dcdcdc
}

#name_search{

  float: left;


}
#search_result {

  position: absolute;
  left: 0;
  width: 90%;

}

#patdiv {

  float: right;
  width: 50%;

}

#patlistdiv {

  float: top;
  width: 50%;

}
#resultdiv{
  visibility: hidden;
  width: 0px;
}
-->
</style>

<script>toggleNotification('off',"");</script>
<script src="js/velos/date.js" type="text/javascript"></script>
<script language="JavaScript">
var jsonPatientArray = null;

var patientArray = new Array();

var openerDocument = window.opener.document;

function selectPatient(patient){
	//modify the patient page that opened this page
	//hide the div that covers the form
	try{
		openerDocument.getElementById("trans").style.visibility="hidden";

		//update fields
//		updateSourceField(openerDocument.getElementById("patientForm").patid, patient.MRN);
		var patientDOBDate = new Date(patient.DOB);
		<%	//when upgrade to 8.7, replace format parameter with global variable
			//calDateFormat defined in dateformatSetting.js%>
		updateSourceField(openerDocument.getElementById("patientForm").patdob, formatDate(patientDOBDate, "MM/dd/yyyy"));
		updateSourceField(openerDocument.getElementById("patientForm").patfname, patient.firstName);
		updateSourceField(openerDocument.getElementById("patientForm").patlname, patient.lastName);

		updateSourceField(openerDocument.getElementById("patientForm").patgender, deferenceCode(patient.gender, "gender"));

		updateSourceField(openerDocument.getElementById("patientForm").patrace, deferenceCode(patient.race, "race"));
		updateSourceField(openerDocument.getElementById("patientForm").patethnicity, deferenceCode(patient.ethnicity, "ethnicity"));

	}
	finally{
		window.close();
	}
}

function deferenceCode(remoteValue, codeType){
	var codeKeyField = openerDocument.getElementById(codeType + "_" + remoteValue);
	if (codeKeyField != null){
		return codeKeyField.value;
	}
	return remoteValue;
}

function selectPatFromList(searchIndex){

	var patient = jsonPatientArray[searchIndex];
	selectPatient(patient);
}

function updateSourceField(fieldObj, value){
	if (!fieldObj){
		return;
	}
	if (value && value != null && value != "null"){
		fieldObj.value = value;
	}

}

function fetchSearchPatient(){
	var patId = document.getElementById("patId").value;

	fetchSinglePat(patId);
}



function fetchSinglePat(patId)
{

	$("resultdiv").innerHTML = "";

	toggleNotification('on',"");

	var options="";
	var options = "mode=single&patId=" + patId;
	new Ajax.Request('/eres/jsp/wsresultDemoJSON.jsp?' + options,{asynchronous:true,
		onSuccess:function(transport)
		{

		 	if (transport.status==200)
		 	{
			 	$("resultdiv").innerHTML = transport.responseText;
			 	toggleNotification('off',"");

			 	//alert(transport.responseText);
		 	}

	 	}
	});
}

function fetchPathList()
{

	var firstName = document.getElementById("firstName").value;
	var middleName = document.getElementById("middleName").value;
	var lastName = document.getElementById("lastName").value;
	var gender = document.getElementById("gender").value;
	var dob = document.getElementById("patdob").value;

	document.getElementById("resultdiv").innerHTML = "";

	toggleNotification('on',"");

	var options = "mode=list&firstName=" + firstName + "&middleName=" + middleName + "&lastName=" + lastName + "&gender="+gender + "&dob=" + dob ;
	new Ajax.Request('/eres/jsp/wsresultDemoJSON.jsp?' + options,{asynchronous:true,
		onSuccess:function(transport)
		{

		 	if (transport.status==200)
		 	{
			 	var resultDIV = document.getElementById("resultdiv")
			 	resultDIV.innerHTML = transport.responseText;
			 	toggleNotification('off',"");


				var jsonError = document.getElementById("jsonError").innerText || document.getElementById("jsonError").textContent;
			 	if (jsonError){
					alert(jsonError);
					return;
			 	}
			 	var jsonReturnString = document.getElementById("jsonReturn").innerText || document.getElementById("jsonReturn").textContent;
			 	if (!jsonReturnString){
			 		alert("No <%=LC.Pat_Patient_Lower%> found.");
					return;

			 	}
			 	jsonPatientArray = YAHOO.lang.JSON.parse(jsonReturnString);

		 		var myColumnDefs = [
								{key:"button", label:"Select", formatter:YAHOO.widget.DataTable.formatButton},
								{key:"MRN", label:"Patient ID", sortable:true, resizeable:true},
								{key:"primaryFacility", label:"Facility", sortable:true, resizeable:true},
		 		                {key:"firstName", label:"First Name", sortable:true, resizeable:true},
		 		                {key:"lastName", label:"Last Name", sortable:true, resizeable:true},
		 		                {key:"DOB", label:"DOB", formatter:YAHOO.widget.DataTable.formatDate, sortable:true, sortOptions:{defaultDir:YAHOO.widget.DataTable.CLASS_DESC},resizeable:true}
		 		               ];
	            var myDataSource = new YAHOO.util.DataSource(jsonPatientArray);
	            myDataSource.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
	            myDataSource.responseSchema = {
	            	fields: [
	 		            	"searchIndex",
	 		            	"MRN",
	 		            	"primaryFacility",
	 		            	"firstName",
	 		            	"lastName",
	 		            	"DOB"] };


		 		var myDataTable =
			 		new YAHOO.widget.DataTable(
					 		"serverpagination",
					 		myColumnDefs,
					 		myDataSource);
			        myDataTable.subscribe("buttonClickEvent", function(oArgs){
			            var oRecord = this.getRecord(oArgs.target);
			            var searchIndex = oRecord.getData().searchIndex;
			            selectPatFromList(searchIndex) });
			        document.getElementById("search_result").style.visibility = "visible";
		 	}
	 	}
	});
}
</script>

<title><%=LC.Pat_Patient%> Search</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<META HTTP-EQUIV="no-cache">

</head>
<body>
<%@ page
	import="com.velos.eres.service.util.StringUtil,com.velos.remoteservice.demographics.*,java.util.*, com.velos.remoteservice.*, com.velos.eres.service.util.EJBUtil, com.velos.eres.business.common.CodeDao"%>
<%@ page import="com.velos.eres.service.util.LC"%>
<jsp:useBean id="sessionmaint" scope="session"
	class="com.velos.eres.service.util.SessionMaint" />
<!-- <jsp:useBean id="ctrldao" scope="request" class="com.velos.esch.business.common.EventAssocDao"/> -->
<%
	HttpSession tSession = request.getSession(true);
	 if (sessionmaint.isValidSession(tSession))
	{
		 CodeDao cd1 = new CodeDao();
		 cd1.getCodeValues("gender");
		 String dGender = cd1.toPullDown("gender", 0, "");

		 //find the remote service
		String accountNumStr = (String)tSession.getAttribute("accountId");
		int accountNo = EJBUtil.stringToNum(accountNumStr);
		IDemographicsService demographicsService =
			DemographicsServiceFactory.getService(accountNo);

		if (demographicsService == null){
			%>An error has occurred finding the demographics service.<%
			return;
		}

%>
	<jsp:include page="sessionlogging.jsp" flush="true" />



		<div id="name_search">
			<form   name="name_search" method="POST" onsubmit="return fetchPathList();">
			 <table class="searchTable" cellspacing="0" cellpadding="2">

				<tr class="searchRow" >
				  <td class="tdDefault" width="15%"> First Name </td>
				  <td class="tdDefault" width="15%"> Middle Name </td>
				  <td class="tdDefault" width="15%"> Last Name </td>
				  <td class="tdDefault" width="15%"> Gender </td>
				  <td class="tdDefault" > DOB </td>
				</tr class="searchRow" >
				<tr class="searchRow" >

				  <td class="tdDefault"> <input type="text" name="firstName" id="firstName"  size="15" > </td>
				   <td class="tdDefault"> <input type="text" name="middleName" id="middleName"  size="15" > </td>

				  <td class="tdDefault"> <input type="text" name="lastName" id="lastName"  size="15" > </td>


				  <td class="tdDefault">
				   <select name="gender" id="gender">
				   	<option value = "F">Female</option>
				   	<option value = "M">Male</option>
				   	<option value = "O">Other</option>
				   	<option value = "U">Unknown</option>
				   	<option value = "" selected="true">Select an Option</OPTION></select>
				  </td>
<%-- INF-20084 Datepicker-- AGodara --%>
				  <td class="tdDefault" id="dob"><input type="text" id="patdob" name="patdob" class="datefield" readonly value="" size="10" maxlength="10" /></td>
				</tr>
				<tr class="searchRow"  height="3">
					<td/>
					<td/>
					<td>
						<button type="submit" onClick=" return fetchPathList();"><%=LC.L_Search%></button>
					</td>


					<td/>
					<td/>
				</tr>
			 </table>

			</form>
		</div>

	<div id="search_result" class="yui-skin-sam tabFormTopN">

			<div id="serverpagination">
			</div>

	</div>
	<div id="resultdiv">

	</div>

<%

	}//close is valid session
%>
</body>
</html>