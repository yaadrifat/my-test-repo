var At_AGlance=M_At_AGlance;
var varPartners=L_Partners_Lower;
var varFaqs=L_Faqs;
var varEvents=L_Events_Lower;
var varProducts=L_Products_Lower;
var varServices=L_Services_Lower;
var varAccounts=L_Accounts_Lower;
var varPricing=L_Pricing_Lower;
var varSecurity=L_Security_Lower;
var varHipaa=L_Hipaa;
var varDemo=L_Demo_Lower;
var varContact=L_Contact_Lower;
var varRegister=L_Register_Lower;
var varLogin=L_Login_Lower;
var InThe_News=L_InThe_News;
var White_Papers=L_White_Papers;
function fwLoadMenus() {
  if (window.fw_menu_0) return;
  window.fw_menu_0 = new Menu("root",92,19,"Arial, Helvetica, sans-serif",12,"#ffffff","#ffffff","#9999ff","#000084");
    /*fw_menu_0.addMenuItem("at a glance","location='ataglance.html'");*****/
  fw_menu_0.addMenuItem(At_AGlance,"location='ataglance.html'");
  /*fw_menu_0.addMenuItem("in the news","location='news.html'");*****/
   fw_menu_0.addMenuItem(InThe_News,"location='news.html'");
 /* fw_menu_0.addMenuItem("white papers","location='whitepapers.html'");*****/
   fw_menu_0.addMenuItem(White_Papers,"location='whitepapers.html'");
   /*fw_menu_0.addMenuItem("partners","location='partners.html'");*****/
  fw_menu_0.addMenuItem(varPartners,"location='partners.html'");
    /*fw_menu_0.addMenuItem("FAQs","location='faqs.html'");*****/
  fw_menu_0.addMenuItem(varFaqs,"location='faqs.html'");
   /* fw_menu_0.addMenuItem("events","location='calendarofevents.html'");*****/
  fw_menu_0.addMenuItem(varEvents,"location='calendarofevents.html'");
   fw_menu_0.hideOnMouseOut=true;
  window.fw_menu_1 = new Menu("root",137,19,"Arial, Helvetica, sans-serif",12,"#ffffff","#ffffff","#9999ff","#000084");
  /*  fw_menu_1.addMenuItem("products &amp; services","location='products.html'");*****/
  fw_menu_1.addMenuItem(varProducts+" &amp; "+varServices,"location='products.html'");
   /*fw_menu_1.addMenuItem("accounts &amp; pricing","location='pricing.html'");*****/
  fw_menu_1.addMenuItem(varAccounts+" &amp; "+varPricing,"location='pricing.html'");
  /*  fw_menu_1.addMenuItem("security &amp; HIPAA","location='security.html'");*****/
  fw_menu_1.addMenuItem(varSecurity+" &amp; "+varHipaa,"location='security.html'");
   fw_menu_1.hideOnMouseOut=true;
  window.fw_menu_2 = new Menu("root",82,19,"Arial, Helvetica, sans-serif",12,"#ffffff","#ffffff","#9999ff","#000084");
   /*fw_menu_2.addMenuItem("demo","location='demo.html'");*****/
  fw_menu_2.addMenuItem(varDemo,"location='demo.html'");
   fw_menu_2.hideOnMouseOut=true;
  window.fw_menu_3 = new Menu("root",82,19,"Arial, Helvetica, sans-serif",12,"#ffffff","#ffffff","#9999ff","#000084");
  /*fw_menu_3.addMenuItem("contact","location='contact.html'");*****/
  fw_menu_3.addMenuItem(varContact,"location='contact.html'");
   fw_menu_3.hideOnMouseOut=true;
  window.fw_menu_4 = new Menu("root",82,19,"Arial, Helvetica, sans-serif",12,"#ffffff","#ffffff","#9999ff","#000084");
  /*  fw_menu_4.addMenuItem("register","location='register.jsp'");*****/
  fw_menu_4.addMenuItem(varRegister,"location='register.jsp'");
   fw_menu_4.hideOnMouseOut=true;
  window.fw_menu_5 = new Menu("root",82,19,"Arial, Helvetica, sans-serif",12,"#ffffff","#ffffff","#9999ff","#000084");
  /*fw_menu_5.addMenuItem("login","location='https://www.veloseresearch.com/eres/jsp/ereslogin.jsp'");*****/
  fw_menu_5.addMenuItem(varLogin,"location='https://www.veloseresearch.com/eres/jsp/ereslogin.jsp'");
   fw_menu_5.hideOnMouseOut=true;

  fw_menu_5.writeMenus();
} // fwLoadMenus()


function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
