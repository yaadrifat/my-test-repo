<%@page import="com.velos.eres.service.util.VelosResourceBundle"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@page import="java.util.ArrayList,java.util.HashMap,org.json.*"%>
<%@page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC" %>
<jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%
HttpSession tSession = request.getSession(false); 
if (!sessionmaint.isValidSession(tSession)) {
	return;
}
String userId = (String)tSession.getAttribute("userId");
int numStudies = 0;

%>
<div onmouseout='return nd();'>
	<table width="100%">
	<tr>
		<td>
		<div id="gadgetMyStudies_settings" style="border:#aaaaaa 1px solid; padding: 2px; display:none">
		    <form id="gadgetMyStudies_formSettings" name="gadgetMyStudies_formSettings" onsubmit="return false;" >
		    <p><b><%=LC.L_Settings_MyStudiesGadget%></b></p>
		    <input id="gadgetMyStudies_studyIds" name="gadgetMyStudies_studyIds" type="hidden"></input>
		    <table style="width:100%; " >
		        <tr>
		            <td>
		            	<s:radio name="gadgetMyStudiesJB.displayOption" list="gadgetMyStudiesJB.displayOptionList" 
							listKey="radioCode" listValue="radioDisplay" value="gadgetMyStudiesJB.displayOption" />
		            	<BR/>
		                <div id="gadgetMyStudies_errMsg_studyIds"></div>
		            </td>
				</tr>
				<tr id="selectStudiesRow" name="selectStudiesRow" style="display:none">
		            <td>
		                <div id="gadgetMyStudies_studies"></div>
	                	<s:if test="%{gadgetMyStudiesJB.settingsStudyCount < 5}">
			                <input id="gadgetMyStudies_study" name="gadgetMyStudies_study" type="text" size="30" ></input>
			                <input id="gadgetMyStudies_studyId" name="gadgetMyStudies_studyId" type="hidden" size="30" ></input>
			                <button id="gadgetMyStudies_addStudyButton" class="gadgetMyStudies_addStudyButton" type="button"><%=LC.L_Add%></button>
		                </s:if>
					 </td>
				</tr>
				<tr id="selectedStudiesRow" name="selectedStudiesRow" style="display:none; width:100%;">
                   	<td style="width:70%;">
                    	<table class="gdt-table" style="width:100%;">
                    		<tr>
                    			<td>
	                    		<div id="gadgetMyStudies_myStudiesHTML"></div>
	                    		</td>
	                    	</tr>
	                    </table>
                    </td>
                    <td style="width:30%;">
                    	<div id="gadgetMyStudies_reallyDeleteHTML"></div>
                    </td>                 
	                <s:hidden id="gadgetMyStudiesJB.settingsStudyIds" name="gadgetMyStudiesJB.settingsStudyIds"/>
                </tr>
		        <tr>
		            <td colspan="2" align="right">
		            <!-- <button type="submit"
		                    id="gadgetMyStudies_saveButton"
		                    class="gadgetMyStudies_saveButton">Save</button>&nbsp;&nbsp;&nbsp;&nbsp; -->
		            <button type="button" 
		                    id="gadgetMyStudies_cancelButton"
		                    class="gadgetMyStudies_cancelButton"><%=LC.L_Close%></button>
		            </td>
		        </tr>
		    </table>
		    </form>
		</div>
		</td>
	</tr>
	<tr>
		<td>
			<form name="gadgetMyStudies_formStudy" id="gadgetMyStudies_formStudy" 
			    onsubmit="gadgetMyStudies.beforeSubmitStudy(this);"
				method="POST" action="studybrowserpg.jsp">
				<input type="text" class="gdt-input-text" size="40" maxlength="100" 
			        name="gadgetMyStudies_studySearch" id="gadgetMyStudies_studySearch" placeholder="<%=MC.M_StdTitle_Kword%>"/>
				<input type="hidden" name="searchCriteria" id="searchCriteria" />
				<button type="button" name="gadgetMyStudies_studySearchButton" id="gadgetMyStudies_studySearchButton" 
			        class="gadgetMyStudies_studySearchButton"><%=LC.L_Search%></button>
				<button type="button" name="gadgetMyStudies_studyAdvancedSearchButton" id="gadgetMyStudies_studyAdvancedSearchButton" 
			        class="gadgetMyStudies_studyAdvancedSearchButton"><%=LC.L_Adva_Search%></button>
			</form>
		</td>
	</tr>
	<tr>
		<td>
			<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
       			<div id="gadgetMyStudies_myStudiesGrid"></div>
       		</div>
		</td>
	</tr>
	</table>
</div>

