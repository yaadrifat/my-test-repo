<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<%
boolean isIrb = "irb_upload_tab".equals(request.getParameter("selectedTab")) ? true : false;
String includeTabsJsp = isIrb ? "irbnewtabs.jsp" : "studytabs.jsp";
if (isIrb) {
%>
<title><%=MC.M_ResComp_NewUpldDocu%><%--Research Compliance >> New Application >> Upload Documents*****--%></title>
<% } else { %>
<title><%=LC.L_Std_Ver%><%--Study >> Versions*****--%></title>
<% } %>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*"%>
<%@ page import="com.velos.eres.service.util.*"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>


<script>
function confirmBox(section,pgRight) {
	if (f_check_perm(pgRight,'E') == true) {
		var paramArray = [section];
		msg=getLocalizedMessageString("L_Del_Section",paramArray);/*msg="Delete Section '" + section+ "'?";*****/
		if (confirm(msg)) {
    		return true;
		}
		else{
			return false;
		}
	} else {
		return false;
	}
}

</script>

<% String src="";
src= request.getParameter("srcmenu");
%>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0">
<br>
<jsp:useBean id="studyVerB" scope="request" class="com.velos.eres.web.studyVer.StudyVerJB"/>
<jsp:useBean id="sectionB" scope="request" class="com.velos.eres.web.section.SectionJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="statHistoryB" scope="request" class="com.velos.eres.web.statusHistory.StatusHistoryJB"/>
<%@ page language = "java" import = "com.velos.eres.business.section.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.studyRights.StudyRightsJB"%>
<DIV class="BrowserTopn" id="div1">

<%
String from = "sectionver";
%>
	<jsp:include page="<%=includeTabsJsp%>" flush="true">
	<jsp:param name="from" value="<%=from%>"/>
	</jsp:include>
</DIV>
<DIV class="BrowserBotN BrowserBotN_S_3" id = "div1">
  	<%


   	HttpSession tSession = request.getSession(true);


   	if (sessionmaint.isValidSession(tSession))
	{
		String uName =(String) tSession.getValue("userName");
		String study = (String) tSession.getValue("studyId");

		String searchCriteria = "";

		searchCriteria = request.getParameter("searchCriteria");
		int studyVerId = EJBUtil.stringToNum(request.getParameter("studyVerId"));
		studyVerB.setStudyVerId(studyVerId);
		studyVerB.getStudyVerDetails();
		String verStatus = "" ;//studyVerB.getStudyVerStatus();

		String verNumber = studyVerB.getStudyVerNumber();
		verStatus = statHistoryB.getLatestStatus(studyVerId,"er_studyver");

		if(verStatus == null)
		verStatus = "";

		String mode = request.getParameter("mode"); //S-search performed by user
		if (searchCriteria==null)
		{
			searchCriteria="";
		}
	   	int pageRight= 0;
	   	if(study == "" || study == null) {
		%>
			<jsp:include page="studyDoesNotExist.jsp" flush="true"/>
		<%
		} else {
			int studyId = EJBUtil.stringToNum(study);
			int len1 = 0;
			String flag = "a";
			String tab = request.getParameter("selectedTab");
   	   	   	StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");

			if ((stdRights.getFtrRights().size()) == 0){
			 	pageRight= 0;
			}else{
				pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYSEC"));
			}

		   	if (pageRight > 0) {
			%>
			<p class="defComments"><%=LC.L_Version_Number%><%--Version Number*****--%>: <%=verNumber%> &nbsp;&nbsp;
			<%
			 if (verStatus.equals("F")) {
			 %>
             <FONT class="Mandatory"><%=MC.M_CntChg_InFreezeVer%><%--You cannot make any changes in a Freeze Version.*****--%></Font>
			 <%}%>
			 </p>

			 <%
			SectionDao sectionDao = sectionB.getStudy(studyVerId,flag,searchCriteria);
		   	ArrayList ids		=sectionDao.getIds();
			int len= ids.size();
			if(len == 0){
					sectionDao = sectionB.getStudy(studyVerId,flag,"");
		   			ids=sectionDao.getIds();
					len1= ids.size();
			}
		   	ArrayList secStudys		=sectionDao.getSecStudys();
		   	ArrayList secNames		=sectionDao.getSecNames();
			ArrayList secNums		=sectionDao.getSecNums();
		   	ArrayList secContents	=sectionDao.getContents();
		   	ArrayList secPubFlags	=sectionDao.getSecPubFlags();
	   	   	ArrayList secSequences	=sectionDao.getSecSequences();
		   	String studySecName = "s";
			String studySecNum = "";
		   	String studySecContents = "h";
		   	String sectionId="";
		   	String secPubFlag="Y";


  	        int counter = 0;
			%>
			<Form name="sectionbrowser" method="post" action="sectionBrowserNew.jsp?srcmenu=tdmenubaritem3&selectedTab=<%=isIrb?"irb_upload_tab":"2"%>&mode=S&studyId=<%=studyId%>&studyVerId=<%=studyVerId%>" onsubmit="">
    			<input type="hidden" name="srcmenu" value='<%=src%>'>
				<input type="hidden" name="selectedTab" value='<%=tab%>'>
			    <table width="100%" cellspacing="0" cellpadding="0" border="0" class="basetbl">
				<tr>
					<td colspan=2>
					<table><tr><td valign=center>
					 <%=LC.L_Search_Sec%><%--Search Section*****--%></td><td valign=center>
					 &nbsp;&nbsp&nbsp;&nbsp&nbsp;&nbsp
					<Input type=text name="searchCriteria" size="40%">
					&nbsp;&nbsp&nbsp;&nbsp&nbsp;&nbsp
					<button type="submit"><%=LC.L_Search%></button>
					</td></tr>
					</table>
					</td>
					</tr>

					<tr>
						<td colspan="2">
						 <%
						 	if(len == 0){
									if (mode.equals("S")){
							%>
								<P class = "defComments"><FONT class="Mandatory"> <%=MC.M_SrchCrit_SecEdit%><%--Your search criteria did not bring any records, hence, all records retrieved.</font> [Click on Section's name to edit its contents]*****--%>
							</P>
						 <%

						 		len = len1;
						 		}
						 }else{
							if (!(searchCriteria.equals(""))) {Object[] arguments = {searchCriteria};%>
								<P class = "defComments"> <%=VelosResourceBundle.getMessageString("M_FlwgSec_NameCont",arguments)%><%--The following section(s) match your search criteria  '<%=searchCriteria%>'. [Click on Section's name to edit its contents]*****--%></P>
						    <%}
						 }
							%>

						</td>
				    </tr>

					<% if (!(verStatus.equals("F"))) {%>
				    <tr>
					   	<td width="50%">
							<P class = "defComments">
					            <A href="studysection.jsp?sectionId=<%=sectionId%>&mode=N&srcmenu=<%=src%>&selectedTab=<%=tab%>&studyVerId=<%=studyVerId%>" onClick="return f_check_perm(<%=pageRight%>,'N')"><%=MC.M_ClkHere_AddNewSec%><%--CLICK HERE</A> to add a new Section*****--%>
							</P>
				        </td>
				        <td width = "50%">
							<%
							if(len > 0) {
							%>
								<P class = "defComments">
							        <A href="sectionSequence.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&studyVerId=<%=studyVerId%>" onClick="return f_check_perm(<%=pageRight%>,'E')"><%=MC.M_ClkHere_ChgSeqSec%><%--CLICK HERE</A> to change the sequencing of the sections*****--%>
								</P>
								<%
							}
							%>
				        </td>
					</tr>
					<%}%>
					<tr>
						<td height="5"></td>
				        <td height="5"></td>
					</tr>
				</table>
			    <table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl outline midalign">
				<th width="25%" > <%=LC.L_Section_Number%><%--Section Number*****--%> </th>
        		<th width="60%" > <%=LC.L_Sec_Name%><%--Section Name*****--%> </th>
        		<th width="15%" > <%=LC.L_Delete%><%--Delete*****--%> </th>
					<%
				    for(counter = 0;counter<len;counter++)
					{
						studySecName=((secNames.get(counter)) == null)?"-":(secNames.get(counter)).toString();
						studySecNum=((secNums.get(counter)) == null)?"-":(secNums.get(counter)).toString();
						//studySecContents=((secContents.get(counter)) == null)?"-":(secContents.get(counter)).toString();
						sectionId=((ids.get(counter)) == null)?"-":(ids.get(counter)).toString();
						//secPubFlag=((secPubFlags.get(counter)) == null)?"-":(secPubFlags.get(counter)).toString();
						if ((counter%2)==0) {
						%>
						    <tr class="browserEvenRow">
					        <%
						}
						else{
						%>
						    <tr class="browserOddRow">
				        <%
						}
						%>
						<td>
						<%=studySecNum%>
						</td>
						<td>
						<A href="studysection.jsp?sectionId=<%=sectionId%>&mode=M&srcmenu=<%=src%>&selectedTab=<%=tab%>&studyVerId=<%=studyVerId%>" ><%=studySecName%></A>
						</td>
						<td align="center">
						<%if(!verStatus.equals("F")){%>
						<A href="studysectiondelete.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&studySectionId=<%=sectionId%>&delMode=null&studyVerId=<%=studyVerId%>" onclick="return confirmBox('<%=studySecName%>',<%=pageRight%>)"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0"/></A>
							<%}%>
						</td>
					</tr>
					<%
					}
					%>

				</table>
			</Form>
			<%
			}else{%>
				<jsp:include page="accessdenied.jsp" flush="true"/>
			<%}
		} //end of if body for check on studyId



	} //end of if session times out
	else
	{
	%>
		<jsp:include page="timeout.html" flush="true"/>
  	<%
	} //end of else body for page right
	%>
	<br>
	<div>
    	<jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
</div>
<div class ="mainMenu" id="emenu">
  	<jsp:include page="getmenu.jsp" flush="true"/>
</div>

</body>

</html>
