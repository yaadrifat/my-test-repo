<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<%@page import="com.velos.eres.service.util.*"%>
<title><%=MC.M_PrsnlzAcc_MyLnks%><%--Personalize Account >> My Links*****--%></title>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<% String src;
src= request.getParameter("srcmenu");
String pname="ulinkBrowser";
%>
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   
 
<SCRIPT language="javascript">
function confirmBox(ln,pgRight) {
	if (f_check_perm(pgRight,'E') == true) {
		var paramArray = [decodeString(ln)];
		if (confirm(getLocalizedMessageString("M_Del_FrmLnks",paramArray)))/*if (confirm("Delete " + decodeString(ln) + " from your links?"))*****/ {// BUG # 4843: Fixed by Prabhat
    		return true;
		} else	{
		return false;
		}
	}else {
		return false;
	}
}
</SCRIPT> 

         <!-- Modified alignments for the defect fix #2997 by Amar -->
<body>
	<jsp:useBean id="ulinkB" scope="request" class="com.velos.eres.web.ulink.ULinkJB"/>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<%@ page language = "java" import = "com.velos.eres.business.ulink.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*"%>
	<%
	HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession))
	{
		String uName = (String) tSession.getValue("userName");
		String tab = request.getParameter("selectedTab");
		int pageRight = 0;	
		GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
		
		//pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("HPERSHOME"));
		pageRight = 7;
		
		if (pageRight > 0 )
		{
		%>
		<%
			String usrId ;  
			usrId = (String) tSession.getValue("userId");
			UsrLinkDao usrLinkDao = ulinkB.getULinkValuesByUserId(Integer.parseInt(usrId));
			ArrayList lnksIds = usrLinkDao.getLnksIds(); 
			ArrayList lnksUris = usrLinkDao.getLnksUris();
			ArrayList lnksDescs = usrLinkDao.getLnksDescs();
			ArrayList lnksGrps = usrLinkDao.getLnksGrpNames();
			String lnkUri = null;
			String lnkDesc = null;
			int lnkId = 0;
			String lnkGrp = null;		
			String oldGrp = null;	
			int len = lnksIds.size();
			int counter = 0;
			%>


<DIV class="tabDefTopN" id = "divtab">
			<jsp:include page="personalizetabs.jsp" flush="true"/>

</DIV>
<DIV class="tabDefBotN" id = "div1">
			<table width="100%" cellspacing="0" cellpadding="0" border="0" >
			<!--  <tr><td>&nbsp;</td></tr> -->
				<tr > 
					<td width = "40%"> 
						<P class = "defComments_txt"> <%=MC.M_ListDisp_LnkToHPg%><%--The list below displays links to be included in the 'My Links' section of your Homepage*****--%>: </P>
				    </td>
				    <td width="20%" align="right"  > 
	 <A class="rhsFont" href="linklist.jsp?mode=N&srcmenu=<%=src%>&selectedTab=<%=tab%>&lnkType=user"><%=LC.L_AddNew_Links_Upper%><%--ADD NEW LINKS*****--%></A>
				<!--   <A href="link.jsp?mode=N&srcmenu=<%=src%>&selectedTab=<%=tab%>&lnkType=user">ADD A NEW LINK</A> -->
				    </td>
			    </tr>
			</table>
			<Form name="usrlnkbrowser" method="post" action="" onsubmit="">
				<Input type="hidden" name="selectedTab" value="<%=tab%>">
				<table class="outline midAlign" width="99%" cellpadding="0" cellspacing="0" >
					<tr> 
						<th width="25%"> <%=LC.L_Sec_Heading%><%--Section Heading*****--%> </th>
						<th width="35%"> <%=LC.L_Link_Desc%><%--Link Description*****--%> </th>
						<th width="30%"> <%=LC.L_Link_Url%><%--Link URL*****--%> </th>
						<th width="10%">  <%=LC.L_Delete%><%--Delete*****--%> </th>
					</tr>
					<%
					for(counter = 0;counter<len;counter++)
					{		
						lnkUri=((lnksUris.get(counter))==null)?"-":(lnksUris.get(counter)).toString();
						lnkDesc=((lnksDescs.get(counter))==null)?"-":(lnksDescs.get(counter)).toString();
						lnkGrp = ((lnksGrps.get(counter))==null)?"-":(lnksGrps.get(counter)).toString();
						lnkId =  ((Integer)lnksIds.get(counter)).intValue();
						if ((counter%2)==0) {
						%>
							<tr class="browserEvenRow"> 
				        <%
						}
						else{
						%>
						    <tr class="browserOddRow"> 
				        <%
						}
						%>
				        <td width="150"> 
							<% if ( ! lnkGrp.equals(oldGrp)){ %>
						        <%= lnkGrp%> 
					        <%}%>
				        </td>
        				<td width="150"> <A href = "link.jsp?mode=M&lnkId=<%= lnkId%>&srcmenu=<%=src%>&selectedTab=<%=tab%>&lnkType=user"><%= lnkDesc%></A> 
				        </td>
				        <td width="150"> <A href="<%= lnkUri%>" target = "_new"><%= lnkUri%></A> 
				        </td>
				        <!-- Changed By PK Bug #4017 --> 
						<td align="center"><A href="deletelnk.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&mode='u'&lnkId=<%=lnkId%>" onClick="return confirmBox('<%= StringUtil.encodeString(lnkUri) %>',<%=pageRight%>);" ><img src="./images/delete.gif" border="0"  title="<%=LC.L_Delete%>"/></A>
						</td>
					</tr>
				    <%
					oldGrp = lnkGrp;
			 		}
					%>
				<!-- <tr> 
						<td width="130" height=20></td>
				        <td width=22 height=20></td>
				    </tr>  -->	
				</table>
			</Form>
			<%
		} //end of if body for page right
		else
		{
		%>
			<jsp:include page="accessdenied.jsp" flush="true"/>
		<%
		} //end of else body for page right
	}//end of if body for session
	else
	{
	%>
		<jsp:include page="timeout.html" flush="true"/>
	<%
	}
	%>
	<div> 
    	<jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
</div>
<div class ="mainMenu" id = "emenu" > 
  <jsp:include page="getmenu.jsp" flush="true"/>
</div>
</body>

</html>

