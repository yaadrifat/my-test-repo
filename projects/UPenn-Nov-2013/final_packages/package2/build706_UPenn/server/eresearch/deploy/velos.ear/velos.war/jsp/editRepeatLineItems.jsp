<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@page import="com.velos.eres.service.util.*"%>
<html>
<head>
<title><%=MC.M_AddOrEdtRptg_LineItem%><%-- Add/Edit Repeating Line Items*****--%> </title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
<!--#5747 02/10/2011 @Ankit-->
<jsp:useBean id="budgetB" scope="page"	class="com.velos.esch.web.budget.BudgetJB" />

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<jsp:include page="include.jsp" flush="true"/>
</head>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<SCRIPT Language="javascript">


function confirmBox(lineitem, pgRight) {

	if (f_check_perm(pgRight,'E') == false) {
		return false;
	}
	var paramArray = [lineitem];
	msg=getLocalizedMessageString("M_Del_FrmRptItm",paramArray);/*msg="Delete " + lineitem + " from Repeat Line Items?";*****/

	if (confirm(msg)) 
	{
    	return true;
	}
	else
	{
		return false;

	} 
 }



 function  validate(formobj){
	 
	var i = 0;
	var countChng = 0;

	var arrlen = formobj.arrlen.value;	
	var defRowsNo = formobj.defRowsNo.value;
		
	var pgRight =parseInt(formobj.pageRight.value,10);
	var mode = formobj.mode.value;
	
	if (!(pgRight < 6 && mode=="M")){
		if(arrlen == 1)
		 {
			if((formobj.itemCptCode.value != "") || (formobj.itemDesc.value != ""))
			 {
			 
				if(formobj.type.value == "")
			 	{
					alert("<%=MC.M_Etr_MandantoryFlds%>");/*alert("Please enter data in all mandatory fields");*****/
					formobj.type.focus();
					return false;
				 }
			  }
		 }
	
		 if(arrlen > 1 )
		 {
			for(i =0 ;i<arrlen;i++){
		
				if(formobj.itemCptCode[i].value != "" ||  formobj.itemDesc[i].value != "")
				 {
					if(formobj.type[i].value == "")
					 {			
						alert("<%=MC.M_Etr_MandantoryFlds%>");/*alert("Please enter data in all mandatory fields.");*****/
						formobj.type[i].focus();
						return false;
					}
				 }
			}
		}
	}
	if(formobj.refresh.value == "false"){
		if(defRowsNo == 1 )
		 {
			if((formobj.newCptCode.value != "") || (formobj.newDesc.value != ""))
			{				 
				if(formobj.newEventdata.value == "")
				 {
					alert("<%=MC.M_Etr_MandantoryFlds%>");/*alert("Please enter data in all mandatory fields");*****/
					formobj.newEventdata.focus();
					return false;
				 }
		  	}		 
		 }
		if(defRowsNo > 1)
		 {
			for(i =0 ;i<defRowsNo;i++){
				
				if(formobj.newCptCode[i].value != "" ||  formobj.newDesc[i].value != "")
				 {
					if(formobj.newEventdata[i].value == "")
					 {			
						alert("<%=MC.M_Etr_MandantoryFlds%>");/*alert("Please enter data in all mandatory fields.");*****/
						formobj.newEventdata[i].focus();
						return false;
					}
				 }
			}
		}
	}
	if (!(validate_col('e-Signature',formobj.eSign))) return false;
	else return true;
}

function refreshPage(formobj, pgRight)
{
	if (f_check_perm(pgRight,'N') == false) {
		return false;
	}


	moreRows = formobj.moreRows.value;
	if(moreRows=='' ){
		alert("<%=MC.M_EtrNum_ForMoreRows%>")/*alert("Please enter the number for More Rows.")*****/
       formobj.moreRows.focus();
       return false;
	}			 			 

	 if(isNaN(moreRows) == true) {
		 alert("<%=MC.M_ValidNum_InAddRows%>");/*alert("Please enter a valid number in Add More Rows.");*****/
		 formobj.moreRows.focus();
		 return false;
	}
	if(pgRight != 5){	     
      /*if (confirm("Refreshing the number of rows will save the data entered above. Do you wish to continue?"))*****/
      if (confirm("<%=MC.M_Refreshing_RowsSvdata%>"))
  	  {
  	   	  formobj.refresh.value = "true"; 	  
  	   	  if (validate(formobj)){
  	   	  formobj.submit();
  	   	   }
  	  	  }
  	 	 else {	return false;}
	}else{
	
  	   	  formobj.refresh.value = "true"; 	  
  	   	  formobj.submit();
	}
}


</SCRIPT>

</head>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="lineitemB" scope="session" class="com.velos.esch.web.lineitem.LineitemJB"/>
<%@ page language = "java" import = "com.velos.esch.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%>
<jsp:include page="include.jsp" flush="true"/>
<% 
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
if (agent1 != null && agent1.indexOf("MSIE") != -1) 
   	ienet = 0; //IE
else
	ienet = 1;

if (ienet == 0) {
%>
<body style="overflow:scroll" >
<%} else {%>
<body >
<%}%>
<DIV class="popDefault" id="div1" style="width:auto;"> 
 <%

	int pageRight = 0;
	

	
	String itemMoreRows = "";
	
	String mode = request.getParameter("mode"); 
	
	String budgetId = request.getParameter("budgetId");
	String bgtcalId  = request.getParameter("bgtcalId");
	
	String budgetTemplate = request.getParameter("budgetTemplate");
	budgetTemplate=(budgetTemplate==null)?"":budgetTemplate;
	
	ArrayList arrLineitemIds = null;
	ArrayList arrLineitemNames = null;
	ArrayList arrLineitemDescs = null;
	ArrayList arrLineitemCptCodes = null;
	ArrayList arrLineitemRepeats = null;
	ArrayList arrApplyInFuture = null;
	ArrayList arrLineitemCategories = null;
	//Rohit CCF-FIN21
	//ArrayList arrLineitemTMIDs = null;
	//ArrayList arrLineitemCDMs = null;
	String defaultCategory = "";
	String ctgryPullDn = "";
	SchCodeDao schDao1 = new SchCodeDao();
	schDao1.getCodeValues("category");
	ctgryPullDn = schDao1.toPullDown("cmbCtgry");
	
	defaultCategory = EJBUtil.integerToString(new Integer (schDao1.getCodeId("category", "ctgry_patcare")) );

	
	String bgtStatCode = request.getParameter("bgtStatCode");
	String bgtStatDec ="";
	
	if (EJBUtil.isEmpty(bgtStatCode))
		bgtStatCode ="W";
	// #5747 02/10/2011 @Ankit
	BudgetDao budgetDao = budgetB.getBudgetInfo(EJBUtil.stringToNum(budgetId));
	ArrayList bgtStatDescs = budgetDao.getBgtCodeListStatusesDescs();
	if(bgtStatDescs!=null && bgtStatDescs.size()>0){
		bgtStatDec = (String) bgtStatDescs.get(0);
	}

	int itemId = 0;				
	String itemName = "";
    String itemDesc = "";
	String itemCptCode = "";					
	String itemRepeat = "";
	String applyInFuture = "";
	String itemCategory = "";
	//Rohit CCF-FIN21
	//String itemTMID = "";
	//String itemCDM = "";
	int count=5;
	String inputClass = "inpDefault";
	String makeReadOnly = "";
	String makeGrey = "Black";
	String makeDisabled = "";
	String ftextGrey = "textDefault";  
	boolean enableFlag = true;
	
	HttpSession tSession = request.getSession(true); 

	

	if (sessionmaint.isValidSession(tSession))	{
		String refresh=request.getParameter("refresh");
		
		String fromRt = request.getParameter("fromRt");
		if (fromRt==null || fromRt.equals("null")) fromRt="";
		
		 if (fromRt.equals("")){
			GrpRightsJB bRights = (GrpRightsJB) tSession.getAttribute("BRights");		
			pageRight = Integer.parseInt(bRights.getFtrRightsByValue("BGTDET"));
		 } else{
			pageRight = EJBUtil.stringToNum(fromRt);
		 }
		

		if ( mode.equals("M") &&(pageRight< 6) ){
			makeReadOnly = "readonly";
			inputClass = "inpGrey";
			makeGrey = "Grey";
			makeDisabled ="disabled";
			ftextGrey = "textGrey";
			enableFlag = false;
		}
	
		if (refresh==null) refresh="false";
		int imoreRows=EJBUtil.stringToNum(request.getParameter("moreRows")); 
		int defRowsNo = 0;
		String pageMode = request.getParameter("pageMode");
		
		int arrlen = 0;
		
		
		LineitemDao lineDao = new LineitemDao();
		
		lineDao = lineitemB.getLineitemsOfRepeatDefaultSec(EJBUtil.stringToNum(bgtcalId));
		arrLineitemIds = lineDao.getLineitemIds();
		
		arrLineitemNames = lineDao.getLineitemNames();
		arrLineitemDescs = lineDao.getLineitemDescs();
		arrLineitemCptCodes = lineDao.getLineitemCptCodes();
		arrLineitemCategories = lineDao.getLineitemCategories();
		//Rohit CCF-FIN21
		//arrLineitemTMIDs = lineDao.getLineitemTMIDs();
		//arrLineitemCDMs = lineDao.getLineitemCDMs();
		
		arrLineitemRepeats = lineDao.getLineitemRepeats();
		arrApplyInFuture = lineDao.getApplyInFuture();
		
		
		arrlen = arrLineitemIds.size();
		

		SchCodeDao schDao = new SchCodeDao();
		boolean b= schDao.getCodeValues("bgtline_repeat");
		
		String repOption = schDao.toPullDown("repeatOption",-1); 
		
		
%>
	<%
  	    //Added by IA 11.03.2006 Comparative Budget
  	      if ( budgetTemplate.equals("C") ){ 
    %>
	<P class="sectionHeadings"><%=MC.M_CompBgt_AddEdtItem%><%-- Comparative Budget >> Add/Edit Repeat Line Items*****--%></P>	
	<%}else{
	%>
	<P class="sectionHeadings"><%=MC.M_PatBgt_AddEdtItem%><%-- <%=LC.Pat_Patient%> Budget >> Add/Edit Repeat Line Items*****--%></P>	
	<%}%>
	
	<%if (!((pageRight == 7 ) || (pageRight==5 ) || (arrlen > 0 && pageRight == 6 )  )){%>
	<P class = "defComments"><FONT class="Mandatory">	
	<%=MC.M_InfoNotSvd_UsrNoRgt%><%-- Information in this page will not be saved, user doesn't have access right*****--%></Font></P>
	<%}%>
				<!--   Fixed Bug No: 4549 Put Mandatory Field Validation -->
    <Form name="editCost" id="editCostRepeatline" method="post" action="editRepeatLineItemsSubmit.jsp" onSubmit="if (validate(document.editCost)==false) {setValidateFlag('false');return false;} else {setValidateFlag('true'); return true;}" >
		<Input type="hidden" name="refresh" value="false" >
		<Input type="hidden" name="mode" value="<%=mode%>"/>
		<Input type="hidden" name="pageRight" value="<%=pageRight%>"/>
		<Input type="hidden" name="budgetId" value=<%=budgetId%> >
		<input type="hidden" name="bgtcalId" value=<%=bgtcalId%>>
		<input type="hidden" name="bgtStatCode" value=<%=bgtStatCode%>>
		<%
				  
		if ((!(bgtStatCode ==null)) && (bgtStatCode.equals("F") || bgtStatCode.equals("T")) ) 
		{ Object[] arguments = {bgtStatDec};
		%>
			 <table width="600" cellspacing="2" cellpadding="2">
			 	<tr>
					<td>
					    	<P class = "defComments"><FONT class="Mandatory"><%=VelosResourceBundle.getMessageString("M_BgtStat_CntChgBgt",arguments)%><%-- Budget Status is '<%=bgtStatDec%>'. You cannot make any changes to the budget.*****--%></Font></P>
					</td>
				</tr>
			</table>
		<%
		}
		if (!(bgtStatCode.equals("F") || bgtStatCode.equals("T") )) 
		{
// SV, 8/25, disable the refresh link if "new" permission is not there.
		if ( (mode.equals("M") && ((pageRight==5) || (pageRight==7)) ) || (mode.equals("N")) ){

		%>
		
		<!-- <table>
		<td><P class="defComments">Add More Rows</P></td>
		 <td><input type="text" name="moreRows" size =3 MAXLENGTH = 3 value="<%=itemMoreRows%>"></td>
		 <td colspan="1"><A href=# onclick="refreshPage(document.editCost, <%=pageRight%>)">Refresh</A></td>
		 </table> -->
	  <%}} %>

	<table width ="100%" >
		<tr>
			<th width="20%" align=center><%=LC.L_Event%><%-- Event*****--%></th>
			<th width="15%" align=center><%=LC.L_Cpt_Code%><%-- CPT Code*****--%></th>
			<th  width="10%" align=center><%=LC.L_Description%><%-- Description*****--%></th>
			<th  width="10%" align=center><%=LC.L_Category%><%-- Category*****--%></th>
			<!-- 
			<th  width="10%" align=center>TMID</th>
			<th  width="10%" align=center>CDM</th>
			 -->
			<th width="20%" align=center><%=LC.L_Repeating_LineItems%><%-- Repeating Line Items*****--%></th>
			<th width="25%">
			<!--SV,REDTAG, 8/26/04		 	 if (mode.equals("M") || arrlen > 0) { -->
		 	 <%if (arrlen > 0) {%>
			 	  <%=LC.L_Apply_Changes%><%-- Apply Changes To*****--%>
			 <%}%>
			 </th>
		</tr>
		<% 
//SV,REDTAG		if (mode.equals("M") || arrlen > 0)
		if (arrlen > 0)
		{	
			 
			 for(int i =0;i<arrlen;i++)
				{
				itemId = ((Integer)arrLineitemIds.get(i)).intValue();				
				itemName = (String)arrLineitemNames.get(i);
				itemDesc = (String)arrLineitemDescs.get(i);
				itemDesc = (itemDesc== null)?"":(itemDesc) ;
				itemCptCode = (String)arrLineitemCptCodes.get(i);
				itemCptCode = (itemCptCode== null)?"":(itemCptCode) ;
				itemRepeat = (String)arrLineitemRepeats.get(i);	
				itemCategory = (String)arrLineitemCategories.get(i);
				//Rohit CCF-FIN21
				//itemTMID = (String)arrLineitemTMIDs.get(i);	
				//itemCDM = (String)arrLineitemCDMs.get(i);
				/*
				if( itemTMID == null)
					itemTMID = "";

				if( itemCDM == null)
					itemCDM = "";
				*/
				
				applyInFuture = (String)arrApplyInFuture.get(i);
			    ctgryPullDn = schDao1.toPullDown("cmbCtgry",EJBUtil.stringToNum(itemCategory), enableFlag);
				
				%>
				 <tr>
				 
				 <td>
				<%if ( mode.equals("M") &&(pageRight< 6) ){  %>
					<%=itemName%>
				<%} else {%>
					 <input type ="hidden" name="lineitemIds" value=<%=itemId%>>
					 <input class ="<%=inputClass%>" type=text name = "type" maxlength=250 value='<%=itemName%>' <%=makeReadOnly%>>
				<%} %>
				</td>
				<td>
				<%if ( mode.equals("M") &&(pageRight< 6) ){  %>
					<%=itemCptCode%>
				<%} else {%>
					<input class ="<%=inputClass%>" type=text name = "itemCptCode" size=10 maxlength=25  value='<%=itemCptCode%>' <%=makeReadOnly%>>
				<%} %>
				</td>
				<td>
				<%if ( mode.equals("M") &&(pageRight< 6) ){  %>
					<%=itemDesc%>
				<%} else {%>
					<input class ="<%=inputClass%>" type ="text" name="itemDesc" maxlength=250 value='<%=itemDesc%>' <%=makeReadOnly%>></td>
				<%} %>
				<td>
				<%if ( mode.equals("M") &&(pageRight< 6) ){  %>
					<%=schDao.getCodeDescription(EJBUtil.stringToNum(itemCategory))%>
				<%} else {%>
					<%=ctgryPullDn%>
				<%} %>
				</td>
				
				<%-- 
				<td ><input class ="<%=inputClass%>"  type=text name="tmid" size=10 maxlength=15  value='<%=itemTMID%>' <%=makeReadOnly%>></td> 
				<td ><input class ="<%=inputClass%>"  type=text name="cdm" size=10 maxlength=15 value='<%=itemCDM%>' <%=makeReadOnly%>></td> 
				--%>
				<td>
				<%if ( mode.equals("M") &&(pageRight< 6) ){  %>
					<%=schDao.getCodeDescription(EJBUtil.stringToNum(itemRepeat))%>
				<%} else {%>
				 	<% repOption = schDao.toPullDown("repeatOption",EJBUtil.stringToNum(itemRepeat), enableFlag); %>
				 	<%=repOption%>
				<%} %>
				</td>
				
				<%if(mode.equals("M") || arrlen > 0){%>
				<td>
				<%if ( mode.equals("M") &&(pageRight< 6) ){  %>
					<%if(applyInFuture.equals("0")){%>
						<%=LC.L_Future_Sections %><%-- Future Sections*****--%>
					<%}else if(applyInFuture.equals("1")){%>
						<%=MC.M_Prev_FutureSec %><%-- Previous and Future Sections*****--%>
					<%} %>
				<%} else {%>
					<select name = "applyFutureCost" <%=makeDisabled%>>
					<%if(applyInFuture.equals("0")){%>
						<option value="0" selected><%=LC.L_Future_Sections %><%-- Future Sections*****--%></option>
						<option value = "1"><%=MC.M_Prev_FutureSec %><%-- Previous and Future Sections*****--%></option>
					<%}else if(applyInFuture.equals("1")){%>
						<option value="0" ><%=LC.L_Future_Sections %><%-- Future Sections*****--%></option>
						<option value = "1" selected><%=MC.M_Prev_FutureSec %><%-- Previous and Future Sections*****--%></option>
					<%}%>
					</select>
				<%} %>
				</td>
 				<% if (!(bgtStatCode.equals("F") || bgtStatCode.equals("T") )){%>
				<td><A href ="editPerCostDelete.jsp?lineitemId=<%=itemId%>&bgtcalId=<%=bgtcalId%>&applyInFuture=<%=applyInFuture%>&from=repLineitem&fromRt=<%=fromRt%>" onClick="return confirmBox('<%=itemName%>', <%=pageRight%>)"><%=LC.L_Delete%><%--Delete*****--%></A></td>
				<%} //for bgtStateCode%>
			<%}%>
			</tr>	
				
				<%}
	}//mode M


	
	if((mode.equals("M")) && (refresh.equals("true")))  { 
	
	
	}
 if((arrlen == 0 && pageMode.equals("first")) || pageMode.equals("refresh")){
  //SV,REDTAG 8/25/04 if ((mode.equals("N")) || (refresh.equals("true"))) {
	   int seqNum = 0;
  //new mode
repOption = schDao.toPullDown("newrepeatOption",-1); 
ctgryPullDn = schDao1.toPullDown("newCmbCtgry", EJBUtil.stringToNum(defaultCategory));

  defRowsNo =10;

  if (refresh.equals("true")) 
  {
   	 defRowsNo = imoreRows;  
  %>
  	<Input type="hidden" name="fromRefresh" value="true" >
  <%
  }
  
 for(int i=0;i<defRowsNo;i++) 
 {
  

%>
<tr>
 	<td><input type=text name="newEventdata" maxlength=250 ></td>
	 <td><input type=text name="newCptCode" maxlength=25 size=10  ></td>
	 <td><input type=text name="newDesc" maxlength=250 ></td>
	<td><%=ctgryPullDn%></td>
<!-- 
	<td ><input type=text name="newtmid" maxlength=15  size=10 value=''></td> 
	<td ><input type=text name="newcdm" maxlength=15 size=10 value=''></td> 
-->
 	<td><%=repOption%></td>

</tr>
 <%
 
  } //loop for despRespNo 
%> 
<% 
 
 //}//mode=N
}
/*SV, REDTAG, 8/26/04, refer to description in bug#1679. mode is not set consistently leading to issues here. Create a new parameter
to communicate to the corresponding update jsp about new rows being added.
*/

 if (defRowsNo > 0) { %>
 	<td><input type=hidden name="newRows" value='Y'></td>
 <%} else { %>
 	<td><input type=hidden name="newRows" value='N'></td>
 <%} %>



		</table>


<%
if (!(bgtStatCode.equals("F") || bgtStatCode.equals("T") )) 
	{



if ((pageRight == 7 ) || (pageRight==5 ) || (arrlen > 0 && pageRight == 6 )  ){%>
<%-- YK 07JAN- Fix for Bug #4383 and #5368 --%>
<table>
		<td><P class="defComments"><%=LC.L_Add_MoreRows%><%-- Add More Rows*****--%></P></td>
		 <td><input type="text" name="moreRows" size =3 MAXLENGTH = 3 value="<%=itemMoreRows%>"></td>
		 <td colspan="1"> <%=MC.M_EntDesiredNum_EsignBelow%><%-- Enter the desired number of new rows in the box to the left, your e-Signature below and then click*****--%> <A href=# onclick="refreshPage(document.editCost, <%=pageRight%>)"><%=LC.L_Refresh%><%-- Refresh*****--%></A></td>
		 </table>
	<BR>
		<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="editCostRepeatline"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>
<%}
		else{%>
			<table width="100%" >
				<tr><td width="100%" align="center">				
				<button onClick = "self.close()"><%=LC.L_Close%></button>
				</td></tr>
			</table>	
		<%}
} //for bgtStatCode
%>
<input type= hidden name="arrlen" value="<%=arrlen%>"/>
<input type=hidden name="pageMode" value="<%=pageMode%>"/>
<input type=hidden name="defRowsNo" value="<%=defRowsNo%>"/>	
<Input type="hidden" name="fromRt" value="<%=fromRt%>" />


</form>



<%

	


} else {  //else of if body for session



%>

  <jsp:include page="timeout.html" flush="true"/>

  <%



}



%>

<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
  
</body>


</html>

