<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title><%=MC.M_AdhocQry_ViewCriteria%><%--Ad-Hoc Query : View Filter Criteria*****--%></title>
	<%@ page language = "java" import = "java.util.*,com.velos.eres.web.dynrep.holder.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB"%>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<jsp:useBean id="dynrepB" scope="page" class="com.velos.eres.web.dynrep.DynRepJB" />

	<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
	<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

 <SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

</head>

<% String src,sql="",tempStr="",sqlStr="",repMode="";
   int strlen=0,firstpos=-1,secondpos=-1;
src= request.getParameter("srcmenu");
%>



<%
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
if (agent1 != null && agent1.indexOf("MSIE") != -1)
   	ienet = 0; //IE
else
	ienet = 1;

if (ienet == 0) {
%>
<body style="overflow:scroll">
<%} else {%>
<body>
<%}%>

<br>

 <DIV class="popDefault" id="div1">
 <P class="sectionHeadings"><%=MC.M_AdhocQry_ViewCriteria%><%--Ad-Hoc Query : View Filter Criteria*****--%></P>

<%




	String value="",patientID="",studyNumber="",orderStr="",order="",flgMatch="",fltrName="", prevId="",fldType="",dynType="",selFltr="";

	String scriteria="",fldData="";
	int personPk=0,studyPk=0,index=-1,arraySize=0,row=0,colLen=0;
	DynRepDao dynDao=new DynRepDao();
	String scriteriaText = "";

	ArrayList selectedfld=null;
	ArrayList flltDtIds=new ArrayList();
	ArrayList dataList=new ArrayList() ;
	ArrayList prevSessColId=new ArrayList();
	ArrayList tempList=new ArrayList();
	ArrayList startBracList=new ArrayList();
	ArrayList endBracList=new ArrayList();
	ArrayList scriteriaList=new ArrayList();
	ArrayList fldDataList=new ArrayList();
	ArrayList sessCol=new ArrayList();
	int totCount  = 0;




	ArrayList masterFieldColList=new ArrayList();
	ArrayList masterFieldNameList=new ArrayList();

	String[] fldNameSelect=null;
	String[] extend=null;
	String extendText = "";


	ArrayList fldNameSelList=new ArrayList();
	ArrayList extendList=new ArrayList();
	ArrayList excludeList=new ArrayList();
	ArrayList multiFltrIds=new ArrayList();
	ArrayList multiFltrNames=new ArrayList();

	String defDateFilterType = "";
	String defDateFilterDateFrom = "";
	String defDateFilterDateTo = "";

	String prevSelForm = "";

	Hashtable htFormPKInfo = new Hashtable();
	int fldSelectArrayCount = 0;
	int fldIndex = 0;
	int velSepPos = 0;
	int formIndex = 0;

	String selColName = "";

	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))
	{
	ReportHolder container=(ReportHolder)tSession.getAttribute("reportHolder");
		if (container==null){%>
	<META HTTP-EQUIV=Refresh CONTENT="0; URL=dynrepbrowse.jsp?srcmenu=<%=src%>">

	<%}else {

	  int pageRight = 0;
  	  GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");
      pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("ADHOC"));



	String acc = (String) tSession.getValue("accountId");
	int accId = EJBUtil.stringToNum(acc);




	FilterContainer filterContainer=null;
	String selValue = "";;
	ArrayList formIds;
    ArrayList formNames;
    String formName = "";
    String formId = "";


	container.compileFormFields();
	multiFltrIds=container.getFilterIds();
	multiFltrNames=container.getFilterNames();


	masterFieldColList = container.getMasterFieldId();
    masterFieldNameList = container.getMasterFieldName();
    formIds = container.getFormIds();
	formNames = container.getFormNames();



	htFormPKInfo = container.getHtFormTablePKInfo();



	container.Display();

	for ( int fil=0;fil < multiFltrIds.size(); fil++)
	{

	   selFltr = (String)multiFltrIds.get(fil);

	  filterContainer=container.getFilterObject(selFltr);


	  if (filterContainer!=null)
	  {
		flltDtIds=filterContainer.getFilterDbId();
		startBracList=filterContainer.getFilterBbrac();
		endBracList=filterContainer.getFilterEbrac();
		fldNameSelList=filterContainer.getFilterCols();
		scriteriaList=filterContainer.getFilterQualifier();
		fldDataList=filterContainer.getFilterData();
		extendList=filterContainer.getFilterOper();
		excludeList=filterContainer.getFilterExclude();
		fltrName = filterContainer.getFilterName();

		defDateFilterType = filterContainer.getFilterDateRangeType();
		defDateFilterDateTo = filterContainer.getFilterDateRangeTo();
		defDateFilterDateFrom = filterContainer.getFilterDateRangeFrom();

		 fltrName=filterContainer.getFilterName();
		 fltrName=(fltrName==null)?"":fltrName;


		}


	int counter=0;
	String name="";

%>

<P class="sectionHeadings"><%=LC.L_Filter_Name%><%--Filter Name*****--%>: <%=fltrName%> </P>
<form name="dynadvanced" method="post">


<%
	CodeDao cdStatus = new CodeDao();
	cdStatus.getCodeValues("patStatus",accId);

	ArrayList arCid = new ArrayList();
	arCid = cdStatus.getCId();

   	ArrayList arCSubType = new ArrayList();
	arCSubType = cdStatus.getCSubType();

   	ArrayList cDesc = new ArrayList();
	cDesc = cdStatus.getCDesc();

	 String statusDDfromSelValue = "";String statusDDToSelValue = "";
	String selMonth = ""; String selMonthYear = ""; String selYear="";

	if (StringUtil.isEmpty(defDateFilterType))
	{
		defDateFilterType = "A";
	}
	if (defDateFilterType.equals("A"))
	{
		defDateFilterDateFrom = "";
		defDateFilterDateTo = "";

	}


	if (defDateFilterType.equals("PS"))
	{
		statusDDfromSelValue = defDateFilterDateFrom ;
		statusDDToSelValue = defDateFilterDateTo ;

		int statidx = 0;

		statidx = arCSubType.indexOf(statusDDfromSelValue);

		if (statidx > -1)
		{
			statusDDfromSelValue = (String) cDesc.get(statidx);
		}

		statidx = arCSubType.indexOf(statusDDToSelValue);

		if (statidx > -1)
		{
			statusDDToSelValue = (String) cDesc.get(statidx);
		}



	}

	if (defDateFilterType.equals("Y"))
	{
		if (defDateFilterDateFrom.length() == 10)
		{
			selYear = defDateFilterDateFrom.substring(6);
		}
	}

	if (defDateFilterType.equals("M"))
	{
		if (defDateFilterDateFrom.length() == 10)
		{
			selMonthYear = defDateFilterDateFrom.substring(6);
			selMonth = defDateFilterDateFrom.substring(0,2);

			if (selMonth.startsWith("0"))
			{
				selMonth = selMonth.substring(1);
			}
		}
	}







%>
	<HR>


<table class="tableDefault" width="60%" border="0">
	<tr>
		<td colspan = 2><b><%=MC.M_DtFtr_DispDtEtr%><%--Date Filter - Display data entered for*****--%>:</b></td>

		<% if (defDateFilterType.equals("A"))
		{ %>
		<td >
			<i><%=LC.L_All%><%--All*****--%></i>
		</td>
		<%}%>


		<% if (defDateFilterType.equals("PS"))
		{ %>
		<td  >
			<%=MC.M_PatFrom_StatusDt%><%Object[] arguments = {statusDDfromSelValue,statusDDToSelValue}; %>
	    <%=VelosResourceBundle.getMessageString("M_PatStat_DateOfTo",arguments)%><%--<%=LC.Pat_Patient%> from status date of : </i><%=statusDDfromSelValue%> <i> to </i><%=statusDDToSelValue%>*****--%>
		</td>
		<%}%>


		<% if (defDateFilterType.equals("Y"))
		{ %>
		<td  >
			<i> <%=LC.L_Year%><%--Year*****--%> </i> : <%= selYear%>
		</td>
		<%}%>

		<% if (defDateFilterType.equals("M"))
		{ %>
		<td  2>
			<i> <%=LC.L_Month%><%--Month*****--%> </i> : <%= selMonth%>&nbsp; <%=selMonthYear%>
		</td>
		<%}%>

		<% if (defDateFilterType.equals("DR"))
		{ %>
		<td  >
			 <i> <%=LC.L_DateRange_Frm%><%--Date Range: From*****--%> </i> <%=defDateFilterDateFrom%>
			 &nbsp;
					<% if( !defDateFilterType.equals("DR")) { %>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<% } %>

			 <i><%=LC.L_To%><%--To*****--%> </i>
			<% if( defDateFilterType.equals("DR")) { %> <%=defDateFilterDateTo %>  <% } %>
		</td>
		<%}%>

	</tr>

</table>
				<HR>

<table class="tableDefault" width="100%" border="1">
<tr>
<th width="10%"><%=LC.L_Exclude%><%--Exclude*****--%></th>
<th width="15%"><%=LC.L_Start_Bracket%><%--Start Bracket*****--%></th>
<th width="15%"><%=LC.L_Frm_Name%><%--Form Name*****--%></th>
<th width="20%"><%=LC.L_Fld_Name%><%--Field Name*****--%></th>
<th width="10%"><%=LC.L_Criteria%><%--Criteria*****--%></th>
<th width="20%"><%=LC.L_Value%><%--Value*****--%></th>
<th width="15%"><%=LC.L_End_Bracket%><%--End Bracket*****--%></th>
<th width="10%"><%=LC.L_AndOrOr%><%--And/Or*****--%></th>
</tr>


<%

  totCount = fldNameSelList.size();

for (int i=0;i<(totCount);i++){
   selColName = (String) fldNameSelList.get(i) ;

   velSepPos = selColName.indexOf("[VELSEP]");

   if (velSepPos > -1)
   {
   	   formId = selColName.substring(0, velSepPos )	;

   	   formIndex   = formIds.indexOf(formId);

   	    if (formIndex > -1)
   	    {
   		   formName = (String) formNames.get(formIndex);
   		 }
   		 else
   		 {
   		 	formName = "";
   		 }

	}
   fldIndex = masterFieldColList.indexOf(selColName.trim());


		   if (fldIndex > -1)
		   {

			   selColName = (String) masterFieldNameList.get(fldIndex);

		   }
		   else
		   {
		   	selColName = "";
		   }

	if (! StringUtil.isEmpty(selColName ))
	{

  if ((i%2)==0)
  {%>
  <tr class="browserEvenRow">
   <%}else{ %>
   <tr class="browserOddRow">
   <%
   }
  	%>
     <td width="10%">
   <%if (excludeList.indexOf((new Integer(i)).toString())>-1){%>
   <p align="center"><input type="checkbox" name="exclude" value="<%=i%>"   checked></p>
   <%}else{%>
   <p align="center"><input type="checkbox" name="exclude" value="<%=i%>" ></p>
   <%}%>
     </td>

   <td width="15%">
   <%if (startBracList.indexOf((new Integer(i)).toString())>-1) { %>
	   <p align="center"> ( </p>
   <%
   	} else {
   %>
   	 &nbsp;
  <% } %>
   </td>

   <td width="20%">
	   	<%= formName %>
	</td>

   <td width="20%">
	   	<%= selColName %>
	</td>
      <td width="10%">
      <%
      	if ( scriteriaList.size()>0 && i<scriteriaList.size() )
      	{
      		scriteria = (String) scriteriaList.get(i);

      		if (! StringUtil.isEmpty(scriteria) )
      		{
      		   if (scriteria.equals("contains") )
      		   {
      		   		scriteriaText = LC.L_Contains/*"Contains"*****/;
      		   } else if (scriteria.equals("notcontains") )
      		   {
      		   		scriteriaText = LC.L_Does_NotContain/*"Does Not Contain"*****/;
      		   }else if (scriteria.equals("isnull") )
      		   {
      		   		scriteriaText = LC.L_Is_Null/*"Is Null"*****/;
      		   }else if (scriteria.equals("isnotnull") )
      		   {
      		   		scriteriaText = LC.L_Is_NotNull/*"Is Not Null"*****/;
      		   }
      		   else if (scriteria.equals("isnotequal") )
      		   {
      		   		scriteriaText = MC.M_Is_NotEqualTo/*"Is Not Equal To"*****/;
      		   }
      		   else if (scriteria.equals("isequalto") )
      		   {
      		   		scriteriaText = LC.L_Is_EqualTo/*"Is Equal To"*****/;
      		   }
      		   else if (scriteria.equals("start") )
      		   {
      		   		scriteriaText = LC.L_Begins_With/*"Begins with"*****/;
      		   }
      		   else if (scriteria.equals("ends") )
      		   {
      		   		scriteriaText = LC.L_Ends_With/*"Ends with"*****/;
      		   }
      		   else if (scriteria.equals("isgt") )
      		   {
      		   		scriteriaText = LC.L_Is_GreaterThan/*"Is Greater Than"*****/;
      		   }
      		   else if (scriteria.equals("islt") )
      		   {
      		   		scriteriaText = LC.L_Is_LessThan/*"Is Less Than"*****/;
      		   }
      		   else if (scriteria.equals("isgte") )
      		   {
      		   		scriteriaText = MC.M_Is_GtrThanEqual/*"Is Greater than Equal"*****/;
      		   }
      		    else if (scriteria.equals("islte") )
      		   {
      		   		scriteriaText = MC.M_Is_LessThanEqual/*"Is Less than Equal"*****/;
      		   }
      		    else if (scriteria.equals("first") )
      		   {
      		   		scriteriaText = LC.L_First_Value/*"First Value"*****/;
      		   }
      		    else if (scriteria.equals("latest") )
      		   {
      		   		scriteriaText = LC.L_Latest_Value/*"Latest Value"*****/;
      		   }
      		    else if (scriteria.equals("highest") )
      		   {
      		   		scriteriaText = LC.L_Highest_Val/*"Highest Value"*****/;
      		   }
      		    else if (scriteria.equals("lowest") )
      		   {
      		   		scriteriaText = LC.L_Lowest_Value/*"Lowest Value"*****/;
      		   }
      		   else
      		   {
      		   		scriteriaText  = "";
      		   }

      		// end of isEmpty
      		}
      	}

      %>
      <%= scriteriaText  %>
      </td>
	<td width="20%">

	<% if ( fldDataList.size() > 0 && i < fldDataList.size() ) {
		fldData = (String) fldDataList.get(i);
		fldData = (fldData==null)?"":fldData;
	} else
	{
		fldData = "";
	}
	 %>
		<%= fldData %>

	</td>
	<td width="15%">
	<%if (i < endBracList.size() && endBracList.indexOf((new Integer(i)).toString())>-1){%>
   <p align="center">)</p>
   <%}else{%>
   &nbsp;
   <%}%>
	</td>
	<td width="10%">
		<% if ( extendList.size() > 0 && i < extendList.size() ) {
		extendText = (String) extendList.get(i);
	} else
	{
		extendText = "";
	}
	if (StringUtil.isEmpty(extendText))
	{
		extendText = "";
	}
	 %>
		<%= extendText%>
</td>

 </tr>
<%
	}
}%>
</table>

<% } //for loop %>
<br>
  <input type="hidden" name="sess" value="keep">
</form>
<%
} // end for (attributes==null)
} else {  //else of if body for session

%>
  <jsp:include page="timeout.html" flush="true"/>

  <%
}


%>

</DIV>


</body>
</html>
