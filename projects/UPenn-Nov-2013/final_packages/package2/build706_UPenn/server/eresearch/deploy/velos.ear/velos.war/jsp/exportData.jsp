<!-- This file is called for exporting data for paginated browsers.
There was a problem in forwading the AJAX call to servlet to download file,we had to setup this JSP as proxy and then forward the request-->
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@ page import="com.velos.eres.service.util.*,java.text.DecimalFormat"%>
<%!
    String htmlEncodeXss(String str) {
        if (str == null) { return null; }
        return StringUtil.htmlEncode(str).replaceAll("\n", "").replaceAll("\t","")
        .replaceAll("\r","").replaceAll("\f","").replaceAll("\u0007","").replaceAll("\u001B","")
        .replaceAll("\u000e","").replaceAll("\\x0e","").replaceAll("\"","&quot;");
    }
%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<html>
<head>
	<title><%=LC.L_File_Download%><%--File Download*****--%></title>
</head>
<SCRIPT LANGUAGE="JavaScript" src="whichcss_skin.js"></SCRIPT>
<!-- <jsp:include page="skinChoser.jsp" flush="true"/> -->
<body>

<%
	HttpSession tSession = request.getSession(true);

	  if (sessionmaint.isValidSession(tSession))
	{
String sessId = tSession.getId();
if (sessId.length()>8) { sessId = sessId.substring(0,8); }
sessId = Security.encrypt(sessId);
char[] chs = sessId.toCharArray();
StringBuffer sb = new StringBuffer();
DecimalFormat df = new DecimalFormat("000");
for (int iX=0; iX<chs.length; iX++) {
    sb.append(df.format((int)chs[iX]));
}
String keySessId = sb.toString();

String url=request.getParameter("url");
url = htmlEncodeXss(url);

String requestURL = request.getRequestURL().toString();
String queryString = request.getQueryString();
	%>
	<form name="dummy" method="post" action="<%=url%>" target="_self">
		<input type ="hidden" name="d" />
        <input type ="hidden" name="key" value="<%=keySessId%>" />
        <input type ="hidden" name="requestURL" value="<%=requestURL%>" />
        <input type ="hidden" name="queryString" value="<%=queryString%>" />
	</form>
<script>
//setTimeout( "window.location.href ='<%=url%>'", 0 );	
	 
	document.dummy.submit();
	 
	
</script>
	
	<% 
		} %>
		
</body>
	</html>		