<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Std_TreatmentArm%><%--<%=LC.Std_Study%> Treatment Arm*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<%@ page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>


<SCRIPT Language="javascript">

 function  validate(formobj)
 {

 	if (   !(validate_col ('Name', formobj.trtname)  )  )  return false


     if (  !(validate_col('e-Signature',formobj.eSign)     )   ) return false

	if(isNaN(formobj.eSign.value) == true)
	{
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again.");*****/
		formobj.eSign.focus();
		 return false;
   	}




  }



</SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="stdTXArmJB" scope="request"  class="com.velos.eres.web.studyTXArm.StudyTXArmJB"/>


<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB"%>

<body>
<DIV class="popDefault" id="div1">
 <%

	HttpSession tSession = request.getSession(true);
   if (sessionmaint.isValidSession(tSession))
   {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
	<jsp:include page="include.jsp" flush="true"/>
<%

	int pageRight =EJBUtil.stringToNum(request.getParameter("pageRight"));
	if (pageRight >= 4)
	{

		String mode=request.getParameter("mode");
		if(mode == null)
		  mode = "";

		String studyId="";
		String studyTXId="";
		String name="";
		String desc="";
		String drugInfo="";


		String accountId=(String)tSession.getAttribute("accountId");
		int tempAccountId=EJBUtil.stringToNum(accountId);
		studyId=request.getParameter("studyId");


		if (mode.equals("M"))
		{

				studyTXId = request.getParameter("stdTXArmId");
				stdTXArmJB.setStudyTXArmId(Integer.parseInt(studyTXId) );
				stdTXArmJB.getStudyTXArmDetails();
				name = stdTXArmJB.getTXName();
				desc = stdTXArmJB.getTXDesc();
				drugInfo = stdTXArmJB.getTXDrugInfo();


		}

		if(desc == null) {desc = "";}
		if(drugInfo == null) {drugInfo = "";}


%>


    <Form name="trtarm" id="trtarmfrm" method="post" action="studytrtarmsubmit.jsp" onsubmit="if (validate(document.trtarm)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

	<table width="99%" cellspacing="0" cellpadding="0" border="0">
  	<tr>
		<td ><P class="sectionHeadings"> <%=LC.L_Treatment_Arm%><%--Treatment Arm*****--%> </P> </td>
	 </tr>
	 </table>

	<Input type="hidden" name="mode" value=<%=mode%> >
	<Input type="hidden" name="studyId" value=<%=studyId%> >
	<Input type="hidden" name="stdTXArmId" value=<%=studyTXId%> >

	<table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl" >
		<tr>
      <td width="20%"><%=LC.L_Name%><%--Name*****--%><FONT class="Mandatory">* </FONT></td>
        <td width="60%"><input type="text" name="trtname" value="<%=name%>" maxlength=250 ></td>
	  </tr>
	  <tr>
      <td width="20%"><%=LC.L_Description%><%--Description*****--%></td>
        <td width="60%">
		<textarea name="trtdesc" MAXLENGTH=4000><%=desc%></textarea></td>
	  </tr>
	  <tr>
      <td width="20%"><%=LC.L_Drug_Info%><%--Drug information*****--%></td>
        <td width="60%"><input type="text" name="trtinfo" value="<%=drugInfo%>" maxlength=250></td>
	  </tr>
	</table>
	<br>
	<%if ((pageRight<6) && (mode.equals("M"))){ %>
	<table width="50%">

	</table>
	<%} else {%>


	<jsp:include page="submitBar.jsp" flush="true">
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="trtarmfrm"/>
			<jsp:param name="showDiscard" value="N"/>
	</jsp:include>


	<%} %>
     <br>

 	 <table width="100%" cellspacing="0" cellpadding="0">
      <td align=right>

      </td>
      </tr>
  </table>

  </Form>

<%

	} //end of if body for page right
	else
	{
%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
<%

	} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
<%

}

%>

 <div class = "myHomebottomPanel">
 <jsp:include page="bottompanel.jsp" flush="true"/>
 </div>
</body>

</html>
















