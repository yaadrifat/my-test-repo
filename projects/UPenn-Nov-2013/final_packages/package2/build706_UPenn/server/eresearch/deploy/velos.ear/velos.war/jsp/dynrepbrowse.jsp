<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
 


<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


<script>

	function setOrder(formObj,orderBy,pgRight) //orderBy column number
{
	var lorderType;
	if (formObj.orderType.value=="asc") {
		formObj.orderType.value= "desc";		
		lorderType="desc";
	} else 	if (formObj.orderType.value=="desc") {
		formObj.orderType.value= "asc";
		lorderType="asc";	
	}

	lsrc = formObj.srcmenu.value;
	lcurPage = formObj.page.value;
	lselectedtab= formObj.selectedTab.value;
	formObj.action="allPatient.jsp?srcmenu="+lsrc+"&page="+lcurPage+"&orderBy="+orderBy+"&orderType="+lorderType+"&selectedTab="+lselectedtab;
	formObj.submit(); 
}
function deleteRep(formobj,repId,repName){
//formobj.mode.value="D";

formobj.repId.value=repId;

repName=decodeString(repName);
formobj.repName.value=repName;
var paramArray = [repName];
if (confirm(getLocalizedMessageString("M_SureWantDel_Rpt",paramArray)))/*if (confirm("Are you sure that you want to delete report '"+repName+" '"))*****/{
formobj.action="dynrepdelete.jsp?repId="+repId;

formobj.submit();
}
else{
return false;
}

}

//JM:07July2006
function copyReportTemplate(formobj,repId,repName){
formobj.repId.value=repId;
repName=decodeString(repName);
formobj.repName.value=repName;
formobj.action="dynrepcopy.jsp?repId="+repId;
formobj.submit();
}

function OpenFilter(repId,repName,formId){
repName=encodeString(repName);
 windowname=window.open("dynfilterbrowse.jsp?repId="+repId+"&repName="+repName+"&formId="+formId,'','resizable=1,status=0, width=850,height=550 top=100,left=100,scrollbars=1');
 windowname.focus();

}
function setFilterText(form){
 	  form.ptxt.value=form.patCode.value;
 	  form.atxt.value=form.patage[form.patage.selectedIndex].text ;
 	  if (form.patgender[form.patgender.selectedIndex].value != "") 
 	       form.gtxt.value=form.patgender[form.patgender.selectedIndex].text ;
 	  if (form.patstatus[form.patstatus.selectedIndex].value != "") 
 	   form.rtxt.value=form.patstatus[form.patstatus.selectedIndex].text ;
 	  form.rbytxt.value=form.regBy.value ;
 	  
 }
 function openDisplay(repId,type){
 /*Changes for: INF-22402 Date:10/04/2012 By:Yogendra Pratap Singh*/
 var calledfrom = document.getElementById('calledfrom').value
 if(calledfrom == "null" ||calledfrom == null ){
	 calledfrom = "<%=LC.L_Adhoc_Queries%>";
 }
 if (type=="S")
 windowname=window.open("dynpreview.jsp?repId="+repId+"&from=browse&calledfrom="+calledfrom,'','resizable=1,status=0, width=850,height=550 top=100,left=100,scrollbars=1');
 else if (type=="M")
 windowname=window.open("mdynpreview.jsp?repId="+repId+"&from=browse&calledfrom="+calledfrom,'','resizable=1,status=0, width=850,height=550 top=100,left=100,scrollbars=1');
 
 windowname.focus();
 }
 
 function openDisplayLinear(repId,type,fmt){
 /*Changes for: INF-22402 Date:10/04/2012 By:Yogendra Pratap Singh*/
 var calledfrom = document.getElementById('calledfrom').value
 if(calledfrom == "null" ||calledfrom == null ){
	 calledfrom = "<%=LC.L_Adhoc_Queries%>";
 }
 if (type=="S")
 {
 	windowname=window.open("dynpreview.jsp?repId="+repId+"&from=browse&calledfrom="+calledfrom,'','resizable=1,status=0, width=850,height=550 top=100,left=100,scrollbars=1');
 	windowname.focus();
 }
 else if (type=="M")
 {
 
 	var showmenu = "no";
 	
 	if (fmt=='E' || fmt=='W')
 	{
 		showmenu = "yes";
 		
 	}
 	
 	windowname=window.open("mdynexport.jsp?repId="+repId+"&from=browse&exp="+fmt+"&calledfrom="+calledfrom,'','toolbar='+showmenu+',menubar='+showmenu+',resizable=1,status=0, width=850,height=550 top=100,left=100,scrollbars=1');
 	windowname.focus();
 }
  
 }
 
 
 function openDisplayB(repId){
 windowname=window.open("dynpreviewB.jsp?repId="+repId+"&from=browse",'Before','resizable=1,status=0, width=850,height=550 top=100,left=100,scrollbars=1');
 
 windowname.focus();
 }
 	</script>
 		
 
<title><%=MC.M_AdhocQry_SvdRptTplt%><%--Ad-Hoc Query >> Saved Report Templates*****--%></title>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.DateUtil,com.velos.eres.business.common.*" %>
<%@ page language = "java" import="com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.person.*,com.velos.eres.service.util.*,com.velos.eres.web.user.*,com.velos.eres.web.grpRights.GrpRightsJB"%>

<jsp:useBean id="codelst" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id="userB" scope="request" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id ="userSiteB" scope="request" class="com.velos.eres.web.userSite.UserSiteJB"/><!--km-->
<jsp:useBean id="dynrep" scope="request" class="com.velos.eres.web.dynrep.DynRepJB"/>
<jsp:useBean id="dynrepdt" scope="request" class="com.velos.eres.web.dynrepdt.DynRepDtJB"/>
<jsp:useBean id="dynrepfltr" scope="request" class="com.velos.eres.web.dynrepfltr.DynRepFltrJB"/>
<jsp:useBean id ="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<% String src;
src= request.getParameter("srcmenu");
String panelDisplay= request.getParameter("panel");
panelDisplay=(panelDisplay==null)?"":panelDisplay;
if (!(panelDisplay.equals("N"))){
%>
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   
<%}
int ienet = 2;

String agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1) 
     ienet = 0; //IE
    else
	ienet = 1;
	if(ienet == 0) {	
%>
<body style="overflow:scroll;">
<%
	} else {
%>
<body>
<%
	}
%>

<%if (!(panelDisplay.equals("N"))) { %>
<DIV class="BrowserTopN" id="div2">
<%}else{%>
<DIV> 
<%}
  HttpSession tSession = request.getSession(true);
  if (sessionmaint.isValidSession(tSession)) {
  if (tSession.getAttribute("attributes")!=null) tSession.removeAttribute("attributes");
  String groupId="",groupNamePullDown="",abnresult="",abnrFlag="";
  String filEnrDt="",labText="",labStr="",groupStr="",abnText="",dateStr="",whereStr="",abnStr="";
  String[] deleteIds;
  userB = (com.velos.eres.web.user.UserJB) tSession.getValue("currentUser");
  int pageRight = 0;
  GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");		
  
  String calledfrom = request.getParameter("calledfrom");
  pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("ADHOC"));  
  	  
//check the module rights for this user
	String modRight = (String) tSession.getValue("modRight");
	int modlen = modRight.length();
	CtrlDao acmod = new CtrlDao();
	acmod.getControlValues("module");
	ArrayList acmodfeature =  acmod.getCValue();
	ArrayList acmodftrSeq = acmod.getCSeq();
	int adhocSeq = acmodfeature.indexOf("ADHOCQ");
	adhocSeq = ((Integer) acmodftrSeq.get(adhocSeq)).intValue();
	char adhocAppRight = modRight.charAt(adhocSeq - 1);
	
    
 if (String.valueOf(adhocAppRight).compareTo("1") == 0 && pageRight >= 4) {
	Calendar cal = new GregorianCalendar();
	int pstudy = 0;
     String searchFilter ="" ;
     String selectedTab = "" ;
     DynRepDao dynDao=new DynRepDao();
     DynRepDao dynDaoFltr=new DynRepDao();
     ArrayList repDeleteIds=new ArrayList();
     ArrayList repFltrIds=new ArrayList();
     
    selectedTab=request.getParameter("selectedTab");
    String uName = (String) tSession.getValue("userName");	
    UserJB user = (UserJB) tSession.getValue("currentUser");
    String userId = (String) tSession.getValue("userId");
    String accId=(String) tSession.getValue("accountId");
    String repId=request.getParameter("repId");
    repId=(repId==null)?"0":repId;
    
    String repName=request.getParameter("repName");
    repName=(repName==null)?"":repName;
    String mode=request.getParameter("mode");
    mode=(mode==null)?"":mode;
    
    
    // study pull down
    int studyId=0;
	
	StringBuffer study=new StringBuffer();	
	String selStudy = "";
	StringBuffer sbRepType =new StringBuffer();	
	String searchDynType = "";
	
	String searchRepName = "";
	StringBuffer sbSearch = new StringBuffer();
	
	selStudy = request.getParameter("study");
	
	if (StringUtil.isEmpty(selStudy))
	{
		selStudy = "";
	}
	
	searchDynType = request.getParameter("searchDynType");
	
	if (StringUtil.isEmpty(searchDynType))
	{
		searchDynType= "";
	}
	
	searchRepName = request.getParameter("searchRepName");
	
	if (StringUtil.isEmpty(searchRepName))
	{
		searchRepName= "";
	}
		searchRepName = searchRepName.trim();
	
	searchRepName = StringUtil.decodeString(searchRepName);
	
	String encodedReportName = "";
	
	if (! StringUtil.isEmpty(searchRepName))
	{
		encodedReportName = StringUtil.encodeString(searchRepName);
	}
	
	
		
	StudyDao studyDao = studyB.getUserStudies(userId, "dummy",0,"");
	study.append("<SELECT NAME=study>") ;
	study.append("<OPTION value=''  selected>"+LC.L_Select_AnOption/*Select an Option*****/+"</OPTION>");
	for (int counter = 0; counter <= (studyDao.getStudyNumbers()).size() -1 ; counter++){
		studyId = ((Integer)((studyDao.getStudyIds()).get(counter))).intValue();
		//FIX #5226
		String studyNumberStr = "";
		studyNumberStr = ""+(studyDao.getStudyNumbers()).get(counter);
		studyNumberStr = (studyNumberStr.length() > 40)? 
				studyNumberStr.substring(0,37)+"...":studyNumberStr;
		
		if (selStudy.length()>0){
			if (studyId==((new Integer(selStudy)).intValue()))
				study.append("<OPTION value = "+ studyId +" selected>" + studyNumberStr + "</OPTION>");
			else
				study.append("<OPTION value = "+ studyId +">" + studyNumberStr + "</OPTION>");
		} else{
			study.append("<OPTION value = "+ studyId +">" + studyNumberStr + "</OPTION>");
		}
	}
	study.append("</SELECT>");
		
    //report Type pull down
    	
    sbRepType.append("<select size='1' name='searchDynType'>");
	sbRepType.append("<option value='S'");
		 if (searchDynType.equals("S")){
		 	 sbRepType.append(" selected ");
		 	 }
	sbRepType.append(">"+LC.L_Study/*LC.Std_Study*****/+"</option>");
			
	sbRepType.append("<option value='P'");		
	
			if (searchDynType.equals("P")){
		 	 sbRepType.append(" selected ");
		 	 }
	sbRepType.append(">"+LC.L_Patient/*LC.Pat_Patient*****/+"</option>");	 	 
	sbRepType.append("<option value='A'");			 	 
	
	if (searchDynType.equals("A")){
		 	 sbRepType.append(" selected ");
		 	 }
	sbRepType.append(">"+LC.L_Account/*Account*****/+"</option>");	 	 	 	 	 	 
	
	sbRepType.append("<option value=''");			 	 
	if (searchDynType.equals("")){
		 	 sbRepType.append(" selected ");
		 	 }
	
	sbRepType.append(">"+LC.L_Select_AnOption/*Select an Option*****/+"</option></select>");	 	 	 	 
	
	//prepare search based where clause
    	
    	if (! StringUtil.isEmpty(searchDynType))
    	{
    		sbSearch.append(" and report_type = '" +searchDynType+"' ");
    	}		
    
    	if (! StringUtil.isEmpty(selStudy))
    	{
    		sbSearch.append(" and fk_study = " +selStudy );
    	}		
    	
    	if (! StringUtil.isEmpty(searchRepName))
    	{
    		sbSearch.append(" and lower(dynrep_name) like '%" + searchRepName.toLowerCase() + "%'");
    	}		
    	
    //	
    
    
 		//commented by Sonia Abrol,get sites in SQl instead of java
  		//km--to get report templates which have access other than the Primary organization
  		/*userB.setUserId(EJBUtil.stringToNum(userId));
		userB.getUserDetails();
		int userPrimOrg = EJBUtil.stringToNum(userB.getUserSiteId());
     				
		ArrayList userSiteIds = new ArrayList();
		ArrayList userSiteSiteIds = new ArrayList();	
     		ArrayList userSiteRights = new ArrayList();
		
		UserSiteDao userSiteDao = userSiteB.getUserSiteTree(EJBUtil.stringToNum(accId),EJBUtil.stringToNum(userId));
		userSiteIds = userSiteDao.getUserSiteIds();
		userSiteSiteIds = userSiteDao.getUserSiteSiteIds();
		userSiteRights = userSiteDao.getUserSiteRights();
		int len1 = userSiteIds.size();
			
			
			String siteIdAcc="";
			int accrights=0;
			int userSiteSiteId=0;		
			int userSiteRight=0;
			for (int counter=0;counter<len1;counter++) {
	 		userSiteSiteId = EJBUtil.stringToNum(((userSiteSiteIds.get(counter)) == null)?"-":(userSiteSiteIds.get(counter)).toString());
     			userSiteRight = EJBUtil.stringToNum(((userSiteRights.get(counter)) == null)?"-":(userSiteRights.get(counter)).toString());
			
			if (userPrimOrg!=userSiteSiteId)
			   accrights=userSiteRight;
			if(accrights!=0)
			siteIdAcc=siteIdAcc+(userSiteSiteId+",");
					
			}
		        siteIdAcc=siteIdAcc+userPrimOrg; */ 
		
 //changed by Sonika to add sharewith  
  //Query modified by Manimaran to get Report templates other than the primary organization
  //Sonia : modified the query to include site check in SQL 
			 String sql = "select pk_dynrep,fk_form,dynrep_name,created_on,(select usr_firstname ||' '|| usr_lastname from er_user where pk_user=a.creator ) creator,report_type,dynrep_sharewith,dynrep_type,(select study_number from er_study where pk_study = fk_study and fk_study is not null) study_number " 
			+ " from er_dynrep a where fk_account=" +accId + sbSearch.toString() + " and  pkg_user.f_getUserAdHocRight("+ userId + " , pk_dynrep) > 0 "; 
			
		 
			
			String countsql = "select count (*) " 
			+ " from er_dynrep a where fk_account=" +accId + sbSearch.toString() + " and  pkg_user.f_getUserAdHocRight("+ userId + " , pk_dynrep) > 0 "; 
			
			/*exists (select b.fk_object from er_objectshare b " 
			+ " where b.object_number=2 and a.pk_dynrep = b.fk_object and ( ( b.objectshare_type = 'U' and (b.fk_objectshare_id="+userId+" ) " 
			+ " OR ( b.fk_objectshare_id in (select pk_site from erv_usersites where fk_user = " +  userId +" and usersite_right > 0 union select fk_siteid from er_user where pk_user = " +  userId +") AND b.objectshare_type = 'O') ))) "; */  
			

		
		//	System.out.println(sql);
			//System.out.println(countsql);
			
			String pagenum = "";
			int curPage = 0;
			long startPage = 1;
			long cntr = 0;
			String pagenumView = "";
			int curPageView = 0;
			long startPageView = 1;
			long cntrView = 0;

				
			pagenum = request.getParameter("pagenum");
			if (pagenum == null)
			{
			pagenum = "1";
			}
			curPage = EJBUtil.stringToNum(pagenum);
			
			String orderBy = "";
			orderBy = request.getParameter("orderBy");
			if (orderBy==null) orderBy="1";
			String orderType = "";
			orderType = request.getParameter("orderType");

			if (orderType == null)
			{
			orderType = "desc";
			}


			pagenumView = request.getParameter("pageView");
			if (pagenumView == null)
			{
			pagenumView = "1";
			}
			curPageView = EJBUtil.stringToNum(pagenumView);

			String orderByView = "";
			orderByView = request.getParameter("orderByView");

			String orderTypeView = "";
			orderTypeView = request.getParameter("orderTypeView");

			if (orderTypeView == null)
			{
			orderTypeView = "asc";
			}

			long rowsPerPage=0;
			long totalPages=0;	
			long rowsReturned = 0;
			long showPages = 0;
			long totalRows = 0;	   
			long firstRec = 0;
			long lastRec = 0;	   
			boolean hasMore = false;
			boolean hasPrevious = false;

			Configuration conf = new Configuration();
			rowsPerPage = conf.getRowsPerBrowserPage(conf.ERES_HOME +"eresearch.xml");
			totalPages = conf.getTotalPagesPerBrowser(conf.ERES_HOME +"eresearch.xml");

				
			BrowserRows br = new BrowserRows();
			br.getPageRows(curPage,rowsPerPage,sql,totalPages,countsql,orderBy,orderType);
			rowsReturned = br.getRowReturned();
			showPages = br.getShowPages();
        
		     startPage = br.getStartPage();
		     
			hasMore = br.getHasMore();
			 
			hasPrevious = br.getHasPrevious();
			 
			
			totalRows = br.getTotalRows();	   	   
			
            
			firstRec = br.getFirstRec();
			
			lastRec = br.getLastRec();	  	   
			
	   
%>


	
   <Form name="repbrowse" method="post" action="dynrepbrowse.jsp?pagenum=1">
  	       	
<!--<Form  name="search" method="post" action="allPatient.jsp">-->
<input type="hidden" name="srcmenu" Value="<%=src%>">
<input type="hidden" name="mode" Value="">
<input type="hidden" name="repId" Value="">
<input type="hidden" name="repName" Value="">

<input type="hidden" name="totcount" Value="<%=rowsReturned%>">
<input type="hidden" name="calledfrom" id="calledfrom" value="<%=calledfrom%>"/>


<table width="99%" class="outline midalign" border="0" cellspacing="0" cellpadding="0">
	<tr height="20" >
	<td colspan="5" class="lhsFont"> <b> <%=LC.L_Search_By%><%--Search By*****--%></b></td>
	</tr>
	<tr class="searchBg">
	 <td  class="lhsFont"><%=LC.L_Rpt_Name%><%--Report Name*****--%>: <input type="text" name="searchRepName" Value="<%=searchRepName%>" size="20"></td>
	 <td ><%=LC.L_Rpt_Type%><%--Report Type*****--%>: <%= sbRepType %></td> 
	<td ><%=LC.L_Study%><%--<%=LC.Std_Study%>*****--%>: <%= study	%></td>
	<td ><P><button type="submit" title="<%=MC.M_SrchBy_RptNameOrType%><%--Search using partial or complete Report Name, Report Type and/or <%=LC.Std_Study%>*****--%>" ><%=LC.L_Search%></button></P></td>
	<!-- <td colspan="4">-->
<td align="right" > <b><font size="1"><!--<A href="dynreptype.jsp?mode=N&srcmenu=<%=src%>&dynType=P">Create Simple Report</A>-->
<% if ( pageRight == 5 || pageRight == 7) {%>	
 <A href="mdynreptype.jsp?mode=N&srcmenu=<%=src%>&dynType=P"><%=LC.L_Create_Rpt_Upper%><%--CREATE REPORT*****--%></A> </font></b>
 <% }%>	&nbsp;	 
	</td></tr>
 </table>

</DIV>
	
<DIV class="BrowserBotN BrowserBotN_AdQ_1" id="div3">	
<table class="outline"  width="100%" border="0" cellspacing="0" cellpadding="0" >

    <tr> 
	<th width="15%" ><%=LC.L_Display%><%--Display*****--%></th>				
	<th width="25%"><%=LC.L_Rpt_TemplateName%><%--Report Template Name*****--%></th>
	<th  width="3%"><%=LC.L_Edit%><%-- Edit --%></th>  		<%--YPS:Modified for-UI Review Quick-fixes--%>				
	<!--<th class="tdDefault" width="10%">Created on</th>		 	-->
	<th  width="10%"><%=LC.L_Created_By%><%--Created By*****--%></th>	
	<th  width="10%"><%=LC.L_Shared_with%><%--Shared with*****--%></th>		
	<th  width="5%"><%=LC.L_Rpt_Type%><%--Report Type*****--%></th>		<%--Modified for-UI Review Quick-fixes--%>
	<th  width="10%"><%=LC.L_Study%><%--<%=LC.Std_Study%>*****--%></th>		
    <th  width="5%"><%=LC.L_Copy%><%--Copy*****--%></th>	<!--JM:07July2006-->				
	<th  width="5%"><%=LC.L_Filters%><%--Filters*****--%></th>	
	<th  width="5%"><%=LC.L_Delete%><%--Delete*****--%></th>	
   </tr>
<!--</Form>-->

<input type="hidden" name="selectedTab" Value=<%=selectedTab%>> 	  
<Input type="hidden" name="orderBy" value="<%=orderBy%>">
<Input type="hidden" name="pagenum" value="<%=curPage%>">	
<Input type="hidden" name="orderType" value="<%=orderType%>">	
		
<%
//Retrieve study count for this user 


	ArrayList studyList=new ArrayList();



	if (totalRows <= 0 ) {

%>

	<tr><td>&nbsp;</td></tr>
	<tr><td colspan="9" align="center"><P class="defComments"><b><%=MC.M_NoMatching_AhocQryFound%><%--No Matching Ad-Hoc Queries Found*****--%> </b></P></td></tr></table>

<%

	} else {

	   int i = 0;


	   if(rowsReturned == 0) {

%>

	<tr><td>&nbsp;</td></tr>
	<tr><td colspan="9" align="center"><P class="defComments"><b><%=MC.M_NoMatching_AhocQryFound%><%--No Matching Ad-Hoc Queries Found*****--%> </b></P></td></tr></table>

<%

	} else {


	
	String patientId = "";
	String totalEnroll="" ; 
	%>
<!--<table class="tableDefault" width="100%" cellspacing="1" cellpadding="0">-->
<%
        
        String creator="" ,createOn="",templateName="",repPk="",repType="",formId="",shareWith,dynRepType="";
        String studyNumber = "";
        String displayRepType = "";
            
		for(i = 1 ; i <=rowsReturned ; i++)
	  	{   
	  	   templateName = br.getBValues(i,"dynrep_name");
		   templateName=(templateName==null)?"-":templateName;
	  	   creator = br.getBValues(i,"creator");
	  	   creator=(creator==null)?"-":creator;
		   createOn=  br.getBValues(i,"created_on");
		   if (createOn==null) createOn="-";
		   repPk= br.getBValues(i,"pk_dynrep") ;
		   repType=  br.getBValues(i,"report_type") ;
		   	   
		   dynRepType=br.getBValues(i,"dynrep_type");

		   studyNumber = br.getBValues(i,"study_number");
		   
		   dynRepType=(dynRepType==null)?"":dynRepType;
		   if (dynRepType.length()==0) dynRepType="S";
		   
		   shareWith=br.getBValues(i,"dynrep_sharewith");
		   shareWith=(shareWith==null)?"":shareWith;
		   if (shareWith.equals("P")) shareWith=LC.L_Private/*Private*****/;
		   if (shareWith.equals("A")) shareWith=LC.L_All_AccUsers/*All Account Users*****/;
		   if (shareWith.equals("S")) shareWith=MC.M_UsrsStd_Team/*All Users in a Study Team****/;
		   if (shareWith.equals("O")) shareWith=MC.M_AllUsr_InOrg/*All Users in an Organization*****/;
		   if (shareWith.equals("G")) shareWith=MC.M_AllUsr_InGrp/*All Users In a Group*****/;
		   
		   if (StringUtil.isEmpty(repType))
		   {
		   		repType = "";
		   }
		   
		   if (StringUtil.isEmpty(studyNumber))
		   {
		   		studyNumber = "";
		   }
		   
		   
		   
		   if (repType.equals("P")) displayRepType=LC.L_Patient/*LC.Pat_Patient*****/;
		   if (repType.equals("A")) displayRepType=LC.L_Account/*Account*****/;
		   if (repType.equals("S")) displayRepType=LC.L_Study/*LC.Std_Study*****/;
		   
		   formId=br.getBValues(i,"fk_form") ;
	 		   if ((i%2)==0) {

%>

      <tr class="browserEvenRow"> 

<%

	 		   }else{

%>

      <tr class="browserOddRow"> 

<% 

	 		   } 
%>
        <td align="center" width="15%"><A title="<%=LC.L_Display_Report%><%--Display Report*****--%>" href="#" onClick="openDisplay(<%=repPk%>,'<%=dynRepType%>');"><img src="../images/jpg/preview.gif" align="absmiddle" border="0"></img></A>
          <A title="<%=MC.M_DispRpt_LinearFmt%><%--Display Report in Linear Format*****--%>" href="#" onClick="openDisplayLinear(<%=repPk%>,'<%=dynRepType%>','L');"><img src="../images/jpg/previewlinear.gif" align="absmiddle" border="0"></img></A>
          <A title="<%=MC.M_Expt_WordFmt%><%--Export to Word Format*****--%>" href="#" onClick="openDisplayLinear(<%=repPk%>,'<%=dynRepType%>','W');"><img src="../images/jpg/word.GIF" align="absmiddle" border="0"></img></A>
		<A title="<%=MC.M_Expt_ExcelFmt%><%--Export to Excel Format*****--%>" href="#" onClick="openDisplayLinear(<%=repPk%>,'<%=dynRepType%>','E');"><img src="../images/jpg/excel.GIF" align="absmiddle" border="0"></img></A> 
        <!--<A href="#" onClick="openDisplayB(<%=repPk%>);">B</A>-->
        </td>
<%
if (dynRepType.equals("S")){
	%>
	<%--YPS:Modified for-UI Review Quick-fixes in   Data Extraction >> Ad hoc queries  --%>
	<td width="25%" align="center"><%=templateName%></td><td><A href="dynreptype.jsp?mode=M&repId=<%=repPk%>&selectedTab=2&srcmenu=<%=src%>&dynType=<%=repType%>"><img src="../jsp/images/edit.gif" style="border-style: none;" title="<%=LC.L_Edit%>"/><%--Edit*****--%></A>
	<%} else {%>
	<td width="25%"  align="center"><%=templateName%></td><td><A href="mdynreptype.jsp?mode=M&repId=<%=repPk%>&selectedTab=1&srcmenu=<%=src%>&dynType=<%=repType%>"><img src="../jsp/images/edit.gif" style="border-style: none;" title="<%=LC.L_Edit%>"/><%--Edit*****--%></A>
	<%}%>
	
	</td>
        <!--<td width="10%"><%=createOn%></td>-->
        <td width="10%"><%=creator%></td>
        <td width="10%"><%=shareWith%></td>	
        <td width="5%"><%=displayRepType%></td>	<%--YPS:Modified for-UI Review Quick-fixes--%>
        <td width="10%"><%=studyNumber%></td>	
	
	<!--JM: 07July2006  -->
	<td width="5%"  align="center">
	  <% if ( pageRight >= 6 ) {%>	
	    <A href="#" OnClick="copyReportTemplate(document.repbrowse,<%=repPk%>,'<%=StringUtil.encodeString(templateName)%>')"><img src="../images/copy.png" title="<%=LC.L_Copy%>" border="0" /></A></td> <%--Fixed Bug#7966:Akshi*****--%> 
	<%} %>  
	
	<!--JM: 07July2006 -->	
		
	<td width="5%" align="center">
	  <% if ( pageRight >= 6 ) {%>	
	    <A href="#" OnClick="OpenFilter(<%=repPk%>,'<%=templateName%>',<%=formId%>)"><img border="0" src="./images/View.gif" title="<%=LC.L_View%><%--View*****--%>"/></A></td>
	<td width="5%"  align="center">
		&nbsp;&nbsp;
		<A href="#" onClick="deleteRep(document.repbrowse,<%=repPk%>,'<%=StringUtil.encodeString(templateName)%>')"><img src="./images/delete.gif" border="0" title="<%=LC.L_Delete%>" /></A>
	  <% } %>	
	</td>	
          </tr>

	  <%


	  }// end for for loop

	  } // end of if for length == 0

  } //end of if for the check of patientResults == null

	  %>

    </table>
    
    <!-- Bug#9751 16-May-2012 Ankit -->
    <table width="98%" cellspacing="0" cellpadding="0" class="midalign" >
	<tr valign="top">
	<td>
	<% if (totalRows > 0) { Object[] arguments = {firstRec,lastRec,totalRows}; %>
		<font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%>: &nbsp;&nbsp;</font><%-- <font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s): &nbsp;&nbsp;</font>*****--%>
	<%}%>	
	</td>
	</tr>
	</table>
	
	<div align="center" class="midalign">	

	<% 

	if (curPage==1) startPage=1;

    for (int count = 1; count <= showPages;count++)
	{
   cntr = (startPage - 1) + count;
	 
	if ((count == 1) && (hasPrevious))
	{   
    %>
	
  	<A href="dynrepbrowse.jsp?pagenum=<%=cntr-1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&srcmenu=<%=src%>&study=<%=selStudy%>&searchDynType=<%=searchDynType%>&searchRepName=<%=encodedReportName%>">< <%=LC.L_Previous%><%--Previous*****--%> <%=totalPages%> ></A>&nbsp;&nbsp;&nbsp;&nbsp;
		
	<%
  	}	
	%>
	
	<%
 
	 if (curPage  == cntr)
	 {
     %>	   
		<FONT class = "pageNumber"><%= cntr %></Font>
       <%
       }
      else
        {
       %>		

	   <A href="dynrepbrowse.jsp?pagenum=<%=cntr%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&srcmenu=<%=src%>&study=<%=selStudy%>&searchDynType=<%=searchDynType%>&searchRepName=<%=encodedReportName%>"><%= cntr%></A>
       <%
    	}	
	 %>
	
	<%
	  }

	if (hasMore)
	{   
   %>
   
  	&nbsp;&nbsp;&nbsp;&nbsp;<A href="dynrepbrowse.jsp?pagenum=<%=cntr+1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&srcmenu=<%=src%>&study=<%=selStudy%>&searchDynType=<%=searchDynType%>&searchRepName=<%=encodedReportName%>">< <%=LC.L_Next%><%--Next*****--%> <%=totalPages%>></A>
	<%
  	}	
	%>
</div>
	
<!--	<table align=center class="tableDefault" height="75px">
	<tr>
<% 

	if (curPage==1) startPage=1;

    for (int count = 1; count <= showPages;count++)
	{
   cntr = (startPage - 1) + count;
	 
	if ((count == 1) && (hasPrevious))
	{   
    %>
	<td colspan = "2" >
  	<A href="dynrepbrowse.jsp?pagenum=<%=cntr-1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&srcmenu=<%=src%>&study=<%=selStudy%>&searchDynType=<%=searchDynType%>&searchRepName=<%=encodedReportName%>">< Previous <%=totalPages%> ></A>&nbsp;&nbsp;&nbsp;&nbsp;
	</td>	
	<%
  	}	
	%>
	<td>
	<%
 
	 if (curPage  == cntr)
	 {
     %>	   
		<FONT class = "pageNumber"><%= cntr %></Font>
       <%
       }
      else
        {
       %>		

	   <A href="dynrepbrowse.jsp?pagenum=<%=cntr%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&srcmenu=<%=src%>&study=<%=selStudy%>&searchDynType=<%=searchDynType%>&searchRepName=<%=encodedReportName%>"><%= cntr%></A>
       <%
    	}	
	 %>
	</td>
	<%
	  }

	if (hasMore)
	{   
   %>
   <td colspan = 3 align = center>
  	&nbsp;&nbsp;&nbsp;&nbsp;<A href="dynrepbrowse.jsp?pagenum=<%=cntr+1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&srcmenu=<%=src%>&study=<%=selStudy%>&searchDynType=<%=searchDynType%>&searchRepName=<%=encodedReportName%>">< Next <%=totalPages%>></A>
	</td>	
	<%
  	}	
	%>
   </tr>
   		
   		
  </table> -->
	
  </DIV>



  </Form>

 		

 <% } else {	//} //end of else body for page right
%>
<jsp:include page="accessdenied.jsp" flush="true"/>
<%}

}//end of if body for session

else{

%>

  <jsp:include page="timeout.html" flush="true"/> 

  <%

}



%>

  <div> 

    <jsp:include page="bottompanel.jsp" flush="true"/>

  </div>

<% if (!(panelDisplay.equals("N"))){ %>
<div class ="mainMenu" id = "emenu">

 <jsp:include page="getmenu.jsp" flush="true"/>

</div>
<%} %>

</body>

</html>


