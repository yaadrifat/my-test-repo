<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@ page language = "java" import = "com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.*,com.velos.eres.business.common.*,java.util.*"%>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<jsp:useBean id="sessionmaint" scope="session"	class="com.velos.eres.service.util.SessionMaint" />
<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
	
<% 
HttpSession tSession = request.getSession(true); 
 if (sessionmaint.isValidSession(tSession))	
 { 	
	
String studyId =request.getParameter("studyId");
String tabsubtype =request.getParameter("tabsubtype");
String appSubmissionType =request.getParameter("appSubmissionType");

String userId = (String) tSession.getValue("userId");

String submissionPK = StringUtil.trueValue(request.getParameter("submissionPK"));
String submissionBoardPK = StringUtil.trueValue(request.getParameter("submissionBoardPK"));

String irbFormHeight = StringUtil.htmlEncodeXss(request.getParameter("irbFormHeight"));

if (irbFormHeight==null || StringUtil.isEmpty(irbFormHeight))
{
	irbFormHeight="570";
}

//create a Hashtable for parameters needed for Action, put it in session. That way we dont have to pass extra parameters
// to all forms pages

Hashtable htIRBParams = new Hashtable();
htIRBParams.put("tabsubtype" ,tabsubtype );
htIRBParams.put("selectedTab" ,tabsubtype );
htIRBParams.put("submissionPK" ,submissionPK );
htIRBParams.put("submissionBoardPK" ,submissionBoardPK );


 	
TeamDao teamDao = new TeamDao();
teamDao.getTeamRights(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(userId));
ArrayList tId = new ArrayList();

tId = teamDao.getTeamIds();

	if (tId.size() <=0)
			{	
				StudyRightsJB stdRightstemp = new StudyRightsJB();
				tSession.putValue("studyRights",stdRightstemp);
				
				
			} else {
					
					stdRights.setId(EJBUtil.stringToNum(tId.get(0).toString()));
					   
					ArrayList teamRights ;
					teamRights  = new ArrayList();
					teamRights = teamDao.getTeamRights();
						 
					stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
					stdRights.loadStudyRights();
					
				 	
					tSession.putValue("studyRights",stdRights);
			}
////////////////////load access rights			
			
ArrayList arTabs = new ArrayList();
 
 arTabs.add("irb_new_tab");
 arTabs.add("irb_assigned_tab");
 arTabs.add("irb_compl_tab");
 arTabs.add("irb_post_tab");
 arTabs.add("irb_pend_tab");
 arTabs.add("irb_ongoing_menu");
 
 arTabs.add("irb_revfull_tab");
 
 arTabs.add("irb_revanc_tab");
 arTabs.add("irb_revexem_tab");
 arTabs.add("irb_revexp_tab");
 
 arTabs.add("irb_meeting");
  
 
 
//arFormTypes are linked with tab types

String[] arFormTypes = { "irb_check1","irb_check2","irb_check3","irb_check5","","irb_sub","irb_check4", "irb_check4","irb_check4","irb_check4","irb_meeting"};

    
String formCategory = "";
String formSubmissionType = "";

int idxForm = -1;

if (! StringUtil.isEmpty(tabsubtype))
{
 
	idxForm = arTabs.indexOf(tabsubtype);
	
	
	if (idxForm > -1)
	{
		formCategory = arFormTypes[idxForm];
	}
	
	formSubmissionType = EIRBDao.getIRBFormSubmissionTypeSQL(tabsubtype,formCategory,appSubmissionType);	 	
	 
}
 if ((! StringUtil.isEmpty(formCategory)) && (! StringUtil.isEmpty(formSubmissionType) ) )
 {
 	 tSession.setAttribute("IRBParams",htIRBParams);
 	 
%>	
<table width="100%" height="100%">
        <tr>
		<td valign="top" width="100%"> 
			
		<iframe name="irbf" src="formfilledstudybrowser.jsp?studyId=<%=studyId%>&calledFromForm=&showPanel=false&formCategory=<%=formCategory%>&submissionType=<%=formSubmissionType%>&irbReviewForm=true" 
				width="100%" height="100%" frameborder="1" scrolling="yes" allowautotransparency=true>
				</iframe>
				
		 </td> 				
		</tr>
</table>			 
 

<%  } else { %>
		<p class="sectionHeadings"><%=MC.M_NoFormsNeeded%><%--No forms needed*****--%></p>
	<% } 
			
}//end of if body for session 
else { %>
 <jsp:include page="timeout.html" flush="true"/> 
 <% } %>
