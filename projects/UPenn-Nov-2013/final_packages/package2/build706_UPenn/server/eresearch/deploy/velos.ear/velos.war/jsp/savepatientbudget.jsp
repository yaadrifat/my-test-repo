<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<HEAD>
</HEAD>
<BODY>
<jsp:useBean id="lineitemB" scope="request"	class="com.velos.esch.web.lineitem.LineitemJB" />
<jsp:useBean id="budgetcalB" scope="request"	class="com.velos.esch.web.budgetcal.BudgetcalJB" />
<jsp:useBean id="budgetB" scope="request"	class="com.velos.esch.web.budget.BudgetJB" />
<jsp:useBean id="bgtSectionB" scope="request"	class="com.velos.esch.web.bgtSection.BgtSectionJB" />
<jsp:useBean id="sessionmaint" scope="session"	class="com.velos.eres.service.util.SessionMaint" />

<Link Rel=STYLESHEET HREF="common.css" type=text/css>


<%@ page language="java"
	import="com.velos.eres.service.util.MC,com.velos.eres.business.common.*,java.text.*,java.util.*,com.velos.eres.service.util.EJBUtil, com.velos.eres.service.util.StringUtil,com.velos.esch.business.budgetcal.impl.*"%>
<%

	String src = null;
	src = request.getParameter("srcmenu");
	String eSign = request.getParameter("eSign");
	eSign=(eSign==null)?"0":eSign;
	String mode = request.getParameter("mode");
	String selectedTab = request.getParameter("selectedTab");
	String pageMode = "initial";

    HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession))

   {
%>
<jsp:include page="sessionlogging.jsp" flush="true" />
<%
//   	String oldESign = (String) tSession.getValue("eSign");
//	if(!oldESign.equals(eSign) && !(eSign.equals("0"))) {
%>

<%
//	} else {


	String ipAdd = (String) tSession.getValue("ipAdd");


	String usr = (String) tSession.getValue("userId");

	String budgetId = request.getParameter("budgetId");

	budgetB.setBudgetId(EJBUtil.stringToNum(budgetId));
	budgetB.getBudgetDetails();

	String budgetTemplate = budgetB.getBudgetType();
	
	String includedIn = request.getParameter("includedIn");
	if (StringUtil.isEmpty(includedIn))
	{
		includedIn="";
	}
	 
 // commented by Amarnadh for Bugzilla issue #3010

//	budgetB.setLastModifiedDate(sdateTime);
    int rete = budgetB.updateBudget();

    int bgtcalId = EJBUtil.stringToNum(request.getParameter("bgtcalId"));

	int rows = EJBUtil.stringToNum(request.getParameter("rows"));

	String sponsorOHead = request.getParameter("sponsorOHead");

	//Added by IA 11.03.2006
	String budgetSectionTypeRows[] = request.getParameterValues("bgtSectionTypeRow");
	String budgetSectionTypeRow;
	//End added
	String sponsorOHeadApply = request.getParameter("sponsorOHeadApply");

	String excludeSOCApply = request.getParameter("excludeSOCApply");

	if(excludeSOCApply == null)
	excludeSOCApply = "0";
	if(excludeSOCApply.equals("on"))
	excludeSOCApply = "1";
	else
	excludeSOCApply = "0";

	sponsorOHeadApply = (sponsorOHeadApply == null)?"N":"Y";

	String clinicOHead = request.getParameter("clinicOHead");

	String clinicOHeadApply = request.getParameter("clinicOHeadApply");

	clinicOHeadApply = (clinicOHeadApply == null)?"N":"Y";

	String fringeBenefit = request.getParameter("fringeBenefit");

	String fringeBenefitApply = request.getParameter("fringeBenefitApply");

	fringeBenefitApply = (fringeBenefitApply == null)?"0":"1";


	String costDiscount = request.getParameter("costDiscount");

	String costDiscountApply = request.getParameter("costDiscountApply");

	costDiscountApply = (costDiscountApply == null)?"0":"1";


	// Added by IA 9/18/2006 Store in the database calculations

	String budgetResearchCost= request.getParameter("nsocTotalResearchCost");

	String budgetResearchTotalCost = request.getParameter("nsocTotalTtlCost");

	String budgetSOCCost = request.getParameter("socTotalResearchCost");

	String budgetSOCTotalCost = request.getParameter("socTotalTtlCost");

	String budgetTotalCost = request.getParameter("totalResearchCost");

	String budgetTotalTotalCost = request.getParameter("totalTtlCost");

	String budgetSalaryCost = request.getParameter("totalSalResCost");

	String budgetSalaryTotalCost = request.getParameter("totalSalTtlCost");

	String budgetFringeCost = request.getParameter("totalFringeResCost");

	String budgetFringeTotalCost = request.getParameter("totalFringeTtlCost");

	String budgetDiscontinueCost = request.getParameter("totalDiscResCost");

	String budgetDiscontinueTotalCost = request.getParameter("totalDiscTtlCost");

	String budgetIndirectCost = request.getParameter("totalIndirectsResCost");

	String budgetIndirectTotalCost = request.getParameter("totalIndirectsTtlCost");

	//End added by IA 9/18/2006


	//Added by IA 11.03.2006

	String budgetResearchSponsorAmount = request.getParameter("nsocTotalSponsorAmount");

	String budgetResearchVariance = request.getParameter("nsocTotalVariance");

	String budgetSOCSponsorAmount = request.getParameter("socTotalSponsorAmount");

	String budgetSOCVariance = request.getParameter("socTotalVariance");

	String budgetTotalSponsorAmount = request.getParameter("TotalSponsorAmount");

	String budgetTotalVariance = request.getParameter("TotalVariance");


	//End added



	String lineitemId = "";
	String unitCost = "";
	String noUnit = "";
	String researchCost = "";
	String stdCareCost = "";
	String category = "";
	String lineItemTotalCost = "";


	//Added by IA 11.03.2006

	String lineItemSponsorAmount = "";

	String lineItemVariance = "";

	//end added





	String discount = "";
	String appIndResCost = "";

	ArrayList lineitemIdArrayList = new ArrayList();
	ArrayList discountArrayList = new ArrayList();
	ArrayList unitCostArrayList = new ArrayList();
	ArrayList noUnitArrayList = new ArrayList();
	ArrayList researchCostArrayList = new ArrayList();
	ArrayList stdCareCostArrayList = new ArrayList();
	ArrayList categoryArrayList = new ArrayList();
	ArrayList appIndirectsArrayList = new ArrayList();
	ArrayList lineItemTotalCostArrayList = new ArrayList();

	//Added by IA 11.03.2006

	ArrayList lineItemSponsorAmountArrayList = new ArrayList();
	ArrayList lineItemVarianceArrayList = new ArrayList();

	//end added

	int output = 0;
	int i = 0;
	int bgtSectionId = 0;
	String bgtSectionPatNo = "";
	String srtCostTotal = "";
	String srtCostGrandTotal = "";
	String socCostTotal = "";
	String socCostGrandTotal = "";
	String srtCostSponsor = "";
	String srtCostVariance = "";

	String socCostSponsor = "";
	String socCostVariance = "";


	String sectionWithPatNoString = "";

	int bgtSectionIdVisible = 0;

	if(rows == 1) {

		bgtSectionIdVisible = 	EJBUtil.stringToNum(request.getParameter("bgtSectionIdVisible"));

		bgtSectionPatNo = 	request.getParameter("patNo");
		budgetSectionTypeRow = request.getParameter("bgtSectionTypeRow");

		if(	budgetSectionTypeRow.equals("O")){

			srtCostTotal =  request.getParameter("nsocSubTotalResearchCost");
			srtCostGrandTotal = request.getParameter("nsocSubTotalTtlCost");
			socCostTotal =  request.getParameter("socSubTotalResearchCost");
			socCostGrandTotal = request.getParameter("socSubTotalTtlCost");

		}
		else
		{
			srtCostTotal =  request.getParameter("nsocSubTotalPatResearchCost");
			srtCostGrandTotal = request.getParameter("nsocSubTotalPatTtlCost");
			socCostTotal =  request.getParameter("socSubTotalPatResearchCost");
			socCostGrandTotal = request.getParameter("socSubTotalPatTtlCost");
		}

			srtCostSponsor = request.getParameter("nsocSubTotalSponsorAmountCost");
			srtCostVariance = request.getParameter("nsocSubTotalVariance");
			socCostSponsor = request.getParameter("socSubTotalSponsorAmountCost");
			socCostVariance = request.getParameter("socSubTotalVariance");



		lineitemId = request.getParameter("lineitemId");
		//discount = request.getParameter("discount");
		if (discount==null) discount = "0";
		unitCost = request.getParameter("unitCost");
		noUnit = request.getParameter("noUnits");
		category= request.getParameter("cmbCtgry");
		discount = request.getParameter("hidDiscountChkbox");
		researchCost = request.getParameter("researchCost");
		stdCareCost = request.getParameter("hidStdCareCost");
		appIndResCost = request.getParameter("hidAppIndirects");
		lineItemTotalCost = request.getParameter("TtlCost");

		//Added by IA 11.03.2006

			lineItemSponsorAmount = request.getParameter("sponsorAmount");
			if (lineItemSponsorAmount==null) lineItemSponsorAmount = "0";

			lineItemVariance = request.getParameter("variance");
			if (lineItemVariance==null) lineItemVariance = "0";

		//end Added


		sectionWithPatNoString = request.getParameter("sectionWithPatNo");


		unitCostArrayList.add(unitCost);
		noUnitArrayList.add(noUnit);
		discountArrayList.add(discount);
		lineitemIdArrayList.add(lineitemId);
		researchCostArrayList.add(researchCost);
		stdCareCostArrayList.add(stdCareCost);
		categoryArrayList.add(category);
		appIndirectsArrayList.add(appIndResCost);
		lineItemTotalCostArrayList.add(lineItemTotalCost);
		//Added By IA 11.03.2005
		lineItemSponsorAmountArrayList.add(lineItemSponsorAmount);
		lineItemVarianceArrayList.add(lineItemVariance);
		//end Added
		//set section patient number
	//	if (!(bgtSectionPatNo == null)) {

		   bgtSectionB.setBgtSectionId(bgtSectionIdVisible);
		   bgtSectionB.getBgtSectionDetails();
		   bgtSectionB.setSrtCostTotal(srtCostTotal);
		   bgtSectionB.setSrtCostGrandTotal(srtCostGrandTotal);
		   bgtSectionB.setSocCostTotal(socCostTotal);
		   bgtSectionB.setSocCostGrandTotal(socCostGrandTotal);
		   if ((bgtSectionPatNo == null)) bgtSectionPatNo = "0" ;
		   bgtSectionB.setBgtSectionPatNo(bgtSectionPatNo);

		   //Added by IA 11.03.2006
			if (	budgetTemplate.equals("C") ){

			   bgtSectionB.setSrtCostSponsorAmount(srtCostSponsor);
			   bgtSectionB.setSrtCostVariance(srtCostVariance);
			   bgtSectionB.setSocCostSponsorAmount(socCostSponsor);
			   bgtSectionB.setSocCostVariance(socCostVariance);
			}
			else
			{
			   bgtSectionB.setSrtCostSponsorAmount("0.00");
			   bgtSectionB.setSrtCostVariance("0.00");
			   bgtSectionB.setSocCostSponsorAmount("0.00");
			   bgtSectionB.setSocCostVariance("0.00");
 			}
		   //end Added
		   int ret = bgtSectionB.updateBgtSection();
	} else {

	  	String bgtSectionIdVisbles[] = request.getParameterValues("bgtSectionIdVisible");


		//		for(int k=0;k<bgtSectionIds.length;k++){}

		String bgtSectionPatNos[] = request.getParameterValues("patNo");

		String srtCostTotals[]=  request.getParameterValues("nsocSubTotalResearchCost");
		String srtCostGrandTotals[] = request.getParameterValues("nsocSubTotalTtlCost");
		String socCostTotals[] = request.getParameterValues("socSubTotalResearchCost");
		String socCostGrandTotals[] =  request.getParameterValues("socSubTotalTtlCost");

		String srtCostPatTotals[] = request.getParameterValues("nsocSubTotalPatResearchCost");
		String srtCostPatGrandTotals[] =  request.getParameterValues("nsocSubTotalPatTtlCost");
		String socCostPatTotals[] =  request.getParameterValues("socSubTotalPatResearchCost");
		String socCostPatGrandTotals[] = request.getParameterValues("socSubTotalPatTtlCost");

		String srtCostSponsors[] = request.getParameterValues("nsocSubTotalSponsorAmountCost");
		String srtCostVariances[] = request.getParameterValues("nsocSubTotalVariance");
		String socCostSponsors[] = request.getParameterValues("socSubTotalSponsorAmountCost");
		String socCostVariances[] = request.getParameterValues("socSubTotalVariance");


		//End added
		//for(int y=0;y<bgtSectionIds.length;y++){}

		String  sectionWithPatNos[] = request.getParameterValues("sectionWithPatNo");

		String perPatientSectionCnt = request.getParameter("perPatientSectionCnt");
		String oneSectionCnt = request.getParameter("oneSectionCnt");

		//set section patient number

		int secPatNos = 0;
		if(bgtSectionIdVisbles != null){

			secPatNos = bgtSectionIdVisbles.length;
		}

		for (i =0;i<secPatNos;i++) {

	  	 // check if there are any lineitems for the section


		  	//  if (!(bgtSectionPatNos[i] == null)) {

			       		  	bgtSectionB.setBgtSectionId(EJBUtil.stringToNum(bgtSectionIdVisbles[i]));
						   	bgtSectionB.getBgtSectionDetails();

						   //check if is one time fee or patient fee

						   budgetSectionTypeRow = budgetSectionTypeRows[i];
						   	if (	budgetSectionTypeRow.equals("O") )
						   	{


								   bgtSectionB.setSrtCostTotal(srtCostTotals[i]);
								   bgtSectionB.setSrtCostGrandTotal(srtCostGrandTotals[i]);
								   bgtSectionB.setSocCostTotal(socCostTotals[i]);
								   bgtSectionB.setSocCostGrandTotal(socCostGrandTotals[i]);


							}
						   else
						   {
								   bgtSectionB.setBgtSectionPatNo(bgtSectionPatNos[i]);
								   bgtSectionB.setSrtCostTotal(srtCostPatTotals[i]);
								   bgtSectionB.setSrtCostGrandTotal(srtCostPatGrandTotals[i]);
								   bgtSectionB.setSocCostTotal(socCostPatTotals[i]);
								   bgtSectionB.setSocCostGrandTotal(socCostPatGrandTotals[i]);

							}


							if (	budgetTemplate.equals("C") ){
								   bgtSectionB.setSrtCostSponsorAmount(srtCostSponsors[i]);
								   bgtSectionB.setSrtCostVariance(srtCostVariances[i]);
								   bgtSectionB.setSocCostSponsorAmount(socCostSponsors[i]);
								   bgtSectionB.setSocCostVariance(socCostVariances[i]);
							}
							else{
								   bgtSectionB.setSrtCostSponsorAmount("0.00");
								   bgtSectionB.setSrtCostVariance("0.00");
								   bgtSectionB.setSocCostSponsorAmount("0.00");
								   bgtSectionB.setSocCostVariance("0.00");
							}




						   int ret = bgtSectionB.updateBgtSection();

			 //   }
		}

		String lineitemIds[] = request.getParameterValues("lineitemId");

		String noUnits[] = request.getParameterValues("noUnits");

		String unitCosts[] = request.getParameterValues("unitCost");
		String discounts[] = request.getParameterValues("hidDiscountChkbox");
		String researchCosts[] = request.getParameterValues("researchCost");
		String stdCareCosts[] = request.getParameterValues("hidStdCareCost");
		String categories[]= request.getParameterValues("cmbCtgry");
		String appIndResCosts[] = request.getParameterValues("hidAppIndirects");
		String lineItemTotalCosts[] = request.getParameterValues("TtlCost");

		//added by IA 11.03.2006

		String lineItemSponsorAmounts[] = request.getParameterValues("sponsorAmount");

		String lineItemVariances[] = request.getParameterValues("variance");


		//end added



		String discountChkbox = "";



		for(i =0;i<rows;i++) {

			lineitemIdArrayList.add(lineitemIds[i]);

		    discountArrayList.add(discounts[i]);

			noUnitArrayList.add(noUnits[i]);

			unitCostArrayList.add(unitCosts[i]);

			researchCostArrayList.add(researchCosts[i]);

			stdCareCostArrayList.add(stdCareCosts[i]);

			categoryArrayList.add(categories[i]);

			appIndirectsArrayList.add(appIndResCosts[i]);

			lineItemTotalCostArrayList.add(lineItemTotalCosts[i]);


			//added by IA 11.03.2006

			if (	budgetTemplate.equals("C") ){
				lineItemSponsorAmountArrayList.add(lineItemSponsorAmounts[i]);
				lineItemVarianceArrayList.add(lineItemVariances[i]);
			}else{

				lineItemSponsorAmountArrayList.add("0");
				lineItemVarianceArrayList.add("0");
			}
			//end added

			}
	}



	output = lineitemB.updateAllLineitems(lineitemIdArrayList,
	 		discountArrayList, unitCostArrayList, noUnitArrayList, researchCostArrayList,
			stdCareCostArrayList,categoryArrayList,appIndirectsArrayList,ipAdd, usr, lineItemTotalCostArrayList, lineItemSponsorAmountArrayList, lineItemVarianceArrayList );

	if(output == -2) {
%>
<br>
<br>
<br>
<br>
<br>
<p class="sectionHeadings" align=center><%=MC.M_DataCnt_Svd%><%--Data could not be saved*****--%></p>
<%
	} else {

		budgetcalB.setBudgetcalId(bgtcalId);
		BudgetcalBean bcsk = budgetcalB.getBudgetcalDetails();
		budgetcalB.setSpOverHead(sponsorOHead);
		budgetcalB.setClOverHead(clinicOHead);
		budgetcalB.setSpFlag(sponsorOHeadApply);
		budgetcalB.setClFlag(clinicOHeadApply);
		budgetcalB.setBudgetFrgBenefit(fringeBenefit);
		budgetcalB.setBudgetFrgFlag(fringeBenefitApply);
		budgetcalB.setBudgetDiscount(costDiscount);
		budgetcalB.setBudgetDiscountFlag(costDiscountApply);
		budgetcalB.setBudgetExcldSOCFlag(excludeSOCApply);

		// Added by IA store calculations database	9/18/2206
		budgetcalB.setBudgetResearchCost(budgetResearchCost);
		budgetcalB.setBudgetResearchTotalCost(budgetResearchTotalCost);
		budgetcalB.setBudgetSOCCost(budgetSOCCost);
		budgetcalB.setBudgetSOCTotalCost(budgetSOCTotalCost);
		budgetcalB.setBudgetTotalCost(budgetTotalCost);
		budgetcalB.setBudgetTotalTotalCost(budgetTotalTotalCost);
		budgetcalB.setBudgetSalaryCost(budgetSalaryCost);
		budgetcalB.setBudgetSalaryTotalCost(budgetSalaryTotalCost);
		budgetcalB.setBudgetFringeCost(budgetFringeCost);
		budgetcalB.setBudgetFringeTotalCost(budgetFringeTotalCost);
		budgetcalB.setBudgetDiscontinueCost(budgetDiscontinueCost);
		budgetcalB.setBudgetDiscontinueTotalCost(budgetDiscontinueTotalCost);
		budgetcalB.setBudgetIndirectCost(budgetIndirectCost);
		budgetcalB.setBudgetIndirectTotalCost(budgetIndirectTotalCost);

		// END Added by IA store calculations database	9/18/2206


		//Added by IA 11.03.2006
		budgetcalB.setBudgetResearchSponsorAmount(budgetResearchSponsorAmount);
		budgetcalB.setBudgetResearchVariance(budgetResearchVariance);
		budgetcalB.setBudgetSOCSponsorAmount(budgetSOCSponsorAmount);
		budgetcalB.setBudgetSOCVariance(budgetSOCVariance);
		budgetcalB.setBudgetTotalSponsorAmount(budgetTotalSponsorAmount);
		budgetcalB.setBudgetTotalVariance(budgetTotalVariance);
		//End Added
		output = budgetcalB.updateBudgetcal();

		if(output == -2) {
%>
<br>
<br>
<br>
<br>
<br>
<p class="sectionHeadings" align=center><%=MC.M_DataCnt_Svd%><%--Data could not be saved*****--%></p>

<%
		} else {
%>
<br>
<br>
<br>
<br>
<br>
<p class="sectionHeadings" align=center><%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%></p>
<META HTTP-EQUIV=Refresh
	CONTENT="2; URL=patientbudget.jsp?includedIn=<%=includedIn%>&srcmenu=<%=src%>&mode=M&selectedTab=<%=selectedTab%>&pageMode=final&bgtcalId=<%=bgtcalId%>&budgetId=<%=budgetId%>&budgetTemplate=<%=budgetTemplate%>">
<%
		}
	}
%>

<%
//}//end of if for eSign check
}//end of if body for session

else
{
%>
<jsp:include page="timeout.html" flush="true" />
<%
}
%>

</BODY>

</HTML>





