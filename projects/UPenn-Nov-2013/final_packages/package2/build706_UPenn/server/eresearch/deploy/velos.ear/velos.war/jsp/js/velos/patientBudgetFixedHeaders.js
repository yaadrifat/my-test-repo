/**
 * This file is created to fixed the header and content 
 * of Patients Budget( type:patient/comparative budget)
 * This file is copy of fixedHeaders.js file
 * @param bodyName
 * @return 
 */
function patientBudgetFixedHeader(bodyName){
	if (navigator.userAgent.indexOf("MSIE") == -1){
		if(document.getElementById("budgetTemplate")){
			if(document.getElementById("budgetTemplate").value=='C' 
				|| document.getElementById("budgetTemplate").value=='P'){
				setPatientBugetStyle();
			}
		}
		if (bodyName)
			var tbodys = document.getElementsByName(bodyName);
		else
			var tbodys = document.getElementsByName("scrollTableBody");
		
		if (tbodys){
			for(var i=0;i<tbodys.length;i++){
				// We can put logic here to check if the current height 
				// of tbody has crossed the specified limit or not 
				do_patientBudgetTbodyscroll(tbodys[i]);
			}
		}
	}else{
		if(document.getElementById("budgetTemplate")){
			if(document.getElementById("budgetTemplate").value=='C' 
				|| document.getElementById("budgetTemplate").value=='P'){
				setPatientBugetStyle();
			}
		}
	}
}

function do_patientBudgetTbodyscroll(_tbody){
	// get the table node 
	var table = _tbody.parentNode;
	
	// Get the Last row in Thead ... 
	// COLGROUPS USING COLSPAN NOT YET SUPPORTED
	var thead = table.getElementsByTagName("THEAD")[0];
	
	var _rows = thead.getElementsByTagName("TR");
	var tableheader = _rows[_rows.length - 1];
	var headercells = tableheader.cells;
	
	// rows of tbody 
	var _frows = _tbody.getElementsByTagName("TR");
	
	// first row of tbody 
	var _fr = _tbody.getElementsByTagName("TR")[0];
	//var _fr = _tbody.getElementsByName("scrollTableRow")[0];
	
	// first row cells .. 
	var _frcells = _fr.cells;

	if (_frcells.length < headercells.length){
		var rowCount = 1;
		while (rowCount < _frows.length){
			// nth  row of tbody 
			_fr = _tbody.getElementsByTagName("TR")[rowCount];
			//var _fr = _tbody.getElementsByName("scrollTableRow")[rowCount];
	
			// nth row cells .. 
			_frcells = _fr.cells;
			
			if (headercells.length == _frcells.length){
				_fr.style.bgColor="blue";
				break;
			}
			rowCount++;
		}
	}

	var lastColHeader = false;
	// Apply width to header ..
	for(var i=0; i<headercells.length; i++){
	//for(var i=headercells.length-1; i>=0; i--){
		var firstColumn = (i == 0)?true:false;
		var lastColumn = (i == headercells.length-1)?true:false;
		var changeWidth = (lastColumn)? ((rowCount >= 1)?true:false)
				:true;
		var headerW ;
		var cellW 	;
		headerW = tableheader.cells[i].offsetWidth+40;
		cellW = _fr.cells[i].offsetWidth+40;
		var scrollbarWidth = _tbody.offsetWidth - _tbody.clientWidth;
		
		if (cellW > headerW){
			//alert(i + ' 1st ' +lastColumn + ' headerW:' + headerW + ' cellW:' + cellW);
			if (!lastColumn){ 			
				tableheader.cells[i].width = cellW;
				_fr.cells[i].width = cellW;
			}
			if (lastColumn){ 
				//alert('cellW-headerW: '+(cellW-headerW));
				if ((cellW-headerW) >= scrollbarWidth){
					lastColHeader = false;
					_fr.cells[i].width = cellW;
					tableheader.cells[i].width = _fr.cells[i].offsetWidth;
					tableheader.cells[i].style.paddingRight = scrollbarWidth+"px";
				}else{
					lastColHeader = true;
					_fr.cells[i].width =  cellW;
					tableheader.cells[i].width = _fr.cells[i].offsetWidth;
					tableheader.cells[i].style.paddingRight = scrollbarWidth+"px";
				}
			} 
		}else{ 
			//alert(i + ' 2nd ' +lastColumn + ' headerW:' + headerW + ' cellW:' + cellW);
			if (!lastColumn){
				tableheader.cells[i].width = headerW;
				_fr.cells[i].width = headerW;
			}
			if (lastColumn){
				//alert('headerW-cellW: '+(headerW-cellW));
				if ((headerW-cellW) >= scrollbarWidth){
					lastColHeader = false;
					tableheader.cells[i].width = headerW;
					_fr.cells[i].width = headerW;
					tableheader.cells[i].style.paddingRight = scrollbarWidth+"px";
				}else{
					lastColHeader = true;
					_fr.cells[i].width = headerW;
					tableheader.cells[i].width = headerW + scrollbarWidth;
					tableheader.cells[i].style.paddingRight = scrollbarWidth+"px";
				}
			}
		}
	}

	var j = headercells.length-1;
	scrollbarWidth = _tbody.offsetWidth - _tbody.clientWidth;
	
	var headerW = tableheader.offsetWidth;
	var rowW = _fr.offsetWidth;
	//alert('headerW: '+headerW+' rowW: '+rowW);
	
	while (headerW != (rowW+scrollbarWidth)){
		if (headerW < rowW){
			tableheader.width = rowW;
			_tbody.style.width = rowW;
		}else{
			tableheader.width = headerW;
			_tbody.style.width = headerW;
		}
		headerW = tableheader.offsetWidth;
		rowW = _tbody.clientWidth;
	}
	//tableheader.style.display = "block";
	//_tbody.style.display = "block"; 
}