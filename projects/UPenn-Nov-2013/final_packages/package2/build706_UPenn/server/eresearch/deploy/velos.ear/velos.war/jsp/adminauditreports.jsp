<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Audit_Rpts%><%--Audit Reports*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>

<SCRIPT language="JavaScript1.1">

function openwin1() {
     window.open("adminusersearchdetails.jsp?fname=&lname=&from=adminauditreports&accountId=0","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=450,left=100,top=200")
	 
;}

//Chn dev adds here
//Chn dev adds here
function fnValidate(formobj)
{
   
	reportChecked=false;
	reportNo=0;	

	for(i=0;i<formobj.repnum.length;i++)
	{
		if (formobj.repnum[i].checked)
		{
	   		reportChecked=true;
			reportNo=formobj.repnum[i].value;
		}

	}

	if (reportChecked==false)
	{

		alert("<%=MC.M_Selc_Rpt%>");/*alert("Please select a Report");*****/

		return false;

	}
		
	switch (reportNo)
	 {

		case "1": //
			if (formobj.datefrom.value=="")
			{

					alert("<%=MC.M_Selc_DateFrom%> ");/*alert("Please select 'Date From' ");*****/
			        formobj.datefrom.focus();   
			        return false;
			}
			else if (formobj.dateto.value=="")
			{

					alert("<%=MC.M_Selc_DateTo%>");/*alert("Please select 'Date To'");*****/
			        formobj.dateto.focus();   
			        return false;
			}	
			break;
		case "4": //
			if(formobj.action[0].checked==false && formobj.action[1].checked==false) 			{

				alert("<%=MC.M_PlsSelectAction%>");/*alert("Please select Action");*****/
			        return false;
			}
			else if (formobj.datefrom.value=="")
			{

					alert("<%=MC.M_Selc_DateFrom%> ");/*alert("Please select 'Date From' ");*****/
			        formobj.datefrom.focus();   
			        return false;
			}
			else if (formobj.dateto.value=="")
			{

					alert("<%=MC.M_Selc_DateTo%> ");/*alert("Please select 'Date To' ");*****/
			        formobj.dateto.focus();   
			        return false;
			}	
			break;
		case "5": //
			
			if(formobj.action[0].checked==false && formobj.action[1].checked==false) 			{

				alert("<%=MC.M_PlsSelectAction%>");/*alert("Please select Action");*****/
			        return false;
			}
			else if (formobj.username.value=="")
			{

				alert("<%=MC.M_Selc_User%> ");/*alert("Please select a User ");*****/
			        formobj.username.focus();   
			        return false;
			}
			else if (formobj.datefrom.value=="")
			{

					alert("<%=MC.M_Etr_TheFromDate%>");/*alert("Enter the From Date");*****/
			        formobj.datefrom.focus();   
			        return false;
			}
			else if (formobj.dateto.value=="")
			{

					alert("<%=MC.M_Selc_DateTo%>");/*alert("Please select 'Date To'");*****/
			        formobj.dateto.focus();   
			        return false;
			}	
			break;
		case "7": //
			if (formobj.datefrom.value=="")
			{

					alert("<%=MC.M_Selc_DateFrom%> ");/*alert("Please select 'Date From' ");*****/
			        formobj.datefrom.focus();   
			        return false;
			}
			else if (formobj.dateto.value=="")
			{

					alert("<%=MC.M_Selc_DateTo%>");/*alert("Please select 'Date To'");*****/
			        formobj.dateto.focus();   
			        return false;
			}	
			else if (formobj.username.value=="")
			{

					alert("<%=MC.M_Selc_User%> ");/*alert("Please select a User ");*****/
			        formobj.username.focus();   
			        return false;
			}
			
			break; 

	}
    
  return true;
}


</SCRIPT>

<% String src="";

src= request.getParameter("srcmenu");

%>	






<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="repdao" scope="request" class="com.velos.eres.business.common.ReportDaoNew"/>
<jsp:useBean id="repdao1" scope="request" class="com.velos.eres.business.common.ReportDaoNew"/>
<jsp:useBean id="repdao2" scope="request" class="com.velos.eres.business.common.ReportDaoNew"/>
<jsp:useBean id="repdao3" scope="request" class="com.velos.eres.business.common.ReportDaoNew"/>
<jsp:useBean id="accountB" scope="request" class="com.velos.eres.web.account.AccountJB"/>

<%@ page language = "java" import = "com.velos.eres.business.section.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB"%>

<body>

<br>
<DIV class="formDefault" id="div1">

	<jsp:include page="adminreporttabs.jsp" flush="true"/> 
<%-- INF-20084 Datepicker-- AGodara --%>
	<jsp:include page="jqueryUtils.jsp" flush="true"/> 
	
	<% 
	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))

		{
	AccountDao accountDao = accountB.getAccounts();
	ArrayList accountIds = accountDao.getAccIds();
	ArrayList userLogNames = accountDao.getAccNames();
	int len = accountDao.getCRows();
	String accountDD = "";
	int accountId = 0;
	String userLogName = "";
	if(len > 0){
		accountDD = accountDD + "<SELECT name=accountId>";		
		accountDD = accountDD + "<OPTION value=0> "+LC.L_All/*All*****/+" </OPTION>";
		
		for(int i=0;i<len;i++){
			userLogName = (String) userLogNames.get(i);
			accountId = ((Integer) accountIds.get(i)).intValue();  
			accountDD = accountDD + "<OPTION value=" + accountId +">" + userLogName + "</OPTION>";
		}
%>
<%			
		accountDD = accountDD + "</SELECT>";
	
	}
	
%>		

	<%/* SV, 8/24/04, fix for bug 1673, _new is not a keyword, _blank is and it opens new window instance each time which is what we want for reports.*/%>
	

<form name="report" METHOD=POST action="auditreport.jsp" target="_blank">
         <br>
         <table width="100%" cellspacing="0" cellpadding="0" border=0 >
         <tr>
          <td colspan = 3>
	     <p class = "defComments"><%=MC.M_SelRpt_ListBelow%><%--Select a report from the list below, specify appropriate filters and click on the 'Display' button.*****--%> </p> 
<%--     <p class = "defComments">Select a report from the list below and click on the 'Display' button. </p> --%>
         <BR>
         </td>
         </tr>
         
         <tr>
          <td width=40%>
         	<Font class=numberFonts > 1.</font><Font class=reportText ><I><%=MC.M_Selc_YourRptType%><%--Select your report type*****--%> </I></font>
         </td>

          <td width=40% >
         	<Font class=numberFonts >2.</font> <Font class=reportText ><I><%=MC.M_Selc_YourRptFilters%><%--Select your report filters*****--%></I></font>
         </td>

          <td width=20% >
         	<Font class=numberFonts >3.</font> <Font class=reportText ><I><%=LC.L_View_YourRpt%><%--View your report*****--%></I></font>
         </td>

<%--		 
          <td width=20% >
         	<Font class=numberFonts >2.</font> <Font class=reportText ><I>View your report</I></font>
         </td>
--%>
         </tr>
         </Table>
         <HR class=blue></hr>
		 <table width="100%" cellspacing="0" cellpadding="0" border=0 >
		 	<tr>
				<td width=45%>
					<input type=radio name=repnum value=1> <%=MC.M_OnBasis_OfDates%><%--On basis of Dates*****--%> </input> <BR>
					<input type=radio name=repnum value=4> <%=MC.M_BasisOf_ActAndDt%><%--On basis of Action and Dates*****--%> </input> <BR>
					<input type=radio name=repnum value=5> <%=MC.M_OnBasis_ActUsrDt%><%--On basis of Action, User and Dates*****--%> </input> <BR>
					<input type=radio name=repnum value=7> <%=MC.M_OnBasis_UsrDt%><%--On basis of User and Dates*****--%> </input> <BR>															
				</td>
				<td width=40%>
<%-- INF-20084 Datepicker-- AGodara --%>
					<b><%=LC.L_Date_From%><%--Date From*****--%></b>
					<input type=text name=datefrom class="datefield" size=8  value='' readonly>
					<br>
					<b><%=LC.L_Date_To%><%--Date To*****--%></b>
					<input type=text name=dateto class="datefield" size=8  value='' readonly>
					<br>
					<B> <%=LC.L_Action%><%--Action*****--%></B>
					<input type=radio name=action value=UPDATE ><%=LC.L_Update%><%--Update*****--%></input>
					<input type=radio name=action value=DELETE ><%=LC.L_Delete%><%--Delete*****--%></input>
					<br>
					<b><%=LC.L_User%><%--User*****--%></b>
					<input type="text" name=username size=15 value="">
					<A HREF='#' onClick='openwin1()'> <%=LC.L_Select_User%><%--Select User*****--%> </A>
					<input type="hidden" name=user>
					<br>
					<b><%=LC.L_Account%><%--Account*****--%></b>
					<%=accountDD%>
					<br>
					(<%=MC.M_LoginNameOfUsr_Acc%><%--This is the login name of the user who created the account*****--%>)
				<td width=15%>
				</td>
			</tr>
			<tr>
				<td colspan=2> </td>
				<td>
					
					<A type="submit" onClick = "return fnValidate(document.report)" href="#"><%=LC.L_Display%></A>
				</td>
			</tr>
		 </table>
<%--		 
Please select a report and enter the corresponding parameters. Following conditions apply as of now :-
<BR><BR>
i) Currently the user can only be the database user in your database. To find this fire the SQL 'select distinct user_name from audit_report'. This is case sensitive. 
<BR>
ii) Enter dates in mm/dd/yyyy format
<BR>
iii) Date from should be less than date To.
<BR>
iv) The report names suggest the parameters that would be used in a particular report.
<BR><BR> 

<table>
	<tr>		
	   	<td>
   			Report Type
	   	</td>
		<td>
			<select name=repNum>
				<option value=1>On basis of dates</option>
				<option value=2>On basis of Action</option>
				<option value=3>On basis of action and user</option>
				<option value=4>On basis of action and dates</option>
				<option value=5>On basis of action, user and dates</option>
				<option value=6>On basis of user</option>
				<option value=7>On basis of user and dates</option>																				
			</select>
		</td>
	</tr>	
	<tr>
	   	<td>
   			Action
	   	</td>
		<td>
			<select name=action>
				<option value=UPDATE>Update</option>
				<option value=>Insert</option> 

				<option value=DELETE>Delete</option>				
			</select>
		</td>
	</tr>
	<tr>
	   	<td>
			Date From    		
	   	</td>
		<td>
			<input name=dateFrom>
		</td>
	</tr>
	<tr>		
	   	<td>
   			Date To
	   	</td>
		<td>
			<input name=dateTo>
		</td>
	</tr>
	<tr>
		<td>
			User
		</td>
		<td>
			<input type="text" name=user value=ER>
		</td>
	</tr>
		

</table>

<input type=submit name=submit value=Submit>
--%>
</form>

<%
 }//end of if body for session

else

{
%>
  <jsp:include page="timeout_admin.html" flush="true"/>
  <%
}
%>   

<div>

<jsp:include page="bottompanel.jsp" flush="true"/> 


</div>

</form>
</div>



<DIV class="mainMenu" id = "emenu">

  <jsp:include page="velosmenus.htm" flush="true"/>   

</DIV>



</body>

</html>
