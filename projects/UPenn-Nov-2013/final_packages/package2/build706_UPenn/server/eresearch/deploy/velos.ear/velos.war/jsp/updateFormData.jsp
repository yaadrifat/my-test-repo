<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Save_Form%><%--Save Form*****--%></title>
<%!
private static final String DEFAULT_DUMMY_CLASS = "[Config_Form_Interceptor_Class]";
%>

<%@page import="com.velos.eres.service.util.*,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*" %>
<%@page import="oracle.xml.parser.v2.*,org.xml.sax.*,org.w3c.dom.*, oracle.xdb.*,com.velos.eres.business.saveForm.*" %>
<%@page import="com.velos.eres.audit.web.AuditRowEresJB" %>
<%@page import="com.velos.eres.business.linkedForms.impl.LinkedFormsBean" %>
<%@page import="com.velos.eres.web.grpRights.GrpRightsJB" %>
<%@page import="com.velos.eres.web.objectShare.ObjectShareJB"%>
<%@page import="com.velos.eres.web.patLogin.PatLoginJB" %>
<%@page import="com.velos.eres.web.studyRights.StudyRightsJB" %>
<%@page import="com.velos.eres.web.storage.*" %>
<%@page import="com.velos.eres.web.intercept.InterceptorJB" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" SRC="formjs.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<SCRIPT Language="javascript">

 function  validate(formobj){



   /* if (!(validate_col('e-Signature',formobj.eSign))) return false


	if(isNaN(formobj.eSign.value) == true) {
	alert("Incorrect e-Signature. Please enter again");
	formobj.eSign.focus();
	return false;
   	}*/

   }
   </script>



<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
 <jsp:useBean id="fldlibB" scope="request" class="com.velos.eres.web.fieldLib.FieldLibJB"/>
  <jsp:useBean id="formLibB" scope="request" class="com.velos.eres.web.formLib.FormLibJB"/>
 <jsp:useBean id="formQuery" scope="request" class="com.velos.eres.web.formQuery.FormQueryJB"/>
<jsp:useBean id="codeB" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
  <jsp:useBean id="eventcrf" scope="request" class="com.velos.esch.web.eventCrf.EventCrfJB"/>
<jsp:useBean id="objShareB" scope="request" class="com.velos.eres.web.objectShare.ObjectShareJB"/>
<jsp:useBean id="linkedFormB" scope="request" class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>
<jsp:useBean id="portalJB" scope="request" class="com.velos.eres.web.portal.PortalJB"/>
<jsp:useBean id="userSiteB" scope="page" class="com.velos.eres.web.userSite.UserSiteJB" />

<%@ page language = "java" import = "java.util.*,com.velos.eres.business.common.*"%>


<body>
<jsp:include page="include.jsp" flush="true"/>
<DIV  id="div1">

<%
	HttpSession tSession = request.getSession(true);
int cnt = 0;

int promptForMarkDoneFlag = 0;
	if (sessionmaint.isValidSession(tSession))
	{

%>
		<jsp:include page="sessionlogging.jsp" flush="true"/>
 <%
		String eSign= request.getParameter("eSign");
		String formStatus = request.getParameter("er_def_formstat");
		String userLogName = request.getParameter("userLogName");
		userLogName = StringUtil.isEmpty(userLogName)? "" : userLogName;
		String override_count_str=request.getParameter("override_count");
		override_count_str=(override_count_str==null)?"0":override_count_str;
		int override_count=EJBUtil.stringToNum(override_count_str);
    	String oldESign = (String) tSession.getValue("eSign");
    	String oldUserId = (String) tSession.getAttribute("loginname");

    	String responseId=request.getParameter("responseId");
    	responseId=(responseId==null)?"":responseId;

    	String parentId=request.getParameter("parentId");
    	parentId=(parentId==null)?"":parentId;


    	String outputTarget = "";
		outputTarget = request.getParameter("outputTarget");

		if(StringUtil.isEmpty(outputTarget))
		{
			outputTarget = "";
		}

	  	String eSignRequired = "";
    	eSignRequired = request.getParameter("eSignRequired");

		if(StringUtil.isEmpty(eSignRequired))
		{
			eSignRequired = "0";
		}


		String param = "";
		String paramVal = "";
		String paramName = "";

		String ignoreRights = "";
		String isEditForm = "";
		String ppCRF = "";
		String selfLogout = "";



		int consentingCode = 0;
		int notconsentingCode = 0;
		String consentingRadio = "";


		//from portal
		ignoreRights = (String) tSession.getValue("pp_ignoreAllRights") ;
		isEditForm = (String) tSession.getValue("pp_isEditForm") ;
		ppCRF = (String) tSession.getValue("pp_scheduleCRF") ;
		selfLogout = (String) tSession.getValue("pp_selfLogout") ;

		String pp_consenting = "";//portal
		pp_consenting = (String) tSession.getValue("pp_consenting") ;

	 	String formDispLocation = request.getParameter("formDispLocation");
		// Calculate access rights -- this part for PP
		boolean isPatientPortal = false;
		if (!StringUtil.isEmpty(ignoreRights)) { // Patient Portal
			isPatientPortal = true; 
		}
		String accountId = (String)tSession.getAttribute("accountId");
		int pageRight = 0;
		LinkedFormsBean linkedFormsBean = 
			linkedFormB.findByFormId(StringUtil.stringToNum(request.getParameter("formId")));
		String linkedType = StringUtil.trueValue(linkedFormsBean.getLFDisplayType()).trim();
		
		if (isPatientPortal) {
			PatLoginJB patLoginJB = (PatLoginJB)tSession.getAttribute("pp_currentLogin");
			String patientPortalPatId = patLoginJB.getPlId();
			if (StringUtil.stringToNum(request.getParameter("formFilledFormId")) > 0) {
				// Make sure patientPortalPatId = fk_per
				String ppFkPer = linkedFormB.getFormResponseFkPer(
						StringUtil.stringToNum(request.getParameter("formFilledFormId")));
				// Make sure creator of form response matches with portal's audit user
				String creatorForPortal = linkedFormB.getCreatorForPortal();
				boolean auditUserMatchesWithCreator = portalJB.comparePortalAuditUserWith(
						StringUtil.stringToNum(creatorForPortal), 
						StringUtil.stringToNum(patLoginJB.getFkPortal()));
				System.out.println("auditUserMatchesWithCreator="+auditUserMatchesWithCreator
						+"patientPortalPatId="+patientPortalPatId+" ppFkPer="+ppFkPer);
				if (auditUserMatchesWithCreator &&
						StringUtil.stringToNum(ppFkPer) > 0 &&
						StringUtil.stringToNum(patientPortalPatId) ==
						StringUtil.stringToNum(ppFkPer)) {
					pageRight = 7;
				}
			} else {
				pageRight = 7; // new form => grant access
			}
			// Deny any attempt of a PP user trying to create/update a response as another patient
			if (StringUtil.stringToNum(request.getParameter("formPatient")) > 0) {
				if (StringUtil.stringToNum(request.getParameter("formPatient")) !=
						StringUtil.stringToNum(patientPortalPatId)) {
					pageRight = 0;
				}
			}
		} else {
			// Calculate access rights -- this part for eResearch user
			pageRight = linkedFormB.getFilledFormUserAccess(request);
	    }

		if (StringUtil.isEmpty(ignoreRights))
		{
			ignoreRights = "false";
			selfLogout = "0";
			pp_consenting = "";

		}else // from portal
		{
			if (StringUtil.isEmpty(pp_consenting))
		     {
		     	pp_consenting = "";
		     }

		     consentingRadio = request.getParameter("vel_consenting");

		     if (StringUtil.isEmpty(consentingRadio))
		     {
		       consentingRadio = "0";
		     } else
		     {

	          if (! StringUtil.isEmpty(pp_consenting) && pp_consenting != null && (!pp_consenting.equals("null")) )
	            {
	        	 //consentingCode = codeB.getCodeId("fillformstat", "consent");
              	 notconsentingCode = codeB.getCodeId("fillformstat", "notconsent");


			     if (EJBUtil.stringToNum(consentingRadio) == notconsentingCode )
			     {
			       selfLogout = "1";
			     }

			     //set the consentingcode as form status

			     formStatus = consentingRadio;
			    }
		     }

		}

		if (StringUtil.isEmpty(isEditForm))
		{
			isEditForm = "false";
		}

		if (StringUtil.isEmpty(ppCRF))
		{
			ppCRF = "false";
		}

		if(("SP".equals(formDispLocation) && (!"true".equals(ignoreRights)) && "L2_ON".equals(LC.L_Auth2_Switch)) && !oldUserId.equals(userLogName)){
		%>
			<br><br><br><br><br><br><br>
			<table width="80%">
				<tr>
					<td width="5%">&nbsp;</td>
					<td >
						<p class="sectionHeadings" align="center">
							<%=MC.M_WrongUserName_EnterAgain%>
						</p>
					</td>
				</tr>
				<tr height=20></tr>
				<tr>
					<td></td>		
				</tr>
			</table>
			<table width="80%" >
				<tr>
					<td align="center">&nbsp;&nbsp;
					<% String agent1 = request.getHeader("USER-AGENT");
					   agent1=agent1.toLowerCase();
					   if (agent1.indexOf("msie") > -1) { // IE %>
						<img class="asIsImage" src="../images/jpg/Back.gif" onClick="history.go(-1);" align="middle" border="0">	       
					<% } else { %>
						<A href="javascript:void(0);" onclick="history.go(-1);"><img class="asIsImage" src="../images/jpg/Back.gif" align="middle" border="0"> </A>
					<% } %>
					</td>
				</tr>
			</table>
			<script>
				if (!(typeof(document.getElementById("notification"))=="undefined") && (document.getElementById("notification")!=null))	
				toggleNotification("off","");
			</script>
		<%
		} else {
		//handlr cases when no-signature is entered
		if (StringUtil.isEmpty(oldESign) || StringUtil.isEmpty(eSign) || eSignRequired.equals("0"))
		{
			eSign = "";
			oldESign = "";
		}



	   if(oldESign.equals(eSign))
	   {
	   		String formId = "";
	   		formId = request.getParameter("formId");

			String fkStorageForSpecimen = request.getParameter("fkStorageForSpecimen");



			if (StringUtil.isEmpty(fkStorageForSpecimen) )
			{
				fkStorageForSpecimen = "";
			}else
			{
				if (fkStorageForSpecimen.equals("null"))
				{
					fkStorageForSpecimen = "";
				}
			}

			String createNewSpecimen= request.getParameter("createNewSpecimen");



			if (StringUtil.isEmpty(createNewSpecimen))
			{
				createNewSpecimen = "0";
			}else
			{
				if (createNewSpecimen.equals("null"))
				{
					createNewSpecimen = "0";
				}
			}




			int errorCode=0;
			String src = "";
			String selectedTab="";

			Enumeration paramNames = request.getParameterNames();

			ArrayList alParam = new ArrayList ();
			ArrayList alParamVal = new ArrayList ();

			ArrayList saveFormParam = new ArrayList();

			saveFormParam.add("formDispLocation");
			saveFormParam.add("formFillMode");
			saveFormParam.add("formId");
			saveFormParam.add("srcmenu");
			saveFormParam.add("formFillDt");
			saveFormParam.add("formPullDown");
			saveFormParam.add("calledFromForm");
			saveFormParam.add("formFilledFormId");
			saveFormParam.add("formLibVer");
			saveFormParam.add("formStudy");
			saveFormParam.add("selectedTab");
			saveFormParam.add("formPatient");
			saveFormParam.add("specimenPk");
			saveFormParam.add("fkStorageForSpecimen");
			saveFormParam.add("createNewSpecimen");
			saveFormParam.add("eSignRequired");

			 String specimenPk = "";
			    specimenPk = request.getParameter("specimenPk");

			     if (StringUtil.isEmpty(specimenPk))
				 {
				 	specimenPk = "";
				 }


			while (paramNames.hasMoreElements())
			{
	   			param = (String) paramNames.nextElement();

	   			if (saveFormParam.indexOf(param) >= 0) // we do not have to save this param
	   			{
	   				continue;
	   			}

				if ((! param.equals("y")) && (! param.equals("x")) && (! param.equals("eSign")))
				{
					if ( ! alParam.contains(param))
					{
						alParam.add(param);
					}
				}

			}
			for (int k = 0; k < alParam.size() ; k++ )
			{
	   			paramName = (String) alParam.get(k);
				if ( request.getParameterValues(paramName) != null )
				{
					paramVal = "";

    				for (int i = 0; i < request.getParameterValues(paramName).length; i++ )
    				{
							if ( i == 0)
							{
								paramVal = request.getParameterValues(paramName)[i] ;
							}
							else
							{
							//changed by sonika on April 28, 04
							//[VELCOMMA] is used as data separator incase of multiple choice
    							paramVal =  paramVal + "[VELCOMMA]" + request.getParameterValues(paramName)[i]  ;
							}

    				}

    				alParamVal.add(paramVal);

				}

			}



////////////////////////////anu




		ArrayList fldName = new ArrayList();
		ArrayList fldValue = new ArrayList();
		ArrayList fldValidation = new ArrayList();
		ArrayList fldSysIds = new ArrayList();
		ArrayList fldMode =  new ArrayList();
		
		ArrayList qStatus = new ArrayList();
		ArrayList qStatComments = new ArrayList();

		int indx = 0;
		int velSepIndx = 0;
		String str ="";
		String strVal = "";
		String rsnStr="";
		ArrayList queryModeList=new ArrayList();
		ArrayList reasonList=new ArrayList();
		ArrayList statusList=new ArrayList();
		ArrayList commentList=new ArrayList();
		String dataEntryDateStr = "er_def_date_01"; 

		for(int i=0;i<alParam.size();i++)
			{
				str = (String)alParam.get(i);

				strVal = (String)alParamVal.get(i);
				if(str.startsWith("hdn_") && !((strVal).equals(""))){
					indx = str.indexOf("fld");	
					String fldValidationType = "";
					if (indx==-1){
						indx = str.indexOf("er_def_date_01");
						fldValidationType = str.substring(str.indexOf("_")+1, str.indexOf("_er_def_date_01"));
					}else{
						fldValidationType = str.substring(str.indexOf("_")+1, str.indexOf("_fld"));
					}

					if (indx < 0) { continue; }
					
					String fldSysId = "";
					fldSysId = str.substring(indx);
					
					String fldSelectStr="qry_"+fldValidationType+"_"+fldSysId;
					int querySelection = 0; 
					querySelection = EJBUtil.stringToNum(request.getParameter(fldSelectStr));
					
					if (querySelection > 0){
						fldSysIds.add(fldSysId);
						fldValue.add(request.getParameter(str.substring(indx)));					
						
						velSepIndx = strVal.indexOf("[VELSEPLABEL]");
						fldValidation.add(strVal.substring(0,velSepIndx));
						fldName.add(strVal.substring(velSepIndx+13));
						//StringTokenizer strTokenizer = new StringTokenizer(strVal,"[VELSEPLABEL]");
						
						String modeStr="";
						modeStr="mode_"+fldValidationType+"_"+fldSysId;
						modeStr=(modeStr==null)?"":modeStr;
						queryModeList.add(request.getParameter(modeStr));					
						
						rsnStr="rsn"+str.substring(3);
						rsnStr=(rsnStr==null)?"":rsnStr;
						String reason = request.getParameter(rsnStr);
						//EDC_AT4 extn
						reason = (reason == null)?"":reason;
						reasonList.add(reason);
						
						String stsStr="";
						stsStr="sts_"+fldValidationType+"_"+fldSysId;
						stsStr=(stsStr==null)?"":stsStr;
						statusList.add(request.getParameter(stsStr));
						
						String cmtStr="";
						cmtStr="cmt_"+fldValidationType+"_"+fldSysId;
						cmtStr=(cmtStr==null)?"":cmtStr;
						String comment = request.getParameter(cmtStr);
						comment=(comment==null)?"":comment;
						commentList.add(comment);
						//fldValidation.add(strTokenizer.nextToken());
						//fldName.add(strTokenizer.nextToken());
						
						qStatComments.add(request.getParameter("comments"+i));
						cnt++;
					}
				}
				
			}
//			System.out.println("fldName\n"+fldName+"fldSysId\n"+fldSysId+"fldValidation\n"+fldValidation);

			if(cnt>0){%>


				<%}
			// If there are overridden fields in the filled form display field labels, value, validations.

///////////////////////////////anu
	//  by salil on 13 oct 2003 to see if the check box type multiple choice exists
	//  or not and to handle the case when no option is selected in a  check box type multiple choice
	// when no option is selected in a check box type multiple choice the element name does not
	 // come  forward to this page if no option is selected and hence the "chkexists"  variable is used to see
	//  if the page has a multiple choice field  and if the page has a a multiple choice field the
	//  a default value(&nbsp) is assigned to this field .

	     int index=alParam.indexOf("chkexists");
		 String chkexists = (String)alParamVal.get(index);

		 if( chkexists.equals("Y")){
		   	FieldLibDao fldLibDao = new FieldLibDao();
			fldLibDao = fldlibB.getFieldsInformation(Integer.parseInt(formId));
			ArrayList fldSysIDs = new ArrayList();
			ArrayList fldTypes = new ArrayList();

			fldSysIDs = fldLibDao.getFldSysID();
			fldTypes =  fldLibDao.getFldType();

			int count= fldSysIDs.size();

			for(int s=0;s<count;s++){
				String fldid = (String) fldSysIDs.get(s);
				String fldType = (String) fldTypes.get(s);

				//modified by sonia abrol, 02/08/06, to add &nbsp; for only multiple choice fields
				if ( ! alParam.contains(fldid) && fldType.equals("M"))
				{
						alParam.add(fldid);
						alParamVal.add("&nbsp;");

				}
   			}

   		}

	SaveFormStateKeeper sk = new SaveFormStateKeeper();


	//get data from request

		String[] stat = new String[cnt];
		String ipAdd = (String) tSession.getValue("ipAdd");
        String usr = (String) tSession.getValue("userId");

		String formFillMode = request.getParameter("formFillMode");
		String formPullDown = request.getParameter("formPullDown");
		String entryChar = "";
		int numEntries = 0;

		if (!(formPullDown==null)) {
			 	StringTokenizer strTokenizer = new StringTokenizer(formPullDown,"*");

	 	     	formId = strTokenizer.nextToken();
			    entryChar = strTokenizer.nextToken();
			    numEntries = EJBUtil.stringToNum(strTokenizer.nextToken());


			}



		String formFillDt = request.getParameter("formFillDt");

		String calledFromForm = request.getParameter("calledFromForm");



   		String showPanel= "";
       showPanel= request.getParameter("showPanel");

      if (StringUtil.isEmpty(showPanel))
      {
     	showPanel= "true";
      }

      		 String formCategory = request.getParameter("formCategory");
		 String submissionType = request.getParameter("submissionType");

    if (StringUtil.isEmpty(formCategory))
			 {
			 	 formCategory="";
			 }

			 if (StringUtil.isEmpty(submissionType))
			 {
			 	 submissionType="";
			 }




		String formFilledFormId = "";
		String patientId = "";
		String studyId = "";
		String urlStr = "";
		String patProtId = "";
		String statid = "";
		String statDesc = "";
		String patientCode = "";
        String fkcrf="";
		String schevent="";
		String formLibVer = "";
		int openId=0;
		int from=0;
		int linkedFormId = 0;
		sk.setFormId(formId);
		sk.setIpAdd(ipAdd)   ;
		sk.setParams(alParam)	;
		sk.setParamValues(alParamVal);
	    sk.setDisplayType(formDispLocation);

		//set the form status in form_completed
		sk.setFormCompleted(formStatus);

		//for new CRf forms, in case of other forms, this will be blank

		schevent = (String) request.getParameter("schevent");
		if (StringUtil.isEmpty(schevent))
		{
			schevent = "";
		}

		if (schevent.equals("null"))
		{
			schevent = "";
		}

		sk.setSchEvent(schevent);

		src = request.getParameter("srcmenu");

		if (formFillMode.equals("N"))
		{
			sk.setCreator(usr) ;



			sk.setSpecimenPk(specimenPk);

			//Retrieve the codeDao for saving the form queries
			if (override_count>0)
			{
			CodeDao cd = new CodeDao();
			openId = cd.getCodeId("query_status", "open");
			for(int i=0;i<cnt;i++)
			{
				statusList.add(""+openId);

  	  	  	 }
			 }
		}
		else
		{

			formFilledFormId =  request.getParameter("formFilledFormId");
			formLibVer = request.getParameter("formLibVer");


			sk.setId(EJBUtil.stringToNum(formFilledFormId))	 ;
			sk.setFormLibVer(formLibVer);
			sk.setLastModifiedBy(usr)	 ;
		}

		if (formDispLocation.equals("PA"))
		{
			patientId = request.getParameter("formPatient");
			selectedTab = request.getParameter("selectedTab");
			sk.setPatientId(patientId);
			if (override_count>0) from=1;
		}
		else if (formDispLocation.equals("A")) //called from account
		{
			sk.setAccountId(accountId);
			if (override_count>0) from=3;
		}
		else if (formDispLocation.equals("S") || formDispLocation.equals("SA"))
		{
			studyId = request.getParameter("formStudy");
			selectedTab = request.getParameter("selectedTab");

			sk.setStudyId(studyId);
			if (override_count>0) from=2;
		}
		else if (formDispLocation.equals("SP"))
		{
			patientId = request.getParameter("formPatient");
			selectedTab = request.getParameter("selectedTab");
			patProtId = request.getParameter("formPatprot");
			studyId = request.getParameter("patStudyId");
			statDesc = request.getParameter("statDesc");
			statid = request.getParameter("statid");
			patientCode = request.getParameter("patientCode");
			sk.setPatprotId(patProtId);
			sk.setPatientId(patientId);
			if (override_count>0) from=4;
		}
	    else if (formDispLocation.equals("C")){
			fkcrf = (String) tSession.getAttribute("fkcrf");
			schevent = (String) tSession.getAttribute("schevent");
			patientId = (String) tSession.getAttribute("patientId");
			statDesc = (String) tSession.getAttribute("statDesc");
			statid = (String) tSession.getAttribute("statid");
			patProtId = (String) tSession.getAttribute("patProtId");
			patientCode = (String) tSession.getAttribute("patientCode");
			studyId = (String) tSession.getAttribute("formStudy");
			sk.setFkCrf(fkcrf);
			sk.setSchEvent(schevent);
			if (override_count>0) from=5;
		}
		
		LinkedFormsDao linkedFormsDao =
				linkedFormB.getLinkedFormByFormId(StringUtil.stringToNum(accountId), StringUtil.stringToNum(formId), formDispLocation);
		if (linkedFormsDao == null || linkedFormsDao.getLFId() == null || linkedFormsDao.getLFId().size() == 0) {
			linkedFormId = 0;
		} else {
			linkedFormId = (Integer)linkedFormsDao.getLFId().get(0);
		}
		boolean addAsNewPrivilegedUser = false;
	    objShareB.getObjectShareDetails(linkedFormId,"P","6", usr, accountId);
	    String noshareId= StringUtil.trueValue(objShareB.getShrdWithIds());
	    if (noshareId.length() > 0 && pageRight >= 5) {
	    	addAsNewPrivilegedUser = true;
		    objShareB.getObjectShareDetails(linkedFormId,"P","6", usr, accountId);
		    noshareId= StringUtil.trueValue(objShareB.getShrdWithIds());
		    String[] shareIdArray = noshareId.split(",");
		    for (int iX=0; iX<shareIdArray.length; iX++) {
			    if (shareIdArray[iX].matches(usr)) {
			    	addAsNewPrivilegedUser = false;
			    }
		    }
	    }
	    if (addAsNewPrivilegedUser && linkedFormId > 0) {
	    	System.out.println("Add Obj share now!");
	    	objShareB = new ObjectShareJB();
	    	objShareB.setFkObj(String.valueOf(linkedFormId));
	    	objShareB.setObjNumber("6");
	    	objShareB.setObjSharedId(usr);
	    	objShareB.setObjSharedType("P");
	    	objShareB.setCreator(usr);
	    	objShareB.setIpAdd(ipAdd);
	    	int retFlag = objShareB.insertIntoObjectShareByType();
	    	//System.out.println("retFlag="+retFlag);
	    }

		
		int ret = 0;
		//SaveFormDao fDao = new SaveFormDao ();
		
		if (pageRight >= 5) { // Has edit or new right (view right assumed)


		 // before form is created, logic for auto creation of specimens and storage kits storage
		 if ((! StringUtil.isEmpty(fkStorageForSpecimen) || createNewSpecimen.equals("1")) && formFillMode.equals("N"))
			{
				String mainFKStorage = request.getParameter("mainFKStorage");
				String specimenId = request.getParameter("specimenId");
				String specType = request.getParameter("specType");
				String specimenStatus = request.getParameter("specimenStatus");
				String statDate = request.getParameter("statDate");
				String strCoordinateY =  request.getParameter("strCoordinateY");
				String strCoordinateX =  request.getParameter("strCoordinateX");
				Hashtable htReturn = new Hashtable();
				String newSpec = "";


				if ( StringUtil.isEmpty(mainFKStorage))
				{
				  mainFKStorage = "";
				}

				if ( StringUtil.isEmpty(specimenId))
				{
				  specimenId = "";
				}

				if ( StringUtil.isEmpty(specType))
				{
				  specType = "";
				}

				if ( StringUtil.isEmpty(specimenStatus))
				{
				  specimenStatus = "";
				}

				if ( StringUtil.isEmpty(statDate))
				{
				  statDate = "";

				}
				if ( StringUtil.isEmpty(strCoordinateY))
				{
					strCoordinateY = "";

				}
				if ( StringUtil.isEmpty(strCoordinateX))
				{
					strCoordinateX = "";

				}
					//KM-For Speciman status date
					statDate = statDate + " " + "00:00:00";
					//create the main storage, assign it to the selected location
					Hashtable htParamsForSpec = new Hashtable();
					htParamsForSpec.put("newlocation" , mainFKStorage);
					htParamsForSpec.put("specimenId" , specimenId);
					htParamsForSpec.put("specType" , specType);
					htParamsForSpec.put("specimenStatus" , specimenStatus);
					htParamsForSpec.put("statDate" , statDate);
					htParamsForSpec.put("fkStorageForSpecimen" , fkStorageForSpecimen);
					htParamsForSpec.put("strCoordinateY" , strCoordinateY);
					htParamsForSpec.put("strCoordinateX" , strCoordinateX);

					htParamsForSpec.put("fkper" , patientId);
					htParamsForSpec.put("fkstudy" , studyId);
					htParamsForSpec.put("fkschevents1" , schevent);


					StorageJB sjb = new StorageJB();
					htReturn = sjb.addFormSpecimenToStorageKit(htParamsForSpec,usr, ipAdd,accountId);

						if (htReturn != null)
						{
							newSpec = (String) htReturn.get("newspecimen");
							if (!StringUtil.isEmpty(newSpec))
							{
					          	sk.setSpecimenPk(newSpec);

							}

						}


			}

		if (formFillMode.equals("M"))
		{
			if (pageRight >= 6) { // Has edit right
			 ret = formLibB.updateFilledFormBySP(sk);
			 if (ret == 0)
			 {
			 	ret = EJBUtil.stringToNum(formFilledFormId);
			 	String remarks = request.getParameter("remarks");
				if (!StringUtil.isEmpty(remarks)){
					AuditRowEresJB auditJB = new AuditRowEresJB();
					auditJB.setReasonForChangeOfFormResponse(ret,StringUtil.stringToNum(usr),remarks);
				}
			 }
			} else {
				ret = -6; // edit right violation
			}
		}
		else // Has new right
		{
			 ret = formLibB.insertFilledFormBySP(sk);

			 //incase of new entry, increment the number of entries
			 if (!(formPullDown==null)) {
			 	numEntries = numEntries +1;
			    formPullDown = formId + "*"+ entryChar + "*" + numEntries;
			}
		}
	} else {
		ret = -6; // access rights violation
	}


			if (ret>=0) {
				// -- Add a hook for customization for after a form update succeeds
				try {
					if (!StringUtil.isEmpty(LC.Config_Form_Interceptor_Class) &&
							!DEFAULT_DUMMY_CLASS.equals(LC.Config_Form_Interceptor_Class)) {
						Class clazz = Class.forName(LC.Config_Form_Interceptor_Class);
						Object obj = clazz.newInstance();
						Object[] args = new Object[2];
						args[0] = (Object)request;
						args[1] = (Integer)ret;
						((InterceptorJB) obj).handle(args);
					}
				} catch(Exception e) {
					e.printStackTrace();
				}
				// -- End of hook
				%>
		<BR><BR><BR><BR><BR>
		<P class="successfulmsg" align="center"><%=MC.M_Data_SavedSucc%><%--Data Saved Successfully*****--%></P>

		<%
			if (! StringUtil.isEmpty(outputTarget))
			{
				%>
					<script>
						  if (window.opener != undefined)
						  {
						  	<%
						  		out.println(" obj = window.opener.document.getElementById('" + outputTarget + "');" );
						  		out.println(" obj.click();");
							%>

							 window.opener = null;
						     setTimeout("self.close()",100);
						  }
					</script>

				<%

			}
		%>

		<% } else if (ret == -6) { %>
		
		<BR><BR><BR><BR><BR>
		<P class="successfulmsg" align="center"><%=MC.M_InsuffAccRgt_CnctSysAdmin%></P>
		
		<% } else {  %>
		
		<BR><BR><BR><BR><BR>
		<P class="successfulmsg" align="center"><%=MC.M_DataNotSvd_Succ%><%--Data was not Saved Successfully*****--%></P>

		 <% if ( ret == -5)
		 {
		   %>
		 	<P class="successfulmsg" align="center"><%=MC.M_FrmHave_IncorrectDb%><%--This form may have incorrect mapping in the database. Please contact your Site Administrator stating this message with the Form name and its Location.Please note that you may see that the form response was saved but Ad-Hoc queries will not be able to pull data for the form response. After the problem is fixed, please delete this form response and save a new one*****--%></P>
		   <%
		 	return;
		 } %>
		<%}





		if (formDispLocation.equals("PA"))
		{

		 if ((! StringUtil.isEmpty(ignoreRights)) && ignoreRights.equals("true") && isEditForm.equals("true"))
			{
				if (selfLogout.equals("0"))
				{
				%>
					<META HTTP-EQUIV=Refresh CONTENT="2; URL=patientHome.jsp">
			  <%
			  	}
			  	else
			  	{
			  		%>
					<META HTTP-EQUIV=Refresh CONTENT="2; URL=patientlogout.jsp">
			  <%
			  	}
			}
			else
			{

				if ((! StringUtil.isEmpty(ignoreRights)) && ignoreRights.equals("true") && (entryChar.equals("E") || (! StringUtil.isEmpty(schevent) && ppCRF.equals("true") ) ) )
				{
					if (selfLogout.equals("0"))
					{

					%>
						<script>
						  if (window.opener != undefined)
						  {
							window.opener.location.reload();
							self.close();
						  }
						</script>
					<%
					}
					else //self logout
					{
						%>
							<script>
							  if (window.opener != undefined)
							  {
								window.opener.location = "patientlogout.jsp";
								self.close();
							  }
							</script>

						<%
					}
				}
				else
				{
					if (selfLogout.equals("0"))
					{
				%>
		 		  <META HTTP-EQUIV=Refresh CONTENT="2; URL=formfilledpatbrowser.jsp?calledFromForm=<%=calledFromForm%>&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&pkey=<%=patientId%>&patProtId=<%=patProtId%>&calledFrom=S&statDesc=<%=statDesc%>&statid=<%=statid%>&patientCode=<%=patientCode%>&formFillDt=<%=formFillDt%>&formPullDown=<%=formPullDown%>&fkStorageForSpecimen=<%=fkStorageForSpecimen%>">
		 	    <%

		 	    	}
		 	    	else //self logout
		 	    	{
		 	    		%>
							<script>
							  if (window.opener != undefined)
							  {
								window.opener.location = "patientlogout.jsp";
								self.close();
							}
							</script>

						<%

		 	    	}
		 	   }
		 	}

		}
		else if(formDispLocation.equals("S") || formDispLocation.equals("SA") )
		{

				//get IRB Params
				Hashtable htIRBParams = new Hashtable();
				htIRBParams = (Hashtable)tSession.getAttribute("IRBParams");

				String IRBSelectedTab = "";
				String submissionPK = "";
				String submissionBoardPK ="";

				if 	 ( htIRBParams != null	 )
			   	{

					IRBSelectedTab  = (String)htIRBParams.get("selectedTab");
					submissionPK = (String) htIRBParams.get("submissionPK");
					submissionBoardPK = (String) htIRBParams.get("submissionBoardPK");


			   	}


			if ((!StringUtil.isEmpty(formCategory)) && (!StringUtil.isEmpty(submissionType)) &&
				  EJBUtil.stringToNum(formStatus) == codeB.getCodeId("fillformstat", "complete") &&
				(EIRBDao.isTabForActionWindow(IRBSelectedTab) == true))
			{
				//remove Attribute
				/*
			   	if (htIRBParams !=null)
			   	{
			   		tSession.removeAttribute("IRBParams");
				}
				*/

			   	// Came from a pop-up Form browser; switch to action page <- This logic removed due to req change
			%>
    			<META HTTP-EQUIV=Refresh CONTENT="5; URL=formfilledstudybrowser.jsp?formCategory=<%=formCategory%>&submissionType=<%=submissionType%>&showPanel=<%=showPanel%>&calledFromForm=<%=calledFromForm%>&srcmenu=<%=src%>&studyId=<%=studyId%>&selectedTab=<%=selectedTab%>&formFillDt=<%=formFillDt%>&formPullDown=<%=formPullDown%>&irbReviewForm=true">
            <%

			}
			else
			{

			%>
			<META HTTP-EQUIV=Refresh CONTENT="2; URL=formfilledstudybrowser.jsp?formCategory=<%=formCategory%>&submissionType=<%=submissionType%>&showPanel=<%=showPanel%>&calledFromForm=<%=calledFromForm%>&srcmenu=<%=src%>&studyId=<%=studyId%>&selectedTab=<%=selectedTab%>&formFillDt=<%=formFillDt%>&formPullDown=<%=formPullDown%>">

		<%
			}

		}
		else if (formDispLocation.equals("A"))//called from Account
		{
		//if(cnt == 0){%>
		<META HTTP-EQUIV=Refresh CONTENT="2; URL=formfilledaccountbrowser.jsp?calledFromForm=<%=calledFromForm%>&srcmenu=<%=src%>&formFillDt=<%=formFillDt%>&formPullDown=<%=formPullDown%>">
		<%//}
		}
		else if (formDispLocation.equals("SP"))
		{


			if (! StringUtil.isEmpty(schevent) ) // if answered for CRF
			{
				if (!calledFromForm.equals("dashboard")){
				%>
				<script>
				  if (window.opener != undefined)
				    {
						window.opener.location.reload();
					}
				</script>
				<%
				}

				promptForMarkDoneFlag = eventcrf.getEventMarkDoneFlagWhenAllCRFsFilled(EJBUtil.stringToNum(schevent));


				if (! StringUtil.isEmpty(ignoreRights))
				{
					if (ignoreRights.equals("true"))
					{
						promptForMarkDoneFlag = 0;
					}

				}

				if (promptForMarkDoneFlag == 1)
				{
			   %>
			   <script>
			   	 self.setTimeout('window.open("promptMarkEventDone.jsp?pkey=<%=patientId%>&fk_sch_eventid=<%=schevent%>","win","width=500,height=370")', 1999);
  				 </script>

			   <%
			   } // if prompt required
			}



			if ((! StringUtil.isEmpty(ignoreRights)) && ignoreRights.equals("true") && isEditForm.equals("true"))
			{
				if (selfLogout.equals("0"))
					{
				%>
				<META HTTP-EQUIV=Refresh CONTENT="2; URL=patientHome.jsp">
			  <%
			  		}
			  	else
			  	{
			  		%>
			  		<META HTTP-EQUIV=Refresh CONTENT="2; URL=patientlogout.jsp">

			  		<%
			  	}
			}
			else
			{

			if (selfLogout.equals("0"))
			  {
				if ( (! StringUtil.isEmpty(ignoreRights)) && ignoreRights.equals("true") && (entryChar.equals("E") || (! StringUtil.isEmpty(schevent) && ppCRF.equals("true") ) ) )
					{
						%>
							<script>
							  if (window.opener != undefined)
							  {
								window.opener.location.reload();
								self.close();
							  }
							</script>
						<%
					}
					else
					{
						if (calledFromForm.equals("dashboard")){
						%>
							<META HTTP-EQUIV=Refresh CONTENT="2; URL=patstudyformdetails.jsp?srcmenu=null&calledFromForm=dashboard&formPullDown=<%=formPullDown%>&selectedTab=null&formId=<%=formId%>&formLibVer=<%=formLibVer%>&mode=M&studyId=<%=studyId%>&pkey=<%=patientId%>&formDispLocation=SP&filledFormId=<%=formFilledFormId%>&patProtId=<%=patProtId%>&calledFrom=S&statDesc=null&statid=null&entryChar=<%=entryChar%>&schevent=<%=schevent%>&fk_sch_events1=<%=schevent%>&fkcrf=null&formFillDt=ALL&responseId=&parentId=&fkStorageForSpecimen=&showPanel=true&submissionType=&formCategory=">
						<%
						}else{
						%>
							<META HTTP-EQUIV=Refresh CONTENT="2; URL=formfilledstdpatbrowser.jsp?calledFromForm=<%=calledFromForm%>&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&pkey=<%=patientId%>&mode=M&patProtId=<%=patProtId%>&studyId=<%=studyId%>&calledFrom=S&statDesc=<%=statDesc%>&statid=<%=statid%>&patientCode=<%=patientCode%>&formFillDt=<%=formFillDt%>&formPullDown=<%=formPullDown%>&fk_sch_events1=<%=schevent%>&responseId=<%=responseId%>&parentId=<%=parentId%>&fkStorageForSpecimen=<%=fkStorageForSpecimen%>&formDispType=<%=linkedType%>">
						<%
						}
				}//for META IFF

			  }//for self logout
			  else
			  {
			  		%>
		  				<script>
		  				  if (window.opener != undefined)
		  				  {
							window.opener.location = "patientlogout.jsp";
							self.close();
						  }
						</script>

			  		<%

			  }
			}

		}

		///////////////////////////////////anu

	//KM-#4016
	ArrayList pkQueryStats = new ArrayList();
	pkQueryStats = (ArrayList)tSession.getAttribute("pkQueryStats");

	ArrayList pkQueryStat = new ArrayList();	

	if (pkQueryStats !=null){

	for(int x =0; x<pkQueryStats.size(); x++)
		pkQueryStat.add(pkQueryStats.get(x)+ "");

	}
	if (ret>=0) {//out.println("formFilledFormId::"+formFilledFormId);

	//tSession.setAttribute("filledFormId",new Integer(ret).toString());
	if (override_count>0)
	//if (formFillMode.equals("N"))
	formQuery.insertIntoFormQuery(ret, Integer.parseInt(formId), from,1,
	(( String [] ) reasonList.toArray( new String [ reasonList.size() ] )),
	(( String [] ) fldSysIds.toArray( new String [ fldSysIds.size() ] )) ,
	(( String [] ) fldValidation.toArray( new String [ fldValidation.size() ] )), 
	(( String [] ) statusList.toArray( new String [ statusList.size() ] )), 
	(( String [] ) commentList.toArray( new String [ commentList.size() ] )),
	(( String [] ) queryModeList.toArray( new String [ queryModeList.size() ] )),
	(( String [] ) pkQueryStat.toArray( new String [ pkQueryStat.size() ] )), EJBUtil.stringToNum(usr), ipAdd);

	%>

	<input type=hidden name="filledFormId" value=<%=ret%>>
	 <input type=hidden name="calledFrom" value=<%=formDispLocation%>>
	 <input type=hidden name="formFillMode" value=<%=formFillMode%>>
	 <input type=hidden name="formDispLocation" value=<%=formDispLocation%>>
	    <input type=hidden name="showPanel" value=<%=showPanel%>>
	    <input type="hidden" name="submissionType" value=<%=submissionType%>>
		<input type="hidden" name="formCategory" value=<%=formCategory%>>




		</Form><%	}//ret >0
			 %>
		<%



	 /////////////////////////////////////anu

    	}//end of if old esign
	else{
%>
		<jsp:include page="incorrectesign.jsp" flush="true"/>
<%
		}//end of else of incorrect of esign
		}//end of else of incorrect userName
     }//end of if body for session

	else
	{
%>
	  <jsp:include page="timeout.html" flush="true"/>
<%

} //end of else of the Session


%>

<%	if(cnt > 0){%>

<%}%>

</body>

</html>

