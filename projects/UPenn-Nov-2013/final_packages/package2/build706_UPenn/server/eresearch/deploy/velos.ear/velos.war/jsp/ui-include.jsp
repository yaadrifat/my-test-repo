<!----KLUDGE: This file need to be removed.All includes should go conditionally as needed. No use importing everything on a page
  There are lot of occrence of this file includes in our hsp pages. Lack of time kept this file here---->
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language = "java" import="com.velos.eres.web.grpRights.GrpRightsJB"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<link type="text/css" rel="stylesheet" href="js/yui/build/datatable/assets/skins/sam/datatable.css">
<link rel="stylesheet" type="text/css" href="js/yui/build/container/assets/skins/sam/container.css"> 
<link rel="stylesheet" type="text/css" href="js/yui/build/calendar/assets/skins/sam/calendar.css">
<link rel="stylesheet" type="text/css" href="js/yui/build/menu/assets/skins/sam/menu.css"> 
<link type="text/css" rel="stylesheet" href="js/yui/build/logger/assets/skins/sam/logger.css">


<style type="text/css">
body {
overflow:scroll;
}
/* custom styles for this example */
.dt-page-nav {z-index:1000;margin-left:1em;
 } /* custom pagination UI */
.hide {display:none;}

.rec-dtl {
/*color:#444444;
position:absolute;
left:8px;
top:5px;*/
}
.d-tbl{
width:98%;
margin-left:0.25em;

} 
.d-btn {
background-position:center;
background-repeat:no-repeat;
background-color: #EBEBEB;
cursor:pointer;
height:16px;
padding:0pt;
white-space:nowrap;
width:16px;
font-size:12px;
}
.split {
background-image:url(../images/jpg/split.gif) ;
background-position:center;
background-repeat:no-repeat;
border:0pt none;
cursor:default;
display:block;
font-size:1px;
height:16px;
margin:0pt 2px;
overflow:hidden;
width:4px;
}
.nav-first {
background-image:url(../images/jpg/nav-first.gif) ;
}
.nav-first-disable {
background-image:url(../images/jpg/nav-first-disable.gif) ;
}
.nav-last {
background-image:url(../images/jpg/LastPage.gif) ;         /*Akshi:Added for Bug#7929*/
}
.nav-last-disable {
background-image:url(../images/jpg/LastDisabledPage.gif) ; /*Akshi:Added for Bug#7929*/
}
.nav-next {
background-image:url(../images/jpg/nav-next.gif) ;
}
.nav-next-disable {
background-image:url(../images/jpg/nav-next-disable.gif) ;
}
.nav-prev {
background-image:url(../images/jpg/PreviousPage.gif) ;      /*Akshi:Added for Bug#7929*/
}
.nav-prev-disable {
background-image:url(../images/jpg/PreviousDisabledPage.gif) ;/*Akshi:Added for Bug#7929*/
}


</style>
<jsp:include page="MC.jsp"></jsp:include> <!-- Messages File -->


<!-- Dependencies -->
<!-- </script>
<script type="text/javascript" src="js/yui/build/dom/event-min.js"></script>
<script type="text/javascript" src="js/yui/build/dom/dom-debug.js"></script>-->
<script type="text/javascript" src="js/yui/build/event/event-min.js"></script>
<script type="text/javascript" src="js/yui/build/yahoo/yahoo-min.js"></script>
<script type="text/javascript" src="js/yui/build/yahoo-dom-event/yahoo-dom-event.js"></script>
<script type="text/javascript" src="js/yui/build/json/json-min.js"></script>
<script type="text/javascript" src="js/yui/build/element/element-min.js"></script>
<script type="text/javascript" src="js/yui/build/datasource/datasource-min.js"></script>
<script type="text/javascript" src="js/yui/build/dragdrop/dragdrop-min.js"></script>
<script type="text/javascript" src="js/yui/build/container/container-min.js"></script> 
<script type="text/javascript" src="js/yui/build/menu/menu-min.js"></script> 
<!-- OPTIONAL: Connection (enables XHR) -->
<script type="text/javascript" src="js/yui/build/connection/connection-min.js"></script>
<script type="text/javascript" src="js/yui/build/dragdrop/dragdrop-min.js"></script>
<!-- Source files -->
<script type="text/javascript" src="js/yui/build/datatable/datatable-debug.js"></script>
<script type="text/javascript" src="js/yui/build/selector/selector-min.js"></script> 
<SCRIPT type="text/javascript" src="js/velos/dataview.js"></SCRIPT>
<script type="text/javascript" src="js/velos/specimen.js"></script>
<script type="text/javascript" src="js/velos/ui-include.js"></script>
<script type="text/javascript" src="js/velos/datagrid.js"></script>
<script type="text/javascript" src="js/velos/calMilestoneSetup.js"></script>
<script type="text/javascript" src="js/velos/coverage.js"></script>
<script type="text/javascript" src="js/velos/subjectgrid.js"></script>
<script type="text/javascript" src="js/velos/yuiUtil.js"></script>
<script type="text/javascript" src="js/velos/milestonegrid.js"></script>
<script type="text/javascript" src="js/velos/multiSpecimens.js"></script>
<script type="text/javascript" src="js/yui/build/element/element-min.js"></script>
<script type="text/javascript" src="js/yui/build/button/button-min.js"></script>
<script type="text/javascript" src="js/velos/manageVisitsGrid.js"></script>
<script type="text/javascript" src="js/velos/generateInvoiceGrid.js"></script>
<script type="text/javascript" src="js/velos/gadgetPatSchedulesGrid.js"></script>
<script type="text/javascript" src="js/yui/build/logger/logger-min.js"></script>


<%
//KM-5788 and 5789
HttpSession tSession = request.getSession(true);
int pageRight = 0;
if (sessionmaint.isValidSession(tSession))
{
	
	GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");

	if (grpRights != null){
		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MSPEC"));
	}
}
%>
<form name="uiinclude" method="post">
<input  type ="hidden" name = "pgright" value =<%=pageRight%>>
</form>




