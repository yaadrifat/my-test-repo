<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title>Start Import</title>

<%@ page import="com.velos.eres.service.util.*,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,com.velos.impex.*,java.text.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<head>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

</head>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import = "java.util.*,com.velos.eres.business.common.*"%>

<body>
<br>
<DIV id="div1"> 

<%
	HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession))
	{

%>	
		<jsp:include page="sessionlogging.jsp" flush="true"/>
 <%   
		String eSign= request.getParameter("eSign");
		
    	String oldESign = (String) tSession.getValue("eSign");
		String param = "";
		String paramVal = "";
		String paramName = "";
		
		String accId ;
		String userId;
		Impex impex = new Impex();
		String reqFilePath = "";
		int reqId = -1;

	   if(oldESign.equals(eSign)) 
	   {
		ImpexRequest expReq = new ImpexRequest();
			
					
		accId = request.getParameter("impAccountId");
		userId = request.getParameter("impUserId");
		reqFilePath = request.getParameter("expfile");

		System.out.println("reqFilePath " + reqFilePath );
		
		reqFilePath = "D:\\work\\eResearch_v62\\a2a\\eresearch.req" ;
//		
		//make a hashtable of parameters 
		
		Enumeration paramNames = request.getParameterNames();
		
		Hashtable htParam = new Hashtable();
		
		while (paramNames.hasMoreElements())
			{
	   			param = (String) paramNames.nextElement();
				
				if ((! param.equals("y")) && (! param.equals("x")) && (! param.equals("eSign")))
				{
					paramVal = request.getParameter(param);
					
					if ( ! htParam.containsKey(param))
					{
						htParam.put(param, paramVal);
					}
				}	
			
			}


	
		expReq.setReqStatus("0"); //export not started
		expReq.setReqBy(userId);

 
		 //set datetime
		 
		Calendar now = Calendar.getInstance();
		String dt = "";
		dt = "" + now.get(now.DAY_OF_MONTH) + now.get(now.MONTH) + (now.get(now.YEAR) - 1900) ;
		Calendar calToday=  Calendar.getInstance();
		String format="yyyy-MM-dd HH:mm:ss";
		SimpleDateFormat todayFormat=new SimpleDateFormat(format);
		dt = todayFormat.format(calToday.getTime());
		
		expReq.setReqDateTime(dt); 
		 
		
		impex.setRequestType("I");		
		impex.setImportPath(reqFilePath);

		impex.setImpexRequest(expReq);
		//set Import Params
		
		impex.setImportParam(htParam);

		reqId = impex.recordImportRequest();
		
		
		impex.start();
		
		%>
		<BR><BR><BR><BR>
		<P align = "center" class="defComments">
			
		We have recorded your request. 
		<BR> Your request Id is : <font color="red"><b><%=reqId%></b></font> . Please use this Id to track your import request.
		<BR> Please wait while we take you to the 'Import Logs' page.
		
		</P>
		  <META HTTP-EQUIV=Refresh CONTENT="2; URL=viewimpstatus.jsp">
		
		<%			
		
    	}//end of if old esign
	else{  
%>
		<jsp:include page="incorrectesign.jsp" flush="true"/>
<%	  
		}//end of else of incorrect of esign
     }//end of if body for session 	
	
	else
	{
%>
	  <jsp:include page="timeout.html" flush="true"/>
<%
	
} //end of else of the Session 
 

%>
</div>
</body>

</html>

