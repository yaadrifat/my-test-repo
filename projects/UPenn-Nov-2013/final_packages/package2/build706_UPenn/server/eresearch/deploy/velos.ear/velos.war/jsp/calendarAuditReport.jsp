<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<%
String budgtStatus1 = request.getParameter("budgetstatus1");
String budgtStatus2 = request.getParameter("budgetstatus2");
String OptionVal = request.getParameter("optionvalue");
String line;
boolean flag=true;
boolean firstTime=true;

%>

<SCRIPT LANGUAGE="JavaScript" src="js\velos\date.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="js\velos\dateformatSetting.js"></SCRIPT>
<script type="text/javascript">
function validates(formobj)
{
	var radioButtons = document.getElementsByName("filterType");
    for (var x = 0; x < radioButtons.length; x ++)
	{
      if (radioButtons[x].checked) 
		{
        var radiovalue = radioButtons[x].value;
		}		
	}	
	var BudgetStatus1 = document.forms[0].elements["dcalCodeStatus"][0].value;
	var BudgetStatus2 = document.forms[0].elements["dcalCodeStatus"][1].value;
	var OptVal = document.getElementById("optionvalue").value;
	
	if (BudgetStatus1 == "" )
		{
		alert("<%=MC.M_PlsSel_CalStat%>");/*alert("Please Select Calendar Status");*****/
		document.forms[0].elements["dcalCodeStatus"][0].focus();
		return false;
		}
	else if (BudgetStatus2 == "")
		{
		alert("<%=MC.M_PlsSel_CalStat%>");/*alert("Please Select calendar Status");*****/
		document.forms[0].elements["dcalCodeStatus"][1].focus();
		return false;
		}
	else if (OptVal == "" )
		{
		alert("<%=MC.M_PlsSel_ApprVal%>");/*alert("Please Select appropriate Value 'And/Or'");*****/	
		document.getElementById("optionvalue").focus();
		return false;
		}
	else if  ((radiovalue == 4 && (document.getElementById("dateFrom").value == "" || document.getElementById("dateTo").value == "" ))
	||(radiovalue == 3 && (document.getElementById("year1").value == "" || document.getElementById("month").value == "" ))
	||(radiovalue == 2 && document.getElementById("year").value == ""))
		{
		alert("<%=MC.M_PlsEtr_VldDtRange%>");/*alert("Please Select Valid Date Range");*****/
		return false;
		}	
		
	else if  ((radiovalue == 4 && (document.getElementById("dateFrom").value > document.getElementById("dateTo").value ))
	)
		{
		alert("<%=MC.M_DtFromLessThan_DtTo%>");/*alert("Date from should be less than date To.");*****/
		return false;
		}	
	return true;
}
function RetriveData()
{


	if (validates(document.reports)==true)
{
	var radioButtons = document.getElementsByName("filterType");
    for (var x = 0; x < radioButtons.length; x ++)
	{
      if (radioButtons[x].checked) 
		{
        var radiovalue = radioButtons[x].value;
		}
	}

var stat1 = document.forms[0].elements["dcalCodeStatus"][0];
var statustext1 = stat1.options[stat1.selectedIndex].text;
var stat2 = document.forms[0].elements["dcalCodeStatus"][1];
var statustext2 = stat2.options[stat2.selectedIndex].text;
var optionval = document.getElementById("optionvalue").value
var statustext = statustext1+' <%=LC.L_And %> '+statustext2+'        ('+optionval+') ';/*var statustext = statustext1+' And '+statustext2+'        ('+optionval+') ';*****/
 
var datefilter = document.getElementById("range").value;
datefilter = datefilter.replace(/:/g," ");
var chbxval= "";
var chbxvalue = document.getElementById("checkbox").checked;
if (chbxvalue == true)
{
chbxval = "ON";
}
else if (chbxvalue == false)
{
chbxval = "OFF";
}
	


win = window.open("calendarAuditReport.jsp?protId="+document.getElementById("protID").value+"&repId="+document.getElementById("ReportID").value+"&repName="+document.getElementById("RepName").value+"&calledFrom="+document.getElementById("calledFrom").value+"&budgetstatus1="+document.forms[0].elements["dcalCodeStatus"][0].value+"&budgetstatus2="+document.forms[0].elements["dcalCodeStatus"][1].value+"&budgetstatustext="+statustext+"&optionvalue="+document.getElementById("optionvalue").value+"&year="+document.getElementById("year").value+"&month="+document.getElementById("month").value+"&year1="+document.getElementById("year1").value+"&dateFrom="+document.getElementById("dateFrom").value+"&dateTo="+document.getElementById("dateTo").value+"&datefilter="+datefilter+"&filterType="+radiovalue+"&checkboxvalue="+chbxval+"&OpenSource=Self",'_self','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');
}
}

function fOpenWindow(type,section,frmobj){
	width=300;
	height= 300;
	if (type == "Y"){
		width=200;
		height=70;
	}

	if (type == "D"){
		width=520;
		height=320;
	}
	if (type == "M"){
		width=335;
		height=70;
	}
	frmobj.year.value ="";
	frmobj.year1.value ="";
	frmobj.month.value ="";
	frmobj.dateFrom.value ="";
	frmobj.dateTo.value ="";

	frmobj.range.value = "";
	if (type == 'A'){
		frmobj.range.value = "[<%=LC.L_All%>]"/*frmobj.range.value = "[All]"*****/
	}
	if (type != 'A'){

		windowName=window.open("selectDateRange.jsp?parm="+type +"&section=" +section,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=" +width +",height="+height)
		windowName.focus();
		
	}
}

function DefaultFilter()
{
var filterVal=4;
	document.getElementsByName("filterType")[3].checked=true;

	var DateTo = formatDate(new Date(),calDateFormat);
var dateFrom=new Date();
dateFrom.setDate(dateFrom.getDate() - 7)
var Datefrom1 = formatDate(dateFrom,calDateFormat);

document.getElementById("dateFrom").value = Datefrom1;
document.getElementById("dateTo").value = DateTo;
document.getElementById("range").value="<%=LC.L_From%>: "+Datefrom1+" <%=LC.L_To%>: "+DateTo;/*document.getElementById("range").value="From: "+Datefrom1+" To: "+DateTo;*****/

/*var FullURL = ""+window.location;

    if (FullURL.indexOf("OpenSource") > 0)
	{
		return;
	}
 
	var filterVal=4;
	document.getElementsByName("filterType")[3].checked=true;
	var dateFrom=new Date();
	dateFrom.setDate(dateFrom.getDate() - 7)
	
	var dateTo = new Date();	
	var monthF = dateFrom.getMonth() + 1;
    if (monthF<10) {monthF = "0"+monthF;}	
	var dayF = dateFrom.getDate();	
	if (dayF<10) {dayF = "0"+dayF;}
	var yearF = dateFrom.getFullYear();
	var monthT = dateTo.getMonth() + 1;	
	if (monthT<10) {monthT = "0"+monthT;}
	var dayT = dateTo.getDate();
	if (dayT<10) {dayT = "0"+dayT;}
	var yearT = dateTo.getFullYear();
	var dateFrom=monthF + "/" + dayF + "/" + yearF;	
	var dateTo=monthT + "/" + dayT + "/" + yearT;	

	document.getElementById("dateFrom").value = dateFrom;
	document.getElementById("dateTo").value = dateTo;
	document.getElementById("range").value="<%=LC.L_From%>: "+dateFrom+" <%=LC.L_To%>: "+dateTo;/*document.getElementById("range").value="From: "+dateFrom+" To: "+dateTo;*****/
}

</script>
</head>
<jsp:useBean id="schcodedao" scope="request" class="com.velos.esch.business.common.SchCodeDao"/>
<% 
/*Code to fetch values in dropdown for Calendar Status*/
String calledFrom  = request.getParameter("calledFrom");
String defaultStatus ="";
String schCodeStDD = "";
if (calledFrom.equals("S")) 
{
 defaultStatus = String.valueOf(schcodedao.getCodeValues("calStatStd"));
 schCodeStDD = schcodedao.toPullDown("dcalCodeStatus",EJBUtil.stringToNum(defaultStatus));
}
else{
defaultStatus = String.valueOf(schcodedao.getCodeValues("calStatLib"));
schCodeStDD = schcodedao.toPullDown("dcalCodeStatus",EJBUtil.stringToNum(defaultStatus));
}
%>

<Form Name="reports" method="post"action = "updateReasonModule.jsp">
<body onload="DefaultFilter();">
		<table width = "1296px" >  
		
		<tr> 	
			<td><%=MC.M_CalStat_Btwn %><!-- Calendar Status Between *****--><FONT class=Mandatory> * </FONT> </td>
			</td><td><div id="showBgtStatus1"><%= schCodeStDD%></div></td><td>  <%=LC.L_And %><!-- And *****-->&nbsp;</td><td> <div id="showBgtStatus2"><%= schCodeStDD%></div>
			</td> 
			<td>
				<select id="optionvalue" name="optionvalue">
				<option value=""><%=LC.L_Select_AnOption %><!-- Select an Option *****--></option>
				<option value="And"><%=LC.L_And %><!-- And *****--></option>
				<option value="Or"><%=LC.L_Or %><!-- Or *****--></option>				

				</select>				
			</td>
         	<td>
			<Input type="radio" id="filterType" name="filterType" value="1" onClick="fOpenWindow('A','1',document.reports)"> <%=LC.L_All %><!-- All *****-->
         	<Input type="radio" id="filterType" name="filterType" value="2" onClick="fOpenWindow('Y','1',document.reports)"> <%=LC.L_Year %><!-- Year *****-->
         	<Input type="radio" id="filterType" name="filterType" value="3" onClick="fOpenWindow('M','1',document.reports)"> <%=LC.L_Month %><!-- Month *****-->
         	<Input type="radio" id="filterType" name="filterType" value="4" onClick="fOpenWindow('D','1',document.reports)"> <%=LC.L_Date_Range %><!-- Date Range *****-->
         	<BR><Font class=comments><%=LC.L_Selected_DateFilter %><!-- Selected Date Filter *****-->:</Font>
			<FONT class=Mandatory> * </FONT> 
         	<input type=text  id ="range" name=range size= "30%" READONLY>
         	</td>
			<td><input type="checkbox" id="checkbox" name="checkbox" ></input>  &nbsp; <%=LC.L_DispDet_View %><!-- Display Detailed View *****--> </td>
			<td><input type="button" value="Search" onclick="RetriveData();"/></td>
		</tr>
		<tr>
			
			<input type="hidden"  id ="filterType" name="filterType" value="<%=request.getParameter("filterType")%>">
            <input type="hidden"  id ="year" name="year" value="<%=request.getParameter("year")%>">
			<input type="hidden" id="month" name="month" value="<%=request.getParameter("month")%>">
			<input type="hidden" id="year1" name="year1" value="<%=request.getParameter("year1")%>">
			<input type="hidden" id="dateFrom" name="dateFrom" value="<%=request.getParameter("dateFrom")%>">
			<input type="hidden" id="dateTo" name="dateTo" value="<%=request.getParameter("dateTo")%>" >
			<input type="hidden" id="ReportID" name="ReportID" value="<%=request.getParameter("repId")%>">
	<!--		<input type="hidden" id="BudgetPk" name="BudgetPk" value="<%=request.getParameter("budgetPk")%>">-->
			<input type="hidden" id="RepName" name="RepName" value="<%=request.getParameter("repName")%>">
			<input type="hidden" id="protID" name="protID" value="<%=request.getParameter("protId")%>">
			<input type="hidden" id="budTempate" name="budTempate" value="<%=request.getParameter("budgetTemplate")%>">
			<input type="hidden" name="budstatus1" id="budstatus1" value="<%=request.getParameter("budgetstatus1")%>">
			<input type="hidden" name="budstatus2" id="budstatus2" value="<%=request.getParameter("budgetstatus2")%>">
			<input type="hidden" name="optionvalue1" id= "optionvalue1" value="<%=request.getParameter("optionvalue")%>">
			<input type="hidden" name="checkbox1" id= "checkbox1" value="<%=request.getParameter("checkboxvalue")%>">
			<input type="hidden"  id ="calledFrom" name="calledFrom" value="<%=request.getParameter("calledFrom")%>">
		</tr>
	</table>

	<br/>
<%
String val = request.getParameter("OpenSource");

if ((val != null) && (val.equals("Self")))
{
	String LeftValue1 = null, RightValue1 = null, AppendText1 = " selected", budgetStatusDD1 = null;
	LeftValue1 = schCodeStDD.substring(0, (schCodeStDD.indexOf(budgtStatus1)+budgtStatus1.length()));
	RightValue1 = schCodeStDD.substring((schCodeStDD.indexOf(budgtStatus1)+budgtStatus1.length()), (schCodeStDD.length()));
	budgetStatusDD1 = LeftValue1 + AppendText1 + RightValue1;
	
	String LeftValue2 = null, RightValue2 = null, AppendText2 = " selected", budgetStatusDD2 = null;
	LeftValue2 = schCodeStDD.substring(0, (schCodeStDD.indexOf(budgtStatus2)+budgtStatus2.length()));
	RightValue2 = schCodeStDD.substring((schCodeStDD.indexOf(budgtStatus2)+budgtStatus2.length()), (schCodeStDD.length()));
	budgetStatusDD2 = LeftValue2 + AppendText2 + RightValue2;
	
	if(budgtStatus1.length()<=0){budgetStatusDD1=schCodeStDD;}
	if(budgtStatus2.length()<=0){budgetStatusDD2=schCodeStDD;}


%>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript" SRC="whichcss_skin.js"></SCRIPT><!--xtra-->
<jsp:useBean id="repB" scope="request" class="com.velos.eres.web.report.ReportJBNew"/>
<jsp:useBean id="userB" scope="request" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>


<jsp:useBean id="codeLst" scope="session" class="com.velos.eres.web.codelst.CodelstJB"/>
<script language="Javascript">

document.getElementById("showBgtStatus1").innerHTML="<%=budgetStatusDD1%>";
document.getElementById("showBgtStatus2").innerHTML="<%=budgetStatusDD2%>";
document.getElementById("optionvalue").value="<%=request.getParameter("optionvalue")%>";
if (document.getElementById("checkbox1").value == 'ON')
{
document.getElementById("checkbox").checked=true;
}
else{document.getElementById("checkbox").checked= false;}
var filterVal="<%=request.getParameter("filterType")%>";
if(filterVal ==1)
{
document.getElementById("range").value="[<%=LC.L_All %>]";/*document.getElementById("range").value="[All]";*****/
document.getElementsByName("filterType")[0].checked=true;
}
if(filterVal ==2)
{
document.getElementById("range").value="<%=LC.L_Year %>: "+"<%=request.getParameter("year")%>";
document.getElementsByName("filterType")[1].checked=true;
}
if(filterVal==3)
{
document.getElementById("range").value="<%=LC.L_Month %>: "+"<%=request.getParameter("month")%>"+" <%=LC.L_Year %>: "+"<%=request.getParameter("year1")%>";
document.getElementsByName("filterType")[2].checked=true;
}
if(filterVal==4)
{
document.getElementById("range").value="<%=LC.L_From %>: "+"<%=request.getParameter("dateFrom")%>"+" <%=LC.L_To %>: "+"<%=request.getParameter("dateTo")%>";
document.getElementsByName("filterType")[3].checked=true;
}
</script>
<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,javax.xml.transform.TransformerFactory,javax.xml.transform.Transformer,javax.xml.transform.Source,javax.xml.transform.stream.StreamSource,javax.xml.transform.stream.StreamResult,javax.xml.transform.TransformerException,javax.xml.transform.TransformerConfigurationException,java.io.*,java.text.*,java.net.URL,com.velos.eres.web.user.UserJB,com.velos.eres.web.grpRights.*"%>
<%
HashMap hm=new HashMap(5); 
	String repDate="";
	String repTitle="&nbsp;";
	
	String repArgs = "";
	String format = "";

	String repName=request.getParameter("repName");
	
	String protId=request.getParameter("protId");
	//Rohit - SW-FIN3C
	/*SM-Commented, as not reqd anymore as all custom labels will be replaced in one step below  
	//String coverage_type_label=VelosResourceBundle.getLabelString("Evt_CoverageType");
	*/
	
	int repId = EJBUtil.stringToNum(request.getParameter("repId"));
	String repArgsDisplay = "";
	int repXslId = 0;

	/*Custom label processing*/
	int startIndx =0;
	int stopIndx =0;
	String labelKeyword ="";
	String labelString ="";
	String messageKeyword ="";
	String messageString ="";
	String interimXSL = "";
	String messageKey ="";
	String tobeReplaced ="";
	String messageParaKeyword []=null;


	//only added for report 107
	String showSelect = request.getParameter("showSelect");

	if (StringUtil.isEmpty(showSelect))
	{
		showSelect = "0";
	}


	String params="";

	String hdr_file_name="";
	String ftr_file_name="";
	String filePath="";
	String hdrfile="";
	String ftrfile="";
	String hdrFilePath="";
	String ftrFilePath="";
	String fileName="";
	String xml=null;

	HttpSession tSession = request.getSession(true);


   if (sessionmaint.isValidSession(tSession))
   {
	   try{
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>

<%
//-----------------------------------------------------------------------
String accSkinId = ""; 
	String usersSkinId = "";
	String userSkin = "";
	String accSkin = "";
	String skin = "default";
	accSkinId = (String) tSession.getValue("accSkin");
	UserJB userB1 = (UserJB) tSession.getValue("currentUser");
	int userId1=userB1.getUserId();
	usersSkinId = userB1.getUserSkin();

	userSkin= codeLst.getCodeCustomCol(EJBUtil.stringToNum(usersSkinId) );
	accSkin= codeLst.getCodeCustomCol(EJBUtil.stringToNum(accSkinId) );
	if (accSkin == null || accSkin.equals("") || accSkin.equals("default") ){
		accSkin = "accSkin";
	}
	else{
		skin = accSkin;
	}
	if (userSkin != null && !userSkin.equals("") ){
		skin = userSkin;

	}

	%>
	<input type="hidden" id="sessUserId" name="sessUserId" value="<%=userId1%>">
	<%

//--------------------------------custom wrk 07-02-2012
String monthString = null;
	String filterType = request.getParameter("filterType");
	String dtFrom=request.getParameter("dateFrom");
	String dtTo=request.getParameter("dateTo");
	String month = request.getParameter("month");
	String year = null;
	
	if(filterType == null) filterType = "";	

	if(filterType.equals("2")) { //Year
		year = request.getParameter("year");
		if (!(year.equals(""))) {
 			
			dtFrom = DateUtil.getFormattedDateString(year,"01","01");
			dtTo = DateUtil.getFormattedDateString(year,"31","12");
		}
	}
	if(filterType.equals("3")) { //Month
		year = request.getParameter("year1");
		if (!(year.equals(""))) {
 			dtFrom = DateUtil.getFormattedDateString(year,"01",month);
		}
		dtTo = DateUtil.getFormattedDateString(year,String.valueOf(DateUtil.getMonthMaxNumberOfDays(month,year)),month);
	}
	if(filterType.equals("1")) { //All Dates
 		dtFrom = DateUtil.getFromDateStringForNullDate();
		dtTo = DateUtil.getToDateStringForNullDate();
	}
	if(dtFrom == null) dtFrom = "";
	dtFrom = dtFrom.trim();			
	if (!(dtFrom.equals(""))) {
		if(filterType.equals("1")) { //All Dates	
			repArgsDisplay = LC.L_All/*"ALL"*****/;	
		}
		else {
			repArgsDisplay = dtFrom + " "+LC.L_To+/*To*****/" " + dtTo ;			
		}

		repArgs= dtFrom + ":" + dtTo;
	}

//----------------------custom end 



	String uName =(String) tSession.getValue("userName");
	int acc = EJBUtil.stringToNum((String) (tSession.getValue("accountId")));
	String accountId= (String) (tSession.getValue("accountId"));
	int userId = EJBUtil.stringToNum((String) (tSession.getValue("userId")));
	userB = (UserJB)tSession.getValue("currentUser");
	String FilledFormId = "";

	int pageRight = 0;
	GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");
    pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("REPORTS"));



	repXslId = repId;
	params = protId;
	
	if (repId == 176)
		{
			params = protId +":" + request.getParameter("budgetstatus1")+":" +request.getParameter("budgetstatus2")+":" +request.getParameter("budgetstatustext")+":" +request.getParameter("optionvalue")+":"+dtFrom + ":" + dtTo+":" +request.getParameter("datefilter")+":" +request.getParameter("checkboxvalue");
		} 
	

	Calendar now = Calendar.getInstance();
	
	repDate = DateUtil.getCurrentDate() ;

	
	ReportDaoNew rD =new ReportDaoNew();

	//The parameters are separated by :
	//They are segregated in the stored procedure SP_GENXML and then passed to report sql
	//System.out.println("**************" + params);


	rD=repB.getRepXml(repId,acc,params);


	ArrayList repXml = rD.getRepXml();
	if (repXml == null || repXml.size() == 0) { //no data found
%>
		<br><br><br><br><br><br><br><br><br>
		<P class="sectionHeadings">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%Object[] arguments = {repName};%><%=VelosResourceBundle.getMessageString("M_NoDataFnd_Rpt",arguments)%><%--No data found for the Report '<%=repName%>'*****--%>.</P>
<%		throw new Exception(VelosResourceBundle.getMessageString("M_NoDataFnd_Rpt",arguments));
	}
//System.out.println("SONIKA "+params);


	rD=repB.getRepXsl(repXslId);

	ArrayList repXsl = rD.getXsls();



	Object temp;
	
	String xsl=null;


	temp=repXml.get(0);

	if (!(temp == null))
	{
		xml=temp.toString();
		//replace the encoded characters
		xml = StringUtil.replace(xml,"&amp;#","&#");

	}
	else
	{
		out.println(MC.M_ErrIn_GetXml);/*out.println("Error in getting XML");*****/
		Rlog.fatal("Report","ERROR in getting XML");
		throw new Exception(MC.M_ErrIn_GetXml);
	}

    try{
		temp=repXsl.get(0);
	} catch (Exception e) {
		out.println(MC.M_ErrIn_GetXsl);/*out.println("Error in getting XSL.");*****/
		Rlog.fatal("Report","ERROR in getting XSL. Record for report " +repId +"does not exist in database.");
		throw new Exception(MC.M_ErrIn_GetXsl);
	}

	if (!(temp == null))	{
		xsl=temp.toString();
	}
	else
	{
		out.println(MC.M_ErrIn_GetXsl);/*out.println("Error in getting XSL.");*****/
		Rlog.fatal("Report","ERROR in getting XSL. Record for report " +repId +"does not exist in database.");
		throw new Exception(MC.M_ErrIn_GetXsl);
	}

	if ((xsl.length())==0) {
		out.println(MC.M_ErrIn_GetXsl);/*out.println("Error in getting XSL.");*****/
		Rlog.fatal("Report","ERROR in getting XSL. Empty XSL found in database for report " +repId);
		throw new Exception(MC.M_ErrIn_GetXsl);
	}

	if ((xml.length()) ==17) { //no data found
%>

		<br><br><br><br><br><br><br><br><br>
		<P class="sectionHeadings">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%Object[] arguments = {repName};%><%=VelosResourceBundle.getMessageString("M_NoDataFnd_Rpt",arguments)%><%--No data found for the Report '<%=repName%>'*****--%>.</P>
<%		throw new Exception(VelosResourceBundle.getMessageString("M_NoDataFnd_Rpt",arguments));
	}


	//get hdr and ftr
	//Changed By Deepali
	rD=repB.getRepHdFtr(repId,acc,userId);

	byte[] hdrByteArray=rD.getHdrFile();
	byte[] ftrByteArray=rD.getFtrFile();
	String hdrflag, ftrflag;
	hdr_file_name="temph["+ now.get(now.DAY_OF_MONTH) + now.get(Calendar.MONTH) + (now.get(now.YEAR) - 1900)+ now.get(now.HOUR_OF_DAY)+ now.get(now.MINUTE)+ now.get(now.SECOND) + "].jpg";
	Configuration.readReportParam(Configuration.ERES_HOME + "eresearch.xml");
	filePath = Configuration.REPTEMPFILEPATH;

	hdrfile=filePath+ "/" + hdr_file_name;
	ftr_file_name="tempf["+ now.get(now.DAY_OF_MONTH) + now.get(Calendar.MONTH) + (now.get(now.YEAR) - 1900)+ now.get(now.HOUR_OF_DAY)+ now.get(now.MINUTE)+ now.get(now.SECOND) + "].jpg";
	ftrfile=filePath+ "/" + ftr_file_name;

	hdrFilePath="../temp/"+hdr_file_name;
	ftrFilePath="../temp/"+ftr_file_name;
//out.println(params);
	//check for byte array
	if (!(hdrByteArray ==null))
	{
		hdrflag="1";
		ByteArrayInputStream fin=new ByteArrayInputStream(hdrByteArray);

		BufferedInputStream fbin=new BufferedInputStream(fin);

		File fo=new File(hdrfile);

		FileOutputStream fout = new FileOutputStream(fo);
		Rlog.debug("3","after output stream");
		int c ;
		while ((c = fbin.read()) != -1){
				fout.write(c);
			}
		fbin.close();
		fout.close();
		}
	else
	{
		hdrflag="0";
	}


		//check for length of byte array
	if (!(ftrByteArray ==null))
		{
		ftrflag="1";
		ByteArrayInputStream fin1=new ByteArrayInputStream(ftrByteArray);
		BufferedInputStream fbin1=new BufferedInputStream(fin1);
		File fo1=new File(ftrfile);
		FileOutputStream fout1 = new FileOutputStream(fo1);
		int c1 ;
		while ((c1 = fbin1.read()) != -1){

			fout1.write(c1);
		}

		fbin1.close();
		fout1.close();
	}
	else
	{
		ftrflag="0";
	}


	//get the folder name
	Configuration.readAppendixParam(Configuration.ERES_HOME + "eresearch.xml");
	com.aithent.file.uploadDownload.Configuration.readSettings("eres");
	com.aithent.file.uploadDownload.Configuration.readUploadDownloadParam(com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD + "fileUploadDownload.xml", "study");
	String fileDnPath = com.aithent.file.uploadDownload.Configuration.DOWNLOADSERVLET ;

	Configuration.readReportParam(Configuration.ERES_HOME + "eresearch.xml");
	filePath = Configuration.REPDWNLDPATH;
	Rlog.debug("report", filePath);
	//make the file name
	fileName="reporthtml"+"["+ now.get(now.DAY_OF_MONTH) + now.get(Calendar.MONTH) + (now.get(now.YEAR) - 1900)+ now.get(now.HOUR_OF_DAY)+ now.get(now.MINUTE)+ now.get(now.SECOND) + "].html" ;
	//make the complete file name
	String htmlFile = filePath + "/"+fileName;
	response.setContentType("text/html");
	
	interimXSL = xsl;
	
	try{
		while(interimXSL.contains("VELLABEL[")){
			startIndx = interimXSL.indexOf("VELLABEL[") + "VELLABEL[".length();
			stopIndx = interimXSL.indexOf("]",startIndx);
			labelKeyword = interimXSL.substring(startIndx, stopIndx);
			labelString = LC.getLabelByKey(labelKeyword);
			interimXSL=StringUtil.replaceAll(interimXSL, "VELLABEL["+labelKeyword+"]", labelString);
		}
		while(interimXSL.contains("VELMESSGE[")){
			startIndx = interimXSL.indexOf("VELMESSGE[") + "VELMESSGE[".length();
			stopIndx = interimXSL.indexOf("]",startIndx);
			messageKeyword = interimXSL.substring(startIndx, stopIndx);
			messageString = MC.getMessageByKey(messageKeyword);
			interimXSL=StringUtil.replaceAll(interimXSL, "VELMESSGE["+messageKeyword+"]", messageString);
		}
		while(interimXSL.contains("VELPARAMESSGE[")){
			startIndx = interimXSL.indexOf("VELPARAMESSGE[") + "VELPARAMESSGE[".length();
			stopIndx = interimXSL.indexOf("]",startIndx);
			messageKeyword = interimXSL.substring(startIndx, stopIndx);
			messageKey=messageKeyword.substring(0, messageKeyword.indexOf("||"));
			messageKey=messageKey.trim();
			messageParaKeyword=messageKeyword.substring(messageKeyword.indexOf("||")+2).split(java.util.regex.Pattern.quote("||"));
			messageString = VelosResourceBundle.getMessageString(messageKey,messageParaKeyword);
			interimXSL=StringUtil.replaceAll(interimXSL, "VELPARAMESSGE["+messageKeyword+"]", messageString);
		}
		if(!interimXSL.contains("word.GIF")){
			if(interimXSL.contains(LC.L_Word_Format)){
				tobeReplaced = "<img border=\"0\" title=\""+LC.L_Word_Format+"\" alt=\""+LC.L_Word_Format+"\" src=\"./images/word.GIF\"></img>";
				interimXSL=StringUtil.replace(interimXSL, LC.L_Word_Format, tobeReplaced);
			}
			if(interimXSL.contains(LC.L_Excel_Format)){
				tobeReplaced = "<img border=\"0\" title=\""+LC.L_Excel_Format+"\" alt=\""+LC.L_Excel_Format+"\" src=\"./images/excel.GIF\"></img>";
				interimXSL=StringUtil.replace(interimXSL,LC.L_Excel_Format, tobeReplaced);
			}
			if(interimXSL.contains(LC.L_Printer_FriendlyFormat)){
				tobeReplaced = "<img border=\"0\" src=\"./images/printer.gif\" title=\""+LC.L_Printer_FriendlyFormat+"\" alt=\""+LC.L_Printer_FriendlyFormat+"\"></img>";
				interimXSL=StringUtil.replace(interimXSL, LC.L_Printer_FriendlyFormat, tobeReplaced);
			}
		}
		xsl = interimXSL;
	}catch (Exception e)
	{
  		out.write("Error in replacing label string: " + e.getMessage());
 	}

	 try
    {

		//first save the output in html file
		TransformerFactory tFactory1 = TransformerFactory.newInstance();
		Reader mR1=new StringReader(xml);
		Reader sR1=new StringReader(xsl);
		Source xmlSource1=new StreamSource(mR1);
		Source xslSource1 = new StreamSource(sR1);

 		Transformer transformer1 = tFactory1.newTransformer(xslSource1);
		//Set the params
		transformer1.setParameter("hdrFileName", hdrFilePath);
		transformer1.setParameter("ftrFileName", ftrFilePath);
		transformer1.setParameter("repTitle",repTitle);
		transformer1.setParameter("repName",repName);
		transformer1.setParameter("repBy",uName);
		transformer1.setParameter("repDate",repDate);
		transformer1.setParameter("argsStr",repArgsDisplay);
		transformer1.setParameter("cond","F");
		transformer1.setParameter("wd","");
		transformer1.setParameter("xd","");
		transformer1.setParameter("hd", "");
		transformer1.setParameter("hdrflag", hdrflag);
		transformer1.setParameter("ftrflag", ftrflag);
		transformer1.setParameter("dl", "");
		transformer1.setOutputProperty("encoding", "ISO-8859-1");

		// Perform the transformation, sending the output to html file
/*SV, 8/26, transformer didn't seem to close the stream properly that, next jsp (repGetExcel.jsp) kept getting 0(zero) bytes for file length,
	even though the file existed and was openable outside the application. So, fix is to, open and close file outside of transformer.

	  	transformer1.transform(xmlSource1, new StreamResult(htmlFile));
*/
		FileOutputStream fhtml=new FileOutputStream(htmlFile);
	  	transformer1.transform(xmlSource1, new StreamResult(fhtml));
	  	fhtml.close();
//--------------------------------------------------------custom work for format

String dummyFile="/dummyBudFile"+"["+ now.get(now.DAY_OF_MONTH) + now.get(Calendar.MONTH) + (now.get(now.YEAR) - 1900)+ now.get(now.HOUR_OF_DAY)+ now.get(now.MINUTE)+ now.get(now.SECOND) + "].html";

String dummyBudFile=filePath+ dummyFile;
		
		
		//Custom Work Begin
		
			FileInputStream fstream = new FileInputStream(htmlFile);
			// Get the object of DataInputStream
            DataInputStream in = new DataInputStream(fstream);
            BufferedReader input = new BufferedReader(new InputStreamReader(in));  
		
			BufferedWriter output=new BufferedWriter(new OutputStreamWriter(new FileOutputStream(dummyBudFile)));
			//BufferedReader input = new BufferedReader(new FileReader(htmlFile));
      
			 
				String myLine="";
				boolean myFlag=false;
			 while((line=input.readLine())!=null)
            {     
               if(line.contains("<SCRIPT LANGUAGE=\"JavaScript\" src=\"validations.js\"></SCRIPT><script>"))
                {
                    flag=false;
                }
               if(line.contains("<TABLE id=\"headerTable\""))
               {
                   flag=true;
               }
			   if(line.contains("<TABLE id=\"budheaderTable\""))
               {
                   flag=true;
               }
			   if(line.contains("<TABLE BORDER=\"1\" id=\"newTable\""))
               {
                   flag=false;
               }
			   if(line.contains("<TABLE id=\"resultTable\" BORDER=\"1\" WIDTH=\"100%\">"))
                {
                    flag=true;
                }
               if(line.contains("<table bgcolor=\"#cccccc\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"98%\">"))
                {
                    flag=false;
                }
			   if(flag==true)
				{
					if(line.contains("<TD ALIGN=\"CENTER\" WIDTH=\"5%\" class=\"reportData\"><input id=\"chbx\" type=\"checkbox\"></TD>"))
                    {
                        line=line.replace("<TD ALIGN=\"CENTER\" WIDTH=\"5%\" class=\"reportData\"><input id=\"chbx\" type=\"checkbox\"></TD>","");
                    }
					
                    if(line.contains("<TH WIDTH=\"5%\" ALIGN=\"CENTER\" class=\"reportHeading\"></TH>"))
                    {
                        line=line.replace("<TH WIDTH=\"5%\" ALIGN=\"CENTER\" class=\"reportHeading\"></TH>","");
                    }
					if(line.contains("<TH style=\"display:none;\" WIDTH=\"7%\" ALIGN=\"CENTER\" class=\"reportHeading\"></TH>"))
                    {
                        line=line.replace("<TH style=\"display:none;\" WIDTH=\"7%\" ALIGN=\"CENTER\" class=\"reportHeading\"></TH>","");
                    }
					
					if(line.contains("<TD style=\"display:none;\" WIDTH=\"10%\" class=\"reportData\"></TD>"))
                    {
                        line=line.replace("<TD style=\"display:none;\" WIDTH=\"10%\" class=\"reportData\"></TD>","");
                    }
					
					if(line.contains("<Textarea onKeyDown=\"limitText(this,4000);\" onKeyUp=\"limitText(this,4000);\" WIDTH=\"10%\" id=\"result_comment\" name=\"remarks\">"))
					{
						line=line.replace("<Textarea onKeyDown=\"limitText(this,4000);\" onKeyUp=\"limitText(this,4000);\" WIDTH=\"10%\" id=\"result_comment\" name=\"remarks\">", "");
                        line=line.replace("</Textarea>","");
					}	
					
					Integer keyValue=new Integer(-1);
					
					if(line.contains("<TD "))
					{
						if(line.contains("&nbsp;"))
						{
							line=line.replace("&nbsp;","");
							myLine=myLine+line;
							myFlag=true;
						}
						else
						{
							if(myFlag=true)
							{
								line=myLine+line;
								myFlag=false;
								myLine="";
							}
							if(line.contains("<TD ALIGN=\"CENTER\" WIDTH=\"10%\" class=\"reportData\">"))
							{	
								if(line.contains("<TD name=\"reasonDd\" ALIGN=\"CENTER\" class=\"reportData\">"))
								{
									if(firstTime==true)
									{
									
									
										int loc=line.indexOf("<TD name=\"reasonDd\" ALIGN=\"CENTER\" class=\"reportData\">");
										
										String subinput=line.substring(loc,line.indexOf("</TD>", loc));
												
										subinput=subinput.substring(subinput.indexOf("\">")+2);								
										
										StringTokenizer st=new StringTokenizer(subinput,";");
										
										while(st.hasMoreTokens())
										{
										
										String s=st.nextToken();
										Integer key=new Integer(s.substring(0,s.indexOf("|")));
										String value=s.substring(s.indexOf("|")+1);
										if(value.equals("Select an Option"))
										{
											value=" ";
										}
										hm.put(key, value);
										
										}
										
									}
									firstTime=false;
									if(line.contains("<input name=\"reason_value\" type=\"hidden\""))
									{
										int subloc=line.indexOf("<input name=\"reason_value\" type=\"hidden\"");
										int subend=line.indexOf(">",subloc);     
										String sub=line.substring(subloc,subend);
										int subv=sub.indexOf("value=");
										keyValue=new Integer(sub.substring(subv+7,sub.indexOf("\"",subv+7)));
									}
									String reasonValue=hm.get(keyValue)+""; 
									int reasonloc=line.indexOf("<TD name=\"reasonDd\" ALIGN=\"CENTER\" class=\"reportData\">");
									String reasonStr=line.substring(reasonloc,line.indexOf("</TD>",reasonloc));
									line=line.replace(reasonStr,"<TD name=\"reasonDd\" ALIGN=\"CENTER\" class=\"reportData\">"+reasonValue);								
								}
							}
							if(line.contains("<input name=\"reason_value\" type=\"hidden\""))
							{
								int xloc=line.indexOf("<input name=\"reason_value\" type=\"hidden\"");
								String replaceline=line.substring(xloc,line.indexOf("></TD>",xloc)+6);
								line=line.replace(replaceline,"");
							}
							output.write(line+"\n");
						}	
					}
					else
					{
						output.write(line+"\n");
					}
				}
			}
			input.close();
			output.close();
			htmlFile=dummyBudFile;
//--------------------------------------------------------custom ends
		//now send it to console
	  	TransformerFactory tFactory = TransformerFactory.newInstance();
		Reader mR=new StringReader(xml);
		Reader sR=new StringReader(xsl);
		Source xmlSource=new StreamSource(mR);
		Source xslSource = new StreamSource(sR);
		// Generate the transformer.
			Transformer transformer = tFactory.newTransformer(xslSource);

		String wordLink = "repGetWord.jsp?htmlFile=" + htmlFile +"&fileDnPath="+fileDnPath+"&filePath="+filePath+"&repId="+repId+"&calledfrom="+LC.L_Calendar;
		String excelLink = "repGetExcel.jsp?htmlFile=" + htmlFile +"&fileDnPath="+fileDnPath+"&filePath="+filePath+"&repId="+repId+"&calledfrom="+LC.L_Calendar;
		//String printLink = "repGetHtml.jsp?htmlFileName=" + fileName +"&fileDnPath="+fileDnPath+"&filePath="+filePath;
		String printLink = "repGetHtml.jsp?htmlFileName=" + dummyFile +"&fileDnPath="+fileDnPath+"&filePath="+filePath+"&repId="+repId+"&calledfrom="+LC.L_Calendar; /*custom*/
		
		if (pageRight < 6)
		{
			String strRightMsg = "javascript:alert(&quot;"+MC.M_NoRgtTo_DldRpt+"&quot;);";/*String strRightMsg = "javascript:alert(&quot;You do not have access rights to download the report&quot;);";*****/
			wordLink = strRightMsg;
			excelLink = strRightMsg;
			printLink = strRightMsg;
		}



		//Set the param for header and footer
		transformer.setParameter("hdrFileName", hdrFilePath);
		transformer.setParameter("ftrFileName", ftrFilePath);
		transformer.setParameter("repTitle",repTitle);
		transformer.setParameter("repName",repName);
		transformer.setParameter("repBy",uName);
		transformer.setParameter("repDate",repDate);
		transformer.setParameter("argsStr",repArgsDisplay);
		transformer.setParameter("cond","T");
		transformer.setParameter("wd",wordLink);
		transformer.setParameter("xd",excelLink);
		transformer.setParameter("hd",printLink);
		transformer.setParameter("hdrflag", hdrflag);
		transformer.setParameter("ftrflag", ftrflag);
		transformer.setParameter("dl", fileDnPath);
		transformer.setParameter("studyApndxParam", "?tableName=ER_STUDYAPNDX&columnName=STUDYAPNDX_FILEOBJ&pkColumnName=PK_STUDYAPNDX&module=study&db=eres&pkValue=");
		transformer.setOutputProperty("encoding", "ISO-8859-1");

		//only for report 107, mock schedule
		
		//KM-24Jul08
		/*if (repId == 107)
		{
		 	transformer.setParameter("showSelect", showSelect);
		 	transformer.setParameter("cond","F");

		}*/

		// Perform the transformation, sending the output to the response.
      	transformer.transform(xmlSource, new StreamResult(out));

    }
	catch (Exception e)
	{
  		out.write(e.getMessage());
 	}

%>
<script> whichcss_skin("<%=skin%>");</script>
<%
	   }catch(Exception exp){
		   Rlog.error("Calendar Audit Report",exp.getMessage());
		   return;
	   }finally{
	String callFrom =LC.L_Calendar;
	if(EJBUtil.isEmpty(calledFrom)){
		callFrom = LC.L_Rpt_Central;
	}
	int downloadFlag=0;
	String downloadFormat = null;
	if(fileName.indexOf("xls")!=-1){
		downloadFlag = 1;
		downloadFormat = LC.L_Excel_Format;
	}else if(fileName.indexOf("doc")!=-1){
		downloadFlag = 1;
		downloadFormat = LC.L_Word_Format;
	}
%>
	<jsp:include page="userReportLogging.jsp" flush="true">
		<jsp:param value="<%=repId %>" 			name="repId" />
		<jsp:param value="<%=fileName%>" 		name="fileName" />
		<jsp:param value="<%=filePath %>" 		name="filePath" />
		<jsp:param value="<%=callFrom%>" 		name="moduleName" />
		<jsp:param value="<%=xml%>" 			name="repXml" />
		<jsp:param value="<%=downloadFlag%>" 	name="downloadFlag" />
		<jsp:param value="<%=downloadFormat%>" 	name="downloadFormat" />
	</jsp:include>
<%}
} //end of if session times out

else

{

%>

<jsp:include page="timeout_childwindow.jsp" flush="true"/>


<%

}
}

else 
{
%>

<%
}
%>

</form>
</html>