<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
String src = request.getParameter("srcmenu");
String selectedTab = request.getParameter("selectedTab");
String mode = "";
mode = request.getParameter("mode");
String includeMode = request.getParameter("includeMode");

String patCode=request.getParameter("patCode");

patCode = (StringUtil.isEmpty(patCode))? "" : patCode;
String stid= request.getParameter("studyId");
int iStudyId = StringUtil.stringToNum(stid);
%>
<%if (!"Y".equals(includeMode)){%>
	<%int requestPatientId = 0;
	if (mode.equals("M")) {
		requestPatientId = StringUtil.stringToNum(request.getParameter("pkey"));
	}
	%>
	<meta http-equiv="Refresh" content="0; URL=/velos/jsp/patientEnrollScreen.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&mode=<%=mode%>&pkey=<%=requestPatientId%>&page=patient&patCode=<%=patCode%>&studyId=<%=iStudyId%>&includeMode=Y">
	<%
	if (requestPatientId >= 0){
		return;
	}
	%>
<%}%>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<%@ page import="org.json.*,com.velos.remoteservice.demographics.*,com.velos.eres.service.util.*,com.velos.eres.web.user.ConfigFacade,com.velos.eres.web.user.ConfigObject, com.velos.eres.web.user.ConfigDetailsObject,com.velos.eres.business.common.CommonDAO"%>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ page language = "java" import = "com.velos.eres.service.util.StringUtil,com.velos.eres.business.common.*,com.velos.esch.business.common.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB, com.velos.eres.web.user.UserJB"%>

<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="commonB" scope="page" class="com.velos.eres.web.common.CommonJB" />
<jsp:useBean id="userBFromSession" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="siteB" scope="request" class="com.velos.eres.web.site.SiteJB"/>
<jsp:useBean id="accB" scope="request" class="com.velos.eres.web.account.AccountJB"/>
<jsp:useBean id="userSiteB" scope="page" class="com.velos.eres.web.userSite.UserSiteJB" />
<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id ="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>


<% boolean hasAccess = false;
String page1 = request.getParameter("page");
page1 = "quick";
String lastName="";
String firstName="";
String specialName="";

String accCode="";

   HttpSession tSession = request.getSession(true);
	String wsEnabled="";
   if (sessionmaint.isValidSession(tSession))	{
	String uName = (String) tSession.getAttribute("userName");
        String userIdFromSession = (String) tSession.getAttribute("userId");

	userBFromSession = (UserJB) tSession.getAttribute("currentUser");
	String acc = (String) tSession.getAttribute("accountId");

	accB.setAccId(StringUtil.stringToNum(acc));
	accB.getAccountDetails();


	//Added by Manimaran for the Enh.#GL7.
	String autoGenPatient = accB.getAutoGenPatient();
	if (autoGenPatient == null)
		autoGenPatient ="";

	%>
	<script>
	patientDetailsQuickFunctions.autoGenPatient = "<%=autoGenPatient%>";
	</script>
	<%

	wsEnabled=accB.getWSEnabled();
	wsEnabled=(wsEnabled==null)?"":wsEnabled;
	if (wsEnabled.length()==0) wsEnabled="N";

	accCode=accB.getAccName();
	accCode=(accCode==null)?"":accCode;


	CodeDao  cdOther = new CodeDao();
	int otherDthCause = cdOther.getCodeId("pat_dth_cause","other");

	CodeDao cdperdth = new CodeDao();
	int deadPerStat = cdperdth.getCodeId("patient_status","dead");

	String pageMode= request.getParameter("pageMode");

	if(pageMode == null)
		pageMode = "initial";

		//modifed by Gopu for MAHI Enahncement on 24th Feb 2005
		//Check for page customized mandatory field exists for the page by calling
		//the method populateObject([AccountId], [module/page]


		ConfigFacade cFacade=ConfigFacade.getConfigFacade();
		HashMap hashPgCustFld = new HashMap();



		ConfigDetailsObject cdoPgField = cFacade.populateObject(Integer.parseInt(acc), "patient");
		//if customized fields exists then put the field name and its value in hashmap
		//with key as the field name and value is the position at which the values can be referred to


		if (cdoPgField.getPcfField()!= null && cdoPgField.getPcfField().size() > 0) {
			for (int i=0;i<cdoPgField.getPcfField().size();i++){
				hashPgCustFld.put((String)cdoPgField.getPcfField().get(i), String.valueOf(i));

		}
	}


	String studyId = null;
	String statDesc = null;
	String statid = null;
	String studyVer = null;


	String disableStr ="";
	String readOnlyStr ="";



	studyId = request.getParameter("studyId");

	statDesc = request.getParameter("statDesc");

	statid = request.getParameter("statid");

	studyVer = request.getParameter("studyVer");

	boolean withSelect = true;
	String dthCause ="";

	 CodeDao cd10 = new CodeDao();


  cd10.getCodeValues("pat_dth_cause");
 String dDthCause = "";




	int pageRight = 0;

	String parPage = "";
	int personPK = 0;
	String patientID = "";
	String organization = "";
	String patOrg = "";
	int orgRight = 0;
	String addEthnicityNames="";
	String addRaceNames="";
    String addEthnicityIds="";
    String addRaceIds="";

	 int usrGroup = 0;
	  boolean completeDet = false;
	  String inputType = "text";
	  String displayStar = "";
	  int patDataDetail = 0;

	personPK = StringUtil.stringToNum(request.getParameter("pkey"));

	//make study dropdown

	ArrayList arStudyId = new ArrayList();
	ArrayList arStudyNum = new ArrayList();

	StudyDao sd = studyB.getUserStudies(userIdFromSession, "dummy",0,"active");
	arStudyId = sd.getStudyIds();
	arStudyNum = sd.getStudyNumbers();

	//create study dropdown

	String strStudyId = "";


	if (!(personPK==0)) {
    		person.setPersonPKId(personPK);
	    	person.getPersonDetails();

    		patientID = person.getPersonPId();
	    	patientID = (patientID==null)?"":patientID;

    		organization =   person.getPersonLocation() ;
	    	organization = (organization==null)?"":organization;

		siteB.setSiteId(StringUtil.stringToNum(organization)) ;
		siteB.getSiteDetails();
		patOrg = siteB.getSiteName();

		//check whether the user has right to edit the patients of the patient org
        orgRight = userSiteB.getUserPatientFacilityRight(StringUtil.stringToNum(userIdFromSession),personPK);

	}

   	if(page1.equals("enrollPatientsearch")){
		patCode=request.getParameter("patCode");
	}

 	mode = request.getParameter("mode");


	usrGroup =StringUtil.stringToNum( userBFromSession.getUserGrpDefault());


	if (mode.equals("M"))	{


		patDataDetail = person.getPatientCompleteDetailsAccessRight( StringUtil.stringToNum(userIdFromSession),usrGroup,personPK );


        if(patDataDetail!=0)
        {
		if( patDataDetail >= 4 )
    		{

			completeDet = true;
			inputType = "text";
			displayStar = "";
		}
		else
		{

			completeDet = false;
			inputType = "hidden";
			displayStar = "<font color = blue>*</font>";
		}
                }

	}


	GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getAttribute("GRights");
	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MPATIENTS"));


 if ((mode.equals("M") && (pageRight >= 4 && orgRight >= 4)) || (mode.equals("N") && (pageRight == 5  || pageRight == 7 ) ))	{



  String special_id="";
  String fname = "";

  String lname = "";

  String aname = "";

  String dob = "";
  String gender = "";

  String ethnicity = "";
  String race = "";

  String deathDate = "";
  String regBy = "";
  String regByName = "";
  String status = "";
  String selstd = "";

  String dGender = "";

  String dEthnicity = "";
  String dRace = "";
  StringBuffer dSites = new StringBuffer();
  StringBuffer dSitesRestricted = new StringBuffer();
  String dStatus = "";
  String phyOther="";
  String orgOther="";
  String orgDD="";
  String selSpecialityIds="";
special_id=request.getParameter("specialityIds");

	String patientFacilityId  = "";

selSpecialityIds=special_id;

ArrayList siteIdsRestricted;
ArrayList siteNamesRestricted;
ArrayList siteAltIdsRestricted;

  CodeDao cd1 = new CodeDao();
  CodeDao cd4 = new CodeDao();
  CodeDao cd5 = new CodeDao();
  CodeDao cd8 = new CodeDao();

  cd1.getCodeValues("gender");
  cd4.getCodeValuesFilterCustom1("ethnicity","eth_prim");
  cd5.getCodeValuesFilterCustom1("race","race_prim");
  cd8.getCodeValues("patient_status");

   int deadStatPk = 0;

  deadStatPk = cd8.getCodeId("patient_status","dead");

  addEthnicityIds = person.getPersonAddEthnicity();

 	 if (addEthnicityIds==null) addEthnicityIds="";

    if(addEthnicityIds.equals(""))
	{
		addEthnicityNames="";
	}

	if (addEthnicityIds.length()>0) addEthnicityNames = cd8.getCodeLstNames(addEthnicityIds);
	if(addEthnicityNames.equals("error"))
	{
	  addEthnicityNames="";
	}


 	addRaceIds = person.getPersonAddRace();
	if (addRaceIds==null) addRaceIds="";

    if(addRaceIds.equals(""))
	{
		addRaceNames="";

	}

       if(special_id==null)
        {


            specialName="";

        }
        else if(special_id!=null)
	{
	if(special_id.length()>0){specialName=cd8.getCodeLstNames(special_id);
        if(specialName.equals("error"))
		{
			 specialName="";
		}
        }
	}
	if (addRaceIds.length()>0) addRaceNames = cd8.getCodeLstNames(addRaceIds);

      	if(addRaceNames.equals("error"))
	{
			addRaceNames="";
	}
  dthCause = person.getPatDthCause();





 //show sites for which the user has new right i.e right to add a patient
  UserSiteDao userSiteDao = userSiteB.getSitesWithNewRight(StringUtil.stringToNum(acc),StringUtil.stringToNum(userIdFromSession));



  ArrayList siteIds = userSiteDao.getUserSiteSiteIds();
  ArrayList siteDescs = userSiteDao.getUserSiteNames() ;


  String siteId = "";
  int len = siteIds.size();
  boolean exists = false;

  /**
   * Shows logged in user's primary organization, default selection

   */
		String userSiteId = "";
		int primSiteId = 0;
		int userId = 0;
		String siteName = "";

    userId = StringUtil.stringToNum(userIdFromSession);
	/*userB.setUserId(userId);
	userB.getUserDetails();*/
	userB=(com.velos.eres.web.user.UserJB)tSession.getAttribute("currentUser");

	userSiteId = userB.getUserSiteId();
	userSiteId=(userSiteId==null)?"":userSiteId;
	primSiteId= StringUtil.stringToNum(userSiteId);

	siteB.setSiteId(primSiteId);
	siteB.getSiteDetails();
	siteName = siteB.getSiteName();



	////KM
	String orgAtt ="";
	if (hashPgCustFld.containsKey("org")) {
		int fldNumOrg= Integer.parseInt((String)hashPgCustFld.get("org"));
		orgAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumOrg));
		if(orgAtt == null) orgAtt ="";
	}

	//int cnt =0 ;
	String onChange="onchange=\"asyncValidate()\"";

	if(!"0".equals(orgAtt)) {
	if(orgAtt.equals("1") || orgAtt.equals("2")) //KM-Disabling dropdown based on custom data.
	    dSites.append("<SELECT NAME=patorganization id='siteId' disabled class='readonly-input' "+ onChange +">") ;
	else
		dSites.append("<SELECT NAME=patorganization id='siteId' "+ onChange +">") ;


	for (int counter = 0; counter <= len-1 ; counter++) {
		siteId =  (String)siteIds.get(counter);
		if(userSiteId.equals(siteId)) {
			dSites.append("<OPTION value = "+ primSiteId +" selected>" + siteName+"</OPTION>");

		} else {
			dSites.append("<OPTION value = "+ siteId +">" + siteDescs.get(counter)+"</OPTION>");
		}
	}
	dSites.append("<OPTION value='' >"+LC.L_Select_AnOption/*Select an Option*****/+"</OPTION>");
	dSites.append("</SELECT>");

	if (wsEnabled.equals("Y"))
	{
		userSiteDao.getSitesWithNewRightRestricted(StringUtil.stringToNum(acc),StringUtil.stringToNum(userIdFromSession));
		siteIdsRestricted=userSiteDao.getUserSiteSiteIds();
		siteAltIdsRestricted=userSiteDao.getUserSiteAltIds();
		siteNamesRestricted=userSiteDao.getUserSiteNames();
		len=siteIdsRestricted.size();
		String onChangeRestricted="onchange=\"patientDetailsQuickFunctions.toggleOrg()\"";
		dSitesRestricted.append("<SELECT NAME=orgDD id='orgDD' "+ onChangeRestricted +">") ;
		dSitesRestricted.append("<OPTION value='' selected='selected'>"+LC.L_Select_AnOption/*Select an Option*****/+"</OPTION>");
		for (int counter = 0; counter <= len-1 ; counter++) {
			siteId =  (String)siteAltIdsRestricted.get(counter);

			dSitesRestricted.append("<OPTION value = "+ siteId +">" + siteNamesRestricted.get(counter)+"</OPTION>");

		}
		dSitesRestricted.append("<OPTION value='0' >"+LC.L_Other/*Other*****/+"</OPTION>");
		dSitesRestricted.append("</SELECT>");
		orgDD=dSitesRestricted.toString();
	}
  /*
  check if the patient organization is already in the dropdown
  for (int counter = 0; counter <= len-1 ; counter++) {
	siteId =  (String)siteIds.get(counter);
  	if (siteId.equals(organization)) {
		exists = true;
		break;
	}
  }*/


 /* dSites.append("<SELECT NAME=patorganization >") ;
	for (int counter = 0; counter <= len-1 ; counter++) {
		siteId =  (String)siteIds.get(counter);
		if (siteId.equals(organization)) {
			dSites.append("<OPTION value = "+ siteId +" selected>" + siteDescs.get(counter)+"</OPTION>");
		} else {
			dSites.append("<OPTION value = "+ siteId +">" + siteDescs.get(counter)+"</OPTION>");
		}
	}
	if (!(organization.equals(""))) {
		if (!(exists)) {
		    dSites.append("<OPTION value = "+ organization +" selected>" + patOrg+"</OPTION>");
		}
	} else {
		dSites.append("<OPTION value='' selected>Select an Option</OPTION>");
	}
	dSites.append("</SELECT>");*/
 	}
%>


<%
  if (mode.equals("M")) {

	   dob =  person.getPersonDob();
	   dob = (dob==null)?"":dob;

	   fname  =  person.getPersonFname();
	   fname = (fname==null)?"":fname;

	  gender  =  person.getPersonGender();
	  gender = (gender==null)?"":gender;

	 lname =  person.getPersonLname();
	 lname = (lname==null)?"":lname;

	 aname = person.getPersonAlias();
	 aname = (aname==null)?"":aname;

	 ethnicity =  person.getPersonEthnicity();
	 ethnicity = (ethnicity==null)?"":ethnicity;

	 race =  person.getPersonRace();
	 race = (race==null)?"":race;

	status =   person.getPersonStatus();
	status = (status==null)?"":status;

	deathDate = person.getPersonDeathDate();
	deathDate = (deathDate==null)?"":deathDate;


	regBy = person.getPersonRegBy();
	regBy = (regBy==null)?"":regBy;

	patientFacilityId = person.getPatientFacilityId();

	if (StringUtil.isEmpty(patientFacilityId ))
		patientFacilityId  = "";

	phyOther=person.getPhyOther();
	phyOther=(phyOther==null)?"":phyOther;
	orgOther=person.getOrgOther();
	orgOther=(orgOther==null)?"":orgOther;

	//get reg by name

        if(regBy!="")
        {

        userB.setUserId(StringUtil.stringToNum(regBy));
	userB.getUserDetails();
        lastName=userB.getUserLastName();
        firstName=userB.getUserFirstName();
        }






        if((lastName=="") && (firstName==""))
        {
        regByName = "";

        }

         else if((firstName!=null)&&(lastName==null))
        {
             regByName=firstName;

        }
        else if((lastName!=null)&&(firstName==null))
        {
            regByName=lastName;

        }
        else if((firstName!=null) && (lastName!=null))
        {

        regByName = userB.getUserLastName() + ", " + userB.getUserFirstName();

        }





	int userPrimOrg = StringUtil.stringToNum(userB.getUserSiteId());



	String enrollId = (String) tSession.getAttribute("enrollId");

page1="enrollPatientsearch";

	if(! page1.equals("enrollPatientsearch"))
	{
%>
	<jsp:include page="patienttabs.jsp" flush="true">
	<jsp:param name="pkey" value="<%=personPK%>"/>
	<jsp:param name="patientCode" value="<%=patientID%>"/>
	<jsp:param name="patProtId" value="<%=enrollId%>"/>
	<jsp:param name="studyId" value="<%=studyId%>"/>
	<jsp:param name="fromPage" value="demographics"/>
	<jsp:param name="page" value="<%=page1%>"/>
	</jsp:include>

<%
 }	//not from enrollpatient search
   }
   else{


   		completeDet = true; //all data is visible
		displayStar = ""; // no star will be visible
		//regBy = userIdFromSession;
		//regByName = uName;
   }

	if(pageMode.equals("final")){

	 	 dthCause = request.getParameter("dthCause");
		 patientID = request.getParameter("patID");

		 dob = request.getParameter("patdob");

		 fname = request.getParameter("patfname");

 		 lname = request.getParameter("patlname");
 		 aname = request.getParameter("aliasname");

		 gender = request.getParameter("patgender");
         ethnicity = request.getParameter("patethnicity");
		 race = request.getParameter("patrace");

		 status = request.getParameter("patstatus");
		 addEthnicityNames = request.getParameter("txtAddEthnicity");
		 addRaceNames = request.getParameter("txtAddRace");
		 regByName  = request.getParameter("patregbyname");

		 phyOther  = request.getParameter("phyOther");
		 deathDate = request.getParameter("patdeathdate");
		 selstd = request.getParameter("selstudyId");


		 }



//////////KM
	   String surStatAtt="";
	   String gendAtt ="";
	   String ethAtt ="";
	   String raceAtt ="";

	   if (hashPgCustFld.containsKey("survivestat")) {
		  int fldNumSurStat= Integer.parseInt((String)hashPgCustFld.get("survivestat"));
		  surStatAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumSurStat));
		  if(surStatAtt == null) surStatAtt ="";
	   }

	  if (hashPgCustFld.containsKey("gender")) {
			int fldNumGender= Integer.parseInt((String)hashPgCustFld.get("gender"));
			gendAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumGender));
			if(gendAtt == null) gendAtt ="";
	  }

	  if (hashPgCustFld.containsKey("ethnicity")) {
			int fldNumEth= Integer.parseInt((String)hashPgCustFld.get("ethnicity"));
			ethAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumEth));
			if(ethAtt == null) ethAtt ="";
	  }

	  if (hashPgCustFld.containsKey("race")) {
			int fldNumRace= Integer.parseInt((String)hashPgCustFld.get("race"));
            raceAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumRace));

			if(raceAtt == null) raceAtt ="";
	  }



	 if(pageMode.equals("final")){

		if(!"0".equals(gendAtt)) {
			if(gendAtt.equals("1"))
				dGender = cd1.toPullDown("patgender", StringUtil.stringToNum(gender),"disabled class='readonly-input'");
			else
				dGender = cd1.toPullDown("patgender", StringUtil.stringToNum(gender));
		}

		if(!"0".equals(ethAtt)) {
			if(ethAtt.equals("1"))
			    dEthnicity = cd4.toPullDown("patethnicity", StringUtil.stringToNum(ethnicity),"disabled class='readonly-input'");
			else
			    dEthnicity = cd4.toPullDown("patethnicity", StringUtil.stringToNum(ethnicity));
		}

		if(!"0".equals(raceAtt)) {
			if(raceAtt.equals("1"))
				dRace = cd5.toPullDown("patrace", StringUtil.stringToNum(race),"disabled class='readonly-input'");
			else
				dRace = cd5.toPullDown("patrace", StringUtil.stringToNum(race));
		}

		if(!"0".equals(surStatAtt)) {
			if(surStatAtt.equals("1"))
	  			dStatus = cd8.toPullDown("patstatus", StringUtil.stringToNum(status),"disabled class='readonly-input'");
		  	else
				dStatus = cd8.toPullDown("patstatus", StringUtil.stringToNum(status));
		}

	}
	else	{



		if(!"0".equals(gendAtt)) {
			//dGender = cd1.toPullDown("patgender");
	        if(gendAtt.equals("1"))
				dGender = cd1.toPullDown("patgender", StringUtil.stringToNum(gender),"disabled class='readonly-input'");
			else
				dGender = cd1.toPullDown("patgender", StringUtil.stringToNum(gender));
		}

		if(!"0".equals(ethAtt)) {
			if(ethAtt.equals("1"))
			    dEthnicity = cd4.toPullDown("patethnicity", StringUtil.stringToNum(ethnicity),"disabled class='readonly-input'");
			else
			    dEthnicity = cd4.toPullDown("patethnicity", StringUtil.stringToNum(ethnicity));
		}

		//dEthnicity = cd4.toPullDown("patethnicity");
		//dRace = cd5.toPullDown("patrace");

		if(!"0".equals(raceAtt)) {
			if(raceAtt.equals("1")) //KM
				dRace = cd5.toPullDown("patrace", StringUtil.stringToNum(race),"disabled class='readonly-input'");
			else
				dRace = cd5.toPullDown("patrace", StringUtil.stringToNum(race));
		}

		//dStatus = cd8.toPullDown("patstatus", StringUtil.stringToNum(status));
        //dStatus = cd8.toPullDown("patstatus");

		int aliveStatus = 0;
		CodeDao cd = new CodeDao();
		aliveStatus = cd.getCodeId("patient_status","A");

		if(!"0".equals(surStatAtt)) {
			if(surStatAtt.equals("1")) //KM
				dStatus = cd8.toPullDown("patstatus", aliveStatus,"disabled class='readonly-input'");
			else
				dStatus = cd8.toPullDown("patstatus", aliveStatus);
		}


	}
    selstd = request.getParameter("selstudyId");

	if(selstd == null || selstd.equals("") || selstd.equals("null"))
	selstd = "0";

	strStudyId = StringUtil.createPullDown("selstudyId", StringUtil.stringToNum(selstd), arStudyId , arStudyNum )	;

	//dDthCause = cd10.toPullDown("dthCause",StringUtil.stringToNum(dthCause),withSelect);



		CodeDao cdaodth = new CodeDao();
		cdaodth.getCodeValues("pat_dth_cause");
		ArrayList causeId = new ArrayList();
		causeId = cdaodth.getCId();

		ArrayList causeDesc = new ArrayList();
		causeDesc = cdaodth.getCDesc();
		int selDthCause = 0;
		StringBuffer strCause = new StringBuffer();

		Integer dthIdTemp = null;
		int dthIdSelTemp = 0;

		selDthCause = StringUtil.stringToNum(dthCause);



	  ////KM

       String codAtt =""; //Added by Manimaran to disable the dropdown based on custom value.
	   if (hashPgCustFld.containsKey("cod")) {

		int fldNumCod= Integer.parseInt((String)hashPgCustFld.get("cod"));
        codAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumCod));

		if(codAtt == null) codAtt ="";

     }

	 if(!"0".equals(codAtt)) {
		  if(codAtt.equals("1"))
	 	     strCause.append("<SELECT NAME='dthCause' disabled class='readonly-input'>") ;
		  else
			 strCause.append("<SELECT NAME='dthCause' >") ;
      ////

		  strCause.append("<OPTION value=''> "+LC.L_Select_AnOption/*Select an Option*****/+" </OPTION>") ;

			for (int dcounter = 0; dcounter  < causeId.size()  ; dcounter++){
				dthIdTemp = (Integer) causeId.get(dcounter);
				if (selDthCause == dthIdTemp.intValue())
				{
					strCause.append("<OPTION value = "+ causeId.get(dcounter)+" SELECTED  >" + causeDesc.get(dcounter)+ "</OPTION>");
				}
				else
				{
					strCause.append("<OPTION value = "+ causeId.get(dcounter)+">" + causeDesc.get(dcounter)+ "</OPTION>");
				}
	
			}
		strCause.append("</SELECT>");
	}
	dDthCause = strCause.toString();
%>
<input type="hidden" name="srcmenu" Value="<%=src%>">
<input type="hidden" name="pataccount" Value="<%=acc%>">
<input type="hidden" name="pkey" Value="<%=personPK%>">
<input type="hidden" name="page" Value="<%=page1%>">
<Input type="hidden" name="mode" value=<%=mode%>>
<Input type="hidden" name="patCode" value=<%=patCode%>>
<Input type="hidden" name="studyId" value=<%=studyId%>>
<Input type="hidden" name="statDesc" value=<%=statDesc%>>
<Input type="hidden" name="statid" value=<%=statid%>>
<Input type="hidden" name="studyVer" value=<%=studyVer%>>
<Input type="hidden" name="deadStatPk" value=<%=deadStatPk%>>
<input type="hidden" id="accountId" name="accountId" value="<%=acc%>">

<input type="hidden" id="patadd1" name="patadd1" size = 30  MAXLENGTH = 100>
<input type="hidden" id="patadd2" name="patadd2" size = 30  MAXLENGTH = 100>
<input type="hidden" id="patcity" name="patcity" size = 20 >
<input type="hidden" id="patstate" name="patstate" >
<input type="hidden" id="patcounty" name="patcounty" size = 20 >
<input type="hidden" id="patzip" name="patzip" >
<input type="hidden" id="patcountry" name="patcountry" size = 30 >
<input type="hidden" id="pathphone" name="pathphone" size = 30 >
<input type="hidden" id="patbphone" name="patbphone" size = 30 >
<input type="hidden" id="patemail" name="patemail" size = 30 >
<%// code modified by Gupta Maddala 102407 %>
<input type=hidden id="aliasname" name="aliasname" size = 20 MAXLENGTH = 20 value='<%=aname%>'>


<input type="hidden" name="selSpecialityIds" value="<%=selSpecialityIds%>">


<input type="hidden" name="selAddEthnicityIds" value="<%=addEthnicityIds%>">
<input type="hidden" name="selAddRaceIds" value="<%=addRaceIds%>">
<input type="hidden" name="deadPerStat" value="<%=deadPerStat%>">
<input type="hidden" name="pageMode" value="final">


<DIV id="studyDIV" class="studyInst comments" >
	<p class="sectionHeadings" id="studyNumberMessage"></p>
</DIV>
<table cellspacing="0" cellpadding="0" class="basetbl midAlign bgcolorFix" border="0">
	<tr>
		<td colspan="5">
			<p><%=LC.L_Patient_Details%><%--Patient Details*****--%></p>
		</td>
	</tr>
  <tr>
      <%

	if (hashPgCustFld.containsKey("patid")) {

		int fldNum= Integer.parseInt((String)hashPgCustFld.get("patid"));
		String patMand = ((String)cdoPgField.getPcfMandatory().get(fldNum));
		String patLable = ((String)cdoPgField.getPcfLabel().get(fldNum));
		String patAtt = ((String)cdoPgField.getPcfAttribute().get(fldNum));

		if(patAtt == null) patAtt ="";
		if(patMand == null) patMand ="";


		if(!patAtt.equals("0")) {
		%>
		<td>
			<%if(patLable !=null){%> 
				<%=patLable%>
			<%} else {%> 
				<%=LC.L_Patient_Id%><%--Patient ID*****--%>
			<%}

			if (patMand.equals("1")) {
			%>
			   <FONT class="Mandatory" id="pgcustompatid">*  </FONT>  
			  	<%if(autoGenPatient.equals("1")){ %>
				(<%=LC.L_SysHypenGen%><%--system-generated*****--%>)
				<%} 
			}
	
		    if(patAtt.equals("1")) {
			   disableStr = "disabled class='readonly-input'"; }
		    else if (patAtt.equals("2") ) {
			 	readOnlyStr = "readonly"; }
	  		 %>
    	</td>

	    <td colspan="5">
			<input type="text" name="patID" id="patid" size = 20 MAXLENGTH = 20 value='<%=patientID%>'   <%=disableStr%>  <%=readOnlyStr%> onblur="if (document.getElementById('siteId').value.length>0) {ajaxvalidate('patient:'+this.id,0,'ajaxPatIdMessage','','<%=MC.M_PatId_AldyExst%><%--This <%=LC.Pat_Patient%> ID already exists for the selected organization or it\'s  parent/child organizations.*****--%>','siteId')}">
			<% if (wsEnabled.equals("Y")){ %>
			 <%=orgDD %><A id="getPatientAnchor" href="javascript:void(0);" onClick=" return patientDetailsQuickFunctions.fetchData();"><%=LC.L_Get_Pat%><%--Get Patient*****--%></A>
			<%} %>
			<span id="ajaxPatIdMessage"></span>
	    </td>
		<%}
	} else {%>
  	<td> <%=LC.L_Patient_Id%><%--Patient ID*****--%> <FONT class="Mandatory" id="pgcustompatid">*  </FONT>  <%if(autoGenPatient.equals("1")){ %>
			(<%=LC.L_SysHypenGen%><%--system-generated*****--%>)
		<%}%>
	</td>
    <td colspan="5">
	<input type="text" name="patID" id="patid" size = 20 MAXLENGTH = 20 value='<%=patientID%>'   onblur="if (document.getElementById('siteId').value.length>0) {ajaxvalidate('patient:'+this.id,0,'ajaxPatIdMessage','','<%=MC.M_PatId_AldyExst%><%--This <%=LC.Pat_Patient%> ID already exists for the selected organization or it\'s  parent/child organizations.*****--%>','siteId')}">
	<% if (wsEnabled.equals("Y")){ %>
	 <%=orgDD %><A id="getPatientAnchor" href="javascript:void(0);" onClick=" return patientDetailsQuickFunctions.fetchData();"><%=LC.L_Get_Pat%><%--Get Patient*****--%></A>
	<%} %>
	<span id="ajaxPatIdMessage"></span>
    </td>

  <%}%>

  </tr>




  <tr>

	 <%if (hashPgCustFld.containsKey("fname")) {

			int fldNumFname= Integer.parseInt((String)hashPgCustFld.get("fname"));
			String fnameMand = ((String)cdoPgField.getPcfMandatory().get(fldNumFname));
			String fnameLable = ((String)cdoPgField.getPcfLabel().get(fldNumFname));
			String fnameAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumFname));

			disableStr ="";//KM-050208
			readOnlyStr ="";

			if(fnameAtt == null) fnameAtt ="";
			if(fnameMand == null) fnameMand ="";

			if(!fnameAtt.equals("0")) {
			if(fnameLable !=null){
			%>
			<td>
			<%=fnameLable%>
			<%} else {%> <td>
			<%=LC.L_First_Name_Patient%><%--First Name*****--%>
			<%}

			if (fnameMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatfname">* </FONT>
			<% }
		%>

	       <%if(fnameAtt.equals("1")) {
			 disableStr = "disabled class='readonly-input'"; }
		   else if (fnameAtt.equals("2") ) {
			 readOnlyStr = "readonly"; }
  		   %>

    </td>
	<td colspan="5">
	<input type=<%=inputType%> id="patfname" name="patfname" size = 20 MAXLENGTH = 30 value='<%=fname%>'  <%=disableStr%>  <%=readOnlyStr%> onblur="if (document.getElementById('siteId').value.length>0) {valChangeReturn=0;ajaxvalidate('patient~fldob:patfname',0,'fldobMessage','','<%=MC.M_PatWithSameNameDobAldy%><%--A <%=LC.Pat_Patient%> already exists with same First Name,Last Name and Date Of Birth.*****--%>','patfname:patlname:patdob:accountId:patid')}"><%=displayStar%>
    </td>
	<%}} else {%>
		<td> <%=LC.L_First_Name_Patient%><%--First Name*****--%></td>
	<td colspan="5">
	<input type=<%=inputType%> id="patfname" name="patfname" size = 20 MAXLENGTH = 30 value='<%=fname%>'  onblur="if (document.getElementById('siteId').value.length>0) {valChangeReturn=0;ajaxvalidate('patient~fldob:patfname',0,'fldobMessage','','<%=MC.M_PatWithSameNameDobAldy%><%--A <%=LC.Pat_Patient%> already exists with same First Name,Last Name and Date Of Birth.*****--%>','patfname:patlname:patdob:accountId:patid')}"><%=displayStar%>
    </td>


	<%}%>

    </tr>
  <tr>


	   <%if (hashPgCustFld.containsKey("lname")) {

			int fldNumLname= Integer.parseInt((String)hashPgCustFld.get("lname"));
			String lnameMand = ((String)cdoPgField.getPcfMandatory().get(fldNumLname));
			String lnameLable = ((String)cdoPgField.getPcfLabel().get(fldNumLname));
			String lnameAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumLname));


			disableStr ="";
			readOnlyStr ="";

			if(lnameAtt == null) lnameAtt ="";
			if(lnameMand == null) lnameMand ="";


			if(!lnameAtt.equals("0")) {
			if(lnameLable !=null){
			%>
			<td >

			<%=lnameLable%>
			<%} else {%> <td >
			<%=LC.L_Last_Name_Patient%><%--Last Name*****--%>
			<%}

			if (lnameMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatlname">* </FONT>
			<% }
		 %>

	       <%if(lnameAtt.equals("1")) {
			 disableStr = "disabled class='readonly-input'"; }
		   else if (lnameAtt.equals("2") ) {
			 readOnlyStr = "readonly"; }
		 %>



    </td>
    <td  colspan="5">
		<input type=<%=inputType%> id="patlname" name="patlname" size = 20 MAXLENGTH = 20  <%=disableStr%>  <%=readOnlyStr%>  value='<%=lname%>' onblur="if (document.getElementById('siteId').value.length>0) {valChangeReturn=0;ajaxvalidate('patient~fldob:patlname',0,'fldobMessage','','<%=MC.M_PatWithSameNameDobAldy%><%--A <%=LC.Pat_Patient%> already exists with same First Name,Last Name and Date Of Birth.*****--%>','patfname:patlname:patdob:accountId:patid')}"><%=displayStar%><span id="fldobMessage"></span>
    </td>
<%}} else {%>
	<td ><%=LC.L_Last_Name_Patient%><%--Last Name*****--%> </td>
    <td colspan="5">
		<input type=<%=inputType%> id="patlname" name="patlname" size = 20 MAXLENGTH = 20    value='<%=lname%>' onblur="if (document.getElementById('siteId').value.length>0) {valChangeReturn=0;ajaxvalidate('patient~fldob:patlname',0,'fldobMessage','','<%=MC.M_PatWithSameNameDobAldy%><%--A <%=LC.Pat_Patient%> already exists with same First Name,Last Name and Date Of Birth.*****--%>','patfname:patlname:patdob:accountId:patid')}"><%=displayStar%><span id="fldobMessage"></span>
    </td>


<%}%>

 </tr>

 <tr>

	   <%

		if (hashPgCustFld.containsKey("dob")) {
			
			int fldNum= Integer.parseInt((String)hashPgCustFld.get("dob"));
			String dobMand = ((String)cdoPgField.getPcfMandatory().get(fldNum));
			String dobLable = ((String)cdoPgField.getPcfLabel().get(fldNum));
			String dobAtt = ((String)cdoPgField.getPcfAttribute().get(fldNum));


			disableStr ="";
			readOnlyStr ="";
			if(dobAtt == null) dobAtt ="";
			if(dobMand == null) dobMand ="";


			if(!dobAtt.equals("0")) {
				if(dobLable !=null){
				%> <td >
				<%=dobLable%>
				<%} else {%> <td >
				<%=LC.L_Date_OfBirth%><%--Date of Birth*****--%>
				<%}
				if (dobMand.equals("1")) {%>
				   <FONT class="Mandatory" id="pgcustompatdob">* </FONT>
				<%} //KM-050208-3465

				if(dobAtt.equals("1")) {
					 disableStr = "disabled class='readonly-input'"; 
				} else if (dobAtt.equals("2") ) {
					 readOnlyStr = "readonly"; 
				}
			 %>
    </td>
    <td colspan="5">
	
<%-- INF-20084 Datepicker-- AGodara --%>
	<% if (completeDet){%>
		<%if (StringUtil.isEmpty(dobAtt)){ %>
			<input type="<%=inputType%>" name="patdob" id="patdob" class="datefield" size = 10 MAXLENGTH=11	value="<%=dob%>" onchange="valChangeReturn=0;ajaxvalidate('patient~fldob:patdob',0,'fldobMessage','','<%=MC.M_PatWithSameNameDobAldy%><%--A <%=LC.Pat_Patient%> already exists with same First Name,Last Name and Date Of Birth.*****--%>','patfname:patlname:patdob:accountId:patid')">
		<%} else if(dobAtt.equals("1") || dobAtt.equals("2")){%>
			<input type=<%=inputType%> id="patdob" name="patdob" <%=readOnlyStr%> <%=disableStr%> size = 10 MAXLENGTH = 11 value='<%=dob%>' onchange="valChangeReturn=0;ajaxvalidate('patient~fldob:patdob',0,'fldobMessage','','<%=MC.M_PatWithSameNameDobAldy%><%--A <%=LC.Pat_Patient%> already exists with same First Name,Last Name and Date Of Birth.*****--%>','patfname:patlname:patdob:accountId:patid')"><%=displayStar%>
		<%}%>
	<%} else{%>
		<input type=<%=inputType%> id="patdob" name="patdob" <%=readOnlyStr%> <%=disableStr%> size = 10 MAXLENGTH = 11 value='<%=dob%>' onchange="valChangeReturn=0;ajaxvalidate('patient~fldob:patdob',0,'fldobMessage','','<%=MC.M_PatWithSameNameDobAldy%><%--A <%=LC.Pat_Patient%> already exists with same First Name,Last Name and Date Of Birth.*****--%>','patfname:patlname:patdob:accountId:patid')"><%=displayStar%>	
	<%} %>
	</td>
  
  <%}
   } else {%>
	<td> <%=LC.L_Date_OfBirth%><%--Date of Birth*****--%>  <FONT class="Mandatory" id="pgcustompatdob">* </FONT>  </td>
    <td colspan="5">
		<input type=<%=inputType%> id="patdob" name="patdob" class="datefield" size = 10 MAXLENGTH = 11 value='<%=dob%>' onchange="valChangeReturn=0;ajaxvalidate('patient~fldob:patdob',0,'fldobMessage','','<%=MC.M_PatWithSameNameDobAldy%><%--A <%=LC.Pat_Patient%> already exists with same First Name,Last Name and Date Of Birth.*****--%>','patfname:patlname:patdob:accountId:patid')"><%=displayStar%>
	</td>


  <%}%>

 </tr>


   <tr>
   <%!String gendMand=null;%>


       <%
		   if (hashPgCustFld.containsKey("gender")) {

			int fldNumGender= Integer.parseInt((String)hashPgCustFld.get("gender"));
			//String
            gendMand = ((String)cdoPgField.getPcfMandatory().get(fldNumGender));
			String gendLable = ((String)cdoPgField.getPcfLabel().get(fldNumGender));
			gendAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumGender));


			if(gendAtt == null) gendAtt ="";
			if(gendMand == null) gendMand ="";


			if(!gendAtt.equals("0")) {
			if(gendLable !=null){
			%> <td >
			<%=gendLable%>
			<%} else {%> <td >
			<%=LC.L_Gender%><%--Gender*****--%>
			<%}

			if (gendMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatgender">* </FONT>
			<% }
		 %>

    </td>
    <td >
		<%=dGender%>  <%if (gendAtt.equals("1")) { %> <input type="hidden" name="patgender" value="">  <%}%><!--KM-->

    </td>
	<%} else if (gendAtt.equals("0")){

	%>
	<input type="hidden" name="patgender" value=""> <!--KM-->

	<%
	}
	 } else {%>
		<td><%=LC.L_Gender%><%--Gender*****--%> </td> <td> <%=dGender%>  </td>

	<%}%>
	<td></td><td></td><td></td>
	</tr>



     <tr>

       <%if (hashPgCustFld.containsKey("ethnicity")) {
			int fldNumEth= Integer.parseInt((String)hashPgCustFld.get("ethnicity"));
			String ethMand = ((String)cdoPgField.getPcfMandatory().get(fldNumEth));
			String ethLable = ((String)cdoPgField.getPcfLabel().get(fldNumEth));
			ethAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumEth));

			if(ethAtt == null) ethAtt ="";
			if(ethMand == null) ethMand ="";

			if(!ethAtt.equals("0")) {
			if(ethLable !=null){
			%><td width="15%">
			<%=ethLable%>
			<%} else {%> <td width="15%">
			<%=LC.L_Prim_Ethnicity%><%--Primary Ethnicity*****--%>
			<%}

			if (ethMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatethnicity">* </FONT>
			<% }
		 %>
    </td>
    <td width="34%" >
		<%=dEthnicity%> <%if(ethAtt.equals("1")) {%><input type="hidden" name="patethnicity" value=""> <%}%><!--KM-->
    </td>

	<%} else if(ethAtt.equals("0")) {%>

	<input type="hidden" name="patethnicity" value=""> <!--KM-->


	<%}} else {%>
	<td width="15%" > <%=LC.L_Prim_Ethnicity%><%--Primary Ethnicity*****--%> </td>
    <td width="34%">
		<%=dEthnicity%>
    </td>

	<% }%>




	 <%if (hashPgCustFld.containsKey("additional1")) {


			int fldNumAdd1= Integer.parseInt((String)hashPgCustFld.get("additional1"));
			String add1Mand = ((String)cdoPgField.getPcfMandatory().get(fldNumAdd1));
			String add1Lable = ((String)cdoPgField.getPcfLabel().get(fldNumAdd1));
			String add1Att = ((String)cdoPgField.getPcfAttribute().get(fldNumAdd1));


			disableStr ="";
			if(add1Att == null) add1Att ="";
			if(add1Mand == null) add1Mand ="";



			if(!add1Att.equals("0")) {
			if(add1Lable !=null){
			%><td width="15%">
			<%=add1Lable%>
			<%} else {%> <td width="15%">
			<%=LC.L_Additional%><%--Additional*****--%>
			<%}

			if (add1Mand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatadditional1">* </FONT>
			<% }
		 %>


		   <%if(add1Att.equals("1")) {
			 disableStr = "disabled class='readonly-input'"; }
		     //else if (add1Att.equals("2") ) {
			 //readOnlyStr = "readonly"; }
		 %>


	</td>
	<td width="35%">
		<input type="text" name="txtAddEthnicity" size = 30 MAXLENGTH = 500  <%=disableStr%> Readonly value='<%=addEthnicityNames%>'>
		<%if(!add1Att.equals("1") &&  !add1Att.equals("2") ) {%>
		<A href="#" onClick="patientDetailsQuickFunctions.openEthnicityWindow()"><%=LC.L_Select_Ethnicity%><%--Select Ethnicity*****--%></A>
		<%}%>

	</td>
	<%}} else { //KM-if custom data is not available

		%>
	<td width="15%">
			<%=LC.L_Additional%><%--Additional*****--%> </td>
	<td width="35%">
		<input type="text" name="txtAddEthnicity" size = 30 MAXLENGTH = 500  Readonly value='<%=addEthnicityNames%>'>
		<A href="#" onClick="patientDetailsQuickFunctions.openEthnicityWindow()"><%=LC.L_Select_Ethnicity%><%--Select Ethnicity*****--%></A>	</td>

	<%}%>
  </tr>

 <tr>

       <%if (hashPgCustFld.containsKey("race")) {
			int fldNumRace= Integer.parseInt((String)hashPgCustFld.get("race"));

			String raceMand = ((String)cdoPgField.getPcfMandatory().get(fldNumRace));
			String raceLable = ((String)cdoPgField.getPcfLabel().get(fldNumRace));
			raceAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumRace));


			if(raceAtt == null) raceAtt ="";if(raceMand == null) raceMand ="";




			if(!raceAtt.equals("0")) {
			if(raceLable !=null){
			%>  <td >
			<%=raceLable%>
			<%} else {%> <td >
			<%=LC.L_Prim_Race%><%--Primary Race*****--%>
			<%}
			if (raceMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatrace">* </FONT>
			<%
		 }%>
    </td>
    <td>
		<%=dRace%> <%if(raceAtt.equals("1")) {%><input type="hidden" name="patrace" value=""> <%}%><!--KM-->
    </td>
	<%} else if(raceAtt.equals("0")){%>
	<input type="hidden" name="patrace" value=""> <!--KM-->

	<%}} else { //KM-if custom data is not available
	%><td > <%=LC.L_Prim_Race%><%--Primary Race*****--%> </td>
    <td>
		<%=dRace%>
    </td>


	<%}%>


	<%if (hashPgCustFld.containsKey("additional2")) {


			int fldNumAdd2= Integer.parseInt((String)hashPgCustFld.get("additional2"));
			String add2Mand = ((String)cdoPgField.getPcfMandatory().get(fldNumAdd2));
			String add2Lable = ((String)cdoPgField.getPcfLabel().get(fldNumAdd2));
			String add2Att = ((String)cdoPgField.getPcfAttribute().get(fldNumAdd2));

			disableStr ="";
			if(add2Att == null) add2Att ="";if(add2Mand == null) add2Mand ="";




			if(!add2Att.equals("0")) {
			if(add2Lable !=null){
			%>
				<td >
			<%=add2Lable%>
			<%} else {%> <td >
			<%=LC.L_Additional%><%--Additional*****--%>
			<%}

			if (add2Mand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatadditional2">* </FONT>
			<%
		 }%>


		  <%if(add2Att.equals("1")) {
			 disableStr = "disabled class='readonly-input'"; }
		     //else if (add2Att.equals("2") ) {
			 //readOnlyStr = "readonly"; }
		 %>


	</td>
	<td >
		<input type="text" name="txtAddRace" size =30 MAXLENGTH = 500  <%=disableStr%> Readonly value='<%=addRaceNames%>'>

		 <%if(!add2Att.equals("1")  &&  !add2Att.equals("2") ) { %>

		<A href="#" onClick="patientDetailsQuickFunctions.openRaceWindow()"><%=LC.L_Select_Race%><%--Select Race*****--%></A>

         <%}%>

	</td>
	<%}} else { //KM-if custom data is not available

	%>
		<td > <%=LC.L_Additional%><%--Additional*****--%> </td>  <td > <input type="text" name="txtAddRace" size =30 MAXLENGTH = 500  Readonly value='<%=addRaceNames%>'>
		<A href="#" onClick="patientDetailsQuickFunctions.openRaceWindow()"><%=LC.L_Select_Race%><%--Select Race*****--%></A> </td>


  <%}%>

  </tr>
  <tr height="7"><td colspan="5"></td></tr>
  <tr>
		<td colspan="5">
			<p><%=LC.L_Reg_Dets%><%--Registration Details*****--%></p>
		</td>
	</tr>
	<tr>
	 <%if (hashPgCustFld.containsKey("org")) {
			int fldNumOrg= Integer.parseInt((String)hashPgCustFld.get("org"));
			String orgMand = ((String)cdoPgField.getPcfMandatory().get(fldNumOrg));
			String orgLable = ((String)cdoPgField.getPcfLabel().get(fldNumOrg));
			orgAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumOrg));


			if(orgAtt == null) orgAtt ="";
			if(orgMand == null) orgMand ="";


			if(!orgAtt.equals("0")) {
			if(orgLable !=null){
			%> <td width="15%">
			<%=orgLable%>
			<%} else {%> <td width="15%">
			<%=LC.L_Organization%><%--Organization*****--%>
			<%}

			if (orgMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatorg">* </FONT>
			<% } %>  <!--KM-050208-->

    </td>
    <td width="35%" >
		<%=dSites%>
		<%if(orgAtt.equals("1")) {%>
			<input type="hidden" name="patorganization" value="">
		<%} else if(orgAtt.equals("2")) {%>
			<input type="hidden" name="patorganization" value="<%=primSiteId%>">
		<%}%>
    </td>
	<%} else if(orgAtt.equals("0")) {%>

	<input type="hidden" name="patorganization" value=""> <!--KM-->
	<%}} else {%> <!-- KM-if custom data is not available.. -->

	<td width="15%"><%=LC.L_Organization%><%--Organization*****--%> <FONT class="Mandatory" id="pgcustompatorg">* </FONT> </td>
    <td  width="35%">

	<%=dSites%>

    <%}%>
	</td>


<%if (hashPgCustFld.containsKey("patfacid")) {

			int fldNumPfid= Integer.parseInt((String)hashPgCustFld.get("patfacid"));
			String pfMand = ((String)cdoPgField.getPcfMandatory().get(fldNumPfid));
			String pfLable = ((String)cdoPgField.getPcfLabel().get(fldNumPfid));
			String pfAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumPfid));

			disableStr ="";
			readOnlyStr ="";
			if(pfAtt == null) pfAtt ="";
			if(pfMand == null) pfMand ="";


			if(!pfAtt.equals("0")) {
			if(pfLable !=null){
			%><td width="15%">
			<%=pfLable%>
			<%} else {%> <td width="15%">
			<%=LC.L_PatFacility_ID%><%--Patient Facility ID*****--%>
			<%}

			if (pfMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatfacid">* </FONT>
			<% }
		 %>

		  <%if(pfAtt.equals("1")) {
			 disableStr = "disabled class='readonly-input'"; }
		    else if (pfAtt.equals("2") ) {
			 readOnlyStr = "readonly"; }
		 %>



    </td>
    <td width="34%">
		<input type="text" name="patFacilityID" size = 30 MAXLENGTH = 20 <%=disableStr%>  <%=readOnlyStr%>  value='<%=patientFacilityId%>'>
	</td>
    	<% }}

	else { //KM-if custom data is not available
		 %>

		<td width="15%"> <%=LC.L_PatFacility_ID%><%--<%=LC.Pat_Patient%> Facility ID*****--%>   </td>
    <td  width="34%">
		<input type="text" name="patFacilityID" size = 30 MAXLENGTH = 20   value='<%=patientFacilityId%>'>
	</td>


  <%}%>
<td></td>
</tr>

	<tr>



	   <%if (hashPgCustFld.containsKey("provider")) {


			int fldNumProv= Integer.parseInt((String)hashPgCustFld.get("provider"));

			String provMand = ((String)cdoPgField.getPcfMandatory().get(fldNumProv));
			String provLable = ((String)cdoPgField.getPcfLabel().get(fldNumProv));
			String provAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumProv));

			disableStr ="";
			if(provAtt == null) provAtt ="";
			if(provMand == null) provMand ="";




			if(!provAtt.equals("0")) {
			if(provLable !=null){
			%><td >
			<%=provLable%>
			<%} else {%> <td >
			<%=LC.L_Provider%><%--Provider*****--%>
			<%}
			if (provMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatprov">* </FONT>
			<%
		 }%>



		  <%if(provAtt.equals("1")) {
			 disableStr = "disabled class='readonly-input'"; }
		     //else if (provAtt.equals("2") ) {//KM
			 //readOnlyStr = "readonly"; }
		 %>


    </td>
    <td >
    	<input type=hidden name=patregby value='<%=regBy%>'>
    	<input type=text name=patregbyname value='<%=regByName%>' <%=disableStr%> readonly>

		  <%if(!provAtt.equals("1") && !provAtt.equals("2")) {%>
		  <A HREF=# onClick="patientDetailsQuickFunctions.openwin1()"><%=LC.L_Select_User%><%--Select User*****--%></A>
		  <%}%>
    </td>
	<%
		 }} else { //KM-if custom value is not available
	 %>
		<td > <%=LC.L_Provider%><%--Provider*****--%> </td> <td >
    	<input type=hidden name=patregby value='<%=regBy%>'>
    	<input type=text name=patregbyname value='<%=regByName%>' readonly> <A HREF=# onClick="patientDetailsQuickFunctions.openwin1()"><%=LC.L_Select_User%><%--Select User*****--%></A> </td>

	 <%}%>





  <%if (hashPgCustFld.containsKey("ifother")) {


			int fldNumIf= Integer.parseInt((String)hashPgCustFld.get("ifother"));

			String ifOthMand = ((String)cdoPgField.getPcfMandatory().get(fldNumIf));
			String ifOthLable = ((String)cdoPgField.getPcfLabel().get(fldNumIf));
			String ifOthAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumIf));


			disableStr ="";
			readOnlyStr ="";
			if(ifOthAtt == null) ifOthAtt ="";
			if(ifOthMand == null) ifOthMand ="";


			if(!ifOthAtt.equals("0")) {
			if(ifOthLable !=null){
			%> <td>
			<%=ifOthLable%>
			<%} else {%> <td >
			<%=LC.L_If_Other%><%--If Other*****--%>
			<%}
			if (ifOthMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatifoth">* </FONT>
			<%
		 }%>


		  <%if(ifOthAtt.equals("1")) {
			 disableStr = "disabled class='readonly-input'"; }
		     else if (ifOthAtt.equals("2") ) {
			 readOnlyStr = "readonly"; }
		 %>



  </td><td ><input type="text" name="phyOther" size = 30 <%=disableStr%>  <%=readOnlyStr%> value="<%=phyOther%>"></td>

  <%}} else { //KM-if custom value is not available

  %>
	<td > <%=LC.L_If_Other%><%--If Other*****--%>  </td><td ><input type="text" name="phyOther" size = 30 value="<%=phyOther%>"></td>

  <%}%>

  </tr>
  <tr>


	 <%if (hashPgCustFld.containsKey("survivestat")) {
			int fldNumSurStat= Integer.parseInt((String)hashPgCustFld.get("survivestat"));
			String surStatMand = ((String)cdoPgField.getPcfMandatory().get(fldNumSurStat));
			String surStatLable = ((String)cdoPgField.getPcfLabel().get(fldNumSurStat));
			surStatAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumSurStat));

			if(surStatAtt == null) surStatAtt ="";
			if(surStatMand == null) surStatMand ="";

			if (!surStatAtt.equals("0")) {
			if(surStatLable !=null){
			%><td >
			<%=surStatLable%>
			<%} else {%> <td >
			<%=LC.L_Survival_Status%><%--Survival Status*****--%>
			<%}

			if (surStatMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatsstate">* </FONT>
      <%}%>

    </td>
    <td>
	<%=dStatus%>
	<%if (surStatAtt.equals("1")) {%>
		<input type="hidden" name="patstatus" value="">
	<%} else if (surStatAtt.equals("2")) {%>
		<input type="hidden" name="patstatus" value="">
	<%}%>
    </td>
	<%} else if(surStatAtt.equals("0")) { %>
	<input type="hidden" name="patstatus" value="">

	<%}} else {%>
	   <td > <%=LC.L_Survival_Status%><%--Survival Status*****--%> <FONT class="Mandatory" id="pgcustompatsstate">* </FONT> </td>
    <td >
	<%=dStatus%>
    </td>

    <%}%>


 <%if (hashPgCustFld.containsKey("dod")) {

			int fldNumDod= Integer.parseInt((String)hashPgCustFld.get("dod"));

			String dodMand = ((String)cdoPgField.getPcfMandatory().get(fldNumDod));
			String dodLable = ((String)cdoPgField.getPcfLabel().get(fldNumDod));
			String dodAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumDod));


			disableStr ="";
			readOnlyStr ="";
			if(dodAtt == null) dodAtt ="";
			if(dodMand == null) dodMand ="";


			if(!dodAtt.equals("0")) {
			if(dodLable !=null){
			%>  <td >
			<%=dodLable%>
			<%} else {%> <td >
			 <%=LC.L_Date_OfDeath%><%--Date of Death*****--%>
			<%}
			if (dodMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatdod">* </FONT>
			<%
		 }%>

		 <%if(dodAtt.equals("1")) {
			 disableStr = "disabled class='readonly-input'"; }
		     else if (dodAtt.equals("2") ) {
			 readOnlyStr = "readonly"; }
		 %>



    </td>
    <td >
<%-- INF-20084 Datepicker-- AGodara --%>	
		<%if (completeDet){%>
			<%if (StringUtil.isEmpty(dodAtt)){ %>
				<input type="text" name="patdeathdate" class="datefield" size = 15 MAXLENGTH = 50	value = <%=deathDate%>>
			<%} else if(dodAtt.equals("1") || dodAtt.equals("2")) {%>
				<input type=<%=inputType%> name="patdeathdate"  <%=disableStr%> <%=readOnlyStr%> size = 10 MAXLENGTH = 11 value="<%=deathDate%>" <%=disableStr%> ><%=displayStar%>
			<%}
		}else{%>
			<input type=<%=inputType%> name="patdeathdate" size = 10 MAXLENGTH = 11 value="<%=deathDate%>" <%=disableStr%> readonly ><%=displayStar%>
		<%}%>
    </td>
	<%}}else {
		//KM-if custom value is not available
	%> 
		<td> <%=LC.L_Date_OfDeath%><%--Date of Death*****--%> </td> 
		<td>
	<% if (completeDet){ %>
			<input type=<%=inputType%> name="patdeathdate" class="datefield" size = 10 MAXLENGTH = 11 value="<%=deathDate%>"  ><%=displayStar%>
	<%}else{%>
	
			<input type=<%=inputType%> name="patdeathdate" size = 10 MAXLENGTH = 11 value="<%=deathDate%>"  ><%=displayStar%>
	<% }
	}%>
	<td>
  </tr>
  <%// May Enhn%>
  <tr>


   <%if (hashPgCustFld.containsKey("cod")) {


			int fldNumCod= Integer.parseInt((String)hashPgCustFld.get("cod"));

			String codMand = ((String)cdoPgField.getPcfMandatory().get(fldNumCod));
			String codLable = ((String)cdoPgField.getPcfLabel().get(fldNumCod));
			codAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumCod));


			if(codAtt == null) codAtt ="";
			if(codMand == null) codMand ="";


			if(!codAtt.equals("0")) {
			if(codLable !=null){
			%><td >
			<%=codLable%>
			<%} else {%> <td >
			 <%=LC.L_Death_Cause%><%--Cause of Death*****--%>
			<%}
			if (codMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatcod">* </FONT>
			<%
		 }%>


 </td><td ><%=dDthCause%> <%if(codAtt.equals("1")) {%><input type="hidden" name="dthCause" value=""> <%}%><!--KM--></td>

  <%}}
  else {  //KM-if backend data is not available
  %>
     <td >  <%=LC.L_Death_Cause%><%--Cause of Death*****--%></td><td ><%=dDthCause%></td>
  <%}%>



<%

String odthCause=request.getParameter("othDthCause");

if(odthCause==null) { odthCause=""; } %> <!--JM: 01.31.2006 #2495 "REASON OF DEATH" changed to "SPECIFY CAUSE"--> <!--<td width="15%">REASON OF DEATH</td><td width="35%"><input type = text name="otherdthCause" size = 30 value="<%--=odthCause--%>"></td> -->



<%if (hashPgCustFld.containsKey("speccause")) {


			int fldNumSpec= Integer.parseInt((String)hashPgCustFld.get("speccause"));
			String specMand = ((String)cdoPgField.getPcfMandatory().get(fldNumSpec));
			String specLable = ((String)cdoPgField.getPcfLabel().get(fldNumSpec));
			String specAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumSpec));


			disableStr ="";
			readOnlyStr ="";
			if(specAtt == null) specAtt ="";
			if(specMand == null) specMand ="";



			if(!specAtt.equals("0")) {
			if(specLable !=null){
			%>  <td >
			<%=specLable%>
			<%} else {%> <td >
			  <%=LC.L_Specify_Cause%><%--Specify Cause*****--%>
			<%}
			if (specMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatspeccause">* </FONT>
			<%
		 }%>


	     <%if(specAtt.equals("1")) {
			 disableStr = "disabled class='readonly-input'"; }
		   else if (specAtt.equals("2") ) {
			 readOnlyStr = "readonly"; }
		 %>



	</td><td ><input type = text name="otherdthCause" size = 30  <%=disableStr%>  <%=readOnlyStr%> value="<%=odthCause%>"></td>
	<%}} else {  //KM-if backend data is not available
	%>
          <td > <%=LC.L_Specify_Cause%><%--Specify Cause*****--%>  </td><td ><input type = text name="otherdthCause" size = 30 value="<%=odthCause%>"></td>

	<%}%>

  </tr>
  <%//-- May Enhn%>
<tr height="7"><td colspan="5"></td></tr>
  </table>
	<BR>
  <table cellspacing="1" cellpadding="0" align="center" class="basetbl outline midAlign bgcolorFix">
<tr height="7"><td colspan="5"></td></tr>
<tr>
	<td width="65%" >
 <%=MC.M_SpecDeptAcc_Dgraphics%><%--Specify groups/departments with access to edit <%=LC.Pat_Patient_Lower%>'s demographics.
		If blank, all groups have access*****--%> &nbsp;
</td><td width="35%">
		<input type=text name=txtSpeciality  size=15 readOnly value="<%=specialName%>">


		<A href="#" onClick="patientDetailsQuickFunctions.openSpecialityWindow()"><%=LC.L_Select_Speciality_Upper%><%--SELECT SPECIALITY*****--%></A>

</td>


	</tr>

<tr>	<td width="100%" align="center" colspan="5" id="selectStudyLabel"><br>
	<%=MC.M_SelEtr_DetsPat%><%--Select a <%=LC.Std_Study_Lower%> to enter screening/enrollment details for this <%=LC.Pat_Patient_Lower%>*****--%>:  &nbsp;<%=strStudyId%></td></tr>

  </table>

<%

	} //end of if body for page right

else

{

%>

<jsp:include page="accessdenied.jsp" flush="true"/>

<%

} //end of else body for page right

}//end of if body for session

else

{

%>

<jsp:include page="timeout.html" flush="true"/>

<%}%>
<%
String accId = (String) tSession.getAttribute("accountId");
int accIdNum = StringUtil.stringToNum(accId);
SettingsDao settingsDao =
	commonB.retrieveSettings(accIdNum, 1);
final String ACCELARATED_ENROLLMENT_CONFIG = "ACCELARATED_ENROLLMENT_CONFIG";
boolean isAccelaratedEnrollOn = false;
for (int x = 0; x<settingsDao.getSettingKeyword().size(); x++){
	String settingKey = (String)settingsDao.getSettingKeyword().get(x);
	if(ACCELARATED_ENROLLMENT_CONFIG.equalsIgnoreCase(settingKey)){
		isAccelaratedEnrollOn = true;
		break;
	}
}


if (wsEnabled.equals("Y") || isAccelaratedEnrollOn){ %>
<div class ="transparent" id = "trans"></div>
<%CommonDAO commonDao=commonB.getCommonHandle();
commonDao.getMappingData("\'race\',\'gender\',\'ethnicity\'",accCode);
ArrayList mappingType=commonDao.getMapping_type();
ArrayList mappingSubTyp=commonDao.getMapping_subTypList();
ArrayList mappingLMapList=commonDao.getMapping_lMapList();
ArrayList mappingRMapList=commonDao.getMapping_rMapList();
%>

<%} %>
