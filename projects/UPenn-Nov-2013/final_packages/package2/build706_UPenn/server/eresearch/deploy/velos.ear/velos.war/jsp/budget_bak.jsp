<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>Budget</title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />

<%@ page import="com.velos.eres.service.util.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.web.user.UserJB,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.esch.business.common.*"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>


<SCRIPT Language="javascript">

function orgnTest(formobj,var1) {
if (document.all) {
	if(var1==1)
	{
	formobj.orgId.style.visibility="hidden";
	document.all["SO"].style.visibility="hidden";
	} else
	{
	formobj.orgId.style.visibility="visible";
	document.all["SO"].style.visibility="visible";
	}
}
}

function setflag(formobj,var1)
 {
 formobj.hidOrgFlag.value = var1; 
}

function openwin1() {
      window.open("usersearchdetails.jsp?fname=&lname=&from=budget","usersearchdetails","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=600,height=450")
;}



 function  validate(formobj){
    if (!(validate_col('Budget Name',formobj.budgetName))) return false	
	if (!(validate_col('Currency',formobj.currencyId))) return false
	
	/*
	
	if (document.all) {
	 	if(!(formobj.orgId.style.visibility=="hidden"))
 		{
 			if (!(validate_col('orgId',formobj.orgId))) return false	
	 	}
	}
	*/


	if (formobj.hidOrgFlag.value == "1")
	{
    	if (!(validate_col('orgId',formobj.orgId))) return false
	}
	
	if (formobj.hidOrgFlag.value == "0")
	{
	
		 i = formobj.orgId.options.selectedIndex;
		 value =formobj.orgId.options[i].value;

    	if (value == '' || value==null)
		{
		}
		else
		{
    		alert("Budget is Organization non-specific. You can not select an Organization");
			return false;
		}
		
	}
	
	if (!(validate_col('e-Signature',formobj.eSign))) return false
	
	if(formobj.budgetStatus.value=='F'){
	
		if(!(confirm("Selecting the Freeze option will not allow further changes to this budget. Are you sure you would like to proceed?"))){
		return false;
		}
	}

	
	if(isNaN(formobj.eSign.value) == true) {
	alert("Incorrect e-Signature. Please enter again");
	formobj.eSign.focus();
	return false;
   }

   }

</SCRIPT>

<body>

<jsp:useBean id="budgetB" scope="page" class="com.velos.esch.web.budget.BudgetJB"/>
<jsp:useBean id="bgtcalB" scope="page" class="com.velos.esch.web.budgetcal.BudgetcalJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="bgtcaldao" scope="request" class="com.velos.esch.business.common.BudgetcalDao"/>

<% String src = request.getParameter("srcmenu");
	String mode =   request.getParameter("mode");
	
	  int ienet = 2;
  int proceed = 1;
  String agent1 ;
  agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1) 
     ienet = 0; //IE
    else
	ienet = 1;
	
	
	
	
	
%>


<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>

</jsp:include>

	<br>	
  <DIV class="formDefault" id="div1"> 
  <%  if(mode.equals("M")){ %>  
		  <P class="sectionHeadings"> Budget >> Open </P>
		  <%}else{%>
		   <P class="sectionHeadings"> Budget >> New  </P>
	<%}%>
  
  <%

	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))

		{

		String uName = (String) tSession.getValue("userName");
		String budgetId=request.getParameter("budgetId");
		int bgtId=EJBUtil.stringToNum(budgetId);
		String fromPage = request.getParameter("fromPage");

		
		if(mode.equals("M"))
		{
		%>		
			<jsp:include page="budgettabs.jsp" flush="true">
			<jsp:param name="budgetType" value="P"/> 
			<jsp:param name="mode" value="<%=mode%>"/>			 
			</jsp:include>
	
		<%
		}

		String budgetname=""; 
		String budgetver="";
		String budgetdesc="";
		String budgetCreatedByName="";


		String budgetCreatedById="";
		String sAcc= (String) tSession.getValue("accountId");
		String cd1="";
		int counter,siteId=0;
		String currencyId="";
		String budgetType="";
		String calStatus="";
		String orgFlag="";
		String orgId="";
		String budgetStudyId="";
		String selected="";
		StringBuffer mainStr = new StringBuffer();
		String bgtCalFlag="";
		String budgetStatus="";
	
		
     	SiteDao siteDao = new SiteDao();
     	siteDao.getSiteValues(EJBUtil.stringToNum(sAcc));
		
			
		SchCodeDao cd = new SchCodeDao();
	    cd.getCodeValues("currency", EJBUtil.stringToNum(sAcc));
		
		String acc = (String) tSession.getValue("accountId");		
		StringBuffer orgn = new StringBuffer();

		if(mode.equals("N")){
		orgn.append("<SELECT NAME=orgId  onClick = return test() >") ;  
		orgn.append("<OPTION value=''  selected >Select an Option</OPTION>");   	
		for (counter = 0; counter <= (siteDao.getSiteIds()).size() -1 ; counter++){
     		siteId = ((Integer)((siteDao.getSiteIds()).get(counter))).intValue(); 
     		orgn.append("<OPTION value = "+ siteId +">" + (siteDao.getSiteNames()).get(counter) + "</OPTION>");
     	}
		     	orgn.append("</SELECT>");
		}

		///////////////////study
		
		
		
		int studyId=0;
		String ver = "";
		StringBuffer study=new StringBuffer();	
		
		StudyDao studyDao = new StudyDao();	
		
		
		studyDao.getBudgetStudyValuesForAccount(sAcc);
		if(mode.equals("N")){
		
			study.append("<SELECT NAME=budgetStudyId>") ;
			study.append("<OPTION value=''  selected>Select an Option</OPTION>");
	
			for (counter = 0; counter <= (studyDao.getStudyNumbers()).size() -1 ; counter++){
			studyId = ((Integer)((studyDao.getStudyIds()).get(counter))).intValue();
			ver = (String) (studyDao.getStudyVersions()).get(counter);
		
			if (ver == null || ver.trim().equals(""))
			{
			ver = "-";
			}		 
			study.append("<OPTION value = "+ studyId +">" + (studyDao.getStudyNumbers()).get(counter) + " version (" + ver + ")</OPTION>");
			}
		
		study.append("</SELECT>");
		}
		

		
		
			int pageRight = 0;
		
		if (mode.equals("M")) {		
			budgetB.setBudgetId(bgtId);
			budgetB.getBudgetDetails();
			budgetname=budgetB.getBudgetName();
			budgetver=budgetB.getBudgetVersion();
			budgetdesc=budgetB.getBudgetdesc();
			budgetCreatedById=budgetB.getBudgetCreator();
			
			userB.setUserId(EJBUtil.stringToNum(budgetCreatedById));
	   		userB.getUserDetails();
		    budgetCreatedByName = userB.getUserFirstName() + " " + userB.getUserLastName();	

				
			currencyId=budgetB.getBudgetCurrency();
			
			cd1 = cd.toPullDown("currencyId",EJBUtil.stringToNum(currencyId));
			budgetType=budgetB.getBudgetType();
			budgetStatus=budgetB.getBudgetStatus();
			calStatus=budgetB.getBudgetCFlag();
			orgFlag=budgetB.getBudgetSFlag();
			orgId=budgetB.getBudgetSiteId();

			bgtCalFlag=budgetB.getBudgetCFlag();
			
			/////////////////////
			
			orgn.append("<SELECT NAME=orgId >") ;  
			orgn.append("<OPTION value=''  selected>Select an Option</OPTION>");   	
			for (counter = 0; counter <= (siteDao.getSiteIds()).size() -1 ; counter++){

			siteId = ((Integer)((siteDao.getSiteIds()).get(counter))).intValue();
				if(siteId==(EJBUtil.stringToNum(orgId)))
				{selected="selected";
				}
				else{
				selected="";
				}
     		 
			
     		orgn.append("<OPTION value = "+ siteId + " " + selected +">" + (siteDao.getSiteNames()).get(counter) + "</OPTION>");
     		}		
	     	orgn.append("</SELECT>");			
			
			
			
			
			//////////////////////
			
			
			
			
			
			budgetStudyId=budgetB.getBudgetStudyId();
			
			
			///////////////////////////////////////////////
			
			
		
			study.append("<SELECT NAME=budgetStudyId>") ;
			study.append("<OPTION value=''  selected>Select an Option</OPTION>");
	
			for (counter = 0; counter <= (studyDao.getStudyNumbers()).size() -1 ; counter++){
			studyId = ((Integer)((studyDao.getStudyIds()).get(counter))).intValue();
			
			if(studyId==EJBUtil.stringToNum(budgetStudyId))
			{		
			selected="selected";				
			} else
			selected="";
			
			
			ver = (String) (studyDao.getStudyVersions()).get(counter);
		
			if (ver == null || ver.trim().equals(""))
			{
			ver = "-";
			}		 
			study.append("<OPTION value = "+ studyId + " " + selected+ ">" + (studyDao.getStudyNumbers()).get(counter) + " version (" + ver + ")</OPTION>");
			}
		
			study.append("</SELECT>");
			
		
		/////////////////////////////////////////////////////
		
			
			
			}else{
			
			String userIdFromSession = (String) tSession.getValue("userId");			
			userB.setUserId(EJBUtil.stringToNum(userIdFromSession));
		    userB.getUserDetails();			
			budgetCreatedByName = userB.getUserFirstName() + " " + userB.getUserLastName(); 
			budgetCreatedById=userIdFromSession;
			
			///////////////
				
				mainStr.append("<SELECT NAME="+ "currencyId" +">") ;
				mainStr.append("<OPTION value='' SELECTED> Select an Option</OPTION>");
			
				
				 ArrayList cDesc=cd.getCDesc();
				 ArrayList cId=cd.getCId();
				 
				for (counter = 0; counter <= (cd.getCRows() -1) ; counter++){
				 if(cDesc.get(counter).equals("Dollars")){	selected="Selected";}
				 else
				 {
				 selected="";
				 }
				mainStr.append("<OPTION value = "+ cId.get(counter)+ "  " + selected + ">" + cDesc.get(counter) + "</OPTION>");
				}
				mainStr.append("</SELECT>");
				//////////////
			}
			
			
%> 


  
  	  <Form name="budget" method="post" action="updatebudget.jsp"  onSubmit="return validate(document.budget)">
  
    	 <table width="600" cellspacing="2" cellpadding="2">
		 <tr><td colspan=3> <A href="copybudget.jsp?from=initial&srcmenu=<%=src%>&calledFrom=budget.jsp">Copy an existing Budget</A></td></tr>
		 <tr><td>
		 </table>
		 
		 <table width="600" cellspacing="2" cellpadding="2">
			<%if ((!(budgetStatus ==null)) && (budgetStatus.equals("F"))&&(mode.equals("M"))) {%>
          	<P class = "defComments"><FONT class="Mandatory">Budget Status is Freeze. You cannot make any changes to the budget.</Font></P>
			<%}%>
			</td></tr>
		</table>
		
		 <table width="600" cellspacing="2" cellpadding="2">
		
	     	<tr> 
      	 		<td width="300"> Budget Name <FONT class="Mandatory">* </FONT> </td>
        		<td> <input type="text" name="budgetName" size = 30   MAXLENGTH = 100 	value="<%=budgetname%>"> </td>
     			 <td width="100"> &nbsp </td>
			     <td width="300"> Version Number </td>				 
	    	    <td>
	        	 <input type="text" name="budgetVer" size = 25 MAXLENGTH = 200 	value="<%=budgetver%>">
		        </td>
    		</tr>		 
		 </table>
	
		 
	  <table width="600" cellspacing="2" cellpadding="2">
		 	<tr> 
		      	<td width="300"> Description  </td>
	        	<td> 
				<TEXTAREA  name="budgetDesc" rows=3 cols=50><%=budgetdesc%></TEXTAREA>
				</td>
	    	 </tr>		 
		 </table>	
		 	 
		 <table width="600" cellspacing="2" cellpadding="2">
	     <tr> 
		      	 <td width="100"> Budget Created By </td>
        		<td width="350"> 
	          	<input type="text" name="budgetCreatedByName" size = 12  MAXLENGTH = 50 	value="<%=budgetCreatedByName%>" readonly >
			  	<input type="hidden" name="budgetCreatedById" value="<%=budgetCreatedById%>">
               	<A HREF=# onClick=openwin1() >Select User</A>
			     </td>		   
	    	    <td width="150">Budget Currency</td>

				<td width="50"><%if(mode.equals("M")){ out.println(cd1);} else{out.println(mainStr);}%></td>
		 </tr>		 
		 

		 <tr> 
      	 <td width="300">Budget Type </td>
 

		  <% if (mode.equals("N")) {%>
			<td>		  
			<select  name="budgetType">			
			<option value=P selected ><%=LC.Pat_Patient%></option>	
			<option value=S >Study</option>	
			</select>
			</td>
			<%	   } else { //mode check %>

			<%if ((!(budgetType ==null)) && (budgetType.equals("P"))) {%>
			    <td width="100"> <%=LC.Pat_Patient%>
        		 <input type="hidden" name="budgetType" value="P"> </td> 			
			<%} else if ((!(budgetType ==null)) && (budgetType.equals("S"))) {%>
			    <td width="100"> Study
			    <input type="hidden" name="budgetType" value="S"> </td>	
			<%}
			
			
			
			
			
			
			 }%>
		  </select>	   
		   
	        <td width="300"> Budget Status</td>
			
			<td>
			<%   if (mode.equals("N")) {	%>
	
			  <select  name="budgetStatus">
			  <option value=F>Freeze</option>	
  			   <option value=W selected>Work in Progress</option>	
			  </select>
			<%	   } else {	%>

			<select  name="budgetStatus">
			<%if ((!(budgetStatus ==null)) && (budgetStatus.equals("F"))) {%>
				<option value=F Selected >Freeze</option>	
				<option value=W >Work in Progress</option>
			<%} else if ((!(budgetStatus ==null)) && (budgetStatus.equals("W"))) {%>
				<option value=F>Freeze</option>	
				<option value=W Selected >Work in Progress</option>	
			<%}else{ %>
				<option value=F>Freeze</option>	
				<option value=W Selected >Work in Progress</option>
			
			 <%} }%>
	 		  </select>
			</td>	

		  
		  </tr>
		  </table>
		  
    	 <table width="600" cellspacing="2" cellpadding="2"> 
		  
		<tr>
	    <td>
		<% if(mode.equals("N")) {%>		
	    <Input type="radio" name="orgFlag" value="0"  onClick=setflag(document.budget,0)> Budget is Organization non-specific
		<input type="hidden" name="hidOrgFlag" value=0>
	    </td>		
	    <tr>
	    <td>
	    <Input type="radio" name="orgFlag" value="1"  <% out.println("checked=true");%> onClick=setflag(document.budget,1) > Budget is specific to the following organization
	    </td>
		<%}else{ %>
		<input type="hidden" name="hidOrgFlag" value=<%=orgFlag%>>
		<Input type="radio" name="orgFlag" value="0" onClick=setflag(document.budget,0) <% if( orgFlag.equals("0")){out.println("checked=true");} %> > Budget is Organisation non-specific
	    </td>		
	    <tr>
	    <td>
	    <Input type="radio" name="orgFlag" value="1"  onClick=setflag(document.budget,1) <% if( orgFlag.equals("1")){out.println("checked=true"); }%>> Budget is specific to the following organization
	    </td>
		
		<%}%>
	
		<td>
		<Font class=comments>
	      	Select Organization <%=orgn%></Font>
	  	

				
			 </td>
      		
	    </tr>
		
		<tr>
	    <td >
	     Associate Budget to the following Study:
	    </td>
		
		<td ><Font class=comments>
	      	Select Study <%=study%></Font>
      	</td>		
	    </tr>
		
		
		</table>
				 
		
		<%-- if(mode.equals("N")) {%>
		 <table width="600" cellspacing="2" cellpadding="2">
		<tr>
			<td>
			<P class="defComments"><b>If Budget Type is <%=LC.Pat_Patient%> Budget</b></P>
		    </td>
			<td> &nbsp </td>
		</tr>			
		
		<tr>
	    <td>
		
	    <Input type="radio" name="bgtCalFlag" value="0" <% out.println("checked=true"); %> > Create a budget without using any existing protocol calender template
	    </td>
		</tr>		
	    <tr>
	    <td>
	    <Input type="radio" name="bgtCalFlag" value="1" > Create a budget from an existing protocol calender
	    </td>
		</tr>
		<%}else{ %>
		
		<tr>
		<td>
		<Input type="radio" name="bgtCalFlag" value="0" <% if( bgtCalFlag.equals("0")){out.println("checked=true");} %> > Create a budget without using any existing protocol calender template
	    </td>
		</tr>
				
	    <tr>
	    <td>
	    <Input type="radio" name="bgtCalFlag" value="1" <% if( bgtCalFlag.equals("1")){out.println("checked=true"); }%>> Create a budget from an existing protocol calender
	    </td>
		</tr>
		</table>
		
		
		<%}--%>


			<%if(!((!(budgetStatus ==null)) && (budgetStatus.equals("F"))&&(mode.equals("M")))) {%>
		
		
		<table width="600" cellspacing="2" cellpadding="2">
		<tr>
	   	<td>
			e-Signature <FONT class="Mandatory">* </FONT>
	   </td>
	   

	   <td>
		<input type="password" name="eSign" maxlength="8" autocomplete="off">
	    <input type="image" src="../images/jpg/proceedbudget.gif"  align="absmiddle" border="0">
	   </td>
	   <td>
	   <button onclick="window.history.back();"><%=LC.L_Back%></button>
	   </td>
		</tr>		
		</table>
		
		 

			
		<%}%>	
	
    <input type="hidden" name="mode" value="<%=mode %>">
	<input type="hidden" name="src"  value="<%=src%>">
	<input type="hidden" name="budgetId" value="<%=budgetId%>">
    <input type="hidden" name="fromPage" value="<%=fromPage%>">	
    <br>	
	
	 
 </Form>

	
	<% if(orgFlag.equals("0")) {%>
	
		<SCRIPT Language="javascript">
		<%

if (ienet == 0)
{ 
		%>
	
		//window.document.budget.orgId.style.visibility="hidden";
		//document.all["SO"].style.visibility="hidden";
		<%
		}else{
		%>
//		window.document.div1.document.budget.orgId.visibility="hidden";
//		document.layers["SO"].visibility="hidden";		
//		alert(document.SO.visibility);
//			submenuItem1.style.visibility ="hidden";
//		window.document.div1.document.budget.SO.visibility="hidden";
//		document.all["SO"].style.visibility="hidden";
//		window.document.div1.document.budget.SO.visibility="hidden";
//alert(window.document.layers.SO.display);
//alert(window.document.div1.document.layers[SO].visibility);
//		window.document.layers.SO.visibility="hidden";
<%}%>


				
		</SCRIPT>	

	<% } %>


	</div>
  
  <%

}//end of if body for session

else

{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}

%>
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
	<div class ="mainMenu" id = "emenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/>
</div>

</body>
</html>
