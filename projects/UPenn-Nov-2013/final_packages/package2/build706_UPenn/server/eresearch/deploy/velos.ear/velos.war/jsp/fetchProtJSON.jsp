<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList,java.util.HashMap,org.json.*"%>
<%@page import="com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.*"%>
<%@page import="com.velos.esch.business.common.EventdefDao"%>
<%@page import="com.velos.esch.business.common.EventAssocDao"%>
<%@page import="com.velos.esch.business.common.SchCodeDao"%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="protVisitB" scope="request" class="com.velos.esch.web.protvisit.ProtVisitJB"/>
<jsp:useBean id="visitdao" scope="request" class="com.velos.esch.business.common.ProtVisitDao"/>

<%
    request.setCharacterEncoding("UTF-8");
    response.setContentType("application/json");
    // response.setContentType("text/html");

	HttpSession tSession = request.getSession(true);
    JSONObject jsObj = new JSONObject();
    if (!sessionmaint.isValidSession(tSession)) {
        // Session is invalid; print an error and exit
        response.sendError(HttpServletResponse.SC_FORBIDDEN, "SESSION_TIME_OUT");
        jsObj.put("error", new Integer(-1));
        jsObj.put("errorMsg", MC.M_UsrNot_LoggedIn/*User is not logged in.*****/);
	    out.println(jsObj.toString());
        return;
    }
    
	String protocolId = request.getParameter("protocolId");
	if(protocolId == "" || protocolId == null || protocolId.equals("null") || protocolId.equals("")) {
	    // A valid protocol ID is required; print an error and exit
        jsObj.put("error", new Integer(-2));
        jsObj.put("errorMsg", MC.M_PcolId_Invalid/*Protocol ID is invalid.*****/);
	    out.println(jsObj.toString());
	    return;
	}
	
    // Define page-wide variables here
    StringBuffer sb = new StringBuffer();
    String calledFrom = request.getParameter("calledFrom");
    String calassoc = request.getParameter("calassoc");
    String pgRight = request.getParameter("doGet");
    boolean isLibCal = false;
    boolean isStudyCal = false;
    String protocolName = null;
    String calStatus = null;
    int errorCode = 0;
    ArrayList eventIds = null, eventNames = null;
    ArrayList eventVisitIds = null;
    ArrayList eventOfflines = null;
    ArrayList visithideFlags = null;//YK: Fix for Bug#7264
    //Yk: Added for Enhancement :PCAL-19743
    ArrayList newEventIds = null;
    
    if ("P".equals(calledFrom) || "L".equals(calledFrom) ) {
        isLibCal = true;
    } else if ("S".equals(calledFrom)) {
        isStudyCal = true;
    } else {
        jsObj.put("error", new Integer(-3));
        jsObj.put("errorMsg", MC.M_CalType_Miss/*Calendar Type is missing.*****/);
	    out.println(jsObj.toString());
        return;
    }
    
    String calStatusPk = "";
	SchCodeDao scho = new SchCodeDao();
	if (isLibCal) {
	//Yk: Modified for Enhancement :PCAL-19743
        EventdefDao defdao = eventdefB.getProtSelectedAndVisitEvents(EJBUtil.stringToNum(protocolId));
        eventIds = defdao.getEvent_ids();
   		eventVisitIds = defdao.getFk_visit();
   		newEventIds = defdao.getNew_event_ids();
   		eventNames = defdao.getNames();
   		eventdefB.setEvent_id(EJBUtil.stringToNum(protocolId));
   		eventdefB.getEventdefDetails();
   		protocolName = eventdefB.getName();
   		
		//KM-#DFin9
		calStatusPk = eventdefB.getStatCode();
		calStatus = scho.getCodeSubtype(EJBUtil.stringToNum(calStatusPk)).trim();


    } else if (isStudyCal) {
    //Yk: Modified for Enhancement :PCAL-19743
        EventAssocDao assocdao = eventassocB.getProtSelectedAndVisitEvents(EJBUtil.stringToNum(protocolId));
        eventIds = assocdao.getEvent_ids();
        newEventIds = assocdao.getNew_event_ids();
   		eventVisitIds = assocdao.getEventVisits();
   		eventNames = assocdao.getNames();
   		eventOfflines = assocdao.getOfflineFlags(); // Unique to event_assoc, not in event_def
   		eventassocB.setEvent_id(EJBUtil.stringToNum(protocolId));
   		eventassocB.getEventAssocDetails();
   		protocolName = eventassocB.getName();
   		visithideFlags = assocdao.getHideFlags();//YK: Fix for Bug#7264
		//KM-#DFin9
		calStatusPk = eventassocB.getStatCode();
		calStatus = scho.getCodeSubtype(EJBUtil.stringToNum(calStatusPk)).trim();

    }

    ArrayList visitIds = null;
	ArrayList visitNames = null;
	ArrayList visitDisplacements = null;
	String visitId = null;
	String visitName = null;
	String offLineFlag = ""; //Yk: Added for Enhancement :PCAL-19743
	
	visitdao = protVisitB.getProtocolVisits(EJBUtil.stringToNum(protocolId));
	visitIds = visitdao.getVisit_ids();
	visitNames = visitdao.getNames();
	visitDisplacements = visitdao.getDisplacements();

    int noOfEvents = eventIds == null ? 0 : eventIds.size();
    if (protocolName == null) { protocolName = ""; }
    int noOfVisits = visitIds == null ? 0 : visitIds.size();
    
    
    jsObj.put("error", errorCode);
    jsObj.put("errorMsg", "");
    jsObj.put("noOfEvents", noOfEvents);
    
    JSONArray jsEvents = new JSONArray();
    for (int iX=0; iX<eventIds.size(); iX++) {
        JSONObject jsObj1 = new JSONObject();
        jsObj1.put(String.valueOf(eventIds.get(iX)), eventNames.get(iX));
        jsEvents.put(jsObj1);
    }
    //YK: Fix for Bug#7264
    JSONArray jsVisits = new JSONArray();
    if(isStudyCal){
    for (int iX=0; iX<eventIds.size(); iX++) {
        JSONObject visitsObj = new JSONObject();
        if(((Integer)eventIds.get(iX)).intValue()!=0){continue ;}
 	    String hideFlag= visithideFlags.get(iX)==null?"0":(String)visithideFlags.get(iX);
        visitsObj.put("key", "v"+String.valueOf(eventVisitIds.get(iX)));
        visitsObj.put("value", hideFlag);
        jsVisits.put(visitsObj);
       
    }
    jsObj.put("visitsHideFlag", jsVisits);
    }
    //Yk: Added for Enhancement :PCAL-19743 -Starts
    ArrayList eventVisitIdArray = new ArrayList();
    ArrayList eventVisitCombinations = new ArrayList();
    
    for (int iX=0; iX<newEventIds.size(); iX++) {

    	if (calledFrom.equals("S")){

			offLineFlag = ((eventOfflines.get(iX)) == null)?"":(eventOfflines.get(iX)).toString();
			
			}
    	//YK : Modified for Bug #7513 
        if (((Integer)eventIds.get(iX)).intValue() == 0 ) { continue; }

    	HashMap eventVisitIdMap = new HashMap();
        eventVisitIdMap.put("event", (String)eventNames.get(iX));
        eventVisitIdMap.put("eventId", String.valueOf(eventIds.get(iX))+"V"+(Integer)eventVisitIds.get(iX));
        eventVisitIdMap.put("key", "e"+String.valueOf(newEventIds.get(iX)));
        eventVisitIdMap.put("value","e"+String.valueOf(newEventIds.get(iX))+"v"+eventVisitIds.get(iX)+"f"+offLineFlag);
        eventVisitIdArray.add(eventVisitIdMap);

        eventVisitCombinations.add(String.valueOf(eventIds.get(iX))+"V"+(Integer)eventVisitIds.get(iX));
    }
   
    ArrayList eventNameArray = new ArrayList();
    for (int iX=0; iX<newEventIds.size(); iX++) {
		//YK : Modified for Bug #7513 
    	HashMap eventNameMap = new HashMap();
        if (((Integer)eventIds.get(iX)).intValue() == 0) { continue; }
        eventNameMap.put("event", (String)eventNames.get(iX));
        eventNameMap.put("eventId", String.valueOf(eventIds.get(iX)));
        eventNameMap.put("key", "e"+String.valueOf(newEventIds.get(iX)));
		eventNameArray.add(eventNameMap);
    }

    //Yk: Modified for Enhancement :PCAL-19743 - Ends
    jsObj.put("events", jsEvents);
    jsObj.put("protocolName", protocolName);
    jsObj.put("calStatus", calStatus);
    jsObj.put("noOfVisits", noOfVisits);
    
    JSONArray jsColArray = new JSONArray();
    {
    	JSONObject jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "event");
    	jsObjTemp1.put("label", LC.L_Event/*Event*****/);
    	jsColArray.put(jsObjTemp1);
    	JSONObject jsObjTemp2 = new JSONObject();
    	jsObjTemp2.put("key", "eventId");
    	jsObjTemp2.put("label", LC.L_EventId/*Event ID*****/);
    	jsColArray.put(jsObjTemp2);
    }
    JSONArray jsDisplacements = new JSONArray();
    for (int iX=0; iX<noOfVisits; iX++) {
        JSONObject jsObj1 = new JSONObject();
        jsObj1.put("key", "v"+String.valueOf(visitIds.get(iX)));
        jsObj1.put("value", visitDisplacements.get(iX));
        jsDisplacements.put(jsObj1);
        JSONObject jsObjTemp = new JSONObject();
        jsObjTemp.put("key", "v"+(visitIds.get(iX)));
        StringBuffer sb1 = new StringBuffer((String)visitNames.get(iX));
        sb1.append(" <input type='checkbox' title=\""+LC.L_Edit+"\" ");
        sb1.append("name='").append("all_v").append(visitIds.get(iX)).append("'");
        sb1.append("id='").append("all_v").append(visitIds.get(iX)).append("'");
        sb1.append(" onclick='VELOS.dataGrid.checkAll(\"v").append(visitIds.get(iX)).append("\");'");
        sb1.append("> ");
        sb1.append("&nbsp;<img src=\"./images/edit_icon.png\" alt=\"\"  class=\"headerImage\" "); ////YK: Added for PCAL-20461 - Adds edit image with each visit in the YUI datagrid 
        sb1.append(" onclick='loadModifySequenceDialog(\"").append(calStatus).append("\", \"").append(pgRight).append("\",\"").append(calledFrom).append("\", \"").append(protocolId).append("\",\"").append(calassoc).append("\",\"").append(visitIds.get(iX)).append("\");'");
        sb1.append(" onmouseover=\"return overlib('"+MC.M_ClkChgSeq_HideEvt/*Click to change sequence and hide/unhide Events*****/+"',CAPTION,'"+LC.L_EdtVst_Evt/*Edit Visit Events*****/+"');\" ");
        sb1.append(" onmouseout=\"return nd();\" ");
        sb1.append("/> ");
        jsObjTemp.put("label", sb1.toString());
        jsObjTemp.put("seq", "v"+(iX+1));
        jsColArray.put(jsObjTemp);
    }
    jsObj.put("displacements", jsDisplacements);
    jsObj.put("colArray", jsColArray);
    
    if (eventOfflines != null) {
        JSONArray offlineKeys = new JSONArray();
        JSONArray offlineValues = new JSONArray();
        for (int iX=0; iX<eventOfflines.size(); iX++) {
        	if (((Integer)eventIds.get(iX)).intValue() == 0
        	        || ((Integer)eventVisitIds.get(iX)).intValue() == 0
        	        || eventOfflines.get(iX) == null) { continue; }
            JSONObject jsObjTemp1 = new JSONObject();
        	jsObjTemp1.put("key", "e"+eventIds.get(iX)+"v"+eventVisitIds.get(iX));
        	offlineKeys.put(jsObjTemp1);
            JSONObject jsObjTemp2 = new JSONObject();
        	jsObjTemp2.put("value", eventOfflines.get(iX));
        	offlineValues.put(jsObjTemp2);
        }
        jsObj.put("offlineKeys", offlineKeys);
        jsObj.put("offlineValues", offlineValues);
    }
    
	ArrayList dataList = new ArrayList();
    for (int iX=0; iX<eventIds.size(); iX++) {
        if (((Integer)eventIds.get(iX)).intValue() < 1) { continue; }
        if (dataList.size() == 0) {
            HashMap map1 = new HashMap();
            map1.put("event", (String)eventNames.get(iX));
            map1.put("eventId", eventIds.get(iX));
            map1.put("v"+eventVisitIds.get(iX), "Y");
            dataList.add(map1);
        } else {
            HashMap map1 = (HashMap)dataList.get(dataList.size()-1);
            if (((Integer)map1.get("eventId")).intValue() == ((Integer)eventIds.get(iX)).intValue()) {
                map1.put("v"+eventVisitIds.get(iX), "Y");
                dataList.set(dataList.size()-1, map1);
            } else {
                HashMap map2 = new HashMap();
                map2.put("event", (String)eventNames.get(iX));
                map2.put("eventId", eventIds.get(iX));
                map2.put("v"+eventVisitIds.get(iX), "Y");
                dataList.add(map2);
            }
        }
    }
    
    //Yk: Modified for Enhancement :PCAL-19743
	// YK : Added for Bug#7470
    String sortOrder =(String)request.getParameter("sortOrder");
   if(!sortOrder.equalsIgnoreCase("undefined") && !sortOrder.equals("")) 
   {
	   sortOrder = (sortOrder.equals("asc")?sortOrder:"desc");
	    if(sortOrder.equalsIgnoreCase("desc"))
	    {
	   		dataList = eventassocB.sortListByKeywordDesc(dataList, "event");
	    }else{
	    	dataList = eventassocB.sortListByKeyword(dataList, "event");
	    }
   }
    
    JSONArray jsEventVisits = new JSONArray();
    for (int iX=0; iX<dataList.size(); iX++) {
        jsEventVisits.put(EJBUtil.hashMapToJSON((HashMap)dataList.get(iX)));
    }
    //YK : Modified for Bug #7513 
    JSONArray jsEventVisitIds = new JSONArray();
    for (int iX=0; iX<eventVisitIdArray.size(); iX++) {
    	jsEventVisitIds.put(EJBUtil.hashMapToJSON((HashMap)eventVisitIdArray.get(iX)));
    }
    JSONArray jsEventName = new JSONArray();
    for (int iX=0; iX<eventNameArray.size(); iX++) {
    	jsEventName.put(EJBUtil.hashMapToJSON((HashMap)eventNameArray.get(iX)));
    }
    
    jsObj.put("dataArray", jsEventVisits);
    jsObj.put("eventNames", jsEventName);
    jsObj.put("eventVisitIds", jsEventVisitIds);
    jsObj.put("eventVisitCombinations", eventVisitCombinations);
    
    out.println(jsObj.toString());
    // <html><head></head><body></body></html>
%>

<%! // Define Java functions here
private String createCheckbox(String eventId, String visitId, boolean checked) {
	StringBuffer sb = new StringBuffer();
	sb.append("<input type='checkbox' ");
	sb.append(" name='e").append(eventId).append("v").append(visitId).append("' ");
	sb.append(" id='e").append(eventId).append("v").append(visitId).append("' ");
	if (checked) {
	    sb.append(" checked ");
	}
	sb.append(" >");
	return sb.toString();
}
%>
