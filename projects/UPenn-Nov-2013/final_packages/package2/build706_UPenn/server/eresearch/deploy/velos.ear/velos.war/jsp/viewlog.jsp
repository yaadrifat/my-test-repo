<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Log_File%><%--Log File*****--%></title>

<%@ page import="com.velos.eres.service.util.*,java.util.*,com.velos.eres.business.common.*,com.velos.impex.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<head>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

</head>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import = "java.util.*,com.velos.eres.business.common.*"%>

<body style="overflow:auto">
<br>

<%
	HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession))
	{

%>	
		<jsp:include page="sessionlogging.jsp" flush="true"/>
 <%   

		String src = "";
		    	
		String expId = null ;
		String logData = null;
		long rowsReturned  = 0;
		
		BrowserRows brObj = new BrowserRows(); 
		
		expId = request.getParameter("expId");
		brObj = DBLogger.getLogFileData(EJBUtil.stringToNum(expId));
		rowsReturned = brObj.getRowReturned();
		
			
	%>
	  <table width="100%" border = "1" >
       <tr> 
        	<th width="20%"><%=LC.L_Timestamp_Upper%><%--TIMESTAMP*****--%></th>
	        <th width="10%"><%=LC.L_Severity_Upper%><%--SEVERITY*****--%></th>
	        <th width="10%"><%=LC.L_Code_Upper%><%--CODE*****--%></th>
			<th width="50%"><%=LC.L_Description_Upper%><%--DESCRIPTION*****--%></th>
			<th width="10%"><%=LC.L_Module_Upper%><%--MODULE*****--%></th>
      </tr>
							
	<%
	int i ; 

	String timestamp = null;
	String severity = null;
	String code = null;
	String desc = null;
	String module = null;

	   	
    for(i = 1;i<=rowsReturned;i++)
	{	
		timestamp = brObj.getBValues(i,"LOG_DATETIME")  ;
		severity =  brObj.getBValues(i,"LOG_SEVERITY")  ; 
		code =  brObj.getBValues(i,"LOG_MESSAGECODE")  ; 
		desc =  brObj.getBValues(i,"LOG_DESC")  ;
		module =  brObj.getBValues(i,"LOG_MODULE")  ;

		if ((i%2)==0) {

  %>
      <tr class="browserEvenRow"> 
        <%

		}

		else{

  %>
      <tr class="browserOddRow"> 
        <%

		}

  %>
        <td><%=timestamp%></td>
		<td> <%=severity%> </td>
        <td> <%=code%></td>
		<td> <%=desc%></td>
		<td> <%=module%></td>
		
	 </tr>
      <%

	}//for

%>
<table>
	<%
        
     }//end of if body for session 	
	
	else
	{
%>
	  <jsp:include page="timeout.html" flush="true"/>
<%
	
} //end of else of the Session 
 

%>

</body>

</html>

