<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>

<HEAD>
<%@ page import="com.velos.eres.service.util.*"%>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=windows-1252">

<TITLE><%=MC.M_PricingUsr_AccInfo%><%--Pricing and User Account Information*****--%></TITLE>

</HEAD>

<SCRIPT>

	function openwin() {

      window.open("","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=500,height=400")

;}

</SCRIPT>



<BODY style = "overflow:auto">



<Link Rel=STYLESHEET HREF="common.css" type=text/css>



<DIV class="staticDefault">
<br><br>

<table width="80%" cellspacing="0" cellpadding="0" align=center>

<tr>
<td id="researchHeadings" align="center"><%=MC.M_UsrAcc_Pricing_Info%><%--User Accounts and Pricing Information*****--%></td>
</tr>

<tr>

<td>
<br>
<br>

<b><font size = 3> <%=LC.L_Individual_Acc%><%--Individual Account*****--%> </b> </font><br><br> <%=MC.M_IndvResers_SignFree%><%--Individual researchers can sign up for a FREE account. All you need to do is*****--%>:

<UL>

<LI><%=MC.M_Fill_RegFrm%><%--Fill out the Registration form*****--%></LI>

<LI><%=MC.M_SelIndividual_AccTyp%><%--Select Individual account as your Account Type*****--%></LI>

<LI><%=LC.L_Submit%><%--Submit*****--%></LI></UL>

</td>

</tr>

<tr><td>

<%=MC.M_AftVerfVelosEmail_UnqId%><%--After verification, Velos will set up your account and notify you by email within one business day. You will be assigned a unique login ID and password, which you can use to begin taking advantage of all services available to you. See below for a list of available services for this account type.*****--%>  <A href="register.jsp"><font size=2><%=LC.L_Reg_Now%><%--Register Now*****--%></font></A>



<tr>

<td>

<br><br>
<font size = 3> <b><%=LC.L_Grp_Acc%><%--Group Account*****--%></b></font>

<br><br>
<%=MC.M_GrpResPvt_CROInvestiPcol%><%--A group of researchers who would like to connect to form a secure and private network should create a Group account. This is ideal for study teams, research sites, CROs, academic medical centers, sponsors or even a group of investigators who wish to communicate and share protocol information.*****--%>



<%=MC.M_ToCreate_GrpAcc%><%--To create a Group Account*****--%>:

<UL>

<LI><%=MC.M_Fill_RegFrm%><%--Fill out the Registration form*****--%></LI>

<LI><%=MC.M_SelGrpAcc_AsAccTyp%><%--Select Group account as your Account Type*****--%></LI>

<LI><%=LC.L_Submit%><%--Submit*****--%></LI></UL>

</td></tr>

<tr><td>

</B><%=MC.M_OrgDetail_ToActvAcc%><%--A Velos representative will contact you to verify details of your organization and number of users. You will be given a pricing estimate based on that. Your account will be activated and you can begin networking with other researchers in your group within a matter of days. See below for a list of available services for this account type.*****--%>



<A href="register.jsp"><font size=2><%=LC.L_Reg_Now%><%--Register Now*****--%></font></A>

</table>



&nbsp;



<P><TABLE BORDER=1 CELLSPACING=1 CELLPADDING=7 WIDTH=500 align=center>

<TR><TD WIDTH="61%" VALIGN="TOP">&nbsp;</TD>

<TD WIDTH="21%" VALIGN="TOP">

<%=LC.L_Individual_Acc%><%--Individual Account*****--%> </TD>

<TD WIDTH="18%" VALIGN="TOP">

<%=LC.L_Grp_Acc%><%--Group Account*****--%></TD>

</TR>

<TR><TD WIDTH="61%" VALIGN="TOP">

<%=MC.M_SingleAcc_StdNews%><%--Single access account for all study data and research news*****--%></TD>

<TD WIDTH="21%" VALIGN="TOP">

&radic;</TD>

<TD WIDTH="18%" VALIGN="TOP">

&radic;</TD>

</TR>

<TR><TD WIDTH="61%" VALIGN="TOP">

<%=MC.M_AccThsd_StdList%><%--Access to over thousands of <%=LC.Std_Study_Lower%> summary listings*****--%></TD>

<TD WIDTH="21%" VALIGN="TOP">

&radic;</TD>

<TD WIDTH="18%" VALIGN="TOP">

&radic;</TD>

</TR>

<TR><TD WIDTH="61%" VALIGN="TOP">

<%=MC.M_SecCommSpon_StdPcol%><%--Secure communication with sponsor or <%=LC.Std_Study_Lower%> protocol creator*****--%></TD>

<TD WIDTH="21%" VALIGN="TOP">

&radic;</TD>

<TD WIDTH="18%" VALIGN="TOP">

&radic;</TD>

</TR>

<TR><TD WIDTH="61%" VALIGN="TOP">

<%=MC.M_Ability_CreateStd%><%--Ability to create detailed <%=LC.Std_Study_Lower%>*****--%></TD>

<TD WIDTH="21%" VALIGN="TOP">

&radic;</TD>

<TD WIDTH="18%" VALIGN="TOP">

&radic;</TD>

</TR>

<TR><TD WIDTH="61%" VALIGN="TOP">

<%=MC.M_PcolSharing_RegUsr%><%--Protocol sharing with other selected registered users*****--%></TD>

<TD WIDTH="21%" VALIGN="TOP">

&radic;</TD>

<TD WIDTH="18%" VALIGN="TOP">

&radic;</TD>

</TR>

<TR><TD WIDTH="61%" VALIGN="TOP">

<%=LC.L_Std_Tracking%><%--<%=LC.Std_Study%> racking*****--%></TD>

<TD WIDTH="21%" VALIGN="TOP">

&radic;

</TD>

<TD WIDTH="18%" VALIGN="TOP">

&radic;

</TD>

</TR>

<TR><TD WIDTH="61%" VALIGN="TOP">

<%=MC.M_Instant_GenrRpts%><%--Instantaneous generation of reports*****--%></TD>

<TD WIDTH="21%" VALIGN="TOP">&radic;

</TD>

<TD WIDTH="18%" VALIGN="TOP">&radic;

</TD>

</TR>

<TR><TD WIDTH="61%" VALIGN="TOP">

<%=MC.M_Personalized_FavLnk%><%--Personalized list of favorite links*****--%>

</TD>

<TD WIDTH="21%" VALIGN="TOP">

&radic;

</TD>

<TD WIDTH="18%" VALIGN="TOP">

&radic;

</TD>

</TR>

<TR><TD WIDTH="61%" VALIGN="TOP">

<%=MC.M_SecAcc_UsrGrp%><%--Secure account shared by users in the group*****--%></TD>

<TD WIDTH="21%" VALIGN="TOP">&nbsp;</TD>

<TD WIDTH="18%" VALIGN="TOP">

&radic;

</TD>

</TR>

<TR><TD WIDTH="61%" VALIGN="TOP">

<%=MC.M_InstAccsInfo_UsrGrp%><%--Instant access to up to date information for all users within the group*****--%></TD>

<TD WIDTH="21%" VALIGN="TOP">&nbsp;</TD>

<TD WIDTH="18%" VALIGN="TOP">

&radic;

</TD>

</TR>

<TR><TD WIDTH="61%" VALIGN="TOP">

<%=MC.M_CreationStdTeam_ResData%><%--Creation of <%=LC.Std_Study_Lower%> teams that can work off the same protocol and research data*****--%></TD>

<TD WIDTH="21%" VALIGN="TOP">&nbsp;</TD>

<TD WIDTH="18%" VALIGN="MIDDLE">

&radic;

</TD>

</TR>

<TR><TD WIDTH="61%" VALIGN="TOP">

<%=MC.M_SharingInfo_AllMem%><%--Sharing of group specific information with all members of the account*****--%></TD>

<TD WIDTH="21%" VALIGN="TOP">&nbsp;</TD>

<TD WIDTH="18%" VALIGN="TOP">

&radic;

</TD>

</TR>

<TR><TD WIDTH="61%" VALIGN="TOP">

<%=MC.M_GenrAccLvl_Rpts%><%--Generation of reports at the account level across the organization or department*****--%></TD>

<TD WIDTH="21%" VALIGN="TOP">&nbsp;</TD>

<TD WIDTH="18%" VALIGN="TOP">

&radic;

</TD>

</TR>

<TR><TD WIDTH="61%" VALIGN="TOP">

<%=MC.M_PcolCal_PatSchGenr%><%--Protocol calendar and <%=LC.Pat_Patient_Lower%> schedule generator*****--%></TD>

<TD WIDTH="21%" VALIGN="TOP">

<%=LC.L_Coming_Soon%><%--Coming Soon*****--%></TD>

<TD WIDTH="18%" VALIGN="TOP">

<%=LC.L_Coming_Soon%><%--Coming Soon*****--%></TD>

</TR>

<TR><TD WIDTH="61%" VALIGN="TOP">

<%=MC.M_PatRecruit_Scrn%><%--<%=LC.Pat_Patient%> recruitment and screening*****--%></TD>

<TD WIDTH="21%" VALIGN="TOP">

<%=LC.L_Coming_Soon%><%--Coming Soon*****--%></TD>

<TD WIDTH="18%" VALIGN="TOP">

<%=LC.L_Coming_Soon%><%--Coming Soon*****--%></TD>

</TR>

</TABLE>



</P>



<div>

<jsp:include page="bottompanel.jsp" flush="true"/>



</div>



</BODY>



</DIV>



</HTML>



