<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title>

</title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="js/yui/build/yahoo/yahoo-min.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="js/yui/build/event/event-min.js"></SCRIPT>
<SCRIPT Language="javascript">

 function  validate(formobj){

 if(formobj.thread.value == 'new'){
 if (!(validate_col('Field',formobj.field))) return false;
 }

 if (formobj.cmbQueryResp){
 	if (!(validate_col('Query type',formobj.cmbQueryResp))) return false;
 }

 if (!(validate_col('Query Status',formobj.cmbStatus))) return false;

 //Commented as per EDC_AT4 
 //if (!(validate_col('Entered By',formobj.enteredByName))) return false

 //if (!(validate_col('Entered On',formobj.enteredOn))) return false



    if (!(validate_col('e-Signature',formobj.eSign))) return false


	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   	}
   	/*AK 03May-2011 Bug #5896*/
    var charCount =formobj.instructions.value.length;
    var paramArray = [charCount];
    document.getElementById("enteredCount").innerHTML=getLocalizedMessageString("L_Etr_Char",paramArray);/*document.getElementById("enteredCount").innerHTML=" Entered Characters: "+charCount;*****/
	if(parseInt(charCount)>=4000)
	{
		alert("<%=MC.M_ExcdLimit4000_PlsComt%>")/*alert("You have exceeded the limit of 4000 characters.\n Please edit your comments.")*****/
		formobj.instructions.focus();
		return false;
	}
	
	return true;

   }
	function openwinQuery() {
    windowNameQ = window.open("usersearchdetails.jsp?fname=&lname=&from=addnewquery","QueryUser","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
	windowNameQ.focus();
}
function countCharactersDefault(){
	countCharacters(document.addnewquery, 4000);
}


</SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id="fieldlibB" scope="request" class="com.velos.eres.web.fieldLib.FieldLibJB"/>

<jsp:useBean id="queryStatusB" scope="request" class="com.velos.eres.web.formQueryStatus.FormQueryStatusJB"/>
<jsp:useBean id="groupB" scope="request" class="com.velos.eres.web.group.GroupJB"/>
<jsp:useBean id="userB" scope="request" class="com.velos.eres.web.user.UserJB"/>
<!-- Virendra: Fixed Bug no. 4708, added import  com.velos.eres.service.util.StringUtil-->
<%@ page language = "java" import = "java.text.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.*,com.velos.eres.web.grpRights.GrpRightsJB,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.StringUtil"%>


<% String src;
//Virendra: Fixed Bug no. 4708, added variable addAnotherWidth
int addAnotherWidth = 0;

src= request.getParameter("srcmenu");

%>
<script>
  YAHOO.util.Event.onDOMReady(countCharactersDefault);
</script>

<body style="overflow:visible"> <%-- YK 06Feb-2011 Bug #5694 --%>
<%--Removed onload defined by YK for #5694, instead wrote onDONReady --%>
<br>
<DIV class="popDefault" id="div1">

  <%

	HttpSession tSession = request.getSession(true);

	String mode=request.getParameter("mode");
	if(mode==null)
	mode="";

 /*<!-- YK Bug#4822 19May2011  -->	*/
	int pageRight =0 ;
	pageRight = EJBUtil.stringToNum((String)session.getAttribute("formQueryRight"));
	int formId = EJBUtil.stringToNum(request.getParameter("formId"));

	String filledFormId =  request.getParameter("filledFormId");
	String from = request.getParameter("from");



	if (sessionmaint.isValidSession(tSession))
	{
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
	<jsp:include page="include.jsp" flush="true"/>
<%
	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");


	if (pageRight >=4)

	{

		String txtname="";
		String txtdesc="";
		String fldPullDown = "";
		String fieldName = "";
		String formQueryId = "";
		String queryStatusId = "";
		String enteredByName = "";


		String note = "";
		String enteredOn = "";
		String enteredBy = "";
		//String type = "";
		String typeId= "";
		String statusId = "";
		String fname = "";
		String lname = "";

		String thread = request.getParameter("thread");
		String pageFrom = request.getParameter("page");
		String entrdBy = request.getParameter("entrdBy");
		
		String studyId ="";
		
    	studyId = request.getParameter("studyId");
	 	
	 	if (StringUtil.isEmpty(studyId))
	 	{
	 		studyId="";
	 	}
     
     
     	String userIdFromSession = (String) tSession.getValue("userId");
     	UserDao uDao = new UserDao();
		uDao.setUsrIds(EJBUtil.stringToInteger(userIdFromSession));
		uDao.getUsersDetails(userIdFromSession);
		String groupId = "";
		groupId = (String) uDao.getUsrDefGrps().get(0);
		
		if(entrdBy == null)
		entrdBy = "";
		//out.println("entrdBy:"+entrdBy);
		if(pageFrom == null)
		pageFrom = "";
		if(thread == null)
		 thread = "";
		 if(thread.equals("old")){
		 fieldName= request.getParameter("fieldName");
		 formQueryId = request.getParameter("formQueryId");

		 }
		String strQueryType = request.getParameter("queryType");

		if(mode.equals("M")){
		queryStatusId = request.getParameter("queryStatusId");

		queryStatusB.setFormQueryStatusId(EJBUtil.stringToNum(queryStatusId));
		queryStatusB.getFormQueryStatusDetails();
		note = queryStatusB.getQueryNotes();
		if(note == null)
		note = "";
		strQueryType = queryStatusB.getFormQueryType() ;
		typeId= queryStatusB.getQueryTypeId();
		enteredBy = queryStatusB.getEnteredBy();

		userB.setUserId(EJBUtil.stringToNum(enteredBy));
		userB.getUserDetails();


		fname = userB.getUserFirstName();
 		if(fname == null)
		fname = "";
		lname = userB.getUserLastName();
		if(lname == null)
		lname = "";
    	enteredByName = fname + " " + lname;
		enteredOn = queryStatusB.getEnteredOn();
		if(enteredOn == null)
		enteredOn = "";
		statusId = queryStatusB.getQueryStatusId();
	}


		fieldName=(fieldName==null)?"":fieldName;
		if (fieldName.length()>0)
		 fieldName=StringUtil.decodeString(fieldName);
		String enteredId = "";


		if(strQueryType == null)
		strQueryType = "";
		int queryType = EJBUtil.stringToNum(strQueryType);
		//out.println("queryType::"+queryType);

		if((!mode.equals("M")) || (queryType == 3 && entrdBy.equals("system-generated"))){
		enteredByName = 	(String) tSession.getValue("userName");
		enteredBy= 	(String) tSession.getValue("userId");
		}
		
		CodeDao cd1 = new CodeDao();
		String roleCodePk="";
		String roleSubType="";
		
		if (! StringUtil.isEmpty(studyId) && EJBUtil.stringToNum(studyId) > 0)
		{
			ArrayList tId = new ArrayList();
			
			TeamDao teamDao = new TeamDao();
			teamDao.getTeamRights(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(userIdFromSession));
			tId = teamDao.getTeamIds();
							
			if (tId != null && tId.size() > 0)
			{
				ArrayList arRoleIds = new ArrayList();
				arRoleIds = teamDao.getTeamRoleIds();
							
				if (arRoleIds != null && arRoleIds.size() >0 )	
				{
					roleCodePk = (String) arRoleIds.get(0);
					roleSubType = cd1.getCodeSubtype(EJBUtil.stringToNum(roleCodePk));
					
					if (StringUtil.isEmpty(roleCodePk))
					{
						roleCodePk="";
					}
				}	
				else
				{
					roleCodePk ="";
				}
				
			}	
			else
			{
				roleCodePk ="";
			}
			
			//Bug #5993
			if (roleCodePk.equals("")){
				int grpId = Integer.parseInt(groupId);
				groupB.setGroupId(grpId);
				groupB.getGroupDetails();
				if (groupB.getGroupSuperUserFlag().equals("1")){
					String rolePK = groupB.getRoleId();
					if (rolePK == null || rolePK.equals("0")){
						roleCodePk = "";
						roleSubType = "";
					}else{
						roleCodePk = rolePK;
						roleSubType = cd1.getCodeSubtype(EJBUtil.stringToNum(roleCodePk));
					}
				}
			}
		} else
			{			
			  roleCodePk ="";
			}  
		
	 	cd1.getCodeValuesForStudyRole("query_status",roleCodePk);
		
		String statusPullDn = "";
		if(mode.equals("M"))
		statusPullDn = cd1.toPullDown("cmbStatus", EJBUtil.stringToNum(statusId), false);
		else
		statusPullDn = cd1.toPullDown("cmbStatus");

		FieldLibDao fDao = new FieldLibDao();

		//fDao = fieldlibB.getFieldsInformation(formId);
		// Only edit and multiple choice type fields are seen
		fDao = fieldlibB.getEditMultipleFields(formId);

		ArrayList fldNames = fDao.getFldName();
		ArrayList fieldIds= fDao.getFieldLibId();

		fldPullDown=EJBUtil.createPullDown("field", 0, fieldIds, fldNames);
		
		
		CodeDao cd2 = new CodeDao();
		if(queryType ==2 || queryType == 1)
		cd2.getCodeValues("form_query");
		else
		cd2.getCodeValues("form_resp");
		
		String queryRespPulln = "";
		if(mode.equals("M"))
		 queryRespPulln = cd2.toPullDown("cmbQueryResp", EJBUtil.stringToNum(typeId), false);
		 else
		 queryRespPulln = cd2.toPullDown("cmbQueryResp");
		 if(!mode.equals("M"))

		 //JM: 11Mar2009
		  
		 enteredOn = DateUtil.dateToString(new Date());



%>

  <P class="defcomments"><b>
 <%-- YK 03DEC-- EDC_AT3/4 REQ--%>
  <%if(mode.equals("M")){
			    
			if(strQueryType.equals("2")){%>
			<%=LC.L_FrmQry_View%><%--Form Query >> View*****--%>
			<%}
			if(strQueryType.equals("3")){%>
			<%=LC.L_QryResp_View%><%--Query Responses >> View*****--%>
			<%}
	}else{
			if(strQueryType.equals("2")){%>
			<%=LC.L_FrmQry_AddNew%><%--Form Query >> Add New*****--%>
			<%}
			if(strQueryType.equals("3")){%>
			<%=MC.M_QryResp_AddNew%><%--Query Responses >> Add New*****--%>
			<%}
	}%>
 <%-- YK 03DEC-- EDC_AT3/4 REQ--%>
	 </b> </P>

  <Form name="addnewquery" id="addnewqry" method="post" action="updateaddnewquery.jsp" onsubmit="if (validate(document.addnewquery)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}" >

  <table width="99%" cellspacing="0" cellpadding="0">
  <%-- YK 03DEC-- EDC_AT3/4 REQ--%>
  <%if(!thread.equals("new")){%>
  <tr height="20">
  <td width="18%"><%=LC.L_Query_Id%><%--Query ID*****--%></td>    <%-- YK 17DEC-- BUG#5660--%>
  <td width="80%"><%=formQueryId%></td>
  <%-- YK 03DEC-- EDC_AT3/4 REQ--%>
  </tr>
  <%}%>	
 
  <tr height="20">
  <td > <%=LC.L_Fld_Name%><%--Field Name*****--%> <FONT class="Mandatory">* </FONT></td>
  <td >
  <%if(thread.equals("new")){%>
  <%=fldPullDown%>
  <%}else{%>
  <%=fieldName%>
  <%}%>	
  </td>
  </tr>
<%if (!studyId.equals("")){ %>
	<tr>
	<%if(queryType ==2 || queryType ==1){%>
		<td ><%=LC.L_Query_Type%><%--Query Type*****--%><FONT class="Mandatory">* </FONT>  </td>
		<td><%=queryRespPulln%></td>
	<%}else{%>
		<%if (roleSubType.equals("role_coord")){ %>
		 <td > <%=LC.L_Response%><%--Response*****--%><FONT class="Mandatory">* </FONT>  </td>
		 <td><%=queryRespPulln%></td>
	  	<%} %>
	<%}%>
	</tr>
<%} else {%>
	<tr>
		  <%if(queryType ==2 || queryType ==1){%>
		  <td ><%=LC.L_Query_Type%><%--Query Type*****--%><FONT class="Mandatory">* </FONT>  </td>
		  <%}else{%>
		  <td > <%=LC.L_Response%><%--Response*****--%><FONT class="Mandatory">* </FONT>  </td>
		  <%}%>
		  <td><%=queryRespPulln%></td>
	  </tr>
<%} %>
<%-- YK 03DEC-- EDC_AT3/4 REQ--%>
  <tr>
	<td > <%=LC.L_Query_Status%><%--Query Status*****--%>  <FONT class="Mandatory">* </FONT>  </td>
  	<td ><%=statusPullDn%></td>
  </tr>
      <%-- YK 03DEC-- EDC_AT3/4 REQ--%>
		<%if(mode.equals("M")){%> 
		<tr>     
        <td > <%=LC.L_Entered_By%><%--Entered By*****--%>  <FONT class="Mandatory">* </FONT></td>
        <td >
		<input type="hidden" name="enteredById" value=<%=enteredBy%>>
          <input type="text" name="enteredByName" size = 20 MAXLENGTH = 100 value='<%=enteredByName%>' READONLY>&nbsp
		  <A href="#" onclick="return openwinQuery()"><%=LC.L_Select_User%><%--Select User*****--%></A>
        </td>
         </tr>
        <%}else {%>
       
		<input type="hidden" name="enteredById" value=<%=enteredBy%>>
          <input type="hidden" name="enteredByName" size = 20 MAXLENGTH = 100 value='<%=enteredByName%>' READONLY>&nbsp
         <%} %>
     
      
      <%if(mode.equals("M")){%>
       <tr>
<%-- INF-20084 Datepicker-- AGodara --%>
        <td> <%=LC.L_Entered_On%><%--Entered On*****--%>  <FONT class="Mandatory">* </FONT></td>
        <td>
        	<input type="text" name="enteredOn" class="datefield" size = 20 MAXLENGTH = 100 value='<%=enteredOn%>' READONLY>
       	</td>
       </tr>
        <%}else {%>
       <input type="hidden" name="enteredOn" size = 20 MAXLENGTH = 100 value='<%=enteredOn%>' READONLY>
        <%} %>
	<%-- YK 03DEC-- EDC_AT3/4 REQ--%>	
		<tr>
		<td > <%=LC.L_Comments%><%--Comments*****--%> </td>
		 <td >
			 <textarea name="instructions" cols="75" rows="4"  MAXLENGTH=500 ><%=note%></textarea>  <div id="charCount"><%=LC.L_CharAllwd_4000%><%--Characters allowed: 4000*****--%></div> <div id="enteredCount"> <div></div>  <%-- YK 17DEC-- EDC_AT4--%> <%-- YK 06Feb-2011 Bug #5694 --%>
        </td>



  </tr>
  </table>
  <br>
<table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cccccc">
<tr>
	<td align=right>
		<%if(pageFrom.equals("H")){%>
		<A type="submit" href= "formQueryHistory.jsp?studyId=<%=studyId%>&formId=<%=formId%>&from=<%=from%>&filledFormId=<%=filledFormId%>&formQueryId=<%=formQueryId%>&fieldName=<%=StringUtil.encodeString(fieldName)%>"><%=LC.L_Back%></A> <!-- YK Bug#4822 19May2011  -->	
		<%}else{%>
		<A type="submit" href= "addeditquery.jsp?studyId=<%=studyId%>&formId=<%=formId%>&from=<%=from%>&filledFormId=<%=filledFormId%>"><%=LC.L_Back%></A><!-- YK Bug#4822 19May2011  -->	
		<%}%>
		<!--<button onclick="window.history.back();"><%=LC.L_Back%></button>-->
	</td>
	<!-- Virendra: Fixed Bug no. 4708, added td with addAnotherWidth  -->
	<td bgcolor="<%=StringUtil.eSignBgcolor%>" valign="baseline" align="left" width="85%" <%=addAnotherWidth > 0 ? "" : "colspan=5" %>>
		<jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="addnewqry"/>
			<jsp:param name="showDiscard" value="N"/>
			<jsp:param name="noBR" value="Y"/>
		</jsp:include>
	</td>
</tr>
</table>
<%if(pageFrom.equals("H")){%>
<A href= "addeditquery.jsp?studyId=<%=studyId%>&formId=<%=formId%>&from=<%=from%>&filledFormId=<%=filledFormId%>"><%=MC.M_BackTo_QryBrowser%><%--Back to main query browser*****--%></A><!-- YK Bug#4822 19May2011  -->	
<%}%>


<input type="hidden" name="thread" value="<%=thread%>">
<input type="hidden" name="filledFormId" value="<%=filledFormId%>">
<input type="hidden" name="formId" value="<%=formId%>">
<input type="hidden" name="from" value="<%=from%>">
<input type="hidden" name="formQueryId" value="<%=formQueryId%>">
<input type="hidden" name="queryType" value="<%=queryType%>">
<input type="hidden" name="queryStatusId" value="<%=queryStatusId%>">
<input type="hidden" name="fieldName" value="<%=fieldName%>">
<input type="hidden" name="mode" value="<%=mode%>">
<input type="hidden" name="studyId" value="<%=studyId%>">



</Form>
  <%

	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>

</body>

</html>


