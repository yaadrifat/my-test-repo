<!-- 
This JSP will be included in gadgetHome.jsp. All non-local javascript variables and functions 
will be global in the mother JSP. Therefore, make sure to prefix all non-local variables
with the gadget ID. All functions should be namespaced to start with gadget ID.
 -->

<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%> 
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%
if (!sessionmaint.isValidSession(request.getSession(false))) {
	return;
}
%>
<style type="text/css">
  input {
      font-family: sans-serif;                
  }
</style>
<script>
// Declare all functions here
var gadgetQuickAccess = {
	acctLinks: {
		data: {},
		deleteLink: {},  // do the actual delete
		formatAcctLinkHTML: {},
		reloadAcctLink: {},
		saveNewLink: {}
	},
	myLinks: {
		data: {},
		deleteLink: {},  // do the actual delete
		formatMyLinkHTML: {},
		reloadMyLink: {},
		formatData: {},
		saveNewLink: {}
	},
	bothLinks: { // functions shared between myLinks and acctLinks
		truncateLinkName: {},
		screenActionForLinks: {},
		openNewLink: {},
		addNewLink: {},
		cancelNewLink: {},
		editLink: {},
		delLink: {},  // Open delete dialog
		clearNewLink: {},
		clearNewLinkErrMsg: {},
		clearLinkESign: {},
		clearLinkDeleteESign: {},
		validateNewLink: {},
		validateDeleteLink: {}
	},
	screenAction: {}, // add actions to the entire screen of this gadget
	submitStudy: {},
	submitStudyAdvanced: {},
	submitPatient: {},
	submitPatientStudy: {},
	beforeSubmitStudy:{},
	beforeSubmitPatient: {},
	beforeSubmitPatientStudy: {}
};

// Implement the functions below
gadgetQuickAccess.myLinks.reloadMyLink = function() {
	try {
    	jQuery.ajax({
    		url:'gadgetQuickAccess_getMyLinks',
    		async: true,
    		cache: false,
    		success: function(resp) {
    			resetSessionWarningOnTimer();
    			gadgetQuickAccess.myLinks.data.array = resp.array;
    			gadgetQuickAccess.myLinks.data.addMore = resp.addMore;
    			gadgetQuickAccess.myLinks.formatMyLinkHTML();
    			gadgetQuickAccess.bothLinks.screenActionForLinks('myLink');
    		}
    	});
	} catch(e) {}
}
gadgetQuickAccess.myLinks.formatMyLinkHTML = function () {
	var str1 = '<table width="100%" cellspacing="0" >';
	for(var iX=0; iX<gadgetQuickAccess.myLinks.data.array.length; iX++) {
		if (iX > 9) { break; }
		str1 += "<tr id='gadgetQuickAccess_myLink-row-"+iX+"'><td>";
		str1 += "<a target=\"_blank\" id=\"gadgetQuickAccess_myLink"+iX+"\"";
		str1 += " href=\""+gadgetQuickAccess.myLinks.data.array[iX].uri+"\"";
		str1 += " title=\""+gadgetQuickAccess.myLinks.data.array[iX].uri+"\">";
		str1 += gadgetQuickAccess.bothLinks.truncateLinkName(gadgetQuickAccess.myLinks.data.array[iX].desc);
		str1 += "</td><td nowrap='nowrap' align='right'>";
		str1 += "<a style=\"opacity:0; filter:alpha(opacity=0);\" href=\"javascript:gadgetQuickAccess.bothLinks.editLink('myLinks',";
		str1 += (iX+1);
		str1 += ");\" id=\"gadgetQuickAccess.myLinks.editMyLink"+iX+"\">";
		str1 += "<img id='gadgetQuickAccess_myLink-edt-img-"+iX;
		str1 += "' class='headerImage' style='border:none; opacity:0; filter:alpha(opacity=0);' title='";
		str1 += "<%=LC.L_Edt_Link_Num%>".replace("\'", "&#39;")+(iX+1);
		str1 += "' src='./images/edit.gif' border=0 />";
		str1 += "</a>&nbsp;";
		str1 += "<a style=\"opacity:0; filter:alpha(opacity=0);\" href=\"javascript:gadgetQuickAccess.bothLinks.delLink('myLinks',";
		str1 += (iX+1);
		str1 += ");\" id=\"gadgetQuickAccess_delMyLink"+iX+"\">";
		str1 += "<img id='gadgetQuickAccess_myLink-del-img-"+iX;
		str1 += "' class='headerImage' style='border:none; opacity:0; filter:alpha(opacity=0);' title='";
		str1 += "<%=LC.L_Del_Link_Num%>".replace("\'", "&#39;")+(iX+1);
		str1 += "' src='./images/delete.gif' border=0 />";
		str1 += "</a>";
		str1 += "</td></tr>";
	}
	if (gadgetQuickAccess.myLinks.data.addMore == 'true') {
		str1 += "<tr><td colspan='2' align='right'><a href='ulinkBrowser.jsp?srcmenu=tdMenuBarItem13&selectedTab=2'";
		str1 += " title='"+"<%=LC.L_More_My_Links%>"+"'>";
		str1 += "<%=LC.L_More_My_Links%>"+"</a>";
		str1 += "</td></tr>";
	}
	str1 += '</table>';
	$('gadgetQuickAccess_myLinksHTML').innerHTML = str1;
}
gadgetQuickAccess.myLinks.formatData = function() {
	var j = {};
	j.myLinksLinkUrl = jQuery.trim($('gadgetQuickAccessJB.myLinksLinkUrl').value);
	j.myLinksLinkSection = jQuery.trim($('gadgetQuickAccessJB.myLinksLinkSection').value);
	j.myLinksLinkId = jQuery.trim($('gadgetQuickAccessJB.myLinksLinkId').value);
	j.myLinksLinkDisplay = jQuery.trim($('gadgetQuickAccessJB.myLinksLinkDisplay').value);
	j.eSign = jQuery.trim($('gadgetQuickAccessJB.myLinksESign').value);
	if ($('gadgetQuickAccessJB.myLinksLinkUrl').value)
		$('gadgetQuickAccessJB.myLinksLinkUrl').value = jQuery.trim($('gadgetQuickAccessJB.myLinksLinkUrl').value);
	if ($('gadgetQuickAccessJB.myLinksLinkSection').value)
		$('gadgetQuickAccessJB.myLinksLinkSection').value = jQuery.trim($('gadgetQuickAccessJB.myLinksLinkSection').value);
	if ($('gadgetQuickAccessJB.myLinksLinkId').value) 
		$('gadgetQuickAccessJB.myLinksLinkId').value = jQuery.trim($('gadgetQuickAccessJB.myLinksLinkId').value);
	if ($('gadgetQuickAccessJB.myLinksLinkDisplay').value)
		$('gadgetQuickAccessJB.myLinksLinkDisplay').value = jQuery.trim($('gadgetQuickAccessJB.myLinksLinkDisplay').value);
	if ($('gadgetQuickAccessJB.myLinksESign').value)
		$('gadgetQuickAccessJB.myLinksESign').value = jQuery.trim($('gadgetQuickAccessJB.myLinksESign').value);
	return j;
}
gadgetQuickAccess.myLinks.saveNewLink = function() {
	if (!gadgetQuickAccess.bothLinks.validateNewLink('myLinks')) { return false; }
	try {
		YUI().use('io', 'json', 'json-parse', function(Y) {
			var formData = Y.JSON.stringify(gadgetQuickAccess.myLinks.formatData());
        	Y.io('gadgetQuickAccess_saveMyLink', {
            	method: 'POST',
            	data: $j('#gadgetQuickAccess_formNewLink').serialize(),
            	sync: true,
            	on: {
                	success: function (tranId, o) {
                		resetSessionWarningOnTimer();
                    	var json = Y.JSON.parse(o.responseText);
                    	if (json.inputErrors) {
                    		var thisValidationId = 'gadgetQuickAccess_newLink';
                    		validationMessages[thisValidationId] = {};
                    		var myValidations = [];
                    		for (var key in json.inputErrors) {
                    			var errorArray = json.inputErrors[key];
                    			for (var iX=0; iX<errorArray.length; iX++) {
                    				myValidations.push(errorArray[iX]);
                    			}
                    			if (myValidations.length > 0) {
                    				validationMessages[thisValidationId][key]
                    					= myValidations;
                    			}
                    		}
                    		formatErrMsgGlobal(thisValidationId);
                    		$j('#gadgetQuickAccess_newMyLinkButtonRow').show();
                    	}  else if (json.operationErrors) {
                    		var myMessage = '';
                    		if (json.operationErrors) {
                    			myMessage = json.operationErrors[0];
                    		}
                    		$('gadgetQuickAccess_formNewLink_resultMsg').innerHTML = wrapDisplayMessage(myMessage); 
                        	$j('#gadgetQuickAccess_newMyLinkButtonRow').show();
                    	} else {
                    		$j('#gadgetQuickAccess_newMyLinkButtonRow').hide();
                    		$('gadgetQuickAccess_formNewLink_resultMsg').innerHTML = wrapDisplayMessage(M_Data_SavedSucc);
                    		setTimeout(function () {
                    			$j("#gadgetQuickAccess_myLinksNewLink").slideUp(200).fadeOut(200);
                    			gadgetQuickAccess.myLinks.reloadMyLink();
                        		$('gadgetQuickAccess_formNewLink_resultMsg').innerHTML = '';
                        		$j('#gadgetQuickAccess_newMyLinkButtonRow').show();
                        		gadgetQuickAccess.bothLinks.clearNewLink('myLinks');
                        		gadgetQuickAccess.bothLinks.clearLinkESign('myLinks');
                    		}, 500);
                    	}
                    },
                    failure: function (tranId, o) {
                		$j('#gadgetQuickAccess_newMyLinkButtonRow').show();
                		$('gadgetQuickAccess_formNewLink_resultMsg').innerHTML = wrapDisplayMessage(M_CommErr_CnctVelos);
                    }
                }
            });
		});
	} catch(e) {}
}
gadgetQuickAccess.myLinks.deleteLink = function() {
	if (!gadgetQuickAccess.bothLinks.validateDeleteLink('myLinks')) { return false; }
	var thisJ = {}
	thisJ.myLinksLinkId = $('gadgetQuickAccessJB.myLinksDeleteLinkId').value;
	thisJ.eSign = $('gadgetQuickAccessJB.myLinksDeleteESign').value;
	try {
		YUI().use('io', 'json', 'json-parse', function(Y) {
			var formData = Y.JSON.stringify(thisJ);
        	Y.io('gadgetQuickAccess_deleteMyLink', {
            	method: 'POST',
            	data: $j('#gadgetQuickAccess_formDeleteLink').serialize(),
            	sync: true,
            	on: {
                	success: function (tranId, o) {
                		resetSessionWarningOnTimer();
                    	var json = Y.JSON.parse(o.responseText);
                    	if (json.inputErrors) {
                    		var thisValidationId = 'gadgetQuickAccess.myLinks.deleteLink';
                    		validationMessages[thisValidationId] = {};
                    		var myValidations = [];
                    		for (var key in json.inputErrors) {
                    			var errorArray = json.inputErrors[key];
                    			for (var iX=0; iX<errorArray.length; iX++) {
                    				myValidations.push(errorArray[iX]);
                    			}
                    			if (myValidations.length > 0) {
                    				validationMessages[thisValidationId][key]
                    					= myValidations;
                    			}
                    		}
                    		formatErrMsgGlobal(thisValidationId);
                        	$j('#gadgetQuickAccess_deleteMyLinkButtonRow').show();
                    	} else if (json.operationErrors) {
                    		var myMessage = '';
                    		if (json.operationErrors) {
                    			myMessage = json.operationErrors[0];
                    		}
                    		$('gadgetQuickAccess_formDeleteLink_resultMsg').innerHTML = wrapDisplayMessage(myMessage); 
                        	$j('#gadgetQuickAccess_deleteMyLinkButtonRow').show();
                    	} else {
                    		$j('#gadgetQuickAccess_deleteMyLinkButtonRow').hide();
                    		$('gadgetQuickAccess_formDeleteLink_resultMsg').innerHTML = wrapDisplayMessage('<%=MC.M_Data_DelSucc%>');
                    		setTimeout(function () {
                    			$j("#gadgetQuickAccess_myLinksNewLink").slideUp(200).fadeOut(200);
                    			gadgetQuickAccess.myLinks.reloadMyLink();
                    		}, 500);
                    		setTimeout(function () {
                        		$j('#gadgetQuickAccess_deleteMyLinkButtonRow').show();
                        		$('gadgetQuickAccess_formDeleteLink_resultMsg').innerHTML = '';
                        		gadgetQuickAccess.bothLinks.clearNewLink('myLinks');
                        		gadgetQuickAccess.bothLinks.clearLinkDeleteESign('myLinks');
                    		}, 1000);
                    	}
                    },
                    failure: function (tranId, o) {
                		$('gadgetQuickAccess_formDeleteLink_resultMsg').innerHTML = wrapDisplayMessage(M_CommErr_CnctVelos);
                    	$j('#gadgetQuickAccess_deleteMyLinkButtonRow').show();
                    }
                }
            });
		});
	} catch(e) {}
}

gadgetQuickAccess.acctLinks.reloadAcctLink = function () {
	try {
    	jQuery.ajax({
    		url:'gadgetQuickAccess_getAcctLinks',
    		async: true,
    		cache: false,
    		success: function(resp) {
    			resetSessionWarningOnTimer();
    			gadgetQuickAccess.acctLinks.data.array = resp.array;
    			gadgetQuickAccess.acctLinks.data.addMore = resp.addMore;
    			gadgetQuickAccess.acctLinks.data.access = resp.access;
    			gadgetQuickAccess.acctLinks.formatAcctLinkHTML();
    			gadgetQuickAccess.bothLinks.screenActionForLinks('acctLink');
    		}
    	});
	} catch(e) {}
}
gadgetQuickAccess.acctLinks.formatAcctLinkHTML = function () {
	var str1 = '<table width="100%" cellspacing="0" >';
	for(var iX=0; iX<gadgetQuickAccess.acctLinks.data.array.length; iX++) {
		if (iX > 9) { break; }
		str1 += "<tr id='gadgetQuickAccess_acctLink-row-"+iX+"'><td>";
		str1 += "<a target=\"_blank\" id=\"gadgetQuickAccess_acctLink"+iX+"\"";
		str1 += " href=\""+gadgetQuickAccess.acctLinks.data.array[iX].uri+"\"";
		str1 += " title=\""+gadgetQuickAccess.acctLinks.data.array[iX].uri+"\">";
		str1 += gadgetQuickAccess.bothLinks.truncateLinkName(gadgetQuickAccess.acctLinks.data.array[iX].desc);
		str1 += "</td><td width='10%' nowrap='nowrap' align='right'>";
		if (gadgetQuickAccess.acctLinks.data.access != '6' && gadgetQuickAccess.acctLinks.data.access != '7') {
			str1 += "</td></tr>"; continue;
		}
		str1 += "<a style=\"opacity:0; filter:alpha(opacity=0);\" href=\"javascript:gadgetQuickAccess.bothLinks.editLink('acctLinks',";
		str1 += (iX+1);
		str1 += ");\" id=\"gadgetQuickAccess.acctLinks.editAcctLink"+iX+"\">";
		str1 += "<img id='gadgetQuickAccess_acctLink-edt-img-"+iX;
		str1 += "' class='headerImage' style='border:none; opacity:0; filter:alpha(opacity=0);' title='";
		str1 += "<%=LC.L_Edt_Link_Num%>".replace("\'", "&#39;")+(iX+1);
		str1 += "' src='./images/edit.gif' border=0 />";
		str1 += "</a>&nbsp;";
		str1 += "<a style=\"opacity:0; filter:alpha(opacity=0);\" href=\"javascript:gadgetQuickAccess.bothLinks.delLink('acctLinks',";
		str1 += (iX+1);
		str1 += ");\" id=\"gadgetQuickAccess_delAcctLink"+iX+"\">";
		str1 += "<img id='gadgetQuickAccess_acctLink-del-img-"+iX;
		str1 += "' class='headerImage' style='border:none; opacity:0; filter:alpha(opacity=0);' title='";
		str1 += "<%=LC.L_Del_Link_Num%>".replace("\'", "&#39;")+(iX+1);
		str1 += "' src='./images/delete.gif' border=0 />";
		str1 += "</a>";
		str1 += "</td></tr>";
	}
	if (gadgetQuickAccess.acctLinks.data.addMore == 'true') {
		str1 += "<tr><td colspan='2' align='right'><a href='accountlinkbrowser.jsp?srcmenu=tdMenuBarItem2&selectedTab=4'";
		str1 += " title='"+"<%=LC.L_More_Links%>"+"'>";
		str1 += "<%=LC.L_More_Links%>"+"</a>";
		str1 += "</td></tr>";
	}
	str1 += '</table>';
	if ($('gadgetQuickAccess_acctLinksHTML')) {
		$('gadgetQuickAccess_acctLinksHTML').innerHTML = str1;
	}
}
gadgetQuickAccess.acctLinks.deleteLink = function() {
	if (!gadgetQuickAccess.bothLinks.validateDeleteLink('acctLinks')) { return false; }
	try {
		YUI().use('io', 'json', 'json-parse', function(Y) {
        	Y.io('gadgetQuickAccess_deleteAcctLink', {
            	method: 'POST',
            	data: $j('#gadgetQuickAccess_formDeleteAcctLink').serialize(),
            	sync: true,
            	on: {
                	success: function (tranId, o) {
                		resetSessionWarningOnTimer();
                    	var json = Y.JSON.parse(o.responseText);
                    	if (json.inputErrors) {
                    		var thisValidationId = 'gadgetQuickAccess.acctLinks.deleteLink';
                    		validationMessages[thisValidationId] = {};
                    		var myValidations = [];
                    		for (var key in json.inputErrors) {
                    			var errorArray = json.inputErrors[key];
                    			for (var iX=0; iX<errorArray.length; iX++) {
                    				myValidations.push(errorArray[iX]);
                    			}
                    			if (myValidations.length > 0) {
                    				validationMessages[thisValidationId][key]
                    					= myValidations;
                    			}
                    		}
                    		formatErrMsgGlobal(thisValidationId);
                        	$j('#gadgetQuickAccess_deleteAcctLinkButtonRow').show();
                    	} else if (json.operationErrors) {
                    		var myMessage = '';
                    		if (json.operationErrors) {
                    			myMessage = json.operationErrors[0];
                    		}
                    		$('gadgetQuickAccess_formDeleteAcctLink_resultMsg').innerHTML = wrapDisplayMessage(myMessage); 
                        	$j('#gadgetQuickAccess_deleteAcctLinkButtonRow').show();
                    	} else {
                    		$j('#gadgetQuickAccess_deleteAcctLinkButtonRow').hide();
                    		$('gadgetQuickAccess_formDeleteAcctLink_resultMsg').innerHTML = wrapDisplayMessage('<%=MC.M_Data_DelSucc%>');
                    		setTimeout(function () {
                    			$j("#gadgetQuickAccess_acctLinksNewLink").slideUp(200).fadeOut(200);
                    			gadgetQuickAccess.acctLinks.reloadAcctLink();
                    		}, 500);
                    		setTimeout(function () {
                        		$j('#gadgetQuickAccess_deleteAcctLinkButtonRow').show();
                        		$('gadgetQuickAccess_formDeleteAcctLink_resultMsg').innerHTML = '';
                        		gadgetQuickAccess.bothLinks.clearNewLink('acctLinks');
                        		gadgetQuickAccess.bothLinks.clearLinkDeleteESign('acctLinks');
                    		}, 1000);
                    	}
                    },
                    failure: function (tranId, o) {
                		$('gadgetQuickAccess_formDeleteAcctLink_resultMsg').innerHTML = wrapDisplayMessage(M_CommErr_CnctVelos);
                    	$j('#gadgetQuickAccess_deleteAcctLinkButtonRow').show();
                    }
                }
            });
		});
	} catch(e) {}
}
gadgetQuickAccess.acctLinks.saveNewLink = function() {
	if (!gadgetQuickAccess.bothLinks.validateNewLink('acctLinks')) { return false; }
	try {
		YUI().use('io', 'json', 'json-parse', function(Y) {
        	Y.io('gadgetQuickAccess_saveAcctLink', {
            	method: 'POST',
            	data: $j('#gadgetQuickAccess_formNewAcctLink').serialize(),
            	sync: true,
            	on: {
                	success: function (tranId, o) {
                		resetSessionWarningOnTimer();
                    	var json = Y.JSON.parse(o.responseText);
                    	if (json.inputErrors) {
                    		var thisValidationId = 'gadgetQuickAccess.acctLinks.newLink';
                    		validationMessages[thisValidationId] = {};
                    		var myValidations = [];
                    		for (var key in json.inputErrors) {
                    			var errorArray = json.inputErrors[key];
                    			for (var iX=0; iX<errorArray.length; iX++) {
                    				myValidations.push(errorArray[iX]);
                    			}
                    			if (myValidations.length > 0) {
                    				validationMessages[thisValidationId][key]
                    					= myValidations;
                    			}
                    		}
                    		formatErrMsgGlobal(thisValidationId);
                    		$j('#gadgetQuickAccess_newAcctLinkButtonRow').show();
                    	}  else if (json.operationErrors) {
                    		var myMessage = '';
                    		if (json.operationErrors) {
                    			myMessage = json.operationErrors[0];
                    		}
                    		$('gadgetQuickAccess_formNewAcctLink_resultMsg').innerHTML = wrapDisplayMessage(myMessage); 
                        	$j('#gadgetQuickAccess_newAcctLinkButtonRow').show();
                    	} else {
                    		$j('#gadgetQuickAccess_newAcctLinkButtonRow').hide();
                    		$('gadgetQuickAccess_formNewAcctLink_resultMsg').innerHTML = wrapDisplayMessage(M_Data_SavedSucc);
                    		setTimeout(function () {
                    			$j("#gadgetQuickAccess_acctLinksNewLink").slideUp(200).fadeOut(200);
                    			gadgetQuickAccess.acctLinks.reloadAcctLink();
                        		$('gadgetQuickAccess_formNewAcctLink_resultMsg').innerHTML = '';
                        		$j('#gadgetQuickAccess_newAcctLinkButtonRow').show();
                        		gadgetQuickAccess.bothLinks.clearNewLink('acctLinks');
                        		gadgetQuickAccess.bothLinks.clearLinkESign('acctLinks');
                    		}, 500);
                    	}
                    },
                    failure: function (tranId, o) {
                		$j('#gadgetQuickAccess_newAcctLinkButtonRow').show();
                		$('gadgetQuickAccess_formNewAcctLink_resultMsg').innerHTML = wrapDisplayMessage(M_CommErr_CnctVelos);
                    }
                }
            });
		});
	} catch(e) {}
}

gadgetQuickAccess.bothLinks.truncateLinkName = function(str) {
	if (!str) { return ''; }
	var newText = truncateLongName(str, 25);
	if (str.length > 25) {
		newText = htmlEncode(newText)+"</a>&nbsp;<img title='"+htmlEncode(str)
				+ "' src='./images/More.png' style='width:20px; height:10px;' border=0 />";
	} else {
		newText = htmlEncode(newText)+"</a>";
	}
	return newText;
}
gadgetQuickAccess.bothLinks.openNewLink = function(whichLink) {
	if (whichLink != "acctLinks" && whichLink != "myLinks") { return; }
	$j("#gadgetQuickAccess_"+whichLink+"NewLink").slideDown(200).fadeIn(200);
	gadgetQuickAccess.bothLinks.clearNewLinkErrMsg(whichLink);
	gadgetQuickAccess.bothLinks.clearLinkESign(whichLink);
}
gadgetQuickAccess.bothLinks.cancelNewLink = function(whichLink) {
	if (whichLink != "acctLinks" && whichLink != "myLinks") { return; }
	gadgetQuickAccess.bothLinks.clearNewLink(whichLink);
	gadgetQuickAccess.bothLinks.clearLinkESign(whichLink);
	gadgetQuickAccess.bothLinks.clearLinkDeleteESign(whichLink);
	$j("#gadgetQuickAccess_"+whichLink+"NewLink").slideUp(200).fadeOut(200);
}
gadgetQuickAccess.bothLinks.screenActionForLinks = function(whichLink) {
	if (whichLink != "acctLink" && whichLink != "myLink") { return; }
	YUI().use('node', 'event-mouseenter', function(Y) {
		var onMouseoverDelLink = function(e) {
			var trNum = -1;
			if (e.target.get('tagName') == 'TD') {
				var pos1 = ('gadgetQuickAccess_'+whichLink+'-row-').length;
				trNum = e.target.get('parentNode').get('id').substring(pos1,pos1+1);
			} else if (e.target.get('tagName') == 'A') {
				var pos1 = ('gadgetQuickAccess_'+whichLink+'-row-').length;
				trNum = e.target.get('parentNode').get('parentNode').get('id').substring(pos1,pos1+1);
			} else if (e.target.get('tagName') == 'IMG') {
				var pos1 = ('gadgetQuickAccess_'+whichLink+'-edt-img-').length;
				trNum = e.target.get('id').substring(pos1,pos1+1);
			}
			if (trNum == -1) { return; }
			$j('#gadgetQuickAccess_'+whichLink+'-row-'+trNum).addClass('gdt-highlighted-row');
			if (Y.one('#gadgetQuickAccess_'+whichLink+'-edt-img-'+trNum)) {
				Y.one('#gadgetQuickAccess_'+whichLink+'-edt-img-'+trNum).setStyle('opacity', '1');
				Y.one('#gadgetQuickAccess_'+whichLink+'-edt-img-'+trNum).get('parentNode').setStyle('opacity', '1');
			}
			if (Y.one('#gadgetQuickAccess_'+whichLink+'-del-img-'+trNum)) {
				Y.one('#gadgetQuickAccess_'+whichLink+'-del-img-'+trNum).setStyle('opacity', '1');
				Y.one('#gadgetQuickAccess_'+whichLink+'-del-img-'+trNum).get('parentNode').setStyle('opacity', '1');
			}
		}
		var onMouseoutDelLink = function(e) {
			var trNum = -1;
			if (e.target.get('tagName') == 'TD') {
				var pos1 = ('gadgetQuickAccess_'+whichLink+'-row-').length;
				trNum = e.target.get('parentNode').get('id').substring(pos1,pos1+1);
			} else if (e.target.get('tagName') == 'A') {
				var pos1 = ('gadgetQuickAccess_'+whichLink+'-row-').length;
				trNum = e.target.get('parentNode').get('parentNode').get('id').substring(pos1,pos1+1);
			} else if (e.target.get('tagName') == 'IMG') {
				var pos1 = ('gadgetQuickAccess_'+whichLink+'-edt-img-').length;
				trNum = e.target.get('id').substring(pos1,pos1+1);
			}
			if (trNum == -1) { return; }
			$j('#gadgetQuickAccess_'+whichLink+'-row-'+trNum).removeClass('gdt-highlighted-row');
			if (Y.one('#gadgetQuickAccess_'+whichLink+'-edt-img-'+trNum)) {
				Y.one('#gadgetQuickAccess_'+whichLink+'-edt-img-'+trNum).setStyle('opacity', '0');
				Y.one('#gadgetQuickAccess_'+whichLink+'-edt-img-'+trNum).get('parentNode').setStyle('opacity', '0');
			}
			if (Y.one('#gadgetQuickAccess_'+whichLink+'-del-img-'+trNum)) {
				Y.one('#gadgetQuickAccess_'+whichLink+'-del-img-'+trNum).setStyle('opacity', '0');
				Y.one('#gadgetQuickAccess_'+whichLink+'-del-img-'+trNum).get('parentNode').setStyle('opacity', '0');
			}
		}
		for(var iX=0; iX<10; iX++) {
			if (!Y.one('#gadgetQuickAccess_'+whichLink+'-row-'+iX)) { continue; }
			Y.one('#gadgetQuickAccess_'+whichLink+'-row-'+iX).on('mousemove', onMouseoverDelLink);
			Y.one('#gadgetQuickAccess_'+whichLink+'-row-'+iX).on('mouseenter', onMouseoverDelLink);
			Y.one('#gadgetQuickAccess_'+whichLink+'-row-'+iX).on('mouseover', onMouseoverDelLink);
			Y.one('#gadgetQuickAccess_'+whichLink+'-row-'+iX).on('mouseleave', onMouseoutDelLink);
			Y.one('#gadgetQuickAccess_'+whichLink+'-row-'+iX).on('mouseout', onMouseoutDelLink);
			var whichLinkFirstCap = whichLink.charAt(0).toUpperCase()+whichLink.substring(1);
			if (!Y.one('#gadgetQuickAccess_del'+whichLinkFirstCap+iX)) { continue; }
			Y.one('#gadgetQuickAccess_del'+whichLinkFirstCap+iX).setStyle('opacity', '0');
			Y.one('#gadgetQuickAccess_del'+whichLinkFirstCap+iX).setStyle('opacity', '0');
		}
	});
}
gadgetQuickAccess.bothLinks.editLink = function(whichLink, num) {
	if (whichLink != "acctLinks" && whichLink != "myLinks") { return; }
	gadgetQuickAccess.bothLinks.openNewLink(whichLink);
	$j('#gadgetQuickAccess_'+whichLink+'FormNewLinkHeader').hide();
	$j('#gadgetQuickAccess_'+whichLink+'FormEditLinkHeader').show();
	$j('#gadgetQuickAccess_'+whichLink+'FormDeleteLinkDiv').hide();
	$('gadgetQuickAccess_'+whichLink+'FormEditLinkHeadderLabel').innerHTML = '<b><%=LC.L_Edt_Link_Num%>'+num+'</b>';
	$j('#gadgetQuickAccess_'+whichLink+'FormNewLinkDiv').slideDown(200).fadeIn(200);
	$('gadgetQuickAccessJB.'+whichLink+'LinkDisplay').focus();
	$('gadgetQuickAccessJB.'+whichLink+'LinkUrl').value = gadgetQuickAccess[whichLink].data.array[num-1].uri;
	$('gadgetQuickAccessJB.'+whichLink+'LinkDisplay').value = gadgetQuickAccess[whichLink].data.array[num-1].desc;
	$('gadgetQuickAccessJB.'+whichLink+'LinkSection').value = gadgetQuickAccess[whichLink].data.array[num-1].section ?
			gadgetQuickAccess[whichLink].data.array[num-1].section : '';
	$('gadgetQuickAccessJB.'+whichLink+'LinkId').value = gadgetQuickAccess[whichLink].data.array[num-1].lnkid;
}
gadgetQuickAccess.bothLinks.delLink = function(whichLink, num) {
	if (whichLink != "acctLinks" && whichLink != "myLinks") { return; }
	gadgetQuickAccess.bothLinks.openNewLink(whichLink);
	var moreString = '';
	if (gadgetQuickAccess[whichLink].data.array[num-1].desc.length > 19) { moreString = '...'; }
	$j('#gadgetQuickAccess_'+whichLink+'FormNewLinkDiv').hide();
	$j('#gadgetQuickAccess_'+whichLink+'FormDeleteLinkDiv').slideDown(200).fadeIn(200);
	gadgetQuickAccess.bothLinks.clearLinkDeleteESign(whichLink);
	$('gadgetQuickAccess_'+whichLink+'FormDeleteLinkHeadderLabel').innerHTML = '<b><%=LC.L_Del_Link_Num%>'+num+': '+
		htmlEncode(truncateLongName(gadgetQuickAccess[whichLink].data.array[num-1].desc, 19))+
		moreString+'</b>';
	$('gadgetQuickAccessJB.'+whichLink+'DeleteLinkId').value = gadgetQuickAccess[whichLink].data.array[num-1].lnkid;
	$('gadgetQuickAccessJB.'+whichLink+'DeleteESign').focus();
}
gadgetQuickAccess.bothLinks.addNewLink = function(whichLink) {
	if (whichLink != "acctLinks" && whichLink != "myLinks") { return; }
	gadgetQuickAccess.bothLinks.openNewLink(whichLink);
	$j('#gadgetQuickAccess_'+whichLink+'FormDeleteLinkDiv').slideUp(200).fadeOut(200);
	$j('#gadgetQuickAccess_'+whichLink+'FormNewLinkDiv').slideDown(200).fadeIn(200);
	$j('#gadgetQuickAccess_'+whichLink+'FormNewLinkHeader').show();
	$j('#gadgetQuickAccess_'+whichLink+'FormEditLinkHeader').hide();
	gadgetQuickAccess.bothLinks.clearNewLink(whichLink);
	gadgetQuickAccess.bothLinks.clearLinkESign(whichLink);
	$('gadgetQuickAccessJB.'+whichLink+'LinkDisplay').focus();
}
gadgetQuickAccess.bothLinks.clearNewLink = function(whichLink) {
	if (whichLink != "acctLinks" && whichLink != "myLinks") { return; }
	$('gadgetQuickAccessJB.'+whichLink+'LinkUrl').value = '';
	$('gadgetQuickAccessJB.'+whichLink+'LinkDisplay').value = '';
	$('gadgetQuickAccessJB.'+whichLink+'LinkSection').value = '';
	$('gadgetQuickAccessJB.'+whichLink+'ESign').value = '';
	$('gadgetQuickAccessJB.'+whichLink+'DeleteESign').value = '';
	$('gadgetQuickAccessJB.'+whichLink+'DeleteLinkId').value = '';
	$('gadgetQuickAccessJB.'+whichLink+'LinkId').value = '';
	gadgetQuickAccess.bothLinks.clearNewLinkErrMsg(whichLink);
};
gadgetQuickAccess.bothLinks.clearNewLinkErrMsg = function(whichLink) {
	if (whichLink != "acctLinks" && whichLink != "myLinks") { return; }
	if ($('gadgetQuickAccessJB.'+whichLink+'LinkUrl_error')) { $('gadgetQuickAccessJB.'+whichLink+'LinkUrl_error').innerHTML = ''; }
	if ($('gadgetQuickAccessJB.'+whichLink+'LinkDisplay_error')) { $('gadgetQuickAccessJB.'+whichLink+'LinkDisplay_error').innerHTML = ''; }
	if ($('gadgetQuickAccessJB.'+whichLink+'LinkSection_error')) { $('gadgetQuickAccessJB.'+whichLink+'LinkSection_error').innerHTML = ''; }
	if ($('gadgetQuickAccessJB.'+whichLink+'ESign_error')) { $('gadgetQuickAccessJB.'+whichLink+'ESign_error').innerHTML = ''; }
	if ($('gadgetQuickAccessJB.'+whichLink+'DeleteESign_error')) { $('gadgetQuickAccessJB.'+whichLink+'DeleteESign_error').innerHTML = ''; }
	if (whichLink == "myLinks") {
		if ($('gadgetQuickAccess_formNewLink_resultMsg')) { $('gadgetQuickAccess_formNewLink_resultMsg').innerHTML = ''; }
		if ($('gadgetQuickAccess_formDeleteLink_resultMsg')) { $('gadgetQuickAccess_formDeleteLink_resultMsg').innerHTML = ''; }
	} else {
		if ($('gadgetQuickAccess_formNewAcctLink_resultMsg')) { $('gadgetQuickAccess_formNewAcctLink_resultMsg').innerHTML = ''; }
		if ($('gadgetQuickAccess_formDeleteAcctLink_resultMsg')) { $('gadgetQuickAccess_formDeleteAcctLink_resultMsg').innerHTML = ''; }
	}
};
gadgetQuickAccess.bothLinks.clearLinkESign = function(whichLink) {
	if (whichLink != "acctLinks" && whichLink != "myLinks") { return; }
	if ($('gadgetQuickAccessJB.'+whichLink+'ESign_eSign')) {
		$('gadgetQuickAccessJB.'+whichLink+'ESign_eSign').innerHTML = ''; 
		$j('#gadgetQuickAccessJB\\.'+whichLink+'ESign_eSign').removeClass('validation-fail');
		$j('#gadgetQuickAccessJB\\.'+whichLink+'ESign_eSign').removeClass('validation-pass');
	}
	if ($('gadgetQuickAccessJB.'+whichLink+'ESign')) { $('gadgetQuickAccessJB.'+whichLink+'ESign').value = ''; }
}
gadgetQuickAccess.bothLinks.clearLinkDeleteESign = function(whichLink) {
	if (whichLink != "acctLinks" && whichLink != "myLinks") { return; }
	if ($('gadgetQuickAccessJB.'+whichLink+'DeleteESign_eSign')) {
		$('gadgetQuickAccessJB.'+whichLink+'DeleteESign_eSign').innerHTML = ''; 
		$j('#gadgetQuickAccessJB\\.'+whichLink+'DeleteESign_eSign').removeClass('validation-fail');
		$j('#gadgetQuickAccessJB\\.'+whichLink+'DeleteESign_eSign').removeClass('validation-pass');
	}
	$('gadgetQuickAccessJB.'+whichLink+'DeleteESign').value = '';
}
gadgetQuickAccess.bothLinks.validateDeleteLink = function(whichLink) {
	if (whichLink != "acctLinks" && whichLink != "myLinks") { return false; }
	var thisValidationId = 'gadgetQuickAccess.'+whichLink+'.deleteLink';
	validationMessages[thisValidationId] = {};
	gadgetQuickAccess.bothLinks.clearNewLinkErrMsg(whichLink);
	gadgetQuickAccess.bothLinks.clearLinkESign(whichLink); // clear the other e-sign
	
	var myLinksESValidations = [];
	var myLinksESKey = 'gadgetQuickAccessJB.'+whichLink+'DeleteESign';
	if (!$(myLinksESKey).value) {
		myLinksESValidations.push("<%=MC.M_FieldRequired%>");
	}
	if (myLinksESValidations.length > 0) {
		validationMessages[thisValidationId][myLinksESKey] = myLinksESValidations;
	}
	
	return formatErrMsgGlobal(thisValidationId);
}
gadgetQuickAccess.bothLinks.validateNewLink = function(whichLink) {
	if (whichLink != "acctLinks" && whichLink != "myLinks") { return false; }
	var thisValidationId = 'gadgetQuickAccess.'+whichLink+'.newLink';
	validationMessages[thisValidationId] = {};
	gadgetQuickAccess.bothLinks.clearNewLinkErrMsg(whichLink);
	gadgetQuickAccess.bothLinks.clearLinkDeleteESign(whichLink); // clear the other e-sign
	
	var myLinksDisplayValidations = []; // The word my in variable name here means local variable
	var myLinksDisplayKey = 'gadgetQuickAccessJB.'+whichLink+'LinkDisplay';
	if ($(myLinksDisplayKey).value) {
		$(myLinksDisplayKey).value = jQuery.trim($(myLinksDisplayKey).value);
	}
	if (!$(myLinksDisplayKey).value || $(myLinksDisplayKey).value.length == 0) {
		myLinksDisplayValidations.push("<%=MC.M_FieldRequired%>");
	}
	if (myLinksDisplayValidations.length > 0) {
		validationMessages[thisValidationId][myLinksDisplayKey] = myLinksDisplayValidations;
	}

	var myLinksUrlValidations = [];
	var myLinksUrlKey = 'gadgetQuickAccessJB.'+whichLink+'LinkUrl';
	if ($(myLinksUrlKey).value) {
		$(myLinksUrlKey).value = jQuery.trim($(myLinksUrlKey).value);
	}
	if (!$(myLinksUrlKey).value || $(myLinksUrlKey).value.length == 0) {
		myLinksUrlValidations.push("<%=MC.M_FieldRequired%>");
	}
	if (myLinksUrlValidations.length > 0) {
		validationMessages[thisValidationId][myLinksUrlKey] = myLinksUrlValidations;
	}
	
	var myLinksSectionValidations = [];
	var myLinksSectionKey = 'gadgetQuickAccessJB.'+whichLink+'LinkSection';
	/*
	if ($(myLinksSectionKey).value) {
		$(myLinksSectionKey).value = jQuery.trim($(myLinksSectionKey).value);
	}
	if (!$(myLinksSectionKey).value) {
		myLinksSectionValidations.push('This field is required.');
	}
	*/
	if (myLinksSectionValidations.length > 0) {
		validationMessages[thisValidationId][myLinksSectionKey] = myLinksSectionValidations;
	}
	
	
	var myLinksESValidations = [];
	var myLinksESKey = 'gadgetQuickAccessJB.'+whichLink+'ESign';
	if (!$(myLinksESKey).value) {
		myLinksESValidations.push("<%=MC.M_FieldRequired%>");
	}
	if (myLinksESValidations.length > 0) {
		validationMessages[thisValidationId][myLinksESKey] = myLinksESValidations;
	}
	
	return formatErrMsgGlobal(thisValidationId);
}

gadgetQuickAccess.screenAction = function() {
	YUI({
	    gallery: 'gallery-2012.06.20-20-07'
	}).use('gallery-placeholder', 'node', function(Y) {
		var addToolTipToButton = function(buttonId) {
			Y.one(buttonId).on('mousemove', onMousemoveToolTip);
			Y.one(buttonId).on('mouseleave', onMouseleaveToolTip);
			Y.one(buttonId).on('mouseout', onMouseleaveToolTip);
		};
		Y.one('#gadgetQuickAccess_study').plug(Y.Plugin.Placeholder, {
			text : "<%=MC.M_StdTitle_Kword%>",
			hideOnFocus : true
		});
		Y.one('#gadgetQuickAccess_patient').plug(Y.Plugin.Placeholder, {
			text : "<%=LC.L_Patient_Id%>",
			hideOnFocus : true
		});
		Y.one('#gadgetQuickAccess_patientStudy').plug(Y.Plugin.Placeholder, {
			text : "<%=LC.L_Patient_StudyId%>",
			hideOnFocus : true
		});
		
		addToolTipToButton('#gadgetQuickAccess_studySearchButton');
		addToolTipToButton('#gadgetQuickAccess_studyAdvancedSearchButton');
		addToolTipToButton('#gadgetQuickAccess_patientSearchButton');
		addToolTipToButton('#gadgetQuickAccess_patientStudySearchButton');
	});
	
	$j("#gadgetQuickAccess_studySearchButton").button();
	$j("#gadgetQuickAccess_patientSearchButton").button();
	$j("#gadgetQuickAccess_patientStudySearchButton").button();
	$j("#gadgetQuickAccess_studyAdvancedSearchButton").button();
	
	$j("#gadgetQuickAccess_studySearchButton").click(function() {
		gadgetQuickAccess.submitStudy();
	});
	$j("#gadgetQuickAccess_patientSearchButton").click(function() {
		gadgetQuickAccess.submitPatient();
	});
	$j("#gadgetQuickAccess_patientStudySearchButton").click(function() {
		gadgetQuickAccess.submitPatientStudy();
	});
	$j("#gadgetQuickAccess_studyAdvancedSearchButton").click(function() {
		gadgetQuickAccess.submitStudyAdvanced();
	});
	
	var setIdToSelectStudy = function() {
		var mySelect = document.getElementsByName("gadgetQuickAccessJB.studyForPatStudySearch")[0]
		mySelect.id = 'gadgetQuickAccessJB.studyForPatStudySearch';
		$j("#gadgetQuickAccessJB\\.studyForPatStudySearch").addClass("gdt-placeholder-select-study");
		
		var mySelect1 = document.getElementsByName("gadgetQuickAccessJB.studyForPatSearch")[0]
		mySelect1.id = 'gadgetQuickAccessJB.studyForPatSearch';
		$j("#gadgetQuickAccessJB\\.studyForPatSearch").addClass("gdt-placeholder-select-study");
	}();

	var setPlaceholderToSelectStudy = function() {
		$j(".gdt-placeholder-select-study").each(function() {
			$j(this).css('width','177px');
			var opts = $j(this).attr('options')
			for (var iX=0; iX<opts.length; iX++) {
				if (opts[iX].value == "" || opts[iX].value == "0") {
					opts[iX].style.color = 'gray';
					opts[iX].text = "Study Number";
				} else { opts[iX].style.color = 'black'; }
			}
		});
		$j(".gdt-placeholder-select-study").change(function() {
			if($j(this).val() == "" || $j(this).val() == "0") {
				$j(this).css('color','gray');
			} else $j(this).css('color','black');
		});
		$j(".gdt-placeholder-select-study").change();
	}();
	
	$j("#gadgetQuickAccess_patientOnStudyTurnedOn").change(function() {
		if ($j(this).attr('checked')) {
			$j("#gadgetQuickAccessJB\\.studyForPatSearch").show("blind");
			$('gadgetQuickAccess_formPatient').action = 'studypatients.jsp?srcmenu=tdmenubaritem5&selectedTab=2&openMode=F';
		} else {
			$j("#gadgetQuickAccessJB\\.studyForPatSearch").hide("blind");
			$('gadgetQuickAccess_formPatient').action = 'allPatient.jsp?srcmenu=tdmenubaritem5&selectedTab=1&searchFrom=initial';
		}
	});
	if ($j("#gadgetQuickAccess_patientOnStudyTurnedOn")) {
		if ($j("#gadgetQuickAccess_patientOnStudyTurnedOn").attr('checked')) {
			$j("#gadgetQuickAccessJB\\.studyForPatSearch").show();
		} else {
			$j("#gadgetQuickAccessJB\\.studyForPatSearch").hide();
		}
	}

	// --- My Links section starts here
	$j("#gadgetQuickAccess_saveNewMyLinkButton").button();
	$j("#gadgetQuickAccess_cancelNewMyLinkButton").button();
	$j("#gadgetQuickAccess_deleteMyLinkButton").button();
	$j("#gadgetQuickAccess_cancelMyLinkButton").button();
	
	$j("#gadgetQuickAccess_saveNewMyLinkButton").click(function() {
		gadgetQuickAccess.myLinks.saveNewLink();
	});
	$j("#gadgetQuickAccess_cancelNewMyLinkButton").click(function() {
		gadgetQuickAccess.bothLinks.cancelNewLink('myLinks');
	});
	$j("#gadgetQuickAccess_deleteMyLinkButton").click(function() {
		gadgetQuickAccess.myLinks.deleteLink();
	});
	$j("#gadgetQuickAccess_cancelMyLinkButton").click(function() {
		gadgetQuickAccess.bothLinks.cancelNewLink('myLinks');
	});
	gadgetQuickAccess.myLinks.reloadMyLink();
	// --- My Links section ends here
	
	// --- Acct Links section starts here
	$j("#gadgetQuickAccess_saveNewAcctLinkButton").button();
	$j("#gadgetQuickAccess_cancelNewAcctLinkButton").button();
	$j("#gadgetQuickAccess_deleteAcctLinkButton").button();
	$j("#gadgetQuickAccess_cancelAcctLinkButton").button();
	
	$j("#gadgetQuickAccess_saveNewAcctLinkButton").click(function() {
		gadgetQuickAccess.acctLinks.saveNewLink();
	});
	$j("#gadgetQuickAccess_cancelNewAcctLinkButton").click(function() {
		gadgetQuickAccess.bothLinks.cancelNewLink("acctLinks");
	});
	$j("#gadgetQuickAccess_deleteAcctLinkButton").click(function() {
		gadgetQuickAccess.acctLinks.deleteLink();
	});
	$j("#gadgetQuickAccess_cancelAcctLinkButton").click(function() {
		gadgetQuickAccess.bothLinks.cancelNewLink("acctLinks");
	});
	gadgetQuickAccess.acctLinks.reloadAcctLink();
	// --- Acct Links section ends here

};
gadgetQuickAccess.beforeSubmitStudy = function(formobj) {
	try {
		if (!$j("#gadgetQuickAccess_study").hasClass('yui3-placeholder-text')) {
			formobj.searchCriteria.value = formobj.gadgetQuickAccess_study.value;
		}
	} catch(e) {
		// console.log('In gadgetQuickAccess.beforeSubmitStudy:'+e);
	}
}
gadgetQuickAccess.submitStudy = function() {
	try {
		if (!$j("#gadgetQuickAccess_study").hasClass('yui3-placeholder-text')) {
			var mySearchCriteria = $j("#gadgetQuickAccess_formStudy").children('#searchCriteria');
			if (mySearchCriteria && mySearchCriteria[0]) {
				mySearchCriteria[0].value = $j("#gadgetQuickAccess_study").val();
			}
		}
		$("gadgetQuickAccess_formStudy").action = "studybrowserpg.jsp";
		$("gadgetQuickAccess_formStudy").method = "POST";
		$("gadgetQuickAccess_formStudy").submit();
	} catch(e) {
		// console.log('In gadgetQuickAccess.submitStudy:'+e);
	}
}
gadgetQuickAccess.submitStudyAdvanced = function() {
	try {
		if (!$j("#gadgetQuickAccess_study").hasClass('yui3-placeholder-text')) {
			var mySearchCriteria = $j("#gadgetQuickAccess_formStudy").children('#searchCriteria');
			if (mySearchCriteria && mySearchCriteria[0]) {
				mySearchCriteria[0].value = $j("#gadgetQuickAccess_study").val();
			}
		}
		$("gadgetQuickAccess_formStudy").action = "advStudysearchpg.jsp";
		$("gadgetQuickAccess_formStudy").method = "POST";
		$("gadgetQuickAccess_formStudy").submit();
	} catch(e) {
		// console.log('In gadgetQuickAccess.submitStudyAdvanced:'+e);
	}
}
gadgetQuickAccess.beforeSubmitPatient = function(formobj) {
	if (!$j("#gadgetQuickAccess_patientOnStudyTurnedOn")) { return false; }
	if ($j("#gadgetQuickAccess_patientOnStudyTurnedOn").attr('checked')) {
		try {
			var selectedStudyVal = formobj['gadgetQuickAccessJB.studyForPatSearch'].value
			formobj.studyId.value = selectedStudyVal;
			if (!$j("#gadgetQuickAccess_patient").hasClass('yui3-placeholder-text')) {
				formobj.patientid.value = formobj.gadgetQuickAccess_patient.value;
			}
		} catch(e) {
			// console.log('In gadgetQuickAccess.beforeSubmitPatient:'+e);
		}
	} else {
		try {
			if (!$j("#gadgetQuickAccess_patient").hasClass('yui3-placeholder-text')) {
				formobj.searchPatient.value = formobj.gadgetQuickAccess_patient.value;
			}
		} catch(e) {
			// console.log('In gadgetQuickAccess.beforeSubmitPatient:'+e);
		}
	}
	return true;
}
gadgetQuickAccess.submitPatient = function() {
	if (!$j("#gadgetQuickAccess_patientOnStudyTurnedOn")) { return; }
	if ($j("#gadgetQuickAccess_patientOnStudyTurnedOn").attr('checked')) {
		try {
			var selectedStudyVal = $('gadgetQuickAccessJB.studyForPatSearch').value
			var mySearchStudy = $j("#gadgetQuickAccess_formPatient").children('#studyId');
			if (mySearchStudy && mySearchStudy[0]) {
				mySearchStudy[0].value = selectedStudyVal;
			}
			if (!$j("#gadgetQuickAccess_patient").hasClass('yui3-placeholder-text')) {
				var mySearchCriteria = $j("#gadgetQuickAccess_formPatient").children('#patientid');
				if (mySearchCriteria && mySearchCriteria[0]) {
					mySearchCriteria[0].value = $j("#gadgetQuickAccess_patient").val();
				}
			}
			$('gadgetQuickAccess_formPatient').action = 'studypatients.jsp?srcmenu=tdmenubaritem5&selectedTab=2&openMode=F';
		} catch(e) {
			// console.log('In gadgetQuickAccess.submitPatient:'+e);
			return;
		}
	} else {
		try {
			if (!$j("#gadgetQuickAccess_patient").hasClass('yui3-placeholder-text')) {
				var mySearchCriteria = $j("#gadgetQuickAccess_formPatient").children('#searchPatient');
				if (mySearchCriteria && mySearchCriteria[0]) {
					mySearchCriteria[0].value = $j("#gadgetQuickAccess_patient").val();
				}
			}
			$("gadgetQuickAccess_formPatient").action = "allPatient.jsp?srcmenu=tdmenubaritem5&selectedTab=1&searchFrom=initial";
		} catch(e) {
			// console.log('In gadgetQuickAccess.submitPatient:'+e);
			return;
		}
	}
	$("gadgetQuickAccess_formPatient").method = "POST";
	$("gadgetQuickAccess_formPatient").submit();
}
gadgetQuickAccess.beforeSubmitPatientStudy = function(formobj) {
	try {
		var selectedStudyVal = formobj['gadgetQuickAccessJB.studyForPatStudySearch'].value
		formobj.studyId.value = selectedStudyVal;
		if (!$j("#gadgetQuickAccess_patientStudy").hasClass('yui3-placeholder-text')) {
			formobj.pstudyid.value = formobj.gadgetQuickAccess_patientStudy.value;
		}
	} catch(e) {
		// console.log('In gadgetQuickAccess.beforeSubmitPatientStudy:'+e);
		return false;
	}
	return true;
}
gadgetQuickAccess.submitPatientStudy = function() {
	try {
		var selectedStudyVal = $('gadgetQuickAccessJB.studyForPatStudySearch').value
		var mySearchStudy = $j("#gadgetQuickAccess_formPatientStudy").children('#studyId');
		if (mySearchStudy && mySearchStudy[0]) {
			mySearchStudy[0].value = selectedStudyVal;
		}
		if (!$j("#gadgetQuickAccess_patientStudy").hasClass('yui3-placeholder-text')) {
			var mySearchCriteria = $j("#gadgetQuickAccess_formPatientStudy").children('#pstudyid');
			if (mySearchCriteria && mySearchCriteria[0]) {
				mySearchCriteria[0].value = $j("#gadgetQuickAccess_patientStudy").val();
			}
		}
		$("gadgetQuickAccess_formPatientStudy").action = "studypatients.jsp?srcmenu=tdmenubaritem5&selectedTab=2&openMode=F";
		$("gadgetQuickAccess_formPatientStudy").method = "POST";
		$("gadgetQuickAccess_formPatientStudy").submit();
	} catch(e) {
		alert(e);
		// console.log('In gadgetQuickAccess.submitPatientStudy:'+e);
	}
}

screenActionRegistry['gadgetQuickAccess'] = gadgetQuickAccess.screenAction;
</script>
