var coverageGridChangeList = null;
var visitLabelArray = null;
var visitDisplacementArray = null;
var visitIdArray = null;
var eventLabelArray = null;
var eventCovArray = null;
var eventCovNotesArray = null;
var eventIdArray = null;
var incomingUrlParams = null;
var calStatus = null;
var calName = null;
var lastTimeSubmitted = 0;
var horizontalPage = 0;
var covMode = null;
var covOptionsStr = null;
var covOptionsIndexArray = null;
var coverageArray = null;
var coverageNotes = null;
var dataArray = null;
var rowChangeList = null;
var covTypeLegend = null;
var myDataTable = null;
var coverageNotesHTML = null;
var coverageNotesIndex = null;
var covNotesHash = null;
var submitFlag = false;
var maxWidth = 700;
var maxHeight = 1000;
var pageRight = 0;
var Preview_AndSave=L_Preview_AndSave;
var CommErr_CnctVelos=M_CommErr_CnctVelos;
var PlsEnterEsign=M_PlsEnterEsign;
var Event_Coverage=L_Event_Coverage;
var Valid_Esign=L_Valid_Esign;
var Esignature=L_Esignature;
var Coverage_Notes=L_Coverage_Notes;
var EdtCvg_AnlysEvt=M_EdtCvg_AnlysEvt;
var PleaseWait_Dots=L_PleaseWait_Dots;
var LoadingPlsWait=L_LoadingPlsWait;
var ErrGetResp_CnctVelos=M_ErrGetResp_CnctVelos;
var Invalid_Esign=L_Invalid_Esign;
var ApplyAll_Vsts=L_ApplyAll_Vsts;
var Coverage_TypeLegend=L_Coverage_TypeLegend;
var Coverage_Type=L_Coverage_Type;
var Coverage_Analysis=L_Coverage_Analysis;
var Event_Name=L_Event_Name;

// Define VELOS.coverageGrid object. "VELOS" is a namespace so that the object can be called globally
VELOS.coverageGrid = function (url, args) {
	this.url = url;  // save it in this.url; later it will be sent in startRequest()
	incomingUrlParams = args.urlParams; // this will be sent to updateEvtVisits.jsp
	var myParams = [];
	if (incomingUrlParams) {
		incomingUrlParams = incomingUrlParams.replace("'", "\"");
		myParams = incomingUrlParams.split("&");
		for (var iX=0; iX<myParams.length; iX++) {
			if (myParams[iX].match(/^pr[=]/)) {
				part = myParams[iX].split("=");
				pageRight = part[1];
				break;
			}
		}
	}
	
	showPanel(); // Show the wait panel; this will be hidden later
	
	// Define handleSuccess() of VELOS.coverageGrid object. This will be added to the callback object to be
	// used when the call is processed successfully.
	this.handleSuccess = function(o) {
		/* Response Object Fields:
		 * o.tId, o.status, o.statusText, o.getResponseHeader[],
		 * o.getAllResponseHeaders, o.responseText, o.responseXML, o.argument
		 */
        this.dataTable = args.dataTable ? args.dataTable : 'serverpagination'; // name of the div to hold coverageGrid
		var respJ = null; // response in JSON format
		try {
			respJ = $J.parse(o.responseText);
		} catch(e) {
			alert(ErrGetResp_CnctVelos);/*alert('Error getting response from server. Please contact Velos Support.');*****/
			return;
		}
		if (respJ.error < 0) {
               var paramArray = [respJ.errorMsg];
			   alert(getLocalizedMessageString("L_Error_Msg", paramArray));/*alert('Error Msg: '+respJ.errorMsg);*****/
		       return;	
		}
		covMode = respJ.covMode;
		calName = respJ.protocolName;
		var myCovSubTypes = respJ.covSubTypes;
		var myCovDescs = respJ.covDescs;
		var myCovPks = respJ.covPks;
		var myCovOptions = [];
		covOptionsIndexArray = [];
		myCovOptions.push(new Option(' ', ' ', false, false))
		var myCovOptionsStr = '<option value=""> </option>';
		for (var iX=0; iX<myCovSubTypes.length; iX++) {
			myCovOptions.push(new Option(myCovSubTypes[iX].key, myCovSubTypes[iX].key, false, false));
			myCovOptionsStr += '<option value="'+myCovPks[iX].key+'">'+myCovSubTypes[iX].key+'</option>';
			covOptionsIndexArray[myCovSubTypes[iX].key]=iX+1; // 0 = '<b>X</b>'
		}
		covOptionsStr = myCovOptionsStr;
		
		var myFieldArray = [];
		var myColumnDefs = [];
		visitIdArray = [];
		if (respJ.colArray) {
			myColumnDefs = respJ.colArray;
			for (var iX=0; iX<myColumnDefs.length; iX++) {
				if (myColumnDefs[iX].key != 'eventCovNotes')
					myColumnDefs[iX].width = myColumnDefs[iX].key == 'event' ? 200: 100;

				myColumnDefs[iX].resizeable = true;
				myColumnDefs[iX].sortable = false;
				var fieldElem = [];
				if (myColumnDefs[iX].key && myColumnDefs[iX].key.match(/^v[0-9]+$/)) {
					visitIdArray.push(myColumnDefs[iX].key);
				}
				if (myColumnDefs[iX].key == 'selectAll') {
					myColumnDefs[iX].width = 110;
				}
				if (myColumnDefs[iX].key == 'eventId') {
					myColumnDefs[iX].hidden = true;
				}
				fieldElem['key'] = myColumnDefs[iX].key; 
				myFieldArray.push(fieldElem);
			}
		}
		visitLabelArray = [];
		for (var iX=0; iX<myColumnDefs.length; iX++) {
			if (!myColumnDefs[iX].label) { continue; }
			if (myColumnDefs[iX].key == 'event' || myColumnDefs[iX].key == 'eventId') { continue; }
			visitLabelArray[myColumnDefs[iX].key] = myColumnDefs[iX].label;
		}
		visitDisplacementArray = [];
		if (respJ.displacements) {
			var myDisplacements = [];
			myDisplacements = respJ.displacements;
			for (var iX=0; iX<myDisplacements.length; iX++) {
				visitDisplacementArray[myDisplacements[iX].key] = myDisplacements[iX].value;
			}
		}
		
		var myDataSource = new YAHOO.util.DataSource(respJ.dataArray);
		myDataSource.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
		myDataSource.responseSchema = {
			fields: myFieldArray
		};
		
		dataArray = respJ.dataArray;

		if (screen.availWidth >= 1000 && screen.availWidth < 1100) { maxWidth = 800; }
		else if (screen.availWidth >= 1100 && screen.availWidth < 1290) { maxWidth = 1000; }
		else { maxWidth = 1200; }
		
		if (screen.availHeight >= 700 && screen.availHeight < 900) { maxHeight = 320; }
		else if (screen.availHeight >= 900 && screen.availHeight < 1000) { maxHeight = 500; }
		
		var calcHeight = dataArray.length*30 + 30;
		if (calcHeight > maxHeight) { calcHeight = maxHeight; }
		
		myDataTable = new YAHOO.widget.DataTable(
			this.dataTable,
			myColumnDefs, myDataSource,
			{
				width:"98%",
				height:calcHeight+"px",
				zindex:40000,
				//caption:"DataTable Caption", 
				scrollable:true 
			}
		);
		
		eventIdArray = [];
		eventLabelArray = [];
		eventCovArray = [];
		eventCovNotesArray = [];
		var eventIdCells = $D.getElementsByClassName('yui-dt-col-eventId', 'td', myDataTable.getTableEl());
		for (var iX=0; iX<eventIdCells.length; iX++) {
			var el = new YAHOO.util.Element(eventIdCells[iX]);
			var parent = new YAHOO.util.Element($D.getAncestorByTagName(el, 'tr'));
			var eventIdTd = parent.getElementsByClassName('yui-dt-col-eventId', 'td')[0];
			var eventIdEl = new YAHOO.util.Element(eventIdTd);
			var eventId = eventIdEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
			eventIdArray.push(eventId);
			var eventTd = parent.getElementsByClassName('yui-dt-col-event', 'td')[0];
			var eventEl = new YAHOO.util.Element(eventTd);
			var eventName = eventEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
			eventLabelArray['e'+eventId] = eventName;
			var eventCellDiv = eventEl.getElementsByClassName('yui-dt-liner', 'div')[0];
			eventCellDiv.innerHTML='<span onmouseover="return VELOS.coverageGrid.getEventMouseOver('+eventId+');" onmouseout="return nd();">'+eventCellDiv.innerHTML+'</span>';
			
			var eventTd = parent.getElementsByClassName('yui-dt-col-eventCoverage', 'td')[0];
			var eventEl = new YAHOO.util.Element(eventTd);
			var eventCoverage = eventEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
			eventCovArray['e'+eventId] = eventCoverage;
			
			var eventTd = parent.getElementsByClassName('yui-dt-col-eventCovNotes', 'td')[0];
			var eventEl = new YAHOO.util.Element(eventTd);
			var eventCellDiv = eventEl.getElementsByClassName('yui-dt-liner', 'div')[0];
			var eventCovNotes = eventCellDiv.innerHTML;
			eventCovNotesArray['e'+eventId] = eventCovNotes;

			//eventCovNotes.replace(replace(/\'/g,'\'');
			var covNotesImage = "<img class='asIsImage' onmouseover='return overlib(\"" + eventCovNotes + "\",CAPTION,\""+ L_Coverage_Notes+"\");'"
				+" onmouseout='return nd();' src='./images/More.png' border='0' complete='complete'/>";
			//alert(covNotesImage);
			eventCovNotes = (eventCovNotes.length > 250)?
					(eventCovNotes.substr(0, 250) +''+ covNotesImage) : eventCovNotes;
			eventCellDiv.innerHTML = eventCovNotes;
		}
		
		var selectAllCells = $D.getElementsByClassName('yui-dt-col-selectAll', 'td', myDataTable.getTableEl());
		for (var iX=0; iX<selectAllCells.length; iX++) {
			var el = new YAHOO.util.Element(selectAllCells[iX]);
			var parent = new YAHOO.util.Element($D.getAncestorByTagName(el, 'tr'));
			var eventIdTd = parent.getElementsByClassName('yui-dt-col-eventId', 'td')[0];
			var eventIdEl = new YAHOO.util.Element(eventIdTd);
			var eventId = eventIdEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
			var myCellDiv = $D.getFirstChild(el);
			myCellDiv.innerHTML = '<select id="all_'+eventId+'" onChange="VELOS.coverageGrid.selectAll('+eventId+
				')" class="yui-dt-dropdown">'+myCovOptionsStr+'</select>';
		}
		
		covNotesHash = respJ.covNotesHash;
		covTypeLegend = respJ.covTypeLegend;
		coverageNotesIndex = []

		if (covMode == 'E') {
			// Change X to <select> and change selectedIndex
			for (var iX=0; iX<dataArray.length; iX++) {
				for (var key in dataArray[iX]) {
					var eventId = dataArray[iX].eventId;
					if (!key.match(/^v[0-9]+$/)) { continue; }
					if ('v0' == key) { continue; }
					var myTr = $('yui-rec'+iX);
					if (!myTr) { continue; }
					var myTd = $D.getElementsByClassName('yui-dt-col-'+key, 'td', myTr);
					var myCellDiv = $D.getFirstChild(myTd[0]);
					var myCellValue = dataArray[iX][key];
						myCellDiv.innerHTML = '<select id="ctype_e'+eventId+key+'" name="ctype_e'+eventId+key+'" class="yui-dt-dropdown">'
							+myCovOptionsStr+'</select>';
					if (myCellValue != '<b>X</b>') {
						if (!$('ctype_e'+eventId+key)) { continue; }
						$('ctype_e'+eventId+key).options.selectedIndex = covOptionsIndexArray[myCellValue];
					}
				}
			}
		} else { // covMode == 'V'
			// Use coverageArray to change some display for selected coverage type
			coverageArray = respJ.coverageArray;
			for (var iX=0; iX<coverageArray.length; iX++) {
				var eventId = coverageArray[iX].eventId;
				for (var key in coverageArray[iX]) {
					if (!key.match(/^v[0-9]+$/)) { continue; }
					if ('v0' == key) { continue; }
					var myFirstTr = $D.getElementsByClassName('yui-dt-first', 'tr', myDataTable.getTableEl());
					var myFirstIndex = 0;
					for (var iY=0; iY<myFirstTr.length; iY++) {
						if (myFirstTr[iY].id.match(/^yui-rec[0-9]+$/)) {
							myFirstIndex = myFirstTr[iY].id.slice(7);
						}
					}
					var eventIndex = coverageArray[iX].eventIndex+parseInt(myFirstIndex,10);
					var myTr = $('yui-rec'+eventIndex);
					if (!myTr) { continue; }
					var myTd = $D.getElementsByClassName('yui-dt-col-'+key, 'td', myTr);
					var myCellDiv = $D.getFirstChild(myTd[0]);
					var myCellValue = coverageArray[iX][key];
					if (myCellValue != '<b>X</b>') {
						myCellDiv.innerHTML = '<b>X</b>&nbsp;'+myCellDiv.innerHTML;
					}
				}
			}
			coverageNotes = respJ.coverageNotes;
			for (var iX=0; iX<coverageNotes.length; iX++) {
				var eventId = coverageNotes[iX].eventId;
				for (var key in coverageNotes[iX]) {
					if (!key.match(/^v[0-9]+$/)) { continue; }
					if ('v0' == key) { continue; }
					var myFirstTr = $D.getElementsByClassName('yui-dt-first', 'tr', myDataTable.getTableEl());
					var myFirstIndex = 0;
					for (var iY=0; iY<myFirstTr.length; iY++) {
						if (myFirstTr[iY].id.match(/^yui-rec[0-9]+$/)) {
							myFirstIndex = myFirstTr[iY].id.slice(7);
						}
					}
					var eventIndex = coverageNotes[iX].eventIndex+parseInt(myFirstIndex,10);
					var myTr = $('yui-rec'+eventIndex);
					if (!myTr) { continue; }
					var myTd = $D.getElementsByClassName('yui-dt-col-'+key, 'td', myTr);
					var myCellDiv = $D.getFirstChild(myTd[0]);
					var myCellValue = coverageNotes[iX][key];
					
					if (myCellValue == 'Y') {
						myCellDiv.innerHTML = myCellDiv.innerHTML+'&nbsp;'+
							'<img class="headerImage" align="bottom" src="images/clip.jpg"/><input type="hidden" id="keyValue" name="keyValue" value="'+eventId+key+'">||';
					
					}
				}
			}
		}
		
		/*YK : Added for Sequence correction for Bug#5898*/
		var superscriptCnt = 0;
		var eventIdCells = $D.getElementsByClassName('yui-dt-col-eventId', 'td', myDataTable.getTableEl());
		for (var iX=0; iX<eventIdCells.length; iX++) {
			var el = new YAHOO.util.Element(eventIdCells[iX]);
			var parent = new YAHOO.util.Element($D.getAncestorByTagName(el, 'tr'));
			var parentId = parent.get('id');
			var cells = $D.getChildren($(parentId));
			for (var iS=0; iS<cells.length; iS++) {
				if (!cells[iS].className) { continue; }
				var pattern = /\byui-dt-col-v[0-9]+\b/;
				var cName = cells[iS].className.match(pattern);
				if (!cName) { continue; }
				var el = new YAHOO.util.Element(cells[iS]);
				var input = el.getElementsByClassName('yui-dt-liner', 'div')[0];
				if(input.innerHTML.lastIndexOf('||')>0)
				{
					input.innerHTML=input.innerHTML.slice(0,input.innerHTML.lastIndexOf('||'))+'<sup>'+(++superscriptCnt)+'</sup>';
					if(input.getElementsByTagName('INPUT')[0]){
					var keyValue=input.getElementsByTagName('INPUT')[0].value;
					coverageNotesIndex.push(keyValue);
					}
				}
			}
		}
		
		for (var iX=0; iX<dataArray.length; iX++) {
			for (var key in dataArray[iX]) {
				var eventId = dataArray[iX].eventId;
				if (!key.match(/^intersectId[0-9]+$/)) { continue; }
				if ('intersectId0' == key) { continue; }
				var myTr = $('yui-rec'+iX);
				if (!myTr) { continue; }
				var visitId = key.split("intersectId")[1];
				//alert(visitId);
				var myTd = $D.getElementsByClassName('yui-dt-col-v'+visitId, 'td', myTr);
				var myCellDiv = $D.getFirstChild(myTd[0]);
				var myCellValue = dataArray[iX][key];
				if (myCellValue && myCellValue != null)
					myCellDiv.innerHTML='<span onmouseover="return VELOS.coverageGrid.getEventMouseOver('+myCellValue+');" onmouseout="return nd();">'+myCellDiv.innerHTML+'</span>';
			}
		}
		
		/*
		// Double loop way
		for (var iV=0; iV<visitIdArray.length; iV++) {
			var selVisitCells = $D.getElementsByClassName('yui-dt-col-'+visitIdArray[iV],
					'td', myDataTable.getTableEl());
			for (var iE=0; iE<selVisitCells.length; iE++) {
				var myCellDiv = $D.getFirstChild(selVisitCells[iE]);
				var myCellValue = ''+myCellDiv.innerHTML;
				if (myCellDiv.innerHTML != '' && myCellDiv.innerHTML != '&nbsp;') {
					myCellDiv.innerHTML = '<select id="ctype_e'+eventIdArray[iE]+visitIdArray[iV]+'" class="yui-dt-dropdown">'
					+myCovOptionsStr+'</select>';
					if (myCellValue != '[Y]') {
						// set selectedIndex here
						document.getElementById(('e'+eventIdArray[iE]+visitIdArray[iV])).options.selectedIndex = 
							covOptionsIndexArray[myCellValue];
					}
				}
			}
		}
		*/
		
		coverageGridChangeList = [];

		// row click to pop up a row editor
		if (covMode != 'E') {
			myDataTable.subscribe("rowClickEvent", function(oArgs){
				var elRow = oArgs.target;
				var oRecord = this.getRecord(elRow);
				var oKey = oRecord.getData().eventId;
				VELOS.coverageGrid.rowDialog(oKey);
			});
		}
		
		
        // Subscribe to events for row selection
		myDataTable.subscribe("cellMouseoverEvent", function(oArgs) {
			myDataTable.onEventHighlightRow(oArgs);
			var elCell = oArgs.target;
			var oColumn = this.getColumn(oArgs.target);
			var oRecord = this.getRecord(oArgs.target);

			if (oColumn.key != 'eventCovNotes'){
				var eventSpan = elCell.getElementsByTagName('span')[0];
				if (eventSpan){
					elCell.onmouseover = eventSpan.onmouseover;
				}else{
					if (!elCell.onmouseover){
						var parent = new YAHOO.util.Element($D.getAncestorByTagName(elCell, 'tr'));
						var parentId = parent.get('id');
						var eventTd = parent.getElementsByClassName('yui-dt-col-event', 'td')[0];
						var eventEl = new YAHOO.util.Element(eventTd);
						var eventSpan = eventEl.getElementsByTagName('span')[0];
						elCell.onmouseover = eventSpan.onmouseover;
						elCell.onmouseout = eventSpan.onmouseout;
					}
				}
				return elCell.onmouseover();
			}
		});
		myDataTable.subscribe("rowMouseoutEvent", function(oArgs) {
			myDataTable.onEventUnhighlightRow(oArgs);
			return nd();
		});
		myDataTable.focus();

		if (!$('save_changes')) {
			var dt = $(this.dataTable);
			var saveDiv = document.createElement('div');
			saveDiv.innerHTML = "<br/><table border=0 width='100%'><tbody><tr align='left'>" +
				"<td width='350'></td>"+
				"<td align='left'><input onclick='VELOS.coverageGrid.rowDialog();' type='button' id='save_changes' name='save_changes' value='"+Preview_AndSave/*Preview and Save*****/+"'/></td>"+
				"</td></tr></tbody></table>";
			$D.insertAfter(saveDiv, dt);
			
		}
		
		calStatus = respJ.calStatus;
		//JM: 31MAR2011: #5938
		//if (calStatus == 'A' || calStatus == 'D' || calStatus == 'F' || calStatus == 'R') {
		if (calStatus == 'A' || calStatus == 'D' || calStatus == 'F') {			
			if ($('save_changes')) { $('save_changes').disabled = true; }
			if ($('submit_btn')) { $('submit_btn').disabled = true; }
			if ($('eSign')) { $('eSign').disabled = true; }
		}
		
		$('coveragenotes').innerHTML = '';
		if (coverageNotesIndex.length > 0) {
			var coverageNotesHTML = '<table><tr><td colspan="2"><img align="bottom" src="images/clip.jpg" width="17" height="15"/></td></tr>';
			for (var iX=0; iX<coverageNotesIndex.length; iX++) {
				coverageNotesHTML += '<tr valign="top"><td>'+(iX+1)+'</td><td> &ndash; '+covNotesHash[coverageNotesIndex[iX]]+'</td></tr>';
			}
			coverageNotesHTML += '</table>';
			$('coveragenotes').innerHTML = coverageNotesHTML;
		}

		hidePanel();
	};
	this.handleFailure = function(o) {
		alert(CommErr_CnctVelos);/*alert('Communication Error. Please contact Velos Support');*****/
		hidePanel();
	};
	this.cArgs = [];
	this.callback = {
		success:this.handleSuccess,
		failure:this.handleFailure,
		argument:this.cArgs
	};
	this.initConfigs(args);
}

VELOS.coverageGrid.fnOnceEnterKeyPress = function(e) {
	try {
        if (e.keyCode == 13 || e.keyCode == 10) {
        	if (VELOS.coverageGrid.validate(document.aForm) == false) { return false; };
			if ('' == document.getElementById('eSignMessage').innerHTML) {
        		alert(PlsEnterEsign);/*alert('Please enter e-Signature.');*****/
        		return false;
			}
        	if (Valid_Esign != document.getElementById('eSignMessage').innerHTML) {
        		alert(document.getElementById('eSignMessage').innerHTML);
        		return false;
        	}
            var thisTimeSubmitted = new Date();
            if (!lastTimeSubmitted) { return true; }
            if (!thisTimeSubmitted) { return true; }
            if ( thisTimeSubmitted - lastTimeSubmitted <= 1000 ) {
                return false;
            }
            lastTimeSubmitted = thisTimeSubmitted;
        }
	} catch(e) {}
	return true;
}

VELOS.coverageGrid.getEventMouseOver = function(eventId) {
	var calledFrom = (document.getElementById("calledFrom"))
		? document.getElementById("calledFrom").value
		: "L";
	calledFrom = (!calledFrom)? "L":calledFrom;
	
	var studyId = (document.getElementById("studyId"))
		? document.getElementById("studyId").value
		: "";
	studyId = (!studyId)? "":studyId;
	
	fnGetEventMouseOver(eventId, "A", calledFrom, studyId);
}

VELOS.coverageGrid.fnClickOnce = function(e) {
	try {
		if ('' == $('eSignMessage').innerHTML) {
			alert(PlsEnterEsign);/*alert('Please enter e-Signature.');*****/
    		return false;
		}
    	if (Valid_Esign != document.getElementById('eSignMessage').innerHTML) {
    		alert(document.getElementById('eSignMessage').innerHTML);
    		return false;
    	}
        var thisTimeSubmitted = new Date();
        if (!lastTimeSubmitted) { return true; }
        if (!thisTimeSubmitted) { return true; }
        if ( thisTimeSubmitted - lastTimeSubmitted <= 1000 ) {
            return false;
        }
        lastTimeSubmitted = thisTimeSubmitted;
	} catch(e) {}
    return true;
}

VELOS.coverageGrid.selectAll = function(eId) {
	var selAllCell = $('all_'+eId);
	for (var iX=0; iX<visitIdArray.length; iX++) {
		var cell = $('ctype_e'+eId+visitIdArray[iX]);
		if (!cell || cell.disabled) { continue; }
		cell.selectedIndex = selAllCell.selectedIndex;
	}
}

VELOS.coverageGrid.validate = function(formobj) {
	var visits = [];
	visits.push("v0"); 
	for (var iX=0; iX<visits.length; iX++) {
		visits.push(visitIdArray[iX]);		
		var name = 'notes_e'+formobj.eventId.value+visits[iX];
		var notes = document.getElementsByName(name)[0];
		if (notes == null || notes.value == null) { 
			if (iX == visitIdArray.length){
				break;
			}else{
				continue;
			}
		}
		//Modified By parminder singh Bug#11310
		if(!twosplcharcheck(notes.value)){
			notes.focus();
			return false;
		}
		if (notes.value.length > 4000) { 
			if (iX == 0)
				var paramArray = [L_Event,notes.value.length];
			else
				var paramArray = [visitLabelArray[visits[iX]],notes.value.length];
		    alert(getLocalizedMessageString("M_NoteMax_4kChar", paramArray));/*alert('Notes for "'+visitLabelArray[visitIdArray[iX]]+'" exceed maximum allowed 4000 characters -- current at '+
				notes.value.length+'.');*****/
			//KM-#5206
			notes.focus();
			return false;
		}
		if (iX == visitIdArray.length)
			break;
	}
	return true;
}

VELOS.coverageGrid.printerFriendly = function() {
	try {
	var newWin = window.open("donotdelete.html","printerWin","toolbar=no,scrollbars=yes,resizable=yes,menubar=yes,status=no,left=20,top=20");
	var myArray = $D.getChildren(myDataTable.getTableEl());
	var eventIdHead = new YAHOO.util.Element($('yui-dt-th-eventId'));
	eventIdHead.addClass("yui-dt-hidden");
	$('yui-dt-th-eventId').style.visibility = "hidden";
	$('yui-dt-th-eventId').style.overflow = "hidden";
	var savedText = $('yui-dt-th-eventId').innerText;
	$('yui-dt-th-eventId').innerText = '';
	newWin.document.write('<html>');
	newWin.document.write('<head><title>'+Coverage_Analysis/*Coverage Analysis*****/+'</title></head><body>');
	newWin.document.write('<link type="text/css" rel="stylesheet" href="js/yui/build/datatable/assets/skins/sam/datatable.css">');
	var paramArray = [calName];
	/*newWin.document.write('<table><tr><td colspan="3"><p style="font-weight:bold;background-color: #e1dce0;">Coverage Analysis for Calendar: '+calName+'</p></td></tr></table>');*****/
	newWin.document.write('<table><tr><td colspan="3"><p style="font-weight:bold;background-color: #e1dce0;">'+getLocalizedMessageString("M_CvgAnly_Cal", paramArray)+'</p></td></tr></table>');
	newWin.document.write('<table cellspacing="1" border="1" >');
	for (var iX=0; iX<myArray.length; iX++) {
		if (myArray[iX].className == 'yui-dt-message') { continue; }
		newWin.document.write(myArray[iX].outerHTML);
	}
	newWin.document.write("</table><br/>");
	if (coverageNotesIndex.length > 0) {
		coverageNotesHTML = '<table><tr><td colspan="2"><img class="headerImage" align="bottom" src="images/clip.jpg"/></td></tr>';
		for (var iX=0; iX<coverageNotesIndex.length; iX++) {
			coverageNotesHTML += '<tr valign="top"><td>'+(iX+1)+'</td><td> &ndash; '+covNotesHash[coverageNotesIndex[iX]]+'</td></tr>';
		}
		coverageNotesHTML += '</table>';
		newWin.document.write(coverageNotesHTML);
	}
	newWin.document.write('</body></html>');
	newWin.document.close(); 
	$('yui-dt-th-eventId').innerText = savedText;
	} catch(e) {}
}

VELOS.coverageGrid.rowDialog = function(eId) {
	if (!$('rowDialog')) {
		var rowDialog = document.createElement('div');
		rowDialog.setAttribute('id', 'rowDialog');
		rowDialog.innerHTML  = '<div id="rowDialogHead" class="hd" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt;"></div>';
		rowDialog.innerHTML  += '<div id="rowDialogBody"></div>';
		$D.insertAfter(rowDialog, $('save_changes'));
	}
	VELOS.rowDialog = new YAHOO.widget.Dialog('rowDialog', 
			{
				visible:false, fixedcenter:true, modal:true, resizeable:true, close:true,
				draggable:"true", autofillheight:"body", constraintoviewport:false
			});
	var editFlag = ((pageRight & 0x0002) >> 1) == 1;
	submitFlag = false; // Used in onHide(); reset this at each instantiation
	$('rowDialogBody').innerHTML  = // '<div class="hd" id="rowDialogHead" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt;"></div>'+
	'<form name="aForm" id="aForm" onSubmit="javascript:submitFlag=true;" method="POST" action="updateCoverage.jsp?'+incomingUrlParams+'"><div class="bd" id="rowForm" style="border-bottom: medium none; font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt;"></div>'+
	'<table><tr><td width="150px"><span id="eSignMessage" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;"></span></td>'+
	'<td id="eSignLabel" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;">'+Esignature/*e-Signature*****/+' <FONT class="Mandatory">*</FONT>&nbsp</td>'+
	'<td><input type="password" name="eSign" id="eSign" maxlength="8" autocomplete="off" '+
	' onkeyup="ajaxvalidate(\'misc:\'+this.id,4,\'eSignMessage\',\''+Valid_Esign/*Valid e-Sign*****/+'\',\''+Invalid_Esign/*Invalid e-Sign*****/+'\',\'sessUserId\')" '+
	' onkeypress="return VELOS.coverageGrid.fnOnceEnterKeyPress(event)" '+
	' />&nbsp;&nbsp;&nbsp;&nbsp;</td>'+
	'<td>'+
	'<button id="submit_btn" type="submit" onclick="if (VELOS.coverageGrid.validate(document.aForm)==false){ return false; }; return '+
	' VELOS.coverageGrid.fnClickOnce(event); " ondblclick="return false">'+getLocalizedMessageString('L_Submit')+'</button>'
	'</td>'+
	'<td>'+
	'&nbsp;&nbsp;&nbsp;&nbsp;<button id="close_btn" onclick="VELOS.rowDialog.destroy();">'+getLocalizedMessageString('L_Close')+'</button>'
	'</td>'+
	'</tr></table><br/>'+
	'</form>';
	var myRowData = null;
	for (var iX=0; iX<dataArray.length; iX++) {
		if (dataArray[iX].eventId == eId) {
			foundRow = true;
			myRowData = dataArray[iX];
			break;
		}
	}
	
	var rowFormStr = '<tr><th>&nbsp;</th>';
	rowFormStr += '<th>'+Event_Coverage/*Event Coverage*****/+'</th>';
	rowFormStr += '<th>'+ApplyAll_Vsts/*Apply to All Visits*****/+'</th>';
	for (var iX=0; iX<visitIdArray.length; iX++) {
		rowFormStr += '<th>'+visitLabelArray[visitIdArray[iX]]+'</th>';
	}
	rowFormStr += '</tr>'; 
	rowFormStr += '<tr align="center"><td><a href="javascript:void(0)" onmouseover="return overlib(\''+covTypeLegend+'\',CAPTION,\''+Coverage_TypeLegend/*Coverage Type Legend*****/+'\');" onmouseout="return nd();">'+Coverage_Type/*Coverage Type*****/+'</a></td>';
	
	var targetStr = '>'+eventCovArray['e'+eId]+'<';
	var selectedCovOptionsStr = covOptionsStr.replace(targetStr, " selected "+targetStr);
	rowFormStr += '<td align="center">'+eventCovArray['e'+eId]
		+'<select id="ctype_e'+eId+'v0" name="ctype_e'+eId+'v0" style="display:none">'+selectedCovOptionsStr+'</select>'
		+ '</td>';
	rowFormStr += '<td><select id="all_'+eId+'" onChange="VELOS.coverageGrid.selectAll('+eId+
		')" class="yui-dt-dropdown">'+covOptionsStr+'</select></td>';
	for (var iX=0; iX<visitIdArray.length; iX++) {
		if (myRowData[visitIdArray[iX]]) {
			var targetStr = '>'+myRowData[visitIdArray[iX]]+'<';
			var selectedCovOptionsStr = covOptionsStr.replace(targetStr, " selected "+targetStr);
			rowFormStr += '<td><select id="ctype_e'+eId+visitIdArray[iX]+'" name="ctype_e'+eId+visitIdArray[iX]+'" class="yui-dt-dropdown">'
			+selectedCovOptionsStr+'</select></td>';
		} else {
			rowFormStr += '<td>&nbsp;</td>';
		}
	}
	rowFormStr += '</tr>';
	rowFormStr += '<tr align="center"><td>'+Coverage_Notes/*Coverage Notes*****/+'</td>';

	eventCovNotesTArea = '<TextArea id="notes_e'+eId+'v0" name="notes_e'+eId+'v0" rows="10" cols="20" >'+eventCovNotesArray['e'+eId]+'</TextArea>';
	rowFormStr += '<td>'+eventCovNotesTArea+'</td>';

	rowFormStr += '<td align="center">&nbsp;</td>';
	/*eventCovNotesTArea = '<TextArea id="all_notes_e'+eId+'" name="all_notes_e'+
	eId+'" rows="10" cols="20" >'+'</TextArea>';
	rowFormStr += '<td>'+eventCovNotesTArea+'</td>';*/
	
	for (var iX=0; iX<visitIdArray.length; iX++) {
		var disableNotes = false;
		var myNotes = myRowData['notes_'+ visitIdArray[iX]];
		if (!myNotes) { myNotes = ''; }
		if (!myRowData[visitIdArray[iX]]) { disableNotes = true; }
		var textArea = null;
		if (disableNotes) {
			textArea = '&nbsp;';
		} else {
			textArea = '<TextArea id="notes_e'+eId+visitIdArray[iX]+'" name="notes_e'+
			eId+visitIdArray[iX]+'" rows="10" cols="20" >'+myNotes+'</TextArea>';
		}
		rowFormStr += '<td>'+textArea+'</td>';
	}
	rowFormStr += '</tr>';
	rowFormStr += '<input type="hidden" id="eventId" name="eventId" value="'+eId+'">';
	
	var eventNameForHeader = eventLabelArray['e'+eId];
	if (eventNameForHeader.length > 50) { eventNameForHeader = eventNameForHeader.slice(0, 27)+'...'; } /*YK MOD 12APR2011 Bug#5301*/
	$('rowDialogHead').innerHTML = EdtCvg_AnlysEvt/*'Edit Coverage Analysis for Event*****/+': '+eventNameForHeader;
	$('rowForm').style.width = (maxWidth-100)+'px';
	$('rowForm').style.height = '35em';
	$('rowForm').style.overflow = 'scroll';

	$('rowForm').innerHTML = 
	'<table style="font-size:9pt;"><tr><td><b>Event: '+eventLabelArray['e'+eId]+'</b></td></tr></table>'+
	'<table width="100%" cellspacing="0" border="0" class="table-basic" '+
	'style="width:59em; height:29em; font-size:8pt;">'+
	rowFormStr+'</table>';

	//JM: 31MAR2011: #5938
	//if (calStatus == 'A' || calStatus == 'D' || calStatus == 'F' || calStatus == 'R' || !editFlag) {
	if (calStatus == 'A' || calStatus == 'D' || calStatus == 'F' || !editFlag) {		
		if ($('save_changes')) { $('save_changes').disabled = true; }
		if ($('submit_btn')) {
			$('submit_btn').disabled = true;
			$('submit_btn').style.visibility = 'hidden';
		}
		if ($('eSignLabel')) {
			$('eSignLabel').style.visibility = 'hidden';
		}
		if ($('eSign')) {
			$('eSign').disabled = true;
			$('eSign').style.visibility = 'hidden';
		}
	}

	var onSuccess = function(o) {
		if (VELOS.transitPanel) { VELOS.transitPanel.hide(); }
		var respJ = null; // response in JSON format
		try {
			respJ = $J.parse(o.responseText);
		} catch(e) {
			// Log error here
			return;
		}
		if (respJ.result == -4) {
			showFlashPanel(respJ.resultMsg);
			setTimeout('VELOS.rowDialog.show();', 1500);
		} else if (respJ.result == 0) {
			// alert("Your data was successfully submitted. The response was: " + respJ.resultMsg);
			if (VELOS.rowDialog) { VELOS.rowDialog.destroy(); }
			showFlashPanel(respJ.resultMsg);
			if (window.parent.reloadCoverageGrid) { setTimeout('window.parent.reloadCoverageGrid();', 1500); }
		} else {
			if (VELOS.rowDialog) { VELOS.rowDialog.destroy(); }
			showFlashPanel(respJ.resultMsg);
			if (window.parent.reloadCoverageGrid) { setTimeout('window.parent.reloadCoverageGrid();', 1500); }
		}
	};
	var onFailure = function(o) {
		if (VELOS.rowDialog) { VELOS.rowDialog.destroy(); }
		if (VELOS.transitPanel) { VELOS.transitPanel.hide(); }
		var paramArray = [o.status];
		   alert(getLocalizedMessageString("M_CommErr_CnctVelosSupp", paramArray));/*alert("Communication Error. Please contact Velos Support: " + o.status);*****/
	};
	var onStart = function(o) {
		showTransitPanel(PleaseWait_Dots);/*showTransitPanel('Please wait...');*****/
	}
	var onHide = function(o) {
		if (!submitFlag) { // if the user clicks the X icon to close (i.e. not when he/she submits)
			setTimeout('VELOS.rowDialog.destroy();',100);
		}
	}

	VELOS.rowDialog.subscribe("hide", function(o) { onHide(o); });
	VELOS.rowDialog.callback.customevents = { onStart:onStart };
	VELOS.rowDialog.callback.success = onSuccess;
	VELOS.rowDialog.callback.failure = onFailure;
	VELOS.rowDialog.render(document.body);
	VELOS.rowDialog.show();

	var showFlashPanel = function(msg) {
		// Initialize the temporary Panel to display while waiting for external content to load
		if (!(VELOS.flashPanel)) {
			VELOS.flashPanel = 
				new YAHOO.widget.Panel("flashPanel",  
					{ width:"350px", 
					  fixedcenter:true, 
					  close:false, 
					  draggable:false, 
					  zindex:5,
					  modal:true,
					  visible:false
					} 
				);
		}
		VELOS.flashPanel.setHeader("");
		VELOS.flashPanel.setBody('<table align="center"><tr><td style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt;"><b>'+msg+'</b></td></tr><table>');
		VELOS.flashPanel.render(document.body);
		VELOS.flashPanel.show();
		setTimeout('VELOS.flashPanel.hide();', 1600);
	}
	
	var showTransitPanel = function(msg) {
		if (!VELOS.transitPanel) {
			VELOS.transitPanel = 
				new YAHOO.widget.Panel("transitPanel",  
					{ width:"350px", 
					  fixedcenter:true, 
					  close:false, 
					  draggable:false, 
					  zindex:6,
					  modal:true,
					  visible:false
					} 
				);
		}
		VELOS.transitPanel.setHeader("");
		VELOS.transitPanel.setBody('<table align="center"><tr><td style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt;"><b>'+msg+'</b></td></tr><table>');
		VELOS.transitPanel.render(document.body);
		VELOS.transitPanel.show();
	}
	
	$j(function() {
		if ($j("input:submit")){
			$j( "input:submit").button();
		}
		if ($j("a:submit")){
			$j( "a:submit").button();
		}
		if ($j("button")){
			$j("button").button();
		}
	});
}
/*
VELOS.coverageGrid.saveDialog = function() {
	if (!$('saveDialog')) {
		var saveDialog = document.createElement('div');
		saveDialog.setAttribute('id', 'saveDialog');
		saveDialog.innerHTML  = '<div class="hd" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt;">Confirm Visit/Evt Changes</div>';
		saveDialog.innerHTML += '<div class="bd" id="insertForm" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt; overflow: visible;"></div>';
		$D.insertAfter(saveDialog, $('save_changes'));
	}
	
	var noChange = true;
	var numChanges = 0;
	var maxChangesDisplayedBeforeScroll = 25;
	$('insertForm').innerHTML = '<form id="dialogForm" method="POST" action="updateEvtVisits.jsp?'+incomingUrlParams+'"><div id="insertFormContent"></div></form>';
	for (oKey in coverageGridChangeList) {
		if (oKey.match(/^e[0-9]+v[0-9]+$/) && coverageGridChangeList[oKey] != undefined) {
			noChange = false;
			var visitName = visitLabelArray[oKey.slice(oKey.indexOf('v'))];
			var eventName = eventLabelArray[oKey.slice(0, oKey.indexOf('v'))];
			$('insertFormContent').innerHTML += visitName+' / '+eventName+' => '
				+(coverageGridChangeList[oKey]?"<b>S</b>elected":"<b>U</b>nselected")+'<br/>';
			numChanges++;
			var opVal = (coverageGridChangeList[oKey]?'A':'D')+'|'+oKey+'|'+
				visitDisplacementArray[oKey.slice(oKey.indexOf('v'))]+'|'+eventName;
			$('insertFormContent').innerHTML += 
				'<input type="hidden" name="ops" value="'+opVal+'" />';
		}
	}
	if (numChanges < maxChangesDisplayedBeforeScroll) {
		$('insertForm').style.width = null;
		$('insertForm').style.height = null;
		$('insertForm').style.overflow = 'visible';
	} else {
		$('insertForm').style.width = '51em';
		$('insertForm').style.height = '35em';
		$('insertForm').style.overflow = 'scroll';
	}
	if (noChange) {
		$('insertFormContent').innerHTML += '&nbsp;&nbsp;There are no changes to save.&nbsp;&nbsp;';
		noChange = true;
	} else {
		$('insertFormContent').innerHTML += 'Total changes: '+numChanges+'<br/><table><tr><td width="150px"><span id="eSignMessage" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;"></span></td>'+
			'<td style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;">e-Signature <FONT class="Mandatory">*</FONT>&nbsp</td>'+
			'<td><input type="password" name="eSign" id="eSign" maxlength="8" '+
			' onkeyup="ajaxvalidate(\'misc:\'+this.id,4,\'eSignMessage\',\'Valid e-Sign\',\'Invalid e-Sign\',\'sessUserId\')" '+
			' onkeypress="return VELOS.coverageGrid.fnOnceEnterKeyPress(event)" '+
			' />&nbsp;</td></tr></table>';
	}
		
	var myDialog = new YAHOO.widget.Dialog('saveDialog', 
			{
				visible:false, fixedcenter:true, modal:true, resizeable:true,
				draggable:"true", autofillheight:"body", constraintoviewport:false
			});
	var handleCancel = function(e) {
		myDialog.cancel();
	};
	var handleSubmit = function(e) {
		try {
			if ('' == document.getElementById('eSignMessage').innerHTML) {
        		alert('Please enter e-Signature.');
        		return false;
			}
			
        	if ('Valid e-Sign' != document.getElementById('eSignMessage').innerHTML) {
        		alert(document.getElementById('eSignMessage').innerHTML);
        		return false;
        	}
        	
	        var thisTimeSubmitted = new Date();
	        if (lastTimeSubmitted && thisTimeSubmitted) {
	        	if ( thisTimeSubmitted - lastTimeSubmitted <= 1000 ) { return false; }
		        lastTimeSubmitted = thisTimeSubmitted;
	        }
		} catch(e) {}
		myDialog.submit();
	};
	var onButtonsReady = function() {
	    if (!$E.getListeners($('dialog_submit'))) { $E.addListener($('dialog_submit'), 'click', handleSubmit, false, true); }
	    if (!$E.getListeners($('dialog_cancel'))) { $E.addListener($('dialog_cancel'), 'click', handleCancel); }
	}
	YAHOO.util.Event.onContentReady("saveDialog", onButtonsReady);
	var myButtons = noChange ? 
		[   { text: "Close", handler: handleCancel } ] :
		[
			{ text: "Save",   handler: handleSubmit },
			{ text: "Cancel", handler: handleCancel }
		];
	var onSuccess = function(o) {
		this.hideTransitPanel();
		var respJ = null; // response in JSON format
		try {
			respJ = $J.parse(o.responseText);
		} catch(e) {
			// Log error here
			return;
		}
		if (respJ.result == 0) {
			// alert("Your data was successfully submitted. The response was: " + respJ.resultMsg);
			showFlashPanel(respJ.resultMsg);
			if (window.parent.reloadCoverageGrid) { setTimeout('window.parent.reloadCoverageGrid();', 1500); }
		} else {
			alert("Error: " + respJ.resultMsg);
		}
	};
	var onFailure = function(o) {
		this.hideTransitPanel();
	    alert("Communication Error. Please contact Velos Support: " + o.status);
	};
	var onStart = function(o) {
		this.showTransitPanel('Please wait...');
	}
	myDialog.callback.customevents = { onStart:onStart };
	myDialog.callback.success = onSuccess;
	myDialog.callback.failure = onFailure;
	myDialog.cfg.queueProperty("buttons", myButtons);
	myDialog.render(document.body);
	myDialog.show();
	if (document.getElementById('eSign')) { document.getElementById('eSign').focus(); }
}
*/

VELOS.coverageGrid.prototype.initConfigs = function(args) {
	if (!args) { return false; }
	if (args.constructor != Object) { return false; }
	if (args.urlParams) { this.urlParams = args.urlParams; }
	else { this.dataTable = 'serverpagination'; }
	if ((args.success) && (typeof args.success == 'function')) { 
		this.handleSuccess = args.success;
	}
	if ((args.failure) && (typeof args.failure == 'function')) {
		this.handleSuccess=args.success;
	}
	return true;
}

VELOS.coverageGrid.prototype.startRequest = function() {
	$C.asyncRequest(
		'POST',
		this.url,
		this.callback,
		this.urlParams
	);
}

VELOS.coverageGrid.prototype.render = function() {
	this.startRequest();
}

VELOS.coverageGrid.prototype.loadingPanel = function() {
	// Initialize the temporary Panel to display while waiting for external content to load
	if (!(VELOS.wait)) {
		VELOS.wait = 
			new YAHOO.widget.Panel("wait",  
				{ width:"240px", 
				  fixedcenter:true, 
				  close:false, 
				  draggable:false, 
				  zindex:4,
				  modal:true,
				  visible:false
				} 
			);

		VELOS.wait.setHeader(LoadingPlsWait);/*VELOS.wait.setHeader("Loading, please wait...");*****/
		VELOS.wait.setBody('<img class="asIsImage" src="../images/jpg/loading_pg.gif" />');
		VELOS.wait.render(document.body);
	}
}

VELOS.coverageGrid.prototype.showPanel = function () {
	if (!(VELOS.wait)) { loadingPanel(); }
	VELOS.wait.show();
}

VELOS.coverageGrid.prototype.hidePanel = function() {
	if (VELOS.wait) { VELOS.wait.hide(); }
}
//Modified By parminder singh Bug#11310
function  twosplcharcheck(dataval)
{
  if(dataval.indexOf("\"")!= -1) {
	   var paramArray = ["\""];
	   alert(getLocalizedMessageString("M_NoSplCharAllowed", paramArray));
    return false;
  }
  if(dataval.indexOf("'")!= -1){
	  var paramArray = ["'"];
	   alert(getLocalizedMessageString("M_NoSplCharAllowed", paramArray));
	  return false;
  }
  return true;
}