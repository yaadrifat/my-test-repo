<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%
if (!sessionmaint.isValidSession(request.getSession(false))) {
	return;
}
%>
<s:property escape="false" value="myLinksHTML" />
