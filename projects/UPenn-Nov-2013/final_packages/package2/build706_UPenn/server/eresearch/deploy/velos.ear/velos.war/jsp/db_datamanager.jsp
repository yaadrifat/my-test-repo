<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_Data_ManagersDboard%><%--Data Managers Dashboard*****--%></title>
<link href="./js/tree/xmlTree.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="./js/chart/EJSChart.js"> </script>
<script type="text/javascript" src="./js/tree/xmlTree.js"></script>
<script>
var gChart;
function getElementsById(sId){ 
	var outArray = new Array();	
	if(typeof(sId)!='string' || !sId)
	{
		return outArray;
	};
	
	if(document.evaluate)
	{
		var xpathString = "//*[@id='" + sId.toString() + "']"
		var xpathResult = document.evaluate(xpathString, document, null, 0, null);
		while ((outArray[outArray.length] = xpathResult.iterateNext())) { }
		outArray.pop();
	}
	else if(document.all)
	{
		
		for(var i=0,j=document.all[sId].length;i<j;i++){
			outArray[i] =  document.all[sId][i];
		}
		
	}else if(document.getElementsByTagName)
	{
	
		var aEl = document.getElementsByTagName( '*' );	
		for(var i=0,j=aEl.length;i<j;i+=1){
		
			if(aEl[i].id == sId )
			{
				outArray.push(aEl[i]);
			};
		};	
		
	};
	
	return outArray;
 }

function callAjaxGetStudyData(nodeNo, snodeNo, linkVal){
	var ao=new VELOS.ajaxObject("ajaxStudyDataFetch.jsp", {
	urlData:"nodeNo="+nodeNo + "&snodeNo="+snodeNo  ,
	reqType:"POST",
	outputElement: "Browser" 
	} 
   );
   ao.startRequest();
}

function showChart(num, name){
	var divChart = document.getElementById("onlyChart"+num);
	var dTable = document.getElementById('dataT');
	dTable.innerHTML ="";

	var tempDiv;
	for (var i=0; i<8; i++){
		tempDiv=document.getElementById("onlyChart"+i);
		tempDiv.style.display = "none";
	}
	divChart.style.display = "block";
	
	switch (num){
	case 0:
		//Query Summary
		if (document.getElementById('openCnt')!= undefined){
			callAjaxGetStudyData(0,0, document.getElementById('openCnt').value);
			if (document.getElementById('openCnt').value != 0 || document.getElementById('resolvedCnt').value!=0 || document.getElementById('reopenCnt').value !=0 || document.getElementById('closedCnt').value!=0){
				dTable.innerHTML = '<table >'
				+'	<tr>'
				+'		<table width ="100%" class="outline">'
				+'			<tr>'
				+'				<th><%=LC.L_Status%><%--Status*****--%></th>'
				+'				<th align="center"><%=LC.L_Count%><%--Count*****--%></th>'
				+'			</tr>'
				+'			<tr class="browserOddRow">'
				+'				<td><%=MC.M_MinQry_ClsrTime%><%--Min query closure time*****--%></td>'
				+'				<td align="center">'+ document.getElementById('min_closure').value +'</td>'
				+'			</tr>'
				+'			<tr class="browserEvenRow">'
				+'				<td><%=MC.M_MaxQry_ClsrTime%><%--Max query closure time*****--%></td>'
				+'				<td align="center">'+ document.getElementById('max_closure').value +'</td>'
				+'			</tr>'
				+'			<tr class="browserOddRow">'
				+'				<td><%=MC.M_AvgQry_ClsrTime%><%--Avg query closure time*****--%></td>'
				+'				<td align="center">'+ document.getElementById('avg_closure').value +'</td>'
				+'			</tr>'
				+'			<tr class="browserEvenRow">'
				+'				<td><%=MC.M_Min_QryCls%><%--Min # queries closed*****--%></td>'
				+'				<td align="center">'+ document.getElementById('min_query').value +'</td>'
				+'			</tr>'
				+'			<tr class="browserOddRow">'
				+'				<td><%=MC.M_Max_QryCls%><%--Max # query closed*****--%></td>'
				+'				<td align="center">'+ document.getElementById('max_query').value +'</td>'
				+'			</tr>'
				+'			<tr class="browserEvenRow">'
				+'				<td><%=MC.M_Avg_QryCls%><%--Avg # query closed*****--%></td>'
				+'				<td align="center">'+ document.getElementById('avg_query').value +'</td>'
				+'			</tr>'
				+'		</table>'
				+'	</tr><BR/>'
				+'	<tr>'
				+'		<table width ="100%">'
				+'			<tr>'
				+'				<th><%=LC.L_Status%><%--Status*****--%></th>'
				+'				<th align="center"><%=LC.L_Count%><%--Count*****--%></th>'
				+'			</tr>'
				+'			<tr class="browserOddRow">'
				+'				<td><%=LC.L_Open%><%--Open*****--%></td>'
				+'				<td align="center">'+ document.getElementById('openCnt').value +'</td>'
				+'			</tr>'
				+'			<tr class="browserEvenRow">'
				+'				<td><%=LC.L_Resolved%><%--Resolved*****--%></td>'
				+'				<td align="center">'+ document.getElementById('resolvedCnt').value +'</td>'
				+'			</tr>'
				+'			<tr class="browserOddRow">'
				+'				<td><%=LC.L_Reopnd%><%--Reopened*****--%></td>'
				+'				<td align="center">'+ document.getElementById('reopenCnt').value +'</td>'
				+'			</tr>'
				+'			<tr class="browserEvenRow">'
				+'				<td><%=LC.L_Closed%><%--Closed*****--%></td>'
				+'				<td align="center">'+ document.getElementById('closedCnt').value +'</td>'
				+'			</tr>'
				+'		</table>'
				+'	</tr>'
				+'</table>';
			} 
		}
		break;
	case 1:
		//Today
		if (document.getElementById('today_openCnt')!= undefined){
			callAjaxGetStudyData(1,0, document.getElementById('today_openCnt').value);
			if (document.getElementById('today_openCnt').value != 0 || document.getElementById('today_resolvedCnt').value!=0 || document.getElementById('today_reopenCnt').value !=0 || document.getElementById('today_closedCnt').value!=0){
				dTable.innerHTML = '<table width ="100%"><tr>'
				+ '	<th><%=LC.L_Status%><%--Status*****--%></th>'
				+ '	<th align="center"><%=LC.L_Count%><%--Count*****--%></th>'
				+ '</tr>'
				+ '<tr class="browserOddRow">'
				+ '	<td><%=LC.L_Open%><%--Open*****--%></td>'
				+ '	<td align="center">'+ document.getElementById('today_openCnt').value +'</td>'
				+ '</tr>'
				+ '<tr class="browserEvenRow">'
				+ '	<td><%=LC.L_Resolved%><%--Resolved*****--%></td>'
				+ '	<td align="center">'+ document.getElementById('today_resolvedCnt').value +'</td>'
				+ '</tr>'
				+ '<tr class="browserOddRow">'
				+ '	<td><%=LC.L_Reopnd%><%--Reopened*****--%></td>'
				+ '	<td align="center">'+ document.getElementById('today_reopenCnt').value +'</td>'
				+ '</tr>'
				+ '<tr class="browserEvenRow">'
				+ '	<td><%=LC.L_Closed%><%--Closed*****--%></td>'
				+ '	<td align="center">'+ document.getElementById('today_closedCnt').value +'</td>'
				+ '</tr></table>';
			}
		}
		break;
	case 2:
		//All Open
		if (document.getElementById('open_today')!= undefined){
			callAjaxGetStudyData(2,0, document.getElementById('open_today').value);
			if ((document.getElementById('open_today').value != 0 || document.getElementById('open_last_7d').value != 0) ||(document.getElementById('open_last_15d').value != 0 || document.getElementById('open_last_30d').value != 0)|| document.getElementById('open_gt30d').value != 0){
				dTable.innerHTML = '<table width ="100%">'
				+ '	<tr>'
				+ '		<th><%=LC.L_Open%><%--Open*****--%></th>'
				+ '		<th align="center"><%=LC.L_Count%><%--Count*****--%></th>'
				+ '	</tr>'
				+ '	<tr class="browserOddRow">'
				+ '		<td><%=LC.L_Today%><%--Today*****--%></td>'
				+ '		<td align="center">'+ document.getElementById('open_today').value +'</td>'
				+ '	</tr>'
				+ '	<tr class="browserEvenRow">'
				+ '		<td><%=LC.L_1_7Days%><%--1-7 Days*****--%></td>'
				+ '		<td align="center">'+ document.getElementById('open_last_7d').value +'</td>'
				+ '	</tr>'
				+ '	<tr class="browserOddRow">'
				+ '		<td><%=LC.L_8_15Days%><%--8-15 Days*****--%></td>'
				+ '		<td align="center">'+ document.getElementById('open_last_15d').value +'</td>'
				+ '	</tr>'
				+ '	<tr class="browserEvenRow">'
				+ '		<td><%=LC.L_16_30Days%><%--16-30 Days*****--%></td>'
				+ '		<td align="center">'+ document.getElementById('open_last_30d').value +'</td>'
				+ '	</tr>'
				+ '	<tr class="browserOddRow">'
				+ '		<td><%=LC.L_Gt_30Days%><%--> 30 Days*****--%></td>'
				+ '		<td align="center">'+ document.getElementById('open_gt30d').value +'</a></td>'
				+ '	</tr>'
				+ '</table>';
			}
		}
		break;
	case 3:
		//All Resolved
		if (document.getElementById('resolved_today')!= undefined){
			callAjaxGetStudyData(3,0, document.getElementById('resolved_today').value);
			if ((document.getElementById('resolved_today').value != 0 || document.getElementById('resolved_last_7d').value != 0) ||(document.getElementById('resolved_last_15d').value != 0 || document.getElementById('resolved_last_30d').value != 0)|| document.getElementById('resolved_gt30d').value != 0){
				dTable.innerHTML = '<table width ="100%">'
				+ '	<tr>'
				+ '		<th><%=LC.L_Resolved%><%--Resolved*****--%></th>'
				+ '		<th align="center"><%=LC.L_Count%><%--Count*****--%></th>'
				+ '	</tr>'
				+ '	<tr class="browserOddRow">'
				+ '		<td><%=LC.L_Today%><%--Today*****--%></td>'
				+ '		<td align="center">'+ document.getElementById('resolved_today').value +'</td>'
				+ '	</tr>'
				+ '	<tr class="browserEvenRow">'
				+ '		<td><%=LC.L_1_7Days%><%--1-7 Days*****--%></td>'
				+ '		<td align="center">'+ document.getElementById('resolved_last_7d').value +'</td>'
				+ '	</tr>'
				+ '	<tr class="browserOddRow">'
				+ '		<td><%=LC.L_8_15Days%><%--8-15 Days*****--%></td>'
				+ '		<td align="center">'+ document.getElementById('resolved_last_15d').value +'</td>'
				+ '	</tr>'
				+ '	<tr class="browserEvenRow">'
				+ '		<td><%=LC.L_16_30Days%><%--16-30 Days*****--%></td>'
				+ '		<td align="center">'+ document.getElementById('resolved_last_30d').value +'</td>'
				+ '	</tr>'
				+ '	<tr class="browserOddRow">'
				+ '		<td><%=LC.L_Gt_30Days%><%--> 30 Days*****--%></td>'
				+ '		<td align="center">'+ document.getElementById('resolved_gt30d').value +'</td>'
				+ '	</tr>'
				+ '</table>';
			}
		}
		break;
	case 4:
		//Form Expectancy
		if (document.getElementById('due_today')!= undefined){
			callAjaxGetStudyData(4,0, document.getElementById('due_today').value);
			if ((document.getElementById('due_today').value != 0 || document.getElementById('due_next_7d').value != 0) ||(document.getElementById('due_next_15d').value != 0 || document.getElementById('due_next_30d').value != 0)){
				dTable.innerHTML = '<table width ="100%"><tr>'
				+ '	<th><%=LC.L_Due%><%--Due*****--%></th>'
				+ '	<th align="center"><%=LC.L_Count%><%--Count*****--%></th>'
				+ '</tr>'
				+ '<tr class="browserOddRow">'
				+ '	<td><%=LC.L_Today%><%--Today*****--%></td>'
				+ '	<td align="center">'+ document.getElementById('due_today').value +'</td>'
				+ '</tr>'
				+ '<tr class="browserEvenRow">'
				+ '	<td><%=LC.L_1_7Days%><%--1-7 Days*****--%></td>'
				+ '	<td align="center">'+ document.getElementById('due_next_7d').value +'</td>'
				+ '</tr>'
				+ '<tr class="browserOddRow">'
				+ '	<td><%=LC.L_8_15Days%><%--8-15 Days*****--%></td>'
				+ '	<td align="center">'+ document.getElementById('due_next_15d').value +'</td>'
				+ '</tr>'
				+ '<tr class="browserEvenRow">'
				+ '	<td><%=LC.L_16_30Days%><%--16-30 Days*****--%></td>'
				+ '	<td align="center">'+ document.getElementById('due_next_30d').value +'</td>'
				+ '</tr>'
				+ '</table>';
			}
		}
		break;
	case 5:
		//Overdue Forms
		if (document.getElementById('overdue_7d')!= undefined){
			callAjaxGetStudyData(5,0, document.getElementById('overdue_7d').value);
			if ((document.getElementById('overdue_7d').value != 0 || document.getElementById('overdue_15d').value != 0) ||(document.getElementById('overdue_30d').value != 0 || document.getElementById('overdue_30gt').value != 0)){
				dTable.innerHTML = '<table width ="100%"><tr>'
				+ '	<th><%=LC.L_Overdue_For%><%--Overdue For*****--%></th>'
				+ '	<th align="center"><%=LC.L_Count%><%--Count*****--%></th>'
				+ '</tr>'
				+ '<tr class="browserEvenRow">'
				+ '	<td><%=LC.L_LtEq_7Days%><%--<= 7 Days*****--%></td>'
				+ '	<td align="center">'+ document.getElementById('overdue_7d').value +'</td>'
				+ '</tr>'
				+ '<tr class="browserOddRow">'
				+ '	<td><%=LC.L_8_15Days%><%--8-15 Days*****--%></td>'
				+ '	<td align="center">'+ document.getElementById('overdue_15d').value +'</td>'
				+ '</tr>'
				+ '<tr class="browserEvenRow">'
				+ '	<td><%=LC.L_16_30Days%><%--16-30 Days*****--%></td>'
				+ '	<td align="center">'+ document.getElementById('overdue_30d').value +'</td>'
				+ '</tr>'
				+ '<tr class="browserOddRow">'
				+ '	<td><%=LC.L_Gt_30Days%><%--> 30 Days*****--%></td>'
				+ '	<td align="center">'+ document.getElementById('overdue_30gt').value +'</td>'
				+ '</tr></table>';
			}
		}
		break;
	case 6:
		//Adverse Events
		if (document.getElementById('aeCnt')!= undefined){
			callAjaxGetStudyData(6,0, document.getElementById('aeCnt').value);
			if (document.getElementById('aeCnt').value != "0" || document.getElementById('saeCnt').value != "0"){
				dTable.innerHTML = '<table width ="100%"><tr width="55%">'
				+ '	<td>'
				+ '		<table width="100%">'
				+ '			<tr>'
				+ '				<th><%=LC.L_Event_Type%><%--Event Type*****--%></th>'
				+ '				<th align="center"><%=LC.L_Count%><%--Count*****--%></th>'
				+ '			</tr>'
				+ '			<tr class="browserOddRow">'
				+ '				<td><%=LC.L_Adverse_Event%><%--Adverse Event*****--%></td>'
				+ '				<td align="center">'+ document.getElementById('aeCnt').value+'</td>'
				+ '			</tr>'
				+ '			<tr class="browserEvenRow">'
				+ '				<td><%=LC.L_Serious_AdvEvt%><%--Serious Adverse Event*****--%></td>'
				+ '				<td align="center">'+ document.getElementById('saeCnt').value+'</td>'
				+ '			</tr>'
				+ '		</table>'
				+ '	</td>'
				+ '</tr></table>';
			}
		}
		break;
	case 7:
		//Visit Related
		if (document.getElementById('doneOutCnt')!= undefined){
			callAjaxGetStudyData(7,0, document.getElementById('doneOutCnt').value);
			if (document.getElementById('notDone').value != "'00/00'|0" || document.getElementById('doneOut').value != "'00/00'|0"){
				dTable.innerHTML = '<table width ="100%"><tr width="55%">'
				+ '	<td>'
				+ '		<table width="100%">'
				+ '			<tr>'
				+ '				<th><%=LC.L_Visit_Type%><%--Visit Type*****--%></th>'
				+ '				<th align="center"><%=LC.L_Count%><%--Count*****--%></th>'
				+ '			</tr>'
				+ '			<tr class="browserOddRow">'
				+ '				<td><%=LC.L_DoneOut_Range%><%--Done Outside Range*****--%></td>'
				+ '				<td align="center">'+ document.getElementById('doneOutCnt').value+'</td>'
				+ '			</tr>'
				+ '			<tr class="browserEvenRow">'
				+ '				<td><%=MC.M_NtDone_PastDue%><%--Not Done/ Past Due*****--%></td>'
				+ '				<td align="center">'+ document.getElementById('notDoneCnt').value+'</td>'
				+ '			</tr>'
				+ '		</table>'
				+ '	</td>'
				+ '</tr></table>';
			}
		}
		break;
	}
	return;
}

function createChart(num, name){
	var chart;

	/*if (!isNaN(gChart)){
		gChart.remove();
	}
	divChart.parentNode.removeChild(divChart);
	
	var divParent = document.getElementById("onlyChartParent");
	divParent.innerHTML = '<div id="onlyChart" class="chart" style="border-width: 1px; border-color: #0000ff;  border-style: solid; width: 450px; height: 200px; display:none"></div>';*/
	divChart = document.getElementById("onlyChart"+num);
	
	switch (num){
	case 0:
		//Query Summary
		if (document.getElementById('openCnt')!= undefined){
			//callAjaxGetStudyData(0,0, document.getElementById('openCnt').value);
			if (document.getElementById('openCnt').value != 0 || document.getElementById('resolvedCnt').value!=0 || document.getElementById('reopenCnt').value !=0 || document.getElementById('closedCnt').value!=0){
				chart = new EJSC.Chart("onlyChart"+num, { 
				    show_legend: false,
				    allow_zoom: false,
				    show_mouse_position: false,
				    title: '<%=LC.L_Query_Summary%><%--Query Summary*****--%> '
				}); 
				//gChart = chart;
				chart.addSeries(new EJSC.PieSeries( 
				    new EJSC.CSVStringDataHandler( document.getElementById('openCnt').value + '|"Open",'
				    + document.getElementById('resolvedCnt').value + '|"Resolved",'
				    + document.getElementById('reopenCnt').value + '|"Reopened",'
				    + document.getElementById('closedCnt').value + '|"Closed"'),
				  	{ 
				      defaultColors: [ 
				          'rgb(255,255,0)',  //Yellow 
				          'rgb(35,107,142)',  //SteelBlue 
				          'rgb(112,219,147)',  //AquaMarine 
				          'rgb(178,34,34)'  //FireBrick 
				      ],
				      title: '<%=LC.L_Query_Summary%><%--Query Summary*****--%> '
				    } 
				));
			} else {
				divChart.innerHTML ="<b><%=MC.M_NoDataDboard_QrySumry%><%--No data for dashboard- Query Summary*****--%></b>";
			}
		} else {
			divChart.innerHTML ="<b><%=MC.M_FtchDboard_QrySmry%><%--Fetching Data for dashboard- Query Summary*****--%> ....</b>";
		}
		break;
	case 1:
		//Today
		if (document.getElementById('today_openCnt')!= undefined){
			//callAjaxGetStudyData(1,0, document.getElementById('today_openCnt').value);
			if (document.getElementById('today_openCnt').value != 0 || document.getElementById('today_resolvedCnt').value!=0 || document.getElementById('today_reopenCnt').value !=0 || document.getElementById('today_closedCnt').value!=0){
				var paramArray = [name];
				chart = new EJSC.Chart("onlyChart"+num, { 
				    show_legend: false,
				    allow_zoom: false,
				    show_mouse_position: false,
				    title: getLocalizedMessageString("L_Qry_Act",paramArray)/*Query Activity name*****/ 
				}); 
				//gChart = chart;
				chart.addSeries(new EJSC.PieSeries( 
				    new EJSC.CSVStringDataHandler( document.getElementById('today_openCnt').value + '|"Open",'
				    + document.getElementById('today_resolvedCnt').value + '|"Resolved",'
				    + document.getElementById('today_reopenCnt').value + '|"Re-opened",'
				    + document.getElementById('today_closedCnt').value + '|"Closed"'),
					{ 
					    defaultColors: [ 
					       'rgb(255,255,0)',  //Yellow 
					       'rgb(35,107,142)',  //SteelBlue 
					       'rgb(112,219,147)',  //AquaMarine 
					       'rgb(178,34,34)'  //FireBrick  
					   ] 
					 } 
				));
			} else{
				var paramArray = [name];
				divChart.innerHTML =getLocalizedMessageString("M_NoDataDboard_QryActv",paramArray)/*<b>No data for dashboard- Query Activity name</b>*****/;
			}
		} else {
			var paramArray = [name];
			divChart.innerHTML =getLocalizedMessageString("M_FetchDboard_QryAct",paramArray)/*<b>Fetching Data for dashboard- Query Activity {0} ....</b>*****/;
		}
		break;
	case 2:
		//All Open
		if (document.getElementById('today_openCnt')!= undefined){
			//callAjaxGetStudyData(2,0, document.getElementById('open_today').value);
			if ((document.getElementById('open_today').value != 0 || document.getElementById('open_last_7d').value != 0) ||(document.getElementById('open_last_15d').value != 0 || document.getElementById('open_last_30d').value != 0)|| document.getElementById('open_gt30d').value != 0){
				//Set Y-axis Xtreme
				var yXtreme=0;
				var paramArray = [name];
				if (parseInt(document.getElementById('open_today').value) > yXtreme){
					yXtreme = parseInt(document.getElementById('open_today').value);
				}
				if (parseInt(document.getElementById('open_last_7d').value) > yXtreme){
					yXtreme = parseInt(document.getElementById('open_last_7d').value);
				}
				if (parseInt(document.getElementById('open_last_15d').value) > yXtreme){
					yXtreme = parseInt(document.getElementById('open_last_15d').value);
				}
				if (parseInt(document.getElementById('open_last_30d').value) > yXtreme){
					yXtreme = parseInt(document.getElementById('open_last_30d').value);
				}
				if (parseInt(document.getElementById('open_gt30d').value) > yXtreme){
					yXtreme = parseInt(document.getElementById('open_gt30d').value);
				}
				
				chart = new EJSC.Chart("onlyChart"+num, { 
				    show_legend: false,
				    allow_zoom: false,
				    show_mouse_position: false,
				    title: getLocalizedMessageString("L_Qry_Act",paramArray)/*Query Activity name*****/
				}); 
				//gChart = chart;
				chart.setYExtremes(0, yXtreme);
				chart.addSeries(new EJSC.BarSeries( 
					new EJSC.ArrayDataHandler([ 
				    ['<%=LC.L_Today%><%--Today*****--%>',document.getElementById('open_today').value],
				    ['<%=LC.L_1To7_Days%><%--1-7 <BR>Days*****--%>',document.getElementById('open_last_7d').value],
				    ['<%=LC.L_8To15_Days%><%--8-15 <BR>Days*****--%>',document.getElementById('open_last_15d').value],
				    ['<%=LC.L_16To30_Days%><%--L_16To30_Days*****--%>',document.getElementById('open_last_30d').value],
				    ['<%=LC.L_Gt30_Days%><%--> 30 <BR>Days*****--%>',document.getElementById('open_gt30d').value]
				   ]), 
				   { 
				      groupedBars: false, 
				      color:  'rgb(178,34,34)',  //FireBrick  
				      opacity: 0,
				      title: "<%=LC.L_Open%><%--Open*****--%>"
				    } 
				    ) 
				); 
			}else{
				var paramArray = [name];
				divChart.innerHTML =getLocalizedMessageString("M_NoDataDboard_QryActv",paramArray)/*<b>No data for dashboard- Query Activity name</b>*****/;
			}
		} else {
			var paramArray = [name];
			divChart.innerHTML =getLocalizedMessageString("M_FetchDboard_QryAct",paramArray)/*<b>Fetching Data for dashboard- Query Activity {0} ....</b>*****/;
		}
		break;
	case 3:
		//All Resolved
		if (document.getElementById('resolved_today')!= undefined){
			//callAjaxGetStudyData(3,0, document.getElementById('resolved_today').value);
			if ((document.getElementById('resolved_today').value != 0 || document.getElementById('resolved_last_7d').value != 0) ||(document.getElementById('resolved_last_15d').value != 0 || document.getElementById('resolved_last_30d').value != 0)|| document.getElementById('resolved_gt30d').value != 0){
				//Set Y-axis Xtreme
				var yXtreme=0;
				var paramArray = [name];
				if (parseInt(document.getElementById('resolved_today').value) > yXtreme){
					yXtreme = parseInt(document.getElementById('resolved_today').value);
				}
				if (parseInt(document.getElementById('resolved_last_7d').value) > yXtreme){
					yXtreme = parseInt(document.getElementById('resolved_last_7d').value);
				}
				if (parseInt(document.getElementById('resolved_last_15d').value) > yXtreme){
					yXtreme = parseInt(document.getElementById('resolved_last_15d').value);
				}
				if (parseInt(document.getElementById('resolved_last_30d').value) > yXtreme){
					yXtreme = parseInt(document.getElementById('resolved_last_30d').value);
				}
				if (parseInt(document.getElementById('resolved_gt30d').value) > yXtreme){
					yXtreme = parseInt(document.getElementById('resolved_gt30d').value);
				}
				
				chart = new EJSC.Chart("onlyChart"+num, { 
				    show_legend: false,
				    allow_zoom: false,
				    show_mouse_position: false,
				    title: getLocalizedMessageString("L_Qry_Act",paramArray)/*Query Activity name*****/
				}); 
				//gChart = chart;
				chart.setYExtremes(0, yXtreme);
				chart.addSeries(new EJSC.BarSeries( 
					new EJSC.ArrayDataHandler([ 
				    ['<%=LC.L_Today%><%--Today*****--%>',document.getElementById('resolved_today').value],
				    ['<%=LC.L_1To7_Days%><%--1-7 <BR>Days*****--%>',document.getElementById('resolved_last_7d').value],
				    ['<%=LC.L_8To15_Days%><%--8-15 <BR>Days*****--%>',document.getElementById('resolved_last_15d').value],
				    ['<%=LC.L_16To30_Days%><%--L_16To30_Days*****--%>',document.getElementById('resolved_last_30d').value],
				    ['<%=LC.L_Gt30_Days%><%--> 30 <BR>Days*****--%>',document.getElementById('resolved_gt30d').value]
				   ]), 
				   { 
				      groupedBars: false, 
				      color: 'rgb(200,0,0)', 
				      opacity: 0,
				      title: "<%=LC.L_Resolved%><%--Resolved*****--%>"
				    } 
				    ) 
				); 
			}else{
				var paramArray = [name];
				divChart.innerHTML =getLocalizedMessageString("M_NoDataDboard_QryActv",paramArray)/*<b>No data for dashboard- Query Activity name</b>*****/;
			}
		} else {
			var paramArray = [name];
			divChart.innerHTML =getLocalizedMessageString("M_FetchDboard_QryAct",paramArray)/*Fetching Data for dashboard- Query Activity {0} ....</b>*****/;
		}
		break;
	case 4:
		//Form Expectancy
		if (document.getElementById('due_today')!= undefined){
			//callAjaxGetStudyData(4,0, document.getElementById('due_today').value);
			if ((document.getElementById('due_today').value != 0 || document.getElementById('due_next_7d').value != 0) ||(document.getElementById('due_next_15d').value != 0 || document.getElementById('due_next_30d').value != 0)){
				var paramArray = [name];
				chart = new EJSC.Chart("onlyChart"+num, { 
				    show_legend: false,
				    allow_zoom: false,
					    show_mouse_position: false,
				    title: getLocalizedMessageString("L_Frm_Exptcy",paramArray)/*Form Expectancy name*****/
				}); 
				//gChart = chart;
				chart.addSeries(new EJSC.PieSeries( 
				    new EJSC.ArrayDataHandler([[document.getElementById('due_today').value,"Due Today"],
				    [document.getElementById('due_next_7d').value,"Due 1-7 Days"],
				    [document.getElementById('due_next_15d').value,"Due 8-15 Days"],
				    [document.getElementById('due_next_30d').value,"Due 16-30 Days"]] 
				  ),
				  { 
				      defaultColors: [ 
				          'rgb(112,219,147)',  //AquaMarine 
				          'rgb(255,140,0)',  //DarkOrange 
				          'rgb(153,50,204)',  //DarkOrchid 
				          'rgb(178,34,34)'  //FireBrick 
				      ],
				      title: '<%=LC.L_Form_Expectancy%><%--Form Expectancy*****--%>'
				    } 
				));
			}else{
				var paramArray = [name];
				divChart.innerHTML =getLocalizedMessageString("M_NoDataDboard_FrmExptcy",paramArray)/*<b>No data for dashboard- Form Expectancy {0}</b>*****/;
			}
		} else {
			var paramArray = [name];
			divChart.innerHTML =getLocalizedMessageString("M_FetchDboard_FrmExptcy",paramArray)/*<b>Fetching Data for dashboard- Form Expectancy {0} ....</b>*****/;
		}
		break;
	case 5:
		//Overdue Forms
		if (document.getElementById('overdue_7d')!= undefined){
			//callAjaxGetStudyData(5,0, document.getElementById('overdue_7d').value);
			if ((document.getElementById('overdue_7d').value != 0 || document.getElementById('overdue_15d').value != 0) ||(document.getElementById('overdue_30d').value != 0 || document.getElementById('overdue_30gt').value != 0)){
				var paramArray = [name];
				chart = new EJSC.Chart("onlyChart"+num, { 
				    show_legend: false,
				    allow_zoom: false,
					    show_mouse_position: false,
				    title: getLocalizedMessageString("L_Overdue_Frms",paramArray)/*Overdue Forms name*****/
				}); 
				//gChart = chart;
				chart.addSeries(new EJSC.PieSeries( 
				    new EJSC.ArrayDataHandler([[document.getElementById('overdue_7d').value,"Overdue <= 7 Days"],
				    [document.getElementById('overdue_15d').value,"Overdue 8-15 Days"],
				    [document.getElementById('overdue_30d').value,"Overdue 16-30 Days"],
				   	[document.getElementById('overdue_30gt').value,"Overdue > 30 Days"]] 
				  ),
				  { 
				      defaultColors: [ 
				          'rgb(255,255,0)',  //Yellow 
				          'rgb(112,219,147)',  //AquaMarine 
				          'rgb(255,140,0)',  //DarkOrange 
				          'rgb(153,50,204)',  //DarkOrchid 
				          'rgb(178,34,34)'  //FireBrick 
				      ],
				      title: '<%=LC.L_Overdue_Forms%><%--Overdue Forms*****--%>'
				    } 
				));
			}else{
				var paramArray = [name];
				divChart.innerHTML =getLocalizedMessageString("M_NoDataDboard_OverFrms",paramArray)/*<b>No data for dashboard- Overdue Forms {0}</b>*****/;
			}
		} else {
			var paramArray = [name];
			divChart.innerHTML =getLocalizedMessageString("M_FetchDboard_OverFrm",paramArray)/*<b>Fetching Data for dashboard- Overdue Forms {0} ....</b>*****/;
		}
		break;
	case 6:
		//Adverse Events
		if (document.getElementById('aeCnt')!= undefined){
			//callAjaxGetStudyData(6,0, document.getElementById('aeCnt').value);
			if (document.getElementById('aeCnt').value != "0" || document.getElementById('saeCnt').value != "0"){
				var paramArray = [name];
				chart = new EJSC.Chart("onlyChart"+num,{
					show_legend: false,
				    allow_zoom: false,
				    show_mouse_position: false,
				    title: getLocalizedMessageString("L_Adv_Evt",paramArray)/*Adverse Events name*****/
				}); 
				//gChart = chart;
				chart.addSeries(new EJSC.LineSeries( 
					new EJSC.CSVStringDataHandler(document.getElementById('aeSeries').value), 
				    { title: "<%=LC.L_Adverse_Events%><%--Adverse Events*****--%>" } 
				));
				chart.addSeries(new EJSC.LineSeries( 
					new EJSC.CSVStringDataHandler(document.getElementById('saeSeries').value), 
				    { title: "<%=LC.L_Serious_AdvEvt%><%--Serious Adverse Event*****--%>" } 
				));
			}else{
				var paramArray = [name];
				divChart.innerHTML =getLocalizedMessageString("M_NoDataDboard_AdvEvts",paramArray)/*<b>No data for dashboard- Adverse Events {0}</b>*****/;
			}
		} else {
			var paramArray = [name];
			divChart.innerHTML =getLocalizedMessageString("M_FetchDboard_AdvEvt",paramArray)/*<b>Fetching Data for dashboard- Adverse Events {0} ....</b>*****/;
		}
		break;
	case 7:
		//Visit Related
		if (document.getElementById('notDone')!= undefined){
			//callAjaxGetStudyData(7,0, document.getElementById('doneOutCnt').value);
			if (document.getElementById('notDone').value != "'00/00'|0" || document.getElementById('doneOut').value != "'00/00'|0"){
				var paramArray = [name];
				chart = new EJSC.Chart("onlyChart"+num,{
					show_legend: false,
				    allow_zoom: false,
				    show_mouse_position: false,
				    title: getLocalizedMessageString(" L_Vist_Rel",paramArray)/*Visit related name*****/,
				    x_axis_size: 50 
				}); 
				//gChart = chart;
				chart.addSeries(new EJSC.LineSeries( 
					new EJSC.CSVStringDataHandler(document.getElementById('notDone').value), 
				    { title: "<%=MC.M_NtDone_PastDue%><%--Not Done/ Past Due*****--%>" } 
				));
				chart.addSeries(new EJSC.LineSeries( 
					new EJSC.CSVStringDataHandler(document.getElementById('doneOut').value), 
				    { title: "<%=LC.L_DoneOut_Range%><%--Done Outside Range*****--%>" } 
				));
			}else{
				var paramArray = [name];
				divChart.innerHTML =getLocalizedMessageString("M_NoDataDboard_VstReltd",paramArray)/*<b>No data for dashboard- Visit related {0}</b>*****/;
			}
		} else {
			var paramArray = [name];
			divChart.innerHTML =getLocalizedMessageString("M_FetchDboard_VstRelated",paramArray)/*<b>Fetching Data for dashboard- Visit related {0} ....</b>******/;
		}
		break;
	}
	return;
}
function toggleBranch(arrayID, index){
	var linkArray = getElementsById(arrayID);
	for(var i = 0; i < linkArray.length; i++){
		if (i != index){
			linkArray[i].style.display = "none";
		} else {
			if (linkArray[i].style.display == "block"){
				linkArray[i].style.display = "none";
			} else{
				linkArray[i].style.display = "block";
				linkArray[i].focus;
			}
		}
	}
}
function confirmBox(name){
	var paramArray = [name];
    if (confirm(getLocalizedMessageString("M_Want_DelStd",paramArray))) {/* if (confirm("Do you want to delete <%=LC.Std_Study%> "+ name+"?" )) {*****/
		return true;
	}  else {
		return false;
	}
}//km

function opensvwin(id) {
     windowName = window.open("studyview.jsp?studyVPk="+ id,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=500,height=400")
	 windowName.focus();
;}

function setOrder(formObj,orderBy) //orderBy column number
{
	var lorderType;
	if (formObj.orderType.value=="asc") {
		formObj.orderType.value= "desc";		
		lorderType="desc";
	} else if (formObj.orderType.value=="desc") {
		formObj.orderType.value= "asc";
		lorderType="asc";	
	}

	lsrc = formObj.srcmenu.value;
	lcurPage = formObj.page.value;
	formObj.action="irb.jsp?srcmenu="+lsrc+"&page="+lcurPage+"&orderBy="+orderBy+"&orderType="+lorderType;
	formObj.submit(); 
}

function setOrderView(formObj,orderBy) //orderBy column number
{
	var lorderType;
	if (formObj.orderTypeView.value=="asc") {
		formObj.orderTypeView.value= "desc";		
		lorderType="desc";
	} else 	if (formObj.orderTypeView.value=="desc") {
		formObj.orderTypeView.value= "asc";
		lorderType="asc";	
	}

	lsrc = formObj.srcmenu.value;
	lcurPage = formObj.pageView.value;
	formObj.action="db_irb2.jsp?srcmenu="+lsrc+"&pageView="+lcurPage+"&orderByView="+orderBy+"&orderTypeView="+lorderType;
	formObj.submit(); 
}
</script>
<jsp:include page="ui-include.jsp" flush="true"></jsp:include>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id="groupB" scope="page" class="com.velos.eres.web.group.GroupJB"/>
<%@ page language = "java" import = "com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.EJBUtil,java.util.*,com.velos.eres.service.util.BrowserRows,com.velos.eres.service.util.Configuration,java.sql.*,com.velos.eres.business.common.*,com.velos.eres.service.util.StringUtil,com.velos.eres.service.util.*"%>

<body>
<%
	//Declarations
	String src="tdMenuBarItem3";
	String countSql;

	long rowsPerPage=0;
	long totalPages=0;	
	long rowsReturned = 0;
	long showPages = 0;
	long totalRows = 0;	   
	long firstRec = 0;
	long lastRec = 0;	   
	int openCount = 0;
	int resolvedCount = 0;
	int reopenCount = 0;
	int closedCount = 0;
	int enrolled = 0;

	String studyNumber = null;
	String studyTitle = null;
	String studyStatus = null;
	String studyActualDt = null;
	String studyIRB = null;
	String studyIRB2 = null;
	String studySite = null;
	String studyPI = null;

	int studyId=0;
	boolean hasMore = false;
	boolean hasPrevious = false;
	CodeDao  cd = new CodeDao();
	String codeLst= cd.getValForSpecificSubType();
	HttpSession tSession = request.getSession(true); 
	String userId= (String) tSession.getValue("userId");
	
	String pagenum = "";
	int curPage = 0;
	String orderBy = "";
	orderBy = request.getParameter("orderBy");

	String orderType = "";
	orderType = request.getParameter("orderType");

	BrowserRows pr;
	BrowserRows br;
	
	long startPage = 1;
	long cntr = 0;
	String pagenumView = request.getParameter("pageView");
	if (pagenumView == null) {pagenumView = "1";}
	int curPageView = EJBUtil.stringToNum(pagenumView);
	
	long startPageView = 1;
	long cntrView = 0;
	String defGroup;
	String groupName;
	String rightSeq;
	
	String orderByView = "";
	orderByView = request.getParameter("orderByView");

	String orderTypeView = "";
	orderTypeView = request.getParameter("orderTypeView");
	
	int usrId;
	CodeDao cd1 = new CodeDao();
	
	ArrayList aRightSeq ;
	int iRightSeq ;
	
	int pageRight = 0;
	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("NPROTOCOL"));
	
	if (sessionmaint.isValidSession(tSession)) {
		if (pageRight > 0 )	{
			int openCnt =0;
			int resolvedCnt =0;
			int reopenCnt =0;
			int closedCnt =0;
			
			String queryCount = "SELECT "
			+ "SUM(DECODE(fk_codelst_querystatus,(select pk_codelst from er_codelst where codelst_type = 'query_status' and codelst_subtyp = 'open'),1,0)) AS open_count, "
			+ "SUM(DECODE(fk_codelst_querystatus,(select pk_codelst from er_codelst where codelst_type = 'query_status' and codelst_subtyp = 'resolved'),1,0)) AS resolved_count, "
			+ "SUM(DECODE(fk_codelst_querystatus,(select pk_codelst from er_codelst where codelst_type = 'query_status' and codelst_subtyp = 're-opened'),1,0)) AS reopen_count, "
			+ "SUM(DECODE(fk_codelst_querystatus,(select pk_codelst from er_codelst where codelst_type = 'query_status' and codelst_subtyp = 'closed'),1,0)) AS closed_count "
			+ "FROM ER_FORMQUERY a, ER_FORMQUERYSTATUS b, ER_FORMFLD c, ER_FORMSEC d, ER_FORMLIB e, ER_PATFORMS f, ER_PATPROT g,er_study z "
			+ "WHERE b.fk_formquery = a.pk_formquery "
			+ "AND a.fk_field = c.fk_field "
			+ "AND d.pk_formsec = c.fk_formsec "
			+ "AND e.pk_formlib = d.fk_formlib "
			+ "AND pk_formlib = f.fk_formlib "
			+ "AND pk_patforms = fk_querymodule "
			+ "AND pk_patprot = fk_patprot "
			+ "AND pk_study = g.fk_study(+) "
			+ "AND b.pk_formquerystatus = (SELECT max(x.pk_formquerystatus) FROM ER_FORMQUERYSTATUS x WHERE x.fk_formquery = a.pk_formquery "
			+ "AND x.fk_codelst_querystatus = b.fk_codelst_querystatus "
			+ "AND trunc(b.entered_on) = (SELECT max(trunc(y.entered_on)) FROM ER_FORMQUERYSTATUS y WHERE y.fk_formquery = a.pk_formquery "
			+ "AND y.fk_codelst_querystatus = b.fk_codelst_querystatus)) "
			+ "AND (EXISTS(select * from er_studyteam h where pk_study = h.fk_study and h.fk_user = "+userId+" and nvl(studyteam_status,'Active') = 'Active') "
			+ "OR Pkg_Phi.F_Is_Superuser("+ userId +", pk_study) = 1) "
			+ "AND f.RECORD_TYPE <> 'D'";
			
			BrowserRows query = new BrowserRows();
			query.getPageRows(1,10,queryCount,50,"select 1 from dual","1","asc");
		
			openCnt = EJBUtil.stringToNum(query.getBValues(1,"open_count"));
			resolvedCnt = EJBUtil.stringToNum(query.getBValues(1,"resolved_count"));
			reopenCnt = EJBUtil.stringToNum(query.getBValues(1,"reopen_count"));
			closedCnt = EJBUtil.stringToNum(query.getBValues(1,"closed_count")); 
		%>
		<Input type="hidden" id="openCnt" name="openCnt" value="<%=openCnt%>"/>
		<Input type="hidden" id="resolvedCnt" name="resolvedCnt" value="<%=resolvedCnt%>"/>
		<Input type="hidden" id="reopenCnt" name="reopenCnt" value="<%=reopenCnt%>"/>
		<Input type="hidden" id="closedCnt" name="closedCnt" value="<%=closedCnt%>"/>
		<%
			int min_query=0;
			int max_query=0;
			float avg_query=0;
			int min_closure=0;
			int max_closure=0;
			float avg_closure=0;
			
			queryCount = "SELECT MIN(COUNT(*)) AS min_query, MAX(COUNT(*)) AS max_query, ROUND(AVG(COUNT(*)),2) AS avg_query "
			+ "FROM ER_FORMQUERY a, ER_FORMQUERYSTATUS b, ER_FORMFLD c, ER_FORMSEC d, ER_FORMLIB e, ER_PATFORMS f, ER_PATPROT g,er_study z, "
			+ "ER_FORMQUERY aa, ER_FORMQUERYSTATUS bb "
			+ "WHERE a.pk_formquery = aa.pk_formquery "
			+ "AND b.fk_formquery = a.pk_formquery "
			+ "AND a.fk_field = c.fk_field "
			+ "AND d.pk_formsec = c.fk_formsec "
			+ "AND e.pk_formlib = d.fk_formlib "
			+ "AND pk_formlib = f.fk_formlib "
			+ "AND pk_patforms = a.fk_querymodule "
			+ "AND pk_patprot = fk_patprot "
			+ "AND pk_study = g.fk_study(+) "
			+ "AND bb.fk_formquery = aa.pk_formquery "
			+ "AND aa.fk_field = c.fk_field "
			+ "AND pk_patforms = aa.fk_querymodule "
			+ "AND b.pk_formquerystatus = (SELECT MAX(x.pk_formquerystatus) FROM ER_FORMQUERYSTATUS x WHERE x.fk_formquery = a.pk_formquery "
			+ "AND x.fk_codelst_querystatus = b.fk_codelst_querystatus "
			+ "AND TRUNC(b.entered_on) = (SELECT MAX(TRUNC(y.entered_on)) FROM ER_FORMQUERYSTATUS y WHERE y.fk_formquery = a.pk_formquery "
			+ "AND y.fk_codelst_querystatus = b.fk_codelst_querystatus) "
			+ "AND x.fk_codelst_querystatus IN (SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'query_status' AND codelst_subtyp = 'open')) "
			+ "AND bb.pk_formquerystatus = (SELECT MAX(x.pk_formquerystatus) FROM ER_FORMQUERYSTATUS x WHERE x.fk_formquery = aa.pk_formquery "
			+ "AND x.fk_codelst_querystatus = bb.fk_codelst_querystatus "
			+ "AND TRUNC(bb.entered_on) = (SELECT MAX(TRUNC(y.entered_on)) FROM ER_FORMQUERYSTATUS y WHERE y.fk_formquery = aa.pk_formquery "
			+ "AND y.fk_codelst_querystatus = bb.fk_codelst_querystatus) "
			+ "AND x.fk_codelst_querystatus IN (SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'query_status' AND codelst_subtyp = 'closed')) "
			+ "AND (EXISTS (SELECT * FROM er_studyteam h WHERE pk_study = h.fk_study AND h.fk_user = "+ userId +" AND NVL(studyteam_status,'Active') = 'Active') "
			+ "OR Pkg_Phi.F_Is_Superuser("+ userId +", pk_study) = 1) "
			+ "AND TRUNC(b.entered_on)=TRUNC(bb.entered_on) "
			+ "AND f.RECORD_TYPE <> 'D' "
			+ "GROUP BY TRUNC(bb.entered_on)";
			
			query = new BrowserRows();
			query.getPageRows(1,10,queryCount,50,"select 1 from dual","1","asc");
			
			if (query.getRowReturned()!= 0){
				min_query = EJBUtil.stringToNum(query.getBValues(1,"min_query"));
				max_query = EJBUtil.stringToNum(query.getBValues(1,"max_query"));
				avg_query = EJBUtil.stringToFloat(query.getBValues(1,"avg_query"));
			}
			
			queryCount = "SELECT MIN( TRUNC(bb.entered_on)- TRUNC(b.entered_on)) AS min_closure, MAX( TRUNC(bb.entered_on)- TRUNC(b.entered_on)) AS max_closure, "
			+ "ROUND(AVG(TRUNC(bb.entered_on)- TRUNC(b.entered_on)),2) AS avg_closure "
			+ "FROM ER_FORMQUERY a, ER_FORMQUERYSTATUS b, ER_FORMFLD c, ER_FORMSEC d, ER_FORMLIB e, ER_PATFORMS f, ER_PATPROT g,er_study z, "
			+ "ER_FORMQUERY aa, ER_FORMQUERYSTATUS bb "
			+ "WHERE a.pk_formquery = aa.pk_formquery "
			+ "AND b.fk_formquery = a.pk_formquery "
			+ "AND a.fk_field = c.fk_field "
			+ "AND d.pk_formsec = c.fk_formsec "
			+ "AND e.pk_formlib = d.fk_formlib "
			+ "AND pk_formlib = f.fk_formlib "
			+ "AND pk_patforms = a.fk_querymodule "
			+ "AND pk_patprot = fk_patprot "
			+ "AND pk_study = g.fk_study(+) "
			+ "AND bb.fk_formquery = aa.pk_formquery "
			+ "AND aa.fk_field = c.fk_field "
			+ "AND pk_patforms = aa.fk_querymodule "
			+ "AND b.pk_formquerystatus = (SELECT MAX(x.pk_formquerystatus) FROM ER_FORMQUERYSTATUS x WHERE x.fk_formquery = a.pk_formquery "
			+ "AND x.fk_codelst_querystatus = b.fk_codelst_querystatus "
			+ "AND TRUNC(b.entered_on) = (SELECT MAX(TRUNC(y.entered_on)) FROM ER_FORMQUERYSTATUS y WHERE y.fk_formquery = a.pk_formquery "
			+ "AND y.fk_codelst_querystatus = b.fk_codelst_querystatus) "
			+ "AND x.fk_codelst_querystatus IN (SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'query_status' AND codelst_subtyp = 'open')) "
			+ "AND bb.pk_formquerystatus = (SELECT MAX(x.pk_formquerystatus) FROM ER_FORMQUERYSTATUS x WHERE x.fk_formquery = aa.pk_formquery "
			+ "AND x.fk_codelst_querystatus = bb.fk_codelst_querystatus "
			+ "AND TRUNC(bb.entered_on) = (SELECT MAX(TRUNC(y.entered_on)) FROM ER_FORMQUERYSTATUS y WHERE y.fk_formquery = aa.pk_formquery "
			+ "AND y.fk_codelst_querystatus = bb.fk_codelst_querystatus) "
			+ "AND x.fk_codelst_querystatus IN (SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'query_status' AND codelst_subtyp = 'closed')) "
			+ "AND (EXISTS (SELECT * FROM er_studyteam h WHERE pk_study = h.fk_study AND h.fk_user = " +userId+ " AND NVL(studyteam_status,'Active') = 'Active')"
			+ "OR Pkg_Phi.F_Is_Superuser("+ userId +", pk_study) = 1) "
			+ "AND f.RECORD_TYPE <> 'D'";
			
			query = new BrowserRows();
			query.getPageRows(1,10,queryCount,50,"select 1 from dual","1","asc");
		
			if (query.getRowReturned()!= 0){
				min_closure = EJBUtil.stringToNum(query.getBValues(1,"min_closure"));
				max_closure = EJBUtil.stringToNum(query.getBValues(1,"max_closure"));
				avg_closure = EJBUtil.stringToFloat(query.getBValues(1,"avg_closure"));
			}
		%>
		<Input type="hidden" id="min_query" name="min_query" value="<%=min_query%>"/>
		<Input type="hidden" id="max_query" name="max_query" value="<%=max_query%>"/>
		<Input type="hidden" id="avg_query" name="avg_query" value="<%=avg_query%>"/>
		<Input type="hidden" id="min_closure" name="min_closure" value="<%=min_closure%>"/>
		<Input type="hidden" id="max_closure" name="max_closure" value="<%=max_closure%>"/>
		<Input type="hidden" id="avg_closure" name="avg_closure" value="<%=avg_closure%>"/>
		<%
			int today_openCnt =0;
			int today_resolvedCnt =0;
			int today_reopenCnt =0;
			int today_closedCnt =0;
			
			int IRCnt =0;
			int ContCnt =0;
			int AmendCnt =0;
			int DevCnt =0;
			
			String IReview="'00/00'|0";
			String Conts="'00/00'|0";
			String Amends="'00/00'|0";
			String Devs="'00/00'|0";
			
			String tempIReview;
			String tempConts;
			String tempAmends;
			String tempDevs;
			
			queryCount = "SELECT 0 AS  DAY, to_char(TRUNC(SYSDATE), 'mm/dd/yyyy') AS DT, SUM(open_count) AS InitialReview, SUM(resolved_count) AS Continuations, SUM(reopen_count) AS Amendments, SUM(closed_count) AS Deviations"
			+" FROM (SELECT "
			+" SUM(DECODE(fk_codelst_querystatus,(SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'query_status' AND codelst_subtyp = 'open'),1,0)) AS open_count, "
			+" SUM(DECODE(fk_codelst_querystatus,(SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'query_status' AND codelst_subtyp = 'resolved'),1,0)) AS resolved_count, "
			+" SUM(DECODE(fk_codelst_querystatus,(SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'query_status' AND codelst_subtyp = 're-opened'),1,0)) AS reopen_count, "
			+" SUM(DECODE(fk_codelst_querystatus,(SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'query_status' AND codelst_subtyp = 'closed'),1,0)) AS closed_count,"
			+" CASE WHEN TRUNC(b.entered_on) = TRUNC(SYSDATE) THEN TRUNC(b.entered_on) END AS dt"
			+" FROM ER_FORMQUERY a, ER_FORMQUERYSTATUS b, ER_FORMFLD c, ER_FORMSEC d, ER_FORMLIB e, ER_PATFORMS f, ER_PATPROT g,er_study z "
			+" WHERE b.fk_formquery = a.pk_formquery "
			+" AND a.fk_field = c.fk_field "
			+" AND d.pk_formsec = c.fk_formsec "
			+" AND e.pk_formlib = d.fk_formlib "
			+" AND pk_formlib = f.fk_formlib "
			+" AND pk_patforms = fk_querymodule "
			+" AND pk_patprot = fk_patprot "
			+" AND pk_study = g.fk_study(+) "
			+" AND b.pk_formquerystatus = (SELECT MAX(x.pk_formquerystatus) FROM ER_FORMQUERYSTATUS x WHERE x.fk_formquery = a.pk_formquery "
			+" AND x.fk_codelst_querystatus = b.fk_codelst_querystatus "
			+" AND trunc(b.entered_on) = (SELECT MAX(trunc(y.entered_on)) FROM ER_FORMQUERYSTATUS y WHERE y.fk_formquery = a.pk_formquery "
			+" AND y.fk_codelst_querystatus = b.fk_codelst_querystatus)) "
			+" AND (EXISTS (SELECT * FROM er_studyteam h WHERE pk_study = h.fk_study AND h.fk_user = "+userId+" AND NVL(studyteam_status,'Active') = 'Active')"
			+" OR Pkg_Phi.F_Is_Superuser("+ userId +", pk_study) = 1) "
			+" AND f.RECORD_TYPE <> 'D' "
			+" GROUP BY b.entered_on)"
			+" WHERE TRUNC(dt)=TRUNC(SYSDATE) "
			+" UNION"
			+" SELECT -1 AS  DAY, to_char(TRUNC(SYSDATE)-1, 'mm/dd/yyyy') AS DT, SUM(open_count) AS InitialReview, SUM(resolved_count) AS Continuations, SUM(reopen_count) AS Amendments, SUM(closed_count) AS Deviations"
			+" FROM (SELECT "
			+" SUM(DECODE(fk_codelst_querystatus,(SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'query_status' AND codelst_subtyp = 'open'),1,0)) AS open_count, "
			+" SUM(DECODE(fk_codelst_querystatus,(SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'query_status' AND codelst_subtyp = 'resolved'),1,0)) AS resolved_count, "
			+" SUM(DECODE(fk_codelst_querystatus,(SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'query_status' AND codelst_subtyp = 're-opened'),1,0)) AS reopen_count, "
			+" SUM(DECODE(fk_codelst_querystatus,(SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'query_status' AND codelst_subtyp = 'closed'),1,0)) AS closed_count,"
			+" CASE WHEN TRUNC(b.entered_on) = TRUNC(SYSDATE)-1 THEN TRUNC(b.entered_on) END AS dt"
			+" FROM ER_FORMQUERY a, ER_FORMQUERYSTATUS b, ER_FORMFLD c, ER_FORMSEC d, ER_FORMLIB e, ER_PATFORMS f, ER_PATPROT g,er_study z "
			+" WHERE b.fk_formquery = a.pk_formquery "
			+" AND a.fk_field = c.fk_field "
			+" AND d.pk_formsec = c.fk_formsec "
			+" AND e.pk_formlib = d.fk_formlib "
			+" AND pk_formlib = f.fk_formlib "
			+" AND pk_patforms = fk_querymodule "
			+" AND pk_patprot = fk_patprot "
			+" AND pk_study = g.fk_study(+) "
			+" AND b.pk_formquerystatus = (SELECT MAX(x.pk_formquerystatus) FROM ER_FORMQUERYSTATUS x WHERE x.fk_formquery = a.pk_formquery "
			+" AND x.fk_codelst_querystatus = b.fk_codelst_querystatus "
			+" AND trunc(b.entered_on) = (SELECT MAX(trunc(y.entered_on)) FROM ER_FORMQUERYSTATUS y WHERE y.fk_formquery = a.pk_formquery "
			+ "AND y.fk_codelst_querystatus = b.fk_codelst_querystatus)) "
			+" AND (EXISTS (SELECT * FROM er_studyteam h WHERE pk_study = h.fk_study AND h.fk_user = "+userId+" AND NVL(studyteam_status,'Active') = 'Active')"
			+" OR Pkg_Phi.F_Is_Superuser("+ userId +", pk_study) = 1) "
			+" AND f.RECORD_TYPE <> 'D' "
			+" GROUP BY b.entered_on)"
			+" WHERE TRUNC(dt)=TRUNC(SYSDATE) -1"
			+" UNION"
			+" SELECT -2 AS  DAY, to_char(TRUNC(SYSDATE)-2, 'mm/dd/yyyy') , SUM(open_count) AS InitialReview, SUM(resolved_count) AS Continuations, SUM(reopen_count) AS Amendments, SUM(closed_count) AS Deviations"
			+" FROM (SELECT "
			+" SUM(DECODE(fk_codelst_querystatus,(SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'query_status' AND codelst_subtyp = 'open'),1,0)) AS open_count, "
			+" SUM(DECODE(fk_codelst_querystatus,(SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'query_status' AND codelst_subtyp = 'resolved'),1,0)) AS resolved_count, "
			+" SUM(DECODE(fk_codelst_querystatus,(SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'query_status' AND codelst_subtyp = 're-opened'),1,0)) AS reopen_count, "
			+" SUM(DECODE(fk_codelst_querystatus,(SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'query_status' AND codelst_subtyp = 'closed'),1,0)) AS closed_count,"
			+" CASE WHEN TRUNC(b.entered_on) = TRUNC(SYSDATE)-2 THEN TRUNC(b.entered_on) END AS dt"
			+" FROM ER_FORMQUERY a, ER_FORMQUERYSTATUS b, ER_FORMFLD c, ER_FORMSEC d, ER_FORMLIB e, ER_PATFORMS f, ER_PATPROT g,er_study z "
			+" WHERE b.fk_formquery = a.pk_formquery "
			+" AND a.fk_field = c.fk_field "
			+" AND d.pk_formsec = c.fk_formsec "
			+" AND e.pk_formlib = d.fk_formlib "
			+" AND pk_formlib = f.fk_formlib "
			+" AND pk_patforms = fk_querymodule "
			+" AND pk_patprot = fk_patprot "
			+" AND pk_study = g.fk_study(+) "
			+" AND b.pk_formquerystatus = (SELECT MAX(x.pk_formquerystatus) FROM ER_FORMQUERYSTATUS x WHERE x.fk_formquery = a.pk_formquery "
			+" AND x.fk_codelst_querystatus = b.fk_codelst_querystatus "
			+" AND trunc(b.entered_on) = (SELECT MAX(trunc(y.entered_on)) FROM ER_FORMQUERYSTATUS y WHERE y.fk_formquery = a.pk_formquery"
			+" AND y.fk_codelst_querystatus = b.fk_codelst_querystatus)) "
			+" AND (EXISTS (SELECT * FROM er_studyteam h WHERE pk_study = h.fk_study AND h.fk_user = "+userId+" AND NVL(studyteam_status,'Active') = 'Active')"
			+" OR Pkg_Phi.F_Is_Superuser("+ userId +", pk_study) = 1) "
			+" AND f.RECORD_TYPE <> 'D' "
			+" GROUP BY b.entered_on)"
			+" WHERE TRUNC(dt)=TRUNC(SYSDATE)-2 "
			+" UNION"
			+" SELECT -3 AS  DAY, to_char(TRUNC(SYSDATE)-3, 'mm/dd/yyyy') , SUM(open_count) AS InitialReview, SUM(resolved_count) AS Continuations, SUM(reopen_count) AS Amendments, SUM(closed_count) AS Deviations"
			+" FROM (SELECT "
			+" SUM(DECODE(fk_codelst_querystatus,(SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'query_status' AND codelst_subtyp = 'open'),1,0)) AS open_count, "
			+" SUM(DECODE(fk_codelst_querystatus,(SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'query_status' AND codelst_subtyp = 'resolved'),1,0)) AS resolved_count, "
			+" SUM(DECODE(fk_codelst_querystatus,(SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'query_status' AND codelst_subtyp = 're-opened'),1,0)) AS reopen_count, "
			+" SUM(DECODE(fk_codelst_querystatus,(SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'query_status' AND codelst_subtyp = 'closed'),1,0)) AS closed_count,"
			+" CASE WHEN TRUNC(b.entered_on) = TRUNC(SYSDATE)-3 THEN TRUNC(b.entered_on) END AS dt"
			+" FROM ER_FORMQUERY a, ER_FORMQUERYSTATUS b, ER_FORMFLD c, ER_FORMSEC d, ER_FORMLIB e, ER_PATFORMS f, ER_PATPROT g,er_study z "
			+" WHERE b.fk_formquery = a.pk_formquery "
			+" AND a.fk_field = c.fk_field "
			+" AND d.pk_formsec = c.fk_formsec "
			+" AND e.pk_formlib = d.fk_formlib "
			+" AND pk_formlib = f.fk_formlib "
			+" AND pk_patforms = fk_querymodule "
			+" AND pk_patprot = fk_patprot "
			+" AND pk_study = g.fk_study(+) "
			+" AND b.pk_formquerystatus = (SELECT MAX(x.pk_formquerystatus) FROM ER_FORMQUERYSTATUS x WHERE x.fk_formquery = a.pk_formquery "
			+" AND x.fk_codelst_querystatus = b.fk_codelst_querystatus "
			+" AND trunc(b.entered_on) = (SELECT MAX(trunc(y.entered_on)) FROM ER_FORMQUERYSTATUS y WHERE y.fk_formquery = a.pk_formquery "
			+" AND y.fk_codelst_querystatus = b.fk_codelst_querystatus )) "
			+" AND (EXISTS (SELECT * FROM er_studyteam h WHERE pk_study = h.fk_study AND h.fk_user = "+userId+" AND NVL(studyteam_status,'Active') = 'Active')"
			+" OR Pkg_Phi.F_Is_Superuser("+ userId +", pk_study) = 1) "
			+" AND f.RECORD_TYPE <> 'D' "
			+" GROUP BY b.entered_on)"
			+" WHERE TRUNC(dt)=TRUNC(SYSDATE)-3"
			+" UNION"
			+" SELECT -4 AS  DAY, to_char(TRUNC(SYSDATE)-4, 'mm/dd/yyyy') , SUM(open_count) AS InitialReview, SUM(resolved_count) AS Continuations, SUM(reopen_count) AS Amendments, SUM(closed_count) AS Deviations"
			+" FROM (SELECT "
			+" SUM(DECODE(fk_codelst_querystatus,(SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'query_status' AND codelst_subtyp = 'open'),1,0)) AS open_count, "
			+" SUM(DECODE(fk_codelst_querystatus,(SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'query_status' AND codelst_subtyp = 'resolved'),1,0)) AS resolved_count, "
			+" SUM(DECODE(fk_codelst_querystatus,(SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'query_status' AND codelst_subtyp = 're-opened'),1,0)) AS reopen_count, "
			+" SUM(DECODE(fk_codelst_querystatus,(SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'query_status' AND codelst_subtyp = 'closed'),1,0)) AS closed_count,"
			+" CASE WHEN TRUNC(b.entered_on) = TRUNC(SYSDATE)-4 THEN TRUNC(b.entered_on) END AS dt"
			+" FROM ER_FORMQUERY a, ER_FORMQUERYSTATUS b, ER_FORMFLD c, ER_FORMSEC d, ER_FORMLIB e, ER_PATFORMS f, ER_PATPROT g,er_study z "
			+" WHERE b.fk_formquery = a.pk_formquery "
			+" AND a.fk_field = c.fk_field "
			+" AND d.pk_formsec = c.fk_formsec "
			+" AND e.pk_formlib = d.fk_formlib "
			+" AND pk_formlib = f.fk_formlib "
			+" AND pk_patforms = fk_querymodule "
			+" AND pk_patprot = fk_patprot "
			+" AND pk_study = g.fk_study(+) "
			+" AND b.pk_formquerystatus = (SELECT MAX(x.pk_formquerystatus) FROM ER_FORMQUERYSTATUS x WHERE x.fk_formquery = a.pk_formquery "
			+" AND x.fk_codelst_querystatus = b.fk_codelst_querystatus "
			+" AND trunc(b.entered_on) = (SELECT MAX(trunc(y.entered_on)) FROM ER_FORMQUERYSTATUS y WHERE y.fk_formquery = a.pk_formquery"
			+" AND y.fk_codelst_querystatus = b.fk_codelst_querystatus)) "
			+" AND (EXISTS (SELECT * FROM er_studyteam h WHERE pk_study = h.fk_study AND h.fk_user = "+userId+" AND NVL(studyteam_status,'Active') = 'Active')"
			+" OR Pkg_Phi.F_Is_Superuser("+ userId +", pk_study) = 1) "
			+" AND f.RECORD_TYPE <> 'D' "
			+" GROUP BY b.entered_on)"
			+" WHERE TRUNC(dt)=TRUNC(SYSDATE)-4"
			+" UNION"
			+" SELECT -5 AS  DAY, to_char(TRUNC(SYSDATE)-5, 'mm/dd/yyyy'), SUM(open_count) AS InitialReview, SUM(resolved_count) AS Continuations, SUM(reopen_count) AS Amendments, SUM(closed_count) AS Deviations"
			+" FROM (SELECT "
			+" SUM(DECODE(fk_codelst_querystatus,(SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'query_status' AND codelst_subtyp = 'open'),1,0)) AS open_count, "
			+" SUM(DECODE(fk_codelst_querystatus,(SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'query_status' AND codelst_subtyp = 'resolved'),1,0)) AS resolved_count, "
			+" SUM(DECODE(fk_codelst_querystatus,(SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'query_status' AND codelst_subtyp = 're-opened'),1,0)) AS reopen_count, "
			+" SUM(DECODE(fk_codelst_querystatus,(SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'query_status' AND codelst_subtyp = 'closed'),1,0)) AS closed_count,"
			+" CASE WHEN TRUNC(b.entered_on) = TRUNC(SYSDATE)-5 THEN TRUNC(b.entered_on) END AS dt"
			+" FROM ER_FORMQUERY a, ER_FORMQUERYSTATUS b, ER_FORMFLD c, ER_FORMSEC d, ER_FORMLIB e, ER_PATFORMS f, ER_PATPROT g,er_study z "
			+" WHERE b.fk_formquery = a.pk_formquery "
			+" AND a.fk_field = c.fk_field "
			+" AND d.pk_formsec = c.fk_formsec "
			+" AND e.pk_formlib = d.fk_formlib "
			+" AND pk_formlib = f.fk_formlib "
			+" AND pk_patforms = fk_querymodule "
			+" AND pk_patprot = fk_patprot "
			+" AND pk_study = g.fk_study(+) "
			+" AND b.pk_formquerystatus = (SELECT MAX(x.pk_formquerystatus) FROM ER_FORMQUERYSTATUS x WHERE x.fk_formquery = a.pk_formquery "
			+" AND x.fk_codelst_querystatus = b.fk_codelst_querystatus "
			+" AND trunc(b.entered_on) = (SELECT MAX(trunc(y.entered_on)) FROM ER_FORMQUERYSTATUS y WHERE y.fk_formquery = a.pk_formquery"
			+" AND y.fk_codelst_querystatus = b.fk_codelst_querystatus)) "
			+" AND (EXISTS (SELECT * FROM er_studyteam h WHERE pk_study = h.fk_study AND h.fk_user = "+userId+" AND NVL(studyteam_status,'Active') = 'Active')"
			+" OR Pkg_Phi.F_Is_Superuser("+ userId +", pk_study) = 1) "
			+" AND f.RECORD_TYPE <> 'D' "
			+" GROUP BY b.entered_on)"
			+" WHERE TRUNC(dt)=TRUNC(SYSDATE)-5"
			+" UNION"
			+" SELECT -6 AS  DAY, to_char(TRUNC(SYSDATE)-6, 'mm/dd/yyyy') , SUM(open_count) AS InitialReview, SUM(resolved_count) AS Continuations, SUM(reopen_count) AS Amendments, SUM(closed_count) AS Deviations"
			+" FROM (SELECT "
			+" SUM(DECODE(fk_codelst_querystatus,(SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'query_status' AND codelst_subtyp = 'open'),1,0)) AS open_count, "
			+" SUM(DECODE(fk_codelst_querystatus,(SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'query_status' AND codelst_subtyp = 'resolved'),1,0)) AS resolved_count, "
			+" SUM(DECODE(fk_codelst_querystatus,(SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'query_status' AND codelst_subtyp = 're-opened'),1,0)) AS reopen_count, "
			+" SUM(DECODE(fk_codelst_querystatus,(SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'query_status' AND codelst_subtyp = 'closed'),1,0)) AS closed_count,"
			+" CASE WHEN TRUNC(b.entered_on) = TRUNC(SYSDATE)-6 THEN TRUNC(b.entered_on) END AS dt"
			+" FROM ER_FORMQUERY a, ER_FORMQUERYSTATUS b, ER_FORMFLD c, ER_FORMSEC d, ER_FORMLIB e, ER_PATFORMS f, ER_PATPROT g,er_study z "
			+" WHERE b.fk_formquery = a.pk_formquery "
			+" AND a.fk_field = c.fk_field "
			+" AND d.pk_formsec = c.fk_formsec "
			+" AND e.pk_formlib = d.fk_formlib "
			+" AND pk_formlib = f.fk_formlib "
			+" AND pk_patforms = fk_querymodule "
			+" AND pk_patprot = fk_patprot "
			+" AND pk_study = g.fk_study(+) "
			+" AND b.pk_formquerystatus = (SELECT MAX(x.pk_formquerystatus) FROM ER_FORMQUERYSTATUS x WHERE x.fk_formquery = a.pk_formquery "
			+" AND x.fk_codelst_querystatus = b.fk_codelst_querystatus "
			+" AND trunc(b.entered_on) = (SELECT MAX(trunc(y.entered_on)) FROM ER_FORMQUERYSTATUS y WHERE y.fk_formquery = a.pk_formquery"
			+" AND y.fk_codelst_querystatus = b.fk_codelst_querystatus)) "
			+" AND (EXISTS (SELECT * FROM er_studyteam h WHERE pk_study = h.fk_study AND h.fk_user = "+userId+" AND NVL(studyteam_status,'Active') = 'Active')"
			+" OR Pkg_Phi.F_Is_Superuser("+ userId +", pk_study) = 1) "
			+" AND f.RECORD_TYPE <> 'D' "
			+" GROUP BY b.entered_on)"
			+" WHERE TRUNC(dt)=TRUNC(SYSDATE)-6"
			+" UNION"
			+" SELECT -7 AS  DAY, to_char(TRUNC(SYSDATE)-7, 'mm/dd/yyyy') , SUM(open_count) AS InitialReview, SUM(resolved_count) AS Continuations, SUM(reopen_count) AS Amendments, SUM(closed_count) AS Deviations"
			+" FROM (SELECT "
			+" SUM(DECODE(fk_codelst_querystatus,(SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'query_status' AND codelst_subtyp = 'open'),1,0)) AS open_count, "
			+" SUM(DECODE(fk_codelst_querystatus,(SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'query_status' AND codelst_subtyp = 'resolved'),1,0)) AS resolved_count, "
			+" SUM(DECODE(fk_codelst_querystatus,(SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'query_status' AND codelst_subtyp = 're-opened'),1,0)) AS reopen_count, "
			+" SUM(DECODE(fk_codelst_querystatus,(SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'query_status' AND codelst_subtyp = 'closed'),1,0)) AS closed_count,"
			+" CASE WHEN TRUNC(b.entered_on) = TRUNC(SYSDATE)-7 THEN TRUNC(b.entered_on) END AS dt"
			+" FROM ER_FORMQUERY a, ER_FORMQUERYSTATUS b, ER_FORMFLD c, ER_FORMSEC d, ER_FORMLIB e, ER_PATFORMS f, ER_PATPROT g,er_study z "
			+" WHERE b.fk_formquery = a.pk_formquery "
			+" AND a.fk_field = c.fk_field "
			+" AND d.pk_formsec = c.fk_formsec "
			+" AND e.pk_formlib = d.fk_formlib "
			+" AND pk_formlib = f.fk_formlib "
			+" AND pk_patforms = fk_querymodule "
			+" AND pk_patprot = fk_patprot "
			+" AND pk_study = g.fk_study(+) "
			+" AND b.pk_formquerystatus = (SELECT MAX(x.pk_formquerystatus) FROM ER_FORMQUERYSTATUS x WHERE x.fk_formquery = a.pk_formquery "
			+" AND x.fk_codelst_querystatus = b.fk_codelst_querystatus "
			+" AND trunc(b.entered_on) = (SELECT MAX(trunc(y.entered_on)) FROM ER_FORMQUERYSTATUS y WHERE y.fk_formquery = a.pk_formquery"
			+" AND y.fk_codelst_querystatus = b.fk_codelst_querystatus )) "
			+" AND (EXISTS (SELECT * FROM er_studyteam h WHERE pk_study = h.fk_study AND h.fk_user = "+userId+" AND NVL(studyteam_status,'Active') = 'Active')"
			+" OR Pkg_Phi.F_Is_Superuser("+ userId +", pk_study) = 1) "
			+" AND f.RECORD_TYPE <> 'D' "
			+" GROUP BY b.entered_on)"
			+" WHERE TRUNC(dt)=TRUNC(SYSDATE)-7";
			
			//out.println ("queryCount " +queryCount);
			
			query = new BrowserRows();
			query.getPageRows(1,10,queryCount,50,"select 8 from dual","1","asc");
		
			if (query.getRowReturned()!= 0){
				for(int counter=1; counter<=query.getRowReturned();counter++){
					if (EJBUtil.stringToNum(query.getBValues(counter,"Day")) == 0){
						today_openCnt = EJBUtil.stringToNum(query.getBValues(counter,"InitialReview"));
						today_resolvedCnt = EJBUtil.stringToNum(query.getBValues(counter,"Continuations"));
						today_reopenCnt = EJBUtil.stringToNum(query.getBValues(counter,"Amendments"));
						today_closedCnt = EJBUtil.stringToNum(query.getBValues(counter,"Deviations")); 
					} else {
						tempIReview = query.getBValues(counter,"InitialReview");
						tempIReview = (tempIReview==null)?"":tempIReview;
						tempConts = query.getBValues(counter,"Continuations");
						tempConts = (tempConts==null)?"":tempConts;
						tempAmends = query.getBValues(counter,"Amendments");
						tempAmends = (tempAmends==null)?"":tempAmends;
						tempDevs = query.getBValues(counter,"Deviations");
						tempDevs = (tempDevs==null)?"":tempDevs;
						
						if (tempIReview.length() == 0){
							IReview = IReview + ",'" + query.getBValues(counter,"DT").substring(0,5) + "'|0";
						} else {
							IReview = IReview + ",'" + query.getBValues(counter,"DT").substring(0,5) + "'|"+ tempIReview;
							IRCnt = IRCnt  + EJBUtil.stringToNum(tempIReview);
						}
						if (tempConts.length() == 0){
							Conts = Conts + ",'" + query.getBValues(counter,"DT").substring(0,5) + "'|0";
						} else {
							Conts = Conts + ",'" + query.getBValues(counter,"DT").substring(0,5) + "'|"+ tempConts;
							ContCnt = ContCnt + EJBUtil.stringToNum(tempConts);
						}
						if (tempAmends.length() == 0){
							Amends = Amends + ",'" + query.getBValues(counter,"DT").substring(0,5) + "'|0";
						} else {
							Amends = Amends + ",'" + query.getBValues(counter,"DT").substring(0,5) + "'|"+ tempAmends;
							AmendCnt = AmendCnt + EJBUtil.stringToNum(tempAmends);
						}
						if (tempDevs.length() == 0){
							Devs = Devs + ",'" + query.getBValues(counter,"DT").substring(0,5) + "'|0";
						} else {
							Devs = Devs + ",'" + query.getBValues(counter,"DT").substring(0,5) + "'|"+ tempDevs;
							DevCnt = DevCnt + EJBUtil.stringToNum(tempDevs);
						}
					}
				}
			}
		%>
		<Input type="hidden" id="today_openCnt" name="today_openCnt" value="<%=today_openCnt%>"/>
		<Input type="hidden" id="today_resolvedCnt" name="today_resolvedCnt" value="<%=today_resolvedCnt%>"/>
		<Input type="hidden" id="today_reopenCnt" name="today_reopenCnt" value="<%=today_reopenCnt%>"/>
		<Input type="hidden" id="today_closedCnt" name="today_closedCnt" value="<%=today_closedCnt%>"/>
		<Input type="hidden" id="InitialReview" name="InitialReview" value="<%=IReview%>"/>
		<Input type="hidden" id="Continuations" name="Continuations" value="<%=Conts%>"/>
		<Input type="hidden" id="Amendments" name="Amendments" value="<%=Amends%>"/>
		<Input type="hidden" id="Deviations" name="Deviations" value="<%=Devs%>"/>
		<Input type="hidden" id="IRCnt" name="IRCnt" value="<%=IRCnt%>"/>
		<Input type="hidden" id="ContCnt" name="ContCnt" value="<%=ContCnt%>"/>
		<Input type="hidden" id="AmendCnt" name="AmendCnt" value="<%=AmendCnt%>"/>
		<Input type="hidden" id="DevCnt" name="DevCnt" value="<%=DevCnt%>"/>
		
		<%
			int open_today = 0;
			int open_last_7d = 0;
			int open_last_15d = 0;
			int open_last_30d = 0;
			int open_gt30d = 0;
		
			queryCount = "SELECT COUNT(CASE WHEN entered_on = TRUNC(SYSDATE) THEN 1 END) AS open_today,"
			+ " COUNT(CASE WHEN (entered_on < TRUNC(SYSDATE) AND entered_on >= TRUNC(SYSDATE) -7) THEN 1 END) AS open_last_7d,"
			+ " COUNT(CASE WHEN (entered_on < TRUNC(SYSDATE) -7 AND entered_on >= TRUNC(SYSDATE) -15) THEN 1 END) AS open_last_15d,"
			+ " COUNT(CASE WHEN (entered_on < TRUNC(SYSDATE) -15 AND entered_on >= TRUNC(SYSDATE) -30) THEN 1 END) AS open_last_30d,"
			+ " COUNT(CASE WHEN (entered_on < TRUNC(SYSDATE) -30) THEN 1 END) AS open_gt30d"
			+ " FROM ("
			+ " SELECT TRUNC(b.entered_on) AS entered_on "
			+ " FROM ER_FORMQUERY a, ER_FORMQUERYSTATUS b, ER_FORMFLD c, ER_FORMSEC d, ER_FORMLIB e, ER_PATFORMS f, ER_PATPROT g,er_study z "
			+ " WHERE b.fk_formquery = a.pk_formquery "
			+ " AND a.fk_field = c.fk_field "
			+ " AND d.pk_formsec = c.fk_formsec "
			+ " AND e.pk_formlib = d.fk_formlib "
			+ " AND pk_formlib = f.fk_formlib "
			+ " AND pk_patforms = fk_querymodule "
			+ " AND pk_patprot = fk_patprot "
			+ " AND pk_study = g.fk_study(+) "
			+ " AND b.pk_formquerystatus = (SELECT MAX(x.pk_formquerystatus) FROM ER_FORMQUERYSTATUS x WHERE x.fk_formquery = a.pk_formquery "
			+ " AND x.fk_codelst_querystatus = b.fk_codelst_querystatus "
			+ " AND trunc(b.entered_on) = (SELECT MAX(trunc(y.entered_on)) FROM ER_FORMQUERYSTATUS y WHERE y.fk_formquery = a.pk_formquery"
			+ " AND y.fk_codelst_querystatus = b.fk_codelst_querystatus)) "
			+ " AND (EXISTS (SELECT * FROM er_studyteam h WHERE pk_study = h.fk_study AND h.fk_user = "+ userId +" AND NVL(studyteam_status,'Active') = 'Active')"
			+ " OR Pkg_Phi.F_Is_Superuser("+ userId +", pk_study) = 1) "
			+ " AND f.RECORD_TYPE <> 'D' "
			+ " AND fk_codelst_querystatus = (SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'query_status' AND codelst_subtyp = 'open')"
			+ " AND trunc(b.entered_on) <= TRUNC(SYSDATE) "
			+ " )";
			
			query = new BrowserRows();
			query.getPageRows(1,10,queryCount,50,"select 1 from dual","1","asc");
		
			open_today = EJBUtil.stringToNum(query.getBValues(1,"open_today"));
			open_last_7d = EJBUtil.stringToNum(query.getBValues(1,"open_last_7d"));
			open_last_15d = EJBUtil.stringToNum(query.getBValues(1,"open_last_15d"));
			open_last_30d = EJBUtil.stringToNum(query.getBValues(1,"open_last_30d"));
			open_gt30d = EJBUtil.stringToNum(query.getBValues(1,"open_gt30d"));
		%>
		<Input type="hidden" id="open_today" name="open_today" value="<%=open_today%>">
		<Input type="hidden" id="open_last_7d" name="open_last_7d" value="<%=open_last_7d%>">
		<Input type="hidden" id="open_last_15d" name="open_last_15d" value="<%=open_last_15d%>">
		<Input type="hidden" id="open_last_30d" name="open_last_30d" value="<%=open_last_30d%>">
		<Input type="hidden" id="open_gt30d" name="open_gt30d" value="<%=open_gt30d%>">
		<%
			int resolved_today = 0;
			int resolved_last_7d = 0;
			int resolved_last_15d = 0;
			int resolved_last_30d = 0;
			int resolved_gt30d = 0;
		
			queryCount = "SELECT COUNT(CASE WHEN entered_on = TRUNC(SYSDATE) THEN 1 END) AS resolved_today,"
			+ " COUNT(CASE WHEN (entered_on < TRUNC(SYSDATE) AND entered_on >= TRUNC(SYSDATE) -7) THEN 1 END) AS resolved_last_7d,"
			+ " COUNT(CASE WHEN (entered_on < TRUNC(SYSDATE) -7 AND entered_on >= TRUNC(SYSDATE) -15) THEN 1 END) AS resolved_last_15d,"
			+ " COUNT(CASE WHEN (entered_on < TRUNC(SYSDATE) -15 AND entered_on >= TRUNC(SYSDATE) -30) THEN 1 END) AS resolved_last_30d,"
			+ " COUNT(CASE WHEN (entered_on < TRUNC(SYSDATE) -30) THEN 1 END) AS resolved_gt30d"
			+ " FROM ("
			+ " SELECT TRUNC(b.entered_on) AS entered_on"
			+ " FROM ER_FORMQUERY a, ER_FORMQUERYSTATUS b, ER_FORMFLD c, ER_FORMSEC d, ER_FORMLIB e, ER_PATFORMS f, ER_PATPROT g,er_study z "
			+ " WHERE b.fk_formquery = a.pk_formquery "
			+ " AND a.fk_field = c.fk_field "
			+ " AND d.pk_formsec = c.fk_formsec "
			+ " AND e.pk_formlib = d.fk_formlib "
			+ " AND pk_formlib = f.fk_formlib "
			+ " AND pk_patforms = fk_querymodule "
			+ " AND pk_patprot = fk_patprot "
			+ " AND pk_study = g.fk_study(+) "
			+ " AND b.pk_formquerystatus = (SELECT MAX(x.pk_formquerystatus) FROM ER_FORMQUERYSTATUS x WHERE x.fk_formquery = a.pk_formquery "
			+ " AND x.fk_codelst_querystatus = b.fk_codelst_querystatus "
			+ " AND trunc(b.entered_on) = (SELECT MAX(trunc(y.entered_on)) FROM ER_FORMQUERYSTATUS y WHERE y.fk_formquery = a.pk_formquery "
			+ " AND y.fk_codelst_querystatus = b.fk_codelst_querystatus )) "
			+ " AND (EXISTS (SELECT * FROM er_studyteam h WHERE pk_study = h.fk_study AND h.fk_user = "+ userId +" AND NVL(studyteam_status,'Active') = 'Active')"
			+ " OR Pkg_Phi.F_Is_Superuser("+ userId +", pk_study) = 1) "
			+ " AND f.RECORD_TYPE <> 'D' "
			+ " AND fk_codelst_querystatus = (SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'query_status' AND codelst_subtyp = 'resolved')"
			+ " AND trunc(b.entered_on) <= TRUNC(SYSDATE) "
			+ " )";
		
			query = new BrowserRows();
			query.getPageRows(1,10,queryCount,50,"select 1 from dual","1","asc");
		
			resolved_today = EJBUtil.stringToNum(query.getBValues(1,"resolved_today"));
			resolved_last_7d = EJBUtil.stringToNum(query.getBValues(1,"resolved_last_7d"));
			resolved_last_15d = EJBUtil.stringToNum(query.getBValues(1,"resolved_last_15d"));
			resolved_last_30d = EJBUtil.stringToNum(query.getBValues(1,"resolved_last_30d"));
			resolved_gt30d = EJBUtil.stringToNum(query.getBValues(1,"resolved_gt30d"));
		%>
		<Input type="hidden" id="resolved_today" name="resolved_today" value="<%=resolved_today%>">
		<Input type="hidden" id="resolved_last_7d" name="resolved_last_7d" value="<%=resolved_last_7d%>">
		<Input type="hidden" id="resolved_last_15d" name="resolved_last_15d" value="<%=resolved_last_15d%>">
		<Input type="hidden" id="resolved_last_30d" name="resolved_last_30d" value="<%=resolved_last_30d%>">
		<Input type="hidden" id="resolved_gt30d" name="resolved_gt30d" value="<%=resolved_gt30d%>">
		<%
			String accountId= (String) tSession.getValue("accountId");
			int due_today = 0;
			int due_next_7d = 0;
			int due_next_15d = 0;
			int due_next_30d = 0;
			int due_30gt = 0;
			int overdue_7d = 0;
			int overdue_15d = 0;
			int overdue_30d = 0;
			int overdue_30gt = 0;
		
			queryCount = "SELECT "
			+ " COUNT(CASE WHEN period = 0 THEN 1 END) AS due_today,"
			+ " COUNT(CASE WHEN period > 0 AND period <= 7 THEN 1 END) AS due_next_7d,"
			+ " COUNT(CASE WHEN period > 7 AND period <= 15 THEN 1 END) AS due_next_15d,"
		 	+ " COUNT(CASE WHEN period > 15 AND period <= 30 THEN 1 END) AS due_next_30d,"
		 	+ " COUNT(CASE WHEN period > 30 THEN 1 END) AS due_30gt"
		 	+ " FROM ("
		 	+ " SELECT ROUND(fuzzy_duration_aft - TRUNC(SYSDATE)) AS period, flag, fuzzy_duration_aft"
		 	+ " FROM ("
		 	+ " SELECT Start_Date_Time,"
		  	+ " CASE "
			+ "  	  WHEN event_durationafter ='D' THEN (Start_Date_Time) + (TO_NUMBER(event_fuzzyafter) * 1)"
			+ " 	  WHEN event_durationafter ='H' THEN (Start_Date_Time) + 1"
			+ " 	  WHEN event_durationafter ='W' THEN (Start_Date_Time) + (TO_NUMBER(event_fuzzyafter) * 7)"
			+ " 	  WHEN event_durationafter ='M' THEN (Start_Date_Time) + (TO_NUMBER(event_fuzzyafter) *30)"
			+ " 	  WHEN event_durationafter IS NULL THEN (Start_Date_Time)"
			+ " END AS fuzzy_duration_aft,"
			+ " (SELECT 'X' FROM dual WHERE TRUNC(SYSDATE) > "
			+ "	  (CASE "
			+ "  	  WHEN event_durationafter ='D' THEN (Start_Date_Time) + (TO_NUMBER(event_fuzzyafter) * 1)"
			+ " 	  WHEN event_durationafter ='H' THEN (Start_Date_Time) + 1"
			+ " 	  WHEN event_durationafter ='W' THEN (Start_Date_Time) + (TO_NUMBER(event_fuzzyafter) * 7)"
			+ " 	  WHEN event_durationafter ='M' THEN (Start_Date_Time) + (TO_NUMBER(event_fuzzyafter) *30)"
			+ " 	  WHEN event_durationafter IS NULL THEN (Start_Date_Time)"
			+ " END)) AS flag"
		 	+ " FROM esch.sch_events1 a, ER_PATPROT b, esch.sch_event_crf c, event_assoc e, ER_STUDY f"
		 	+ " WHERE a.fk_assoc = c.fk_event "
		 	+ " AND e.event_id = a.fk_assoc "
		 	+ " AND pk_patprot = a.fk_patprot "
		 	+ " AND a.status = 0 AND b.patprot_stat = 1 "
		 	+ " AND f.pk_study = b.fk_study AND fk_account = "+ accountId
		 	+ " AND (EXISTS (SELECT * FROM er_studyteam h WHERE pk_study = h.fk_study AND h.fk_user = "+ userId +" AND NVL(studyteam_status,'Active') = 'Active')"
			+ " OR Pkg_Phi.F_Is_Superuser("+ userId +", pk_study) = 1) "
			+ " AND c.FK_FORM > 0"
		 	+ " )) WHERE period >= 0 ";
		
			query = new BrowserRows();
			query.getPageRows(1,10,queryCount,50,"select 1 from dual","1","asc");
		
			due_today = EJBUtil.stringToNum(query.getBValues(1,"due_today"));
			due_next_7d = EJBUtil.stringToNum(query.getBValues(1,"due_next_7d"));
			due_next_15d = EJBUtil.stringToNum(query.getBValues(1,"due_next_15d"));
			due_next_30d = EJBUtil.stringToNum(query.getBValues(1,"due_next_30d"));
			due_30gt = EJBUtil.stringToNum(query.getBValues(1,"due_30gt"));
			
			queryCount = "SELECT "
		 	+ " COUNT(CASE WHEN period < 0 AND period >= -7 THEN 1 END) AS overdue_7d,"
		 	+ " COUNT(CASE WHEN period < -7 AND period >= -15 THEN 1 END) AS overdue_15d,"
		 	+ " COUNT(CASE WHEN period < -15 AND period >= -30 THEN 1 END) AS overdue_30d,"
		 	+ " COUNT(CASE WHEN period < -30 THEN 1 END) AS overdue_30gt"
		 	+ " FROM ("
		 	+ " SELECT ROUND(fuzzy_duration_aft - TRUNC(SYSDATE)) AS period, flag, fuzzy_duration_aft"
		 	+ " FROM ("
		 	+ " SELECT Start_Date_Time,"
		  	+ " CASE "
			+ "  	  WHEN event_durationafter ='D' THEN (Start_Date_Time) + (TO_NUMBER(event_fuzzyafter) * 1)"
			+ " 	  WHEN event_durationafter ='H' THEN (Start_Date_Time) + 1"
			+ " 	  WHEN event_durationafter ='W' THEN (Start_Date_Time) + (TO_NUMBER(event_fuzzyafter) * 7)"
			+ " 	  WHEN event_durationafter ='M' THEN (Start_Date_Time) + (TO_NUMBER(event_fuzzyafter) *30)"
			+ " 	  WHEN event_durationafter IS NULL THEN (Start_Date_Time)"
			+ " END AS fuzzy_duration_aft,"
			+ " (SELECT 'X' FROM dual WHERE TRUNC(SYSDATE) > "
			+ "	  (CASE "
			+ "  	  WHEN event_durationafter ='D' THEN (Start_Date_Time) + (TO_NUMBER(event_fuzzyafter) * 1)"
			+ " 	  WHEN event_durationafter ='H' THEN (Start_Date_Time) + 1"
			+ " 	  WHEN event_durationafter ='W' THEN (Start_Date_Time) + (TO_NUMBER(event_fuzzyafter) * 7)"
			+ " 	  WHEN event_durationafter ='M' THEN (Start_Date_Time) + (TO_NUMBER(event_fuzzyafter) *30)"
			+ " 	  WHEN event_durationafter IS NULL THEN (Start_Date_Time)"
			+ " END)) AS flag"
		 	+ " FROM esch.sch_events1 a, ER_PATPROT b, esch.sch_event_crf c, event_assoc e, ER_STUDY f"
		 	+ " WHERE a.fk_assoc = c.fk_event "
		 	+ " AND e.event_id = a.fk_assoc "
		 	+ " AND pk_patprot = a.fk_patprot "
		 	+ " AND f.pk_study = b.fk_study AND fk_account = "+ accountId
		 	+ " AND (EXISTS (SELECT * FROM er_studyteam h WHERE pk_study = h.fk_study AND h.fk_user = "+ userId +" AND NVL(studyteam_status,'Active') = 'Active')"
			+ " OR Pkg_Phi.F_Is_Superuser("+ userId +", pk_study) = 1) "
			+ " AND NOT EXISTS (SELECT * FROM er_patforms d WHERE fk_patprot = pk_patprot AND d.fk_per = b.fk_per AND d.fk_formlib = c.fk_form AND d.fk_sch_events1(+) = a.event_id AND d.RECORD_TYPE <> 'D') "
		 	+ " AND FK_FORM > 0"
		 	+ " )) WHERE period < 0 ";
		 	
			query = new BrowserRows();
			query.getPageRows(1,10,queryCount,50,"select 1 from dual","1","asc");
		
			overdue_7d = EJBUtil.stringToNum(query.getBValues(1,"overdue_7d"));
			overdue_15d = EJBUtil.stringToNum(query.getBValues(1,"overdue_15d"));
			overdue_30d = EJBUtil.stringToNum(query.getBValues(1,"overdue_30d"));
			overdue_30gt = EJBUtil.stringToNum(query.getBValues(1,"overdue_30gt"));
		%>
		<Input type="hidden" id="due_today" name="due_today" value="<%=due_today%>">
		<Input type="hidden" id="due_next_7d" name="due_next_7d" value="<%=due_next_7d%>">
		<Input type="hidden" id="due_next_15d" name="due_next_15d" value="<%=due_next_15d%>">
		<Input type="hidden" id="due_next_30d" name="due_next_30d" value="<%=due_next_30d%>">
		<Input type="hidden" id="due_30gt" name="due_30gt" value="<%=due_30gt%>">
		<Input type="hidden" id="overdue_7d" name="overdue_7d" value="<%=overdue_7d%>">
		<Input type="hidden" id="overdue_15d" name="overdue_15d" value="<%=overdue_15d%>">
		<Input type="hidden" id="overdue_30d" name="overdue_30d" value="<%=overdue_30d%>">
		<Input type="hidden" id="overdue_30gt" name="overdue_30gt" value="<%=overdue_30gt%>">
		<%
			String aeSeries = "'00/00'|0";
			String saeSeries = "'00/00'|0";
			String aestdate = "";
			String AdvTYP = "";
			String Prevaestdate = "";
			String PrevAdvTYP = "";
			String aeNum = "0";
			int aeCnt =0;
			int saeCnt =0;
			
			queryCount = "SELECT TO_CHAR(ae_stdate, 'mm/dd/yyyy') as ae_stdate, CASE WHEN Cnt IS NULL THEN 0 ELSE Cnt END AS Cnt FROM "
			+ "(SELECT TRUNC(SYSDATE-7) as ae_stdate, (SELECT COUNT(*) FROM ESCH.sch_adverseve, ER_STUDY, ER_PER "
		 	+ " WHERE pk_study = fk_study "
		 	+ " AND pk_per = fk_per "
		 	+ " AND ER_PER.fk_account = "+ accountId +" "
		 	+ " AND TO_DATE(AE_STDATE) = TRUNC(SYSDATE-7) "
		 	+ " AND (SELECT codelst_desc FROM ESCH.sch_codelst WHERE pk_codelst = fk_codlst_aetype) = 'Serious Adverse Event'"
		 	+ " AND (EXISTS (SELECT * FROM er_studyteam h WHERE pk_study = h.fk_study AND h.fk_user = "+ userId +" AND NVL(studyteam_status,'Active') = 'Active')"
			+ " OR Pkg_Phi.F_Is_Superuser("+ userId +", pk_study) = 1) "
		 	+ " GROUP BY  ae_stdate) AS Cnt"
		 	+ " FROM dual"
		 	+ " UNION"
		 	+ " SELECT TRUNC(SYSDATE-6) as ae_stdate, (SELECT COUNT(*) FROM ESCH.sch_adverseve, ER_STUDY, ER_PER "
		 	+ " WHERE pk_study = fk_study "
		 	+ " AND pk_per = fk_per "
		 	+ " AND ER_PER.fk_account = "+ accountId +" "
		 	+ " AND TO_DATE(AE_STDATE) = TRUNC(SYSDATE-6) "
		 	+ " AND (SELECT codelst_desc FROM ESCH.sch_codelst WHERE pk_codelst = fk_codlst_aetype) = 'Serious Adverse Event'"
		 	+ " AND (EXISTS (SELECT * FROM er_studyteam h WHERE pk_study = h.fk_study AND h.fk_user = "+ userId +" AND NVL(studyteam_status,'Active') = 'Active')"
			+ " OR Pkg_Phi.F_Is_Superuser("+ userId +", pk_study) = 1) "
			+ " GROUP BY  ae_stdate) AS Cnt"
		 	+ " FROM dual"
		 	+ " UNION"
		 	+ " SELECT TRUNC(SYSDATE-5) as ae_stdate, (SELECT COUNT(*) FROM ESCH.sch_adverseve, ER_STUDY, ER_PER "
		 	+ " WHERE pk_study = fk_study "
		 	+ " AND pk_per = fk_per "
		 	+ " AND ER_PER.fk_account = "+ accountId +" "
		 	+ " AND TO_DATE(AE_STDATE) = TRUNC(SYSDATE-5) "
		 	+ " AND (SELECT codelst_desc FROM ESCH.sch_codelst WHERE pk_codelst = fk_codlst_aetype) = 'Serious Adverse Event'"
		 	+ " AND (EXISTS (SELECT * FROM er_studyteam h WHERE pk_study = h.fk_study AND h.fk_user = "+ userId +" AND NVL(studyteam_status,'Active') = 'Active')"
			+ " OR Pkg_Phi.F_Is_Superuser("+ userId +", pk_study) = 1) "
			+ " GROUP BY  ae_stdate) AS Cnt"
		 	+ " FROM dual"
		 	+ " UNION"
		 	+ " SELECT TRUNC(SYSDATE-4) as ae_stdate, (SELECT COUNT(*) FROM ESCH.sch_adverseve, ER_STUDY, ER_PER "
		 	+ " WHERE pk_study = fk_study "
		 	+ " AND pk_per = fk_per "
		 	+ " AND ER_PER.fk_account = "+ accountId +" "
		 	+ " AND TO_DATE(AE_STDATE) = TRUNC(SYSDATE-4) "
		 	+ " AND (SELECT codelst_desc FROM ESCH.sch_codelst WHERE pk_codelst = fk_codlst_aetype) = 'Serious Adverse Event'"
		 	+ " AND (EXISTS (SELECT * FROM er_studyteam h WHERE pk_study = h.fk_study AND h.fk_user = "+ userId +" AND NVL(studyteam_status,'Active') = 'Active')"
			+ " OR Pkg_Phi.F_Is_Superuser("+ userId +", pk_study) = 1) "
		 	+ " GROUP BY  ae_stdate) AS Cnt"
		 	+ " FROM dual"
		 	+ " UNION"
		 	+ " SELECT TRUNC(SYSDATE-3) as ae_stdate, (SELECT COUNT(*) FROM ESCH.sch_adverseve, ER_STUDY, ER_PER "
		 	+ " WHERE pk_study = fk_study "
		 	+ " AND pk_per = fk_per "
		 	+ " AND ER_PER.fk_account = "+ accountId +" "
		 	+ " AND TO_DATE(AE_STDATE) = TRUNC(SYSDATE-3) "
		 	+ " AND (SELECT codelst_desc FROM ESCH.sch_codelst WHERE pk_codelst = fk_codlst_aetype) = 'Serious Adverse Event'"
		 	+ " AND (EXISTS (SELECT * FROM er_studyteam h WHERE pk_study = h.fk_study AND h.fk_user = "+ userId +" AND NVL(studyteam_status,'Active') = 'Active')"
			+ " OR Pkg_Phi.F_Is_Superuser("+ userId +", pk_study) = 1) "
		 	+ " GROUP BY  ae_stdate) AS Cnt"
		 	+ " FROM dual"
		 	+ " UNION"
		 	+ " SELECT TRUNC(SYSDATE-2) as ae_stdate, (SELECT COUNT(*) FROM ESCH.sch_adverseve, ER_STUDY, ER_PER "
		 	+ " WHERE pk_study = fk_study "
		 	+ " AND pk_per = fk_per "
		 	+ " AND ER_PER.fk_account = "+ accountId +" "
		 	+ " AND TO_DATE(AE_STDATE) = TRUNC(SYSDATE-2) "
		 	+ " AND (SELECT codelst_desc FROM ESCH.sch_codelst WHERE pk_codelst = fk_codlst_aetype) = 'Serious Adverse Event'"
		 	+ " AND (EXISTS (SELECT * FROM er_studyteam h WHERE pk_study = h.fk_study AND h.fk_user = "+ userId +" AND NVL(studyteam_status,'Active') = 'Active')"
			+ " OR Pkg_Phi.F_Is_Superuser("+ userId +", pk_study) = 1) "
			+ " GROUP BY  ae_stdate) AS Cnt"
		 	+ " FROM dual"
		 	+ " UNION"
		 	+ " SELECT TRUNC(SYSDATE-1) as ae_stdate, (SELECT COUNT(*) FROM ESCH.sch_adverseve, ER_STUDY, ER_PER "
		 	+ " WHERE pk_study = fk_study "
		 	+ " AND pk_per = fk_per "
		 	+ " AND ER_PER.fk_account = "+ accountId +" "
		 	+ " AND TO_DATE(AE_STDATE) = TRUNC(SYSDATE-1) "
		 	+ " AND (SELECT codelst_desc FROM ESCH.sch_codelst WHERE pk_codelst = fk_codlst_aetype) = 'Serious Adverse Event'"
		 	+ " AND (EXISTS (SELECT * FROM er_studyteam h WHERE pk_study = h.fk_study AND h.fk_user = "+ userId +" AND NVL(studyteam_status,'Active') = 'Active')"
			+ " OR Pkg_Phi.F_Is_Superuser("+ userId +", pk_study) = 1) "
		 	+ " GROUP BY  ae_stdate) AS Cnt"
		 	+ " FROM dual)"
		 	+ " ORDER BY ae_stdate asc";
			
			query = new BrowserRows();
			countSql = "select count(*) as cnt from (" +queryCount+ ")";
			query.getPageRows(1,10,queryCount,50,countSql,"2","asc");
			
			if (query.getRowReturned()!= 0){
				for(int counter=1; counter<=query.getRowReturned();counter++){
					aestdate = query.getBValues(counter,"ae_stdate").substring(0,5);
		
					if (query.getBValues(counter,"Cnt") == null) {
						aeNum = "0";
					}else{
						aeNum = query.getBValues(counter,"Cnt");
					}
					saeSeries = saeSeries + ",'" + aestdate + "'|" + aeNum + "";
					saeCnt = saeCnt + EJBUtil.stringToNum(query.getBValues(counter,"Cnt"));
				}
			}
			
			queryCount = "SELECT TO_CHAR(ae_stdate, 'mm/dd/yyyy') as ae_stdate, CASE WHEN Cnt IS NULL THEN 0 ELSE Cnt END AS Cnt FROM "
			+ " (SELECT TRUNC(SYSDATE-7) as ae_stdate, (SELECT COUNT(*) FROM ESCH.sch_adverseve, ER_STUDY, ER_PER "
		 	+ " WHERE pk_study = fk_study "
		 	+ " AND pk_per = fk_per "
		 	+ " AND ER_PER.fk_account = "+ accountId +" "
		 	+ " AND TO_DATE(AE_STDATE) = TRUNC(SYSDATE-7) "
		 	+ " AND (SELECT codelst_desc FROM ESCH.sch_codelst WHERE pk_codelst = fk_codlst_aetype) = 'Adverse Event'"
			+ " AND (EXISTS (SELECT * FROM er_studyteam h WHERE pk_study = h.fk_study AND h.fk_user = "+ userId +" AND NVL(studyteam_status,'Active') = 'Active')"
			+ " OR Pkg_Phi.F_Is_Superuser("+ userId +", pk_study) = 1) "
		 	+ " GROUP BY  ae_stdate) AS Cnt"
		 	+ " FROM dual"
		 	+ " UNION"
		 	+ " SELECT TRUNC(SYSDATE-6) as ae_stdate, (SELECT COUNT(*) FROM ESCH.sch_adverseve, ER_STUDY, ER_PER "
		 	+ " WHERE pk_study = fk_study "
		 	+ " AND pk_per = fk_per "
		 	+ " AND ER_PER.fk_account = "+ accountId +" "
		 	+ " AND TO_DATE(AE_STDATE) = TRUNC(SYSDATE-6) "
		 	+ " AND (SELECT codelst_desc FROM ESCH.sch_codelst WHERE pk_codelst = fk_codlst_aetype) = 'Adverse Event'"
		 	+ " AND (EXISTS (SELECT * FROM er_studyteam h WHERE pk_study = h.fk_study AND h.fk_user = "+ userId +" AND NVL(studyteam_status,'Active') = 'Active')"
			+ " OR Pkg_Phi.F_Is_Superuser("+ userId +", pk_study) = 1) "
			+ " GROUP BY  ae_stdate) AS Cnt"
		 	+ " FROM dual"
		 	+ " UNION"
		 	+ " SELECT TRUNC(SYSDATE-5) as ae_stdate, (SELECT COUNT(*) FROM ESCH.sch_adverseve, ER_STUDY, ER_PER "
		 	+ " WHERE pk_study = fk_study "
		 	+ " AND pk_per = fk_per "
		 	+ " AND ER_PER.fk_account = "+ accountId +" "
		 	+ " AND TO_DATE(AE_STDATE) = TRUNC(SYSDATE-5) "
		 	+ " AND (SELECT codelst_desc FROM ESCH.sch_codelst WHERE pk_codelst = fk_codlst_aetype) = 'Adverse Event'"
		 	+ " AND (EXISTS (SELECT * FROM er_studyteam h WHERE pk_study = h.fk_study AND h.fk_user = "+ userId +" AND NVL(studyteam_status,'Active') = 'Active')"
			+ " OR Pkg_Phi.F_Is_Superuser("+ userId +", pk_study) = 1) "
		 	+ " GROUP BY  ae_stdate) AS Cnt"
		 	+ " FROM dual"
		 	+ " UNION"
		 	+ " SELECT TRUNC(SYSDATE-4) as ae_stdate, (SELECT COUNT(*) FROM ESCH.sch_adverseve, ER_STUDY, ER_PER "
		 	+ " WHERE pk_study = fk_study "
		 	+ " AND pk_per = fk_per "
		 	+ " AND ER_PER.fk_account = "+ accountId +" "
		 	+ " AND TO_DATE(AE_STDATE) = TRUNC(SYSDATE-4) "
		 	+ " AND (SELECT codelst_desc FROM ESCH.sch_codelst WHERE pk_codelst = fk_codlst_aetype) = 'Adverse Event'"
		 	+ " AND (EXISTS (SELECT * FROM er_studyteam h WHERE pk_study = h.fk_study AND h.fk_user = "+ userId +" AND NVL(studyteam_status,'Active') = 'Active')"
			+ " OR Pkg_Phi.F_Is_Superuser("+ userId +", pk_study) = 1) "
		 	+ " GROUP BY  ae_stdate) AS Cnt"
		 	+ " FROM dual"
		 	+ " UNION"
		 	+ " SELECT TRUNC(SYSDATE-3) as ae_stdate, (SELECT COUNT(*) FROM ESCH.sch_adverseve, ER_STUDY, ER_PER "
		 	+ " WHERE pk_study = fk_study "
		 	+ " AND pk_per = fk_per "
		 	+ " AND ER_PER.fk_account = "+ accountId +" "
		 	+ " AND TO_DATE(AE_STDATE) = TRUNC(SYSDATE-3) "
		 	+ " AND (SELECT codelst_desc FROM ESCH.sch_codelst WHERE pk_codelst = fk_codlst_aetype) = 'Adverse Event'"
		 	+ " AND (EXISTS (SELECT * FROM er_studyteam h WHERE pk_study = h.fk_study AND h.fk_user = "+ userId +" AND NVL(studyteam_status,'Active') = 'Active')"
			+ " OR Pkg_Phi.F_Is_Superuser("+ userId +", pk_study) = 1) "
		 	+ " GROUP BY  ae_stdate) AS Cnt"
		 	+ " FROM dual"
		 	+ " UNION"
		 	+ " SELECT TRUNC(SYSDATE-2) as ae_stdate, (SELECT COUNT(*) FROM ESCH.sch_adverseve, ER_STUDY, ER_PER "
		 	+ " WHERE pk_study = fk_study "
		 	+ " AND pk_per = fk_per "
		 	+ " AND ER_PER.fk_account = "+ accountId +" "
		 	+ " AND TO_DATE(AE_STDATE) = TRUNC(SYSDATE-2) "
		 	+ " AND (SELECT codelst_desc FROM ESCH.sch_codelst WHERE pk_codelst = fk_codlst_aetype) = 'Adverse Event'"
		 	+ " AND (EXISTS (SELECT * FROM er_studyteam h WHERE pk_study = h.fk_study AND h.fk_user = "+ userId +" AND NVL(studyteam_status,'Active') = 'Active')"
			+ " OR Pkg_Phi.F_Is_Superuser("+ userId +", pk_study) = 1) "
		 	+ " GROUP BY  ae_stdate) AS Cnt"
		 	+ " FROM dual"
		 	+ " UNION"
		 	+ " SELECT TRUNC(SYSDATE-1) as ae_stdate, (SELECT COUNT(*)  FROM ESCH.sch_adverseve, ER_STUDY, ER_PER "
		 	+ " WHERE pk_study = fk_study "
		 	+ " AND pk_per = fk_per "
		 	+ " AND ER_PER.fk_account = "+ accountId +" "
		 	+ " AND TO_DATE(AE_STDATE) = TRUNC(SYSDATE-1) "
		 	+ " AND (SELECT codelst_desc FROM ESCH.sch_codelst WHERE pk_codelst = fk_codlst_aetype) = 'Adverse Event'"
		 	+ " AND (EXISTS (SELECT * FROM er_studyteam h WHERE pk_study = h.fk_study AND h.fk_user = "+ userId +" AND NVL(studyteam_status,'Active') = 'Active')"
			+ " OR Pkg_Phi.F_Is_Superuser("+ userId +", pk_study) = 1) "
		 	+ " GROUP BY  ae_stdate) AS Cnt"
		 	+ " FROM dual)"
		 	+ " ORDER BY ae_stdate asc";
			
			query = new BrowserRows();
			countSql = "select count(*) as cnt from (" +queryCount+ ")";
			query.getPageRows(1,10,queryCount,50,countSql,"2","asc");
			
			if (query.getRowReturned()!= 0){
				for(int counter=1; counter<=query.getRowReturned();counter++){
					aestdate = query.getBValues(counter,"ae_stdate").substring(0,5);
		
					if (query.getBValues(counter,"Cnt") == null) {
						aeNum = "0";
					}else{
						aeNum = query.getBValues(counter,"Cnt");
					}
					
					aeSeries = aeSeries + ",'" + aestdate + "'|" + aeNum + "";
					aeCnt = aeCnt + EJBUtil.stringToNum(query.getBValues(counter,"Cnt"));
				}
			}	
			
			/*queryCount = "SELECT Cnt, ae_stdate,  AdvTYPE " 
			+ "FROM (SELECT COUNT(*) AS Cnt, TO_CHAR(ae_stdate,'mm/dd/yyyy') AS ae_stdate,  'Adverse Event'  AS AdvType "
			+ "FROM ESCH.sch_adverseve, ER_STUDY, ER_PER "
			+ "WHERE pk_study = fk_study AND "
			+ "pk_per = fk_per AND "
			+ "ER_PER.fk_account =  "+accountId+" AND "
			+ "to_date(AE_STDATE) BETWEEN trunc(SYSDATE-7) AND trunc(SYSDATE-1) "
			+ "AND (SELECT codelst_desc FROM ESCH.sch_codelst WHERE pk_codelst = fk_codlst_aetype) = 'Adverse Event' "
			+ "GROUP BY ae_stdate "
			+ "UNION "
			+ "SELECT COUNT(*) AS Cnt, TO_CHAR(ae_stdate,'mm/dd/yyyy') AS ae_stdate,  'Serious Adverse Event' AS AdvType "
			+ "FROM ESCH.sch_adverseve, ER_STUDY, ER_PER "
			+ "WHERE pk_study = fk_study AND "
			+ "pk_per = fk_per AND "
			+ "ER_PER.fk_account = "+accountId+" AND "
			+ "to_date(AE_STDATE) BETWEEN trunc(SYSDATE-7) AND trunc(SYSDATE-1) "
			+ "AND (SELECT codelst_desc FROM ESCH.sch_codelst WHERE pk_codelst = fk_codlst_aetype) = 'Serious Adverse Event' "
			+ "GROUP BY  ae_stdate) ";
			
			query = new BrowserRows();
			countSql = "select count(*) as cnt from (" +queryCount+ ")";
			query.getPageRows(1,10,queryCount,50,countSql,"2 asc,3","asc");
			
			if (query.getRowReturned()!= 0){
				for(int counter=1; counter<=query.getRowReturned();counter++){
					aestdate = query.getBValues(counter,"ae_stdate").substring(0,5);
					AdvTYP = query.getBValues(counter,"AdvType");
					
					System.out.println("aestdate " +aestdate);
					System.out.println("AdvTYP " +AdvTYP);
					System.out.println("Prevaestdate " +Prevaestdate);
					System.out.println("PrevAdvTYP " +PrevAdvTYP);
		
					if (AdvTYP.compareTo("Adverse Event")==0){
						if (PrevAdvTYP.compareTo("Adverse Event")==0){
							saeSeries = saeSeries + ",'" + Prevaestdate + "'|0"+ "";
						} 
						aeSeries = aeSeries + ",'" + aestdate + "'|" + query.getBValues(counter,"Cnt") + "";
						aeCnt = aeCnt + EJBUtil.stringToNum(query.getBValues(counter,"Cnt"));
					}else{
						if (PrevAdvTYP.compareTo("Serious Adverse Event")==0){
							aeSeries = aeSeries + ",'" + aestdate + "'|0"+ "";
						} else {
							if (PrevAdvTYP.compareTo("Adverse Event")==0){
								//AdvTYP == "Serious Adverse Event" and PrevAdvTYP == "Adverse Event"
								if (Prevaestdate.compareTo(aestdate)!=0){
									aeSeries = aeSeries + ",'" + aestdate + "'|0"+ "";
									if (Prevaestdate.compareTo("")!= 0){
										saeSeries = saeSeries + ",'" + Prevaestdate + "'|0"+ "";
									}
								}
							}
						}
						saeSeries = saeSeries + ",'" + aestdate + "'|" + query.getBValues(counter,"Cnt") + "";
						saeCnt = saeCnt + EJBUtil.stringToNum(query.getBValues(counter,"Cnt"));
					}
					Prevaestdate = aestdate;
					PrevAdvTYP = AdvTYP;
				}
			}*/
		%>
		<Input type="hidden" id="aeSeries" name="aeSeries" value="<%=aeSeries%>">
		<Input type="hidden" id="saeSeries" name="saeSeries" value="<%=saeSeries%>">
		<Input type="hidden" id="aeCnt" name="aeCnt" value="<%=aeCnt%>">
		<Input type="hidden" id="saeCnt" name="saeCnt" value="<%=saeCnt%>">
		<%
			String notDone = "'00/00'|0";
			String doneOut = "'00/00'|0";
			String schdate = "";
			String eventStat = "";
			String Prevschdate = "";
			String PrevEventStat = "";
			int notDoneCnt =0;
			int doneOutCnt =0;
			
			queryCount = "SELECT  COUNT(*) AS Cnt, TO_CHAR(PTSCH_ACTSCH_DATE, 'mm/dd/yyyy') AS Start_Date_Time, EVENT_STATUS "
			+ "FROM ("
			+ "SELECT PTSCH_ACTSCH_DATE, EVENT_STATUS, "
			+ "CASE WHEN eventstat_date > fuzzy_duration_aft OR eventstat_date < fuzzy_duration_bef THEN 'Yes' ELSE 'No' END AS out_of_range "
			+ "FROM ( "
			+ "SELECT c.FK_STUDY as PK_STUDY, (SELECT STUDY_NUMBER FROM ERES.ER_STUDY WHERE PK_STUDY = c.FK_STUDY) STUDY_NUMBER, "
			+ "fk_per, PERSON_CODE, PATPROT_PATSTDID, "
			+ "(SELECT NAME FROM EVENT_ASSOC WHERE EVENT_ASSOC.EVENT_ID = a.SESSION_ID) PTSCH_CALENDAR, "
			+ "(SELECT VISIT_NAME FROM SCH_PROTOCOL_VISIT WHERE PK_PROTOCOL_VISIT = a.FK_VISIT) PTSCH_VISIT, "
			+ "a.DESCRIPTION PTSCH_EVENT, Start_Date_Time PTSCH_ACTSCH_DATE, "
			+ "(SELECT  NVL(FUZZY_PERIOD,0)  || ' ' ||  NVL(F_Get_Durunit(EVENT_DURATIONBEFORE),'days') || ' Before / ' || "
			+ "NVL(EVENT_FUZZYAFTER,0) || ' ' ||  NVL(F_Get_Durunit(EVENT_DURATIONAFTER),'days') || ' After' "
			+ "FROM EVENT_ASSOC WHERE EVENT_ASSOC.EVENT_ID = a.fk_assoc) PTSCH_VISIT_WIN, "
			+ " CASE "
			+ "  	  WHEN event_durationbefore ='D' THEN (Start_Date_Time) - (TO_NUMBER(fuzzy_period) * 1)"
			+ " 	  WHEN event_durationbefore ='H' THEN (Start_Date_Time) - 1"
			+ " 	  WHEN event_durationbefore ='W' THEN (Start_Date_Time) - (TO_NUMBER(fuzzy_period) * 7)"
			+ " 	  WHEN event_durationbefore ='M' THEN (Start_Date_Time) - (TO_NUMBER(fuzzy_period) *30)"
			+ " 	  WHEN event_durationbefore IS NULL THEN (Start_Date_Time)"
			+ " END AS fuzzy_duration_bef,"
			+ " CASE "
			+ "  	  WHEN event_durationafter ='D' THEN (Start_Date_Time) + (TO_NUMBER(event_fuzzyafter) * 1)"
			+ " 	  WHEN event_durationafter ='H' THEN (Start_Date_Time) + 1"
			+ " 	  WHEN event_durationafter ='W' THEN (Start_Date_Time) + (TO_NUMBER(event_fuzzyafter) * 7)"
			+ " 	  WHEN event_durationafter ='M' THEN (Start_Date_Time) + (TO_NUMBER(event_fuzzyafter) *30)"
			+ " 	  WHEN event_durationafter IS NULL THEN (Start_Date_Time)"
			+ " END AS fuzzy_duration_aft,"
			+ "CASE "
			+ "	 WHEN Start_Date_Time < trunc(SYSDATE) AND (SELECT codelst_subtyp FROM sch_codelst WHERE pk_codelst = isconfirmed) = 'ev_notdone' THEN 'Past Scheduled Date' "
			+ "	 ELSE (SELECT CODELST_DESC FROM SCH_CODELST WHERE ISCONFIRMED = PK_CODELST) "
			+ "END AS EVENT_STATUS, "
			+ "EVENT_EXEON EVENTSTAT_DATE, a.notes EVENTSTAT_NOTES "
			+ "FROM SCH_EVENTS1 a, person b, ER_PATPROT c, EVENT_ASSOC d "
			+ "WHERE pk_person = c.fk_per "
			+ "AND pk_patprot = fk_patprot "
			+ "AND d.EVENT_ID = a.fk_assoc "
			+ "AND Start_Date_Time < trunc(SYSDATE) "
			+ "AND b.fk_ACCOUNT = "+accountId+") t "
			+ "WHERE (t.EVENT_STATUS = 'Past Scheduled Date' OR (t.EVENT_STATUS = 'Done' AND (t.eventstat_date > fuzzy_duration_aft OR t.eventstat_date < fuzzy_duration_bef)))"
			+ " AND (EXISTS (SELECT * FROM er_studyteam h WHERE t.Pk_study = h.fk_study AND h.fk_user = "+ userId +" AND NVL(studyteam_status,'Active') = 'Active')"
			+ " OR Pkg_Phi.F_Is_Superuser("+ userId +", t.pk_study) = 1) "
			+ ") "
			+ "WHERE (PTSCH_ACTSCH_DATE <= TRUNC(SYSDATE) "
			+ "AND PTSCH_ACTSCH_DATE >= TRUNC(SYSDATE-7) "
			+ ") "
			+ "GROUP BY PTSCH_ACTSCH_DATE, EVENT_STATUS ";
			
			query = new BrowserRows();
			countSql = "select count(*) from (" +queryCount+ ")";
			
			queryCount = queryCount + "ORDER BY PTSCH_ACTSCH_DATE asc ";
			query.getPageRows(1,100,queryCount,50,countSql," EVENT_STATUS ","asc");
			
			if (query.getRowReturned()!= 0){
				for(int counter=1; counter<=query.getRowReturned();counter++){
					schdate = query.getBValues(counter,"Start_Date_Time").substring(0,5);
					eventStat = query.getBValues(counter,"EVENT_STATUS");
					//System.out.println(counter + " " + query.getBValues(counter,"Start_Date_Time") + " " + query.getBValues(counter,"EVENT_STATUS") + " " + query.getBValues(counter,"Cnt"));
					
					if (eventStat.compareTo("Done")==0){
						if (PrevEventStat.compareTo("Done")==0){
							notDone = notDone + ",'" + Prevschdate + "'|0"+ "";
						} 
						doneOut = doneOut + ",'" + schdate + "'|" + query.getBValues(counter,"Cnt") + "";
						doneOutCnt = doneOutCnt + EJBUtil.stringToNum(query.getBValues(counter,"Cnt"));
					}else{
						if (PrevEventStat.compareTo("Past Scheduled Date")==0){
							doneOut = doneOut + ",'" + schdate + "'|0"+ "";
						} else {
							if (PrevEventStat.compareTo("")==0){
								doneOut = doneOut + ",'" + schdate + "'|0"+ "";
							} else {
								if (PrevEventStat.compareTo("Done")==0){
									//eventStat == "Past Scheduled Date" and PrevEventStat == "Done"
									if (Prevschdate.compareTo(schdate)!=0){
										doneOut = doneOut + ",'" + schdate + "'|0"+ "";
										if (Prevschdate.compareTo("")!= 0){
											notDone = notDone + ",'" + Prevschdate + "'|0"+ "";
										}
									}
								}
							}
						}
						notDone = notDone + ",'" + schdate + "'|" + query.getBValues(counter,"Cnt") + "";
						notDoneCnt = notDoneCnt + EJBUtil.stringToNum(query.getBValues(counter,"Cnt"));
					}
					Prevschdate = schdate;
					PrevEventStat = eventStat;
					//System.out.println("doneOutCnt " + doneOutCnt);
					//System.out.println("notDoneCnt " + notDoneCnt);
				}
			}
		%>
		<Input type="hidden" id="notDone" name="notDone" value="<%=notDone%>">
		<Input type="hidden" id="doneOut" name="doneOut" value="<%=doneOut%>">
		<Input type="hidden" id="doneOutCnt" name="doneOutCnt" value="<%=doneOutCnt%>">
		<Input type="hidden" id="notDoneCnt" name="notDoneCnt" value="<%=notDoneCnt%>">
		
		<!--div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div-->
		<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
		<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
		<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
		
		<br>
		<Form name="dmDashboardpg" method=post>
		<!-- New Dashlet BEGIN-->
		<div class="tmpHeight"></div>
		
		<DIV id="charts" class="chartDisplay midalign">		
		<table width="100%" cellpadding="0" cellspacing="0" border="0">
			<tr>
			<td width="1%"></td>
			<td width="15%" class="lhsFont tmpHeight" valign="top">	
				<div id="tree" >
					<span class="trigger" onClick="showChart(0,'<%=LC.L_Query_Summary%><%--Query Summary*****--%>');">
						<img src="./js/tree/closed.gif" id="IQSummary"> <%=LC.L_Query_Summary%><%--Query Summary*****--%><br>
					</span>
					<span class="trigger" onClick="showBranch('Status');">
						<img src="./js/tree/closed.gif" id="IStatus"> <%=LC.L_Query_Activity%><%--Query Activity*****--%><br>
					</span>
					<span class="branch" id="Status" >
						<img src="./js/tree/doc.gif"><a href="#" onClick="showChart(1,'-Today' );"><%=LC.L_Today%><%--Today*****--%></a>
						<br/>
						<img src="./js/tree/doc.gif"><a href="#" onClick="showChart(2,'- All Open');"><%=LC.L_All_Open%><%--All Open*****--%></a>
						<br/>
						<img src="./js/tree/doc.gif"><a href="#" onClick="showChart(3,'- All Resolved');"><%=LC.L_All_Resolved%><%--All Resolved*****--%></a>
						<br/>
					</span>
					<span class="trigger" onClick="showChart(4,'By Days' );">
						<img src="./js/tree/closed.gif" id="IExpectancy"> <%=LC.L_Form_Expectancy%><%--Form Expectancy*****--%><br>
					</span>
					<span class="trigger" onClick="showChart(5,'By Days' );">
						<img src="./js/tree/closed.gif" id="IOverDue"> <%=LC.L_Overdue_Forms%><%--Overdue Forms*****--%><br>
					</span>
					<span class="trigger" onClick="showBranch('Deviation');">
						<img src="./js/tree/closed.gif" id="IDeviation"> <%=LC.L_Pcol_Deviations%><%--Protocol Deviations*****--%><br>
					</span>
					<span class="branch" id="Deviation" >
						<img src="./js/tree/doc.gif"><a href="#" onClick="showChart(6,'Last 7 Days');"><%=LC.L_Adverse_Events%><%--Adverse Events*****--%></a>
						<br/>
						<img src="./js/tree/doc.gif"><a href="#" onClick="showChart(7,'Last 7 Days');"><%=LC.L_Visit_Related%><%--Visit related*****--%></a>
						<br/>
					</span>
				</div> 
			</td>
			<td  width="25%" align="left">
				<span style="height: 230px;"></span>
				<span id="onlyChartParent" style="height: 230px;">
					<div id="onlyChart0" name="onlyChart" class="chart" style="border-width: 1px; border-color: #A80101; border-style: solid; width: 460px; height: 210px; display:none"></div>
					<div id="onlyChart1" name="onlyChart" class="chart" style="border-width: 1px; border-color: #A80101; border-style: solid; width: 460px; height: 210px; display:none"></div>
					<div id="onlyChart2" name="onlyChart" class="chart" style="border-width: 1px; border-color: #A80101; border-style: solid; width: 460px; height: 210px; display:none"></div>
					<div id="onlyChart3" name="onlyChart" class="chart" style="border-width: 1px; border-color: #A80101; border-style: solid; width: 460px; height: 210px; display:none"></div>
					<div id="onlyChart4" name="onlyChart" class="chart" style="border-width: 1px; border-color: #A80101; border-style: solid; width: 460px; height: 210px; display:none"></div>
					<div id="onlyChart5" name="onlyChart" class="chart" style="border-width: 1px; border-color: #A80101; border-style: solid; width: 460px; height: 210px; display:none"></div>
					<div id="onlyChart6" name="onlyChart" class="chart" style="border-width: 1px; border-color: #A80101; border-style: solid; width: 460px; height: 210px; display:none"></div>
					<div id="onlyChart7" name="onlyChart" class="chart" style="border-width: 1px; border-color: #A80101; border-style: solid; width: 460px; height: 210px; display:none"></div>
				</span>
			</td>					
			<td width="35%" align="left" valign="top">
				<div id="dataT"></div>
			</td></tr>
		</table>
		</DIV>
		<!-- New Dashlet END -->
		<div id="Browser">
			<jsp:include page="ajaxStudyDataFetch.jsp?nodeNo=0&snodeNo=0" flush="true"/>
		</div>
		<script>
			createChart(0,'Query Summary'); showChart(0,'Query Summary');
			createChart(1,'-Today' );
			createChart(2,'- All Open' );
			createChart(3,'- All Resolved' );
			createChart(4,'By Days' );
			createChart(5,'By Days' );
			createChart(6,'Last 7 Days' );
			createChart(7,'Last 7 Days' );
		</script>
		</Form>
	<%} //end of if body for page right
		else{%>
			<jsp:include page="accessdenied.jsp" flush="true"/>
		<%} //end of else body for page right
	} //end of if body for session
	else {%>
		<jsp:include page="timeout.html" flush="true"/>
	<%}%>
</body>
</html>
