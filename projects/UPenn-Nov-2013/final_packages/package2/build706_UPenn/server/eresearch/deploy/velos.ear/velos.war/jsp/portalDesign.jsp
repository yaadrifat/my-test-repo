<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>


<head>
	<title><%=LC.L_Portal_DesignDets%><%-- Portal Design Details*****--%></title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<jsp:include page="include.jsp" flush="true"/>
</head>


	<%@ page language = "java" import = "java.util.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*, com.velos.esch.business.common.*,com.velos.eres.web.grpRights.GrpRightsJB"%>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<jsp:useBean id="portalDesignB" scope="request" class="com.velos.eres.web.portalDesign.PortalDesignJB"/>
	<jsp:useBean id="eventAssocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
	<jsp:useBean id="portalB"  scope="request" class="com.velos.eres.web.portal.PortalJB"/>
	<jsp:useBean id="FormB"  scope="request" class="com.velos.eres.web.formLib.FormLibJB"/>


<%

	String src;
	String resetFlagForFK_IDs = "";
	src= request.getParameter("srcmenu");


%>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="formjs.js"></SCRIPT>
<SCRIPT Language="javascript">
	var clrAll = "";
	if (document.getElementById('eSign')) { document.getElementById('eSign').disabled=false; }
	if (document.getElementById('submit_btn')) { document.getElementById('submit_btn').disabled=false; }

	function  blockSubmit() {
		setTimeout('document.getElementById(\'eSign\').disabled=true', 10);
		setTimeout('document.getElementById(\'submit_btn\').disabled=true', 10);
	}

	function fnClear(){
	document.getElementById('pmFromId').value = "";
	document.getElementById('pmToId').value = "";
	document.portalDesign.pmFromUnit.value="";
	document.portalDesign.pmToUnit.value="";

	}

	function getWhereClauseForFormsLookup1(formtype, study, pat) {
		return "portalDesign|"+formtype+"|||"+study+"|"+pat;
	}
	function openFormsLookup(formobj,mode)
	{

		accId=formobj.accId.value;
		userId=formobj.userId.value;
		studyId=formobj.studyId.value;
		var maxSelect = "";

		if (studyId=="")
		{
			studyId="0";
		}

		type='P';
		var filter="";

		var fieldName="";

		if (mode=="multi")
		{
			fieldName="selForms";
			idFieldName="paramFormIds";

			if ((formobj.rdForm[1].checked==false) ) {
				alert("<%=MC.M_SelDisp_FrmLnk%>");/*alert("Please Select 'Display Form(s) as Link(s)'");*****/
	        	return false;
	        }
		}
		else if (mode=="single")
		{
			fieldName="selForm";
			idFieldName="paramFormIds";

			maxSelect = "&maxselect=1";
			if ((formobj.rdForm[0].checked==false)   ) {
				alert("<%=MC.M_SelFrm_InEditMode%> ");/*alert("Please Select 'Open Form in Edit Mode'");*****/
	            return false;
	   		}
		}
		else
		{
			fieldName="selConsentForm";
			idFieldName="selConsentFormId";
			maxSelect = "&maxselect=1";
		}


		filter =  getWhereClauseForFormsLookup1(type, parseInt(studyId), 0);
		formobj.dfilter.value=filter;
		formobj.target='formLkp';
		formobj.action="multilookup.jsp?viewId=&viewName=Form Browser&form=portalDesign&seperator=,&frameMode=TB&keyword="+fieldName+"|VELFORMNAME~"+idFieldName+"|VELFORMPK|[VELHIDE]&filter=[VELGET:dfilter]" + maxSelect;

		windowname=window.open('donotdelete.html' ,'formLkp',"toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=800,height=600 top=100,left=100 0, ");
		formobj.submit();
		formobj.action="updatePortalDesign.jsp";
		formobj.target="";
		windowname.focus();


	}

	//GUI: Open Form in Edit Mode
	function fnPatForm() {
		clrAll = "2";
		if(document.portalDesign.patSchelinkFormCalId.value=="" || document.portalDesign.patSchelinkFormCalId.value==null)
		document.portalDesign.patSchelinkFormCalId.value=document.portalDesign.pkCalId.value;
		document.portalDesign.inclChks.value="";
		document.getElementById('dispformedit').value = "";
		document.portalDesign.paramFormIds.value = "";

		//JM: 13Aug2007: added
		document.getElementById('onepatsch').checked = false;
		document.getElementById('linkpatsch').checked = false;

		document.getElementById('linkpatschedit').value = "";
		document.getElementById('patschedit').value = "";

		document.getElementById('formalign').value = '';
		document.getElementById('schalign').value = '';
		document.getElementById('linkpatschedit').value = "";
		document.getElementById('patschedit').value = "";
		document.getElementById('rdDates_all').checked = true;
		fnClear();


		fnDisableEnable('E');

	}

	function fnDisableEnable(formtype)
	{
		if (formtype == 'E')
		{
			document.getElementById('onepatsch').disabled = true;
			document.getElementById('linkpatsch').disabled = true;
			document.getElementById('rdDates_selected').disabled = true;
			document.getElementById('schalign').disabled = true;
			document.getElementById('pmFromId').disabled = true;
			document.getElementById('pmFromUnitId').disabled = true;
			document.getElementById('pmToId').disabled = true;
			document.getElementById('pmToUnitId').disabled = true;

		}
		else
		{
			document.getElementById('onepatsch').disabled = false;
			document.getElementById('linkpatsch').disabled = false;
			document.getElementById('rdDates_selected').disabled = false;
			document.getElementById('schalign').disabled = false;
			document.getElementById('pmFromId').disabled = false;
			document.getElementById('pmFromUnitId').disabled = false;
			document.getElementById('pmToId').disabled = false;
			document.getElementById('pmToUnitId').disabled = false;


		}

	}

	//GUI: Display Form(s) as Links
	function fnDispForms() {
		document.getElementById('oneformedit').value = "";
		document.portalDesign.paramFormIds.value = "";

		fnDisableEnable('L');
	}

	function fnPatSchedule() {
		clrAll = "2";
		if(document.portalDesign.patSchelinkFormCalId.value=="" || document.portalDesign.patSchelinkFormCalId.value==null)
		document.portalDesign.patSchelinkFormCalId.value=document.portalDesign.pkCalId.value;
		document.portalDesign.inclChks.value="";
		document.getElementById('linkpatschedit').value = "";
		document.getElementsByName('paramprotCalId')[0].value="";
		if(document.getElementsByName('calsPresentOnStudy')[0].value=='false'){
			alert("<%=MC.M_NoCalAval_ForPortal%>");
			document.getElementsByName('rdpatSchedule')[0].checked=false;
			return false;
		}
	}

	function fnPatScheduleForms() {
		clrAll = "";
		document.getElementById('patschedit').value = "";
		document.getElementsByName('paramprotCalId')[0].value="";
		if(document.getElementsByName('calsPresentOnStudy')[0].value=='false'){
			alert("<%=MC.M_NoCalAval_ForPortal%>");
			document.getElementsByName('rdpatSchedule')[1].checked=false;
			return false;
		}
		
	}

	//JM: 16Aug2007: added this JS function to reset the radio buttons and dates: Bugzilla issue #2601
	function clearAllSlected() {

		//JM: 25Mar2011, #5742
		clrAll = "1";
		document.portalDesign.inclChks.value="";		

		document.getElementById('onepatsch').disabled = false;
		document.getElementById('linkpatsch').disabled = false;
		document.getElementById('onepatsch').checked = false;
		document.getElementById('linkpatsch').checked = false;

		document.getElementById('oneformrd').checked = false;
		document.getElementById('dispformrd').checked = false;

		document.portalDesign.paramFormIds.value = "";
		document.portalDesign.paramprotCalId.value = "";

		document.getElementById('oneformedit').value = "";
		document.getElementById('dispformedit').value = "";
		document.getElementById('linkpatschedit').value = "";
		document.getElementById('patschedit').value = "";

		document.getElementById('schalign').disabled = false;
		document.getElementById('pmFromId').disabled = false;
		document.getElementById('pmFromUnitId').disabled = false;
		document.getElementById('pmToId').disabled = false;
		document.getElementById('pmToUnitId').disabled = false;
		document.getElementById('rdDates_selected').disabled = false;


		document.getElementById('formalign').value = "";

		//dont need to reset it. oneopetion is selected by default in new mode too
		document.portalDesign.portalFormAfterResp.value = "BL";

		document.getElementById('schalign').value = "";

		document.getElementById('rdDates_all').checked = true;
		fnClear();
	}

	function validate(formobj) {

		document.portalDesign.clrAll.value = clrAll;


		if ((formobj.rdForm[0].checked==true) && (formobj.selForm.value=="")) {
			alert("<%=MC.M_SelFrm_LookupMode%>");/*alert("Please Select Form(s) from the lookup for the 'Form in Edit Mode'");*****/
	        //formobj.selForm.focus();
	        return false;
	    }

	    if ((formobj.rdForm[1].checked==true) && (formobj.selForms.value=="")) {
	    	alert("<%=MC.M_SelFrm_LookupLnks%>");/*alert("Please Select Form(s) from the lookup for the 'Form(s) as Link(s)'");*****/
	        //formobj.selForms.focus();
	        return false;
	    }
	      // Modified By Amarnadh for Bugzilla issue #3062
		 //Modified for Bug #7619, #7682  By- Yogendra
	      if(formobj.resetFlagForFK_IDs.value==""){
	      
			if ((formobj.rdpatSchedule[0].checked==true) && (formobj.selCal.value=="")) {
				alert("<%=MC.M_PlsLookupCal_PatSch%>");/*alert("Please Select a calendar from the lookup for the '<%=LC.Pat_Patient%> Schedule Only' ");*****/
				//formobj.selCal.focus();
		        return false;
		    }
		   
		    if ((formobj.rdpatSchedule[1].checked==true) && (formobj.selCalWf.value=="")) {
		    	alert("<%=MC.M_PlsSelCal_LookupPatSch%>");/*alert("Please Select a calendar from the lookup for the '<%=LC.Pat_Patient%> Schedule with Linked Forms' ");*****/
		        //formobj.selCalWf.focus();
		        return false;
			}
	
		   
		    if ((formobj.rdpatSchedule[0].checked==false)&& (formobj.rdpatSchedule[1].checked==false) && (document.getElementById('pmFrom').value !="" || document.getElementById('pmTo').value != ""	) ){
		    	alert("<%=MC.M_DtCntSvd_UntilPatSch%>");/*alert("Dates can not be saved until an item from the <%=LC.Pat_Patient%> Schedule is selected");*****/
				return false;
		    }
	    }
		    
		if (! (splcharcheck(formobj.designBgColor.value))) {
		    formobj.designBgColor.focus();
			return false;
		}

		if (! (checkquote(formobj.designBgColor.value))) {
			formobj.designBgColor.focus();
			return false;
		}

		if (document.portalDesign.status.value !='' && 	document.portalDesign.align.value!=''){
			if ( document.portalDesign.status.value == document.portalDesign.align.value){
				alert("<%=MC.M_Selc_DiffAlignment%>");/*alert("Please Select a different alignment");*****/
				document.portalDesign.status.focus();
				return false;
			}
		}

		/*if (document.portalDesign.align.value=='C' && document.portalDesign.status.value!=''){
			alert("You can not select any other alignment for <%=LC.Pat_Patient%> Schedule section if 'Center alignment' is selected for <%=LC.Pat_Patient%> Forms section." );
			document.portalDesign.align.focus();
			return false;
		}

		if (document.portalDesign.status.value=='C' && document.portalDesign.align.value!=''){
			alert("You can not select any other alignment for <%=LC.Pat_Patient%> Forms section if 'Center alignment' is selected for <%=LC.Pat_Patient%> Schedule section." );
			document.portalDesign.status.focus();
			return false;
		}*/

		if (formobj.rdForm[0].checked==false && formobj.rdForm[1].checked==false && document.portalDesign.align.value!=''){
			alert("<%=MC.M_SelOneFrom_PatFrmSec%>");/*alert("Please Select one item from <%=LC.Pat_Patient%> Form section");*****/
			return false;
		}

		if (formobj.rdpatSchedule[0].checked==false && formobj.rdpatSchedule[1].checked==false && document.portalDesign.status.value!=''){
			alert("<%=MC.M_SelOneFrom_PatSchSec%>");/*alert("Please Select one item from <%=LC.Pat_Patient%> Schedule section");*****/
			return false;
		}

		//for center alignment

		if ( (formobj.rdForm[0].checked==true || formobj.rdForm[1].checked==true ) &&
		  (formobj.rdpatSchedule[0].checked==true  || formobj.rdpatSchedule[1].checked==true ) && ( document.portalDesign.status.value=='C' || document.portalDesign.align.value=='C'))
		{
			alert("<%=MC.M_CentreAlignAval_OneOptSel%>");/*alert("Center alignment is only available when only one option, either 'Include Forms' or 'Include Schedule' is selected. Please change the alignment");*****/
			return false;
		}

//JM: 14Dec2009: #3116
		if ((document.portalDesign.pmFromId.value !=0 && document.portalDesign.pmFromId.value !="") && document.portalDesign.pmFromUnit.value ==""){
			alert("<%=MC.M_PlsSel_Duration%>");/*alert("Please select the Duration Unit");*****/
			document.portalDesign.pmFromUnit.focus();
			return false;
		}
		if ((document.portalDesign.pmToId.value !=0 && document.portalDesign.pmToId.value !="") && document.portalDesign.pmToUnit.value ==""){
			alert("<%=MC.M_PlsSel_Duration%>");/*alert("Please select the Duration Unit");*****/
			document.portalDesign.pmToUnit.focus();
			return false;
		}

		if ( document.portalDesign.pmFromId.value =='' && document.portalDesign.pmFromUnit.value !=""){
			alert("<%=MC.M_Etr_Dur%>");/*alert("Please enter the Duration");*****/
			document.portalDesign.pmFromId.focus();
			return false;
		}
		if (document.portalDesign.pmToId.value =='' && document.portalDesign.pmToUnit.value !=""){
			alert("<%=MC.M_Etr_Dur%>");/*alert("Please enter the Duration");*****/
			document.portalDesign.pmToId.focus();
			return false;
		}





		if (!(validate_col('e-Sign', formobj.eSign))) return false;

		if (isNaN(formobj.eSign.value)==true) {
			alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
            formobj.eSign.focus();
            return false;
        }


//JM: 12JAN2011: #5721

		calendarId=formobj.paramprotCalId.value;
		oldcalendarId=formobj.paramprotCalIdOld.value;		
		tmpCal=formobj.calIdVal.value;

		
		var selValChecks ="";

		selValChecks =formobj.inclChks.value;

		if (oldcalendarId=="") {			
		
			//do nothing...		
		}else{			

			if (((calendarId !=oldcalendarId) && (tmpCal!=calendarId)) ||  (tmpCal!=calendarId )){				
				selValChecks = "";	
				formobj.inclChks.value="";
						
			}

		}	
        
		return true;
	}

	function openFormWindow(formobj,formname, editname) {
		formorg1=formobj.selForm.value;
		windowName=window.open("selectForm.jsp?openerform="+formname+"&selectForms="+formorg1+"&editcolumn="+editname,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=350,height=300");
		windowName.focus();
	}


	//JM: get the Calendar look up
	function openLookup(formobj,url,filter) {
	
		if(url.indexOf('selCalWf|')>=0 && !document.getElementById('linkpatsch').checked)
		{
			alert("<%=MC.M_PlsSelPatSchLkd_First%>"	);
			return false;
		}if(url.indexOf('selCal|')>=0 && !document.getElementById('onepatsch').checked){
			alert("<%=MC.M_PlsSelPatSch_First%>"	);
			return false;
		}
		
		var tempValue;
		var urlL="";
		formobj.target="Lookup";
		formobj.method="post";
		urlL="multilookup.jsp?"+url;
		if (filter.length>0){

			if (filter.indexOf('portalDesign1')>=0){

	  			tempValue="";
	  			tempValue=formobj.paramstudyId.value;

	  				if ((tempValue == 'null') || tempValue == '0'){
	  					alert("<%=MC.M_NoCalAval_ForPortal%>");/*alert("There is no Calendar available for this portal");*****/
	   				return;
	   				}else{
	   					filter += tempValue;
					}
	  		}

			urlL=urlL+"&"+filter;
	 	}
	 	
		formobj.action=urlL;

		formWin =open('donotdelete.html','Lookup','resizable=1,status=0, width=850,height=550 top=100,left=100,menubar=no,scrollbars=1');
		if (formWin && !formWin.closed) formWin.focus();
		formobj.submit();
		formobj.target="";
		formobj.action="updatePortalDesign.jsp";
		void(0);
	}

//JM: 07DEC2010	

	function callfunformaccesspg(formobj){
		calendarIds=formobj.paramprotCalId.value;
		oldcalendarIds=formobj.paramprotCalIdOld.value;		
		tmpCals=formobj.calIdVal.value;

		
		var selValChecks ="";
		selValChecks =formobj.inclChks.value;

		if (oldcalendarIds=="") {			
			if ((calendarIds !="") && (tmpCals!=calendarIds)){	
				selValChecks = "";	
				formobj.inclChks.value="";		
			}	
		}else{			
			if (tmpCals!=""){
				if ((calendarIds !=oldcalendarIds) && (tmpCals!=calendarIds) ){				
					selValChecks = "";	
					formobj.inclChks.value="";
							
				}else if ((tmpCals==calendarIds)){			
	
	
				}else if (tmpCals!=calendarIds ){				
					selValChecks = "";
					formobj.inclChks.value="";
	
				}
			}

		}
		
		portalId=formobj.portalId.value;
		
		if ((formobj.rdpatSchedule[1].checked==true) && (formobj.selCalWf.value=="")) {
			alert("<%=MC.M_SelcCal_PatSchFrm%>");/*alert("Please select the Calendar for <%=LC.Pat_Patient%> Schedule with Linked Forms");*****/
			return false; 
		}else if ((formobj.rdpatSchedule[1].checked==true) && (formobj.selCalWf.value!="")) {
			formWin =open('portalformaccesspg.jsp?parentForm=portalDesign&calIds='+calendarIds+"&portalId="+portalId+"&selChkVals="+selValChecks,'','resizable=1,status=0, width=850,height=550 top=100,left=100,menubar=no,scrollbars=1');
			if (formWin && !formWin.closed) formWin.focus();		
			
		}else if ((formobj.rdpatSchedule[1].checked==false) && (formobj.selCalWf.value!="")){
			alert("<%=MC.M_PlsSel_PatSchLkdFrm%>");/*alert("Please select the item '<%=LC.Pat_Patient%> Schedule with Linked Forms'");*****/
			return false; 
			
		}else if ((formobj.rdpatSchedule[1].checked==false) && (formobj.selCalWf.value=="")){
			alert("<%=MC.M_SelcItem_PatSchLkdCal%>");/*alert("Please select the item '<%=LC.Pat_Patient%> Schedule with Linked Forms' and select one Calendar");*****/
			return false; 
			
		}
	}



</SCRIPT>
</HEAD>

<BODY style="overflow:auto;">

<%

	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession)) {

		String mode = "";
	    mode = request.getParameter("mode");

		String uName = (String) tSession.getValue("userName");

		//JM: 14Jun2007
		int pageRight = 0;
		GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");
		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("PORTALAD"));


	String accId = (String) tSession.getValue("accountId");
	String userId = (String)tSession.getValue("userId");
	String portalId = "";
	String stdobjIds="";
    String orgobjIds="";
    String selfLogout = "";
	String checkedSelfLogout = "";


	portalId = request.getParameter("portalId");
	selfLogout = request.getParameter("selfLogout");

	if (StringUtil.isEmpty(selfLogout))
	{
		selfLogout = "0";
	}

	if (selfLogout.equals("1"))
	{
		checkedSelfLogout = "Checked";
	}


	boolean editFormSelected = false;
	boolean linkFormSelected = false;

	String portalModIdForm = "";
	String portalModIdSchedule = "";
	String portalModAlignForm = "";
	String portalModAlignSchedule = "";
	String portalModTypeForm = "";
	String portalModTypeSchedule = "";

	String fkIdForm = "";
	String fkIdSchedule = "";

	int  portalModId = 0;
	String portalModType = "";

	String fkId ="";
	String portalModAlign = "";
	String portalModFrom = "";
	String portalModTo = "";
	String portalModFromUnit = "";
	String portalModToUnit = "";
	String portalModFormResp ="";
	String portalBgColor = "";
	String portalHeader ="";
	String portalFooter ="";
	String strVal=""; String  calName = "";
	String  calsPresentOnStudy = "false";
	String portalName = "";
	String portalConsentingForm = "";
	String portalConsentingFormName = "";


	portalB.setPortalId(EJBUtil.stringToNum(portalId));
    portalB.getPortalDetails();
	portalName = portalB.getPortalName();

	portalConsentingForm = portalB.getPortalConsentingForm();

	if (! StringUtil.isEmpty(portalConsentingForm))
	{
		FormB.setFormLibId(EJBUtil.stringToNum(portalConsentingForm));
		FormB.getFormLibDetails();
		portalConsentingFormName = FormB.getFormLibName();
		System.out.println("portalConsentingFormName"+portalConsentingFormName);

	}
	else
	{
		portalConsentingForm="";
	}

	PortalDesignDao pdDao = portalDesignB.getPortalModValues(EJBUtil.stringToNum(portalId));
	ArrayList portalKeys = pdDao.getPortalKeys();
	ArrayList portalModIds = pdDao.getPortalModIds();
	ArrayList fkIds = pdDao.getFkIds();
	ArrayList portalModTypes = pdDao.getPortalModTypes();
	ArrayList portalModAligns = pdDao.getPortalModAligns();
	ArrayList portalModFroms = null;
	portalModFroms = pdDao.getPortalModFroms();

	String[] arFormAfterRespOptions = {"BL","P","N"};
	String[] arFormAfterRespOptionsDesc = {MC.M_NotAvt_ForPat,LC.L_Printer_FriendlyFormat,MC.M_NewFrm_EachTime};/*String[] arFormAfterRespOptionsDesc = {"Not available for "+LC.Pat_Patient,"Printer Friendly Format","New Form each time"};*****/
	StringBuffer ddFormAfterResp = new StringBuffer();

	ArrayList portalModTos = pdDao.getPortalModTos();
 	ArrayList portalModFromUnits = pdDao.getPortalModFromUnits();
	ArrayList portalModToUnits = pdDao.getPortalModToUnits();
	ArrayList portalModFormResps = pdDao.getPortalModFormResps();
	ArrayList portalBgColors = pdDao.getPortalBgColors();
	ArrayList portalHeaders = pdDao.getPortalHeaders();
	ArrayList portalFooters = pdDao.getPortalFooters();


	//JM:14Aug2007
	int len = portalModIds.size();
	//int len = portalKeys.size();
	if (len ==0)
		   mode ="N";
		else
		   mode = "M";

	//System.out.println("mode=" + mode + "length="  +len);
	
	   String studyId =null;
	   studyId = request.getParameter("studyId");
	   if (StringUtil.isEmpty(studyId))
	   {
	   	studyId = "";
	   }
	   
	   if(!(eventAssocB.getStudyProts(StringUtil.stringToNum(studyId)).getEvent_ids().isEmpty())){
			calsPresentOnStudy="true";
		}
	for(int counter=0; counter<len; counter++){
	//portalModId = ((Integer)portalModIds.get(counter)).intValue();//

	portalModType = (portalModTypes.get(counter))==null?"":(portalModTypes.get(counter)).toString();//

	fkId = (fkIds.get(counter))==null?"":(fkIds.get(counter)).toString();//


	//System.out.println("fkId" + fkId  );
	//portalModAlign = (portalModAligns.get(counter))==null?"":(portalModAligns.get(counter)).toString();//



	portalBgColor = (portalBgColors.get(counter))==null?"":(portalBgColors.get(counter)).toString();
	portalHeader = (portalHeaders.get(counter))==null?"":(portalHeaders.get(counter)).toString();
	portalFooter = (portalFooters.get(counter))==null?"":(portalFooters.get(counter)).toString();


//		The following segment is for the forms
		if(mode.equals("M") && (!fkId.equals("")) && ( (portalModType.equals("LF")) || (portalModType.equals("EF")))){

		portalModFormResp = (portalModFormResps.get(counter))==null?"":(portalModFormResps.get(counter)).toString();

		portalModIdForm= (portalModIds.get(counter))==null?"":portalModIds.get(counter).toString();	// comma septd id
		portalModAlignForm = (portalModAligns.get(counter))==null?"":portalModAligns.get(counter).toString();	//L R C
		portalModTypeForm = (portalModTypes.get(counter))==null?"":portalModTypes.get(counter).toString();	// EF LF
		fkIdForm = (fkIds.get(counter))==null?"":fkIds.get(counter).toString();	// fk_id
		//System.out.println("fkIdForm" + fkIdForm);

		DynRepDao drDao = new DynRepDao();
		String delimiter =",";
		drDao.getFormNames(fkId,delimiter);
		ArrayList frmList = drDao.getFormNameList();
			for(int i=0;i<frmList.size();i++)
		    	strVal = strVal+frmList.get(i).toString() + ",";
				strVal = strVal.substring(0,strVal.length()-1);
		}

//		The following segment is for the Calendars
		if(mode.equals("M") && ( (portalModType.equals("S")) || (portalModType.equals("SF")))){
		portalModIdSchedule = (portalModIds.get(counter))==null?"":portalModIds.get(counter).toString();// comma septd id
		portalModAlignSchedule = (portalModAligns.get(counter))==null?"":portalModAligns.get(counter).toString();	//
		portalModTypeSchedule = (portalModTypes.get(counter))==null?"":portalModTypes.get(counter).toString();	// S SF
		fkIdSchedule = (fkIds.get(counter))==null?"":fkIds.get(counter).toString();	// fk_id
		String resetFlagData = fkIdSchedule;
		
		portalModFrom = (portalModFroms.get(counter))==null?"":(portalModFroms.get(counter)).toString();
		portalModTo = (portalModTos.get(counter))==null?"":(portalModTos.get(counter)).toString();
		portalModFromUnit = (portalModFromUnits.get(counter))==null?"":(portalModFromUnits.get(counter)).toString();
		portalModToUnit = (portalModToUnits.get(counter))==null?"":(portalModToUnits.get(counter)).toString();


		String[] fkIdScheduleArray = fkIdSchedule.split(",");
		//Modified for Bug #7619, #7682  AGodara
		fkIdSchedule="";
		for(String scheduleId:fkIdScheduleArray){
			eventAssocB.setEvent_id(StringUtil.stringToNum(scheduleId));
			if((eventAssocB.getEventAssocDetails())!=null){
				calName = calName +", "+ (eventAssocB.getName());
				fkIdSchedule = fkIdSchedule +","+(eventAssocB.getEvent_id());
			}
		}
		if(!calName.equals("") && !fkIdSchedule.equals("")){
			calName = calName.substring(1);
			fkIdSchedule = fkIdSchedule.substring(1);
		}
		//Modified for Bug #7619, #7682  By- Yogendra
		if(fkIdScheduleArray.length > 0 && calName.equals("") && fkIdSchedule.equals("")){
			if(null != resetFlagData && resetFlagData != ""){
				portalDesignB.setModifiedBy(uName);
				//To reset the ER_PORTAL_MODULES table FK_ID field. When all calendar(s) and form(s)
				// deleted from the study.
				resetFlagForFK_IDs = "reset";
		   	   	//portalDesignB.resetPortalDesignData(EJBUtil.stringToNum(portalId));
			}
		}
	}

}//end of for loop



 // create form after response drop down String

 ddFormAfterResp.append("<SELECT name = \"portalFormAfterResp\" > ");
	if (StringUtil.isEmpty(portalModFormResp))
	{
		portalModFormResp = "";
		//ddFormAfterResp.append("<option value=''>Select an Option</option> ");
	}

  for (int j=0;j<arFormAfterRespOptions.length;j++)
  {
 		ddFormAfterResp.append("<option value='"+ arFormAfterRespOptions[j] + "'" );
 		if (portalModFormResp.equals(arFormAfterRespOptions[j]))
 		{
 			ddFormAfterResp.append(" selected ");
 		}
 		ddFormAfterResp.append(">"+ arFormAfterRespOptionsDesc[j]+"</option>");
  }
 ddFormAfterResp.append("</SELECT>");

 
%>
<DIV class="popDefault" id="div1">
<P class="sectionHeadings"> <%=MC.M_MngPor_Designer%><%-- Manage Portal >> Designer*****--%></P>
<p CLASS="defComments"><b><%=LC.L_Portal_Name%><%-- Portal Name*****--%>: </b> <%= portalName%><p>

<Form name="portalDesign" id="portalDesign_form"  method="post" action="updatePortalDesign.jsp" onsubmit ="if (validate(document.portalDesign)==false) {setValidateFlag('false');return false;} else {setValidateFlag('true'); blockSubmit(); return true;}">
	<input type="hidden" name="mode" value="<%=mode%>">
	<input type="hidden" name="selFormIds" value="<%=fkId%>">
	<input type="hidden" name="portalId" value=<%=portalId%>>
	<input type="hidden" name ="portalModType" value="<%=portalModType%>" >

	<input type="hidden" name ="paramstudyId" value=<%=studyId%> >
	<input type="hidden" name ="studyId" value=<%=studyId%> >
	
	<input type="hidden" name="resetFlagForFK_IDs" value="<%=resetFlagForFK_IDs%>" >
	
	
	
	<input type="hidden" name ="fromFormAccessPage"  >
	
	<input type="hidden" name ="portalformKey" >
	<input type="hidden" name ="hiddenEvtId" >
	<input type="hidden" name ="hiddenFormId"  >
	<input type="hidden" name ="inclChks"  >
	
	<input type="hidden" name ="pfCounter" >
	
	<input type="hidden" name ="calIdVal"  >	
	
	<input type="hidden" name ="clrAll" >
	
 	
	
	
	



	<TABLE WIDTH="100%" CELLSPACING="0" CELLPADDING="0" border="0" class="basetbl">

	<TR>
			<TD WIDTH="10%">&nbsp;</TD>
			<TD WIDTH="10%"><A href='#' onclick ="clearAllSlected()" ><%=MC.M_Reset_FrmAndSch%><%-- Reset Forms and Schedule*****--%></A></TD>
        	</TR>
		<tr>

		<TR>
			<TD WIDTH="10%"><P CLASS="defComments"><B><%=LC.L_Include_PatForms%><%-- Include <%=LC.Pat_Patient%> Forms*****--%></B></P></TD>
			<TD WIDTH="10%">&nbsp;</TD>
        	</TR>
		<tr>
        <%

		if(mode.equals("M") && portalModTypeForm.equals("EF")) {
			editFormSelected = true;
		%>
			<td width="10%">
				<input type="radio" id="oneformrd" name="rdForm" value="EF"  checked OnClick="fnPatForm();"><%=MC.M_OpenFrm_EditMode%><%-- Open Form in Edit Mode*****--%> &nbsp;
			</td>
			<td width="10%"><input type="text" id="oneformedit" name="selForm" value="<%=strVal%>" READONLY >&nbsp;&nbsp;&nbsp;<A href="#"  id="oneformanch" onClick="openFormsLookup(document.portalDesign,'single')"><%=LC.L_Select_Form%><%-- Select Form*****--%></A></td>
		<%} else {%>

        	<td width="10%">
				<input type="radio" name="rdForm" id="oneformrd" value="EF" OnClick="fnPatForm();"><%=MC.M_OpenFrm_EditMode%><%-- Open Form in Edit Mode*****--%> &nbsp;
			</td>
			<td width="10%"><input type="text" id="oneformedit" name="selForm" value="" READONLY >&nbsp;&nbsp;&nbsp;<A href="#" id="oneformanch" onClick="openFormsLookup(document.portalDesign,'single')"><%=LC.L_Select_Form%><%-- Select Form*****--%></A></td>
	<%}%>
		</tr>

		<TR>

		<%
		if(mode.equals("M") && portalModTypeForm.equals("LF")) {

		 linkFormSelected = true;
		%>

			<TD WIDTH="20%"><INPUT TYPE="radio" id="dispformrd" NAME="rdForm" VALUE="LF" checked ONCLICK="fnDispForms();"><%=MC.M_DispFrm_Links%><%-- Display Form(s) as Links*****--%>&nbsp;</TD>
			<TD WIDTH="20%"><INPUT TYPE="text" id="dispformedit" NAME="selForms" VALUE="<%=strVal%>" READONLY>&nbsp;&nbsp;&nbsp;<A href="#" NAME="openForm1" id="dispformanch" ONCLICK="openFormsLookup(document.portalDesign,'multi')"><%=LC.L_Select_Form%><%-- Select Form*****--%></A></TD>
		<%} else {%>
			 <TD WIDTH="20%"><INPUT TYPE="radio" id="dispformrd" NAME="rdForm" VALUE="LF" ONCLICK="fnDispForms();"><%=MC.M_DispFrm_Links%><%-- Display Form(s) as Links*****--%>&nbsp;</TD>
			<TD WIDTH="20%"><INPUT TYPE="text" id="dispformedit" NAME="selForms" VALUE="" READONLY>&nbsp;&nbsp;&nbsp;<A href="#" NAME="openForm1" id="dispformanch" ONCLICK="openFormsLookup(document.portalDesign,'multi')"><%=LC.L_Select_Form%><%-- Select Form*****--%></A></TD>
		<%}
		//paramFormIds will hold the comma separated form ids from the lookup for both EF & LF
		if(mode.equals("M")){
			//System.out.println("fkIdForm - r" + fkIdForm);
		%>
			<Input TYPE="hidden" NAME="paramFormIds" value="<%=fkIdForm%>">
			<Input TYPE="hidden" NAME="pkFormId" value="<%=portalModIdForm%>">
		<%}else{%>
			<Input TYPE="hidden" NAME="paramFormIds" value="">
		<%}%>
		</TR>

		<TR>
			<TD height="10"></TD>
		</TR>


			<TR>
			<TD align="left"><%=LC.L_Align_FormLinks%><%-- Align Form Links*****--%>&nbsp;</TD>
			<TD WIDTH="20%">
				<SELECT NAME="align" id="formalign">
					<% if(portalModAlignForm.equals("")){%>
					<OPTION VALUE='' selected><%=LC.L_Select_AnOption%><%-- Select an Option*****--%></OPTION>
					<%}else {%>
					<OPTION VALUE=''><%=LC.L_Select_AnOption%><%-- Select an Option*****--%></OPTION>
					<%} if(portalModAlignForm.equals("L")) {%>
					<OPTION value="L" selected><%=LC.L_Left%><%-- Left*****--%></OPTION>
					<%}else {%>
					<OPTION value="L"><%=LC.L_Left%><%-- Left*****--%></OPTION>
					<%}if(portalModAlignForm.equals("C")) {%>
					<OPTION value="C" selected><%=LC.L_Center%><%-- Center*****--%></OPTION>
					<%}else {%>
					<OPTION value="C"><%=LC.L_Center%><%-- Center*****--%></OPTION>
					<%}if(portalModAlignForm.equals("R")) {%>
					 <OPTION value="R" selected><%=LC.L_Right%><%-- Right*****--%></OPTION>
					<%}else {%>
					 <OPTION value="R"><%=LC.L_Right%><%-- Right*****--%></OPTION>
					<%}%>
				</SELECT>
			</TD>

			</TR>

			<TR>
			<TD align="left"><%=LC.L_Form_ResponseDisp%><%-- Form Response Display*****--%> &nbsp;</TD>
				<TD WIDTH="20%">
					<%= ddFormAfterResp%>
				</TD>

			</TR>

		<TR>
			<TD colspan=2><font class="Mandatory"><%=MC.M_NotSelFrm_HaveChildLnk%><%-- Please do not select forms that have 'Child Form' links*****--%></font></TD>
		</TR>

		<TR>
			<TD colspan=2><hr/></TD>
		</TR>

		<TR>
			<TD WIDTH="15%"><P CLASS="defComments"><B><%=LC.L_Include_PatSch%><%-- Include <%=LC.Pat_Patient%> Schedule*****--%></B></P></TD>
		</TR>

		<TR>
			<TD>&nbsp;</TD>
		</TR>

		<TR>
		<%

		if(mode.equals("M") && portalModTypeSchedule.equals("S") && !fkIdSchedule.equals("")) {
		%>
			<TD WIDTH="20%"><INPUT TYPE="radio" id="onepatsch" NAME="rdpatSchedule" VALUE="S" checked onclick ="fnPatSchedule();" ><%=LC.L_Pat_SchOnly%><%-- <%=LC.Pat_Patient%> Schedule Only*****--%>&nbsp;</TD>
			<td><DIV id="protcaldataDIV"><Input TYPE="text" id="patschedit" NAME="selCal" SIZE="20" READONLY value="<%=calName%>">&nbsp;&nbsp;&nbsp;<A href="javascript:void(0);" onClick="return openLookup(document.portalDesign,'viewId=6016&form=portalDesign&seperator=,&defaultvalue=&keyword=selCal|name~paramprotCalId|event_id|[VELHIDE]','dfilter=portalDesign1|')"><%= LC.L_Select_Cal%><%-- Select Calendar*****--%></A></DIV></TD>
		<%}else{%>
			<TD WIDTH="20%"><INPUT TYPE="radio" id="onepatsch" NAME="rdpatSchedule" VALUE="S" onclick ="fnPatSchedule();"><%=LC.L_Pat_SchOnly%><%-- <%=LC.Pat_Patient%> Schedule Only*****--%>&nbsp;</TD>
			<td><DIV id="protcaldataDIV"><Input TYPE="text" id="patschedit" NAME="selCal" SIZE="20" READONLY value="">&nbsp;&nbsp;&nbsp;<A href="javascript:void(0);" onClick="return openLookup(document.portalDesign,'viewId=6016&form=portalDesign&seperator=,&defaultvalue=&keyword=selCal|name~paramprotCalId|event_id|[VELHIDE]','dfilter=portalDesign1|')"><%= LC.L_Select_Cal%><%-- Select Calendar*****--%></A></DIV></TD>
		<%} %>
		</TR>

		<TR>
	    <%
	    if(mode.equals("M") && portalModTypeSchedule.equals("SF") && !fkIdSchedule.equals("")) {
	    %>
			<TD WIDTH="20%"><INPUT TYPE="radio" id="linkpatsch" NAME="rdpatSchedule" VALUE="SF" checked onclick ="fnPatScheduleForms();" ><%=MC.M_PatSch_WithFrm%><%-- <%=LC.Pat_Patient%> Schedule with Linked Forms*****--%>&nbsp;</TD>
			<td><DIV id="protcaldataDIV"><Input TYPE="text" id="linkpatschedit" NAME="selCalWf" SIZE="20" READONLY value="<%=calName%>">&nbsp;&nbsp;&nbsp;<A href="javascript:void(0);" onClick="return openLookup(document.portalDesign,'viewId=6016&form=portalDesign&seperator=,&defaultvalue=&keyword=selCalWf|name~paramprotCalId|event_id|[VELHIDE]','dfilter=portalDesign1|')"><%= LC.L_Select_Cal%><%-- Select Calendar*****--%></A></DIV></TD>
		<%}else{%>
			<TD WIDTH="20%"><INPUT TYPE="radio" id="linkpatsch" NAME="rdpatSchedule" VALUE="SF" onclick ="fnPatScheduleForms();"><%=MC.M_PatSch_WithFrm%><%-- <%=LC.Pat_Patient%> Schedule with Linked Forms*****--%>&nbsp;</TD>
			<td><DIV id="protcaldataDIV"><Input TYPE="text" id="linkpatschedit" NAME="selCalWf" SIZE="20" READONLY value="">&nbsp;&nbsp;&nbsp;<A href="javascript:void(0);" onClick="return openLookup(document.portalDesign,'viewId=6016&form=portalDesign&seperator=,&defaultvalue=&keyword=selCalWf|name~paramprotCalId|event_id|[VELHIDE]','dfilter=portalDesign1|')"><%= LC.L_Select_Cal%><%-- Select Calendar*****--%></A></DIV></TD>
		<%} %>

		<% if(mode.equals("M")) {%>
			<Input TYPE="hidden" NAME="paramprotCalId" value="<%=fkIdSchedule%>">
			<Input TYPE="hidden" NAME="pkCalId" value="<%=portalModIdSchedule%>">
			<Input TYPE="hidden" NAME="paramprotCalIdOld" value="<%=fkIdSchedule%>">
			<Input TYPE="hidden" NAME=calsPresentOnStudy value="<%=calsPresentOnStudy%>">			
		<%}else{ %>
			<Input TYPE="hidden" NAME="paramprotCalId" value="">
		<%}%>

               <Input TYPE="hidden" NAME="patSchelinkFormCalId" value="">

        </TR>
		<TR>
			<TD height="10"></TD>
		</TR>
		<TR>
			<TD height="10"></TD><TD height="10" ><A href=# onclick=callfunformaccesspg(document.portalDesign)> <%= LC.L_Form_Access%><%-- Form Access*****--%></A></TD>
		</TR>
		
		<TR>
			<TD height="10"></TD>
		</TR>

		<TR>

			<TD align="left"><%=LC.L_Align_Schedule%><%-- Align Schedule*****--%>&nbsp;</TD>
			<TD WIDTH="25%">
				<SELECT NAME="status" id="schalign">
						<% if(portalModAlignSchedule.equals("")){%>
						<OPTION VALUE='' selected><%=LC.L_Select_AnOption%><%-- Select an Option*****--%></OPTION>
						<%}else {%>
						<OPTION VALUE=''><%=LC.L_Select_AnOption%><%-- Select an Option*****--%></OPTION>
						<%}if(portalModAlignSchedule.equals("L")) {%>
						<OPTION value="L" selected><%=LC.L_Left%><%-- Left*****--%></OPTION>
						<%}else {%>
						<OPTION value="L"><%=LC.L_Left%><%-- Left*****--%></OPTION>
						<%}if(portalModAlignSchedule.equals("C")) {%>
						<OPTION value="C" selected><%=LC.L_Center%><%-- Center*****--%></OPTION>
						<%}else {%>
						<OPTION value="C"><%=LC.L_Center%><%-- Center*****--%></OPTION>
						<%}if(portalModAlignSchedule.equals("R")) {%>
						 <OPTION value="R" selected><%=LC.L_Right%><%-- Right*****--%></OPTION>
						<%}else {%>
						 <OPTION value="R"><%=LC.L_Right%><%-- Right*****--%></OPTION>
						<%}%>
				</SELECT>
			</TD>


        </TR>



	<TR>
		<TD WIDTH="20%"><br><%=MC.M_CalSelAbv_AplyFwgWin%>:<br></TD>
		</TR>

	<TR>

		<% if (mode.equals("N") ){%>
			<TD WIDTH="20%"><INPUT TYPE="radio" NAME="rdDates" VALUE="S" CHECKED onclick="fnClear();" >&nbsp;&nbsp; <%=LC.L_All_Dates%><%-- All Dates*****--%></TD>
		<% } else if (mode.equals("M") && (portalModFrom.equals("0")|| portalModFrom.equals("") )&& (portalModTo.equals("0")|| portalModTo.equals(""))){%>
		<TD WIDTH="20%"><INPUT TYPE="radio" id= "rdDates_all" NAME="rdDates" VALUE="S" CHECKED onclick="fnClear();" > <%=LC.L_All_Dates%><%-- All Dates*****--%></TD>
		<%} else {%>
		<TD WIDTH="20%"><INPUT TYPE="radio" id= "rdDates_all" NAME="rdDates" VALUE="S" onclick="fnClear();" ><%=LC.L_All_Dates%><%-- All Dates*****--%></TD>
		<%} %>

    </TR>

	<TR>

		<% if  (mode.equals("M")&& (!portalModFrom.equals("0") || !portalModTo.equals("0")) && (!portalModFrom.equals("") || !portalModTo.equals(""))){%>
			<TD><INPUT TYPE="radio" NAME="rdDates" id= "rdDates_selected" VALUE="O" CHECKED ><%=LC.L_Dates%><%-- Dates*****--%>
		<%}else{%>
			<TD><INPUT TYPE="radio" NAME="rdDates" id= "rdDates_selected" VALUE="O" ><%=LC.L_Dates%><%-- Dates*****--%>
		<%} %>
		<% if(mode.equals("M")){ %>
			&nbsp;&nbsp; <INPUT TYPE="text" id = "pmFromId" NAME="pmFrom" SIZE="3" maxlength="3" VALUE="<%=portalModFrom%>">
		<%} else { %>
			&nbsp;&nbsp; <INPUT TYPE="text" id = "pmFromId" NAME="pmFrom" SIZE="3" maxlength="3" VALUE="">
		<%}%>

		 <SELECT NAME="pmFromUnit" id="pmFromUnitId">
			<% if(portalModFromUnit.equals("")){%>
			<OPTION VALUE="" SELECTED> <%=LC.L_Select_AnOption%><%-- Select an Option*****--%></OPTION>
			<% }if(portalModFromUnit.equals("D")){%>
			<OPTION VALUE="D" selected><%=LC.L_Days%><%-- Days*****--%></OPTION>
			<%}else{ %>
			<OPTION VALUE="D"><%=LC.L_Days%><%-- Days*****--%></OPTION>
			<%}if(portalModFromUnit.equals("W")){%>
			<OPTION VALUE="W" selected><%=LC.L_Weeks%><%-- Weeks*****--%></OPTION>
			<%}else{ %>
			<OPTION VALUE="W"> <%-- *****--%><%=LC.L_Weeks%><%-- Weeks*****--%></OPTION>
			<%}if(portalModFromUnit.equals("M")){%>
			<OPTION VALUE="M" selected><%=LC.L_Months%><%-- Months*****--%></OPTION>
			<%}else{ %>
			<OPTION VALUE="M"><%=LC.L_Months%><%-- Months*****--%></OPTION>
			<%}%>

		</SELECT>&nbsp;<%=LC.L_Before_And%><%-- before and*****--%> &nbsp;
  		<% if(mode.equals("M")){ %>
        	 <INPUT TYPE="text" ID="pmToId" NAME="pmTo" SIZE="3" maxlength="3" VALUE="<%=portalModTo%>">
        <%} else { %>
        	 <INPUT TYPE="text" ID="pmToId"  NAME="pmTo" SIZE="3" maxlength="3" VALUE="">
        <%}%>
		 <SELECT NAME="pmToUnit" id= "pmToUnitId">
			<% if(portalModToUnit.equals("")) {%>
			<OPTION VALUE="" SELECTED> <%=LC.L_Select_AnOption%><%-- Select an Option*****--%></OPTION>
			<%}if(portalModToUnit.equals("D")) {%>
			<OPTION VALUE="D" selected><%=LC.L_Days%><%-- Days*****--%></OPTION>
			<%}else{ %>
			<OPTION VALUE="D"><%=LC.L_Days%><%-- Days*****--%></OPTION>
			<%}if(portalModToUnit.equals("W")) {%>
			<OPTION VALUE="W" selected><%=LC.L_Weeks%><%-- Weeks*****--%></OPTION>
			<%}else{ %>
			<OPTION VALUE="W"> <%=LC.L_Weeks%><%-- Weeks*****--%></OPTION>
			<%}if(portalModToUnit.equals("M")) {%>
			<OPTION VALUE="M" selected><%=LC.L_Months%><%-- Months*****--%></OPTION>
			<%}else{ %>
			<OPTION VALUE="M"><%=LC.L_Months%><%-- Months*****--%></OPTION>
			<%}%>
		</SELECT>&nbsp;<%=LC.L_After_CurDate%><%-- After Current Date*****--%></TD>
	</TR>


<tr>
<td><%=MC.M_PortalReqLayer_SelFrm%><%-- If this Portal requires an eConsenting layer, Select Form*****--%>:</td>
<td>
	<INPUT TYPE="text"  NAME="selConsentForm" VALUE="<%=portalConsentingFormName%>" READONLY>&nbsp;&nbsp;&nbsp;
	<A href="#"  ONCLICK="openFormsLookup(document.portalDesign,'cons')"><%=LC.L_Select_Form%><%-- Select Form*****--%></A>

	<INPUT TYPE="hidden"  NAME="selConsentFormId" VALUE="<%=portalConsentingForm%>" >
</td>
</tr>

		<tr>
			<td width="16%" colspan=3><br><br><input type="checkbox" name="selfLogout" value="1" <%= checkedSelfLogout %> >&nbsp;&nbsp;
				<%=MC.M_AutoLgutPat_AfterFrmSubm%><%-- Automatically Logout <%=LC.Pat_Patient%> after Form Submission*****--%>
			</td></tr>
</table>

<br>

<TABLE WIDTH="100%" CELLSPACING="0" CELLPADDING="0" border="0" class="basetbl">

	<TR>
		<TD colspan="2"><P CLASS="defComments"><B><%=LC.L_Page_Format%><%-- Page Format*****--%></B></P></TD>
	</TR>

	<TR>
		<TD colspan="2" height="10"></TD>
	</TR>

	<TR>
		<TD width="10%"> <%=LC.L_Page_BackgroundColor%><%-- Page Background Color*****--%> </TD>

		<TD width="25%" align="left">

			<INPUT TYPE="text" NAME="designBgColor" size =10 maxlength="6" VALUE="<%=portalBgColor%>">
			(<%=MC.M_EtrHtmlWout_Symbol%><%-- enter HTML color code without the symbol '#'*****--%>)
		</TD>

	</TR>

	<TR>
		<TD colspan="2" height="10"></TD>
	</TR>

	<TR>
		<TD colspan="2"> <%=MC.M_HeadInfo_HtmlDispPg%><%-- Header Info (HTML tags, content etc, to be displayed at the top of the page)*****--%> </TD>
	</TR>

	<TR>
		<TD colspan="2" height="10"></TD>
	</TR>
    	<% if(mode.equals("M")) {%>
	<TR>
		<TD colspan="2"><TEXTAREA NAME="designHeaderInfo" ROWS="2" COLS="50" VALUE="" MAXLENGTH="4000"><%=portalHeader%></TEXTAREA></TD>
	</TR>
        <%} else { %>
        <TR>
		<TD colspan="2"><TEXTAREA NAME="designHeaderInfo" ROWS="2" COLS="50" VALUE="" MAXLENGTH="4000"></TEXTAREA></TD>
	</TR>

        <%}%>
	<TR>
		<TD colspan="2" height="10"></TD>
	</TR>

	<TR>
		<TD colspan="2"><%=MC.M_FootInfo_HtmlDispPg%><%-- Footer Info (HTML tags, content etc, to be displayed at the bottom of the page)*****--%></TD>
	</TR>

	<TR>
		<TD colspan="2" height="10"></TD>
	</TR>
    	<% if(mode.equals("M")) {%>
	<TR>
		<TD colspan="2"><TEXTAREA NAME="designFooterInfo" ROWS="2" COLS="50" VALUE="" MAXLENGTH="4000"><%=portalFooter%></TEXTAREA></TD>
	</TR>
	<%} else { %>
	<TR>
		<TD colspan="2"><TEXTAREA NAME="designFooterInfo" ROWS="2" COLS="50" VALUE="" MAXLENGTH="4000"></TEXTAREA></TD>
	</TR>

	<%}%>
</TABLE>

<p class="defComments"><%=MC.M_Note_CrfLnk_RespDisp%><%-- Please note: For CRF forms linked with the <%=LC.Pat_Patient_Lower%> schedule, 'Form Response Display' option 'Not available for <%=LC.Pat_Patient%>' will be applied.*****--%> </p>
<TABLE WIDTH="100%" CELLSPACING="0" CELLPADDING="0">

 <% if (pageRight>=6) {%>
<jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="copyverfrm"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>
<%} %>
	<input type="hidden" value="<%=accId%>" name= "accId" />
	<input type="hidden" value="<%=userId%>" name= "userId" />
	<input type="hidden" value="" name= "dfilter" />


</FORM>

<%
		if (editFormSelected )
		{
			%>
				<script>
					fnDisableEnable('E');
				</script>
			<%

		}


	}//end of if body for session
else {
%>
	<jsp:include page="timeout.html" flush="true"/>
<% } %>

<div class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</DIV>


</BODY>

</HTML>


