<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<%@page import="com.velos.eres.service.util.*"%>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<Link Rel=STYLESHEET HREF="common.css" type=text/css>

</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
  <jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

  <%@ page language = "java" import = "com.velos.eres.business.study.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%>
  <%

	HttpSession tSession = request.getSession(true); 

	int ret = 0;

	int eventId=0;
	String name ="";
	String description ="";
	String eventType="";
	String accId="";
	String src = null;
	String mode="";
	String catMode="";
	String msg="";
	String catId=request.getParameter("catId");
	String catName=request.getParameter("catName");
	String cost = request.getParameter("cost");
	String calStatus=request.getParameter("calStatus");
	String duration= request.getParameter("duration");
	String protocolId = request.getParameter("protocolId");
	String calledFrom = request.getParameter("calledFrom");
	
	String eSign = request.getParameter("eSign");

    if (sessionmaint.isValidSession(tSession)) {
%>
<%	
	String oldESign = (String) tSession.getValue("eSign");


	if(!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} else {
	String ipAdd = (String) tSession.getValue("ipAdd");
	String usr = (String) tSession.getValue("userId");

	accId = (String) tSession.getValue("accountId");

	src = request.getParameter("srcmenu");

	eventType = "L";

	mode = request.getParameter("mode");
	catMode = request.getParameter("catMode");

    String[] categoryArray= new String[10];
    String[] categoryDesc = new String[10];
    String[] categorysave= new String[10] ;
    String[] categorydescsave=new String[10];
    int j=0;
 	categoryArray = request.getParameterValues("categoryName");
 	categoryDesc= request.getParameterValues("desc");
 	
 	   for (int i=0;i<categoryArray.length;i++)
     {  
     	 if (categoryArray[i].length()>0){
     	 	 categorysave[j] = categoryArray[i] ;
     	 	 categorydescsave[j]=categoryDesc[i];
    
     	 	 j++ ;
     	
       }
      	
      }
    
   for (int i=0;i<categorysave.length;i++)
   {
   	name = categorysave[i];
   	description = categorydescsave[i];  
   	if ((name=="") || (name==null) ) {
   	}
   	
   	else {

	eventdefB.setName(name); 
	eventdefB.setDescription(description);
	eventdefB.setEvent_type(eventType);
	eventdefB.setUser_id(accId); 
	
	if (catMode.equals("M")){	

	   eventdefB.setEvent_id(EJBUtil.stringToNum(request.getParameter("categoryId")));

	   eventdefB.setModifiedBy(usr);
	   eventdefB.setIpAdd(ipAdd);

	   ret = eventdefB.updateEventdef();


	}else{

		eventdefB.setCreator(usr); 
		eventdefB.setIpAdd(ipAdd);

		eventdefB.setEventdefDetails();
		ret = eventdefB.getEvent_id();
		//out.print("return" +ret);
	
  } } 
  %> 
  
<%  	if (ret == -1){Object[] arguments1 = {name};%>
			<br><br><br><br><br><p class = "successfulmsg" align = center>
			 <%=VelosResourceBundle.getMessageString("M_CategNameDupl_ClickBack",arguments1)%><%--Category Name "<%=name%> "  is duplicate. Click on "Back" Button to change the name***** --%> <Br><Br>
			<button name="back" onclick="window.history.back();"><%=LC.L_Back%></button></p>
		<% return;} 
		} //end for for loop
		%>
 		
<br>
<br>
<br>
<br>
<br>

<p class = "successfulmsg" align = center> <%=msg%> </p>
<%if (calledFrom.equals("L")){%>
	<META HTTP-EQUIV=Refresh CONTENT="0; URL=selecteventus.jsp?catId=<%=catId%>&catName=<%=catName%>&cost=<%=cost%>&mode=<%=mode%>&calStatus=<%=calStatus%>&srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>"> 	
<%}else{ %>
  
   <jsp:forward page="/jsp/selecteventus.jsp" ></jsp:forward>
   
<%  }
}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>

</HTML>





