<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML> 

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:include page="include.jsp" flush="true"/>	
<jsp:useBean id="submB" scope="request" class="com.velos.eres.web.submission.SubmissionJB"/>
<jsp:useBean id="submBoardB" scope="request" class="com.velos.eres.web.submission.SubmissionBoardJB"/>
<jsp:useBean id="submStatusB" scope="request" class="com.velos.eres.web.submission.SubmissionStatusJB"/>

<%@ page language = "java" import = "com.velos.eres.business.study.*,com.velos.eres.business.common.*,java.util.*,java.text.*,com.velos.eres.service.util.*,com.velos.eres.web.account.AccountJB,com.velos.esch.business.common.*"%>

<%
  HttpSession tSession = request.getSession(true); 
  String accId = (String) tSession.getValue("accountId");
  int iaccId = EJBUtil.stringToNum(accId);
  String studyId = (String) tSession.getValue("studyId");
  String grpId = (String) tSession.getValue("defUserGroup");
  String ipAdd = (String) tSession.getValue("ipAdd");
  String usr = (String) tSession.getValue("userId");
  int iusr = EJBUtil.stringToNum(usr);
  
  String src= StringUtil.htmlEncodeXss(request.getParameter("srcmenu"));
  String selectedTab = StringUtil.htmlEncodeXss(request.getParameter("selectedTab"));
  String eSign = request.getParameter("eSign");
  if (sessionmaint.isValidSession(tSession))
  {	
	
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%   
    String oldESign = (String) tSession.getValue("eSign");
	if(!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} else {
	    // Create a new submission bean
        submB.setStudy(studyId);
	    submB.setIpAdd(ipAdd);
	    submB.setCreator(usr);
	    submB.setLastModifiedBy(usr);
	    int newSubmId = submB.createSubmission();
	    String resultMsg = null;
        if( newSubmId > 0 ) {
            resultMsg = MC.M_Data_SvdSucc/*"Data was saved successfully"*****/;
        } else {
            resultMsg = MC.M_Err_SavingData/*"There was an error while saving data"*****/;
%>
  <br><br><br><br>
  <p class = "successfulmsg" align = center><%=resultMsg%></p>
  <META HTTP-EQUIV=Refresh CONTENT="3; URL=irbnewcheck.jsp?mode=M&selectedTab=<%=selectedTab%>&studyId=<%=studyId%>">
</BODY>
</HTML>
<%          
            return; // An error occurred; let's get out of here
        }

        // Create a new submission status bean for submission
        EIRBDao eIrbDao = new EIRBDao();
        eIrbDao.getCurrentOverallStatus(EJBUtil.stringToNum(studyId), newSubmId);
        String currentOverallStatus = eIrbDao.getCurrentOverallStatus();
        // If the overall status does not already exist for this study, create a new one
        if (currentOverallStatus == null || currentOverallStatus.length() == 0) {
            submStatusB.setFkSubmission(String.valueOf(newSubmId));
            submStatusB.setFkSubmissionBoard(null); // For overall status, this is null
            //submStatusB.setSubmissionEnteredBy(usr);
            submStatusB.setCreator(usr);
            submStatusB.setLastModifiedBy(usr);
            submStatusB.setIpAdd(ipAdd);
            submStatusB.setSubmissionStatusDate(null);
            submStatusB.setSubmissionAssignedTo(null);
            submStatusB.setSubmissionCompletedBy(null);
            submStatusB.setSubmissionNotes(null);
         //   submStatusB.setSubmissionStatusBySubtype("submitted");

            int newSubmStatusId = submStatusB.createSubmissionStatus();
            if( newSubmStatusId < 1 ) {
                resultMsg = MC.M_Err_SavingData/*"There was an error while saving data"*****/;
%>
  <br><br><br><br>
  <p class = "successfulmsg" align = center><%=resultMsg%></p>
  <META HTTP-EQUIV=Refresh CONTENT="3; URL=irbnewcheck.jsp?mode=M&selectedTab=<%=selectedTab%>&studyId=<%=studyId%>">
</BODY>
</HTML>
<%          
                return; // An error occurred; let's get out of here
            }
        }
        
	    // Figure out which board was selected
        eIrbDao.getReviewBoards(EJBUtil.stringToNum(accId),EJBUtil.stringToNum(grpId));
        ArrayList boardNameList = eIrbDao.getBoardNameList();
        ArrayList boardIdList = eIrbDao.getBoardIdList();
	    for (int iBoard=0; iBoard<boardIdList.size(); iBoard++) {
	        if (!"on".equals(request.getParameter("submitTo"+iBoard))) { continue; }

	        // Create a new submission_board bean for each board selected
		    submBoardB.setFkSubmission(String.valueOf(newSubmId));
		    submBoardB.setFkReviewBoard((String) boardIdList.get(iBoard));
		    submBoardB.setCreator(usr);
		    submBoardB.setLastModifiedBy(usr);
		    submBoardB.setIpAdd(ipAdd);
		    submBoardB.setFkReviewMeeting(null);
		    submBoardB.setSubmissionReviewer(null);
		    submBoardB.setSubmissionReviewType(null);
		    
		    Hashtable ht = submBoardB.createSubmissionBoard();
		    
		    int newSubmBoardId = ((Integer)ht.get("id")).intValue(); 
		    if (newSubmBoardId < 1) {
		        resultMsg = MC.M_Err_SavingData/*"There was an error while saving data"*****/;
		        break;
		    }
		    
		    // Create a new submission status bean for this submission_board
            submStatusB.setFkSubmission(String.valueOf(newSubmId));
            submStatusB.setFkSubmissionBoard(String.valueOf(newSubmBoardId));
            submStatusB.setSubmissionEnteredBy(usr);
            submStatusB.setCreator(usr);
            submStatusB.setLastModifiedBy(usr);
            submStatusB.setIpAdd(ipAdd);
            submStatusB.setSubmissionStatusDate(null);
            submStatusB.setSubmissionAssignedTo(null);
            submStatusB.setSubmissionCompletedBy(null);
            submStatusB.setSubmissionNotes(null);
		    if (ht.containsKey("previouslySubmitted")) {
		        submStatusB.setSubmissionStatusBySubtype("resubmitted");
		    } else {
		        submStatusB.setSubmissionStatusBySubtype("submitted");
		    }
            
            int newSubmStatusIdForBoard = submStatusB.createSubmissionStatus();
            if( newSubmStatusIdForBoard < 1 ) {
                resultMsg = MC.M_Err_SavingData/*"There was an error while saving data"*****/;
                break;
            }
	    } // End of board loop
        
%>
  <br><br><br><br>
  <p class = "successfulmsg" align = center><%=resultMsg%></p>
  <META HTTP-EQUIV=Refresh CONTENT="1; URL=irbnewcheck.jsp?mode=M&selectedTab=<%=selectedTab%>&studyId=<%=studyId%>&autoPopulate=Y">
<%        
    }// end of if body for e-sign
  } //end of if body for valid session
  else
  {
  %>
  <jsp:include page="timeout.html" flush="true"/>
  <%
  }
%>

</BODY>
</HTML>
