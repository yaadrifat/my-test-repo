<%--

	Project Name:		Velos eResearch
	Author:				Jnanamay Majumdar
	Created on Date:	29Oct2007
	Purpose:			Gui for Specimen Update Status
	File Name:			editmultiplespecimenstatus.jsp

--%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title> <%=MC.M_Updt_MultiSpmenStat%><%--Update Multiple Specimen Status*****--%></title>


<%@ page import ="java.util.*, java.io.*, org.w3c.dom.*,com.velos.eres.business.common.*"%>
<meta http-equiv="Content-type" content="text/html; charset=UTF-8">
</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>

<script Language="javascript">

//JM: 20JAN2010: #4616
function RLtrim(paseedPar) {
	return paseedPar.replace(/^\s+|\s+$/g,"");
}

function openLookup(formobj, name, cntr) {


	formobj.target="Lookup";
	formobj.method="post";


	if(name=='statUser'){


	formobj.action="multilookup.jsp?viewId=6000&form=specimenstat&seperator=,"+
		"&keyword=statUserFName"+cntr+"|USRFNAME|[VELHIDE]~statUserLName"+cntr+"|USRLNAME|[VELHIDE]~specCreatorId"+cntr+"|USRPK|[VELHIDE]~specCreatedBy"+cntr+"|[VELEXPR]*[VELKEYWORD=USRFNAME]*[VELSTR=]*[VELSPACE]*[VELKEYWORD=USRLNAME]&maxselect=1";

	}
if(name=='receipient'){


	formobj.action="multilookup.jsp?viewId=6000&form=specimenstat&seperator=,"+
		"&keyword=userFName"+cntr+"|USRFNAME|[VELHIDE]~userLName"+cntr+"|USRLNAME|[VELHIDE]~creatorId"+cntr+"|USRPK|[VELHIDE]~createdBy"+cntr+"|[VELEXPR]*[VELKEYWORD=USRFNAME]*[VELSTR=]*[VELSPACE]*[VELKEYWORD=USRLNAME]&maxselect=1";

	}


formWin =open('donotdelete.html','Lookup','resizable=1,status=0, width=850,height=550 top=100,left=100,menubar=no,scrollbars=1');

	if (formWin && !formWin.closed) formWin.focus();
	formobj.submit();
	formobj.target="_self";
	formobj.action="editmultiplespecimenstatussave.jsp";
	void(0);
}




function validateme(formObj){
 	counter = 0;
 	totRows = formObj.cntNumrows.value;



 	for(cnt = 1; cnt<=totRows; cnt++){


 	specStat = "formObj.specStat" + cnt;
	specStats=eval(specStat).value;


	statDt = "formObj.statDt" + cnt;
	statDts=eval(statDt).value;

	if ((specStats != '' && statDts =='') || (specStats == '' && statDts !='')){
    alert("<%=MC.M_Etr_MandantoryFlds%>");/*alert("Please enter data in all mandatory fields");*****/
    return false;
    }


    if (specStats =='' && statDts ==''){
        counter++;

	    if (counter == totRows){
	        alert("<%=MC.M_AtleastOne_SpmenStat%>");/*alert("Please enter atleast one Specimen Status");*****/
	    	return false;
	    }

	}

	quantity = "formObj.quantity" + cnt;
	quantitys=eval(quantity).value;


	//Bug #4223
	specQuantityUnit = "formObj.specQuantityUnit" + cnt;
	if (eval(specQuantityUnit).disabled){
		specQuantityUnit = "formObj.specQtyUnitEdit" + cnt;
	}else{
		eval("formObj.specQtyUnitEdit" + cnt).value = eval(specQuantityUnit).value;
	}
	specQuantityUnits=eval(specQuantityUnit).value;

	if(isNaN(quantitys) == true) {
		alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid number");*****/
		return false;
	}


 	if (quantitys != '' && (specQuantityUnits ==''||specQuantityUnits =='null')){
	    alert("<%=MC.M_Selc_SpmenUnit%>");/*alert("please select the Specimen Unit");*****/
	    return false;
    }


    if (RLtrim(quantitys) == '' && specQuantityUnits !='' && specQuantityUnits !='null' && (specStats != '' && statDts !='')){
	    alert("<%=MC.M_Etr_Qty%>");/*alert("please enter the Quantity");*****/
	    return false;
    }



    if ((quantitys !='' && specQuantityUnits !='') && ((specStats == '' && statDts =='') || (specStats != '' && statDts =='') || (specStats == '' && statDts !=''))){
	    alert("<%=MC.M_Etr_MandantoryFlds%>");/*alert("Please enter data in all mandatory fields");*****/
	    return false;
    }


//JM: 04Sep2009: #4204
	if (quantitys < 0 ){
	    alert("<%=MC.M_PositiveNum_ForQty%>");/*alert("please enter a positive number for the Quantity");*****/
	    return false;
    }

	statHH = "formObj.statusHH"+cnt;
	hrChk = eval(statHH).value;
	if (!validateDataRange(hrChk,'>=',0,'and','<',24) || ! isInteger(hrChk) )
	{
	   alert("<%=MC.M_Vldt_NumAsPositive%>");/*alert("Please enter a positive number below 24");*****/
		eval("formObj.statusHH"+cnt+".focus()");
		return false;
	}

	statMM = "formObj.statusMM"+cnt;
	minChk = eval(statMM).value;
	if (!validateDataRange(minChk,'>=',0,'and','<',60) || ! isInteger(minChk))
	{
	   alert("<%=MC.M_PlsEtrPtv_NumBlw60%>");/*alert("Please enter a positive number below 60");*****/
		eval("formObj.statusMM"+cnt+".focus()");
		return false;
	}

	statSS = "formObj.statusSS"+cnt;
	secChk = eval(statSS).value;

	if (!validateDataRange(secChk,'>=',0,'and','<',60) || ! isInteger(secChk) )
	{
		alert("<%=MC.M_PlsEtrPtv_NumBlw60%>");/*alert("Please enter a positive number below 60");*****/
		eval("formObj.statusSS"+cnt+".focus()");
		return false;
	}


 }


 if(!(validate_col('e-Signature',formObj.eSign))) return false
	if (isNaN(formObj.eSign.value) == true){
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formObj.eSign.focus();
		return false;
	}

if (quantitys!='' && specQuantityUnits!='' && specQuantityUnits !='null')
	alert("<%=MC.M_StatQtyCurAmt_ClkReCalcu%>");/*alert("Status Quantity selected may affect the Current Amount of individual specimen. To see the latest Current Amount please click on the 'Re-Calculate' link in the Specimen details page.");*****/

}

//KM-#INVP2.10
function updateAll(formobj)
{
	totrows = formobj.cntNumrows.value;

	var idx = formobj.specStatTop.options.selectedIndex ;
	var status = formobj.specStatTop.options[idx].value;

	//Commented as per Bug #4223 Units should not be editable and are pre-populated
    //var idxUnit = formobj.specQuantityUnitTop.options.selectedIndex ;
	//var quantityUnit = formobj.specQuantityUnitTop.options[idxUnit].value;


    dtVal=formobj.statDtTop.value;
	specCreatorIdVal = formobj.specCreatorIdTop.value;
	specCreatedByVal = formobj.specCreatedByTop.value;
	statUserLNameVal = formobj.statUserLNameTop.value;
	statUserFNameVal = formobj.statUserFNameTop.value;

	quantityVal = formobj.quantityTop.value;

	creatorIdVal = formobj.creatorIdTop.value;
	createdByVal = formobj.createdByTop.value;
	userLNameVal = formobj.userLNameTop.value;
	userFNameVal = formobj.userFNameTop.value;

	trackNumVal = formobj.trackNumTop.value;
	statusNotes = formobj.statusNotesTop.value;

	statusHHVal = formobj.statusHHTop.value;
	statusMMVal = formobj.statusMMTop.value;
	statusSSVal = formobj.statusSSTop.value;


	if (!validateDataRange(statusHHVal,'>=',0,'and','<',24) || ! isInteger(statusHHVal) )
	{
	   alert("<%=MC.M_Vldt_NumAsPositive%>");/*alert("Please enter a positive number below 24");*****/
		formobj.statusHHTop.focus();
		return false;
	}

	if (!validateDataRange(statusMMVal,'>=',0,'and','<',60) || ! isInteger(statusMMVal) )
	{
		alert("<%=MC.M_PlsEtrPtv_NumBlw60%>");/*alert("Please enter a positive number below 60");*****/

		formobj.statusMMTop.focus();
		return false;
	}

	if (!validateDataRange(statusSSVal,'>=',0,'and','<',60) || ! isInteger(statusSSVal) )
	{
		alert("<%=MC.M_PlsEtrPtv_NumBlw60%>");/*alert("Please enter a positive number below 60");*****/
		formobj.statusSSTop.focus();
		return false;
	}


	for(i=1;i<=totrows;i++) {
		specStat = "formobj.specStat" + i;
		eval(specStat).value = status;

		//Commented as per Bug #4223 Units should not be editable and are pre-populated
		//specQuantity = "formobj.specQuantityUnit"+i;
		//eval(specQuantity).value = quantityUnit;


		statDt = "formobj.statDt"+i;
		eval(statDt).value = dtVal;


		specCreatorId = "formobj.specCreatorId"+i;
		eval(specCreatorId).value = specCreatorIdVal;
		specCreatedBy = "formobj.specCreatedBy"+i;
		eval(specCreatedBy).value = specCreatedByVal;
		statUserLName = "formobj.statUserLName"+i;
		eval(statUserLName).value = statUserLNameVal;
		statUserFName = "formobj.statUserFName"+i;
		eval(statUserFName).value = statUserFNameVal;

		quantity = "formobj.quantity"+i;
		eval(quantity).value = quantityVal;

		creatorId = "formobj.creatorId"+i;
		eval(creatorId).value = creatorIdVal;
		createdBy = "formobj.createdBy"+i;
		eval(createdBy).value = createdByVal;
		userLName = "formobj.userLName"+i;
		eval(userLName).value = userLNameVal;
		userFName = "formobj.userFName"+i;
		eval(userFName).value = userFNameVal;

		trackNum = "formobj.trackNum"+i;
		eval(trackNum).value = trackNumVal;
		eval("formobj.statusNotes"+i).value  = statusNotes;

		statusH = "formobj.statusHH"+i;
		eval(statusH).value = statusHHVal;
		statusM = "formobj.statusMM"+i;
		eval(statusM).value = statusMMVal;
		statusS = "formobj.statusSS"+i;
		eval(statusS).value = statusSSVal;

	}

}

function setCurrentTime(formobj, turn){

  var currentTime = new Date();
  var hours = currentTime.getHours();
  var minutes = currentTime.getMinutes();
  var seconds = currentTime.getSeconds();
  if (turn == '0'){
	  formobj.statusHHTop.value = hours;
	  formobj.statusMMTop.value = minutes;
	  formobj.statusSSTop.value = seconds;
  }else{
  	 statusHHStr = "formobj.statusHH"+turn;
  	 eval(statusHHStr).value = hours;

  	 statusMMStr = "formobj.statusMM" + turn;
  	 eval(statusMMStr).value = minutes;

  	 statusSSStr = "formobj.statusSS" + turn;
  	 eval(statusSSStr).value = seconds;

  }

}



</script>

<%@ page language = "java" import="java.util.*,com.velos.eres.service.util.StringUtil"%>
<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*, com.velos.eres.web.user.UserJB"%>

<jsp:useBean id="sess" scope="request" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="specJB" scope="request" class="com.velos.eres.web.specimen.SpecimenJB"/>

<jsp:include page="include.jsp" flush="true"/>


<BODY>
<%
HttpSession tSession = request.getSession(true);
if (sess.isValidSession(tSession)){
	String defUserGroup = (String) tSession.getAttribute("defUserGroup");

	String userIdFromSession = (String) tSession.getValue("userId");
	String accId = (String) tSession.getValue("accountId");

	String selStrs = request.getParameter("selStrs");
 	if (selStrs==null) selStrs="";



	String mode= "";
	mode = request.getParameter("mode");
	if (EJBUtil.isEmpty(mode))
		mode="N";


	//create the status drop down but inside the loop below
	CodeDao cdSpecStat = new CodeDao();
	cdSpecStat.getCodeValues("specimen_stat");
	cdSpecStat.setCType("specimen_stat");
	cdSpecStat.setForGroup(defUserGroup);


	String specStat ="";


	//create the Specimen Type drop down but inside the loop
	CodeDao cdSpecTyp = new CodeDao();
	cdSpecTyp.getCodeValues("specimen_type");
	String specType = "";


	//Create the Specimen Quantity Unit drop down but inside the loop
	CodeDao cdSpecQuantUnit = new CodeDao();
	cdSpecQuantUnit.getCodeValues("spec_q_unit");
	String specQuantityUnit	="";

	String  ownr = "";






	String mainsql = "";
	String countsql = "";

	StringBuffer sbSelect = new StringBuffer();
	StringBuffer sbFrom = new StringBuffer();
	StringBuffer sbWhere = new StringBuffer();



	sbSelect.append(" SELECT b.pk_specimen_status, b.fk_specimen, a.spec_id, a.SPEC_QUANTITY_UNITS ");
	sbFrom.append(" FROM er_specimen a,  er_specimen_status b ") ;
	sbWhere.append(" WHERE a.fk_account = " + accId +" AND a.pk_specimen = b.fk_specimen AND a.spec_id IS NOT NULL ");

	sbWhere.append(" and a.pk_specimen in (" + selStrs + ")");

	sbWhere.append(" AND b.pk_specimen_status IN( SELECT pk_specimen_status FROM er_specimen_status WHERE pk_specimen_status = (SELECT pk_specimen_status FROM er_specimen_status WHERE fk_specimen = pk_specimen");
	sbWhere.append(" AND pk_specimen_status = (SELECT MAX(pk_specimen_status) FROM er_specimen_status WHERE fk_specimen = pk_specimen AND ss_date = (SELECT MAX(ss_date) FROM er_specimen_status WHERE fk_specimen = pk_specimen))   )  )");








 			mainsql = sbSelect.toString() + sbFrom.toString() + sbWhere.toString();
			//out.println("mainsql=====>"+mainsql);
		    countsql = " Select count(*) from ( " + mainsql + " )";

			String pagenum = "";
			int curPage = 0;
			long startPage = 1;
			long cntr = 0;


			pagenum = request.getParameter("page");
			if (pagenum == null)
			{
			pagenum = "1";
			}
			curPage = EJBUtil.stringToNum(pagenum);

			String orderBy = "";
			orderBy = request.getParameter("orderBy");

			if (EJBUtil.isEmpty(orderBy)){
			orderBy = "a.SPEC_COLLECTION_DATE	";
			}

			String orderType = "";

			orderType = request.getParameter("orderType");

			if (EJBUtil.isEmpty(orderType))
			{
			orderType = "desc nulls last";

			}


			long rowsPerPage=0;
			long totalPages=0;
			long rowsReturned = 0;
			long showPages = 0;
			long totalRows = 0;
			long firstRec = 0;
			long lastRec = 0;
			boolean hasMore = false;
			boolean hasPrevious = false;


			rowsPerPage =  Configuration.ROWSPERBROWSERPAGE ;
			totalPages =Configuration.PAGEPERBROWSER ;

			BrowserRows br = new BrowserRows();

			br.getPageRows(curPage,rowsPerPage,mainsql,totalPages,countsql,orderBy,orderType);

	    	rowsReturned = br.getRowReturned();
			showPages = br.getShowPages();
			startPage = br.getStartPage();
			hasMore = br.getHasMore();
			hasPrevious = br.getHasPrevious();
			totalRows = br.getTotalRows();
			firstRec = br.getFirstRec();
			lastRec = br.getLastRec();






%>

<p class = "sectionHeadings" ><%=MC.M_Updt_MultiSpmenStat%><%--Update Multiple Specimen Status*****--%></P>
<br>


<form name="specimenstat" id="multispecimenstat" method=post action="editmultiplespecimenstatussave.jsp" onsubmit="if (validateme(document.specimenstat)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">


<table class="basetbl outline midalign"><tr><td>
<table width="100%" >


	<tr>
	<th> <%=LC.L_Specimen_Id%><%--Specimen ID*****--%> </th>
	<th> <%=LC.L_Status%><%--Status*****--%> <FONT class="Mandatory">* </FONT></th>
	<th colspan=2> <%=LC.L_Status_Date%><%--Status Date*****--%><FONT class="Mandatory">* </FONT></th>
	<th colspan=2> <%=LC.L_Status_By%><%--Status By*****--%></th>
	<th colspan=2> <%=LC.L_Quantity%><%--Quantity*****--%></th>
	<th colspan=2> <%=LC.L_Recipient%><%--Recipient*****--%></th>
	<th > <%=LC.L_Tracking_Number%><%--Tracking Number*****--%> </th>
	<th colspan=3> <%=LC.L_Notes%><%--Notes*****--%> </th>
	</tr>



<br>




<%
String dSpecStatTop =cdSpecStat.toPullDown("specStatTop");
/*Bug #4223 The qty units dd is made disabled to make sure quantity units are not editable.*/
String dSpecQuantUnitTop =cdSpecQuantUnit.toPullDown("specQuantityUnitTop","DISABLED");
String statusSS = "00";
String statusMM = "00";
String statusHH = "00";


%>
<%-- INF-20084 Datepicker-- AGodara --%>
<tr>
	<td></td>
	<td><%=dSpecStatTop%> </td>
	<td width="20%"><Input type = "text" name="statDtTop" size=15 align = right class="datefield" readonly>
	</td>
	<td>
	</td>

	<td width="25%">
	<input type="hidden" name="specCreatorIdTop" size = 20 >
	<input type="text" name="specCreatedByTop" size = 20 readonly>

	<input type="hidden" name="statUserLNameTop" size = 20 >
	<input type="hidden" name="statUserFNameTop" size = 20 >

	</td>
	<td width="15%">
	<A href=# onClick="return openLookup(document.specimenstat,'statUser','Top');"><%=LC.L_Select%><%--SELECT*****--%></A>
	</td>

	<td width="5%">
	<input type="text" name="quantityTop" maxlength=500 size=5>
	</td>

	<td> <%=dSpecQuantUnitTop%> </td>


	<td width="25%">
	<input type="hidden" name="creatorIdTop"  size = 20 >
	<input type="text" name="createdByTop" size = 20 readonly>

	<input type="hidden" name="userLNameTop" size = 20 >
	<input type="hidden" name="userFNameTop" size = 20 >
	</td>
	<td width="15%">
	<A href=# onClick="return openLookup(document.specimenstat,'receipient','Top');"><%=LC.L_Select%><%--SELECT*****--%> </A>

	</td>


	<td >
	<input type="text" name="trackNumTop" maxlength=500 size=15>
	</td>


	<td><textarea name="statusNotesTop" rows=1 cols=25 MAXLENGTH = 20000></textarea></td>

</tr>

<tr><td></td><td></td><td><input type="text" name="statusHHTop" value="<%=statusHH%>" maxlength=2 size=1>:<input type="text" name="statusMMTop" value="<%=statusMM%>" maxlength=2 size=1>:
		<input type="text" name="statusSSTop" value="<%=statusSS%>" maxlength=2 size =1>
</td> <td><a href=# onclick="setCurrentTime(document.specimenstat, 0)"><%=LC.L_Cur_Time%><%--Current Time*****--%> </a></td><td></td><td></td><td></td>
<td><a href=# onclick="updateAll(document.specimenstat)"><%=LC.L_Set_ToAll%><%--Set To All*****--%> </a></td>

<td></td><td></td><td></td></tr>

<tr><td></td><td colspan="11"> <hr class="thinLine"></td> </tr>

<%
			String specimenStatusPk = "";
			String fkSpecimen = "";
			String specimenId = "";





			for(int i = 1 ; i <=rowsReturned ; i++){

			 specimenStatusPk = br.getBValues(i,"pk_specimen_status");
			 specimenStatusPk=(specimenStatusPk==null)?"":specimenStatusPk;

			 fkSpecimen = br.getBValues(i,"fk_specimen");
			 fkSpecimen=(fkSpecimen==null)?"":fkSpecimen;

			 specimenId = br.getBValues(i,"spec_id");
			 specimenId=(specimenId==null)?"":specimenId;

			 int specIdLen = specimenId.length();
			 if (specIdLen > 50)
			 specimenId = specimenId.substring(1,50)+"...";




	%>


<input type="hidden" name="specStatPk<%=i%>" value="<%=specimenStatusPk%>">

<input type="hidden" name="fkSpec<%=i%>" value="<%=fkSpecimen%>">
	<tr width = "100%">



	<td>
	<p class="defComments"><%=specimenId%>
	</p>
	</td>



	<td>
	<%
	specStat = "specStat" + i ;
	String dSpecStat =cdSpecStat.toPullDown(""+specStat);


	%>

	<%=dSpecStat%>
	</td>
<%-- INF-20084 Datepicker-- AGodara --%>
	<td width="20%"><Input type = "text" name="statDt<%=i%>" size=15 align = right class="datefield" readonly>
	</td>
	<td></td>



	<td width="25%">
	<input type="hidden" name="specCreatorId<%=i%>" size = 20 >
	<input type="text" name="specCreatedBy<%=i%>" size = 20 readonly>

	<input type="hidden" name="statUserLName<%=i%>" size = 20 >
	<input type="hidden" name="statUserFName<%=i%>" size = 20 >

	</td>
	<td width="15%">
	<A href=# onClick="return openLookup(document.specimenstat,'statUser',<%=i%>);"><%=LC.L_Select%><%--SELECT*****--%></A>
	</td>


	<td width="5%">
	<input type="text" name="quantity<%=i%>" maxlength=500 size=5>
	</td>
	<td>
	<%
	String specQuUnit = br.getBValues(i,"SPEC_QUANTITY_UNITS");
	%>
	<input type="hidden" name="specQtyUnitEdit<%=i%>"  size = 20 value=<%=specQuUnit%>>
	<%
	specQuantityUnit = "specQuantityUnit" + i ;
	/*Bug #4223 The qty units dd is made disabled to make sure quantity units are not editable.*/
	/*Status quantity units are pre-populated with specimen quantity units*/
	String dSpecQuantUnit="";
	if (specQuUnit==null){
		dSpecQuantUnit=cdSpecQuantUnit.toPullDown(""+specQuantityUnit, EJBUtil.stringToNum(""), "DISABLED");
	}else{
		dSpecQuantUnit=cdSpecQuantUnit.toPullDown(""+specQuantityUnit,EJBUtil.stringToNum(specQuUnit),"DISABLED");
	}
	%>
	<%=dSpecQuantUnit%>
	</td>




	<td width="25%">
	<input type="hidden" name="creatorId<%=i%>"  size = 20 >
	<input type="text" name="createdBy<%=i%>" size = 20 readonly>

	<input type="hidden" name="userLName<%=i%>" size = 20 >
	<input type="hidden" name="userFName<%=i%>" size = 20 >
	</td>
	<td width="15%">
	<A href=# onClick="return openLookup(document.specimenstat,'receipient',<%=i%>);"><%=LC.L_Select%><%--SELECT*****--%> </A>

	</td>


	<td >
	<input type="text" name="trackNum<%=i%>" maxlength=500 size=15>
	</td>


	<td><textarea name="statusNotes<%=i%>" rows=1 cols=25 MAXLENGTH = 20000></textarea></td>






	</tr>
	<tr><td></td><td></td><td>
	<input type="text" name="statusHH<%=i%>" value="<%=statusHH%>" maxlength=2 size=1>:
	<input type="text" name="statusMM<%=i%>" value="<%=statusMM%>" maxlength=2 size=1>:
	<input type="text" name="statusSS<%=i%>" value="<%=statusSS%>" maxlength=2 size =1>
</td><td><a href=# onclick="setCurrentTime(document.specimenstat, <%=i%>)"><%=LC.L_Cur_Time%><%--Current Time*****--%> </a></td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>



<%}//end of for loop %>
</table>
<input type="hidden" name="cntNumrows" value="<%=rowsReturned%>">
<br>

<jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="multispecimenstat"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>

</td></tr></table>

</form>

<%

}
//end of session
else{
%>

<jsp:include page="timeout.html" flush='true'></jsp:include>
<%}%>

	<div class = "myHomebottomPanel">
	<jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
</BODY>

</html>