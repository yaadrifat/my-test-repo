<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList,java.util.HashMap"%>
<%@page import="com.velos.eres.widget.business.common.UIGadgetDao" %>
<%@page import="org.json.*"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%!
String createRow(int rowNum, int numCol) {
	StringBuffer sb1 = new StringBuffer();
	sb1.append("<tr>\n");
	for(int iCol=1; iCol<numCol+1; iCol++) {
		sb1.append("<td>");
		sb1.append("Row=").append(rowNum).append(" Col=").append(iCol);
		sb1.append("</td>\n");
	}
	sb1.append("</tr>\n");
	return sb1.toString();
}
%>
<%
HttpSession tSession = request.getSession(true); 
if (!sessionmaint.isValidSession(tSession)) {
	return;
}
String userId = (String)tSession.getAttribute("userId");
UIGadgetDao uiGadgetDao = new UIGadgetDao();
String settings = uiGadgetDao.getGadgetInstanceSettings(userId);
String numRows = null, numCols = null;
if (settings != null && settings.length() > 0) {
	try {
		JSONObject parentJObj = null;
		parentJObj = (JSONObject) new JSONTokener(settings).nextValue();
		JSONObject jobj = (JSONObject)parentJObj.get("gadgetSample1");
		numRows =  jobj.getString("numRows");
		numCols =  jobj.getString("numCols");
	} catch(Exception e) {
		//e.printStackTrace();
	    System.out.println("gadgetSample1.jsp:JSON exception: " + e);
		numRows = null;
		numCols = null;
	}
}
%>
<table style="width:100%">
<tr>
<td>
<div id="gadgetSample1_settings" style="border:#aaaaaa 1px solid; padding: 2px; display:none">
    <form id="gadgetSample1_formSettings" name="gadgetSample1_formSettings" onsubmit="return false;" >
    <p><b>Settings for Gadget Sample 1</b></p>
    <table style="width:100%; " >
        <tr>
            <td>
                <div id="gadgetSample1_numRows_error"></div>
            </td>
            <td>
                <div id="gadgetSample1_numCols_error"></div>
            </td>
        </tr>
        <tr>
            <td>
                <label for="gadgetSample1_numRows">Number of Rows</label><FONT class="Mandatory"> *</FONT><br/>
                <input id="gadgetSample1_numRows" name="gadgetSample1_numRows" type="text" size="20" ></input>
            </td>
            <td>
                <label for="gadgetSample1_numCols">Number of Columns</label><FONT class="Mandatory"> *</FONT><br/>
                <input id="gadgetSample1_numCols" name="gadgetSample1_numCols" type="text" size="20" ></input>
            </td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" align="right">
            <button type="submit"
                    id="gadgetSample1_saveButton"
                    class="gadgetSample1_saveButton">Save</button>&nbsp;&nbsp;&nbsp;&nbsp;
            <button type="button" 
                    id="gadgetSample1_cancelButton"
                    class="gadgetSample1_cancelButton">Cancel</button>
            </td>
        </tr>
    </table>
    </form>
</div>
</td>
</tr>

<tr>
<td>
<div>
    <table border="1">
    <%
    if (numRows == null || numRows.length() == 0) { numRows = "0"; } 
    if (numCols == null || numCols.length() == 0) { numCols = "0"; } 
    int numRowsInt = 0;
    int numColsInt = 0;
    try { numRowsInt = Integer.parseInt(numRows); } catch(Exception e) {}
    try { numColsInt = Integer.parseInt(numCols); } catch(Exception e) {}
    for(int iRow=1; iRow<numRowsInt+1; iRow++) {
    	out.println(createRow(iRow, numColsInt));
    }
    %>
    </table>
</div>
</td>
</tr>
</table>