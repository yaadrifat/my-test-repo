<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<HEAD>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=windows-1252">
<TITLE>eResearch FAQs</TITLE>

<%@ page import="com.velos.eres.service.util.LC"%>
</HEAD>

<BODY style = "overflow:auto">

<Link Rel=STYLESHEET HREF="common.css" type=text/css>

<DIV class="staticDefault">

<table width="100%" cellspacing="0" cellpadding="0" border=0>
<tr>
<td height="20">
</td>
</tr>
<tr>
<td id="aboutHeadings" width="100%" align="center">
V e l o s   e R e s e a r c h FAQs
</td>

</tr>
</table>

<table width="100%" cellspacing="0" cellpadding="0" border=0>
<tr>
<td height="20">
</td>
</tr>
<tr>
<td><B>CONTENTS</B>
</td>
</tr>
</table>

<table width="100%" cellspacing="0" cellpadding="0" border=0>
<tr>
<td height="20">
<A HREF="#sec1"><b>SECTION 1 WHAT, WHO AND WHY</b></A>
</td>
</tr>

<tr>
<td height="20">
<A HREF="#sec1.1">1.1   WHAT IS VELOS ERESEARCH?</A>
</td>
</tr>

<tr>
<td height="20">
<A HREF="#sec1.2">1.2   WHO SHOULD USE VELOS ERESEARCH?</A>
</td>
</tr>

<tr>
<td height="20">
<A HREF="#sec1.3">1.3   WHAT ARE THE ADVANTAGES OFFERED BY VELOS ERESEARCH?</A>
</td>
</tr>

<tr>
<td height="20">
<A HREF="#sec1.4">1.4   HOW DOES VELOS ERESEARCH COMPARE TO THE CONVENTIONAL SYSTEM OF CLINICAL RESEARCH MANAGEMENT?</A>
</td>
</tr>

</table>

<br>

<table width="100%" cellspacing="0" cellpadding="0" border=0>
<tr>
<td height="20">
<A HREF="#sec2"><b>SECTION 2   GETTING STARTED</b></A>
</td>
</tr>

<tr>
<td height="20">
<A HREF="#sec2.1">2.1   WHAT DO I NEED TO RUN THE APPLICATION?</A>
</td>
</tr>

<tr>
<td height="20">
<A HREF="#sec2.2">2.2   HOW DO I CREATE AN ACCOUNT?</A>
</td>
</tr>

<tr>
<td height="20">
<A HREF="#sec2.3">2.3   INDIVIDUAL VS. GROUP ACCOUNT?</A>
</td>
</tr>

<tr>
<td height="20">
<A HREF="#sec2.4">2.4   HOW SECURE IS MY DATA?</A>
</td>
</tr>

</table>

<br>

<table width="100%" cellspacing="0" cellpadding="0" border=0>
<tr>
<td height="20">
<A HREF="#sec3"><b>SECTION 3   FEATURES/SERVICES AVAILABLE IN VELOS ERESEARCH</b></A>
</td>
</tr>

<tr>
<td height="20">
<A HREF="#sec3.1">3.1	WHAT FEATURES ARE AVAILABLE IN THIS VERSION OF VELOS ERESEARCH?</A>
</td>
</tr>

<tr>
<td height="20">
<A HREF="#sec3.2">3.2   WHAT FEATURES WILL BE AVAILABLE IN FUTURE PHASES OF VELOS ERESEARCH?</A>
</td>
</tr>

<tr>
<td height="20">
<A HREF="#sec3.3">3.3   ARE THERE ADDITIONAL AVAILABLE SERVICES FOR CUSTOMIZATION?</A>
</td>
</tr>

</table>

<br>

<table width="100%" cellspacing="0" cellpadding="0" border=0>
<tr>
<td height="20">
<A HREF="#sec4"><b>SECTION 4   NEW TRIAL NOTIFICATION SYSTEM</b></A>
</td>
</tr>

<tr>
<td height="20">
<A HREF="#sec4.1">4.1   WHAT IS THE NEW TRIAL NOTIFICATION SYSTEM?</A>
</td>
</tr>

<tr>
<td height="20">
<A HREF="#sec4.2">4.2   HOW DO I SIGN UP FOR THIS FEATURE?</A>
</td>
</tr>


</table>

<BR><BR>

<table width="100%" cellspacing="0" cellpadding="0" border=0>

<tr  height = 150>
<td id = 'sec1'>
<b>Section 1 What, Who and Why</b>
<HR>
</td>
</tr>

<tr>
<td id = 'sec1.1'>
<b>1.1   What is Velos eResearch?</b>
<br><br>
Velos eResearch is a complete clinical research management solution designed for research teams that accelerates the clinical study process, is easy to use and produces quality study results.  Velos eResearch is an Investigator-centered, Internet-based software product that expedites the research process by supporting study administration, data coordination from participating centers, protocol management streamlining and process standardization.

Velos eResearch is ushering in a new generation of Internet-based research capabilities that fundamentally improves the pervasiveness and efficiency of clinical research, both internally and with trial sponsors.  Velos eResearch enables better <%=LC.Pat_Patient_Lower%> enrollment and communication, efficient transfer of protocol and data between investigators, the IRB and the participating sites, and better task/time management, thereby reducing the elapsed time and effort required to execute clinical trials.

This is a revolutionary new way of managing clinical research.  All the steps in the research process are on the web; it can be accessed from anywhere, anytime.  The account is protected by a password, giving access only to authorized personnel.  It is easy to use, reliable and completely secure.
</td>
</tr>


<tr>
<td id = 'sec1.2'><br><br>
<b>1.2   Who should use Velos eResearch?</b>
<br><br>
Velos eResearch is a useful tool for anyone involved in conducting and managing clinical research. It can be used by:

<ul><li>Large facilities, like hospitals or pharmaceutical organizations, that conduct several trials at a given time involving many different centers.  The application provides tools by which they can coordinate the efforts of the participating centers, thus enhancing their program outcome and shortening the duration of the trial. </li>

<li>Small groups of users, such as a study team, can streamline their research process and communicate more effectively.</li>

<li>Primary Investigators who want to reach a global community of research associates and / or potential <%=LC.Pat_Patients_Lower%>.</li>

<li>Individuals, like research students or physicians, can receive valuable help creating protocols, generating reports and decreasing their paperwork load.  They have a centralized point of access to research news and study data, secure communication and study management tools  all in one place, at one time.</li>

<li><%=LC.Pat_Patients%> and healthcare personnel can stay informed about the latest trials and news in their field of interest.</li>
</ul>
</td>
</tr>


<tr>
<td id = 'sec1.3'><br><br>
<b>1.3   What are the advantages offered by Velos eResearch?</b>
<br><br>
With increasing emphasis being placed on completing a clinical trial efficiently and on time, researchers are continuously on the lookout for newer technologies that will help them streamline processes and coordinate activities.  We believe the opportunity for radical improvement lies in supplying the infrastructure that supports the investigator activities that lead up to data submission.  <br>
In doing so, Velos eResearch provides the opportunity to:

<ul><li>Improve quality of research work </li>

<li>Increase revenue capture and research volume</li>

<li>Reduce costs through more efficient trial execution</li>

<li>Improve ones position in the research value chain</li>

</ul>
<br>
Together, these capabilities can create enormous value for any research program or institution.
</td>
</tr>



<tr>
<td id = 'sec1.4'><br><br>
<b>1.4   How does Velos eResearch compare to the conventional system of clinical research management?</b>
<br><br>

	<table border = 1 width = "75%">
	<tr BGCOLOR = "#E5D1FD">
		<td>
		Conventional Clinical Research
		</td>
		<td>
		Velos eResearch
		</td>

	</tr>

	<tr>
		<td>
		Starting the trial process may take months due to complex process of protocol creation and distribution
		</td>
		<td>
		Quick start to the trial process. Fast protocol creation and instant distribution.
		</td>

	</tr>
	<tr>
		<td>
		Unnecessary time involved in communication, coordination and travel
		</td>
		<td>
		Better time management shortens duration of the trial
		</td>

	</tr>
	<tr>
		<td>
		Lots of paperwork
		</td>
		<td>
		Paperless solution
		</td>

	</tr>
	<tr>
		<td>
		Report generation is time consuming and complicated
		</td>
		<td>
		Effortless generation of reports and schedules
		</td>

	</tr>
</table>
</tr>


<tr  height = 150>
<td id = 'sec2'>
<b>Section 2   Getting Started</b>
<HR>
</td>
</tr>

<tr>
<td id = 'sec2.1'>
<b>2.1   What do I need to run the application?</b>
<br><br>
To be a user of eResearch, all you need is a computer and an Internet connection  there is no software to download and you do not need a programmer.  All features are simple and easy to use.
</td>
</tr>

<tr>
<td id = 'sec2.2'><br><br>
<b>2.2   How do I create an account?</b>
<br><br>
To create an account, click on the 'Sign Up' link. Complete and submit the registration form.  After verification, Velos will create an account for you and notify you by email within one business day. You will be assigned a unique login ID and password, enabling you to begin using the various features and services of the application.
</td>
</tr>



<tr>
<td id = 'sec2.3'><br><br>
<b>2.3   Individual vs. Group Account?</b>
<br><br>
An Individual Account is especially designed for the investigator or researcher who wishes to centralize all research-related activities to a single access, personalized account.  This is a free account with the following features:

<ul>
<li>Single access account for all study data and research news</li>

<li>Access to thousands of study summary listings</li>

<li>Secure communication with sponsor or study protocol creator</li>

<li>Ability to create detailed study protocol</li>

<li>Protocol sharing with other selected registered users</li>
<li>Study protocol tracking </li>
<li>Automated study management</li>
<li>Instantaneous generation of reports</li>
<li>Personalized list of favorite links </li>

</ul>

A Group Account aims to bring together participants from a research network onto a common platform. Research organizations, academic medical centers, CROs, departments or study teams to suit their needs can customize this account type.  All users in the group have access to the features mentioned above for the individual, as well as the added benefits of the following features targeting the group as a whole:

<ul>
<li>Secure account shared by users in the group</li>
<li>Instant access to up-to-date information for all users within the group</li>
<li>Creation of study teams that can work off the same protocol and research data</li>
<li>Sharing of group specific information with all members of the account</li>
<li>Generation of reports at the account level  across the organization or department</li>
</ul>

</td>
</tr>

<tr>
<td id = 'sec2.4'><br><br>
<b>2.4   How secure is my data?</b>
<br><br>
128  bit encryption and firewall protection ensure system security and protection of incoming and outgoing data.  Access to the accounts is protected by passwords; users can be assigned selective rights in accordance with their role in the study.  All information entered by you is yours and is not visible to other users, unless you wish to share selected information with specified users.

This application can be run on the Internet or your facilitys Intranet.  To run it on the Intranet, please contact Customer Support for more information.

</td>
</tr>


<tr  height = 150>
<td id = 'sec3'>
<b>Section 3   Features/Services Available in Velos eResearch</b>
<HR>
</td>
</tr>

<tr>
<td id = 'sec3.1'>
<b>3.1	What features are available in this version of Velos eResearch?</b>
<br><br>
<ul>
<li>Personalized Accounts
	<ul><li>Single access account for all study data and research news</li>
	<li>Access to thousands of study summary listings</li>
	<li>List of favorite links</li></ul>

</li>

<li>Study Protocol</li>
	<ul><li>Creation of an online study protocol that can be accessed by authorized users from anywhere in the world</li>
	<li>Broadcast study summaries to thousands of registered researchers</li>
	<li>Protocol sharing with other selected registered users</li>
	<li>Study protocol tracking</li>
	<li>Immediate access to all protocol updates</li>
	<li>Interaction with IRB for review/approval</li></ul>

<li>Communication</li>
	<ul><li>Secure message center</li>
	<li>Communication with study protocol creator or research team</li>
	<li>Notification of new trials</ul></li>


<li>Instantaneous generation of reports</li>
	<ul><li>Study specific reports</li>
	<li>Across organization or department reports</ul></li>

</ul>

</td>
</tr>

<tr>
<td id = 'sec3.2'><br><br>
<b>3.2	What features will be available in future phases of Velos eResearch?</b>
<br><br>
Velos eResearch is to be released in a series of phases with increasing functionality. The first phase allows the user to complete the basic functions at the core of clinical research, such as study search, investigator recruitment, protocol creation, it's distribution and tracking, the IRB process and reporting.  Later phases will provide more advanced features as listed below:
<ul>
<li>Secure account shared by users in the group</li>
<li><%=LC.Pat_Patient%> recruitment and screening</li>
<li><%=LC.Pat_Patient%> enrollment to a study</li>
<li>Secure online <%=LC.Pat_Patient_Lower%> profiles</li>
<li>Automatic generation of <%=LC.Pat_Patient_Lower%> schedules</li>
<li><%=LC.Pat_Patient%> specific reports </li>
<li>Task management via reminders, alerts, to do lists etc.</li>
<li>Resource and budget management</li>
<li>Integration with other clinical modules of the Velos suite</li>

</td>
</tr>


<tr>
<td id = 'sec3.3'><br><br>
<b>3.3   Are there additional available services for customization?</b>
<br><br>
At Velos, we understand the differing needs of our various user constituencies and provide additional services to ensure that all those needs are met.  Some of these services are listed below:

<ul>
<li>Account customization to include organization-specific logo and links</li>

<li>Online protocol uploading</li>

<li>Creation of study-specific calendar templates</li>

<li>Integration with existing organizational systems such as <%=LC.Pat_Patient_Lower%> data, labs, scheduling, etc. (Contingent to evaluation on a case-by-case basis)
</li>

</ul>
<br> For information about these or additional services, please contact our Customer Support department
</td>
</tr>


<tr  height = 150>
<td id = 'sec4'>
<b>Section 4   New Trial Notification System</b>
<HR>
</td>
</tr>

<tr>
<td id = 'sec4.1'>
<b>4.1   What is the New Trial Notification System?</b>
<br><br>
The New Trial Notification System is a free service provided by Velos eResearch to match users with trials in their areas of interest. <%=LC.Pat_Patients%> and healthcare professionals, who are interested in staying informed of new trials, find this to be a very useful feature.

Users who sign up need to specify the areas that they are interested in and whenever a new trial is added in those categories, an email will be sent to the user notifying the user about the new trial.

</td>
</tr>


<tr>
<td id = 'sec4.2'><br><br>
<b>4.2   How do I sign up for this feature?</b>
<br><br>
To sign up for the Notification System, you need to be a registered user.  Once you are a registered user, you can select the therapeutic areas of interest.  When a new trial is added to the listing in that category, an email will be sent notifying you about the new trial.
</td>
</tr>


</table>
<br><br><br><br><br><br>
<div>
<jsp:include page="bottompanel.jsp" flush="true">
</jsp:include>
</div>

</BODY>

</DIV>

</HTML>
