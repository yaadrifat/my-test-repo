<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

 <% String type = request.getParameter("type"); 
//Modified By Parminder Singh Bug#10693
 String cCalledFrom = "";
	cCalledFrom = ((request.getParameter("cCalledFrom")) == null)?"P":request.getParameter("cCalledFrom");
 String calledFrom=null;                 //Added to resolve Bug#7556,7596,7597 and 7598 : Raviesh              
 type=(type==null)?"":type;
 if (type.equals("L")){
	 //Added to resolve Bug#7598 : Raviesh 
	 //Modified By Parminder Singh Bug#10693
	 if(cCalledFrom.equals("S")){
      calledFrom = "protocollist.jsp?mode=N&srcmenu=tdmenubaritem3&selectedTab=7&calledFrom=S&calassoc=S";	
	 }
	 else{
	  calledFrom = "protocollist.jsp?mode=N&srcmenu=tdmenubaritem3&selectedTab=1";	
	 }
 %>
<title><%=LC.L_Cal_LibBrowser%><%--Calendar Library Browser*****--%></title>
 <%}else if  (type.equals("C")){
	 //Added to resolve Bug#7596 and 7597 : Raviesh
	 calledFrom = "fieldLibraryBrowser.jsp?srcmenu=tdmenubaritem4&mode=initial&selectedTab=4";
	 %>
<title><%=LC.L_Fld_LibBrowser%><%--Field Library Browser*****--%></title> 
 <%} else{
	 //Added to resolve Bug#7556 : Raviesh
	 calledFrom = "formLibraryBrowser.jsp?srcmenu=tdmenubaritem4&selectedTab=3";
 %>
 <title><%=LC.L_Fld_LibBrowser%><%--Form Library Browser*****--%></title> 
 <%} %>
 


<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<BODY>
 <jsp:useBean id="catLibB" scope="request" class="com.velos.eres.web.catLib.CatLibJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import ="com.velos.eres.business.common.*,java.util.*, com.velos.eres.service.util.EJBUtil" %>
  
<%
 int iret = -1;
 int iupret = 0;

 String recordType = "";
 

 String mode =request.getParameter("mode");
 
 String catLibId =request.getParameter("catLibId");
 
// String eSign = request.getParameter("eSign");

HttpSession tSession = request.getSession(true); 
if (sessionmaint.isValidSession(tSession)) 
{
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%--	
	String oldESign = (String) tSession.getValue("eSign");


	if(!oldESign.equals(eSign)) {

  <jsp:include page="incorrectesign.jsp" flush="true"/>	

	} 
else
	{ --%>
	
<%	String ipAdd = (String) tSession.getValue("ipAdd");
	String usr = (String) tSession.getValue("userId");
	String name = StringUtil.stripScript(request.getParameter("categoryName"));
	String desc = request.getParameter("categoryDesc");
	type = request.getParameter("type");
	
   	if(mode.equals("M"))
    	{
    	
        	int icatLibId=Integer.parseInt(catLibId);        	
        	catLibB.setCatLibId(icatLibId);		
        	catLibB.getCatLibDetails();
        	catLibB.setCatLibName(name);
        	catLibB.setCatLibDesc(desc);
        	catLibB.setLastModifiedBy(usr);
        	catLibB.setIPAdd(ipAdd);
		catLibB.setRecordType("M");
        	iupret=catLibB.updateCatLib();
    	} else
    	{
         	
         	String accId="";
         	accId = (String) tSession.getValue("accountId");
         
         	catLibB.setCatLibName(name); 
         	catLibB.setCatLibDesc(desc);
         	catLibB.setCatLibType(type);
         	catLibB.setCatLibAcc(accId);
         	catLibB.setCatLibCreator(usr);
         	catLibB.setIPAdd(ipAdd);
		catLibB.setLastModifiedBy(usr);
		catLibB.setRecordType("N");
         	catLibB.setCatLibDetails();
         	iret = catLibB.getCatLibId();	
    	}  
  %> 
  
<%  	if ((iupret == -2) || (iret==0)) {%>
			<br><br><br><br><br><p class = "successfulmsg" align = center>
			<%=MC.M_Data_NotSvdSucc%><%--Data not saved successfully*****--%></p> <Br><Br>
			<button name="back" onclick="window.history.back();"><%=LC.L_Back%></button></p>
	<%
	 //Added by Manimaran to find the duplicate Category or Formtype Name on May25,2005 for May Enhancement.
	 }else if((iupret== -3)||(iret== -3)) 
		{ 
		if(type.equals("C")){%>
		<br><br><br><br><br><p class = "successfulmsg" align = center>
	<%=MC.M_CatNameDup_ClkBack%><%--Category Name is duplicate. Click on "Back" Button to change the name*****--%></p><Br>
	<input type="Button" name="back" value="<%=LC.L_Back%><%--Back*****--%>"	 			
	 onclick="window.history.back()"></p>
	<% return;}
		else if (type.equals("L")){%>
		<br><br><br><br><br><p class = "successfulmsg" align = center>
	<%=MC.M_CalTypeName_ClkBackBtn%><%--Calendar Type Name is duplicate. Click on "Back" Button to change the name*****--%></p><Br>
	<input type="Button" name="back" value="<%=LC.L_Back%><%--Back*****--%>"	 			
	 onclick="window.history.back()"></p>
	 <%return;}else{%>
	 <br><br><br><br><br><p class = "successfulmsg" align = center>
	<%=MC.M_FrmTypName_Dupli%><%--Form Type Name is duplicate. Click on "Back" Button to change the name*****--%></p><Br>
	<input type="Button" name="back" value="<%=LC.L_Back%><%--Back*****--%>"	 			
	 onclick="window.history.back()"></p>
	 <%return;}
	     
	 
	 
	 }else {%>
	<br><br><br><br><br><p class = "successfulmsg" align = center>
	<%=MC.M_Data_SavedSucc%><%--Data saved successfully*****--%></p>			
	
		<script>		
			//Added to resolve Bug#7556,7596,7597 and 7598 : Raviesh
			//Modified By Parminder Singh Bug#10693					
			window.opener.location.href = "<%=calledFrom%>";
			/* Bug#10144 18-Jun-2012 -Sudhir*/
			//window.opener.location.reload();
			setTimeout("self.close()",1000);
		</script>
	<%}
		

		
//}//esign 

}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>

</HTML>





