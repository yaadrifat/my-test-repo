<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title><%=MC.M_Pending_MsgsInEres%><%--Pending messages in eResearch*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<Link Rel=STYLESHEET HREF="common.css" type=text/css>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<body>
<jsp:useBean id="msgcntrB" scope="session" class="com.velos.eres.web.msgcntr.MsgcntrJB"/>

<%@ page language = "java" import = "com.velos.eres.business.msgcntr.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%><%@page import="com.velos.eres.service.util.MC"%><%@page import="com.velos.eres.service.util.LC"%>

<%
   MsgsPendingValidationStateKeeper msk = null;
   ArrayList msgStats;
   ArrayList userIds;
   ArrayList userLastNames;
   ArrayList userFirstNames; 
   ArrayList userMiddleNames; 
   ArrayList userStats;

   msgStats = new ArrayList();
   userIds = new ArrayList();
   userLastNames = new ArrayList();
   userFirstNames = new ArrayList();
   userMiddleNames = new ArrayList();
   userStats = new ArrayList();

   msk = msgcntrB.findByMsgsPendingValidation();

   msgStats = msk.getMsgStats();
   userIds = msk.getUsrIds();
   userLastNames = msk.getUsrLastNames();
   userFirstNames = msk.getUsrFirstNames();
   userMiddleNames = msk.getUsrMiddleNames();
   userStats = msk.getUsrStats();

   int len = userIds.size();
   int counter = 0;

   String msgStatus = "";
   String userLastName = "";
   String userFirstName = "";
   String userMiddleName = "";
   String userStatus = "";
   String userId = "";

   String pagename = "accountcreation.jsp";
%>

<%=len%>

<jsp:include page="panel.htm" flush="true"/>   

<DIV class="browserDefault">

<Form name="groupbrowser" method="post" action="" onsubmit="">

<P class = "defComments">
<%=MC.M_FlwUsrSentMsg_InEres%><%--Following are the users whose have sent messages in eResearch*****--%>
</P>

<table width="500" >
 <tr>
   <th>
	<%=LC.L_Msg_Status%><%--Message Status*****--%>
   </th>
   <th>
	<%=LC.L_User_Name%><%--User Name*****--%>
   </th>
   <th>
	<%=LC.L_User_Status%><%--User Status*****--%>
   </th>
  </tr>

 <%
    for(counter = 0;counter<len;counter++)
	{	  
	msgStatus=((msgStats.get(counter)) == null)?"-":(msgStats.get(counter)).toString();

	if (msgStatus.equals("R")) {
		msgStatus=LC.L_Read/*"Read"*****/;
	}
	else if (msgStatus.equals("U"))  {
		msgStatus=LC.L_Unread/*"UnRead"*****/;
	}

	userId=((userIds.get(counter)) == null)?"-":(userIds.get(counter)).toString();

 userLastName=((userLastNames.get(counter))==null)?"-":(userLastNames.get(counter)).toString();
     userFirstName=((userFirstNames.get(counter))==null)?"-":(userFirstNames.get(counter)).toString();
  
 userMiddleName=((userMiddleNames.get(counter)) ==null)?"-":(userMiddleNames.get(counter)).toString();

   userStatus=((userStats.get(counter)) == 			null)?"-":(userStats.get(counter)).toString();

	if (userStatus.equals("A")) {
		userStatus=LC.L_Activate/*"Avtivate"*****/;
	}
	else if (userStatus.equals("D"))  {
		userStatus=LC.L_Deactivate/*"Deactivate"*****/;
	}
	else if (userStatus.equals("R"))  {
		userStatus=LC.L_Reactivate/*"Reactivate"*****/;
	}

	if ((counter%2)==0) {
  %>
	
		<tr class="browserEvenRow">	
  <%
	}
	else{
  %>
		
			<tr class="browserOddRow">	
  <%
	}
  %>

		<td>
		 <%= msgStatus%>
		</td>		
		<td>
	<A HREF="<%=pagename%>?userId=<%=userId%>&mode=M"><%=userLastName%><%=userMiddleName%> <%=userFirstName%> </A>
		</td>
		<td>
		 <%= userStatus%>
		</td>
		</tr>

<%
		}
%>

</table>
</Form>
</div>
</body>
</html>
