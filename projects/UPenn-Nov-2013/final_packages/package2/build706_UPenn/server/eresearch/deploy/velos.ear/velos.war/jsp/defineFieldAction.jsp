<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
<head>
<title><%=LC.L_Define_InterFldAction%><%--Define Inter-Field Action*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

 

</head>
<jsp:include page="include.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>


<SCRIPT Language="javascript">

 function  validate(formobj)
 {
    populateKeyWordParams(formobj);
    
    if (  !(validate_col('Select Source Field',formobj.sourceFldDD     )     )   ) return false
    if (  !(validate_col('Select Action Condition',formobj.actionCondition     )     )   ) return false
    
    //responses not needed for is null and not null validations
    if (formobj.actionCondition.value != 'velIsNull' && formobj.actionCondition.value != 'velIsNotNull' )
    {
    	if (  !(validate_col('\'Select Responses\'',formobj.dispconditionvalue)     )   ) return false
    }
    	
    if (  !(validate_col('\'Select Target Fields\'',formobj.disptargetvalue)     )   ) return false
    if (  !(validate_col('Select Action',formobj.velreadonlyordisable)     )   ) return false
        
 	if(formobj.codeStatus.value != "WIP"){	
 	 	if (  !(validate_col('e-Signature',formobj.eSign)     )   ) return false
 		 
 		if(isNaN(formobj.eSign.value) == true) 
 			{
 			alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
 			formobj.eSign.focus();
 			 return false;
 	   		}
 	 	}
}

function populateKeyWordParams(formobj)
{
    var value ;
    value = formobj.sourceFldDD.options[formobj.sourceFldDD.selectedIndex].value;
    var fldId , sysid;
    fld = value.substring(0, value.indexOf("[VELSEP]"));
    sysid = value.substring(value.indexOf("[VELSEP]")+8,value.length);
    
    formobj.velsource.value = sysid;
    formobj.velsourcefldid.value = fld;
    
    value = formobj.actionCondition.options[formobj.actionCondition.selectedIndex].value;
    formobj.velcondition.value = value;    
    
    
}

function openConditionValue(formobj)
{
        sourceFld = formobj.velsourcefldid.value;
        if ((fnTrimSpaces(sourceFld)=='') || sourceFld==0)
        {
            alert("<%=MC.M_PlsSel_SrcFld%>");/*alert("Please select a Source Field");*****/
            return false;
        }
        
        sourceFormName = formobj.name;
        conditionDispFld = "dispconditionvalue";
        conditionIdFld = "velconditionid";
        conditionKeywordFld = "velconditionvalue";
        
        paramStr="selectActionConditionValues.jsp?fieldLibId="+sourceFld+"&sourceFormName="+sourceFormName+"&conditionDispFld="+conditionDispFld+"&conditionIdFld="+conditionIdFld + "&conditionKeywordFld=" + conditionKeywordFld;
        
        openPopWindow(paramStr);
}

function openTarget(formobj)
{
        sourceFormName = formobj.name;
        targetDispFld = "disptargetvalue";
        targetIdFld = "veltargetid";
        targetKeywordFld = "veltarget";
        formId = formobj.formId.value;
        paramStr="selectActionTargetFields.jsp?formId="+formId +"&sourceFormName="+sourceFormName+"&targetDispFld="+targetDispFld+"&targetIdFld="+targetIdFld + "&targetKeywordFld=" + targetKeywordFld;
        
        openPopWindow(paramStr);
}


function openPopWindow(paramStr)
{
    windowName=window.open(paramStr,"FieldAction","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=650,height=450 ,top=200 ,left=250");
    windowName.focus();
    
}


</SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>


<%@ page language = "java" import = "com.velos.eres.business.fieldAction.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.tags.*"%>
<%@ page language = "java" import = "com.velos.eres.web.fieldLib.FieldActionJB"%>

<body>
<DIV class="popDefault" id="div1"> 
 <%

    HttpSession tSession = request.getSession(true);
    int pageRight = 0;    
    
    FieldActionJB fldActionJB = new FieldActionJB();

    FieldActionDao fldActionDaoInfo = new FieldActionDao();
    
    FieldLibDao fldlibDao = new FieldLibDao();
    
    FieldActionStateKeeper fsk = new FieldActionStateKeeper();     

    VFieldAction vfldAction = new VFieldAction ();
    
   if (sessionmaint.isValidSession(tSession))
   {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
	
<%
     
		// get page right
			
	 String pageRightParam = request.getParameter("pgrt");		
	 pageRight = EJBUtil.stringToNum(pageRightParam);
	 
     
    if (pageRight >= 4)
        
    {
        String mode="";
        String codeStatus = request.getParameter("codeStatus");
        mode=request.getParameter("mode");
        String formId = request.getParameter("formId");
        String fieldAction = request.getParameter("fldActionId");
        
        String pullDownSourceField = "";
        ArrayList sourcefieldIds = new ArrayList();
        ArrayList sourcefieldNames = new ArrayList();
        ArrayList sourcefieldSystemIds = new ArrayList();
        ArrayList sourcefieldUids = new ArrayList();
        
        Hashtable htActionInfo = new Hashtable();
        String fldActionCondition = "";
        String fldActionType = "";
        String fldAction  = "";
        String selectedSourceFld  = "";
        int selectedSourceFldInt = 0;                
        
        String selectedVelconditionvalue = "";
        String selectedDispconditionvalue = "";
        String selectedVelconditionid = "";
        
        String selectedVeltarget = "";
        String selectedDisptargetvalue = "";
        String selectedVeltargetid = ""; 
        int fieldActionInt = 0;
        String selectedVelReadonlyOrDisable = "";
        String selectedVelSource = "";
        String selectedVelCondition = ""; 

        //get source form fields
            
        fldlibDao = fldActionJB.getFieldsForFieldAction(EJBUtil.stringToNum(formId));
        
        if (EJBUtil.isEmpty(mode) )
        {    mode="N";
        }

        if (EJBUtil.isEmpty(fieldAction))
        {
            fieldAction="";
        }    
        
        fieldActionInt = EJBUtil.stringToNum(fieldAction);
            
        if (mode.equals("M"))
        {  
            ArrayList arVal = new ArrayList();
            String keywordval = "";
            
            fsk = fldActionDaoInfo.getFieldActionDetails(fieldActionInt);
            
                //get data
                 selectedSourceFld = fsk.getFldId();
                 fldActionType = fsk.getFldActionType();
                 fldActionCondition = fsk.getFldActionCondition();        
                 htActionInfo = fsk.getFldActionInfo();            
                 selectedSourceFldInt = EJBUtil.stringToNum(selectedSourceFld);
                
                //get selected responses /conditions for the action field
                
                if (htActionInfo.containsKey("[VELCONDITIONVALUE]"))    
                {
                    
                    arVal = (ArrayList) htActionInfo.get("[VELCONDITIONVALUE]");
                    for (int i = 0; i < arVal.size(); i++)
                    {
                    	keywordval = (String) arVal.get(i);
                    	if (keywordval != null){
	                        selectedVelconditionvalue = selectedVelconditionvalue + "[VELSEP]" + keywordval;
                    	}
                    }        
                    selectedVelconditionvalue  = selectedVelconditionvalue  + "[VELSEP]";
                }    
                
                if (htActionInfo.containsKey("[VELCONDITIONVALUE]"))    
                {                 
                    arVal = (ArrayList) htActionInfo.get("[VELCONDITIONVALUE]");
                    for (int i = 0; i < arVal.size(); i++)
                    {
                    	keywordval = (String) arVal.get(i);
                    	if (keywordval != null){
	                        selectedDispconditionvalue= selectedDispconditionvalue + ",[" + keywordval + "]";
                    	}
                    }        
                    
                     if (selectedDispconditionvalue.length() > 1)
                    {
                        selectedDispconditionvalue  = selectedDispconditionvalue.substring(1,selectedDispconditionvalue.length());
                    }

                        
                }    
                if (htActionInfo.containsKey("[VELCONDITIONID]"))    
                {
                    
                    arVal = (ArrayList) htActionInfo.get("[VELCONDITIONID]");
                    for (int i = 0; i < arVal.size(); i++)
                    {
                        keywordval    = (String) arVal.get(i);
                        if (! EJBUtil.isEmpty(keywordval))
                        {    
                            selectedVelconditionid = selectedVelconditionid + "[VELSEP]" + keywordval;
                        }    
                    }        
                    if (selectedVelconditionid.length() > 0)
                    {
                        selectedVelconditionid= selectedVelconditionid + "[VELSEP]";
                    }    
                
                }    

                if (htActionInfo.containsKey("[VELTARGETID]"))    
                {
                    
                    arVal = (ArrayList) htActionInfo.get("[VELTARGETID]");
                    for (int i = 0; i < arVal.size(); i++)
                    {
                        keywordval    = (String) arVal.get(i);
                        if (! EJBUtil.isEmpty(keywordval))
                        {    
                            selectedVeltargetid = selectedVeltargetid + "[VELSEP]" + keywordval;
                        }    
                    }        
                    if (selectedVeltargetid.length() > 0)
                    {
                        selectedVeltargetid= selectedVeltargetid + "[VELSEP]";
                    }    
                
                }        
                if (htActionInfo.containsKey("[VELTARGET]"))    
                {
                    
                    arVal = (ArrayList) htActionInfo.get("[VELTARGET]");
                    for (int i = 0; i < arVal.size(); i++)
                    {
                        keywordval    = (String) arVal.get(i);
                        if (! EJBUtil.isEmpty(keywordval))
                        {    
                            selectedVeltarget = selectedVeltarget + "[VELSEP]" + keywordval;
                        }    
                    }        
                    if (selectedVeltarget.length() > 0)
                    {
                        selectedVeltarget= selectedVeltarget + "[VELSEP]";
                    }    
                
                }
                
            selectedDisptargetvalue = vfldAction.getSingleFieldActionKeywordField(fieldActionInt,"[VELTARGETID]");

            
                if (htActionInfo.containsKey("[VELREADONLYORDISABLE]"))    
                {
                    
                    arVal = (ArrayList) htActionInfo.get("[VELREADONLYORDISABLE]");
                    keywordval    = "";
                    if (arVal.size() > 0)
                    {
                        keywordval    = (String) arVal.get(0);
                    }        
                    
                    selectedVelReadonlyOrDisable  = keywordval    ;
            
                }
            
            
            if (htActionInfo.containsKey("[VELSOURCE]"))    
                {
                    
                    arVal = (ArrayList) htActionInfo.get("[VELSOURCE]");
                    keywordval    = "";
                    if (arVal.size() > 0)
                    {
                        keywordval    = (String) arVal.get(0);
                    }        
                    
                    selectedVelSource  = keywordval    ;
            
                }    

            if (htActionInfo.containsKey("[VELCONDITION]"))    
                {
                    
                    arVal = (ArrayList) htActionInfo.get("[VELCONDITION]");
                    keywordval    = "";
                    if (arVal.size() > 0)
                    {
                        keywordval    = (String) arVal.get(0);
                    }        
                    
                    selectedVelCondition  = keywordval    ;
            
                }    
                    
                
        }
            
        sourcefieldIds = fldlibDao.getFieldLibId();
        sourcefieldNames = fldlibDao.getFldName();
        sourcefieldSystemIds = fldlibDao.getFldSysID();
        sourcefieldUids =  fldlibDao.getFldUniqId();
        
        String sourceFldName = "";
        String sourceFldSystemId = "";
        Integer sourceFldId = null;
        String sourcefieldUid = "";
        
        int intFldId  = 0;    

        StringBuffer sbSource = new StringBuffer();
        
        ///////////////////////////////////////////
        sbSource.append("<SELECT NAME=\"sourceFldDD\" onChange=\"populateKeyWordParams(document.fldAction)\">") ;

        for (int counter = 0; counter <= sourcefieldIds.size() -1 ; counter++)
        {
            sourceFldId = (Integer) sourcefieldIds.get(counter);
            intFldId =  sourceFldId.intValue() ;
            sourceFldSystemId = ( String) sourcefieldSystemIds.get(counter); 
            sourceFldName = (String) sourcefieldNames.get(counter); 
            sourcefieldUid = (String) sourcefieldUids.get(counter);
            if (StringUtil.isEmpty(sourcefieldUid))
            {
            	sourcefieldUid = "-";
            }
            
            if (selectedSourceFldInt == intFldId )
            {    

                sbSource.append("<OPTION SELECTED value = \""+ intFldId + "[VELSEP]" + sourceFldSystemId +"\">" + sourceFldName + " ["+ sourcefieldUid+"]</OPTION>");            
            }    
            else
            {    
                sbSource.append("<OPTION value = "+ intFldId + "[VELSEP]" + sourceFldSystemId +">" + sourceFldName + " ["+ sourcefieldUid+"]</OPTION>");            
             }                
            
        }
            if (mode.equals("N"))
            {    
                sbSource.append("<OPTION value ='' SELECTED>"+LC.L_Select_AnOption+"</OPTION>");/*sbSource.append("<OPTION value ='' SELECTED>Select an Option</OPTION>");*****/            
            }    
            sbSource.append("</SELECT>");

            
        ////////////////////////////////////////    
            
        StringBuffer sbCondition = new StringBuffer();
        
        String conditionOptions = "";

        conditionOptions = VFieldAction.getActionConditionOptions(fldActionCondition);
            
        sbCondition.append("<SELECT NAME=\"actionCondition\">") ;
        sbCondition.append(conditionOptions);                    
        sbCondition.append("</SELECT>");
        
        ////////////////////////////////////////    
            
        StringBuffer sbAction = new StringBuffer();
        
        sbAction.append("<SELECT NAME=\"velreadonlyordisable\">") ;
        
//        if (selectedVelReadonlyOrDisable.equals()
    
    
        String [] arrDisable = {LC.L_Disabled_Lower,LC.L_Readonly_Lower,LC.L_Hidden_Lower,""};/*String [] arrDisable = {"disabled","readonly","hidden",""};******/
        String [] arrDisableDisp = {LC.L_Disable,LC.L_Read_Only,LC.L_Hide,LC.L_Select_AnOption};/*String [] arrDisableDisp = {"Disable","Read Only","Hide","Select an Option"};*****/
        
        String disableVal = "";
        String disableValDisp = "";
        
        for (int i=0; i < arrDisable.length; i++)
        {
            disableVal = arrDisable[i];
            disableValDisp = arrDisableDisp[i];
            
            if (disableVal.equals(selectedVelReadonlyOrDisable))
            {
                sbAction.append("<OPTION value = \""+ disableVal+"\" SELECTED>"+disableValDisp+"</OPTION>");
            }
            else
            {
                sbAction.append("<OPTION value = \""+ disableVal+"\">"+disableValDisp+"</OPTION>");
            }    
        }
        
        sbAction.append("</SELECT>");
    
    
%>

    <Form name="fldAction" id="fldActionFrm" method="post" action="updateFieldAction.jsp" onsubmit="if (validate(document.fldAction)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
    
    <table width="99%" cellspacing="0" cellpadding="0" border="0" class="midalign">
      <tr>
        <td ><P class="sectionHeadings"> <%=LC.L_Define_FldAction%><%--Define Field Action*****--%> </P> </td>
        
     </tr>   
     </table>
     <br>
 
    
    <Input type="hidden" name="mode" value=<%=mode%>>
    <Input type="hidden" name="formId" value=<%=formId%>>
    <Input type="hidden" name="codeStatus" value=<%=codeStatus%>>
    <Input type="hidden" name="fieldActionId" value=<%=fieldAction%>>  
    <Input type="hidden" name="actionType" value="tempGrey">  

    <Input type="hidden" name="velsource" value="<%=selectedVelSource %>">  
    <Input type="hidden" name="velsourcefldid" value="<%=selectedSourceFldInt%>">      
    <Input type="hidden" name="velcondition" value="<%=selectedVelCondition%>" >      

    <table width="99%" cellspacing="2" cellpadding="2" border="0" class="midalign" >
     <tr> 
      <td width="30%"><%=LC.L_Select_SourceFld%><%--Select Source Field*****--%><FONT class="Mandatory">* </FONT></td>
        <td width="69%"><%=sbSource.toString()%> </td>
      </tr>
       <tr> 
      <td ><%=LC.L_Select_ActionCondition%><%--Select Action Condition*****--%><FONT class="Mandatory">* </FONT></td>
        <td><%=sbCondition.toString()%> </td>
      </tr>  
       <tr> 
      <td ><%=LC.L_Select_Responses%><%--Select Responses*****--%> <FONT class="Mandatory">* </FONT></td>
        <td ><Input type="hidden" name="velconditionvalue" value="<%=selectedVelconditionvalue%>">
        <Input type="text" name="dispconditionvalue" READONLY value="<%=selectedDispconditionvalue%>">
        <Input type="hidden" name="velconditionid"  value="<%=selectedVelconditionid%>">
        <A href= "#" onClick="openConditionValue(document.fldAction)"><%=LC.L_Select%><%--Select*****--%></A>
        </td>
      </tr>  
       <tr> 
      <td ><%=LC.L_Select_TargetFlds%><%--Select Target Fields*****--%> <FONT class="Mandatory">* </FONT></td>
        <td><Input type="hidden" name="veltarget" value="<%=selectedVeltarget%>">
        <Input type="text" name="disptargetvalue" READONLY value="<%=selectedDisptargetvalue%>">
        <Input type="hidden" name="veltargetid"  value="<%=selectedVeltargetid%>">
        <A href= "#" onClick="openTarget(document.fldAction)"><%=LC.L_Select%><%--Select*****--%></A>
        </td>
      </tr>  
      <tr> 
      		<td ><%=LC.L_Select_Action%><%--Select Action*****--%><FONT class="Mandatory">* </FONT></td>
       		<td ><%=sbAction.toString()%></td>
      </tr>
	</table>     
	<br/>
	<% if (pageRight >= 6) { if(codeStatus.equals("WIP")){
						%>
						<table align="center"><tr>
	  <td align="center"><br />
				<button type="submit" id="submit_btn" ondblclick="return false"><%=LC.L_Submit%></button>
			</td> 
	  </tr></table>
     
<%}else{ 
%>

	<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="editBoxFieldFrm"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include> 

<br>
	<% } } %>
	<p class="defComments"><%=MC.M_DoNotEtr_RespForIsNull%><%--Do not enter any 'Responses' for 'Is Null' and 'Is not null' conditions*****--%></p>	
  </Form>
  
<%
    } //end of if body for page right
    else
    {
%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
<%

    } //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
<%

}

%>
<div class = "myHomebottomPanel"> 
 <jsp:include page="bottompanel.jsp" flush="true"/>
 </div> 
</body>

</html>



