<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML><HEAD><TITLE><%=LC.L_Local_SampleSize%><%--Local Sample size*****--%></TITLE>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

</HEAD>

<script>

function validate(formobj) {

 	
 	 	
 	 totalSample = formobj.total.value;
 	 if(totalSample == 1){
 	 if(isNaN(formobj.localSample.value) == true)
		   {
	     		alert("<%=MC.M_ValidNum_SampleSize%>");/*alert("Please enter a valid number in Local Sample Size.");*****/
				 formobj.localSample.focus();
				 return false;
			} 	
		
		}
		else{
			for(i=0;i<totalSample;i++){
			if(isNaN(formobj.localSample[i].value) == true)
		   {
				alert("<%=MC.M_ValidNum_SampleSize%>");/*alert("Please enter a valid number in Local Sample Size.");*****/
				 formobj.localSample[i].focus();
				 return false;
			} 
		}	
			}
			
	if (!(validate_col('e-Signature',formobj.eSign))) return false
     if(isNaN(formobj.eSign.value) == true) {
    	 alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
	   }

	return true;
}
 
 
</script>


<%
int ienet = 2;

String agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1) 
     ienet = 0; //IE
    else
	ienet = 1;
	if(ienet == 0) {	
%>
<body style="overflow:scroll;" >
<%
	} else {
%>
<body>
<%
	}
%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.service.util.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%>

<jsp:useBean id="studySiteB" scope="request" class="com.velos.eres.web.studySite.StudySiteJB"/>


<%
	HttpSession tSession = request.getSession(true); 
	 if (sessionmaint.isValidSession(tSession))
	{
	 %>
	<jsp:include page="sessionlogging.jsp" flush="true"/> 
	<jsp:include page="include.jsp" flush="true"/>

<% 	
	 String accountId = (String) tSession.getValue("accountId");
	 String studyId = request.getParameter("studyId");
	 

         
	 //get study sites with Local sample size
	 StudySiteDao ssDao = new StudySiteDao();
	 ssDao = studySiteB.getStdSitesLSampleSize(EJBUtil.stringToNum(studyId));
	 
 	 ArrayList studySiteIds  = ssDao.getStudySiteIds();
	 ArrayList siteIds = ssDao.getSiteIds();
	 ArrayList siteNames = ssDao.getSiteNames();
	 ArrayList lSampleSizes= ssDao.getLSampleSize();
 	 
	 
	 Integer studySiteId; 
   	 Integer siteId;
	 String siteName="";
	 String lSampleSize="";
	 int total = studySiteIds.size();
	 
  	 
   	
%>
<Form class=formDefault name="studySite" id="studySiteForm" action="updateLSampleSize.jsp" method="post" onSubmit="if (validate(document.studySite)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}" >
<BR>
<P class = "sectionHeadings">		
<%=LC.L_Local_SampleSizeFor%><%--Local Sample Size For*****--%>
</P>	
<table width="100%">

<!--<tr>
   <th width="45%" >Other Study ID Type</th>
   <th width="55%" >Other Study ID</th>				
</tr> -->
<% 
	int counter = 0,cbcount=0;
	for (counter = 0; counter <= studySiteIds.size() -1 ; counter++)
		{
				siteName = (String) siteNames.get(counter); 
 	 			lSampleSize = (String) lSampleSizes.get(counter); 
 	 			if(lSampleSize == null)
 	 				lSampleSize  = "";
				studySiteId = (Integer) studySiteIds.get(counter) ; 
   	 			siteId = (Integer) siteIds.get(counter) ;  
			
	%>
		
	<tr>
		     <td width="10%"></td>
		     <td class=tdDefault width="20%" align=left>
		     <input type="hidden" name="siteId" value=<%=siteId%>> <%=siteName%></td>	
		     <td width="70%"> <input type="text" name=localSample value="<%=lSampleSize%>" maxlength="10"> <%-- YK BUG#5564 --%>
			<input type="hidden" name="studySiteId" value=<%=studySiteId%>> </td>
	</tr>	
	
	<%
		}	
	%>
</table>

	
	<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="studySiteForm"/>
		<jsp:param name="showDiscard" value="N"/>
	</jsp:include>
	
	
<input type = "hidden" name="total" value=<%=total%>>
	</form>
<%}else{%>
<jsp:include page="timeout_childwindow.jsp" flush="true"/> 
<%}%>

<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>

