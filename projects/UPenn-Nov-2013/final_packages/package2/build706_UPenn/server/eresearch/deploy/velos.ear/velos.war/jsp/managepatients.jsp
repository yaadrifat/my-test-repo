<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>

<title><%=LC.L_Mng_Pats%><%--Manage <%=LC.Pat_Patients%>*****--%></title>

<Link Rel=STYLESHEET HREF="common.css" type=text/css>



<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</HEAD>



<% String src;

src= request.getParameter("srcmenu");

%>



<jsp:include page="panel.jsp" flush="true">

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>



<BODY>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="patStudyStat" scope="page" class="com.velos.eres.web.patStudyStat.PatStudyStatJB" />
<jsp:useBean id="studyStatus" scope="page" class="com.velos.eres.web.studyStatus.StudyStatusJB" />


<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.*"%>
<DIV class="browserDefault" id="div1">
  <%HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession))
{
 	String uName = (String) tSession.getValue("userName");
   	String userId = (String) tSession.getValue("userId");


	int pageRight = 0;
	int studyId = 0;
	int stdRight = 0;

//******************GET STUDY TEAM RIGHTS and put the same in session, this will be a separate obj as no study is selected //***************************************************************

	TeamDao teamDao = new TeamDao();

	studyId = EJBUtil.stringToNum(request.getParameter("studyId"));
	tSession.putValue("studyId",request.getParameter("studyId"));
	teamDao.getTeamRights(studyId,EJBUtil.stringToNum(userId));

	ArrayList tId = teamDao.getTeamIds();

	int patdetright = 0;

	if (tId.size() == 0) {

		pageRight=0 ;

	}else
	{

		stdRights.setId(EJBUtil.stringToNum(tId.get(0).toString()));

			ArrayList teamRights ;
			teamRights  = new ArrayList();
			teamRights = teamDao.getTeamRights();

			stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
			stdRights.loadStudyRights();


		tSession.putValue("studyRights",stdRights);

		if ((stdRights.getFtrRights().size()) == 0){

		 	pageRight= 0;
			patdetright = 0;

		}else{

			pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYMPAT"));
			patdetright = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYVPDET"));

			tSession.putValue("studyManagePat",new Integer(pageRight));
			tSession.putValue("studyViewPatDet",new Integer(patdetright));

			//out.print(stdRight);

		}

	}


//**************************************************************************************************************
//********************************************************************************



%>
  <%-- IN THE ABOVE LINE OF CODE THE Id (MSITES) FOR SITES NEED TO REPLACED BY THAT OF Study--%>
  <%

if (pageRight > 0 )

	{

		if(studyStatus.checkStatus(request.getParameter("studyId")) == 0 ) {
%>
  <P class = "userName"> <%= uName %> </P>
  <P class="sectionHeadings"> <%=MC.M_MngPcol_MngPats %><%-- Manage Protocols >> Manage <%=LC.Pat_Patients%>*****--%> </P>
  <Form name="studybrowser" method="post" action="" onsubmit="">
    <table width="100%" cellspacing="0" cellpadding="0" border=0 >
      <tr >
        <td width = "100%">
		          <P class = "defComments"> <%=MC.M_StdInActv_UseStatTab%><%-- This Study is not currently Open (ie active). Please activate the study using the study status tab before managing the <%=LC.Pat_Patients%>.*****--%>
		          </P>
        </td>
      </tr>
    </table>
  </Form>
<%
		} else {

%>
  <P class = "userName"> <%= uName %> </P>
  <P class="sectionHeadings"> <%=MC.M_MngPcol_MngPats %><%-- Manage Protocols >> Manage <%=LC.Pat_Patients%>*****--%> </P>
  <Form name="studybrowser" method="post" action="" onsubmit="">
    <table width="100%" cellspacing="0" cellpadding="0" border=0 >
      <tr >
        <td width = "30%">
            <A href = "enrollpatientsearch.jsp?searchFrom=initial&srcmenu=<%=src%>"><img src="../images/jpg/Enroll.gif" border=none></A>
		</td>
        <td width = "30%">
          <A type="submit" href = "patientsearch.jsp?openMode=F&srcmenu=tdmenubaritem3&patid=&patstatus="  onClick="return f_check_perm(<%=pageRight%>,'E')"><%=LC.L_Search%></A>
		  </td>
		  <td width = "30%">
		   <A href = "patientdetails.jsp?srcmenu=tdmenubaritem3&mode=N&selectedTab=1&pkey=&page=patient" onClick="return f_check_perm(<%=pageRight%>,'N')"><img src="../images/jpg/Register.gif" border=none></A>
		   </td>
		   </tr>
		   <tr>
		   <td width = "30%">  <P class = "defComments">
            <%=MC.M_EnrlPat_ToStd%><%-- Enroll a <%=LC.Pat_Patient_Lower%> to a study*****--%></P>
			</td>
			<td width = "30%">  <P class = "defComments">
		  <%=MC.M_Pat_ViewDmgrpg%><%-- Search a <%=LC.Pat_Patient_Lower%> and view demographics*****--%> </P>
		  </td>
 			<td width = "30%"><P class = "defComments">
			<%=MC.M_RegPat_BeforeEnrl%><%-- Register a <%=LC.Pat_Patient_Lower%> before enrollment*****--%> </P>
			</td>
		  </tr>
	      <tr>
    	    <td height="10" colspan=3></td>
	      </tr>

		  <tr><td width=100% colspan=3>
          <P class = "defComments"><%=MC.M_PatRegStd_PatMayReg%><%-- <%=LC.Pat_Patients%> must be registered in the system before
            they can be enrolled to a Study.<br>
            In case you do not find your <%=LC.Pat_Patient_Lower%> in the <%=LC.Pat_Patient%> Search results
            page, your <%=LC.Pat_Patient_Lower%> may not be registered.*****--%><br>
            <br>
             </P>
        </td>
      </tr>
      <tr>
        <td height="10" colspan=3></td>
      </tr>
    </table>
  </Form>



  <Form name="results" method="post">
<%

	PatStudyStatDao pssdao = patStudyStat.getStudyPatients(request.getParameter("studyId"));
	ArrayList patientIds = pssdao.getPatientIds();
	ArrayList patientEnrollDates = pssdao.getPatientEnrollDates();
	ArrayList patientCodes = pssdao.getPatientCodes();
	ArrayList patientLnames = pssdao.getPatientLnames();
	ArrayList patientFNames = pssdao.getPatientFNames();
	ArrayList patientStats = pssdao.getPatientStats();
	ArrayList patientProtIds = pssdao.getPatientProtIds();
	int length = pssdao.getCRows();
	if (length == 0) {
%>
	<P class="defComments"><%=MC.M_NoPat_AssignedToStd%><%--No <%=LC.Pat_Patients_Lower%> have been assigned to this study.*****--%></P>
<%
	} else {
	   int i = 0;
	   String patientId = null;
	   String patientEnrollDate = null;
	   String patientCode = null;
	   String patientLname = null;
	   String patientFName = null;
	   String patientStat = null;
	   String patientProtId = null;
	%>
	<P class="defComments"><%=MC.M_ListPat_CurrEnrlStd%><%-- The list below displays the <%=LC.Pat_Patients_Lower%> that are currently enrolled in
            this Study. Click on '"+<%=LC.Pat_Patient%>+" ID' to view the details.*****--%></P>
    <table class=tableDefault width="100%" border=0>
      <tr>
        <th width=15% align =center><%=LC.L_Patient_Id%><%--<%=LC.Pat_Patient%> ID*****--%></th>
	  <th width=15% align =center><%=LC.L_Enrolled_On%><%--Enrolled On*****--%></th>
	  <th width=15% align =center><%=LC.L_Status%><%--Status*****--%></th>
   </tr>
<%

		for(i = 0 ; i < length; i++)
	  	{
	 	   patientId = patientIds.get(i).toString();
	 	   patientEnrollDate = (String) patientEnrollDates.get(i);
	 	   patientCode = (String) patientCodes.get(i);
	 	   patientLname = (String) patientLnames.get(i);
	 	   patientFName = (String) patientFNames.get(i);
		   patientStat = (String) patientStats.get(i);
		   patientProtId = (String) patientProtIds.get(i);
	 	   patientEnrollDate =	DateUtil.dateToString(java.sql.Date.valueOf(patientEnrollDate.substring(0,10)));

	 	   if ((i%2)==0) {
%>
      <tr class="browserEvenRow">
<%
	 		   }else{
%>
      <tr class="browserOddRow">
<%
	 		   }
%>
        <td width =15%> <A href ="enrollpatient.jsp?srcmenu=tdMenuBarItem3&selectedTab=2&mode=M&pkey=<%=patientId%>&patProtId=<%=patientProtId%>&patientCode=<%=patientCode%>&page=patientEnroll" > <%=patientCode%> </A> </td>
        <td width =15%> <%=patientEnrollDate%> </td>
        <td width =10%> <%=patientStat%> </td>
      </tr>
	  <%

	  }// end for for loop
	  } // end of if for length == 0
	  %>
    </table>

  </Form>



  <%
	} // end of if for study status
	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<DIV class="mainmenu" id = "emenu">
  <jsp:include page="menus.htm" flush="true"/>
</DIV>
</body>

</html>



