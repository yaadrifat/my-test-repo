/**
 * General utility functions 
 */


/*
 * Returns client brower's IE version in float but usually in integer; 0 if not IE. 
 */
function getIEVersionNumber() {
	var ua = navigator.userAgent;
    var MSIEOffset = navigator.userAgent.indexOf("MSIE ");
    if (MSIEOffset == -1) { return 0;}
    return parseFloat(ua.substring(MSIEOffset + 5, ua.indexOf(";", MSIEOffset)));
}

/*
 * Returns true if IE6; false otherwise. 
 */
function isIE6Browser() {
	var isIE6 = false;
	var thisIE = getIEVersionNumber();
	if (thisIE >= 6.0 && thisIE < 7) { isIE6 = true; }
	return isIE6;
}

/**
 * This gets the value of the string from the message bundle based on the key. It first looks for
 * the key-value pair in the custom bundle. If it does not exist there, it looks for the same in
 * the default bundle. 
 * 
 * @param key - as defined in the properties of the message bundle
 * @param argArray - an array containing any number of arguments that can be passed in to replace the
 *        parameters ({0}, {1},...) in the value string
 * @return - value for the specified key. If either the bundle or key-value does not exist,
 *        it returns a "" string. It does not throw an exception.
 */
function getLocalizedMessageString(key, argArray) {
    if (key == undefined) { return ""; }
    
    var msgString = eval(key);
    
    if (argArray){
	    if (argArray.length != 0 ){
	    	for (var i=0; i<argArray.length; i++){
	    		msgString = msgString.replace("{"+i+"}",argArray[i]);
	    	}
	    }
    }
    return msgString;
}

function fnGetEventMouseOver(eventId, eventType, source, studyId) {
	var eMODiv = document.getElementById("eMO"+eventId);
	studyId = (!studyId)? "":studyId;
	studyId = (studyId == "null")? "":studyId;
	
	if (!eMODiv){
		eMODiv = document.createElement('div');
		eMODiv.setAttribute('id', "eMO"+eventId);
		eMODiv.setAttribute('name', "eMO"+eventId);
		eMODiv.style.display = "none";
		document.body.appendChild(eMODiv);
		
		//jQuery.ajax({ url:"getEventMouseOver.jsp?eventId="+eventId+"&calledFrom="+calledFrom });
		var ao=new VELOS.ajaxObject("getEventMouseOver.jsp", {
			urlData:"eventId="+eventId+"&eventType="+eventType+"&source="+source+"&studyId="+studyId,
			reqType:"POST",
			outputElement: "eMO"+eventId,
			success: " eval(document.getElementById('eMO"+eventId+"').innerHTML); " 
			}
		);
		ao.startRequest();
	} else {
		eval(document.getElementById("eMO"+eventId).innerHTML);
	}
}

function fnGetPatientMouseOver(patientPk, studyId) {
	var patMODiv = document.getElementById("patMO"+patientPk);
	studyId = (!studyId)? "":studyId;
	studyId = (studyId == "null")? "":studyId;
	
	if (!patMODiv){
		patMODiv = document.createElement('div');
		patMODiv.setAttribute('id', "patMO"+patientPk);
		patMODiv.setAttribute('name', "patMO"+patientPk);
		patMODiv.style.display = "none";
		document.body.appendChild(patMODiv);
		
		//jQuery.ajax({ url:"getEventMouseOver.jsp?patientPk="+patientPk+"&calledFrom="+calledFrom });
		var ao=new VELOS.ajaxObject("getPatientMouseOver.jsp", {
			urlData:"patientId="+patientPk+"&studyId="+studyId,
			reqType:"POST",
			outputElement: "patMO"+patientPk,
			success: " eval(document.getElementById('patMO"+patientPk+"').innerHTML); " 
			}
		);
		ao.startRequest();
	} else {
		eval(document.getElementById("patMO"+patientPk).innerHTML);
	}
}

function fnGetStudyMouseOver(studyId) {
	studyId = (!studyId)? "":studyId;
	studyId = (studyId == "null")? "":studyId;
	var studyMODiv = document.getElementById("stdMO"+studyId);
	
	if (!studyMODiv){
		studyMODiv = document.createElement('div');
		studyMODiv.setAttribute('id', "stdMO"+studyId);
		studyMODiv.setAttribute('name', "stdMO"+studyId);
		studyMODiv.style.display = "none";
		document.body.appendChild(studyMODiv);

		var ao=new VELOS.ajaxObject("getStudyMouseOver.jsp", {
			urlData:"studyId="+studyId,
			reqType:"POST",
			outputElement: "stdMO"+studyId,
			success: " eval(document.getElementById('stdMO"+studyId+"').innerHTML); " 
			}
		);
		ao.startRequest();
	} else {
		eval(document.getElementById("stdMO"+studyId).innerHTML);
	}
}