<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.velos.eres.service.util.LC" %>

<%--Include JSPs --%>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<jsp:include page="velos_includes.jsp"></jsp:include>

<jsp:include page="studySummaryInclude.jsp" flush="true"/>
<jsp:include page="newStudyIdsInclude.jsp" flush="true"/>
<%----%>
<%!
static final String studyScreen_AdminGrps = "[Config_StudyScreen_AdminGrps]";
static final String studyScreen_TeamRoles = "[Config_StudyScreen_TeamRoles]";
%>
<div id="submitFailedDialog" title="<%=LC.L_Error%>" class="comments" style="display:none; overflow:auto">
	<table align="center">
		<tr><td height="20px">&nbsp;</td></tr>
		<tr align="center">
			<td id="errorMessageTD"></td>
		</tr>
		<tr><td height="20px">&nbsp;</td></tr>
		<tr align="center">
			<td>
				<button onclick='$j("#submitFailedDialog").dialog("close");'><%=LC.L_Close%></button>
			</td>
		</tr>
	</table>
</div>
<script>
var studyScreenFunctions = {
	studyScrnIsAdmin: false,
	studyScrnAdminGrps:"<%=((studyScreen_AdminGrps.equals(LC.Config_StudyScreen_AdminGrps)) ? 
			"" : LC.Config_StudyScreen_AdminGrps)%>",
	studyScrnTeamRoles: "<%=((studyScreen_TeamRoles.equals(LC.Config_StudyScreen_TeamRoles))? 
			"" : LC.Config_StudyScreen_TeamRoles)%>",
	validateStudyScreen: {}
};

{
	var grp = ''+studyFunctions.defUserGroup;
	var studyScrnAdminGrps = (studyScreenFunctions.studyScrnAdminGrps).split(",");
	studyScreenFunctions.studyScrnIsAdmin = false;
	
	for (var indx=0; indx < studyScrnAdminGrps.length; indx++){
		if (grp == jQuery.trim(studyScrnAdminGrps[indx])){
			studyScreenFunctions.studyScrnIsAdmin = true;
			break;
		}
	}
}
</script>

<script type="text/javascript">
var isValidatedForm = false;
studyScreenFunctions.validateStudyScreen = function() {
	//Call all the validate functions one-by-one
	if(!studyFunctions.validate(studyFunctions.formObj, studyFunctions.autoGenStudy, studyFunctions.sessAccId)){
		return false;
	}
	
	if (!moreStudyDetFunctions.validate(moreStudyDetFunctions.formObj)){ 
		return false;
	}

	if (studyScreenJS && studyScreenJS.validate){
		if (!studyScreenJS.validate()){
			return false;
		}
	}

	if (isValidatedForm) { return true; }
	isValidatedForm = true;
	
	$j.post('updateStudyScreen',
		$j('#studyScreenForm').serialize(),
		function(data) {
			var errorMap = data.errorMap;
			var hasErrors = false;
			for (var key in errorMap) {
				hasErrors = true;
				isValidatedForm = false;
				$j('#errorMessageTD').html(errorMap[key]);
				break;
			}
			if (hasErrors) {
				$j('#submitFailedDialog').dialog({
					modal:true,
					closeText: '',
					close: function() {
						$j("#submitFailedDialog" ).dialog("destroy");
					}
				});
			} else {
				$("studyScreenForm").onSubmit=null;
				$("studyScreenForm").action ="genericMessage.jsp?messageKey=M_Data_SvdSucc&id=5";
				$("studyScreenForm").submit();
				$("studyScreenForm").action = '#';
				$("studyScreenForm").onSubmit = "return studyScreenFunctions.validateStudyScreen();";
			}
		}
	);
	return false;
};
</script>

<%--Custom JS inclusion --%>
<script type="text/javascript" src="./js/velosCustom/studyScreen.js"></script>
<%----%>