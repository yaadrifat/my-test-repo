<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.json.*"%>
<%@ page language = "java" import="java.util.*,com.velos.eres.service.util.*,com.aithent.audittrail.reports.AuditUtils"%>
<jsp:useBean id="InvB" scope="request" class="com.velos.eres.web.invoice.InvoiceJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/> 

<% 
request.setCharacterEncoding("UTF-8");
response.setContentType("text/html");

	String src;
	src= request.getParameter("srcmenu");
	//System.out.println("src => "+src);

String invoiceId= "";
String selectedTab = request.getParameter("selectedTab");
//System.out.println("selectedTab 1=> "+selectedTab);
ArrayList resulT = new ArrayList();


HttpSession tSession = request.getSession(true); 
String oldESign = (String) tSession.getAttribute("eSign");
String eSign = request.getParameter("eSign");
//System.out.println("eSign 1=> "+eSign);
	if (!sessionmaint.isValidSession(tSession)) {
		// Session is invalid; print an error and exit
		response.sendError(HttpServletResponse.SC_FORBIDDEN, "SESSION_TIME_OUT");
		HashMap mapData = new HashMap();
		mapData.put("result", -1);
		mapData.put("resultMsg", MC.M_UsrNot_LoggedIn);/*jsObj.put("resultMsg", "User is not logged in.");*****/
		resulT.add(mapData);
		JSONArray jsAllMenuOptions = new JSONArray();
		for (int iX=0; iX<resulT.size(); iX++) {
			jsAllMenuOptions.put(EJBUtil.hashMapToJSON((HashMap)resulT.get(iX)));
		}
			out.println(jsAllMenuOptions.toString());
			
		return;	
	}else {
		HashMap mapData = new HashMap();

		invoiceId= request.getParameter("invoiceId");

		String studyId = request.getParameter("studyId");

		String invcName = request.getParameter("invcName");

		//System.out.println("invoiceId 1=> "+invoiceId);
		studyId=request.getParameter("studyId");	
		//System.out.println("studyId 1=> "+studyId);

		int ret=0;
			
		String delMode=request.getParameter("delMode");

		invoiceId= request.getParameter("invoiceId");			
		//System.out.println("invoiceId => "+invoiceId);
		studyId=request.getParameter("studyId");	
		//System.out.println("studyId => "+studyId);         

		//delMode=null;
		InvB.setId(EJBUtil.stringToNum(invoiceId));

		if(delMode==null || delMode=="" || !delMode.equalsIgnoreCase("true"))
			ret = InvB.deleteInvoice(AuditUtils.createArgs(tSession,"",LC.L_Milestones));
		else
			ret = InvB.selectInvoiceReconciledOrNot(AuditUtils.createArgs(tSession,"",LC.L_Milestones));

		if (ret==-3) {
%>
<input type="hidden" id="resultMsg" value="<%=invoiceId%>">
<input type="hidden" id="message" value="<%=MC.M_NotDelete%>">
<input type="hidden" id="invcName" value="<%=invcName%>">
<%
		}else if (ret==-2) {
%>
<input type="hidden" id="resultMsg" value="<%=invoiceId%>">
<input type="hidden" id="message" value="<%=MC.M_Failed%>">
<input type="hidden" id="invcName" value="<%=invcName%>">
<%
		}else { 
%>
<input type="hidden" id="resultMsg" value="<%=invoiceId%>">
<input type="hidden" id="message" value="">
<input type="hidden" id="invcName" value="<%=invcName%>">
<%
		}

  }//end of if body for session 
%> 


