<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=MC.M_Fld_SpcTypSubmit%><%--Field -Space Type Submit*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="fieldLibJB" scope="request"  class="com.velos.eres.web.fieldLib.FieldLibJB"/>
<jsp:useBean id="formSecJB" scope="request"  class="com.velos.eres.web.formSec.FormSecJB"/>
<jsp:useBean id="formFieldJB" scope="request"  class="com.velos.eres.web.formField.FormFieldJB"/>


<body><%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.*,com.velos.eres.web.grpRights.GrpRightsJB, com.velos.eres.business.fieldLib.impl.FieldLibBean"%>
<DIV class="popDefault" id="div1"> 

<%
	HttpSession tSession = request.getSession(true);
	String codeStatus = request.getParameter("codeStatus");
	if (sessionmaint.isValidSession(tSession))
	{
		
%>
	<jsp:include page="sessionlogging.jsp" flush="true"/>
   
<%		String eSign= request.getParameter("eSign");
		String oldESign = (String) tSession.getValue("eSign");
	  	 if((codeStatus.equals("WIP")) || ((!codeStatus.equals("WIP") && !codeStatus.equals("")) && (oldESign.equals(eSign)))){
			int errorCode=0;
			String mode=request.getParameter("mode");
			String fieldLibId="";
			
			String fldLength="";
			String fldType="S";
			String fldInstructions = "";
		
			String accountId="";
			String fldLibFlag="F";
			String fldBrowserFlg="0" ;
			String fldName="";
			
			
			// THE STYLE OBJECT WHICH TAKES VALUES OF ATTRIBUTES OF THE FIELD  
			// LIKE BOLD, SAMELINE, ALIGN , ITALICS
			Style aStyle = new Style();
			String formId="";
			String formFldId="";
			String formSecId="";
			
			String fldSeq="";
			
			String samLinePrevFld="";
			
			String creator="";
			String ipAdd="";
			int fldLengthNum = 0;
			String labelDisplayWidth = "15";
		
			creator=(String) tSession.getAttribute("userId");
			ipAdd=(String)tSession.getAttribute("ipAdd");
	     	accountId=(String)tSession.getAttribute("accountId");
			
			mode=request.getParameter("mode");
			String recordType=mode;  
			StringBuffer sbSpace = new StringBuffer();
			
	
			formId=request.getParameter("formId");
			fldLength=request.getParameter("fldLength");
			
			if (! StringUtil.isEmpty(fldLength))
			{
				fldLengthNum = EJBUtil.stringToNum(fldLength);
			}
			
			fldName="Space Break" ;
			
			//generate space
			//for (int i = 0; i < fldLengthNum; i++)
			//	{
					sbSpace.append("&#xa0;");
			//	}
			
			/*if (fldLengthNum <= 15 )	
			{
				labelDisplayWidth = String.valueOf(fldLengthNum);
			}*/		
			
			labelDisplayWidth = String.valueOf(fldLengthNum);
				
			fldInstructions = sbSpace.toString();
			
						
			formSecId=request.getParameter("section");
			fldSeq=request.getParameter("sequence");
						
			samLinePrevFld=request.getParameter("sameLine");
						
			//  We SET THE STYLE WE CHECK ITS VALUES TO BEFORE SENDING TO THE DB
			
			if (samLinePrevFld==null)
			aStyle.setSameLine("0");
			else if (samLinePrevFld.equals("1"))
			aStyle.setSameLine("1");
	
			FieldLibBean fieldLsk = new FieldLibBean();
			
			fieldLsk.setFormId(formId );
			
			
			fieldLsk.setFldInstructions(fldInstructions);
			fieldLsk.setFldLength(fldLength);
			
			fieldLsk.setFldType(fldType);
			fieldLsk.setIpAdd(ipAdd);
			fieldLsk.setRecordType(recordType);
			fieldLsk.setAccountId(accountId);
			fieldLsk.setLibFlag(fldLibFlag);
			fieldLsk.setFldName(fldName);
		
			fieldLsk.setFormFldBrowserFlg(fldBrowserFlg);
			
			fieldLsk.setFormSecId(formSecId );
			fieldLsk.setFormFldSeq( fldSeq );
			
		
			fieldLsk.setAStyle(aStyle);
			fieldLsk.setFldDisplayWidth(labelDisplayWidth);
			
			
			if (mode.equals("N"))
			{
				
				fieldLsk.setCreator(creator);
				errorCode=fieldLibJB.insertToFormField(fieldLsk);
				
			}

			else if (mode.equals("M"))
			{
			
		
				
				formFldId=request.getParameter("formFldId");
				formFieldJB.setFormFieldId(EJBUtil.stringToNum(formFldId));
				formFieldJB.getFormFieldDetails();
				
				fieldLibId=formFieldJB.getFieldId();
								
				fieldLibJB.setFieldLibId(EJBUtil.stringToNum(fieldLibId) );
				fieldLibJB.getFieldLibDetails();
				fieldLsk  = fieldLibJB.createFieldLibStateKeeper();
				 	
				
				aStyle.setSameLine(samLinePrevFld);		
				
				fieldLsk.setFormId(formId );
				fieldLsk.setFormFldId(formFldId );
				fieldLsk.setFldInstructions(fldInstructions);
				fieldLsk.setFldLength(fldLength);
				fieldLsk.setFldType(fldType);
				fieldLsk.setIpAdd(ipAdd);
				fieldLsk.setRecordType(recordType);
				fieldLsk.setAccountId(accountId);
				fieldLsk.setLibFlag(fldLibFlag);
				fieldLsk.setFldName(fldName);
			
				fieldLsk.setModifiedBy(creator);
				fieldLsk.setFormSecId(formSecId );
				fieldLsk.setFormFldSeq( fldSeq );
				
				fieldLsk.setAStyle(aStyle);
				fieldLsk.setFldDisplayWidth(labelDisplayWidth);
				
				errorCode=fieldLibJB.updateToFormField(fieldLsk);
				
						
		 }
		
		
		if ( errorCode == -2 || errorCode == -3 )
		{
%>				
				<br><br><br><br><br><p class = "successfulmsg" align = center>		
	 			<p class = "successfulmsg" ><%=MC.M_Data_NotSvdSucc%><%--Data not saved successfullyl*****--%> </p>

<%
		
		} //end of if for Error Code
		else
		{
%>	
		
<%
		
%>		

		<br><br><br><br><br>
		<p class = "successfulmsg" align = center><%=MC.M_Data_SavedSucc%><%--Data Saved Successfully*****--%> </p>
		<script>
			window.opener.location.reload();
			setTimeout("self.close()",1000);
		</script>		
			
<%
		}//end of else for Error Code Msg Display
			}//end of if old esign
			else{  
		%>
			<jsp:include page="incorrectesign.jsp" flush="true"/>
		<%	  
					}//end of else of incorrect of esign */
		     }//end of if body for session 	
			
			else
	{

%>
		  <jsp:include page="timeout.html" flush="true"/>
<%
	
} //end of else of the Session 
 

%>

</body>

</html>
