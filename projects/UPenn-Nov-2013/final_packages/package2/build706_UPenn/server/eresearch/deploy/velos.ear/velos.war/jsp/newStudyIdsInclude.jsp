<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC" %>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%
HttpSession tSession = request.getSession(true);
if (!sessionmaint.isValidSession(tSession))
	return;

String accId = (String) tSession.getAttribute("accountId");
%>
<script>
var moreStudyDetFunctions = {
	formObj: {},

	openLookup: {},
	openMultiLookup: {},
	moreStudyDetCodeJSON: {},
	getMoreStudyDetCode: {},
	getMoreStudyDetCodePK: {},
	getMoreStudyDetField: {},

	setValue4ChkBoxGrp: {},
	setValue: {},
	setDD: {},
	validate: {},
	fixTextAreas: {}
};

jQuery(document).ready(function() {
	moreStudyDetFunctions.formObj = document.studyScreenForm;
	moreStudyDetFunctions.setDD(moreStudyDetFunctions.formObj);
	moreStudyDetFunctions.fixTextAreas();
});

moreStudyDetFunctions.fixTextAreas = function(){
	
	//Maximum possible database limit is 4000 charcters
	var characters= 4000;
	$j(".mdTextArea").keyup(function(){
	    
		if($j(this).val().length > characters){
	        $j(this).val($j(this).val().substr(0, characters));
		}	
	});
}

moreStudyDetFunctions.openLookup = function (viewId, keyword){
	if (!viewId) return;
	var dfilter = '';
	var formName = moreStudyDetFunctions.formObj.name;
	windowName = window.open("getlookup.jsp?viewId="+viewId+"&form="+formName+"&dfilter="+dfilter+"&keyword="+keyword,"Information",
			"toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=700,height=550 top=100,left=100 0, ");
	windowName.focus();
};

moreStudyDetFunctions.openMultiLookup = function (viewId, keyword){
	if (!viewId) return;
	var dfilter = '';
	var formName = moreStudyDetFunctions.formObj.name;
	windowName = window.open("multilookup.jsp?viewId="+viewId+"&form="+formName+"&seperator=,&dfilter="+dfilter+"&keyword="+keyword,"Information",
			"toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=700,height=550 top=100,left=100 0, ");
	
	//http://66.237.42.119/velos/jsp/multilookup.jsp?viewId=6000&form=reports&seperator=,&defaultvalue=[ALL]&keyword=dummy|USRFNAME|[VELHIDE]~dummy1|USRLNAME|[VELHIDE]~paramuserId|USRPK|[VELHIDE]~seluserId|[VELEXPR]*[VELKEYWORD=USRLNAME]*[VELSTR=;]*[VELSPACE]*[VELKEYWORD=USRFNAME]
	windowName.focus();
};

moreStudyDetFunctions.getMoreStudyDetCode = function(codeSubType){
	 return moreStudyDetFunctions.moreStudyDetCodeJSON[codeSubType];
};

moreStudyDetFunctions.getMoreStudyDetCodePK = function(codeSubType){
	var moreStudyDetJSON = moreStudyDetFunctions.moreStudyDetCodeJSON[codeSubType]; 
	if (!moreStudyDetJSON) return -1;

	return moreStudyDetFunctions.moreStudyDetCodeJSON[codeSubType].pk;
};

moreStudyDetFunctions.getMoreStudyDetField = function(codeSubType){
	var pk = moreStudyDetFunctions.getMoreStudyDetCodePK(codeSubType);
	if (pk < 0) return null; 
	return document.getElementsByName('alternateId'+pk)[0];
};

moreStudyDetFunctions.setValue4ChkBoxGrp = function(obj,iElementId) {
	var chkFlds = document.getElementsByName('alternateId'+iElementId+"Checks");
	var hiddenInputFld =  document.getElementById('alternateId'+iElementId);
	hiddenInputFld.value = '';
	for (var indx = 0; indx < chkFlds.length; indx++){
		if (chkFlds[indx].checked){
			var checkValue = chkFlds[indx].value;
			if (hiddenInputFld.value.length==0){
				hiddenInputFld.value = checkValue;
			} else {
				hiddenInputFld.value += ','+checkValue;
			}
		}
	}
};

moreStudyDetFunctions.setValue = function(formobj,iElementId,cbcount) {
	var chkFld = formobj['alternateId'+iElementId];
	var value = chkFld.value;
	if (value=="Y") {
		chkFld.value="N";
		chkFld.checked=false;
	} else if ((value.length==0) || (value=="N"))  {
		chkFld.value="Y";
		chkFld.checked=true;
	} else { // <== there is some junk data in the DB column already
		chkFld.value="Y";
		chkFld.checked=true;
	}
};

moreStudyDetFunctions.setDD = function(formobj){
	//var optvalue=document.getElementsByName("ddlist")[0].value;
	
	if (!document.getElementsByName("ddlist")[0]) return;
	var optvalue=formobj.ddlist.value;
	if (optvalue.length>0)
	{
		var arrayofStrings=optvalue.split("||");
		if (arrayofStrings.length>1) {
			for (var j=0;j<arrayofStrings.length;j++)
			{
				var ddStr=arrayofStrings[j];
				var arrayofDD=ddStr.split(":");
				var ddcount=arrayofDD[0];
				var selvalue=arrayofDD[1];
				var ddFld = formobj['alternateId'+ddcount];
				ddFld.id = ddFld.name;
				if (ddFld && ddFld.options) {
					var opt = ddFld.options;
					for (var i=0;i<opt.length;i++){
						if (opt[i].value==selvalue){
							ddFld.selectedIndex=i ;
						}
					}
				}
			}
		} else {
			var ddStr=arrayofStrings[0];
			var arrayofDD=ddStr.split(":");
			var ddcount=arrayofDD[0];
			var selvalue=arrayofDD[1];
			var ddFld = formobj['alternateId'+ddcount];
			ddFld.id = ddFld.name;
			if (ddFld && ddFld.options) {
			 	var opt = ddFld.options; 
				if (opt == undefined)
			    	opt = ddFld.options; 
				for (var i=0;i<opt.length;i++){
					if (opt[i].value==selvalue){
						ddFld.selectedIndex=i ;
					}
				}
			}
		}// end else
	}//optvalue.length>0	
};

moreStudyDetFunctions.validate = function(formobj) {
	for (oKey in moreStudyDetFunctions.moreStudyDetCodeJSON){
		var fieldJSON = moreStudyDetFunctions.moreStudyDetCodeJSON[oKey];
		if (fieldJSON && fieldJSON.fieldType == 'date'){
			var fld = document.getElementById('alternateId'+fieldJSON.pk);
			if (!validate_date_optionalmsg(fld, true)){
				fld.focus();
				return false;
			}
		}
	}
 	if (!(validate_col('e-Signature',formobj.eSign))) return false;

	if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
	}	
	return true;
};
</script>
