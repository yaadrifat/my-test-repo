<!DOCTYPE html>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@page import="com.velos.eres.ctrp.web.CtrpDraftJBHelper"%>
<%
String dnld=Configuration.DOWNLOADSERVLET ;
int result = 0;
String zipFileName = null;
if(null != request.getAttribute("result"))
	 result = (Integer) request.getAttribute("result");
if(null != request.getAttribute("zipFileName"))
	zipFileName = (String) request.getAttribute("zipFileName");

	String fileName = zipFileName;
%>

<%@page import="com.velos.eres.service.util.LC"%>
<%@page import="com.velos.eres.service.util.MC"%>
<%@page import="com.aithent.file.uploadDownload.Configuration"%><html>
<head>
<title><%=LC.L_DownloadCTRPDraft%><%--Download CTRP Draft*****--%>
</title>
<script>
function downloadFile(){
	document.openCSV.file.value = "<%=zipFileName%>";
	document.openCSV.dnldurl.value="<%=dnld%>";
	document.openCSV.moduleName.value="ctrpDraft";
	document.openCSV.action="postFileDownload.jsp";
	//formobj.action=dnldurl;
	
	document.openCSV.target = "_filedownload";
	document.openCSV.method = "POST";
	document.openCSV.submit();
	
}
</script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:include page="localization.jsp" flush="true" />
<jsp:include page="include.jsp" flush="true" />
<jsp:include page="ui-include.jsp" flush="true" />
</head>
<body onload="downloadFile()">
<s:form name="openCSV" action="openCSV">
	<%--Hidden Field For File Type --%>
	<input type="hidden" name="zipFileName" value="<%=fileName%>" />
	<input type="hidden" name="result" value="<%=result%>" />
	
	<input type="hidden" name="file" value="<%=fileName%>" />
	<input type="hidden" name="dnldurl" value="<%=dnld%>" />
	<input type="hidden" name="moduleName" value="ctrpDraft" />
	<table width="80%">
	<tr height="40px"></tr>
		<tr align="center">
			<%if (result==1) {%>
			<td colspan="2"><%=MC.CTRP_CSV_GENERATED%><br />
			<%=MC.CTRP_CSV_WARNING %></td>
			<%} else {%>
			<td colspan="2"><%=MC.CTRP_CSV_NOT_GENERATED%></td>
			<%}%>
		</tr>
		<tr height="20px"></tr>
		<tr align="center">
			<td colspan="2">
			<%if (result==1) {	%>
			<input class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-button-text" type="submit" value="<%=LC.L_Download%>" name="<%=LC.L_Download%>"/> &nbsp;&nbsp;&nbsp;
			<%}%>
			<input class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-button-text" type="button" onclick="javascript:window.close();" value="<%=LC.L_Close%>" name="<%=LC.L_Close%>"></input>
			</td>

		</tr>
	</table>
	<!--Commented for Bug#15883 
	<script>
	downloadFile();
	</script> -->
</s:form>
<div><jsp:include page="bottompanel.jsp" flush="true" /></div>
</body>
</html>
