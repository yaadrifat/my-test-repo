<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.web.storage.StorageJB,com.velos.eres.business.common.CodeDao,com.velos.eres.service.util.*,com.aithent.audittrail.reports.AuditUtils,com.velos.eres.web.storageStatus.StorageStatusJB"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<jsp:useBean id="storageJB" scope="request" class="com.velos.eres.web.storage.StorageJB"/>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>


<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
}
function goBack() {
	if (navigator.userAgent.indexOf("Firefox")>-1) {window.history.go(-2);} else {history.go(-1);}
}
</SCRIPT>

<% String src;
	src= request.getParameter("srcmenu");
%>
<jsp:include page="skinChoser.jsp" flush="true"/>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<BODY>
<br>

<DIV class="formDefault" id="div1">
<%



HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession))	{
	String delMode=request.getParameter("delMode");
	String selStrs = request.getParameter("selStrs");
	String from_page = request.getParameter("fromPg");
	from_page =(from_page==null)?"":from_page;


String usr = null;
usr = (String) tSession.getValue("userId");

	int flag =1;

	if (delMode==null) {
		delMode="final";
%>


	<%
	StringTokenizer idStrgs=new StringTokenizer(selStrs,",");
	int Ids=idStrgs.countTokens();
	String[] strArrStrgs = new String[Ids] ;

	for(int cnt=0;cnt<Ids;cnt++)
	{
					strArrStrgs[cnt] = idStrgs.nextToken();
	}
	int k = storageJB.deleteStorages(strArrStrgs,flag,EJBUtil.stringToNum(usr));
	if(k == -4)
	{
%>
	<br><br><br><br><br>
	<table align="center">
	<tr>
	<td align ="center">
	<p class="successfulmsg"><%=MC.M_SelStorChg_RemSpmenRec%><%--The selected Storage Unit(s) cannot be changed. There are one or more Storage or child storage records linked with specimen(S). Please remove the association with the related specimen records and try again later.*****--%><p>
      </td>
	  </tr>
	  <tr height=20></tr>
	  <tr>
	  <td align="center">
	  <button onclick="goBack();"><%=LC.L_Back%></button>
	  </td>
	  <tr>
	  </table>
<%}else {%>
	<FORM name="deleteStorages" id="delStoragesFrm" method="post" action="deletestorages.jsp?srcmenu=<%=src%>" onSubmit="if (validate(document.deleteStorages)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

	<br><br>

	<TABLE width="98%" cellspacing="0" cellpadding="0" >
		<tr><td align="center">
		<b><%=MC.M_PlsEtrEsign_Del%><%--Please enter e-Signature to proceed with deletion*****--%> </b>
		</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>

		</table>

<jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="copyverfrm"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>


 	 <input type="hidden" name="delMode" value="<%=delMode%>">
	 <input type="hidden" name="selStrs" value="<%=selStrs%>">
	 <input type="hidden" name="fromPg" value="<%=from_page%>">




	</FORM>
<%}
	} else {
			String eSign = request.getParameter("eSign");
			String oldESign = (String) tSession.getValue("eSign");
			if(!oldESign.equals(eSign)) {
%>
	 		  <jsp:include page="incorrectesign.jsp" flush="true"/>
<%
			} else {

				StringTokenizer idStrs=new StringTokenizer(selStrs,",");
				int numIds=idStrs.countTokens();
				int i=1,k=0,codelstItem=0;
				String storageCapacity="";
				String multiSpecimen="";
				String capacityUnit="";
				String parStorage="";
				ArrayList<String> cldSpecList=new ArrayList();
				ArrayList pk_Storages=null;
				String[] strArrStrs = new String[numIds] ;
				flag = 2;
				for(k=0;k<numIds;k++)
				cldSpecList.add(idStrs.nextToken()); 		
				for(int j=0;j<cldSpecList.size();j++){	
					strArrStrs[0] = cldSpecList.get(j);
					storageJB.setPkStorage(EJBUtil.stringToNum(cldSpecList.get(j)));
					storageJB.getStorageDetails();
					parStorage=storageJB.getFkStorage();
					StorageDao storDao = storageJB.getAllChildren(EJBUtil.stringToNum(cldSpecList.get(j)));
					pk_Storages = storDao.getPkStorages();
					if(!pk_Storages.isEmpty())
					{
					for(k=0;k<pk_Storages.size();k++)
					cldSpecList.remove(pk_Storages.get(k).toString());
					} 
					if(!StringUtil.isEmpty(parStorage)) {
					storageJB.setPkStorage(EJBUtil.stringToNum(parStorage));
					storageJB.getStorageDetails();
					storageCapacity = storageJB.getStorageCapNum();
					multiSpecimen = storageJB.getMultiSpecimenStorage();
					capacityUnit = storageJB.getFkCodelstCapUnits();
					CodeDao cd5 = new CodeDao();
					codelstItem = cd5.getCodeId("cap_unit", "Items");
					if ((EJBUtil.stringToNum(capacityUnit) == codelstItem) && (EJBUtil.stringToNum(multiSpecimen) == 1) && !StringUtil.isEmpty(storageCapacity) && EJBUtil.stringToNum(storageCapacity)!=0)
						{
						i = storageJB.deleteStorages(strArrStrs,flag,EJBUtil.stringToNum(usr),AuditUtils.createArgs(tSession,"",LC.L_Manage_Invent));
						if(i==0){
							String user="",ipAdd="";
							ipAdd = (String) tSession.getValue("ipAdd");
							user = (String) tSession.getValue("userId");
							StorageJB sjbc = new StorageJB();
							sjbc.setPkStorage(EJBUtil.stringToNum(parStorage));
							sjbc.getStorageDetails();
						    sjbc.setStorageCapNum(EJBUtil.stringToNum(storageCapacity)-1+"");
							sjbc.setModifiedBy(usr);
							sjbc.setIpAdd(ipAdd);
							sjbc.updateStorage();
							}
						i = storageJB.deleteStorages(strArrStrs,flag,EJBUtil.stringToNum(usr),AuditUtils.createArgs(tSession,"",LC.L_Manage_Invent));
						}
						else
						i = storageJB.deleteStorages(strArrStrs,flag,EJBUtil.stringToNum(usr),AuditUtils.createArgs(tSession,"",LC.L_Manage_Invent));
					}
					else
					i = storageJB.deleteStorages(strArrStrs,flag,EJBUtil.stringToNum(usr),AuditUtils.createArgs(tSession,"",LC.L_Manage_Invent));
					
	}

	
	// Modified for INF-18183 ::: AGodara 
//	int i = storageJB.deleteStorages(strArrStrs,flag,EJBUtil.stringToNum(usr),AuditUtils.createArgs(tSession,"",LC.L_Manage_Invent));


	%>
		<br><br><br><br>
		<%
if(i == 0) {
%>
<p class = "successfulmsg" align = center> <%=MC.M_Del_Succ%><%--Deleted successfully*****--%>.</p>
<%
		} else {
%>
<p class = "successfulmsg" align = center> <%=MC.M_DataCnt_BeDel%><%--Data could not be deleted.*****--%> </p>
<%
		}
		if(from_page.equals("Kit")){%>
	<META HTTP-EQUIV=Refresh CONTENT="1; URL=storagekitbrowser.jsp?srcmenu=tdmenubaritem6&selectedTab=3&searchFrom=search">

		<%}else{%>
	<META HTTP-EQUIV=Refresh CONTENT="1; URL=storageadminbrowser.jsp?srcmenu=tdmenubaritem6&selectedTab=2&searchFrom=search">
	<%
	}

	} //end esign
	} //end of delMode
}//end of if body for session
else{
%>
<jsp:include page="timeout.html" flush="true"/>
<%
}
%>

  <div class = "myHomebottomPanel">
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</DIV>

<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/>
</div>

</BODY>
</HTML>

