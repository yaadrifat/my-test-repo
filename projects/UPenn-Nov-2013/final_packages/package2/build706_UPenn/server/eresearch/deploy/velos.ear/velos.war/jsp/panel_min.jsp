<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script type="text/javascript" src="js/calendar/calendar.js"></script>
<SCRIPT LANGUAGE="JavaScript" SRC="whichcss_skin.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<script src="js/yui/3.6.0/build/yui/yui-min.js"></script>
<!--RK <SCRIPT LANGUAGE="JavaScript" SRC="resolution.js"></SCRIPT> -->

<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="formjs.js"></SCRIPT>
<!-- 
<SCRIPT LANGUAGE="JavaScript" SRC="js/velos/prototype.js"></SCRIPT>
 -->
<SCRIPT LANGUAGE="JavaScript" SRC="js/dojo/dojo.js"></SCRIPT>
<script type="text/javascript" src="js/yui/build/yahoo-dom-event/yahoo-dom-event.js"></script>
<SCRIPT LANGUAGE="JavaScript" SRC="js/velos/ajaxengine.js"></SCRIPT>
<jsp:include page="jqueryUtils.jsp"></jsp:include> 
<jsp:include page="sessionwarning.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" SRC="js/velos/fixedHeaders.js"></SCRIPT>

<link rel="shortcut icon" href="favicon.ico" />
<link rel="stylesheet" type="text/css" media="all" href="js/calendar/calendar-win2k-cold-1.css" title="win2k-cold-1" />
 <!-- main calendar program -->


  <!-- language for the calendar -->
  <script type="text/javascript" src="js/calendar/lang/calendar-en.js"></script>

  <!-- the following script defines the Calendar.setup helper function, which makes
       adding a calendar a matter of 1 or 2 lines of code. -->
  <script type="text/javascript" src="js/calendar/calendar-setup.js"></script>

<SCRIPT>


function openwin() {
      window.open("","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=500,height=400");
      }


function exitApp(event){
	if (event.clientY < -1000 && event.clientX < -1000){
	window.open("logout.jsp","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=1,height=1")
}

}

</SCRIPT>



<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="codeLst" scope="session" class="com.velos.eres.web.codelst.CodelstJB"/>
<%@ page language = "java" import = "java.util.*,com.velos.eres.web.user.UserJB, com.velos.eres.service.util.*,com.velos.eres.web.grpRights.GrpRightsJB"%>

<SCRIPT LANGUAGE="JavaScript" SRC="js/velos/onload.js"></SCRIPT>
<body onLoad="onLoad();" onunload="exitApp(event);" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0">
<div id="notification" style="visibility:hidden"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

<script>toggleNotification('on',"");</script>
<%

HttpSession tSession = request.getSession(true);
tSession = request.getSession(true);


if (sessionmaint.isValidSession(tSession))
	{


 /*
  *
 JM: 11/08/2005 Modified: when user's esign or password expired application menubar should not be shown lest
 user can enter into system
  *
  */

	String protocolManagementRight = (String) tSession.getValue("protocolManagementRight");

	int protocolManagementRightInt = 0;
	protocolManagementRightInt = EJBUtil.stringToNum(protocolManagementRight);


   String checkPass = "off";
   String checkESign = "off";

   GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getAttribute("GRights");

   int userReports = Integer.parseInt(grpRights.getFtrRightsByValue("REPORTS"));

   int patientRights= 0;
   int studyRights= 0;

   patientRights = Integer.parseInt(grpRights.getFtrRightsByValue("MPATIENTS"));
   studyRights= Integer.parseInt(grpRights.getFtrRightsByValue("NPROTOCOL"));

   UserJB userB1 = (UserJB) tSession.getValue("currentUser");

   //Date eSignExpDate = new Date(userB1.getUserESignExpiryDate().substring(6) + "/" + userB1.getUserESignExpiryDate().substring(0,2) + "/" + userB1.getUserESignExpiryDate().substring(3,5));
   //Date pwdExpDate = new Date(userB1.getUserPwdExpiryDate().substring(6) + "/" + userB1.getUserPwdExpiryDate().substring(0,2) + "/" + userB1.getUserPwdExpiryDate().substring(3,5));

    //JM: 04Mar2009
    String eSignExpDt = userB1.getUserESignExpiryDate();
    if (StringUtil.isEmpty(eSignExpDt)) eSignExpDt=DateUtil.getFormattedDateString("9999","01","01");
    eSignExpDt= DateUtil.dateToString(DateUtil.stringToDate(eSignExpDt, null));

	//JM: 04Mar2009
    String pwdExpDt = userB1.getUserPwdExpiryDate();
    if (StringUtil.isEmpty(pwdExpDt)) pwdExpDt=DateUtil.getFormattedDateString("9999","01","01");
    pwdExpDt= DateUtil.dateToString(DateUtil.stringToDate(pwdExpDt, null));

   	Date eSignExpDate = DateUtil.stringToDate(eSignExpDt,null);
	Date pwdExpDate = DateUtil.stringToDate(pwdExpDt,null);


   int userId=userB1.getUserId();
   Date d = new Date();

   if(d.after(pwdExpDate))	checkPass = "on";
   if(d.after(eSignExpDate))	checkESign = "on";

//////////////////Added by IG to handle skin in the acc_name column er_account table

	String accName = (String) tSession.getValue("accName");
	String accId = (String) tSession.getValue("accountId");
	accName=(accName==null)?"default":accName;
	String accSkinId = "";
	String usersSkinId = "";
	String userSkin = "";
	String accSkin = "";
	String skin = "default";
	accSkinId = 	(String) tSession.getValue("accSkin");
//	userB1.setUserId(userId);
//	userB1.getUserDetails();


	usersSkinId = userB1.getUserSkin();
	userSkin= codeLst.getCodeCustomCol(EJBUtil.stringToNum(usersSkinId) );


	accSkin= codeLst.getCodeCustomCol(EJBUtil.stringToNum(accSkinId) );

	if (accSkin == null || accSkin.equals("") || accSkin.equals("default") ){

		accSkin = "accSkin";

	}
	else{


		skin = accSkin;
	}

	if (userSkin != null && !userSkin.equals("") ){

		skin = userSkin;

	}

	//	System.out.println("*******skinname...."+skin);


%>
<script> whichcss_skin("<%=skin%>");</script>

<%
	//////End added by IG
	String logoName = "../images/acclogo/account_" + accName + ".jpg";
	String uName = (String) tSession.getValue("userName");

	//VA- check if the dynamic report modile is exited, the
	// clean the seesion hashmap from session
	String sess=(String)request.getParameter("sess");
	if (sess==null)  tSession.removeAttribute("attributes");

	//check if eventArray still exists and if it does,remove it
	//eventArray session variable is used in selecteventus.jsp page
	tSession.removeValue("EventArray");

	//end VA

	//Sonia Abrol, to read default time zone from database
	//TimeZone tz = TimeZone.getDefault();

	String tzDesc = "";

	tzDesc = (String) tSession.getValue("defTZDesc");
	if (StringUtil.isEmpty(tzDesc))
	{
		tzDesc = "";
	}

%>
<input type="hidden" name="accSkin" id="accSkin" value="<%=skin%>">
<input type="hidden" id="sessUserId" value="<%=userId%>">
<input type="hidden" id="sessAccId" value="<%=accId%>">
<jsp:include page="menusie.jsp"></jsp:include>
<jsp:include page="sessionlogging.jsp" flush="true"/> 

<table width="100%" border="0" border-style="solid" >
<tr valign="bottom">
<td align="left" width="50%" class= "grayComments lhsFont" valign="bottom">
	<%=LC.L_Cur_Page%><%--Current Page*****--%>:
	<script type="text/javascript">
	<!--
	document.write(document.title);
	//-->
	</script>
</td>
</tr>
</table>
<%}%>

</body>
</html>
