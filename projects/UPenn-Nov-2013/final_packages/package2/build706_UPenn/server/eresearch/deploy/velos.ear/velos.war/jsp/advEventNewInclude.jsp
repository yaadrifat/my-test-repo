<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language = "java" import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>
<SCRIPT language="JavaScript1.1">
var advEventNewFunctions = {
	formObj: {},
	openwin1: {},
	openAdverseEvent: {},
	openWin: {},
	openWinMoreDetails: {},
	openwindowdict: {},
	openwindowcalc: {},
	openWinStatus: {},
	validate: {},
	disableDictionaryFlds: {},
	fixTextAreas:{},
	lockdown:{}
};

advEventNewFunctions.lockdown = function () {
	
	var formStatusSub = $j("select[name='formStatus'] option:selected").attr("data-subtype");
	if(formStatusSub=='ae_lockdown'){	
		$j('#adverseEventScreenForm').removeAttr("onsubmit");
		$j('#submitBarTable').hide();
	}
};

advEventNewFunctions.fixTextAreas = function (frm) {
	//Maximum possible database limit is 4000 charcters
	
	limitChars('descripton', 2000, 'charlimitinfodescripton');
	limitChars('outcomeNotes', 4000, 'charlimitinfooutcomeNotes');
	limitChars('notes', 4000, 'charlimitinfonotes');
	
	if(document.getElementById("remarks")){
		limitChars('remarks', 4000, 'charlimitinforemarks');
	}
	
	$j("#descripton").keyup(function(){
		var divId = 'charlimitinfo'+this.id;
		limitChars(this.id, 2000, divId);
	});
	
	$j("#outcomeNotes").keyup(function(){
		var divId = 'charlimitinfo'+this.id;
		limitChars(this.id, 4000, divId);
	});
	
	$j("#notes").keyup(function(){
		var divId = 'charlimitinfo'+this.id;
		limitChars(this.id, 4000, divId);
	});
		
	$j("#remarks").keyup(function(){
		var divId = 'charlimitinfo'+this.id;
		limitChars(this.id, 4000, divId);
	});
	
};


advEventNewFunctions.openwin1 = function (frm) {
   windowName = window.open("usersearchdetails.jsp?fname=&lname=&from="+frm,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200");
   windowName.focus();
};

advEventNewFunctions.openAdverseEvent = function (studyId, personId, src) {
   windowName = window.open("adveventlookup.jsp?&studyId="+studyId+"&personPK="+personId+"&srcmenu="+src,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200");
   windowName.focus();
};

advEventNewFunctions.openWin = function () {
    windowName = window.open("","Information","toolbar=yes,location=yes,scrollbars=yes,resizable=yes,menubar=yes,width=600,height=300");
	windowName.focus();
};

advEventNewFunctions.openWinMoreDetails = function (modId, mode, modName,pageRight){
	if (mode == 'M'){
		windowname=window.open("moredetails.jsp?modId=" + modId+"&modName="+modName,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=650,height=550 top=120,left=200 0, ");
		windowname.focus();
	} else if (mode == 'N')	{
		alert("<%=MC.M_SavePage_BeforeDets%>");/*alert("Please save the page first before entering More Details");*****/
		return false;
	}
};

advEventNewFunctions.openwindowdict = function(studyAdvlkpVer,versionnum,formobj){
	
	currentDictSettingValue = formobj.currentDictSetting.value;

	if (studyAdvlkpVer == "0"){
	 	windowName = window.open("defaultGrade.jsp?parentform="+formobj.name+"&gradefld=grade&advnamefld=advName&dispfield=advGradeText&advDictionaryField=advDictionary&currentDictSettingValue="+currentDictSettingValue,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=500,height=350 top=200,left=200");   windowName.focus();
	}else{
   		var calc = "Grade : fdatacolumn fdispcolumn";
		
   		if (versionnum == 4) {
			var keyexpr = "advDictionary|[VELEXPR]*[VELSTR="+currentDictSettingValue+"]~aeCategory|adv_lkp_category~aeToxicity|adv_lkp_toxic~aeToxicityDesc|adv_lkp_tdesc~grade|adv_lkp_grade~aeGradeDesc|adv_lkp_gdesc~MedDRAcode|adv_lkp_meddra~advName|adv_lkp_tshort~dictValSel|[VELEXPR]";
		} else if (versionnum == 3){	
	 		var keyexpr = "advDictionary|[VELEXPR]*[VELSTR="+currentDictSettingValue+"]~aeCategory|nci3_category~grade|nci3_grade~aeGradeDesc|nci3_desc~aeToxicity|nci3_toxicity~MedDRAcode|nci3_meddra~advName|nci3_shrtname~dictValSel|[VELEXPR]";
	 	} else if(versionnum == 2){
	 		var keyexpr = "advDictionary|[VELEXPR]*[VELSTR="+currentDictSettingValue+"]~aeCategory|adv_lkp_category~grade|adv_lkp_grade~aeGradeDesc|adv_lkp_desc~aeToxicity|adv_lkp_toxic~aeToxicityDesc|adv_lkp_tdesc~dictValSel|[VELEXPR]";
	 	}else{
	 		var keyexpr = "advDictionary|[VELEXPR]*[VELSTR="+currentDictSettingValue+"]~advGradeText|[VELEXPR]*[VELSTR=Grade:]*[VELSPACE]*[VELKEYWORD=adv_lkp_grade]*[VELSPACE]*[VELKEYWORD=adv_lkp_toxic]~grade|adv_lkp_grade~advName|adv_lkp_toxic~MedDRAcode|adv_lkp_meddra~dictValSel|[VELEXPR]";
		}
		
		windowName = window.open("multilookup.jsp?viewId=" +studyAdvlkpVer + "&form="+formobj.name+"&dfilter=&frameMode=TB&maxselect=1&keyword="+keyexpr ,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=900,height=550 top=100,left=100")
		windowName.focus();
	}
};

advEventNewFunctions.openwindowcalc = function(nciVersion,pkey){
	if (nciVersion == '0' || nciVersion == ''){
		alert("<%=MC.M_NoAdvEvtDictAssoc_Std%>");/*alert("There is no Adverse Event Dictionary associated with the study")*****/
		return ;
	}
	else
	{
		windowname=window.open("getAdvToxicity.jsp?nciVersion=" + nciVersion + "&pkey=" + pkey + "&parentform=adverseEventScreenForm&gradefld=grade&advnamefld=advName&dispfield=advGradeText" ,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=500,height=350 top=120,left=200 0, ");
		windowname.focus();
	}
};

advEventNewFunctions.validate = function(formobj){
	
	
	if (document.getElementById('grade')) {
		if(isNaN(document.getElementById('grade').value)){
			alert('<%=LC.L_Adv_EvtGrade%> can only be a number');
			document.getElementById('grade').focus();
			return false;
		}
	}
	
	/** $j("input[name='startDt']").each(function(){
	
	if(isFutureDate($j(this).val())){
		alert(M_DtCnt_FutDt);
		this.focus();
		return false;
	}
});
*/

	
	if (document.getElementById('mandae')) {
	     if (!(validate_col('Adverse-Eventtype',formobj.adve_type))) return false;
	}

	if (document.getElementById('mandstdate')) {
		 if (!(validate_col('Start Date',formobj.startDt))) return false;
	}
	

    if (document.getElementById('mandent')) {
		 if (!(validate_col('Entered By',formobj.enteredByName))) return false;
	}

    var mVal = formobj.advMode.value;
	if (mVal){
	 	
		var fdaVal = document.getElementById("fdaStudy").value;
		if (fdaVal == "1" && mVal == "M"){
			if (  !(validate_col("<%=LC.L_ReasonForChangeFDA%>",formobj.remarks))) return false;
		}
	}

    if (formobj.userLogName){
		if (  !(validate_col('<%=LC.L_UserName%>',formobj.userLogName))) return false;
	}
    if (!(validate_col('e-Signature',formobj.eSign))) return false;


	 //KM

	 if (document.getElementById('pgcustomtype')) {
	   if (!(validate_col('Adverse-Eventtype',formobj.adve_type))) return false;
	 }

	 if (document.getElementById('pgcustomaegrading')) {
	   if (!(validate_col('Adverse Event/Grading',formobj.advGradeText))) return false;
	 }

     if (document.getElementById('pgcustommeddra')) {
	   if (!(validate_col('MedDRA code',formobj.MedDRAcode))) return false;
	 }

     if (document.getElementById('pgcustomdict')) {
	   if (!(validate_col('Dictionary',formobj.advDictionary))) return false;
	 }

	  if (document.getElementById('pgcustomdesc')) {
	   if (!(validate_col('Description',formobj.descripton))) return false;
	 }

	  if (document.getElementById('pgcustomcourse')) {
	   if (!(validate_col('Treatment Course',formobj.treatment))) return false;
	 }

	  if (document.getElementById('pgcustomstdt')) {
	   if (!(validate_col('Start Date ',formobj.startDt))) return false;
	 }

	  if (document.getElementById('pgcustomstopdt')) {
	   if (!(validate_col('Stop Date ',formobj.stopDt))) return false;
	 }

	  if (document.getElementById('pgcustomdiscdt')) {
	   if (!(validate_col('Discovery Date',formobj.aediscoveryDt))) return false;
	 }

	  if (document.getElementById('pgcustomlogdt')) {
	   if (!(validate_col('Logged Date',formobj.aeloggedDt))) return false;
	 }

	  if (document.getElementById('pgcustoment')) {
	   if (!(validate_col('Entered By ',formobj.enteredByName))) return false;
	 }

	  if (document.getElementById('pgcustomrep')) {
	   if (!(validate_col('Reported By',formobj.reportedByName))) return false;
	 }


	  if (document.getElementById('pgcustomattr')) {
	   if (!(validate_col('Attribution',formobj.adve_relation))) return false;
	 }

	  if (document.getElementById('pgcustomlink')) {
	   if (!(validate_col('Linked To',formobj.linkedToName))) return false;
	 }

	  if (document.getElementById('pgcustomaction')) {
	   if (!(validate_col('Action',formobj.outaction))) return false;
	 }

	  if (document.getElementById('pgcustomrec')) {
	   if (!(validate_col('Recovery Description',formobj.adve_recovery))) return false;
	 }

	  if (document.getElementById('pgcustomoutcome')) {
	   if (!(validate_col('Outcome Notes',formobj.outcomeNotes))) return false;
	 }

	  if (document.getElementById('pgcustomnotes')) {
	   if (!(validate_col('Notes',formobj.notes))) return false;
	 }

	  if (document.getElementById('pgcustomformstat')) {
	   if (!(validate_col('Form Status',formobj.formStatus))) return false;
	 }

	  
	  if (document.getElementById('pgcustomaecategory')) {
		   if (!(validate_col('Category',formobj.aeCategory))) return false;
		 }
	  if (document.getElementById('pagecustomaetoxicity')) {
		   if (!(validate_col('Toxicity',formobj.aeToxicity))) return false;
		 }
	  if (document.getElementById('pagecustomaetoxicitydesc')) {
		   if (!(validate_col('Toxicity Description',formobj.aeToxicityDesc))) return false;
		 }
	  if (document.getElementById('pagecustomgrade')) {
		   if (!(validate_col('Grade',formobj.grade))) return false;
		 }
	  if (document.getElementById('pagecustomaegradedesc')) {
		   if (!(validate_col('Grade Description',formobj.aeGradeDesc))) return false;
		}
	  if (document.getElementById('pagecustomadvname')) {
		   if (!(validate_col('Adverse Event Name',formobj.advName))) return false;
		}

	//KM

	 //added for date field validation
     if (!(validate_date(formobj.startDt))) return false;
     if (!(validate_date(formobj.stopDt))) return false;



     if (!(validate_date(formobj.aediscoveryDt))) return false;
     if (!(validate_date(formobj.aeloggedDt))) return false;



	 if (!(validate_date(formobj.outcomeDt))) return false;
	var test="";


	for(var i=0;i<formobj.outcomeType.length;i++)
	{
		if(formobj.outcomeType[i].checked==true)
		{
		test+="1" ;
		} else
		{
		test+="0";
		}
	}

	formobj.outcomeString.value=test;
	//alert(formobj.outcomeString.value);


	test="";
	for(var i=0;i<formobj.addInfo.length;i++)
	{
		if(formobj.addInfo[i].checked==true)
		{
		test+="1" ;
		} else
		{
		test+="0";
		}
	}
	formobj.addInfoString.value=test;

	test="";
	for(var i=0;i<formobj.advNotify.length;i++)
	{
		if(formobj.advNotify[i].checked==true)
		{
		test+="1" ;
		} else
		{
		test+="0";
		}
	}

	var isDeathSelected = false;
	if($j("input[data-subType='al_death']:checked").length>0){
		isDeathSelected = true;
	}
	
	formobj.advNotifyString.value=test;
	if(isDeathSelected == true && formobj.outcomeDt.value != '')	{
	if( confirm("<%=MC.M_SelDthPat_FutureNotific%>")){/*if( confirm("You have selected 'Death' as an outcome for this <%=LC.Pat_Patient%>. To prevent any further action connected to this <%=LC.Pat_Patient%>, such as future notifications, would you like to deactivate this <%=LC.Pat_Patient%>'s schedule now?")){*****/
	formobj.death.value = "Yes";
 	} else{
	formobj.death.value = "No";
	return false;
	}
	}
	if (formobj.outcomeDt.value != '')	{
		if(isDeathSelected == false)
		{
			alert("<%=MC.M_EtrPatDtDth_DthOutcome%>");/*alert("You have entered <%=LC.Pat_Patient%>'s Date of Death. Please select 'Death' in Outcome Type");*****/
			return false;
		}
	}
	if (formobj.outcomeDt.value == '' && isDeathSelected == true){
			alert("<%=MC.M_Selc_DtOfDth%>");/*alert("Please Select Date of Death");*****/
			formobj.outcomeDt.focus();
			return false;
	}




	if (document.getElementById('pgcustomouttype')) {
	  var val = false;
	  for(var i=0;i<formobj.outcomeType.length;i++)
	  {
		if(formobj.outcomeType[i].checked==true)
			val = true;
	  }

	  if (val == false)
	  {
		   alert("<%=MC.M_SelAtleast_OneOutcomeTyp%> ");/* alert("Please select atleast one option for 'Outcome Type' ");*****/
		   return false;
	  }
	}



	if (document.getElementById('pgcustomaddl')) {

	  var val = false;
	  for(var i=0;i<formobj.addInfo.length;i++)
	  {
		if(formobj.addInfo[i].checked==true)
			val = true;
	  }

	  if (val == false)
	  {
		   alert("<%=MC.M_SelAtleast_OneOptAddlInfo%> ");/*alert("Please select atleast one option for 'Additional Information' ");*****/
		   return false;
	  }
	}

 //Fixed Bug No:3687.Put validations on combination of Date and checkbox in case of Notified Dates(IRB,FDA...)
	if (document.getElementById('pgcustomnotified')) {

	  var val = false;
	  var datenotify="";
	  for(var i=0;i<formobj.advNotify.length;i++)
	  {
            //alert(i);
		if(formobj.advNotify[i].checked==true)
		{

			if(document.getElementById("notifyDate"+i).value=="")
			{
				alert("<%=MC.M_Selc_DtForNotif%>");/*alert("Please select date for Notified")*****/
				return false;
			}else
			{
			val = true;
			}
			//alert(formobj.advNotify[i].checked+ "and "+ val +" i "+i);
		}else
		{
			if(document.getElementById("notifyDate"+i).value!="")
			{
				alert("<%=MC.M_PlsSelOpt_DtSel%> ");/*alert("Please select option for 'The Following were Notified:' where date has been selected ");*****/
				   return false;
			}else
			{
			val = true;
			}


		}


	  }

	  if (val == false)
	  {
		   alert("<%=MC.M_SelAtleastOneOpt_ForNtfi%> ");/*alert("Please select atleast one option for 'The Following were Notified:' ");*****/
		   return false;
	  }
	}



	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
			 return false;
     }

	  //condition for date field validation by JM on 07April05

     if ((formobj.startDt.value != null && formobj.startDt.value != '' )&&(formobj.stopDt.value != null && formobj.stopDt.value !=''))
	{

		if (CompareDates(formobj.startDt.value,formobj.stopDt.value, '>'))
		{
		  	alert ("<%=MC.M_StartDtCnt_GtrThanStopDt%>");/*alert ("Start Date can not be greater than Stop Date");*****/

			formobj.startDt.focus();
			return false;
		}

	}

	//da= new Date();
	//var str1,str2;
	//str1=parseInt(da.getMonth());
	//str2=str1+1;
	//todate = str2+'/'+da.getDate()+'/'+da.getFullYear();


	//JM: 20Apr2009, #4026
	todate = formatDate(new Date(),calDateFormat);

	 if (formobj.outcomeDt.value != null && formobj.outcomeDt.value != '' )
	{

		if (CompareDates(formobj.outcomeDt.value,todate, '>'))
		{
		  	alert ("<%=MC.M_DthDtNotGtr_TodayDt_adve%>");/*alert ("Death Date should not be greater than Today's Date");*****/

			formobj.outcomeDt.focus();
			return false;
		}
	}
	
	var formStatusSub = $j("select[name='formStatus'] option:selected").attr("data-subtype");
	if(formStatusSub=='ae_lockdown'){
		var proceed = window.confirm('<%=MC.M_AE_FRM_LKDWN%>');
		if(!proceed){
			return proceed;
		}
	}
	
	return true;
};

advEventNewFunctions.checkoutcome = function(formobj,subtype){
	
	if(subtype=='al_death'){
	
		$j("input[name='outcomeType']").each(function(){
			
			if($j(this).attr("data-subtype")!='al_death'){
				
				if($j(this).is(":checked")){
					alert("<%=MC.M_CntSel_DthOptWithOth%> ");/*alert("You cannot select Death option along with any other option ");*****/
					//formobj.outcomeType[0].checked=false;
					formobj.outcomeDt.value ="";
					$j("input[data-subType='al_death']").attr( "checked", false );				
					return false;
				}
			}					
		});		
	}else{
		var isDeathSelected = false;
		if($j("input[data-subType='al_death']:checked").length>0){
			isDeathSelected = true;
		}
		
		if(isDeathSelected==true)	{
				alert("<%=MC.M_CntSelOpt_WithDthOpt%>");/*alert("You cannot select any option along with Death option");*****/
				//formobj.outcomeType[boxval].checked=false;
				$j("input[data-subType='"+subtype+"']").attr( "checked", false );
				return false;
			}
	}

};

advEventNewFunctions.openWinStatus = function(patId,patStatPk,pageRight,study,orgRight) {

	changeStatusMode = "yes";

	if (f_check_perm_org(pageRight,orgRight,'N')) {
		windowName= window.open("patstudystatus.jsp?studyId="+study+"&changeStatusMode=yes&statid=" + patStatPk + "&pkey=" + patId,"patstatusdetails","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=550,height=560");
		windowName.focus();
	}
};

advEventNewFunctions.disableDictionaryFlds = function(){
	
	var dictionayVersion=$j('#studyAdvlkpVerNumber').val();
	if (dictionayVersion == 4) {
				
	} else if (dictionayVersion == 3){	
		if(!$j("input[name='pagecustomaetoxicitydesc']")){
			$j("input[name='aeToxicityDesc']").attr("disabled","true");
		}
		
		if($j("input[name='pagecustomaetoxicitydesc']")){
			$j("input[name='aeToxicityDesc']").attr("disabled","true");
		}
		
	} else if(dictionayVersion == 2){
		if(!$j("input[name='pagecustomadvname']")){
			$j("input[name='advName']").attr("disabled","true");
		}
		
		if($j("input[name='pagecustomadvname']")){
			$j("input[name='advName']").attr("disabled","true");
		}
		
	}else if(dictionayVersion == 0){
 		$j("input[name='aeCategory'],input[name=aeToxicity],input[name='aeToxicityDesc'],input[name='MedDRAcode'],input[name='advName']").val("").removeAttr("readonly").removeClass("readonly");
 	}
	
};

$j(document).ready(function(){
	
	advEventNewFunctions.disableDictionaryFlds();
	advEventNewFunctions.fixTextAreas();
	advEventNewFunctions.lockdown();
	
});


</SCRIPT>
