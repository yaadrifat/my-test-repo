<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=MC.M_Updt_OtherStdIds%><%--Update Other <%=LC.Std_Study%> IDs*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<%@ page import="com.velos.eres.service.util.LC"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="altId" scope="request"  class="com.velos.eres.web.studyId.StudyIdJB"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.studyId.impl.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.*"%>

<body>
<DIV class="popDefault">

<%
	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))
	{

%>

		<jsp:include page="sessionlogging.jsp" flush="true"/>
 <%
		String eSign= request.getParameter("eSign");
    	String oldESign = (String) tSession.getValue("eSign");

    	 String defUserGroup = (String) tSession.getAttribute("defUserGroup");


	   if(oldESign.equals(eSign))
	   {


			int errorCode=0;
			int rows = 0;

			String recordType="" ;
			String creator="";
			String ipAdd="";
			String accountId="";
			String studyId="";


			creator=(String) tSession.getAttribute("userId");
			ipAdd=(String)tSession.getAttribute("ipAdd");

			studyId = request.getParameter("studyId");

			ArrayList arRecordType = new ArrayList();
			ArrayList arAlternateId = new ArrayList();
			ArrayList arAlternateValues = new ArrayList();
			ArrayList arId = new ArrayList();
			//String[] arElementId = request.getParameterValues("elementId");

			Enumeration params = request.getParameterNames();
			while (params.hasMoreElements()) {
			    String param = (String)params.nextElement();
			    if (param.startsWith("alternateId")) {
			    	String paramVal = request.getParameter(param);
			    	if (null != paramVal){
			    		String mdId = param.substring(param.indexOf("alternateId")+("alternateId".length()));
			    		arAlternateId.add(mdId);
			    		arAlternateValues.add(paramVal);
			    		paramVal = request.getParameter("recordType"+mdId);
				    	if (null != paramVal){
				    		arRecordType.add(paramVal);
				    	}
				    	paramVal = request.getParameter("id"+mdId);
				    	if (null != paramVal){
				    		arId.add(paramVal);
				    	}
			      	}
			    }
			}



			StudyIdBean sskMain = new StudyIdBean();

			ArrayList alElementData = arAlternateValues;

				int len = alElementData.size();

				for ( int j = 0 ; j< len ; j++)
				{
					StudyIdBean sskTemp = new StudyIdBean();

					if   ( ( !(EJBUtil.isEmpty ((String)arAlternateValues.get(j))) &&  "N".equals((String)arRecordType.get(j))) || "M".equals((String)arRecordType.get(j)))
					{
						sskTemp.setId(EJBUtil.stringToNum((String)arId.get(j)));
						sskTemp.setStudyId(studyId) ;
						sskTemp.setStudyIdType((String)arAlternateId.get(j));
						sskTemp.setAlternateStudyId((String)arAlternateValues.get(j));
						sskTemp.setRecordType((String)arRecordType.get(j));
						if ("N".equals((String)arRecordType.get(j)))
						{
							sskTemp.setCreator(creator);
						}else
						{
							sskTemp.setModifiedBy(creator);
						}
						sskTemp.setIpAdd(ipAdd);
						sskMain.setStudyIdBeans(sskTemp);
					}
				}

			errorCode = altId.createMultipleStudyIds(sskMain,defUserGroup);

		%>
		<br><br><br><br><br><br><br>
		<table "width = 500" align = "center">
			<tr>
				<td width = 500>



		<%


		if ( errorCode == -2  )
		{
%>
			<p class = "successfulmsg" >
	 			<%=MC.M_DataNotSvd_Succ%><%--Data was not saved successfully*****--%>
			</p>

<%

		} //end of if for Error Code

		else

		{
%>

		<p class = "successfulmsg" >
			<%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%>
		</p>

		</td>
		</tr>
		</table>
		<script>
			//window.opener.location.reload();
			setTimeout("self.close()",1000);
		</script>

<%
		}//end of else for Error Code Msg Display
	}//end of if old esign
	else{
%>
		<jsp:include page="incorrectesign.jsp" flush="true"/>
<%
			}//end of else of incorrect of esign
     }//end of if body for session

	else
	{

%>
		  <jsp:include page="timeout.html" flush="true"/>
<%

} //end of else of the Session


%>

</body>

</html>

