<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Create_Mstones%><%--Create Milestones*****--%></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:include page="include.jsp" flush="true"/>
<jsp:include page="ui-include.jsp" flush="true"></jsp:include>
</head>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<jsp:include page="skinChoser.jsp" flush="true"/>

<SCRIPT LANGUAGE="JavaScript">
function showDatePicker(dtID){	
	if($j.browser.msie){
		 var offset = $j(dtID).offset();
	     var xPos = offset.left;
	     var yPos = offset.top;
		 var brwserH = $j(window).height();
		if(brwserH>=600 && brwserH<=950)
		{
			$j('.ui-datepicker').css('margin-left',(xPos-1450));
		}
		else if(brwserH>485){
			$j('.ui-datepicker').css('margin-left',(xPos-500));
		}
		else{
			$j('.ui-datepicker').css('margin-left',(xPos-1250));
		}
	}
	else{
		var offset = $j(dtID).offset();
	     var xPos = offset.left;
	     var yPos = offset.top;
		 var brwserH = $j(window).height();
		if(brwserH>=600 && brwserH<=950)
		{
			$j('.ui-datepicker').css('margin-left',(xPos-1450));
		}
		else if(brwserH>485){
			$j('.ui-datepicker').css('margin-left',(xPos-500));
		}
		else{
			$j('.ui-datepicker').css('margin-left',(xPos-1334));
		}
	}
}

//KM-#4518
function asyncValidate(formobj)
{
	if(formobj){
	selval = formobj.bgtcalId.value;
	studyId = formobj.studyId.value;

	if (formobj.bgtcalFlag){
		formobj.bgtcalFlag.value = formobj.bgtcalId.value;
		if (formobj.bgtcalId.value!=''){
			var bgtcalFlag = formobj.bgtcalFlag.options[formobj.bgtcalFlag.selectedIndex].text;
		
			var flag = 'none';
			if (bgtcalFlag=='S'){
				flag = 'none';
				$j('#tempTd').attr('style','display:inline;');
				$j('#tempTdNew').attr('style','display:none;');
			}else{
				$j('#tempTd').attr('style','display:none;');
				$j('#tempTdNew').attr('style','display:inline;');
				if (document.getElementById('agent').value == '0'){
					 flag = 'block';
				}else{
					flag = '';
				}
			}
			
			var countArray = document.getElementsByName("count");
			var statusArray = document.getElementsByName("dStatus");
			
			for (var i=0; i<countArray.length; i++){
				if (bgtcalFlag=='S') countArray[i].value ='';
				countArray[i].style.display=flag;
				if (bgtcalFlag=='S') statusArray[i].value='';
				statusArray[i].style.display=flag;
			}
		
			var countTDArray = document.getElementsByName("countTD");
			var statusTDArray = document.getElementsByName("statusTD");
			
			for (var i=0; i<countTDArray.length; i++){
				countTDArray[i].style.display=flag;
				statusTDArray[i].style.display=flag;
			}
		} 
	}
	if (selval == '')
		selval =0;
	new VELOS.ajaxObject("ajaxValidation.jsp", {
   		urlData:"bgtcalId="+selval+"&studyId="+studyId,
		   reqType:"POST",
		   outputElement: "calStatMessage" }

   ).startRequest();
		}

}
//KM
window.onload = function() {
	asyncValidate(document.milerule);
 	enableSubmit(true);
}

function validateHoldback(evt){
	if(evt.shiftKey) return false;
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	//delete, backspace & arrow keys
	if (charCode == 8 || charCode == 46 || charCode == 37 || charCode == 39 || charCode == 110 || charCode == 190 )
		return true;

	if (charCode > 31 && (!((47 < charCode && charCode  < 58) || (95 < charCode &&  charCode < 106))))
		return false;
	return true;
}

function holdback_applyAll(){	
	var holdBackVal=$j("#holdback").val();
	var mileArr = [{name:'milestoneType',id:'#VisitMilestone'},{name:'milestoneType',id:'#EventMilestone'},
	           		{name:'milestoneType',id:'#AdditionalMilestone'}];
		
	for(var mile=0; mile< mileArr.length;mile++){
		document.getElementsByName('holdbackAmt')[mile].value = holdBackVal;
		}
}

function validate(formobj) {


mileStoneWIPStat=formobj.isWIPStat.value


if (mileStoneWIPStat ==0){

	alert("<%=MC.M_NoRoleRgt_ToCreateMstone%>");/*alert("You do not have role rights to create Milestone(s)");*****/
	return false;

}




     var selectedRule;
	 var ruleSubType;
	 var typecount = 0;

	 var calendarct ;

     calendarct = formobj.calcount.value;

     formobj.milestoneTypeSelected[0].value = "0";
     formobj.milestoneTypeSelected[1].value = "0";
     formobj.milestoneTypeSelected[2].value = "0";
     /*FIN-22375 Date :16 Aug 2012 By : Yogendra Pratap*/
     formobj.sponsorAmountSelected[0].value = "0";
     formobj.sponsorAmountSelected[1].value = "0";
     formobj.sponsorAmountSelected[2].value = "0";

         for (counter = 0 ; counter < 2 ; counter ++)
         {
        	/*FIN-22375 Date :16 Aug 2012 By : Yogendra Pratap*/ 
         if(!formobj.milestoneType[counter].checked && formobj.holdbackAmt[counter].value!=null){
 	 			formobj.holdbackAmt[counter].value=="";
 	 		}else if(formobj.holdbackAmt[counter].value == "")
 			{
 				formobj.holdbackAmt[counter].value = "0";
 			}

         if(formobj.budgetTemplate.value=='C'){
 	     	if (formobj.milestoneType[counter].checked && formobj.sponsorAmountChk[counter].checked){
 	   		 formobj.sponsorAmountSelected[counter].value = "1";
 	   	 	}
      	}
			if (formobj.milestoneType[counter].checked)
			{
				if (calendarct == 0)
				{
						alert("<%=MC.M_NoCalSel_ToBgt%>");/*alert("There is no Calendar selected for this Budget. Visit and Event Milestones can be created for a Calendar only.");*****/
					   	return false;
				}
				formobj.milestoneTypeSelected[counter].value = "1";



				if (! validate_col('Calendar',formobj.bgtcalId)  )
			        {

						formobj.bgtcalId.focus();
			      	   	return false;
			         }

				// Fin-22373 Date: 26-Sep-2012 Ankit
				if ((validate_col_no_alert('<%=LC.L_Date_From%>',formobj.dateFrom[counter])) && ( validate_col_no_alert('<%=LC.L_Date_To%>',formobj.dateTo[counter])))
		        {
					if(CompareDates(formobj.dateFrom[counter].value,formobj.dateTo[counter].value)){
						alert("<%=MC.M_DtToNotLessThan_DtFrom%>")
						formobj.dateTo[counter].focus();
						return false;
					}
		        }

				typecount = typecount + 1;
				if (counter == 0)
				{

				    if (! validate_col('Visit Milestone Rule',formobj.rule)  )
			        {

						formobj.rule.focus();
			      	   	return false;
			         }

			        selectedRule  =  formobj.rule.selectedIndex;
		        	ruleSubType = formobj.ruleSubType[selectedRule].value;


			         if ( ( ! validate_col_no_alert('Event Status',formobj.dEventStatus[counter] ))  )
	        			{

				    		if (ruleSubType != 'vm_4')
				    		{
								alert("<%=MC.M_Selc_EvtStatus%>");/*alert("Please select Event Status");*****/
								formobj.dEventStatus[counter].focus();
				    	  	   return false;
				    	  	 }
	         			}


	         		if ( ( validate_col_no_alert('Event Status',formobj.dEventStatus[counter] ))  )
	        			{

				    		if (ruleSubType == 'vm_4')
				    		{
								alert("<%=MC.M_CntSelEvt_MstoneRule%>");/*alert("You cannot select an Event Status with the selected Milestone Rule");*****/
								formobj.dEventStatus[counter].focus();
				    	  	    return false;
				    	  	}
	         			}


		          }
			      else //counter != 0
		          {
		            if (! validate_col('Event Milestone Rule',formobj.EMrule)  )
			         {

						formobj.EMrule.focus();
			      	   	return false;
			         }

			        selectedRule  =  formobj.EMrule.selectedIndex;
			        ruleSubType = formobj.EVRuleSubType[selectedRule].value;


			        if ( !validate_col_no_alert('Event Status',formobj.dEventStatus[counter] )  )
	        		{

		    			if (ruleSubType != 'em_4')
		    			{
		    				alert("<%=MC.M_Selc_EvtStatus%>");/*alert("Please select Event Status");*****/
							formobj.dEventStatus[counter].focus();
		    	  	   		return false;
		    	  	 	}
 			   		 }

 			   		if ((validate_col_no_alert('Event Status',formobj.dEventStatus[counter])) )
			        {

			    		if (ruleSubType == 'em_4')
			    		{
			    			alert("<%=MC.M_CntSelEvt_MstoneRule%>");/*alert("You cannot select an Event Status with the selected Milestone Rule");*****/
							formobj.dEventStatus[counter].focus();
			    	  	   return false;
			    	  	 }

			         }

	          	}


	       	if (! isInteger(formobj.mLimit[counter].value))
			{
					formobj.mLimit[counter].focus();
					alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid Number");*****/
				 	return false;
			}

			if ( !isInteger(formobj.count[counter].value) )
			{
				if (!isNegNum(formobj.count[counter].value))
				{
					formobj.count[counter].focus();
					alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid Number");*****/
				 	return false;
				}
				else
				{
					if (formobj.count[counter].value == '-0')
					{
						formobj.count[counter].focus();
						alert("<%=MC.M_InvalidNegVal_EtrVldCnt%>");/*alert("Invalid negative value. Please enter a valid 'Count'");*****/
					 	return false;
					}

					if (parseInt(formobj.count[counter].value) < -1)
					{
						formobj.count[counter].focus();
						alert("<%=MC.M_CntLess_EtrValidCount%>");/*alert("The count cannot be less than -1. Please enter a valid 'Count'");*****/
					 	return false;
					}
				}
			}
			/*FIN-22381_09262012 Date :19 OCT 2012 By : Yogendra Pratap*/
			if (( !validate_col_no_alert("<%=LC.L_Mstone_Status%>",formobj.milestoneStat[counter]) )){
				alert("<%=MC.M_Selc_MstoneStatus%>");
				formobj.milestoneStat[counter].focus();
				return false;
			}			

       } //for checkbox check

     }


      
	if(!formobj.milestoneType[2].checked && formobj.holdbackAmt[2].value!=null){
	 			formobj.holdbackAmt[2].value=="";
	 		}else if(formobj.holdbackAmt[2].value == ""){
				formobj.holdbackAmt[2].value = "0";
			}
      //for additional milestones
     if (formobj.milestoneType[2].checked)
     {
     	typecount = typecount + 1;



     	if (parseInt(calendarct) > 0)
     	{
     		if (! validate_col('Calendar',formobj.bgtcalId)  )
			        {

						formobj.bgtcalId.focus();
			      	   	return false;
			      }

     	}
     	formobj.milestoneTypeSelected[2].value = "1";
     	/*FIN-22375 Date :16 Aug 2012 By : Yogendra Pratap*/
     	if(formobj.budgetTemplate.value=='C'){
	     	if (formobj.milestoneType[2].checked && formobj.sponsorAmountChk[2].checked){
	   		 formobj.sponsorAmountSelected[2].value = "1";
	   	 	}
     	}
     	/*FIN-22381_09262012 Date :19 OCT 2012 By : Yogendra Pratap*/
     	if (( ! validate_col_no_alert("<%=LC.L_Mstone_Status%>",formobj.milestoneStat[2]) )){
     		alert("<%=MC.M_Selc_MstoneStatus%>");
			formobj.milestoneStat[2].focus();
			return false;
		}
     }

     if (typecount == 0)
     {
    	 for (counter = 0 ; counter <= 2 ; counter ++){
         if(!formobj.milestoneType[counter].checked && formobj.holdbackAmt[counter].value!=null){
	 			formobj.holdbackAmt[counter].value=="";
	 		}else if(formobj.holdbackAmt[counter].value == "")
			{
				formobj.holdbackAmt[counter].value = "0.00";
			}
    	 }	
     	alert("<%=MC.M_SelAtleast_OneMstoneTyp%>");/*alert("Please select atleast one Milestone Type");*****/
     	return false;

     }
	if (!(validate_col('e-Signature',formobj.eSign))) return false
	if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
	}
}

function validateHoldbackNumber(holdbackVal){
	if(holdbackVal > 99.99 || holdbackVal < 0){
		if(document.getElementById("holdback").value > 99.99 || document.getElementById("holdback").value < 0){
			document.getElementById("holdback").value = "0.00";
			alert("<%=MC.M_ValEtrFwg_2Dgtholdback %>");
			setTimeout(function() { $j('[name="mainHoldback"]').focus(); }, 0);
		}
		else if(document.getElementById("holdbackVM").value > 99.99 || document.getElementById("holdbackVM").value < 0){
			document.getElementById("holdbackVM").value = "0.00";
			alert("<%=MC.M_ValEtrFwg_2Dgtholdback %>");
			setTimeout(function() { $j('#holdbackVM').focus(); }, 0);
			}
		else if(document.getElementById("holdbackEM").value > 99.99 || document.getElementById("holdbackEM").value < 0){
			document.getElementById("holdbackEM").value = "0.00";
			alert("<%=MC.M_ValEtrFwg_2Dgtholdback %>");
			setTimeout(function() { $j('#holdbackEM').focus(); }, 0);
			}
		else if(document.getElementById("holdbackAM").value > 99.99 || document.getElementById("holdbackAM").value < 0){
			document.getElementById("holdbackAM").value = "0.00";
			alert("<%=MC.M_ValEtrFwg_2Dgtholdback %>");
			setTimeout(function() { $j('#holdbackAM').focus(); }, 0);
			}
		return false;
	}
}

function fclose() {
	self.close();
}


</SCRIPT>

<%
int ienet = 2;

String agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1)
     ienet = 0; //IE
    else
	ienet = 1;
	if(ienet == 0) {
%>
<body style="overflow:scroll;">
<%
	} else {
%>
<body>
<%
	}
%>
<P class="sectionHeadings"><%=LC.L_Create_Mstones%><%--Create Milestones*****--%> </P>

<input type="hidden" id="agent" name="agent" value="<%=ienet%>"/>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="budgetcalB" scope="request"	class="com.velos.esch.web.budgetcal.BudgetcalJB" />
<jsp:useBean id="protvisitB" scope="request" class="com.velos.esch.web.protvisit.ProtVisitJB"/>

<jsp:useBean id="stdRightsB" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="milestoneB" scope="request" class="com.velos.eres.web.milestone.MilestoneJB"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:include page="include.jsp" flush="true"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB, com.velos.esch.business.common.EventAssocDao,com.velos.esch.business.common.*,com.velos.eres.service.util.*"%>
<%
   HttpSession tSession = request.getSession(true);
   if (sessionmaint.isValidSession(tSession)) {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%

		int accId = EJBUtil.stringToNum((String) tSession.getValue("accountId"));
		String uName = (String) tSession.getValue("userName");
		String userIdFromSession = (String) tSession.getValue("userId");

		String studyId = request.getParameter("studyId");
		String bgtcalId ;
		String budgetId = request.getParameter("budgetId");
		/*FIN-22375 Date :16 Aug 2012 By : Yogendra Pratap*/
		String budgetTemplate ="";
		budgetTemplate = request.getParameter("budgetTemplate");
		String bgtcalIdStr = "";
		String bgtcalFlagStr = "";
		int baselineIndex = -1;
		int calcount = 0;
		int mileRight = 0;
		String textDisp="";

		//KM-08Jun10-#4966
		CodeDao cdStat = new CodeDao();
		/* Changes for FIN-22381_09262012 : 17 Oct 2012 : By Yogendra Pratap Singh */
		//int wipPk = cdStat.getCodeId("milestone_stat","WIP");
		//String wipDesc = cdStat.getCodeDescription(wipPk);
		

		ArrayList teamId ;
		teamId = new ArrayList();
		TeamDao teamDao = new TeamDao();
		teamDao.getTeamRights(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(userIdFromSession));
		teamId = teamDao.getTeamIds();

		if (teamId.size() <=0)
		{
			mileRight  = 0;
			StudyRightsJB stdRightstemp = new StudyRightsJB();
		}
		if (teamId.size() == 0) {
			mileRight=0 ;
		}else {
			stdRightsB.setId(EJBUtil.stringToNum(teamId.get(0).toString()));
		 	ArrayList teamRights ;
						teamRights  = new ArrayList();
						teamRights = teamDao.getTeamRights();

						stdRightsB.setSuperRightsStringForStudy((String)teamRights.get(0));
						stdRightsB.loadStudyRights();


			if ((stdRightsB.getFtrRights().size()) == 0){
				mileRight = 0;
			}else{
				mileRight = Integer.parseInt(stdRightsB.getFtrRightsByValue("MILESTONES"));
			}
		}

		GrpRightsJB grprightsJB = (GrpRightsJB) tSession.getValue("GRights");
		int mileGrpRight = Integer.parseInt(grprightsJB.getFtrRightsByValue("MILEST"));



		if ((mileRight == 5 || mileRight  ==7) && (mileGrpRight >= 4))//JM: 06Jun2010: #4973
		{
			BudgetcalDao budgetcalDao = budgetcalB.getAllBgtCals(EJBUtil.stringToNum(budgetId));

			ArrayList bgtcalIds = budgetcalDao.getBgtcalIds();
			ArrayList bgtcalProtNames = budgetcalDao.getBgtcalProtNames();
			ArrayList bgtcalProtIds = budgetcalDao.getBgtcalProtIds();
			ArrayList bgtcalFlags = budgetcalDao.getBgtcalClinicFlags(); //Retrieving Admin/studysetup flags 

			while(bgtcalIds != null && (bgtcalProtIds.indexOf("0")>=0))
			{
				//remove the default baseline record
				baselineIndex = bgtcalProtIds.indexOf("0");
				if (baselineIndex > -1)
				{
					bgtcalIds.remove(baselineIndex);
					bgtcalProtNames.remove(baselineIndex);
					bgtcalProtIds.remove(baselineIndex);
					bgtcalFlags.remove(baselineIndex);
				}
			}

			calcount = bgtcalIds.size();

			if(bgtcalIds != null && calcount > 0) {

				String ddName = "bgtcalId onchange ='asyncValidate(document.milerule)'";
				if (ienet != 0)
					ddName += " onkeydown='asyncValidate(document.milerule)'";

				bgtcalIdStr = LC.L_Select_Cal/*Select Calendar*****/+": &nbsp;  " +  EJBUtil.createPullDown(ddName,0,bgtcalIds,bgtcalProtNames) ;
				bgtcalFlagStr = " " +  EJBUtil.createPullDown("bgtcalFlag style='display:none'",0,bgtcalIds,bgtcalFlags) ;

			}
			else
			{
				bgtcalIdStr = MC.M_NoCalLnkBgt_AddlMstone/*There are no Calendars linked with this Budget. You can only create 'Additional Milestones'*****/+" "+"<input type=hidden name=\"bgtcalId\" value=0>";
			}



			ArrayList curr = new ArrayList();


			CodeDao codeLst = new CodeDao();
			String 	dRule = "";

			CodeDao codeLstEventRule = new CodeDao();
			String dEMRule = "";



		//JM: 16Aug2010: #4971

		String roleCodePk="";

			ArrayList tId = new ArrayList();

			TeamDao teamRoleDao = new TeamDao();
			teamRoleDao.getTeamRights(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(userIdFromSession));
			tId = teamRoleDao.getTeamIds();

			if (tId != null && tId.size() > 0)
			{
				ArrayList arRoles = new ArrayList();
				arRoles = teamRoleDao.getTeamRoleIds();

				if (arRoles != null && arRoles.size() >0 )
				{
					roleCodePk = (String) arRoles.get(0);

					if (StringUtil.isEmpty(roleCodePk))
					{
						roleCodePk="";
					}
				}
				else
				{
					roleCodePk ="";
				}

			}
			else
			{
				roleCodePk ="";
			}

		CodeDao cdMs = new CodeDao();

		cdMs.getCodeValuesForStudyRole("milestone_stat",roleCodePk);

		ArrayList statSubTypes = cdMs.getCSubType();
		
		String msStat = cdMs.toPullDown("milestoneStat");


	%>

	<DIV  id="div1">


	   <form name="milerule" method="post" id="milerulefrm" action="saveMultiMilestones.jsp" onsubmit="ripLocaleFromAll(); if (validate(document.milerule)== false) { setValidateFlag('false'); applyLocaleToAll(); return false; } else { setValidateFlag('true'); return true;}">


	   <input type="hidden" name="studyId" value="<%=studyId%>">
	   <input type="hidden" name="budgetId" value="<%=budgetId%>">
	   <input type="hidden" name="calcount" value="<%=calcount%>">
		<%--FIN-22375 Date :16 Aug 2012 By : Yogendra Pratap --%>
	   <input type="hidden" name="budgetTemplate" value="<%=budgetTemplate%>"/>

	   <% if (!statSubTypes.contains("WIP")){%>

       	<input type="hidden" name="isWIPStat" value="0">

       <%}else{%>
       <input type="hidden" name="isWIPStat" value="WIP">

       <%}%>





	<table width="100%" class="basetbl"> <tr> <td width="14%"> <%= bgtcalIdStr + bgtcalFlagStr%> </td>
	  <td width="4%" align="left" style="padding-left: 0px;padding-right: 0px;"><%=LC.L_Holdback_Amt+" "+"%" %></td>
	  <td width="1%" align="left" style="padding-left: 0px;"><input name = "mainHoldback" id="holdback" onkeydown="return validateHoldback(event);" class="index numberfield selValue" onblur = "return validateHoldbackNumber(this.value);" type="text" maxlength="5" size="5" value=""></td>
 	  <td width="20%" align="left"><button id="apply_all"  name="apply_all" type="button" aria-disabled="false" onclick="holdback_applyAll();"><%=LC.L_Apply_All%></button></td>
	
	<span id="calStatMessage" > <%=textDisp%> </span></td> </tr></table><BR>
		<table class="basetbl" width="100%">

		<%
		int i = 0;


			ArrayList fk_visits= new ArrayList();




			String dStatus = "";
			String dpayType = "";
			String dpayFor = "";
			CodeDao codeLstPayType = new CodeDao();
			CodeDao codeLstPayFor = new CodeDao();

			ArrayList ruleSubType = new ArrayList();
			ArrayList EMRuleSubType = new ArrayList();

			CodeDao codeLstStatus = new CodeDao();
			SchCodeDao cdEvent = new SchCodeDao();

			String dEventStatus = "";

			codeLstStatus.getCodeValues("patStatus",accId);
			dStatus = codeLstStatus.toPullDown("dStatus");

	 		cdEvent.getCodeValues("eventstatus",0);
	 		dEventStatus =   cdEvent.toPullDown("dEventStatus");


			//JM: 30Dec2009: #3357
			codeLstPayType.getCodeValues("milepaytype");

	        //retrieve 'Receivable' by default
	        int default_sel = codeLstPayType.getCodeId("milepaytype", "rec");

			dpayType = EJBUtil.createPullDownWithStrNoSelectIfHidden("dpayType",""+default_sel,codeLstPayType.getCId(),codeLstPayType.getCDesc(),codeLstPayType.getCodeHide());




			codeLstPayFor.getCodeValues("milePayfor");
			 dpayFor = codeLstPayFor.toPullDown("dpayFor");



		  %>

		  <%

			codeLst.getCodeValues("VM");
			dRule = codeLst.toPullDown("rule");

			codeLstEventRule.getCodeValues("EM");
			dEMRule = codeLstEventRule.toPullDown("EMrule");


			ruleSubType = codeLst.getCSubType();

			if (ruleSubType != null )
			{
				for ( int k=0; k < ruleSubType.size(); k++)
				{
				 	%>
			 			<input type=hidden name=ruleSubType value='<%=ruleSubType.get(k) %>'>
				 	<%

				}
			}

			EMRuleSubType = codeLstEventRule.getCSubType();

			if (EMRuleSubType != null )
			{
				for ( int k=0; k < EMRuleSubType.size(); k++)
				{
				 	%>
			 			<input type=hidden name=EVRuleSubType value='<%=EMRuleSubType.get(k) %>'>
				 	<%

				}
			}

			%>
			<table width="100%" class="basetbl">
			<tr>
			<!--<th class="headernoColor" width = '35%'>Milestone Type<FONT class="Mandatory">* </FONT> </th> -->
			<th class="headernoColor" width = '20%'><%=LC.L_Milestone_Rule%><%--Milestone Rule*****--%><FONT class="Mandatory">* </FONT></th>
			<th class="headernoColor" width = '20%'><%=LC.L_Event_Status%><%--Event Status*****--%><FONT class="Mandatory">* </FONT></th>
			<%if(budgetTemplate.equalsIgnoreCase("C")){%><th class="headernoColor" width = '10%'><%=LC.L_Sponsor_Amount%><%--Sponsor Amount*****--%></th><%} %>			
			<th class="headernoColor"  width=10% >&nbsp;&nbsp;&nbsp;<%=LC.L_Holdback_Amt%>&nbsp;&nbsp;&nbsp;<%--Holdback*****--%></th>
			<th id="countTD" name="countTD" class="headernoColor"  width=10% ><%=LC.L_Pat_Count%><%--Patient Count*****--%></th>
			<th id="statusTD" name="statusTD" class="headernoColor"  width=15% ><%=LC.L_Patient_Status%><%--Patient Status*****--%></th>
			<th class="headernoColor" width='5%' ><%=LC.L_Limit%><%--Limit*****--%></th>
			<th class="headernoColor" width='5%' ><%=LC.L_Payment_Type%><%--Payment Type*****--%></th>
			<th class="headernoColor" width='15%' ><%=LC.L_Payment_For%><%--Payment For*****--%></th>			
			<th class="headernoColor" width=15% ><%=LC.L_Date_From%><%--Date From*****--%> </th>
			<th class="headernoColor" width=15% ><%=LC.L_Date_To%><%--Date To*****--%> </th>
			<th class="headernoColor" width=15% ><%=LC.L_Mstone_Status%><%--Milestone Status*****--%> <FONT class="Mandatory">* </FONT></th>
			</tr>

			<tr>
			<td></td>
			<td></td>
			<td id="countTD" name="countTD"></td>
			<td id="statusTD" name="statusTD" ></td>
			<%if(budgetTemplate.equalsIgnoreCase("C")){%><td></td><%} %>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>
			<% for ( int kk = 0; kk < 2; kk++) { %>
		 		<tr>
					<td align=left  class="tdDefault">
						<% if ( kk == 0 ) { %>
							<input type=checkbox name="milestoneType" value="VM" id = "VisitMilestone"> <b><%=LC.L_Visit_Mstones%><%--Visit Milestones*****--%> </b>
							<input type=hidden name="milestoneTypeSelected" value="0">
							<input type=hidden name="milestoneTypeCode" value="VM">


						<% } else { %>
							<input type=checkbox name="milestoneType" value="EM" id = "EventMilestone"> <b><%=LC.L_Evt_Mstones%><%--Event Milestones*****--%> </b>
							<input type=hidden name="milestoneTypeSelected" value="0">
							<input type=hidden name="milestoneTypeCode" value="EM">
						<% } %>
				</td>
				</tr>
				<tr>	<td align=left>
						<% if ( kk == 0 ) { %>
							<%=dRule%>
						<% } else { %>
							<%=dEMRule%>
						<% } %>

					</td>

					<td align=left><%=dEventStatus%></td>
					<%--FIN-22375 Date :16 Aug 2012 By : Yogendra Pratap --%>
					<% if ( kk == 0 ) { %>
						<%if(budgetTemplate.equalsIgnoreCase("C")){%>
							<td align="center"><input type="checkbox" name="sponsorAmountChk" value="VM"/></td>
							<td align="left" style="padding-left:0px;"><input id="holdbackVM" name = "holdbackAmt" onkeydown="return validateHoldback(event);" onblur = "return validateHoldbackNumber(this.value);" class="index numberfield selValue" type="text" maxlength="5" size="5" value="">%</td>
						<%}else{ %>
						<td align="left" style="padding-left:0px;"><input id="holdbackVM" name = "holdbackAmt" onkeydown="return validateHoldback(event);" onblur = "return validateHoldbackNumber(this.value);" class="index numberfield selValue" type="text" maxlength="5" size="5" value="">%</td>
						<%} %>
						<input type=hidden name="sponsorAmountSelected" value="0">
						<input type=hidden name="sponsorAmountCode" value="VM">
					<%}else{ %>
						<%if(budgetTemplate.equalsIgnoreCase("C")){%>
							<td align="center"><input type="checkbox" name="sponsorAmountChk" value="EM"/></td>
							<td align="left" style="padding-left:0px;"><input id="holdbackEM" name = "holdbackAmt" onkeydown="return validateHoldback(event);" onblur = "return validateHoldbackNumber(this.value);" class="index numberfield selValue" type="text" maxlength="5" size="5" value="">%</td>
						<%}else{ %>
							<td align="left" style="padding-left:0px;"><input id="holdbackEM" name = "holdbackAmt" onkeydown="return validateHoldback(event);" onblur = "return validateHoldbackNumber(this.value);" class="index numberfield selValue" type="text" maxlength="5" size="5" value="">%</td>
						<%} %>
						<input type=hidden name="sponsorAmountSelected" value="0">
						<input type=hidden name="sponsorAmountCode" value="EM">
					<%} %>
					
					<td align="center" id="countTD" name="countTD"><input type=text name=count size=5 maxlength=10 class="leftAlign numberfield" data-unitsymbol="" data-formatas="number" /></td>
					<td id="statusTD" name="statusTD"><%=dStatus%></td>

				<td><input type=text name="mLimit" size=3 maxlength=10  class="leftAlign numberfield" data-unitsymbol="" data-formatas="number"></td>
			<td >
		<%=dpayType%>
		</td>
		<td >
		<%=dpayFor%>
		</td>
		
		<td align = center><input type="text" onclick="showDatePicker('#dateFrom<%=kk %>');" name="dateFrom" id="dateFrom<%=kk %>" class="datefield" size ="12" MAXLENGTH ="8" /></td>
		<td align = center><input type="text" onclick="showDatePicker('#dateTo<%=kk %>');" name="dateTo" id="dateTo<%=kk %>" class="datefield" size ="12" MAXLENGTH ="8" /></td>
		<td align = center> <%=msStat%> </td>
		</tr>

		 <% } //end of for %>
		 <!-- for additional milestones-->

		  	<tr>
				<td align=left  class="tdDefault">
			 		<br><br>
							<input type=checkbox name="milestoneType" value="AM" id="AdditionalMilestone"> <b><%=LC.L_Addl_Mstones%><%--Additional Milestones*****--%> </b>
							<input type=hidden name="milestoneTypeSelected" value="0">
							<input type=hidden name="milestoneTypeCode" value="AM">

							<input type=hidden name="count" size=5 maxlength=10 class="leftAlign" >
							<input type=hidden name="dEventStatus" value="0">
							<input type=hidden name="dStatus" value="0">


			 	</td>
				</tr>
				<tr>
					<td align=left>-</td>
					<td align=left id="tempTd" style="display:none;">-</td>
					<td id="statusTD" name="statusTD" align=left>-</td>
					<%--FIN-22375 Date :16 Aug 2012 By : Yogendra Pratap --%>
					<%if(budgetTemplate.equalsIgnoreCase("C")){%>
						<td align="center"><input type="checkbox" name="sponsorAmountChk" value="AM"/></td>
						<td align="left" style="padding-left:0px;"><input id="holdbackAM" name = "holdbackAmt" class="index numberfield selValue"  onkeydown="return validateHoldback(event);" onblur = "return validateHoldbackNumber(this.value);" type="text"  maxlength="5" size="5" value="">%</td>
					<%}else{%>
					<td align="left" style="padding-left:0px;"><input id="holdbackAM" name = "holdbackAmt" class="index numberfield selValue" onkeydown="return validateHoldback(event);" onblur = "return validateHoldbackNumber(this.value);" type="text"  maxlength="5" size="5" value="">%</td>
					<%} %>	
						<input type=hidden name="sponsorAmountSelected" value="0">
						<input type=hidden name="sponsorAmountCode" value="AM">
					<td id="countTD" name="countTD" align=left>-</td>
					<td align=left id="tempTdNew">-</td>
				<td>-<input type=hidden name="mLimit" size=3 maxlength=10  class="leftAlign" ></td>
			<td >
		<%=dpayType%>
		</td>
		<td >
		<%=dpayFor%>
		</td>
		<td align = center> <%=msStat%> </td>
		<td >-<input type="hidden" name="dateFrom" class="datefield" size ="12" MAXLENGTH ="8" /></td>
		<td >-<input type="hidden" name="dateTo" class="datefield" size ="12" MAXLENGTH ="8" /></td>
		</tr>
		<tr><td colspan=7><p class="defComments">
		<%/* Changes for FIN-22381_09262012 : 17 Oct 2012 : By Yogendra Pratap Singh */
		/*FIN-22375 Date :16 Aug 2012 By : Yogendra Pratap */
		if(!budgetTemplate.equalsIgnoreCase("C")){ %>
		<%=MC.M_NoteMstone_PaitentSponsor_Amt_OptNonCal%><%--<b> Please Note: </b><BR>- Milestone Amount is Total Cost/ Patient amount. To use sponsor amounts as milestone amounts, please select Sponsor Amount from a comparative budget. <BR>- If ''Additional Milestones'' option is selected, all non-Calendar related line items will be published as ''Additional Milestones''*****--%>
    <%}else{ %>
    	<%=MC.M_NoteMstone_Sponsor_Amt_OptNonCal %><%--<b> Please Note: </b><BR>- Milestone Amount is Total Cost/ Patient amount. To use sponsor amounts as milestone amounts, please select Sponsor Amount. <BR>- If ''Additional Milestones'' option is selected, all non-Calendar related line items will be published as ''Additional Milestones' --%>
    <%}%> 

		</td> </p>
		</tr>
		<tr>
			<td colspan=7>
				<jsp:include page="submitBar.jsp" flush="true">
					<jsp:param name="displayESign" value="Y"/>
					<jsp:param name="formID" value="milerulefrm"/>
					<jsp:param name="showDiscard" value="Y"/>
					<jsp:param name="noBR" value="Y"/>
				</jsp:include>
			</td>
		</tr>
	</table>

	  </Form>
	  <%

		}
		else
		{
			%>

		  <jsp:include page="accessdenied.jsp" flush="true"/>

		  <%

		}
	} //end of if session times out

else

{

%>
  <jsp:include page="timeout_childwindow.jsp" flush="true"/>
  <%

} //end of else body for page right

%>
</div>
<div class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>


</body>

</html>












