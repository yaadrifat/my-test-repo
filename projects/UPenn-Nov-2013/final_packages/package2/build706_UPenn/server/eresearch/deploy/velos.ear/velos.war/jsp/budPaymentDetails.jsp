<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>
<title><%=LC.L_Payment_Dets%><%--Payment Details*****--%></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="calendar.js"></SCRIPT>

<SCRIPT  Language="JavaScript1.2">
 





  function validate(formobj)
  {
  	var ct;
  	var i;
    ct =   parseInt(formobj.applyAmount.length);
    
    if (isNaN(ct))
    {
    	ct = 1;
    }
    
    if (ct == 1)
    {
    	if (! isDecimalOrNumberValAndNotBlank(formobj.applyAmount.value) && !isNegNum(formobj.applyAmount.value))
			{
				alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid Number");*****/
				formobj.applyAmount.focus();
				return false;
			}
							
    }
    else if (ct > 1)
    {
    	for (i =0; i<ct;i++)
    	{
    		if (! isDecimalOrNumberValAndNotBlank(formobj.applyAmount[i].value) && !isNegNum(formobj.applyAmount[i].value))
			{
    			alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid Number");*****/
				formobj.applyAmount[i].focus();
				return false;
			}
    	
    	}
    
    }
    
	if (!(validate_col('e-Signature',formobj.eSign))) return false;
	if(isNaN(formobj.eSign.value) == true)  {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
	}		
	return true;
}
</SCRIPT>

</HEAD>


<BODY>

  <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
  <jsp:useBean id="PayB" scope="request" class="com.velos.eres.web.milepayment.PaymentDetailJB"/>
  <jsp:useBean id="BgtSecB" scope="request" class="com.velos.esch.web.bgtSection.BgtSectionJB"/>
   <jsp:useBean id="BgtB" scope="request" class="com.velos.esch.web.budget.BudgetJB"/>
     
  <%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.esch.business.common.*"%>
  <%



	String eSign = request.getParameter("eSign");	
	String mode = request.getParameter("mode");
	
	String studyId = request.getParameter("studyId");
	String paymentPk = request.getParameter("paymentPk");
	String pageRight = request.getParameter("pR");
	String budgetIdStr = request.getParameter("budgetId");
	
	int budId= 0;
	budId = EJBUtil.stringToNum(budgetIdStr);
	int secCount = 0;
	
boolean notReconciled = false;
HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession))

   {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
	<jsp:include page="include.jsp" flush="true"/>
<%   
   	String oldESign = (String) tSession.getValue("eSign");
	String eventId = (String) tSession.getValue("eventId");

	String ipAdd = (String) tSession.getValue("ipAdd");
	String usr = (String) tSession.getValue("userId");
	

	
%>

<%
if (StringUtil.isEmpty(mode))
{
	mode = "N";
}


	
%>

<SCRIPT>
//	fclose_to_role();
</SCRIPT>
 <!-- Get budget sections-->
 <%
 int totalCount  = 0;
%>
 	
 	<DIV class="popDefault"  > 	
 	
  <jsp:include page="paymenttabs.jsp" flush="true">
	<jsp:param name="studyId" value="<%=studyId%>"/>
	<jsp:param name="selectedTab" value="3"/>
	<jsp:param name="paymentPk" value="<%=paymentPk%>"/>
	<jsp:param name="pR" value="<%=pageRight%>"/>
  </jsp:include>
 	
 	
 	<form name="buddet" id="budpaymentdet" action="savepaymentdetails.jsp" method="post" onSubmit="ripLocaleFromAll(); if (validate(document.buddet)== false) { setValidateFlag('false'); applyLocaleToAll(); return false; } else { setValidateFlag('true'); return true;}" >
		<input type="hidden" name="studyId" value="<%=studyId%>"/> 	
		<input type="hidden" name="paymentPk" value="<%=paymentPk%>"/> 	
		<input type="hidden" name="pR" value="<%=pageRight%>"/> 	
		<input type="hidden" name="nextPage" value="linkBudgetPayBrowser.jsp"/> 	
		
		
	 		<table width="100%" class="basetbl outline midalign" cellpadding="0" cellspacing="0">
			<tr> 
			<th></th>
	        <th><%=MC.M_Applied_AmtToDate%><%--Applied Amount To Date*****--%></th>		
	        <th><%=MC.M_AddlAmt_ForPay%><%--Additional Applied Amount from this payment*****--%></th>		
	      </tr>		
 	
 	<%
 		
 		
 		
 		
 		
 		
 		// get previous data totals/or previous data entered for the same payment and invoice
 		PaymentDetailsDao pdao = new PaymentDetailsDao();
 		PaymentDetailsDao pdaoLevel1 = new PaymentDetailsDao();
 		int idxLevel1 = -1;
 		String idxLevel1Type = "";
 		int idxLevel2 = -1;
 		SchCodeDao schDao = new SchCodeDao();
		schDao.getCodeValues("category");
 		
 		int alreadyEnteredCount = 0; 
 		String prevAppliedAmountTotal = "0.00";
 		String prevAmount = "0.00";
 		String oldPk = "";
 		ArrayList arSectionIds = new ArrayList();
 		ArrayList arSectionNames = new ArrayList();
 		ArrayList arSectionCals = new ArrayList();
 		ArrayList arSectionCalnames = new ArrayList();
		ArrayList arLevel1 = new ArrayList();
	 	ArrayList arLevel1Amount = new ArrayList();
	 	ArrayList arLevel1PrevAppliedTotal= new ArrayList();
	 	ArrayList arLevel1OldPk = new ArrayList();
		ArrayList  arLevel1LinkedTo = new ArrayList();
 		String calName = "";
 		String secName = "";
 		String secId = "";
 		String calId = "";
 		String strType = "";
 		String arType[] = new String[2];
 		int secCountTotal = 0;
 		String strTitle = "";
 		String oldCal = "";
 		
 		String budName = "";
 		

 		arType[0] = "BC"; //budget categories
 		arType[1] = "BS"; //budget sections
 	 
	 		BgtSectionDao bud = new BgtSectionDao ();
	 		
	 		ArrayList arSectionId = new ArrayList();
	 		ArrayList arSectionName = new ArrayList();
	 		
	 		BgtB.setBudgetId(budId);
 			BgtB.getBudgetDetails();
 			budName = BgtB.getBudgetName();
			//old data 		
	 		
	 		pdao = PayB.getPaymentDetailsWithTotals(EJBUtil.stringToNum(paymentPk),budId,"B");
	 		
	 		alreadyEnteredCount = pdao.getPaymentDetailRowCount();
	 		
	 		notReconciled = true;
	 		
	 		if (alreadyEnteredCount <= 0) //there was no payment data added for this invoice and payment, get any previous totals (all payments)
	 		{
				 			
	 	 		pdao = PayB.getPaymentDetailsWithTotals(0,budId,"B");
	 			alreadyEnteredCount = pdao.getPaymentDetailRowCount();
	
	 		}
	 		
			 		if (alreadyEnteredCount > 0) // get data
			 		{
			 			//The control will be here if a payment is reconciled with any milestone/invoice/bugdet
				 		notReconciled = false;
			 			
			 			//get Main record
			
			 			prevAppliedAmountTotal =  (String )(pdao.getAppliedTotal()).get(0);
			 			prevAmount = (String ) (pdao.getAmount()).get(0);
			 			if (StringUtil.isEmpty(prevAmount))
			 			{
			 				prevAmount = "0.00";
			 				
			 			}
			 			oldPk = (String ) (pdao.getId()).get(0);
						
			 			pdaoLevel1 = pdao.getArLevel1();
			 			 			 			
			 			arLevel1 = pdaoLevel1.getLinkToLevel1();
			 	 		arLevel1Amount = pdaoLevel1.getAmount();
			 	 		arLevel1PrevAppliedTotal= pdaoLevel1.getAppliedTotal();
			 	 		arLevel1OldPk = pdaoLevel1.getId();
			 	 		arLevel1LinkedTo = pdaoLevel1.getLinkedTo();
			 		}
	 		
	 		//main record
	 		%>
	 			<tr > <%Object[] arguments = {budName};%>
	 			<td><%=VelosResourceBundle.getLabelString("L_Bgt_Dyna",arguments)%><%--Budget:</b> <%=budName%>*****--%> </td>
	 			<!-- Mukul: BUG 4020 NumberUtil.roundOffNo() function used to roundoff number upto two decimal places -->
	 			
	 			<td><span class="numberfield" data-unitsymbol="" data-formatas="currency"><%=prevAppliedAmountTotal%></span></td>
	 			<%String styleStr ="";
				if ((notReconciled && !isAccessibleFor(EJBUtil.stringToNum(pageRight),'N')) ||
					((!notReconciled) && !isAccessibleFor(EJBUtil.stringToNum(pageRight),'E'))){
					styleStr = "READONLY";
				}
				%> 	
			 	<td>	<input type="text" <%=styleStr %> name="applyAmount" class="numberfield" data-unitsymbol="" data-formatas="currency" value="<%=prevAmount%>" /></td>
		 		<input type= "hidden"  name="linktoId"  value="<%=budId%>" />
	 			<input type="hidden" name="linkType"  value="B" />
	 			<input type="hidden" name="oldpk"  value="<%=oldPk%>" />
		 		<input type= "hidden"  name="level2Id"  value="0" />
		 		<input type= "hidden"  name="level1Id"  value="0" />
		 		</tr>
	 		<%
			
			for (int k=0; k<arType.length; k++) 
			{
				strType = arType[k] ;
				
				if (strType.equals("BS"))
				{
					bud = BgtSecB.getAllBgtSections(budId);
		 		 		 		
			 		
			 		arSectionIds = bud.getBgtSectionIds();
			 		arSectionNames = bud.getBgtSectionNames();
			 		arSectionCals = bud.getBgtCals();
			 		arSectionCalnames  = bud.getBgtCalNames();
			 		strTitle = LC.L_Sections/*"Sections"*****/;
			 		
			 	}
			 	else
			 	{
			 		arSectionIds = schDao.getCId();
			 		arSectionNames = schDao.getCDesc();
			 		strTitle = LC.L_Categories/*"Categories"*****/;
			 	
			 	}	
			 	if (arSectionIds != null)
			 		{
			 			secCount = arSectionIds.size();
			 			
			 		}
			 		else
			 		{
				 		secCount  = 0;
			 		}
			 		
			 		secCountTotal = secCountTotal  + secCount;
			 		
			 	%>
		 		<tr > 
			        <td colspan = 6><br><p class="sectionHeadings"><%=strTitle%></p></td>
		    	 </tr>		
		
			 	<%	
					
			 		for (int i =0; i < secCount ; i++)
			 		{
			 				
							
					
			 				if (strType.equals("BS"))
							{
				 				calName =   (String)arSectionCalnames.get(i);
						 		calId = (String)arSectionCals.get(i);
						 		secId = (String)arSectionIds.get(i);
						 		
						 	}
						 	else
						 	{
							 	secId = String.valueOf(((Integer)arSectionIds.get(i)).intValue());
							 	calName =  ""; 
						 	}
						 		
							secName = (String)arSectionNames.get(i);
					 		
			
					 		
					 		// get the old data if any
					 		if (arLevel1 != null)
						 		  {
							 		  idxLevel1 = arLevel1.indexOf(secId);
							 		  
							 		  if (idxLevel1 >= 0)
							 		  {
							 		  	// check the record type, as for budgets, level 1 will also contain categories
							 		  	idxLevel1Type = (String) arLevel1LinkedTo.get(idxLevel1);
							 		  	
							 		  	if (!idxLevel1Type.equals(strType))
							 		  	{
							 		  	  idxLevel1 = arLevel1.lastIndexOf(secId); 
							 		  	}
							 		  	
							 		  	// check the record type, as for budgets, level 1 will also contain categories
							 		  	idxLevel1Type = (String) arLevel1LinkedTo.get(idxLevel1);
							 		  	
							 		  	if (idxLevel1Type.equals(strType))
							 		  	{
								 			prevAmount = (String) arLevel1Amount.get(idxLevel1);
								  			oldPk = (String ) arLevel1OldPk.get(idxLevel1);
								  			prevAppliedAmountTotal =  (String ) arLevel1PrevAppliedTotal.get(idxLevel1);
								  			
								  		}
								  		else
								  		{
									  		 prevAmount = "0.00";
								 			 oldPk  = "0";
								 			 prevAppliedAmountTotal = "0.00";
								  		
								  		}	
							 		  }
							 		  else
							 		  {
							 			 prevAmount = "0.00";
							 			 oldPk  = "0";
							 			prevAppliedAmountTotal = "0.00";
							 		  }
						 		  } 
			 		  		
			 		  		if (!oldCal.equals(calId))
			 		  		{
			 		  		%>
								<TR  >	
								<%Object[] argument1 = {calName};%>
			 		  				<td colspan = 6><br><p class="sectionHeadings"><%=VelosResourceBundle.getLabelString("L_CalName",argument1)%><%--Calendar Name: <%=calName%>*****--%></p></td>
			 		  			</TR>	
			 		  		<%	
			 		  		}
					 		
							if(i%2==0){ 
							%>	
								<TR class="browserEvenRow">	
							<%
							}else{
							%>
								<TR class="browserOddRow">
							<% 
							} 
							%>
				 			<td><%=secName %></td>
				 			<td><span class="numberfield" data-unitsymbol="" data-formatas="currency"><%=prevAppliedAmountTotal %></span></td>
				 			<!-- Mukul: BUG 4020 NumberUtil.roundOffNo() function used to roundoff number upto two decimal places -->
				 			<td><input type="text" <%=styleStr%> name="applyAmount" class="numberfield" data-unitsymbol="" data-formatas="currency" value="<%=prevAmount%>" />
						 		<input type= "hidden"  name="level1Id"  value="<%=secId%>" />
					 			<input type="hidden" name="linkType"  value="<%=strType%>" />
					 			<input type="hidden" name="oldpk"  value="<%=oldPk%>" />
	 					 		<input type= "hidden"  name="linktoId"  value="<%=budId%>" />
	 					 		<input type= "hidden"  name="level2Id"  value="0" />
	 					 		
						 	</td>
						 	
			 			</tr>
			 			
		
			 			<%
						oldCal = calId;
			 			
			 			// end of for
			 		}
			} // for types	 	
 		
 
 %>
 </table>

<P>
<br>
<% 
	//add 1 for default budget row
	secCountTotal = secCountTotal + 1;
%>
<input type= "hidden"  name="totalCount"  value="<%=secCountTotal%>" />


</P>


<% String showSubmit ="";
	if((notReconciled && isAccessibleFor(EJBUtil.stringToNum(pageRight),'N')) ||
		((!notReconciled) && isAccessibleFor(EJBUtil.stringToNum(pageRight),'E')))
	{ showSubmit ="Y";} else {showSubmit="N";} 
%>

<%if (showSubmit.equals("Y")){ %>
<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="budpaymentdet"/>
		<jsp:param name="showDiscard" value="N"/>
		<jsp:param name="showSubmit" value="<%=showSubmit%>"/>
   </jsp:include>
<%} %>

</form>
</div>
<%

 


}//end of if body for session

else
{
%>
  <jsp:include page="timeout_childwindow.jsp" flush="true"/> 
  <%
}
%>

</BODY>

<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>

</HTML>
<%!
private boolean isAccessibleFor(int pageRight, char type) {
    switch(type) {
    case 'V': return ((pageRight & 0x0004) >> 2) == 1;
    case 'E': return ((pageRight & 0x0002) >> 1) == 1;
    case 'N': return  (pageRight & 0x0001) == 1;
    }
    return false;
}
%>