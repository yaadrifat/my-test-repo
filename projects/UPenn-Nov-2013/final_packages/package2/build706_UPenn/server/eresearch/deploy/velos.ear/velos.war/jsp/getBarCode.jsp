<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<!-- Creation date: 3/6/2002 -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ page language = "java" import = "com.velos.eres.service.util.*,java.awt.image.BufferedImage,java.io.*, org.krysalis.barcode4j.impl.code128.Code128Bean
,org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider,
	org.krysalis.barcode4j.tools.UnitConv "%>

<title><%=LC.L_Generate_BarCode%><%--Generate Bar Code*****--%></title>
</head>

<script>
	function test() { 
		alert("<%=LC.L_Dsf%>");/*alert("dsf");*****/
	}
</script>

	<script language="Javascript" src="SearchJS2.js">
</script>
		
		
<form name="gen" method="post" action="getBarCode.jsp">
	
	<%
 	String codeNumber = "";

	codeNumber = request.getParameter("codeNumber");
	
	if (StringUtil.isEmpty(codeNumber))
	{
		codeNumber="Welcome2eSample";
	}
%>

		<input type = text name="codeNumber" value="<%=codeNumber%>"/>
		<button type="submit"><%=LC.L_Submit%></button>

		
<%
 	
        try {
            //Create the barcode bean
            Code128Bean bean = new Code128Bean();
            
            final int dpi = 150;
            
            //Configure the barcode generator
            bean.setModuleWidth(UnitConv.in2mm(1.0f / dpi)); //makes the narrow bar 
                                                             //width exactly one pixel
            
            bean.doQuietZone(true);
            
            //Open output file
            
            //File outputFile = new File("..//temp//out.jpg");
            
            File outputFile = new File("..//server//eresearch//deploy//eres.war//jsp//esample/"+codeNumber+".jpg");
            
            
            OutputStream outf = new FileOutputStream(outputFile);
            try {
                //Set up the canvas provider for monochrome JPEG output 
                BitmapCanvasProvider canvas = new BitmapCanvasProvider(
                        outf, "image/jpeg", dpi, BufferedImage.TYPE_BYTE_BINARY, false, 0);
            
                //Generate the barcode
                bean.generateBarcode(canvas, codeNumber);
            
                //Signal end of generation
                canvas.finish();
                %>
                 <BR><BR><img src="..//jsp/esample//<%=codeNumber%>.jpg" />
                    
                     
                	<%
            } finally {
                out.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
   
%>
</form>