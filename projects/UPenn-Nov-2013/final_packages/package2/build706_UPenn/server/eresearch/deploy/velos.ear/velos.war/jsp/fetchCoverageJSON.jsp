<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList,java.util.HashMap,org.json.*"%>
<%@page import="com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.*,com.velos.eres.service.util.StringUtil"%>
<%@page import="com.velos.eres.business.common.SettingsDao"%>
<%@page import="com.velos.esch.business.common.EventdefDao"%>
<%@page import="com.velos.esch.business.common.EventAssocDao"%>
<%@page import="com.velos.esch.business.common.SchCodeDao"%>
<%@page import="com.velos.esch.service.util.Rlog"%>
<%@page import="com.velos.esch.web.protvisit.ProtVisitCoverageJB"%>
<%@page import="com.velos.eres.web.studyRights.StudyRightsJB" %>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="protVisitB" scope="request" class="com.velos.esch.web.protvisit.ProtVisitJB"/>
<jsp:useBean id="visitdao" scope="request" class="com.velos.esch.business.common.ProtVisitDao"/>
<jsp:useBean id="commonB" scope="request" class="com.velos.eres.web.common.CommonJB"/>

<%
    request.setCharacterEncoding("UTF-8");
    response.setContentType("application/json");
    // response.setContentType("text/html");

	HttpSession tSession = request.getSession(true);
    if (!sessionmaint.isValidSession(tSession)) {
        // Session is invalid; print an error and exit
        response.sendError(HttpServletResponse.SC_FORBIDDEN, "SESSION_TIME_OUT");
        JSONObject jsObj = new JSONObject();
        jsObj.put("error", new Integer(-1));
        jsObj.put("errorMsg", MC.M_UsrNot_LoggedIn/*User is not logged in.*****/);
	    out.println(jsObj.toString());
        return;
    }
    
	String protocolId = StringUtil.htmlEncodeXss(request.getParameter("protocolId"));
	if(protocolId == "" || protocolId == null || protocolId.equals("null") || protocolId.equals("")) {
	    // A valid protocol ID is required; print an error and exit
        JSONObject jsObj = new JSONObject();
        jsObj.put("error", new Integer(-2));
        jsObj.put("errorMsg", MC.M_PcolId_Invalid/*Protocol ID is invalid.*****/);
	    out.println(jsObj.toString());
	    return;
	}
	
	ProtVisitCoverageJB pvCoverageJB = new ProtVisitCoverageJB();
	String calledFrom = StringUtil.htmlEncodeXss(request.getParameter("calledFrom"));
	int finDetRight = StringUtil.stringToNum(request.getParameter("fin"));
    
	String jsString = null;
	try {
	    jsString = pvCoverageJB.fetchCoverageJSON(protocolId, calledFrom, finDetRight, (String)tSession.getAttribute("accountId"));
	} catch(Exception e) {
	    Rlog.fatal("fetchCoverage", "Error while calling fetchCoverageJSON "+e);
	}
	out.println(StringUtil.trueValue(jsString));
    // <html><head></head><body></body></html>
%>

<%! // Define Java functions here
private String createCheckbox(String eventId, String visitId, boolean checked) {
	StringBuffer sb = new StringBuffer();
	sb.append("<input type='checkbox' ");
	sb.append(" name='e").append(eventId).append("v").append(visitId).append("' ");
	sb.append(" id='e").append(eventId).append("v").append(visitId).append("' ");
	if (checked) {
	    sb.append(" checked ");
	}
	sb.append(" >");
	return sb.toString();
}
%>
