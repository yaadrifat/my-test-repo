<%@taglib prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.service.util.LC"%>
<%@page import="com.velos.eres.service.util.MC"%>
<%@page import="com.velos.eres.web.grpRights.GrpRightsJB"%>

<%@page import="com.velos.eres.business.common.*,java.util.*"%>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id="groupB" scope="page" class="com.velos.eres.web.group.GroupJB"/>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<%@page import="com.velos.eres.service.util.EJBUtil"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:include page="ui-include.jsp" flush="true"/> 
<%!
private boolean isAccessibleFor(int pageRight, char type) {
    switch(type) {
    case 'V': return ((pageRight & 0x0004) >> 2) == 1;
    case 'E': return ((pageRight & 0x0002) >> 1) == 1;
    case 'N': return  (pageRight & 0x0001) == 1;
    }
    return false;
}
%>
<%
int ctrpRights=0;
try {	if (!sessionmaint.isValidSession(request.getSession(false))) { return; }
	GrpRightsJB grpRights = (GrpRightsJB) request.getSession(false).getAttribute("GRights");
	if (grpRights == null) { return; }
	ctrpRights=Integer.parseInt(grpRights.getFtrRightsByValue("REGREPORT_CTRP"));
	if (!isAccessibleFor(ctrpRights, 'V')){
		out.println("<div><p>&nbsp;</p><p class='sectionHeadings' align='center'>"+LC.L_Access_Forbidden+"</p></div>");
		return;
	}
	
	CodeDao cdDraftStatus = new CodeDao();
	String readyDraftStatus = cdDraftStatus.getCodeDescription(
			cdDraftStatus.getCodeId("ctrpDraftStatus","readySubmission")); 
%>
<style>
 	.ui-autocomplete {
		max-height: 150px;
		overflow-y: auto;
		height:150px;
		position: absolute; cursor: default;
		background-color:#FFFFFF;
	}
	/*Override the Class for making the text Black
	 to make visible to USER 
	 for Bug# 15782*/
	.ui-widget-content A {
    /*color: #AA202E;*/
    color: #000000;
}
</style>
<script >
var readyDraftStatus = "<%=readyDraftStatus%>";
var L_Access_Rights = "<%=LC.L_Access_Rights%>";
var L_Valid_Esign ="<%=LC.L_Valid_Esign%>";
var L_Invalid_Esign ="<%=LC.L_Invalid_Esign%>";
var CTRP_DraftTrialSubmCategory = "<%=LC.CTRP_DraftTrialSubmCategory%>";
var L_Create="<%=LC.L_Create%>";

function checkSearch()
{ 
	
	document.getElementById('studyIds').value="";
	document.getElementById('ctrpUnmark').value="";
	paginate_study.runFilter();
	$j("#messages").delay(800).fadeOut('slow');
	arrayDownload = [];
	checkedDraftIds="";
	
	
}
var studyIds="";

//Modified for S-22306 Enhancement : Raviesh
studypatLink = function(elCell, oRecord, oColumn, oData) {
	var data=oData;
		var record=oRecord;
 		var htmlStr="";
 		var studyNumber=record.getData("STUDY_NUMBER");
 		if(studyNumber.length>50){
 			studyNumber=studyNumber.substring(0,50);
 			htmlStr="<a href=\"study.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&studyId="+record.getData("PK_STUDY")+"\"><img src=\"../images/jpg/study.gif\" alt=\"<%=LC.L_Std_Admin%><%--<%=LC.Std_Study%> Administration*****--%>\" border=0></a> &nbsp;"+studyNumber+"&nbsp;<img class=\"asIsImage\" src=\"./images/More.png\" style=\"border-style: none;\" onmouseover=\"return overlib(\'"+record.getData("STUDY_NUMBER")+"\',CAPTION,'<%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Title*****--%>' ,LEFT , ABOVE);\" onmouseout=\"return nd();\"/>"; 			
		}else{
			htmlStr="<a href=\"study.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&studyId="+record.getData("PK_STUDY")+"\"><img src=\"../images/jpg/study.gif\" alt=\"<%=LC.L_Std_Admin%><%--<%=LC.Std_Study%> Administration*****--%>\" border=0></a> &nbsp;"+studyNumber;
			}
 		var sTeamRight=oRecord.getData("STUDY_TEAM_RIGHTS");
 		sTeamRight=((sTeamRight==null) || (!sTeamRight) )?"":sTeamRight;
 		var seq=parseInt($('rightSeq').value);
		if (!seq) seq=0;
		var mPatRight=0;
		if (sTeamRight.length>seq)
		{
 		mPatRight=parseInt(sTeamRight.substring(seq-1,seq));
 		mPatRight=((mPatRight==null) || (!mPatRight) )?"":mPatRight;
		}
		elCell.innerHTML =htmlStr;
};

	draftStatusLink = function(elCell, oRecord, oColumn, oData) {

		var data=oData;
		var record=oRecord;
	    var status=record.getData("FK_CODELST_STAT")==null?"":record.getData("FK_CODELST_STAT");

		$j('#ctrpStudiesNDraftOpts').show();

		var statusSubType=record.getData("FK_CODELST_STAT_SUBTYPE")==null?"":record.getData("FK_CODELST_STAT_SUBTYPE");
		var draftType=record.getData("CTRP_DRAFT_TYPE")==null?"":record.getData("CTRP_DRAFT_TYPE");
		var researchType=record.getData("CTRP_DRAFT_TYPE_STAT")==null?"":record.getData("CTRP_DRAFT_TYPE_STAT");
		var draftPK=record.getData("PK_CTRP_DRAFT")==null?"":record.getData("PK_CTRP_DRAFT");
		var studyPK=record.getData("PK_STUDY")==null?"":record.getData("PK_STUDY");

	    var htmlStr="";
	    var imagStr="";

	    if(<%=isAccessibleFor(ctrpRights, 'V')%>) 
	    {
			switch(draftType){
			case "1" ://Industrial
				if (researchType != "indus"){
					imagStr ="&nbsp;<img src=\"./images/ctrp_images/redExlamation.png\" style=\"border-style: none;\" />&nbsp;"+status;
				   	htmlStr="<a href=\"javascript:void(0);\" onClick=\"return alert('Page under construction'); return false;\" style=\"text-decoration:none;\" onmouseover=\"return overlib('Conflict found between Study Summary &gt;&gt;Research Type and Trial Submission Category',CAPTION,'"+ CTRP_DraftTrialSubmCategory +"');\" onmouseout=\"return nd();\">"+imagStr+"</a>";
				}else{
					if(statusSubType=="readySubmission"){
				    	imagStr="<img src=\"./images/ctrp_images/success.png\" style=\"border-style: none;\"/>&nbsp;"+status;
				    	htmlStr="<a href=\"ctrpDraftIndustrial?draftId="+draftPK+"\" style=\"text-decoration:none;\">&nbsp;"+imagStr+"</a>";
				    }else {
			    		imagStr="<img src=\"./images/ctrp_images/exclaimation.png\" style=\"border-style: none;\"/>&nbsp;"+status;
						htmlStr="<a href=\"ctrpDraftIndustrial?draftId="+draftPK+"\" style=\"text-decoration:none;\">&nbsp;"+imagStr+"</a>";
				    }
				}
				break;
			case "0" ://Non-Industrial
				if (researchType != "other" && researchType != "insti" && researchType != "national"){
					imagStr ="&nbsp;<img src=\"./images/ctrp_images/redExlamation.png\" style=\"border-style: none;\" />&nbsp;"+status;
				   	htmlStr="<a href=\"ctrpDraftNonIndustrial?draftId="+draftPK+"\" style=\"text-decoration:none;\"onmouseover=\"return overlib('Conflict found between Study Summary &gt;&gt; Research Type and Trial Submission Category',CAPTION,'"+ CTRP_DraftTrialSubmCategory +"');\" onmouseout=\"return nd();\">"+imagStr+"</a>";
				}else{
					if(statusSubType=="readySubmission"){
				    	imagStr="<img src=\"./images/ctrp_images/success.png\" style=\"border-style: none;\"/>&nbsp;"+status;
				    	htmlStr="<a href=\"ctrpDraftNonIndustrial?draftId="+draftPK+"\" style=\"text-decoration:none;\">&nbsp;"+imagStr+"</a>";
				    }else {
			    		imagStr="<img src=\"./images/ctrp_images/exclaimation.png\" style=\"border-style: none;\"/>&nbsp;"+status;
						htmlStr="<a href=\"ctrpDraftNonIndustrial?draftId="+draftPK+"\"  return false;\" style=\"text-decoration:none;\">&nbsp;"+imagStr+"</a>";
				    }
				}
				break;
			}
	    }else{
	    	imagStr="<img src=\"./images/ctrp_images/Frozen.png\" style=\"border-style: none;\" />&nbsp;"+status;
		    htmlStr="<div align=\"center\" ><a href=\"javascript:void(0);\" style=\"text-decoration:none;\"onmouseover=\"return overlib('"+M_InsuffAccRgt_CnctSysAdmin+"',CAPTION,'"+ L_Access_Rights +"');\" onmouseout=\"return nd();\">"+imagStr+"</a></div>";
        }
	   
	    elCell.innerHTML =htmlStr;
};

var arrayDownload = [];
function setDownloadArray(pkDraft, fkStudy, draftTypeSubType, draftStatSubType){
	var arraySize = arrayDownload.length;
	arrayDownload[arraySize] = 'pkDraft:'+pkDraft+'fkStudy:'+fkStudy;

	arrayDownload[arraySize] += 'draftTypeSubType:';
	if (draftTypeSubType != undefined && draftTypeSubType.length > 0){
		arrayDownload[arraySize] += draftTypeSubType;
	}
	arrayDownload[arraySize] += 'draftStatSubType:';
	if (draftStatSubType != undefined && draftStatSubType.length > 0){
		arrayDownload[arraySize] += draftStatSubType;
	}
}
//Added for Bug#8464 | Date: 20 FEB 2012 |By :YPS
var checkedDraftIds="";
function selectedDraftIds(status,checkdraftId,draftTypeSubType,draftStatSubType){
		if(status.checked){
			if(checkedDraftIds==""){
				checkedDraftIds=$j.trim(checkdraftId)+"~"+$j.trim(draftTypeSubType)+"~"+$j.trim(draftStatSubType)+",";
			}else{
				checkedDraftIds=$j.trim(checkedDraftIds)+$j.trim(checkdraftId)+"~"+$j.trim(draftTypeSubType)+"~"+$j.trim(draftStatSubType)+",";
			}
		}
		else{
			var replaceStr = $j.trim(checkdraftId)+"~"+$j.trim(draftTypeSubType)+"~"+$j.trim(draftStatSubType)+",";
			checkedDraftIds = replaceCheckedDraftIds($j.trim(checkedDraftIds), replaceStr, '');
			$j.trim(checkedDraftIds);
		}
	}
//Added for Bug#8464 | Date: 20 FEB 2012 |By :YPS
function replaceCheckedDraftIds(allSelectedDraftIds, replace, replaceWithStr) {
	  return allSelectedDraftIds.replace(new RegExp(replace, 'g'),replaceWithStr);
}

studyCheckBox = function(elCell, oRecord, oColumn, oData) {
		var data=oData;
    		var record=oRecord;
    		var htmlStr="";

    		var pkDraft=oRecord.getData("PK_CTRP_DRAFT");
    		var draftTypeSubType=oRecord.getData("CTRP_DRAFT_TYPE_STAT");
    		var draftStatSubType=oRecord.getData("FK_CODELST_STAT_SUBTYPE");
    		var fkStudy=oRecord.getData("PK_STUDY");
    		setDownloadArray(pkDraft, fkStudy, draftTypeSubType, draftStatSubType);
    		
    		var sTeamRight=oRecord.getData("STUDY_TEAM_RIGHTS");
     		sTeamRight=((sTeamRight==null) || (!sTeamRight) )?"":sTeamRight;
    		var seq=parseInt($('rightSeq').value);
    		if (!seq) seq=0;
    		var mPatRight=0;
    		if (sTeamRight.length>seq)
    		{
     		mProtRight=parseInt(sTeamRight.substring(seq-1,seq));
     		mProtRight=((mProtRight==null) || (!mProtRight) )?"":mProtRight;
    		}
    		if (f_check_perm_noAlert(mProtRight,'E'))
    		{
        		 htmlStr="<input type='checkbox' name='studycheckboxes' id='studycheckboxes' onclick=\"selectedDraftIds(this,'"+pkDraft+"','"+draftTypeSubType+"','"+draftStatSubType+"')\" value='"+record.getData("PK_STUDY")+"'>";
    		}else
    		{
    		 htmlStr="<a href=\"javascript:void(0);\" style=\"text-decoration:none;\"onmouseover=\"return overlib('"+M_InsuffAccRgt_CnctSysAdmin+"',CAPTION,'"+ CTRP_DraftTrialSubmCategory +"');\" onmouseout=\"return nd();\">&nbsp;<img src=\"./images/ctrp_images/Frozen.png\" style=\"border-style: none;\" /></a>";
    		 htmlStr +="<input type='checkbox' disabled name='studycheckboxes' id='studycheckboxes' value='"+record.getData("PK_STUDY")+"'>";
        		}

    		
    		elCell.innerHTML =htmlStr;
};

statusLink  =
	 function(elCell, oRecord, oColumn, oData)
	  {
		var record=oRecord;
      		var htmlStr="";
      		var cl="";
    		var status=record.getData("STATUS"); 
    		var sub=record.getData("STATUS_SUBTYPE"); 
    		  sub=((sub==null) || (!sub) )?"":sub;
      		if (sub=="temp_cls") cl="Red"; 
      		if (sub=="active") cl="Green";
      		var nt=record.getData("STUDYSTAT_NOTE");
      		nt=((nt==null) || (!nt) )?"":nt;
      		nt=htmlEncode(nt);
      		nt = escapeSingleQuoteEtc(nt);
      		var tt="<tr><td><font size=2><%=LC.L_Note%>:</font><font size=1>"+nt+"</font></td></tr><tr><td><font size=2><%=LC.L_Organization%>:</font><font size=1> " +record.getData("SITE_NAME")    <%--Fixed bug#7913:Akshi--%>
      		+"</font></td></tr>";
      		if(status!="...")
      		{
          		htmlStr="<A href=\"studystatusbrowser.jsp?srcmenu=tdmenubaritem3&selectedTab=3&studyId="+record.getData("PK_STUDY")   
      		+ "\" onmouseover=\"return overlib('"+tt+"',CAPTION,'"+record.getData("STUDYSTAT_DATE_DATESORT")+"');\" onmouseout=\"return nd();\"><FONT class=\""+cl+"\">"+record.getData("STATUS")+"</FONT> </A>";
      		}else{
      			htmlStr=status;
          	}
      		elCell.innerHTML=htmlStr;
		
    }
//Modified for S-22306 Enhancement : Raviesh    
titleLink = 
	function(elCell, oRecord, oColumn, oData)
	  {
		var record=oRecord;
        		var htmlStr="";
		var title = record.getData("STUDY_TITLE"); 
		title = ((title==null) || (!title) )? "-":title;
		title = htmlEncode(title);
		title = escapeSingleQuoteEtc(title);
		if(title.length>50)
		{
		var studyTitle=title.substring(0,50);
		htmlStr = studyTitle+"&nbsp;<img class=\"asIsImage\" src=\"./images/More.png\" border=\"0\" onmouseover=\"return overlib(\'"+title+"\',CAPTION,'<%=LC.L_Study_Title%><%--<%=LC.Std_Study%> Title*****--%>' ,LEFT , ABOVE);\" onmouseout=\"return nd();\">";
		}else{
			htmlStr=title;
		}
		elCell.innerHTML = htmlStr;
     }
resType=	function(elCell, oRecord, oColumn, oData)
{
	var record=oRecord;
		var htmlStr="";
	var title = record.getData("RESEARCH_TYPE"); 
	var researchType=record.getData("CTRP_DRAFT_TYPE_STAT")==null?"":record.getData("CTRP_DRAFT_TYPE_STAT");
	title = ((title==null) || (!title) )? "-":title;
	title = htmlEncode(title);
	title = escapeSingleQuoteEtc(title);
	var color;
	//Bug#7936
	if (researchType == "other" || researchType == "insti" || researchType == "national"){
	color="green";			
	}else if(researchType == "indus"){
		color="blue";
	}else{
	 color="red";
	 }
	htmlStr = "<font color="+color+">"+title+"</font>"; 
	elCell.innerHTML = htmlStr;
}
$E.addListener(window, "load", function() {  
	clearValues();
	paginate_study=new VELOS.Paginator('ctrpDraftBrowser',{
					rowsPerPage  : 10,
	 				sortDir:"asc", 
					sortKey:"STUDY_NUMBER",
					defaultParam:"userId,accountId,grpId,superuserRights,pmtclsId",
					filterParam:"searchCriteria,nciTrialIden,draftActFrmDt,draftActToDt,ctrpDraftSearch,protocolStatus,nciTrialStatus,ctrpDraftAction,ctrpDraftStatuses",
					dataTable:'serverpagination',
					rowSelection:[10,15,20,50],
					navigation: true,
					rowsPerPage:10
					});
	checkSearch();
	checkedDraftIds=""; 
	}

)  

function postQuery(type)
{	
	var checkBoxes =document.getElementsByName("studycheckboxes").length;
	var checkedStudy = new Array();
	var checkedDraft = new Array();

	var indusFlag = false;
	var nonIndusFlag = false;
	var isAnyDraftSeleted = false;
	var isAnyStudySeleted = false;

	var strDownload = '';
	var statDelimiter = 'draftStatSubType:';
	var typeDelimiter = 'draftTypeSubType:';
	var pkDraftDelimiter='pkDraft:';
	var fkStudyDelimiter ='fkStudy:';
	
	var startPt = 0;
	var endPt = 0;
	
	var cS=0;

	var confirmFlag = false;
	//Modified on 15/Feb/2012 For Bug# 8464 :Yogendra
	var finalCheckedDraftIds="";
	var checkedDraftIdsArr = new Array();
	if(type=="download"){
		if(checkedDraftIds.length>0){
			checkedDraftIdsArr = checkedDraftIds.split(",");
			for(var i=0; i<checkedDraftIdsArr.length-1;i++ ){
				var checkIdData  = checkedDraftIdsArr[i];
				var checkIdDataArray = new Array();
				checkIdDataArray = checkIdData.split("~");
					if(finalCheckedDraftIds==""){	 
						finalCheckedDraftIds = checkIdDataArray[0];
						if(checkIdDataArray[0].toString()=='0'){
							isAnyStudySeleted = true;
						}	
					}else{
						finalCheckedDraftIds = finalCheckedDraftIds+","+checkIdDataArray[0];
						if(checkIdDataArray[0].toString()=='0'){
							isAnyStudySeleted = true;
						}
					}
					if(checkIdDataArray[2] != 'readySubmission'){
						if(confirmFlag==false)
							confirmFlag =true;
					}
					if(checkIdDataArray[1] == "indus"){
						indusFlag = true;
					}
					if(checkIdDataArray[1] == "other" || checkIdDataArray[1] == "insti" || checkIdDataArray[1] == "national"){
						nonIndusFlag = true;
					}
					if (indusFlag && nonIndusFlag){
						alert(CTRP_IndNonIndDraftsDownload);
						return;
					}
			} 
		}else{
			alert(M_PlsSel_OneStd);
			return false;
		}

		if(isAnyStudySeleted == true){
			alert(M_CTRP_ONLY_DRFT_DWN);
			return false;
		}
		
		if(confirmFlag){
			if(!confirm(CTRP_ConfirmDraftDownload))
				return;
		}
	}else{

		if(checkedDraftIds.length>0){
			checkedDraftIdsArr = checkedDraftIds.split(",");
			for(var i=0; i<checkedDraftIdsArr.length-1;i++ ){
				var checkIdData  = checkedDraftIdsArr[i];
				var checkIdDataArray = new Array();
				checkIdDataArray = checkIdData.split("~")
				if(checkIdDataArray[0].toString()=='0'){
					isAnyStudySeleted = true;
				}else{
					isAnyDraftSeleted = true;
				}
			}
		}
		
		for (var i=0;i<checkBoxes;i++)
		{   
			if(document.getElementsByName("studycheckboxes")[i].checked)
			{
				//Modified for Bug#8464 | Date: 20 FEB 2012 |By :YPS
				checkedStudy[cS]=document.getElementsByName("studycheckboxes")[i].value;
				cS++;
			}
		}
		
		if(checkedStudy.length<=0)
		{
			alert(M_PlsSel_OneStd);
			return false;
		}
	}
	if(type=="download"){

		var flagValue =0;
		if(nonIndusFlag){
			flagValue=1;
		}
		document.getElementById('studyIds').value=checkedStudy;
		document.getElementById('draftIds').value=finalCheckedDraftIds;
		//Modified For Bug#8464 | Date:28 Feb 2012 | By:Yogendra Pratap Singh
		var width  = 400;
 		var height = 200;
 		var left   = (screen.width  - width)/2;
 		var top    = (screen.height - height)/2;
 		var params = 'width='+width+', height='+height;
 		 params += ', top='+top+', left='+left;
 		 params += ', directories=no';
 		 params += ', location=no';
 		 params += ', menubar=no';
 		 params += ', resizable=no';
 		 params += ', scrollbars=no';
 		 params += ', status=yes';
 		 params += ', toolbar=no';
		var win = window.open("ctrpDraftExport?studyIds="+checkedStudy+"&industryFlag="+indusFlag+"&nonIndustryFlag="+flagValue+"&selectedDraftIds="+finalCheckedDraftIds,'',params);
		clearValues();
		document.getElementById('searchCriteria').value="";
		document.getElementById('nciTrialIden').value="";
		document.getElementById('draftActFrmDt').value="";
		document.getElementById('draftActToDt').value="";
		paginate_study.runFilter();
		win.focus();
	}else{
		if(type=="unmark"){
			if(isAnyDraftSeleted==true){
				alert("<%=MC.M_CTRP_ONLY_STD_UNMRK%>");
				return false;
			}
			$j("#confirmMessage").html("<%=MC.CTRP_DraftConfmReportUnmark%>");
			document.getElementById('ctrpUnmark').value='1';
		}else if(type=="del"){


			if(checkedDraftIds.length>0){
				checkedDraftIdsArr = checkedDraftIds.split(",");
				for(var i=0; i<checkedDraftIdsArr.length-1;i++ ){
					var checkIdData  = checkedDraftIdsArr[i];
					var checkIdDataArray = new Array();
					checkIdDataArray = checkIdData.split("~");
						if(finalCheckedDraftIds==""){	 
							finalCheckedDraftIds = checkIdDataArray[0];
							if(checkIdDataArray[0].toString()=='0'){
								isAnyStudySeleted = true;
							}	
						}else{
							finalCheckedDraftIds = finalCheckedDraftIds+","+checkIdDataArray[0];
							if(checkIdDataArray[0].toString()=='0'){
								isAnyStudySeleted = true;
							}
						}
				
					
				} 
			}

			document.getElementById('studyIds').value=checkedStudy;
			document.getElementById('draftIds').value=finalCheckedDraftIds;

			
			if(isAnyStudySeleted==true){
				alert("<%=MC.M_CTRP_ONLY_DRFT_DEL%>");
				return false;
			}
			 var proceed = confirm(M_SureWantDel_SelDrfts);	
			 if (!proceed){
				return false;
			 }
			 $j("#confirmMessage").html("<%=MC.CTRP_DraftConfmDraftDelete%>");
			 document.getElementById('draftDelete').value='1';
		}
		document.getElementById('studyIds').value=checkedStudy;
		openMsgDialog();
	}
} 
var isIE6 = isIE6Browser();
/*This function is used to open dialog box for Notification*/
$j(function(){

	/*This function is used to open dialog box for Notification*/
	$j(".setNotification").click(function () {
		hideSplashMessage();
		$j('input:checkbox').removeAttr('checked');
		$j("#eSigns").val('');	
		ajaxvalidate('misc:eSigns',4,'eSignMessages',L_Valid_Esign,L_Invalid_Esign,'sessUserId');
		$j("#ctrpDraftJB\\.toUser").val('');	
		$j("#ctrpDraftJB\\.dummy").val('');	
		$j("#ctrpDraftJB\\.Email").val('');	
		$j("#ctrpDraftJB\\.userPK").val('');	
				if($j.browser.msie){
					$j( "#dialogNotify" ).dialog({
						modal:true,
						height: '100%',
						width: '35%' ,
						close: function(ev, ui) { $j(this).dialog('destroy'); }
					});
				}
				else{
					$j( "#dialogNotify" ).dialog({
						modal:true,
						width: '35%' ,
						close: function(ev, ui) { $j(this).dialog('destroy'); }
					});
				}			

	});//Notificationfunction closed

	/*This function is used to save Notification*/
	$j("#submit_btn").click(function(){ 
		if(!(validate_chk_radio("at least one option",$j(".ctrpDraftJB\\.statusCountVal"))))
	    {   
	    return false;
	    }
		if (!(validate_col('e-Mail ID',document.getElementById("ctrpDraftJB.Email")))) return false;
		var statusCountVal="";
		   $j("input:checked").each(function() {
			   if(statusCountVal=='')	
				   statusCountVal=this.value;
			   else
			   statusCountVal=statusCountVal+','+this.value;		
			});
		var userPK=$j("#ctrpDraftJB\\.userPK").val();
		if($j("#eSignMessages").text()==L_Invalid_Esign){
			alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
			$j("#eSigns").focus();
			return false;
		}

		if($j("#eSigns").val()==null || $j("#eSigns").val()==''){
			alert("<%=MC.M_PlsEnterEsign%>");/*alert("Please enter e-Signature");*****/
			$j("#eSigns").focus();
			return false;
		}//if closed
		else if (isNaN($j("#eSigns").val()) == true || $j("#eSigns").val().length<=3) {
			alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
			$j("#eSigns").focus();
			return false;
		}else{			
			showProgressMessage();
			var urlParam = "userFK="+userPK+"&statusCountVal="+statusCountVal;				
					if (isIE6) { 					
						submitData1(urlParam); 
					}//if closed
					else { 					
						submitData1(urlParam); 					
					}//else closed
			
				return true;
		}//else closed		
				
	});//#submit_btn function closed

});//main function closed

/*This function is used to save data*/
function submitData1(urlParam) {
		$j.ajax({
		url:'ctrpDraftNotification',
		type: "POST",
		async:false,
		data:urlParam,		
		success: handleSuccessNotify,
		error:function(response) { hideSplashMessage(); alert(data); return false; }
	});	
}//submitData function closed


/*This is callback function to recieve response from server for saving Notification data*/
function handleSuccessNotify(data) {
	try{
			var response=$j(data);
			var errorMap = data.errorMap;
			for (var key in errorMap) {
				if(errorMap[key]==M_DataNotSvd_Succ)
				{
					$j("#dialogNotify").dialog("close");
					hideSplashMessage();
				if($j.browser.msie){
					$j( "#dialogNotSaved" ).dialog({
						modal:true,
						height: '100%',
						width: '35%' 
					});
				}
				else{
					$j( "#dialogNotSaved" ).dialog({
						modal:true,
						width: '35%' 
					});
				}		
				return false;	
				}
			}
			if (!data && !data.result) { hideSplashMessage(); return false; }
			else{
					showSplashMessage();		
					setTimeout('$j("#dialogNotify").dialog("destroy"); hideSplashMessage(); ', 2000);	
			}//else closed
			
	}catch(e1){}
	return false;		
}//handleSuccessNotify function closed


/*This function is used to open dialog box for User Delete*/
$j(function(){

	$j("#closeBttn1").click(function () {
		$j("#dialogConfiguredNotify").dialog("close");
		$j('input:checkbox').removeAttr('checked');
		$j("#eSigns").val('');	
		ajaxvalidate('misc:eSigns',4,'eSignMessages',L_Valid_Esign,L_Invalid_Esign,'sessUserId');
		$j("#ctrpDraftJB\\.toUser").val('');	
		$j("#ctrpDraftJB\\.dummy").val('');	
		$j("#ctrpDraftJB\\.Email").val('');	
		$j("#ctrpDraftJB\\.userPK").val('');	
				if($j.browser.msie){
					$j( "#dialogNotify" ).dialog({
						modal:true,
						height: '100%',
						width: '35%' ,
						close: function(ev, ui) { $j(this).dialog('destroy'); }
					});
				}
				else{
					$j( "#dialogNotify" ).dialog({
						modal:true,
						width: '35%' ,
						close: function(ev, ui) { $j(this).dialog('destroy'); }
					});
				}			
	});

	/*This function is used to open dialog box for User Deleted From Notification*/
	$j(".configuredNotification").click(function () {
		$j("#dialogNotify").dialog("close");
		hideSplashMessage1();
		$j(".ctrpDraftJB\\.userPK option:selected").removeAttr("selected");
		$j("#eSignss").val('');	
		ajaxvalidate('misc:eSignss',4,'eSignMessagess',L_Valid_Esign,L_Invalid_Esign,'sessUserId');	
				if($j.browser.msie){
					$j( "#dialogConfiguredNotify" ).dialog({
						modal:true,
						height: '100%',
						width: '35%' ,
						close: function(ev, ui) {$j(this).dialog('destroy');  }
					});
				}
				else{
					$j( "#dialogConfiguredNotify" ).dialog({
						modal:true,
						width: '35%' ,
						close: function(ev1, ui1) {$j(this).dialog('destroy');  }
					});
				}			

	});//User Deleted From Notification function closed

	/*This function is used to delete User From Notification*/
	$j("#submit_btn1").click(function(){ 
		 var count=0;
		  var userFK=0;
		var userFkId;
		var statusCountVal1="";
		   $j("input[name='ctrpDraftJB.statusCountVal1']").each(function() {
			   var userFkId='#ctrpDraftJB\\.userPK'+count;
			   if($j(userFkId).val()!='' && $j(userFkId).val()!=null)
			   {
			   if(statusCountVal1==''){
				   statusCountVal1=this.value;
				   userFK=$j(userFkId).val(); 
			   }
			   else{
			   statusCountVal1=statusCountVal1+','+this.value;
			   userFK=userFK+";"+$j(userFkId).val(); 
			   }	
			   }
			   count++;
			});
			if(userFK==0)
			{
				alert(M_SelAtLeast_OneOpt);
				return false;
			}
		if($j("#eSignMessagess").text()==L_Invalid_Esign){
			alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
			$j("#eSignss").focus();
			return false;
		}

		if($j("#eSignss").val()==null || $j("#eSignss").val()==''){
			alert("<%=MC.M_PlsEnterEsign%>");/*alert("Please enter e-Signature");*****/
			$j("#eSignss").focus();
			return false;
		}//if closed
		else if (isNaN($j("#eSignss").val()) == true || $j("#eSignss").val().length<=3) {
			alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
			$j("#eSignss").focus();
			return false;
		}else{			
			showProgressMessage1();
			var urlParam = "statusCountVal="+statusCountVal1+"&userFK="+userFK;			
					if (isIE6) { 					
						submitData2(urlParam); 
					}//if closed
					else { 					
						submitData2(urlParam); 					
					}//else closed
			
				return true;
		}//else closed		
				
	});//#submit_btn function closed

});//main function closed

/*This function is used to save data*/
function submitData2(urlParam) {
		$j.ajax({
		url:'ctrpDeleteNotificationUsers',
		type: "POST",
		async:false,
		data:urlParam,		
		success: handleSuccess1,
		error:function(response) { hideSplashMessage1(); alert(data); return false; }
	});	
}//submitData function closed


/*This is callback function to recieve response from server for deleting User From Notification*/
function handleSuccess1(data) {
	try{
			var response=$j(data);
			var errorMap = data.errorMap;
			for (var key in errorMap) {
				if(errorMap[key]==M_DataNotDel_Succ)
				{
					$j("#dialogConfiguredNotify").dialog("close");
					hideSplashMessage1();
				if($j.browser.msie){
					$j( "#dialogNotDelete" ).dialog({
						modal:true,
						height: '100%',
						width: '35%' 
					});
				}
				else{
					$j( "#dialogNotDelete" ).dialog({
						modal:true,
						width: '35%' 
					});
				}
				return false;			
				}
			}
			if (!data && !data.result) { hideSplashMessage1(); return false; }
			else{
					showSplashMessage1();		
					setTimeout('$j("#dialogConfiguredNotify").dialog("destroy"); hideSplashMessage1(); ', 2000);	
			}//else closed
			
	}catch(e1){}
	return false;		
}//handleSuccess1 function closed

function submitData()
{
	var eSign=$j("#eSign").val();
	if (eSign.length<=0)
	{
		alert(M_PlsEnterEsign);
		$j("#eSign").focus();
		return false;
	}
	var eSignMessage=$j("#eSignMessage").html();
	if(eSignMessage!=L_Valid_Esign)
	{
		alert(M_IncorrEsign_EtrAgain);
		$j("#eSign").focus();
		return false;
	}

document.getDraftBrowser.submit();
}
function openMsgDialog()
{
/*YK: Added for bug #7932*/
	var myDialog = new YAHOO.widget.Dialog('enterEsign', 
		{
			visible:false, fixedcenter:true, modal:true, resizeable:true,
			draggable:"true", autofillheight:"body", constraintoviewport:false
			
		});
	var handleCancel = function() {
	     clearValues();
	     myDialog.cancel();
	};
	var handleSubmit = function() {
		//Bug #8253- check for valid e-sign
		var eSignMessage=$j("#eSignMessage").html();
		if(eSignMessage!=L_Valid_Esign)
		{
			alert(M_IncorrEsign_EtrAgain);
			$j("#eSign").focus();
			return false;
		}
		if(document.getElementById("enterEsign")!=undefined){
			var enterEsign = document.getElementById("enterEsign");
			enterEsign.parentNode.removeChild(enterEsign);
			$j(".underlay").attr("style","display:none;");
			$j("#enterEsign_c").attr("style","display:none;");
		}
		showTransitPanel(PleaseWait_Dots);
		setTimeout('showTransitPanel(M_Data_SavedSucc);', 1000);
		document.getDraftBrowser.submit();
	};
	var myButtons =[
	    		{ text: getLocalizedMessageString('L_Save'),   handler: handleSubmit },
	    		{ text: getLocalizedMessageString('L_Cancel'), handler: handleCancel }
	    	     ];
		myDialog.cfg.queueProperty("buttons", myButtons);
		myDialog.render(document.body);
		myDialog.show();
		$j("#enterEsign").show();
		$j(".container-close").click(function() {
			clearValues();
		});
		if (document.getElementById('eSign')) { document.getElementById('eSign').focus(); }
var showTransitPanel = function(msg) {
if (!VELOS.transitPanel) {
	VELOS.transitPanel = 
		new YAHOO.widget.Panel("transitPanel",  
			{ width:"240px", 
			  fixedcenter:true, 
			  close:false, 
			  draggable:false, 
			  zindex:4,
			  modal:true,
			  visible:false
			} 
		);
}
VELOS.transitPanel.setHeader("");
VELOS.transitPanel.setBody('<table align="center"><tr><td style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt;">'+msg+'</td></tr><table>');
VELOS.transitPanel.render(document.body);
VELOS.transitPanel.show();
}
$j(function() {
	if ($j("input:submit")){
		$j( "input:submit").button();
	}
	if ($j("a:submit")){
		$j( "a:submit").button();
	}
	if ($j("button")){
		$j("button").button();
	}
});	
}
function clearValues()
{
	$j("#eSign").val("");
	ajaxvalidate('misc:eSign',4,'eSignMessage',L_Valid_Esign,L_Invalid_Esign,'sessUserId');
	document.getElementById('studyIds').value="";
	document.getElementById('ctrpUnmark').value="";
	document.getElementById('draftDelete').value="";
	checkedDraftIds="";
	finalCheckedDraftIds="";
}
var showTransitPanel = function(msg) {
	if (!VELOS.transitPanel) {
		VELOS.transitPanel =
			new YAHOO.widget.Panel("transitPanel",
				{ width:"240px",
				  fixedcenter:true,
				  close:false,
				  draggable:false,
				  zindex:4,
				  modal:true,
				  visible:false
				}
			);
	}
	VELOS.transitPanel.setHeader("");
	VELOS.transitPanel.setBody('<table align="center"><tr><td style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt;">'+msg+'</td></tr><table>');
	VELOS.transitPanel.render(document.body);
	VELOS.transitPanel.show();
}

<!-- Modified By Parminder Singh Bug#10391 -->
var fnOnceEnterKeyPress = function(e) {
	var eSign=$j("#eSign").val();
	 if (e.keyCode == 13 || e.keyCode == 10) {
	if (eSign.length<=0)
	{
		alert(M_PlsEnterEsign);
		$j("#eSign").focus();
		return false;
	}
	var eSignMessage=$j("#eSignMessage").html();
	if(eSignMessage!=L_Valid_Esign)
	{
		alert(M_IncorrEsign_EtrAgain);
		$j("#eSign").focus();
		return false;
	}

if(document.getElementById("enterEsign")!=undefined){
	var enterEsign = document.getElementById("enterEsign");
	enterEsign.parentNode.removeChild(enterEsign);
	$j(".underlay").attr("style","display:none;");
	$j("#enterEsign_c").attr("style","display:none;");
}
showTransitPanel(PleaseWait_Dots);
setTimeout('showTransitPanel(M_Data_SavedSucc);', 400);
document.getDraftBrowser.submit();

	 }
}

<!-- Modified By Parminder Singh 1 -->
var fnOnceEnterKeyPresss = function(e) {
	var eSign=$j("#eSigns").val();
	 if (e.keyCode == 13 || e.keyCode == 10) {
	if (eSign.length<=0)
	{
		alert(M_PlsEnterEsign);
		$j("#eSigns").focus();
		return false;
	}
	var eSignMessage=$j("#eSignMessages").html();
	if(eSignMessage!=L_Valid_Esign)
	{
		alert(M_IncorrEsign_EtrAgain);
		$j("#eSigns").focus();
		return false;
	}
document.getDraftBrowser.submit();
}
}

<!-- Modified By Parminder Singh 2 -->
var fnOnceEnterKeyPressss = function(e) {
	var eSign=$j("#eSignss").val();
	 if (e.keyCode == 13 || e.keyCode == 10) {
	if (eSign.length<=0)
	{
		alert(M_PlsEnterEsign);
		$j("#eSignss").focus();
		return false;
	}
	var eSignMessage=$j("#eSignMessagess").html();
	if(eSignMessage!=L_Valid_Esign)
	{
		alert(M_IncorrEsign_EtrAgain);
		$j("#eSignss").focus();
		return false;
	}
document.getDraftBrowser.submit();
}
}

/*This function is used to show loading message.*/
function showProgressMessage() {
	$j("#formdiv").hide();	
	$('progressMsg').style.display = 'block';
	$('successMsg').style.display = 'none';
}//showProgressMessage function closed

/*This function is used to show successfull message.*/
function showSplashMessage() {
	$j("#formdiv").hide();
	$('progressMsg').style.display = 'none';
	$('successMsg').style.display = 'block';
	var reloadTime = 1000; // FF
	if (navigator.userAgent.indexOf("MSIE") != -1) { 1990; } // IE
	setTimeout('window.location.reload();', reloadTime);

}//showSplashMessage function closed

/*This function is used to hide successfull and Loading message.*/
function hideSplashMessage() {
	$j('#eSignMessages').hide();
	$j("#formdiv").show();
	$('progressMsg').style.display = 'none';
	$('successMsg').style.display = 'none';
}//hideSplashMessage function closed


/*This function is used to show loading message.*/
function showProgressMessage1() {
	$j("#formdiv1").hide();	
	$('progressMsg1').style.display = 'block';
	$('successMsg1').style.display = 'none';
}//showProgressMessage function closed

/*This function is used to show successfull message.*/
function showSplashMessage1() {
	$j("#formdiv1").hide();
	$('progressMsg1').style.display = 'none';
	$('successMsg1').style.display = 'block';
	var reloadTime = 1000; // FF
	if (navigator.userAgent.indexOf("MSIE") != -1) { 1990; } // IE
	setTimeout('window.location.reload();', reloadTime);

}//showSplashMessage function closed

/*This function is used to hide successfull and Loading message.*/
function hideSplashMessage1() {
	$j('#eSignMessagess').hide();
	$j("#formdiv1").show();
	$('progressMsg1').style.display = 'none';
	$('successMsg1').style.display = 'none';
}//hideSplashMessage function closed


function fnClickOnce(e) {
	try {
        var thisTimeSubmitted = new Date();
        if (!lastTimeSubmitted) { return true; }
        if (!thisTimeSubmitted) { return true; }
        if ( thisTimeSubmitted - lastTimeSubmitted <= 1000 ) {
            return false;
        }
        lastTimeSubmitted = thisTimeSubmitted;
	} catch(e) {}
    return true;
}//fnClickOnce function closed


function openLookup() {
	var windowToOpen="multilookup.jsp?viewId=6000&form=ctrpNotification&dfilter=&seperator=,&maxselect="+
    "&keyword=ctrpDraftJB.Email|USREMAIL|[VELHIDE]~ctrpDraftJB.userPK|USRPK|[VELHIDE]~ctrpDraftJB.dummy|USRFNAME|[VELHIDE]~ctrpDraftJB.dummy|USRLNAME|[VELHIDE]~ctrpDraftJB.toUser|[VELEXPR]*[VELKEYWORD=USRFNAME]*[VELSTR=]*[VELSPACE]*[VELKEYWORD=USRLNAME]";
	formWin =open(windowToOpen,'Lookup','resizable=1,status=0, width=850,height=550 top=100,left=100,menubar=no,scrollbars=1');
	formWin.focus();
	}


//Called by paginator for creating Create View History column 
viewHistoryLink=function(elCell, oRecord, oColumn, oData){
	var data=oData;
	var record=oRecord;
	var draftPK=record.getData("PK_CTRP_DRAFT")==null?"":record.getData("PK_CTRP_DRAFT");
	var draftPIName=record.getData("STUDY_PI_NAME")==null?"":record.getData("STUDY_PI_NAME");
	var draftNum=record.getData("DRAFT_NUM")==null?"":record.getData("DRAFT_NUM");
	var studyTitle=escape(record.getData("STUDY_TITLE")==null?"":record.getData("STUDY_TITLE"));

	var htmlStr;
	if(draftPK=='' || draftPK =='0'){
		htmlStr="";
	}else{
		if(<%=isAccessibleFor(ctrpRights, 'V')%>){	
			imagStr="<img src=\"./images/History.gif\" style=\"border-style: none;\"/>&nbsp;"+status;
			htmlStr="<a href=\"javascript:void(0)\" onclick=\"viewDraftHistory("+draftPK+",'"+studyTitle+"','"+draftNum+"','"+draftPIName+"');\" style=\"text-decoration:none;\" title=\"View History\">&nbsp;"+imagStr+"</a>";
		}else{
			htmlStr="";
		}
	}
	elCell.innerHTML =htmlStr;
};
// Called by paginator for creating Create draft column 
createDraftLink=function(elCell, oRecord, oColumn, oData){
	
    var data=oData;
    var record=oRecord;
    var status=record.getData("FK_CODELST_STAT")==null?"":record.getData("FK_CODELST_STAT");
    var draftType=record.getData("CTRP_DRAFT_TYPE")==null?"":record.getData("CTRP_DRAFT_TYPE");
    var draftPK=record.getData("PK_CTRP_DRAFT")==null?"":record.getData("PK_CTRP_DRAFT");
    var studyPK=record.getData("PK_STUDY")==null?"":record.getData("PK_STUDY");
	var researchType=record.getData("CTRP_DRAFT_TYPE_STAT")==null?"":record.getData("CTRP_DRAFT_TYPE_STAT");
	
    var htmlStr="";
    var imagStr="";
  
    if(<%=isAccessibleFor(ctrpRights, 'N')%>){
		if(draftType==-1){
			if(researchType != "indus" && researchType != "other" && researchType != "insti" && researchType != "national"){
				imagStr ="&nbsp;<img src=\"./images/ctrp_images/redExlamation.png\" style=\"border-style: none;\" />&nbsp;"+status;
				htmlStr="<div align=\"center\" ><a href=\"javascript:void(0);\" style=\"text-decoration:none;\"onmouseover=\"return overlib('"+M_TrialCatNat_ExtrIndlStdPg+"',CAPTION,'"+ CTRP_DraftTrialSubmCategory +"');\" onmouseout=\"return nd();\">"+imagStr+"</a></div>";
			}else{
				imagStr="<img src=\"./images/ctrp_images/newDraft.png\" style=\"border-style: none;\" title=\"Create Draft\"/>&nbsp;";
				htmlStr="<div align=\"center\" ><a href=\"javascript:void(0)\" onclick=\"createNewDraft("+draftType+","+draftPK+",'"+researchType+"',"+studyPK+");\" style=\"text-decoration:none;\">&nbsp;"+imagStr+"</a></div>";
			}
		}else{
			imagStr="<img src=\"./images/ctrp_images/newDraft.png\" style=\"border-style: none;\" title=\"Create Draft\" />&nbsp;";
			htmlStr="<div align=\"center\" ><a href=\"javascript:void(0)\" onclick=\"createNewDraft("+draftType+","+draftPK+",'"+researchType+"',"+studyPK+");\" style=\"text-decoration:none;\">&nbsp;"+imagStr+"</a></div>";
		}
	}else{
		imagStr="<img src=\"./images/ctrp_images/newDraft.png\" title='"+L_Create+"' style=\"border-style: none;\" title=\"Create Draft\" />&nbsp;";
		htmlStr="<div align=\"center\" ><a href=\"javascript:void(0);\" style=\"text-decoration:none;\"onmouseover=\"return overlib('"+M_InsuffAccRgt_CnctSysAdmin+"',CAPTION,'"+ L_Access_Rights +"');\" onmouseout=\"return nd();\"></a></div>";
    }
    elCell.innerHTML =htmlStr;
};
// To display dialog for copy/create a draft and assign actions to buttons for copy/create 
function createNewDraft(draftType,draftPK,researchType,studyPK){

	if(draftType==1){

		$j('#createNewDraftMsg').html(M_CTRP_Cpy_Drft);
		$j("#createNwDraftBtn").click(function(){
			window.location.href = "ctrpDraftIndustrial?studyId=" + studyPK;
		});
		$j('#copyDraftBtn').show();
		$j("#copyDraftBtn").click(function(){
			window.location.href = "ctrpDraftIndustrialCopy?draftId=" + draftPK;
		});
	}else if(draftType==0){
		$j('#createNewDraftMsg').html(M_CTRP_Cpy_Drft);
		$j("#createNwDraftBtn").click(function(){
			window.location.href = "ctrpDraftNonIndustrial?studyId=" + studyPK;
		});
		$j('#copyDraftBtn').show();
		$j("#copyDraftBtn").click(function(){
			window.location.href = "ctrpDraftNonIndustrialCopy?draftId=" + draftPK;
		});
	}else{
		$j('#createNewDraftMsg').html(M_CTRP_Create_Drft);
		$j('#copyDraftBtn').hide();
		if(researchType=='indus'){
			$j("#createNwDraftBtn").click(function(){
				window.location.href = "ctrpDraftIndustrial?studyId=" + studyPK;
			});
		}else{
			$j("#createNwDraftBtn").click(function(){
				window.location.href = "ctrpDraftNonIndustrial?studyId=" + studyPK;
			});
		}
	}
	// To create dialog for copy/create a draft
	if($j.browser.msie){
		$j( "#createNewDraftDiv" ).dialog({
			modal: true,
			closeOnEscape: true,
			height: '100%',
			width: '35%' ,
			close: function(ev, ui) {$j(this).dialog('destroy');  }
		});
	}else{
		$j( "#createNewDraftDiv" ).dialog({
			modal: true,
			closeOnEscape: true,
			width: '35%' ,
			close: function(ev1, ui1) {$j(this).dialog('destroy');  }
		});
	}	

}
// To display draft's status history and assign actions relted to deletion of statues
function viewDraftHistory(draftPK,studyTitle,draftNum,draftPIName){
	studyTitle=unescape(studyTitle);
	var urlParam = "draftId="+draftPK;
	var draftStatDetails = '<table width="80%"><tr><td class="defComments" width="40%">'+Std_Study+':&nbsp;<b> '+studyTitle+'</b></td><td class="defComments" width="20%">'+L_CTRP_Drft_Num+':&nbsp;<b>'+draftNum+'</b></td><td class="defComments" width="40%">'+L_PI+':&nbsp;<b> '+draftPIName+'</b></td></tr></table>';

	if(draftPK!=null){
		openMessageDialog('progressMsgStatHis');
		$j.ajax({
			url:'ctrpDraftGetStatuses',
			type: "POST",
			async:false,
			data:urlParam,		
			success:(function (data){

				var userDataMap = data.userDataMap;
				
				var statIdsArray = userDataMap['statIds'].split(",");
				var statDateArray = userDataMap['statDate'].split(",");
				var statDescArray = userDataMap['statDesc'].split(",");
				var statEnteredByArray = userDataMap['statEnteredBy'].split(",");
				var statNotesArray = userDataMap['statNotes'].split(",");
				var isCurrArray = userDataMap['isCurr'].split(",");

				var nciStatIdsArray = userDataMap['nciStatIds'].split(",");
				var nciStatDateArray = userDataMap['nciStatDate'].split(",");
				var nciStatDescArray = userDataMap['nciStatDesc'].split(",");
				var nciStatEnteredByArray = userDataMap['nciStatEnteredBy'].split(",");
				var nciStatNotesArray = userDataMap['nciStatNotes'].split(",");
				var nciIsCurrArray = userDataMap['nciIsCurr'].split(",");

				if(typeof statIdsArray === 'string'){statIdsArray=[statIdsArray];}
				if(typeof statDateArray === 'string'){statDateArray=[statDateArray];}
				if(typeof statDescArray === 'string'){someVar=[statDescArray];}
				if(typeof statEnteredByArray === 'string'){statEnteredByArray =[statEnteredByArray];}
				if(typeof statNotesArray === 'string'){statNotesArray =[ statNotesArray ];}
				if(typeof isCurrArray === 'string'){isCurrArray =[ isCurrArray ];}
				
				if(typeof nciStatIdsArray === 'string'){nciStatIdsArray =[ nciStatIdsArray ];}
				if(typeof nciStatDateArray === 'string'){nciStatDateArray =[ nciStatDateArray ];}
				if(typeof nciStatDescArray === 'string'){nciStatDescArray =[ nciStatDescArray ];}
				if(typeof nciStatEnteredByArray === 'string'){nciStatEnteredByArray =[ nciStatEnteredByArray ];}
				if(typeof nciStatNotesArray === 'string'){nciStatNotesArray =[ nciStatNotesArray ];}
				if(typeof nciIsCurrArray === 'string'){nciIsCurrArray =[ nciIsCurrArray ];}

				var draftStatHistory = '<table width="100%" class="basetbl midalign" cellspacing="0" cellpadding="0" border="1px"><tr><th width="30%">'+L_CTRP_DraftStatus+'</th><th width="15%">'+L_Status_Date+'</th><th width="30%">';
				draftStatHistory = draftStatHistory  + L_CTRP_Stat_RecrdBy+'</th><th width="15%">'+L_Delete+'<input type="checkbox" id="allDraftStatChkBx" value="0"/></th></tr>';
			
				if(statIdsArray.length==0 || statIdsArray==''){
					draftStatHistory = draftStatHistory + '<tr><td colspan="4"><p>'+L_NoRes_Found+'<p></td></tr>';
				}else{
					for(var i=0; i<statIdsArray.length;i++ ){
						draftStatHistory = draftStatHistory+'<tr><td>'+statDescArray[i]+'</td><td>'+statDateArray[i]+'</td><td>'+statEnteredByArray[i]+'</td><td align="center">';
						if(statNotesArray[i]=='SysGenerated' || isCurrArray[i] =='1'){
							draftStatHistory =draftStatHistory+'&nbsp;</td>';
						}else{
							draftStatHistory =draftStatHistory+'<input type="checkbox" name="draftStatChkBx" value='+statIdsArray[i]+' ></td>';
						}
						draftStatHistory = draftStatHistory+'</tr>';
					}
				}
				draftStatHistory = draftStatHistory+'</table>';

				var draftNciStatHisory = '<table width="100%" class="basetbl midalign" cellspacing="0" cellpadding="0" border="1px"><tr><th width="30%">'+L_CTRP_DraftNCITrialProc_Stat+'</th><th width="15%">';
				draftNciStatHisory = draftNciStatHisory + L_Status_Date+'</th><th width="30%">'+L_CTRP_Stat_RecrdBy+'</th><th width="15%">'+L_Delete+'<input type="checkbox" id="allNCIStatChkBx" value="0"/></th></tr>';
				
				if(nciStatIdsArray.length==0 || nciStatIdsArray==''){
					draftNciStatHisory = draftNciStatHisory + '<tr><td colspan="4" ><p>'+L_NoRes_Found+'</p></td></tr>';
				}else{
					for(var i=0; i<nciStatIdsArray.length;i++ ){
						draftNciStatHisory = draftNciStatHisory+'<tr><td>'+nciStatDescArray[i]+'</td><td>'+nciStatDateArray[i]+'</td><td>'+nciStatEnteredByArray[i]+'</td><td align="center">';
						draftNciStatHisory =draftNciStatHisory+'<input type="checkbox" name="draftStatChkBx" value='+nciStatIdsArray[i]+'></td></tr>';
					}
				}
				draftNciStatHisory = draftNciStatHisory+'</table>';
				
				$j("#draftStatHistoryDiv").html(draftStatHistory);
				$j("#draftNciStatHisoryDiv").html(draftNciStatHisory);
				$j("#draftStatDetails").html(draftStatDetails);
				
				// To assign submit action to button on status history dialog
				$j("#statHisSubmit").click(function(){ 
					
					var statusCountVal="";
					$j("input[name=draftStatChkBx]").each(function() {
						if(this.checked){						
							if(statusCountVal==''){	
								statusCountVal=this.value;
							}else{
								statusCountVal=statusCountVal+','+this.value;
							}
						}				   
					});

					if(statusCountVal.length==0){
						alert(M_SelAtLeast_OneOpt);
						return false;
					}
					
					var urlParam = "statusCountVal="+statusCountVal;
					//var urlParam = urlParam+":"+draftPK;					
					submitStatDelData(urlParam,draftPK); 
				});


				// To check/un-check all draft stat check-boxes
				$j('#allDraftStatChkBx').bind('change',function(){
					if(this.checked){
						$j('#draftStatHistoryDiv :checkbox').attr('checked','checked'); 
					}else{					
						$j('#draftStatHistoryDiv :checkbox').removeAttr('checked');
					}
				});

				// To check/un-check all NCI stat check-boxes
				$j('#allNCIStatChkBx').bind('change',function(){
					if(this.checked){
						$j('#draftNciStatHisoryDiv :checkbox').attr('checked','checked'); 
					}else{					
						$j('#draftNciStatHisoryDiv :checkbox').removeAttr('checked');
					}

				});

				// Open a dialog for Status history of draft								
				if($j.browser.msie){
					$j( "#statHisDialog" ).dialog({
						modal: true,
						closeOnEscape: true,
						overflow: scroll,
						height: '80%',
						width: '55%' ,
						close: function(ev, ui) {$j(this).dialog('destroy');  }
					});
				}else{
					$j( "#statHisDialog" ).dialog({
						modal: true,
						closeOnEscape: true,
						height: 'auto',
						width: '55%' ,
						close: function(ev1, ui1) {$j(this).dialog('destroy');  }
					}).height("auto");
				}
				closeMessageDialog('progressMsgStatHis');
			}),error:function(response) {closeMessageDialog('progressMsgStatHis'); return false; }
		});	
	}else{
		// caught in error			
	}

}
// To submit draft status Ids for deletion 
function submitStatDelData(urlParam,draftPK) {

		openMessageDialog('progressMsgStatHis');
		
		$j.ajax({
		url:'ctrpDraftStatusDel?draftPK='+draftPK,
		type: "POST",
		async:false,
		data:urlParam,		
		success: handleStatDelSuccess,
		error:function(response) { closeMessageDialog('progressMsgStatHis'); alert(data); return false; }
	});	
}
// To handle sucess of draft status deletion
function handleStatDelSuccess(data) {


	openMessageDialog('successMsgStatHis');
	closeMessageDialog('progressMsgStatHis');
	closeMessageDialog('statHisDialog');
	try{
		var response=$j(data);
		var errorMap = data.errorMap;
		for (var key in errorMap) {
			if(errorMap[key]==M_DataNotSvd_Succ){
				if($j.browser.msie){
					$j( "#dialogNotSaved" ).dialog({
						modal:true,
						height: '100%',
						width: '35%' 
					});
				}else{
					$j( "#dialogNotSaved" ).dialog({
						modal:true,
						width: '35%' 
					});
				}		
				return false;	
			}
		}
		if (!data && !data.result){
			return false;
		}	
	}catch(e1){}
	setTimeout("closeMessageDialog('successMsgStatHis')",1500);
	paginate_study.runFilter();
	return false;		
}
// Genric function to open a dialog with supplied Id
function openMessageDialog(divId)
{
	var messageHgth = navigator.userAgent.indexOf("MSIE") != -1 ? 130 : 70;
	jQuery("#"+divId).dialog({
		height: messageHgth,width: 300,position: 'center',resizable: false,modal: true,autoOpen: false
    }).siblings('.ui-dialog-titlebar').remove();
	jQuery("#"+divId).dialog("open");
}
//Genric function to close a dialog with supplied Id
function closeMessageDialog(divId)
{
	jQuery("#"+divId).dialog("close");
}
// Array to hold study auto complete data
var studyAutoCompleteData = [];

//calling to featch study auto complete data
$E.addListener(window, "load", function() {

	$j.ajax({
		url:'ctrpDraftGetAutoComplete',
		type: "POST",
		async:false,
		success:(function (data){

			var fetchedData = data.userDataMap

			$j.each(fetchedData, function(key1, value1) {
				studyAutoCompleteData.push( { id: key1, value: value1 } );
			});
			$j('#selStudyIds').bind('focus',openAuto('selStudyIds','searchCriteria',studyAutoCompleteData,'Study'));

		})
		,error:function(response) { alert(data); return false; }
	});	
});

</script>
<body  class="yui-skin-sam" style="overflow: hidden;">

<div style="margin-top: 10px;">
<s:form method="post" action="ctrpDraftBrowser" name="getDraftBrowser" id="getDraftBrowser" onsubmit="return false;">

<table class="basetbl" width="99%">
<tr>
	<td align="left" colspan="6" >&nbsp;<%=LC.L_Search_By %>:</td>
</tr>
<tr>	
	<td id="lookupid" align="right"><%=LC.L_Study_Number%></td>
	<td>
		<input type="text" name="selStudyIds" id="selStudyIds" ></input>
		<input type="hidden" id="searchCriteria" name="searchCriteria" /></input>
	</td>
	<td align="right" >&nbsp;<%=LC.CTRP_DraftNCITrialId %></td>
	<td>
		<s:textfield id="nciTrialIden" name="nciTrialIden" maxlength="25"></s:textfield>
	</td>
	<td align="right" >&nbsp;<%=LC.L_Study_Status %></td>
	<td>
		<s:property escape="false" value="ctrpDraftJB.protocolStatus" />
	</td>
</tr>
<tr>
	<td align="right" ><%=LC.CTRP_DraftStatus %></td>
	<td>
		<s:property escape="false" value="ctrpDraftJB.draftStatusesMenu" />
	</td>
	<td align="right" ><%=LC.CTRP_DspStdWith %></td>
	<td>
		<s:property escape="false" value="ctrpDraftJB.studyDraftsMenu" />
	</td>
	<td align="right">
		<s:property escape="false" value="ctrpDraftJB.draftActionsMenu" />
	</td>
	<td>
		<s:textfield id="draftActFrmDt" name="draftActFrmDt" cssClass="datefield" size="10"></s:textfield>
		<%=LC.L_To %>
		<s:textfield id="draftActToDt" name="draftActToDt" cssClass="datefield" size="10"></s:textfield>
	</td>
</tr>
<tr>
	<td align="right" ><%=LC.CTRP_DraftNCITrialProc_Stat %></td>
	<td colspan="2">
		<s:property escape="false" value="ctrpDraftJB.nciProcStatMenu" />
	</td>
	<td colspan="3"></td>
</tr>
<tr>	
	<td colspan="6" align="right" style="padding-right: 10px">
<%if (isAccessibleFor(ctrpRights, 'V')){ %>
		<A class="setNotification" href="#"><%=LC.L_ConfigureCtrpStatusNotifications%></A>&nbsp;&nbsp;&nbsp;
<%} %>
	<button type="submit" onclick="checkSearch();"><%=LC.L_Search %></button>
	</td>
</tr>
</table>
 <hr></hr>
<!--  <div style="margin-top: 10px;" id="messages" >
<table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
<tr align="center"><td align="center">
<s:label cssClass="defComments"><s:property value="%{#request.message}"/></s:label>
</td></tr> </table>
</div> -->
<div id="ctrpStudiesNDraftOpts" style="display: none; margin-left: 10px;">
<%if (isAccessibleFor(ctrpRights, 'N') || isAccessibleFor(ctrpRights, 'E') ){// alert alert %>
	<%=LC.L_To_Selected %>
	<%if (isAccessibleFor(ctrpRights, 'E')){ //alert alert%> 
	<a href="javascript:void(0)" onclick="postQuery('del');"><%=LC.L_Delete_Draft %></a>&nbsp;&nbsp;&nbsp;
	<%} %>
	<%if (isAccessibleFor(ctrpRights, 'N')){ %>
	<a href="javascript:void(0)" onclick="postQuery('download');"><%=LC.L_Download%></a>&nbsp;&nbsp;&nbsp;
	<%} %>
<%} %>
<a href="javascript:void(0)" onclick="postQuery('unmark');"><%=LC.L_UnmarkCtrp_Reoprtable %></a>
</div>

<br>
<s:hidden id="accountId" name="accountId" value="%{ctrpDraftJB.accountId}"></s:hidden>
<s:hidden id="studyIds" name="studyIds" ></s:hidden>
<s:hidden id="draftIds" name="draftIds" ></s:hidden>
<s:hidden id="ctrpUnmark"  name="ctrpUnmark" ></s:hidden>
<s:hidden id="draftDelete"  name="draftDelete" ></s:hidden>
<s:hidden id="userId" name="userId" value="%{ctrpDraftJB.userIdS}"></s:hidden>
<s:hidden id="grpId" name="grpId" value="%{ctrpDraftJB.grpId}"></s:hidden>
<s:hidden id="groupName" name="groupName" value="%{ctrpDraftJB.groupName}"></s:hidden>
<s:hidden id="rightSeq" name="rightSeq" value="%{ctrpDraftJB.rightSeq}"></s:hidden>
<s:hidden id="superuserRights" name="superuserRights" value="%{ctrpDraftJB.superuserRights}"></s:hidden>
<s:hidden id="pmtclsId" name="pmtclsId" value="%{ctrpDraftJB.pmtclsId}"></s:hidden>
<s:hidden id="moduleName"  name="moduleName" value="Ctrp"></s:hidden>
<div id="serverpagination" style="margin-top: 10px; margin-left: 5px; " >
</div>
<br/>
<div id="enterEsign" style="display: none; background-color:#f2f2f2; width:400px;">
<div class="hd" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt; width:400px;">
<span id="confirmMessage"></span>
&nbsp;
</div>
<div class="bd" id="insertEsign" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 8pt; overflow: visible; width:400px;">
	<table width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td colspan="4" align="left" class="black"><%=MC.M_Etr_Esign_ToProc %></td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
	<td width="25%" id="eSignLabel" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;" ><%=LC.L_Esignature%><FONT class="Mandatory">*</FONT>&nbsp;</td>
	<td width="20%"  >
	<!-- Modified By Parminder Singh Bug#10391 -->
	<input type="password" name="eSign" id="eSign" autocomplete="off" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt; vertical-align: bottom;" maxlength="8" 
	 onkeyup="ajaxvalidate('misc:'+this.id,4,'eSignMessage','<%=LC.L_Valid_Esign%>','<%=LC.L_Invalid_Esign%>','sessUserId')"  onkeypress="return fnOnceEnterKeyPress(event);"></input></td>
	 <td width="30%" align="center" ><%--<button name="submit" onclick="return submitData();" ><%=LC.L_Submit %></button>--%></td>
	</tr>
	<tr><td colspan="4" style="margin-top: 10px; height: 40px;" align="center">&nbsp;<span id="eSignMessage" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;"></span></td></tr>
	</table>
   </div>
	<div class="ft" style="width:400px;"></div>
</div>
</s:form>
</div>

<div id="dialogNotify" title="<%=LC.L_ConfigureCtrpNotification%>" style="display:none; background-color:#f2f2f2; width:400px;">
<div id="formdiv" style="display:block; font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt;">
<s:form method="post" action="#" name="ctrpNotification" id="ctrpNotification" onsubmit="return false;">
<p><A class="configuredNotification" href="#"><%=LC.L_ConfiguredCtrpStatusNotifications%></A></p>
<div class = "successfulmsg"  style="overflow:auto;left: 0; right: 0; max-height: 240px; min-height: 0; height:auto !important; height: 210px;">
<table border="1px">
	<thead>
	<tr>
	<td colspan="2" align="center" style="font-weight: bold;" >
	<%=LC.L_Status%>
	</td>
	</tr>
	</thead>
	   		<s:property escape="false" value="ctrpDraftJB.studyStatusChckLst" />
            </table>
			</div>
			<div >
			<table>
			<tr><td colspan="10"></td></tr>
			<tr>
			<td align="left" >
						<%=LC.CTRP_EmailNotify%><FONT class="Mandatory">*</FONT>
					</td>
			<td align="left" > 
				<s:textfield name="ctrpDraftJB.toUser" id="ctrpDraftJB.toUser" size="35" readonly="true"/>
				<s:hidden name="ctrpDraftJB.Email" id="ctrpDraftJB.Email"  />
				<s:hidden name="ctrpDraftJB.dummy" id="ctrpDraftJB.dummy" />
				<s:hidden name="ctrpDraftJB.userPK" id="ctrpDraftJB.userPK" />
					</td>
					<td width="24%" ><p style="font-family: Arial,Helvetica,sans-serif;font-size: 12px;font-weight: bold;text-color: #AA202E;">
					<a href="#" onClick='openLookup()'><%=LC.L_Select_User_S%><%--Select user(s)*****--%> </a> </p></td> 
				</tr>
			</table>
<%if (isAccessibleFor(ctrpRights, 'N')){ %>			
<p style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;"><%=LC.L_Esignature/*e-Signature*****/%><FONT class="Mandatory">*</FONT>&nbsp;
	<input type="password" name="eSigns" id="eSigns" autocomplete="off" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt; vertical-align: bottom;" maxlength="8" 
	 onkeyup="$j('#eSignMessages').show(); ajaxvalidate('misc:'+this.id,4,'eSignMessages','<%=LC.L_Valid_Esign%>','<%=LC.L_Invalid_Esign%>','sessUserId')"  onkeypress="return fnOnceEnterKeyPresss(event);"></input>
	 &nbsp;</p>
<p><span id="eSignMessages" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;"></span></p>
<p align="right"><button id="submit_btn" onclick="return fnClickOnce(event);" ondblclick="return false;"><%=LC.L_Save%></button>&nbsp;&nbsp;&nbsp;<button id="closeBtn" onclick="$j('#dialogNotify').dialog('close');return false;" ><%=LC.L_Cancel%></button></p>
<%} else{%>	
<p align="right"><button id="closeBton" onclick="$j('#dialogNotify').dialog('close');return false;" ><%=LC.L_Cancel%></button></p>
<%} %>	
</div>
</s:form>
</div>

<div id='progressMsg' style="display:none;"><p class="sectionHeadings" align="center"> <%=LC.L_PleaseWait_Dots %><%-- Please wait...*****--%> </p></div>
<div id='successMsg' style="display:none;"><p class="sectionHeadings" align="center"> <%=MC.M_Data_SvdSucc %><%-- Data was saved successfully*****--%> </p></div>

</div>
<div id="dialogNotSaved" style="display:none; top: 0px;">
<div><p class="sectionHeadings" align="center"  ><%=MC.M_DataNotSvd_Succ%></p></div>
<p align="center"><button id="closeBttnAftr" onclick="$j('#dialogNotDelete').dialog('close');return false;"><%=LC.L_Close%></button></p>
</div>
<div id="dialogConfiguredNotify" title="<%=LC.L_ConfiguredCtrpStatusNotifications%>" style="display:none; background-color:#f2f2f2; width:400px;">
<div id="formdiv1" style="display:block; font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt;">
<s:form method="post" action="#" name="ctrpStudyStatus" id="ctrpStudyStatus" onsubmit="return false;">
<div><p class="sectionHeadings" align="left"><%=LC.L_EditUserStudyStatus %></p></div>
<div class = "successfulmsg1"  style="overflow:auto;left: 0; right: 0; max-height: 240px; min-height: 0; height:auto !important; height: 240px;">
<table border="1px" width="99%">
	<tr>
	<th width='60%'><%=LC.L_Study_Status%></th>
	<th width='40%'><%=LC.L_User%></th>
	</td>
	</tr>
	<s:property escape="false" value="ctrpDraftJB.studyStatusUserLst" />
            </table>
			</div>
			<div>
<%if (isAccessibleFor(ctrpRights, 'E')){ %>	
<p style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;"><%=LC.L_Esignature/*e-Signature*****/%><FONT class="Mandatory">*</FONT>&nbsp;
	<input type="password" name="eSignss" id="eSignss" autocomplete="off" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt; vertical-align: bottom;" maxlength="8" 
	 onkeyup="$j('#eSignMessagess').show(); ajaxvalidate('misc:'+this.id,4,'eSignMessagess','<%=LC.L_Valid_Esign%>','<%=LC.L_Invalid_Esign%>','sessUserId')"  onkeypress="return fnOnceEnterKeyPressss(event);"></input>
	 &nbsp;</p>
<p><span id="eSignMessagess" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;"></span></p>
<p align="right"><button id="submit_btn1" onclick="return fnClickOnce(event);" ondblclick="return false;"><%=LC.L_Save%></button>&nbsp;&nbsp;&nbsp;<button id="closeBttn1" onclick="return false;" ><%=LC.L_Cancel%></button></p>
<%}else {%>
<p align="right"><button id="closeBttn1" onclick="return false;" ><%=LC.L_Cancel%></button></p>
<%} %>
</div>

</s:form>
</div>

<div id='progressMsg1' style="display:none;"><p class="sectionHeadings" align="center"> <%=LC.L_PleaseWait_Dots %><%-- Please wait...*****--%> </p></div>
<div id='successMsg1' style="display:none;"><p class="sectionHeadings" align="center"> <%=MC.M_Data_SvdSucc %><%-- Data was saved successfully*****--%> </p></div>

</div>
<div id="dialogNotDelete" style="display:none; top: 0px;">
<div><p class="sectionHeadings" align="center"  ><%=MC.M_DataNotDel_Succ%></p></div>
<p align="center"><button id="closeBttnAftr" onclick="$j('#dialogNotDelete').dialog('close');return false;"><%=LC.L_Close%></button></p>
</div>
<!-- 22472 -->
<div id="createNewDraftDiv" title="<%=LC.CTRP_CreateNwDraft%>" style="display:none; background-color:#f2f2f2; width:400px;">
<table width="90%">
<tr><td id="createNewDraftMsg"><%=MC.M_CTRP_Cpy_Drft %></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td align="center">
	<button type="submit" id="copyDraftBtn"><%=LC.CTRP_Crt_Drft_Cpy_Frmlst%></button>
	&nbsp;&nbsp;&nbsp;
	<button type="submit" id="createNwDraftBtn"><%=LC.CTRP_Crt_Blnk_Drft%></button>
</td></tr>
</table>
</div>

<div id="statHisDialog" title="<%=LC.CTRP_DraftHistory%>" style="display:none;">

<div id="statHisdiv1">
<s:form method="post" action="#" name="ctrpStatusHis" id="ctrpStatusHis" onsubmit="return false;">
<div id="draftStatDetails"></div>
<br/>
<p class="comments"><b><%=LC.CTRP_DraftStatus %></b></p>
<div id="draftStatHistoryDiv" style="height:150px;overflow:auto;align:center;"></div>
<br/>
<p class="comments"><b><%=LC.CTRP_DraftNCITrialProc_Stat %><b></p>
<div id="draftNciStatHisoryDiv" align="center"  style="height:150px;overflow:auto;align:center;"></div>
<div>
<%if (isAccessibleFor(ctrpRights, 'E')){ %>	
<p align="center"><button id="statHisSubmit" onclick="return fnClickOnce(event);" ondblclick="return false;"><%=LC.L_Submit%></button>
<%}%>
</div>

</s:form>
</div>
</div>
<div id='progressMsgStatHis' style="display:none;"><p class="sectionHeadings" align="center"> <%=LC.L_PleaseWait_Dots %><%-- Please wait...*****--%> </p></div>
<div id='successMsgStatHis' style="display:none;"><p class="sectionHeadings" align="center"> <%=MC.M_Data_SvdSucc %><%-- Data was saved successfully*****--%> </p></div>




</body>
<%} catch(Exception e) { return; }
%> 