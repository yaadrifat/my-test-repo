<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title><%=LC.L_Form_RptPreview%><%--Form Report Preview*****--%></title>
	<%@ page language = "java" import = "java.util.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB"%>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<jsp:useBean id="dynrepB" scope="page" class="com.velos.eres.web.dynrep.DynRepJB" />
	<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
	<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<script>   
var formWin=null;
function openFormWin(formobj,fmt) {
if (fmt=="S"){
formobj.target="_new";
formobj.action="dynsasexport.jsp?calledfrom="+document.getElementById("calledfrom").value;
} else {
formobj.target="_new";
formobj.action="dynrepexport.jsp?calledfrom="+document.getElementById("calledfrom").value;
}
formobj.exp.value=fmt;
//formWin = open('','formWin','menubar=1,resizable=1,status=0, width=850,height=550 top=50,left=100,scrollbars=1');
//if (formWin && !formWin.closed) formWin.focus();
formobj.submit();
//document.dynreport.submit();
formobj.target="";
formobj.action="";
void(0);
}
</script>
</head>
<% String src,sql="",tempStr="",sqlStr="",name="",selValue="",repFldStr="";
   int strlen=0,firstpos=-1,secondpos=-1,patId=0,studyId=0;
src= request.getParameter("srcmenu");
%>
<%
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1) 
     ienet = 0; //IE
    else
	ienet = 1;
	if(ienet == 0) {	
%>
<body style="overflow:scroll;">
<%
	} else {
%>
<body>
<%
	}
%>
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
<form  name="preview" METHOD="POST">
<%
	
	int seq=0;
	String eolString="";
	String mode = request.getParameter("mode");
	String selectedTab = request.getParameter("selectedTab");
	String value="",patientID="",studyNumber="",orderStr="",order="",filter="",dataOrder="",fltrDD="",prevSecId="",mapSecId="",headAlign="",footAlign="";
	String calledfrom = null;
	int personPk=0,studyPk=0,repId=0,index=-1;
	DynRepDao dynDao=new DynRepDao();
	DynRepDao dynDaoFltr=new DynRepDao();
	ArrayList selectedfld=null;
	ArrayList dataList=new ArrayList() ;
	ArrayList tempVal=new ArrayList();
	ArrayList tempList=new ArrayList();
	ArrayList lfltrIds= new ArrayList();
	ArrayList lfltrNames= new ArrayList();
	ArrayList lfltrStrs= new ArrayList();
	ArrayList repFldNames=new ArrayList();
	ArrayList repFldNamesSeq=new ArrayList();
	ArrayList mapSecNames=new ArrayList();
	ArrayList mapSecIds=new ArrayList();
	
	HttpSession tSession = request.getSession(true);
	
	if (sessionmaint.isValidSession(tSession))
	{ 
	calledfrom = request.getParameter("calledfrom");
	String from=request.getParameter("from");
	from=(from==null)?"":from;
	String fltrId=request.getParameter("fltrId");
	if (fltrId==null) fltrId="";
	String repIdStr=request.getParameter("repId");
	if (repIdStr==null) repIdStr="";
	 fltrId=request.getParameter("selFltr");
	if (fltrId==null) fltrId="0";
	int userId=EJBUtil.stringToNum((String)tSession.getAttribute("userId"));
	 
	 HashMap FldAttr=new HashMap();
	 HashMap TypeAttr=new HashMap();
	 HashMap FltrAttr=new HashMap();
	 HashMap SortAttr= new HashMap();
	 HashMap RepAttr= new HashMap();
	 HashMap attributes=new HashMap();
	 //This is done to remove the session varibale from previous report.
	 if (from.equals("browse")){
	 }else{
	 attributes=(HashMap)tSession.getAttribute("attributes");
	 if (attributes==null) attributes=new HashMap();
	 }
	 if (attributes.isEmpty()){
	  repId=(new Integer(repIdStr)).intValue();
	  if (repId<=0)
	  {
	   %>
	   <script>window.close();</script>
	  <%}
	  
	dynDao=dynrepB.populateHash(repIdStr);
	attributes=dynDao.getAttributes();
	tSession.setAttribute("attributes",attributes);
	 }
	 if (attributes.containsKey("FldAttr")) FldAttr=(HashMap)attributes.get("FldAttr");
	 if (attributes.containsKey("TypeAttr")) TypeAttr=(HashMap)attributes.get("TypeAttr");
	 if (attributes.containsKey("FltrAttr")) FltrAttr=(HashMap)attributes.get("FltrAttr");
	 if (attributes.containsKey("SortAttr")) SortAttr=(HashMap)attributes.get("SortAttr");
	 if (attributes.containsKey("RepAttr")) RepAttr=(HashMap)attributes.get("RepAttr");
	 
	 if (repIdStr.length()>0)
	 	if (RepAttr.containsKey("sessCol")) repIdStr=(String)RepAttr.get("repId");
	/*String[] fldCol=request.getParameterValues("fldCol");
	ArrayList fldCols=EJBUtil.strArrToArrayList(fldCol);
	String[] fldName=request.getParameterValues("fldName");
	ArrayList fldNames=EJBUtil.strArrToArrayList(fldName);
	String[] fldDispName=request.getParameterValues("fldDispName");
	String[] fldOrder=request.getParameterValues("fldOrder");
	String[] fldWidth=request.getParameterValues("fldWidth");*/
	ArrayList fldCols=new ArrayList();
	ArrayList secFldCols=new ArrayList();
	ArrayList fldNames=new ArrayList();
	ArrayList fldDispName=new ArrayList();
	ArrayList tmpFldNames=new ArrayList();
	ArrayList tmpFldDispName=new ArrayList();
	ArrayList fldWidth=new ArrayList();
	if (FldAttr.containsKey("sessCol")) fldCols=(ArrayList)FldAttr.get("sessCol");
	if (FldAttr.containsKey("sessColName")) fldNames=(ArrayList)FldAttr.get("sessColName");
	if (FldAttr.containsKey("sessColDisp")) fldDispName=(ArrayList)FldAttr.get("sessColDisp");
	if (FldAttr.containsKey("sessColWidth")) fldWidth=(ArrayList)FldAttr.get("sessColWidth");
	
	
	if (filter.length()==0){
	if (FltrAttr.containsKey("filter")) filter=(String)FltrAttr.get("filter");
	if (filter==null) filter="";
	filter.trim();
	}
	 //end
	 
	 
	 
	 dynDaoFltr=dynrepB.getFilter(repIdStr);
	 if (dynDaoFltr!=null){
	 lfltrIds=dynDaoFltr.getFltrIds();
	 lfltrNames=dynDaoFltr.getFltrNames();
	 lfltrStrs=dynDaoFltr.getFltrStrs();
	 
	 }
	 
	if ((lfltrIds.size()>1) && (fltrId.equals("0")) && (filter.length()==0)){
	 
	 fltrDD=EJBUtil.createPullDown("selFltr",0,lfltrIds,lfltrNames);
	 %>
	 <br><br><br><br><br><br><br><br>
<table align="center">
<tr>
<td align=center>
<p class = "sectionHeadings">
<%=MC.M_SelFilter_ToProc%><%--Please Select a filter to proceed*****--%> <%=fltrDD%>
</p>
</td><td><button type="submit"><%=LC.L_Search%></button></td>
</tr>
<input type="hidden" name="from"  value=<%=from%>>
<input type="hidden" name="repId"  value=<%=repIdStr%>>
<tr height=20></tr>
<tr>
	
<td>
		
</td>		
</tr>		
</table>		
	 <%}
	 else{
	/* System.out.println("*****************************************************************************************");
	if (attributes.containsKey("FldAttr")) System.out.println("I am here-FldAttr"+attributes.get("FldAttr"));
	System.out.println("*****************************************************************************************");
	if (attributes.containsKey("TypeAttr")) System.out.println("I am here-TypeAttr"+attributes.get("TypeAttr"));
	System.out.println("*****************************************************************************************");
	if (attributes.containsKey("FltrAttr")) System.out.println("I am here-FltrAttr"+attributes.get("FltrAttr"));
	System.out.println("*****************************************************************************************");
	if (attributes.containsKey("SortAttr")) System.out.println("I am here-SortAttr"+attributes.get("SortAttr"));
	System.out.println("*****************************************************************************************");
	if (attributes.containsKey("RepAttr")) System.out.println("I am here-RepAttr"+attributes.get("RepAttr"));
	System.out.println("*****************************************************************************************");*/
	 //end
	 
	 if (fltrId.equals("0")) {
	  
	 if (lfltrIds.size()>0){
	  
	 if (fltrId.equals("0")) fltrId=(String)lfltrIds.get(0);
	 if ((filter.trim()).length()==0) filter=(String)lfltrStrs.get(0);
	 filter=(filter==null)?"":filter;
	 
	 }
	 
	 } else {
	 
	  if (lfltrIds.size()>0) index=lfltrIds.indexOf(fltrId);
	 
	 if ((index>=0) && (lfltrStrs.size()>0))
	 filter=(String)lfltrStrs.get(index);
	 if (filter==null) filter="";
	 }
	 
	 
	 fltrDD=EJBUtil.createPullDown("selFltr",EJBUtil.stringToNum(fltrId),lfltrIds,lfltrNames);
	 String formId=request.getParameter("formId");
	if (formId==null) formId="";
	if (formId.length()==0) {
	if (TypeAttr.containsKey("formId")) formId=(String)TypeAttr.get("formId");
	}
	if (formId==null) formId="";
	 
	//retrieve filterSTring
	 
	//dynDaoFltr=dynrepB.getFilterDetails(fltrId);
	if (dynDaoFltr!=null)
	{
	 dynDaoFltr.setFkForm(formId);
	 
	 filter=filter.trim();
	 
	 if ((filter).length()==0) filter=dynDaoFltr.getFltrStr();
	 }
	 
	String formType=(String)TypeAttr.get("dynType");
	formType=(formType==null)?"":formType;
	formType=(formType=="study")?"S":formType;
	formType=(formType=="pat")?"P":formType;
	orderStr=request.getParameter("order");
	if (orderStr==null) orderStr="";
	if (orderStr.length()==0){
	if (SortAttr.containsKey("repOrder")) dataOrder=(String)SortAttr.get("repOrder");
	if (dataOrder==null) dataOrder="";
	if (dataOrder.length()>0){
	dataOrder=StringUtil.replace(dataOrder,"[$$]",",");
	dataOrder=StringUtil.replace(dataOrder,"[$]"," ");
	orderStr=" order by "+ dataOrder;
	}
	}

	String repHeader=request.getParameter("repHeader");
	String repFooter=request.getParameter("repFooter");
	if (repHeader==null) repHeader="";
	if (repHeader.length()==0){
	if (RepAttr.containsKey("repHdr")) repHeader=(String)RepAttr.get("repHdr");
	if (repHeader==null) repHeader="";
	}
	//Extract alignment String from report Header
	if ((repHeader.indexOf("[VELCENTER]")>=0)||(repHeader.indexOf("[VELLEFT]")>=0)||(repHeader.indexOf("[VELRIGHT]")>=0))
	 {
	   headAlign=repHeader.substring(0,(repHeader.indexOf(":",0)));
	   repHeader=repHeader.substring(headAlign.length()+1);
	   }
	   else
	   { 
	   	headAlign=request.getParameter("headerAlign");
		if (headAlign==null) headAlign="center";
	   }
	   
	   if (headAlign.equals("[VELCENTER]")) headAlign="center";
	   if (headAlign.equals("[VELLEFT]")) headAlign="left";
	   if (headAlign.equals("[VELRIGHT]")) headAlign="right";
	 
	//end extracting header
	
	if (repFooter==null) repFooter="";
	if (repFooter.length()==0){
	if (RepAttr.containsKey("repFtr")) repFooter=(String)RepAttr.get("repFtr");
	if (repFooter==null) repFooter="";
	}
	
	//Extract alignment String from report Footer
	if ((repFooter.indexOf("[VELCENTER]")>=0)||(repFooter.indexOf("[VELLEFT]")>=0)||(repFooter.indexOf("[VELRIGHT]")>=0))
	 {
	   footAlign=repFooter.substring(0,(repFooter.indexOf(":",0)));
	   repFooter=repFooter.substring(footAlign.length()+1);
	   
	   }
	   else { 
	   footAlign=request.getParameter("footerAlign");
	   if (footAlign==null) footAlign="center";
	   }
	   if (footAlign.equals("[VELCENTER]")) footAlign="center";
	   if (footAlign.equals("[VELLEFT]")) footAlign="left";
	   if (footAlign.equals("[VELRIGHT]")) footAlign="right";
	 
	//end extracting footer
	
	tmpFldNames=fldNames;
	tmpFldDispName=fldDispName;
	for (int i=0;i<fldCols.size();i++){
	  tempStr=(String)fldCols.get(i) ;
	  name=(String)fldNames.get(i);
	   if (name==null) name="";
	  if (name.length()>30) name=name.substring(0,30);
	  if (name.indexOf("?")>=0) 
	   name=name.replace('?','q');
	   if (name.indexOf("\\")>=0)
	   name=name.replace('\\','S');
	  /*if (order.length()>0) 
	    if (orderStr.length()>0){
	    orderStr=orderStr + " , \"" + name+"\" " + order;
	    }
	    else{
	    orderStr="\""+name+ "\" " + order;
	    }*/
	  if (tempStr.indexOf("|")>=0){
	  //store the repeated fields names in an arraylist and the sequence to remove the repeated fldnames from original fldNames list
	  //populate secfldCols to retirve section for only repeated fields.
	  secFldCols.add(tempStr);
	  repFldNames.add((String)fldDispName.get(i));
	  repFldNamesSeq.add(new Integer(i));
	  tempStr=tempStr.replace('|',',');
	   tempStr=StringUtil.replaceAll(tempStr, ",", "||'[VELSEP]'||");
	     if (sqlStr.length()==0)
	  	sqlStr=" ("+tempStr+") as \"" + name+ "\"" ;
		 else
		sqlStr=sqlStr+",  ("+tempStr+") as \"" + name +"\"";
	}
	else{
		if (sqlStr.length()==0)  sqlStr=tempStr +" as \"" +name +"\"";
		else sqlStr=sqlStr + ", " + tempStr + " as \"" + name+"\"";
	}
	}
	
	 /* check if the filter is 'undefined' and set it to blank
	 */
	 filter=(filter.equals("undefined")?"":filter);
	if (formType.equals("S"))
	if ((filter.trim()).length()>0){
	filter=StringUtil.decodeString(filter);
	sql =" select ID,fk_patprot as PATPROT,(SELECT study_number FROM ER_STUDY WHERE pk_study=ID) AS study_num,  " + sqlStr + " from er_formslinear where fk_form="+formId +"  and " + filter  ;
	}else {
	sql =" select ID,fk_patprot as PATPROT, (SELECT study_number FROM ER_STUDY WHERE pk_study=ID) AS study_num," + sqlStr + " from er_formslinear where fk_form="+formId ;
	}
	
		
	if (formType.equals("P"))
	if ((filter.trim()).length()>0)
	{
	filter=StringUtil.decodeString(filter);
	sql =" select ID,(select patprot_patstdid from er_patprot where pk_patprot = a.fk_patprot) as PATPROT,(SELECT per_code FROM ER_PER WHERE pk_per=ID ) pat_code,  " + sqlStr + " from er_formslinear a where fk_form="+formId +"  and " + filter  ;
	}
	else 
	{
	sql =" select ID,(select patprot_patstdid from er_patprot where pk_patprot = a.fk_patprot) as PATPROT, (SELECT per_code FROM ER_PER WHERE pk_per=ID ) pat_code,  " + sqlStr + " from er_formslinear a where fk_form="+formId  ;
	}
	//attach patient /study id criteira
	if (formType.equals("P")) 
	{
	 /* Attach Organizartion check */
	 sql = sql + " AND ID IN (  SELECT pk_per FROM ER_PER c ,ER_USERSITE d  WHERE c.fk_site = d. fk_site AND d. fk_user=" + userId+
	 " AND d.usersite_right>0 )";
	 if (TypeAttr.containsKey("perId")) patId=EJBUtil.stringToNum((String)TypeAttr.get("perId"));
	if (TypeAttr.containsKey("studyId")) studyId=EJBUtil.stringToNum((String)TypeAttr.get("studyId"));
	if (patId>0)	sql = sql+" and Id="+patId ; 
	else if (studyId>0)	sql=sql +" and Id in (select fk_per from er_patprot where fk_study="+studyId+ " and patprot_stat=1)";
	
	/*put a check to restrcit data for those studies which user don't have access to*/
//        sql=sql + " AND " + userId +" IN ( SELECT fk_user FROM ER_STUDYTEAM WHERE fk_user="+ userId +" AND " +  
//	" fk_study=(SELECT fk_study FROM ER_PATPROT WHERE pk_patprot=a.fk_patprot) " +  
//	"  AND ((Pkg_Util.f_checkRight(study_team_rights,'STUDYFRMACC','study_rights')=1)  OR (study_team_usr_type='S')))";
	
//sql=sql + " AND " + userId +" IN ( SELECT fk_user FROM ER_STUDYTEAM WHERE fk_user="+ userId +" AND " +  
	//" ((a.fk_patprot is null) or (fk_study=(SELECT fk_study FROM ER_PATPROT WHERE pk_patprot=a.fk_patprot))) " +  
	//"  AND ((Pkg_Util.f_checkRight(study_team_rights,'STUDYFRMACC','study_rights')=1)  OR (study_team_usr_type='S')))";
        sql=sql + " AND ((a.fk_patprot is null) or (" + userId +" IN ( SELECT fk_user FROM ER_STUDYTEAM WHERE fk_user="+ userId +" AND " +  
	" ((fk_study=(SELECT fk_study FROM ER_PATPROT WHERE pk_patprot=a.fk_patprot))) " +  
	"  AND ((Pkg_Util.f_checkRight(study_team_rights,'STUDYFRMACC','study_rights')=1)  OR (study_team_usr_type='S')))))";

	}
	if (formType.equals("S")) 
	{
	
	if (TypeAttr.containsKey("studyId")) studyId=EJBUtil.stringToNum((String)TypeAttr.get("studyId"));
	if (studyId>0) {	
	sql = sql+" and Id="+studyId;
	}
	else 
	{ 
	 /* block studies to which user does not have access*/
	 sql=sql+" AND "+ userId +" IN ( SELECT fk_user FROM ER_STUDYTEAM WHERE fk_user="+ userId + " and fk_study=ID " + 
	 " AND ((Pkg_Util.f_checkRight(study_team_rights,'STUDYFRMACC','study_rights')=1)  OR (study_team_usr_type='S'))) " ;
	}
	
	}
	
	//end attach criteria
	
	if (orderStr.length()>0)	sql =sql+ "  " + orderStr ;
	else {
	if (formType.equals("P")) sql =sql+ "  " + "order by pat_code" ;
	else if (formType.equals("S")) sql =sql+ "  " + "order by study_num" ;
	 }
	//sql="select "+ sqlStr+" from er_formslinear where fk_form="+formId; 
	System.out.println(sql+formType);
	dynDao=dynrepB.getReportData(sql,(( String [] ) fldNames.toArray ( new String [ fldNames.size() ] )),formType);
	//dynDao.getReportData(sql,fldName);
	dataList=dynDao.getDataCollection();
	
	System.out.println("size of data" + dataList.size());
	//loop through fldNames to remove the repested field names,since they are stoes in repFldNames
	
	/*for(int counter=0;counter<repFldNamesSeq.size();counter++){
		//fldNames.remove((((Integer)repFldNamesSeq.get(counter)).intValue()) - counter);
		tmpFldNames.remove(((Integer)repFldNamesSeq.get(counter)).intValue() - counter);
		tmpFldDispName.remove(((Integer)repFldNamesSeq.get(counter)).intValue() - counter);
	}*/
	
	//Retrieve section information
	//DynRepDao dyndao=dynrepB.getMapData(formId);
	DynRepDao dyndao=new DynRepDao();
	if (secFldCols!=null){
	if (secFldCols.size()>0){
	dyndao=dynrepB.getSectionDetails(secFldCols,formId);
	dyndao.preProcessMap();
	mapSecNames=dyndao.getMapColSecNames();
	mapSecIds=dyndao.getMapColSecIds();
	}
	}
	//System.out.println("DataList:\n" + dataList + "\n mapSecNames\n" + mapSecNames + "\n mapSecIds\n" + mapSecIds + "\nfldDispName:\n" + fldDispName+ "\n repFldAMES:\n" + repFldNames);	
	//end retrieve section
	
	%>
 <table width="100%" bgcolor="lightgrey">
         <tr>
            <td class="reportPanel"> 
<%=MC.M_Download_ReportIn%><%--Download the report in*****--%>: 
<A href="#" onClick="openFormWin(document.preview,'W')"><%=LC.L_Word_Format%><%--Word Format*****--%></A>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<A href="#" onClick="openFormWin(document.preview,'E')"><%=LC.L_Excel_Format%><%--Excel Format*****--%></A>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<A href="#" onClick="openFormWin(document.preview,'H')"><%=LC.L_Printer_FriendlyFormat%><%--Printer Friendly Format*****--%></A>&nbsp;&nbsp;&nbsp;&nbsp;
<%repIdStr=(repIdStr==null)?"0":repIdStr;
if ((EJBUtil.stringToNum(repIdStr))>0){%>
<A href="#" onClick="openFormWin(document.preview,'S')"><%=LC.L_Sas_Format%><%--SAS Format*****--%></A>
<%}%>
            </td>
         </tr>
      </table>
<P class="sectionHeadings"> <%=LC.L_FrmRpt_Prev%><%--Form Report >> Preview*****--%> </P>
<input type="hidden" name="from"  value=<%=from%>>
<input type="hidden" name="repId"  value=<%=repIdStr%>>
<input type="hidden" name="filter"  value='<%=StringUtil.encodeString(filter)%>'>
<input type="hidden" name="exp"  value="">
<input type="hidden" name="calledfrom" id="calledfrom" value="<%=calledfrom%>"/>
<%if (lfltrIds.size()>1){%>
<table width="100%" border="0"><tr><td><p  class="sectionHeadings"><%=LC.L_Select_Filter%><%--Select Filter*****--%>&nbsp;&nbsp;&nbsp;&nbsp;<%=fltrDD%>&nbsp;&nbsp;&nbsp;&nbsp;<input type="image" src="./images/select.gif" align="absmiddle" border="0"></p></td>
</tr></table>
<%}%>
<table width="100%">
<%if (repHeader.length()>0){
%>
<tr><td class="reportName" align="<%=headAlign%>"><pre><%=repHeader%></pre></td></tr>
<tr height="20"></tr>
<%}%>
</table>
<hr class="thickLine">
<table border="1" width="100%" cellspacing="0" cellpadding="0">
<%if (formType.equals("S")){
%>
  <TH class="reportClrHead" width="5%"><%=LC.L_Study_Number%><%--Study Number*****--%></TH>
  <%} else if (formType.equals("P")){
  %>
  <TH class="reportClrHead" width="5%"><%=LC.L_Patient_Id%><%--<%=LC.Pat_Patient%> ID*****--%></TH>
  
<%}
	int secIndex=-1;
 for(int count=0;count<fldNames.size();count++){
   index= (repFldNamesSeq.indexOf(new Integer(count)));
   if (index>=0) continue;%>
   
   <TH class="reportClrHead" width="<%=(String)fldWidth.get(count)%>%"><%=(String)fldDispName.get(count)%></TH>
   
<%}
if (mapSecIds!=null){
if (mapSecIds.size()>0){
for (int count=0;count<mapSecIds.size();count++){
	mapSecId=(String)mapSecIds.get(count);
	mapSecId=(mapSecId==null)?"":mapSecId.trim();
	if (mapSecId.length()>0){
    if ((mapSecId).equals(prevSecId)){continue;}else{%>
	<TH class="reportClrHead"><%=mapSecNames.get(count)%></TH>
	<%}
	prevSecId=(String)mapSecIds.get(count);
} else {%>
	<TH class="reportClrHead"></TH>
<%}
}
%>
<%
}
}
for (int i=0;i<dataList.size();i++){
  tempList = (ArrayList) dataList.get(i);
   tempVal=new ArrayList();
   //System.out.println("TEMPLIST" + tempList);
  if ((i%2)==0){
   %>
  <tr class="browserEvenRow">
   <%}else{
   %>
   <tr class="browserOddRow">
   <%}if (formType.equals("P")){
	personPk=EJBUtil.stringToNum((String)tempList.get(0));
	if (!(personPk==0)) {	
    	person.setPersonPKId(personPk);
    	person.getPersonDetails();
    	patientID = person.getPersonPId();
    	patientID = (patientID==null)?"":patientID;
	} else {
	patientID=(String)tempList.get(1);
	if (patientID==null) patientID="-";
	}
	%>
	<td width="5%"><%=patientID%></td>
	<%
	}
   %>
   <%if (formType.equals("S")){
	studyPk=EJBUtil.stringToNum((String)tempList.get(0));
	if (!(studyPk==0)) {	
    	studyB.setId(studyPk);
	studyB.getStudyDetails();
	studyNumber = studyB.getStudyNumber();
    	studyNumber = (studyNumber==null)?"":studyNumber;
	
	}
	%>
	<td width="5%"><%=studyNumber%></td>
	<%
	}
   %>
  <%if (tempList.size()==0) continue;
   int maxTokens=0,tempCountToken=0;
   for (int j=2;j<tempList.size();j++){
  	ArrayList valList=new ArrayList();
  	seq=0;
  	value=(String)tempList.get(j);
	if (value!=null){
	if (value.indexOf("[VELSEP]")>=0){
	value=StringUtil.replaceAll(value,"[VELSEP]","~");
	StringTokenizer valSt=new StringTokenizer(value,"~");
	tempCountToken=valSt.countTokens();
	//valList.add(valSt);
	tempVal.add(valSt);
	if (tempCountToken>maxTokens)	maxTokens=tempCountToken;
	continue;
	//value=EJBUtil.replaceAll(value,"[VELSEP]",", ");
	}
	}
	//System.out.println("EOL"+System.getProperty("line.separator"));
	//if (value!=null) value=StringUtil.replace(value,"[VELSEP]","<br>");
	else value="-";
	if (value.indexOf("[VELSEP1]")>=0){
	value=value.substring(0,(value.indexOf("[VELSEP1]")));
	}	
  %>
   <td width="<%=(String)fldWidth.get(j-2)%>%"><%=value%></td>
 <%	
}%>
<% if (repFldNames.size()>0){%>
<td>
<table border="1">
<%int colCounter=0,counter=0,toCount=0,currCount=0;
  String currSec="",prevSec="";
  String dispVal="";
StringTokenizer tempSt;

while (colCounter<repFldNames.size())
{
 currSec=(String)mapSecNames.get(colCounter);
 //do the processing here to display data for the repeated fields
 toCount=(colCounter);

 
if ((colCounter>0) && !currSec.equals(prevSec))
{
if (maxTokens>0){

for (int tCount=1;tCount<=maxTokens;tCount++){
//for (int tCount=1;tCount<=toCount;tCount++){

//while (currCount<toCount) {

%>
<tr>
<%
counter=0;

//while (counter<tempVal.size()){

while ((counter + currCount )<toCount){
   dispVal="";
   tempSt=((StringTokenizer)tempVal.get(counter+currCount));
      if (tempSt.hasMoreTokens()) dispVal=(String)tempSt.nextToken();
      dispVal=(dispVal==null)?"":dispVal;
      //if (dispVal.length()==0) dispVal="-";
      %>
      <td><%=dispVal%></td>
<%counter++;
 }%>
 </tr>
 <%
 
 }
 currCount=toCount;
 //counter=toCount;
 }
//end processing here 
%>
</table></td>
<td><table border="1">
<%if (((String)repFldNames.get(colCounter)).length()>=10){%> 
<th><font size="1"><a href="javascript:void(0);" onmouseover="return overlib('<%="<tr><td><font size=1>"+LC.L_Section/*Section*****/+": "+mapSecNames.get(colCounter)+"</font></td></tr><tr><td><font size=1>"+LC.L_Fld_Name/*Field Name*****/+": "+repFldNames.get(colCounter)+"</font></td></tr>"%>');" onmouseout="return nd();"><%=((String)repFldNames.get(colCounter)).substring(0,10)%></font></th>
<%} else {%>
<th><font size="1"><a href="javascript:void(0);" onmouseover="return overlib('<%="<tr><td><font size=1>"+LC.L_Section/*Section*****/+": "+mapSecNames.get(colCounter)+"</font></td></tr><tr><td><font size=1>"+LC.L_Fld_Name/*Field Name*****/+": "+repFldNames.get(colCounter)+"</font></td></tr>"%>');" onmouseout="return nd();"><%=((String)repFldNames.get(colCounter))%></font></th>
<%}%>
<%}
  else{%>
 <%if (((String)repFldNames.get(colCounter)).length()>=10){%> 
<th><font size="1"><a href="javascript:void(0);" onmouseover="return overlib('<%="<tr><td><font size=1>"+LC.L_Section/*Section*****/+": "+mapSecNames.get(colCounter)+"</font></td></tr><tr><td><font size=1>"+LC.L_Fld_Name/*Field Name*****/+": "+repFldNames.get(colCounter)+"</font></td></tr>"%>');" onmouseout="return nd();"><%=((String)repFldNames.get(colCounter)).substring(0,10)%></font></th>
<%}else{%>
<th><font size="1"><a href="javascript:void(0);" onmouseover="return overlib('<%="<tr><td><font size=1>"+LC.L_Section/*Section*****/+": "+mapSecNames.get(colCounter)+"</font></td></tr><tr><td><font size=1>"+LC.L_Fld_Name/*Field Name*****/+": "+repFldNames.get(colCounter)+"</font></td></tr>"%>');" onmouseout="return nd();"><%=((String)repFldNames.get(colCounter))%></font></th>
<%}%>
<%}
 colCounter++;
 prevSec=currSec;
}
/*if (maxTokens>0){
for (int tCount=1;tCount<=maxTokens;tCount++){%>
<tr>
<%
while (counter<tempVal.size()){
 dispVal="";
   tempSt=((StringTokenizer)tempVal.get(counter));
      if (tempSt.hasMoreTokens()) dispVal=(String)tempSt.nextToken();
      dispVal=(dispVal==null)?"":dispVal;
      if (dispVal.length()==0) dispVal="-";
      if ((colCounter>0) && !currSec.equals(prevSec)){
       %>
  <td><%=dispVal%></td></tr></table></td><td><table border="1"><tr>
<%}else{%>
<td><%=dispVal%></td>
<%}counter++;
 }}}*/
  //do this processing to display the elements of last section to the end of list


if ((toCount+1) == repFldNames.size())
	if (maxTokens>0){
	for (int tCount=1;tCount<=maxTokens;tCount++){
	//for (int tCount=1;tCount<=toCount;tCount++){
	
	//while (currCount<toCount) {
	
	%>
	<tr>
	<%
	counter=0;
	//while (counter<tempVal.size()){
	
	while ((counter + currCount )<toCount+1){
	   dispVal="";
	   tempSt=((StringTokenizer)tempVal.get(counter+currCount));
	      if (tempSt.hasMoreTokens()) dispVal=(String)tempSt.nextToken();
	      dispVal=(dispVal==null)?"":dispVal;
	      //if (dispVal.length()==0) dispVal="-";
	      %>
	      <td><%=dispVal%></td>
	<%counter++;
	 }%>
	 </tr>
	 <%
	 
	 }
	 currCount=toCount;
	 //counter=toCount;
	 }
	
	

//End for do this processing to display the elements of last section to the end of list


 %>
</table></td>
<%}//end for repFldNames.size()>0
%>
</tr>
<%
}
%>
</table>
<table width="100%">
<hr class="thickLine">
<tr height="20"></tr>
<%
%>
<%if (repFooter.length()>0){
%>
<tr><td class="reportFooter" align="<%=footAlign%>" ><pre><%=repFooter%></pre></td></tr>
<%}
%>
</table>
<input type="hidden" name="sess" value="keep">
</form>
<%
	/*Changes for: INF-22402 Date:10/04/2012 By:Yogendra Pratap Singh*/
	if(EJBUtil.isEmpty(calledfrom)){
		calledfrom = LC.L_Rpt_Central;
	}
	int downloadFlag=0;
	String downloadFormat = null;
	String fileDnPath = null;
	String filename = null;
	%>
	<jsp:include page="userReportLogging.jsp" flush="true">
		<jsp:param 	value="<%=repId%>" 		    name="repId"/>
		<jsp:param 	value="<%=filename %>"		name="fileName"/>
		<jsp:param 	value="<%=fileDnPath %>"	name="filePath"/>
		<jsp:param 	value="<%=calledfrom%>"		name="moduleName"/>
		<jsp:param 	value="" 					name="repXml"/>
		<jsp:param 	value="<%=downloadFlag%>"	name="downloadFlag"/>
		<jsp:param  value="<%=downloadFormat%>" name="downloadFormat"/>
	</jsp:include>
<%
}
} else {  //else of if body for session
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
 
 
</body>
</html>
