<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=MC.M_EditRepeat_ItemsSubmit%><%--Edit Repeating Line Items Submit*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

</head>


<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="lineitemB" scope="session" class="com.velos.esch.web.lineitem.LineitemJB"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.service.util.EJBUtil"%>

<body>
<DIV class="popDefault" id="div1"> 

<%
	HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession))
	{
		
%>	
		<jsp:include page="sessionlogging.jsp" flush="true"/>
 <%   
		String eSign= request.getParameter("eSign");
    	String oldESign = (String) tSession.getValue("eSign");
	   if(oldESign.equals(eSign)) 
	   {
			int errorCode=0;
			String mode=request.getParameter("mode");
			if(mode == null)
				mode ="";
			
			String refresh=request.getParameter("refresh");
			
			//new responses made through refresh link
			String fromRefresh=request.getParameter("fromRefresh");
			
			int budgetId = EJBUtil.stringToNum(request.getParameter("budgetId"));
			
			String strBgtCalId = request.getParameter("bgtcalId");
			
			int bgtcalId = EJBUtil.stringToNum(strBgtCalId);
			
			int arrlen = EJBUtil.stringToNum(request.getParameter("arrlen"));
			
			String pageMode = request.getParameter("pageMode");
			
			int output= 0 ;



			if (fromRefresh==null) fromRefresh="false";
									
			String moreRows=request.getParameter("moreRows");
			
		
			int countRows = 0;
		
		
			String accountId="";
		
		
			
			String creator="";
			String ipAdd="";
		
			creator=(String) tSession.getAttribute("userId");
			
			ipAdd=(String)tSession.getAttribute("ipAdd");
			
	     	accountId=(String)tSession.getAttribute("accountId");
			
	     	String fromRt = request.getParameter("fromRt");
	     	if (fromRt==null || fromRt.equals("null")) fromRt="";
	     	
			String recordType=mode;  
	
			
			
			if (mode.equals("M"))
			{
				
			}
			
			
if((arrlen == 0 && pageMode.equals("first")) || pageMode.equals("refresh")){
//SV, REDTAG, 8/26/04			if ((mode.equals("N")) || fromRefresh.equals("true") )
/*SV, REDTAG, 8/25/04, refer to description in bug#1679. mode is not set consistently leading to issues here. 
the new request parm, newRows, communicates that new rows have been added.
*/

String newRows = request.getParameter("newRows"); 
			if ((newRows.equals("Y")) || fromRefresh.equals("true") )

			{
				String[] newEventdata = request.getParameterValues("newEventdata"); 
				
				String[] newCptCode = request.getParameterValues("newCptCode");  
				
				String[] newDesc = request.getParameterValues("newDesc");  
				
				String[] repeatOption = request.getParameterValues("newrepeatOption");  
				
				String[] newCategory= request.getParameterValues("newCmbCtgry"); 
				////Rohit CCF-FIN21
				String[] newtmid = null; 
				String[] newcdm= null; 
				

			for(int cnt=0 ;	cnt < newEventdata.length ; cnt++)
				{					
					if(!newEventdata[cnt].equals("")){
						countRows++;
					}
				}
				
				String[] strEventData = new String[countRows];
				String[] strCptCode = new String[countRows];
				String[] strDesc = new String[countRows];
				String[] strRepOpt = new String[countRows];
				String[] strCategory = new String[countRows];
				String[] strtmid = new String[countRows];
				String[] strcdm = new String[countRows];
				
				int i=0;
				for(int count=0 ;count < newEventdata.length ; count++)
				{
					if(!newEventdata[count].equals("")){
						strEventData[i] = newEventdata[count];
						strCptCode[i] = newCptCode[count];
						strDesc[i] = newDesc[count];
						strRepOpt[i] = repeatOption[count];
						strCategory[i] = newCategory[count];
						//Rohit CCF-FIN21
						strtmid[i] = null;
						strcdm[i] = null;
						i++;
					}
				}
				
			
				output = lineitemB.setRepeatLineitems(strEventData,strDesc,strCptCode,strRepOpt,strCategory,strtmid,strcdm, "0", strBgtCalId, ipAdd, creator);

				
				
				
			}
}	
			
	
//Fixed #5094 and #5065
			if (mode.equals("M") && arrlen > 0)
			{
				String[] saLineitemIds = request.getParameterValues("lineitemIds"); 
     			String[] saNames = request.getParameterValues("type");            			
             	String[] saItemCptCode=request.getParameterValues("itemCptCode");
             	String[] saItemDesc=request.getParameterValues("itemDesc");
             	String[] saRepeat=request.getParameterValues("repeatOption");
             	String[] saApplyFutureCost=request.getParameterValues("applyFutureCost");
				String[] saCategory= request.getParameterValues("cmbCtgry"); 
				//Rohit CCF-FIN21
				int arrayLength = (saCategory== null)? 0 : saCategory.length;
				String[] satmid = new String[arrayLength]; 
				String[] sacdm= new String[arrayLength]; 
				            
			
		
			lineitemB.updateRepeatLineitems(bgtcalId,saLineitemIds,saNames ,
									  saItemDesc,  saItemCptCode,
									  saRepeat,saApplyFutureCost,saCategory,satmid,sacdm,
									  EJBUtil.stringToNum(creator),ipAdd);
			
		}
		
		
		if ( errorCode == -2 || errorCode == -3 )
		{
%>

<%             if (errorCode == -3 )
				{
%>
				<br><br><br><br><br><p class = "sectionHeadings" align = center>
				 <%=MC.M_UnqFldName_EtrUnqFldId%><%--The unique field name already exists. Please enter a different unique field id*****--%></P>
				<button tabindex=2 onClick="history.go(-1);"><%=LC.L_Back%></button>		
				
<%
				} else {
%>				
				<br><br><br><br><br><p class = "sectionHeadings" align = center>		
	 			<%=MC.M_Not_Succ%><%--Not Successful*****--%></p>
<%				}
		} //end of if for Error Code

		else

		{
		//tSession.setAttribute("lineItemDeleted", "Y");  --KM
%>		<br><br><br><br><br><p class = "sectionHeadings" align = center>	
		<%=MC.M_Data_SavedSucc%><%--Data saved Successfully*****--%></p>
		<%
	if (refresh.equals("true")) 
		{ %>
 <META HTTP-EQUIV=Refresh CONTENT="1; URL=editRepeatLineItems.jsp?mode=M&moreRows=<%=moreRows%>&budgetId=<%=budgetId%>&bgtcalId=<%=bgtcalId%>&refresh=true&pageMode=refresh&fromRt=<%=fromRt%>"> 
		<%} else {%>
			      <script>
					window.opener.location.reload();
					setTimeout("self.close()",1000);
			</script>    	 

		 <%}%> 
		
	<%
		}//end of else for Error Code Msg Display
	}//end of if old esign
	else{  
%>
		<jsp:include page="incorrectesign.jsp" flush="true"/>
<%	  
			}//end of else of incorrect of esign
     }//end of if body for session 	
	
	else
	{

%>
		  <jsp:include page="timeout.html" flush="true"/>
<%
	
} //end of else of the Session 
 

%>
</DIV>
</body>

</html>
