<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.velos.eres.service.util.StringUtil" %>
<%
String src = request.getParameter("srcmenu");
String selectedTab = request.getParameter("selectedTab");
String mode = request.getParameter("mode");
String includeMode = request.getParameter("includeMode");
int requestStudyId = 0;
if (mode.equals("M")) {
	requestStudyId = StringUtil.stringToNum(request.getParameter("studyId"));
}
%>
<meta http-equiv="Refresh" content="0; URL=/velos/jsp/studyScreen.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&mode=<%=mode%>&studyId=<%=requestStudyId%>&includeMode=Y">
<%
if (requestStudyId >= 0){
	return;
}
%>