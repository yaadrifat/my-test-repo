<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>	
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=MC.M_CreateStd_Ver%><%--Create Study Version*****--%></title>

<!-- YK 05Aug11: Removed for Bug #6728 already using Whichcss_skin.js in panel.jsp -->
<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
}
</SCRIPT>

</HEAD>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id ="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.web.studyRights.StudyRightsJB" %>

<% String src;
	src= request.getParameter("srcmenu");
%>
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>  

<BODY>
<br>

<DIV class="formDefault" id="div1">

<% 
String studyId= "";
String user ="" ;
String calledfrom = "";
String searchCriteria = request.getParameter("searchCriteria");
String delMode = request.getParameter("delMode");
studyId = request.getParameter("studyId");
calledfrom = request.getParameter("calledfrom");
HttpSession tSession = request.getSession(true);
 
if (sessionmaint.isValidSession(tSession))
   {
   	user = (String) tSession.getValue("userId");
	 int ret = 0;
	int ienet = 2;
	String agent1 = request.getHeader("USER-AGENT");
   	if (agent1 != null && agent1.indexOf("MSIE") != -1) 
    	ienet = 0; //IE
    else
		ienet = 1;
		
	ArrayList tId ;
	tId = new ArrayList();		
	TeamDao teamDao = new TeamDao();
	teamDao.getTeamRights(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(user));
	tId = teamDao.getTeamIds();
	int pageRight = 0;
	if (tId.size() > 0) { 			
	   	StudyRightsJB stdRights =new StudyRightsJB();
		stdRights.setId(EJBUtil.stringToNum(tId.get(0).toString()));
		 
		ArrayList teamRights ;
				teamRights  = new ArrayList();
				teamRights = teamDao.getTeamRights();
					 
				stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
				stdRights.loadStudyRights();
				 
		 
		pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYVER"));
	}
	if(pageRight == 5) {
		if (delMode == null) {
			delMode="final";
%>
	<FORM name="versioncreate" method="post" action="createversion.jsp" onSubmit="return validate(document.versioncreate)">
	<br><br>

	<P class="defComments"><%=MC.M_EsignToProc_WithCreateVer%><%--Please enter e-Signature to proceed with Create Version.*****--%></P>
		
	<TABLE width="100%" cellspacing="0" cellpadding="0" >
	
	<tr>
	   <td width="40%">
		<%=LC.L_Esignature%><%--e-Signature*****--%> <FONT class="Mandatory">* </FONT> 
	   </td>
	   <td width="60%">
		<input type="password" name="eSign" maxlength="8" SIZE=15 autocomplete="off">
	   </td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>
		 <button type="submit"><%=LC.L_Submit%></button>
		</td>
	</tr>
	</table>

	<input type="hidden" name="delMode" value="<%=delMode%>">
	<input type="hidden" name="srcmenu" value="<%=src%>">
	<input type="hidden" name="calledfrom" value="<%=calledfrom%>">
	<input type="hidden" name="studyId" value="<%=studyId%>">  
	<input type="hidden" name="searchCriteria" value="<%=searchCriteria%>">
	

	</FORM>
<%
	} else {	
	
			String eSign = request.getParameter("eSign");	
			String oldESign = (String) tSession.getValue("eSign");
			if(!oldESign.equals(eSign)) {
%>

 		  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
			} else {

		 
	 
	 ret = studyB.createVersion(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(user));

%>
<br>
<br>
<br>
<br>
<br>

<%
	if (ret == 0)
	{
%>
<p class = "sectionHeadings" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%> </p>
	<%
		if (calledfrom.equals("H"))
		{
		%>
		
		<META HTTP-EQUIV=Refresh CONTENT="1; URL=myHome.jsp?srcmenu=<%=src%>">
     <%
		}
		else
		{
		%>
		<META HTTP-EQUIV=Refresh CONTENT="1; URL=studybrowserpg.jsp?searchCriteria=<%=searchCriteria%>">
		<%
		}
	}
else {
%>

<p class = "sectionHeadings" align = center> <%=MC.M_StdVer_CntCreated%><%--Study Version could not be created*****--%> </p>


<%
}
} //end esign
} //end of delmode
}else{ //else of if for page Right
%>
	<jsp:include page="accessdenied.jsp" flush="true"/>
<%
} // end of if for page Right
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
 <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
  
</DIV>

<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/> 
</div>

</body>
</HTML>
