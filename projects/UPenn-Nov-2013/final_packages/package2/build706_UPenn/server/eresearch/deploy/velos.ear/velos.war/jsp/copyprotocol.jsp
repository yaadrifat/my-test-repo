<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Copy_Cal%><%--Copy Calendar*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>

<SCRIPT Language="javascript">

 function  validate(){

     formobj=document.select

	if (!(validate_col('Protocol Id',formobj.protocolId))) return false
     if (!(validate_col('Protocol Name',formobj.protocolName))) return false
     if (!(validate_col('Protocol Type',formobj.calendartyp))) return false
// }
     /* Bug#9899 5-Jun-2012 -Sudhir*/
    // if (!allsplcharcheck(formobj.protocolName.value))  
 	//{
 	//	formobj.protocolName.focus();  return false;
 //	}
}
function validate1(){
	if (((document.getElementById("protocolName").value) == "") || ((document.getElementById("protocolName").value.charAt(0))==" ")){
		alert("<%=MC.M_PcolName_CantLeftBlank%>");/*alert("The calendar name cannot be left blank. Please enter a value.");*****/
		document.getElementById("protocolName").focus();
		 return false
		}
}
//	Added by Gopu on 05th May 2005 for fixing May Enhancement - Validation - Clicking on link 'Copy an Existing Calendar' with in the 		calendar then we should show that calendar already selected in the dropdown

//	  function fnNoduplicatecal(formobj){
<%--	var index;
		index=formobj.protocolId.selectedIndex;
	

			if(fnTrimSpaces(formobj.protocolId.options[index].text).toLowerCase() == fnTrimSpaces(formobj.protocolName.value).toLowerCase()){
			alert("<%=MC.M_CalNameDupli_GoBtnToProceed%>")/*alert("The calender name you have entered already exists. Please enter a new name.")*****/
			formobj.protocolName.focus();	
			return false;
		  } else 
			return true;
		}
--%>
</SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="ctrldao" scope="request" class="com.velos.esch.business.common.EventdefDao"/>
<jsp:useBean id ="userSiteB" scope="request" class="com.velos.eres.web.userSite.UserSiteJB"/>



<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id ="userB" scope="page" class="com.velos.eres.web.user.UserJB"/> 
<jsp:useBean id="catLibJB" scope="request"  class="com.velos.eres.web.catLib.CatLibJB"/>

<%@ page language = "java" import = "com.velos.eres.service.util.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB"%>


<% String src;

src= request.getParameter("srcmenu");

%>

<jsp:include page="panel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>   


<body>

<DIV class="formDefault" id="div1"> 
  <%

	String protocolId ="";
	String protocolName ="";
ArrayList eventIds = new ArrayList();
ArrayList names = new ArrayList(); 
	
int length = 0;

	String from = request.getParameter("from");
	
	String calledFrom = request.getParameter("calledFrom");

	HttpSession tSession = request.getSession(true); 
    

	if (sessionmaint.isValidSession(tSession))
	{
		String uName = (String) tSession.getValue("userName");

		String accId = (String) tSession.getValue("accountId");	
		int accountId = EJBUtil.stringToNum(accId);
		String ipAdd = (String) tSession.getValue("ipAdd");
		String userId = null;
		userId = (String) tSession.getValue("userId");
		
		ArrayList userSiteIds = new ArrayList();
		ArrayList userSiteSiteIds = new ArrayList();	
     	        ArrayList userSiteRights = new ArrayList();

		//Added by IA to handle multiple organizations
		userB.setUserId(EJBUtil.stringToNum(userId));
		userB.getUserDetails();
		int userPrimOrg = EJBUtil.stringToNum(userB.getUserSiteId());
		UserSiteDao userSiteDao = userSiteB.getUserSiteTree(EJBUtil.stringToNum(accId),EJBUtil.stringToNum(userId));
		userSiteIds = userSiteDao.getUserSiteIds();
		userSiteSiteIds = userSiteDao.getUserSiteSiteIds();
		userSiteRights = userSiteDao.getUserSiteRights();
		int len1 = userSiteIds.size();
			
			
			String siteIdAcc="";
			int accrights=0;
			int userSiteSiteId=0;		
			int userSiteRight=0;
			for (int cnt=0;cnt<len1;cnt++) {
	 		userSiteSiteId = EJBUtil.stringToNum(((userSiteSiteIds.get(cnt)) == null)?"-":(userSiteSiteIds.get(cnt)).toString());
     		userSiteRight = EJBUtil.stringToNum(((userSiteRights.get(cnt)) == null)?"-":(userSiteRights.get(cnt)).toString());
			
			if (userPrimOrg!=userSiteSiteId)
			   accrights=userSiteRight;
			if(accrights!=0)
			siteIdAcc=siteIdAcc+(userSiteSiteId+",");
					
			}
		        siteIdAcc=siteIdAcc+userPrimOrg; 
		
		        
		    
		//JM:        
		CatLibDao catLibDao= new CatLibDao();
		catLibDao = catLibJB.getAllCategories(accountId,"L");
		ArrayList ids= new ArrayList();
		ArrayList catLibNames= new ArrayList();
		
		ids = catLibDao.getCatLibIds();
		catLibNames= catLibDao.getCatLibNames();

		        
		        int calendarType=0;
		        eventdefB.setEvent_id(EJBUtil.stringToNum(protocolId));
		        calendarType=EJBUtil.stringToNum(eventdefB.getEventCalType());
		        
		        String dCalType;
		        
		        dCalType=EJBUtil.createPullDown("calendartyp", calendarType, ids, catLibNames);
		        
		        
		        
		        String calType=request.getParameter("calendartyp");
		        calType=(calType==null)?"":calType;		        
		        
		
		

%>


<%
//JM: 24Sep2007: if st/ moved above here to include to the <Form> tag, issue #2368
if(from.equals("initial")) {
%>
<Form name="select" id="selectfrmid" method="post" action="copyprotocol.jsp?from=final&srcmenu=<%=src%>&calledFrom=<%=calledFrom%>" onsubmit = "if (validate()== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

	
	<TABLE width="100%" cellspacing="2" cellpadding="2" border="0" class="basetbl midAlign outline">
	<tr height="7"></tr>
		<tr>
		<td align="right" width="35%">
		<%=MC.M_SelcCal_ToCopy%><%--Select Calendar to be copied*****--%><FONT class="Mandatory">* </FONT>&nbsp;&nbsp;  
</td>
<%
String eventType="P";
ctrldao= eventdefB.getHeaderList(accountId, EJBUtil.stringToNum(userId), eventType, siteIdAcc);
eventIds=ctrldao.getEvent_ids() ; 
names= ctrldao.getNames(); 
length = eventIds.size();

%>
<td>
<select  name="protocolId">
<OPTION value ='' SELECTED><%=LC.L_Select_AnOption%><%--Select an Option*****--%></OPTION> 
<%
for(int i = 0; i< length;i++)
{
%>
<option value=<%=eventIds.get(i)%>> <%=names.get(i)%> </option>

<%
}
%>
</select>
</td>
</tr>
	<tr height="10"></tr>
<tr> 
         
        <td align="right"><%=MC.M_SelTyp_ForNewCal%><%--Select Calendar Type for the new Calendar*****--%><FONT class="Mandatory" >* </FONT>&nbsp;&nbsp; </td>
        <td><%=dCalType%></td>
</tr>
	<tr height="10"></tr>
<tr>
<td align="right">
<%=LC.L_New_CalName%><%--New Calendar Name*****--%><FONT class="Mandatory">* </FONT>  &nbsp;&nbsp;
</td><td>
<input type="text" name="protocolName" size="50"></td>

</tr>
	<tr height="7"></tr>
 </table>
 
 <table width="100%" cellspacing="0" cellpadding="0">
  
 <jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="N"/>
		<jsp:param name="formID" value="selectfrmids"/>
		<jsp:param name="showDiscard" value="N"/>
 </jsp:include>
  
  </table>
 
</form>

<%
} //end of if for checking whether from.equals("initial")


if(from.equals("final")) {
   protocolId = request.getParameter("protocolId");
   protocolName = StringUtil.stripScript(request.getParameter("protocolName"));
   Object[] arguments = {protocolName};
   protocolName	= protocolName.trim();
   
   int newProtocolId = eventdefB.CopyProtocol(protocolId,protocolName,calType, userId,ipAdd);
   

   if(newProtocolId == -1) {
		
%>
<Form name="name" method="post" action="copyprotocol.jsp?from=final&srcmenu=<%=src%>&calledFrom=<%=calledFrom%>" onsubmit = "if (validate1()== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

<!--JM: 24Sep2007: #2368-->
<input type=hidden name="calendartyp" value="<%=calType%>">

<input type=hidden name="protocolId" value="<%=protocolId%>">
<P class = "defComments">
<%=VelosResourceBundle.getMessageString("M_CalNameDupli_GoBtnToProceed",arguments)%> <%--The Calendar Name already exists. Please enter a unique Calendar Name and click on "Go" button. --%>
<%--The protocol name you have entered already exists. Please enter a new name.*****--%> </P>
<input type="text" id = "protocolName" name="protocolName" value="<%=protocolName%>">
<input type="Submit" name="submit" value="<%=LC.L_Go%><%--Go*****--%>">
<%
   } else {
%>
<P class = "defComments"> <%=MC.M_PcolCal_CopiedSuccNew%><%--The  calendar has been copied successfully.*****--%> </P>

<META HTTP-EQUIV=Refresh CONTENT="2;
 URL=protocolcalendar.jsp?mode=M&srcmenu=<%=src%>&selectedTab=1&calledFrom=P&protocolId=<%=newProtocolId%>&srcmenu=tdmenubaritem4&pageNo=1&displayType=V&headingNo=1&displayDur=3&protocolName=<%=protocolName%>">
<%
}

}%>

</Form>

<%

} else {  //else of if body for session

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
<div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

</div>
<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/>
</div>
<script>
$j("#submit_btn").attr('style','margin-left:40%;');
</script>
</body>

</html>


