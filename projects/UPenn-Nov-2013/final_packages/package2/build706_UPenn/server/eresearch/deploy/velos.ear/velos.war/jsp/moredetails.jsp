<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.web.grpRights.GrpRightsJB" %>
<%@page import="com.velos.eres.web.study.StudyJB" %>
<%@page import="com.velos.eres.web.user.UserJB,com.velos.eres.web.studyRights.StudyRightsJB" %>
<%@page import="com.velos.esch.web.eventassoc.EventAssocJB" %>
<%@page import="com.velos.esch.web.eventdef.EventdefJB" %>
<%@page import="com.velos.esch.web.advEve.AdvEveJB" %>
<%@page import="org.json.*"%>

<%!

public static final String Str_input = "input";
public static final String Str_lookup = "lookup";
public static final String Str_checkbox = "checkbox";
public static final String Str_chkbox = "chkbox";
public static final String Str_dropdown = "dropdown";
public static final String	Str_splfld = "splfld";
public static final String Str_date = "date";
public static final String Str_RO_input = "readonly-input";
public static final String Str_Hidden_input = "hidden-input";
public static final String Str_selection_single = "single";
public static final String Str_selection_multi = "multi";
public static final String Str_tarea="textarea";
public static final String Str_tarea_RO="readonly-textarea";
public static final String Str_ModuleName_AE = "advtype";
public static final String Str_ModuleName_Event = "evtaddlcode";
public static final String Str_ModuleName_User = "user";

%>

<%
boolean mdIncludedFlag = false;
if((Str_ModuleName_AE.equals(request.getParameter("modName"))) || (Str_ModuleName_User.equals(request.getParameter("modName")))
		|| (Str_ModuleName_Event.equals(request.getParameter("modName")))){
	mdIncludedFlag = true;
}
if (!mdIncludedFlag){%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML><HEAD><TITLE><%=LC.L_More_Dets%><%--More Details*****--%></TITLE>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:include page="include.jsp" flush="true"/>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
</HEAD>
<%} %>
<jsp:include page="moreDetailsInclude.jsp">
	<jsp:param value='<%=request.getParameter("modName") %>' name="modName"/>
</jsp:include>


<%if (!mdIncludedFlag){%>
<%
int ienet = 2;

String agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1) 
     ienet = 0; //IE
    else
	ienet = 1;
	if(ienet == 0) {	
%>
<body style="overflow:scroll;">
<%
	} else {
%>
<body>
<%
	}
%>
<%}%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%><%@ page import="com.velos.eres.service.util.*"%>

<jsp:useBean id="mdJB" scope="request" class="com.velos.eres.web.moreDetails.MoreDetailsJB"/>
<%if (!mdIncludedFlag){%>
<DIV class="popDefault" id="div1">
<%} %>
<%
	HttpSession tSession = request.getSession(true); 
	 if (sessionmaint.isValidSession(tSession))
	{
	 %>
	<jsp:include page="sessionlogging.jsp" flush="true"/>

<% 	
	 String accountId = (String) tSession.getAttribute("accountId");
	 String sessUserId = (String) tSession.getAttribute("userId");
	 String modId = request.getParameter("modId");
	 String modName=request.getParameter("modName");
	 String pageRightStr=request.getParameter("pageRight");
	 pageRightStr=(pageRightStr==null)?"":pageRightStr;
	 //Modified By Yogendra: Bug# 7985
	 String accessFromPage = request.getParameter("accessFromPage");
	 accessFromPage=(accessFromPage==null)?"":accessFromPage;
	 String calledFrom = StringUtil.trueValue(request.getParameter("calledFrom"));
	  
	  String defUserGroup = (String) tSession.getAttribute("defUserGroup");
	 int isEventMD = 0;
	  	
	  	
	 int pageRight=EJBUtil.stringToNum(pageRightStr);
	 int finDetRight = 0;
	 
	 if (Str_ModuleName_Event.equals(request.getParameter("modName"))) {
		//removed rights check as it would be checked in AE page
	 }
	 else if (Str_ModuleName_AE.equals(request.getParameter("modName"))) {
		 //removed rights check as it would be checked in AE page
	 }
	 
	 //int studyRights = EJBUtil.stringToNum(request.getParameter("studyRights"));

         
	 //get study ids
	 MoreDetailsDao mdDao = new MoreDetailsDao();
	 if (isEventMD == 1) {
		mdDao = mdJB.getEventMoreDetails(EJBUtil.stringToNum(modId),modName,defUserGroup,finDetRight);
	 }else{
	 	mdDao = mdJB.getMoreDetails(EJBUtil.stringToNum(modId),modName,defUserGroup);
	 }
	 
 	 ArrayList modElementIdList  = new ArrayList();
	 ArrayList modElementDescList = new ArrayList();
	 ArrayList idList = new ArrayList();
	 ArrayList modElementDataList = new ArrayList();
 	 ArrayList recordTypeList = new ArrayList();
	 ArrayList dispTypeList=new ArrayList();
	 ArrayList dispDataList=new ArrayList();
	 ArrayList mdElementKindList=new ArrayList();
	 
	 // Retrieve CODELST_SUBTYP
	 ArrayList modElementKeysList = new ArrayList();
	 
	 idList = mdDao.getId();
	 modElementIdList =  mdDao.getMdElementIds();
	 modElementDescList = mdDao.getMdElementDescs();
	 modElementDataList = mdDao.getMdElementValues();
	 modElementKeysList = mdDao.getMdElementKeys();
	 
	 recordTypeList = mdDao.getRecordTypes ();
	 dispTypeList=mdDao.getDispTypes();
	 dispDataList=mdDao.getDispDatas();
	 if (isEventMD == 1) 
	 	mdElementKindList=mdDao.getMdElementKinds();

	 String strElementDesc ; 
 	 String strElementValue ; 
  	 String strRecordType ; 
  	 Integer iElementId; 
   	 Integer iId;
	 String disptype="";
	 String htmlAttrs="";
	 String dispdata="";
	 String ddStr="";
	 String strElementKind = "";
   	 boolean mdDisplayed = false;
   	 String subType="";
%>
<%if (!mdIncludedFlag){%>
<Form  name="moredetails" id="moredet" action="updatemoredetails.jsp" method="post" onSubmit="if (moreDetailsFunctions.validate(document.moredetails)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}" >
<P class="sectionHeadingsFrm">		
<%=LC.L_More_Dets%><%--More Details*****--%>
</P>
<%} %>
<table width="100%" cellspacing="2" cellpadding="2" border="0" class="basetbl">
<!--<tr>
   <th width="45%" >Other Study ID Type</th>
   <th width="55%" >Other Study ID</th>				
</tr> -->
<% 
	int counter = 0,cbcount=0;
	for (counter = 0; counter <= modElementIdList.size() -1 ; counter++)
		{
				strElementDesc = (String) modElementDescList.get(counter); 
 	 			strElementValue = (String) modElementDataList.get(counter); 
				iElementId = (Integer) modElementIdList.get(counter) ;
				String disTypeStr =(String) dispTypeList.get(counter); 
				subType = (String) modElementKeysList.get(counter);
				//htmlAttrs, seprated by ~ , the string part would be pasted as such, use valid html attr with spacing
				// implmented for text area only
				
				
				if(disTypeStr!=null){
					if(disTypeStr.contains("~")){
						String [] parts = disTypeStr.split("~");
						disptype = parts[0];
						htmlAttrs = parts[1];
					}else{
						disptype=disTypeStr;
						htmlAttrs="";
					}
				}else{
					disptype="";
					htmlAttrs="";
				}
				
 	 			dispdata=(String) dispDataList.get(counter);
				if (disptype==null) disptype="";
				if (dispdata==null) dispdata="";
				
				if ((disptype.toLowerCase()).equals("dropdown")) {
					if (null != dispdata){
						dispdata = StringUtil.replaceAll(dispdata, "alternateId", "alternateId"+iElementId);
					}
				}
				
 	 			if (strElementValue == null)
 	 				strElementValue = "";
 	 				
  	 			strRecordType = (String) recordTypeList.get(counter); 
   	 			iId = (Integer) idList.get(counter) ;  
   	 			if (isEventMD == 1) 
					strElementKind = (String) mdElementKindList.get(counter) ; 
	%>
	<%if (isEventMD != 1 || (isEventMD == 1 && StringUtil.isAccessibleFor(finDetRight, 'V'))
		|| (isEventMD == 1 && (null == strElementKind || !strElementKind.equals("financial")))){%>
		<%--More details except event --%>
		<%
		String finDetAccess = "";
		if (null != strElementKind && strElementKind.equals("financial")){
			if (isEventMD == 1 && !StringUtil.isAccessibleFor(finDetRight, 'E')){
				if ((disptype.toLowerCase()).equals(Str_checkbox) ||(disptype.toLowerCase()).equals(Str_chkbox)) {
					finDetAccess = " class='readonly-input' disabled='true' ";
				} else
				if ((disptype.toLowerCase()).equals("dropdown")) {
					finDetAccess = " class='readonly-input' disabled='true' ";
					if (null != dispdata){
						dispdata = StringUtil.replaceAll(dispdata, "<SELECT ", "<select ");
						dispdata = StringUtil.replaceAll(dispdata, "<select ", "<select"+ finDetAccess);
					}
				}else{
					finDetAccess = " class='readonly-input' readonly='true' ";
				}
			}
		}%>
		<% 
		
		  if(disptype.equals(Str_Hidden_input)){%>
			<input type="hidden" data-subtype="<%=subType %>" readonly <%=finDetAccess%> name = "alternateId<%=iElementId%>" value = "<%=strElementValue.trim()%>"  />
		<%}else{%>
		<tr>
			<%
			if ("".equals(finDetAccess)){
				mdDisplayed = true;
			}
			%>
		     <td width="20%" >
				<%= strElementDesc %>
			 </td>
		     <td width="80%" >
			    <% if(Str_lookup.equals(disptype)){
			    //lookup
			    String lkpKeyworkStr = "";
			    int lkpPK = 0;
			    try{
				    JSONObject lookupJSON = new JSONObject(dispdata);
				    lkpPK = StringUtil.stringToNum(""+lookupJSON.get("lookupPK"));
				    if (lkpPK <= 0){
				    	System.out.println("More Details: invalid lookup PK");
				    	continue;
				    }

				    JSONArray mappingArray = (JSONArray)lookupJSON.get("mapping");
					for (int indx=0; indx < mappingArray.length(); indx++){
						JSONObject lkpJSON = (JSONObject)mappingArray.get(indx);
				        try{
				        	String source = lkpJSON.getString("source").trim();
				            String target = lkpJSON.getString("target").trim();
				            if (StringUtil.isEmpty(source) || StringUtil.isEmpty(target)) continue;

				            target = ("alternateId".equals(target))? "alternateId"+iElementId : target;

				            if (!StringUtil.isEmpty(lkpKeyworkStr))
								lkpKeyworkStr += "~";
				            lkpKeyworkStr += target+"|"+source;
				        }catch(Exception e){
				        	System.out.println("More Details: invalid lookup mapping");
				        }
					}
					%>
					<input type="text" class='readonly-input' id="alternateId<%=iElementId%>" name="alternateId<%=iElementId%>" readonly value="<%=strElementValue.trim()%>" size="25" maxlength="100"/>
					<%if (lkpPK > 0){
						String selection = "";
						try {
				    		selection = (String)lookupJSON.get("selection");
						} catch (Exception e){}
				    	selection = (StringUtil.isEmpty(selection))? Str_selection_single : selection;
				    	%>
				    	<%if (Str_selection_single.equals(selection)){%>
						<A id="alternateId<%=iElementId%>Link" href=# onClick="moreDetailsFunctions.openLookup(<%=lkpPK %>,'<%=lkpKeyworkStr%>')"><%=LC.L_Select%><%--Select*****--%></A>
						<%} %>
						<%if (Str_selection_multi.equals(selection)){%>
						<A id="alternateId<%=iElementId%>Link" href=# onClick="moreDetailsFunctions.openMultiLookup(<%=lkpPK %>,'<%=lkpKeyworkStr%>')"><%=LC.L_Select%><%--Select*****--%></A>
						<%} %>
					<%}
			    } catch (Exception e){
			    	//e.printStackTrace();
			    	System.out.println("More Study Details: lookup configuration error!");
			    }			   
			
	}else if (Str_chkbox.equals(disptype) || Str_checkbox.equals(disptype)){
				//checkbox 
				if (!StringUtil.isEmpty(dispdata)){
					//checkbox-group
					List<String> strChkBxVals = Arrays.asList(strElementValue.split(","));
			        try{
						JSONArray checkboxArray = new JSONArray(dispdata);

						for (int indx=0; indx < checkboxArray.length(); indx++){
							JSONObject checkboxJSON = (JSONObject)checkboxArray.get(indx);
					        try{
								String data = checkboxJSON.getString("data");
								String display = checkboxJSON.getString("display");
								String checked = "";
								
								if(strChkBxVals.contains(data)){
									checked = "checked";
								}
								if (indx % 5 == 0){
								%>
									<br>
								<%
								}
				             %>
								<input type="checkbox" data-subtype="<%=subType %>" name="alternateId<%=iElementId%>Checks" value="<%=data.trim()%>" onClick="moreDetailsFunctions.setValue4ChkBoxGrp(this,<%=iElementId%>)"  <%=checked%> /> <%=display%> 
							<%
					       	}catch(Exception e){}
						}%>
						<input type="hidden" name="alternateId<%=iElementId%>" id="alternateId<%=iElementId%>" value="<%=strElementValue.trim() %>"/>
						<% 
			        }catch(Exception e){
			        	System.out.println("More Details: checkbox configuration error!");
			        }
				}else {
					cbcount=cbcount+1;
					%>
					<input type = "hidden" name = "alternateId<%=iElementId%>" value = "<%=strElementValue.trim()%>" size = "25" maxlength = "100" >
					<%if ((strElementValue.trim()).equals("Y")){%>
					 <input type="checkbox" data-subtype="<%=subType %>" name="alternate" value="<%=strElementValue.trim()%>" onClick="moreDetailsFunctions.setValue(this,<%=iElementId%>,<%=cbcount%>)" checked>
					<% }else{%>
					  <input type="checkbox"  data-subtype="<%=subType %>" name="alternate" value="<%=strElementValue.trim()%>" onClick="moreDetailsFunctions.setValue(this,<%=iElementId%>,<%=cbcount%>)">
					<%}%>
				<%}} else if ((disptype.toLowerCase()).equals("dropdown")) {
					
					if (ddStr.length()==0) ddStr=(iElementId)+":"+strElementValue;
					else ddStr=ddStr+"||"+(iElementId)+":"+strElementValue;
					%>
					
					
				  <%=dispdata%>
				  
				 <%}  else if ((disptype.toLowerCase()).equals("splfld")) {%> 
				  <%=dispdata%>
				 <%}else if ((disptype.toLowerCase()).equals("date")) {%>
				 <input name="alternateId<%=iElementId%>" type="text" class="datefield" size="10" readOnly  value="<%=strElementValue.trim()%>">
				<%}else if(disptype.equals(Str_tarea)){%>
					<textarea class="mdTextArea" <%=finDetAccess%> <%=htmlAttrs%> name = "alternateId<%=iElementId%>"  rows="4" cols="50" ><%=strElementValue.trim()%></textarea>
				<%} else if(disptype.equals(Str_tarea_RO)){%>
					<textarea class="mdTextArea readonly-input"  <%=finDetAccess%> <%=htmlAttrs%> readonly name = "alternateId<%=iElementId%>"  rows="4" cols="50" ><%=strElementValue.trim()%></textarea>
				<%} else if(disptype.equals(Str_RO_input)){%>
					<input type="text" class='readonly-input' readonly <%=finDetAccess%> name = "alternateId<%=iElementId%>" value = "<%=strElementValue.trim()%>" size = "25" />
				<%}else {%>
					<input type="text" <%=finDetAccess%> name = "alternateId<%=iElementId%>" value = "<%=strElementValue.trim()%>" size = "25" />
				<%}} %>
				
				
				<input type = "hidden" name = "recordType<%=iElementId%>" value = "<%=strRecordType %>" >
				<input type = "hidden" name = "id<%=iElementId%>" value = "<%=iId%>" >
				<input type = "hidden" name = "elementId" value = "<%=iElementId%>" >
				<input type = "hidden" name = "modId" value = "<%=modId%>" >
				<input type = "hidden" name = "modName" value = "<%=modName%>" >
				<input type = "hidden" name = "mdSubtype" value = "<%=subType%>" >
			 </td>	
		</tr>
	<%} %>
	<%
		}	
	%>
	<input type="hidden" name="cbcount" value=<%=cbcount%>>
	<input type="hidden" name="ddlist" value="<%=ddStr%>">
	
	<% 
	 String showSubmit = "";
	 if (pageRight>5) {
		showSubmit = "Y";
	} else {showSubmit ="N";}
	%>
	</table>
	<%if (!mdIncludedFlag){%>
	<%if (mdDisplayed){ %>
	<table width="98%">
	<tr><td style="width: 60%">
	<% if (modElementIdList.size() > 0) { %>
	<jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="moredet"/>
			<jsp:param name="showDiscard" value="N"/>
			<jsp:param name="showSubmit" value="<%=showSubmit%>"/>
	</jsp:include>
	<% }  %>
	</td>
	<%--Modified By Yogendra: Bug# 7985 --%>
	<%if(accessFromPage.equalsIgnoreCase("fetchProt")){ %>
		<td style="width: 30%"><button type="button" onclick="window.history.back();"><%=LC.L_Back%></button></td>
	<%}%>
	</tr>
	</table>
	<%} %>
	<%} %>
	<%if (!mdIncludedFlag){%>
	</form>
	<%} %>
	
<%}else{%>
<jsp:include page="timeout_childwindow.jsp" flush="true"/> 
<%}%>
<%if (!mdIncludedFlag){%>
<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
<%} %>
