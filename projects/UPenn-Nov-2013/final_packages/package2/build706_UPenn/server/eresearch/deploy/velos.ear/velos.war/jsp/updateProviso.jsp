<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
</HEAD>

<BODY>
  <jsp:useBean id="submissionProvisoB" scope="request" class="com.velos.eres.web.submission.SubmissionProvisoJB"/>
  <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
  <%@ page language = "java" import = "java.util.*,com.velos.eres.service.util.MC,com.velos.eres.service.util.EJBUtil, com.velos.eres.business.common.*"%>
  <%

	String src = null;
	src = request.getParameter("srcmenu");
	String eSign = request.getParameter("eSign");
	String mode = request.getParameter("mode");
	
	String provisoDate = request.getParameter("provDate");
	String proviso = request.getParameter("proviso");
	String provisoEnteredBy = request.getParameter("enteredBy");
	String submissionPK = request.getParameter("submissionPK");
	String submissionBoardPK = request.getParameter("submissionBoardPK");
	String provisoType = request.getParameter("ddProvisoType");
	
	String tabsubtype = request.getParameter("tabsubtype");

	if (EJBUtil.isEmpty(mode))
		mode = "N";

	HttpSession tSession = request.getSession(true);

	if (sessionmaint.isValidSession(tSession))

   {
     %>
     <jsp:include page="sessionlogging.jsp" flush="true"/>

     <%

   	String oldESign = (String) tSession.getValue("eSign");

	if(!oldESign.equals(eSign))
	{
%>
  	   <jsp:include page="incorrectesign.jsp" flush="true"/>
<%
	}
	else
  	{
		String ipAdd = (String) tSession.getValue("ipAdd");
		String usr = (String) tSession.getValue("userId");

%>

<%
	if(mode.equals("M"))
	{
	        String provisoId = request.getParameter("provisoId");


	 		submissionProvisoB.setId(EJBUtil.stringToNum(provisoId));
			submissionProvisoB.getSubmissionProvisoDetails();
	}
	

			submissionProvisoB.setFkSubmission(submissionPK);
            submissionProvisoB.setFkSubmissionBoard(submissionBoardPK);
            submissionProvisoB.setSubmissionProvisoDate(provisoDate);
            
            submissionProvisoB.setProvisoEnteredBy(provisoEnteredBy);
            submissionProvisoB.setSubmissionProviso(proviso);
            submissionProvisoB.setIpAdd(ipAdd);
		
			submissionProvisoB.setProvisoType(provisoType); 
			
	if(mode.equals("N"))
	{
    		submissionProvisoB.setCreator(usr);
            submissionProvisoB.createSubmissionProviso();

	}	//mode = N
	else if (mode.equals("M"))
	{
    	 submissionProvisoB.setLastModifiedBy(usr);
         submissionProvisoB.updateSubmissionProviso();

	}
	%>
	  <br>
      <br>
      <br>
      <br>
      <br>
     <p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%> </p>
	
	<META HTTP-EQUIV=Refresh CONTENT="1; URL=irbProvisos.jsp?submissionPK=<%=submissionPK%>&submissionBoardPK=<%=submissionBoardPK%>&tabsubtype=<%=tabsubtype%>">
	
	  
<%
}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>

</HTML>





