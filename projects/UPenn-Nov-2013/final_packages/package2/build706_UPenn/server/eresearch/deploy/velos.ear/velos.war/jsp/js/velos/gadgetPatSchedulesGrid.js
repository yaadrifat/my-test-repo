 function includeJS(file)
{

  var script  = document.createElement('script');
  script.src  = file;
  script.type = 'text/javascript';
  script.defer = true;

  document.getElementsByTagName('head').item(0).appendChild(script);

}

/* include any js files here */

// Introduced for localization
includeJS('js/velos/velosUtil.js');
//includeJS('js/yui/build/event-simulate/event-simulate.js');
var CommErr_CnctVelos= M_CommErr_CnctVelos; /*Storing Value from MC.jsp to local var*/
// Define gadgetPatSchedulesGrid object. "VELOS" is a namespace so that the object can be called globally
var myDataTable;
var respJ = null; // response in JSON format
var twoDecimalNumber = {
	decimalPlaces:2,
	thousandsSeparator:','//thousandsSeparator,
};

VELOS.gadgetPatSchedulesGrid = function (args) {
	incomingUrlParams = args.urlParams; 
	if (incomingUrlParams) {
		incomingUrlParams = incomingUrlParams.replace("'", "\"");
	}
	showPanel(); // Show the wait panel; this will be hidden later
	
	// Define handleSuccess() of VELOS.gadgetPatSchedulesGrid object. This will be added to the callback object to be
	// used when the call is processed successfully.
	//this.handleSuccess = function(o) {
		this.dataTable = args.dataTable ? args.dataTable : 'gadgetPatSchedulesGrid'; // name of the div to hold gadgetPatSchedulesGrid
		try {
			respJ = args.respJ;
		} catch(e) {
			alert(CommErr_CnctVelos);/*alert(svrCommErr);*****/
			return;
		}
		if (respJ.error < 0) {
			var paramArray = [respJ.errorMsg];
			alert(getLocalizedMessageString("L_Error_Msg",paramArray));/*alert('Error Msg: '+respJ.errorMsg);*****/
			return;
		}
		
		var columnArray = YAHOO.lang.JSON.parse(respJ.colArray);
		var dataArray = YAHOO.lang.JSON.parse(respJ.dataArray);
		
		//Getting all common JSOn data for all rows
		var myColumnDefs = [];
		myColumnDefs = VELOS.gadgetPatSchedulesGrid.processColumns(respJ,columnArray);
		
		var myDataSource = new YAHOO.util.DataSource(dataArray);		
		myDataSource.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
		myDataSource.responseSchema = {
			fields:respJ.fieldsArray
		};

		myDataTable = new YAHOO.widget.DataTable(
				this.dataTable,
			myColumnDefs, myDataSource,
			{
				width: (document.getElementById("list1").clientWidth-17) +"px",
				sortedBy:{key:args.sortBy, dir:args.sortDir},
				scrollable:true
			}
		);

		var fnMouseOver = function(oArgs) {
		    var target = oArgs.target;
			var column = this.getColumn(target);
			var oRecord = this.getRecord(target);
			
			if(column.key!='studyNumber'){
				return nd();
			}

			var studyId = oRecord.getData('fk_study');
			return fnGetStudyMouseOver(studyId);
		};

		myDataTable.subscribe("mousemoveEvent", fnMouseOver);
		myDataTable.subscribe("cellMouseoverEvent", fnMouseOver);
		myDataTable.subscribe("cellMouseoutEvent", function(oArgs) {
			return nd();
		});

		myDataTable.focus();
		hidePanel();
	//};
	this.handleFailure = function(o) {
		var paramArray = [o.responseText];
        alert(getLocalizedMessageString("M_CommErr_CnctVelosSupp",paramArray));/*alert(svrCommErr+":"+o.responseText);*****/
		hidePanel();
	};
	this.cArgs = [];
	this.callback = {
		success:this.handleSuccess,
		failure:this.handleFailure,
		argument:this.cArgs
	};
	//this.initConfigs(args);
}

/**
 * Added method to set the editor for all columns
 */
VELOS.gadgetPatSchedulesGrid.cellEditors = function(tempColumnsDef) {	
	if (tempColumnsDef.type){
		var type= tempColumnsDef.type;
	 	var typeInfo =VELOS.gadgetPatSchedulesGrid.types[type];
	 	if (!tempColumnsDef.formatter){
	 	  if (typeInfo.formatter)
	 		 tempColumnsDef.formatter=typeInfo.formatter;
	 	}
	}
}

VELOS.gadgetPatSchedulesGrid.processColumns = function (respJ,colArray){
	myColumnDefs = [];
	var isIe = jQuery.browser.msie;	
	if (colArray) {
		myColumnDefs = colArray;
		var colArray = [];
		for (var iX=0; iX<myColumnDefs.length; iX++) {
			if (myColumnDefs[iX].hidden && "true" == myColumnDefs[iX].hidden) {
				if (myColumnDefs[iX].key != 'patientId'){
					/*patientId was a visible column before but now hidden 
					but is still used for sorting so needs to be in colArray*/
					continue;
				}
			}
			VELOS.gadgetPatSchedulesGrid.cellEditors(myColumnDefs[iX]);
			if (myColumnDefs[iX].key == 'visitStatus')
				myColumnDefs[iX].width = (isIe==true?30:25);
			myColumnDefs[iX].resizeable = true;
			if (myColumnDefs[iX].key != 'visitStatus')
				myColumnDefs[iX].sortable = true;
			colArray.push(myColumnDefs[iX]);
		}
		myColumnDefs = colArray;
	}
	return myColumnDefs;
	
}

formatAnImage = function(el, oRecord, oColumn, oData) {
	if (oData != null){
		var key = oColumn.key;
		var fk_per = oRecord.getData('fk_per');
		
		switch (key){
		case 'patientId':
			var patientAccess = oRecord.getData('patientAccess');
			var patientId = oRecord.getData('patientId');
			
			if (f_check_perm_noAlert(patientAccess,'V')){			
				el.innerHTML = "<table style=\"border-style:none\">"
					+ "<tr style=\"border-style:none; background:none\"><td width='10%' style=\"border-style:none\">"
					+ "<A href='javascript:void(0)' onclick='VELOS.gadgetPatSchedulesGrid.getPatientClick("+fk_per+")' " 
					+ "onmouseover=\"return VELOS.gadgetPatSchedulesGrid.getPatientMouseOver("+fk_per+")\" onmouseout=\"return nd();\">"
					+ "<img src=\"./images/View.gif\" border=\"0\" align=\"left\"/>" 
					+ "</A></td>"
					+ "<td width='90%' style=\"border-style:none;\">&nbsp;"+patientId+"</td></tr></table>";
			} else {
				el.innerHTML = patientId;
			}
			break;
		case 'patientStudyId':
			var studyAccess = oRecord.getData('studyAccess');
			
			if (f_check_perm_noAlert(studyAccess,'V')){
				var patientStudyId = oRecord.getData('patientStudyId');

				el.innerHTML = "<table style=\"border-style:none\">"
					+ "<tr style=\"border-style:none; background:none\"><td width='90%' style=\"border-style:none\">"
					+ "<A href='javascript:void(0)' "
					+ "onclick='VELOS.gadgetPatSchedulesGrid.getPatStudyIdClick("+fk_per+")'>" +patientStudyId+ "</A>"
					+ "</td></tr></table>";
			}
			break;
		case 'patStudyStatus':
			var studyAccess = oRecord.getData('studyAccess');
			
			if (f_check_perm_noAlert(studyAccess,'V')){
				var patStudyStatus = oRecord.getData('patStudyStatus');
			
				el.innerHTML = "<table style=\"border-style:none\">"
					+ "<tr style=\"border-style:none; background:none\"><td width='90%' style=\"border-style:none\">"+patStudyStatus+"&nbsp;</td>"
					+ "<td width='10%' style=\"border-style:none;\"><A href='javascript:void(0)' "
					+ "onclick='VELOS.gadgetPatSchedulesGrid.getPatStudyStatClick("+fk_per+")'>"
					+ "<img src=\"./images/edit.gif\" border=\"0\" align=\"left\"" 
					+ "onmouseover=\"return VELOS.gadgetPatSchedulesGrid.getPatStudyStatMouseOver("+fk_per+")\" onmouseout=\"return nd();\"/>"
					+ "</A></td></tr></table>";
			}	
			break;
		case 'studyNumber':
			var studyAccess = oRecord.getData('studyAccess');

			if (f_check_perm_noAlert(studyAccess,'V')){
				var studyNumber = oRecord.getData('studyNumber');
				//el.innerHTML = studyNumber;
				el.innerHTML = (studyNumber.length > 15)? studyNumber.substring(0, 12) + "&nbsp;<img class='asIsImage' " 
					+ "onmouseover='return overlib(\""+studyNumber+"\",CAPTION,\""+L_Study_Number+"\");' "
					+ "onmouseout='return nd();' " 
					+ "src='./images/More.png' border='0' complete='complete'/>" : studyNumber;

			}
			break;
		case 'visitName':
			var studyAccess = oRecord.getData('studyAccess');

			if (f_check_perm_noAlert(studyAccess,'V')){
				var visitName = oRecord.getData('visitName');
				//el.innerHTML = visitName;
				el.innerHTML = (visitName.length > 15)? visitName.substring(0, 12) + "&nbsp;<img class='asIsImage' " 
					+ "onmouseover='return overlib(\""+visitName+"\",CAPTION,\""+L_Visit_Name+"\");' "
					+ "onmouseout='return nd();' " 
					+ "src='./images/More.png' border='0' complete='complete'/>" : visitName;

			}
			break;
		case 'visitStatus':
			var studyAccess = oRecord.getData('studyAccess');

			if (f_check_perm_noAlert(studyAccess,'V')){
				//var visitStatus = oRecord.getData('visitStatus');
			
				el.innerHTML = "<A title='"+L_Edit+"' href='javascript:void(0)' "
					+ "onclick='VELOS.gadgetPatSchedulesGrid.getVisitStatClick("+fk_per+")'>"
					+ "<img src=\"./images/edit.gif\" border=\"0\" align=\"left\"" 
					+ "</A>";
			}
			break;
		default:
			break;
		}
	}
};

formatTwoDecimalNumber = function(el, oRecord, oColumn, oData) {
	var formattedData ='';
	if (oData != null){
		//formattedData = YAHOO.util.Number.format(oData, twoDecimalNumber);
		formattedData = oData;
	}
	el.innerHTML = formattedData;

	$j(el).formatCurrency({ region:appNumberLocale});
	oRecord.setData(oColumn.key, oData);
	if (myDataTable && myDataTable._oCellEditor){
		myDataTable._oCellEditor.value = oData;
	}
};

/**
 * JSON to customize the fields.
 */
VELOS.gadgetPatSchedulesGrid.types = {
		number: {
			formatter:formatTwoDecimalNumber,//'number',
			parser:'number',
			// I am using my own RegularExpressionCellEditor, defined below
			editor:'regexp',
			editorOptions: {
				disableBtns:true,
				regExp:'^\\d{0,11}([.,]\\d{0,2})?$',
				validator: YAHOO.widget.DataTable.validateNumber
			}
		},
		image: {
			formatter:formatAnImage			
		}
};

VELOS.gadgetPatSchedulesGrid.prototype.initConfigs = function(args) {
	if (!args) { return false; }
	if (args.constructor != Object) { return false; }
	else { this.dataTable = 'gadgetPatSchedulesGrid'; }
	if ((args.success) && (typeof args.success == 'function')) {
		this.handleSuccess = args.success;
	}
	if ((args.failure) && (typeof args.failure == 'function')) {
		this.handleSuccess=args.success;
	}
	return true;
}

VELOS.gadgetPatSchedulesGrid.prototype.startRequest = function() {
	return true;
}

VELOS.gadgetPatSchedulesGrid.prototype.render = function() {
	this.startRequest();
}

VELOS.gadgetPatSchedulesGrid.prototype.loadingPanel = function() {
	// Initialize the temporary Panel to display while waiting for external content to load
	if (!(VELOS.wait)) {
		VELOS.wait =
			new YAHOO.widget.Panel("wait",
				{ width:"240px",
				  fixedcenter:true,
				  close:false,
				  draggable:false,
				  zindex:4,
				  modal:true,
				  visible:false
				}
			);

		VELOS.wait.setHeader(L_LoadingPlsWait);/*VELOS.wait.setHeader("Loading, please wait...");*****/
		VELOS.wait.setBody('<img class="asIsImage" src="../images/jpg/loading_pg.gif" />');
		VELOS.wait.render(document.body);
	}
}

VELOS.gadgetPatSchedulesGrid.prototype.showPanel = function () {
	if (!(VELOS.wait)) { loadingPanel(); }
	VELOS.wait.show();
}

VELOS.gadgetPatSchedulesGrid.prototype.hidePanel = function() {
	if (VELOS.wait) { VELOS.wait.hide(); }
}

VELOS.gadgetPatSchedulesGrid.getPatientMouseOver = function(inPatientPk) {
	var arrRecords = myDataTable.getRecordSet();
	for(rec=0; rec < arrRecords.getLength(); rec++){
		var tempRecord = arrRecords.getRecord(rec);
		var patientPk = tempRecord.getData("fk_per");
		
		if (patientPk == inPatientPk){
			var studyId = tempRecord.getData("fk_study");
			fnGetPatientMouseOver(patientPk, studyId);
			rec = arrRecords.getLength();
			return;
		}
	}
}

VELOS.gadgetPatSchedulesGrid.getPatientClick = function(inPatientPk) {
	var arrRecords = myDataTable.getRecordSet();
	for(rec=0; rec < arrRecords.getLength(); rec++){
		var tempRecord = arrRecords.getRecord(rec);
		var fk_per = tempRecord.getData("fk_per");
		
		if (fk_per == inPatientPk){
			var patientId = tempRecord.getData("patientId");
			var studyId = tempRecord.getData("fk_study");
			var pk_patStudyStatus = tempRecord.getData("pk_patStudyStatus");
			
			var url="patientdetails.jsp?srcmenu=tdmenubaritem5&selectedTab=1&mode=M&pkey="+fk_per+"&patientCode="+patientId+"&page=patientEnroll"
			+"&studyId="+studyId+"&generate=N&statid="+pk_patStudyStatus+"&studyVer=null";
			rec = arrRecords.getLength();

			document.forms["gadgetPatSchedules_visitFilter"].action = url;
			document.forms["gadgetPatSchedules_visitFilter"].submit(); 
		}
	}
}

VELOS.gadgetPatSchedulesGrid.getPatStudyStatMouseOver = function(inPatientPk) {
	var arrRecords = myDataTable.getRecordSet();
	for(rec=0; rec < arrRecords.getLength(); rec++){
		var tempRecord = arrRecords.getRecord(rec);
		var fk_per = tempRecord.getData("fk_per");
		
		if (fk_per == inPatientPk){
			var patientId = tempRecord.getData("patientId");
			var studyId = tempRecord.getData("studyId");
			var pk_patStudyStatus = tempRecord.getData("pk_patStudyStatus");
			
			var data = tempRecord.getData("patStudyStatusReason");
			data = (data)? data : "";
			var mouseOver ="<b>"+L_Reason+": </b>"+data+"<br>";
			
			data = tempRecord.getData("patStudyStatusDate");
			data = (data)? data : "";
			mouseOver += "<b>"+L_Status_Date+": </b>"+data+"<br>";
			
			data = tempRecord.getData("patStudyStatusNotes");
			data = (data)? data : "";
			mouseOver += "<b>"+L_Notes+": </b>"+data+"<br>";
			
			return overlib(mouseOver,CAPTION,L_Patient_Status,RIGHT,ABOVE);
		}
	}
}

VELOS.gadgetPatSchedulesGrid.getPatStudyStatClick = function(inPatientPk) {
	var arrRecords = myDataTable.getRecordSet();
	for(rec=0; rec < arrRecords.getLength(); rec++){
		var tempRecord = arrRecords.getRecord(rec);
		var fk_per = tempRecord.getData("fk_per");
		
		if (fk_per == inPatientPk){
			var patientId = escape(encodeString(tempRecord.getData("patientId")));
			var studyId = tempRecord.getData("fk_study");
			var pk_patStudyStatus = tempRecord.getData("pk_patStudyStatus");
			var patprotId = tempRecord.getData("patprotId");
			
			var url="enrollpatient.jsp?srcmenu=tdmenubaritem5&selectedTab=2&pkey="+fk_per+"&patProtId="+patprotId+"&patientCode="+patientId+"&page=patientEnroll"
			+"&studyId="+studyId;
			rec = arrRecords.getLength();

			document.forms["gadgetPatSchedules_visitFilter"].action = url;
			document.forms["gadgetPatSchedules_visitFilter"].submit();
			return;
		}
	}
}

VELOS.gadgetPatSchedulesGrid.getPatStudyIdClick = function(inPatientPk) {
	var arrRecords = myDataTable.getRecordSet();
	for(rec=0; rec < arrRecords.getLength(); rec++){
		var tempRecord = arrRecords.getRecord(rec);
		var fk_per = tempRecord.getData("fk_per");
		
		if (fk_per == inPatientPk){
			var patientId = escape(encodeString(tempRecord.getData("patientId")));
			var studyId = tempRecord.getData("fk_study");
			var pk_patStudyStatus = tempRecord.getData("pk_patStudyStatus");
			var patprotId = tempRecord.getData("patprotId");

			var url="patientschedule.jsp?srcmenu=tdmenubaritem5&selectedTab=3&mode=M&pkey="+fk_per+"&patProtId="+patprotId+"&patientCode="+patientId+"&page=patientEnroll"
			+"&studyId="+studyId +"&generate=N&statid="+pk_patStudyStatus;
			rec = arrRecords.getLength();

			document.forms["gadgetPatSchedules_visitFilter"].action = url;
			document.forms["gadgetPatSchedules_visitFilter"].submit();
			return;
		}
	}
}

VELOS.gadgetPatSchedulesGrid.getVisitStatClick = function(inPatientPk) {
	var arrRecords = myDataTable.getRecordSet();
	for(rec=0; rec < arrRecords.getLength(); rec++){
		var tempRecord = arrRecords.getRecord(rec);
		var fk_per = tempRecord.getData("fk_per");
		
		if (fk_per == inPatientPk){
			var patientId = escape(encodeString(tempRecord.getData("patientId")));
			var studyId = tempRecord.getData("fk_study");
			var pk_patStudyStatus = tempRecord.getData("pk_patStudyStatus");
			var patprotId = tempRecord.getData("patprotId");

			var url="editMulEventDetails.jsp?patProtId="+patprotId+"&pkey="+fk_per+"&studyId="+studyId +"&&calledFrom=E";
			rec = arrRecords.getLength();

			windowName=window.open(url,L_Information,"toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=900,height=500,left=150");
			windowName.focus();
			return false;
		}
	}
}
//End here
