<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<html>
<head>
<title> <%=MC.M_MngPat_AdvEvtDets%><%-- Manage Patient >> Adverse Event Details*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="userSiteB" scope="page" class="com.velos.eres.web.userSite.UserSiteJB" />
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="eventAssocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="patEnrollB" scope="request" class="com.velos.eres.web.patProt.PatProtJB"/>
<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="patB" scope="request" class="com.velos.eres.web.patStudyStat.PatStudyStatJB"/>

<%@ page language = "java" import = "com.velos.eres.business.section.*,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.esch.business.common.*"%>

<% String src= request.getParameter("srcmenu");
%>

<%--Include JSP --%>
<jsp:include page="adverseEventScreenInclude.jsp" flush="true"/>
<%----%>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

  <%
   HttpSession tSession = request.getSession(true);
   if (sessionmaint.isValidSession(tSession)){
		int pageRight = 7;
		int orgRight = 0;
		String studyId = (String) tSession.getAttribute("studyId");
		String userIdFromSession = (String) tSession.getAttribute("userId");
		//GET STUDY TEAM RIGHTS
		TeamDao teamDao = new TeamDao();
		teamDao.getTeamRights(StringUtil.stringToNum(studyId),StringUtil.stringToNum(userIdFromSession));
		ArrayList tId = teamDao.getTeamIds();
		if (tId.size() == 0) {
			pageRight=0 ;
	    }else {
			stdRights.setId(StringUtil.stringToNum(tId.get(0).toString()));

		 	ArrayList teamRights = new ArrayList();
			teamRights = teamDao.getTeamRights();

			stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
			stdRights.loadStudyRights();

    	if ((stdRights.getFtrRights().size()) == 0){
    	 	pageRight= 0;
    	}else{
    		pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYMPAT"));
    	}
    }
	
	String adveventId= request.getParameter("adveventId");
		
	String protocolId = "";
	String mode=request.getParameter("mode");
	String statid=request.getParameter("statid");
	String accId = (String) tSession.getAttribute("accountId");
	String studyVer = request.getParameter("studyVer");
	String studyNum = request.getParameter("studyNum");
	String enrollId =(String) tSession.getAttribute("enrollId");
	String patientId = "";
	String patProtId=(String) request.getParameter("patProtId");
	String eventId=(String) request.getParameter("eventId");
	String pkey = request.getParameter("pkey");
	String patStatSubType = "";
	String patStudyStat =  "";
	String patStudyId;

	int patStudyStatpk = 0;
	patEnrollB.setPatProtId(StringUtil.stringToNum(enrollId));
	patEnrollB.getPatProtDetails();
	protocolId =patEnrollB.getPatProtProtocolId();
	patStudyId = patEnrollB.getPatStudyId();
	String protName = "";
	if(protocolId != null) {
		eventAssocB.setEvent_id(StringUtil.stringToNum(protocolId));
		eventAssocB.getEventAssocDetails();
		protName = 	eventAssocB.getName();
	}

	String studyTitle = "";
	String studyNumber = "";
	studyB.setId(StringUtil.stringToNum(studyId));

	studyB.getStudyDetails();
	studyTitle = studyB.getStudyTitle();
	studyNumber = studyB.getStudyNumber();
	int fdaStudy = ("1".equals(studyB.getFdaRegulatedStudy()))? 1:0;

	int siteId = 0;
	String siteName = "";

	String advName = "";
	String grade = "";
	String gradeText = "";

	if(mode.equals("N"))
	{

	}else	{

		//get current patient status and its subtype
		patStatSubType = patB.getPatStudyCurrentStatusWithCodeSubType(studyId, pkey);
		patStudyStat = patB.getPatStudyStat();
		patStudyStatpk = patB.getId();
		statid = patStudyStat ;
		if (StringUtil.isEmpty(patStatSubType))
			patStatSubType= "";


		userB.setUserId(StringUtil.stringToNum(userIdFromSession));
        userB.getUserDetails();
		if (StringUtil.stringToNum(userB.getUserAccountId()) != StringUtil.stringToNum(accId)) {
%>	<body>
		<jsp:include page="accessdenied.jsp" flush="true"/>
	   	<div class = "myHomebottomPanel">
		<jsp:include page="bottompanel.jsp" flush="true"/>
		</div>
	</body>
	</html>
<%
			return;
		}

	}
%>
<body style="overflow:hidden;">

<%
	
	String uName = (String) tSession.getAttribute("userName");
	int personPK = 0;
	personPK = StringUtil.stringToNum(pkey);
	person.setPersonPKId(personPK);
	person.getPersonDetails();
	patientId = person.getPersonPId();
	siteId = StringUtil.stringToNum(person.getPersonLocation());
	orgRight = userSiteB.getMaxRightForStudyPatient(StringUtil.stringToNum(userIdFromSession), personPK , StringUtil.stringToNum(studyId) );
	if (orgRight > 0){
		orgRight = 7;
	}
%>
<DIV class="BrowserTopn" id="div1">
	<jsp:include page="patienttabs.jsp" flush="true">
	<jsp:param name="pkey" value="<%=pkey%>"/>
	<jsp:param name="patientCode" value="<%=patientId%>"/>
	<jsp:param name="patProtId" value="<%=patProtId%>"/>
	<jsp:param name="studyId" value="<%=studyId%>"/>
	<jsp:param name="studyVer" value="<%=studyVer%>"/>
	<jsp:param name="studyNum" value="<%=studyNum%>"/>
	</jsp:include>
</div>
<SCRIPT LANGUAGE="JavaScript">

$j(document).ready(function(){
	var screenWidth = screen.width;
	var screenHeight = screen.height;
	if(screenWidth>1280 || screenHeight>1024){
		$j('#div2').css('height','68%');
	}
});
</SCRIPT>
<DIV class="tabFormBotN tabFormBotN_adventnew_1" id="div2">
<table width="100%" >
 <tr>
	<td class=tdDefault width = 20% >
	<%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%>: <a href="study.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&studyId=<%=studyId%>"><%=studyNumber%></a> &nbsp;&nbsp;&nbsp;&nbsp;
	<a href="#" onmouseover="return overlib('<%=StringUtil.escapeSpecialCharJS(studyTitle)%>',CAPTION,'<%=LC.L_Study_Title%><%--<%=LC.Std_Study%> Title*****--%>');" onmouseout="return nd();"><%=LC.L_View_Title%><%--View Title*****--%></a>  &nbsp;&nbsp;&nbsp;&nbsp;
<%if(protocolId != null) {%>
 	<!--Protocol Calendar: <%=protName%>-->
 </tr>
 		<tr height="8"><td></td></tr>
	<%}%>
 </table>
 <%
//Modified by Manimaran for November Enhancement PS4.
if((patStatSubType.trim().equalsIgnoreCase("lockdown"))){ %>
	<P class = "defComments"><FONT class="Mandatory"><%=MC.M_StdStatLkdwn_CntEdtAdvEvt%><%--<%=LC.Pat_Patient%>'s <%=LC.Std_Study_Lower%> status is 'Lockdown'.You cannot edit the Adverse Event.*****--%> </FONT>
		<A onclick="openWinStatus('<%=pkey%>','<%=patStudyStatpk%>','<%=pageRight%>','<%=studyId%>','<%=orgRight%>')"  href="#"  ><%=LC.L_Click_Here%><%--Click Here*****--%></A>
		<FONT class="Mandatory"> <%=MC.M_ToChg_TheStat%><%--to change the status.*****--%></FONT>
	</P>
<%} %>
<form name="adverseEventScreenForm" id="adverseEventScreenForm" method="post" action="#" onSubmit="if (aeScreenFunctions.validateAEScreen()== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
<%
  	boolean hasAccess = false;
	String acc = (String) tSession.getAttribute("accountId");
	 %>
	<div style="border:1">
	<div id="aeSection1" name="aeSection" class="portlet portletstatus ui-widget ui-widget-border ui-helper-clearfix ui-corner-all">
		<div id="aeTab1content" onclick="toggleDiv('aeTab1')" class="portlet-header portletstatus ui-widget 
		ui-widget-header ui-widget-content ui-helper-clearfix ui-corner-all" style="background-color:rgb(140, 130, 130)">
			<span class="ui-icon ui-icon-triangle-1-s"></span>
			<%=LC.L_Adverse_Event%>
		</div>
		<div id="aeTab1">
			<jsp:include page="advevent_new.jsp" >
				<jsp:param name="adveventId" value="<%=adveventId%>"/>
				<jsp:param name="studyId" value="<%=studyId%>"/>
				<jsp:param name="srcmenu" value="<%=src%>"/>
				<jsp:param name="selectedTab" value="tdMenuBarItem5"/>
				<jsp:param name="page" value="patientEnroll"/>
				<jsp:param name="pkey" value="<%=pkey%>"/>
				<jsp:param name="visit" value="1"/>
				<jsp:param name="patProtId" value="<%=patProtId %>"/>
				<jsp:param name="patientCode" value="<%=patientId%>"/>
				<jsp:param name="includeMode" value="Y"/>
			</jsp:include>
		</div>
	</div>
	<div id="aeSection2" name="aeSection" class="portlet portletstatus ui-widget ui-widget-border ui-helper-clearfix ui-corner-all">
		<div id="aeTab2content" onclick="toggleDiv('aeTab2');" class="portlet-header portletstatus ui-widget 
		ui-widget-header ui-widget-content ui-helper-clearfix ui-corner-all" style="background-color:rgb(140, 130, 130)">
			<span class="ui-icon ui-icon-triangle-1-s"></span>
			<%=MC.M_MoreAdvEvt_Dets%>
		</div>
		<div id='aeTab2'>
			<jsp:include page="moredetails.jsp" >
				<jsp:param name="modId" value="<%=adveventId%>"/>
				<jsp:param name="modName" value="advtype"/>
				<jsp:param name="includeMode" value="Y"/>
			</jsp:include>
		</div>
	</div>
	</div>
	<br>
	
	<% if ( (mode.equals("N") && StringUtil.isAccessibleFor(pageRight, 'N'))
			|| (mode.equals("M") && StringUtil.isAccessibleFor(pageRight, 'E')) ){%>
	
	<jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="adverseEventScreenForm"/>
		<jsp:param name="showDiscard" value="N"/>
	</jsp:include> 
	<div class = "myHomebottomPanel">
	    <jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
	<div class ="mainMenu" id="emenu">
	  	<jsp:include page="getmenu.jsp" flush="true"/>
	</div>

	<%}else{ %>

	<div class = "myHomebottomPanel">
	    <jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
	<div class ="mainMenu" id="emenu">
	  	<jsp:include page="getmenu.jsp" flush="true"/>
	</div>


	<%} // End of access right%>
	<input type="hidden" id="studyNumber" value=<%=studyNumber%>/>
	<input type="hidden" id="patProt_patStdId" value=<%=patStudyId %> />
</form>
<%} //end of if session times out
	else
	{
	%>
		<jsp:include page="timeout.html" flush="true"/>
	<%} %>

</div>
<%--Custom JS inclusion --%>
<script type="text/javascript" src="./js/velosCustom/aeScreen.js"></script>
<%----%>
</body>
</html>
