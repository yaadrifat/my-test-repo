<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<%@page import="java.math.BigDecimal"%>
<HTML>

<HEAD>
<title><%=LC.L_Payment_Dets%><%--Payment Details*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="calendar.js"></SCRIPT>

<SCRIPT  Language="JavaScript1.2">

function validate(formobj)
{
	var ct;
	var isDec;
	var isNumber;
	var i;
	var isNeg;

	ct = formobj.applyAmount.length;

	if (ct == undefined)
	{
			isDec= isDecimalOrNumberValAndNotBlank(formobj.applyAmount.value);
			isNeg = isNegNum(formobj.applyAmount.value);

			if (!isDec && !isNeg)
			{
				 	alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid Number");*****/
					formobj.applyAmount.focus();
					return false;

			}
	}
	else
	{
		 for ( i=0; i < ct; i++)
		 {
			isDec= isDecimalOrNumberValAndNotBlank(formobj.applyAmount[i].value);
			isNeg = isNegNum(formobj.applyAmount[i].value);
			if (!isDec && !isNeg)
			{
					alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid Number");*****/
					formobj.applyAmount[i].focus();
					return false;

			 }
			 
			isDec= isDecimalOrNumberValAndNotBlank(formobj.applyHoldback[i].value);
			isNeg = isNegNum(formobj.applyHoldback[i].value);
			if (!isDec && !isNeg)
			{
					alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid Number");*****/
					formobj.applyHoldback[i].focus();
					return false;

			 }
		 }
	}
	 // Fixed bug No: 4582
	  if (!(validate_col('e-Signature',formobj.eSign))) return false
	  if(isNaN(formobj.eSign.value) == true)
	   {
			alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
			formobj.eSign.focus();
			return false;
		}

  return true;
}
</SCRIPT>

</HEAD>



  <jsp:useBean id="InvDetB" scope="request" class="com.velos.eres.web.invoice.InvoiceDetailJB"/>
  <jsp:useBean id="InvB" scope="request" class="com.velos.eres.web.invoice.InvoiceJB"/>
   <jsp:useBean id="PayB" scope="request" class="com.velos.eres.web.milepayment.PaymentDetailJB"/>

  <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
  <%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*"%>
  <jsp:include page="include.jsp" flush="true"/>
  <%


	String studyId = request.getParameter("studyId");
	String invPkStr = request.getParameter("invPk");
	String paymentPk = request.getParameter("paymentPk");
	String pageRight = request.getParameter("pR");

	boolean notReconciled = false;
HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession))

   {



	String usr = (String) tSession.getAttribute("userId");

	int invPk = EJBUtil.stringToNum(invPkStr);
	int totalCount = 1; // starts with a count for invoice



 if (invPk > 0)
 {
	 	//KM-to fix the issue-3328
	 	String[] arMilestoneType =  new String[5];
 		arMilestoneType[0] = "PM";
 		arMilestoneType[1] = "VM";
 		arMilestoneType[2] = "EM";
 		arMilestoneType[3] = "SM";
 		arMilestoneType[4] = "AM"; //KM
 		Hashtable htParam = new Hashtable();
 		double totalInvoicedAmount = 0;
 		BigDecimal totalInvoicedAmount1 = new BigDecimal("0.0");

 		htParam.put("calledFrom","INV");
 		String linkType = "";

 		String[] arMilestoneTypeHeader =  new String[5];
 		arMilestoneTypeHeader[0] = LC.L_Pat_StatusMstones/*LC.Pat_Patient+" Status Milestones"*****/;
 		arMilestoneTypeHeader[1] = LC.L_Visit_Mstones/*"Visit Milestones"*****/;
 		arMilestoneTypeHeader[2] = LC.L_Evt_Mstones/*"Event Milestones"*****/;
 		arMilestoneTypeHeader[3] = LC.L_Std_Mstones/*LC.Std_Study +" Milestones"*****/;
 		arMilestoneTypeHeader[4] = LC.L_Addl_Mstones/*"Additional Milestones"*****/;


 		String invNumber = "";

 		InvB.setId(invPk);
 		InvB.getInvoiceDetails();
 		invNumber  = InvB.getInvNumber();

 		totalInvoicedAmount = InvB.getTotalAmountInvoiced(invPk);
 		totalInvoicedAmount1= new BigDecimal(totalInvoicedAmount);

 		PaymentDetailsDao pdao = new PaymentDetailsDao();

 		PaymentDetailsDao pdaoLevel1 = new PaymentDetailsDao();
 		int idxLevel1 = -1;
 		int idxLevel2 = -1;

 		int alreadyEnteredCount = 0;
 		String prevAppliedAmountTotal = "0.00";
 		String prevAmount = "0.00";
 		double prevHoldbackAmount = 0f;
 		BigDecimal prevHoldbackAmt1 = new BigDecimal("0.0");
 		String oldPk = "";
 		ArrayList arLevel1 = new ArrayList();
 		ArrayList arLevel1Amount = new ArrayList();
 		ArrayList<Double> arLevel1HoldbackAmount = new ArrayList<Double>();
 		ArrayList arLevel1PrevAppliedTotal= new ArrayList();
 		ArrayList arLevel1OldPk= new ArrayList();
 		Hashtable htLevel2Mapping = new Hashtable();
 		//ArrayList arLevel2MileAchvd= new ArrayList();


 		// get previous data totals/or previous data entered for the same payment and invoice


 		pdao = PayB.getPaymentDetailsWithTotals(EJBUtil.stringToNum(paymentPk),invPk,"I");

 		alreadyEnteredCount = pdao.getPaymentDetailRowCount();

 		notReconciled = true;
 		
 		if (alreadyEnteredCount <= 0) //there was no payment data added for this invoice and payment, get any previous totals (all payments)
 		{

 	 		pdao = PayB.getPaymentDetailsWithTotals(0,invPk,"I");
 			alreadyEnteredCount = pdao.getPaymentDetailRowCount();

 		}

 		if (alreadyEnteredCount > 0) // get data
 		{
 			//The control will be here if a payment is reconciled with any milestone/invoice/bugdet
	 		notReconciled = false;
	 		
 			//get Main record

 			prevAppliedAmountTotal =  (String )(pdao.getAppliedTotal()).get(0);
 			prevAmount = (String ) (pdao.getAmount()).get(0);
 			if (StringUtil.isEmpty(prevAmount))
 			{
 				prevAmount = "0.00";

 			}
 			oldPk = (String ) (pdao.getId()).get(0);

 			pdaoLevel1 = pdao.getArLevel1();
 			htLevel2Mapping = pdao.getLevel2Mapping();
 			arLevel1 = pdaoLevel1.getLinkToLevel1();
 	 		arLevel1Amount = pdaoLevel1.getAmount();
 	 		arLevel1HoldbackAmount = pdaoLevel1.getPrevHoldBackAmount();
 	 		arLevel1PrevAppliedTotal= pdaoLevel1.getAppliedTotal();
 	 		arLevel1OldPk = pdaoLevel1.getId();
 		}


 		%>
 		<jsp:include page="sessionlogging.jsp" flush="true"/>

		<BODY class="repBody" style="overflow: hidden;">

  <jsp:include page="paymenttabs.jsp" flush="true">
	<jsp:param name="studyId" value="<%=studyId%>"/>
	<jsp:param name="selectedTab" value="1"/>
	<jsp:param name="paymentPk" value="<%=paymentPk%>"/>
	<jsp:param name="pR" value="<%=pageRight%>"/>
  </jsp:include>
<div id="containerDiv" style="overflow:auto; height:90%;">
		<form name="pay" method = "post" id="linkInvDet" action="savepaymentdetails.jsp" onSubmit="ripLocaleFromAll(); if (validate(document.pay)== false) { setValidateFlag('false'); applyLocaleToAll(); return false; } else { setValidateFlag('true'); return true;}">
 			<hr class= "thinLine" />

 			<TABLE width="100%" style="border: solid black 1px;border-collapse: collapse;">
 			<tr><TH class="reportHeading" WIDTH="25%" ALIGN="CENTER"></TH>
 			<TH class="reportHeading" WIDTH="15%" ALIGN="CENTER"><%=LC.L_Invoiced_Amt%><%--Invoiced Amount*****--%> </TH>
 			<TH class="reportHeading" WIDTH="20%" ALIGN="CENTER"><%=MC.M_Applied_AmtToDate%><%--Applied Amount To Date*****--%></TH>
 			<TH class="reportHeading" WIDTH="20%" ALIGN="CENTER"><%=MC.M_AddlAmt_ForPay%><%--Additional Applied Amount from this payment*****--%></TH>
 			</tr>

			<tr><td><b><%=LC.L_Invoice%><%--Invoice*****--%>#</b>   <%=invNumber%> </td>
			<td ALIGN="CENTER"><span class="numberfield" data-unitsymbol="" data-formatas="currency"><%=totalInvoicedAmount1%></span></td>
			<td ALIGN="CENTER"><span class="numberfield" data-unitsymbol="" data-formatas="currency"><%=prevAppliedAmountTotal%></span></td>
			<%String styleStr ="";
			if ((notReconciled && !isAccessibleFor(EJBUtil.stringToNum(pageRight),'N')) ||
				((!notReconciled) && !isAccessibleFor(EJBUtil.stringToNum(pageRight),'E'))){
				styleStr = "READONLY";
			}
			%>
			<td ALIGN="CENTER"> <input type="text" <%=styleStr %> name="applyAmount" class="numberfield" data-unitsymbol="" data-formatas="currency" value = "<%=prevAmount%>" <%=(styleStr!=null && styleStr!="")?"onfocus=\"this.blur();\"":""%> /></td>
			<!-- Mukul: BUG 4020 NumberUtil.roundOffNo() function used to roundoff number upto two decimal places -->
		 	<input type="hidden"  name ="applyHoldback" value="0"/>
		 	<input type="hidden" name="holdback"  value="false" />
		 	<input type="hidden"  name ="level1Id" value="0"/>
		 	<input type="hidden"  name="level2Id" value="0"/>
		 	<input type="hidden"  name="detailType" value="I"/>
		 	<input type="hidden"  name="linkType" value="I"/>
		 	<input type="hidden"  name ="linktoId" value="<%= invPk%>"/>
		 	<input type="hidden"  name ="oldpk" value="<%= oldPk%>"/>
		 	<input type="hidden"  name ="mileDetFKAchieved" value="0"/>

			</tr>

 		<!--  One record for main invoice -->

 		<%

	for (int t = 0; t <arMilestoneType.length;t++)
 		{

 		InvoiceDetailDao inv = new InvoiceDetailDao ();
 		StringBuffer sbPM = new StringBuffer();
 		//get patient milestones invoice details
 		inv = InvDetB.getInvoiceDetailsForMilestoneType(invPk,arMilestoneType[t],htParam);

 		ArrayList arMilestoneIds = new ArrayList();
 		ArrayList arMilestoneRuleDescs = new ArrayList();
 		ArrayList arMilestoneAchievedCounts = new ArrayList();
 	//	ArrayList arMileAmounts = new ArrayList();
 		ArrayList arMileAmountsInvoiced = new ArrayList();
 		ArrayList arMileAmountsHoldback = new ArrayList();
 		ArrayList arMilePatientCodes = new ArrayList();
 		ArrayList arPatientPks= new ArrayList();
 		ArrayList arMileAchievedDate = new ArrayList();
 		ArrayList arShowDetail = new ArrayList();
		ArrayList arDetailType = new ArrayList();
		ArrayList arLinkFKAchs = new ArrayList();

 		String mileStoneId = "";
 		arMilestoneIds = inv.getMilestone();
 		arMilestoneRuleDescs = inv.getMilestonesDesc();
 		arMilestoneAchievedCounts = inv.getMilestonesAchievedCount();
 		//arMileAmounts = inv.getAmountDue();
 		arMileAmountsInvoiced =  inv.getAmountInvoiced();
 		arMileAmountsHoldback = inv.getAmountHoldback();
 		arPatientPks = inv.getPatient();
 		arLinkFKAchs = inv.getLinkMileAchieved();

 		arMilePatientCodes = inv.getPatCode();
 		arMileAchievedDate = inv.getAchDate();
 		arShowDetail = inv.getDisplayDetail();
 		arDetailType = inv.getDetailType();

 		String mileDesc = "";
 		int milecount = 0;
 		int dispHoldback = 0;
 		BigDecimal amountInvoiced = new BigDecimal("0.0");
 		BigDecimal amountHoldback = new BigDecimal("0.0");
 		BigDecimal totalAmountInvoiced = new BigDecimal("0.0");
 		String detailType = "";
 		PaymentDetailsDao pdaoLevel2 = new PaymentDetailsDao();
 		ArrayList arLevel2Amount = new ArrayList();
 		ArrayList<Double> arLevel2HoldbackAmount = new ArrayList<Double>();
 		ArrayList arLevel2PrevAppliedTotal= new ArrayList();
 		ArrayList arLevel2OldPk= new ArrayList();
 		ArrayList arLevel2 = new ArrayList();
		ArrayList arLevel2MileAchvd= new ArrayList();


		if (arMilestoneIds != null)
 		{
 			milecount = arMilestoneIds.size();
 		}
 		else
 		{
	 		milecount  = 0;
 		}

 		// for each milestone, get the milestone achieved details
	%>
 		<tr style="border: none;" colspan=4><td><b><%= arMilestoneTypeHeader[t]%></b></td></tr>

 		<%

 		for (int i =0; i < milecount ; i++)
 		{
 			mileStoneId = (String )arMilestoneIds.get(i);

			amountInvoiced = new BigDecimal(Double.parseDouble((String )arMileAmountsInvoiced.get(i)));
			amountHoldback = new BigDecimal(Double.parseDouble((String )arMileAmountsHoldback.get(i)));
			detailType = (String) arDetailType.get(i);
			
			if(amountHoldback.doubleValue()>0 && amountInvoiced.doubleValue()>0)
				dispHoldback = 2;
			else
				dispHoldback = 1;
		 for(int j=0; j < dispHoldback ; j++)
			{
			 if(j==0)
			 totalCount = totalCount + 1;
			if (detailType.equals("H"))
			{
				if(j==1)
 			   		totalAmountInvoiced = totalAmountInvoiced.add(amountHoldback);
 			    else
 			    {
 			    	if(amountInvoiced.doubleValue()>0)
	 		   			totalAmountInvoiced = totalAmountInvoiced.add(amountInvoiced);
 			    	else
 				   		totalAmountInvoiced = totalAmountInvoiced.add(amountHoldback); 
 			    }
	 		   mileDesc = (String ) arMilestoneRuleDescs.get(i);
	 		  linkType = "M"; // for milestones

	 		  // find data in level1 arrayLists
	 		  if (arLevel1 != null)
	 		  {
		 		  idxLevel1 = arLevel1.indexOf(mileStoneId);

		 		  if (idxLevel1 >= 0)
		 		  {
		 			prevAmount = (String) arLevel1Amount.get(idxLevel1);
		 			prevHoldbackAmount = arLevel1HoldbackAmount.get(idxLevel1);
		 			prevHoldbackAmt1 = new BigDecimal(prevHoldbackAmount);
		  			oldPk = (String ) arLevel1OldPk.get(idxLevel1);
		  			prevAppliedAmountTotal =  (String ) arLevel1PrevAppliedTotal.get(idxLevel1);

		 		  }
		 		  else
		 		  {
		 			 prevAmount = "0.00";
		 			 prevHoldbackAmount = 0f;
		 			 prevHoldbackAmt1 = new BigDecimal(prevHoldbackAmount);
		 			 oldPk  = "0";
		 			prevAppliedAmountTotal = "0.00";
		 		  }
	 		  }

				//get the level2 DAo for the milestone
	 			pdaoLevel2 = (PaymentDetailsDao) htLevel2Mapping.get(mileStoneId);

		 		if (pdaoLevel2 != null)
		 		{

					arLevel2Amount = pdaoLevel2.getAmount();
					arLevel2HoldbackAmount = pdaoLevel2.getPrevHoldBackAmount();
	 		 		arLevel2PrevAppliedTotal= pdaoLevel2.getAppliedTotal();
		 	 		arLevel2OldPk = pdaoLevel2.getId();
		 	 		arLevel2 = pdaoLevel2.getLinkToLevel2();
		 	 		arLevel2MileAchvd = pdaoLevel2.getLinkMileAchieved();
		 		}

	 		}
	 		else
	 		{
	 			mileDesc = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + (String)arMilePatientCodes.get(i);
		 		linkType = "P"; // for patient

		 		// find data in level2

		 		if (pdaoLevel2 != null)
		 		{

		 			//idxLevel2 = arLevel2.indexOf((String )arPatientPks.get(i));
		 			idxLevel2 = arLevel2MileAchvd.indexOf((String )arLinkFKAchs.get(i));

			 		  if (idxLevel2 >= 0)
			 		  {
			 			prevAmount = (String) arLevel2Amount.get(idxLevel2);
			 			prevHoldbackAmount =  arLevel2HoldbackAmount.get(idxLevel2);
			 			prevHoldbackAmt1 = new BigDecimal(prevHoldbackAmount);
			 			if (StringUtil.isEmpty(prevAmount))
			 			{
			 				prevAmount = "0";
			 			}
			  			oldPk = (String ) arLevel2OldPk.get(idxLevel2);
			  			prevAppliedAmountTotal =  (String ) arLevel2PrevAppliedTotal.get(idxLevel2);

			 		  }
			 		  else
			 		  {
			 			 prevAmount = "0.00";
			 			 prevHoldbackAmount = 0f;
			 			 prevHoldbackAmt1 = new BigDecimal(prevHoldbackAmount);
			 			 oldPk  = "0";
			 			 prevAppliedAmountTotal = "0.00";
			 		  }
		 		}
		 		else
		 		{
		 			 prevAmount = "0.00";
		 			 prevHoldbackAmount = 0f;
		 			 prevHoldbackAmt1 = new BigDecimal(prevHoldbackAmount);
		 			 oldPk  = "0";
		 			 prevAppliedAmountTotal = "0.00";

		 		}

	 		}


			out.println("<tr ");
				if(i%2==0){
					out.println(" <tr class=\"browserEvenRow\"");
			}else{

				out.println(" <tr class=\"browserOddRow\"");
			}

			if(mileDesc.length() > 500) { //KM
				mileDesc = mileDesc.substring(0,500)+"...";
			}
				
			if(j==1){
				out.println(" style=\"border: none;\"><td>"+ mileDesc+"<sup>H</sup></td>");
				 %>
		 	<td ALIGN="CENTER"><span class="numberfield" data-unitsymbol="" data-formatas="currency"><%=amountHoldback%></span></td>
		 	<td ALIGN="CENTER"><span class="numberfield" data-unitsymbol="" data-formatas="currency"><%=prevHoldbackAmt1%></span></td>
		 	<td ALIGN="CENTER"> <input <%=styleStr%> type="text" name="applyHoldback" class="numberfield" data-unitsymbol="" data-formatas="currency" value="<%=prevHoldbackAmt1%>"/></td>
		 	<input type="hidden" name="holdback"  value="true" />
		 	<%}else{
				if(amountInvoiced.doubleValue()>0)
				{
					out.println(" style=\"border: none;\"><td>"+ mileDesc+"</td>"); 
					if(amountHoldback.doubleValue()<=0) 
					{%>
			<input type="hidden"  name ="applyHoldback" value="0"/>
		 	<input type="hidden" name="holdback"  value="false" />
		 	<%} %>
			<td ALIGN="CENTER"><span class="numberfield" data-unitsymbol="" data-formatas="currency"><%=amountInvoiced%></span></td>
			<td ALIGN="CENTER"><span class="numberfield" data-unitsymbol="" data-formatas="currency"><%=prevAppliedAmountTotal%></span></td>
			<td ALIGN="CENTER"> <input <%=styleStr%> type="text" name="applyAmount" class="numberfield" data-unitsymbol="" data-formatas="currency" value="<%=prevAmount%>"/></td>	
			<input type="hidden"  name ="level1Id" value="<%=mileStoneId%>"/>
		 	<input type="hidden"  name="level2Id" value="<%= (String )arPatientPks.get(i)%>"/>
		 	<input type="hidden"  name="detailType" value="<%= detailType%>"/>
		 	<input type="hidden"  name="linkType" value="<%=linkType %>"/>
		 	<input type="hidden"  name ="linktoId" value="<%= invPk%>"/>
		 	<input type="hidden"  name ="oldpk" value="<%= oldPk%>"/>
		 	<input type="hidden" name="mileDetFKAchieved"  value="<%= EJBUtil.stringToNum((String)arLinkFKAchs.get(i))%>" />
			<%  	} 
				else
				    {
					out.println(" style=\"border: none;\"><td>"+ mileDesc+"<sup>H</sup></td>");
				    	if(amountInvoiced.doubleValue()<=0) 
					{%>
			<input type="hidden"  name ="applyAmount" value="0"/>
		 	<input type="hidden" name="holdback"  value="true" />
		 	<%} %>
		 	<td ALIGN="CENTER"><span class="numberfield" data-unitsymbol="" data-formatas="currency"><%=amountHoldback%></span></td>
		 	<td ALIGN="CENTER"><span class="numberfield" data-unitsymbol="" data-formatas="currency"><%=prevHoldbackAmt1%></span></td>
		 	<td ALIGN="CENTER"> <input <%=styleStr%> type="text" name="applyHoldback" class="numberfield" data-unitsymbol="" data-formatas="currency" value="<%=prevHoldbackAmt1%>"/></td>
			<input type="hidden"  name ="level1Id" value="<%=mileStoneId%>"/>
		 	<input type="hidden"  name="level2Id" value="<%= (String )arPatientPks.get(i)%>"/>
		 	<input type="hidden"  name="detailType" value="<%= detailType%>"/>
		 	<input type="hidden"  name="linkType" value="<%=linkType %>"/>
		 	<input type="hidden"  name ="linktoId" value="<%= invPk%>"/>
		 	<input type="hidden"  name ="oldpk" value="<%= oldPk%>"/>
		 	<input type="hidden" name="mileDetFKAchieved"  value="<%= EJBUtil.stringToNum((String)arLinkFKAchs.get(i))%>" />    
				<%  }
				}%>
	 			</tr>
			<%
			}

		} // for patient milestones

	  } // for milestone type loop
 	} // if pkInv > 0

 	%>
	</table><hr class= "thinLine" />
	<input type="hidden"  name ="paymentPk" value="<%=paymentPk%>"/>
	<input type="hidden"  name ="totalCount" value="<%=totalCount%>"/>
	<input type="hidden"  name ="pR" value="<%=pageRight%>"/>
	<input type="hidden"  name ="studyId" value="<%=studyId%>"/>

	<table width="100%">

   <tr>

  <!-- <td>

		e-Signature <FONT class="Mandatory">* </FONT>

   </td>

   <td>

		<input type="password" name="eSign" maxlength="8">

		</td>-->
		<td width="85%" align="center">
<% String showSubmit ="";
	if((notReconciled && isAccessibleFor(EJBUtil.stringToNum(pageRight),'N')) ||
		((!notReconciled) && isAccessibleFor(EJBUtil.stringToNum(pageRight),'E')))
	{ showSubmit ="Y";} else {showSubmit="N";} 
%>

<%if (showSubmit.equals("Y")){ %>
		<jsp:include page="submitBar.jsp" flush="true">
				<jsp:param name="displayESign" value="Y"/>
				<jsp:param name="formID" value="linkInvDet"/>
				<jsp:param name="showDiscard" value="N"/>
		</jsp:include>
<%}%>
   </td>

   </tr>

   </table>


	</form>
	</div>
   	</BODY>
	<%
}//end of if body for session

else
{
%>
 <jsp:include page="timeout_childwindow.jsp" flush="true"/>
  <%
}
%>



<script>

</script>
<div class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</HTML>
<%!
private boolean isAccessibleFor(int pageRight, char type) {
    switch(type) {
    case 'V': return ((pageRight & 0x0004) >> 2) == 1;
    case 'E': return ((pageRight & 0x0002) >> 1) == 1;
    case 'N': return  (pageRight & 0x0001) == 1;
    }
    return false;
}
%>
<script>
$j('html, body').attr('style','overflow:hidden;');
</script>