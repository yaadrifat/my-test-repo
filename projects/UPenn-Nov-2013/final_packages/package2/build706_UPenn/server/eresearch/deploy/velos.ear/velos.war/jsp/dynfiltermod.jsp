<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title><%=LC.L_Filter%><%--Filter*****--%></title>
	<%@ page language = "java" import = "java.util.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB"%>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<jsp:useBean id="dynrepB" scope="page" class="com.velos.eres.web.dynrep.DynRepJB" />
	<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
	<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="resolution.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<script>

function  validate(formobj){

	fltrName=fnTrimSpaces(formobj.fltrName.value);
	totcount=formobj.totcount.value;
	
	if (fltrName.length==0){
	alert("<%=MC.M_Specify_FilterName%>");/*alert("Please Specify a Filter Name");*****/
	formobj.fltrName.focus();
	return false;
	
	}
	if(isNaN(formobj.addrow.value) == true) {

	alert("<%=MC.M_InorrectRowsNum_ReEtr%>");/*alert("Incorrect number of rows specified. Please enter again");*****/

	formobj.addrow.focus();
	
	return false;
   }	
     if (!(validate_col('eSign',formobj.eSign))) return false


	if(isNaN(formobj.eSign.value) == true) {

	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect E-Signature. Please enter again");*****/

	formobj.eSign.focus();
	
	return false;
   }
   
   if (!setFilter(formobj)) return false;
   formobj.action="updatedynfilter.jsp";
   
   formobj.submit();
   }
   function changeMode(formobj,modeStr){

formobj.mode.value=modeStr;

formobj.submit();
}
function setType(formobj,count){
totcount=formobj.totcount.value;
if (totcount==1){

index=formobj.fldName.selectedIndex;
if (index<totcount){

type=formobj.fldTypeOrig.value;
formobj.fldType.value=type;

}
}else if (totcount>1){

index=formobj.fldName[count].selectedIndex;
if (index<totcount){
type=formobj.fldTypeOrig[index].value
formobj.fldType[count].value=type;

}

}
}
function setFilter(formobj){
totcount=formobj.totcount.value;
formId=formobj.formId.value;
var startcount=0;
var endcount=0;
var filterTemp="";
var filter="";
var criteriaStr="";
var extend="";
var prevCount=-1;
if (totcount==1){
for (i=0;i<totcount;i++){
if (formobj.exclude.checked)
continue;
startBrac=formobj.startBracket.value;
 
 if (formobj.startBracket.checked)
    startcount++; 
 fldName=formobj.fldName.value;
 criteria=formobj.scriteria.value;
 fltdata=formobj.fltData.value;
 endBrac=formobj.endBracket.value;
 fldType=formobj.fldType.value;
 if (formobj.startBracket.checked)
    endcount++;
 //extend=formobj.extend.value;
if (fldName.length==0 && fltdata.length==0 && criteria.length==0)
continue;
if (((fldName.length>0) && fltdata.length>0) && criteria.length==0){
alert("<%=MC.M_SelValue_ForQualifier%>");/*alert("Please select a value for the Qualifier.");*****/
formobj.scriteria.focus();
return false;
}

if (((fldName.length>0) && fltdata.length==0) && criteria.length==0){
alert("<%=MC.M_SelValue_ForQualifier%>");/*alert("Please select a value for the Qualifier.");*****/
formobj.scriteria.focus();
return false;
}
if ((fldName.length==0) && (fltdata.length>0 && criteria.length>0)){
 alert("<%=MC.M_Selc_FldFld%>");/*alert("Please select a filter Field.");*****/
 formobj.fldName.focus();
 return false;
}
if (((fldName.length>0) && criteria.length>0) &&  fltdata.length==0 && (criteria!="first" && criteria!="latest" && criteria!="highest" && criteria!="lowest" && criteria!="isnull")){
var paramArray = [i];
alert(getLocalizedMessageString("M_Row_PlsEtrVal",paramArray));/*alert("Row#"+i+"-Please enter a value.");*****/
formobj.fltData.focus();
return false;
}
if ((fldType=="ED") && (criteria=="isequalto" || criteria=="isgt" || criteria=="islt" || criteria=="isgte" || criteria=="islte" )){ 
if (!validate_date(formobj.fltData[i])) return false;
}
if ((fldType=="EN") && (criteria=="isequalto" || criteria=="isgt" || criteria=="islt" || criteria=="isgte" || criteria=="islte" )){
if(isNaN(formobj.fltData.value) == true) {
	alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid number.");*****/
	formobj.fltData.focus();
	return false;
   }
}

if ((fldName.length==0 && fltdata.length==0) &&  (criteria.length>0 )) 
continue;
if ((fldName.length==0 && criteria.length==0) && (fltdata.length>0))  
continue;
if (formobj.startBracket.checked){
	 filterTemp="(";
  }
   if (criteria=="isnull"){
 if (fldName.indexOf("|")>-1){ 
    tempArray=fldName.split("|");
    /*for(count=0;count<tempArray.length;count++){
    
    if (criteriaStr.length==0)
    criteriaStr="("+tempArray[count]+") is null ";
    else 
    criteriaStr=criteriaStr+" or ("+fldName+") is null ";
    }*/
    } else{
   criteriaStr="("+fldName+")  is null"
   }
  }
 if (criteria=="contains"){
 if (fldName.indexOf("|")>-1){ 
    tempArray=fldName.split("|");
    for(count=0;count<tempArray.length;count++){
    
    if (criteriaStr.length==0)
    criteriaStr="lower("+tempArray[count]+") like lower('%"+ fltdata +"%')";
    else 
    criteriaStr=criteriaStr+" or lower("+fldName+") like lower('%"+ fltdata +"%')";
    }
    } else{
   criteriaStr="lower("+fldName+") like lower('%"+ fltdata +"%')"
   }
  }
 if (criteria=="isequalto"){
   if (fldType=="ED") criteriaStr="F_TO_DATE("+fldName+") = TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
   else if (fldType=="EN") criteriaStr="TO_NUMBER("+fldName+") = TO_NUMBER('"+ fltdata +"')"
  else criteriaStr="lower("+fldName+") = lower('"+ fltdata +"')"
  }
  if (criteria=="isnotequal"){
   if (fldType=="ED") criteriaStr="F_TO_DATE("+fldName+") != TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
   else if (fldType=="EN") criteriaStr="TO_NUMBER("+fldName+") != TO_NUMBER('"+ fltdata +"')"
  else criteriaStr="lower("+fldName+") != lower('"+ fltdata +"')"
  }
  if (criteria=="start"){
   criteriaStr="lower("+fldName+") like lower('"+ fltdata +"%')"
  }
  if (criteria=="ends"){
   criteriaStr="lower("+fldName+") like lower('%"+ fltdata +"')"
  }
   if (criteria=="isgt"){
   if (fldType=="ED") criteriaStr="F_TO_DATE("+fldName+") > TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
   else if (fldType=="EN") criteriaStr="TO_NUMBER("+fldName+") > TO_NUMBER('"+ fltdata +"')"
   else   criteriaStr="lower("+fldName+") > lower('%"+ fltdata +"%')"
   }
 if (criteria=="islt"){
  if (fldType=="ED") criteriaStr="F_TO_DATE("+fldName+") < TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
  else if (fldType=="EN") criteriaStr="TO_NUMBER("+fldName+") < TO_NUMBER('"+ fltdata +"')"
   else criteriaStr="lower("+fldName+") < lower('%"+ fltdata +"%')"
  
  }
 if (criteria=="isgte"){
    if (fldType=="ED") criteriaStr="F_TO_DATE("+fldName+") >= TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
    else if (fldType=="EN") criteriaStr="TO_NUMBER("+fldName+") >= TO_NUMBER('"+ fltdata +"')"
   else    criteriaStr="lower("+fldName+") >= lower('%"+ fltdata +"%')"
   
  }
  if (criteria=="islte"){
   if (fldType=="ED") criteriaStr="F_TO_DATE("+fldName+") <= TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
   else if (fldType=="EN") criteriaStr="TO_NUMBER("+fldName+") <= TO_NUMBER('"+ fltdata +"')"
   else criteriaStr="lower("+fldName+") <= lower('%"+ fltdata +"%')"
  }
  if (criteria=="first"){
   criteriaStr="filldate=(select min(filldate) from er_formslinear where fk_form="+formId+")";
  }
  if (criteria=="latest"){
   criteriaStr="filldate=(select max(filldate) from er_formslinear where fk_form="+formId+")";
  }
  
  if (criteria=="lowest"){
   criteriaStr="lower("+fldName+") =(select min(lower("+ fldName+")) from er_formslinear where fk_form="+ formId+")" 
  }
  if (criteria=="highest"){
   criteriaStr="lower("+fldName+") =(select max(lower("+ fldName+")) from er_formslinear where fk_form="+ formId+")" 
  }
  if (criteriaStr.length>0){
  filter=filter+filterTemp+criteriaStr;
  if (formobj.endBracket.checked){
    filter=filter+" )";
  }
  }
 

}//end Fpr loop
}
else{ //else for totcount
criteriaStr="";
for(i=0;i<totcount;i++){
filterTemp="";
criteriaStr="";
if (formobj.exclude[i].checked)
 continue;
 startBrac=formobj.startBracket[i].value;
  if (formobj.startBracket[i].checked)
    startcount++; 
 fldName=formobj.fldName[i].value;
 criteria=formobj.scriteria[i].value;
 
 fltdata=formobj.fltData[i].value;
 endBrac=formobj.endBracket[i].value;
 fldType=formobj.fldType[i].value;
  if (formobj.endBracket[i].checked)
    endcount++;
if ((fldName.length==0 && fltdata.length==0) &&  (criteria.length>0)) 
continue;
if ((fldName.length==0 && criteria.length==0) && (fltdata.length>0))  
continue;
if ((fldName.length==0) && (fltdata.length==0 &&  criteria.length==0)) 
continue;
    
    
if ((i>0) && (filter.length>0))  {
 extend=formobj.extend[prevCount].value;
 
 }
 
 else {
 extend="";
 }
 prevCount=i;
 
if (((fldName.length>0) && fltdata.length>0) && criteria.length==0){
var paramArray = [i];
alert(getLocalizedMessageString("M_Row_PlsSelQualifier",paramArray));/*alert("Row#"+i+"-Please select a value for the Qualifier.");*****/
formobj.scriteria[i].focus();
return false;

}

if (((fldName.length>0) && fltdata.length==0) && criteria.length==0){
var paramArray = [i];
alert(getLocalizedMessageString("M_Row_PlsSelQualifier",paramArray));/*alert("Row#"+i+"-Please select a value for the Qualifier.");*****/
formobj.scriteria[i].focus();
return false;
}

if ((fldName.length==0) && (fltdata.length>0 && criteria.length>0)){
 var paramArray = [i];
 alert(getLocalizedMessageString("M_Row_PlsSelFld",paramArray));/*alert("Row#"+i+"-Please select a filter Field.");*****/
 formobj.fldName[i].focus();
 return false;
}
if (((fldName.length>0) && criteria.length>0) &&  fltdata.length==0 && (criteria!="first" && criteria!="latest" && criteria!="highest" && criteria!="lowest" && criteria!="isnull")){
var paramArray = [i];
alert(getLocalizedMessageString("M_Row_PlsEtrVal",paramArray));/*alert("Row#"+i+"-Please enter a value.");*****/
formobj.fltData[i].focus();
return false;
}

if ((fldType=="ED") && (criteria=="isequalto" || criteria=="isgt" || criteria=="islt" || criteria=="isgte" || criteria=="islte" )) { 
if (!validate_date(formobj.fltData[i])) return false;
}
if ((fldType=="EN") && (criteria=="isequalto" || criteria=="isgt" || criteria=="islt" || criteria=="isgte" || criteria=="islte" )){
if(isNaN(formobj.fltData[i].value) == true) {
	alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid number.");*****/
	formobj.fltData[i].focus();
	return false;
   }
}
if (formobj.startBracket[i].checked){
	 filterTemp=" "+extend+" (";
  }
  else {
  	 filterTemp=" "+extend+" ";
  } 
   if (criteria=="isnull"){
   if (fldName.indexOf("|")>-1){ 
    tempArray=fldName.split("|");
    /*for(count=0;count<tempArray.length;count++){
    if (criteriaStr.length==0)
    criteriaStr="(("+tempArray[count]+") is null ";
    else 
    criteriaStr=criteriaStr+" or ("+tempArray[count]+") is null";
    }
    criteriaStr=criteriaStr+")";*/
    } else{
   criteriaStr="("+fldName+") is null"
   }
  }  
 if (criteria=="contains"){
   if (fldName.indexOf("|")>-1){ 
    tempArray=fldName.split("|");
    for(count=0;count<tempArray.length;count++){
    if (criteriaStr.length==0)
    criteriaStr="(lower("+tempArray[count]+") like lower('%"+ fltdata +"%')";
    else 
    criteriaStr=criteriaStr+" or lower("+tempArray[count]+") like lower('%"+ fltdata +"%')";
    }
    criteriaStr=criteriaStr+")";
    } else{
   criteriaStr="lower("+fldName+") like lower('%"+ fltdata +"%')"
   }
  }
  if (criteria=="isequalto"){
   if (fldName.indexOf("|")>-1){ 
    tempArray=fldName.split("|");
    for(count=0;count<tempArray.length;count++){
       
    if (criteriaStr.length==0){
    if (fldType=="ED") criteriaStr="(F_TO_DATE("+tempArray[count]+") =TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
    else if (fldType=="EN") criteriaStr="(TO_NUMBER("+tempArray[count]+") =TO_NUMBER('"+ fltdata +"')"
    else criteriaStr="(lower("+tempArray[count]+") = lower('"+ fltdata +"')"
    }
    else{ 
        
	if (fldType=="ED") criteriaStr=criteriaStr +" or F_TO_DATE("+tempArray[count]+") = TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
	else if (fldType=="EN") criteriaStr=criteriaStr +" or TO_NUMBER("+tempArray[count]+") = TO_NUMBER('"+ fltdata +"')"
	else criteriaStr=criteriaStr +" or lower("+tempArray[count]+") = lower('"+ fltdata +"')"
	
	}
    }
    criteriaStr=criteriaStr+")";
    }else{
    if (fldType=="ED") criteriaStr="F_TO_DATE("+fldName+") = TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
    else if (fldType=="EN") criteriaStr="TO_NUMBER("+fldName+") = TO_NUMBER('"+ fltdata +"')"
    else  criteriaStr="lower("+fldName+") = lower('"+ fltdata +"')"
   
  }
  }
  if (criteria=="isnotequal"){
   if (fldName.indexOf("|")>-1){ 
    tempArray=fldName.split("|");
    for(count=0;count<tempArray.length;count++){
       
    if (criteriaStr.length==0){
    if (fldType=="ED") criteriaStr="(F_TO_DATE("+tempArray[count]+") !=TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
    else if (fldType=="EN") criteriaStr="(TO_NUMBER("+tempArray[count]+") !=TO_NUMBER('"+ fltdata +"')"
    else criteriaStr="(lower("+tempArray[count]+") != lower('"+ fltdata +"')"
    }
    else{ 
        
	if (fldType=="ED") criteriaStr=criteriaStr +" or F_TO_DATE("+tempArray[count]+") != TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
	else if (fldType=="EN") criteriaStr=criteriaStr +" or TO_NUMBER("+tempArray[count]+") != TO_NUMBER('"+ fltdata +"')"
	else criteriaStr=criteriaStr +" or lower("+tempArray[count]+") != lower('"+ fltdata +"')"
	
	}
    }
    criteriaStr=criteriaStr+")";
    }else{
    if (fldType=="ED") criteriaStr="F_TO_DATE("+fldName+") != TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
    else if (fldType=="EN") criteriaStr="TO_NUMBER("+fldName+") != TO_NUMBER('"+ fltdata +"')"
    else  criteriaStr="lower("+fldName+") != lower('"+ fltdata +"')"
   
  }
  }
  if (criteria=="start"){
   if (fldName.indexOf("|")>-1){ 
    tempArray=fldName.split("|");
    for(count=0;count<tempArray.length;count++){
    if (criteriaStr.length==0)
    criteriaStr="(lower("+tempArray[count]+") like lower('"+ fltdata +"%')"
    else
    criteriaStr=criteriaStr+" or lower("+tempArray[count]+") like lower('"+ fltdata +"%')"
    }
    criteriaStr=criteriaStr+")";
    }else{
   criteriaStr="lower("+fldName+") like lower('"+ fltdata +"%')"
   }
  }
  if (criteria=="ends"){
   if (fldName.indexOf("|")>-1){ 
    tempArray=fldName.split("|");
    for(count=0;count<tempArray.length;count++){
    if (criteriaStr.length==0)
    criteriaStr="(lower("+tempArray[count]+") like lower('%"+ fltdata +"')"
    else 
    criteriaStr=criteriaStr + " or lower("+tempArray[count]+") like lower('%"+ fltdata +"')"
    }
    criteriaStr=criteriaStr+")";
  }else{
    criteriaStr="lower("+fldName+") like lower('%"+ fltdata +"')"
    }
  }
  if (criteria=="isgt"){
   if (fldName.indexOf("|")>-1){ 
    tempArray=fldName.split("|");
    for(count=0;count<tempArray.length;count++){
    if (criteriaStr.length==0){
    if (fldType=="ED") criteriaStr="(F_TO_DATE("+tempArray[count]+") > TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
    else if (fldType=="EN") criteriaStr="(To_NUMBER("+tempArray[count]+") > TO_NUMBER('"+ fltdata +"')"
    else    criteriaStr="(lower("+tempArray[count]+") > lower('%"+ fltdata +"%')"
    }else{
    if (fldType=="ED") criteriaStr=criteriaStr + " or F_TO_DATE("+tempArray[count]+") > TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
    else if (fldType=="EN") criteriaStr=criteriaStr + " or TO_NUMBER("+tempArray[count]+") > TO_NUMBER('"+ fltdata +"')"
    else criteriaStr=criteriaStr + " or lower("+tempArray[count]+") > lower('%"+ fltdata +"%')"
    }
    }
    criteriaStr=criteriaStr+")";
   }else{
   if (fldType=="ED") criteriaStr="F_TO_DATE("+fldName+") > TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
   else if (fldType=="EN") criteriaStr="TO_NUMBER("+fldName+") > TO_NUMBER('"+ fltdata +"')"
   else   criteriaStr="lower("+fldName+") > lower('%"+ fltdata +"%')"
   }
  }
  if (criteria=="islt"){
  if (fldName.indexOf("|")>-1){ 
    tempArray=fldName.split("|");
    for(count=0;count<tempArray.length;count++){
    if (criteriaStr.length==0){
    if (fldType=="ED") criteriaStr="(F_TO_DATE("+tempArray[count]+") < TO_DATE('"+ fltdata +"','mm/dd/yyyy)"
    else if (fldType=="EN") criteriaStr="(TO_NUMBER("+tempArray[count]+") < TO_NUMBER('"+ fltdata +"')"
    else criteriaStr="(lower("+tempArray[count]+") < lower('%"+ fltdata +"%')"
    }else{
    if (fldType=="ED") criteriaStr=criteriaStr+" or F_TO_DATE("+tempArray[count]+") < TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
    else if (fldType=="EN") criteriaStr=criteriaStr+" or TO_NUMBER("+tempArray[count]+") < TO_NUMBER('"+ fltdata +"')"
    else  criteriaStr=criteriaStr+" or lower("+tempArray[count]+") < lower('%"+ fltdata +"%')"
    }
    }
    criteriaStr=criteriaStr+")";
   }else{
   if (fldType=="ED") criteriaStr="F_TO_DATE("+fldName+") < TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
   else if (fldType=="EN") criteriaStr="TO_NUMBER("+fldName+") < TO_NUMBER('"+ fltdata +"')"
   else criteriaStr="lower("+fldName+") < lower('%"+ fltdata +"%')"
   }
  }
  if (criteria=="isgte"){
  if (fldName.indexOf("|")>-1){ 
    tempArray=fldName.split("|");
    for(count=0;count<tempArray.length;count++){
    if (criteriaStr.length==0){
    if (fldType=="ED") criteriaStr="(F_TO_DATE("+tempArray[count]+") >= TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
     else if (fldType=="EN") criteriaStr="(TO_NUMBER("+tempArray[count]+") >= TO_NUMBER('"+ fltdata +"')"
    else criteriaStr="(lower("+tempArray[count]+") >= lower('%"+ fltdata +"%')"
    }
    else{
    if (fldType=="ED") criteriaStr=criteriaStr+ " or F_TO_DATE("+tempArray[count]+") >= TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
    else if (fldType=="EN") criteriaStr=criteriaStr+ " or TO_NUMBER("+tempArray[count]+") >= TO_NUMBER('"+ fltdata +"')"
    else criteriaStr=criteriaStr+ " or lower("+tempArray[count]+") >= lower('%"+ fltdata +"%')"
    }
    }
    criteriaStr=criteriaStr+")";
   }else{
   if (fldType=="ED") criteriaStr="F_TO_DATE("+fldName+") >= TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
   else if (fldType=="EN") criteriaStr="TO_NUMBER("+fldName+") >= TO_NUMBER('"+ fltdata +"')"
   else    criteriaStr="lower("+fldName+") >= lower('%"+ fltdata +"%')"
   }
  }
  if (criteria=="islte"){
  if (fldName.indexOf("|")>-1){ 
    tempArray=fldName.split("|");
    for(count=0;count<tempArray.length;count++){
    if (criteriaStr.length==0){
    	if (fldType=="ED") criteriaStr="(F_TO_DATE("+tempArray[count]+") <= TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
	else if (fldType=="EN") criteriaStr="(TO_NUMBER("+tempArray[count]+") <= TO_NUMBER('"+ fltdata +"')"
    	else criteriaStr="(lower("+tempArray[count]+") <= lower('%"+ fltdata +"%')"
	}else{
	if (fldType=="ED") criteriaStr=criteriaStr+ "or F_TO_DATE("+tempArray[count]+") <= TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
	else if (fldType=="EN") criteriaStr=criteriaStr+ "or TO_NUMBER("+tempArray[count]+") <= TO_NUMBER('"+ fltdata +"')"
	else criteriaStr=criteriaStr+ "or lower("+tempArray[count]+") <= lower('%"+ fltdata +"%')"
	}
    }
    criteriaStr=criteriaStr+")";
   }else{
   if (fldType=="ED") criteriaStr="F_TO_DATE("+fldName+") <= TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
   else if (fldType=="EN") criteriaStr="TO_NUMBER("+fldName+") <= TO_NUMBER('"+ fltdata +"')"
   else criteriaStr="lower("+fldName+") <= lower('%"+ fltdata +"%')"
   }
  }
  
  if (criteria=="first"){
   criteriaStr="filldate=(select min(filldate) from er_formslinear where fk_form="+formId+")";
  }
  if (criteria=="latest"){
   criteriaStr="filldate=(select max(filldate) from er_formslinear where fk_form="+formId+")";
  }
  
  if (criteria=="lowest"){
  if (fldName.indexOf("|")>-1){ 
    tempArray=fldName.split("|");
    for(count=0;count<tempArray.length;count++){
    if (criteriaStr.length==0)
    criteriaStr="(lower("+tempArray[count]+") =(select min(lower("+ tempArray[count]+")) from er_formslinear where fk_form="+ formId+")"
    else 
    criteriaStr=criteriaStr + " or lower("+tempArray[count]+") =(select min(lower("+ tempArray[count]+")) from er_formslinear where fk_form="+ formId+")"
    }
    criteriaStr=criteriaStr+")";
   }else{
   criteriaStr="lower("+fldName+") =(select min(lower("+ fldName+")) from er_formslinear where fk_form="+ formId+")"
   }
  }
  if (criteria=="highest"){
  if (fldName.indexOf("|")>-1){ 
    tempArray=fldName.split("|");
    for(count=0;count<tempArray.length;count++){
    if (criteriaStr.length==0)
    criteriaStr="(lower("+tempArray[count]+") =(select max(lower("+ tempArray[count]+")) from er_formslinear where fk_form="+ formId+")"
    else
    criteriaStr=criteriaStr + " or lower("+tempArray[count]+") =(select max(lower("+ tempArray[count]+")) from er_formslinear where fk_form="+ formId+")"
    }
    criteriaStr=criteriaStr+")";
    }else{
   criteriaStr="lower("+fldName+") =(select max(lower("+ fldName+")) from er_formslinear where fk_form="+ formId+")"
   }
  }
  if (criteriaStr.length>0){
  filter=filter+filterTemp+criteriaStr;
  if (formobj.endBracket[i].checked){
    filter=filter+" )";
  }
  }
    
  
}//end for
}//end else totcount
if (startcount!=endcount){
alert("<%=MC.M_BeginBrac_EndMismatch%>");/*alert("Begin Bracket/End Bracker Mismatch. Please Verify");*****/
return false;
}

//window.opener.document.forms["dynreport"].elements["repFilter"].value=filter;
if (filter.length>0) filter="(" + filter+")" ;

formobj.filter.value=filter;


return true;


//window.close();
}
function setInclude(formobj,count){
totcount=formobj.totcount.value;
if (totcount==1){

formobj.include.checked=true;
} else
{
formobj.include[count].checked=true;
}


}


</script>

</head>

<% String src,sql="",tempStr="",sqlStr="",repMode="";
   int strlen=0,firstpos=-1,secondpos=-1;
src= request.getParameter("srcmenu");
%>

<%
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
if (agent1 != null && agent1.indexOf("MSIE") != -1) 
   	ienet = 0; //IE
else
	ienet = 1;

if (ienet == 0) {
%>
<body style="overflow:scroll">
<%} else {%>
<body>
<%}%>

<br>
 <DIV class="popDefault" id="div1">  
<%
	
	String selectedTab = request.getParameter("selectedTab");
	String value="",patientID="",studyNumber="",orderStr="",order="",flgMatch="",fltrName="",fltrDD="",prevId="",fldType="";
	String[] scriteria=null,fldData=null,fldSelect= null;
	int personPk=0,studyPk=0,index=-1,arraySize=0,row=0,colLen=0;
	DynRepDao dynDao=new DynRepDao();
	ArrayList selectedfld=null;
	ArrayList flltDtIds=new ArrayList();
	ArrayList dataList=new ArrayList() ;
	ArrayList prevSessColId=new ArrayList();
	ArrayList tempList=new ArrayList();
	ArrayList startBracList=new ArrayList();
	ArrayList endBracList=new ArrayList();
	ArrayList scriteriaList=new ArrayList();
	ArrayList fldDataList=new ArrayList();
	ArrayList lmapCols= new ArrayList();
	ArrayList lmapDispNames= new ArrayList();
	HashMap attributes = new HashMap();
	HashMap FldAttr = new HashMap();
	HashMap FltrAttr = new HashMap();
	HashMap TypeAttr = new HashMap();
	
	String[] fldName=null;
	String[] fldCol=null;
	String[] fldNameSelect=null;
	String[] extend=null;
	ArrayList fldTypeList=new ArrayList();
	ArrayList fldNameSelList=new ArrayList();
	ArrayList extendList=new ArrayList();
	ArrayList excludeList=new ArrayList();
	ArrayList multiFltrIds=new ArrayList();
	ArrayList multiFltrNames=new ArrayList();
	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))
	{ 
	String mode = request.getParameter("mode");
	mode=(mode==null)?"":mode;
	System.out.println("mode for"+mode);
	fltrName=request.getParameter("fltrName");
	fltrName=(fltrName==null)?"":fltrName;
	int selValue=EJBUtil.stringToNum(request.getParameter("fltrId"));
	String formId=request.getParameter("formId");
	if (formId==null) formId="";
	String addrow=request.getParameter("addrow");
	if (addrow==null) addrow="";
	else row=EJBUtil.stringToNum(addrow);
	String repId=request.getParameter("repId");
	String repName=request.getParameter("repName");
	repId=(repId==null)?repId="":repId;
	repName=(repName==null)?repName="":repName;
	if (mode.equals("addrow")) {
	fldName=request.getParameterValues("fldLocal");
	fldCol=request.getParameterValues("fldLocalCol");
	for(int len=0;len<fldName.length;len++){
	System.out.println(" count " + len + " with fldname " + fldName[len] + " and  fldCol " + fldCol[len] );
	}
	}
	else {
	//get the form fields 
	dynDao=dynrepB.getMapData(formId);
	//dyndao.getMapData(formId);
	dynDao.preProcessMap();
	lmapCols= dynDao.getMapCols();
	
	fldCol=( String [] ) lmapCols.toArray ( new String [ lmapCols.size() ] );
	lmapDispNames=dynDao.getMapDispNames();
	fldName=( String [] ) lmapDispNames.toArray ( new String [ lmapDispNames.size() ] );
	fldTypeList=dynDao.getMapColTypes();
	}
	if (mode.equals("addrow")){
	String[] startBracket=request.getParameterValues("startBracket");
	String[] endBracket=request.getParameterValues("endBracket");
	String[] exclude = request.getParameterValues("exclude");
	String[] fldTypeOrigArray=request.getParameterValues("fldTypeOrig");
	String[] fltDt=request.getParameterValues("fltrDtId");
	if (fltDt!=null) 
	flltDtIds=EJBUtil.strArrToArrayList(fltDt);
	if (startBracket!=null)
	startBracList=EJBUtil.strArrToArrayList(startBracket);
	if (endBracket!=null)
	endBracList=EJBUtil.strArrToArrayList(endBracket);
	fldNameSelect=request.getParameterValues("fldName");
	if (fldNameSelect!=null)
	fldNameSelList=EJBUtil.strArrToArrayList(fldNameSelect);
	scriteria=request.getParameterValues("scriteria");
	if (scriteria!=null)
	scriteriaList=EJBUtil.strArrToArrayList(scriteria);
	fldData=request.getParameterValues("fltData");
	if (fldData!=null) 
	fldDataList=EJBUtil.strArrToArrayList(fldData);
	extend=request.getParameterValues("extend");
	if (extend!=null)
	extendList=EJBUtil.strArrToArrayList(extend);
	System.out.println(fldNameSelect);
	if (exclude!=null)
	excludeList=EJBUtil.strArrToArrayList(exclude);
	if (fldTypeOrigArray!=null)
	fldTypeList=EJBUtil.strArrToArrayList(fldTypeOrigArray);
	}
	
	if ((selValue>0) && (fldNameSelList.size()==0)){
	System.out.println("%%%%%%%%%%%%%%%%%%%%%%ForDB_selValue"+selValue);
	dynDao=dynrepB.getFilterDetails(EJBUtil.integerToString(new Integer(selValue)));
	startBracList=dynDao.getFiltBbrac();
	endBracList=dynDao.getFiltEbrac();
	fldNameSelList=dynDao.getFiltCol();
	flltDtIds= dynDao.getFltrColIds();
	scriteriaList=dynDao.getFiltQf();
	fldDataList=dynDao.getFiltData();
	extendList=dynDao.getFiltOper();
	excludeList=dynDao.getFiltExclude();
	if (fldNameSelList.size() > fldName.length)	row=row+(fldNameSelList.size() - fldName.length);
//	setFiltSeq(EJBUtil.integerToString(new Integer(rs.getInt("dynrepfilterdt_seq"))));
	}
	System.out.println("Selected filter" + fldNameSelList + row );
	//request.setAttribute("fldCol",selectList);
	//create a dropdown for fieldColumns
	int counter=0;
	String name="";
	StringBuffer fldDD=new StringBuffer();	
	
%>

<P class="sectionHeadings">  <%=LC.L_FrmRpt_Adv%><%--Form Report >> Advanced*****--%> </P>
<form name="dynfilter" method="post" action="dynfiltermod.jsp">
<input type="hidden" name="formId" value="<%=formId%>">
<input type="hidden" name="filter" value="">
<input type="hidden" name="totcount" value="<%=fldName.length+row%>">
<input type="hidden" name="repId" value="<%=repId%>">
<input type="hidden" name="repName" value="<%=repName%>">
<table width="100%"><tr>
<td width="60%"><%=LC.L_Specify_FilterName%><%--Specify Filter Name*****--%>&nbsp;&nbsp;<Input Type="text" name="fltrName" value="<%=StringUtil.decodeString(fltrName)%>"></td>
<Input Type="hidden" name="fltrId" value="<%=selValue%>">
<td width="20%"><%=LC.L_Add %><%--Add*****--%>&nbsp; <input type="text" name="addrow" value="0" size="2" maxlength="2" >&nbsp;<%=LC.L_More_Rows%><%--more rows*****--%></td>
<td><button type="submit" onClick="return changeMode(document.dynfilter,'addrow');"><%=LC.L_Refresh%></button></td>
<td><button onClick = "self.close()"><%=LC.L_Close%></button> </td>
</tr></table>
<input type="hidden" name="mode" value="">
<input type="hidden" name="srcmenu" value="<%=src%>">
<table class="tableDefault" width="100%" border="1">
<tr>
<th width="10%"><%=LC.L_Exclude%><%--Exclude*****--%></th>
<th width="15%"><%=LC.L_Start_Bracket%><%--Start Bracket*****--%></th>
<th width="20%"><%=LC.L_Select_Fld%><%--Select Field*****--%></th>
<th width="10%"><%=LC.L_Criteria%><%--Criteria*****--%></th>
<th width="20%"><%=LC.L_Value%><%--Value*****--%></th>
<th width="15%"><%=LC.L_End_Bracket%><%--End Bracket*****--%></th>
<th width="10%"><%=LC.L_AndOrOr%><%--And/Or*****--%></th>
</tr>
<%
System.out.println("fldName"+fldName.length+"ROW"+row);
for (int i=0;i<(fldName.length+row);i++){
	
  if ((i%2)==0)
  {%>
  <tr>
   <%}else{ %>
   <tr>
   <%}
   if (i<(fldName.length)){%>
     <input type="hidden" name="fldLocal" value="<%=fldName[i]%>">
     <input type="hidden" name="fldLocalCol" value="<%=fldCol[i]%>">
     
     <%}else{%>
     <input type="hidden" name="fldLocal" value="">
     <input type="hidden" name="fldLocalCol" value="">
     <%}%>
     
   <td width="10%">
   <%if (excludeList.indexOf((new Integer(i)).toString())>-1){%>
   <p align="center"><input type="checkbox" name="exclude" value="<%=i%>"   checked></p>
   <%}else{%>
   <p align="center"><input type="checkbox" name="exclude" value="<%=i%>" ></p>
   <%}%>
     </td>
   <td width="15%">
   <%if (startBracList.indexOf((new Integer(i)).toString())>-1){%>
   <p align="center"><input type="checkbox" name="startBracket" value="<%=i%>"   checked></p>
   <%}else{%>
   <p align="center"><input type="checkbox" name="startBracket" value="<%=i%>" ></p>
   <%}%>
   </td>
   <%if (i<(flltDtIds.size())){%>
        <input type="hidden" name="fltrDtId" value=<%=flltDtIds.get(i)%>>
	<%}else{%>
	<input type="hidden" name="fltrDtId" value="0">
	<%}%>
	
   <td width="20%"><%
   if (fldName!=null){
   flgMatch="N";
   %>
	<SELECT NAME='fldName' onChange="setType(document.dynfilter,<%=i%>)"> ;
	<%for (counter = 0; counter < (fldName.length) ; counter++){
		name =fldCol[counter];
		if (name==null) name="";
		/*if (name.length()>=32) name=name.substring(0,30);
		if (name.indexOf("?")>=0) 
		name=name.replace('?','q');
		if (name.indexOf("\\")>=0)
		name=name.replace('\\','S');*/
		if (name.length()>0){
		
		
		if ((fldNameSelList.size()>0) && (i<fldNameSelList.size())){
		
		if (name.equals(fldNameSelList.get(i))){
		flgMatch="Y";
		if (counter<fldTypeList.size())
		fldType=(String)fldTypeList.get(counter);
		if (fldType==null) fldType="";
		%>
		<OPTION value ='<%=name%>' selected><%=fldName[counter]%></OPTION>;
		<%}else{
		%>
		
		<OPTION value='<%=name%>'><%=fldName[counter]%></OPTION>
		<%}
		}
		else{
		
		%>
		<OPTION value='<%=name%>'><%=fldName[counter]%></OPTION>
		<%}
		}

	} 
	if (flgMatch.equals("N")){%>
		<OPTION value=''  selected><%=LC.L_Select_AnOption%><%--Select an Option*****--%></OPTION>
		<%}else{%>
	<OPTION value='' ><%=LC.L_Select_AnOption%><%--Select an Option*****--%></OPTION>
	<%}%>
	</SELECT>
	<%}%>
	</td>
	<%if (i<(fldTypeList.size())){%>
        <input type="hidden" name="fldType" value="<%=(fldType.length()==0)?"":fldType%>">
	<input type="hidden" name="fldTypeOrig" value="<%=fldTypeList.get(i)%>">
	<%}else{%>
	<input type="hidden" name="fldType" value="">
	<input type="hidden" name="fldTypeOrig" value="">
	<%}%>
   
      <td width="10%"> <select size="1" name="scriteria">
		  <option value="contains" <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if ((scriteriaList.get(i)).equals("contains")){%> Selected <%}}%>><%=LC.L_Contains%><%--Contains*****--%></option>
		  <option value="isnull" <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if ((scriteriaList.get(i)).equals("isnull")){%> Selected <%}}%>><%=LC.L_Is_Null%><%--Is Null*****--%></option>
		  <option value="isnotequal" <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if ((scriteriaList.get(i)).equals("isnotequal")){%> Selected <%}}%>><%=MC.M_Is_NotEqualTo%><%--Is Not Equal To*****--%></option>
  			<option value="isequalto" <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if ((scriteriaList.get(i)).equals("isequalto")){%> Selected <%}}%>><%=LC.L_Is_EqualTo%><%--Is equal to*****--%></option>
			  <option value="start" <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if ((scriteriaList.get(i)).equals("start")){%> Selected <%}}%>><%=LC.L_Begins_With%><%--Begins with*****--%></option>
			  <option value="ends" <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if ((scriteriaList.get(i)).equals("ends")){%> Selected <%}}%>><%=LC.L_Ends_With%><%--Ends with*****--%></option>
			  <option value="isgt" <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if ((scriteriaList.get(i)).equals("isgt")){%> Selected <%}}%>><%=LC.L_Is_GreaterThan%><%--Is Greater Than*****--%></option>
			  <option value="islt" <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if ((scriteriaList.get(i)).equals("islt")){%> Selected <%}}%>><%=LC.L_Is_LessThan%><%--Is Less Than*****--%></option>
			  <option value="isgte" <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if ((scriteriaList.get(i)).equals("isgte")){%> Selected <%}}%>><%=MC.M_Is_GtrThanEqual%><%--Is Greater than Equal*****--%></option>
			  <option value="islte" <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if ((scriteriaList.get(i)).equals("islte")){%> Selected <%}}%>><%=MC.M_Is_LessThanEqual%><%--Is Less than Equal*****--%></option>
			  <option value="first" <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if ((scriteriaList.get(i)).equals("first")){%> Selected <%}}%>><%=LC.L_First_Value%><%--First Value*****--%></option>
			  <option value="latest" <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if ((scriteriaList.get(i)).equals("latest")){%> Selected <%}}%>><%=LC.L_Latest_Value%><%--Latest Value*****--%></option>
			  <option value="highest" <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if ((scriteriaList.get(i)).equals("highest")){%> Selected <%}}%>><%=LC.L_Highest_Val%><%--Highest Value*****--%></option>
			  <option value="lowest" <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if ((scriteriaList.get(i)).equals("lowest")){%> Selected <%}}%>><%=LC.L_Lowest_Value%><%--Lowest Value*****--%></option>
			  <option value='' <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if (((String)scriteriaList.get(i)).length()==0){%> Selected <%}}else{%>Selected<%}%>><%=LC.L_Select_AnOption%><%--Select an Option*****--%></option>
			  </select></td>
	<td width="20%"><input type="text" name="fltData" maxLength="50" value=<%=(fldDataList.size()>0 && i<fldDataList.size())?((fldDataList.get(i)==null)?"":fldDataList.get(i)):""%>></td>
	<td width="15%">
	<%if (endBracList.indexOf((new Integer(i)).toString())>-1){%>
   <p align="center"><input type="checkbox" name="endBracket" value="<%=i%>" checked></p>
   <%}else{%>
   <p align="center"><input type="checkbox" name="endBracket" value="<%=i%>"></p>
   <%}%>
	</td>
<%if (i<((fldName.length+row)-1)){ %>
	<td width="10%"><select size="1" name="extend">
	
<option value="and" <%if ((extendList.size()>0) && (i<extendList.size())) {if ((extendList.get(i)).equals("and")){%> Selected <%}}%>><%=LC.L_And%><%--And*****--%></option>
<option value="or" <%if ((extendList.size()>0) && (i<extendList.size())) {if ((extendList.get(i)).equals("or")){%> Selected <%}}%>><%=LC.L_Or%><%--Or*****--%></option>
</select></td>
<%} else {%>
<input name="extend" value="and" type="hidden">
<%}%>
	
   <!--<td width="10%"><select size="1" name="fldOrder">
   <option value="">Select an Option</option>   
<option value="Asc">Ascending</option>
<option value="Desc">Descending</option>
</select></td><td width="65%" ></td>-->
      			
   
  
 </tr>
<%}%>
</table>

      <table width="100%" >
   <tr><td width="30%">	<A type="submit" href="dynfilterbrowse.jsp?repId=<%=repId%>&repName='<%=StringUtil.encodeString(repName)%>'&formId=<%=formId%>"><%=LC.L_Back%></A></td>
   <td width="15%">

		<%=LC.L_Esignature%><%--e-Signature*****--%> <FONT class="Mandatory">* </FONT>

	   </td>

	   <td width="50%">

		<input type="password" name="eSign" maxlength="8" autocomplete="off">
<input type="image" onClick="return validate(document.dynfilter)" src="../images/jpg/Submit.gif" align="absmiddle" border="0">
	   </td>


	</tr></table>
	
  
</form>
<%

} else {  //else of if body for session

%>
  <jsp:include page="timeout.html" flush="true"/>

  <%
}


%>
 
</DIV>
 


</body>
</html>
