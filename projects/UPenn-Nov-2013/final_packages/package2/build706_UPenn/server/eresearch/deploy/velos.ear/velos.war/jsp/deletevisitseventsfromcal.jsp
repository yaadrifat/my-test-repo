<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<%@ page language = "java" import="com.velos.eres.service.util.*,com.velos.esch.business.common.*, com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%>
<%@ page import="com.aithent.audittrail.reports.AuditUtils"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>


<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
}
</SCRIPT>

<% String src;
	src= request.getParameter("srcmenu");
%>
<jsp:include page="skinChoser.jsp" flush="true"/>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<BODY>
<br>

<DIV class="formDefault" id="div1">

<%
HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession))	{
	String selStrs =request.getParameter("selStrs");
	String tableName =request.getParameter("tableName");
	String selectedTab=request.getParameter("selectedTab");
	String duration=request.getParameter("duration");
	String protocolId=request.getParameter("protocolId");
	String calledFrom=request.getParameter("calledFrom");
	String mode=request.getParameter("mode");
	String calStatus=request.getParameter("calStatus");

	String calAssoc=request.getParameter("calassoc");
    calAssoc=(calAssoc==null)?"":calAssoc;


    //to check if the id is to delete the visit(s), not the events
    String visitFlags = request.getParameter("visitFlags");

	//KM-#5949
	String usr = null;
	usr = (String) tSession.getValue("userId");

	String delMode=request.getParameter("delMode");
	if (delMode==null) {
		delMode="final";




%>
	<FORM name="deletevisitorevent" id="delvisitorevt" method="post" action="deletevisitseventsfromcal.jsp" onSubmit="if (validate(document.deletevisitorevent)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	<br><br>

	<TABLE width="98%" cellspacing="0" cellpadding="0" >
		<tr><td align="center">
		<b><%=MC.M_PlsEtrEsign_Del%><%--Please enter e-Signature to proceed with deletion*****--%> </b>
		</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>

		</table>

<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="delvisitorevt"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>


 	 <input type="hidden" name="delMode" value="<%=delMode%>">
  	 <input type="hidden" name="srcmenu" value="<%=src%>">

  	 <input type="hidden" name="tableName" value="<%=tableName%>">
  	 <input type="hidden" name="selectedTab" value="<%=selectedTab%>">
  	 <input type="hidden" name="duration" value="<%=duration%>">
  	 <input type="hidden" name="protocolId" value="<%=protocolId%>">
  	 <input type="hidden" name="calledFrom" value="<%=calledFrom%>">
  	 <input type="hidden" name="mode" value="<%=mode%>">
  	 <input type="hidden" name="calStatus" value="<%=calStatus%>">
   	 <input type="hidden" name="calassoc" value="<%=calAssoc%>">
   	 <input type="hidden" name="selStrs" value="<%=selStrs%>" size="150">
   	 <input type="hidden" name="visitFlags" value="<%=visitFlags%>" size="150">


	</FORM>
<%
	} else {
			String eSign = request.getParameter("eSign");
			String oldESign = (String) tSession.getValue("eSign");
			if(!oldESign.equals(eSign)) {
%>
	 		  <jsp:include page="incorrectesign.jsp" flush="true"/>
<%
			} else {

				StringTokenizer idStrs=new StringTokenizer(selStrs,",");
				int numIds=idStrs.countTokens();
				String[] strArrStrs = new String[numIds] ;


				//JM: 30Apr2008, added for, Enh. #C9
				String strVisitStrs = "";



				StringTokenizer flagStrs=new StringTokenizer(visitFlags,",");
				int numFlags=flagStrs.countTokens();
				String[] strArrFlags = new String[numFlags] ;


				for(int cnt=0;cnt<numIds;cnt++){

					strArrStrs[cnt] = idStrs.nextToken();
					strArrFlags[cnt] = flagStrs.nextToken();


					if (strArrFlags[cnt].equals("1")){
						strVisitStrs = strVisitStrs + strArrStrs[cnt] +",";
					}


				}

			StringTokenizer flagVisitStrs=new StringTokenizer(strVisitStrs,",");

			int numVisits=flagVisitStrs.countTokens();
			String[] strArrVisits = new String[numVisits] ;
			int childVisitCount = 0;



			for(int cnt=0;cnt<numVisits;cnt++){

			strArrVisits[cnt] = flagVisitStrs.nextToken();


			ProtVisitDao protVisitDao = new ProtVisitDao();
			childVisitCount = protVisitDao.getProtocolVisitChildCount(EJBUtil.stringToNum(strArrVisits[cnt]));
				if(childVisitCount > 0) {

				break;

				}



			}

			if(childVisitCount > 0) {
			%>
			<br><br><br><br><br><br><br>
			<table width=100%>
				<tr>
				<td align=center>

				<p class = "successfulmsg">
				 <%=MC.M_VisitsCntDel_ReferedMore%><%--Visit(s) Cannot be deleted, referred by one or more child visits*****--%></p>
				</td>
				</tr>
                                 <tr>
				<td align=center>
					<A type="submit" href="fetchProt.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&calassoc=<%=calAssoc%>"><%=LC.L_Back%></A>
				</td>
				</tr>

			</table>

			<%

			}else{


				//KM-#5949, Modified for INF-18183 ::: Ankit
				String moduleName = calledFrom.equals("P")? LC.L_Cal_Lib : LC.L_Study_Calendar;
				int i=eventdefB.deleteEvtOrVisits(strArrStrs,tableName, strArrFlags, EJBUtil.stringToNum(usr),AuditUtils.createArgs(session,"",moduleName));


%>

<br><br><br><br><br> <p class = "successfulmsg" align = center> <%=MC.M_VisitsOrEvents_DelSucc%><%--Visit(s)/Event(s) deleted successfully*****--%></p>
<META HTTP-EQUIV=Refresh CONTENT="1; URL=fetchProt.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&calassoc=<%=calAssoc%>">

<%	}//////////
	} //end esign
	} //end of delMode
}//end of if body for session
else{
%>
<jsp:include page="timeout.html" flush="true"/>
<%
}
%>

  <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

</DIV>

<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/>
</div>

</BODY>
</HTML>

