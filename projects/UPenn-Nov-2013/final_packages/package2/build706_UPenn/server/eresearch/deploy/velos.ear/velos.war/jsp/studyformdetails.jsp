<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%
boolean isIrb = "irb_form_tab".equals(request.getParameter("selectedTab")) ? true : false;
%>

<title><%=LC.L_Std_Form%><%-- <%=LC.Std_Study%> Form*****--%></title>

<SCRIPT>
function openUserSearch(formobj,colName) {
//alert(formobj);
//alert(colName);
  //  windowName=window.open("formusersearch.jsp?formobj="+formobj+"&colName="+colName,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
//	windowName.focus();
}
// Added the parameter 'formLibVerNumber' for to fix the bugzilla issue #2786
function openPrintWin(formId,filledFormId,studyId,formLibVerNumber) {
   windowName=window.open("formprint.jsp?formId="+formId+"&filledFormId="+filledFormId+"&studyId="+studyId+"&formLibVerNumber="+formLibVerNumber+"&linkFrom=S","Information1","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
   windowName.focus();
}

function openQueries(formId,filledFormId,studyId) {
	  windowName=window.open("addeditquery.jsp?studyId="+studyId+"&formId="+formId+"&filledFormId="+filledFormId+"&from=2","Information1","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
   windowName.focus();

}

function  openReport(formobj,reportId,reportName,repFilledFormId,act,displayMode){
	/*formobj.repId.value = reportId;
	formobj.repName.value = reportName;
	formobj.repFilledFormId.value = repFilledFormId;
	formobj.filterType.value = "A";*/

	formobj.action="repRetrieve.jsp?repId="+reportId+"&repName="+reportName+"&repFilledFormId="+repFilledFormId+"&filterType=A&displayMode="+displayMode;
	formobj.target="_new";
	formobj.submit();
	formobj.action=act;
	formobj.target="";
	}
function confirmBox(pageRight)
{
	if (f_check_perm(pageRight,'E') == true)
		{
			msg="<%=LC.L_Del_ThisResponse%>";/*msg="Delete this Response?";*****/

			if (confirm(msg))
			{
    			return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
}

</SCRIPT>

</head>

<%
String src= request.getParameter("srcmenu");
String selectedTab = StringUtil.htmlEncodeXss(request.getParameter("selectedTab"));
String studyId = request.getParameter("studyId");
String formIdStr=request.getParameter("formId");
formIdStr=(formIdStr==null)?"":formIdStr;
int formId = EJBUtil.stringToNum(formIdStr);
String from = "form";
String dformPullDown="";
String entryChar=request.getParameter("entryChar");
String formFillDt = request.getParameter("formFillDt");
String formPullDown = request.getParameter("formPullDown");
String fldMode = request.getParameter("fldMode");
String eSignRequired = "";

String calledFromForm = "";
       calledFromForm = request.getParameter("calledFromForm");

      if (StringUtil.isEmpty(calledFromForm))
      {
     	calledFromForm = "";
      }

   String showPanel= "";
       showPanel= request.getParameter("showPanel");

      if (StringUtil.isEmpty(showPanel))
      {
     	showPanel= "true";
      }

		 String formCategory = request.getParameter("formCategory");
		 String submissionType = request.getParameter("submissionType");

    if (StringUtil.isEmpty(formCategory))
			 {
			 	 formCategory="";
			 }

			 if (StringUtil.isEmpty(submissionType))
			 {
			 	 submissionType="";
			 }




   String outputTarget = ""; //if a target name is passed, open the new and edit page in this target
	   outputTarget = request.getParameter("outputTarget");

	 if (StringUtil.isEmpty(outputTarget))
	 {
	 	outputTarget = "";
	 }

	 String specimenPk = "";
    specimenPk = request.getParameter("specimenPk");

     if (StringUtil.isEmpty(specimenPk))
	 {
	 	specimenPk = "";
	 }


   if (calledFromForm.equals("") && showPanel.equals("true"))
   {

%>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
 <% }
 else
 { %>
 	<jsp:include page="include.jsp" flush="true"/>

 	<%
 }
 %>
<script>
	 checkQuote="N";
	 </script>
<%@page language = "java" import="java.util.*,com.velos.eres.business.common.FormLibDao,com.velos.eres.business.common.SaveFormDao,com.velos.eres.business.common.LinkedFormsDao,com.velos.eres.service.util.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.user.UserJB,java.util.*,com.velos.eres.business.common.CodeDao" %>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="lnkformB" scope="request" class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>
<jsp:useBean id="formLibB" scope="request" class="com.velos.eres.web.formLib.FormLibJB"/>
<jsp:useBean id="userSite" scope="page" class="com.velos.eres.web.userSite.UserSiteJB" /> <!--KM-->



<body style="overflow:auto;">
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

<br>
<%
if (calledFromForm.equals("") && showPanel.equals("true"))
   {
%>
<div class="BrowserTopn" id="divtab">

<% String includeTabsJsp = isIrb ? "irbnewtabs.jsp" : "studytabs.jsp"; %>
<jsp:include page="<%=includeTabsJsp%>" flush="true">
<jsp:param name="from" value="<%=from%>"/>
<jsp:param name="selectedTab" value="<%=selectedTab%>"/>
<jsp:param name="studyId" value="<%=studyId%>"/>
</jsp:include>
</div>
<div class="BrowserBotN BrowserBotN_S_3" id="div1">
<%
   }else{
%>
<div>
<%}%>
<%
HttpSession tSession = request.getSession(true);
 if (sessionmaint.isValidSession(tSession))
 {
	 //EDC_AT4 extn
	 if (studyId == null || studyId.equals("null")){
	 	tSession.setAttribute("studyIdForFormQueries","");	
	 }else{
	 	tSession.setAttribute("studyIdForFormQueries",studyId);
	 }
 
 	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
	StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");

 	String findStr = "";
	StringBuffer sbuffer = null;
	int len = 0;
	int pos = 0;
	int pageRight = 0;
	int study_acc_form_right = 0,formRights=0;
	int study_team_form_access_right =0;

	String studyRoleCodePk;

	studyRoleCodePk = (String) tSession.getValue("studyRoleCodePk");

	if (StringUtil.isEmpty(studyRoleCodePk))
	{
		studyRoleCodePk = "";
	}

	Hashtable htMoreParam = new Hashtable();

	htMoreParam.put("teamRolePK",studyRoleCodePk);

    lnkformB.findByFormId((formId));
	String formLinkedFrom = lnkformB.getLnkFrom();
	String formDispType = lnkformB.getLFDisplayType();
	if (formDispType==null) formDispType="-";
	formDispType = formDispType.trim();

	if (formLinkedFrom==null) formLinkedFrom="-";

	if (StringUtil.isEmpty(outputTarget))
	 {


			if (formLinkedFrom.equals("S"))
			{

		    	pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMACC"));
		    } else if (formLinkedFrom.equals("A"))
			{
		    	 if (formDispType.equals("S")) { //specific study form

		    		pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMACC"));
				 } else if (formDispType.equals("SA")) { //all study forms
				 	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("STUDYFRMSACC"));
				 }
			}



			if ((stdRights.getFtrRights().size()) == 0){
			 	study_team_form_access_right= 0;
			}else{
			 study_acc_form_right = Integer.parseInt(grpRights.getFtrRightsByValue("STUDYFRMSACC"));
				 study_team_form_access_right=Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMACC"));
			}

			formRights = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMMANAG"));

	 }
	 /*
	 else //bypass all access rights
	 {
	 		pageRight = 7;
	 		study_acc_form_right = 7;
	 		study_team_form_access_right = 7;
	 		formRights = 7;
	 }
	 */
	 
	//Bug# 6638
	String accessRight = "";
	if(isAccessibleFor(study_team_form_access_right,'E')){
		if(isAccessibleFor(pageRight,'E')){
			accessRight="E";
		}else if(isAccessibleFor(pageRight,'V') || isAccessibleFor(pageRight,'N')){
			accessRight="V";
		}
	}else{
		if(isAccessibleFor(study_team_form_access_right,'E')){
			accessRight="E";
		}else if(isAccessibleFor(study_team_form_access_right,'V') || isAccessibleFor(study_team_form_access_right,'N')){
			accessRight="V";
		}
	}
	String strFormId = request.getParameter("formPullDown");
    String formLibVer = request.getParameter("formLibVer");
 // -- Start of Bug 14656 fix
   	// Make sure that formId is a valid Account Form ID
	//accFormId = request.getParameter("formId");
 
 	//int formIdstr=0;
	StringTokenizer strTokenizers = new StringTokenizer(strFormId,"*");
	//formIdstr = StringUtil.stringToNum(strTokenizers.nextToken());
	int studyFormIdInt = -1;
	if (!StringUtil.isEmpty(formIdStr)) {
		UserJB userB1 = (UserJB) tSession.getAttribute("currentUser");
		/*Comment for Bug# 16271*/
		/*LinkedFormsDao lnkFrmDao1 = lnkformB.getStudyForms(
				StringUtil.stringToNum((String)tSession.getAttribute("accountId")), 
				StringUtil.stringToNum((String)studyId),
				StringUtil.stringToNum((String)tSession.getAttribute("userId")),
				StringUtil.stringToNum(userB1.getUserSiteId()));*/
		/*Added for Bug# 16271 to get all the form of study*/				
		LinkedFormsDao lnkFrmDao1 = lnkformB.getStudyForms(
				StringUtil.stringToNum((String)tSession.getAttribute("accountId")),
				StringUtil.stringToNum((String)studyId),
				StringUtil.stringToNum((String)tSession.getAttribute("userId")),
				StringUtil.stringToNum(userB1.getUserSiteId()), 
		         		study_acc_form_right,  
		         		study_team_form_access_right, 
		         		isIrb, 
		         		submissionType, 
		         		formCategory);
		boolean isValidAccountFormId = false;
		try { studyFormIdInt = Integer.parseInt(formIdStr); } catch(Exception e) {}
		for (Object myFormId : lnkFrmDao1.getFormId()) {
			int myFormIdInt = ((Integer) myFormId).intValue();
			if (studyFormIdInt == myFormIdInt ) {
				isValidAccountFormId = true;
				break;
			}
		}
		if (!isValidAccountFormId) {
			pageRight = 0; // Invalid formId in request parameter => reject
		}
	}
	
	/*if (!StringUtil.isEmpty(formIdStr)) {
		UserJB userB1 = (UserJB) tSession.getAttribute("currentUser");
		LinkedFormsDao lnkFrmDao1 = lnkformB.getStudyLinkedForms(
				StringUtil.stringToNum((String)studyId));
		boolean isValidAccountFormId = false;
		try { studyFormIdInt = Integer.parseInt(formIdStr); } catch(Exception e) {}
		for (Object myFormId : lnkFrmDao1.getFormId()) {
			int myFormIdInt = ((Integer) myFormId).intValue();
			if (studyFormIdInt == myFormIdInt && (studyFormIdInt==formIdstr)) {
				isValidAccountFormId = true;
				break;
			}
		}
		if (!isValidAccountFormId) {
			pageRight = 0; // Invalid formId in request parameter => reject
		}
	}*/
	
	
	// -- End of Bug 14656 fix
	
	// -- Start of Bug 14659 fix
	// Make sure the formId and formLibVer combination is valid
	//int studyFormIdInt = formId;
	if (!StringUtil.isEmpty(formLibVer)) {
		FormLibDao formLibDao1 = new FormLibDao();
		int fkFormLib = formLibDao1.getFkFormLibForPkFormLibVer(StringUtil.stringToNum(formLibVer));
		if (studyFormIdInt > 0 && studyFormIdInt != fkFormLib) {
			pageRight = 0; // Invalid formId and formLibVer combination in request parameters => reject
		}
	}
	// -- End of Bug 14659 fix
	
	session.setAttribute("formQueryRight",""+pageRight);
	int orgRight  = 0;
	if (!StringUtil.isEmpty(formIdStr)) {
		UserJB userB1 = (UserJB) tSession.getAttribute("currentUser");
	 orgRight = userSite.getRightForUserSite(StringUtil.stringToNum((String)tSession.getAttribute("userId")), 
			 	StringUtil.stringToNum(userB1.getUserSiteId()));
	}
	
	if (! lnkformB.hasFormAccess(formId, StringUtil.stringToNum((String)tSession.getAttribute("userId"))))    			
	{
		//user does not have access rights to this form
		study_acc_form_right = 0;
		study_team_form_access_right = 0;
	}
	
   	int filledFormUserAccessInt = lnkformB.getFilledFormUserAccess(
    		StringUtil.stringToNum(request.getParameter("filledFormId")),
    		StringUtil.stringToNum(request.getParameter("formId")),
    		StringUtil.stringToNum((String)tSession.getAttribute("userId")) );
    
	if (pageRight>=4 && filledFormUserAccessInt > 0 || orgRight>0 )
	{

       String mode = request.getParameter("mode");
       SaveFormDao saveDao= new SaveFormDao();
       //Create the dropdown here for mahi enhancement
       ArrayList arrFrmNames = null;
		 ArrayList arrNumEntries = null;
 		 ArrayList arrFrmIds = null;
		 ArrayList arrEntryChar = null;
		 ArrayList arrFrmInfo = new ArrayList();
		 String frmName = "";
		  String numEntries = "";
		 String frmInfo= "";
		  String firstFormInfo = "";

  	    

	 	 String accId=(String)tSession.getAttribute("accountId");
		 int iaccId=EJBUtil.stringToNum(accId);
 	     String userId = (String) tSession.getValue("userId");
	     UserJB userB = (UserJB) tSession.getValue("currentUser");
   	     String siteId = userB.getUserSiteId();
	 	 int istudyId = EJBUtil.stringToNum(studyId);
		 boolean userHasFormAccess = false;

		 if (study_acc_form_right>=4 || study_team_form_access_right>=0)
		{
		 LinkedFormsDao lnkFrmDao = new LinkedFormsDao();
		 lnkFrmDao = lnkformB.getStudyForms(iaccId,istudyId, EJBUtil.stringToNum(userId), EJBUtil.stringToNum(siteId),
		  study_acc_form_right,  study_team_form_access_right,isIrb, submissionType, formCategory);


		 arrFrmIds = lnkFrmDao.getFormId();
		 arrFrmNames = lnkFrmDao.getFormName();
		 arrEntryChar = lnkFrmDao.getFormEntryChar();
		 arrNumEntries = lnkFrmDao.getFormSavedEntriesNum();

		 if (arrFrmIds.size() > 0) {
    		 if (strFormId==null) {
    		 	formId = EJBUtil.stringToNum(arrFrmIds.get(0).toString());
    			entryChar = arrEntryChar.get(0).toString();
    			numEntries = arrNumEntries.get(0).toString();
    			firstFormInfo = arrFrmIds.get(0).toString() + "*"+ entryChar + "*" + arrNumEntries.get(0).toString();
    		 }
    		 else {
    		 	 StringTokenizer strTokenizer = new StringTokenizer(strFormId,"*");
    		 	 if (calledFromForm.equals("") && showPanel.equals("true"))
    	 		 {

	    	 	     formId = EJBUtil.stringToNum(strTokenizer.nextToken());
	    			 entryChar = strTokenizer.nextToken();
	    		     numEntries = strTokenizer.nextToken();
	    			 firstFormInfo = strFormId;
	    		}
	    		else
    			 {
    			 	formId = EJBUtil.stringToNum(strTokenizer.nextToken());
    			 	entryChar = strTokenizer.nextToken();

    			 	lnkformB.findByFormId(formId );
		   		 	numEntries = lnkformB.getLfDataCnt();
    			 	//get the number of times the form was answered
    			 	//prepare strFormId again
    			 	strFormId = formId + "*" + entryChar + "*" + numEntries;
    			 	firstFormInfo = strFormId;

    			 }
    		 }

    		 for (int i=0;i<arrFrmIds.size();i++)
    		 {  //store the formId, entryChar and num Entries separated with a *
    		  	 frmInfo = arrFrmIds.get(i).toString() + "*"+ arrEntryChar.get(i).toString() + "*" + arrNumEntries.get(i).toString();
    		 	 arrFrmInfo.add(frmInfo);
    		 	 if ((Integer)arrFrmIds.get(i) == StringUtil.stringToNum(request.getParameter("formId"))) {
    		 		userHasFormAccess = true;
    		 	 }
    		 }


    	 	 dformPullDown = EJBUtil.createPullDownWithStrNoSelect("formPullDown",firstFormInfo, arrFrmInfo , arrFrmNames);
		 }
		 }
		 if (!userHasFormAccess) {
%>
<jsp:include page="accessdenied.jsp" flush="true"/>
<div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</body>
</html>
<%
			return;
		 }
    		 StringBuffer formBuffer = new StringBuffer(dformPullDown);

			 //submit the form when the user selects the form name
			 /* if (calledFromForm.equals(""))
			 {
			 	formBuffer.replace(0,7,"<SELECT onChange=\"document.studyform.submit();\"");
			 }
			 else
			 {
			 //	formBuffer.replace(0,7,"<SELECT DISABLED onChange=\"document.studyform.submit();\"");
			 }*/

			 dformPullDown = formBuffer.toString();
       //end mahi enhancements

       String filledFormId = request.getParameter("filledFormId");
       String formDispLocation = request.getParameter("formDispLocation");
       String formHtml = "";
	   String name="";
	   String formLibVerNumber="";
	   int fillFormStat=0;


	   formLibB.setFormLibId((formId));
	   formLibB.getFormLibDetails();
	   name=formLibB.getFormLibName();

	  eSignRequired =formLibB.getEsignRequired();

	   if (StringUtil.isEmpty(eSignRequired))
	   {
	   		eSignRequired = "1";
	   }

       String formStatus = formLibB.getFormLibStatus();
	   //Added by Manimaran for November Enhancement F6
	   //JM: 122206
	   //formLibVerNumber = formLibB.getFormLibVersionNumber(EJBUtil.stringToNum(formLibVer));
		//added by IA 11.05.2006
		FormLibDao fd = new FormLibDao();
		if ((formLibVer==null)|| formLibVer.equals("")){

			formLibVer = fd.getLatestFormLibVersionPK(EJBUtil.stringToNum(formIdStr));

		}
		//end added

	   formLibVerNumber = formLibB.getFormLibVersionNumber(EJBUtil.stringToNum(formLibVer));


	   CodeDao cdao = new CodeDao();
	   cdao.getCodeValuesById(EJBUtil.stringToNum(formStatus));
	   ArrayList arrSybType = cdao.getCSubType();
	   String statSubType = arrSybType.get(0).toString();
		%>
		<form name="studyform" id="studyForm" action="formfilledstudybrowser.jsp" method="post">
		<input type="hidden" name="srcmenu" value=<%=src%>>
     		<input type="hidden" name="selectedTab" value="<%=selectedTab%>">
     		<input type="hidden" name="studyId" value=<%=studyId%>>
     		<input type="hidden" id="formRights" value=<%=formRights%>>
    	<%  if ("true".equals(request.getParameter("irbReviewForm"))) { %>
    		<input type="hidden" name="irbReviewForm" value="true">
    	<%  }  %>

		<table width="98%" border="0" >
		<% if (calledFromForm.equals("") && showPanel.equals("true"))
		{
			%>

			<tr bgcolor="#dcdcdc">
			<td width="60%" align="right"><%=LC.L_Jump_ToForm%><%--Jump to Form*****--%>: <%=dformPullDown%></td>
			<td >
			<button type="submit" onclick="javascript:fn_nextpage(document.studyform.formPullDown.value) ;"><%=LC.L_Go%></button>
			</td></tr>
			<tr height="5"><td></td></tr>
			</table>
	<table width="100%" border="0">
			<% } %>
		<tr bgcolor="#dcdcdc">
			 <td width="50%"><!--KM-to fix the Bug 2809 -->
			 	 <b> <%=LC.L_Open_FormName%><%--Open Form Name*****--%>: </b><%=name%>&nbsp;&nbsp;
			 </td>
			 <td width="50%">
			 	 <%if (mode.equals("M")){%>

		 		<A onclick="openQueries(<%=formId%>,<%=filledFormId%>,<%=studyId%>)" href=#> <%=LC.L_AddOrEdit_Queries%><%--Add/Edit Queries*****--%></A>
			 &nbsp;
			 <%}%>


		 		<A onclick="openPrintWin(<%=formId%>,<%=filledFormId%>,<%=studyId%>,<%=formLibVerNumber%>)" href=#><img src="./images/printer.gif" title="<%=LC.L_Printer_FriendlyFormat%><%--Printer Friendly Format*****--%>" alt="<%=LC.L_Printer_FriendlyFormat%><%--Printer Friendly Format*****--%>" align="absmiddle" border="0"></img></A>&nbsp;
				<%if (entryChar.equals("E") && (!StringUtil.isEmpty(filledFormId)) ){%>
		 		 <A href="#" onclick="openReport(document.studyform,'101','Form Audit Trail','<%=filledFormId%>','formfilledstudybrowser.jsp?srcmenu=tdMenuBarItem5&page=1','')" title="<%=LC.L_ViewAud_Trial%><%--View Audit Trail*****--%>"><%=LC.L_Audit%><%--Audit*****--%></A>
		 		 &nbsp; <A href="#" onclick="openReport(document.studyform,'101','Track Changes','<%=filledFormId%>','formfilledstudybrowser.jsp?srcmenu=tdMenuBarItem5&page=1','<%=accessRight%>')" title="<%=LC.L_Track_Changes%><%--Track Changes*****--%>"><%=LC.L_Track_Changes%><%--Track Changes*****--%></A>

					&nbsp;<A onclick="return confirmBox(<%=pageRight%>);" href="formrespdelete.jsp?srcmenu=<%=src%>&calledFromForm=<%=calledFromForm%>&formCategory=<%=formCategory%>&submissionType=<%=submissionType%>&showPanel=<%=showPanel%>&selectedTab=<%=selectedTab%>&formId=<%=formId%>&formLibVer=<%=formLibVer%>&mode=M&studyId=<%=studyId%>&formDispLocation=S&filledFormId=<%=filledFormId%>&calledFrom=S&entryChar=<%=entryChar%>&formFillDt=<%=formFillDt%>&formPullDown=<%=formPullDown%>&outputTarget=<%=outputTarget%>"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0" /></A>
			<%}%>

			 </td>
		</tr>

		<!--Added by Manimaran for the November Enhancement F6-->
		<!--tr>
		<td width="40%">
			 	 <b> Version Number: <%=formLibVerNumber%></b>
		</td>
		</tr-->
		</table>
		</form>
		<script>
				function fn_nextpage(val)
		{
				 document.studyform.submit();
		}
		</script>
			<%
       if (mode.equals("N"))
        {
			formHtml =  lnkformB.getFormHtmlforPreview((formId),htMoreParam);

        }
        else
        {
        	saveDao=lnkformB.getFilledFormHtml(EJBUtil.stringToNum(filledFormId),formDispLocation,"Y",htMoreParam);
			formHtml =  saveDao.getHtmlStr();
        }

		//Replace Submit image with button
		findStr ="<input id=\"submit_btn\" border=\"0\" align=\"absmiddle\" src=\"../images/jpg/Submit.gif\" type=\"image\">";
	    len = findStr.length();
	    pos = formHtml.lastIndexOf(findStr);

	    if (pos > 0)
	    {
		    sbuffer = new StringBuffer(formHtml);
		    if(pageRight==4  || (pageRight == 6 && mode.equals("N")) || (pageRight < 6 && mode.equals("M")) || (entryChar.equals("O")&& mode.equals("M")) || (statSubType.equals("L")) ||(saveDao.isLocked()) ){
		    	sbuffer.replace(pos,pos+len,"");
		    }else{
		    	sbuffer.replace(pos,pos+len,"<button type='submit'>"+LC.L_Submit+"</button>");
		    }
		    formHtml = sbuffer.toString();
	    }
	    
	    findStr = "name=\"eSign\"";
	    pos = formHtml.lastIndexOf(findStr);
	    if(pos > 0)
	    {
	    	sbuffer = new StringBuffer(formHtml);
	    	sbuffer.insert(pos-1," autocomplete=\"off\" ");
	    	formHtml = sbuffer.toString();  	    	
	    }
	    
	    findStr ="cannot be greater than today's date";
  	    len = findStr.length();
  	    pos = formHtml.lastIndexOf(findStr);

  	    if (pos > 0)
  	    {
  		    sbuffer = new StringBuffer(formHtml);
			sbuffer.replace(pos,pos+len,"cannot be a future date.");
  		    formHtml = sbuffer.toString();
  	    }
		
		//in case of view right, remove the submit button
		//in case of lockdown status, remove the submit button
		//in case of lockdown status, remove the submit button
		if(pageRight==4  || (pageRight == 6 && mode.equals("N")) || (pageRight < 6 && mode.equals("M")) || (entryChar.equals("O")&& mode.equals("M")) || (statSubType.equals("L")) ||(saveDao.isLocked()) ){

		    // Disable the Form tag for issue 4632
		    formHtml = formHtml.replaceAll("onSubmit=\"return validationWithAjax\\(\\)\"","")
			   .replaceAll("action=\"updateFormData.jsp\"","action=\"javascript:void(0)\"")
			   .replaceAll("formobj.action=\"querymodal.jsp\"","formobj.action=\"javascript:void(0)\"");

			//Added by Manimaran for the issue 2941
			sbuffer = new StringBuffer(formHtml);
			sbuffer.append("<table><tr><td>"+LC.L_Form_VersionNumber+": "+formLibVerNumber+"</td></tr></table>");/*sbuffer.append("<table><tr><td>Form Version Number: "+formLibVerNumber+"</td></tr></table>");*****/
			sbuffer.append("<input type=\"hidden\" id=\"formId\" name=\"formId\" value='"+formId+"'/>");
			formHtml = sbuffer.toString();

		} else {

		   findStr = "</Form>";
		   len = findStr.length();
		   pos = formHtml.lastIndexOf(findStr);
		   sbuffer = new StringBuffer(formHtml);
		   StringBuffer paramBuffer = new StringBuffer();

		  //  by salil on 13 oct 2003 to see if the check box type multiple choice exists
    	  //  or not and to handle the case
	     // when no option is selected in a check box type multiple choice the element name does not
	  	 // be  carried forward to the next page hence the "chkexists"  variable is used to see
		 // if the page has a multiple choice field or not .

		   String chkexists ="";
		   String tochk="type=\"checkbox\"";
		   if(formHtml.indexOf(tochk) == -1 )
		   {
		     chkexists ="N";
		   }
		   else
		   {
		   	 chkexists ="Y";
		   }

		   paramBuffer.append("<input type=\"hidden\" name=\"formDispLocation\" value='"+formDispLocation+"'/>");
		   paramBuffer.append("<input type=\"hidden\" name=\"formStudy\" value='"+studyId+"'/>");
		   paramBuffer.append("<input type=\"hidden\" name=\"formFillMode\" value='"+mode+"'/>");
		   paramBuffer.append("<input type=\"hidden\" id=\"formId\" name=\"formId\" value='"+formId+"'/>");
		   paramBuffer.append("<input type=\"hidden\" name=\"srcmenu\" value='"+src+"'/>");
		   paramBuffer.append("<input type=\"hidden\" name=\"formFillDt\" value='"+formFillDt+"'/>");
		   paramBuffer.append("<input type=\"hidden\" name=\"formPullDown\" value='"+formPullDown+"'/>");
		   paramBuffer.append("<input type=\"hidden\" name=\"selectedTab\" value='"+selectedTab+"'/>");
		   paramBuffer.append("<input type=\"hidden\" name=\"calledFromForm\" value='"+calledFromForm+"'/>");
           paramBuffer.append("<input type=\"hidden\" name=\"fldMode\" value='"+fldMode+"'/>");
			paramBuffer.append("<input type=\"hidden\" name=\"showPanel\" value='"+showPanel+"'/>");
			paramBuffer.append("<input type=\"hidden\" name=\"submissionType\" value='"+submissionType+"'/>");
			paramBuffer.append("<input type=\"hidden\" name=\"formCategory\" value='"+formCategory+"'/>");
			paramBuffer.append("<br><table><tr><td>Form Version Number: "+formLibVerNumber+"</td></tr></table>");
			paramBuffer.append("<input type=\"hidden\" name=\"outputTarget\" value='"+outputTarget+"'/>");
			paramBuffer.append("<input type=\"hidden\" name=\"specimenPk\" value='"+specimenPk+"'/>");
			paramBuffer.append("<input type=\"hidden\" name=\"eSignRequired\" value='"+eSignRequired+"'/>");


			if (!mode.equals("N"))
		    {
		    	paramBuffer.append("<input type=\"hidden\" name=\"formFilledFormId\" value='"+filledFormId+"'/>");
		    	paramBuffer.append("<input type=\"hidden\" name=\"formLibVer\" value='"+formLibVer+"'/>");

			}
			//also append the paramBuffer
		   sbuffer.replace(pos,pos+len,"<input type=\"hidden\" name=\"mode\" value='"+mode+"'/> <input type=\"hidden\" name=\"chkexists\" value='"+chkexists+"'/>"+paramBuffer.toString()+" </Form>");

		   //sbuffer.replace(pos,pos+len,"<input type=\"hidden\" name=\"mode\" value='"+mode+"'/>  </Form>");
		   formHtml = sbuffer.toString();
		}
 	%>

  	<%=formHtml%>

 <%

	} //end of if body for page right

	else
	{
%>
    <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

  	} //end of else body for page right

  } //end of if for session

 else
 {  //else of if body for session
  %>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
 %>
<br>
<div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
</div>
<%if (calledFromForm.equals("")&& showPanel.equals("true") )
   {
%>

<div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</div>


<%
	}
%>
	<script>
elementOverride=(document.getElementById("override_count"));
if (!(typeof(elementOverride)=="undefined") && (elementOverride!=null) ) {
if ((document.getElementById("override_count").value)>0) setDisableSubmit('false');
}
linkFormSubmit('fillform');</script>

<%
if ( eSignRequired.equals("0") )
{
  %>
  	<script>
	  	elemeSign = (document.getElementById("eSign"));
		elemeSignLabel = (document.getElementById("eSignLabel"));

		if (!(typeof(elemeSign)=="undefined") && (elemeSign!=null) ) {
			elemeSign.style.visibility = "hidden";
			elemeSign.value="11";
		}

		if (!(typeof(elemeSignLabel)=="undefined") && (elemeSignLabel!=null) ) {

			elemeSignLabel.style.visibility = "hidden";
		}
	</script>
  <%

}
%>


</body>
</html>
<%!
private boolean isAccessibleFor(int pageRight, char type) {
    switch(type) {
    case 'V': return ((pageRight & 0x0004) >> 2) == 1;
    case 'E': return ((pageRight & 0x0002) >> 1) == 1;
    case 'N': return  (pageRight & 0x0001) == 1;
    }
    return false;
}
%>
