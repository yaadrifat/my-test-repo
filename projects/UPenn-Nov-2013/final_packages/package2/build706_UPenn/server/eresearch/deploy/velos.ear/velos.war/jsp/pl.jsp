<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Pcol_Cal%><%--Protocol calenders*****--%></title>
<%@ page language = "java" import = "com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.*"%>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<META HTTP-EQUIV="no-cache">

</head>
<Script>

function openName(id)
{
	var src = document.group.src.value;
	var mode =document.group.mode.value;
  group.action= "protocolcalender.jsp?mode="+mode +"&protocolId=" +id +"&srcmenu="+src ;
  group.submit();
}



</Script>
<% String src;
src= request.getParameter("src");
%>
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   
<body>
<br>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="ctrldao" scope="request" class="com.velos.esch.business.common.EventdefDao"/>
<% int accountId=0;  
   int pageRight = 0;
   HttpSession tSession = request.getSession(true); 
%>
<DIV class="browserDefault" id="div1">
	<%
	if (sessionmaint.isValidSession(tSession))
	{

%>
	
	<form name="protocol" method=post action="pl.jsp">
	<input type=hidden name=src value=<%=src%>>
	<input type=hidden name=mode value="M">
	<% 

	   	String acc = (String) tSession.getValue("accountId");
	   	String uName = (String) tSession.getValue("userName");
	   	String study = (String) tSession.getValue("studyId");
	   	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
		  pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));
	   	

	   	accountId = EJBUtil.stringToNum(acc);


	   	String grpName = null;
	   	String grpDesc = null;

	   	int counter = 0;
	   	if (pageRight > 0 )
		{
			Vector vec = new Vector();
			
			Column pkGrp = new Column("Protocol Id");
			pkGrp.hidden = true;
			
			vec.add(pkGrp);
			
			
			Column pkProtocolName = new Column("Name");
			pkProtocolName.funcName = "openName";
			pkProtocolName.passAsParameter = true;
			pkProtocolName.width = "25%"	;
			pkProtocolName.align = "center"	;		
			vec.add(pkProtocolName);
			
			Column protocolDesc1 = new Column("Description");
			protocolDesc1.width = "45%" ;
			protocolDesc1.align = "center"	;
			vec.add(protocolDesc1);
			
			Column protocolStatus = new Column("Status");
			protocolStatus.hidden = true;
			vec.add(protocolStatus);
			

			
			String sSQL = "select EVENT_ID, "
					+ "CHAIN_ID, "
					+ "EVENT_TYPE, " 												
					+ "NAME, "												
					+ "NOTES, "												
					+ "COST, "
					+ "COST_DESCRIPTION, "																		
					+ "DURATION, "												
					+ "USER_ID, "												
					+ "LINKED_URI, "
					+ "FUZZY_PERIOD, "												
					+ "MSG_TO, "
					+ "STATUS, "
					+ "DESCRIPTION, "
					+ "DISPLACEMENT "													
					+ "FROM ESCH.EVENT_DEF "
					+ "WHERE  USER_ID="+accountId+" AND " 
					+ "TRIM(UPPER(EVENT_TYPE))= "+"'"+"P"+"'"+" ORDER BY NAME";
	
			String title =MC.M_PcolCalCurListed_UsePcol/*"The following are the Protocol Calendars currently listed in your Library. Select the Calendar that you wish to use in your Protocol or you may create a new Calendar."*****/;
			
			String srchType=request.getParameter("srchType");
			String sortType=request.getParameter("sortType");
			String sortOrder=request.getParameter("sortOrder");

			request.setAttribute("sSQL",sSQL);

			request.setAttribute("srchType",srchType);
			request.setAttribute("vec",vec);
			request.setAttribute("sTitle",title);

			String s_scroll=request.getParameter("scroll");
			request.setAttribute("scroll",s_scroll);
			request.setAttribute("sortType",sortType);
			request.setAttribute("sortOrder",sortOrder);
		%>
			<P class = "userName"> <%= uName %> </P>
			<P class="sectionHeadings"> <%=LC.L_MngAcc_Grp%><%--Manage Account >> Groups*****--%></P>
			
			<br>
 		<table width="100%" cellspacing="0" cellpadding="0" border="0" >
			<tr> 
			    <td width = "50%"> 
			        <P class="defComments"><%=MC.M_PcolCalCurListed_UsePcol%><%--The following are the Protocol Calendars currently listed in your Library. Select the Calendar that you wish to use in your Protocol or you may create a new Calendar.*****--%></P>
			    </td>
			    <td width="50%" align="right"> 
				<p>
 				<A href="protocolcalendar.jsp?srcmenu=<%=src%>&mode=N&selectedTab=1" onClick="return f_check_perm(<%=pageRight%>,'N')"><%=MC.M_Create_ANewCal%><%--Create a New Calendar*****--%></A>
			  	</p>
			    </td>
		    </tr>
		</table>

			<jsp:include page="result.jsp?jsFile=searchJS" flush="true"></jsp:include> 
	</form>
				<%	
		} //end of if body for page right
		else
		{
		%>
			<jsp:include page="accessdenied.jsp" flush="true"/>
		<%
		} //end of else body for page right
	}//end of if body for session
	else
	{
	%>
		<jsp:include page="timeout.html" flush="true"/>
	<%
	}
	%>
	<div> 
    	<jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
	</DIV>
<div class ="mainMenu" id="emenu"> 
  	<jsp:include page="getmenu.jsp" flush="true"/>
</div>
</body>

</html>
