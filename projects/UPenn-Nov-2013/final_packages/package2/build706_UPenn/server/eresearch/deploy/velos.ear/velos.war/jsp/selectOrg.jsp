<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<title><%=LC.L_Select_Org%><%--Select Organization*****--%></title>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
function removeAll(formobj)
{
	openerform=formobj.openerform.value;
	
	//Added by Manimaran to fix the Bug 2409.
	formRows = formobj.formRows.value;
	counter = formobj.counter.value;
	
	if (openerform=="selectForms"){
	     //km	 
             if(formRows==1){
		 window.opener.document.selectForms.formOrgIds.value="";	  
		 window.opener.document.selectForms.formOrg.value="";
	     }
	     else {
	     	 window.opener.document.selectForms.formOrgIds[counter].value="";	  
		 window.opener.document.selectForms.formOrg[counter].value="";
	     }
	   
	} else {
	 	window.opener.document.forms[openerform].selOrgIds.value='';
		window.opener.document.forms[openerform].selOrg.value='';
		   }
	 self.close();   

}

	function getOrganization(formobj)
	{
	selIds = new Array();
	selOrg = new Array(); //array of selected organization
	totrows = formobj.totalrows.value;
	checked = false;
	var k=0;
	if (totrows==1) {
		if (formobj.chkOrg.checked) {
			checked=true;
		}else {
			checked=false;
		}
	} else {
		for (i=0;i<totrows;i++) {
			if (formobj.chkOrg[i].checked) {
				checked=true;
				break;
			} else {
				checked=false;
			}		
		}		
	}
	
	if (!checked) {
		alert("<%=MC.M_Selc_AnOrg%>");/*alert("Please select an Organization.");*****/
		return false;
	}	
		
	if (totrows==1) {
		if (formobj.chkOrg.checked) {
			selValue = formobj.chkOrg.value;
			pos = selValue.indexOf("*");
			selIds[0] = selValue.substring(0,pos);
			selOrg[0] = selValue.substring(pos+1);
			
		}
	} else {
		j=0;
		for (i=0;i<totrows;i++) {
			if (formobj.chkOrg[i].checked) {
				
				selValue = formobj.chkOrg[i].value;
				pos = selValue.indexOf("*");
				selIds[j] = selValue.substring(0,pos);
				selOrg[j] = selValue.substring(pos+1);
				j++;
				var len=j;
			} 	
		}
				
	}

	openerform = formobj.openerform.value;
	counter = formobj.counter.value;
	formRows = formobj.formRows.value;	
   if (document.layers) {
	
   if (openerform=="null") { 
	 
      window.opener.document.div1.document.formlib.selOrgIds.value = selIds;
	  window.opener.document.div1.document.formlib.selOrg.value = selOrg;
   } else if (openerform=="selectForms") {
	  
      if (formRows==1) {//check if no. of rows is 1 or more
		  window.opener.document.div1.selectForms.formOrgIds.value = selIds;
	      window.opener.document.div1.document.selectForms.formOrg.value = selOrg;
	 } else {
		 window.opener.document.div1.selectForms.formOrgIds[counter].value = selIds;
	      window.opener.document.div1.document.selectForms.formOrg[counter].value = selOrg;	 
	 }   
   }
   else 
	{   //form is passed as a string
		 eval("window.opener.document.div1.document."+openerform+".selOrgIds.value='"+selIds+"'");
		 eval("window.opener.document.div1.document."+openerform+".selOrg.value='"+selOrg+"'");			   		
	}   
} else {
   if (openerform=="null") { 
	   
	  window.opener.document.formlib.selOrgIds.value=selIds;
	  window.opener.document.formlib.selOrg.value=selOrg;
   } else if (openerform=="selectForms") {
	   if (formRows==1) {
		  window.opener.document.selectForms.formOrgIds.value=selIds;
		  window.opener.document.selectForms.formOrg.value=selOrg;
	   } else {
		  window.opener.document.selectForms.formOrgIds[counter].value=selIds;
		  window.opener.document.selectForms.formOrg[counter].value=selOrg;	   
	   } 
   }
   else 
	 { 
	 
             //form is passed as a string
	     eval("window.opener.document."+openerform+".selOrgIds.value='"+selIds+"'" ) ;
		 eval("window.opener.document."+openerform+".selOrg.value='"+selOrg+"'" ) ;		  
	 }	   
}

self.close();
	}
	
	
	
	</SCRIPT>

<!--Modified by Amarnadh for issue #3231 -->
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.StringUtil,com.velos.eres.service.util.EJBUtil,com.velos.eres.business.common.UserSiteDao,com.velos.eres.service.util.*"%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="siteB" scope="request" class="com.velos.eres.web.site.SiteJB"/>
<jsp:include page="include.jsp" flush="true"/>
<%
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
if (agent1 != null && agent1.indexOf("MSIE") != -1) 
   	ienet = 0; //IE
else
	ienet = 1;

if (ienet == 0) {
%>
<body style="overflow:scroll">
<%} else {%>
<body>
<%}%>


<DIV class="popDefault" style="width:310px"> 
<P class="sectionHeadings"> <%=MC.M_FrmLib_SelOrg%><%--Form Library >> Select Organization*****--%> </P>
<p class="defcomments"><A href="#" onClick="removeAll(document.org)"><%=MC.M_Rem_SelcOrg%><%--Remove Already Selected Organization*****--%></A></p>
<%
	HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession))	{
 %>
<jsp:include page="sessionlogging.jsp" flush="true"/> 

<%
  		String selectOrgs=request.getParameter("selectOrgs");//km
		selectOrgs=StringUtil.decodeString(selectOrgs); //Amarnadh
		
		//used when called from linkformstostudy.jsp
  		String openerform = request.getParameter("openerform");
		//out.println("openerform::"+openerform);
  		String counter = request.getParameter("counter");
  		String formRows = request.getParameter("formRows");
								 	
		int accId=0;
	    	accId =   EJBUtil.stringToNum((String) (tSession.getValue("accountId")));

		ArrayList arrOrg=null;
		ArrayList arrSiteIds=null;
		String orgName="";
		String orgId="";
		SiteDao siteDao=new SiteDao();
		siteDao.getSiteValues(accId);
		arrOrg=siteDao.getSiteNames();
		arrSiteIds=siteDao.getSiteIds();
		int orgLen=arrOrg.size();
		%>
<Form  name="org" id="selOrgForm" method="post" action="" onsubmit="if (getOrganization(document.org)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
   <input type="hidden" name="openerform" value="<%=openerform%>">
   <input type="hidden" name="formRows" value="<%=formRows%>">   
   <input type="hidden" name="counter" value="<%=counter%>">     
    
    <table width="100%" cellspacing="2" cellpadding="0" border=0 >
      <tr> 
		<th width="50%"><%=LC.L_Organization_Name%><%--Organization Name*****--%></th>
			      <th width="10%"></th>
      </tr>
		<%
		boolean flag=false;
		for(int i=0;i<orgLen;i++)
			{
			orgName = arrOrg.get(i).toString();
			orgId=arrSiteIds.get(i).toString();
			
			if(i%2==0)
			{%>

          <tr class=browserEvenRow>
			<%}
			   else
			{%>
				<tr class=browserOddRow>
			<%}%>
			<td><%=orgName%></td>
			<% 
		       	   //km
			  String selectOrgName="";
			  StringTokenizer st = new StringTokenizer(selectOrgs,",");
			  while (st.hasMoreTokens()) {
			     selectOrgName=st.nextToken();
			     if(orgId.equals(selectOrgName)){ //KM
			        flag=true;
			     break;
			     }
			     else
			       flag=false;
			  }
			 
			 if (flag==true){
			 //km
			 %>
			<td><input type="checkbox" checked name="chkOrg" value="<%=orgId%>*<%=orgName%>"></td>
			<%}else{%>
		    <td><input type="checkbox" name="chkOrg" value="<%=orgId%>*<%=orgName%>"></td>
		    <%}%>
		   </tr>
		<%	
		}%>
		<tr height=10>
		<td></td>
		</tr>
	<tr align="right">
    <td align="right" colspan="2">
	<jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="N"/>
			<jsp:param name="formID" value="selOrgForm"/>
			<jsp:param name="showDiscard" value="N"/>
	</jsp:include>
    </td>
	</tr>
    </table>
	<Input type="hidden" name="checkedrows" value=0>
	<Input type="hidden" name="totalrows" value=<%=orgLen%> >
</Form>

<%
}//end of if body for session
else
{
%>
  <jsp:include page="timeout.html" flush="true"/> 
  <%
}
%>

<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</div>
</body>

</html>

