<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>

<HEAD>
<%@ page import="com.velos.eres.service.util.LC"%>
<title><%=LC.L_Std_SearchResults%><%--<%=LC.Std_Study%> Search Results*****--%></title>

<Link Rel=STYLESHEET HREF="common.css" type=text/css>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</HEAD>


<SCRIPT>
function register() {

window.opener.document.location = "register.jsp"
      window.close()

;}
</SCRIPT>


<%--

<jsp:include page="homepanel.jsp" flush="true">



</jsp:include>

--%>

<BODY style="overflow:scroll;">

<% response.setHeader("Cache-Control", "max-age=1, must-revalidate");%>

<jsp:useBean id="studyB" scope="session" class="com.velos.eres.web.study.StudyJB"/>
<jsp:useBean id="grpRights" scope="session" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.study.*,com.velos.eres.web.user.UserJB,com.velos.eres.service.util.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.ulink.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB"%>
<%--

<DIV class="browserDefault" id="div1">

--%>
<%

StudyDao studyDao = new StudyDao();

String search = request.getParameter("search");

String searchOn = request.getParameter("searchOn");

String from = request.getParameter("from");

String criteria = "";

String userId = "";



HttpSession tSession = request.getSession(true);

if ( (sessionmaint.isValidSession(tSession) && from.equals("afterLogin")) || from.equals("homePage") )

{
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
	if(from.equals("afterLogin")) {

		userId = (String) tSession.getValue("userId");

	}





if(searchOn.equals("keyword")) {

	studyDao = studyB.getStudyValuesKeyword(search);

	criteria = LC.L_Keyword/*Keyword*****/;

}

if(searchOn.equals("title")) {

	studyDao = studyB.getStudyValuesTitle(search);

	criteria = LC.L_Study_Title/*LC.Std_Study +" Title"*****/;

}

if(searchOn.equals("author")) {

	studyDao = studyB.getStudyValuesAuthor(search);

	criteria = LC.L_Author_Name/*Author Name*****/;

}

if(searchOn.equals("tarea")) {

	studyDao = studyB.getStudyValuesTarea(search);

	criteria = LC.L_Therapeutic_Area/*Therapeutic Area*****/;

}



ArrayList studyIds 		= studyDao.getStudyIds();

ArrayList studyTitles 		= studyDao.getStudyTitles();

ArrayList studyObjectives 	= studyDao.getStudyObjectives();

ArrayList studySummarys 	= studyDao.getStudySummarys();

ArrayList studyDurs 		= studyDao.getStudyDurations();

ArrayList studyDurUnits 	= studyDao.getStudyDurationUnits();

ArrayList studyEstBgnDts 	= studyDao.getStudyEstBeginDates();

ArrayList studyActBghDts 	= studyDao.getStudyActBeginDates();

ArrayList studyParentIds 	= studyDao.getStudyParents();

ArrayList studyAuthors 		= studyDao.getStudyAuthors();

ArrayList studyNumbers 		= studyDao.getStudyNumbers();



int studyId = 0;

String studyTitle = "";

String studyAuthor = "";

String studyNumber = "";



int len = studyIds.size();

int counter = 0;



%>
<Form name="studybrowser" method="post" action="" onsubmit="">
  <table width="100%" cellspacing="0" cellpadding="0" border=0 >
    <tr >
      <td width = "100%">
        <P class = "defComments">
<%
   if(len <= 0) {
%>
	  <br>
          <%=MC.M_NoStdPublicView_RegSbcrb%><%--<B>None of the Studies </B> in this category are available for public viewing at the moment. You may register and subscribe to the Notification Service to receive e-mail announcements of studies released for public viewing in the future.*****--%> <BR>
          <BR>
          <A HREF="register.jsp" onClick="register()"><%=LC.L_Reg_Now%><%--Register Now*****--%></A>
<%
   } else {
	  Object[] arguments = {criteria,search}; 
%>
        
	    <%=VelosResourceBundle.getMessageString("M_YourSrch_YieldStd",arguments)%> <%--Your search criteria ("<%=criteria%>" Like "<%=search%>") yielded the following Studies*****--%>
<%
   }
%>
        </P>
      </td>
    </tr>
    <tr>
      <td height="10"></td>
      <td height="10"></td>
    </tr>
  </table>
  <% if (len > 0) {

%>
  <table width="98%" >
    <tr>
      <th width="20%"> <%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%> </th>
      <th width="60%"> <%=LC.L_Study_Title%><%--<%=LC.Std_Study%> Title*****--%> </th>
    </tr>
    <%

    for(counter = 0;counter<len;counter++)

	{	studyId = EJBUtil.stringToNum(((studyIds.get(counter)) == null)?"-":(studyIds.get(counter)).toString());



		studyAuthor = (studyAuthors.get(counter)).toString();

%>
    <%--

--%>
    <%

		studyTitle = ((studyTitles.get(counter)) == null)?"-":(studyTitles.get(counter)).toString();



		studyNumber = ((studyNumbers.get(counter)) == null)?"-":(studyNumbers.get(counter)).toString();



		if ((counter%2)==0) {

  %>
    <tr class="browserEvenRow">
      <%

		}

		else{

  %>
    <tr class="browserOddRow">
      <%

		}

  %>
      <td>
        <%

	if(from.equals("afterLogin")) {

%>
        <A Href="viewstudy.jsp?srcmenu=&studyId=<%=studyId%>"><%=studyNumber%></A>
        <%

} else {

%>
        <%=studyNumber%>
        <%}

%>
      </td>
      <td> <%=studyTitle%> </td>
      <% if(from.equals("afterLogin")) {

%>
      <td> <%=MC.M_Send_RequestFor%><%--Send a request for*****--%> <BR>
        <A href="updateNewMsgcntr.jsp?userId=<%=userId%>&studyId=<%=studyId%>&authorId=<%=studyAuthor%>&reqType=V&msgText=Request for View right"><%=LC.L_View%><%--View*****--%></A>
        <A href="updateNewMsgcntr.jsp?userId=<%=userId%>&studyId=<%=studyId%>&authorId=<%=studyAuthor%>&reqType=M&msgText=Request for Modify right"><%=LC.L_Modify%><%--Modify*****--%></A>
      </td>
      <%

} //end of if for checking the value of "from"

%>
    </tr>
    <%

		}

	} //end of if body for len>0

%>
  </table>
</Form>
<%



} //end of if body for session

else

{

%>
<jsp:include page="timeout.html" flush="true"/>
<%

}

%>
<%--

</div>

--%>
<DIV class="mainMenu" id = "emenu"> </DIV>
</body>

</html>



