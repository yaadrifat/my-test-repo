<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>

<%@page import="com.velos.eres.web.study.StudyJB"%><html>
<html>
<head>
<title><%=LC.L_Pat_StdStatus%><%--Patient Study Status*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,java.text.*" %>
<%@ page import="com.velos.eres.business.common.*,com.velos.eres.service.util.*,com.velos.eres.web.user.UserJB,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.business.patProt.impl.*,com.velos.eres.web.user.*,com.velos.eres.web.patFacility.*"%>

<%@ page import="com.velos.eres.service.util.*,com.velos.eres.web.user.ConfigFacade,com.velos.eres.web.user.ConfigObject, com.velos.eres.web.user.ConfigDetailsObject,com.velos.eres.business.common.CommonDAO"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:include page="include.jsp" flush="true"/>
</head>

<SCRIPT Language="javascript">

 window.name="patstudystatus";
 
 function fnClickAddAnother(e) {
	try {
        var thisTimeSubmitted = new Date();
    	document.getElementById("addOneMoreStat").value = "1";
        if (!lastTimeSubmitted) { return true; }
        if (!thisTimeSubmitted) { return true; }
        if ( thisTimeSubmitted - lastTimeSubmitted <= 1000 ) {
        	document.getElementById("addOneMoreStat").value = "0";
            return false;
        }
        lastTimeSubmitted = thisTimeSubmitted;
	} catch(e) {}
    return true;
}

 function fChangeReason(formobj)
 {

	formobj.action="patstudystatus.jsp" ;
	formobj.submit();
 }


 function fAddTxt(formobj)
 {


 if(formobj.patstatus.disabled == true)
   formobj.statusDisabled.value = "1";
 formobj.patstatus.value = formobj.patstatus.value;
	formobj.action="patstudystatus.jsp" ;
	formobj.submit();
 }
 function  validate(formobj){

	//................post 8.9.1 check for future date - depending on configuration
	 if (VAL_PATSTUDYSTAT_RESTRICT_FUTURE_STATDATE == 1 && isFutureDate(formobj.StatusDate.value)){
	 
		//JM: 29MAR2011: #5751,#5752 		
		if (!(validate_date(formobj.StatusDate))){		
			return false;
		} else{
			alert("<%=MC.M_RestrictFutureDate%>");
		}		
		formobj.StatusDate.focus();
		return false;
	 }
	 	 
 //prompt message when in patient demographics status is 'dead' and here patient status is changed from dead to some other status

 if(formobj.deadPerStat.value == formobj.perStatus.value && formobj.survival.value!=formobj.presurvival.value && formobj.presurvival.value!= "" && formobj.presurvival.value == formobj.deadPatStat.value)
 {
	 alert("<%=MC.M_PriorDead_DgraphicPat%>");/*alert("The prior status of 'Dead' entered here was transferred to the <%=LC.Pat_Patient%> Demographics page. Please make corresponding change to <%=LC.Pat_Patient%> Demographics, if required");*****/
 return false;
 }

//Added by Manimaran for September Enhancement S8.
  if (formobj.currentStat.checked) {
            formobj.currStat.value=1;
  } else {
	    formobj.currStat.value=0;
  }



// RK - 8/9/2005 Commented code
//	if (formobj.patstatus.value==formobj.prevStat.value && formobj.prevStat.value != ''){
//	alert("Please change the status");
//	return false;
//	}


    if (document.getElementById('mandstat')) {
	   if (!(validate_col('Status',formobj.patstatus))) return false;
	}

	if (document.getElementById('mandstatdt')) {
	    if (!(validate_col('Date',formobj.StatusDate))) return false;

	}
	

	////KM

	if (document.getElementById('pgcustomstat')) {
	   if (!(validate_col('Status',formobj.patstatus))) return false;
	}


	if (document.getElementById('pgcustomreason')) {
	   if (!(validate_col('Reason',formobj.patstatreason))) return false;
	}
	if (document.getElementById('pgcustomstatdt')) {
	    if (!(validate_col('Date',formobj.StatusDate))) return false;
	}

	if (document.getElementById('pgcustomnotes')) {
	    if (!(validate_col('Notes',formobj.Notes))) return false;
	}
	if (document.getElementById('pgcustomrandom')) {
	    if (!(validate_col('Randomization Number',formobj.patRandomizationNumber))) return false;
	}
	if (document.getElementById('pgcustomenrby')) {
	    if (!(validate_col('Enrolled By',formobj.patEnrollByName))) return false;
	}
	if (document.getElementById('pgcustomfollowup')) {
	    if (!(validate_col('Next Follow-up Date',formobj.followUpDate))) return false;
	}
	if (document.getElementById('pgcustomicver')) {
	    if (!(validate_col('Informed Consent Version Number',formobj.infConsentVerNumber))) return false;
	}
	if (document.getElementById('pgcustomscrnum')) {
	    if (!(validate_col('Screen Number',formobj.patScreenNumber))) return false;
	}

	if (document.getElementById('pgcustomscrby')) {
	    if (!(validate_col('Screened By',formobj.patScreenedByName))) return false;
	}

	if (document.getElementById('pgcustomscroutcome')) {
	    if (!(validate_col('Screening Outcome',formobj.patScreenOutcome))) return false;
	}


	if (document.getElementById('pgcustompatstdid')) {
	    if (!(validate_col('<%=LC.L_Patient_StudyId%>',formobj.patStudyId))) return false;
	}
	if (document.getElementById('pgcustomenrsite')) {
	    if (!(validate_col('Enrolling Site',formobj.enrollingSite))) return false;
	}
	if (document.getElementById('pgcustomassignedto')) {
	    if (!(validate_col('Assigned To',formobj.patAssignToName))) return false;
	}
	if (document.getElementById('pgcustomphysician')) {
	    if (!(validate_col('Physician',formobj.patPhysicianName))) return false;
	}
	if (document.getElementById('pgcustomtloc')) {
	    if (!(validate_col('Treatment Location',formobj.patTreatLoc))) return false;
	}
	if (document.getElementById('pgcustomtorg')) {
	    if (!(validate_col('Treating Organization',formobj.patorg))) return false;
	}
	if (document.getElementById('pgcustomdiscode')) {
	    if (!(validate_col('Disease Code',formobj.patDiseaseCode))) return false;
	}
	if (document.getElementById('pgcustomothrdiscode')) {
	    if (!(validate_col('Other Disease Code',formobj.patOtherDisCode))) return false;
	}
	/*if (document.getElementById('pgcustomtSpecSite')) {
	    if (!(validate_col('Specific Site1',formobj.patMoreDisCode1))) return false;
	}
	if (document.getElementById('pgcustomtSpecSite')) {
	    if (!(validate_col('Specific Site2',formobj.patMoreDisCode2))) return false;
	}*/
	if (document.getElementById('pgcustomflag')) {
	    if (!(validate_col('Evaluable Flag',formobj.evalFlag))) return false;
	}
	if (document.getElementById('pgcustomestat')) {
	    if (!(validate_col('Evaluable Status',formobj.evalStat))) return false;
	}
	if (document.getElementById('pgcustominestat')) {
	    if (!(validate_col('Inevaluable Status',formobj.inevalStat))) return false;
	}
	if (document.getElementById('pgcustomsurstat')) {
	    if (!(validate_col('Survival Status',formobj.survival))) return false;
	}
	if (document.getElementById('pgcustomdod')) {
	    if (!(validate_col('Date of Death',formobj.deathDate))) return false;
	}
	if (document.getElementById('pgcustomcod')) {
	    if (!(validate_col('Cause of Death',formobj.dthCause))) return false;
	}
	if (document.getElementById('pgcustomspeccause')) {
	    if (!(validate_col('Specify Cause',formobj.othDthCause))) return false;
	}
	if (document.getElementById('pgcustomdeathrel')) {
	    if (!(validate_col('Death Related to Study',formobj.dDthRelStd))) return false;
	}
	if (document.getElementById('pgcustomreasonof')) {
	    if (!(validate_col('Reason of Death Related to Study',formobj.othDthCauseRel))) return false;
	}

	////KM


	// Validation for 'Date of Birth' and 'Patient Status Date' by Gopu for Sept. Enhancement'05.

	if (formobj.StatusDate.value != null && formobj.StatusDate.value !=''){
	 		if (CompareDates(formobj.patdob.value,formobj.StatusDate.value,'>')){
	 			alert ("<%=MC.M_StatDtCnt_LtDob%>");/*alert ("Status Date cannot be less than Date of Birth");*****/
				formobj.StatusDate.focus();
				return false;
				}
			}

	 // Validation for 'Date of Birth' and 'Patient Death Date' by Gopu for Sept. Enhancement'05.

	  if (formobj.deathDate.value != null && formobj.deathDate.value !=''){
	  		if (CompareDates(formobj.patdob.value,formobj.deathDate.value,'>')){
	  			alert ("<%=MC.M_DthDtCntLess_DtOfBirth%>");/*alert ("<%=LC.Pat_Patient%> Death Date cannot be less than Date of Birth");*****/
				formobj.deathDate.focus();
				return false;
				}
			}

	 // Validation for 'Status Date' and 'Patient Death Date' by Gopu for Sept. Enhancement'05.

	 //Commented by Manimaran on 10July08 as per the Bug Description(3474)

	 /* if ((formobj.StatusDate.value != null && formobj.StatusDate.value !='') &&
	  		(formobj.deathDate.value != null && formobj.deathDate.value !='')){
	  		if (CompareDates(formobj.StatusDate.value,formobj.deathDate.value,'>')){
			  	alert ("<%=LC.Pat_Patient%> Status Date should not be greater than Death Date");
				formobj.deathDate.focus();
				return false;
				}
			}
	*/


	  //added for date field validation
	 if (!(validate_date(formobj.StatusDate))) return false

	  if(formobj.selStatSubType.value == 'followup')
	 {
	      if (!(validate_date(formobj.followUpDate))) return false
	 }
	 // Date of death is mandatory when Status is selected as 'dead'
	   if(formobj.deadPatStat.value == formobj.survival.value && formobj.deathDate.value == ''){

	   if(document.getElementById('deathHidden')) {
	   }
	   else
	   {
		   alert("<%=MC.M_Etr_DtOfDth%>");/*alert("Please enter Date of Death");*****/
	   }

	   if(formobj.deathDate.disabled == false) { //KM-24Jul08

	   if(document.getElementById('deathHidden')) {
		   return true;
	   }
	   else
	   {
		 formobj.deathDate.focus();
	   }
	   }

	   return false;
	   }
	 // Check for the death fields when dead is not selected in the survival status
	 if(formobj.deadPatStat.value != formobj.survival.value){

	  if (formobj.deathDate.value != null && formobj.deathDate.value != ''){
		  alert("<%=MC.M_EtrPatDtDth_StatDead%>");/*alert("You have entered <%=LC.Pat_Patient%>'s Date of Death. Please change the <%=LC.Pat_Patient%>'s Survival Status to 'Dead'");*****/
  	 return false;
	 }


	 if (formobj.dthCause.value != null && formobj.dthCause.value != '' )
	 {
		 alert("<%=MC.M_EtrDthCause_StatDead%>");/*alert("You have entered Cause of Death. Please change the <%=LC.Pat_Patient%>'s Survival Status to 'Dead'");*****/
  	 return false;
	 }

	 if(formobj.dDthRelStd.value != null && formobj.dDthRelStd.value != '')
	 {
		 alert("<%=MC.M_EntRelDthStd_PatSurv%>");/*alert("You have entered Death Related to Study. Please change the Patient's Survival Status to 'Dead'");*****/
  	 return false;

	 }

	 if(formobj.dthCause.value != null && formobj.dthCause.value != '')
	 {
	 	if(formobj.othDthCause.value != null && formobj.othDthCause.value != '')
		{
	   //alert("You have entered Reason of Death. Please change the <%=LC.Pat_Patient%>'s Survival Status to 'Dead'");
	   alert("<%=MC.M_EtrSpecifyCause_StatDead%>");/*alert("You have entered 'Specify Cause' . Please change the <%=LC.Pat_Patient%>'s Survival Status to 'Dead'");*****/

  	 return false;
	   }
	 }
	 if(formobj.dDthRelStd.value != null && formobj.dDthRelStd.value != '')
	 {
	 	if(formobj.othDthCauseRel.value != null && formobj.othDthCauseRel.value != '')
		{
	 		alert("<%=MC.M_EtrReasonDth_StatDead%>");/*alert("You have entered Reason of Death Related to Study. Please change the Patient's Survival Status to 'Dead'");*****/
  	 return false;
	   }
	 }

	}

	   if (!(validate_date(formobj.deathDate))) return false

	   //da= new Date();
	   //var str1,str2;
	   //str1=parseInt(da.getMonth());
	   //str2=str1+1;
	   //todate = str2+'/'+da.getDate()+'/'+da.getFullYear();

	   //JM: 20Apr2009, #4026
		todate = formatDate(new Date(),calDateFormat);

	   if (formobj.deathDate.value != null && formobj.deathDate.value !=''){
			if (CompareDates(formobj.deathDate.value,todate,'>')){
				alert ("<%=MC.M_DthDtNotGrt_Today%>");/*alert ("Date of Death should not be greater than Today's Date");*****/
				formobj.deathDate.focus();
				return false;
				}
			}



     var statusSubType;

		statusSubType = formobj.selStatSubType.value;

		if (statusSubType == 'enrolled')
		{
			if (!(validate_col('Enrolled By',formobj.patEnrollByName))) return false;

		}


	 if (document.getElementById('mandpatstdid')) {
		if (!(validate_col('<%=LC.L_Patient_StudyId%>',formobj.patStudyId))) return false;
	 }

	var mVal = document.getElementById("changeStatusMode").value;
	if (mVal){
	 	var fdaVal = document.getElementById("fdaStudy").value;
		if (fdaVal == "1" && mVal == "no"){
			if (  !(validate_col('<%=LC.L_ReasonForChangeFDA%>',formobj.remarks))) return false;
		}
	}

	 if (!(validate_col('e-Signature',formobj.eSign))) return false
     if(isNaN(formobj.eSign.value) == true) {
    	 alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/

	formobj.eSign.focus();
	return false;

	  }

   }
// CTRP-21708-05012013 @Ankit
function openWinICD(codestr,lkpId, lkpColumn){	    
	windowName=window.open("getlookup.jsp?viewId="+lkpId+"&form=status&dfilter=&keyword="+codestr+"|"+lkpColumn+"","Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=700,height=550 top=100,left=100");
    windowName.focus();
}
function showHide(){	
	if(document.getElementById("moreDiagCode1").style.display == 'block'){
		document.getElementById("moreDiagCode2").style.display='block';
		document.getElementById("addMore").style.display='none';
	}
	else{
		document.getElementById("moreDiagCode1").style.display='block';
		if(document.getElementById("moreDiagCode2").style.display == 'block'){
			document.getElementById("addMore").style.display='none';
		}
	}
}
function hideDiv(divId, field){	
	if(field.value == ''){
		divId.style.display='none';
		document.getElementById("addMore").style.display='';
		field.value='';
	}
	else{
		alert ("<%=MC.M_DEL_MORE_DIS_CODE%>");
	}
}

function selectAnatomicSite(formobj){
var name=formobj.anaSitename.value;
var id=formobj.anaSiteid.value;
var windowToOpen="multilookup.jsp?viewId=6011&form="+formobj.name+"&seperator=;&keyword=anaSitename|CODEDESC~anaSiteid|CODEPK|[VELHIDE]&dfilter=codelst_type='disease_site'"
	windowName = window.open(windowToOpen,'Lookup','resizable=1,status=0, width=850,height=550 top=100,left=100,menubar=no,scrollbars=1')
	windowName.focus();
};

</SCRIPT>

<SCRIPT language="JavaScript1.1">

function openwin12() {

      windowName=window.open("usersearchdetails.jsp?from=calenderstatus&fname=&lname=","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=500,height=400")
	  windowName.focus();

;}

</SCRIPT>

<jsp:useBean id="patB" scope="request" class="com.velos.eres.web.patStudyStat.PatStudyStatJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="personB" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="userSiteB" scope="page" class="com.velos.eres.web.userSite.UserSiteJB" />
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="patProtB" scope="page" class="com.velos.eres.web.patProt.PatProtJB" />
<jsp:useBean id="CommonB" scope="page" class="com.velos.eres.web.common.CommonJB" />



<%
int ienet = 2;

String agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1)
     ienet = 0; //IE
    else
	ienet = 1;
	if(ienet == 0) {
%>
<body style="overflow:scroll;">

<%
	} else {
%>
<body>
<%
	}
%>


<br>


<DIV class="popDefault" id="div1">

  <%
	HttpSession tSession = request.getSession(true);
  boolean hasAccess = false;
	if (sessionmaint.isValidSession(tSession))
	{
 %>
 	  <jsp:include page="sessionlogging.jsp" flush="true"/>
 <%
	  String defUserGroup = (String) tSession.getAttribute("defUserGroup");

		String ddEnrollingSite = "";

		String statid = "";
		String notes = "";
		String startDate = "";
		String patStudyStat = "";
		String studyId = "";
		String patId = "";
		String pcode="";
		String studyNum = "";
		String mode = "";
		String othDthCause = "";
		String othDthCauseRel = "";

		PatProtBean psk = null;

		String statusDisabledVal = request.getParameter("statusDisabled");
		if(statusDisabledVal == null)
		statusDisabledVal = "";

		String patAssignedTo = "";
		String patAssignedToName = "";
		String patPhysician = "";
		String patPhysicianName = "";
		String patTreatment = "";
		String patTreatLoc = "";
		String patSelOrg = "";//
		String dGrphReportable = "";
		
		String patDiseaseCode = "";
		String patOtherDisCode = "";
		String patMoreDisCode1 = "";
		String patMoreDisCode2 = "";
		String anaSiteid = "";
		String anaSitename = "";
		PatientDao ptDao=new PatientDao();
		String presurvival ="";

		String patStudyId = "";
		String oldPatStudyId = "";//KM
		String patRandomizationNumber = "";
		String infConsentVerNumber = "";
		String followUpDate = "";

		String patEnrolledBy = "";
		String patEnrolledByName = "";
		String enrollingSite = "";

		String screenNumber = "";
		String screenedBy = "";
		String screenedByName = "";
		String screeningOutcome = "";
		//KM
  		String currentStat="";
		String currentStatId=request.getParameter("currentStatId");//KM

		boolean withSelect = true;
        String evalFlag = "";
        String evalStat = "";
        String inevalStat = "";
        String survival = "";
        String dthCause= "";
        String dthRelStd = "";

        String dEvalFlag = "";
        String dEvalStat = "";
        String dInevalStat = "";
        String dSurvival = "";
        String dDthCause= "";
		String ddDthRelStd = "";
        String dDthRelStd = "";
		String deathDate = "";
		int patStudyIdAuto = 0;
		String readOnlyStr = "";
		SettingsDao settingsdao = new SettingsDao();
		ArrayList arSettingsValue = new ArrayList();


		String userIdFromSession = (String) tSession.getValue("userId");
		
		
		String statReason = "";
		int siteId=0;
		int personPK = 0;
		int orgRight=0;
		String patientDefSite = "";

		CodeDao cTreat = new CodeDao();
		cTreat.getCodeValues("treatloc");
		CtrlDao ctrlValue=new CtrlDao(); //CTRP-21708-05012013 @Ankit
		//String ctrlVal=ctrlValue.getControlValue("icd");
		ArrayList cValue = new ArrayList();
		ArrayList cCustom1 = new ArrayList();
		String lkpId = "";
		String lkpColumn = "";
		
		ctrlValue.getControlValues("icd");//km
		cValue=ctrlValue.getCValue();
		cCustom1=ctrlValue.getCCustom1();
		if((cValue!=null && cValue.size()>0) && (cCustom1!=null && cCustom1.size()>0))
		{
			lkpId = (String)cValue.get(0);
			lkpColumn = (String)cCustom1.get(0);
		}
		String treatPullDn = "";


        mode =request.getParameter("mode");
        String changeStatusMode = request.getParameter("changeStatusMode");


        patStudyIdAuto = StringUtil.stringToNum(request.getParameter("patStudyIdAuto"));

        //this param is only passed from patientstudies enrollment
		String patStudiesEnrollingOrg = request.getParameter("patStudiesEnrollingOrg");

		if (StringUtil.isEmpty(patStudiesEnrollingOrg) || patStudiesEnrollingOrg.equals("null"))
		{
	      patStudiesEnrollingOrg = "";
		}

		String acc = (String) tSession.getValue("accountId");
		int accId = StringUtil.stringToNum(acc);

		

		ConfigFacade cFacade=ConfigFacade.getConfigFacade();
		HashMap hashPgCustFld = new HashMap();

		ConfigDetailsObject cdoPgField = cFacade.populateObject(Integer.parseInt(acc), "patstudystatus");

		if (cdoPgField.getPcfField()!= null && cdoPgField.getPcfField().size() > 0) {
			for (int i=0;i<cdoPgField.getPcfField().size();i++){
				hashPgCustFld.put((String)cdoPgField.getPcfField().get(i), String.valueOf(i));
			}
		}



	  //KM

	  String disableStr ="";
	  String statAtt = "";
	  String reasonAtt = "";
	  String enrsiteAtt = "";
	  String tlocAtt = "";
	  String torgAtt = "";
	  String flagAtt = "";
	  String estatAtt = "";
	  String inestatAtt = "";
	  String surstatAtt ="";
	  String codAtt = "";
	  String deathRelAtt ="";
	  String scrOutcomeAtt ="";
	  
      if (hashPgCustFld.containsKey("patstatus")) {
			int fldNumStat = Integer.parseInt((String)hashPgCustFld.get("patstatus"));
			statAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumStat));
			if(statAtt == null) statAtt ="";
      }

	  if (hashPgCustFld.containsKey("reason")) {
			int fldNumReason = Integer.parseInt((String)hashPgCustFld.get("reason"));
			reasonAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumReason));
			if(reasonAtt == null) reasonAtt ="";
      }

	  if (hashPgCustFld.containsKey("enrollingsite")) {
			int fldNumSite = Integer.parseInt((String)hashPgCustFld.get("enrollingsite"));
			enrsiteAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumSite));
			if(enrsiteAtt == null) enrsiteAtt ="";
      }

	  if (hashPgCustFld.containsKey("treatmentlocation")) {
			int fldNumTloc = Integer.parseInt((String)hashPgCustFld.get("treatmentlocation"));
			tlocAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumTloc));
			if(tlocAtt == null) tlocAtt ="";
      }

	  if (hashPgCustFld.containsKey("treatingorg")) {
			int fldNumTorg = Integer.parseInt((String)hashPgCustFld.get("treatingorg"));
			torgAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumTorg));
			if(torgAtt == null) torgAtt ="";
      }
	  if (hashPgCustFld.containsKey("evaluableflag")) {
			int fldNumFlag = Integer.parseInt((String)hashPgCustFld.get("evaluableflag"));
			flagAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumFlag));
			if(flagAtt == null) flagAtt ="";
      }
	  if (hashPgCustFld.containsKey("evaluablestatus")) {
			int fldNumEstat = Integer.parseInt((String)hashPgCustFld.get("evaluablestatus"));
			estatAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumEstat));
			if(estatAtt == null) estatAtt ="";
      }
	  if (hashPgCustFld.containsKey("inevaluablestatus")) {
			int fldNumInstat = Integer.parseInt((String)hashPgCustFld.get("inevaluablestatus"));
			inestatAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumInstat));
			if(inestatAtt == null) inestatAtt ="";
      }
	  if (hashPgCustFld.containsKey("survivalstatus")) {
			int fldNumSurstat = Integer.parseInt((String)hashPgCustFld.get("survivalstatus"));
			surstatAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumSurstat));
			if(surstatAtt == null) surstatAtt ="";
      }
	  if (hashPgCustFld.containsKey("cod")) {
			int fldNumCod = Integer.parseInt((String)hashPgCustFld.get("cod"));
			codAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumCod));
			if(codAtt == null) codAtt ="";
      }
	  if (hashPgCustFld.containsKey("deathrelated")) {
			int fldNumDeathRel = Integer.parseInt((String)hashPgCustFld.get("deathrelated"));
			deathRelAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumDeathRel));
			if(deathRelAtt == null) deathRelAtt ="";
      }
	  if (hashPgCustFld.containsKey("screeningoutcome")) {
			int fldNumScroutcome = Integer.parseInt((String)hashPgCustFld.get("screeningoutcome"));
			scrOutcomeAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumScroutcome));
			if(scrOutcomeAtt == null) scrOutcomeAtt ="";
      }


	  //KM



		if (changeStatusMode==null) changeStatusMode = "-";

		if (mode == null) mode ="initial" ;



		personPK = StringUtil.stringToNum(request.getParameter("pkey"));
		studyId = request.getParameter("studyId");
		//Added by Manimaran for the Enhancement PS4.
		PatStudyStatDao ps = new PatStudyStatDao();
		ps.getPatStatHistory(personPK, StringUtil.stringToNum(studyId));

		StudyJB stdJb = new StudyJB();
		stdJb.setId(StringUtil.stringToNum(studyId));
		stdJb.getStudyDetails();
		
		int ctrpReportable = StringUtil.stringToNum(stdJb.getStudyCtrpReportable());
		
		ArrayList patStatPks = new ArrayList();
		ArrayList currentStats =  new ArrayList();
		patStatPks = ps.getPatientStatusPk();
		currentStats = ps.getCurrentStats();
  	  	int len= ps.getCRows();

		String currentVal="";
		for (int i=0;i<len;i++) {
		     currentVal = (currentStats.get(i) == null)?"":(currentStats.get(i)).toString();
		     if ( currentVal.equals("1")) {
			  currentStatId = (String)patStatPks.get(i).toString();
		     }
    		}

    	personB.setPersonPKId(personPK);
     	personB.getPersonDetails();
    	pcode = personB.getPersonPId();
		dthCause = personB.getPatDthCause();
		deathDate = personB.getPersonDeathDate();
		othDthCause = personB.getDthCauseOther();

    	siteId = StringUtil.stringToNum(personB.getPersonLocation());
    	patientDefSite = personB.getPersonLocation();

		int perStatus=  StringUtil.stringToNum(personB.getPersonStatus());
		// Get the codeId for patient status as 'dead'
		CodeDao cd = new CodeDao();
		int deadPerStat = cd.getCodeId("patient_status","dead");

		CodeDao cdOtherDth = new CodeDao();
		int otherDthCause = cd.getCodeId("pat_dth_cause","other");

		CodeDao cdOtherDthRel = new CodeDao();
		int otherDthCauseRel = cd.getCodeId("ptst_dth_stdrel","other");
		// Get the codeId for patient's survival status as 'dead'
		CodeDao cdPat = new CodeDao();
		int  deadPatStat = cdPat.getCodeId("ptst_survival","dead");

		//patSelOrg = request.getParameter("patOrg");//JM


    	orgRight = userSiteB.getMaxRightForStudyPatient(StringUtil.stringToNum(userIdFromSession), personPK , StringUtil.stringToNum(studyId) );
		if (orgRight > 0)
		{
			System.out.println("patient edit status" + orgRight);
			orgRight = 7;
		}


		if (mode.equals("initial"))
		{
			 statid = request.getParameter("statid");


			 if (! StringUtil.isEmpty(statid) )
			 {

				patB.setId(StringUtil.stringToNum(statid));
				patB.getPatStudyStatDetails();
				patId = patB.getPatientId();

				psk = patB.getPatProtObj();

				if (changeStatusMode.equals("no") )
				{
					notes = patB.getNotes();
					startDate = patB.getStartDate();
					patStudyStat = patB.getPatStudyStat();
					statReason = patB.getPatStudyStatReason();
					studyId = 	patB.getStudyId();
					patId = patB.getPatientId();

				}
				else //first status
				{
					studyId = request.getParameter("studyId");
				}


				if (changeStatusMode.equals("no"))
				{

					screenNumber = patB.getScreenNumber();
					if (screenNumber == null) screenNumber ="";
					screenedBy = patB.getScreenedBy();
					screeningOutcome = patB.getScreeningOutcome();
					currentStat = patB.getCurrentStat();
				}


				if (psk  != null)
				{

					patTreatLoc = psk.getTreatmentLoc();
					if (null == patTreatLoc) { patTreatLoc = ""; } 
					patSelOrg = psk.getpatOrg();//
					patPhysician = psk.getPatProtPhysician();
					dGrphReportable = psk.getdGrphReportable();
					
					patDiseaseCode  = psk.getPatDiseaseCode();
					patOtherDisCode = psk.getPatOtherDisCode();
					patMoreDisCode1 = psk.getPatMoreDisCode1();
					patMoreDisCode2 = psk.getPatMoreDisCode2();
					anaSiteid   = psk.getAnatomicSite();
					patAssignedTo = psk.getAssignTo();
					patStudyId = psk.getPatStudyId();
					oldPatStudyId = patStudyId;//KM
					evalFlag = psk.getPtstEvalFlag();
					if (null == evalFlag) { evalFlag = ""; } 

					evalStat = psk.getPtstEval();
					if (null == evalStat) { evalStat = ""; } 

					inevalStat = psk.getPtstInEval();
					if (null == inevalStat) { inevalStat = ""; } 

					presurvival = psk.getPtstSurvival();

					enrollingSite = psk.getEnrollingSite();
					patStudiesEnrollingOrg = ""; // we only need this param for first status

					if(presurvival == null)
					presurvival = "";
					survival = presurvival;

					dDthRelStd = psk.getPtstDthStdRel();
					deathDate = psk.getPtstDeathDate();

					othDthCauseRel = psk.getPtstDthStdRelOther();

					if (StringUtil.isEmpty(patPhysician))
						patPhysician = "";
					
					if (StringUtil.isEmpty(dGrphReportable))
						dGrphReportable = "1";
					if (StringUtil.isEmpty(patAssignedTo))
						patAssignedTo = "";
					
					if (StringUtil.isEmpty(patDiseaseCode))
						patDiseaseCode = "";
					if (StringUtil.isEmpty(patOtherDisCode))
						patOtherDisCode = "";
					if (StringUtil.isEmpty(patMoreDisCode1))
						patMoreDisCode1 = "";
					if (StringUtil.isEmpty(patMoreDisCode2))
						patMoreDisCode2 = "";
					if (StringUtil.isEmpty(anaSiteid))
					anaSiteid = "";
					anaSitename=ptDao.getAnatomicSites(StringUtil.integerToString(new Integer(psk.getPatProtId())));
					anaSitename=(anaSitename==null)?"":(anaSitename);
					if (!StringUtil.isEmpty(anaSiteid))
					   anaSiteid = anaSiteid.replace(',',';');
				}
				} //if prevStat is mot null
				else //first status
				{
					studyId = request.getParameter("studyId");
				}
			}
			else
			{

				if(statusDisabledVal.equals("1")){
				patStudyStat = request.getParameter("patstatusdisable");
				}
				else{
				patStudyStat = request.getParameter("patstatus");
				}

				statid = request.getParameter("prevStat");

				notes = request.getParameter("Notes");
				startDate = request.getParameter("StatusDate");
				statReason = request.getParameter("dpatstatreason");
				request.getParameter("dpatstatreason");
				patId = request.getParameter("patientId");
				studyId = request.getParameter("studyId");
				patPhysician = request.getParameter("patPhysician");
				dGrphReportable = request.getParameter("dGrphReportable");
				dGrphReportable = dGrphReportable==null?"0":dGrphReportable;
				patAssignedTo = request.getParameter("patAssignTo");
				patTreatLoc = request.getParameter("patTreatLoc");

				patSelOrg = request.getParameter("patOrg");//JM


				patStudyId = request.getParameter("patStudyId");
				oldPatStudyId = request.getParameter("oldPatStudyId");//KM

				evalFlag = request.getParameter("evalFlag");
    			evalStat = request.getParameter("evalStat");
    			inevalStat = request.getParameter("inevalStat");
    			survival = request.getParameter("survival");
				deathDate = request.getParameter("deathDate");
				
				patDiseaseCode  = request.getParameter("patDiseaseCode");
				patOtherDisCode = request.getParameter("patOtherDisCode");
				patMoreDisCode1 = request.getParameter("patMoreDisCode1");
				patMoreDisCode2 = request.getParameter("patMoreDisCode2");
				anaSiteid  = request.getParameter("anaSiteid");
				if (StringUtil.isEmpty(patDiseaseCode))
					patDiseaseCode = "";
				if (StringUtil.isEmpty(patOtherDisCode))
					patOtherDisCode = "";
				if (StringUtil.isEmpty(patMoreDisCode1))
					patMoreDisCode1 = "";
				if (StringUtil.isEmpty(patMoreDisCode2))
					patMoreDisCode2 = "";
				if (StringUtil.isEmpty(anaSiteid))
					anaSiteid = "";
				if (!StringUtil.isEmpty(anaSiteid))
				{
				anaSitename=request.getParameter("anaSitename");
				anaSitename=(anaSitename==null)?"":(anaSitename);
				if (!StringUtil.isEmpty(anaSiteid))
				   anaSiteid = anaSiteid.replace(',',';');
				 }
				//KM-24Jul08
				if (deathDate == null ) deathDate = "";
    			dthCause = request.getParameter("dthCause");
				dDthRelStd = request.getParameter("dDthRelStd");
				othDthCause = request.getParameter("othDthCause");
				othDthCauseRel = request.getParameter("othDthCauseRel");
				enrollingSite = request.getParameter("enrollingSite");

				if (changeStatusMode.equals("no"))
				{

					screenNumber  = request.getParameter("screenNumber");
					if (screenNumber == null) screenNumber ="";
					screenedBy  = request.getParameter("screenedBy");
					screeningOutcome  = request.getParameter("screeningOutcome");
				}

			}




		if (StringUtil.isEmpty(patStudyId)) 	 // in new mode
		{
			settingsdao = CommonB.retrieveSettings(StringUtil.stringToNum(studyId),3)	;


			ArrayList settingsKeyword=settingsdao.getSettingKeyword();
			int index = 0;
			String patStudyIdAutoStr  = "";
				if (settingsdao != null)
				{
					index=settingsKeyword.indexOf("PATSTUDY_ID_GEN");
					arSettingsValue = settingsdao.getSettingValue();
					if (arSettingsValue != null && arSettingsValue.size() > 0)
					{
						if (index>=0) patStudyIdAutoStr = (String) arSettingsValue.get(index);
						if (StringUtil.isEmpty(patStudyIdAutoStr))
						{
							patStudyIdAutoStr = "";
						}

						if (patStudyIdAutoStr.equals("auto"))
						{
							patStudyIdAuto = 1;
							patStudyId = "<system-generated>";

						}
						else
						{
							patStudyIdAuto = 0;
							patStudyId = pcode;

						}

					}
					else
					{
						patStudyIdAuto = 0;
						patStudyId = pcode;
					}

				}// end of if for settings dao
				else
					{
						patStudyId = pcode;
						patStudyIdAuto = 0;
					}
		}

		if (patStudyIdAuto == 1)
		{
			readOnlyStr = " READONLY ";
		}
		else
		{
			readOnlyStr = "";
		}

		if(tlocAtt.equals("1") || tlocAtt.equals("2"))
			treatPullDn = cTreat.toPullDown("patTreatLoc",StringUtil.stringToNum(patTreatLoc),"disabled");
		else
			treatPullDn = cTreat.toPullDown("patTreatLoc",StringUtil.stringToNum(patTreatLoc));

		if (!StringUtil.isEmpty(patAssignedTo))
				{
		  			userB.setUserId(StringUtil.stringToNum(patAssignedTo));
					userB.getUserDetails();
					patAssignedToName = userB.getUserFirstName() + " " + userB.getUserLastName();
				}
				if (!StringUtil.isEmpty(patPhysician))
				{
		  			userB.setUserId(StringUtil.stringToNum(patPhysician));
					userB.getUserDetails();
					patPhysicianName  = userB.getUserFirstName() + " " + userB.getUserLastName();
				}
				


		if (StringUtil.isEmpty(notes))
			notes="";

		if (! StringUtil.isEmpty(screenedBy))
		{
			userB.setUserId(StringUtil.stringToNum(screenedBy));
			userB.getUserDetails();
			screenedByName = userB.getUserFirstName() + " " + userB.getUserLastName();
		}
		else
		{
			screenedBy = "";
		}

		studyB.setId(StringUtil.stringToNum(studyId));
	    studyB.getStudyDetails();
		studyNum = studyB.getStudyNumber();
		int fdaStudy = ("1".equals(studyB.getFdaRegulatedStudy()))? 1:0;

		String dStatus = "";
		String dpatstatreason = "";

		int selStatPos = 0;
		String selStatSubType = "";

	    CodeDao cd1 = new CodeDao();
		CodeDao cdReason = new CodeDao();

		String dob = "";
		dob= personB.getPersonDob();



// RK: 04/06/05 - Retrieve codelst by Account

		cd1.getCodeValues("patStatus",accId);
	    cd1.setCType("patStatus");
	    cd1.setForGroup(defUserGroup);
	    cd1.getHiddenCodelstDataForUser();


		// for the selected status, get its subtype

		ArrayList arCid = new ArrayList();
		arCid = cd1.getCId();

   		ArrayList arCSubType = new ArrayList();
		arCSubType = cd1.getCSubType();

   		ArrayList cDesc = new ArrayList();
		cDesc = cd1.getCDesc();


		//create dropdown for patient study status
		StringBuffer mainStr = new StringBuffer();
		int pcounter = 0;
		Integer codeIdTemp = null;
		int codeIdSelTemp = 0;
		String hideStr = "";

		codeIdSelTemp = StringUtil.stringToNum(patStudyStat);


		 if(changeStatusMode.equals("no")){
			mainStr.append("<SELECT NAME='patstatus' onChange='fChangeReason(document.status)' disabled>") ;
			}
		 else{

			if (statAtt.equals("1"))
				mainStr.append("<SELECT NAME='patstatus' onChange='fChangeReason(document.status)' disabled >") ;
			else
				mainStr.append("<SELECT NAME='patstatus' onChange='fChangeReason(document.status)' >") ;
			}



		for (pcounter = 0; pcounter  <= cDesc.size() -1 ; pcounter++){

			codeIdTemp = (Integer) arCid.get(pcounter);

			hideStr = cd1.getCodeHide(pcounter) ;

			if (StringUtil.isEmpty(hideStr))
			{
				hideStr = "N";
			}



			if ((codeIdSelTemp == codeIdTemp.intValue()) && (changeStatusMode.equals("no") || (!mode.equals("initial"))) )
			{
				mainStr.append("<OPTION value = "+ arCid.get(pcounter)+" SELECTED  >" + cDesc.get(pcounter)+ "</OPTION>");
			}
			else
			{
			   if (hideStr.equals("N"))
                	{
					mainStr.append("<OPTION value = "+ arCid.get(pcounter)+">" + cDesc.get(pcounter)+ "</OPTION>");
					}
			}


		}
		if(changeStatusMode.equals("yes") && mode.equals("initial") )
		{
			mainStr.append("<OPTION value = '' SELECTED >"+LC.L_Select_AnOption/*Select an Option*****/+"</OPTION>") ;
		}
		else
		{
			mainStr.append("<OPTION value = '' >"+LC.L_Select_AnOption/*Select an Option*****/+"</OPTION>") ;
		}
		mainStr.append("</SELECT>");


		dStatus = mainStr.toString();
		//////

		// find position of selected code in the array

		if (! StringUtil.isEmpty(patStudyStat))
		{
			selStatPos = arCid.indexOf(Integer.valueOf(patStudyStat));

		}



		if (selStatPos >= 0 )
		{
			selStatSubType = (String) arCSubType.get(selStatPos);

			if (! StringUtil.isEmpty(selStatSubType ))
			{
				cdReason.getCodeValues(selStatSubType.trim());

				cdReason.setCType(selStatSubType.trim());
	            cdReason.setForGroup(defUserGroup);

				if (reasonAtt.equals("1") || reasonAtt.equals("2"))
					dpatstatreason = cdReason.toPullDown("patstatreason",StringUtil.stringToNum(statReason),"disabled");
				else
					dpatstatreason = cdReason.toPullDown("patstatreason",StringUtil.stringToNum(statReason));
			}
			else
			{
				selStatSubType = "";
			}


		}


// JM: 01Oct2006
patB.setId(StringUtil.stringToNum(statid));
patB.getPatStudyStatDetails();
psk = patB.getPatProtObj();
if (psk  != null) patSelOrg = psk.getpatOrg();

//	JM: 07August2006: Added Treating Org drop down
	int AccSiteId = 0;
	String selected = "";
	StringBuffer ddSites = new StringBuffer();

	SiteDao siteDao = new SiteDao();
	siteDao.getSiteValues(accId);

	if (torgAtt.equals("1") || torgAtt.equals("2"))
		ddSites.append("<SELECT NAME=patorg disabled>") ;
	else
		ddSites.append("<SELECT NAME=patorg >") ;


	ddSites.append("<OPTION value='' >"+LC.L_Select_AnOption/*Select an Option*****/+"</OPTION>");
	for (int counter = 0; counter <= (siteDao.getSiteIds()).size() -1 ; counter++){

		AccSiteId = ((Integer)((siteDao.getSiteIds()).get(counter))).intValue();

		if(AccSiteId==(StringUtil.stringToNum(patSelOrg)))
		{
		  selected="selected";
		}
		else{
		selected="";
		}


		ddSites.append("<OPTION value = "+ AccSiteId + " " + selected +">" + (siteDao.getSiteNames()).get(counter) + "</OPTION>");
	}
ddSites.append("</SELECT>");
//JM: 07Aug06


		int pageRight = 0;


 		if (selStatSubType.equals("enrolled"))
		{

			///////////////


			//get enrollment data
			if (mode.equals("initial"))
			{
				if (changeStatusMode.equals("no"))
				{
					if (psk  != null)
					{

						patRandomizationNumber  = psk.getPatProtRandomNumber();
						patEnrolledBy   = psk.getPatProtUserId();


					}
				}
			}
			else
			{
				if (changeStatusMode.equals("no"))
					{
					patRandomizationNumber  = request.getParameter("patRandomizationNumber");
					patEnrolledBy   = request.getParameter("patEnrollBy");

				}


			}

			if (!StringUtil.isEmpty(patEnrolledBy))
				{
		  			userB.setUserId(StringUtil.stringToNum(patEnrolledBy));
					userB.getUserDetails();
					patEnrolledByName = userB.getUserFirstName() + " " + userB.getUserLastName();
				}
				if (StringUtil.isEmpty(patEnrolledBy))
				{
				   	userB.setUserId(StringUtil.stringToNum(userIdFromSession));
					userB.getUserDetails();
					patEnrolledByName = userB.getUserFirstName() + " " + userB.getUserLastName();
					patEnrolledBy = userIdFromSession;
				}

			if (StringUtil.isEmpty(patRandomizationNumber))
				patRandomizationNumber = "";





		}


	if (selStatSubType.equals("infConsent"))
		{
			//get enrollment data
			if (mode.equals("initial"))
			{
				if (changeStatusMode.equals("no"))
				{
					if (psk  != null)
						//infConsentVerNumber  = psk.getPtstConsentVer();
						//JM: added 01.31.2006
						infConsentVerNumber  = patB.getPtstConsentVer();

				}
			}
			else
			{
				if (changeStatusMode.equals("no"))

					infConsentVerNumber  = request.getParameter("infConsentVerNumber");
			}


			if (StringUtil.isEmpty(infConsentVerNumber))
				infConsentVerNumber = "";



		}


if (selStatSubType.equals("followup"))
		{
			//get enrollment data
			if (mode.equals("initial"))
			{
				if (changeStatusMode.equals("no"))
				{
					if (psk  != null)
						//followUpDate  = psk.getPtstNextFlwup();
						//JM: added 01.31.2006
						followUpDate  = patB.getPtstNextFlwup();
				}
			}
			else
			{
				if (changeStatusMode.equals("no"))

					followUpDate  = request.getParameter("followUpDate");
			}


			if (StringUtil.isEmpty(followUpDate))
				followUpDate = "";



		}
	//******************GET STUDY TEAM RIGHTS //***************************************************************



	TeamDao teamDao = new TeamDao();
	teamDao.getTeamRights(StringUtil.stringToNum(studyId),StringUtil.stringToNum(userIdFromSession));
	ArrayList tId = teamDao.getTeamIds();
	int patdetright = 0;
	if (tId.size() == 0) {
		pageRight=0 ;
	}else
	{
		stdRights.setId(StringUtil.stringToNum(tId.get(0).toString()));

			ArrayList teamRights ;
						teamRights  = new ArrayList();
						teamRights = teamDao.getTeamRights();

						stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
						stdRights.loadStudyRights();


		if ((stdRights.getFtrRights().size()) == 0){
		 	pageRight= 0;
			patdetright = 0;
		}else{
			pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYMPAT"));
			patdetright = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYVPDET"));

		}
	}


//**************************************************************************************************************
		if(dthCause == null || dthCause.equals("null"))
				dthCause = "";
		if(dDthRelStd == null || dDthRelStd.equals("null"))
				dDthRelStd = "";
		if(othDthCause == null || othDthCause.equals("null"))
				othDthCause = "";
		if(othDthCauseRel == null || othDthCauseRel.equals("null"))
				othDthCauseRel = "";
%>
<%String pkey = request.getParameter("pkey");
		String patProtId= request.getParameter("patProtId");
		String patientCode= pcode;

 if(	(orgRight >= 6 && pageRight >= 6 && changeStatusMode.equals("no") )|| ( (orgRight == 5 ||  orgRight == 7) && (pageRight == 5 ||  pageRight == 7) && changeStatusMode.equals("yes") ) )

	{
	 hasAccess = true;
%>
  <Form id="statusForm" name="status" method="post" action="updatepatstatus.jsp" onSubmit="if (validate(document.status)==false) {setValidateFlag('false');return false;} else {setValidateFlag('true'); return true;}">

   <input type = "hidden" name ="oldPatStudyId" size ="20" value ="<%=oldPatStudyId%>" >
  <table width="1000" cellspacing="0" cellpadding="0">
	<tr>
	  <td>
         <P> <%=LC.L_Patient_Id%><%--Patient ID*****--%>: &nbsp;&nbsp;<%=pcode%> &nbsp;&nbsp;&nbsp; <%=LC.L_Study_Number%><%--Study Number*****--%>: &nbsp;&nbsp;<%=studyNum%></P>
     </td>
    </tr>
    </table>
    <br>
	<P class="sectionHeadings" style="display:block;"><%=LC.L_Pat_StdStatus%><%--Patient Study Status*****--%></P>
	<input type="hidden" name="pkey"  value="<%=pkey%>">
	<input type="hidden" name="patProtId"  value="<%=patProtId%>">
	<input type="hidden" name="patientCode"  value="<%=patientCode%>">

    <input type="hidden" name="studyId" size = 15 MAXLENGTH = 50 value="<%=studyId %>">
	<input type="hidden" name="prevStat" size = 15 MAXLENGTH = 50 value="<%= statid %>">
    <input type="hidden" name="patientId" size = 15 MAXLENGTH = 50 value="<%= pkey %>">
    <input type="hidden" name="mode" size = 15 MAXLENGTH = 50 value="final">
    <input type="hidden" name="pcode" size = 15 MAXLENGTH = 50 value="<%=pcode%>">
	<input type="hidden" name="siteId" size = 15 MAXLENGTH = 50 value="<%=siteId%>">
	<input type="hidden" id="fdaStudy" name="fdaStudy" value="<%=fdaStudy%>">
	<input type="hidden" id="changeStatusMode" name="changeStatusMode" value="<%=changeStatusMode%>">
	<input type="hidden" name="selStatSubType" size = 15  value="<%= selStatSubType%>">
	<input type="hidden" name="patdob" value="<%=dob%>">

	<table width="1000" cellspacing="0" cellpadding="0">
	  <tr>
	 <%if (hashPgCustFld.containsKey("patstatus")) {
			int fldNumStat = Integer.parseInt((String)hashPgCustFld.get("patstatus"));
			String statMand = ((String)cdoPgField.getPcfMandatory().get(fldNumStat));
			String statLable = ((String)cdoPgField.getPcfLabel().get(fldNumStat));
			statAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumStat));


			if(statAtt == null) statAtt ="";
			if(statMand == null) statMand ="";


			if(!statAtt.equals("0")) {
				if(statLable !=null){
				%> <td width="20%">
				<%=statLable%>
				<%} else {%> <td width="20%">
				<%=LC.L_Status%><%-- Status*****--%>
				<%}%>
	
				<%--if (statMand.equals("1")) {--%>
				   <FONT class="Mandatory" id="pgcustomstat">* </FONT>
				<%-- } --%>
		    </td>
		
			<td><%= dStatus%></td>
			<%} else if(statAtt.equals("0")) {%>
				<input type="hidden" name="patstatus" value=<%=patStudyStat%> >
			<%}%>
		<%} else {%>
		<td width="20%"> <%=LC.L_Status%><%-- Status*****--%> <FONT class="Mandatory" id="mandstat">* </FONT></td>
        <td>
			<%= dStatus%>
        </td>
    <%}%>

</tr>


 <tr>
	 <%if (hashPgCustFld.containsKey("reason")) {
			int fldNumReason = Integer.parseInt((String)hashPgCustFld.get("reason"));
			String reasonMand = ((String)cdoPgField.getPcfMandatory().get(fldNumReason));
			String reasonLable = ((String)cdoPgField.getPcfLabel().get(fldNumReason));
			reasonAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumReason));


			if(reasonAtt == null) reasonAtt ="";
			if(reasonMand == null) reasonMand ="";


			if(!reasonAtt.equals("0")) {
			if(reasonLable !=null){
			%> <td width="20%">
			<%=reasonLable%>
			<%} else {%> <td width="20%">
			<%=LC.L_Reason%><%-- Reason*****--%>
			<%}

			if (reasonMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomreason">* </FONT>
			<% } %>

    </td>
     <td>

	<%= dpatstatreason%>
	<%if(reasonAtt.equals("1")) {%>
		<input type="hidden" name="patstatreason" value="">
	<%} else if (reasonAtt.equals("2")) {%>
		<input type="hidden" name="patstatreason" value="<%=statReason%>"> 
	<%}%>
    </td>
	<%} else if(reasonAtt.equals("0")) {%>

	<input type="hidden" name="patstatreason" value= <%=statReason%>  >
	<%}} else {%>

		<td width="20%"><%=LC.L_Reason%><%-- Reason*****--%></td>
        <td>
			<%= dpatstatreason%>
        </td>

    <%}%>

</tr>


  <td>
   <%
				if (mode.equals("initial") && !changeStatusMode.equals("no"))
					{
						notes = "";
						startDate = "";
					}
   %>


	<tr>
		<%
				if (hashPgCustFld.containsKey("statusdate")) {

				int fldNumStatdt = Integer.parseInt((String)hashPgCustFld.get("statusdate"));
				String statusDtMand = ((String)cdoPgField.getPcfMandatory().get(fldNumStatdt));
				String statusDtLable = ((String)cdoPgField.getPcfLabel().get(fldNumStatdt));
				String statusDtAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumStatdt));

				disableStr ="";
				readOnlyStr ="";

				if(statusDtAtt == null) statusDtAtt ="";
  				if(statusDtMand == null) statusDtMand ="";

				if(!statusDtAtt.equals("0")) {

				if(statusDtLable !=null){
				%>
				<td width="20%">
				 <%=statusDtLable%>
				<%} else {%> <td width="20%">
				  <%=LC.L_Status_Date%><%-- Status Date*****--%>
				<%} %>


			   <%--if (statusDtMand.equals("1")) {--%>
			   <FONT class="Mandatory" id="pgcustomstatdt">* </FONT>
		 	   <%-- } --%>

			   <%if(statusDtAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }
				 else if (statusDtAtt.equals("2") ) {
					 readOnlyStr = "readonly"; }

  		       %>

 			  </td>
			  <td>
				<%-- INF-20084 Datepicker-- AGodara --%>
				<%if (StringUtil.isEmpty(statusDtAtt)){ %>
					<input type="text" name="StatusDate" class="datefield" size = 15 MAXLENGTH = 50	value ="<%=startDate%>">
				<%} else if(statusDtAtt.equals("1") || statusDtAtt.equals("2")) { %>
			  		<input type="text" name="StatusDate" size = 15 MAXLENGTH = 50 <%=readOnlyStr%> <%=disableStr%> value = "<%=startDate%>">
				<%}%>
			  </td>
			<%}else{%>
				<input type="hidden" name="StatusDate" size = 15 MAXLENGTH = 50  value = <%=startDate%>>
		<% }} else {%>
			<td width="20%"> <%=LC.L_Status_Date%><%-- Status Date*****--%> <FONT class="Mandatory" id="mandstatdt">* </FONT></td>
			<td><input type="text" name="StatusDate" class="datefield" size = 15 MAXLENGTH = 50	value = <%=startDate%>></td>
		<%}%>
		
	</tr>


	<!--Added by Manimaran for the September Enhancement (S8)-->
	<tr>
	<td colspan="2">
	<% if (changeStatusMode.equals("yes")) { %>
	<!--KM - Modified based on code review -->
	<input type="checkbox" name="currentStat" checked> <%=MC.M_PatCurStat_InStd%><%-- This is <%=LC.Pat_Patient_Lower%>'s current status in this <%=LC.Std_Study_Lower%>*****--%>
	<%}else if(StringUtil.stringToNum(currentStat)==0) { %>
	<input type="checkbox" name="currentStat"> <%=MC.M_PatCurStat_InStd%><%-- This is <%=LC.Pat_Patient_Lower%>'s current status in this <%=LC.Std_Study_Lower%>*****--%>
	<%} else if(StringUtil.stringToNum(currentStat)==1) { %>
	<!-- Modified For Bug#10546 : Raviesh -->
	<input type="checkbox" name="currentStat" <%if(statid.toString().equalsIgnoreCase(currentStatId.toString())){ %>checked disabled <%} %>> <%=MC.M_PatCurStat_InStd%><%-- This is <%=LC.Pat_Patient_Lower%>'s current status in this <%=LC.Std_Study_Lower%>*****--%>
	<%}%>
	</td>
	</tr>


	<tr>
        <%if (hashPgCustFld.containsKey("notes")) {
			int fldNumNotes = Integer.parseInt((String)hashPgCustFld.get("notes"));
			String notesMand = ((String)cdoPgField.getPcfMandatory().get(fldNumNotes));
			String notesLable = ((String)cdoPgField.getPcfLabel().get(fldNumNotes));
			String notesAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumNotes));

			disableStr ="";
			readOnlyStr ="";
			if(notesAtt == null) notesAtt ="";
			if(notesMand == null) notesMand ="";


			if(!notesAtt.equals("0")) {
			if(notesLable !=null){
			%><td width="20%">
			<%=notesLable%>
			<%} else {%><td width="20%">
			 <%=LC.L_Notes%><%-- Notes*****--%>
			<%}

			if (notesMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomnotes">* </FONT>
			<% }
		 %>

		  <%if(notesAtt.equals("1")) {
			 disableStr = "disabled class='readonly-input'"; }
		     else if (notesAtt.equals("2") ) {
			 readOnlyStr = "readonly"; }
		 %>

     </td>
    <td>

		<TextArea name="Notes" rows=3 cols=30  <%=disableStr%>  <%=readOnlyStr%>><%=notes%></TextArea>
	</td>
    	<% } else { %>

	 <TextArea name="Notes"  Style = "visibility:hidden"  rows=3 cols=30 ><%=notes%></TextArea>

     <% }} else {
		 %>
  	 <td width="20%"> <%=LC.L_Notes%><%-- Notes*****--%></td>
	 <td>
  	 <TEXTAREA name="Notes"  rows=3 cols=30><%=notes%></TEXTAREA></td>
    <%}%>
  </tr>
  </table>

	<% int screenFieldCount = 0;%>

	<!--Screening Details-->
		<%if (selStatSubType.equals("screening"))
		 {
			CodeDao cdScreen = new CodeDao();
			cdScreen.getCodeValues("screenOutcome");
			String strSreenDD = "";

			if (scrOutcomeAtt.equals("1") || scrOutcomeAtt.equals("2"))
				strSreenDD = cdScreen.toPullDown("patScreenOutcome", StringUtil.stringToNum(screeningOutcome),"disabled");
			else
				strSreenDD = cdScreen.toPullDown("patScreenOutcome", StringUtil.stringToNum(screeningOutcome));

		 %>

 	<p id="screenPTag" class = "sectionHeadings" style="display:block;"><%=LC.L_Screening_Dets%><%-- Screening Details*****--%></p>
	<table id="screenTable" width="1000" cellspacing="0" cellpadding="0">

	<tr>
	<%if (hashPgCustFld.containsKey("screennumber")) {

			int fldNumScrnum = Integer.parseInt((String)hashPgCustFld.get("screennumber"));
			String scrnumMand = ((String)cdoPgField.getPcfMandatory().get(fldNumScrnum));
			String scrnumLable = ((String)cdoPgField.getPcfLabel().get(fldNumScrnum));
			String scrnumAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumScrnum));

			disableStr ="";
			readOnlyStr ="";
			if(scrnumAtt == null) scrnumAtt ="";
			if(scrnumMand == null) scrnumMand ="";


			if(!scrnumAtt.equals("0")) {
				screenFieldCount = screenFieldCount+1;
			if(scrnumLable !=null){
			%><td width="20%">
			<%=scrnumLable%>
			<%} else {%> <td width="20%">
			<%=LC.L_Screen_Number%><%-- Screen Number*****--%>
			<%}

			if (scrnumMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomscrnum">* </FONT>
			<% }
		 %>

		  <%if(scrnumAtt.equals("1")) {
			 disableStr = "disabled class='readonly-input'"; }
		     else if (scrnumAtt.equals("2") ) {
			 readOnlyStr = "readonly"; }
		 %>

    </td>
    <td>
		<input type="text" name="patScreenNumber" size = 15  <%=disableStr%>  <%=readOnlyStr%> MAXLENGTH = 50 value='<%=screenNumber%>'>

	</td>
    <% }    else { %>

	<input type="hidden" name="patScreenNumber" size = 15   MAXLENGTH = 50 value='<%=screenNumber%>'>


	<% }
	}else {
		 %>
		<%screenFieldCount = screenFieldCount+1;%>
		<td width="20%"> <%=LC.L_Screen_Number%><%-- Screen Number*****--%></td>
		<td>
			<input type="text" name="patScreenNumber" size = 15 MAXLENGTH = 50 value='<%=screenNumber%>'>
		 </td>

   <%}%>

   </tr>

			<tr>
		<%

				if (hashPgCustFld.containsKey("screenedby")) {

				int fldNumScrby = Integer.parseInt((String)hashPgCustFld.get("screenedby"));
				String scrbyMand = ((String)cdoPgField.getPcfMandatory().get(fldNumScrby));
				String scrbyLable = ((String)cdoPgField.getPcfLabel().get(fldNumScrby));
				String scrbyAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumScrby));

				disableStr ="";
				readOnlyStr ="";

				if(scrbyAtt == null) scrbyAtt ="";
  				if(scrbyMand == null) scrbyMand ="";

				if(!scrbyAtt.equals("0")) {

				if(scrbyLable !=null){
				%>
				<td width="20%">
				 <%=scrbyLable%>
				<%} else {%> <td width="20%">
				 <%=LC.L_Screened_By%><%--Screened By*****--%>
				<% }


			   if (scrbyMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomscrby">* </FONT>
		 	   <% }
			   %>

			   <%if(scrbyAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }
  		       %>
 			   </td>
			   <td>
			   <% if(!scrbyAtt.equals("1")) { %>
			    <input type=hidden name=patScreenedBy value='<%=screenedBy%>'>
			   <%}%>
			    <input type=text name=patScreenedByName size=30 <%=disableStr%> value="<%=screenedByName%>"  readonly>

			  <%if(!scrbyAtt.equals("1") && !scrbyAtt.equals("2")) { %>
			   <A HREF=# onClick=openCommonUserSearchWindow("status","patScreenedBy","patScreenedByName")><%=LC.L_Select_User%><%--Select User*****--%></A>
			  <%}%>
			  <%screenFieldCount = screenFieldCount+1;%>
			  </td>

				<%} else { %>

			  <input type=hidden name=patScreenedBy value='<%=screenedBy%>'>
			  <input type=hidden name=patScreenedByName size=30 value="<%=screenedByName%>" >
				<% }}  else {%>
				<%screenFieldCount = screenFieldCount+1;%>
				<td width="20%"> <%=LC.L_Screened_By%><%--Screened By*****--%></td>
        		  <td>
						<input type=hidden name=patScreenedBy value='<%=screenedBy%>'>
          				<input type=text name=patScreenedByName size=30 value="<%=screenedByName%>"  readonly>
          				<A HREF=# onClick=openCommonUserSearchWindow("status","patScreenedBy","patScreenedByName")><%=LC.L_Select_User%><%--Select User*****--%></A>
       			</td>

			 <%}%>
			</tr>


		<tr>
		 <%if (hashPgCustFld.containsKey("screeningoutcome")) {
				int fldNumScroutcome = Integer.parseInt((String)hashPgCustFld.get("screeningoutcome"));
				String scrOutcomeMand = ((String)cdoPgField.getPcfMandatory().get(fldNumScroutcome));
				String scrOutcomeLable = ((String)cdoPgField.getPcfLabel().get(fldNumScroutcome));
				scrOutcomeAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumScroutcome));


				if(scrOutcomeAtt == null) scrOutcomeAtt ="";
				if(scrOutcomeMand == null) scrOutcomeMand ="";


				if(!scrOutcomeAtt.equals("0")) {
					screenFieldCount = screenFieldCount+1;
				if(scrOutcomeLable !=null){
				%> <td width="20%">
				<%=scrOutcomeLable%>
				<%} else {%> <td width="20%">
				<%=LC.L_Screening_Outcome%><%--Screening Outcome*****--%>
				<%}

				if (scrOutcomeMand.equals("1")) {
					%>
				   <FONT class="Mandatory" id="pgcustomscroutcome">* </FONT>
				<% } %>

		</td>
		 <td>

		<%=strSreenDD%>
		<%if(scrOutcomeAtt.equals("1")) {%>
			<input type="hidden" name="patScreenOutcome" value="">
		<%} else if (scrOutcomeAtt.equals("2")){%>
			<input type="hidden" name="patScreenOutcome" value="<%=StringUtil.stringToNum(screeningOutcome)%>">
		<%}%>
		</td>
		<%} else if(scrOutcomeAtt.equals("0")) {%>

		<input type="hidden" name="patScreenOutcome" value="<%=screeningOutcome%>">
		<%}} else {%>
			<%screenFieldCount = screenFieldCount+1;%>
			<td width="20%"> <%=LC.L_Screening_Outcome%><%--Screening Outcome*****--%></td>
        	 <td>
        				<%=strSreenDD%>
        	 </td>

		<%}%>

		 </tr>


 	</table>


		<%
		  }
		%>
	<input id="screenFieldCount" type="hidden" value="<%=screenFieldCount%>" name="screenFieldCount">
	<!--End of Screening Details-->

	<!-- Enrollment Details -->
	<% int enrollFieldCount = 0;
	%>

	<% if (selStatSubType.equals("enrolled"))
		 {
		 	 %>

		<p id="enrollPTag" class = "sectionHeadings"  style="display:block;" ><%=LC.L_Enrollment_Dets%><%--Enrollment Details*****--%></p>
		<table id="enrollTable" width="1000" cellspacing="0" cellpadding="0">


	 <tr>
	<%if (hashPgCustFld.containsKey("randomnumber")) {

			int fldNumRandom = Integer.parseInt((String)hashPgCustFld.get("randomnumber"));
			String randomMand = ((String)cdoPgField.getPcfMandatory().get(fldNumRandom));
			String randomLable = ((String)cdoPgField.getPcfLabel().get(fldNumRandom));
			String randomAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumRandom));

			disableStr ="";
			readOnlyStr ="";
			if(randomAtt == null) randomAtt ="";
			if(randomMand == null) randomMand ="";


			if(!randomAtt.equals("0")) {
				enrollFieldCount = enrollFieldCount+1;
			if(randomLable !=null){
			%> <td width="20%">
			<%=randomLable%>
			<%} else {%> <td>
			<%=LC.L_Randomization_Number%><%--Randomization Number*****--%>
			<%}

			if (randomMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomrandom">* </FONT>
			<% }
		 %>

		  <%if(randomAtt.equals("1")) {
			 disableStr = "disabled class='readonly-input'"; }
		     else if (randomAtt.equals("2") ) {
			 readOnlyStr = "readonly"; }
		 %>

    </td>
    <td>

		<input type="text" name="patRandomizationNumber" <%=disableStr%>  <%=readOnlyStr%> size = 15 MAXLENGTH = 50 value='<%=patRandomizationNumber%>'>

	</td>
    	<% }}

	else {
		 enrollFieldCount = enrollFieldCount+1;
		 %>

		<td width="20%"> <%=LC.L_Randomization_Number%><%--Randomization Number*****--%></td>
			  <td>
				<input type="text" name="patRandomizationNumber" size = 15 MAXLENGTH = 50 value='<%=patRandomizationNumber%>'>
	    </td>

  <%}%>

</tr>


	<tr>
		<%
				if (hashPgCustFld.containsKey("enrolledby")) {

				int fldNumEnrBy = Integer.parseInt((String)hashPgCustFld.get("enrolledby"));
				String enrByMand = ((String)cdoPgField.getPcfMandatory().get(fldNumEnrBy));
				String enrByLable = ((String)cdoPgField.getPcfLabel().get(fldNumEnrBy));
				String enrByAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumEnrBy));

				disableStr ="";
				readOnlyStr ="";

				if(enrByAtt == null) enrByAtt ="";
  				if(enrByMand == null) enrByMand ="";

				if(!enrByAtt.equals("0")) {
					enrollFieldCount = enrollFieldCount+1;
				if(enrByLable !=null){
				%>
				<td width="20%">
				 <%=enrByLable%>
				<%} else {%> <td width="20%">
				<%=LC.L_Enrolled_By%><%--Enrolled By*****--%>
				<% }


			   if (enrByMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomenrby">* </FONT>
		 	   <% }
			   %>

			   <%if(enrByAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }
  		       %>
 			   </td>
			   <td>
			   <% if(!enrByAtt.equals("1")) { %>
			    <input type=hidden name=patEnrollBy value='<%=patEnrolledBy%>'>
			   <%}%>
			    <input type=text name=patEnrollByName size=30 <%=disableStr%> value="<%=patEnrolledByName%>" readonly>

			  <%if(!enrByAtt.equals("1") && !enrByAtt.equals("2")) { %>
			     <A HREF=# onClick=openCommonUserSearchWindow("status","patEnrollBy","patEnrollByName") ><%=LC.L_Select_User%><%--Select User*****--%></A>
			  <%}%>

			  </td>

				<%} else { %>

			  <input type=hidden name=patAssignTo value="<%=patAssignedTo%>">
			  <input type=hidden name=patAssignToName size=30  value="<%=patAssignedToName%>"  >

				<% }}  else {%>
					<%enrollFieldCount = enrollFieldCount+1;%>
				<td width="20%">
				   <%=LC.L_Enrolled_By%><%--Enrolled By*****--%> <FONT class="Mandatory">* </FONT>
				</td>
				<td>
				<input type=hidden name=patEnrollBy value='<%=patEnrolledBy%>'>
					  <input type=text name=patEnrollByName size=30 value="<%=patEnrolledByName%>" readonly>
					  <A HREF=# onClick=openCommonUserSearchWindow("status","patEnrollBy","patEnrollByName") ><%=LC.L_Select_User%><%--Select User*****--%></A>
				</td>

			 <%}%>
			</tr>


	</table>

	<%
	  }
	%>
	<input id="enrollFieldCount" type="hidden" value="<%=enrollFieldCount%>" name="enrollFieldCount">
	<!-- End of Enrollment Details -->

	<!-- Informed Consent Details -->
	<% int consentFieldCount = 0;%>
	<% if (selStatSubType.equals("infConsent")){%>

		<p id="consentPTag" class = "sectionHeadings" style="display:block"><%=LC.L_Informed_ConsentDets%><%--Informed Consent Details*****--%></p>
		<table id="consentTable" width="1000" cellspacing="0" cellpadding="0">

	<tr>
	<%if (hashPgCustFld.containsKey("icversionnumber")) {

			int fldNumIcver = Integer.parseInt((String)hashPgCustFld.get("icversionnumber"));
			String icverMand = ((String)cdoPgField.getPcfMandatory().get(fldNumIcver));
			String icverLable = ((String)cdoPgField.getPcfLabel().get(fldNumIcver));
			String icverAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumIcver));

			disableStr ="";
			readOnlyStr ="";
			if(icverAtt == null)icverAtt ="";
			if(icverMand == null)icverMand ="";


			if(!icverAtt.equals("0")) {
				consentFieldCount = consentFieldCount+1;
			if(icverLable !=null){
			%>  <td width="20%">
			<%=icverLable%>
			<%} else {%>  <td width="20%">
			<%=MC.M_InformCons_VerNum%><%--Informed Consent Version Number*****--%>
			<%}

			if (icverMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomicver">* </FONT>
			<% }
		 %>

		  <%if(icverAtt.equals("1")) {
			 disableStr = "disabled class='readonly-input'"; }
		     else if (icverAtt.equals("2") ) {
			 readOnlyStr = "readonly"; }
		 %>

    </td>
    <td>
		<input type="text" name="infConsentVerNumber" size = 15 <%=disableStr%>  <%=readOnlyStr%> MAXLENGTH = 250 value='<%=infConsentVerNumber%>'>

	</td>
    <% }    else { %>
	<input type="hidden" name="infConsentVerNumber" size = 15  MAXLENGTH = 250 value='<%=infConsentVerNumber%>'>

	<% }
	}else {%>
		<%consentFieldCount = consentFieldCount+1; %>
		<td width="20%"><%=MC.M_InformCons_VerNum%><%--Informed Consent Version Number*****--%></td>
			  <td>
				<input type="text" name="infConsentVerNumber" size = 15 MAXLENGTH = 250 value='<%=infConsentVerNumber%>'>
	    </td>
  <%}%>

  </tr>
</table>

	<%
	  }
	%>
	<input id="consentFieldCount" type="hidden" value="<%=consentFieldCount%>" name="consentFieldCount">
	<!-- End of Informed Consent Details -->
	<!-- Follow-up Details -->
	<% int followFieldCount = 0;%>
	<% if (selStatSubType.equals("followup")){%>

		<p id="followPTag" class = "sectionHeadings" ><%=LC.L_Flwup_Dets%><%--Follow-up Details*****--%></p>
		<table id="followTable" width="1000" cellspacing="0" cellpadding="0">


		<tr>
		<%
				if (hashPgCustFld.containsKey("followupdate")) {

				int fldNumFollowup = Integer.parseInt((String)hashPgCustFld.get("followupdate"));
				String followupMand = ((String)cdoPgField.getPcfMandatory().get(fldNumFollowup));
				String followupLable = ((String)cdoPgField.getPcfLabel().get(fldNumFollowup));
				String followupAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumFollowup));

				disableStr ="";
				readOnlyStr ="";

				if(followupAtt == null) followupAtt ="";
  				if(followupMand == null) followupMand ="";

				if(!followupAtt.equals("0")) {
					followFieldCount =followFieldCount+1;
				if(followupLable !=null){
				%>
				<td width="20%">
				 <%=followupLable%>
				<%} else {%> <td width="20%">
				  <%=LC.L_NxtFlwup_Dt%><%--Next Follow-up Date*****--%>
				<% }


			   if (followupMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomfollowup">* </FONT>
		 	   <% }
			   %>

			   <%if(followupAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }
				 else if (followupAtt.equals("2") ) {
					 readOnlyStr = "readonly"; }

  		       %>

 			  </td>
			  <td>
				<%-- INF-20084 Datepicker-- AGodara --%>
				<%if(StringUtil.isEmpty(followupAtt)){%>
					<input type="text" name="followUpDate" class="datefield" size = 15 MAXLENGTH = 50	value="<%=followUpDate%>">
				<%} else if(followupAtt.equals("1") || followupAtt.equals("2")) { %>
					<input type="text" name="followUpDate" size = 15 MAXLENGTH = 50 <%=readOnlyStr%> <%=disableStr%> value="<%=followUpDate%>">
				<%}%>
			  </td>
			<%} else { %>
			      <input type="hidden" name="followUpDate" size = 15 MAXLENGTH = 50 value=<%=followUpDate%>>
		 <%}}else {
		 			followFieldCount =followFieldCount+1; 
		 %>
			 	 <td width="20%"><%=LC.L_NxtFlwup_Dt%><%--Next Follow-up Date*****--%></td>
		  	 	 <td><input type="text" name="followUpDate" class="datefield" size = 15 MAXLENGTH = 50	value=<%=followUpDate%>>
 		  </td>
			 <%}%>
		</tr>
	</table>
	<%
	  }
	%>
	<input id="followFieldCount" type="hidden" value="<%=followFieldCount%>" name="followFieldCount">
	<!-- End of Follow-up Details -->

	<!-- Additional Information -->
	<%
		ArrayList  patSitePks = new ArrayList ();
		ArrayList  patSitenNames = new ArrayList ();

		PatFacilityDao patFacilityDao = new PatFacilityDao();

		PatFacilityJB patFacility = new PatFacilityJB();
		patFacilityDao = patFacility.getPatientFacilitiesSiteIds(StringUtil.stringToNum(patId));

		if (patFacilityDao == null)
		{
			patFacilityDao = new PatFacilityDao();
		}

		patSitePks = patFacilityDao.getPatientSite();
		patSitenNames = patFacilityDao.getPatientSiteName();

		if (patSitePks == null)
		{
			patSitePks = new ArrayList();
			patSitenNames = new ArrayList();
		}

		if (StringUtil.isEmpty(enrollingSite))
		{
			enrollingSite = String.valueOf(siteId);

		}

		if (! StringUtil.isEmpty(patStudiesEnrollingOrg) && StringUtil.stringToNum(patStudiesEnrollingOrg) > 0)
		{
		   enrollingSite = patStudiesEnrollingOrg;
		}

		if (enrsiteAtt.equals("1") || enrsiteAtt.equals("2"))
		    ddEnrollingSite = EJBUtil.createPullDownWithStrNoSelect("enrollingSite",  enrollingSite, patSitePks, patSitenNames, "disabled");
		else
		    ddEnrollingSite = EJBUtil.createPullDownWithStrNoSelect("enrollingSite",  enrollingSite, patSitePks, patSitenNames);

	%>

	<% int aInfoFieldCount = 0;%>

	<p id="aInfoPTag" class = "sectionHeadings" style="display:block;"><%=LC.L_Addl_Info%><%--Additional Information*****--%></p>
	<table id="aInfoTable" width="1000" cellspacing="0" cellpadding="0">
	   <tr>
		<%if (hashPgCustFld.containsKey("patstdid")) {

			int fldNumPatstdid = Integer.parseInt((String)hashPgCustFld.get("patstdid"));
			String patstdidMand = ((String)cdoPgField.getPcfMandatory().get(fldNumPatstdid));
			String patstdidLable = ((String)cdoPgField.getPcfLabel().get(fldNumPatstdid));
			String patstdidAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumPatstdid));

			disableStr ="";
			readOnlyStr ="";
			if(patstdidAtt == null) patstdidAtt ="";
			if(patstdidMand == null) patstdidMand ="";


			if(!patstdidAtt.equals("0")) {
				aInfoFieldCount =aInfoFieldCount+1;
				if(patstdidLable !=null){
				%><td width ="20%">
				<%=patstdidLable%>
				<%} else {%> <td width ="20%">
				<%=LC.L_Patient_StudyId%><%--Patient Study ID*****--%>
				<%}

				if (patstdidMand.equals("1")) {
					%>
				   <FONT class="Mandatory" id="pgcustompatstdid">* </FONT>
				<% } %>

			  <%if(patstdidAtt.equals("1")) {
					disableStr = "disabled class='readonly-input'";
				} else if (patstdidAtt.equals("2") ) {
					readOnlyStr = "readonly";
				}
		 %>

			</td>
			<td>
				<input type=text name=patStudyId size=20 maxlength = 20  <%=disableStr%> value='<%=patStudyId%>' <%=readOnlyStr%>>
				<input type=hidden name="patStudyIdAuto" size=10 maxlength = 20 value='<%=patStudyIdAuto%>'>
			</td>
	    	<% } else {
			%>
			<input type=hidden name=patStudyId size=20 maxlength = 20  value='<%=patStudyId%>' >
			<input type=hidden name="patStudyIdAuto" size=10 maxlength = 20 value='<%=patStudyIdAuto%>'>
			<% }
		} else {
			aInfoFieldCount =aInfoFieldCount+1;
			 %>
			<td width ="20%"> <%=LC.L_Patient_StudyId%><%-- Patient Study ID*****--%>  <FONT class="Mandatory" id="mandpatstdid">* </FONT></td>
		    <td>
		   		<input type=text name=patStudyId size=20 maxlength = 20 value='<%=patStudyId%>' <%=readOnlyStr%>>
				<input type=hidden name="patStudyIdAuto" size=10 maxlength = 20 value='<%=patStudyIdAuto%>'>
		    </td>
	   <%}%>

		</tr>


		<tr>
	 	<%if (hashPgCustFld.containsKey("enrollingsite")) {
			int fldNumEnrsite = Integer.parseInt((String)hashPgCustFld.get("enrollingsite"));
			String enrsiteMand = ((String)cdoPgField.getPcfMandatory().get(fldNumEnrsite));
			String enrsiteLable = ((String)cdoPgField.getPcfLabel().get(fldNumEnrsite));
			enrsiteAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumEnrsite));


			if(enrsiteAtt == null) enrsiteAtt ="";
			if(enrsiteMand == null) enrsiteMand ="";


			if(!enrsiteAtt.equals("0")) {
				aInfoFieldCount =aInfoFieldCount+1;
				if(enrsiteLable !=null){
				%> <td width="20%">
				<%=enrsiteLable%>
				<%} else {%> <td width="20%">
				<%=LC.L_Enrolling_Site%><%--Enrolling Site*****--%>
				<%}

				if (enrsiteMand.equals("1")) {
					%>
				   <FONT class="Mandatory" id="pgcustomenrsite">* </FONT>
				<% } %>

			    </td>
			     <td>

			 		<%=ddEnrollingSite%>
			 		<%if(enrsiteAtt.equals("1")) {%>
			 			<input type="hidden" name="enrollingSite" value="">
			 		<%} else if(enrsiteAtt.equals("2")) {%>
			 			<input type="hidden" name="enrollingSite" value="<%=enrollingSite%>">
			 		<%}%>
		   		</td>
			<%} else if(enrsiteAtt.equals("0")) {%>

				<input type="hidden" name="enrollingSite" value="">
			<%}
		} else {%>
			<%aInfoFieldCount =aInfoFieldCount+1; %>
		  <td width="20%"> <%=LC.L_Enrolling_Site%><%--Enrolling Site*****--%></td>
		  <td >
			    <%=ddEnrollingSite%>
		  </td>

    	<%}%>

 		</tr>


		<tr>
		<%
		if (hashPgCustFld.containsKey("assignedto")) {

			int fldNumAssgnTo = Integer.parseInt((String)hashPgCustFld.get("assignedto"));
			String assgnToMand = ((String)cdoPgField.getPcfMandatory().get(fldNumAssgnTo));
			String assgnToLable = ((String)cdoPgField.getPcfLabel().get(fldNumAssgnTo));
			String assgnToAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumAssgnTo));

			disableStr ="";
			readOnlyStr ="";

			if(assgnToAtt == null) assgnToAtt ="";
 			if(assgnToMand == null) assgnToMand ="";

			if(!assgnToAtt.equals("0")) {
				aInfoFieldCount =aInfoFieldCount+1;
				if(assgnToLable !=null){
				%>
				<td width="20%">
				 <%=assgnToLable%>
				<%} else {%> <td width="20%">
				 	<%=LC.L_Assigned_To%><%--Assigned To*****--%>
				<% }


			   	if (assgnToMand.equals("1")) {
				%>
			    <FONT class="Mandatory" id="pgcustomassignedto">* </FONT>
		 	    <% } %>

				<%if(assgnToAtt.equals("1")) {
			    	disableStr = "disabled class='readonly-input'";
			    }%>
		 		</td>
				<td>
				<% if(!assgnToAtt.equals("1")) { %>
				 	<input type=hidden name=patAssignTo value="<%=patAssignedTo%>">
				<%}%>
				 <input type=text name=patAssignToName size=30  <%=disableStr%> value="<%=patAssignedToName%>" readonly >

				<%if(!assgnToAtt.equals("1") && !assgnToAtt.equals("2")) { %>
				  <span><A HREF=# onClick=openCommonUserSearchWindow("status","patAssignTo","patAssignToName") ><%=LC.L_Select_User%><%--Select User*****--%></A></span>
				<%}%>

				</td>

			<%} else { %>

			  <input type=hidden name=patAssignTo value="<%=patAssignedTo%>">
			  <input type=hidden name=patAssignToName size=30  value="<%=patAssignedToName%>"  >

			<% }
		}  else {%>
			<%aInfoFieldCount =aInfoFieldCount+1; %>
			<td width="20%">
      			<%=LC.L_Assigned_To%><%--Assigned To*****--%>
   			</td>
   			<td>
				 <input type=hidden name=patAssignTo value="<%=patAssignedTo%>">
	         		<input type=text name=patAssignToName size=30 value="<%=patAssignedToName%>" readonly >
			   <span><A HREF=# onClick=openCommonUserSearchWindow("status","patAssignTo","patAssignToName") ><%=LC.L_Select_User%><%--Select User*****--%></A></span>
		 	</td>
		<%}%>
		</tr>


		 <tr>
		<%

		if (hashPgCustFld.containsKey("physician")) {

			int fldNumPhysican = Integer.parseInt((String)hashPgCustFld.get("physician"));
			String physcianMand = ((String)cdoPgField.getPcfMandatory().get(fldNumPhysican));
			String physcianLable = ((String)cdoPgField.getPcfLabel().get(fldNumPhysican));
			String physcianAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumPhysican));

			disableStr ="";
			readOnlyStr ="";

			if(physcianAtt == null) physcianAtt ="";
 				if(physcianMand == null) physcianMand ="";

			if(!physcianAtt.equals("0")) {
				aInfoFieldCount =aInfoFieldCount+1;
				if(physcianLable !=null){
				%>
				<td width="20%">
				 <%=physcianLable%>
				<%} else {%> <td width="20%">
				 <%=LC.L_Physician%><%--Physician*****--%>
				<% }


			   if (physcianMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomphysician">* </FONT>
		 	   <% }
			   %>

		   		<%if(physcianAtt.equals("1")) {
		      		disableStr = "disabled class='readonly-input'"; }
 		       %>
			   </td>
			   <td>
			   <% if(!physcianAtt.equals("1")) { %>
			    <input type=hidden name=patPhysician value='<%=patPhysician%>'>
			   <%}%>
			    <input type=text name=patPhysicianName size=30  <%=disableStr%> value="<%=patPhysicianName%>" readonly>

			  <%if(!physcianAtt.equals("1") && !physcianAtt.equals("2")) { %>
			     <span><A HREF=# onClick=openCommonUserSearchWindow("status","patPhysician","patPhysicianName") ><%=LC.L_Select_User%><%--Select User*****--%></A></span>
			  <%}%>

			  </td>

			<%} else { %>

			<input type=hidden name=patPhysician value='<%=patPhysician%>'>
			<input type=hidden name=patPhysicianName size=30  value="<%=patPhysicianName%>" >


			<% }
		}  else {%>
			<%aInfoFieldCount =aInfoFieldCount+1; %>
			<td width="20%">
      			<%=LC.L_Physician%><%--Physician*****--%>
   			</td>
   			<td>
			<input type=hidden name=patPhysician value='<%=patPhysician%>'>
         		<input type=text name=patPhysicianName size=30 value="<%=patPhysicianName%>" readonly>
		      <span><A HREF=# onClick=openCommonUserSearchWindow("status","patPhysician","patPhysicianName") ><%=LC.L_Select_User%><%--Select User*****--%></A></span>
		 </td>

		 <%}%>
	</tr>

	<tr>
	 <%if (hashPgCustFld.containsKey("treatmentlocation")) {
			int fldNumLocation = Integer.parseInt((String)hashPgCustFld.get("treatmentlocation"));
			String tlocMand = ((String)cdoPgField.getPcfMandatory().get(fldNumLocation));
			String tlocLable = ((String)cdoPgField.getPcfLabel().get(fldNumLocation));
			tlocAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumLocation));


			if(tlocAtt == null) tlocAtt ="";
			if(tlocMand == null) tlocMand ="";


			if(!tlocAtt.equals("0")) {
				aInfoFieldCount =aInfoFieldCount+1;
				if(tlocLable !=null){
				%> <td width="20%">
				<%=tlocLable%>
				<%} else {%> <td width="20%">
				<%=LC.L_Treatment_Loc%><%--Treatment Location*****--%>
				<%}

				if (tlocMand.equals("1")) {
					%>
				   <FONT class="Mandatory" id="pgcustomtloc">* </FONT>
				<% } %>

			    </td>
			     <td >

				<%=treatPullDn%>
				<%if(tlocAtt.equals("1")) {%>
					<input type="hidden" name="patTreatLoc" value="">
				<%} else if (tlocAtt.equals("2")){%>
					<input type="hidden" name="patTreatLoc" value="<%=patTreatLoc%>">
				<%}%>
			    </td>
			<%} else if(tlocAtt.equals("0")) {%>

				<input type="hidden" name="patTreatLoc" value=<%=patTreatLoc%>>
			<%}
		} else {%>
			<%aInfoFieldCount =aInfoFieldCount+1; %>
			<td width="20%"><%=LC.L_Treatment_Loc%><%--Treatment Location*****--%></td>
			<td>
				<%=treatPullDn%>
			</td>
		<%}%>

	</tr>


	<tr>
		<%if (hashPgCustFld.containsKey("treatingorg")) {
			int fldNumTorg = Integer.parseInt((String)hashPgCustFld.get("treatingorg"));
			String torgMand = ((String)cdoPgField.getPcfMandatory().get(fldNumTorg));
			String torgLable = ((String)cdoPgField.getPcfLabel().get(fldNumTorg));
			torgAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumTorg));


			if(torgAtt == null) torgAtt ="";
			if(torgMand == null) torgMand ="";


			if(!torgAtt.equals("0")) {
				aInfoFieldCount =aInfoFieldCount+1;
				if(torgLable !=null){
				%> <td width="20%">
				<%=torgLable%>
				<%} else {%> <td width="20%">
				<%=LC.L_Treating_Org%><%--Treating Organization*****--%>
				<%}

				if (torgMand.equals("1")) {
					%>
				   <FONT class="Mandatory" id="pgcustomtorg">* </FONT>
				<% } %>

			    </td>
			     <td>

				<%=ddSites%>
				<%if(torgAtt.equals("1")) {%>
					<input type="hidden" name="patorg" value="">
				<%} else if (torgAtt.equals("2")){%>
					<input type="hidden" name="patorg" value="<%=patSelOrg%>">
				<%}%>
			    </td>
			<%} else if(torgAtt.equals("0")) {%>

				<input type="hidden" name="patorg" value=<%=patSelOrg%> >
			<%}
		} else {%>
			<%aInfoFieldCount =aInfoFieldCount+1; %>
			<td width="20%">
				<%=LC.L_Treating_Org%><%--Treating Organization*****--%>
				</td>
				<td>
					<%=ddSites%>
			</td>
		<%}%>

		</tr>
		
		<!-- CTRP-21708-05012013 @Ankit Start-->
			<tr>
				<%
				if (hashPgCustFld.containsKey("diseaseCode")) {

				int fldNumDiseaseSite = Integer.parseInt((String)hashPgCustFld.get("diseaseCode"));
				String disSiteMand = ((String)cdoPgField.getPcfMandatory().get(fldNumDiseaseSite));
				String disSiteLable = ((String)cdoPgField.getPcfLabel().get(fldNumDiseaseSite));
				String disSiteAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumDiseaseSite));

				disableStr ="";
				readOnlyStr ="";

				if(disSiteAtt == null) disSiteAtt ="";
  				if(disSiteMand == null) disSiteMand ="";

				if(!disSiteAtt.equals("0")) {
				aInfoFieldCount =aInfoFieldCount+1;
				if(disSiteLable !=null){
				%>
				<td width="20%">
				<%=disSiteLable%>
				<%} else {%> <td width="20%">
				 <%=LC.L_Diag_Code%><%--Disease Code*****--%>
				<% }


			   if (disSiteMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomdiscode">* </FONT>
		 	   <% }
			   %>

			   <%if(disSiteAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }

  		       %>
 			  </td>


			  <td><input type="text" name="patDiseaseCode" size="20" value="<%=patDiseaseCode%>" readonly   <%=disableStr%>  >&nbsp;&nbsp;

			  <%if(!disSiteAtt.equals("1") && !disSiteAtt.equals("2")) { %>
			  	<a href="javascript:void(0);" onClick="openWinICD('patDiseaseCode','<%=lkpId%>','<%=lkpColumn%>');"><img border='0' src='./images/edit.gif' title='<%=LC.L_Select_Diag_Code%>'></a>&nbsp;&nbsp;
			  	<a onClick="javascript:document.status.patDiseaseCode.value='';" href="javascript:void(0);" ><img border='0' src='./images/delete.gif' title='Delete'></a>
			  </td>
			  

			  <% }} else { %>
			 	<input type="hidden" name="patDiseaseCode" value="<%=patDiseaseCode%>">
			 <% } }  else { 
				 aInfoFieldCount =aInfoFieldCount+1;
			 %>

			  <td width="20%"> <%=LC.L_Diag_Code%><%--Disease Code*****--%> </td>
			  <td><input type="text" name="patDiseaseCode" size="20" value="<%=patDiseaseCode%>" readonly>&nbsp;&nbsp;
				  <a href="javascript:void(0);" onClick="openWinICD('patDiseaseCode','<%=lkpId%>','<%=lkpColumn%>');"><img border='0' src='./images/edit.gif' title='<%=LC.L_Select_Diag_Code%>'></a>&nbsp;&nbsp;
				  <a onClick="javascript:document.status.patDiseaseCode.value='';" href="javascript:void(0);" ><img border='0' src='./images/delete.gif' title='Delete'></a>
			  </td>
			 <%}%>
			 </tr>

			 <%--<tr>
				<%
				String addMoreDisplay = "style='display:block;'";
				if((!patMoreDisCode1.equals("")) && (!patMoreDisCode2.equals(""))){
					addMoreDisplay = "display:none;";
				}
				
				if (hashPgCustFld.containsKey("othrDiseaseCode")) {

				int fldNumDiseaseSite = Integer.parseInt((String)hashPgCustFld.get("othrDiseaseCode"));
				String disSiteMand = ((String)cdoPgField.getPcfMandatory().get(fldNumDiseaseSite));
				String disSiteLable = ((String)cdoPgField.getPcfLabel().get(fldNumDiseaseSite));
				String disSiteAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumDiseaseSite));

				disableStr ="";
				readOnlyStr ="";

				if(disSiteAtt == null) disSiteAtt ="";
  				if(disSiteMand == null) disSiteMand ="";

				if(!disSiteAtt.equals("0")) {
				aInfoFieldCount =aInfoFieldCount+1;
				if(disSiteLable !=null){
				%>
				<td width="20%">
				<%=disSiteLable%>
				<%} else {%> <td width="20%">
				 <%=LC.L_Othr_Diag_Code%>--%><%--Other Disease Code*****--%>
				<%--<% }


			   if (disSiteMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomothrdiscode">* </FONT>
		 	   <% }
			   %>

			   <%if(disSiteAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }

  		       %>
 			  </td>


			  <td><input type="text" name="patOtherDisCode" size="20" value="<%=patOtherDisCode%>" readonly   <%=disableStr%>  >&nbsp;&nbsp;

			  <%if(!disSiteAtt.equals("1") && !disSiteAtt.equals("2")) { %>
			  	<a href="javascript:void(0);" onClick="openWinICD('patOtherDisCode','<%=lkpId%>','<%=lkpColumn%>');"><img border='0' src='./images/edit.gif' title='<%=LC.L_Select_Othr_Diag_Code%>'></a>&nbsp;&nbsp;
			  	<a onClick="javascript:document.status.patOtherDisCode.value='';" href="javascript:void(0);" ><img border='0' src='./images/delete.gif' title='Delete'></a>&nbsp;&nbsp;
			  	<a onClick="showHide();" href="javascript:void(0);" ><img border='0' src='./images/Add.gif' id='addMore' title='<%=LC.L_Add_More%>' style='<%=addMoreDisplay%>'></a>&nbsp;&nbsp;
			  </td>
			  

			  <% }} else { %>
			 	<input type="hidden" name="patOtherDisCode" value="<%=patOtherDisCode%>">
			 <% } }  else {
					 aInfoFieldCount =aInfoFieldCount+1;	%>

			  <td width="20%"> <%=LC.L_Othr_Diag_Code%>--%><%--Other Disease Code*****--%> <%--</td>
			  <td width="80%"><input type="text" name="patOtherDisCode" size="20" value="<%=patOtherDisCode%>" readonly>&nbsp;&nbsp;
				<a href="javascript:void(0);" onClick="openWinICD('patOtherDisCode','<%=lkpId%>','<%=lkpColumn%>');"><img border='0' src='./images/edit.gif' title='<%=LC.L_Select_Othr_Diag_Code%>'></a>&nbsp;&nbsp;
			  	<a onClick="javascript:document.status.patOtherDisCode.value='';" href="javascript:void(0);" ><img border='0' src='./images/delete.gif' title='Delete'></a>&nbsp;&nbsp;
			  	<a onClick="showHide();" href="javascript:void(0);" ><img border='0' src='./images/Add.gif' id='addMore' title='<%=LC.L_Add_More%>' style='<%=addMoreDisplay%>'></a>&nbsp;&nbsp;
			  	
			  </td>
			 <%}%>
			 </tr>--%>
			 
			 <tr>
						<%
				if (hashPgCustFld.containsKey("anatomicsite")) {

				int fldNumAnatomicSite = Integer.parseInt((String)hashPgCustFld.get("anatomicsite"));
				String anaSiteMand = ((String)cdoPgField.getPcfMandatory().get(fldNumAnatomicSite));
				String anaSiteLable = ((String)cdoPgField.getPcfLabel().get(fldNumAnatomicSite));
				String anaSiteAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumAnatomicSite));

				disableStr ="";
				readOnlyStr ="";

				if(anaSiteAtt == null) anaSiteAtt ="";
  				if(anaSiteMand == null) anaSiteMand ="";

				if(!anaSiteAtt.equals("0")) {

				if(anaSiteLable !=null){
				%>
				<td width="20%">
				<%=anaSiteLable%>
				<%} else {%> <td width="20%">
				 <%=LC.L_Anatomic_Site%><%--Anatomic Site*****--%>
				<% }


			   if (anaSiteMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomdissite">* </FONT>
		 	   <% }
			   %>

			   <%if(anaSiteAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }

  		       %>
 			  </td>


			  <td><input type="text" id="anaSitename" name="anaSitename" size="20" value="<%=anaSitename%>" readonly <%=disableStr%>>&nbsp;
			  <%if(!anaSiteAtt.equals("1") && !anaSiteAtt.equals("2")) { %>
			<a href="#" onClick="selectAnatomicSite(document.status)"><%=LC.L_Select_AnatomicSites%><%--Select Anatomic Site(s)*****--%></a></td>
			  <input type="hidden" id="anaSiteid" name="anaSiteid" value="<%=anaSiteid%>">

			  <% }} else { %>
			 <input type="hidden" id="anaSiteid" name="anaSiteid" value="<%=anaSiteid%>">

			 <% } }  else { %>

			  <td width="20%"> <%=LC.L_Anatomic_Site%><%--Anatomic Site*****--%> </td>
			  <td><input type="text" id="anaSitename" name="anaSitename" size="20" value="<%=anaSitename %>" readonly>&nbsp;&nbsp;<a href="#" onClick="selectAnatomicSite(document.status)"><%=LC.L_Select_AnatomicSites%><%--Select Anatomic Site(s)*****--%></a>  </td>
			 	<input type="hidden" id="anaSiteid" name="anaSiteid" value="<%=anaSiteid %>">
			 <%}%>
			 </tr>

		</table>
		
		<% 				
		String divDisplay1 = "style='display:none;'";
		if(!patMoreDisCode1.equals("")){
			divDisplay1 = "style='display:block;'";
		}
		%>
		<div id="moreDiagCode1" <%=divDisplay1%>>
		<%aInfoFieldCount =aInfoFieldCount+1; %>
		<table width="1000" id="aInfoTable1" cellspacing="0" cellpadding="0">
		<tr>
			  <td width="20%"> <%=LC.L_More_Diag_Code1%><%--Disease Site*****--%> </td>
			  <td><input type="text" name="patMoreDisCode1" size="20" value="<%=patMoreDisCode1%>" readonly>&nbsp;&nbsp;
				  <a href="javascript:void(0);" onClick="openWinICD('patMoreDisCode1','<%=lkpId%>','<%=lkpColumn%>');"><img border='0' src='./images/edit.gif' title='<%=LC.L_Select_More_Diag_Code%>'></a>&nbsp;&nbsp;
				  <a onClick="javascript:document.status.patMoreDisCode1.value='';" href="javascript:void(0);" ><img border='0' src='./images/delete.gif' title='Delete'></a>&nbsp;&nbsp;
				  <a onClick="hideDiv(document.getElementById('moreDiagCode1'), document.status.patMoreDisCode1);" href="javascript:void(0);" ><img border='0' src='./images/HideIcons.gif' title='Hide'></a>
			  </td>
		</tr>
		</table>
		</div>
		<% 				
		String divDisplay2 = "style='display:none;'";
		if(!patMoreDisCode2.equals("")){
			divDisplay2 = "style='display:block;'";
		}
		%>
		<div id="moreDiagCode2" <%=divDisplay2%>>
		<%aInfoFieldCount =aInfoFieldCount+1; %>
		<table width="1000" id="aInfoTable2" cellspacing="0" cellpadding="0">
		<tr>
			  <td width="20%"> <%=LC.L_More_Diag_Code2%><%--Disease Site*****--%> </td>
			  <td><input type="text" name="patMoreDisCode2" size="20" value="<%=patMoreDisCode2%>" readonly>&nbsp;&nbsp;
				  <a href="javascript:void(0);" onClick="openWinICD('patMoreDisCode2','<%=lkpId%>','<%=lkpColumn%>');"><img border='0' src='./images/edit.gif' title='<%=LC.L_Select_More_Diag_Code%>'></a>&nbsp;&nbsp;
				  <a onClick="javascript:document.status.patMoreDisCode2.value='';" href="javascript:void(0);" ><img border='0' src='./images/delete.gif' title='Delete'></a>&nbsp;&nbsp;
				  <a onClick="hideDiv(document.getElementById('moreDiagCode2'), document.status.patMoreDisCode2);" href="javascript:void(0);" ><img border='0' src='./images/HideIcons.gif' title='Hide'></a>
			  </td>
		</tr>
		</table>
		</div>
	<!-- CTRP-21708-05012013 @Ankit End-->		
	<table id="aInfoTable3" width="1000" cellspacing="0" cellpadding="0">
	<tr>
	<td colspan="2">
	<% if (ctrpReportable==1) {
		String checkedValue= "checked";
	  if(dGrphReportable.equalsIgnoreCase("0")){
		  checkedValue="";  
	  }
		%>
	<input type="checkbox" name="dGrphReportable" id="dGrphReportable" <%=checkedValue%>> <%=MC.M_DemoRepAtPatStd %>
	<%}else{ %>
	<span style="display: none;">
	<input type="checkbox" name="dGrphReportable" id="dGrphReportable" >
	</span>
	
	<%
		
	}%>
	</td>
	</tr>
		
	</table>
	<!-- End of Additional Information -->
	<input id="aInfoFieldCount" type="hidden" value="<%=aInfoFieldCount%>" name="aInfoFieldCount">
<%//May Enhn


CodeDao cdao1 = new CodeDao();
CodeDao cdao2 = new CodeDao();
CodeDao cdao3 = new CodeDao();
CodeDao cdao4 = new CodeDao();
CodeDao cdao5 = new CodeDao();
CodeDao cdao6 = new CodeDao();

 cdao1.getCodeValues("ptst_eval_flag"); //KM

 if(flagAtt.equals("1") || flagAtt.equals("2"))
	 dEvalFlag = cdao1.toPullDown("evalFlag",StringUtil.stringToNum(evalFlag),"disabled");
 else
	 dEvalFlag = cdao1.toPullDown("evalFlag",StringUtil.stringToNum(evalFlag),withSelect);

 cdao2.getCodeValues("ptst_eval");

 if(estatAtt.equals("1") || estatAtt.equals("2"))
	 dEvalStat = cdao2.toPullDown("evalStat",StringUtil.stringToNum(evalStat),"disabled");
 else
	 dEvalStat = cdao2.toPullDown("evalStat",StringUtil.stringToNum(evalStat),withSelect);

 cdao3.getCodeValues("ptst_ineval");

 if(inestatAtt.equals("1") || inestatAtt.equals("2"))
	 dInevalStat = cdao3.toPullDown("inevalStat",StringUtil.stringToNum(inevalStat),"disabled");
 else
	 dInevalStat = cdao3.toPullDown("inevalStat",StringUtil.stringToNum(inevalStat),withSelect);

 cdao4.getCodeValues("ptst_survival");

 if(surstatAtt.equals("1") || surstatAtt.equals("2"))
	 dSurvival = cdao4.toPullDown("survival",StringUtil.stringToNum(survival),"disabled");
 else
	 dSurvival = cdao4.toPullDown("survival",StringUtil.stringToNum(survival),withSelect);


 cdao5.getCodeValues("pat_dth_cause");
// dDthCause = cdao5.toPullDown("dthCause",StringUtil.stringToNum(dthCause),withSelect);
 ArrayList causeId = new ArrayList();
 causeId = cdao5.getCId();

 ArrayList causeDesc = new ArrayList();
 causeDesc = cdao5.getCDesc();

 int selDthCause = 0;
	StringBuffer strCause = new StringBuffer();

		Integer dthIdTemp = null;
		int dthIdSelTemp = 0;

		selDthCause = StringUtil.stringToNum(dthCause);

	if(codAtt.equals("1") || codAtt.equals("2"))
		strCause.append("<SELECT NAME='dthCause' disabled >") ;
	else
		strCause.append("<SELECT NAME='dthCause'  >") ;

	strCause.append("<OPTION value=''> "+LC.L_Select_AnOption/*Select an Option*****/+" </OPTION>") ;

		for (int dcounter = 0; dcounter  < causeId.size()  ; dcounter++){
			dthIdTemp = (Integer) causeId.get(dcounter);
			if (selDthCause == dthIdTemp.intValue())
			{
				strCause.append("<OPTION value = "+ causeId.get(dcounter)+" SELECTED  >" + causeDesc.get(dcounter)+ "</OPTION>");
			}
			else
			{
				strCause.append("<OPTION value = "+ causeId.get(dcounter)+">" + causeDesc.get(dcounter)+ "</OPTION>");
			}


		}

		strCause.append("</SELECT>");


		dDthCause = strCause.toString();


 cdao6.getCodeValues("ptst_dth_stdrel");
// dDthRelStd = cdao6.toPullDown("dDthRelStd",StringUtil.stringToNum(dDthRelStd),withSelect);
 ArrayList relCauseId = new ArrayList();
 relCauseId = cdao6.getCId();
 ArrayList relCauseDesc = new ArrayList();
 relCauseDesc = cdao6.getCDesc();

 int selRelDthCause = 0;
 StringBuffer strRelCause = new StringBuffer();
		Integer relDthIdTemp = null;
		int relDthIdSelTemp = 0;
		selRelDthCause = StringUtil.stringToNum(dDthRelStd);

		//JM: 31Aug2010: #3356
		String hideCode = "";

		if(deathRelAtt.equals("1") || deathRelAtt.equals("2"))
			strRelCause.append("<SELECT NAME='dDthRelStd' disabled >") ;
		else
			strRelCause.append("<SELECT NAME='dDthRelStd'  >") ;

		strRelCause.append("<OPTION value= ''> "+LC.L_Select_AnOption/*Select an Option*****/+" </OPTION>") ;
		for (int rcounter = 0; rcounter  < relCauseId.size()  ; rcounter++){
			relDthIdTemp = (Integer) relCauseId.get(rcounter);
			hideCode = cdao6.getCodeHide(rcounter);
			if (selRelDthCause == relDthIdTemp.intValue())
			{
				strRelCause.append("<OPTION value = "+ relCauseId.get(rcounter)+" SELECTED  >" + relCauseDesc.get(rcounter)+ "</OPTION>");
			}
			else
			{
				if (! StringUtil.isEmpty(hideCode) && (!hideCode.equals("Y"))){
					strRelCause.append("<OPTION value = "+ relCauseId.get(rcounter)+">" + relCauseDesc.get(rcounter)+ "</OPTION>");
				}
			}

		}

		strRelCause.append("</SELECT>");

		ddDthRelStd = strRelCause.toString();


	if(othDthCause == null || othDthCause.equals("null"))
		othDthCause = "";

		if(othDthCauseRel == null || othDthCauseRel.equals("null"))
		othDthCauseRel = "";%>

		<p id="EvalStatusPTag" class = "sectionHeadings" style="display:block;" ><%=LC.L_Evaluable_Status%><%--Evaluable Status*****--%></p>
	<table id="EvalStatusTable" width="1000" cellspacing="0" cellpadding="0" style="display:block;" >


	<tr>
		<% int evalStatusFieldCount = 0;%>
	 <%if (hashPgCustFld.containsKey("evaluableflag")) {
			int fldNumFlag = Integer.parseInt((String)hashPgCustFld.get("evaluableflag"));
			String flagMand = ((String)cdoPgField.getPcfMandatory().get(fldNumFlag));
			String flagLable = ((String)cdoPgField.getPcfLabel().get(fldNumFlag));
			flagAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumFlag));


			if(flagAtt == null) flagAtt ="";
			if(flagMand == null) flagMand ="";


			if(!flagAtt.equals("0")) {
				evalStatusFieldCount = evalStatusFieldCount+1;

			if(flagLable !=null){
			%> <td width="20%">
			<%=flagLable%>
			<%} else {%> <td width="20%">
			<%=LC.L_Evaluable_Flag%><%--Evaluable Flag*****--%>
			<%}

			if (flagMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomflag">* </FONT>
			<% } %>

    </td>
     <td>

	<%=dEvalFlag%>
	<%if(flagAtt.equals("1")) {%>
		<input type="hidden" name="evalFlag" value="">
	<%} else if (flagAtt.equals("2")) {%>
		<input type="hidden" name="evalFlag" value="<%=evalFlag%>">
	<%}%>
    </td>
	<%} else if(flagAtt.equals("0")) {%>

	<input type="hidden" name="evalFlag" value="<%=evalFlag%>">
	<%}} else {
		evalStatusFieldCount = evalStatusFieldCount+1; %>

		<td width="20%"><%=LC.L_Evaluable_Flag%><%--Evaluable Flag*****--%></td><td><%=dEvalFlag%></td>

    <%}%>

  </tr>


	<tr>
	 <%if (hashPgCustFld.containsKey("evaluablestatus")) {
			int fldNumEstat = Integer.parseInt((String)hashPgCustFld.get("evaluablestatus"));
			String estatMand = ((String)cdoPgField.getPcfMandatory().get(fldNumEstat));
			String estatLable = ((String)cdoPgField.getPcfLabel().get(fldNumEstat));
			estatAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumEstat));


			if(estatAtt == null) estatAtt ="";
			if(estatMand == null) estatMand ="";


			if(!estatAtt.equals("0")) {
				evalStatusFieldCount = evalStatusFieldCount+1;

			if(estatLable !=null){
			%> <td width="20%">
			<%=estatLable%>
			<%} else {%> <td width="20%">
			<%=LC.L_Evaluable_Status%><%--Evaluable Status*****--%>
			<%}

			if (estatMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomestat">* </FONT>
			<% } %>

    </td>
     <td>

	<%=dEvalStat%>
	<%if(estatAtt.equals("1")) {%>
		<input type="hidden" name="evalStat" value="">
	<%} else if (estatAtt.equals("2")){%>
		<input type="hidden" name="evalStat" value="<%=evalStat%>">
	<%}%>
    </td>
	<%} else if(estatAtt.equals("0")) {%>

	<input type="hidden" name="evalStat" value=<%=evalStat%> >
	<%}} else {
			evalStatusFieldCount = evalStatusFieldCount+1;
		%>

		<td width="20%"><%=LC.L_Evaluable_Status%><%--Evaluable Status*****--%></td><td><%=dEvalStat%></td>

    <%}%>

  </tr>

	<tr>
	 <%if (hashPgCustFld.containsKey("inevaluablestatus")) {
			int fldNumInstat = Integer.parseInt((String)hashPgCustFld.get("inevaluablestatus"));
			String inestatMand = ((String)cdoPgField.getPcfMandatory().get(fldNumInstat));
			String inestatLable = ((String)cdoPgField.getPcfLabel().get(fldNumInstat));
			inestatAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumInstat));


			if(inestatAtt == null) inestatAtt ="";
			if(inestatMand == null) inestatMand ="";


			if(!inestatAtt.equals("0")) {
				evalStatusFieldCount = evalStatusFieldCount+1;

			if(inestatLable !=null){
			%> <td width="20%">
			<%=inestatLable%>
			<%} else {%> <td width="20%">
			<%=LC.L_Inevaluable_Status%><%--Inevaluable Status*****--%>
			<%}

			if (inestatMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustominestat">* </FONT>
			<% } %>

    </td>
     <td>

	<%=dInevalStat%>
	<%if(inestatAtt.equals("1")) {%>
		<input type="hidden" name="inevalStat" value="">
	<%} else if (inestatAtt.equals("2")){%>
		<input type="hidden" name="inevalStat" value="<%=inevalStat%>">
	<%}%>
    </td>
	<%} else if(inestatAtt.equals("0")) {%>

	<input type="hidden" name="inevalStat" value=<%=inevalStat%> >
	<%}} else {
		evalStatusFieldCount = evalStatusFieldCount+1;
		%>

		<td width="20%"><%=LC.L_Inevaluable_Status%><%--Inevaluable Status*****--%></td><td><%=dInevalStat%></td>

    <%}%>

  </tr>


</table>

<input id="evalStatusFieldCount" type="hidden" value="<%=evalStatusFieldCount%>" name="evalStatusFieldCount">

<% int patstatFieldCount=0;%>

<p class = "sectionHeadings" id="patstatPTag" style="display:block;" ><%=LC.L_Patient_Status%><%--Patient Status*****--%></p>
	<table id = "patstatTable" width="1000" cellspacing="0" cellpadding="0" style="display:block;" >
	<tr>
	 <%if (hashPgCustFld.containsKey("survivalstatus")) {
			int fldNumSurstat = Integer.parseInt((String)hashPgCustFld.get("survivalstatus"));
			String surstatMand = ((String)cdoPgField.getPcfMandatory().get(fldNumSurstat));
			String surstatLable = ((String)cdoPgField.getPcfLabel().get(fldNumSurstat));
			surstatAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumSurstat));


			if(surstatAtt == null) surstatAtt ="";
			if(surstatMand == null) surstatMand ="";


			if(!surstatAtt.equals("0")) {
				patstatFieldCount = patstatFieldCount+1;

			if(surstatLable !=null){
			%> <td width="20%">
			<%=surstatLable%>
			<%} else {%> <td width="20%">
			<%=LC.L_Survival_Status%><%--Survival Status*****--%>
			<%}

			if (surstatMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomsurstat">* </FONT>
			<% } %>

    </td>
     <td>

	<%=dSurvival%>
	<%if(surstatAtt.equals("1")) {%>
		<input type="hidden" name="survival" value="">
	<%} else if (surstatAtt.equals("2")){%>
		<input type="hidden" name="survival" value="<%=survival%>">
	<%}%>
    </td>
	<%} else if(surstatAtt.equals("0")) {%>

	<input type="hidden" name="survival" value=<%=survival%> >
	<%}} else {
		patstatFieldCount = patstatFieldCount+1;

		%>

		<td width="20%"><%=LC.L_Survival_Status%><%--Survival Status*****--%></td><td><%=dSurvival%></td>

    <%}%>

  </tr>

	<tr>
		<%
				if (hashPgCustFld.containsKey("dod")) {

				int fldNumDod = Integer.parseInt((String)hashPgCustFld.get("dod"));
				String dodMand = ((String)cdoPgField.getPcfMandatory().get(fldNumDod));
				String dodLable = ((String)cdoPgField.getPcfLabel().get(fldNumDod));
				String dodAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumDod));

				disableStr ="";
				readOnlyStr ="";

				if(dodAtt == null) dodAtt ="";
  				if(dodMand == null) dodMand ="";

				if(!dodAtt.equals("0")) {
					patstatFieldCount = patstatFieldCount+1;

				if(dodLable !=null){
				%>
				<td width="20%">
				 <%=dodLable%>
				<%} else {%> <td width="20%">
				  <%=LC.L_Date_OfDeath%><%--Date of Death*****--%>
				<% }


			   if (dodMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomdod">* </FONT>
		 	   <% }
			   %>

			   <%if(dodAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }
				 else if (dodAtt.equals("2") ) {
					 readOnlyStr = "readonly"; }

  		       %>

 			  </td>
			  <td>
			  <%-- INF-20084 Datepicker-- AGodara --%>
			  <%
			  //KM-31July08
			  	if(StringUtil.isEmpty(dodAtt)){%>
					<input type="text" name="deathDate" class="datefield" size = 15 MAXLENGTH = 50	value="<%=deathDate%>">
				<%} else if(dodAtt.equals("1") || dodAtt.equals("2")) { %>
					<input type="text" name="deathDate" size = 15 MAXLENGTH = 50 <%=readOnlyStr%> <%=disableStr%>	value="<%=deathDate%>" >
				<%}%>
			  </td>
			<%} else { %>
				 <input type="hidden" name="deathDate" id ="deathHidden" size = 15 MAXLENGTH = 50	value=<%=deathDate%> >
		<% }}  else {
				patstatFieldCount = patstatFieldCount+1;
		%>
				<td width="20%"><%=LC.L_Date_OfDeath%><%--Date of Death*****--%></td>
				<td><input type="text" name="deathDate" class="datefield" size = 15 MAXLENGTH = 50	value=<%=deathDate%> >
 			</td>
		<%}%>
		</tr>


	<tr>
	 <%if (hashPgCustFld.containsKey("cod")) {
			int fldNumCod = Integer.parseInt((String)hashPgCustFld.get("cod"));
			String codMand = ((String)cdoPgField.getPcfMandatory().get(fldNumCod));
			String codLable = ((String)cdoPgField.getPcfLabel().get(fldNumCod));
			codAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumCod));


			if(codAtt == null) codAtt ="";
			if(codMand == null) codMand ="";


			if(!codAtt.equals("0")) {
				patstatFieldCount = patstatFieldCount+1;

			if(codLable !=null){
			%> <td width="20%">
			<%=codLable%>
			<%} else {%> <td width="20%">
			<%=LC.L_Death_Cause%><%--Cause of Death*****--%>
			<%}

			if (codMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomcod">* </FONT>
			<% } %>

    </td>
     <td>

	<%=dDthCause%>
	<%if(codAtt.equals("1")) {%>
		<input type="hidden" name="dthCause" value="">
	<%} else if (codAtt.equals("2")){%>
		<input type="hidden" name="dthCause" value="<%=dthCause%>">
	<%}%>
    </td>
	<%} else if(codAtt.equals("0")) {%>

	<input type="hidden" name="dthCause" value=<%=dthCause%> >
	<%}} else {
		patstatFieldCount = patstatFieldCount+1;

		%>

		<td width="20%"><%=LC.L_Death_Cause%><%--Cause of Death*****--%></td><td><%=dDthCause%></td>

    <%}%>

  </tr>

   <tr>
	<%if (hashPgCustFld.containsKey("specifycause")) {

			int fldNumSpecCause = Integer.parseInt((String)hashPgCustFld.get("specifycause"));
			String specCauseMand = ((String)cdoPgField.getPcfMandatory().get(fldNumSpecCause));
			String specCauseLable = ((String)cdoPgField.getPcfLabel().get(fldNumSpecCause));
			String specCauseAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumSpecCause));

			disableStr ="";
			readOnlyStr ="";
			if(specCauseAtt == null) specCauseAtt ="";
			if(specCauseMand == null) specCauseMand ="";


			if(!specCauseAtt.equals("0")) {
				patstatFieldCount = patstatFieldCount+1;

			if(specCauseLable !=null){
			%><td width="20%">
			<%=specCauseLable%>
			<%} else {%> <td width="20%">
			<%=LC.L_Specify_Cause%><%--Specify Cause*****--%>
			<%}

			if (specCauseMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomspeccause">* </FONT>
			<% }
		 %>

		  <%if(specCauseAtt.equals("1")) {
			 disableStr = "disabled class='readonly-input'"; }
		     else if (specCauseAtt.equals("2") ) {
			 readOnlyStr = "readonly"; }
		 %>

    </td>
    <td>
		<input type = text name="othDthCause" <%=disableStr%>  <%=readOnlyStr%> value="<%=othDthCause%>">

	</td>
    	<% } else {

    	%>

	<input type = hidden name="othDthCause" value="<%=othDthCause%>">

	<% } }

	else {
		patstatFieldCount = patstatFieldCount+1;

		 %>

		<td width="20%"><%=LC.L_Specify_Cause%><%--Specify Cause*****--%></td><td><input type = text name="othDthCause" value="<%=othDthCause%>"> </td>


  <%}%>

</tr>


	<tr>
	 <%if (hashPgCustFld.containsKey("deathrelated")) {
			int fldNumDeathRel = Integer.parseInt((String)hashPgCustFld.get("deathrelated"));
			String deathRelMand = ((String)cdoPgField.getPcfMandatory().get(fldNumDeathRel));
			String deathRelLable = ((String)cdoPgField.getPcfLabel().get(fldNumDeathRel));
			deathRelAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumDeathRel));


			if(deathRelAtt == null) deathRelAtt ="";
			if(deathRelMand == null) deathRelMand ="";


			if(!deathRelAtt.equals("0")) {
				patstatFieldCount = patstatFieldCount+1;

			if(deathRelLable !=null){
			%> <td width="20%">
			<%=deathRelLable%>
			<%} else {%> <td width="20%">
			<%=MC.M_Dth_RelToStd%><%--Death Related to Study*****--%>
			<%}

			if (deathRelMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomdeathrel">* </FONT>
			<% } %>

    </td>
     <td>

	<%=ddDthRelStd%>
	<%if(deathRelAtt.equals("1")) {%>
		<input type="hidden" name="dDthRelStd" value="">
	<%} else if (deathRelAtt.equals("2")) {%>
		<input type="hidden" name="dDthRelStd" value="<%=dDthRelStd%>">
	<%}%>
    </td>
	<%} else if(deathRelAtt.equals("0")) {%>

	<input type="hidden" name="dDthRelStd" value="<%=dDthRelStd%>">
	<%}} else {
		patstatFieldCount = patstatFieldCount+1;

		%>

		<td width="20%"><%=MC.M_Dth_RelToStd%><%--Death Related to Study*****--%></td><td><%=ddDthRelStd%></td>

    <%}%>

  </tr>


	<tr>
	<%if (hashPgCustFld.containsKey("reasonofdeathrel")) {

			int fldNumReasonOf = Integer.parseInt((String)hashPgCustFld.get("reasonofdeathrel"));
			String reasonOfMand = ((String)cdoPgField.getPcfMandatory().get(fldNumReasonOf));
			String reasonOfLable = ((String)cdoPgField.getPcfLabel().get(fldNumReasonOf));
			String reasonOfAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumReasonOf));

			disableStr ="";
			readOnlyStr ="";
			if(reasonOfAtt == null) reasonOfAtt ="";
			if(reasonOfMand == null) reasonOfMand ="";


			if(!reasonOfAtt.equals("0")) {
				patstatFieldCount = patstatFieldCount+1;

			if(reasonOfLable !=null){
			%><td width="20%">
			<%=reasonOfLable%>
			<%} else {%> <td width="20%">
			<%=MC.M_DthReason_ToStd%><%--Reason of Death Related to Study*****--%>
			<%}

			if (reasonOfMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomreasonof">* </FONT>
			<% }
		 %>

		  <%if(reasonOfAtt.equals("1")) {
			 disableStr = "disabled class='readonly-input'"; }
		     else if (reasonOfAtt.equals("2") ) {
			 readOnlyStr = "readonly"; }
		 %>

    </td>
    <td>
		<input type = text name="othDthCauseRel" <%=disableStr%>  <%=readOnlyStr%> value="<%=othDthCauseRel%>">

	</td>
    <% }    else { %>

	<input type = hidden name="othDthCauseRel"  value="<%=othDthCauseRel%>">

	<% }}

	else {
			patstatFieldCount = patstatFieldCount+1;

		 %>

		<td width="20%"><%=MC.M_DthReason_ToStd%><%--Reason of Death Related to Study*****--%></td><td><input type = text name="othDthCauseRel" value="<%=othDthCauseRel%>"></td>

  <%}%>

</tr>

<input id="patStatusFieldCount" type="hidden" value="<%=patstatFieldCount%>" name="patStatusFieldCount">

<script>
	if (document.getElementById('evalStatusFieldCount'))
	{
		var oevalStatusFieldCount;

		oevalStatusFieldCount	= document.getElementById('evalStatusFieldCount');
		if (oevalStatusFieldCount.value == 0)
		{
		 var oEvalStatusPTag = document.getElementById('EvalStatusPTag');
		 var oEvalStatusTable = document.getElementById('EvalStatusTable');

		 if (oEvalStatusPTag)
		 {
		 	oEvalStatusPTag.style.display ="none";
		 }


		 if (oEvalStatusTable)
		 {
		 	oEvalStatusTable.style.display ="none";
		 }

		}
	}

	if (document.getElementById('patStatusFieldCount'))
	{
		var opatStatusFieldCount;

		opatStatusFieldCount= document.getElementById('patStatusFieldCount');
		if (opatStatusFieldCount.value == 0)
		{
		 var opatstatPTag = document.getElementById('patstatPTag');
		 var opatstatTable = document.getElementById('patstatTable');

		 if (opatstatPTag)
		 {
		 	opatstatPTag.style.display ="none";
		 }


		 if (opatstatTable)
		 {
		 	opatstatTable.style.display ="none";
		 }

		}
	}
	if (document.getElementById('screenFieldCount'))
	{
		var oscreenFieldCount;

		oscreenFieldCount = document.getElementById('screenFieldCount');
		if (oscreenFieldCount.value == 0)
		{
		 var oscreenPTag = document.getElementById('screenPTag');
		 var oscreenTable = document.getElementById('screenTable');

		 if (oscreenPTag)
		 {
		 	oscreenPTag.style.display ="none";
		 }


		 if (oscreenTable)
		 {
		 	oscreenTable.style.display ="none";
		 }

		}
	}
	if (document.getElementById('enrollFieldCount'))
	{
		var oenrollFieldCount;

		oenrollFieldCount = document.getElementById('enrollFieldCount');
		if (oenrollFieldCount.value == 0)
		{
		 var oenrollPTag = document.getElementById('enrollPTag');
		 var oenrollTable = document.getElementById('enrollTable');

		 if (oenrollPTag)
		 {
		 	oenrollPTag.style.display ="none";
		 }


		 if (oenrollTable)
		 {
		 	oenrollTable.style.display ="none";
		 }

		}
	}
	if (document.getElementById('consentFieldCount'))
	{
		var oconsentFieldCount;

		oconsentFieldCount = document.getElementById('consentFieldCount');
		if (oconsentFieldCount.value == 0)
		{
		 var oconsentPTag = document.getElementById('consentPTag');
		 var oconsentTable = document.getElementById('consentTable');

		 if (oconsentPTag)
		 {
		 	oconsentPTag.style.display ="none";
		 }


		 if (oconsentTable)
		 {
		 	oconsentTable.style.display ="none";
		 }

		}
	}
	if (document.getElementById('followFieldCount'))
	{
		var ofollowFieldCount;

		ofollowFieldCount = document.getElementById('followFieldCount');
		if (ofollowFieldCount.value == 0)
		{
		 var ofollowPTag = document.getElementById('followPTag');
		 var ofollowTable = document.getElementById('followTable');

		 if (ofollowPTag)
		 {
		 	ofollowPTag.style.display ="none";
		 }


		 if (ofollowTable)
		 {
		 	ofollowTable.style.display ="none";
		 }

		}
	}
	if (document.getElementById('aInfoFieldCount'))
	{
		var oaInfoFieldCount;

		oaInfoFieldCount = document.getElementById('aInfoFieldCount');
		if (oaInfoFieldCount.value == 0)
		{
		 var oaInfoPTag = document.getElementById('aInfoPTag');
		 var oaInfoTable = document.getElementById('aInfoTable');
		 var oaInfoTable1 = document.getElementById('aInfoTable1');
		 var oaInfoTable2 = document.getElementById('aInfoTable2');
		 var oaInfoTable3 = document.getElementById('aInfoTable3');

		 if (oaInfoPTag)
		 {
		 	oaInfoPTag.style.display ="none";
		 }


		 if (oaInfoTable)
		 {
		 	oaInfoTable.style.display ="none";
		 	oaInfoTable1.style.display ="none";
		 	oaInfoTable2.style.display ="none";
		 	oaInfoTable3.style.display ="none";
		 }

		}
	}
</script>

<% if (changeStatusMode.equals("no")){%>
	<tr>
      <td width="20%"><%=LC.L_ReasonForChangeFDA%>
      	<%if (fdaStudy == 1){%>&nbsp;<FONT class="Mandatory">* </FONT><%} %>
      </td>
	  <td width="60%"><textarea id="remarks" name="remarks" rows="3" cols="30" MAXLENGTH="4000"></textarea></td>
  	</tr>
<%} %>
</table>

<% String showSubmit="";
if(	(orgRight >= 6 && pageRight >= 6 && changeStatusMode.equals("no")) || ( (orgRight == 5 ||  orgRight == 7) && (pageRight == 5 ||  pageRight == 7) && changeStatusMode.equals("yes") ) )
{ showSubmit ="Y";} else {showSubmit="N";}%>
<table>
	<tr>
		<td>
			<jsp:include page="submitBar.jsp" flush="true">
				<jsp:param name="displayESign" value="Y"/>
				<jsp:param name="formID" value="statusForm"/>
				<jsp:param name="showDiscard" value="N"/>
				<jsp:param name="showSubmit" value="<%=showSubmit%>"/>
			</jsp:include>
		</td>
		<td>
			<button type='submit'onclick="return fnClickAddAnother(event);"><%=LC.L_Submit_AddAnother%></button>
		</td>
	</tr>
</table>

	<%//--May enhn%>


    <br>

<input type=hidden name="presurvival" value="<%=presurvival%>">
<input type=hidden name="deadPerStat" value="<%=deadPerStat%>">
<input type=hidden name="perStatus" value="<%=perStatus%>">
<input type=hidden name="deadPatStat" value="<%=deadPatStat%>">
<input type=hidden name="patstatusdisable" value="<%=patStudyStat%>">
<input type=hidden name="statusDisabled">
<input type=hidden name="patientDefSite" value = "<%=patientDefSite%>">
<!-- Added by Manimaran for September Enhancement(S8) -->
<input type="hidden" name="currStat" value="">
 <input type="hidden" name="currentStatId" value=<%=currentStatId%>>
<input type=hidden name="patStudiesEnrollingOrg" value="<%=patStudiesEnrollingOrg%>">
<input type=hidden id="addOneMoreStat" name="addOneMoreStat" value="0"/>





  </Form>

  <%



	} //end of if body for page right



else



{



%>

  <jsp:include page="accessdenied.jsp" flush="true"/>

  <%



} //end of else body for page right



}//end of if body for session



else



{



%>

 <jsp:include page="timeout_childwindow.jsp" flush="true"/>

  <%



}



%>

  <div class = "myHomebottomPanel">
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

</Div>
</body>

</html>



