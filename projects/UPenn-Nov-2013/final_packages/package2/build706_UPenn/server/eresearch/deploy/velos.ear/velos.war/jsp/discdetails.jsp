<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></HEAD>
<jsp:include page="popupJS.js" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>
<SCRIPT>

 function  validate(formobj)

 {

	if(!(validate_col('Discontinuation  Date',formobj.discDate))) return false
    if (!(validate_col('Discontinuation Reason',formobj.discReason))) return false
    var mVal = document.getElementById("mode").value;
	if (mVal){
	 	var fdaVal = document.getElementById("fdaStudy").value;
		if (fdaVal == "1" && mVal == "M"){
			if (  !(validate_col('<%=LC.L_ReasonForChangeFDA%>',formobj.remarks))) return false;
		}
	}
	if (!(validate_col('E-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {

		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/

	formobj.eSign.focus();

	return false;

   }

}
</SCRIPT>


<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>

<BODY>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<br>

<DIV class="popDefault"  id="div1">

<%@ page language = "java" import = "com.velos.eres.business.common.*,com.velos.eres.service.util.*,java.util.*,com.velos.eres.business.patProt.*,com.velos.eres.web.patStudyStat.*,com.velos.esch.business.common.*"%>
<%@ page language = "java" import = "com.velos.eres.web.study.StudyJB"%>

<jsp:include page="include.jsp" flush="true"/>

<%

//declare variables



  int patProtPK  = 0;	

  String mode = null;

  String  accStartDt="";

  int saved = 0;

   mode = request.getParameter("mode");

   String protocolId=request.getParameter("protocolId");
   String patientId = request.getParameter("patientId");
   String protocolStDt = request.getParameter("protStDate");
   
   String studyId= request.getParameter("studyId");
   int fdaStudy = 0;
   {
	StudyJB studyJB = new StudyJB();
	studyJB.setId(StringUtil.stringToNum(studyId));
	studyJB.getStudyDetails();
	fdaStudy = ("1".equals(studyJB.getFdaRegulatedStudy()))? 1:0;
   }
   String prevProtocol = request.getParameter("prevProtocolId");

   int personPK = EJBUtil.stringToNum(request.getParameter("personPK"));
   patProtPK = EJBUtil.stringToNum(request.getParameter("patProtId"));

   String eSign = request.getParameter("eSign");
   String newSelDay = null;
	
	 String	dayFlag = request.getParameter("dayflag");

	if (dayFlag.equals("0"))

		{

			newSelDay = null;

		}

		else

		{

			newSelDay = request.getParameter("selDay");

			if ((newSelDay == null || "null".equals(newSelDay)) || (newSelDay.trim().equals("")))
			{

				newSelDay = null;

			}

		}   

				

	String prevSelDay = request.getParameter("prevSelDay");

		

	HttpSession tSession = request.getSession(true); 

	

	if (sessionmaint.isValidSession(tSession))



	{

 
		String displayStr = "";

		String displayStrNew = "";

  		 

		String oldESign = (String) tSession.getValue("eSign");

		String status = "";

		String statusDesc = "";

	

		

	if(!oldESign.equals(eSign)) {

%>

  <jsp:include page="incorrectesign.jsp" flush="true"/>	

<%

	} else {



	String ipAdd = (String) tSession.getValue("ipAdd");

	String usr = null;

	usr = (String) tSession.getValue("userId");





	int id = 0;

	 

	if (protocolId == null)	 protocolId="";

	if (prevProtocol.equals("null")) prevProtocol="";

	if (protocolStDt == null) protocolStDt="";

		////////////////////////////////////////vc



		status = request.getParameter("statid");

		statusDesc = request.getParameter("statDesc");

		

		String prevprotStDate = request.getParameter("prevprotStDate");		





%>





<form name = "disc" id="discschdet" METHOD="POST" action="updatepatenrolldetails.jsp" onsubmit="if (validate(document.disc)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">



<Input type="hidden" id="mode" name="mode" value="<%=mode%>">
<Input type="hidden" id="fdaStudy" value="<%=fdaStudy%>">
<Input type="hidden" name="protocolId" value=<%=protocolId%>>
<Input type="hidden" name="patientId" value=<%=patientId%>>
<Input type="hidden" name="protStDate" value=<%=protocolStDt%>>
<Input type="hidden" name="studyId" value=<%=studyId%>>
<Input type="hidden" name="prevProtocolId" value=<%=prevProtocol%>>
<Input type="hidden" name="prevprotStDate" value=<%=prevprotStDate%>>
<input type="hidden" name="personPK" Value="<%=personPK%>">
<Input type="hidden" name="patProtId" value=<%=patProtPK%>>
<Input type="hidden" name="selDay" value=<%=newSelDay%>>
<Input type="hidden" name="prevSelDay" value=<%=prevSelDay%>>
<Input type="hidden" name="calledFrom" value="disc">

<BR>	


<p class = "defComments"> <font class = "mandatory"><%=displayStrNew%> </font></p>	



<table width="500" cellspacing="0" cellpadding="0" >

    <tr>

	<td colspan = 2>
	<br>
	<p class = "sectionHeadings" ><%=LC.L_Pcol_DiscontDets%><%--Protocol Discontinuation  Details*****--%></p><br>

	</td>

	</tr>
  <tr> 
    <td width="30%">
       <%=LC.L_Discont_Date%><%--Discontinuation Date*****--%> <FONT class="Mandatory">* </FONT>
    </td>
<%-- INF-20084 Datepicker-- AGodara --%>
    <td><input type=text name="discDate" class="datefield" size = 10 MAXLENGTH = 10 	value='' READONLY></td></tr>
<tr> 

    <td width="30%">

	<%=LC.L_Discont_Reason%><%--Discontinuation Reason*****--%> <FONT class="Mandatory">* </FONT>

    </td>
    <td>

	<textarea name="discReason" rows=5 cols=50 MAXLENGTH = 2000></textarea>

    </td>
</tr>
<tr>
	<td width="30%">
		<%=LC.L_ReasonForChangeFDA%> <%if (fdaStudy == 1){%><FONT class="Mandatory">* </FONT><%} %>
    </td>
    <td>
		<textarea id="remarks" name="remarks" rows="3" cols="50" MAXLENGTH="4000"></textarea>
    </td>
</tr>


<%--<tr>

    <td colspan = 2>

	<Input type="checkbox" name="retalerts" CHECKED> Retain current Alert and Notification Settings for this <!--<%=LC.Pat_Patient%> 

    </td>

</tr> --%>

</table>

<br>

<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="discschdet"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>
</form>

<%

}//end of if for eSign check

}//end of if body for session

else

{

%>

<jsp:include page="timeout.html" flush="true"/> 

<%

}

%>

</div>
<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</BODY>

</HTML>
