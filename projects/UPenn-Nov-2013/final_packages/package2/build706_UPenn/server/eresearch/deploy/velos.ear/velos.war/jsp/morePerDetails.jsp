<%!
static final String Str_input = "input", Str_lookup = "lookup", Str_chkbox = "chkbox", Str_checkbox = "checkbox", Str_dropdown = "dropdown",
	Str_splfld = "splfld", Str_date = "date", Str_RO_input = "readonly-input", Str_Hidden_input = "hidden-input";
static final String Str_selection_single = "single", Str_selection_multi = "multi";
public static final String Str_tarea="textarea";
public static final String Str_tarea_RO="readonly-textarea";

%>
<!-- saved from url=(0022)http://internet.e-mail -->
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.web.person.PersonJB" %>
<%@page import="com.velos.eres.web.grpRights.GrpRightsJB" %>
<%@page import="com.velos.eres.web.userSite.UserSiteJB" %>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,java.lang.reflect.*"%>
<%@ page import="com.velos.eres.service.util.LC, com.velos.eres.service.util.StringUtil"%>
<%@ page import="org.json.*"%>
<jsp:useBean id="pId" scope="request" class="com.velos.eres.web.perId.PerIdJB"/>
<%
	HttpSession tSession = request.getSession(true);
	 if (sessionmaint.isValidSession(tSession))
	{
	 %>
	<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
	 String accountId = (String) tSession.getAttribute("accountId");
	 String userIdFromSession = (String) tSession.getAttribute("userId");
	 String perId = request.getParameter("perId");
	int pageRights = StringUtil.stringToNum(request.getParameter("pageRights"));

	String defUserGroup = (String) tSession.getAttribute("defUserGroup");

	String strPerIdTypesDesc ;
 	String strAlternateId ;
  	String strRecordType ;
  	Integer intPerIdType;
   	Integer intId;
	String disptype="";
	String dataType="";
	String ddStr="";
	String dispdata="";

	ArrayList perIdType  = new ArrayList();
	ArrayList perIdTypesDesc = new ArrayList();
	ArrayList id = new ArrayList();
	ArrayList alternateIdKey = new ArrayList();
	ArrayList alternateId = new ArrayList();
	ArrayList recordType = new ArrayList();
	ArrayList dispTypes=new ArrayList();
	ArrayList dispData=new ArrayList();
	
	 //get patient ids
	PerIdDao pidDao = new PerIdDao();
	pidDao = pId.getPerIds(StringUtil.stringToNum(perId),defUserGroup);

	id = pidDao.getId();
	perIdType =  pidDao.getPerIdType();
	perIdTypesDesc = pidDao.getPerIdTypesDesc();
	alternateId = pidDao.getAlternateId();
	alternateIdKey = pidDao.getAlternateIdKey();
	recordType = pidDao.getRecordType ();
	dispTypes=pidDao.getDispType();
	dispData=pidDao.getDispData();

	if (!StringUtil.isEmpty(perId) && (StringUtil.stringToNum(perId) > 0)){	
		// Access rights check
		PersonJB personJB = new PersonJB();
		personJB.setPersonPKId(StringUtil.stringToNum(perId));
		personJB.getPersonDetails();
		int patCompleteDetailsRight = personJB.getPatientCompleteDetailsAccessRight(
				StringUtil.stringToNum(userIdFromSession),
				StringUtil.stringToNum(defUserGroup),
				StringUtil.stringToNum(perId));
		GrpRightsJB grpRights = (GrpRightsJB) tSession.getAttribute("GRights");
		pageRights = Integer.parseInt(grpRights.getFtrRightsByValue("MPATIENTS"));
		UserSiteJB userSiteJB = new UserSiteJB();
		int orgRight = userSiteJB.getUserPatientFacilityRight(
				 StringUtil.stringToNum(userIdFromSession),
				 StringUtil.stringToNum(perId));
		if (patCompleteDetailsRight < 4 || pageRights < 4 || orgRight < 4) {
			%>
			<jsp:include page="accessdenied.jsp" flush="true"/>
			<%
			 return;
		 }
	 }
%>
<table  id="morePerDetailsTable" class="basetbl" width="100%" border="0" cellspacing="2" cellpadding="2">
<%
	int counter = 0,cbcount=0;
	for (counter = 0; counter <= perIdType.size() -1 ; counter++)
		{
				strPerIdTypesDesc = (String) perIdTypesDesc.get(counter);
 	 			strAlternateId = (String) alternateId.get(counter);
 	 			intPerIdType = (Integer) perIdType.get(counter) ;
 	 			disptype=(String) dispTypes.get(counter);
				dispdata=(String) dispData.get(counter);

 	 			 %>
 	 			 <script>
 	 				morePatientDetFunctions.morePatientDetCodeJSON["<%=alternateIdKey.get(counter)%>"] = {
 	 						pk: "<%=intPerIdType%>", 
 	 						fieldType: '<%=(StringUtil.isEmpty(disptype))? Str_input : disptype%>'
 	 				};
 	 				//alert(morePatientDetFunctions.morePatientDetCodeJSON["<%=alternateIdKey.get(counter)%>"]);
 	 			 </script>
 	 			 <%

				if (disptype==null) disptype="";
 	 			disptype=disptype.toLowerCase();

				if (dispdata==null) dispdata="";
				if (disptype.equals(Str_dropdown)) {
					if (null != dispdata){
						dispdata = StringUtil.replaceAll(dispdata, "alternateId", "alternateId"+intPerIdType);
					}
				}

 	 			if (strAlternateId == null)
 	 				strAlternateId = "";

  	 			strRecordType = (String) recordType.get(counter);
   	 			intId = (Integer) id.get(counter) ;

			%>
			<%if (Str_Hidden_input.equals(disptype)){
				%>
				<input type="hidden" readonly name="alternateId<%=intPerIdType%>" value="<%=strAlternateId.trim()%>" size="25" maxlength="100"/>
				<input type = "hidden" name = "recordType<%=intPerIdType%>" value = "<%=strRecordType %>" >
				<input type = "hidden" name = "id<%=intPerIdType%>" value = "<%=intId%>" >
				<input type = "hidden" name = "perIdType" value = "<%=intPerIdType%>" >
				<input type = "hidden" name = "perId" value = "<%=perId%>" >
				<%
				continue;
			}	
			%>

	<tr>
		     <td class=tdDefault width="20%" >
				<%= strPerIdTypesDesc %>
			 </td>
		     <td class=tdDefault width="80%" > <%--[<%=alternateIdKey.get(counter)%>]&nbsp; --%>
			<%if (Str_lookup.equals(disptype)){ %>
			    <%//lookup
			    String lkpKeyworkStr = "";
			    int lkpPK = 0;
			    try{
				    JSONObject lookupJSON = new JSONObject(dispdata);
				    lkpPK = StringUtil.stringToNum(""+lookupJSON.get("lookupPK"));
				    if (lkpPK <= 0){
				    	System.out.println("Study More Details: invalid lookup PK");
				    	continue;
				    }

				    JSONArray mappingArray = (JSONArray)lookupJSON.get("mapping");
					for (int indx=0; indx < mappingArray.length(); indx++){
						JSONObject checkboxJSON = (JSONObject)mappingArray.get(indx);
				        try{
				        	String source = checkboxJSON.getString("source").trim();
				            String target = checkboxJSON.getString("target").trim();
				            if (StringUtil.isEmpty(source) || StringUtil.isEmpty(target)) continue;
	
				            target = ("alternateId".equals(target))? "alternateId"+intPerIdType : target;

				            if (!StringUtil.isEmpty(lkpKeyworkStr))
								lkpKeyworkStr += "~";
				            lkpKeyworkStr += target+"|"+source;
				        }catch(Exception e){
				        	System.out.println("Study More Details: invalid lookup mapping");
				        }
					}
					%>
					<input class='readonly-input' type="text" id="alternateId<%=intPerIdType%>" name="alternateId<%=intPerIdType%>" readonly value="<%=strAlternateId.trim()%>" size="25" maxlength="100"/>
					<%if (lkpPK > 0){
						String selection = "";
						try {
				    		selection = (String)lookupJSON.get("selection");
						} catch (Exception e){}
				    	selection = (StringUtil.isEmpty(selection))? Str_selection_single : selection;
				    	%>
				    	<%if (Str_selection_single.equals(selection)){%>
						<A id="alternateId<%=intPerIdType%>Link" href=# onClick="morePatientDetFunctions.openLookup(<%=lkpPK %>,'<%=lkpKeyworkStr%>')"><%=LC.L_Select%><%--Select*****--%></A>
						<%} %>
						<%if (Str_selection_multi.equals(selection)){%>
						<A id="alternateId<%=intPerIdType%>Link" href=# onClick="morePatientDetFunctions.openMultiLookup(<%=lkpPK %>,'<%=lkpKeyworkStr%>')"><%=LC.L_Select%><%--Select*****--%></A>
						<%} %>
					<%}
			    } catch (Exception e){
			    	//e.printStackTrace();
			    	System.out.println("Study More Details: lookup configuration error!");
			    }			   
			} else if (Str_chkbox.equals(disptype) || Str_checkbox.equals(disptype)){%>
				<%//checkbox 
				if (!StringUtil.isEmpty(dispdata)){
					//checkbox-group
					String[] strAlternateIds = strAlternateId.split(",");
			        try{
						JSONArray checkboxArray = new JSONArray(dispdata);

						for (int indx=0; indx < checkboxArray.length(); indx++){
							JSONObject checkboxJSON = (JSONObject)checkboxArray.get(indx);
					        try{
								String data = checkboxJSON.getString("data");
								String display = checkboxJSON.getString("display");
								String checked = "";
								if((","+strAlternateId+",").indexOf(","+data+",") > -1){
									checked = "checked";
								}
								if (indx % 5 == 0){
								%>
									<br>
								<%
								}
				             %>
								<input type="checkbox" id="alternateId<%=intPerIdType%>Checks<%=indx%>" name="alternateId<%=intPerIdType%>Checks" value="<%=data.trim()%>" onClick="morePatientDetFunctions.setValue4ChkBoxGrp(this,<%=intPerIdType%>)" size = "25" maxlength = "100" <%=checked%>> <%=display%>
							<%
					       	}catch(Exception e){}
						}
			        }catch(Exception e){
			        	//e.printStackTrace();
				    	System.out.println("Patient More Details: checkbox configuration error!");
			        }
					%>
					<input type="hidden" id="alternateId<%=intPerIdType%>" name="alternateId<%=intPerIdType%>" value="<%=strAlternateId.trim()%>" size = "25" maxlength = "100" >
					<%
				} else {
					cbcount=cbcount+1;
					%>
					<input type = "hidden" name = "alternateId<%=intPerIdType%>" value = "<%=strAlternateId.trim()%>" size = "25" maxlength = "100" >
					<%if ((strAlternateId.trim()).equals("Y")){%>
					 <input type="checkbox" name="alternate" value="<%=strAlternateId.trim()%>" onClick="morePatientDetFunctions.setValue(morePatientDetFunctions.formObj,<%=intPerIdType%>,<%=cbcount%>)" checked>
					<% }else{%>
					  <input type="checkbox"  name="alternate" value="<%=strAlternateId.trim()%>" onClick="morePatientDetFunctions.setValue(morePatientDetFunctions.formObj,<%=intPerIdType%>,<%=cbcount%>)">
					<%}%>
				<%} %>
			<%} else if ((disptype.toLowerCase()).equals("dropdown")) {
				//dropdown
				if (ddStr.length()==0) ddStr=(intPerIdType)+":"+strAlternateId;
					else ddStr=ddStr+"||"+(intPerIdType)+":"+strAlternateId;
				%>
				 <%=dispdata%>
			<%} else if ((disptype.toLowerCase()).equals("date")) {%>
				<%//Date input%>
				<%-- INF-20084 Datepicker-- AGodara --%>
				<input name="alternateId<%=intPerIdType%>" type="text" class="datefield" size="10" readOnly  value="<%=strAlternateId.trim()%>">
			<%}else if (Str_RO_input.equals(disptype)){%>
				<%//Read-only input %>
				<input type="text" class='readonly-input' readonly id="alternateId<%=intPerIdType%>" name="alternateId<%=intPerIdType%>" value="<%=strAlternateId.trim()%>" size="25" maxlength="100"/>
			<%} else if(Str_tarea.equals(disptype)){%>
				<textarea class="mdTextArea" name = "alternateId<%=intPerIdType%>"  rows="4" cols="50" ><%=strAlternateId.trim()%></textarea>
			<%} else if(Str_tarea_RO.equals(disptype)){%>
				<textarea class="mdTextArea" readonly name = "alternateId<%=intPerIdType%>"  rows="4" cols="50" ><%=strAlternateId.trim()%></textarea>
			<%} else {%>
				<input type = "text" name = "alternateId<%=intPerIdType%>" value = "<%=strAlternateId.trim()%>" size = "25" maxlength = "100" >
			<%}%>
				<input type = "hidden" name = "recordType<%=intPerIdType%>" value = "<%=strRecordType %>" >
				<input type = "hidden" name = "id<%=intPerIdType%>" value = "<%=intId%>" >
				<input type = "hidden" name = "perIdType" value = "<%=intPerIdType%>" >
				<input type = "hidden" name = "perId" value = "<%=perId%>" >
			 </td>
			 <td width="30%">&nbsp;</td>
	</tr>
	<%
		}
	%>
	<input type="hidden" name="cbcount" value=<%=cbcount%>>
	<input type="hidden" name="ddlist" value="<%=ddStr%>">
</table>
<%}else{%>
<jsp:include page="timeout_childwindow.jsp" flush="true"/>
<%}%>
