<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=LC.L_SvdRpt_Del%><%--Saved Reports Delete*****--%></title>
<%-- agodara 08Aug11: Removed for Bug #6728, already using Whichcss_skin.js in panel.jsp --%>
<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
}
</SCRIPT>

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<%@ page language = "java" import="java.util.*,com.velos.eres.service.util.*,com.velos.eres.service.util.EJBUtil"%>
<jsp:useBean id="savedRepB" scope="request" class="com.velos.eres.web.savedRep.SavedRepJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/> 

<% String src;
	src= request.getParameter("srcmenu");
%>

<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>  

<BODY> 
<br>

<DIV class="formDefault" id="div1">
<% 
String milestoneId= "";
String selectedTab = request.getParameter("selectedTab");
		
HttpSession tSession = request.getSession(true); 
 if (sessionmaint.isValidSession(tSession))	{

	String savedRepId= request.getParameter("savedRepId");
	String studyId = request.getParameter("studyId");
	int ret=0;		
	String delMode=request.getParameter("delMode");
	
	if (delMode == null) {
		delMode="final";
%>
	<FORM name="savedrepdelete" method="post" action="savedrepdelete.jsp" onSubmit="return validate(document.savedrepdelete)">
	<br><br>

	<P class="defComments"><%=MC.M_EsignToProc_WithRptDel%><%--Please enter e-Signature to proceed with Saved Reports Delete.*****--%></P>
		
	<TABLE width="100%" cellspacing="0" cellpadding="0" >
	
	<tr>
	   <td width="40%">
		<%=LC.L_Esignature%><%--e-Signature*****--%> <FONT class="Mandatory">* </FONT> 
	   </td>
	   <td width="60%">
		<input type="password" name="eSign" maxlength="8" SIZE=15 autocomplete="off">
	   </td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>
		 <button type="submit"><%=LC.L_Submit%></button>
		</td>
	</tr>
	</table>

	<input type="hidden" name="delMode" value="<%=delMode%>">
	<input type="hidden" name="srcmenu" value="<%=src%>">
	<input type="hidden" name="savedRepId" value="<%=savedRepId%>">
	<input type="hidden" name="studyId" value="<%=studyId%>">  
	<input type="hidden" name="selectedTab" value="<%=selectedTab%>">
	

	</FORM>
<%
	} else {
  

			String eSign = request.getParameter("eSign");	
			String oldESign = (String) tSession.getValue("eSign");
			if(!oldESign.equals(eSign)) {
%>

 		  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
			} else {

//			ret = lineitemB.lineitemDelete(EJBUtil.stringToNum(lineitemId));
			ret =savedRepB.removeSavedRep(EJBUtil.stringToNum(savedRepId)); 	

  
			if (ret==-2) {%>
				<br><br><br><br><br> <p class = "successfulmsg" align = center>  <%=LC.L_Rpt_NotDel%><%--Report not deleted.*****--%> </p>			
			<%}else { %>
				<br><br><br><br><br> <p class = "successfulmsg" align = center> <%=MC.M_Rpt_Remd_Succ%><%--Report removed successfully.*****--%> </p>
				<META HTTP-EQUIV=Refresh CONTENT="1; URL=savedrepbrowser.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&studyId=<%=studyId%>">				
		<%

			}
			
	
			} //end esign
			
			

	} //end of delMode
	
  }//end of if body for session 
else { %>
 <jsp:include page="timeout.html" flush="true"/> 
 <% } %>
 <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
  
</DIV>

<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/> 
</div>

</body>
</HTML>


