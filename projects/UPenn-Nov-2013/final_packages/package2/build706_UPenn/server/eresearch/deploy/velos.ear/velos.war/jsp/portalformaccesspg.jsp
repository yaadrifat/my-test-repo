<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
	<title><%=LC.L_Portal_FormAccess%><%--Portal Form Access*****--%></title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<jsp:include page="include.jsp" flush="true"/>
</head>


	<%@ page language = "java" import = "java.util.*,com.velos.eres.service.util.*,com.velos.esch.business.common.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.esch.web.eventassoc.EventAssocJB"%>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<jsp:useBean id="PFormsB"  scope="request" class="com.velos.esch.web.portalForms.PortalFormsJB"/>
	

<script Language="Javascript">

function  validate(formobj){
	/*if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
	alert("Incorrect e-Signature. Please enter again");
	formobj.eSign.focus();
	return false;
   }*/


   //JM: 28DEC2010: #5654
   arrPortalKeys = new Array();
   arrHiddenEvtIds = new Array();
   arrHiddenFormIds = new Array();
   arrInclChks = new Array();
	
   parentform = formobj.parentForm.value;
   counter= formobj.counter.value; 
	
   
   if (counter==1) {

	   arrPortalKeys[0]=formobj.portalformKey.value;
	   arrHiddenEvtIds[0]=formobj.hiddenEvtId.value;
	   arrHiddenFormIds[0]=formobj.hiddenFormId.value;	   
	   if(formobj.formIdAccess.checked==true){			
		   arrInclChks[0]= formobj.inclChks.value="Y";   
	   }else{
		   arrInclChks[0]= formobj.inclChks.value="N";
	   }
	   
			
	} else if (counter>1){		
		
		if(formobj.formIdAccess.length > 1){
			for(i=0;i<formobj.formIdAccess.length;i++){

				arrPortalKeys[i]=formobj.portalformKey[i].value;
				arrHiddenEvtIds[i]=formobj.hiddenEvtId[i].value;
				arrHiddenFormIds[i]=formobj.hiddenFormId[i].value;	
				   
					if(formobj.formIdAccess[i].checked==true){
						arrInclChks[i]= formobj.inclChks[i].value="Y";
						
					}else{
						arrInclChks[i]= formobj.inclChks[i].value="N";	
					}
			}
		}
				
	}  
   
   
   if (document.layers) {

	   if (parentform=="portalDesign") {		   
		   
		   	  window.opener.document.portalDesign.fromFormAccessPage.value="ture";
		   	  window.opener.document.portalDesign.portalformKey.value= arrPortalKeys;
			  window.opener.document.portalDesign.hiddenEvtId.value= arrHiddenEvtIds;
			  window.opener.document.portalDesign.hiddenFormId.value= arrHiddenFormIds;
			  window.opener.document.portalDesign.inclChks.value=arrInclChks;
			  window.opener.document.portalDesign.pfCounter.value=counter;

			  window.opener.document.portalDesign.calIdVal.value=formobj.calId.value;
	   }
   }else{
	   if (parentform=="portalDesign") {
		   	  window.opener.document.portalDesign.fromFormAccessPage.value="ture";
			  window.opener.document.portalDesign.portalformKey.value= arrPortalKeys;
			  window.opener.document.portalDesign.hiddenEvtId.value= arrHiddenEvtIds;
			  window.opener.document.portalDesign.hiddenFormId.value= arrHiddenFormIds;
			  window.opener.document.portalDesign.inclChks.value=arrInclChks;
			  window.opener.document.portalDesign.pfCounter.value=counter;
			  
			  window.opener.document.portalDesign.calIdVal.value=formobj.calId.value;
  	   }   
   }
   self.close();
}
   



function fnChkValue(formobj){

 	
	if(parseInt(formobj.counter.value)>0){			   
		if(formobj.formIdAccess.length > 1){
			for(i=0;i<formobj.formIdAccess.length;i++){
				   
					if(formobj.formIdAccess[i].checked==true){
						formobj.inclChks[i].value="Y";
						
					}else{
						formobj.inclChks[i].value="N";	
					}
			}
		}
		else{
			if(formobj.formIdAccess.checked==true){
	
				formobj.inclChks.value="Y";
					
			}else{
	
				formobj.inclChks.value="N";	
			}	
		}			
	}
	
}

</script>


<Body>


<% 

HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession)) {

	String mode = "";
    mode = request.getParameter("mode");

	String uName = (String) tSession.getValue("userName");

	//JM: 14Jun2007
	int pageRight = 0;
	GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");
	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("PORTALAD"));
	
	
	
	
String calendarIds = request.getParameter("calIds");

String portalId = request.getParameter("portalId");

String parentFormName = request.getParameter("parentForm");

String selChkFrmVals=request.getParameter("selChkVals");

String[] calIdsArray = calendarIds.split(",");
int totalCount=0;
%>
<Form name = "portalformaccess" id="portalformaccessfrm" method="post" action="" onsubmit = "if (validate(document.portalformaccess)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

<input type="hidden" name="portalId" value="<%=portalId%>"></input>
<input type="hidden" name="calId" value="<%=calendarIds%>"></input>
<input type="hidden" name="fromFormAccessPage" value="true"></input>
<input type="hidden" name="parentForm" value="<%=parentFormName%>">	
<div class="popDefault"><br></br>
<P class="sectionHeadings"> <%=MC.M_MngPrtl_PrtlAccs%><%--Manage Portal >> Portal Form Access*****--%></P>
<%


//Looping over the Calenders Ids  PP-18306 AGodara
for(int calCount=0;calCount<calIdsArray.length;calCount++){
	
	PortalFormsDao pfDao = PFormsB.getCheckedFormsForPortalCalendar(StringUtil.stringToNum(calIdsArray[calCount]), StringUtil.stringToNum(portalId));
	EventAssocJB eventAssocB = new EventAssocJB();
	eventAssocB.setEvent_id(StringUtil.stringToNum(calIdsArray[calCount]));
	eventAssocB.getEventAssocDetails();
	String calName = eventAssocB.getName();
	
		
	ArrayList pfIds = pfDao.getPortalFormIds();
	ArrayList visitIds = pfDao.getVisitIds();
	ArrayList visitNames = pfDao.getVisitNames();
	ArrayList eventIds = pfDao.getEventIds(); 
	ArrayList eventNames = pfDao.getEventNames(); 
	ArrayList formIds = pfDao.getFormIds();
	ArrayList formNames = pfDao.getFormNames();
	ArrayList checkedForms = pfDao.getCheckedForms();
	
	
	
	String pfId = "";
	String visitId = "";
	String visitName = "";
	String eventId = "";
	String eventName = "";
	String formId = "";
	String formName = "";
	String checkedForm = "";

	String oldVisitName= "";
	String oldEventName= "";
	String visitDispVal= "";
	String evtDispVal= "";

	String oldVisitId="";

	int len = eventIds.size();
	totalCount = totalCount+len;
	if(len>0){
%>
	<table width="100%" border="1" cellpadding="5" cellspacing="2" >
<BR></BR>
	<tr align="left"><b><%=LC.L_Cal_Name%>:&nbsp;<%=calName%></b></tr>
			<tr>
				<th width="15%" align="center"><%=LC.L_Visit%><%--Visit*****--%> </th>
				<th width="40%" align="center"><%=LC.L_Event%><%--Event*****--%></th>
				<th width="40%" align="center"><%=LC.L_Form%><%--Form*****--%></th>
				<th width="5%" align="center"><%=LC.L_Include%><%--Include*****--%> </th>
			</tr>



	

<%
int chkIndex = 0;
String isChkd = "";
selChkFrmVals = selChkFrmVals+",";


	


for(int counter=0; counter<len; counter++){
	
	
	pfId = (pfIds.get(counter))==null?"0":(pfIds.get(counter)).toString();
	visitId = (visitIds.get(counter))==null?"":(visitIds.get(counter)).toString();
	visitName = (visitNames.get(counter))==null?"":(visitNames.get(counter)).toString();
	if (oldVisitName.equals(visitName)){ 
		visitDispVal = LC.L_No/*"No"*****/;
	}else{
		visitDispVal = LC.L_Yes/*"Yes"*****/;
	}
	
	
	eventId = (eventIds.get(counter))==null?"":(eventIds.get(counter)).toString();
	eventId = eventId.trim();
	eventName = (eventNames.get(counter))==null?"":(eventNames.get(counter)).toString();
	
	//JM: 25JAN2011: #5744
	if (oldVisitId.equals(visitId)){
		if (oldEventName.equals(eventName)){ 
			evtDispVal = LC.L_No/*"No"*****/;
		}else{
			evtDispVal = LC.L_Yes/*"Yes"*****/;
		}
	}else{
		
		evtDispVal = LC.L_Yes/*"Yes"*****/;
		
	}
	
	
	oldEventName = eventName; 
	oldVisitName = visitName; 	
	oldVisitId = visitId;
	
	formId = (formIds.get(counter))==null?"":(formIds.get(counter)).toString();
	formName = (formNames.get(counter))==null?"":(formNames.get(counter)).toString();
	
	checkedForm = (checkedForms.get(counter))==null?"":(checkedForms.get(counter)).toString();
	
	
	//JM: 29DEC2010: #5654
	if (selChkFrmVals.indexOf(",") >=0){
	chkIndex = selChkFrmVals.indexOf(",");
	isChkd = selChkFrmVals.substring(0,chkIndex);
	selChkFrmVals = selChkFrmVals.substring(chkIndex+1, selChkFrmVals.length());
	
	}
	

	String isChecked = "";
	if (!isChkd.equals("")){
		if (isChkd.equals("Y")){	
			isChecked = "checked";	
		}else{
			isChecked = "";
		}		
	}else{		
		if (EJBUtil.stringToNum(checkedForm)>0 ){	
			isChecked = "checked";	
		}else{
			isChecked = "";
		}
	}

	   if ((counter%2)==0) {  %>
		<tr class="browserEvenRow"> 
	<% } else { %>
		<tr class="browserOddRow"> 
	<% } %>
			
			
				<td width="25%" align="left"><%if(!visitDispVal.equals(LC.L_No/*"No"*****/)){ %><%=visitName %><%}else{ %>&nbsp;<%} %></td>				
				<td width="25%" align="left"><%if(!evtDispVal.equals(LC.L_No/*"No"*****/)){ %><%=eventName %><%}else{%> &nbsp;<%} %></td>
				<td width="25%" align="left"><%=formName%></td>
				<td width="25%" align="center"><input type="checkbox" name="formIdAccess" value="<%=checkedForm%>" <%=isChecked%> onclick="fnChkValue(document.portalformaccess)"> </input> </td>
				<input type="hidden" name="inclChks"></input>
				<input type="hidden" name="hiddenEvtId" value="<%=eventId%>"></input>
				<input type="hidden" name="hiddenFormId" value="<%=formId%>"></input>				
				<input type="hidden" name="portalformKey" value="<%=pfId%>"></input>
				
			</tr>


<%}%>
</table>
<%
	}} %>
 		
 		<input type="hidden" name="counter" value="<%=totalCount%>"></input>
 		<BR><BR><BR>		
		<% if (pageRight>=6) {%>		
			<jsp:include page="submitBar.jsp" flush="true">
			<jsp:param name="displayESign" value="N"/>
			<jsp:param name="formID" value="portalformaccessfrm"/>
			<jsp:param name="showDiscard" value="N"/>				
		</jsp:include>
		<%} %>
		

</div>
</form>





<%



	}//end of if body for session
else {
%>
	<jsp:include page="timeout.html" flush="true"/>
<% } %>


 <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</Body>


</html>