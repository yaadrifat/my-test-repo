<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@page import="com.velos.eres.service.util.*"%>
<html>
<head>
<title><%=MC.M_AddEdt_PersCst%><%-- Add/Edit Personnel Cost*****--%> </title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
<!--#5747 02/10/2011 @Ankit-->
<jsp:useBean id="budgetB" scope="page"	class="com.velos.esch.web.budget.BudgetJB" />

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<jsp:include page="include.jsp" flush="true"/>
</head>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<SCRIPT Language="javascript">

function maxDigits(value)
{
 
   cnt = 0;
   
   for(i=0;i<value.length;i++)
	{
	  if(value.charAt(i)=='.'){
		 
		  if(cnt <= 8){
			
		  return true;
		  }
		  else {
			 
		  return false;
		  }
	  }
	else{
		cnt = cnt+1;
		
	  }
	  if(cnt > 8)
		  return false;
	  
	}

}
function confirmBox(lineitem, pgRight) {

	if (f_check_perm(pgRight,'E') == false) {
		return false;
	}
	var paramArray = [lineitem];
	msg=getLocalizedMessageString("M_Del_FrmPrslSec",paramArray);/*msg="Delete " + lineitem + " from Personnel Section?";*****/

	if (confirm(msg)) 
	{
    	return true;
	}
	else
	{
		return false;

	} 
 }



function confirmIncl(formobj,itemName) {
	var paramArray = [itemName];
	msg=getLocalizedMessageString("M_ChgInclLoss_DataEtrProc",paramArray);/*msg = "Changing 'Inclusion' for '"+itemName+"' may lead to loss of personnel cost related data entered previously. Do you want to proceed?"*****/
	if(confirm(msg))	
	{	
		return true;			
	}		
			else{
							
				return false;
			}
 }


 function  validate(formobj){
	 var i = 0;
	 var countChng = 0;
	
	var arrlen = formobj.arrlen.value;	
	var defRowsNo = formobj.defRowsNo.value;

	var pgRight =parseInt(formobj.pageRight.value,10);
	var mode = formobj.mode.value;
	
	if (!(pgRight < 6 && mode=="M")){
		
		if(arrlen == 1)
		 {
			
		if((formobj.itemIncl.value == "0") && (formobj.budgetStatus.value == "1"))
			 {
			 if(confirmIncl(formobj,formobj.type.value))	{
			
			 }
			 else{
				 formobj.budgetStatus.focus();
				 return false;
				
	
			 }
	
			 }
			if(formobj.type.value != "")
			 {	
			 	if(isNaN(formobj.rate.value)==true ){
				alert("<%=MC.M_RateMustBeNum_ReEtr%>");/*alert("Rate has to be a number. Please enter again.");*****/
				formobj.rate.focus();
				return false;
				}
				if(maxDigits(formobj.rate.value)==false){
					alert("<%=MC.M_IntegerPartCnt_Exceed%>");/*alert("Integer part cannot exceed more than 8 digits.");*****/
					formobj.rate.focus();
					return false;
				}
			 }
	
		 }
		 if(arrlen > 1 )
		 {
			for(i =0 ;i<arrlen;i++){
		
			if((formobj.itemIncl[i].value == "0") &&( formobj.budgetStatus[i].value == "1"))
			 {
				if(confirmIncl(formobj,formobj.type[i].value))
				 {
							 
				 }
				 else{
			
					 formobj.budgetStatus[i].focus();
					 return false;
	
				 }
			 countChng++;
			 }
			}
			if(countChng > 0)
				{
				//confirmIncl(formobj,arrlen,i);	
				}
				
			for(i =0 ;i<arrlen;i++){
		
			
			 if(formobj.type[i].value != "")
			 {
				if(isNaN(formobj.rate[i].value)==true ){
				alert("<%=MC.M_RateMustBeNum_ReEtr%>");/*alert("Rate has to be a number. Please enter again.");*****/
				formobj.rate[i].focus();
				return false;
				}
				if(maxDigits(formobj.rate[i].value)==false){
					alert("<%=MC.M_IntegerPartCnt_Exceed%>");/*alert("Integer part cannot exceed more than 8 digits.");*****/
					formobj.rate[i].focus();
					return false;
				}
	
			 }
			}
		 }
	}
	
	if(formobj.refresh.value == "false"){
		if(defRowsNo == 1 )
			 {
				if(formobj.newType.value != "")
				 {
					if(isNaN(formobj.newRate.value)==true ){
					alert("<%=MC.M_RateMustBeNum_ReEtr%>");/*alert("Rate has to be a number. Please enter again.");*****/
					formobj.newRate.focus();
					return false;
					}
					if(maxDigits(formobj.newRate.value)==false){
					alert("<%=MC.M_IntegerPartCnt_Exceed%>");/*alert("Integer part cannot exceed more than 8 digits.");*****/
					formobj.newRate.focus();
					return false;
					}
				}
	 
			 }
		if(defRowsNo > 1)
			 {
				for(i =0 ;i<defRowsNo;i++){
				 if(formobj.newType[i].value != "")
				 {
					if(isNaN(formobj.newRate[i].value)==true ){
					alert("<%=MC.M_RateMustBeNum_ReEtr%>");/*alert("Rate has to be a number. Please enter again.");*****/
					formobj.newRate[i].focus();
					return false;
					}
					if(maxDigits(formobj.newRate[i].value)==false){
					alert("<%=MC.M_IntegerPartCnt_Exceed%>");/*alert("Integer part cannot exceed more than 8 digits.");*****/
					formobj.newRate[i].focus();
					return false;
				}
				 }
			}
		}
	}
	 if (!(validate_col('e-Signature',formobj.eSign))) return false;
		else 
			return true;
	
   }

function refreshPage(formobj, pgRight)
{
	ripLocaleFromAll();
	if (f_check_perm(pgRight,'N') == false) {
		return false;
	}

	moreRows = formobj.moreRows.value;
	if(moreRows=='' ){
       alert("<%=MC.M_EtrNum_ForMoreRows%>")/*alert("Please enter the number for More Rows.")*****/
       formobj.moreRows.focus();
       return false;
	}			 			 

	 if(isNaN(moreRows) == true) {
	     alert("<%=MC.M_ValidNum_InAddRows%>");/*alert("Please enter a valid number in Add More Rows.");*****/
		 formobj.moreRows.focus();
		 return false;
	}
	if(pgRight != 5){	     
      if (confirm("<%=MC.M_Refreshing_RowsSvdata%>"))/*if (confirm("Refreshing the number of rows will save the data entered above. Do you wish to continue?"))*****/
  	  {
  	   	  formobj.refresh.value = "true"; 	  
  	   	  if (validate(formobj)){
  	   	  formobj.submit();
  	   	   }
  	  	  }
  	 	 else {	return false;}
	}else{
	
  	   	  formobj.refresh.value = "true";
  	   	  formobj.submit();
	}
}


</SCRIPT>

</head>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="lineitemB" scope="session" class="com.velos.esch.web.lineitem.LineitemJB"/>
<%@ page language = "java" import = "com.velos.esch.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,java.math.BigDecimal"%>
<jsp:include page="include.jsp" flush="true"/>
<% 
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
if (agent1 != null && agent1.indexOf("MSIE") != -1) 
   	ienet = 0; //IE
else
	ienet = 1;

if (ienet == 0) {
%>
<body style="overflow:scroll" >
<%} else {%>
<body >
<%}%>
<DIV class="popDefault" id="div1"> 
 <%

	int pageRight = 0;
	

	
String itemMoreRows = "";
	
	String mode = request.getParameter("mode"); 
	
	String budgetId = request.getParameter("budgetId");
	
	String bgtStatCode = request.getParameter("bgtStatCode");
	
	String budgetTemplate = request.getParameter("budgetTemplate");
	budgetTemplate=(budgetTemplate==null)?"":budgetTemplate;
	
	String bgtStatDec ="";
	String ctgryPullDn = "";
	SchCodeDao schDao = new SchCodeDao();
	schDao.getCodeValues("category");
	int perPk = schDao.getCodePKByDesc("category", "Personnel" , "ctgry_per");
	
	ctgryPullDn = schDao.toPullDown("cmbCtgry",perPk,1);
	
	if (EJBUtil.isEmpty(bgtStatCode))
		bgtStatCode ="W";
	
	// #5747 02/10/2011 @Ankit
	BudgetDao budgetDao = budgetB.getBudgetInfo(EJBUtil.stringToNum(budgetId));
	ArrayList bgtStatDescs = budgetDao.getBgtCodeListStatusesDescs();
	if(bgtStatDescs!=null && bgtStatDescs.size()>0){
		bgtStatDec = (String) bgtStatDescs.get(0);
	}


	
	String bgtcalId  = request.getParameter("bgtcalId");
	ArrayList arrLineitemIds = null;
	ArrayList arrLineitemNames = null;
	ArrayList arrLineitemRates = null;
	ArrayList arrLineitemInclusions = null;
	ArrayList arrLineitemNotes = null;
	ArrayList arrApplyInFuture = null;
	ArrayList arrLineitemCategories = null;
	//Rohit CCF-FIN21
	//ArrayList arrLineitemTMIDs = null;
	//ArrayList arrLineitemCDMs = null;
	
	
	int itemId = 0;				
	String itemName = "";
    String itemRate = "";
	String itemInclusion = "";					
	String itemNote = "";
	String applyInFuture = "";
	String inputClass = "inpDefault";
	String makeReadOnly = "";
	String makeGrey = "Black";
	String makeDisabled = "";
	String ftextGrey = "textDefault"; 
	String itemCategory = "";
	//Rohit CCF-FIN21
	//String itemTMID = "";
	//String itemCDM = "";
	BigDecimal bRate = null;
	boolean enableFlag = true;
	HttpSession tSession = request.getSession(true); 

	

	if (sessionmaint.isValidSession(tSession))	{
		String refresh=request.getParameter("refresh");

		String fromRt = request.getParameter("fromRt");
		if (fromRt==null || fromRt.equals("null")) fromRt="";
		
		if (fromRt.equals("")){
			GrpRightsJB bRights = (GrpRightsJB) tSession.getAttribute("BRights");		
			pageRight = Integer.parseInt(bRights.getFtrRightsByValue("BGTDET"));
		 } else{
			pageRight = EJBUtil.stringToNum(fromRt);
		 }
		
		if ( mode.equals("M") &&(pageRight< 6) ){
			makeReadOnly = "readonly";
			inputClass = "inpGrey";
			makeGrey = "Grey";
			makeDisabled ="disabled";
			ftextGrey = "textGrey";
			enableFlag = false;					
		}
	
		if (refresh==null) refresh="false";
		int imoreRows=EJBUtil.stringToNum(request.getParameter("moreRows")); 
		int defRowsNo = 0;
		String pageMode = request.getParameter("pageMode");
		
		int arrlen = 0;
		
		
		LineitemDao lineDao = new LineitemDao();

		lineDao = lineitemB.getLineitemsOfDefaultSection(EJBUtil.stringToNum(bgtcalId));
		arrLineitemIds = lineDao.getLineitemIds();
		
		arrLineitemNames = lineDao.getLineitemNames();
		arrLineitemRates = lineDao.getLineitemSponsorUnits();
		arrLineitemInclusions = lineDao.getLineitemInclusions();
		arrLineitemCategories = lineDao.getLineitemCategories();
		//Rohit CCF-FIN21
		//arrLineitemTMIDs = lineDao.getLineitemTMIDs();
		//arrLineitemCDMs = lineDao.getLineitemCDMs();
		
		arrLineitemNotes = lineDao.getLineitemNotes();
		arrApplyInFuture = lineDao.getApplyInFuture();
		
		
		arrlen = arrLineitemIds.size();
		
		
%>

	<%
  	    //Added by IA 11.03.2006 Comparative Budget
  	      if ( budgetTemplate.equals("C") ){ 
    %>
		<P class="sectionHeadings"><%=MC.M_CompBgt_AddEdtCst%><%-- Comparative Budget >> Add/Edit Personnel Cost*****--%></P>	
	
	<%}else{ %>
		<P class="sectionHeadings"><%=MC.M_PatBgt_AddEdtCst%><%--<%=LC.Pat_Patient%> Budget >> Add/Edit Personnel Cost*****--%></P>	
	<%} %>
	
	<%if (!((pageRight == 7 ) || (pageRight==5 ) || (arrlen > 0 && pageRight == 6 )  )){%>
	<P class = "defComments"><FONT class="Mandatory">	
	<%=MC.M_InfoNotSvd_UsrNoRgt%><%--Information in this page will not be saved, user doesn't have access right*****--%></Font></P>
	<%}%>	
		<!--   Fixed Bug No: 4549 Put Mandatory Field Validation -->
    <Form name="editCost" id="editPersCost" method="post" action="editPersonnelCostSubmit.jsp" onSubmit="ripLocaleFromAll(); if (validate(document.editCost)==false) {setValidateFlag('false'); applyLocaleToAll(); return false;} else {setValidateFlag('true'); return true;}">
		<Input type="hidden" name="refresh" value="false"/>
		<Input type="hidden" name="mode" value="<%=mode%>"/>
		<Input type="hidden" name="pageRight" value="<%=pageRight%>"/>
		<Input type="hidden" name="budgetId" value="<%=budgetId%>"/>
		<input type="hidden" name="bgtcalId" value="<%=bgtcalId%>"/>
		<input type="hidden" name="bgtStatCode" value="<%=bgtStatCode%>"/>
		<Input type="hidden" name="fromRt" value="<%=fromRt%>"/>
		<%
				  
		if ((!(bgtStatCode ==null)) && (bgtStatCode.equals("F") || bgtStatCode.equals("T")) ) 
		{ Object[] arguments = {bgtStatDec};
		%>
			 <table width="600" cellspacing="2" cellpadding="2">
			 	<tr>
					<td>
					    	<P class = "defComments"><FONT class="Mandatory"><%=VelosResourceBundle.getMessageString("M_BgtStat_CntChgBgt",arguments)%><%--Budget Status is '<%=bgtStatDec%>'. You cannot make any changes to the budget.*****--%></Font></P>
					</td>
				</tr>
			</table>
		<%}
		
		if (!(bgtStatCode.equals("F") || bgtStatCode.equals("T") )) 
		{
// SV, 8/25, disable the refresh link if "new" permission is not there.
			if ( (mode.equals("M") && ((pageRight==5) || (pageRight==7)) ) || (mode.equals("N")) ){

		%>

	<!--	 <table>
		 <td><P class="defComments">Add More Rows</P></td>
		 <td><input type="text" name="moreRows" size =3 MAXLENGTH = 3 value="<%=itemMoreRows%>"></td>
		 <td colspan="1"><A href=# onclick="refreshPage(document.editCost, <%=pageRight%>)">Refresh</A></td>
		 </table> -->
		 <%}} %> 
		 
       <table width ="100%" class="basetbl">
		 <tr>
		 <th width ="15%" align='center'><%=LC.L_Personnel_Type%><%--Personnel Type*****--%></th>
		 <th width ="5%" align='center'><%=LC.L_Rate%><%--Rate*****--%></th>
		 <th width ="15%" align='center'><%=LC.L_Include_In%><%--Include in*****--%></th>
		 <th width ="10%" align='center'><%=LC.L_Notes%><%--Notes*****--%></th>
		 <th width ="10%" align='center'><%=LC.L_Category%><%--Category*****--%></th>
<!--
		 <th width ="10%" align=center><B>TMID</th>
	     <th width ="10%" align=center><B>CDM</th>
-->
		 <th width="20%">
<% /*SV,REDTAG, 8/26/04		 	 if (mode.equals("M") || arrlen > 0) { */
		 	 if (arrlen > 0) {%>
			 	  <%=LC.L_Apply_Changes%><%--Apply Changes To*****--%>
			 <%}%>
		 </th>

		 </tr>
		<% 

//SV, REDTAG		if (mode.equals("M") || arrlen > 0)
		if (arrlen > 0)
			{		
			
				 for(int i =0;i<arrlen;i++)
				{
				itemId = ((Integer)arrLineitemIds.get(i)).intValue();				
				itemName = (String)arrLineitemNames.get(i);
    			itemRate = (String)arrLineitemRates.get(i);
				
				if(itemRate == null)
					itemRate = "0";
			
				bRate = new BigDecimal(itemRate);
                bRate = bRate.setScale(2,5);
				itemRate = (itemRate== null)?"":(itemRate) ;
				itemInclusion = (String)arrLineitemInclusions.get(i);					
				itemNote = (String)arrLineitemNotes.get(i);
				itemNote = (itemNote== null)?"":(itemNote) ;
				applyInFuture = (String)arrApplyInFuture.get(i);
				itemCategory = (String)arrLineitemCategories.get(i);	
				//Rohit CCF-FIN21
				/*
				itemTMID = (String)arrLineitemTMIDs.get(i);	
				itemCDM = (String)arrLineitemCDMs.get(i);	
				
				if(itemTMID == null)
					itemTMID = "";
				if(itemCDM == null)
					itemCDM = "";
			*/
				ctgryPullDn = schDao.toPullDown("cmbCtgry",EJBUtil.stringToNum(itemCategory), enableFlag);		
				
				%>
				 <tr>
				 
				<td>
				<%if ( mode.equals("M") &&(pageRight< 6) ){  %>
					<%=itemName%>
				<%} else {%>
					 <input type ="hidden" name="lineitemIds" value='<%=itemId%>'>
					 <input class ="<%=inputClass%>" type=text name = "type" maxlength="150" value='<%=itemName%>' <%=makeReadOnly%>>
				<%} %>
				</td>
				 <input type ="hidden" name="itemIncl" value='<%=itemInclusion%>'>
				<td>
				<%if ( mode.equals("M") &&(pageRight< 6) ){  %>
					<%=bRate%>
				<%} else {%>
					<input class ="<%=inputClass%> numberfield" data-unitsymbol="" data-formatas="" type=text name = "rate" maxlength="11" size="10" value='<%=bRate%>' <%=makeReadOnly%>></td>
				<%} %>
				 <td>
				<%if ( mode.equals("M") &&(pageRight< 6) ){  %>
					<%if(itemInclusion.equals("1")){%>
						<%=MC.M_Personnel_CostSec%><%--Personnel Cost Section only*****--%>
					<%}else if(itemInclusion.equals("0")){%>
						<%=LC.L_All_Sections%><%--All sections*****--%>
					<%}else if(itemInclusion.equals("2")){%>
						<%=MC.M_PerPat_FeeSec%><%--All Per <%=LC.Pat_Patient%> Fee sections*****--%>
					<%}%>
				<%} else {%> 
					 <select  name="budgetStatus" <%=makeDisabled%>>		
					 <%if(itemInclusion.equals("1")){%>
					  <option value='1' selected><%=MC.M_Personnel_CostSec%><%--Personnel Cost Section only*****--%></option>
					  <option value='0'><%=LC.L_All_Sections%><%--All sections*****--%></option>	
					  <option value='2'><%=MC.M_PerPat_FeeSec%><%--All Per <%=LC.Pat_Patient%> Fee sections*****--%> </option>
					 <%}else if(itemInclusion.equals("0")){%>
					 <option value='1'><%=MC.M_Personnel_CostSec%><%--Personnel Cost Section only*****--%></option>
					  <option value='0' selected><%=LC.L_All_Sections%><%--All sections*****--%></option>
					 <option value='2'><%=MC.M_PerPat_FeeSec%><%--All Per <%=LC.Pat_Patient%> Fee sections*****--%> </option>
					 <%}else if(itemInclusion.equals("2")){%>
					 <option value='1'><%=MC.M_Personnel_CostSec%><%--Personnel Cost Section only*****--%></option>
					  <option value='0' ><%=LC.L_All_Sections%><%--All sections*****--%></option>
					 <option value='2' selected><%=MC.M_PerPat_FeeSec%><%--All Per <%=LC.Pat_Patient%> Fee sections*****--%> </option>
					 <%}%>
					  </select>
				<%} %>
				</td>
				<td>
					<%if ( mode.equals("M") &&(pageRight< 6) ){  %>
						<%=itemNote%>
					<%} else {%>
					<input class ="<%=inputClass%>" type="text" name = "notes" maxlength='1000' value='<%=itemNote%>' <%=makeReadOnly%>>
					<%} %>
				</td>
				<td>
					<%if ( mode.equals("M") &&(pageRight< 6) ){  %>
						<%=schDao.getCodeDescription(EJBUtil.stringToNum(itemCategory))%>
					<%} else {%>
					 	<%=ctgryPullDn%>
					<%} %>
				</td>
<%--
				<td align=center><input class ="<%=inputClass%>" type=text name="tmid" size = 10 maxlength=15  value='<%=itemTMID%>' <%=makeReadOnly%>></td> 
				<td align=center><input class ="<%=inputClass%>" type=text name="cdm" size = 10 maxlength=15 value='<%=itemCDM%>' <%=makeReadOnly%>></td> 
--%>
				<%if(mode.equals("M") || arrlen > 0){%>
				
				<td> 
					<%if (pageRight< 6){  %>
						<%if(applyInFuture.equals("0")){%>
							<%=LC.L_FutPersCst%><%--Future Personnel Costs*****--%>
						<%} else { %>
							<%=MC.M_AllPrev_FutPresCst%><%--All Previous and Future Personnel Costs*****--%>
						<%} %>
					<%} else {%>
					<select name = "applyFutureCost" <%=makeDisabled%>>
					<%if(applyInFuture.equals("0")){%>
						<option value="0" selected><%=LC.L_FutPersCst%><%--Future Personnel Costs*****--%></option>
						<option value = "1"><%=MC.M_AllPrev_FutPresCst%><%--All Previous and Future Personnel Costs*****--%></option>
					 <%}else if(applyInFuture.equals("1")){%>
					  	<option value="0" ><%=LC.L_FutPersCst%><%--Future Personnel Costs*****--%></option>
					 	<option value = "1" selected><%=MC.M_AllPrev_FutPresCst%><%--All Previous and Future Personnel Costs*****--%></option>
					 <%}%>
					</select>
					<%} %>
				</td>
					<%if (!(bgtStatCode.equals("F") || bgtStatCode.equals("T") )) {	%>
				 	<td>
					 	<A href ="editPerCostDelete.jsp?lineitemId=<%=itemId%>&bgtcalId=<%=bgtcalId%>&applyInFuture=<%=applyInFuture%>&from=perCost&fromRt=<%=fromRt%>" onClick="return confirmBox('<%=itemName%>', <%=pageRight%>)"><%=LC.L_Delete%><%--Delete*****--%></A>
					 </td>
				 	<% } // for bgtstatcode%>
			 	<%} %>
				</tr>	
				
				<%}
	}//mode M


	
	if((mode.equals("M")) && (refresh.equals("true")))  { 
	
	
	}
 if((arrlen == 0 && pageMode.equals("first")) || pageMode.equals("refresh")){
 //SV,REDTAG if ((mode.equals("N")) || (refresh.equals("true"))) {
	   int seqNum = 0;
  //new mode
 
	ctgryPullDn = schDao.toPullDown("newCmbCtgry",perPk,1);

  defRowsNo =10;

  if (refresh.equals("true")) 
  {
   	 defRowsNo = imoreRows;  
  %>
  	<Input type="hidden" name="fromRefresh" value="true" >
  <%
  }
  
 for(int i=0;i<defRowsNo;i++) 
 {%>
    <tr> 
        <td> 
	  <tr>
		 <td><input type=text name = "newType" maxlength=150></td>
		 <td><input type=text data-unitsymbol="" data-formatas="" class="numberfield" name = "newRate" maxlength=11></td>
		 <td> <select  name="newBudgetStatus">				 
			  <option value=1><%=MC.M_Personnel_CostSec%><%--Personnel Cost Section only*****--%></option>	
  			  <option value=0><%=LC.L_All_Sections%><%--All sections*****--%></option>
			  <option value=2><%=MC.M_PerPat_FeeSec%><%--All Per <%=LC.Pat_Patient%> Fee sections*****--%> </option>
			  </select></td>
		 <td><input type=text name = "newNotes" maxlength=1000></td>
	     <td><%=ctgryPullDn%></td>
<!--
	     <td align=center><input type=text name="newtmid" size = 10 maxlength=15 value=''></td> 
         <td align=center><input type=text name="newcdm" size = 10 maxlength=15 value=''></td> 
-->
    </tr>
 
 
<% } //loop for despRespNo 
%> 
<% 
 //}
 }//mode=N
/*SV, REDTAG, 8/26/04, refer to description in bug#1679. mode is not set consistently leading to issues here. Create a new parameter
to communicate to the corresponding update jsp about new rows being added.
*/
  if (defRowsNo > 0) { %>
 	<td><input type=hidden name="newRows" value='Y'></td>
 <%} else { %>
 	<td><input type=hidden name="newRows" value='N'></td>
 <%} %>

		</table>


 
		


<%
if (!(bgtStatCode.equals("F") || bgtStatCode.equals("T") )) 
	{




//mode.equals("M") && pageRight >= 5%>

<%//if ( (mode.equals("M") &&(pageRight>=5) || (arrlen>0 || pageRight>6)) || (mode.equals("N")) ){
if ((pageRight == 7 ) || (pageRight==5 ) || (arrlen > 0 && pageRight == 6 )  ){
%>
<%-- YK 07JAN- Fix for Bug #4383 and #5368 --%>
<table>
		 <td><P class="defComments"><%=LC.L_Add_MoreRows%><%--Add More Rows*****--%></P></td>
		 <td><input type="text" name="moreRows" size =3 MAXLENGTH = 3 value="<%=itemMoreRows%>"></td>
		 <td colspan="1"> <%=MC.M_EntDesiredNum_EsignBelow%><%--Enter the desired number of new rows in the box to the left, your e-Signature below and then click*****--%> <A href=# onclick="refreshPage(document.editCost, <%=pageRight%>)"><%=LC.L_Refresh%><%--Refresh --%></A></td>
		 </table>
<BR>
		<jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="editPersCost"/>
			<jsp:param name="showDiscard" value="N"/>
		</jsp:include>
<%
}
	else{%>
			<table width="100%" >
				<tr><td width="100%" align="center">				
				<button onClick="self.close();"><%=LC.L_Close%></button>
				</td></tr>
			</table>	
		<%}


}
%>
<input type=hidden name="arrlen" value=<%=arrlen%>>
<input type=hidden name="pageMode" value=<%=pageMode%>>
<input type=hidden name="defRowsNo" value=<%=defRowsNo%>>	



</Form>



<%

	


} else {  //else of if body for session



%>

  <jsp:include page="timeout.html" flush="true"/>

  <%



}



%>

<div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

  
</body>


</html>
