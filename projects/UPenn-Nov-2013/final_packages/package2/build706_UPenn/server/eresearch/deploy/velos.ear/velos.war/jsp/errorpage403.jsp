<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title>Register with eResearch</title>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel=STYLESHEET href="common.css" type=text/css>
</head>
 
<jsp:include page="panel.jsp" flush="true"/>   

<body>
<br>
<DIV class="formDefault" id="div1" > 
  <%	int userId = 0;
	String userAddresId = "";
	String dJobType = "" ;
	String dRoleType= "" ;
	String dPrimSpl= "" ;
	CodeDao cd = new CodeDao();
	CodeDao cd2 = new CodeDao();
	CodeDao cd3 = new CodeDao();
	cd2.getCodeValues("role_type"); 	
	cd.getCodeValues("job_type"); 
	cd3.getCodeValues("prim_sp");
      dRoleType = cd2.toPullDown("roleType");	
      dJobType = cd.toPullDown("userCodelstJobtype");	
      dPrimSpl = cd3.toPullDown("primarySpeciality");		
%>
  <Form name="signup" method="post" action="accountsave.jsp" >
    <p>&nbsp;</p>
    <table width="680" cellspacing="0" cellpadding="0">
      <tr> 
        <td height="30"> 
          <p class = "defComments"><font size="4" face="Arial, Helvetica, sans-serif">We're 
            sorry, the system is unable to display the requested page. This may 
            be because of the following reasons :</font></p>
        </td>
      </tr>
    </table>
    <p>&nbsp;</p>
    <table width="82%" cellspacing="0" cellpadding="0" align=left>
      <tr> 
        <td> 
          <div align="left"><b> Step 1: Determine Your Current Internet Browser 
            Security Level</b> </div>
        </td>
      </tr>
      <tr> 
        <td> 
          <div align="left">1. Open your Internet browser, along the top of the 
            window, go to Help and select the About Internet Explorer option. 
          </div>
        </td>
      </tr>
      <tr> 
        <td> 
          <div align="left">2. A box will open in the middle of your page, as 
            shown below. Check to ensure that it shows Cipher Strength: 128-bit. 
            If it is less than 128-bit, then you will need to upgrade your Internet 
            to support our web sites security requirements. </div>
        </td>
      </tr>
      <tr> 
        <td height=10> 
          <div align="left"></div>
        </td>
      </tr>
      <tr> 
        <td align=center> 
          <div align="left"><img src="../images/jpg/ieaboutus.jpg"></div>
        </td>
      </tr>
      <tr> 
        <td height=10> 
          <div align="left"></div>
        </td>
      </tr>
      <tr> 
        <td> 
          <div align="left"><b> Step 2: Upgrade Your Internet Browser</b></div>
        </td>
      </tr>
      <tr> 
        <td> 
          <div align="left">1. Go to <a href="http://www.microsoft.com/windows/ie"> 
            http://www.microsoft.com/windows/ie</a> </div>
        </td>
      </tr>
      <tr> 
        <td> 
          <div align="left">2. From the Downloads option, select Internet Explorer 
            5.5 (SP2) and Internet Tools. </div>
        </td>
      </tr>
      <tr> 
        <td> 
          <div align="left">3. You will see the following steps on how to download 
            the new version. </div>
        </td>
      </tr>
      <tr> 
        <td height=20> 
          <div align="left"></div>
        </td>
      </tr>
      <tr> 
        <td> 
          <div align="left"><b>How to download and install </b></div>
        </td>
      </tr>
      <tr> 
        <td> 
          <div align="left">1. Select your language from the drop-down list above 
            and click Download Now. </div>
        </td>
      </tr>
      <tr> 
        <td> 
          <div align="left">2. Choose Run this program from its current location 
            and click OK. </div>
        </td>
      </tr>
      <tr> 
        <td> 
          <div align="left">3. Click Yes if asked whether you would like to install 
            and run Microsoft Internet Explorer 5.5 Service Pack 1 (SP1) and Internet 
            Tools. </div>
        </td>
      </tr>
      <tr> 
        <td> 
          <div align="left">Accept the License Agreement and click Next to begin</div>
        </td>
      </tr>
      <tr> 
        <td height=10> 
          <div align="left"></div>
        </td>
      </tr>
      <tr> 
        <td align=center> 
          <div align="left"><img src="../images/jpg/ieservicepack.jpg"></div>
        </td>
      </tr>
      <tr> 
        <td height=10> 
          <div align="left"></div>
        </td>
      </tr>
      <tr> 
        <td> 
          <div align="left">4. During the installation process, select the option 
            for Install Now - Typical set of components. Once your installation 
            is complete, you will need to restart your computer before the new 
            settings will take effect.</div>
        </td>
      </tr>
      <tr> 
        <td height=20> 
          <div align="left"></div>
        </td>
      </tr>
      <tr> 
        <td> 
          <div align="left"><b> Step 3: Check For Other Known Problems </b></div>
        </td>
      </tr>
      <tr> 
        <td> 
          <div align="left">IE5 56 bit versions, specifically IE5.002919+2920, 
            do not handle SuperCerts on Apache platforms properly due to poor 
            SGC implementation. You may rectify this problem in one of the following 
            two ways: </div>
      <tr> 
        <td> 
          <div align="left">1. <http://www.microsoft.com/windows/ie> Upgrade your 
            browser as mentioned in the above section, or </div>
        </td>
      </tr>
      <tr> 
        <td> 
          <div align="left">2. Open your Internet browser, along the top of the 
            window, go to Tools and under Internet Options, select the Advanced 
            tab. </div>
        </td>
      </tr>
      <tr> 
        <td> 
          <div align="left">3. Scroll down the list of options to Security and 
            uncheck the box for Use SSL 3.0 as shown in the image below. </div>
        </td>
      </tr>
      <tr> 
        <td height=10> 
          <div align="left"></div>
        </td>
      </tr>
      <tr> 
        <td align=center> 
          <div align="left"><img src="../images/jpg/iesslsecurity.jpg"></div>
        </td>
      </tr>
      <tr> 
        <td height=10> 
          <div align="left"></div>
        </td>
      </tr>
    </table>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <div class="staticDefault"> </div>
    <P class = "sectionHeadings"><br>
    </P>
  </Form>
</div>
</body>
</html>
