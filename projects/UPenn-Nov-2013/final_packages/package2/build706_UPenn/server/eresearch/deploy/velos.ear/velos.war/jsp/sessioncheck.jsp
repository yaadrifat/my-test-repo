<%@page import="com.velos.eres.service.util.StringUtil"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.HashMap,org.json.*,com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%
	if(request.getParameter("invalidateflag")==null){
		request.setCharacterEncoding("UTF-8");
	    response.setContentType("application/json");
	}

	HttpSession tSession = request.getSession(true);
	/* INF-22330 06-Aug-2012 -Sudhir*/
    JSONObject jsObj = new JSONObject();
    String invalidateflag = (String)request.getParameter("invalidateflag");
    invalidateflag= invalidateflag==null?"":invalidateflag; 
    if(invalidateflag.equalsIgnoreCase("true")){/* INF-22330 06-Aug-2012 -Sudhir to check session is invalidate or not */
    	String portalUser = (String) tSession.getAttribute("portalUser");
    	if (StringUtil.isEmpty(portalUser)){
    		if (!sessionmaint.inValidSession(tSession)) {
    			//response.sendError(HttpServletResponse.SC_FORBIDDEN, "SESSION_INVALID");
	            //jsObj.put("error", new Integer(-1));
	            //jsObj.put("errorMsg", MC.M_LoggedInDiffrentSessionAreYouSureToReLogin);
	    	    %>
				<input type="hidden" name="message" id="message" value="true" />
				<%
				//out.println(jsObj.toString());
	            //return;
    		}
    	}
    }
    else{
    	boolean invalidateSession = false;
    	String portalUser = (String) tSession.getAttribute("portalUser");
    	if (!StringUtil.isEmpty(portalUser)){
    		//invalidateSession = sessionmaint.inValidSession(tSession);
    	}

    	if (!sessionmaint.isValidSession(tSession)) {
	        // Session is invalid; print an error and exit
	        response.sendError(HttpServletResponse.SC_FORBIDDEN, "SESSION_TIME_OUT");
	        jsObj.put("error", new Integer(-1));
	        jsObj.put("errorMsg", MC.M_UsrNot_LoggedIn/*"User is logged in."*****/);
		    out.println(jsObj.toString());
	        return;
	    }
    }
	    jsObj.put("error", new Integer(0));
	    jsObj.put("errorMsg", MC.M_UsrNot_LoggedIn/*"User is logged in."*****/);
	    out.println(jsObj.toString());
    
    // <html><head></head><body></body></html>
%>
