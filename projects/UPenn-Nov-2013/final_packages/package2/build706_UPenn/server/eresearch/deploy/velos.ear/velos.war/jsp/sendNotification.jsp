<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</HEAD>


<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<BODY>



<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>

<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />

<jsp:useBean id="studyNotifyB" scope="page" class="com.velos.eres.web.studyNotify.StudyNotifyJB" />

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import = "com.velos.eres.business.study.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.service.util.Configuration"%>


<%

HttpSession tSession = request.getSession(true);
String accId = (String) tSession.getValue("accountId");



 String src= request.getParameter("srcmenu");
 String selectedTab = request.getParameter("selectedTab");
  String userMailStatus = "";
   int metaTime = 1;

   int output = 0;
   String studyId = "";
   String studyTArea = "" ;
   String studyNumber = "" ;

String eSign = request.getParameter("eSign");

//for notification


if (sessionmaint.isValidSession(tSession))
 {
 %>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
	String oldESign = (String) tSession.getValue("eSign");

	if(!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>
<%
	} else {


	studyId	= (String) tSession.getValue("studyId");

    studyB.setId(EJBUtil.stringToNum(studyId));

    studyB.getStudyDetails();
   
    if(studyB.getStudyPubFlag().equals("Y")) {

	   output = studyNotifyB.notifySubscribers(EJBUtil.stringToNum(studyId));
	   
	   
	%>
	<br><br><br><br>
 <%
  if ( output == 0 )
  {
  	metaTime = 1;

  %>
	<p class = "sectionHeadings" align = center> <%=MC.M_AllSubscrib_Notified%><%--All Subscribers are Notified*****--%> </p>

 <%
	 } else
	{
	 	metaTime = 10;
     %>
		<p class = "redMessage" align = center> 
				<%=MC.M_ErrSendingNotfic_ToSub%><%--There was an error in sending notification to the subscribers*****--%><br>
		</p>

	 <%
  		}

	//end of changes for notification
} else { //else for the Pubflag if check

%>

<br>

<br>

<br>

<br>

<p class = "sectionHeadings" align = center>

	<%=MC.M_MailCanSent_ToPublicStd%><%--Mail can only be sent to public <%=LC.Std_Studies_Lower%>. To make this <%=LC.Std_Study_Lower%> public please go to the <%=LC.Std_Study%> Summary tab and make some section public.*****--%>

</p>

<%
} //end of if for checking public flag
}//end of if for eSign check
}

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
<%

}
%>


<META HTTP-EQUIV=Refresh CONTENT="<%=metaTime%>; URL=notify.jsp?mode=M&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&studyId=<%=studyId%>">

</BODY>
</HTML>

