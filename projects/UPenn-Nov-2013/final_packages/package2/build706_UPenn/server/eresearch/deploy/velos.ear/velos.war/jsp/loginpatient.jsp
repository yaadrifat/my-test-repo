<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- Added to avoid auto response flushing prior to session creation :: AGodara -->
<%@ page buffer="64kb"%>
<jsp:include page="localization.jsp" flush="false"/>
<HTML>
<TITLE> <%=LC.L_Validate_User%><%--Validate User*****--%> </TITLE>
<%!
String htmlEncode(String input) {
    if (input == null) { return ""; }
    return StringUtil.htmlEncodeXss(input);
}
%>
<SCRIPT LANGUAGE="JavaScript" SRC="whichcss_skin.js"></SCRIPT>
<HEAD>
 
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</HEAD>
<script>
var formWin = null;

function openHome(form,act){
var w = screen.availWidth-10;
var h = screen.availHeight-120;
//document.login.target="formWin";
//document.login.action=act;
document.location.href = act;
//myHome = open('donotdelete.html','formWin','resizable=yes,menubar=yes,toolbar=yes,location=no,status=yes,maximize=yes,left=0,top=0,screenX=0,screenY=0,height=' + h + ',width=' + w)

//myHome = open(act,'formWin','resizable=yes,menubar=yes,toolbar=yes,location=yes,status=yes,maximize=yes,left=0,top=0,screenX=0,screenY=0,height=' + h + ',width=' + w)
//if (myHome && !myHome.closed) 
//{
//document.login.submit();
//window.history.back();
// myHome.focus();
//}
//else {
	//popupBlock();
//}

	
//void(0);
}

function popupBlock()
{
	document.login.target="";
	logname=document.login.loguser.value;
	logurl=document.login.logurl.value;
	document.login.action="patientlogin.jsp?mode=popup&logname="+logname+"&logurl="+logurl;
	document.login.submit();
}
	</script>	
<body>
<form name="login"  METHOD="POST">


<jsp:useBean id="patLoginB" scope="request" class="com.velos.eres.web.patLogin.PatLoginJB"/>
<jsp:useBean id="userSessionB" scope="request" class="com.velos.eres.web.userSession.UserSessionJB"/>
<jsp:useBean id="userLB" scope="request" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id="personB" scope="request" class="com.velos.eres.web.person.PersonJB"/>

<jsp:useBean id="alnot" scope="request" class="com.velos.esch.web.alertNotify.AlertNotifyJB"/>
<jsp:useBean id="portalJB" scope="request" class="com.velos.eres.web.portal.PortalJB"/>
<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>


<!-- Gopu : import the objects for customized filed -->
<%@ page import="java.util.*,java.text.*,com.velos.eres.service.util.*,java.util.*,java.text.SimpleDateFormat,com.velos.eres.web.user.ConfigFacade, com.velos.eres.business.common.*"%>	

<% String login;

//modifed by Gopu for MAHI Enahncement on 8th Mar 2005
	ConfigFacade.getConfigFacade();

%>
<% login= request.getParameter("username"); 

HttpSession tempSession = request.getSession(false);
if(!(tempSession == null)){
	tempSession.invalidate();
}
%>

<%! String password; %>

<% password =request.getParameter("password"); 
	String scWidth = "";

	String scHeight = "";
	int permittedFailedAccessCount = 0;



	scHeight= request.getParameter("scHeight");

	scWidth = request.getParameter("scWidth");
	

	 
%>
<!-- commented by gopu to fix bugzilla issues # 2439 -->	

<input type="hidden" name="logurl" value=<%=request.getServerName()%>>


<input type="hidden" name="loguser" id="loguser" value="<%=htmlEncode(login)%>">
<input type="hidden" name="password" id="password" value="<%=htmlEncode(password)%>">
<input type="hidden" name="userId" id="userId" value="0">

<% 
 int output; boolean isAutoLogin = false; String newPasswordCreated = "";
 patLoginB.validateLoginPassword(login,password,request.getRemoteAddr());
 
 

 %>



<% 
int velosid = 0;
permittedFailedAccessCount = Configuration.FAILEDACCESSCOUNT;
Configuration conf = new Configuration();
String validLoginId = conf.getVelosAdminId(conf.ERES_HOME+"eresearch.xml");
velosid = EJBUtil.stringToNum(validLoginId);

output = 0;
int failCount = 0; int userFound = 0; int origfailCount = 0;
String loginFlagStr = ""; 
int loginFlag = 0;
String failedUserStat = "";
String portalId = "";
String portalUser = "";
String loginTime = "";
String dateOfDeath = "";
StringBuffer moduleString = new StringBuffer();


	output=EJBUtil.stringToNum(patLoginB.getPlId());
	//BK-3184 09-jul-2010
	personB.setPersonPKId( output);
	personB.getPersonDetails();
	dateOfDeath = personB.getPersonDeathDate();
	
	
	portalId = patLoginB.getFkPortal();
	
	if (! StringUtil.isEmpty(portalId))
	{
		portalJB.setPortalId(EJBUtil.stringToNum(portalId));
		portalJB.getPortalDetails();
		 
	}
		
 	//BK-3184 If the patient is dead.  
	if (output > 0  && (!(portalJB.getPortalStatus()).equals("A")) || !StringUtil.isEmpty(dateOfDeath)) //if the portal status is not active
	{
		output = 0; 
	}
 %>

<% if (output==0) 
  {
	%>
    	<SCRIPT>
    	var w = screen.availWidth-10;
	    var h = screen.availHeight-120;
		document.location.href = "signerror.jsp?flag=true";
       // signError = window.open("signerror.jsp","","resizable=yes,menubar=yes,toolbar=yes,location=no,status=yes,maximize=yes,left=0,top=0,screenX=0,screenY=0,height=" + h + ",width=" + w,"new")
       // window.history.back();
		//if (signError && !signError.closed)  signError.focus(); 
		//else 
		//	popupBlock();
        </SCRIPT>
    <%	
	}



 if (output > 0) 
 
  {
	String stat = "A";
	
%>
<%


if (stat.equals("A"))

{
%>
<%
	HttpSession thisSession = request.getSession(true);
	//get the audit user for the linked portal
  	
  	
  	ctrl.getControlValues("module");
	int ctrlrows = ctrl.getCRows();
  	
  	for (int h=0;h<ctrlrows;h++)
  	{
  		moduleString.append("1");
  		
  	}
  	 
  	isAutoLogin = patLoginB.getIsAutoLogin();
  	
  	if (isAutoLogin)
  	{
  		newPasswordCreated = patLoginB.getPlPassword();
  	}  

	thisSession.setAttribute("pp_currentLogin",patLoginB);
  	thisSession.setAttribute("pp_loginname",  patLoginB.getPlLogin());
  	
  	
  	
  	portalUser = portalJB.getPortalAuditUser();
  	
  	userLB.setUserId(EJBUtil.stringToNum(portalUser));
  	userLB.getUserDetails();
  	
  	thisSession.setAttribute("currentUser",userLB);
  	
  	
	loginTime = DateUtil.getCurrentDateTime();
	
	thisSession.setAttribute("portalUser",portalUser);
	thisSession.setAttribute("userId",portalUser);//remove this hardcoding
	//thisSession.setAttribute("pkUserSession",portalUser);//remove this hardcoding
	thisSession.setAttribute("modRight",moduleString.toString());//remove this hardcoding
	
	
	//find patient 
	personB.setPersonPKId( output);
	personB.getPersonDetails();
	
	String uName;
	String sessionTime = null;
	String accSkin = null;
	int userSession = 0;
	String patient_code="";


	
	String patLastName = "";
	String patFirstName = "";
				
	patLastName = personB.getPersonLname();
	patFirstName = personB.getPersonFname();

	if ( StringUtil.isEmpty(patLastName) || patLastName.equals("null")   )
	{
		patLastName = "";
	}
	
	if ( StringUtil.isEmpty(patFirstName) || patFirstName.equals("null")   )
	{
		patFirstName = "";
	}
	

	uName = patFirstName + " " + patLastName;
	
	if ( (! StringUtil.isEmpty(uName) ) && uName.equals("null null") )
	{
		uName = "";
	}
	
	patient_code = personB.getPersonPId();

	sessionTime = patLoginB.getPlLogOutTime() ;
	
	thisSession.setAttribute("pp_patientName", uName );
	thisSession.setAttribute("pp_patient_code", patient_code );
	
	//thisSession.setAttribute("eSign", eSign );
	thisSession.setAttribute("pp_ipAdd",request.getRemoteAddr());
	//thisSession.setAttribute("userId", String.valueOf(userLB.getUserId()));
	thisSession.setAttribute("pp_accountId", personB.getPersonAccount());
	
	thisSession.setAttribute("pp_session", sessionTime);
	thisSession.setAttribute("pp_ignoreAllRights", "true");
   	
	
	//set screen width n height in session
	thisSession.setAttribute("appScHeight",scHeight);
   	thisSession.setAttribute("appScWidth",scWidth);
	thisSession.setAttribute("sessionName","er");
	

	//bind the session for tracking after setting all the attributes
	
	EresSessionBinding eresSession = new EresSessionBinding(); 
	thisSession.setAttribute("eresSessionBinder",eresSession);

	userSession = Integer.parseInt(sessionTime) * 60;
	thisSession.setMaxInactiveInterval(userSession);

 
%>

<!--Session is set<%=userSession%>-->


<%

int contentTime = 1;

	if (isAutoLogin)
	{
		contentTime = 5;
	%>
		<BR><BR><BR><BR><BR><BR>
		<p class="sectionHeadings" align="center"> <b><%=MC.M_NewLog_PlsWait%><%-- A new login was created for you <BR>
		Please wait...*****--%></b>
		</p>
		
	<%
	}
%>

<META HTTP-EQUIV=Refresh CONTENT="<%=contentTime%>; URL=patientHome.jsp">




<%
 

}



}



%>
<!-- Added by gopu to fix bugzilla issues # 2439 -->	
<script>
	document.getElementById("userId").value = "<%=output%>";
</script>

</form>

</body>

</HTML>















 

