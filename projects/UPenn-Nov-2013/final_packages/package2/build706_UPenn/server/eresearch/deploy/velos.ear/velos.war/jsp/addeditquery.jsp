<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_AddOrEdit_Queries%><%--Add/Edit Queries*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>				
<%-- Nicholas : Start --%>
<jsp:include page="skinChoser.jsp" flush="true"/>
<%--<SCRIPT LANGUAGE="JavaScript" SRC="whichcss.js"></SCRIPT>--%>
<%-- Nicholas : End --%>
<Link Rel=STYLESHEET HREF='./styles/velos_popup.css' type=text/css>
<script>

function openQueryHistoryWin(formobj,formQueryId,fieldName)
{
        param1="formQueryHistory.jsp?formQueryId="+formQueryId+"&fieldName="+encodeString(fieldName);
		windowName= window.open(param1,"information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,left=90,top=80,width=800,height=300");
		windowName.focus();
}

function openAddNewWin(filledFormId, from, formId)
{
  param1="addNewQuery.jsp?queryType=2&thread=new&&filledFormId="+filledFormId+"&from="+from+"&formId="+formId;
		windowName= window.open(param1,"information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,left=90,top=80,width=600,height=500");
		windowName.focus();

}



	
</script>


<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id="formQueryDao" scope="request" class="com.velos.eres.business.common.FormQueryDao"/>
<jsp:useBean id="formQueryB" scope="request" class="com.velos.eres.web.formQuery.FormQueryJB"/>

<%@ page language = "java" import = "com.velos.eres.business.section.*,java.util.*,com.velos.eres.service.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB"%>

<BR>


<%
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
if (agent1 != null && agent1.indexOf("MSIE") != -1) 
   	ienet = 0; //IE
else
	ienet = 1;

if (ienet == 0) {
%>
<body style="overflow:scroll">
<%} else {%>
<body>
<%}%>







<DIV id="div1"> 
<b><p class="defcomments"> <%=LC.L_FrmAdd_EditQry%><%--Forms >> Add/Edit Queries*****--%></p></b>

  <%
   HttpSession tSession = request.getSession(true); 
   if (sessionmaint.isValidSession(tSession))
   { 
	   /* YK Bug#4822 19May2011 */	
      int pageRight = 0;
      pageRight = EJBUtil.stringToNum((String)session.getAttribute("formQueryRight"));
	  String userId = (String) tSession.getValue("userId");
      String accountId = (String) tSession.getValue("accountId");		
	 
		GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
		int account_form_rights = Integer.parseInt(grpRights.getFtrRightsByValue("MACCFRMS"));

   
     
     String filledFormId = "";
     String from = "";
     int count = 0;
	String studyId = "";

	
	   int iaccId=EJBUtil.stringToNum(accountId);

    
	 if (isAccessibleFor(pageRight, 'V'))
	 {
	 	 filledFormId = request.getParameter("filledFormId");
	 	 from = request.getParameter("from");
	 	
	 	studyId = request.getParameter("studyId");
	 	
	 	if (StringUtil.isEmpty(studyId))
	 	{
	 		studyId="";
	 	}
	 	
	 		
		 
		 String formId= request.getParameter("formId");
%>
		<Form  name="addEditQueries" method="post" action="">
	  <%
		int formQueryId = 0;
		int queryStatusId = 0;
		String entrdOn = "";
		int fieldId = 0; 
		String queryStat = "";
		String entrdBy = "";
		String fieldName = "";
		String queryType = "";
	
		formQueryDao = formQueryB.getQueriesForForm(EJBUtil.stringToNum(formId), EJBUtil.stringToNum(filledFormId), EJBUtil.stringToNum(from));

		ArrayList formQueryIds = formQueryDao.getFormQueryIds();
		
				
		ArrayList queryStatusIds = formQueryDao.getFormQueryStatusIds();
		
		ArrayList queryTypes = formQueryDao.getQueryType();
		
				
		ArrayList enteredOn = formQueryDao.getEnteredOn();
		
				
		//ArrayList fieldIds = formQueryDao.getFieldIds() ;
				
		ArrayList queryStatus = formQueryDao.getQueryStatus();
				
		ArrayList enteredBy = formQueryDao.getEnteredBy();
				
		ArrayList fieldNames = formQueryDao.getFieldNames();
		
		int rowsReturned = formQueryIds.size();
	
	
	  %>
	    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="basetbl">
		<tr>
	    <td width="80%"><P class="sectionheadings"><%=LC.L_Queries_ForForm%><%--Queries for Form*****--%></P></td>
		<td>
		<%if (isAccessibleFor(pageRight, 'E')){%>
		<A href="addNewQuery.jsp?studyId=<%=studyId%>&queryType=2&thread=new&filledFormId=<%=filledFormId%>&from=<%=from%>&formId=<%=formId%>&from=1" ><%=LC.L_Add_New%><%--Add New*****--%> </A>
		<%}
		%>
		</td>		
		</tr>
		
		</table>
<%-- Nicholas : Start --%>
    <table class="basetbl outline" width="100%" cellspacing="0" cellpadding="0" border="0">
<%-- Nicholas : End --%>
       <tr> 

       <th > <%=LC.L_Query_Id%><%--Query ID*****--%></th> <%-- YK -20DEC EDC_AT3 REQ {POINT 3} --%>
       <th > <%=LC.L_Field%><%--Field*****--%></th>
	   <th > <%=LC.L_Query%><%--Query*****--%></th>
	   <th > <%=LC.L_Latest_Status%><%--Latest Status*****--%></th>
	   <th > <%=LC.L_Query_By%><%--Query By*****--%></th>
	   <th > <%=LC.L_Query_Date%><%--Query Date*****--%></th>	  
	   </tr>
	   <% 
	    
	    for(count = 0;count < rowsReturned;count++)
					{
						 formQueryId = (   (Integer)formQueryIds.get(count)  ).intValue();
						 
						 queryStatusId = (   (Integer)queryStatusIds.get(count)  ).intValue();
						 
						 queryType =  ((queryTypes.get(count)) == null)?"-":(queryTypes.get(count)).toString();
					 	 
						 entrdOn = ((enteredOn.get(count)) == null)?"-":(enteredOn.get(count)).toString();
						 
						 queryStat = ((queryStatus.get(count)) == null)?"-":(queryStatus.get(count)).toString();
						 
						 entrdBy = ((enteredBy.get(count)) == null)?"-":(enteredBy.get(count)).toString();
						
						 fieldName = ((fieldNames.get(count)) == null)?"-":(fieldNames.get(count)).toString();
						 
						 
						if ((count%2)==0) 
						{
						%>
					      	<tr class="browserEvenRow">
						<%}else
						 { %>
				      		<tr class="browserOddRow">
						<%} %>
						<td><%=formQueryId%></td> <%-- YK -20DEC EDC_AT3 REQ {POINT 3} --%>
						<td>
						<A href="formQueryHistory.jsp?studyId=<%=studyId%>&formQueryId=<%=formQueryId%>&fieldName=<%=StringUtil.encodeString(fieldName)%>&filledFormId=<%=filledFormId%>&from=<%=from%>&formId=<%=formId%>"><%=fieldName%></A>
						</td>
						<td><%=queryType%></td>
						<td><%=queryStat%></td>						
						<td><%
						if(entrdBy.equals("system-generated")){%>
						&lt;<%=entrdBy%>&gt;
						<%}else{%>
						<%=entrdBy%>
						<%}%>
						</td>
						
						<td><%=entrdOn%></A>
						</td>
								
			    	 	</tr>
						
				<%	}//counter
	%>
	  </table>
	  </FORM>
 <%
		
	} //end of if body for page right

else
{%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%
} //end of else body for page right
	}
else
{ %>
 <jsp:include page="timeout.html" flush="true"/> 
<%}%>
  
</body>

</html>
<%!
private boolean isAccessibleFor(int pageRight, char type) {
    switch(type) {
    case 'V': return ((pageRight & 0x0004) >> 2) == 1;
    case 'E': return ((pageRight & 0x0002) >> 1) == 1;
    case 'N': return  (pageRight & 0x0001) == 1;
    }
    return false;
}
%>