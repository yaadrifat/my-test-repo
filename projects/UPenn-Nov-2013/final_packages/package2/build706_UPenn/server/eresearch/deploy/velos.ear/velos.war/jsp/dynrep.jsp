<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title><%=LC.L_Dynamic_RptCreation%><%--Dynamic Report Creation*****--%></title>
	<%@ page language = "java" import = "java.util.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB"%>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<jsp:useBean id="dynrepB" scope="page" class="com.velos.eres.web.dynrep.DynRepJB" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<SCRIPT>
window.name = "dynrep";
var formWin = null;
 function  validate(formobj){
 	  //sonika
 	  	  alert("<%=MC.M_RptCreatedPrev_ChgCntSave%>");/*alert("This report has been created in a previous version of Ad-Hoc Query. You can make changes to 'Preview' but can not save them.");*****/
	  	return false;

		 if((formobj.rbSharedWith[2].checked == true) && (formobj.selGrpNames.value==""))
		 {
		 alert("<%=MC.M_PlsSelectGrp%>");/*alert("Please Select Group");*****/
		 return false;
		 }

		 if((formobj.rbSharedWith[3].checked == true) && (formobj.selStudy.value==""))
		 {
		 alert("<%=MC.M_PlsSelectStd%>");/*alert("Please Select Study");*****/
		 return false;
		 }

		 if((formobj.rbSharedWith[4].checked == true) && (formobj.selOrg.value==""))
		 {
		 alert("<%=MC.M_PlsSelectOrg%>");/*alert("Please Select Organization");*****/
		 return false;
		 }
 
 //end sonika
 

 repName=fnTrimSpaces(formobj.repName.value);
 if (repName.length==0){
 alert("<%=MC.M_Etr_RptTemplateName%>");/*alert("Please enter report template name");*****/
 formobj.repName.focus();
 return false;
 }
 
 if (!(validateDataSize(formobj.repDesc,500,'Report Description'))) return false ;
	
     if (!(validate_col('eSign',formobj.eSign))) return false
     if (!(validateDataSize(formobj.repHeader,4000,'Report Header'))) 
     { 
     	
     	return false;
     }
     
      
	if(isNaN(formobj.eSign.value) == true) {

	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect E-Signature. Please enter again");*****/

	formobj.eSign.focus();
	
	return false;
   }
formobj.action="updatedynrep.jsp";
formobj.target="";
}
function openFormWin(formobj,act) {

formobj.target="formWin";
formobj.action=act;
formWin = open('donotdelete.html','formWin','resizable=1,status=0, width=850,height=550 top=100,left=100,scrollbars=1');
if (formWin && !formWin.closed) formWin.focus();
//formobj.submit();
document.dynrep.submit();
void(0);
}

function setAlignment(formobj,param)
{
if (formobj.repFooter.value.length>0)
{
  if (param=="CF") formobj.footerAlign.value="[VELCENTER]";
  if (param=="LF") formobj.footerAlign.value="[VELLEFT]";
  if (param=="RF") formobj.footerAlign.value="[VELRIGHT]";
  }
  if (formobj.repHeader.value.length>0) 
  {
  if (param=="CH") formobj.headerAlign.value="[VELCENTER]";
  if (param=="LH") formobj.headerAlign.value="[VELLEFT]";
  if (param=="RH") formobj.headerAlign.value="[VELRIGHT]";
  }
}

function openLookup(formobj){
accId=formobj.accId.value;
userId=formobj.userId.value;
studyId=formobj.study1.value;
studyflg=formobj.studyselect[0].checked;
if (studyflg){
	studyList=formobj.studyList.value;	
   filter=" ab.Fk_study in ( "+ studyList + ")  and  ab.FK_FORMLIB=b.pk_formlib and e.FK_FORMLIB=b.pk_formlib and " +  
 	 "  ab.record_type <> 'D'  and e.formstat_enddate IS NULL and e.fk_codelst_stat in " + 
	 "  (select pk_codelst from er_codelst where lower(codelst_subtyp)='a' and lower(codelst_type)='frmstat' ) and ab.lf_displaytype IN ('S')";
}
else{
if (studyId.length==0){
alert("<%=MC.M_PlsSel_StdFirst%>");/*alert("Please Select a Study First");*****/
return;
 }
 filter=" ab.Fk_study= "+ studyId + "  and  ab.FK_FORMLIB=b.pk_formlib and e.FK_FORMLIB=b.pk_formlib and " +  
 	 "  ab.record_type <> 'D'  and e.formstat_enddate IS NULL and e.fk_codelst_stat in " + 
	 "  (select pk_codelst from er_codelst where lower(codelst_subtyp)='a' and lower(codelst_type)='frmstat') and ab.lf_displaytype IN ('S') ";
}
//filter1=" F2.FK_ACCOUNT ="+ accId + " AND F2.FK_FORMLIB = F1.PK_FORMLIB and " +   
//" F2.record_type <> 'D'   AND F4.FK_FORMLIB = F1.PK_FORMLIB   and F4.formstat_enddate IS NULL " +  
// "  and lf_displaytype IN ( 'S','SP','A','SA','PA' )  and F4.fk_codelst_stat in (select pk_codelst from er_codelst where lower(codelst_desc)='active') " ;
    //alert(filter);
 
 
   
windowname=window.open("getlookup.jsp?viewId=&viewName=Form Browser&form=dynreport&dfilter=" + filter + "&keyword=formName|VELFORMNAME~formId|VELFORMPK" ,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=700,height=550 top=100,left=100 0, "); 
windowname.focus();	
}
function selectFlds(formobj){
formId=formobj.formId.value;
if (formId.length==0){
alert("<%=MC.M_Selc_Frm%>");/*alert("Please select a form");*****/
return false ;
}

formName=formobj.formName.value;
src=formobj.srcmenu.value;
selectedTab=formobj.selectedTab.value;
repHeader=formobj.repHeader.value;
repFooter=formobj.repFooter.value;
from=formobj.from.value;
studyId=formobj.study1.value;
windowname=window.open("dynrepselect.jsp?formId=" +formId + "&srcmenu=" + src+"&selectedTab="+selectedTab+"&formName="+formName+"&repHeader="+repHeader+"&repFooter="+repFooter+"&from="+from+"&study1="+studyId ,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=850,height=550 top=100,left=100 0, "); 
windowname.focus();
}
function setSeqFlag(formobj){
formobj.seqChg.value="Y";

}





</SCRIPT>
<% String src,tempStr="",fldCol="";
   int strlen=0,firstpos=-1,secondpos=-1;
src= request.getParameter("srcmenu");
System.out.println(src);

%>



<jsp:include page="panel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>
<jsp:param name="sess" value="keep"/>

</jsp:include>   

<body>
<DIV class="formDefault" id="div1"> 
<%
        String formId="",formName="",formType="",fldType="",repIdStr="",fldDisp="",fldWidth="",fldId="";
	String repName="",repHdr="",repFtr="",repType="",repFilter="",selStudy="",repDesc="",dynType="",shareWith="",headAlign="",footAlign="";
	String[] fldName,fldOrder;
	int seq=0,repId=0;
	String mode = request.getParameter("mode");
	mode=(mode==null)?"":mode;
	mode=(mode.equals("link"))?"M":mode;
	String selectedTab = request.getParameter("selectedTab");
	ArrayList selectedfld=null;
	HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession))
	{
	 HashMap attributes=new HashMap();
	 HashMap SortAttr=new HashMap();
	 HashMap TypeAttr=new HashMap();
	 HashMap FltrAttr=new HashMap();
	 HashMap RepAttr=new HashMap();
	 
	 attributes=(HashMap)tSession.getAttribute("attributes");
	 	if (attributes==null){%>
	<META HTTP-EQUIV=Refresh CONTENT="0; URL=dynrepbrowse.jsp?srcmenu=<%=src%>">
	
	<%}else {
	 if (attributes.containsKey("SortAttr")) SortAttr=(HashMap)attributes.get("SortAttr");
	 if (attributes.containsKey("RepAttr")) RepAttr=(HashMap)attributes.get("RepAttr");
	 if (attributes.containsKey("TypeAttr")) TypeAttr=(HashMap)attributes.get("TypeAttr");
	 if (attributes.containsKey("FltrAttr")) FltrAttr=(HashMap)attributes.get("FltrAttr");
	 fldName=request.getParameterValues("fldName");
	 if (fldName==null){
	 fldName=(String[])SortAttr.get("fldName");
	 } else{
	 SortAttr.put("fldName",fldName);
	 } 
	  fldOrder=request.getParameterValues("fldOrder");
	  if (fldOrder==null){
	 fldOrder=(String[])SortAttr.get("fldOrder");
	 } else{
	 SortAttr.put("fldOrder",fldOrder);
	 }
	 
	 attributes.put("SortAttr",SortAttr);
	 String repfilter=request.getParameter("filter");
	 repfilter=(repfilter==null)?"":repfilter;
	 if (repfilter.length()==0) if (FltrAttr.containsKey("repFilter")) formId=(String)FltrAttr.get("repFilter");
	 repfilter=(repfilter==null)?"":repfilter;
	 
	 String dataOrder=request.getParameter("dataOrder");
	 dataOrder=(dataOrder==null)?"":dataOrder;
	 if (dataOrder.length()==0) if (SortAttr.containsKey("dataOrder")) dataOrder=(String)SortAttr.get("dataOrder");
	 dataOrder=(dataOrder==null)?"":dataOrder;
	 
	 String order=request.getParameter("order");
	 order=(order==null)?"":order;
	 if ((order.length()==0) && (dataOrder.length()>0)) {
	 order =" Order By " + StringUtil.replaceAll(dataOrder,"[$]"," ");
	 order=StringUtil.replaceAll(order,"[$$]",",");
	 System.out.println("order***"+order+"***dataOrder***"+dataOrder);
	 }
	 if (RepAttr.containsKey("shareWith")) shareWith=(String)RepAttr.get("shareWith");
	 shareWith=(shareWith==null)?"":shareWith;
	 System.out.println("********ShareWith*************"+shareWith);
	 if (RepAttr.containsKey("repHdr")) repHdr=(String)RepAttr.get("repHdr");
	 repHdr=(repHdr==null)?"":repHdr;
	 if ((repHdr.indexOf("[VELCENTER]")>=0)||(repHdr.indexOf("[VELLEFT]")>=0)||(repHdr.indexOf("[VELRIGHT]")>=0))
	 {
	   headAlign=repHdr.substring(0,(repHdr.indexOf(":",0)));
	   repHdr=repHdr.substring(headAlign.length()+1);
	   }
	   else headAlign="[VELCENTER]"; 
	   
	 System.out.println("repHdr"+repHdr);
	 System.out.println("headalign"+headAlign);
	 	 
	 if (RepAttr.containsKey("repFtr")) repFtr=(String)RepAttr.get("repFtr");
	 repFtr=(repFtr==null)?"":repFtr;
	 if ((repFtr.indexOf("[VELCENTER]")>=0)||(repFtr.indexOf("[VELLEFT]")>=0)||(repFtr.indexOf("[VELRIGHT]")>=0))
	  {
	  footAlign=repFtr.substring(0,(repFtr.indexOf(":",0)));
	  repFtr=repFtr.substring(footAlign.length()+1);
	 }
	 else footAlign="[VELCENTER]";
	 System.out.println("repFtr+"+repFtr);
	 System.out.println("footAlign+"+footAlign);
	 if (RepAttr.containsKey("repName")) repName=(String)RepAttr.get("repName");
	 repName=(repName==null)?"":repName;
	 System.out.println("repName"+repName);
	 if (RepAttr.containsKey("repDesc")) repDesc=(String)RepAttr.get("repDesc");
	 repDesc=(repDesc==null)?"":repDesc;
	 System.out.println("repDEsc"+repDesc);
	 if (RepAttr.containsKey("repId")) repId=EJBUtil.stringToNum((String)RepAttr.get("repId"));
	 System.out.println("repId"+repId);
	 
	 
	  
	 formId= request.getParameter("formId");
	 if (formId==null) formId="";
	 if (formId.length()==0){
	 System.out.println("****************formId.length()"+formId+formId.length()+"*********************");
	 if (TypeAttr.containsKey("formId")) formId=(String)TypeAttr.get("formId");
	 }
	System.out.println("****************formId.length()"+formId+formId.length()+"*********************");
	
	 formName=request.getParameter("formName");
	  if (attributes.containsKey("TypeAttr")) formType=((String)((HashMap)attributes.get("TypeAttr")).get("formType"));
	 formType=(formType==null)?"":formType;
	 
	 if (TypeAttr.containsKey("dynType")) dynType=(String) TypeAttr.get("dynType");
	if (dynType==null) dynType="";
	 String accId = (String) tSession.getValue("accountId");
	 String userId=	(String) tSession.getValue("userId");
	 String userIdFromSession = (String) tSession.getValue("userId");
	 if( formName==null) formName="";
	  if (formName.length()==0) formName=(String)TypeAttr.get("formName");
	 if( formName==null) formName="";
	 if( formId==null) formId="";
	 int counter=0;
	 System.out.println("*****************************************************************************************");
	if (attributes.containsKey("FldAttr")) System.out.println("I am here-FldAttr"+attributes.get("FldAttr"));
	System.out.println("*****************************************************************************************");
	if (attributes.containsKey("TypeAttr")) System.out.println("I am here-TypeAttr"+attributes.get("TypeAttr"));
	System.out.println("*****************************************************************************************");
	if (attributes.containsKey("FltrAttr")) System.out.println("I am here-FltrAttr"+attributes.get("FltrAttr"));
	System.out.println("*****************************************************************************************");
	if (attributes.containsKey("SortAttr")) System.out.println("I am here-SortAttr"+attributes.get("SortAttr"));
	System.out.println("*****************************************************************************************");
	if (attributes.containsKey("RepAttr")) System.out.println("I am here-ReptAttr"+attributes.get("RepAttr"));
	System.out.println("*****************************************************************************************");
	
%>
<P class="sectionHeadings"> <%=LC.L_AdhocQry_Save%><%--Ad-Hoc Query >> Save*****--%> </P>
<%

%>

<form name="dynrep"  METHOD=POST  action="updatedynrep.jsp" onsubmit="return validate(document.dynrep);">
	


<table><tr>
<%if (attributes.get("TypeAttr")!=null){%>
<td><A href="dynreptype.jsp?mode=link&srcmenu=<%=src%>&dynType=<%=dynType%>"><%=LC.L_Select_RptType%><%--Select Report Type*****--%></A></td>
<%}else{%>
<td><p class="sectionHeadings"><%=LC.L_Select_RptType%><%--Select Report Type*****--%></p></td>
<%}%>
<td><p class="sectionHeadings">>></p></td>
<%if (attributes.get("FldAttr")!=null){%>
<td><A href="dynselectflds.jsp?mode=link&srcmenu=<%=src%>"><%=LC.L_Select_Flds%><%--Select Fields*****--%></A></td>

<%}else{%>
<td><p class="sectionHeadings"><%=LC.L_Select_Flds%><%--Select Fields*****--%></p></td>
<%}%>
<td><p class="sectionHeadings">>></p></td>
<%if (attributes.get("FltrAttr")!=null){%>
<td><A href="dynfilter.jsp?mode=link&srcmenu=<%=src%>"><%=LC.L_Filter_Criteria%><%--Filter Criteria*****--%></A></td>

<%}else{%>
<td><p class="sectionHeadings"><%=LC.L_Filter_Criteria%><%--Filter Criteria*****--%></p></td>
<%}%>
<td><p class="sectionHeadings">>></p></td>
<%if (attributes.get("SortAttr")!=null){%>
<td><A href="dynorder.jsp?mode=link&srcmenu=<%=src%>"><%=LC.L_Sort_Criteria%><%--Sort Criteria*****--%></A></td>

<%}else{%>
<td><p class="sectionHeadings"><%=LC.L_Sort_Criteria%><%--Sort Criteria*****--%></p></td>
<%}%>
<td><p class="sectionHeadings">>></p></td>
<%if (attributes.get("RepAttr")!=null){%>
<td><A href="dynrep.jsp?mode=link&srcmenu=<%=src%>"><font color="red"><b><%=LC.L_Preview_Save%><%--Preview & Save*****--%></b></font></A></td>

<%}else{%>
<td><p class="sectionHeadings"><font color="red"><b><%=LC.L_Preview_Save%><%--Preview & Save*****--%></b></font></p></td>
<%}%>
</tr></table>
<input type=hidden name="mode" value=<%=mode%>>
<input type=hidden name="srcmenu" value=<%=src%>>
<input type=hidden name="selectedTab" value=<%=selectedTab%>>
<input type=hidden name="accId" value="<%=accId%>">
<input type=hidden name="userId" value="<%=userIdFromSession%>">
<input type="hidden" name="formId" readonly value="<%=formId%>">
<input type="hidden" name="formType" readonly value="<%=repType%>">
<input type="hidden" name="from" readonly value="study">
<input type="hidden" name="repId" readonly value="<%=repId%>">
<input name="repFilter" type="hidden" size="100" value="<%=repfilter%>">
<input name="order" type="hidden" size="100" value="<%=order%>">
<input name="dataOrder" type="hidden" size="100" value="<%=dataOrder%>">
<P class="sectionHeadings"><%=LC.L_Selected_Form%><%--Selected Form*****--%>: <%=formName%> </P>
   <br><P class="sectionHeadings"> <%=LC.L_Format_YourRpt%><%--Format Your Report*****--%>  </P>
   <td width="50%"><input type="hidden" name="headerAlign" value="<%=headAlign%>" size= "50" maxlength="100"></td>
   <td width="50%"><input type="hidden" name="footerAlign" value="<%=footAlign%>"  size="50" maxlength="100"></td>
  <table width="100%">
  <tr height="90"></tr>
     <tr><td width="15%"><%=LC.L_Rpt_Header%><%--Report Header*****--%></td>
     <td width="25%"><textarea type="text" name="repHeader" value="<%=repHdr%>" rows="3" cols="50"><%=repHdr%></textArea></td>
     <td>
     <table>
     <tr><td><input name="alignHeader" type="radio" <% if (headAlign.equals("[VELCENTER]")){ %>checked <%}%> onClick="setAlignment(document.dynrep,'CH')"><%=LC.L_Center%><%--Center*****--%></td></tr>
     <tr><td><input name="alignHeader" type="radio" <% if (headAlign.equals("[VELLEFT]")){ %>checked <%}%> onClick="setAlignment(document.dynrep,'LH')"><%=LC.L_Left%><%--Left*****--%></td></tr>
     <tr><td><input name="alignHeader" type="radio" <% if (headAlign.equals("[VELRIGHT]")){ %>checked <%}%> onClick="setAlignment(document.dynrep,'RH')"><%=LC.L_Right%><%--Right*****--%></td></tr>
     </table>
     </td>
     
     </tr>
     <tr><td width="15%"><%=LC.L_Rpt_Footer%><%--Report Footer*****--%></td>
     
     <td width="25%"><textarea type="text" name="repFooter" value="<%=repFtr%>" rows="3" cols="50"><%=repFtr%></textArea></td>
     <td>
     <table>
     <tr><td><input name="alignFooter" type="radio" checked <% if (footAlign.equals("[VELCENTER]")){ %>checked <%}%> onClick="setAlignment(document.dynrep,'CF')"><%=LC.L_Center%><%--Center*****--%></td></tr>
     <tr><td><input name="alignFooter" type="radio" <% if (footAlign.equals("[VELLEFT]")){ %>checked <%}%> onClick="setAlignment(document.dynrep,'LF')"><%=LC.L_Left%><%--Left*****--%></td></tr>
     <tr><td><input name="alignFooter" type="radio"  <% if (footAlign.equals("[VELRIGHT]")){ %>checked <%}%> onClick="setAlignment(document.dynrep,'RF')"><%=LC.L_Right%><%--Right*****--%></td></tr>
     </table>
     </td>
     </tr>
     </table>
     <table width="100%">
     <tr><td width="50%"></td><td><A href="#" onClick="openFormWin(document.dynrep,'dynpreview.jsp');"><img src="../images/jpg/displayreport.jpg" align="absmiddle" border="0"></A></td></tr>
     </table>
     <table>
      <tr><td width="15%"><%=MC.M_Save_RptTemplate%><%--Save Report Template as*****--%><FONT class="Mandatory">* </FONT> </td><td width="50%"><input type="text" name="repName" value="<%=repName%>"></td></tr>
      <tr><td width="15%"><%=LC.L_Template_Desc%><%--Template Description*****--%> </td><td width="50%">
      <textarea type="text" name="repDesc" value="<%=repDesc%>" rows="3" cols="30"><%=repDesc%></textArea>
      </td></tr>
     	<!--sonika-->
<%	
	String repMode = "";	
	if (TypeAttr.containsKey("repMode")) repMode = (String)TypeAttr.get("repMode");
%>
   	 <jsp:include page="objectsharewith.jsp" flush="true">
   	 <jsp:param name="objNumber" value="2"/>
   	 <jsp:param name="mode" value="<%=repMode%>"/>
   	 <jsp:param name="formobject" value="document.dynrep"/>
   	 <jsp:param name="formnamevalue" value="dynrep"/>
   	 <jsp:param name="fkObj" value="<%=repId%>"/>	 		  	
   	 <jsp:param name="sharedWith" value="<%=shareWith%>"/>	 		  	 		 	 	 	 
	 </jsp:include>
	<!--end sonika-->    
    </table>
    <table width="100%" >
   <tr><td width="15%">

		<%=LC.L_Esignature%><%--e-Signature*****--%> <FONT class="Mandatory">* </FONT>

	   </td>

	   <td width="50%">

		<input type="password" name="eSign" maxlength="8" autocomplete="off">

	   </td>

	</tr></table>
	<table width="100%" >
<tr><td width="50%"></td>
<td>&nbsp;<button type="submit"><%=LC.L_Submit%></button></td>
</tr>
<!--<tr><td width="50%"></td><td><A href="#" onClick="openFormWin(document.dynrep,'dynpreview.jsp');"><img src="../images/jpg/displayreport.jpg" align="absmiddle" border="0"></A></td></tr>-->

	</table>
<input type="hidden" name="sess" value="keep">
</form>

<%
}// end for (attributes==null)
} else {  //else of if body for session

%>
  <jsp:include page="timeout.html" flush="true"/>

  <%
}


%>
 <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</DIV>
  <div class ="mainMenu" id = "emenu">
  <jsp:include page="getmenu.jsp" flush="true"/>
  </div>


</body>
</html>
