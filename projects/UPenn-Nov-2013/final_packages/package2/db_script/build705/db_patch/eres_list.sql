whenever oserror exit -1
whenever sqlerror exit -1
@eres/00_er_version.sql
@eres/01_ER_REPORT_Update.sql
@eres/02_ER_LKPCOL_UPDATE.sql
@eres/03_ERV_PATIENT_ADVERSE_EVENTS.sql
@eres/04_ER_REPXSL_DEL.sql
@eres/05_ER_STUDY_ALTER_TABLE.sql
@eres/06_ER_CODELST_UPDATE.sql
spool done_eres.txt
quit
