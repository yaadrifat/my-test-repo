set define off;

DELETE from er_repxsl where pk_repxsl in (181);

commit;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,304,4,'04_ER_REPXSL_DEL.sql',sysdate,'v9.3.0 #705');

commit;