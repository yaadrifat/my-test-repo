whenever oserror exit -1
whenever sqlerror exit -1
@eres/01_NCI4_LKPUP.sql
@eres/02_SAE_PAGECUSTOM.sql &1
@eres/03_Update_race_records.sql
@eres/04_PatientEnrollmentWorkflow.sql &2
@eres/05_NewCodelistItems.sql
@eres/06_ExistingCodelistItems.sql
spool done_eres.txt
quit
