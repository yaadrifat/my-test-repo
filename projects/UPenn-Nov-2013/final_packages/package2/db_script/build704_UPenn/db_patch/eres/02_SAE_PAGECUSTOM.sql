whenever oserror exit -1
whenever sqlerror exit -1

SET DEFINE OFF;
DECLARE
  v_cnt           NUMBER;
  v_acc           NUMBER := [FK_ACCOUNT];
  v_fk_pagecustom NUMBER;
BEGIN
  
  SELECT PK_PAGECUSTOM
  INTO v_fk_pagecustom
  FROM ERES.er_pagecustom
  WHERE PAGECUSTOM_PAGE='adverseevent'
  AND FK_ACCOUNT=v_acc;
  
  SELECT COUNT(*)
  INTO v_cnt
  FROM er_pagecustomflds
  WHERE FK_PAGECUSTOM=v_fk_pagecustom
  AND pagecustomflds_field='aetype';
  IF(v_cnt                <1) THEN
    INSERT
    INTO er_pagecustomflds
      (
        pk_pagecustomflds,
        fk_pagecustom,
        pagecustomflds_field,
        pagecustomflds_mandatory,
        PAGECUSTOMFLDS_LABEL
      )
      VALUES
      (
        seq_er_pagecustomflds.nextval,
        v_fk_pagecustom,
        'aetype',
        1,
        'Was this event serious?'
      );
  ELSE
    UPDATE er_pagecustomflds
    SET pagecustomflds_mandatory=1,
      PAGECUSTOMFLDS_LABEL='Was this event serious?'
    WHERE FK_PAGECUSTOM=v_fk_pagecustom
    AND pagecustomflds_field='aetype';
  END IF;

END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,303,8,'02_SAE_PAGECUSTOM.sql',sysdate,'v9.3.0 #704');

commit;
