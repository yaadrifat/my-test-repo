whenever oserror exit -1
whenever sqlerror exit -1

set define off;

declare
v_exists NUMBER := 0;
v_nextVal NUMBER := 0;

v_PK_WORKFLOW NUMBER := 0;
v_PK_ACTIVITY NUMBER := 0;
v_PK_WORKFLOWACTIVITY NUMBER := 0;

v_PK_WFForm NUMBER := [PK_FORMLIB];
begin
	update WORKFLOW_ACTIVITY SET WA_DISPLAYQUERY = 'SELECT COUNT(*) cnt from er_formslinear where fk_form = '|| TO_CHAR(v_PK_WFForm) ||' and id = :3 and fk_patprot in (select pk_patprot from er_patprot where fk_study = :2 and fk_per = :3)'
	where WA_NAME = 'More Subject Details' and WA_SEQUENCE = 4;		
	COMMIT;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,305,10,'06_UpdateFormIDForWFActivity.sql',sysdate,'v9.3.0 #706');

commit;
