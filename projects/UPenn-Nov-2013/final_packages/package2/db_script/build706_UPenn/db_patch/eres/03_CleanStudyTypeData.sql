set define off;

update er_study set FK_CODELST_TYPE = null where FK_CODELST_TYPE in (
select pk_codelst FROM ER_CODELST WHERE CODELST_TYPE = 'study_type' AND CODELST_SUBTYP in('healthSResearch', 'basicScienceTx', 
'treatment', 'supportive', 'retroAnalysis'));

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,305,7,'03_CleanStudyTypeData.sql',sysdate,'v9.3.0 #706');

commit;