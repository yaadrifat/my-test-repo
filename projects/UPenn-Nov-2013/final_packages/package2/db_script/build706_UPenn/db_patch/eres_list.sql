whenever oserror exit -1
whenever sqlerror exit -1
@eres/01_pgcustom.sql &1
@eres/02_AnatomicSite_Lookup.sql
@eres/03_CleanStudyTypeData.sql
@eres/04_UpdateCodeItems.sql
@eres/05_SiteType_Lookup.sql
@eres/06_UpdateFormIDForWFActivity.sql &2
spool done_eres.txt
quit
