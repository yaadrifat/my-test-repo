CREATE OR REPLACE PACKAGE BODY "ERES"."PKG_WORKFLOW"
AS
PROCEDURE "SP_UPDATE_WORKFLOW"(
    p_entity_id     NUMBER,
    p_workflow_type VARCHAR2,
    p_userid        NUMBER,
    pendingtasks OUT NUMBER )
AS

TYPE CurTyp
IS
  REF
  CURSOR;
  -- define weak REF CURSOR type
    cur_cv CurTyp; -- declare cursor variable
    v_pk_wf   NUMBER;
    v_pk_wfi  NUMBER;
    sql_fetch VARCHAR2(4000);
    tmp_rescount number;
  BEGIN
    
    SELECT PK_WORKFLOW
    INTO v_pk_wf
    FROM WORKFLOW
    WHERE WORKFLOW_TYPE=p_workflow_type;
  
  SELECT PK_WFINSTANCEID
  INTO v_pk_wfi
  FROM WORKFLOW_INSTANCE
  WHERE FK_ENTITYID=p_entity_id
  AND FK_WORKFLOWID=v_pk_wf;
  
  FOR alltasks IN
  (  SELECT wa_keytable,
      wfa.pk_workflowactivity,
      wfa.wa_sequence,
      wfa.wa_terminateflag,
      wi.fk_workflowid,
      t.pk_taskid,
      t.fk_entity,
      t.fk_wfinstanceid,
      wfa.WA_DISPLAYQUERY
    FROM 
      workflow_activity wfa,
      workflow_instance wi,
      task t
    WHERE wi.pk_wfinstanceid       = t.fk_wfinstanceid
    AND fk_wfinstanceid       = v_pk_wfi
    ORDER BY wa_sequence)
  
  LOOP
    
    sql_fetch := ''; -- clear
    sql_fetch := REPLACE( alltasks.WA_DISPLAYQUERY, ':1', p_entity_id );
    plog.DEBUG(pCTX,'sqlfetch  ' || sql_fetch);
    
    IF(LENGTH(sql_fetch) >1 ) THEN
      -- check the post condition status
      OPEN cur_cv FOR sql_fetch;
      LOOP
        FETCH cur_cv INTO tmp_rescount ; -- fetch next row
        EXIT
      WHEN cur_cv%NOTFOUND; -- exit loop when last row is fetched
        IF(tmp_rescount > 0) THEN
          -- update task
          plog.DEBUG(pCTX,'pk_taskid  ' || alltasks.pk_taskid);
    
          UPDATE task
          SET task_completeflag = 1,
            completedon         = sysdate
          WHERE pk_taskid      IN (alltasks.pk_taskid);
        END IF;
      END LOOP; -- end of pending tasks
    END IF;
  END LOOP;
  
  SELECT COUNT(*)
  INTO pendingtasks
  FROM TASK,
    WORKFLOW_INSTANCE
  WHERE PK_WFINSTANCEID=FK_WFINSTANCEID
  AND PK_WFINSTANCEID  =v_pk_wfi
  AND task_completeflag=0;
  
  plog.DEBUG(pCTX,'pendingtasks  ' || pendingtasks);
    
  
  IF(pendingtasks      =0) THEN
    UPDATE WORKFLOW_INSTANCE SET WI_STATUSFLAG=1 WHERE PK_WFINSTANCEID=v_pk_wfi;
  END IF;
END SP_UPDATE_WORKFLOW;
END PKG_WORKFLOW;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,302,4,'04_PKG_WORKFLOW_BODY.sql',sysdate,'v9.3.0 #703');

commit;
