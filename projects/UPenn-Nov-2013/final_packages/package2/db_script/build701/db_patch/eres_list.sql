whenever oserror exit -1
whenever sqlerror exit -1
@eres/00_er_version.sql
@eres/01_ALTER_COLUMN.sql
@eres/02_ERV_PATIENT_ADVERSE_EVENTS.sql
@eres/03_AHQ_LOOKUP.sql
@eres/04_ER_REPXSL_DEL.sql
@eres/05_ER_REPORT_88.sql
@eres/06_ER_REPORT_85.sql
@eres/07_f_create_aexml.sql
@eres/08_ERV_FORMQUERY.sql
@eres/09_ERV_PERSON_COMPLETE.sql
@eres/10_DELETE_PATIENT.sql
@eres/11_CodelistLookupChanges.sql
@eres/12_MoreStudyDetailsAHQ.sql
spool done_eres.txt
quit
