set define off;

update er_codelst set codelst_desc = codelst_subtyp || ' - ' || codelst_desc where codelst_type = 'country';

commit;


update er_codelst set codelst_custom_col1 = 'eth_prim', codelst_custom_col = null where codelst_type = 'ethnicity' and codelst_custom_col = 'primary';

commit;

declare
v_lkpFilterExists NUMBER := 0;
begin
	select count(*) into  v_lkpFilterExists from er_lkpfilter where LKPFILTER_KEY = 'countryCode';

	if (v_lkpFilterExists = 0) then
		INSERT INTO ER_LKPFILTER (PK_LKPFILTER, LKPFILTER_KEY, LKPFILTER_SQL, LKPFILTER_COMMENT)
		VALUES (SEQ_ER_LKPFILTER.nextval, 'countryCode', 'codelst_type=''country'' AND codelst_hide <> ''Y''', 'Country lookup filter');

		COMMIT;
	end if;
end;
/

update er_codelst set codelst_custom_col1 = codelst_custom_col, codelst_custom_col = null where codelst_type = 'disease_site' and codelst_custom_col is not null;

commit;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,300,11,'11_CodelistLookupChanges.sql',sysdate,'v9.3.0 #701');

commit;