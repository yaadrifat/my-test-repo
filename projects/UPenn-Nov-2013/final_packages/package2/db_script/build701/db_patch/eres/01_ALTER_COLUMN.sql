SET DEFINE OFF;

-- Increasing the column size so no data loss issues expected


ALTER TABLE EPAT.PAT_PERID MODIFY (PERID_ID VARCHAR2(4000));

ALTER TABLE ERES.ER_STUDYID MODIFY (STUDYID_ID VARCHAR2(4000));

/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,300,1,'01_ALTER_COLUMN.sql',sysdate,'v9.3.0 #701');

commit;
