whenever oserror exit -1
whenever sqlerror exit -1

SET DEFINE OFF;
DECLARE
  v_cnt           NUMBER;
  v_acc           NUMBER := [FK_ACCOUNT];
  v_fk_pagecustom NUMBER;
BEGIN
 
  SELECT PK_PAGECUSTOM
  INTO v_fk_pagecustom
  FROM ERES.er_pagecustom
  WHERE PAGECUSTOM_PAGE='adverseevent'
  AND FK_ACCOUNT       =v_acc;
  
  SELECT COUNT(*)
  INTO v_cnt
  FROM er_pagecustomflds
  WHERE FK_PAGECUSTOM     =v_fk_pagecustom
  AND pagecustomflds_field='aetype';
  
  IF(v_cnt                <1) THEN
    INSERT
    INTO er_pagecustomflds
      (
        pk_pagecustomflds,
        fk_pagecustom,
        pagecustomflds_field,
        pagecustomflds_mandatory
      )
      VALUES
      (
        seq_er_pagecustomflds.nextval,
        v_fk_pagecustom,
        'aetype',
        1
      );
  ELSE
    UPDATE er_pagecustomflds
    SET pagecustomflds_mandatory=1,
      PAGECUSTOMFLDS_LABEL      =NULL
    WHERE FK_PAGECUSTOM         =v_fk_pagecustom
    AND pagecustomflds_field    ='aetype';
  END IF;
  
  SELECT COUNT(*)
  INTO v_cnt
  FROM ER_CODELST
  WHERE CODELST_TYPE='advtype'
  AND CODELST_SUBTYP='gradeOvrRideL';
  
  IF(v_cnt          <1) THEN
    INSERT
    INTO ER_CODELST
      (
        PK_CODELST,
        CODELST_TYPE,
        CODELST_SUBTYP,
        CODELST_DESC,
        CODELST_CUSTOM_COL,
        CODELST_SEQ
      )
      VALUES
      (
        SEQ_ER_CODELST.nextval,
        'advtype',
        'gradeOvrRideL',
        '<h4><font color="red">The Grade for this Event was previously overridden</font><h4>',
        'splfld',
        1
      );
  END IF;
  
  SELECT COUNT(*)
  INTO v_cnt
  FROM ER_CODELST
  WHERE CODELST_TYPE='advtype'
  AND CODELST_SUBTYP='gradeOvrRideF';
  
  IF(v_cnt          <1) THEN
    INSERT
    INTO ER_CODELST
      (
        PK_CODELST,
        CODELST_TYPE,
        CODELST_SUBTYP,
        CODELST_DESC,
        CODELST_CUSTOM_COL,
        CODELST_SEQ
      )
      VALUES
      (
        SEQ_ER_CODELST.nextval,
        'advtype',
        'gradeOvrRideF',
        'The Grade for this Event was previously overridden',
        'hidden-input',
        1
      );
  END IF;
  
  UPDATE ER_CODELST
  SET CODELST_SEQ   =0
  WHERE CODELST_TYPE='advtype'
  AND CODELST_SUBTYP='rep_typ';

END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,302,8,'02_gradeOvrRide.sql',sysdate,'v9.3.0 #703');

commit;
