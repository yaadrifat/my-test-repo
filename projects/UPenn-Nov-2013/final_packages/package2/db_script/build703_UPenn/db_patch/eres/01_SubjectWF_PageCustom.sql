whenever oserror exit -1
whenever sqlerror exit -1

set define off;

declare
v_PK_PAGECUSTOM NUMBER := 0;
v_FK_ACCOUNT NUMBER := [FK_ACCOUNT];
begin
	SELECT PK_PAGECUSTOM INTO v_PK_PAGECUSTOM FROM ER_PAGECUSTOM WHERE FK_ACCOUNT = v_FK_ACCOUNT AND PAGECUSTOM_PAGE = 'patient';
	
	if (v_PK_PAGECUSTOM <= 0) then
		INSERT INTO ER_PAGECUSTOM (PK_PAGECUSTOM, FK_ACCOUNT, PAGECUSTOM_PAGE) 
		VALUES (SEQ_ER_PAGECUSTOM.nextval, v_FK_ACCOUNT, 'patient');

		COMMIT;
		
		SELECT PK_PAGECUSTOM INTO v_PK_PAGECUSTOM FROM ER_PAGECUSTOM WHERE FK_ACCOUNT = v_FK_ACCOUNT AND PAGECUSTOM_PAGE = 'patient';
	else 
		DELETE FROM ER_PAGECUSTOMFLDS WHERE FK_PAGECUSTOM = v_PK_PAGECUSTOM;
	end if;


	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'patid', null, 1, '');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'fname', null, 1, 'First Initial');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'midname', 0, null, '');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'lname', null, 1, 'Last Initial');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'dob', null, 0, '');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'gender', null, 1, 'Sex');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'maritalstat', 0, null, '');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'bloodgrp', 0, null, '');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'ssn', 0, null, '');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'email', 0, null, '');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'address1', 0, null, '');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'address2', 0, null, '');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'city', 0, null, '');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'state', 0, null, '');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'county', 0, null, '');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'zipcode', null, null, 'Zip Code');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'country', null, 1, '');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'hphone', 0, null, '');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'wphone', 0, null, '');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'ethnicity', null, 1, 'Ethnicity');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'race', null, 1, 'Race');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'additional1', null, null, 'Additional Ethnicities');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'additional2', null, 1, 'Multiple Races');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'dod', null, null, 'Date Deceased');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'cod', null, null, '');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'speccause', 0, null, '');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'pnotes', null, null, 'Comments');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'patfacid', null, null, 'MRN#');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'timezone', null, null, '');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'employment', 0, null, '');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'education', 0, null, '');

	COMMIT;
end;
/

	
declare
v_PK_PAGECUSTOM NUMBER := 0;
v_FK_ACCOUNT NUMBER := [fk_account];
begin
	SELECT PK_PAGECUSTOM INTO v_PK_PAGECUSTOM FROM ER_PAGECUSTOM WHERE FK_ACCOUNT = v_FK_ACCOUNT AND PAGECUSTOM_PAGE = 'patstudystatus';
	
	if (v_PK_PAGECUSTOM <= 0) then
		INSERT INTO ER_PAGECUSTOM (PK_PAGECUSTOM, FK_ACCOUNT, PAGECUSTOM_PAGE) 
		VALUES (SEQ_ER_PAGECUSTOM.nextval, v_FK_ACCOUNT, 'patstudystatus');
		
		COMMIT;
		
		SELECT PK_PAGECUSTOM INTO v_PK_PAGECUSTOM FROM ER_PAGECUSTOM WHERE FK_ACCOUNT = v_FK_ACCOUNT AND PAGECUSTOM_PAGE = 'patstudystatus';
	else 
		DELETE FROM ER_PAGECUSTOMFLDS WHERE FK_PAGECUSTOM = v_PK_PAGECUSTOM;
	end if;

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'patstatus', null, 1, 'Current Status');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'reason', 0, null, '');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'statusdate', null, 1, 'Date on Study');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'notes', null, null, 'Comments');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'patstdid', null, 1, 'Subject ID#');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'enrollingsite', null, null, 'Institution');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'assignedto', 0, null, '');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'physician', 0, null, '');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'treatmentlocation', 0, null, '');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'treatingorg', 0, null, '');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'diseaseCode', 0, null, '');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'othrDiseaseCode', 0, null, '');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'evaluableflag', 0, null, '');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'evaluablestatus', 0, null, '');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'inevaluablestatus', 0, null, '');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'dod', null, null, 'Date Deceased');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'cod', null, null, 'Cause');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'specifycause', 0, null, '');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'deathrelated', null, null, 'Death Relation to Study');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'reasonofdeathrel', 0, null, '');

	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'randomnumber', 0, null, '');
	
	COMMIT;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,302,7,'01_SubjectWF_PageCustom.sql',sysdate,'v9.3.0 #703');

commit;
