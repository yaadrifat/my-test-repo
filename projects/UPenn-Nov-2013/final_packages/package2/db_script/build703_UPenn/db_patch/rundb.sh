#!/bin/bash

none() { return 0; }
show_usage() {
  echo "===Usage:";
  echo "rundb.sh DB_NAME ERES_PWD ESCH_PWD EPAT_PWD FK_ACCOUNT FK_FORMLIB";
  return 0;
}

if [ "$1" -a "$2" -a "$3" -a "$4" ];
then
  none;
else
  show_usage;
  exit -1;
fi

if [ ! -f done_eres.txt ];
then
  echo "===Running eres...";
  sqlplus eres/$2@$1 @eres_list.sql $5 $6
  if [ ! -f done_eres.txt ];
  then
    echo "An error occurred in eres.";
    exit -1;
  fi
fi

touch "done_all.txt";
echo "===Done with this build";
exit 0;
