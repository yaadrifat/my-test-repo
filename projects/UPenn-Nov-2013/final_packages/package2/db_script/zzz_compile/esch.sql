Set heading off; 
set feedback off; 
set echo off; 
Set lines 999; 
Spool run_invalid_esch.sql 
select   'ALTER ' || OBJECT_TYPE || ' ' ||  OWNER || '.' || OBJECT_NAME || ' COMPILE;' 
from   dba_objects where  status = 'INVALID' and owner='ESCH' and  object_type in ('PACKAGE','FUNCTION','PROCEDURE','TRIGGER','VIEW');
select   'ALTER PACKAGE' || ' ' ||  OWNER || '.' || OBJECT_NAME || ' COMPILE BODY;'   
from   dba_objects where  status = 'INVALID' and owner='ESCH' and  object_type in ('PACKAGE BODY');
spool off; 
set heading on; 
set feedback on; 
set echo on; 
@run_invalid_esch.sql