set define off;

DECLARE
v_count NUMBER := 0;
BEGIN
	SELECT count(*) INTO v_count FROM SYS.user_sequences WHERE SEQUENCE_NAME = 'SEQ_ER_PAGECUSTOMFLDS';
	if (v_count = 0) then
		execute immediate 'CREATE SEQUENCE  "ERES"."SEQ_ER_PAGECUSTOMFLDS"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 5047 NOCACHE  NOORDER  NOCYCLE';
	end if;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,298,3,'03_sequence.sql',sysdate,'v9.3.0 #699');
commit;