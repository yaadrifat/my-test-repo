#!/bin/bash

none() { return 0; }
show_usage() {
  echo "===Usage:";
  echo "rundb.sh DB_NAME ERES_PWD ESCH_PWD EPAT_PWD ";
  return 0;
}

if [ "$1" -a "$2" -a "$3" -a "$4" ];
then
  none;
else
  show_usage;
  exit -1;
fi

if [ ! -f done_eres.txt ];
then
  echo "===Running eres...";
  sqlplus eres/$2@$1 @eres_list.sql $1 $2
  if [ ! -f done_eres.txt ];
  then
    echo "An error occurred in eres.";
    exit -1;
  fi
fi

if [ ! -f done_esch.txt ];
then
  echo "===Running esch...";
  sqlplus esch/$3@$1 @esch_list.sql $1 $3
  if [ ! -f done_esch.txt ];
  then
    echo "An error occurred in esch.";
    exit -1;
  fi
fi

if [ ! -f done_epat.txt ];
then
  echo "===Running epat...";
  sqlplus epat/$4@$1 @epat_list.sql $1 $4
  if [ ! -f done_epat.txt ];
  then
    echo "An error occurred in epat.";
    exit -1;
  fi
fi

if [ -r xsl ];
then
  if [ ! -f done_xsl.txt ];
  then
    echo "===Running xsl...";
    cd xsl
    ./loadxsl.sh $2 $1
    cd ..
    if [ ! -f xsl/genrepxsl.log ];
    then
      exit -1;
    fi
    if [ -f xsl/GENREPXSL.bad ];
    then
      exit -1;
    else
      touch done_xsl.txt
    fi
  fi
fi

if [ -r xsl-fo ];
then
  if [ ! -f done_xsl-fo.txt ];
  then
    echo "===Running xsl-fo...";
    cd xsl-fo
    ./loadxsl.sh $2 $1
    cd ..
    if [ ! -f xsl-fo/genrepxsl.log ];
    then
      exit -1;
    fi
    if [ -f xsl-fo/GENREPXSL.bad ];
    then
      exit -1;
    else
      touch done_xsl-fo.txt
    fi
  fi
fi

if [ -r messagetext ];
then
  if [ ! -f done_messagetext.txt ];
  then
    echo "===Running messagetext...";
    cd messagetext
    ./loadmsg.sh $3 $1
    cd ..
    if [ ! -f messagetext/msg.log ];
    then
      exit -1;
    fi
    if [ -f messagetext/message.bad ];
    then
      exit -1;
    else
      touch done_messagetext.txt
    fi
  fi
fi

touch "done_all.txt";
echo "===Done with this build";
exit 0;
