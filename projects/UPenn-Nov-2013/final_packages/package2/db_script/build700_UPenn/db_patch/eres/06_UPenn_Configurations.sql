whenever oserror exit -1
whenever sqlerror exit -1

SET DEFINE OFF;

DECLARE
v_cnt NUMBER;
v_acc NUMBER;
v_fk_pagecustom NUMBER;

BEGIN
v_cnt := 0;
v_acc := [FK_ACCOUNT];

SELECT count(*) INTO v_cnt FROM ERES.er_pagecustom 
where PAGECUSTOM_PAGE='adverseevent' AND FK_ACCOUNT=v_acc;


IF (v_cnt!=1)
THEN
  INSERT INTO ERES.er_pagecustom (PK_PAGECUSTOM,FK_ACCOUNT,PAGECUSTOM_PAGE)
  VALUES (ERES.SEQ_ER_PAGECUSTOM.nextval,v_acc,'adverseevent');
END IF;
SELECT pk_pagecustom INTO v_fk_pagecustom FROM ERES.er_pagecustom 
  where PAGECUSTOM_PAGE='adverseevent' AND FK_ACCOUNT=v_acc;

-- delete all entries from page custom
  DELETE FROM ERES.er_pagecustomflds 
  WHERE fk_pagecustom=v_fk_pagecustom;

----------------------------------------

INSERT
INTO ERES.er_pagecustomflds
  (pk_pagecustomflds,fk_pagecustom,pagecustomflds_field,pagecustomflds_mandatory,pagecustomflds_label)
  VALUES (ERES.seq_er_pagecustomflds.nextval,v_fk_pagecustom,'aetype',1,'Was this event serious?');


UPDATE SCH_CODELST SET CODELST_HIDE='Y'
WHERE  CODELST_TYPE='adve_type' AND CODELST_SUBTYP='al_none';

UPDATE SCH_CODELST SET CODELST_HIDE='Y'
WHERE  CODELST_TYPE='adve_type' AND CODELST_SUBTYP = 'al_lateadv';


SELECT COUNT(*) INTO v_cnt FROM ESCH.SCH_CODELST 
WHERE CODELST_TYPE='adve_type' AND CODELST_SUBTYP='al_adve';

IF v_cnt>0
THEN
  UPDATE ESCH.SCH_CODELST SET CODELST_HIDE='N',CODELST_DESC='No',CODELST_SEQ=1
  WHERE  CODELST_TYPE='adve_type' AND CODELST_SUBTYP='al_adve';
ELSE
  INSERT INTO ESCH.SCH_CODELST 
  (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
  values (ESCH.SCH_CODELST_SEQ1.nextval,'adve_type','al_adve','No','N',1);
END IF;


SELECT COUNT(*) INTO v_cnt FROM ESCH.SCH_CODELST
WHERE  CODELST_TYPE='adve_type' AND CODELST_SUBTYP='al_sadve';

IF v_cnt>0
THEN
  UPDATE ESCH.SCH_CODELST SET CODELST_HIDE='N',CODELST_DESC='Yes',CODELST_SEQ=2 
  WHERE CODELST_TYPE='adve_type' AND CODELST_SUBTYP='al_sadve';
ELSE
  INSERT INTO ESCH.SCH_CODELST 
  (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
  values (ESCH.SCH_CODELST_SEQ1.nextval,'adve_type','al_sadve','Yes','N',2);
END IF;
--------------------------------------------------------------------------------

INSERT
INTO ERES.er_pagecustomflds
  (pk_pagecustomflds,fk_pagecustom,pagecustomflds_field,pagecustomflds_mandatory,
    pagecustomflds_label)
  VALUES (ERES.seq_er_pagecustomflds.nextval,v_fk_pagecustom,'discoverydate',
    0,'Date Study Staff/PI Aware');  
-------------------------------------------------------------------
INSERT
INTO ERES.er_pagecustomflds
  (pk_pagecustomflds,fk_pagecustom,pagecustomflds_field,pagecustomflds_mandatory,
    pagecustomflds_label)VALUES
  (ERES.seq_er_pagecustomflds.nextval,v_fk_pagecustom,'attribution',1,
    'Does PI Believe Event is Related to Study Regimen?');


UPDATE ESCH.SCH_CODELST SET CODELST_HIDE='Y' 
WHERE CODELST_TYPE='adve_relation' AND CODELST_SUBTYP='ad_unknown';

UPDATE ESCH.SCH_CODELST SET CODELST_HIDE='Y' 
WHERE CODELST_TYPE='adve_relation' AND CODELST_SUBTYP = 'ad_unlike';



SELECT COUNT(*) INTO v_cnt FROM ESCH.SCH_CODELST 
WHERE CODELST_TYPE='adve_relation' AND CODELST_SUBTYP='ad_nr';
IF v_cnt>0
THEN
  UPDATE ESCH.SCH_CODELST SET CODELST_HIDE='N',CODELST_DESC='Unrelated',CODELST_SEQ=1 
  WHERE CODELST_TYPE='adve_relation' AND CODELST_SUBTYP='ad_nr';
ELSE
  INSERT INTO ESCH.SCH_CODELST 
  (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
  values (ESCH.SCH_CODELST_SEQ1.nextval,'adve_relation','ad_nr','Unrelated','N',1);
END IF;


SELECT COUNT(*) INTO v_cnt FROM ESCH.SCH_CODELST 
WHERE CODELST_TYPE='adve_relation' AND CODELST_SUBTYP='ad_def         ';
IF v_cnt>0
THEN
  UPDATE ESCH.SCH_CODELST SET CODELST_HIDE='N',CODELST_DESC='Definitely',CODELST_SEQ=2 
  WHERE CODELST_TYPE='adve_relation' AND CODELST_SUBTYP='ad_def         ';
ELSE
  INSERT INTO ESCH.SCH_CODELST 
  (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
  values (ESCH.SCH_CODELST_SEQ1.nextval,'adve_relation','ad_def','Definitely','N',2);
END IF;




SELECT COUNT(*) INTO v_cnt FROM ESCH.SCH_CODELST 
WHERE CODELST_TYPE='adve_relation' AND CODELST_SUBTYP='ad_pos';
IF v_cnt>0
THEN
  UPDATE ESCH.SCH_CODELST SET CODELST_HIDE='N',CODELST_DESC='Possibly',CODELST_SEQ=3 
  WHERE CODELST_TYPE='adve_relation' AND CODELST_SUBTYP='ad_pos';
ELSE
  INSERT INTO ESCH.SCH_CODELST 
  (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
  values (ESCH.SCH_CODELST_SEQ1.nextval,'adve_relation','ad_pos','Possibly','N',3);
END IF;

SELECT COUNT(*) INTO v_cnt FROM ESCH.SCH_CODELST 
WHERE CODELST_TYPE='adve_relation' AND CODELST_SUBTYP='ad_prob';
IF v_cnt>0
THEN
  UPDATE ESCH.SCH_CODELST SET CODELST_HIDE='N',CODELST_DESC='Probably',CODELST_SEQ=4 
  WHERE CODELST_TYPE='adve_relation' AND CODELST_SUBTYP='ad_prob';
ELSE
  INSERT INTO ESCH.SCH_CODELST 
  (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
  values (ESCH.SCH_CODELST_SEQ1.nextval,'adve_relation','ad_prob','Probably','N',4);
END IF;
--------------------------------------------------------------------------------

SELECT COUNT(*) INTO v_cnt FROM ESCH.SCH_CODELST 
WHERE CODELST_TYPE='outcome' AND CODELST_SUBTYP='al_cong';
IF v_cnt>0
THEN
  UPDATE ESCH.SCH_CODELST SET CODELST_HIDE='N',CODELST_DESC='Congenital anomaly',CODELST_SEQ=1 
  WHERE CODELST_TYPE='outcome' AND CODELST_SUBTYP='al_cong';
ELSE
  INSERT INTO ESCH.SCH_CODELST 
  (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
  values (ESCH.SCH_CODELST_SEQ1.nextval,'outcome','al_cong','Congenital anomaly','N',1);
END IF;

SELECT COUNT(*) INTO v_cnt FROM ESCH.SCH_CODELST 
WHERE CODELST_TYPE='outcome' AND CODELST_SUBTYP='al_interven';
IF v_cnt>0
THEN
  UPDATE ESCH.SCH_CODELST SET CODELST_HIDE='N',CODELST_DESC='Consulting doctor/Ongoing',CODELST_SEQ=2 
  WHERE CODELST_TYPE='outcome' AND CODELST_SUBTYP='al_interven';
ELSE
  INSERT INTO ESCH.SCH_CODELST 
  (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
  values (ESCH.SCH_CODELST_SEQ1.nextval,'outcome','al_interven','Consulting doctor/Ongoing','N',2);
END IF;

-- 'Death'/al_death
SELECT COUNT(*) INTO v_cnt FROM ESCH.SCH_CODELST 
WHERE CODELST_TYPE='outcome' AND CODELST_SUBTYP='al_death';
IF v_cnt>0
THEN
  UPDATE ESCH.SCH_CODELST SET CODELST_HIDE='N',CODELST_DESC='Fatal',CODELST_SEQ=3 
  WHERE CODELST_TYPE='outcome' AND CODELST_SUBTYP='al_death';
ELSE
  INSERT INTO ESCH.SCH_CODELST 
  (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
  values (ESCH.SCH_CODELST_SEQ1.nextval,'outcome','al_death','Fatal','N',3);
END IF;

SELECT COUNT(*) INTO v_cnt FROM ESCH.SCH_CODELST 
WHERE CODELST_TYPE='outcome' AND CODELST_SUBTYP='al_lifeth';
IF v_cnt>0
THEN
  UPDATE ESCH.SCH_CODELST SET CODELST_HIDE='N',CODELST_DESC='Life-Threatening',CODELST_SEQ=4 
  WHERE CODELST_TYPE='outcome' AND CODELST_SUBTYP='al_lifeth';
ELSE
  INSERT INTO ESCH.SCH_CODELST 
  (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
  values (ESCH.SCH_CODELST_SEQ1.nextval,'outcome','al_lifeth','Life-Threatening','N',4);
END IF;

SELECT COUNT(*) INTO v_cnt FROM ESCH.SCH_CODELST 
WHERE CODELST_TYPE='outcome' AND CODELST_SUBTYP='al_noflwup';
IF v_cnt>0
THEN
  UPDATE ESCH.SCH_CODELST SET CODELST_HIDE='N',CODELST_DESC='No follow-up',CODELST_SEQ=5 
  WHERE CODELST_TYPE='outcome' AND CODELST_SUBTYP='al_noflwup';
ELSE
  INSERT INTO ESCH.SCH_CODELST 
  (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
  values (ESCH.SCH_CODELST_SEQ1.nextval,'outcome','al_noflwup','No follow-up','N',5);
END IF;

SELECT COUNT(*) INTO v_cnt FROM ESCH.SCH_CODELST 
WHERE CODELST_TYPE='outcome' AND CODELST_SUBTYP='al_recover';
IF v_cnt>0
THEN
  UPDATE ESCH.SCH_CODELST SET CODELST_HIDE='N',CODELST_DESC='Recovered',CODELST_SEQ=6 
  WHERE CODELST_TYPE='outcome' AND CODELST_SUBTYP='al_recover';
ELSE
  INSERT INTO ESCH.SCH_CODELST 
  (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
  values (ESCH.SCH_CODELST_SEQ1.nextval,'outcome','al_recover','Recovered','N',6);
END IF;

SELECT COUNT(*) INTO v_cnt FROM ESCH.SCH_CODELST 
WHERE CODELST_TYPE='outcome' AND CODELST_SUBTYP='al_refflwup';
IF v_cnt>0
THEN
  UPDATE ESCH.SCH_CODELST SET CODELST_HIDE='N',CODELST_DESC='Referred for Follow-Up',CODELST_SEQ=7 
  WHERE CODELST_TYPE='outcome' AND CODELST_SUBTYP='al_refflwup';
ELSE
  INSERT INTO ESCH.SCH_CODELST 
  (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
  values (ESCH.SCH_CODELST_SEQ1.nextval,'outcome','al_refflwup','Referred for Follow-Up','N',7);
END IF;

SELECT COUNT(*) INTO v_cnt FROM ESCH.SCH_CODELST 
WHERE CODELST_TYPE='outcome' AND CODELST_SUBTYP='al_hosp';
IF v_cnt>0
THEN
  UPDATE ESCH.SCH_CODELST SET CODELST_HIDE='N',CODELST_DESC='Resulted in or prolonged inpatient hospitalization'
  ,CODELST_SEQ=8 WHERE CODELST_TYPE='outcome' AND CODELST_SUBTYP='al_hosp';
ELSE
  INSERT INTO ESCH.SCH_CODELST 
  (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
  values (ESCH.SCH_CODELST_SEQ1.nextval,'outcome','al_hosp','Resulted in or prolonged inpatient hospitalization','N',8);
END IF;

SELECT COUNT(*) INTO v_cnt FROM ESCH.SCH_CODELST 
WHERE CODELST_TYPE='outcome' AND CODELST_SUBTYP='al_disable';
IF v_cnt>0
THEN
  UPDATE ESCH.SCH_CODELST SET CODELST_HIDE='N',CODELST_DESC='Resulted in persistent or significant disability/incapacity'
  ,CODELST_SEQ=9 WHERE CODELST_TYPE='outcome' AND CODELST_SUBTYP='al_disable';
ELSE
  INSERT INTO ESCH.SCH_CODELST 
  (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
  values (ESCH.SCH_CODELST_SEQ1.nextval,'outcome','al_disable','Resulted in persistent or significant disability/incapacity','N',9);
END IF;

SELECT COUNT(*) INTO v_cnt FROM ESCH.SCH_CODELST 
WHERE CODELST_TYPE='outcome' AND CODELST_SUBTYP='al_medjudge';
IF v_cnt>0
THEN
  UPDATE ESCH.SCH_CODELST SET CODELST_HIDE='N',CODELST_DESC='Serious based on medical judgment'
  ,CODELST_SEQ=10 WHERE CODELST_TYPE='outcome' AND CODELST_SUBTYP='al_medjudge';
ELSE
  INSERT INTO ESCH.SCH_CODELST 
  (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
  values (ESCH.SCH_CODELST_SEQ1.nextval,'outcome','al_medjudge','Serious based on medical judgment','N',10);
END IF;
-------------------------------------------------------------------------------
INSERT
INTO ERES.er_pagecustomflds
  (pk_pagecustomflds,fk_pagecustom,pagecustomflds_field,pagecustomflds_mandatory,
    pagecustomflds_label) VALUES
  (ERES.seq_er_pagecustomflds.nextval,v_fk_pagecustom,'action',0,'Treatment Action Taken'
  );

SELECT COUNT(*) INTO v_cnt FROM ESCH.SCH_CODELST 
WHERE CODELST_TYPE='outaction' AND CODELST_SUBTYP='oa_none';
IF v_cnt>0
THEN
  UPDATE ESCH.SCH_CODELST SET CODELST_HIDE='N',CODELST_DESC='None',CODELST_SEQ=1 
  WHERE CODELST_TYPE='outaction' AND CODELST_SUBTYP='oa_none';
ELSE
  INSERT INTO ESCH.SCH_CODELST 
  (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
  values (ESCH.SCH_CODELST_SEQ1.nextval,'outaction','oa_none','None','N',1);
END IF;


SELECT COUNT(*) INTO v_cnt FROM ESCH.SCH_CODELST 
WHERE CODELST_TYPE='outaction' AND CODELST_SUBTYP='oa_dosemedi';
IF v_cnt>0
THEN
  UPDATE ESCH.SCH_CODELST SET CODELST_HIDE='N',CODELST_DESC='Dose Interrupted',CODELST_SEQ=2 
  WHERE CODELST_TYPE='outaction' AND CODELST_SUBTYP='oa_dosemedi';
ELSE
  INSERT INTO ESCH.SCH_CODELST 
  (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
  values (ESCH.SCH_CODELST_SEQ1.nextval,'outaction','oa_dosemedi','Dose Interrupted','N',2);
END IF;


SELECT COUNT(*) INTO v_cnt FROM ESCH.SCH_CODELST 
WHERE CODELST_TYPE='outaction' AND CODELST_SUBTYP='oa_mediSurInter';
IF v_cnt>0
THEN
  UPDATE ESCH.SCH_CODELST SET CODELST_HIDE='N',CODELST_DESC='Dose Discontinued',CODELST_SEQ=3 
  WHERE CODELST_TYPE='outaction' AND CODELST_SUBTYP='oa_mediSurInter';
ELSE
  INSERT INTO ESCH.SCH_CODELST 
  (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
  values (ESCH.SCH_CODELST_SEQ1.nextval,'outaction','oa_mediSurInter','Dose Discontinued','N',3);
END IF;

SELECT COUNT(*) INTO v_cnt FROM ESCH.SCH_CODELST 
WHERE CODELST_TYPE='outaction' AND CODELST_SUBTYP='oa_doseredu';
IF v_cnt>0
THEN
  UPDATE ESCH.SCH_CODELST SET CODELST_HIDE='N',CODELST_DESC='Dose Reduced',CODELST_SEQ=4 
  WHERE CODELST_TYPE='outaction' AND CODELST_SUBTYP='oa_doseredu';
ELSE
  INSERT INTO ESCH.SCH_CODELST 
  (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
  values (ESCH.SCH_CODELST_SEQ1.nextval,'outaction','oa_doseredu','Dose Reduced','N',4);
END IF;


SELECT COUNT(*) INTO v_cnt FROM ESCH.SCH_CODELST 
WHERE CODELST_TYPE='outaction' AND CODELST_SUBTYP='oa_countmedi';
IF v_cnt>0
THEN
  UPDATE ESCH.SCH_CODELST SET CODELST_HIDE='N',CODELST_DESC='Concomitant Medicine',CODELST_SEQ=5 
  WHERE CODELST_TYPE='outaction' AND CODELST_SUBTYP='oa_countmedi';
ELSE
  INSERT INTO ESCH.SCH_CODELST 
  (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
  values (ESCH.SCH_CODELST_SEQ1.nextval,'outaction','oa_countmedi','Concomitant Medicine','N',5);
END IF;

-------------------------------------------
UPDATE ESCH.SCH_CODELST SET CODELST_HIDE='Y' 
WHERE CODELST_TYPE='adve_recovery' AND CODELST_SUBTYP='ad_mod';

UPDATE ESCH.SCH_CODELST SET CODELST_HIDE='Y' 
WHERE CODELST_TYPE='adve_recovery' AND CODELST_SUBTYP = 'ad_none';

UPDATE ESCH.SCH_CODELST SET CODELST_HIDE='Y' 
WHERE CODELST_TYPE='adve_recovery' AND CODELST_SUBTYP='ad_nyr';

UPDATE ESCH.SCH_CODELST SET CODELST_HIDE='Y' 
WHERE CODELST_TYPE='adve_recovery' AND CODELST_SUBTYP = 'ad_rec_un';



SELECT COUNT(*) INTO v_cnt FROM ESCH.SCH_CODELST 
WHERE CODELST_TYPE='adve_recovery' AND CODELST_SUBTYP='ad_comp';
IF v_cnt>0
THEN
  UPDATE ESCH.SCH_CODELST SET CODELST_HIDE='N',CODELST_DESC='Without Treatment',CODELST_SEQ=4 
  WHERE CODELST_TYPE='adve_recovery' AND CODELST_SUBTYP='ad_comp';
ELSE
  INSERT INTO ESCH.SCH_CODELST 
  (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
  values (ESCH.SCH_CODELST_SEQ1.nextval,'adve_recovery','ad_comp','Without Treatment','N',1);
END IF;


SELECT COUNT(*) INTO v_cnt FROM ESCH.SCH_CODELST 
WHERE CODELST_TYPE='adve_recovery' AND CODELST_SUBTYP='ad_min';
IF v_cnt>0
THEN
  UPDATE ESCH.SCH_CODELST SET CODELST_HIDE='N',CODELST_DESC='With Treatment',CODELST_SEQ=2 
  WHERE CODELST_TYPE='adve_recovery' AND CODELST_SUBTYP='ad_min';
ELSE
  INSERT INTO ESCH.SCH_CODELST 
  (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
  values (ESCH.SCH_CODELST_SEQ1.nextval,'adve_recovery','ad_min','With Treatment','N',2);
END IF;
------------------------------------------------------------------------

UPDATE ESCH.SCH_CODELST SET CODELST_HIDE='Y' 
WHERE CODELST_TYPE='adve_info' AND CODELST_SUBTYP = 'adv_frmcomp';

UPDATE ESCH.SCH_CODELST SET CODELST_HIDE='Y' 
WHERE CODELST_TYPE='adve_info' AND CODELST_SUBTYP = 'adv_rev_sign';

UPDATE ESCH.SCH_CODELST SET CODELST_HIDE='Y' 
WHERE CODELST_TYPE='adve_info' AND CODELST_SUBTYP = 'adv_frmsub';


SELECT COUNT(*) INTO v_cnt FROM ESCH.SCH_CODELST 
WHERE CODELST_TYPE='adve_info' AND CODELST_SUBTYP='adv_consent';
IF v_cnt>0
THEN
  UPDATE ESCH.SCH_CODELST SET CODELST_HIDE='N',CODELST_DESC='This event was Listed in Consent Form',CODELST_SEQ=1 
  WHERE CODELST_TYPE='adve_info' AND CODELST_SUBTYP='adv_consent';
ELSE
  INSERT INTO ESCH.SCH_CODELST 
  (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
  values (ESCH.SCH_CODELST_SEQ1.nextval,'adve_info','adv_consent','This event was Listed in Consent Form','N',1);
END IF;

-- 'Unexpected'/adv_unexp
SELECT COUNT(*) INTO v_cnt FROM ESCH.SCH_CODELST 
WHERE CODELST_TYPE='adve_info' AND CODELST_SUBTYP='adv_unexp';
IF v_cnt>0
THEN
  UPDATE ESCH.SCH_CODELST SET CODELST_HIDE='N',CODELST_DESC='This event was an Unanticipated Event (per IRB policy)?'
  ,CODELST_SEQ=2 WHERE CODELST_TYPE='adve_info' AND CODELST_SUBTYP='adv_unexp';
ELSE
  INSERT INTO ESCH.SCH_CODELST 
  (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
  values (ESCH.SCH_CODELST_SEQ1.nextval,'adve_info','adv_unexp','This event was an Unanticipated Event (per IRB policy)?','N',2);
END IF;

-- 'Listed in protocol'/adv_protocol
SELECT COUNT(*) INTO v_cnt FROM ESCH.SCH_CODELST 
WHERE CODELST_TYPE='adve_info' AND CODELST_SUBTYP='adv_protocol';
IF v_cnt>0
THEN
  UPDATE ESCH.SCH_CODELST SET CODELST_HIDE='N',CODELST_DESC='This event was Listed in Protocol',CODELST_SEQ=3 
  WHERE CODELST_TYPE='adve_info' AND CODELST_SUBTYP='adv_protocol';
ELSE
  INSERT INTO ESCH.SCH_CODELST 
  (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
  values (ESCH.SCH_CODELST_SEQ1.nextval,'adve_info','adv_protocol','This event was Listed in Protocol','N',3);
END IF;

-- 'Protocol violation'/adv_violation
SELECT COUNT(*) INTO v_cnt FROM ESCH.SCH_CODELST 
WHERE CODELST_TYPE='adve_info' AND CODELST_SUBTYP='adv_violation';
IF v_cnt>0
THEN
  UPDATE ESCH.SCH_CODELST SET CODELST_HIDE='N',CODELST_DESC='This event was reportable to the DSMC (per DSMC policy)?'
  ,CODELST_SEQ=4 WHERE CODELST_TYPE='adve_info' AND CODELST_SUBTYP='adv_violation';
ELSE
  INSERT INTO ESCH.SCH_CODELST 
  (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
  values (ESCH.SCH_CODELST_SEQ1.nextval,'adve_info','adv_violation','This event was reportable to the DSMC (per DSMC policy)?','N',4);
END IF;

-- 'Patient dropped out due to this event'/adv_dopped
SELECT COUNT(*) INTO v_cnt FROM ESCH.SCH_CODELST 
WHERE CODELST_TYPE='adve_info' AND CODELST_SUBTYP='adv_dopped';
IF v_cnt>0
THEN
  UPDATE ESCH.SCH_CODELST SET CODELST_HIDE='N',CODELST_DESC='This event was reported to the Medical Monitor'
  ,CODELST_SEQ=5 WHERE CODELST_TYPE='adve_info' AND CODELST_SUBTYP='adv_dopped';
ELSE
  INSERT INTO ESCH.SCH_CODELST 
  (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
  values (ESCH.SCH_CODELST_SEQ1.nextval,'adve_info','adv_dopped','This event was reported to the Medical Monitor','N',5);
END IF;

-- 'Dose Limiting Toxicity'/
SELECT COUNT(*) INTO v_cnt FROM ESCH.SCH_CODELST 
WHERE CODELST_TYPE='adve_info' AND CODELST_SUBTYP='adv_dlt';
IF v_cnt>0
THEN
  UPDATE ESCH.SCH_CODELST SET CODELST_HIDE='N',CODELST_DESC='This event constitutes a Dose Limiting Toxicity',CODELST_SEQ=6 
  WHERE CODELST_TYPE='adve_info' AND CODELST_SUBTYP='adv_dlt';
ELSE
  INSERT INTO ESCH.SCH_CODELST 
  (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
  values (ESCH.SCH_CODELST_SEQ1.nextval,'adve_info','adv_dlt','This event constitutes a Dose Limiting Toxicity','N',6);
END IF;

---------------------------------------------------------------------------


SELECT COUNT(*) INTO v_cnt FROM ESCH.SCH_CODELST 
WHERE CODELST_TYPE='adve_notify' AND CODELST_SUBTYP='an_co_grp';
IF v_cnt>0
THEN
  UPDATE ESCH.SCH_CODELST SET CODELST_HIDE='N',CODELST_DESC='Cooperative Group',CODELST_SEQ=2 
  WHERE CODELST_TYPE='adve_notify' AND CODELST_SUBTYP='an_co_grp';
ELSE
  INSERT INTO ESCH.SCH_CODELST 
  (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
  values (ESCH.SCH_CODELST_SEQ1.nextval,'adve_notify','an_co_grp','Cooperative Group','N',2);
END IF;

SELECT COUNT(*) INTO v_cnt FROM ESCH.SCH_CODELST 
WHERE CODELST_TYPE='adve_notify' AND CODELST_SUBTYP='an_nci';
IF v_cnt>0
THEN
  UPDATE ESCH.SCH_CODELST SET CODELST_HIDE='N',CODELST_DESC='NCI',CODELST_SEQ=3 
  WHERE CODELST_TYPE='adve_notify' AND CODELST_SUBTYP='an_nci';
ELSE
  INSERT INTO ESCH.SCH_CODELST 
  (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
  values (ESCH.SCH_CODELST_SEQ1.nextval,'adve_notify','an_nci','NCI','N',3);
END IF;


SELECT COUNT(*) INTO v_cnt FROM ESCH.SCH_CODELST 
WHERE CODELST_TYPE='adve_notify' AND CODELST_SUBTYP='an_dsmc';
IF v_cnt>0
THEN
  UPDATE ESCH.SCH_CODELST SET CODELST_HIDE='N',CODELST_DESC='DSMC',CODELST_SEQ=5 
  WHERE CODELST_TYPE='adve_notify' AND CODELST_SUBTYP='an_dsmc';
ELSE
  INSERT INTO ESCH.SCH_CODELST 
  (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
  values (ESCH.SCH_CODELST_SEQ1.nextval,'adve_notify','an_dsmc','DSMC','N',5);
END IF;



SELECT COUNT(*) INTO v_cnt FROM ESCH.SCH_CODELST 
WHERE CODELST_TYPE='adve_notify' AND CODELST_SUBTYP='an_ctrc';
IF v_cnt>0
THEN
  UPDATE ESCH.SCH_CODELST SET CODELST_HIDE='N',CODELST_DESC='CTRC',CODELST_SEQ=6 
  WHERE CODELST_TYPE='adve_notify' AND CODELST_SUBTYP='an_ctrc';
ELSE
  INSERT INTO ESCH.SCH_CODELST 
  (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
  values (ESCH.SCH_CODELST_SEQ1.nextval,'adve_notify','an_ctrc','CTRC','N',6);
END IF;

-----------------------------------------------------

INSERT
INTO ERES.er_pagecustomflds
  (pk_pagecustomflds,fk_pagecustom,pagecustomflds_field,pagecustomflds_mandatory,
    pagecustomflds_label)
  VALUES (ERES.seq_er_pagecustomflds.nextval,v_fk_pagecustom,'notes',0,'Comments');  

----------------------------------------

INSERT
INTO ERES.er_pagecustomflds
  (pk_pagecustomflds,fk_pagecustom,pagecustomflds_field,pagecustomflds_mandatory,
    pagecustomflds_label)
  VALUES (ERES.seq_er_pagecustomflds.nextval,v_fk_pagecustom,'description',1,'Description of Event');  

--------------------------------------------
INSERT
INTO ERES.er_pagecustomflds
  (pk_pagecustomflds,fk_pagecustom,pagecustomflds_field,pagecustomflds_mandatory,
    pagecustomflds_label)
  VALUES (ERES.seq_er_pagecustomflds.nextval,v_fk_pagecustom,'loggeddate',1,'Data Entry Date');

--------------------------------------------------
INSERT INTO ERES.ER_CODELST
  (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_SEQ,CODELST_CUSTOM_COL,
    CODELST_CUSTOM_COL1) VALUES
  (ERES.SEQ_ER_CODELST.NEXTVAL,'advtype','rep_typ','Report Type',1,
    'dropdown','<select name="alternateId"><option value="followUpRep">Follow-up Report</option><option value="initialRep">Initial Report</option></select>'
  );
-----------------------------------------------------------

INSERT
INTO ERES.ER_CODELST
  (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_SEQ,CODELST_CUSTOM_COL
  ) VALUES
  (ERES.SEQ_ER_CODELST.NEXTVAL,'advtype','grad_comments','Comments on Grade',2,'text_area'); 
----------------------------------------

INSERT
INTO ERES.ER_CODELST
  (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1
  ) VALUES
  (ERES.SEQ_ER_CODELST.NEXTVAL,'advtype','ae_duration','Duration',3,'dropdown',
    '<select name="alternateId"><option value="hours">Hours</option><option value="days">Days</option><option value="minutes">Minutes</option><option value="unknown">Unknown</option><option value="na">N/A</option></select>'
  );
  
  ---------------------
INSERT INTO ERES.ER_CODELST
  (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_SEQ) VALUES
    (ERES.SEQ_ER_CODELST.NEXTVAL,'advtype','ae_frequency','Frequency',4);    
 ---------------------------
INSERT INTO ERES.ER_CODELST
  (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_SEQ) VALUES
  (ERES.SEQ_ER_CODELST.NEXTVAL,'advtype','C','CTCAE Settings','chkbox','[{data:"ctcae_na", display:"CTCAE Not Applicable"}]',5);    
 
END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,299,13,'06_UPenn_Configurations.sql',sysdate,'v9.3.0 #700');

commit;
