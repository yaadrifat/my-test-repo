Make a copy of the original db_script folder in your local drive, and perform the below steps in the copied version.

1) Open upgradedb_upenn2.sh with a text editor.
2) Set proper values for all of these variables: DB_NAME ERES_PWD ESCH_PWD EPAT_PWD.
3) Open a cmd and cd into this directory.
4) Type './upgradedb_upenn2.sh' and press Enter.
5) All the scripts should get executed and DB objects will be compiled at the end.


In case of an error:

The upgradedb_upenn2 script should stop immediately when an error is encountered. The user of the upgradedb_upenn2 script will need to solve the problem by examining the error and then manually execute the rest of db scripts in that build for that schema (epat/eres/esch). Then manually create an empty file with naming convention
done_xxx.txt where xxx is the schema that you manually fixed (specifically one of these: done_eres.txt, done_esch.txt, done_epat.txt, done_xsl.txt, done_all.txt) in the db_patch folder of the build where the problem happened.

Then rename out.txt to out1.txt (or something else just to save it) and re-run upgradedb_upenn2. The upgradedb_upenn2 script will pick up from the next schema of the same build where the error occurred (or from the next schema if all three schemas were run). It looks for the files done_xxx.txt in the db_patch folder of every build to check what was already executed and skip what is flagged.

For example, if a problem happened in build123 for ERES script #2 of 3, then fix script #2 and manually run it and also manually run #3. Then place an empty done_eres.txt file as mentioned above. Next time upgradedb_upenn2 is run, it will resume from build123 ESCH, and then build123 EPAT, and then build124 ERES, etc.
