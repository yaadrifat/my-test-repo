#!/bin/bash

export DB_NAME=""
export ERES_PWD=""
export ESCH_PWD=""
export EPAT_PWD=""

none() { return 0; }
show_usage() {
  echo "===Usage:";
  echo "Open upgradedb_upenn2.sh in an editor and set these variables:";
  echo "  DB_NAME ERES_PWD ESCH_PWD EPAT_PWD ";
  return 0;
}
if [ "$DB_NAME" -a "$ERES_PWD" -a "$ESCH_PWD" -a "$EPAT_PWD" ];
then
  none;
else
  show_usage;
  exit -1;
fi

export FK_ACCOUNT=0
export PK_FORMLIB=0

# Main execution starts here
echo "===Starting DB patch script"
echo "===Starting DB patch script" 1>&2> out.txt

for buildnum in 698 699 700 700_UPenn 701 701_UPenn 702 703 703_UPenn 704 704_UPenn 705 706 706_UPenn
do
  if [ -f build$buildnum/db_patch/done_all.txt ];
  then
    continue;
  fi
  echo "===Updating to build$buildnum..."
  echo "===Updating to build$buildnum..." >> out.txt
  cd build$buildnum/db_patch
  ./rundb.sh $DB_NAME $ERES_PWD $ESCH_PWD $EPAT_PWD $FK_ACCOUNT $PK_FORMLIB 1>&2>> ../../out.txt
  cd ../..
  if [ ! -f build$buildnum/db_patch/done_all.txt ];
  then
    echo "===An error occurred. See out.txt for details. Exiting all..."
    echo "===An error occurred. Exiting all..." >> out.txt
    exit -1;
  fi
done

# Complile invalid objects
echo "===Compiling invalid objects..."
echo "===Compiling invalid objects..." >> out.txt
cd zzz_compile
sqlplus eres/$ERES_PWD@$DB_NAME @compile.sql $ERES_PWD $ESCH_PWD $EPAT_PWD 1>&2>> ../out.txt
rm run_invalid_eres.sql
rm run_invalid_esch.sql
rm run_invalid_epat.sql
cd ..
echo "===All done" >> out.txt
echo "===All done. Logs are in out.txt."
exit 0;
