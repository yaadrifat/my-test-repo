SET define OFF;

update er_report set REP_HIDE='Y' where PK_REPORT in(91,92,113,114,119);
commit;

-- INSERTING into ER_REPORT
--- Velos Invoice
DECLARE
  v_record_exists NUMBER := 0;
BEGIN
  SELECT COUNT(*) INTO v_record_exists FROM er_report WHERE pk_report = 181;
  IF (v_record_exists = 0) THEN
    INSERT
    INTO ER_REPORT
      (
        PK_REPORT,
        REP_NAME,
        REP_DESC,
        REP_SQL,
        FK_ACCOUNT,
        FK_CODELST_TYPE,
        IP_ADD,
        REP_HIDE,
        REP_COLUMNS,
        REP_FILTERBY,
        GENERATE_XML,
        REP_TYPE,
        REP_SQL_CLOB,
        REP_FILTERKEYWORD,
        REP_FILTERAPPLICABLE
      )
      VALUES
      (
        181,
        'CCSG Data Table 3',
        'CCSG Data Table 3',
        '',
        0,
        NULL,
        NULL,
        'N',
        'Disease Site, Newly Registered Patients, Total Patients Newly Enrolled in Interventional treatment trials',
        'Date:ReportingOrg:EnrollingOrg:TreatingOrg:StudyType',
        1,
        'rep_nci',
        '',
        '[DATEFILTER]D:repOrgId:enrOrgId:trtOrgId:studyType',
        'date:repOrgId:enrOrgId:trtOrgId:studyType'
      );
    COMMIT;
    
    UPDATE er_report SET rep_sql = 'SELECT disease_site,
  org_name,
  check_studyType,
  COUNT 
FROM
  ( select codelst_desc as disease_site,
    (select site_name from er_site where pk_site in (:repOrgId)) as org_name,
    ((select count(*) from ER_CODELST WHERE pk_codelst  IN (:studyType))-(SELECT COUNT(*) FROM ER_CODELST
      WHERE pk_codelst  IN (:studyType)
      AND CODELST_CUSTOM_COL=''interventional'')) as check_studyType,
  (SELECT count(*)
      FROM ER_PATPROT patprt, er_patstudystat patst,er_study stdy, er_studysites stsite
      WHERE stdy.pk_study = patprt.fk_study
      AND patprt.FK_PER = patst.fk_per
      AND stsite.fk_study=stdy.pk_study
      AND  patprt.fk_study          = patst.fk_study
      AND FK_CODELST_STAT = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp = ''enrolled'')
      AND CURRENT_STAT=1
      AND PATSTUDYSTAT_DATE BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
      AND fk_site_enrolling in (:enrOrgId)
      AND PATPROT_TREATINGORG in (:trtOrgId)
      AND PK_CODELST in (SELECT  TO_NUMBER(column_value) FROM  XMLTABLE(ANATOMIC_SITE))
      AND FK_CODELST_STUDYSITETYPE = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp = ''primary'' and CODELST_CUSTOM_COL=''lead'')
      AND FK_CODELST_TYPE in (
      select pk_codelst from ER_CODELST where pk_codelst in (:studyType) and CODELST_CUSTOM_COL=''interventional''
      )
      and FK_SITE in (:repOrgId)
      ) as count
  
  from er_codelst
  where  codelst_type = ''disease_site''
  ) '
    WHERE pk_report = 181;
    COMMIT;

UPDATE er_report SET rep_sql_clob = 'SELECT disease_site,
  org_name,
  check_studyType,
  COUNT 
FROM
  ( select codelst_desc as disease_site,
    (select site_name from er_site where pk_site in (:repOrgId)) as org_name,
    ((select count(*) from ER_CODELST WHERE pk_codelst  IN (:studyType))-(SELECT COUNT(*) FROM ER_CODELST
      WHERE pk_codelst  IN (:studyType)
      AND CODELST_CUSTOM_COL=''interventional'')) as check_studyType,
  (SELECT count(*)
      FROM ER_PATPROT patprt, er_patstudystat patst,er_study stdy, er_studysites stsite
      WHERE stdy.pk_study = patprt.fk_study
      AND patprt.FK_PER = patst.fk_per
      AND stsite.fk_study=stdy.pk_study
      AND  patprt.fk_study          = patst.fk_study
      AND FK_CODELST_STAT = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp = ''enrolled'')
      AND CURRENT_STAT=1
      AND PATSTUDYSTAT_DATE BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
      AND fk_site_enrolling in (:enrOrgId)
      AND PATPROT_TREATINGORG in (:trtOrgId)
      AND PK_CODELST in (SELECT  TO_NUMBER(column_value) FROM  XMLTABLE(ANATOMIC_SITE))
      AND FK_CODELST_STUDYSITETYPE = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp = ''primary'' and CODELST_CUSTOM_COL=''lead'')
      AND FK_CODELST_TYPE in (
      select pk_codelst from ER_CODELST where pk_codelst in (:studyType) and CODELST_CUSTOM_COL=''interventional''
      )
      and FK_SITE in (:repOrgId)
      ) as count
  
  from er_codelst
  where  codelst_type = ''disease_site''
  ) '
    WHERE pk_report = 181;
    COMMIT;
    
  END IF;
  v_record_exists := 0;
  SELECT COUNT(*) INTO v_record_exists FROM er_report WHERE pk_report = 182;
  IF (v_record_exists = 0) THEN
    INSERT
    INTO ER_REPORT
      (
        PK_REPORT,
        REP_NAME,
        REP_DESC,
        REP_SQL,
        FK_ACCOUNT,
        FK_CODELST_TYPE,
        IP_ADD,
        REP_HIDE,
        REP_COLUMNS,
        REP_FILTERBY,
        GENERATE_XML,
        REP_TYPE,
        REP_SQL_CLOB,
        REP_FILTERKEYWORD,
        REP_FILTERAPPLICABLE
      )
      VALUES
      (
        182,
        'CCSG Data Table 4',
        'CCSG Data Table 4',
        '',
        0,
        NULL,
        NULL,
        'Y',
        NULL,
        NULL,
        1,
        'rep_nci',
        '',
        NULL,
        'date'
      );
    COMMIT;
  END IF;

END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,303,6,'06_ER_REPORT_INSERT.sql',sysdate,'v9.3.0 #704');

commit;
