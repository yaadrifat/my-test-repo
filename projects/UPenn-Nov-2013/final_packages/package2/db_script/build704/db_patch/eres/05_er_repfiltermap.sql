DECLARE
filterMID number := 0;
filterFK number := 0;
begin

	select Max(PK_REPFILTERMAP)+1 into filterMID from er_repfiltermap;
	select PK_REPFILTER into filterFK from er_repfilter where REPFILTER_KEYWORD = 'studyType';

	INSERT INTO er_repfiltermap (pk_repfiltermap,fk_repfilter,repfiltermap_repcat,repfiltermap_seq) 
	VALUES (filterMID,filterFK, 'data safety monitoring',12);

	COMMIT;
	select Max(PK_REPFILTERMAP)+1 into filterMID from er_repfiltermap;
	select PK_REPFILTER into filterFK from er_repfilter where REPFILTER_KEYWORD = 'repOrgId';

	INSERT INTO er_repfiltermap (pk_repfiltermap,fk_repfilter,repfiltermap_repcat,repfiltermap_seq) 
	VALUES (filterMID,filterFK, 'data safety monitoring',13);
		
	COMMIT;
	select Max(PK_REPFILTERMAP)+1 into filterMID from er_repfiltermap;
	select PK_REPFILTER into filterFK from er_repfilter where REPFILTER_KEYWORD = 'enrOrgId';

	INSERT INTO er_repfiltermap (pk_repfiltermap,fk_repfilter,repfiltermap_repcat,repfiltermap_seq) 
	VALUES (filterMID,filterFK, 'data safety monitoring',14);
		
	COMMIT;
	select Max(PK_REPFILTERMAP)+1 into filterMID from er_repfiltermap;
	select PK_REPFILTER into filterFK from er_repfilter where REPFILTER_KEYWORD = 'trtOrgId';

	INSERT INTO er_repfiltermap (pk_repfiltermap,fk_repfilter,repfiltermap_repcat,repfiltermap_seq) 
	VALUES (filterMID,filterFK, 'data safety monitoring',15);
		
	COMMIT;
	
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,303,5,'05_er_repfiltermap.sql',sysdate,'v9.3.0 #704');

commit;


