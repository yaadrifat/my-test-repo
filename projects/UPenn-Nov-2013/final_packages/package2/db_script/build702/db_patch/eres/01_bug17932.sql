set define off;

declare
v_pkCodelst NUMBER := 0;
begin
  select pk_codelst into v_pkCodelst from er_codelst where codelst_type = 'patStatus' and codelst_subtyp = 'enrolled';
  
  if (v_pkCodelst > 0) then
    for rec in (select pk_patprot, pss.patstudystat_date as patstudystat_date, nvl(pss.creator,0) as creator from er_patprot pp, er_patstudystat pss 
    where PATPROT_ENROLDT is null and pss.fk_per = pp.fk_per and pss.fk_study = pp.fk_study and pss.fk_codelst_stat = v_pkCodelst)
    loop
      if (rec.patstudystat_date is not null) then
        update er_patprot set PATPROT_ENROLDT = rec.patstudystat_date where pk_patprot = rec.pk_patprot;
        
        if (rec.creator > 0) then
          update er_patprot set FK_USER = rec.creator where pk_patprot = rec.pk_patprot;
        end if;
        commit;
      end if;
    end loop;
  end if;
end;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,301,1,'01_bug17932.sql',sysdate,'v9.3.0 #702');

commit;

