set define off;

UPDATE ER_CODELST SET  CODELST_HIDE='Y' WHERE CODELST_TYPE = 'disease_site' AND CODELST_DESC='Gyn, GU';

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,311,7,'07_UpdateExistingCodeItems.sql',sysdate,'v9.3.0 #712');

commit;