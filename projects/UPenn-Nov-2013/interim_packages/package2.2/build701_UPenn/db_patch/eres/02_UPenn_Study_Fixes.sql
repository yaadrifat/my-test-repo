set define off;

declare
v_PK_PAGECUSTOM NUMBER := 0;
v_FK_ACCOUNT NUMBER := [fk_account];
begin
	SELECT PK_PAGECUSTOM INTO v_PK_PAGECUSTOM FROM ER_PAGECUSTOM WHERE FK_ACCOUNT = v_FK_ACCOUNT AND PAGECUSTOM_PAGE = 'studysummary';
	
	if (v_PK_PAGECUSTOM <= 0) then
		INSERT INTO ER_PAGECUSTOM (PK_PAGECUSTOM, FK_ACCOUNT, PAGECUSTOM_PAGE) 
		VALUES (SEQ_ER_PAGECUSTOM.nextval, v_FK_ACCOUNT, 'studysummary');
		
		COMMIT;
		
		SELECT PK_PAGECUSTOM INTO v_PK_PAGECUSTOM FROM ER_PAGECUSTOM WHERE FK_ACCOUNT = v_FK_ACCOUNT AND PAGECUSTOM_PAGE = 'studysummary';
	else 
		DELETE FROM ER_PAGECUSTOMFLDS WHERE FK_PAGECUSTOM = v_PK_PAGECUSTOM AND PAGECUSTOMFLDS_FIELD = 'pinvestigator';
	end if;
	
	INSERT INTO ER_PAGECUSTOMFLDS (PK_PAGECUSTOMFLDS, FK_PAGECUSTOM, PAGECUSTOMFLDS_FIELD, PAGECUSTOMFLDS_ATTRIBUTE, PAGECUSTOMFLDS_MANDATORY, PAGECUSTOMFLDS_LABEL) 
	VALUES (SEQ_ER_PAGECUSTOMFLDS.nextval, v_PK_PAGECUSTOM, 'pinvestigator', 1, , '');

	COMMIT;
end;
/

UPDATE ER_CODELST SET 
CODELST_CUSTOM_COL1 = '<SELECT NAME="alternateId"><OPTION value = "">No Option Selected</OPTION><OPTION value = "Interventional">Interventional</OPTION><OPTION value = "Observational">Observational</OPTION><OPTION value = "Ancillary">Ancillary</OPTION><OPTION value = "Correlative">Correlative</OPTION><OPTION value = "Retrospective">Retrospective Analysis </OPTION><OPTION value = "Biospecimen">Biospecimen </OPTION></SELECT>'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='PR_RESEARCH_CAT' AND CODELST_CUSTOM_COL='dropdown';

COMMIT;


--Missing disease site 'No Cancer'
update er_codelst set codelst_subtyp = 'diseaseSite_NC' where codelst_type ='disease_site' and codelst_desc = 'No Cancer';

commit;

declare
v_CodeExists NUMBER := 0;
begin
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'disease_site' AND CODELST_SUBTYP = 'diseaseSite_NC';
	
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'disease_site', 'diseaseSite_NC', 'No Cancer');
	END IF;
end;
/

-- Remove all references to new disease site 'Breast'  
declare
v_pkcode NUMBER := 0;
v_disSite VARCHAR2(200) := '';
begin
	select max(pk_codelst)  into v_pkcode from er_codelst where codelst_type = 'disease_site' and codelst_subtyp ='breast';

	if (v_pkcode > 0) then 
		update er_study set study_disease_site = null where study_disease_site = '' || v_pkcode;
		
		commit;

		for rec in (select pk_study, study_disease_site from er_study where ',' || study_disease_site || ',' like '%,' || v_pkcode || ',%')
		loop
			v_disSite := replace(',' || rec.study_disease_site|| ',', ',' || v_pkcode || ',', ',');
			v_disSite := trim(BOTH ',' from v_disSite);
            
			update er_study set study_disease_site = v_disSite where pk_study = rec.pk_study;
		end loop;
	
		commit;
		
		delete from er_codelst where pk_codelst = v_pkcode;
	
		commit;
	end if;
end;
/


-- Remove all references to new disease site 'GI'
declare
v_pkcode NUMBER := 0;
v_disSite VARCHAR2(200) := '';
begin
	select max(pk_codelst) into v_pkcode from er_codelst where codelst_type = 'disease_site' and codelst_subtyp ='diseaseSite_GI';

	if (v_pkcode > 0) then 
		update er_study set study_disease_site = null where study_disease_site = '' || v_pkcode;
		
		commit;
	
		for rec in (select pk_study, study_disease_site from er_study where ',' || study_disease_site || ',' like '%,' || v_pkcode || ',%')
		loop	
			v_disSite := replace(',' || rec.study_disease_site|| ',', ',' || v_pkcode || ',', ',');
			v_disSite := trim(BOTH ',' from v_disSite);
			
			update er_study set study_disease_site = v_disSite where pk_study = rec.pk_study;
		end loop;
			
		commit;
		
		delete from er_codelst where pk_codelst = v_pkcode;
	
		commit;
	end if;
	
end;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,300,14,'02_UPenn_Study_Fixes.sql',sysdate,'v9.3.0 #701');

commit;