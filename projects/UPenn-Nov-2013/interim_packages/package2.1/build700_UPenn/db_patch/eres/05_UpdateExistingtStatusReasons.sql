set define off;
	
Update er_codelst set codelst_subtyp='automated' where codelst_type = 'enroll_appr' and codelst_desc = 'Automated';
Update er_codelst set codelst_subtyp='automated' where codelst_type = 'enroll_appr' and codelst_subtyp = 'enroll_apr1';

Update er_codelst set codelst_subtyp='eligible' where codelst_type = 'enroll_appr' and codelst_desc = 'Eligible';
Update er_codelst set codelst_subtyp='eligible' where codelst_type = 'enroll_appr' and codelst_subtyp = 'enroll_apr2';

Update er_codelst set codelst_subtyp='ineligible' where codelst_type = 'enroll_appr' and codelst_desc = 'Ineligible';
Update er_codelst set codelst_subtyp='ineligible' where codelst_type = 'enroll_appr' and codelst_subtyp = 'enroll_apr3';

Update er_codelst set codelst_subtyp='eligNotMet' where codelst_type = 'enroll_denied' and codelst_desc = 'Eligibility not met';
Update er_codelst set codelst_subtyp='eligNotMet' where codelst_type = 'enroll_denied' and codelst_subtyp = 'enroll_denied1';

Update er_codelst set codelst_subtyp='other' where codelst_type = 'enroll_denied' and codelst_desc = 'Other';
Update er_codelst set codelst_subtyp='other' where codelst_type = 'enroll_denied' and codelst_subtyp = 'enroll_denied2';

Update er_codelst set codelst_subtyp='automated' where codelst_type = 'enroll_pending' and codelst_desc = 'Automated';
Update er_codelst set codelst_subtyp='automated' where codelst_type = 'enroll_pending' and codelst_subtyp = 'enroll_pending1';

Update er_codelst set codelst_subtyp='disProgression' where codelst_type = 'followup' and codelst_desc = 'Disease Progression';
Update er_codelst set codelst_subtyp='disProgression' where codelst_type = 'followup' and codelst_subtyp = 'follow6221';

Update er_codelst set codelst_subtyp='survivalOnly' where codelst_type = 'followup' and codelst_desc = 'Survival Only';
Update er_codelst set codelst_subtyp='survivalOnly' where codelst_type = 'followup' and codelst_subtyp = 'follow6222';

Update er_codelst set codelst_subtyp='adverseEvent' where codelst_type = 'offstudy' and codelst_desc = 'Adverse Event';
Update er_codelst set codelst_subtyp='adverseEvent' where codelst_type = 'offstudy' and codelst_subtyp = 'offst6206';

Update er_codelst set codelst_subtyp='alternatTherapy' where codelst_type = 'offstudy' and codelst_desc = 'Alternative Therapy';
Update er_codelst set codelst_subtyp='alternatTherapy' where codelst_type = 'offstudy' and codelst_subtyp = 'offst7148';

Update er_codelst set codelst_subtyp='compResponse' where codelst_type = 'offstudy' and codelst_desc = 'Complete Response Per Protocol';
Update er_codelst set codelst_subtyp='compResponse' where codelst_type = 'offstudy' and codelst_subtyp = 'offst7152';

Update er_codelst set codelst_subtyp='wDrawAfterTreat' where codelst_type = 'offstudy' and codelst_desc = 'Consent Withdrawn After Treatment Started';
Update er_codelst set codelst_subtyp='wDrawAfterTreat' where codelst_type = 'offstudy' and codelst_subtyp = 'offst6210';

Update er_codelst set codelst_subtyp='wDrawBefTreat' where codelst_type = 'offstudy' and codelst_desc = 'Consent Withdrawn Before Treatment Started';
Update er_codelst set codelst_subtyp='wDrawBefTreat' where codelst_type = 'offstudy' and codelst_subtyp = 'offst7146';

Update er_codelst set codelst_subtyp='disProgAftTreat' where codelst_type = 'offstudy' and codelst_desc = 'Disease Progression After Treatment Started';
Update er_codelst set codelst_subtyp='disProgAftTreat' where codelst_type = 'offstudy' and codelst_subtyp = 'offst6205';

Update er_codelst set codelst_subtyp='disProgBefTreat' where codelst_type = 'offstudy' and codelst_desc = 'Disease Progression Before Treatment Started';
Update er_codelst set codelst_subtyp='disProgBefTreat' where codelst_type = 'offstudy' and codelst_subtyp = 'offst7144';

Update er_codelst set codelst_subtyp='lostToFollowUp' where codelst_type = 'offstudy' and codelst_desc = 'Lost to F/U';
Update er_codelst set codelst_subtyp='lostToFollowUp' where codelst_type = 'offstudy' and codelst_subtyp = 'offst6211';

Update er_codelst set codelst_subtyp='noncompliance' where codelst_type = 'offstudy' and codelst_desc = 'Noncompliance';
Update er_codelst set codelst_subtyp='noncompliance' where codelst_type = 'offstudy' and codelst_subtyp = 'offst6209';

Update er_codelst set codelst_subtyp='other' where codelst_type = 'offstudy' and codelst_desc = 'Other';
Update er_codelst set codelst_subtyp='other' where codelst_type = 'offstudy' and codelst_subtyp = 'offst7154';

Update er_codelst set codelst_subtyp='othComplicCond' where codelst_type = 'offstudy' and codelst_desc = 'Other Complicating Condition';
Update er_codelst set codelst_subtyp='othComplicCond' where codelst_type = 'offstudy' and codelst_subtyp = 'offst7150';

Update er_codelst set codelst_subtyp='patientDied' where codelst_type = 'offstudy' and codelst_desc = 'Patient Died';
Update er_codelst set codelst_subtyp='patientDied' where codelst_type = 'offstudy' and codelst_subtyp = 'offst6212';

Update er_codelst set codelst_subtyp='physDiscretion' where codelst_type = 'offstudy' and codelst_desc = 'Physician Discretion';
Update er_codelst set codelst_subtyp='physDiscretion' where codelst_type = 'offstudy' and codelst_subtyp = 'offst7156';

Update er_codelst set codelst_subtyp='sponsorTerminte' where codelst_type = 'offstudy' and codelst_desc = 'Sponsor Termination';
Update er_codelst set codelst_subtyp='sponsorTerminte' where codelst_type = 'offstudy' and codelst_subtyp = 'offst6207';

Update er_codelst set codelst_subtyp='studyComplete' where codelst_type = 'offstudy' and codelst_desc = 'Study Complete';
Update er_codelst set codelst_subtyp='studyComplete' where codelst_type = 'offstudy' and codelst_subtyp = 'offst6208';

Update er_codelst set codelst_subtyp='adverseEvent' where codelst_type = 'offtreat' and codelst_desc = 'Adverse Event';
Update er_codelst set codelst_subtyp='adverseEvent' where codelst_type = 'offtreat' and codelst_subtyp = 'off6214';

Update er_codelst set codelst_subtyp='alternatTherapy' where codelst_type = 'offtreat' and codelst_desc = 'Alternative Therapy';
Update er_codelst set codelst_subtyp='alternatTherapy' where codelst_type = 'offtreat' and codelst_subtyp = 'off7147';

Update er_codelst set codelst_subtyp='compResponse' where codelst_type = 'offtreat' and codelst_desc = 'Complete Response Per Protocol';
Update er_codelst set codelst_subtyp='compResponse' where codelst_type = 'offtreat' and codelst_subtyp = 'off7151';

Update er_codelst set codelst_subtyp='wDrawAfterTreat' where codelst_type = 'offtreat' and codelst_desc = 'Consent Withdrawn After Treatment Started';
Update er_codelst set codelst_subtyp='wDrawAfterTreat' where codelst_type = 'offtreat' and codelst_subtyp = 'off6218';

Update er_codelst set codelst_subtyp='wDrawBefTreat' where codelst_type = 'offtreat' and codelst_desc = 'Consent Withdrawn Before Treatment Started';
Update er_codelst set codelst_subtyp='wDrawBefTreat' where codelst_type = 'offtreat' and codelst_subtyp = 'off7145';

Update er_codelst set codelst_subtyp='disProgAftTreat' where codelst_type = 'offtreat' and codelst_desc = 'Disease Progression After Treatment Started';
Update er_codelst set codelst_subtyp='disProgAftTreat' where codelst_type = 'offtreat' and codelst_subtyp = 'off6213';

Update er_codelst set codelst_subtyp='disProgBefTreat' where codelst_type = 'offtreat' and codelst_desc = 'Disease Progression Before Treatment Started';
Update er_codelst set codelst_subtyp='disProgBefTreat' where codelst_type = 'offtreat' and codelst_subtyp = 'off7143';

Update er_codelst set codelst_subtyp='lostToFollowUp' where codelst_type = 'offtreat' and codelst_desc = 'Lost to F/U';
Update er_codelst set codelst_subtyp='lostToFollowUp' where codelst_type = 'offtreat' and codelst_subtyp = 'off6219';

Update er_codelst set codelst_subtyp='noncompliance' where codelst_type = 'offtreat' and codelst_desc = 'Noncompliance';
Update er_codelst set codelst_subtyp='noncompliance' where codelst_type = 'offtreat' and codelst_subtyp = 'off6217';

Update er_codelst set codelst_subtyp='other' where codelst_type = 'offtreat' and codelst_desc = 'Other';
Update er_codelst set codelst_subtyp='other' where codelst_type = 'offtreat' and codelst_subtyp = 'off7153';

Update er_codelst set codelst_subtyp='othComplicCond' where codelst_type = 'offtreat' and codelst_desc = 'Other Complicating Condition';
Update er_codelst set codelst_subtyp='othComplicCond' where codelst_type = 'offtreat' and codelst_subtyp = 'off7149';

Update er_codelst set codelst_subtyp='patientDied' where codelst_type = 'offtreat' and codelst_desc = 'Patient Died';
Update er_codelst set codelst_subtyp='patientDied' where codelst_type = 'offtreat' and codelst_subtyp = 'off6220';

Update er_codelst set codelst_subtyp='physDiscretion' where codelst_type = 'offtreat' and codelst_desc = 'Physician Discretion';
Update er_codelst set codelst_subtyp='physDiscretion' where codelst_type = 'offtreat' and codelst_subtyp = 'off7155';

Update er_codelst set codelst_subtyp='sponsorTerminte' where codelst_type = 'offtreat' and codelst_desc = 'Sponsor Termination';
Update er_codelst set codelst_subtyp='sponsorTerminte' where codelst_type = 'offtreat' and codelst_subtyp = 'off6215';

Update er_codelst set codelst_subtyp='treatmentComp' where codelst_type = 'offtreat' and codelst_desc = 'Treatment Complete';
Update er_codelst set codelst_subtyp='treatmentComp' where codelst_type = 'offtreat' and codelst_subtyp = 'off6216';

commit;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,299,12,'05_UpdateExistingtStatusReasons.sql',sysdate,'v9.3.0 #700');

commit;