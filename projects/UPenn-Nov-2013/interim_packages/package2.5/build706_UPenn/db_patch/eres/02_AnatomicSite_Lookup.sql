set define off;

declare
v_MAXPK NUMBER := 0;
v_pk_LKPLIB NUMBER := 0;
v_pk_LKPVIEW NUMBER := 0;
v_pk_LKPCOL NUMBER := 0;
v_pk_LKPVIEWCOL NUMBER := 0;
begin
	SELECT COUNT(PK_LKPLIB) INTO v_pk_LKPLIB FROM ER_LKPLIB WHERE LKPTYPE_NAME = 'Custom Anatomic Site' AND LKPTYPE_TYPE = 'custAnatomicSite';
	IF (v_pk_LKPLIB = 0) THEN
		SELECT MAX(PK_LKPLIB) INTO v_MAXPK FROM ER_LKPLIB;

		v_pk_LKPLIB := SEQ_ER_LKPLIB.NEXTVAL();
		WHILE (v_pk_LKPLIB <= v_MAXPK)
		LOOP
			v_pk_LKPLIB := SEQ_ER_LKPLIB.NEXTVAL();
		END LOOP;
		
		INSERT INTO ER_LKPLIB VALUES(v_pk_LKPLIB, 'Custom Anatomic Site', '1.0', 'Custom Anatomic Site lookup introduced by Velos', 'custAnatomicSite', null, null);
	ELSE
		SELECT PK_LKPLIB INTO v_pk_LKPLIB FROM ER_LKPLIB WHERE LKPTYPE_NAME = 'Custom Anatomic Site' AND LKPTYPE_TYPE = 'custAnatomicSite';
	END IF;
	
	SELECT COUNT(PK_LKPVIEW) INTO v_pk_LKPVIEW FROM ER_LKPVIEW WHERE FK_LKPLIB = v_pk_LKPLIB;
	IF (v_pk_LKPVIEW = 0) THEN
		SELECT MAX(PK_LKPVIEW)+1 INTO v_pk_LKPVIEW FROM ER_LKPVIEW;
		
		INSERT INTO ER_LKPVIEW (PK_LKPVIEW, LKPVIEW_NAME, LKPVIEW_DESC, FK_LKPLIB)
		VALUES(v_pk_LKPVIEW, 'Custom Anatomic Site', 'Custom Anatomic Site lookup introduced by Velos', v_pk_LKPLIB);
	ELSE
		SELECT PK_LKPVIEW INTO v_pk_LKPVIEW FROM ER_LKPVIEW WHERE FK_LKPLIB = v_pk_LKPLIB;
	END IF;
	
	SELECT COUNT(PK_LKPCOL) INTO v_pk_LKPCOL FROM ER_LKPCOL WHERE FK_LKPLIB = v_pk_LKPLIB;
	IF (v_pk_LKPCOL = 0) THEN
		SELECT MAX(PK_LKPCOL) INTO v_MAXPK FROM ER_LKPCOL;

		v_pk_LKPCOL := SEQ_ER_LKPCOL.NEXTVAL();
		WHILE (v_pk_LKPCOL <= v_MAXPK)
		LOOP
			v_pk_LKPCOL := SEQ_ER_LKPCOL.NEXTVAL();
		END LOOP;

		INSERT INTO ER_LKPCOL (PK_LKPCOL, FK_LKPLIB, LKPCOL_NAME, LKPCOL_DISPVAL, LKPCOL_DATATYPE, LKPCOL_LEN, LKPCOL_TABLE, LKPCOL_KEYWORD) 
		VALUES (v_pk_LKPCOL, v_pk_LKPLIB, 'custom001', 'Anatomic Site Name', 'varchar2', '100', 'ER_LKPDATA', 'anatomicSiteName');
		
		SELECT MAX(PK_LKPVIEWCOL)+1 INTO v_pk_LKPVIEWCOL FROM ER_LKPVIEWCOL;
		
		INSERT INTO ER_LKPVIEWCOL (PK_LKPVIEWCOL, FK_LKPCOL, LKPVIEW_IS_SEARCH, LKPVIEW_SEQ, FK_LKPVIEW, LKPVIEW_DISPLEN, LKPVIEW_IS_DISPLAY) 
		VALUES (v_pk_LKPVIEWCOL, v_pk_LKPCOL, 'Y', 1, v_pk_LKPVIEW, '10%', 'Y');
	END IF;
	
	IF (v_pk_LKPLIB > 0) THEN
		DELETE FROM ER_LKPDATA WHERE FK_LKPLIB = v_pk_LKPLIB;
		
		SELECT MAX(PK_LKPDATA) INTO v_MAXPK FROM ER_LKPDATA;

		v_pk_LKPCOL := SEQ_ER_LKPDATA.NEXTVAL();
		WHILE (v_pk_LKPCOL <= v_MAXPK)
		LOOP
			v_pk_LKPCOL := SEQ_ER_LKPDATA.NEXTVAL();
		END LOOP;

		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (v_pk_LKPCOL, v_pk_LKPLIB,'Anus');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Adenoid Cystic');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Adenoid Cystic - Salivary Glands');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Adenoid Cystic - Breast');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Adenoid Cystic - Lacrimal Gland');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Adenoid Cystic - Eye');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Adenoid Cystic - Lung');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Adenoid Cystic - Bartholin Gland');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Adenoid Cystic - Trachea');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Adenoid Cystic - Paranasal Sinuses');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Bladder');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Bladder - Germ Cell');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Bladder - Transitional Cell');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Bladder - Adencarcinoma');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Bladder - Squamous Cell');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Bones and Joints');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Bones and Joints - Osteosarcoma');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Bones and Joints - Chondrosarcoma');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Bones and Joints - Ewings');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Bones and Joints - Malignant Fibrous Histiosarcoma');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Bones and Joints - Giant Cell');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Bones and Joints - Adamantiomas');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Bones and Joints - Chordoma');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Brain and Nervous System');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Brain and Nervous System - Glioblastoma Multiforme');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Brain and Nervous System - Glioma');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Brain and Nervous System - Glioma - Astrocytoma');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Brain and Nervous System - Glioma - Oligodendrogliomas');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Brain and Nervous System - Glioma - Ependymomas');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Brain and Nervous System - Glioma - Ghoroid Plexus Papilloma');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Brain and Nervous System - Meningioma');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Brain and Nervous System - Pituitary Adenoma');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Brain and Nervous System - Vestibular Schwannoma');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Brain and Nervous System - Medulloblastoma');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Brain and Nervous System - Craniopharyngiomas');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Brain and Nervous System - Germ Cell');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Brain and Nervous System - Pineal Region');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Breast-Female');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Breast-Female - DCIS');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Breast-Female - Invasive Ductal');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Breast-Female - Invasive Ductal - Tubulur');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Breast-Female - Invasive Ductal - Municous');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Breast-Female - Invasive Ductal - Medullary');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Breast-Female - Invasive Ductal - Papillary');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Breast-Female - Invasive Lobular');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Breast-Female - Inflammatory');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Breast-Female - Philodes Tumor');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Breast-Female - Angiosarcoma');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Breast-Female - Osteosarcoma');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Breast-Female - Metaplastic');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Breast-Female - Adenoid Cystic');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Breast-Female - Pagets');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Breast-Male');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Cervix Uteri');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Cervix Uteri - Squamous');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Cervix Uteri - Adenocarcinoma');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Cervix Uteri - Adenosquamous');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Colon (Bowell)');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Corpus Uteri');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Corpus Uteri - Adenocarcinoma');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Corpus Uteri - Sarcoma');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Esophagus');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Eye and Orbit');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Eye and Orbit - Chorodial');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Eye and Orbit - Eyelid');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Eye and Orbit - Iris');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Eye and Orbit - Opitic Nerve');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Eye and Orbit - Retinal');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Eye and Orbit - Conjunctival');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Eye and Orbit - Infiltrative Intraocular');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Eye and Orbit - Metastatic Ocular');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Eye and Orbit - Orbital');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Hodgkins Lymphoma');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Hodgkins Lymphoma - Classic');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Hodgkins Lymphoma - Nodular Lymphocyte-predominant');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Ill-defined Sites');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Kaposis Sarcoma');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Kidney');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Kidney - Clear Cell');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Kidney - Papillary');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Kidney - Chromophobe');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Kidney - Collecting Duct');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Kidney - Transitional Cell');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Kidney - Wilms');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Kidney - Renal Sarcoma');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Kidney - Unclassified');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Larynx');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Leukemia, not otherwise specified');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Leukemia, Other');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Lip, Oral Cavity, Pharynx');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Lip, Oral Cavity, PharynxMetastatic Squamous; Occult Primary');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Lip, Oral Cavity, Pharynx - Lip');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Lip, Oral Cavity, Pharynx - Oral Cavity');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Lip, Oral Cavity, Pharynx - Oral Cavity - Tongue');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Lip, Oral Cavity, Pharynx - Oral Cavity - Gums');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Lip, Oral Cavity, Pharynx - Oral Cavity - Cheeks');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Lip, Oral Cavity, Pharynx - Oral Cavity - Mouth Floor');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Lip, Oral Cavity, Pharynx - Oral Cavity - Hard Palate');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Lip, Oral Cavity, Pharynx - Pharynx');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Lip, Oral Cavity, Pharynx - Pharynx- Nasopharynx');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Lip, Oral Cavity, Pharynx - Pharynx- Oropharynx');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Lip, Oral Cavity, Pharynx - Pharynx- Hyporpharynx');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Lip, Oral Cavity, Pharynx - Salivary Gland');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Lip, Oral Cavity, Pharynx - Paranasal Sinus');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Lip, Oral Cavity, Pharynx - Nasal Cavity');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Lip, Oral Cavity, Pharynx - Upper Neck Lymph Nodes');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Liver');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Lung');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Lung - Non-Small Cell');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Lung - Non-Small Cell - Squamous');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Lung - Non-Small Cell - Adenocarinoma');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Lung - Non-Small Cell - Brochioalveolar');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Lung - Non-Small Cell - Large-cell undifferentiated');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Lung - Small Cell');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Lung - Bronchiocarcinoid');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Lymphoid Leukemia');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Lymphoid Leukemia - Accute Lymphocytic');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Lymphoid Leukemia - Chronic Lymphocytic');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Melanoma, Skin');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Melanoma, Skin - Superficial Spreading');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Melanoma, Skin - Nodular');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Melanoma, Skin - Lentigo Maligna');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Melanoma, Skin - Acral Lentinginous');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Multiple Myeloma');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Multiple Sites');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Mycosis Fungoides');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Myeloid and Monocytic Leukemia');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Myeloid and Monocytic Leukemia - Acute Myelogenous');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Myeloid and Monocytic Leukemia - Chronic Myelogenous');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Myeloid and Monocytic Leukemia - Myelodysplastic Syndromes');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Myeloid and Monocytic Leukemia - Polycythemia Vera');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Non-Hodgkins Lymphoma');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Non-Hodgkins Lymphoma - B-Cell');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Non-Hodgkins Lymphoma - B-Cell - Burkitt');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Non-Hodgkins Lymphoma - B-Cell - Diffuse Large Cell');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Non-Hodgkins Lymphoma - B-Cell - Follicular');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Non-Hodgkins Lymphoma - B-Cell - Immunoblastic Large Cell');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Non-Hodgkins Lymphoma - B-Cell - Precursor B-Lymphoblastic');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Non-Hodgkins Lymphoma - B-Cell - Mantel');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Non-Hodgkins Lymphoma - B-Cell - Lymphoproliferative Disorders');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Non-Hodgkins Lymphoma - T-Cell');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Non-Hodgkins Lymphoma - T-Cell - Anaplastic Large');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Non-Hodgkins Lymphoma - T-Cell - Precusor T-Lymphoblastic');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'NUT Midline Carcinomas - Head');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'NUT Midline Carcinomas - Neck');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'NUT Midline Carcinomas - Mediastinum');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Other Digestive Organ');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Other Endocrine');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Other Endocrine - Adrenocortical');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Other Endocrine - Pituitary');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Other Endocrine - Parathyroid');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Other Endocrine - Thymus');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Other Endocrine - Multiple Endocrine Neoplasia');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Other Female Genital');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Other Female Genital - Fallopian');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Other Female Genital - Vulvar');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Other Female Genital - Peritoneal');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Other Hematopoietic');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Other Hematopoietic - Plasmacytoma');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Other Hematopoietic - Macroglobulinemia');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Other Hematopoietic - Monoclonal Gmmopathy Unspecified');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Other Male Genital');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Other Male Genital - Testicular');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Other Male Genital - Scrotum');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Other Male Genital - Penile');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Other Respiratory and Intrathoracic Organs');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Other Respiratory and Intrathoracic Organs - Mesothelioma');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Other Respiratory and Intrathoracic Organs - Fimoma');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Other Respiratory and Intrathoracic Organs - Mediastinal');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Other Respiratory and Intrathoracic Organs - Mediastinal- Germ Cell');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Other Respiratory and Intrathoracic Organs - Mediastinal- Primary');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Other Respiratory and Intrathoracic Organs - Mediastinal- Thymomas');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Other Respiratory and Intrathoracic Organs - Tracheal');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Other Respiratory and Intrathoracic Organs - Non-Mesothelioma Pleural');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Other Skin');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Other Skin - Basal Cell');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Other Skin - Squamous Cell');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Other Skin - Neuroendocrine Cell');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Other Urinary');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Other Urinary - Renal Pelvis');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Other Urinary - Ureter');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Other Urinary - Urethral');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Other Urinary - Wilms');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Ovary');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Ovary - Epithelial');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Ovary - Germ Cell');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Ovary - Papillary Serous');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Ovary - Adenocarcinoma');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Ovary - Clear Cell');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Ovary - Endometrioid');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Ovary - Sex - Cord Stromal');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Ovary - Mucinous Cystadenocarcinoma');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Ovary - Mucinous Adenocarcinoma');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Ovary - Mullerian Tumor');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Ovary - Brenner Tumor');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Ovary - Teratoma');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Ovary - Cystadenocarcinoma');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Pancreas');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Peritoneum');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Prostate');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Prostate - Adenocarcinoma');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Prostate - Prostatic Intraepithelial Neoplasia');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Rectum');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Small Intestine');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Soft Tissue');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Soft Tissue - Fibrosarcoma');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Soft Tissue - Liposarcoma');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Soft Tissue - Leiomyosarcoma');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Soft Tissue - Rhabdomyosarcoma');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Soft Tissue - Epithelioid Hemangioendothelioma');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Soft Tissue - Angiosarcoma');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Soft Tissue - Lymphangiosarcoma');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Soft Tissue - Glomangiosarcoma');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Soft Tissue - Hemangiopericytoma');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Soft Tissue - Synovial');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Soft Tissue - Malignant Granular Cell');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Soft Tissue - Schwannoma');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Soft Tissue - Neurofibrosarcoma');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Soft Tissue - GIST');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Soft Tissue - Mesenchymoma');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Stomach');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Stomach - Adenocarcinoma');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Stomach - Carcinoid');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Thyroid');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Thyroid - Papillary');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Thyroid - Medullary');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Thyroid - Follicular');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Thyroid - Anaplastic');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Thyroid - Poorly Differentiated');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Thyroid - Squamous Cell');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'Unknown');
		INSERT INTO ER_LKPDATA (PK_LKPDATA, FK_LKPLIB, CUSTOM001) values (SEQ_ER_LKPDATA.NEXTVAL, v_pk_LKPLIB,'No Cancer');

	END IF;
	commit;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,305,6,'02_AnatomicSite_Lookup.sql',sysdate,'v9.3.0 #706');

commit;