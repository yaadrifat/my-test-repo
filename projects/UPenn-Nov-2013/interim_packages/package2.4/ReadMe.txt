/////*********This readMe is specific to package #2.4 based on v9.3.0 build #704**********////
-----------------------------------------------------------------------------------------------------
1. Please copy the resource bundle files in eResearch_jboss510\conf folder.

-----------------------------------------------------------------------------------------------------

2. If you are taking build without deleting files under velos.ear folder then Please follow the instructions mentioned below.

a. Please delete all files under -
\eResearch_jboss510\server\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\eres\gems
The files under mentioned folder have been renamed to have correct case. Hence files have been renamed as Workflow*.class instead of WorkFlow*.class.

2. Please delete file workFlow.jsp under -
\eResearch_jboss510\server\eresearch\deploy\velos.ear\velos.war\jsp