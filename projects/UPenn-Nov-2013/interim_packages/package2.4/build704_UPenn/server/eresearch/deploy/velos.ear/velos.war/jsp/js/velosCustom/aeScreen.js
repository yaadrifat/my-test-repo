var aeScreen = {
	validate: {},
	validationOverride:{},
	overrideValidationFlag:{},
	moveElements: {},
	naCheckboxFn: {},
	repTypeAccessRt: {},
	defaultSelected: {},
	gradeOvrRideFn:{},
	aeNameMandate:{}
};


aeScreen.aeNameMandate = function(){
	
				$j('#advNameLabel').append('   <FONT class="Mandatory">* </FONT>');

}
aeScreen.validate = function(){
	
	
if($j("#dictValSel").length>0){
		if($j("#dictValSel").val()==" "){
			
		}else {
			$j("input[data-subtype='gradeOvrRideF']").val("N");
		}
}
	
if(!(document.getElementById('pagecustomadvname'))){
	if(!$j('[type=checkbox][data-subtype="ctcae_na"]').is(':checked')&& $j('#studyAdvlkpVerNumber').val() !=2){
				
			if($j('#advName').val()==''||$j('#advName').val()==null){
				alert('Please Select a value for "Adverse Event Name" from Dictionay');
				$j('#advName').focus();
				return false;
			}else{
				return true;
			}		
	}else{
		return true;
	}
}	
};

aeScreen.overrideValidationFlag = false;

// This skips validations, make sure to include other validate functions, as application might not work or fail
// without required fields
aeScreen.validationOverride = function(){
	return true;
};

aeScreen.moveElements = function(){
	
}

aeScreen.defaultSelected = function(){
	
	var mode = $j("#mode").val();
	
	if(mode=='N'){
		$j("select[name='adve_type']").find("option[data-subtype='al_adve']").attr("selected","selected");
	}
}


aeScreen.repTypeAccessRt = function(){
	var mode = $j("#mode").val();
	if(!aeScreenFunctions.aeScrnIsAdmin && mode!='N'){
		var savedValue = $j('input[name="mdSubtype"][value="rep_typ"]').parent().find('select option:selected').val();
		$j('input[name="mdSubtype"][value="rep_typ"]').parent().find('select option[value!="'+savedValue+'"]').remove();
	}
	
}


// Implements the CTCAE NA check-box functionality
aeScreen.naCheckboxFn = function(){
	
	
	/*
	 Modify here the to change the drop down, value should always be a number,
	 and it would be saved to Grade column in the database*/
	var gradeDD = '<select name="gradeDD" style="display:none;">';
		gradeDD = gradeDD+'<option value="">Select an Option</option>';	
		gradeDD = gradeDD+'<option value="1">Mild</option>';
		gradeDD = gradeDD+'<option value="2">Moderate</option>';
		gradeDD = gradeDD+'<option value="3">Severe</option>';
		gradeDD = gradeDD+'<option value="4">Life Threatening</option>';
		gradeDD = gradeDD+'<option value="5">Death</option>';
		gradeDD = gradeDD+'</select>';
	
	
		
	
	// adding the 'grade' drop-down to the page
	$j('input[name="grade"]').parent().append(gradeDD);
	
	//adding listener to gradeDD
	$j('select[name="gradeDD"]').live('change',function(){
		$j('input[name="grade"]').val($j('select[name="gradeDD"]').val());
		$j("#dictValSel").val("");
	});
		

		
	// Hiding elements if the NA check-box is selected
	$j('[type=checkbox][data-subtype="ctcae_na"]').each(function(){
	    if($j(this).is(':checked')){
	    	$j('input[name="MedDRAcode"]').val("").parents("tr").hide();
	    	$j('input[name="aeGradeDesc"]').val("").parents("tr").hide();
	    	$j('input[name="aeToxicityDesc"]').val("").parents("tr").hide();
	    	$j('input[name="aeToxicity"]').val("").parents("tr").hide();
	    	$j('input[name="advDictionary"]').val("").parents("tr").hide();
	    	$j('input[name="advName"]').val("").parents("tr").hide();
	    	$j('input[name="aeCategory"]').val("Not Applicable").attr("readonly","readonly");
	    	$j('input[name="grade"]').hide();
	    	$j('select[name="gradeDD"]').show();
	    	$j('#nciDictRow').hide();
	    	
	    	$j("#dictValSel").val("");
	    	$j('input[value="gradeOvrRideL"][name="mdSubtype"]').parents("tr").hide();
    	
	    	var gradeValue = $j('input[name="grade"]').val();
	    	$j('select[name="gradeDD"]').find('option[value="'+gradeValue+'"]').attr("selected", "selected");
	    	 
	    } 
	});
	
	// adding listener to check-box
	$j('[type=checkbox][data-subtype="ctcae_na"]').live('change', function(){
	    if($j(this).is(':checked')){
	    	//Hide
	    	$j('input[name="MedDRAcode"]').val("").parents("tr").hide();
	    	$j('input[name="aeGradeDesc"]').val("").parents("tr").hide();
	    	$j('input[name="aeToxicityDesc"]').val("").parents("tr").hide();
	    	$j('input[name="aeToxicity"]').val("").parents("tr").hide();
	    	$j('input[name="advDictionary"]').val("").parents("tr").hide();
	    	$j('input[name="advName"]').val("").parents("tr").hide();
	    	$j('input[name="aeCategory"]').val("Not Applicable").attr("readonly","readonly");
	    	$j('input[name="grade"]').val("");
			$j('input[name="grade"]').hide();
	    	$j('select[name="gradeDD"]').show();
	    	$j('#nciDictRow').hide();
	    	
	    	$j("#dictValSel").val("");	    	
	    	
	    } else {
	    	$j('input[name="MedDRAcode"]').parents("tr").show();
	    	$j('input[name="aeGradeDesc"]').parents("tr").show();
	    	$j('input[name="aeToxicityDesc"]').parents("tr").show();
	    	$j('input[name="aeToxicity"]').parents("tr").show();
	    	$j('input[name="advDictionary"]').parents("tr").show();
	    	$j('input[name="advName"]').val("").parents("tr").show();
	    	$j('input[name="aeCategory"]').val("").parents("tr").show();
	    	
	    	$j('select[name="gradeDD"]').val("").hide();
	    	$j('input[name="grade"]').val("").show();
	    	$j('#nciDictRow').show();
	    	
	    	
	    }
	});

};


aeScreen.populateAELoggedDate = function(){
	var mode = $j("#mode").val();
	if(mode=='N'){
		$j("input[name='aeloggedDt']").datepicker();
		$j("input[name='aeloggedDt']").datepicker("setDate", new Date());
	}
};

aeScreen.gradeOvrRideFn = function(){
	
	if($j("input[data-subtype='gradeOvrRideF']").val()=='Y'){
						
	}else{
		$j('input[value="gradeOvrRideL"][name="mdSubtype"]').parents("tr").hide();
	}
	
	
};


$j(document).ready( function() {
	
	// move elements from More details section to main section
	//aeScreen.moveElements();
		
	aeScreen.naCheckboxFn();
	aeScreen.repTypeAccessRt();
	aeScreen.populateAELoggedDate();
	aeScreen.defaultSelected();
	aeScreen.gradeOvrRideFn();
	aeScreen.aeNameMandate();
});