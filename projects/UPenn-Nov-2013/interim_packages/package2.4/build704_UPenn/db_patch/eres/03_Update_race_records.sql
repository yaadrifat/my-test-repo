set define off;

UPDATE ER_CODELST SET CODELST_DESC='American Indian or Alaskan Native', CODELST_CUSTOM_COL1='race_prim,race_second',CODELST_HIDE='Y' WHERE CODELST_TYPE = 'race' AND CODELST_SUBTYP='race_indala';

UPDATE ER_CODELST SET CODELST_CUSTOM_COL1='race_prim,race_second' WHERE CODELST_TYPE = 'race' AND CODELST_SUBTYP='race_AmIndian';

UPDATE ER_CODELST SET CODELST_CUSTOM_COL1='race_prim,race_second' WHERE CODELST_TYPE = 'race' AND CODELST_SUBTYP='race_AlaskNativ';

DELETE FROM ER_CODELST WHERE CODELST_TYPE = 'race' AND CODELST_SUBTYP = 'race_AsiaPac';	

UPDATE ER_CODELST SET   CODELST_CUSTOM_COL1='race_prim,race_second' WHERE CODELST_TYPE = 'race' AND CODELST_SUBTYP='race_PacIsland';

UPDATE ER_CODELST SET  CODELST_CUSTOM_COL1='race_second' WHERE CODELST_TYPE = 'race' AND CODELST_SUBTYP='race_OPI';

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,303,9,'03_Update_race_records.sql',sysdate,'v9.3.0 #704');

commit;