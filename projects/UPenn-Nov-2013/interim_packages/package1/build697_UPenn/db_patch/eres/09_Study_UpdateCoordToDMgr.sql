set define off;

declare
v_roleExisits NUMBER := 0;
v_roleCoordPK NUMBER := 0;
v_roleDmPK NUMBER := 0;
begin
	select pk_codelst INTO v_roleCoordPK from er_codelst where codelst_subtyp = 'role_coord';
	if (v_roleCoordPK > 0)  then
		select count(*) into v_roleExisits from er_studyteam where FK_CODELST_TMROLE = v_roleCoordPK;
	
		if (v_roleExisits > 0) then
			select pk_codelst INTO v_roleDmPK from er_codelst where codelst_subtyp = 'role_dm';

			if (v_roleDmPK > 0) then 
				update er_studyteam set FK_CODELST_TMROLE = v_roleDmPK WHERE FK_CODELST_TMROLE = v_roleCoordPK;
				commit;
			end if;
		end if;
	end if;
end;
/