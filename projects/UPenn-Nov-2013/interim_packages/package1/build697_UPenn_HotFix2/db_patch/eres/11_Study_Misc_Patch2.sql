set define off;

UPDATE ER_PAGECUSTOMFLDS SET PAGECUSTOMFLDS_ATTRIBUTE = '0', PAGECUSTOMFLDS_LABEL = NULL, PAGECUSTOMFLDS_FIELD = 'sponsorlookup' WHERE PAGECUSTOMFLDS_FIELD = 'Sponsorlookup';

COMMIT;

UPDATE ER_CODELST SET CODELST_CUSTOM_COL = 'dropdown' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP ='PR_COOPGROUP_DD';
UPDATE ER_CODELST SET CODELST_CUSTOM_COL = 'dropdown' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP ='PR_CONSORTIUM';

COMMIT;

UPDATE ER_CODELST SET 
CODELST_CUSTOM_COL1='[{data:"CACTIS", display:"CACTIS"},{data:"CAMRIS", display:"CAMRIS"},{data:"CISC", display:"CISC"},{data:"DOD", display:"DOD"},{data:"FDA", display:"FDA"},{data:"IBC", display:"IBC"},{data:"IBS", display:"IBS"},{data:"IDS", display:"IDS"},{data:"IRB", display:"IRB"},{data:"PERIOP", display:"PERIOP"},{data:"PET", display:"PET"},{data:"RDC", display:"RDC"},{data:"SPONSOR", display:"SPONSOR"},{data:"TCRC", display:"TCRC"},{data:"NATIONL", display:"NATIONAL#"}]' 
WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='OTH_REVIEW_GRP';

COMMIT;

declare
v_CodeExists NUMBER := 0;
begin
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'role' AND CODELST_SUBTYP = 'role_contact1';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'role', 'role_contact1', 'Contact 1');
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'role' AND CODELST_SUBTYP = 'role_contact2';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'role', 'role_contact2', 'Contact 2');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'role' AND CODELST_SUBTYP = 'role_contact3';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'role', 'role_contact3', 'Contact 3');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'role' AND CODELST_SUBTYP = 'role_contact4';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'role', 'role_contact4', 'Contact 4');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'role' AND CODELST_SUBTYP = 'role_contact5';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'role', 'role_contact5', 'Contact 5');
	END IF;
end;
/

declare
v_roleExisits NUMBER := 0;
v_roleCoordPK NUMBER := 0;
v_roleDmPK NUMBER := 0;
begin
	select pk_codelst INTO v_roleCoordPK from er_codelst where codelst_subtyp = 'role_coord';
	if (v_roleCoordPK > 0)  then
		select count(*) into v_roleExisits from er_studyteam where FK_CODELST_TMROLE = v_roleCoordPK;
	
		if (v_roleExisits > 0) then
			select pk_codelst INTO v_roleDmPK from er_codelst where codelst_subtyp = 'role_dm';

			if (v_roleDmPK > 0) then 
				update er_studyteam set FK_CODELST_TMROLE = v_roleDmPK WHERE FK_CODELST_TMROLE = v_roleCoordPK;
				commit;
			end if;
		end if;
	end if;
end;
/

UPDATE ER_CODELST SET  CODELST_DESC='Data Manager', CODELST_HIDE= 'Y' WHERE CODELST_TYPE = 'role' AND CODELST_SUBTYP='role_coord';

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,296,1,'01_Study_UserProgMembershipLookup.sql',sysdate,'v9.3.0 #697');

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,296,2,'02_Study_UpdateNullCodelistSubtypes.sql',sysdate,'v9.3.0 #697');

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,296,3,'03_Study_PageCustomFields.sql',sysdate,'v9.3.0 #697');

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,296,4,'04_Study_StudyMoreDetailsFields.sql',sysdate,'v9.3.0 #697');

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,296,5,'05_Study_UserMoreDetailsFields.sql',sysdate,'v9.3.0 #697');

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,296,6,'06_Study_UpdateExistingCodeItems.sql',sysdate,'v9.3.0 #697');

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,296,7,'07_Study_CreateNewCodeItems.sql',sysdate,'v9.3.0 #697');

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,296,8,'08_Study_UpdateTAreaCodeItems.sql',sysdate,'v9.3.0 #697');

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,296,9,'09_Study_UpdateCoordToDMgr.sql',sysdate,'v9.3.0 #697');

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,296,10,'10_Study_Misc_Patch1.sql',sysdate,'v9.3.0 #697');

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,296,11,'11_Study_Misc_Patch2.sql.sql',sysdate,'v9.3.0 #697');

commit;

