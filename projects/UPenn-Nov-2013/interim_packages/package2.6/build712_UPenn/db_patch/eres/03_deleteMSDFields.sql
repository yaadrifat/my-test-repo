set define off;

delete from er_studyid where fk_codelst_idtype in (select pk_codelst 
from er_codelst where codelst_type = 'studyidtype'and 
codelst_subtyp in ('PR_DISEASE_STAT', 'HORM_REC_STATUS', 'HER2_NEU_STATUS'));

commit;

delete from er_codelst where codelst_type = 'studyidtype'and 
codelst_subtyp in ('PR_DISEASE_STAT', 'HORM_REC_STATUS', 'HER2_NEU_STATUS');

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,311,3,'03_deleteMSDFields.sql',sysdate,'v9.3.0 #712');

commit;