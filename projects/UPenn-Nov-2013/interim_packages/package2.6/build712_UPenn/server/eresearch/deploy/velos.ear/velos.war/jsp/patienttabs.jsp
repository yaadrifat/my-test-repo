<%@page import="com.velos.eres.web.patProt.PatProtJB"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="grpRights" scope="request" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="siteB" scope="request" class="com.velos.eres.web.site.SiteJB"/>
<jsp:useBean id="codeLst" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="patEnrollB" scope="request" class="com.velos.eres.web.patProt.PatProtJB"/>

<!-- added by Ganapathy on 05-05-05 -->
<jsp:useBean id="userb" scope="page" class="com.velos.eres.web.user.UserJB" />

<!-- added on 13Mar05 for Mahi-II enhancement-->
<jsp:useBean id="personB" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="lnkformB" scope="request" class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>
<jsp:useBean id="commonB" scope="request" class="com.velos.eres.web.common.CommonJB"/>

<%!
static String DUMMY_WFSWITCH ="PatEnrollWf_ON";
static String DUMMY_WorkflowType ="[Workflow_PatEnroll_WfType]";
%>

<%@ page language = "java" import = "com.velos.eres.business.section.*,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.StringUtil,com.velos.eres.service.util.DateUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.esch.business.common.*,com.velos.eres.service.util.*,com.velos.eres.web.user.UserJB,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.business.common.CtrlDao,com.velos.eres.business.common.SettingsDao,com.velos.eres.service.util.BrowserRows,com.velos.eres.service.util.Configuration"%>

<%@ page import="com.velos.eres.web.objectSettings.ObjectSettingsCache, com.velos.eres.web.objectSettings.ObjectSettings"%>

<%
	// added on 30th march for fixing the bug 2074
	HttpSession tS1 = request.getSession(true);
	// added by Ganapathy on 05-05-05
	int patDataDetail=0;
	int personPK=0;
	int usrGroup=0;
	String mode = "M";
	String selclass="unselectedTab";
	String userId="";
	String acc="";
	String tab= request.getParameter("selectedTab");

	String page1 = request.getParameter("page");
	String patientFName="";
	String patientLName="";
	String patientName="";

	String statDesc=request.getParameter("statDesc");

	String statid=request.getParameter("statid");

	String studyNum=request.getParameter("studyNum");

	String fromPage=request.getParameter("fromPage");

	String enrollDate = "";

//added on 13Mar05 for Mahi-II enhancement

String skipLinkedForms = StringUtil.trueValue(request.getParameter("skipLinkedForms"));
String src= request.getParameter("srcmenu");
String selectedTab111 = request.getParameter("selectedTab");
int patId=StringUtil.stringToNum(request.getParameter("pkey"));

	String calledFromForm = "";
       calledFromForm = request.getParameter("calledFromForm");
	    if (StringUtil.isEmpty(calledFromForm))
      {
     	calledFromForm = "";
      }

 String formFillDt = "";
 formFillDt = request.getParameter("formFillDt") ;
 if (formFillDt == null) formFillDt = "ALL";


	  //////////////////





	if (fromPage==null) {
		fromPage="-";
	}
	if(page1 == null)
	  page1 = "";
	String patientId="";
	String patientCode = "";
	String studyId = "";
	String studyVer = "";
	String patProtId="";
	String patStudyId = "";
	String enrollId = "";
	String pkey="";


	boolean fromStudy = true;

	boolean isEnrol = false;
	boolean isSchedule = false;
	boolean isAdv = false;
	boolean isAlert = false;
	boolean isForms = false;
	//km -to fix the Bug 2408.
	patientCode= StringUtil.decodeString(request.getParameter("patientCode"));	// added by Ganapathy on 05-05-05
	if(patientName==null)
	patientName="";
	if(patientCode==null)
		patientCode = "";



	int eptRight = 0;
	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))
	{
		acc = (String) tSession.getValue("accountId");
		patientId = (String) request.getParameter("pkey");
		pkey = (String) request.getParameter("pkey");
		studyId  = (String) request.getParameter("studyId");

		String studyIdFromSession =(String) tSession.getAttribute("studyId");
		if (!studyId.equals(studyIdFromSession)){
			tSession.setAttribute("studyId",studyId);
		}

		studyVer = request.getParameter("studyVer");
		String modRight = (String) tSession.getValue("modRight");
		int modlen = modRight.length();
		ctrl.getControlValues("module");
		int ctrlrows = ctrl.getCRows();
		ArrayList feature =  ctrl.getCValue();
		ArrayList ftrDesc = ctrl.getCDesc();
		ArrayList ftrSeq = ctrl.getCSeq();
		ArrayList ftrRight = new ArrayList();
		String strR;

		//KM
		int pageRight = 0;
		String patRightString = null;


		ObjectSettingsCache objCache = ObjectSettingsCache.getObjectSettingsCache();
		ArrayList tabList = objCache.getAccountObjects(StringUtil.stringToNum(acc), "pat_tab");

		String protocolManagementRight = (String) tSession.getValue("protocolManagementRight");

		int protocolManagementRightInt = 0;
		protocolManagementRightInt = StringUtil.stringToNum(protocolManagementRight);


			// added by Ganapathy on 05-05-05
		String userIdFromSession = (String) tSession.getValue("userId");
		userb.setUserId(StringUtil.stringToNum(userIdFromSession));
		userb.getUserDetails();
		usrGroup=StringUtil.stringToNum(userb.getUserGrpDefault());
		personPK=StringUtil.stringToNum(request.getParameter("pkey"));
		tSession.setAttribute("personPK", ""+personPK);

		patDataDetail=person.getPatientCompleteDetailsAccessRight(StringUtil.stringToNum(userIdFromSession),usrGroup,personPK);

	if(page1.equals("patientEnroll"))
	{

		enrollId= request.getParameter("patProtId");

		if (StringUtil.isEmpty(enrollId)) // if there is no patProtId passed/is empty
			{
				//check if there is any current enrollment
				patEnrollB.findCurrentPatProtDetails(StringUtil.stringToNum(studyId),StringUtil.stringToNum(pkey) );
				enrollId =  String.valueOf(patEnrollB.getPatProtId()); //get patProtId from current enrollment
			}
		else
			{

				patEnrollB.setPatProtId(StringUtil.stringToNum(enrollId));
				patEnrollB.getPatProtDetails();
			}

		String enrollIdFromSession =(String) tSession.getAttribute("enrollId");
		if (!enrollId.equals(enrollIdFromSession)){
			tSession.setAttribute("enrollId",enrollId);
		}

		patStudyId = patEnrollB.getPatStudyId();
		enrollDate = patEnrollB.getPatProtEnrolDt();

		if(StringUtil.isEmpty(patStudyId))
			{
				patStudyId="";
			}
		patProtId = enrollId;
	}


		for (int counter = 0; counter <= (modlen - 1);counter ++) {
			strR = String.valueOf(modRight.charAt(counter));
			ftrRight.add(strR);
		}
		grpRights.setGrSeq(ftrSeq);
		grpRights.setFtrRights(ftrRight);
		grpRights.setGrValue(feature);
		grpRights.setGrDesc(ftrDesc);
		eptRight  = Integer.parseInt(grpRights.getFtrRightsByValue("MODEPT"));
		studyVer = request.getParameter("studyVer");


		// To check for the account level rights
		//String modRight = (String) tSession.getValue("modRight");
		 int patProfileSeq = 0, formLibSeq = 0;
		 acmod.getControlValues("module");
		 ArrayList acmodfeature =  acmod.getCValue();
		 ArrayList acmodftrSeq = acmod.getCSeq();
		 char formLibAppRight = '0';
		 char patProfileAppRight = '0';
		 patProfileSeq = acmodfeature.indexOf("MODPATPROF");
		 formLibSeq = acmodfeature.indexOf("MODFORMS");
		 formLibSeq = ((Integer) acmodftrSeq.get(formLibSeq)).intValue();
		 patProfileSeq = ((Integer) acmodftrSeq.get(patProfileSeq)).intValue();
		 formLibAppRight = modRight.charAt(formLibSeq - 1);
		 patProfileAppRight = modRight.charAt(patProfileSeq - 1);


		GrpRightsJB grpRightsPat = (GrpRightsJB) tSession.getValue("GRights");

		if (grpRightsPat!= null)
		{
			patRightString = grpRightsPat.getFtrRightsByValue("PATFRMSACC");
		}
		if (patRightString != null)
		{
			pageRight = StringUtil.stringToNum((patRightString));
		}



%>
<!--  <table  class= "selectedTab" cellspacing="0" cellpadding="0" border="0">  -->
<table id="patientTabs" name="patientTabs" cellspacing="0" cellpadding="0" border="0">
	<tr>

<%
	//Modified for Bug #7685  By- Yogendra
	if (tab.equals("4") || tab.equals("7") || tab.equals("11") || tab.equals("1") )
	{
		fromStudy=false;
	}

	for (int iX=0; iX<tabList.size(); iX++) {
	ObjectSettings settings = (ObjectSettings)tabList.get(iX);
	selclass = "unselectedTab";

	if ("0".equals(settings.getObjVisible())) {
		continue;
	}

	boolean showThisTab = false;
	if ("1".equals(settings.getObjSubType())) {
		if( page1.equals("patient")  || page1.equals("patientEnroll")) {
		   showThisTab = true;
		}
	}
	else if ("11".equals(settings.getObjSubType())) {
		if ( page1.equals("patientEnroll") || page1.equals("patient")) { //KM-#4099
			if ((String.valueOf(formLibAppRight).compareTo("1") == 0 || String.valueOf(patProfileAppRight).compareTo("1") == 0) &&(pageRight >=4)    )
			{
			showThisTab = true;

			}
		}
	}
	else if ("2".equals(settings.getObjSubType()) )  {
		if ( page1.equals("patientEnroll") || page1.equals("patient")) {
			if (protocolManagementRightInt > 0) {
			showThisTab = true;


			if (tab.equals("3") || tab.equals("6") || tab.equals("5") || tab.equals("8")|| tab.equals("2")  )
			{
				selclass="selectedTab";
				fromStudy = true;
			}else
				{
					selclass="unselectedTab";
				}

			if (tab.equals("2"))
				isEnrol = true;
			else if (tab.equals("3"))
				isSchedule = true;
			else if (tab.equals("6"))
				isAdv = true;
			else if (tab.equals("5"))
				isAlert = true;
			else if (tab.equals("8"))
				isForms  = true;

			}
		}
	}
	else if ("4".equals(settings.getObjSubType())) {
		if ( page1.equals("patientEnroll") || page1.equals("patient")) {
		showThisTab = true;

		}
	}
	else if ("7".equals(settings.getObjSubType())) {
		if(eptRight==1){
			showThisTab = true;

   }}
   else {
		showThisTab = true;
	}

	if (!showThisTab) { continue; }

	if (tab == null) {
		selclass = "unselectedTab";
	} else if (tab.equals(settings.getObjSubType()) ) {

		selclass = "selectedTab";

	}


	 %>

	<td  valign="TOP">
		<table class="<%=selclass%>"  cellspacing="0" cellpadding="0" border="0">
		<tr>
    	<!-- 	<td rowspan="3" valign="top" wclassth="7">
      					<img src="../images/leftgreytab.gif" width=8 height=20 border="0" alt="">
    		</td>  -->
    	<td>
		<%
			if ("1".equals(settings.getObjSubType())) {
			if(page1.equals("patient")) {%>
			<a href="patientdetails.jsp?mode=M&srcmenu=tdmenubaritem5&selectedTab=1&pkey=<%=patientId%>&page=<%=page1%>&patCode=<%=StringUtil.encodeString(patientCode)%>&studyId=<%=studyId%>"><%=settings.getObjDispTxt()%>
        					</a>
		<%} if(page1.equals("patientEnroll")) {  %>
			<a href="patientdetails.jsp?mode=<%=mode%>&srcmenu=tdmenubaritem5&selectedTab=1&pkey=<%=patientId%>&page=<%=page1%>&studyId=<%=studyId%>&calledFrom=S&statDesc=<%=statDesc%>&statid=<%=statid%>&studyVer=<%=studyVer%>"><%=settings.getObjDispTxt()%>
        	</a>
		 <%} } else if ("11".equals(settings.getObjSubType())) {
				if ( page1.equals("patientEnroll") || page1.equals("patient")) {
					if (  (String.valueOf(formLibAppRight).compareTo("1") == 0 || String.valueOf(patProfileAppRight).compareTo("1") == 0) &&(pageRight >=4) )
					{

			%>
			<a href="formfilledpatbrowser.jsp?srcmenu=tdmenubaritem5&selectedTab=11&pkey=<%=patientId%>&patientCode=<%=StringUtil.encodeString(patientCode)%>"><%=settings.getObjDispTxt()%>
    						</a>

		  <% }}}
			else if ("2".equals(settings.getObjSubType()) )  {
				if ( page1.equals("patientEnroll") || page1.equals("patient")) {

				if (StringUtil.isEmpty(patProtId))
				{
    		 	patProtId= request.getParameter("patProtId");
				}
				if (protocolManagementRightInt > 0) {

		  %>
			<a href="patientstudies.jsp?srcmenu=tdMenuBarItem5&selectedTab=2&mode=<%=mode%>&pkey=<%=patientId%>&patProtId=<%=patProtId%>&patientCode=<%=StringUtil.encodeString(patientCode)%>&studyId=<%=studyId%>&statDesc=<%=statDesc%>&statid=<%=statid%>&studyVer=<%=studyVer%>&page=patient">
			  <%=settings.getObjDispTxt()%>
    		 </a>
		  <%}}}
		  else if ("4".equals(settings.getObjSubType())) {
				if ( page1.equals("patientEnroll") || page1.equals("patient")) {


		  %>
		<a href="patientreports.jsp?srcmenu=tdMenuBarItem5&selectedTab=4&mode=<%=mode%>&pkey=<%=patientId%>&patProtId=<%=patProtId%>&patientCode=<%=StringUtil.encodeString(patientCode)%>&page=<%=page1%>&generate=N&studyId=<%=studyId%>&statDesc=<%=statDesc%>&statid=<%=statid%>&studyVer=<%=studyVer%>"><%=settings.getObjDispTxt()%> </a>


		<%}}
		else if ("7".equals(settings.getObjSubType())) {
		if(eptRight==1){

			if( page1.equals("patientEnroll")){
		%>
		<a		     					href="perapndx.jsp?srcmenu=tdMenuBarItem5&selectedTab=7&mode=<%=mode%>&pkey=<%=patientId%>&page=<%=page1%>&studyId=<%=studyId%>&calledFrom=S&statDesc=<%=statDesc%>&statid=<%=statid%>&studyVer=<%=studyVer%>"><%=settings.getObjDispTxt()%>
		</a>
		<%}else if(page1.equals("patient")){%>
		<a href="perapndx.jsp?srcmenu=tdMenuBarItem5&selectedTab=7&mode=<%=mode%>&pkey=<%=patientId%>&page=<%=page1%>&studyId=<%=studyId%>&calledFrom=P&statDesc=<%=statDesc%>&statid=<%=statid%>&studyVer=<%=studyVer%>"><%=settings.getObjDispTxt()%>
			</a>
		<%}%>

		<%}}%>

		</td>
	<!--	<td rowspan="3" valign="top" wclassth="7">
        	<img src="../images/rightgreytab.gif" wclassth=7 height=20 border="0" alt="">
         </td> -->
        </tr>
        </table>
	 </td>
	<%}%>
	</tr>
	<table  class="tabBorder" width="99%" height="5"><tr><td></td></tr></table>
<!-- 	<tr>
		<td colspan=5 height=10>
		</td>
	</tr>  -->



	<%
	if (!(fromPage.equals("demographics"))) { //not to be shown in demographics tab
		person.setPersonPKId(StringUtil.stringToNum(patientId));
		person.getPersonDetails();
    	String organization = person.getPersonLocation();
    	String genderId = "";
    	String gender ="";
		CodeDao cd = new CodeDao();
		int  deadStatPk = cd.getCodeId("patient_status","dead");

		int patStatusId = 0;
		String patStatus = "";
    	String dob = "";
    	String yob = "";

		int patientMob=0;
		int patientDob=0;
		int patientYob=0;
		int patientAge=0;
		int sysYear=0;
		int sysMonth=0;
		int sysDate=0;
		int noOfMonths=0;
		int noOfYears=0;
		int noOfDays=0;

    	String age = "";
    	String siteName = "";
		String deathDate = "";
    	Calendar cal1 = new GregorianCalendar();

    	genderId = person.getPersonGender();
	dob = person.getPersonDob();
	// added by Ganapathy on 05-05-05
	patientFName=person.getPersonFname();
	patientLName=person.getPersonLname();
	if((patientFName!=null) && (patientLName!=null))
		{
		patientName=patientFName+" "+patientLName;
		}
	else if((patientFName!=null) && (patientLName==null))
		{
		patientName=patientFName;
		}
		else if((patientFName==null) && (patientLName!=null))
		{
			patientName=patientLName;
		}
		else if((patientFName==null)&&(patientLName==null))
		{		patientName="";
		}
    	gender = codeLst.getCodeDescription(StringUtil.stringToNum(genderId));

		patStatusId= StringUtil.stringToNum(person.getPersonStatus());


		deathDate = person.getPersonDeathDate();

    	if (gender==null){ gender=""; }
		//Modified by Gopu to check blank spaces for DOB
		//Modified for MAHI enhancement
		//Modifed on 20th Feb 2005
    	//if(dob != null && dob!=""){
   		//yob = dob.substring(6,10);
   		//age = (new Integer(cal1.get(Calendar.YEAR) - StringUtil.stringToNum(yob))).toString() + "&nbsp;years";
		if(dob != null && dob!="")
		{
              java.util.Date dtDob = DateUtil.stringToDate(dob);
			  patientYob = dtDob.getYear()+1900;
			  //Added by Manimaran on 041105 for Fixing the Bug # 1928 (Age Calculation)
			  patientMob = dtDob.getMonth()+1;
			  patientDob = dtDob.getDate();

			  //by sonia for issue 2497
			  if (! StringUtil.isEmpty(deathDate)) // if patient is dead, calculate age till his dod
			  {
	              java.util.Date dtDeath = DateUtil.stringToDate(deathDate);
	              sysYear = dtDeath.getYear()+1900;
	              sysMonth = dtDeath.getMonth()+1;
	              sysDate = dtDeath.getDate();
			  }
			  else
			  {
				  sysMonth=cal1.get(Calendar.MONTH)+1;
				  sysDate=cal1.get(Calendar.DATE);
				  sysYear=cal1.get(Calendar.YEAR);
				}
			  if (sysYear==patientYob)
	              patientAge=0;
	          if (sysYear > patientYob)
	          {
				  patientAge=sysYear-patientYob;
                  if(patientMob > sysMonth)
		             patientAge--;
                  if(patientMob==sysMonth && patientDob>sysDate)
			         patientAge--;
	          }
    		  if(patientAge!=0)
				  {Object[] arguments1 = {String.valueOf(patientAge)};
	    		  age = VelosResourceBundle.getLabelString("L_Spc_Years",arguments1);}/*age = String.valueOf(patientAge)+"&nbsp;years";******/
			  if(patientAge==0)
			  {
				  if(patientMob <= sysMonth && sysDate>=patientDob)
					  noOfMonths=sysMonth-patientMob;
				  else if(patientMob<sysMonth && patientDob>sysDate)
						noOfMonths=(sysMonth-patientMob)-1;
				  else if (patientMob>=sysMonth&&patientDob<=sysDate)
				  {
					  noOfMonths=(patientMob-sysMonth);
					  noOfMonths=12-(noOfMonths);
				  }
				  else if(patientMob>=sysMonth&&patientDob>sysDate)
				  {
					  noOfMonths=patientMob-sysMonth;
					  noOfMonths=11-(noOfMonths);
				  }
				  Object[] arguments2 = {String.valueOf(noOfMonths)};
	    		  age = VelosResourceBundle.getLabelString("L_Spc_Months",arguments2);/*age=String.valueOf(noOfMonths)+"&nbsp;months";*****/
			  }
			  if(patientAge==0 && noOfMonths==0)
			  {
				  if(patientDob<=sysDate)
					  noOfDays=(sysDate-patientDob)+1;
				  else
				  {
					  noOfDays=sysDate-patientDob;
					  if (patientMob==1)
						  noOfDays=31-(-noOfDays);
					  if (patientMob==2)
					  {
						  noOfDays=28-(-noOfDays);
						  if(sysYear%4==0 && (sysYear % 100 != 0 || sysYear % 400 == 0))
							  noOfDays=29-(-noOfDays);
					  }
					  if (patientMob==3)
						  noOfDays=31-(-noOfDays);
					  if (patientMob==4)
						  noOfDays=30-(-noOfDays);
					  if (patientMob==5)
						  noOfDays=31-(-noOfDays);
					  if (patientMob==6)
						  noOfDays=30-(-noOfDays);
					  if (patientMob==7)
						  noOfDays=31-(-noOfDays);
					  if (patientMob==8)
						  noOfDays=31-(-noOfDays);
					  if (patientMob==9)
						  noOfDays=30-(-noOfDays);
					  if (patientMob==10)
						  noOfDays=31-(-noOfDays);
					  if (patientMob==11)
						  noOfDays=30-(-noOfDays);
					  if (patientMob==12)
						  noOfDays=31-(-noOfDays);
					   noOfDays=noOfDays+1;
				  }
				  Object[] arguments3 = {String.valueOf(noOfDays)};
	    		  age = VelosResourceBundle.getLabelString("L_Spc_Days",arguments3);/*age= String.valueOf(noOfDays)+"&nbsp;days";*****/
			  }
			  //age = (new Integer(cal1.get(Calendar.YEAR) - StringUtil.stringToNum(yob))).toString() + "&nbsp;years";
        //  age=patientAge+"&nbsp;years";

    	} else {
    		age = "-";
    	}

    	siteB.setSiteId( StringUtil.stringToNum(organization));
    	siteB.getSiteDetails();
    	siteName = siteB.getSiteName();
    	%>
	</table>

    <table width="98%" cellspacing="0" cellpadding="0" class= "patHeader" border="0">
     <tr>
     <td width="30%"><font size="1">
    	<%patientCode=StringUtil.decodeString(patientCode);%>
    		<%=LC.L_PatID%><%--Pat.ID*****--%>: <%=patientCode%>&nbsp;&nbsp;&nbsp;
<%--
// Bug 6538: Removing the hidden DOB 
<input name = "patient_dob" type="hidden" value="DOB:<%=patDataDetail >= 4 ? dob : "*"%>"/>
 --%>
		<%if( page1.equals("patientEnroll")&& (fromStudy == true ) ){%>
		<%=LC.L_Pat_StudyId_Short%><%--Pat.<%=LC.Std_Study%> ID*****--%>: <%=patStudyId%>&nbsp;&nbsp;&nbsp;
		<%}%>
	<%=LC.L_Age%><%--Age*****--%>: <%=patDataDetail >= 4 ? age : "*"%>&nbsp;&nbsp;&nbsp;
	<%=LC.L_Gender%><%--Gender*****&--%>: <%=gender%>&nbsp;&nbsp;&nbsp;
		<!-- added by Ganapathy on 05-05-05 -->
		<%if( patDataDetail >= 4 ){%>
	<%=LC.L_PatName%><%--Pat.Name*****--%>: <%=patientName%>&nbsp;&nbsp;&nbsp;
    	<%}else{%>
		<%=LC.L_Pat_Name%><%--<%=LC.Pat_Patient%> Name*****--%>: *&nbsp;&nbsp;&nbsp;
	<%}%>
		<%=LC.L_Org%><%--Org*****--%>: <%=siteName%>
	</font>
	</td>
	<td  width="20%" align="center"><font size="1" color="red">
	<%if(deadStatPk == patStatusId){
		%>
		<%=LC.L_Pat_DiedOn%><%--<%=LC.Pat_Patient%> Died on*****--%> <%=deathDate%>&nbsp;&nbsp;&nbsp;
	<%}
   	if (StringUtil.isEmpty(enrollDate) && page1.equals("patientEnroll") && (fromStudy == true ))
   		{
   		%>
		<%=MC.M_PatNot_YetEnrl%><%--<%=LC.Pat_Patient%> is not yet 'Enrolled'.*****--%>&nbsp;&nbsp;&nbsp;
		<%
   		}%>

	</font>
	</td>
	
	<td>
	<%if ("Y".equals(CFG.Workflows_Enabled)) {%>
		<%if (DUMMY_WFSWITCH.equals(CFG.Workflow_PatEnroll_WfSwitch) && !DUMMY_WorkflowType.equals(CFG.Workflow_PatEnroll_WfType)){%>
			<font size="1">
			<%
			PatProtJB patEnrollB_WF = new PatProtJB();
			patEnrollB_WF.findOldestPatProtDetails(StringUtil.stringToNum(studyId),personPK);
			int patProtPk = patEnrollB_WF.getPatProtId();
			String params = patProtPk+"|"+studyId+"|"+personPK;
			%>
			<%if (patProtPk > 0){%>
			<jsp:include page="workflow.jsp" flush="true">
				<jsp:param name="entityId" value="<%=patProtPk%>" />
				<jsp:param name="workflow_type" value="<%=CFG.Workflow_PatEnroll_WfType%>" />
				<jsp:param name="params" value="<%=params %>" />
			</jsp:include>
			<%} %>
			&nbsp;&nbsp;Workflow &nbsp;<a id="showWorkflowTasksLink" name="showWorkflowTasksLink" href="#"><img class="headerImage" align="bottom" src="images/exclamation.png" border="0"></a>
			</font>
		<%}%>
	<%}%>
	</td>
	<td></td>
	
	</tr>


	<%}%>

	</table>

	<%
	if (page1.equals("patientEnroll") && (fromStudy == true ) )
	{
	%>

<!-- for Mahi-II, added on 13Mar05 by JM -->
</div>
<div class="tabFormTop_top1" id="divsub">
<Form method="post" id ="pattab" name="pattab" action="formfilledstdpatbrowser.jsp" onsubmit="">
     		<input type="hidden" name="srcmenu" value=<%=src%>>
     		<input type="hidden" name="selectedTab" value="8">
     		<input type="hidden" name="studyId" id="studyId" value="<%=studyId%>">
     		<input type="hidden" name="statDesc" value="<%=statDesc%>">
     		<input type="hidden" name="statid" value="<%=statid%>">
     		<input type="hidden" name="patProtId" value=<%=patProtId%>>
     		<input type="hidden" name="patientCode" value="<%=StringUtil.encodeString(patientCode)%>">
     		<input type="hidden" name="mode" value=<%=mode%>>
     		<input type="hidden" name="pkey"  id="pkey" value=<%=pkey%>>
<!--		<input type="hidden" name="patientName" value="<%=patientName%>> -->


	<table  cellspacing="0" cellpadding="0" border="0" width="100%" height="28" class="basetbl outline midAlign">
		<tr>
			<td width="1%"> <a href="studypatients.jsp?srcmenu=tdmenubaritem5&studyId=<%=studyId%>&patid=&patstatus=&selectedTab=2"><img src="../images/jpg/patient.gif" title="<%=LC.L_PatMgmt_EnrlPat%><%--<%=LC.Pat_Patient%> Management - Enrolled <%=LC.Pat_Patients%>--%>" border=0 ></a></td>
			<td width="8%">
				<% if (isEnrol) {  %>
				<!-- salil-->
					<a href="enrollpatient.jsp?srcmenu=tdMenuBarItem5&selectedTab=2&mode=<%=mode%>&pkey=<%=patientId%>&patProtId=<%=patProtId%>&patientCode=<%=StringUtil.encodeString(patientCode)%>&page=patientEnroll&studyId=<%=studyId%>&statDesc=<%=statDesc%>&statid=<%=statid%>&studyVer=<%=studyVer%>"><font color="red"><%=LC.L_ScreenOrEnrol%><%--SCREENING/ ENROLLMENT*****--%> </font></a>
				<% } else { %>
				<a href="enrollpatient.jsp?srcmenu=tdMenuBarItem5&selectedTab=2&mode=<%=mode%>&pkey=<%=patientId%>&patProtId=<%=patProtId%>&patientCode=<%=StringUtil.encodeString(patientCode)%>&page=patientEnroll&studyId=<%=studyId%>&statDesc=<%=statDesc%>&statid=<%=statid%>&studyVer=<%=studyVer%>"><%=LC.L_ScreenOrEnrol%><%--SCREENING/ ENROLLMENT*****--%></a>
				<% } %>
			</td>
			<td width="6%">
				<% if (isSchedule) { %>
					<a href="patientschedule.jsp?srcmenu=tdMenuBarItem5&selectedTab=3&mode=<%=mode%>&pkey=<%=patientId%>&page=<%=page1%>&patProtId=<%=patProtId%>&patientCode=<%=StringUtil.encodeString(patientCode)%>&page=patientEnroll&generate=N&studyId=<%=studyId%>&statDesc=<%=statDesc%>&statid=<%=statid%>&studyVer=<%=studyVer%>" ><font color="red"><%=LC.L_Schedule%><%--SCHEDULE*****--%></font></a>


				<% } else {%>
				<a href="patientschedule.jsp?srcmenu=tdMenuBarItem5&selectedTab=3&mode=<%=mode%>&pkey=<%=patientId%>&page=<%=page1%>&patProtId=<%=patProtId%>&patientCode=<%=StringUtil.encodeString(patientCode)%>&page=patientEnroll&generate=N&studyId=<%=studyId%>&statDesc=<%=statDesc%>&statid=<%=statid%>&studyVer=<%=studyVer%>"><%=LC.L_Schedule%><%--SCHEDULE*****--%></a>
				<% } %>
			</td>

			<%
			if(eptRight==1)
				{
			%>

			<td width="5%">
			<% if (isAdv) {  %>
					<A HREF="adveventbrowser.jsp?srcmenu=tdMenuBarItem5&page=patientEnroll&selectedTab=6&pkey=<%=patientId%>&visit=1&statDesc=<%=statDesc%>&statid=<%=statid%>&patProtId=<%=patProtId%>&studyId=<%=studyId%>&studyNum=<%=studyNum%>&studyVer=<%=studyVer%>"><font color="red"><%=LC.L_Adverse_Events%><%--ADVERSE EVENTS*****--%></font></A>
				<% } else { %>
            	<A HREF="adveventbrowser.jsp?srcmenu=tdMenuBarItem5&page=patientEnroll&selectedTab=6&pkey=<%=patientId%>&visit=1&statDesc=<%=statDesc%>&statid=<%=statid%>&patProtId=<%=patProtId%>&studyId=<%=studyId%>&studyNum=<%=studyNum%>&studyVer=<%=studyVer%>"><%=LC.L_Adverse_Events%><%--ADVERSE EVENTS*****--%></A>
				<% } %>
			</td>

			<!-- <td>
			<% if (isAlert) {  %>
				<a href="alertnotify.jsp?srcmenu=tdMenuBarItem5&selectedTab=5&mode=<%=mode%>&pkey=<%=patientId%>&patProtId=<%=patProtId%>&page=patientEnroll&generate=N&studyId=<%=studyId%>&statDesc=<%=statDesc%>&statid=<%=statid%>&studyVer=<%=studyVer%>&patientCode=<%=patientCode%>"><font color="red">Alerts/Notifications </font></a>
			<% } else { %>
				<a href="alertnotify.jsp?srcmenu=tdMenuBarItem5&selectedTab=5&mode=<%=mode%>&pkey=<%=patientId%>&patProtId=<%=patProtId%>&page=patientEnroll&generate=N&studyId=<%=studyId%>&statDesc=<%=statDesc%>&statid=<%=statid%>&studyVer=<%=studyVer%>&patientCode=<%=patientCode%>">Alerts/Notifications</a>
			<% } %>
			</td> -->

			 <%
				}

			 //to be displayed only in case of Forms account right
			  if (String.valueOf(formLibAppRight).compareTo("1") == 0)
				{
		 //added by JM on 14Mar05 for Mahi-II enhancement to get form names drop down from this page
		 ArrayList arrFrmNames = null;
		 ArrayList arrNumEntries = null;
 		 ArrayList arrFrmIds = null;
		 ArrayList arrEntryChar = null;
		 ArrayList arrFrmInfo = new ArrayList();

		 String entryChar="";
		 String numEntries = "";
		 String frmInfo= "";
		 int formId = 0;
		 String firstFormInfo = "";

  	     String strFormId = request.getParameter("formPullDown");
	 	 String accId=(String)tSession.getAttribute("accountId");
		 int iaccId=StringUtil.stringToNum(accId);
 	     //String
		 userId = (String) tSession.getValue("userId");
	     UserJB userB = (UserJB) tSession.getValue("currentUser");
   	     String siteId = userB.getUserSiteId();
	 	 int istudyId = StringUtil.stringToNum(studyId);
	     int ipatProtId = StringUtil.stringToNum(patProtId);

		 //int personPK = StringUtil.stringToNum(request.getParameter("pkey"));
		 personPK = StringUtil.stringToNum(request.getParameter("pkey"));
    	 personB.setPersonPKId(personPK);
     	 personB.getPersonDetails();
    	 //String
		 patientId = personB.getPersonPId();

		 LinkedFormsDao lnkFrmDao = new LinkedFormsDao();
		 if (!"Y".equals(skipLinkedForms)) {
		 	lnkFrmDao = lnkformB.getPatientStudyForms(iaccId,personPK,istudyId, StringUtil.stringToNum(userId), StringUtil.stringToNum(siteId));
		 }

		 arrFrmIds = lnkFrmDao.getFormId();
		 arrFrmNames = lnkFrmDao.getFormName();
		 arrEntryChar = lnkFrmDao.getFormEntryChar();
		 arrNumEntries = lnkFrmDao.getFormSavedEntriesNum();

		  //VA-05/05/05-check if lab is hidden for this study,if yes,don't add in DD
		 SettingsDao settingDao=commonB.retrieveSettings(istudyId,3,"FORM_HIDE");
		 ArrayList hideList=settingDao.getSettingValue();
		 boolean hideLab=false;
		  //-1 means lab is blocked
		 if (hideList.indexOf("-1")>=0) hideLab=true;


	 	 if (strFormId==null) {
		    if (arrFrmIds.size() > 0) {
				formId = StringUtil.stringToNum(arrFrmIds.get(0).toString());
          			entryChar = arrEntryChar.get(0).toString();
          			numEntries = arrNumEntries.get(0).toString();
          			firstFormInfo = arrFrmIds.get(0).toString() + "*"+ entryChar + "*" + arrNumEntries.get(0).toString();
    		} else {
				  if (!hideLab) firstFormInfo="lab";
			}
		 }
		 else if (strFormId.equals("lab")) {
		 	 if (!hideLab) firstFormInfo="lab";
	     }
     	else {
     		  //StringTokenizer strTokenizer = new StringTokenizer(strFormId,"*");
		  String[] st=StringUtil.chopChop(strFormId,'*');
    		  if (calledFromForm.equals(""))
    	 		 {
		 	  	     formId = StringUtil.stringToNum(st[0]);
	     			 entryChar = st[1];
				 entryChar=(entryChar==null)?"":entryChar;
	     		     numEntries = st[2];
			     numEntries=(numEntries==null)?"":numEntries;
			     if ((numEntries.length()==0) && (entryChar.length()==0))
			     	firstFormInfo=st[0];
				else
	     			 firstFormInfo = strFormId;
	     		}
	     		else
    			 {
    			 	 formId = StringUtil.stringToNum(st[0]);
    			 	 entryChar = st[1];

    			 	 lnkformB.findByFormId(formId );
		   		 	 numEntries = lnkformB.getLfDataCnt();
    			 	 //get the number of times the form was answered
    			 	 //prepare strFormId again
    			 	 strFormId = formId + "*" + entryChar + "*" + numEntries;
    			 	 firstFormInfo = strFormId;
    			 }
     	   }
	    		int index=-1;
			if (firstFormInfo.indexOf("*")<0 && !(firstFormInfo.trim()).toLowerCase().equals("lab") && (firstFormInfo.length()>0))
	    		index=arrFrmIds.indexOf(new Integer(firstFormInfo));

			 if (index >=0)
			 {
			    firstFormInfo=firstFormInfo + "*"+ arrEntryChar.get(index).toString() + "*" + arrNumEntries.get(index).toString();
			    entryChar=arrEntryChar.get(index).toString();
			    numEntries=arrNumEntries.get(index).toString();
			 }


     		 for (int i=0;i<arrFrmIds.size();i++)
     		 {  //store the formId, entryChar and num Entries separated with a *
     		  	 frmInfo = arrFrmIds.get(i).toString() + "*"+ arrEntryChar.get(i).toString() + "*" + arrNumEntries.get(i).toString();
     		 	 arrFrmInfo.add(frmInfo);
     		 }

		 if (!hideLab)
		 {
		  //to add lab entry in the DD

		 arrFrmInfo.add("lab");
    		 arrFrmNames.add(LC.L_Lab);/*arrFrmNames.add("Lab");*****/
		 }

		 String dformPullDown="";
		 if (arrFrmInfo.size()>0)
		 {
     	 	   dformPullDown= EJBUtil.createPullDownWithStrNoSelect("formPullDown",firstFormInfo, arrFrmInfo , arrFrmNames);
		 }
	 		 StringBuffer formBuffer = new StringBuffer(dformPullDown);

			 String formPullDown111=request.getParameter("formPullDown");


/*			 if (calledFromForm.equals(""))
			 {
				 formBuffer.replace(0,7,"<SELECT onChange=\"document.stdpatform.submit();\"");
			 } else
			  {
			 	 formBuffer.replace(0,7,"<SELECT DISABLED onChange=\"document.stdpatform.submit();\"");
			  }*/

			 dformPullDown = formBuffer.toString();

			 //end,added by JM on 14Mar05 for Mahi-II enhancement
				 %>

			<td width="3%" align="right">
						<% if (isForms) {  %>
				<a href="formfilledstdpatbrowser.jsp?mode=<%=mode%>&srcmenu=tdmenubaritem5&selectedTab=8&pkey=<%=pkey%>&patProtId=<%=patProtId%>&studyId=<%=studyId%>&calledFrom=S&statDesc=<%=statDesc%>&statid=<%=statid%>&patientCode=<%=StringUtil.encodeString(patientCode)%>"><font color="red"><%=LC.L_Forms%><%--FORMS*****--%></a></font>
			<% } else { %>
    			<a href="formfilledstdpatbrowser.jsp?mode=<%=mode%>&srcmenu=tdmenubaritem5&selectedTab=8&pkey=<%=pkey%>&patProtId=<%=patProtId%>&studyId=<%=studyId%>&calledFrom=S&statDesc=<%=statDesc%>&statid=<%=statid%>&patientCode=<%=StringUtil.encodeString(patientCode)%>">
				<%=LC.L_Forms%><%--FORMS*****--%> </a>&nbsp;
			<% }%>
			</td><td width="12%">
			 <%if (isForms) {
			  dformPullDown=(dformPullDown==null)?"":dformPullDown;
			  if (dformPullDown.length()>0){%>
			&nbsp;  <%=dformPullDown%>
			</td><td width="2%" >
			<button type="submit" onclick="javascript:fn_nextpage(document.pattab.formPullDown.value)"><%=LC.L_Go%></button>
			</td>

			<!-- added on 30th march for fixing the bug 2074 -->
			 <%}}
			 if(formPullDown111==null){
				formPullDown111 = firstFormInfo;
			 }
			 tS1.putValue("Sess",formPullDown111);
			 %>

			<input type="hidden" name="formPullDown111" value="<%=formPullDown111%>">
			<script>
	//Added on 1st April for fixing the Mahi Enhancement Phase II

		function check(val)
			{

				/* var myvar;
					myvar=document.pattab.formPullDown.options[document.pattab.formPullDown.selectedIndex].value;
					var frmId;
					if(myvar.indexOf("*E*")>0)
						{
							 frmId = myvar.substring(0, myvar.indexOf("*E*"));
								 window.location.href="patstudyformdetails.jsp?srcmenu=<%=src%>&selectedTab=8&formId="+frmId+"&mode=N&pkey=<%=pkey%>&patProtId=<%=patProtId%>&studyId=<%=studyId%>&calledFrom=S&statDesc=<%=statDesc%>&statid=<%=statid%>&patientCode=<%=patientCode%>&calledFromForm=<%=calledFromForm%>&formFillDt=<%=formFillDt%>&entryChar=E&formPullDown="+myvar;
						}
					else if(myvar.indexOf("*M*")>0)
						{
						 frmId = myvar.substring(0, myvar.indexOf("*M*"));
						 window.location.href="patstudyformdetails.jsp?srcmenu=<%=src%>&selectedTab=8&formId="+frmId+"&mode=N&pkey=<%=pkey%>&patProtId=<%=patProtId%>&studyId=<%=studyId%>&calledFrom=S&statDesc=<%=statDesc%>&statid=<%=statid%>&patientCode=<%=patientCode%>&calledFromForm=<%=calledFromForm%>&formFillDt=<%=formFillDt%>&entryChar=E&formPullDown="+myvar;

						}*/

			}

//////////////////////////////////



			//document.pattab111.formPullDown.value="<%=strFormId%>"; //set the selected form in the dropdown


			function fn_nextpage(strform){

				/*if (strform.indexOf("*E*0")>0) {
					if (strform.indexOf("*E*0")>0) { //open single entry form in new mode
						var formid = strform.substring(0, strform.indexOf("*E*"));

					//window.location.href="patstudyformdetails.jsp?srcmenu=<%=src%>&selectedTab=8&formId="+formid+"&mode=N&pkey=<%=pkey%>&patProtId=<%=patProtId%>&studyId=<%=studyId%>&calledFrom=S&statDesc=<%=statDesc%>&statid=<%=statid%>&patientCode=<%=patientCode%>&calledFromForm=<%=calledFromForm%>&formFillDt=<%=formFillDt%>&entryChar=E&formPullDown="+strform;
					//bClicked=document.pattab.bclicked.value;
					//document.pattab.action="patstudyformdetails.jsp?srcmenu=<%=src%>&selectedTab=8&formId="+formid+"&mode=N&pkey=<%=pkey%>&patProtId=<%=patProtId%>&studyId=<%=studyId%>&calledFrom=S&statDesc=<%=statDesc%>&statid=<%=statid%>&patientCode=<%=patientCode%>&calledFromForm=<%=calledFromForm%>&formFillDt=<%=formFillDt%>&entryChar=E&formPullDown="+strform+"bclicked="+bClicked;
					//document.pattab.submit();
					//document.pattab.action="formfilledstdpatbrowser.jsp";

					}
				}
				else {   */
					document.pattab.submit();
				}
			//}

			</script>
				<%}
		%>

		</tr>

	</table>
	
	</Form>
	<%
	}
	
	} //Session Valid


%>



