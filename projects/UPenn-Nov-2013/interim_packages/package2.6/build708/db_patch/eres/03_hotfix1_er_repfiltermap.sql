DECLARE
filterMID number := 0;
filterFK number := 0;
begin

	select Max(PK_REPFILTERMAP)+1 into filterMID from er_repfiltermap;
	select PK_REPFILTER into filterFK from er_repfilter where REPFILTER_KEYWORD = 'studyResType';

	INSERT INTO er_repfiltermap (pk_repfiltermap,fk_repfilter,repfiltermap_repcat,repfiltermap_seq) 
	VALUES (filterMID,filterFK, 'data safety monitoring',16);

	COMMIT;
	
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,307,3,'03_hotfix1_er_repfiltermap.sql',sysdate,'v9.3.0 #708.01');

commit;
