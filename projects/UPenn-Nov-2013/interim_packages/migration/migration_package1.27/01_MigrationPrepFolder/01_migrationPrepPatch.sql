SET DEFINE OFF;
SET SERVEROUTPUT ON;

DECLARE
  v_item_exists number := 0; 
  v_site		varchar2(4000);	
  v_account		varchar2(4000);	
BEGIN

select count(*) into v_item_exists from ERES.ER_CODELST where 
CODELST_TYPE = 'studyPurpose' and CODELST_SUBTYP = 'ancillary';
if (v_item_exists = 0) then
 Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ)
 Values
   (SEQ_ER_CODELST.nextval, NULL, 'studyPurpose', 'ancillary', 'Ancillary', 
    'Y', 150);
  commit;
  dbms_output.put_line('One row inserted');
else
  dbms_output.put_line('Row already exists');
end if;

select count(*) into v_item_exists from ERES.ER_CODELST where 
CODELST_TYPE = 'studyPurpose' and CODELST_SUBTYP = 'earlyDetection';
if (v_item_exists = 0) then
 Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ)
 Values
   (SEQ_ER_CODELST.nextval, NULL, 'studyPurpose', 'earlyDetection', 'Early Detection', 
    'Y', 151);
  commit;
  dbms_output.put_line('One row inserted');
else
  dbms_output.put_line('Row already exists');
end if;

select count(*) into v_item_exists from er_user where usr_firstname = 'Data' and usr_lastname = 'Migration' and usr_type = 'N' and fk_account = (select pk_site from er_site where site_name = 'University of Pennsylvania');

if (v_item_exists = 0) then
 select pk_site into v_site from er_site where site_name = 'University of Pennsylvania';
 Insert into ERES.ER_USER
   (PK_USER, FK_ACCOUNT, FK_SITEID, USR_LASTNAME, USR_FIRSTNAME, USR_TYPE)
 Values
   (SEQ_ER_STUDY.nextval, 50, v_site, 'Migration', 'Data', 'N');
  commit;
  dbms_output.put_line('One row inserted');
else
  dbms_output.put_line('Row already exists');
end if;

--Track Patch where DB version major is 403
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,403,24,'01_migrationPrepPatch.sql',sysdate,'v9.3.0 #712');

COMMIT;

EXCEPTION 
WHEN OTHERS THEN
ROLLBACK;

END;
/

set define off;

ALTER TABLE ESCH.SCH_ADVERSEVE MODIFY AE_CATEGORY VARCHAR2(500);
ALTER TABLE ESCH.SCH_ADVERSEVE MODIFY AE_TOXICITY VARCHAR2(500);
ALTER TABLE ESCH.SCH_ADVERSEVE MODIFY AE_TOXICTY_DESC VARCHAR2(4000);
ALTER TABLE ESCH.SCH_ADVERSEVE MODIFY AE_GRADE_DESC VARCHAR2(4000);

update er_codelst 
set codelst_custom_col1 = '<select name="alternateId"><option value="">Select An Option</option><option value="followUpRep">Follow-up Report</option><option value="initialRep">Initial Report</option></select>'
where codelst_type = 'advtype' and codelst_subtyp = 'rep_typ';

commit;

update er_codelst 
set codelst_custom_col1 = '<select name="alternateId"><option value="">Select An Option</option><option value="hours">Hours</option><option value="days">Days</option><option value="minutes">Minutes</option><option value="unknown">Unknown</option><option value="na">N/A</option></select>'
where codelst_type = 'advtype' and codelst_subtyp = 'ae_duration';

commit;
