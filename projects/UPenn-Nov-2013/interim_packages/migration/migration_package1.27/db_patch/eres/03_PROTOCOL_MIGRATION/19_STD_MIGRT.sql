CREATE TABLE DM_STUDYSUBTYPE (COLUMN_SUBTYPE VARCHAR2(4000), FORM_SUBTYPE VARCHAR2(4000));

INSERT INTO DM_STUDYSUBTYPE (FORM_SUBTYPE, COLUMN_SUBTYPE) 
VALUES ('PR_CTSRMC_REVIEWED_BIT', 'CTSRMC_REVIEWED');

INSERT INTO DM_STUDYSUBTYPE (FORM_SUBTYPE, COLUMN_SUBTYPE) 
VALUES ('PR_OTHER_REVIEW_GROUP_MC', 'OTH_REVIEW_GRP');

INSERT INTO DM_STUDYSUBTYPE (FORM_SUBTYPE, COLUMN_SUBTYPE) 
VALUES ('PR_CACTIS_NUM', 'PR_CACTIS_NUM');

INSERT INTO DM_STUDYSUBTYPE (FORM_SUBTYPE, COLUMN_SUBTYPE) 
VALUES ('PR_CACTIS_DATE', 'PR_CACTIS_DATE');

INSERT INTO DM_STUDYSUBTYPE (FORM_SUBTYPE, COLUMN_SUBTYPE) 
VALUES ('PR_CAMRIS_NUM', 'PR_CAMRIS_NUM');

INSERT INTO DM_STUDYSUBTYPE (FORM_SUBTYPE, COLUMN_SUBTYPE) 
VALUES ('PR_CAMRIS_DATE', 'PR_CAMRIS_DATE');

INSERT INTO DM_STUDYSUBTYPE (FORM_SUBTYPE, COLUMN_SUBTYPE) 
VALUES ('PR_CISC_NUM', 'PR_CISC_NUM');

INSERT INTO DM_STUDYSUBTYPE (FORM_SUBTYPE, COLUMN_SUBTYPE) 
VALUES ('PR_CISC_DATE', 'PR_CISC_DATE');

INSERT INTO DM_STUDYSUBTYPE (FORM_SUBTYPE, COLUMN_SUBTYPE) 
VALUES ('PR_DOD_NUM', 'PR_DOD_NUM');

INSERT INTO DM_STUDYSUBTYPE (FORM_SUBTYPE, COLUMN_SUBTYPE) 
VALUES ('PR_DOD_DATE', 'PR_DOD_DATE');

INSERT INTO DM_STUDYSUBTYPE (FORM_SUBTYPE, COLUMN_SUBTYPE) 
VALUES ('PR_FDA_NUM', 'PR_FDA_NUM');

INSERT INTO DM_STUDYSUBTYPE (FORM_SUBTYPE, COLUMN_SUBTYPE) 
VALUES ('PR_FDA_DATE', 'PR_FDA_DATE');

INSERT INTO DM_STUDYSUBTYPE (FORM_SUBTYPE, COLUMN_SUBTYPE) 
VALUES ('PR_IBC_NUM', 'PR_IBC_NUM');

INSERT INTO DM_STUDYSUBTYPE (FORM_SUBTYPE, COLUMN_SUBTYPE) 
VALUES ('PR_IBC_DATE', 'PR_IBC_DATE');

INSERT INTO DM_STUDYSUBTYPE (FORM_SUBTYPE, COLUMN_SUBTYPE) 
VALUES ('PR_IBS_NUM', 'PR_IBS_NUM');

INSERT INTO DM_STUDYSUBTYPE (FORM_SUBTYPE, COLUMN_SUBTYPE) 
VALUES ('PR_IBS_DATE', 'PR_IBS_DATE');

INSERT INTO DM_STUDYSUBTYPE (FORM_SUBTYPE, COLUMN_SUBTYPE) 
VALUES ('PR_IDS_NUM', 'PR_IDS_NUM');

INSERT INTO DM_STUDYSUBTYPE (FORM_SUBTYPE, COLUMN_SUBTYPE) 
VALUES ('PR_IDS_DATE', 'PR_IDS_DATE');

INSERT INTO DM_STUDYSUBTYPE (FORM_SUBTYPE, COLUMN_SUBTYPE) 
VALUES ('PR_IRB_NUM', 'PR_IRB_NUM');

INSERT INTO DM_STUDYSUBTYPE (FORM_SUBTYPE, COLUMN_SUBTYPE) 
VALUES ('PR_IRB_DATE', 'PR_IRB_DATE');

INSERT INTO DM_STUDYSUBTYPE (FORM_SUBTYPE, COLUMN_SUBTYPE) 
VALUES ('PR_PERIOP_NUM', 'PR_PERIOP_NUM');

INSERT INTO DM_STUDYSUBTYPE (FORM_SUBTYPE, COLUMN_SUBTYPE) 
VALUES ('PR_PERIOP_DATE', 'PR_PERIOP_DATE');

INSERT INTO DM_STUDYSUBTYPE (FORM_SUBTYPE, COLUMN_SUBTYPE) 
VALUES ('PR_PET_NUM', 'PR_PET_NUM');

INSERT INTO DM_STUDYSUBTYPE (FORM_SUBTYPE, COLUMN_SUBTYPE) 
VALUES ('PR_PET_DATE', 'PR_PET_DATE');

INSERT INTO DM_STUDYSUBTYPE (FORM_SUBTYPE, COLUMN_SUBTYPE) 
VALUES ('PR_RDC_NUM', 'PR_RDC_NUM');

INSERT INTO DM_STUDYSUBTYPE (FORM_SUBTYPE, COLUMN_SUBTYPE) 
VALUES ('PR_RDC_DATE', 'PR_RDC_DATE');

INSERT INTO DM_STUDYSUBTYPE (FORM_SUBTYPE, COLUMN_SUBTYPE) 
VALUES ('PR_SPONSOR_NUM', 'PR_SPONSOR_NUM');

INSERT INTO DM_STUDYSUBTYPE (FORM_SUBTYPE, COLUMN_SUBTYPE) 
VALUES ('PR_SPONSOR_DATE', 'PR_SPONSOR_DATE');

INSERT INTO DM_STUDYSUBTYPE (FORM_SUBTYPE, COLUMN_SUBTYPE) 
VALUES ('PR_TCRC_NUM', 'PR_TCRC_NUM');

INSERT INTO DM_STUDYSUBTYPE (FORM_SUBTYPE, COLUMN_SUBTYPE) 
VALUES ('PR_TCRC_DATE', 'PR_TCRC_DATE');

INSERT INTO DM_STUDYSUBTYPE (FORM_SUBTYPE, COLUMN_SUBTYPE) 
VALUES ('PR_CORE_SERVICE', 'PR_CORE_SERVICE');

INSERT INTO DM_STUDYSUBTYPE (FORM_SUBTYPE, COLUMN_SUBTYPE) 
VALUES ('PR_SECONDARY_SUPPORTING_GROUP', 'PR_SEC_SUPP_GRP');

INSERT INTO DM_STUDYSUBTYPE (FORM_SUBTYPE, COLUMN_SUBTYPE) 
VALUES ('PR_PROTOCOL_GROUP_COOPGROUP_DD', 'PR_COOPGROUP_DD');

INSERT INTO DM_STUDYSUBTYPE (FORM_SUBTYPE, COLUMN_SUBTYPE) 
VALUES ('PR_CONSORTIUM', 'PR_CONSORTIUM');

INSERT INTO DM_STUDYSUBTYPE (FORM_SUBTYPE, COLUMN_SUBTYPE) 
VALUES ('PR_PRIMARY_TREATMENT_STAGE', 'PRIM_Tx_STAGE');

INSERT INTO DM_STUDYSUBTYPE (FORM_SUBTYPE, COLUMN_SUBTYPE) 
VALUES ('PR_SECONDARY_TREATMENT_STAGE', 'SEC_Tx_STAGE');

INSERT INTO DM_STUDYSUBTYPE (FORM_SUBTYPE, COLUMN_SUBTYPE) 
VALUES ('PR_TERTIARY_TREATMENT_STAGE', 'TERT_Tx_STAGE');

INSERT INTO DM_STUDYSUBTYPE (FORM_SUBTYPE, COLUMN_SUBTYPE) 
VALUES ('PR_TYPE_TREATMENT', 'PR_TYPE_Tx');

INSERT INTO DM_STUDYSUBTYPE (FORM_SUBTYPE, COLUMN_SUBTYPE) 
VALUES ('PR_PENN_ACCRUAL_GOAL_LOW', 'PENN_GOAL_LOW');

INSERT INTO DM_STUDYSUBTYPE (FORM_SUBTYPE, COLUMN_SUBTYPE) 
VALUES ('PR_PENN_ACCRUAL_GOAL_HIGH', 'PENN_GOAL_HIGH');

INSERT INTO DM_STUDYSUBTYPE (FORM_SUBTYPE, COLUMN_SUBTYPE) 
VALUES ('PR_ENROLLMENT_TYPE', 'ENROLLMENT_TYPE');

INSERT INTO DM_STUDYSUBTYPE (FORM_SUBTYPE, COLUMN_SUBTYPE) 
VALUES ('PR_GRANTEE', 'PR_GRANTEE');

INSERT INTO DM_STUDYSUBTYPE (FORM_SUBTYPE, COLUMN_SUBTYPE) 
VALUES ('PR_DEMOGRAPHICS_CAPTURED_BIT', 'PR_DEMOG_CAPTUR');

INSERT INTO DM_STUDYSUBTYPE (FORM_SUBTYPE, COLUMN_SUBTYPE) 
VALUES ('PR_PENN_INITIATED_SITES', 'PENN_INIT_SITE');

INSERT INTO DM_STUDYSUBTYPE (FORM_SUBTYPE, COLUMN_SUBTYPE) 
VALUES ('PR_STUDY_RISK', 'PR_STUDY_RISK');

--Track Patch where DB version major is 403
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,403,19,'19_STD_MIGRT.sql',sysdate,'v9.3.0 #712');

COMMIT;
/