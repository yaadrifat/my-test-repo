DECLARE
V_UPCC                   VARCHAR2(100);
V_COOP                   VARCHAR2(100);
V_FK_FORM                NUMBER := 294;
V_CODELST_TYPE           VARCHAR2(15) := 'studyidtype';
V_SUBTYPE                VARCHAR2(15);
V_COLUMN_NAME            VARCHAR2(100);
V_PK_CODELST             NUMBER;
V_PK_STUDYID             NUMBER;
V_SQL                    LONG;
V_FORM_VALUE             VARCHAR2(4000);
V_CREATOR                NUMBER;
V_CODELST_CUSTOM_COL     VARCHAR2(4000);
V_ERES_VALUE             VARCHAR2(4000);
V_FK_CODELST_PURPOSE     NUMBER;
V_HEALTH_SER_RES         NUMBER;
V_PK_HEALTH              NUMBER;
V_COL900                 NUMBER;
V_PK_FORMSLINEAR         NUMBER;
BEGIN
    --More Study Detail Migration
    SELECT PK_USER INTO V_CREATOR FROM ER_USER WHERE UPPER(USR_FIRSTNAME) = UPPER('DATA') AND UPPER(USR_LASTNAME) = UPPER('MIGRATION');
    
    
    FOR k IN (SELECT PK_FORMSLINEAR FROM DM_STUDYFORMDATA WHERE AFTER_900_STATUS  = 1 OR PK_FORMSLINEAR = 53878 OR AFTER_900_STATUS  =10)
	LOOP
		V_PK_FORMSLINEAR := k.PK_FORMSLINEAR;
	FOR j IN (SELECT COL_900 FROM DM_STUDYFORMDATA WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR)    
            LOOP
            V_COL900 := j.COL_900;
            
                SELECT NVL((SELECT FK_CODELST_PURPOSE FROM ER_STUDY WHERE PK_STUDY = V_COL900), 0) INTO V_FK_CODELST_PURPOSE FROM DUAL;
            
                SELECT PK_CODELST INTO V_HEALTH_SER_RES FROM ER_CODELST WHERE CODELST_DESC = 'Health Services Research' AND CODELST_TYPE = 'studyPurpose';
                SELECT NVL((SELECT PK_STUDYID FROM ER_STUDYID WHERE FK_STUDY = V_COL900 AND FK_CODELST_IDTYPE = (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_SUBTYP = 'PR_RESEARCH_CAT' AND  CODELST_TYPE = V_CODELST_TYPE)), 0) INTO V_PK_HEALTH FROM DUAL;
            
            
                FOR i IN (SELECT DST.COLUMN_SUBTYPE AS COLUMN_SUBTYPE, EMP.MP_MAPCOLNAME AS MP_MAPCOLNAME FROM DM_STUDYSUBTYPE DST, ER_MAPFORM EMP WHERE EMP.FK_FORM =V_FK_FORM AND TRIM(UPPER(DST.FORM_SUBTYPE)) = TRIM(UPPER(EMP.MP_UID)))
                LOOP
                
                V_SUBTYPE := i.COLUMN_SUBTYPE;
                V_COLUMN_NAME := i.MP_MAPCOLNAME;
                IF UPPER(i.MP_MAPCOLNAME) = UPPER('col79') THEN
                    V_COLUMN_NAME := 'CASE '||i.MP_MAPCOLNAME||' WHEN NULL THEN ''No'' ELSE '||i.MP_MAPCOLNAME||' END';
                END IF;
            
                    SELECT PK_CODELST, NVL(CODELST_CUSTOM_COL, 'abcde') INTO V_PK_CODELST,V_CODELST_CUSTOM_COL FROM ER_CODELST WHERE UPPER(CODELST_SUBTYP) = UPPER(V_SUBTYPE) AND UPPER(CODELST_TYPE) = UPPER(V_CODELST_TYPE);
            
                    SELECT NVL((SELECT PK_STUDYID FROM ER_STUDYID WHERE FK_CODELST_IDTYPE = V_PK_CODELST AND FK_STUDY = V_COL900), 0) INTO V_PK_STUDYID FROM DUAL;
                    V_SQL := 0;
            
                    V_SQL := 'SELECT'||' '|| V_COLUMN_NAME||' '||'FROM ER_FORMSLINEAR WHERE fk_form = '||V_FK_FORM||' and col900 ='||V_COL900||' AND PK_FORMSLINEAR ='||V_PK_FORMSLINEAR;
                    EXECUTE IMMEDIATE V_SQL INTO V_FORM_VALUE;
            
                    IF UPPER(V_CODELST_CUSTOM_COL) = UPPER('DROPDOWN') AND V_FORM_VALUE IS NOT NULL THEN
                        SELECT TRIM(REPLACE(SUBSTR(V_FORM_VALUE,INSTR(V_FORM_VALUE, ']')), ']', ''))  INTO V_ERES_VALUE FROM DUAL;
						IF V_COLUMN_NAME = 'col46' THEN 
							SELECT TRIM(SUBSTR(V_FORM_VALUE,1,INSTR(V_FORM_VALUE, '[')-1))  INTO V_ERES_VALUE FROM DUAL;
						END IF;
                    ELSIF UPPER(V_CODELST_CUSTOM_COL) = UPPER('CHECKBOX') AND V_FORM_VALUE IS NOT NULL THEN
                        SELECT TRIM(REPLACE(REPLACE(SUBSTR(V_FORM_VALUE,1, INSTR(UPPER(V_FORM_VALUE),'[VELSEP1]')),']',''),'[','') ) INTO V_ERES_VALUE FROM DUAL;
                    ELSIF UPPER(V_CODELST_CUSTOM_COL) = UPPER('HIDDEN-INPUT') AND V_FORM_VALUE IS NOT NULL THEN
                        IF LENGTH(V_FORM_VALUE) > 0 THEN
                            V_FORM_VALUE := 'Y' ;
                        ELSIF LENGTH(V_FORM_VALUE) = 0 THEN
                            V_FORM_VALUE := 'N' ;
                        END IF;
                        V_ERES_VALUE := V_FORM_VALUE;
                    ELSIF UPPER(V_CODELST_CUSTOM_COL) = UPPER('DATE') AND V_FORM_VALUE IS NOT NULL THEN
                        V_ERES_VALUE := V_FORM_VALUE;
                    ELSIF UPPER(V_CODELST_CUSTOM_COL) = UPPER('LOOKUP') AND V_FORM_VALUE IS NOT NULL THEN
                        V_ERES_VALUE := V_FORM_VALUE;                        
                    ELSIF V_CODELST_CUSTOM_COL = 'abcde' AND V_FORM_VALUE IS NOT NULL THEN
            
                        V_ERES_VALUE := V_FORM_VALUE;
                    ELSE
                        V_ERES_VALUE := 'NOACTION';
                    END IF;
                    
                    IF V_PK_STUDYID > 0 AND V_ERES_VALUE <> 'NOACTION' THEN
                        UPDATE ER_STUDYID SET STUDYID_ID = V_ERES_VALUE WHERE PK_STUDYID = V_PK_STUDYID;
                    ELSIF V_PK_STUDYID = 0 AND V_ERES_VALUE <> 'NOACTION' THEN
                        INSERT INTO ER_STUDYID (PK_STUDYID, FK_STUDY, FK_CODELST_IDTYPE, STUDYID_ID, CREATOR, CREATED_ON) VALUES (SEQ_ER_STUDYID.NEXTVAL, V_COL900, V_PK_CODELST, V_ERES_VALUE, V_CREATOR, SYSDATE);
                    ELSE
                        null;
            
                    END IF;
            
                END LOOP;
            
            IF V_FK_CODELST_PURPOSE = V_HEALTH_SER_RES THEN
                IF V_PK_HEALTH <> 0 THEN
                    UPDATE ER_STUDYID SET STUDYID_ID = 'Retrospective' WHERE PK_STUDYID = V_PK_STUDYID;
                ELSIF V_PK_HEALTH = 0 THEN
                    INSERT INTO ER_STUDYID (PK_STUDYID, FK_STUDY, FK_CODELST_IDTYPE, STUDYID_ID, CREATOR, CREATED_ON) VALUES (SEQ_ER_STUDYID.NEXTVAL, V_COL900, V_HEALTH_SER_RES, 'Retrospective', V_CREATOR, SYSDATE);
                END IF;
            ELSE 
                NULL;
            END IF;
			UPDATE ER_STUDYID SET FK_CODELST_IDTYPE = (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_SUBTYP = 'PR_RESEARCH_CAT' AND  CODELST_TYPE = V_CODELST_TYPE) WHERE STUDYID_ID = 'Retrospective' AND FK_STUDY = V_COL900;
            commit;
            END LOOP;
		  END LOOP;
        
 --Track Patch where DB version major is 403
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,403,20,'20_STD_MIGRT.sql',sysdate,'v9.3.0 #712');
COMMIT;

EXCEPTION 
WHEN OTHERS THEN
ROLLBACK;

END; 
/