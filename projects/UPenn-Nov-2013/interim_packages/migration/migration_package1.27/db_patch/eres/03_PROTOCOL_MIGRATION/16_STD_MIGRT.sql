DECLARE
V_FIRST_NAME                VARCHAR2(4000);
V_LAST_NAME                 VARCHAR2(4000);
V_FK_AUTHOR                 NUMBER;
V_STUDY_PRINV               VARCHAR2(4000);
V_FK_ACCOUNT                NUMBER;
V_FK_CODELST_TAREA          VARCHAR2(4000);
V_FK_CODELST_RESTYPE        VARCHAR2(4000);
V_FK_CODELST_SPONSOR        VARCHAR2(4000);
V_FK_CODELST_PHASE          VARCHAR2(4000);
V_FK_CODELST_TYPE           VARCHAR2(4000);
V_STUDY_DISEASE_SITE        VARCHAR2(4000);
V_STUDY_DUR                 VARCHAR2(4000);
V_STUDY_DUR_UNIT            VARCHAR2(4000);
V_FK_CODELST_SCOPE          VARCHAR2(4000);
V_STUDY_MAJ_AUTH            VARCHAR2(4000);
V_NCT_NUMBER                VARCHAR2(4000);
V_INDIDE_NUMBER             VARCHAR2(4000);
V_G_COMMENTS                VARCHAR2(4000);
V_PK_INDIDE                 NUMBER;
V_FK_FORM                   NUMBER := 294;
V_COL900                    NUMBER;
V_PK_FORMSLINEAR            NUMBER;
V_CREATOR_MOD               NUMBER;
V_AFTER_900_STATUS			NUMBER;
V_COOP_TRIAL				NUMBER;
V_PK_STUDYID				NUMBER;
BEGIN
    SELECT PK_USER INTO V_CREATOR_MOD FROM ER_USER WHERE UPPER(USR_FIRSTNAME) = UPPER('Data') AND UPPER(USR_LASTNAME) = UPPER('Migration');
	SELECT PK_CODELST INTO V_COOP_TRIAL FROM ER_CODELST WHERE CODELST_SUBTYP='COOP_TRIAL' AND CODELST_TYPE='studyidtype';
       
        FOR i IN (SELECT PK_FORMSLINEAR,COL_900,AFTER_900_STATUS FROM DM_STUDYFORMDATA WHERE AFTER_900_STATUS  =1 OR PK_FORMSLINEAR=53878 OR AFTER_900_STATUS  =10)
		LOOP
		V_PK_FORMSLINEAR := i.PK_FORMSLINEAR;
		V_COL900 := i.COL_900;
		V_AFTER_900_STATUS := i.AFTER_900_STATUS;
            
            SELECT COL86,COL87,CREATOR,col34, CASE TRIM(REGEXP_SUBSTR(col45,'[^[]+')) WHEN 'Institutional (In-House)' THEN 'Institutional (In-House)' WHEN 'Industrial (Pharma)' THEN 'Industrial' WHEN 'Cooperative Group' THEN 'National' WHEN 'Other Externally Peer Reviewed' THEN 'Externally Peer-Reviewed' WHEN 'CHOP' THEN 'CHOP' ELSE NULL END AS COL45, col49,TRIM(REGEXP_SUBSTR(col50,'[^[]+')) AS COL50, CASE TRIM(REGEXP_SUBSTR(col51,'[^[]+'))  WHEN 'Companion' THEN 'Health Services Research' WHEN 'Retrospective Analysis' THEN 'Health Services Research' WHEN 'Correlative' THEN 'Basic Science' WHEN 'Epidemiologic' THEN 'Other' WHEN 'Observational' THEN 'Other' WHEN 'Outcome' THEN 'Other' WHEN 'Prevention Intervention' THEN 'Prevention' WHEN 'Supportive Care' THEN 'Supportive' WHEN 'Therapeutic Intervention' THEN 'Treatment' ELSE TRIM(REGEXP_SUBSTR(col51,'[^[]+'))  END AS col51, col52,col158,CASE TRIM(REGEXP_SUBSTR(col70,'[^[]+')) WHEN 'Pilot or Ancillary (NO THERAPEUTIC AGENTS)' THEN 'Multiple' ELSE TRIM(REGEXP_SUBSTR(col70,'[^[]+')) END AS col70, col71, TRIM(REGEXP_SUBSTR(col75,'[^[]+')) AS  col75,TRIM(REGEXP_SUBSTR(col80,'[^[]+')) AS  COL80,col159 INTO V_FIRST_NAME,V_LAST_NAME,V_FK_AUTHOR,V_FK_CODELST_TAREA,V_FK_CODELST_RESTYPE,V_FK_CODELST_SPONSOR,V_FK_CODELST_PHASE, V_FK_CODELST_TYPE,V_INDIDE_NUMBER,V_G_COMMENTS,V_STUDY_DISEASE_SITE, V_STUDY_DUR, V_STUDY_MAJ_AUTH,V_FK_CODELST_SCOPE,V_NCT_NUMBER FROM ER_FORMSLINEAR WHERE FK_FORM=V_FK_FORM AND PK_FORMSLINEAR = V_PK_FORMSLINEAR;
			
			SELECT NVL((SELECT PK_STUDYID FROM ER_STUDYID WHERE FK_STUDY = V_COL900 AND FK_CODELST_IDTYPE = V_COOP_TRIAL), 0) INTO V_PK_STUDYID FROM DUAL;

            IF LENGTH(V_FIRST_NAME) > 0 AND LENGTH(V_LAST_NAME) > 0 THEN
                SELECT NVL((SELECT PK_USER  FROM ER_USER WHERE TRIM(UPPER(USR_FIRSTNAME)) = TRIM(UPPER(V_FIRST_NAME)) AND TRIM(UPPER(USR_LASTNAME)) = TRIM(UPPER(V_LAST_NAME))), NULL) INTO V_STUDY_PRINV FROM DUAL;                               
                    
                    UPDATE ER_STUDY 
                        SET 
                            FK_CODELST_TAREA   =  NVL((SELECT PK_CODELST FROM ER_CODELST WHERE UPPER(CODELST_DESC)=UPPER(V_FK_CODELST_TAREA) AND ROWNUM=1 AND CODELST_TYPE = 'tarea'), NULL),
                            FK_CODELST_RESTYPE =  NVL((SELECT PK_CODELST FROM ER_CODELST WHERE UPPER(CODELST_DESC)=UPPER(V_FK_CODELST_RESTYPE) AND CODELST_TYPE= 'research_type' AND ROWNUM=1), NULL),
                            FK_CODELST_SPONSOR =  NVL((SELECT PK_CODELST FROM ER_CODELST WHERE UPPER(CODELST_DESC)=UPPER(V_FK_CODELST_SPONSOR) AND ROWNUM=1), NULL),
                            FK_CODELST_PHASE   =  NVL((SELECT PK_CODELST FROM ER_CODELST WHERE UPPER(CODELST_DESC)=UPPER(V_FK_CODELST_PHASE) AND ROWNUM=1), NULL),
                            FK_CODELST_PURPOSE    =  NVL((SELECT PK_CODELST FROM ER_CODELST WHERE UPPER(CODELST_DESC)=UPPER(V_FK_CODELST_TYPE) AND ROWNUM=1 AND CODELST_TYPE = 'studyPurpose'), NULL),
                            STUDY_DISEASE_SITE =  NVL((SELECT PK_CODELST FROM ER_CODELST WHERE UPPER(CODELST_DESC)=UPPER(V_STUDY_DISEASE_SITE) AND ROWNUM=1), NULL),
                            FK_CODELST_SCOPE   =  NVL((SELECT PK_CODELST FROM ER_CODELST WHERE UPPER(CODELST_DESC)=UPPER(V_FK_CODELST_SCOPE) AND UPPER(CODELST_TYPE)=UPPER('STUDYSCOPE') AND ROWNUM=1), NULL),
                            STUDY_MAJ_AUTH       =   CASE  V_STUDY_MAJ_AUTH WHEN 'Yes' THEN 'Y' ELSE NULL END,
                            STUDY_DUR= TO_NUMBER(V_STUDY_DUR),
                            STUDY_DURUNIT='months',
                            STUDY_INFO = V_G_COMMENTS,
                            NCT_NUMBER=V_NCT_NUMBER, 
                            STUDY_DIVISION =    NVL((SELECT PK_CODELST FROM ER_CODELST WHERE  UPPER(CODELST_DESC)=UPPER('ABRAMSON CANCER CENTER')), NULL),
                            STUDY_PRINV = (V_STUDY_PRINV),
                            STUDY_INVIND_NUMBER = '',
                            STUDY_INVIND_FLAG = '',
                            FK_AUTHOR = V_FK_AUTHOR,
                            LAST_MODIFIED_BY=V_CREATOR_MOD,
                            LAST_MODIFIED_DATE=SYSDATE
                        WHERE PK_STUDY = V_COL900;
						
						IF V_AFTER_900_STATUS=1 THEN
							IF V_PK_STUDYID = 0 THEN
							INSERT INTO ER_STUDYID (PK_STUDYID, FK_STUDY, FK_CODELST_IDTYPE, STUDYID_ID, CREATOR, CREATED_ON) VALUES (SEQ_ER_STUDYID.NEXTVAL, V_COL900, V_COOP_TRIAL, 'Y', V_CREATOR_MOD, SYSDATE);
							ELSE 
							UPDATE ER_STUDYID SET STUDYID_ID = 'Y' WHERE FK_STUDY = V_COL900 AND FK_CODELST_IDTYPE = V_COOP_TRIAL;
							END IF;
						ELSIF V_AFTER_900_STATUS=10 THEN
							IF V_PK_STUDYID = 0 THEN
							INSERT INTO ER_STUDYID (PK_STUDYID, FK_STUDY, FK_CODELST_IDTYPE, STUDYID_ID, CREATOR, CREATED_ON) VALUES (SEQ_ER_STUDYID.NEXTVAL, V_COL900, V_COOP_TRIAL, 'N', V_CREATOR_MOD, SYSDATE);
							ELSE 
							UPDATE ER_STUDYID SET STUDYID_ID = 'N' WHERE FK_STUDY = V_COL900 AND FK_CODELST_IDTYPE = V_COOP_TRIAL;
							END IF;
						END IF;
						
                    SELECT NVL((SELECT (PK_STUDY_INDIIDE) FROM ER_STUDY_INDIDE WHERE FK_STUDY= V_COL900),0) INTO V_PK_INDIDE FROM DUAL;
                    
                    IF V_INDIDE_NUMBER IS NOT NULL THEN
                    IF V_PK_INDIDE <> 0 THEN
                  
                    UPDATE ER_STUDY 
                        SET 
                        STUDY_INVIND_FLAG = 0
                        WHERE PK_STUDY = V_COL900;
                        
                     UPDATE ER_STUDY_INDIDE 
                            SET INDIDE_TYPE = 1 ,
                                INDIDE_NUMBER = SUBSTR(V_INDIDE_NUMBER, 0, 24),
                                FK_CODELST_INDIDE_GRANTOR = (SELECT PK_CODELST FROM ER_CODELST WHERE UPPER(CODELST_DESC)=UPPER('NOT RECORDED') AND UPPER(CODELST_TYPE)=UPPER('INDIDEGRANTOR')),
                                FK_CODELST_INDIDE_HOLDER  = (SELECT PK_CODELST FROM ER_CODELST WHERE UPPER(CODELST_DESC)=UPPER('NOT RECORDED') AND UPPER(CODELST_TYPE)=UPPER('INDIDEHOLDER')),                                 
                                LAST_MODIFIED_BY = V_CREATOR_MOD,
                                LAST_MODIFIED_DATE=SYSDATE
                        WHERE PK_STUDY_INDIIDE = V_PK_INDIDE;
                    ELSIF  V_PK_INDIDE = 0 then
                    
                        INSERT INTO ER_STUDY_INDIDE 
                        (
                            PK_STUDY_INDIIDE,
                            INDIDE_TYPE,
                            INDIDE_NUMBER,
                            FK_CODELST_INDIDE_GRANTOR,
                            FK_CODELST_INDIDE_HOLDER,
                            FK_STUDY,
                            CREATOR,
                            CREATED_ON
                        )
                        VALUES
                        (
                            SEQ_ER_STUDY_INDIDE.NEXTVAL,
                            1 ,
                            SUBSTR(V_INDIDE_NUMBER, 0, 24),
                            (SELECT PK_CODELST FROM ER_CODELST WHERE UPPER(CODELST_DESC)=UPPER('NOT RECORDED') AND UPPER(CODELST_TYPE)=UPPER('INDIDEGRANTOR')),                           
                            (SELECT PK_CODELST FROM ER_CODELST WHERE UPPER(CODELST_DESC)=UPPER('NOT RECORDED') AND UPPER(CODELST_TYPE)=UPPER('INDIDEHOLDER')),
                            V_COL900,
                            V_CREATOR_MOD,
                            SYSDATE
                        );
                    END IF;
                    ELSE 
                    NULL;
                    END IF;
                END IF;            
           
       
		END LOOP;
		
    
--Track Patch where DB version major is 403
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,403,16,'16_STD_MIGRT.sql',sysdate,'v9.3.0 #712');	
COMMIT;

EXCEPTION WHEN OTHERS THEN
ROLLBACK;

END;
/