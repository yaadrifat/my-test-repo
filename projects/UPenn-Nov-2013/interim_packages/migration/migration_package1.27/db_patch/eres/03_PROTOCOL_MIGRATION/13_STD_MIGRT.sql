DECLARE
	CURSOR C_STUDYFORMDATATEMP IS SELECT PK_FORMSLINEAR,UPPER(TRIM(UPCC_CODE)) AS UPCC_CODE, UPPER(TRIM(COOP_GROUP)) AS COOP_GROUP , COL_900 FROM DM_STUDYFORMDATA;
	V_PK_FORMSLINEAR 	NUMBER;
	V_UPCC_CODE 		VARCHAR2(4000);
	V_COOP_GROUP 		VARCHAR2(4000);
	V_UPCC_STUDY_COUNT 	NUMBER;
	V_COOP_STUDY_COUNT 	NUMBER;
	V_col900_COUNT 		NUMBER;
	V_COOP_COUNT 		NUMBER;
	V_COLNINEHUND 		NUMBER;
	V_COLNINECOUNT 		NUMBER;
	V_UPCC_COUNT_INCOOP NUMBER;
	V_COOP_COUNT_INUPCC NUMBER;
	V_UDATE_EXECUTED 	BOOLEAN := FALSE;
BEGIN
	OPEN C_STUDYFORMDATATEMP;
	LOOP
		FETCH C_STUDYFORMDATATEMP INTO V_PK_FORMSLINEAR,V_UPCC_CODE, V_COOP_GROUP, V_COLNINEHUND;
		EXIT WHEN C_STUDYFORMDATATEMP%NOTFOUND;
		V_UPCC_STUDY_COUNT := 0;
		V_COOP_STUDY_COUNT := 0;
		V_COOP_COUNT := 0;
		V_UPCC_COUNT_INCOOP := 0;
		V_COOP_COUNT_INUPCC := 0;
		V_COLNINECOUNT		:=0;
		V_UDATE_EXECUTED := FALSE;
		
		IF (LENGTH(V_UPCC_CODE) > 0 AND LENGTH(V_COOP_GROUP) > 0 ) THEN
			
			select count(pk_study) into V_COLNINECOUNT from er_study where pk_study=V_COLNINEHUND;
			select count(pk_formslinear) into V_col900_COUNT from DM_STUDYFORMDATA where COL_900 = V_COLNINEHUND;
			
			IF V_COLNINECOUNT = 1 and V_col900_COUNT <= 1 then
				UPDATE DM_STUDYFORMDATA SET REMARKS = 'UPCC# and Cooperative Group# are not mutually exclusive' ,REMARKS_COL900 = 'UNIQUE STUDY FOUND ON THE BASIS OF COL900' , REMARKS_UPCC_COOP='', FORM_VALUES=1, FINAL_STATUS=1 WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR  ;
			end if;
			
			IF V_COLNINECOUNT = 0 and V_col900_COUNT <= 1 THEN
				
				IF UPPER(V_UPCC_CODE) = 'CTSRMC ONLY' OR UPPER(V_UPCC_CODE) = 'REQUESTING A UPCC' THEN
					SELECT COUNT(PK_STUDY) INTO V_COOP_STUDY_COUNT FROM ER_STUDY WHERE UPPER(STUDY_NUMBER) = UPPER(V_COOP_GROUP);
					
					IF V_COOP_STUDY_COUNT = 1 THEN
						UPDATE DM_STUDYFORMDATA SET REMARKS = 'UPCC# and Cooperative Group# are not mutually exclusive' ,REMARKS_UPCC_COOP = 'UNIQUE STUDY FOUND ON THE BASIS OF COOP GROUP' , REMARKS_COL900='STUDY NOT FOUND ON THE BASIS OF COL900', FORM_VALUES=1, FINAL_STATUS=2 WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR  ;
					ELSIF V_COOP_STUDY_COUNT = 0 THEN
						SELECT COUNT(PK_STUDY) INTO V_UPCC_STUDY_COUNT FROM ER_STUDY WHERE UPPER(STUDY_NUMBER) = UPPER(V_UPCC_CODE);
						IF V_UPCC_STUDY_COUNT = 1 THEN
							UPDATE DM_STUDYFORMDATA SET REMARKS = 'UPCC# and Cooperative Group# are not mutually exclusive' ,REMARKS_UPCC_COOP = 'UNIQUE STUDY FOUND ON THE BASIS OF UPCC#' , REMARKS_COL900='STUDY NOT FOUND ON THE BASIS OF COL900', FORM_VALUES=1, FINAL_STATUS=3 WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR  ;
						ELSIF V_UPCC_STUDY_COUNT = 0 THEN
							UPDATE DM_STUDYFORMDATA SET REMARKS = 'UPCC# and Cooperative Group# are not mutually exclusive' ,REMARKS_UPCC_COOP = 'STUDY NOT FOUND ON THE BASIS UPCC# AND COOP GROUP' , REMARKS_COL900='STUDY NOT FOUND ON THE BASIS OF COL900', FORM_VALUES=1, FINAL_STATUS=0 WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR  ;
						ELSE
							UPDATE DM_STUDYFORMDATA SET REMARKS = 'UPCC# and Cooperative Group# are not mutually exclusive' ,REMARKS_UPCC_COOP = 'DUPLICATE STUDY FOUND ON THE BASIS OF UPCC#', REMARKS_COL900='STUDY NOT FOUND ON THE BASIS OF COL900' , FORM_VALUES=1, FINAL_STATUS=-2 WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR  ;
						END IF;
					ELSE
						UPDATE DM_STUDYFORMDATA SET REMARKS = 'UPCC# and Cooperative Group# are not mutually exclusive' ,REMARKS_UPCC_COOP = 'DUPLICATE STUDY FOUND ON THE BASIS OF COOP GROUP', REMARKS_COL900='STUDY NOT FOUND ON THE BASIS OF COL900' , FORM_VALUES=1, FINAL_STATUS=-2 WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR  ;
					END IF;
				
				ELSIF UPPER(V_UPCC_CODE) != 'CTSRMC ONLY' AND UPPER(V_UPCC_CODE) != 'REQUESTING A UPCC' THEN
					
					SELECT COUNT(PK_STUDY) INTO V_UPCC_STUDY_COUNT FROM ER_STUDY WHERE UPPER(STUDY_NUMBER) = UPPER(V_UPCC_CODE);
					
					IF V_UPCC_STUDY_COUNT = 1 THEN
						UPDATE DM_STUDYFORMDATA SET REMARKS = 'UPCC# and Cooperative Group# are not mutually exclusive' ,REMARKS_UPCC_COOP = 'UNIQUE STUDY FOUND ON THE BASIS OF UPCC#',REMARKS_COL900='STUDY NOT FOUND ON THE BASIS OF COL900', FORM_VALUES=1, FINAL_STATUS=3 WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR  ;
					ELSIF V_UPCC_STUDY_COUNT = 0 THEN
						SELECT COUNT(PK_STUDY) INTO V_COOP_STUDY_COUNT FROM ER_STUDY WHERE UPPER(STUDY_NUMBER) = UPPER(V_COOP_GROUP);
						IF V_COOP_STUDY_COUNT = 1 THEN
							UPDATE DM_STUDYFORMDATA SET REMARKS = 'UPCC# and Cooperative Group# are not mutually exclusive' ,REMARKS_UPCC_COOP = 'UNIQUE STUDY FOUND ON THE BASIS OF COOP GROUP' ,REMARKS_COL900='STUDY NOT FOUND ON THE BASIS OF COL900', FORM_VALUES=1, FINAL_STATUS=2 WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR  ;
						ELSIF V_COOP_STUDY_COUNT = 0 THEN
							UPDATE DM_STUDYFORMDATA SET REMARKS = 'UPCC# and Cooperative Group# are not mutually exclusive' ,REMARKS_UPCC_COOP = 'STUDY NOT FOUND ON THE BASIS OF UPCC# AND COOP GROUP' , FORM_VALUES=1,REMARKS_COL900='STUDY NOT FOUND ON THE BASIS OF COL900', FINAL_STATUS=0 WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR  ;
						ELSE
							UPDATE DM_STUDYFORMDATA SET REMARKS = 'UPCC# and Cooperative Group# are not mutually exclusive' ,REMARKS_UPCC_COOP = 'DUPLICATE STUDY FOUND ON THE BASIS OF COOP GROUP' , FORM_VALUES=1,REMARKS_COL900='STUDY NOT FOUND ON THE BASIS OF COL900', FINAL_STATUS=-2 WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR  ;
						END IF;
					ELSE
						UPDATE DM_STUDYFORMDATA SET REMARKS = 'UPCC# and Cooperative Group# are not mutually exclusive' ,REMARKS_UPCC_COOP = 'DUPLICATE STUDY FOUND ON THE BASIS OF UPCC#' , FORM_VALUES=1, FINAL_STATUS=-2 WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR  ;
					END IF;
				END IF;
			END IF;
			IF V_col900_COUNT > 1 THEN
					UPDATE DM_STUDYFORMDATA SET REMARKS = 'UPCC# and Cooperative Group# are not mutually exclusive' ,REMARKS_COL900 = 'DUPLICATE WITHIN COL900' , REMARKS_UPCC_COOP='', FORM_VALUES=2, FINAL_STATUS=-3 WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR  ;
				END IF;
			V_UDATE_EXECUTED := TRUE;
		END IF;

		IF (LENGTH(V_UPCC_CODE) > 0 AND V_UDATE_EXECUTED = FALSE) THEN
			
			select count(pk_study) into V_COLNINECOUNT from er_study where pk_study=V_COLNINEHUND;
			select count(pk_formslinear) into V_col900_COUNT from DM_STUDYFORMDATA where COL_900 = V_COLNINEHUND;
			
			IF V_COLNINECOUNT = 1 and V_col900_COUNT <= 1 then
				UPDATE DM_STUDYFORMDATA SET REMARKS = 'UPCC# and Cooperative Group# are mutually exclusive' ,REMARKS_COL900 = 'UNIQUE STUDY FOUND ON THE BASIS OF COL900' , REMARKS_UPCC_COOP='', FORM_VALUES=2, FINAL_STATUS=4 WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR  ;
			ELSIF V_COLNINECOUNT = 0  and V_col900_COUNT <= 1  THEN
				SELECT COUNT(PK_STUDY) INTO V_UPCC_STUDY_COUNT FROM ER_STUDY WHERE UPPER(STUDY_NUMBER) = UPPER(V_UPCC_CODE);
					IF V_UPCC_STUDY_COUNT = 1 THEN 
						UPDATE DM_STUDYFORMDATA SET REMARKS = 'UPCC# and Cooperative Group# are mutually exclusive' ,REMARKS_COL900 = 'STUDY NOT FOUND ON THE BASIS OF COL900' , REMARKS_UPCC_COOP='UNIQUE STUDY FOUND ON THE BASIS OF UPCC#', FORM_VALUES=2, FINAL_STATUS=5 WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR  ;
					ELSIF V_UPCC_STUDY_COUNT = 0 THEN 
						UPDATE DM_STUDYFORMDATA SET REMARKS = 'UPCC# and Cooperative Group# are mutually exclusive' ,REMARKS_COL900 = 'STUDY NOT FOUND ON THE BASIS OF COL900' , REMARKS_UPCC_COOP='STUDY NOT FOUND ON THE BASIS OF UPCC#', FORM_VALUES=2, FINAL_STATUS=0 WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR  ;
					ELSIF V_UPCC_STUDY_COUNT > 1 THEN 
						UPDATE DM_STUDYFORMDATA SET REMARKS = 'UPCC# and Cooperative Group# are mutually exclusive' ,REMARKS_COL900 = 'STUDY NOT FOUND ON THE BASIS OF COL900' , REMARKS_UPCC_COOP='DUPLICATE STUDY FOUND ON THE BASIS OF UPCC#', FORM_VALUES=2, FINAL_STATUS=-2 WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR  ;
					END IF;	
			ELSE 
					UPDATE DM_STUDYFORMDATA SET REMARKS = 'UPCC# and Cooperative Group# are mutually exclusive' ,REMARKS_COL900 = 'DUPLICATE WITHIN COL900' , REMARKS_UPCC_COOP='', FORM_VALUES=2, FINAL_STATUS=-2 WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR  ;
			END IF;
			V_UDATE_EXECUTED := TRUE;
		END IF;
		
		IF (LENGTH(V_COOP_GROUP) > 0 AND V_UDATE_EXECUTED = FALSE) THEN
			
			select count(pk_study) into V_COLNINECOUNT from er_study where pk_study=V_COLNINEHUND;
			select count(pk_formslinear) into V_col900_COUNT from DM_STUDYFORMDATA where COL_900 = V_COLNINEHUND; 
			
			IF V_COLNINECOUNT = 1 and V_col900_COUNT <= 1 then
				UPDATE DM_STUDYFORMDATA SET REMARKS = 'UPCC# and Cooperative Group# are mutually exclusive' ,REMARKS_COL900 = 'UNIQUE STUDY FOUND ON THE BASIS OF COL900' , REMARKS_UPCC_COOP='', FORM_VALUES=3, FINAL_STATUS=6 WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR  ;
			
			ELSIF V_COLNINECOUNT = 0  and V_col900_COUNT <= 1  THEN
				SELECT COUNT(PK_STUDY) INTO V_COOP_STUDY_COUNT FROM ER_STUDY WHERE UPPER(STUDY_NUMBER) = UPPER(V_COOP_GROUP);
					IF V_COOP_STUDY_COUNT = 1 THEN 
						UPDATE DM_STUDYFORMDATA SET REMARKS = 'UPCC# and Cooperative Group# are mutually exclusive' ,REMARKS_COL900 = 'STUDY NOT FOUND ON THE BASIS OF COL900' , FORM_VALUES=3, REMARKS_UPCC_COOP='UNIQUE STUDY FOUND ON THE BASIS OF COOP GROUP', FINAL_STATUS=7 WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR  ;
					ELSIF V_COOP_STUDY_COUNT = 0 THEN 
						UPDATE DM_STUDYFORMDATA SET REMARKS = 'UPCC# and Cooperative Group# are mutually exclusive' ,REMARKS_COL900 = 'STUDY NOT FOUND ON THE BASIS OF COL900' , FORM_VALUES=3, REMARKS_UPCC_COOP='STUDY NOT FOUND ON THE BASIS OF COOP GROUP', FINAL_STATUS=0 WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR  ;
					ELSIF V_COOP_STUDY_COUNT > 1 THEN 
						UPDATE DM_STUDYFORMDATA SET REMARKS = 'UPCC# and Cooperative Group# are mutually exclusive' ,REMARKS_COL900 = 'STUDY NOT FOUND ON THE BASIS OF COL900' , FORM_VALUES=3, REMARKS_UPCC_COOP='DUPLICATE STUDY FOUND ON THE BASIS OF COOP GROUP', FINAL_STATUS=-2 WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR  ;
					END IF;	
			ELSE 
				UPDATE DM_STUDYFORMDATA SET REMARKS = 'UPCC# and Cooperative Group# are mutually exclusive' ,REMARKS_COL900 = 'DUPLICATE WITHIN COL900' , REMARKS_UPCC_COOP='', FORM_VALUES=3, FINAL_STATUS=-2  WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR  ;
			END IF;	
		END IF;
	COMMIT;		
	
	END LOOP;
	CLOSE C_STUDYFORMDATATEMP;

--Track Patch where DB version major is 403
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,403,13,'13_STD_MIGRT.sql',sysdate,'v9.3.0 #712');	
COMMIT;	
EXCEPTION WHEN OTHERS THEN
ROLLBACK;
	DBMS_OUTPUT.PUT_LINE('EXCEPTION IN STUDY STATUS ANALYSIS '||SQLERRM) ;
	
END;
/