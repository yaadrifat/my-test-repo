SET SERVEROUTPUT ON;

DECLARE 
CURSOR C_PATIENT_RECORDS IS SELECT ID,col900,col3,col4,col11,TO_DATE(col16,'MM-DD-yyyy') AS col16,TRIM(REGEXP_SUBSTR(col7,'[^[]+')) AS col7,col262,col263,TRIM(REGEXP_SUBSTR(col8,'[^[]+')) AS col8,col264,TRIM(REGEXP_SUBSTR(col30, '[^[]+')) AS col30 ,CREATED_ON, REGEXP_SUBSTR(COL36, '[^[]+'), COL38, REGEXP_SUBSTR(COL37, '[^[]+') FROM ER_FORMSLINEAR WHERE FK_FORM=295;
V_STUDYID 					NUMBER;
V_COL900 					NUMBER;
V_PATIENT_INITIALS 			VARCHAR2(2000);
V_PATIENT_MRN_NUM 			VARCHAR2(2000);
V_PATIENT_MULTIPLE_RACES 	VARCHAR2(2000);
V_PATIENT_DOB 				DATE;
V_PATIENT_GENDER 			VARCHAR2(2000);
V_PATIENT_COUNTRY 			VARCHAR2(2000);
V_PATIENT_ZIP_CODE			VARCHAR2(2000);
V_PATIENT_RACE_DD 			VARCHAR2(2000);
V_PATIENT_ETHNICITY			VARCHAR2(4000);
V_PERSON_ADD_ETHNICITY 		VARCHAR2(4000);
V_PATIENT_STUDY_RELATION 	VARCHAR2(4000);
V_PK_PERSON					NUMBER;
V_TO_RACE_VALUE 			VARCHAR2(2000);
V_FINAL_RACE 				VARCHAR2(4000);
V_TO_SURVIVAL_STATUS		VARCHAR2(2000);
V_TO_ETHNICITY_VALUE 		VARCHAR2(2000);
V_PK_PATPROT 				NUMBER;
V_PTST_SURVIVAL 			NUMBER;
V_PK_PATSTUDYSTAT 			NUMBER;
V_CREATED_ON  				DATE;
V_CREATOR             		NUMBER;
V_MULTIPLE_VALUES 			VARCHAR2(4000);
V_COUNTER 					NUMBER := 1;
V_FROM_VALUE 				VARCHAR2(4000);
V_TO_VALUE 					VARCHAR2(4000);
V_ALTER 					VARCHAR2(4000);
N 							NUMBER;
V_TRIG						VARCHAR2(4000);
V_TRIG_1					VARCHAR2(4000);
--V_REASON					VARCHAR2(4000);
--V_REASON_NUM				NUMBER;
V_PAT_DEM					NUMBER;
V_WITHRWN_STATUS			NUMBER;
V_PAT_DEM_NULL				NUMBER;
V_INSTITUTE					VARCHAR2(4000);
V_INSTITUTE_NUM				NUMBER;
V_COL38						VARCHAR2(4000);
V_COL37						VARCHAR2(4000);
V_INSTITUTE_ACT				VARCHAR2(4000);
V_PATFACILITY				NUMBER;
BEGIN

N:=0;
V_TRIG:= 'ALTER TRIGGER EPAT.PERSON_AU0 DISABLE';
V_TRIG_1:= 'ALTER TRIGGER EPAT.PERSON_AU0 ENABLE';
EXECUTE IMMEDIATE V_TRIG;
    SELECT NVL((SELECT PK_USER  FROM ER_USER WHERE UPPER(USR_FIRSTNAME) = UPPER('DATA') AND UPPER(USR_LASTNAME) = UPPER('MIGRATION')),0) INTO V_CREATOR FROM DUAL;
	SELECT PK_CODELST INTO V_WITHRWN_STATUS FROM ER_CODELST WHERE UPPER(CODELST_TYPE) = UPPER('patStatus') AND UPPER(CODELST_DESC) = UPPER('Withdrawn');
	SELECT PK_CODELST into V_PAT_DEM_NULL FROM ER_CODELST WHERE UPPER(CODELST_TYPE)=UPPER('patient_status') AND UPPER(CODELST_DESC)=UPPER('Alive');
OPEN C_PATIENT_RECORDS;


    LOOP
		V_PAT_DEM := null;
		V_FINAL_RACE := null;
		V_INSTITUTE_ACT := NULL;
        FETCH C_PATIENT_RECORDS INTO V_STUDYID,V_COL900,V_PATIENT_INITIALS,V_PATIENT_MRN_NUM,V_PATIENT_MULTIPLE_RACES,V_PATIENT_DOB,V_PATIENT_GENDER,V_PATIENT_COUNTRY,V_PATIENT_ZIP_CODE,V_PATIENT_RACE_DD,V_PERSON_ADD_ETHNICITY,V_PATIENT_STUDY_RELATION,V_CREATED_ON, V_INSTITUTE, V_COL38, V_COL37;
		
		--DBMS_OUTPUT.PUT_LINE(V_PERSON_ADD_ETHNICITY);
		
		--SELECT NVL((SELECT PK_SITE FROM ER_SITE WHERE SITE_NAME = V_INSTITUTE WHERE ROWNUM=1), NULL) INTO V_INSTITUTE_NUM FROM DUAL;
		
        EXIT WHEN C_PATIENT_RECORDS%NOTFOUND;
		
		--------------------------------
		IF V_INSTITUTE = 'Other Institution' THEN
		
		select case trim(V_COL38) WHEN    'Abington'    THEN    'Abington Memorial Hospital'
		WHEN    'Abington Memorial'    THEN    'Abington Memorial Hospital'
		WHEN    'ACC'    THEN    'Abramson Cancer Center'
		WHEN    'Baylor'    THEN    'Baylor Health Care System'
		WHEN    'Case Western'    THEN    'Case Western Reserve University'
		WHEN    'CHI'    THEN    'California Healthcare Insitute'
		WHEN    'Children''s Hospital of Philadelphia'    THEN    'Children''s Hospital of Philadelphia'
		WHEN    'chop'    THEN    'Children''s Hospital of Philadelphia'
		WHEN    'CHOP'    THEN    'Children''s Hospital of Philadelphia'
		WHEN    'CINJ'    THEN    'Rutgers Cancer Institute of New Jersey'
		WHEN    'CINJ/UMDNJ'    THEN    'Rutgers Cancer Institute of New Jersey'
		WHEN    'FCCC'    THEN    'Fox Chase Cancer Center'
		WHEN    'Fox Chase'    THEN    'Fox Chase Cancer Center'
		WHEN    'Fox Chase Cancer Center'    THEN    'Fox Chase Cancer Center'
		WHEN    'Jacobi Medical Center'    THEN    'Jacobi Medical Center'
		WHEN    'MADACC'    THEN    'MD Anderson Cancer Center'
		WHEN    'MDACC'    THEN    'MD Anderson Cancer Center'
		WHEN    'MDAnderson'    THEN    'MD Anderson Cancer Center'
		WHEN    'MDAnderson Cancr Center'    THEN    'MD Anderson Cancer Center'
		WHEN    'Northwestern University'    THEN    'Northwestern University'
		WHEN    'PCAM'    THEN    'Abramson Cancer Center'
		WHEN    'Perelman Center'    THEN    'Abramson Cancer Center'
		WHEN    'PITT'    THEN    'University of Pittsburgh'
		WHEN    'Princeton Medical Center'    THEN    'University Medical Center of Princeton at Plainsboro'
		WHEN    'Procure'    THEN    'ProCure Treatment Centers, Inc.'
		WHEN    'ProCure CHI'    THEN    'ProCure Treatment Centers, Inc.'
		WHEN    'ROSWELL PARK'    THEN    'Roswell Park Cancer Institute'
		WHEN    'Roswell park Cancer Institute'    THEN    'Roswell Park Cancer Institute'
		WHEN    'Roswell park cancer Institute'    THEN    'Roswell Park Cancer Institute'
		WHEN    'Roswell Park Cancer Institute'    THEN    'Roswell Park Cancer Institute'
		WHEN    'ROSWELL PARK CANCER INSTUTITE'    THEN    'Roswell Park Cancer Institute'
		WHEN    'SLOAN KETTERING'    THEN    'Memorial Sloan Kettering Cancer Center'
		WHEN    'St. Mary''s Regional CC'    THEN    'St. Mary''s Regional Medical Center'
		WHEN    'Tobacco Reseach Center'    THEN    'Abramson Cancer Center'
		WHEN    'Tobacco Research Center'    THEN    'Abramson Cancer Center'
		WHEN    'UChicago'    THEN    'University of Chicago Medicine Comprehensive Cancer Center'
		WHEN    'UMD'    THEN    'University of Maryland Greenebaum Cancer Center'
		WHEN    'UNC'    THEN    'UNC Lineberger Comprehensive Cancer Center'
		WHEN    'unc'    THEN    'UNC Lineberger Comprehensive Cancer Center'
		WHEN    'Univ. of Maryland Greenbaum Cancer Center'    THEN    'University of Maryland Greenebaum Cancer Center'
		WHEN    'Univeristy of Maryland'    THEN    'University of Maryland Greenebaum Cancer Center'
		WHEN    'University Hospitals Case Medical Center'    THEN    'University Hospitals Case Medical Center'
		WHEN    'University of Chicago'    THEN    'University of Chicago Medicine Comprehensive Cancer Center'
		WHEN    'University of Maryland'    THEN    'University of Maryland Greenebaum Cancer Center'
		WHEN    'University of Maryland Greenebaum Cancer Center'    THEN    'University of Maryland Greenebaum Cancer Center'
		WHEN    'University of Pennsylvania'    THEN    'Abramson Cancer Center'
		WHEN    'University of Pittsburgh'    THEN    'University of Pittsburgh'
		WHEN    'Vanderbilt'    THEN    'Vanderbilt-Ingram Cancer Center'
		WHEN    'washington university'    THEN    'Siteman Cancer Center'
		WHEN    'Washington University'    THEN    'Siteman Cancer Center'
		WHEN    'Wilkes-Barre'    THEN    'Geisinger Cancer Institute'	ELSE NULL END INTO V_INSTITUTE_ACT FROM DUAL;
		
		ELSIF V_INSTITUTE = 'Network Site' THEN
		
		select CASE trim(V_COL37) WHEN	'Cape Regional Medical Center'	THEN	'Cape Regional Medical Center'
		WHEN	'Chester County Hospital'	THEN	'Chester County Hospital'
		WHEN	'Chestnut Hill Health System'	THEN	'Chestnut Hill Health System'
		WHEN	'Doylestown Hospital'	THEN	'Doylestown Hospital'
		WHEN	'Redeemer Health System'	THEN	'Holy Redeemer Health System'
		WHEN	'Lancaster General Hospital'	THEN	'Lancaster General Hospital'
		WHEN	'Shore Memorial Hospital'	THEN	'Shore Memorial Hospital'
		WHEN	'Capital Health System'	THEN	'Capital Health System'
		WHEN	'Hackettstown Regional Medical Center'	THEN	'Hackettstown Regional Medical Center'
		WHEN	'Lourdes Health System'	THEN	'Lourdes Health System'
		WHEN	'Ocean Medical Center'	THEN	'Ocean Medical Center'
		WHEN	'Phoenixville Hospital'	THEN	'Phoenixville Hospital'
		WHEN	'St. Joseph Medical Center'	THEN	'St. Joseph Medical Center'
		WHEN	'St. Lukes Hospital and Health Network'	THEN	'St. Lukes Hospital and Health Network'
		WHEN	'Warren Hospital'	THEN	'Warren Hospital' ELSE NULL END INTO V_INSTITUTE_ACT FROM DUAL;
		
		ELSIF V_INSTITUTE = 'Presbyterian Medical Center' THEN
		
		V_INSTITUTE_ACT:= 'Presbyterian Medical Center';
		
		ELSIF V_INSTITUTE = 'Hospital University of Pennsylvania' THEN
		
		V_INSTITUTE_ACT:= 'Abramson Cancer Center';
		
		ELSIF V_INSTITUTE = 'Pennsylvania Hospital' then
		
		V_INSTITUTE_ACT:= 'Pennsylvania Hospital';
		
		end if;	
		
		SELECT NVL((SELECT PK_SITE FROM ER_SITE WHERE SITE_NAME = V_INSTITUTE_ACT and ROWNUM=1), NULL) INTO V_INSTITUTE_NUM FROM DUAL;
		--------------------------------
       
	   --SELECT NVL((SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_DESC = V_REASON AND CODELST_TYPE= 'withdrawnT'), NULL) INTO V_REASON_NUM  FROM DUAL;
	   N:=N+1;
	   update TestingMigration1 set col900 = V_COL900, status = N;
	   COMMIT;
        IF(LENGTH(V_PATIENT_MULTIPLE_RACES)>0) THEN
            SELECT (REGEXP_COUNT(V_PATIENT_MULTIPLE_RACES, ';') + 1) INTO V_MULTIPLE_VALUES FROM DUAL;
            FOR I IN 1..V_MULTIPLE_VALUES LOOP 
                SELECT REGEXP_SUBSTR(V_PATIENT_MULTIPLE_RACES,'[^;]+',1,V_COUNTER) INTO V_FROM_VALUE FROM DUAL;
                SELECT ( TO_CHAR((SELECT PK_CODELST FROM ER_CODELST WHERE UPPER(CODELST_TYPE)=UPPER('RACE') AND UPPER(CODELST_DESC) = UPPER((SELECT TO_VALUE FROM DM_MAPPING WHERE UPPER(FROM_VALUE)= UPPER(V_FROM_VALUE) AND UPPER(MAPPING_TYPE)=UPPER('RACE')))))) INTO V_TO_VALUE FROM DUAL ;
                V_COUNTER := V_COUNTER+1;
                V_FINAL_RACE:=V_FINAL_RACE||V_TO_VALUE||',';
            END LOOP;
        END IF;
        V_COUNTER := 1; -- RESET COUNTER
        V_PATIENT_MULTIPLE_RACES := '';

        IF LENGTH(V_PERSON_ADD_ETHNICITY)>0 THEN
                SELECT TO_VALUE INTO V_TO_ETHNICITY_VALUE FROM DM_MAPPING WHERE TRIM(UPPER(FROM_VALUE)) = TRIM(UPPER(V_PERSON_ADD_ETHNICITY)) AND UPPER(MAPPING_TYPE)=UPPER('ETHNICITY');
                IF UPPER(V_TO_ETHNICITY_VALUE) = UPPER('HISPANIC') THEN
                    V_PERSON_ADD_ETHNICITY := V_PERSON_ADD_ETHNICITY;
                else
                    V_PERSON_ADD_ETHNICITY := NULL;
                END IF;                
        
		ELSE
				V_PERSON_ADD_ETHNICITY:= 0;		
		END IF;
        IF(LENGTH(V_PATIENT_STUDY_RELATION)>0) THEN
            SELECT TO_VALUE INTO V_TO_SURVIVAL_STATUS FROM DM_MAPPING WHERE TRIM(UPPER(FROM_VALUE)) = TRIM(UPPER(V_PATIENT_STUDY_RELATION)) AND UPPER(MAPPING_TYPE)=UPPER('survivalstatus');
            SELECT PK_CODELST into V_PTST_SURVIVAL  FROM ER_CODELST WHERE UPPER(CODELST_TYPE)=UPPER('PTST_SURVIVAL') AND UPPER(CODELST_DESC)=UPPER('DECEASED');
			SELECT PK_CODELST into V_PAT_DEM  FROM ER_CODELST WHERE UPPER(CODELST_TYPE)=UPPER('patient_status') AND UPPER(CODELST_DESC)=UPPER('Deceased');
        ELSE
            SELECT NVL((SELECT COUNT(PK_PATPROT) FROM ER_PATPROT WHERE PK_PATPROT = V_COL900 AND FK_CODELST_PTST_SURVIVAL IS NOT NULL),0) INTO V_PK_PATPROT FROM DUAL;
            IF(V_PK_PATPROT = 0) THEN
                SELECT PK_CODELST into V_PTST_SURVIVAL  FROM ER_CODELST WHERE UPPER(CODELST_TYPE)=UPPER('PTST_SURVIVAL') AND UPPER(CODELST_DESC)=UPPER('ALIVE');
				SELECT PK_CODELST into V_PAT_DEM  FROM ER_CODELST WHERE UPPER(CODELST_TYPE)=UPPER('patient_status') AND UPPER(CODELST_DESC)=UPPER('Alive');
            END IF;
        END IF;
            
        SELECT NVL((SELECT COUNT(PK_PERSON) FROM EPAT.PERSON WHERE PK_PERSON = V_COL900),0) INTO V_PK_PERSON FROM DUAL;
        IF(V_PK_PERSON > 0) THEN
            
            UPDATE er_patfacility SET PAT_FACILITYID = COALESCE(PAT_FACILITYID,V_PATIENT_MRN_NUM),last_modified_by = V_CREATOR ,last_modified_date = sysdate where  FK_PER = V_PK_PERSON;
          
            UPDATE EPAT.PERSON SET 
            PERSON_LNAME             = COALESCE(PERSON_LNAME,V_PATIENT_INITIALS) ,
            PERSON_ADD_RACE          = COALESCE(PERSON_ADD_RACE, RTRIM(V_FINAL_RACE, ',')),
            PERSON_DOB               = COALESCE(PERSON_DOB,V_PATIENT_DOB),
            FK_CODELST_GENDER        = COALESCE(FK_CODELST_GENDER,(SELECT PK_CODELST FROM ER_CODELST WHERE UPPER(CODELST_TYPE) = UPPER('GENDER') AND UPPER(CODELST_DESC) = UPPER(V_PATIENT_GENDER) AND ROWNUM = 1)),
            PERSON_COUNTRY           = COALESCE(PERSON_COUNTRY,V_PATIENT_COUNTRY),
            PERSON_ZIP               = COALESCE(PERSON_ZIP,V_PATIENT_ZIP_CODE),
            FK_CODELST_RACE          = COALESCE(FK_CODELST_RACE,(SELECT PK_CODELST FROM ER_CODELST WHERE UPPER(CODELST_TYPE) = UPPER('RACE') AND UPPER(CODELST_DESC) = UPPER(V_PATIENT_RACE_DD) AND ROWNUM = 1)),
            FK_CODELST_ETHNICITY     = COALESCE(FK_CODELST_ETHNICITY,(SELECT PK_CODELST FROM ER_CODELST WHERE UPPER(CODELST_TYPE) = UPPER('ETHNICITY') AND UPPER(CODELST_DESC) = UPPER(V_TO_ETHNICITY_VALUE) AND ROWNUM = 1)),
            PERSON_ADD_ETHNICITY     = COALESCE(PERSON_ADD_ETHNICITY, TO_CHAR((SELECT PK_CODELST FROM ER_CODELST WHERE UPPER(CODELST_TYPE) = UPPER('ETHNICITY') AND UPPER(CODELST_DESC) = UPPER(V_PERSON_ADD_ETHNICITY) AND ROWNUM = 1))),
            LAST_MODIFIED_BY         = V_CREATOR,
            LAST_MODIFIED_DATE       = SYSDATE,
			FK_CODELST_PSTAT		 = NVL(V_PAT_DEM, V_PAT_DEM_NULL)
            WHERE 
            PK_PERSON = V_COL900;                
            
        END IF;
        
        V_PK_PATPROT := 0;
        SELECT NVL((SELECT COUNT(PK_PATPROT) FROM ER_PATPROT WHERE     FK_PER = V_COL900),0) INTO V_PK_PATPROT FROM DUAL;
        IF V_PK_PATPROT > 0 THEN
			IF V_INSTITUTE_NUM IS NOT NULL THEN
            UPDATE ER_PATPROT SET FK_CODELST_PTST_SURVIVAL = V_PTST_SURVIVAL, FK_CODELST_PTST_DTH_STDREL = (select pk_codelst from er_codelst where upper(codelst_type)=upper('PTST_DTH_STDREL') and upper(codelst_desc)=upper(V_TO_SURVIVAL_STATUS)) , LAST_MODIFIED_DATE = SYSDATE ,LAST_MODIFIED_BY = V_CREATOR, FK_SITE_ENROLLING = V_INSTITUTE_NUM  where FK_PER = V_COL900 AND FK_STUDY = V_STUDYID;
			
			SELECT COUNT(*) INTO V_PATFACILITY FROM ER_PATFACILITY WHERE FK_PER=V_COL900 AND FK_SITE=V_INSTITUTE_NUM;
		
				IF V_PATFACILITY > 0 THEN
					NULL;
				ELSE
					insert into ER_PATFACILITY (PK_PATFACILITY,FK_PER,FK_SITE,PAT_FACILITYID,PATFACILITY_ACCESSRIGHT,PATFACILITY_DEFAULT,CREATOR,IS_READONLY)
					values (SEQ_ER_PATFACILITY.NEXTVAL, V_COL900, V_INSTITUTE_NUM, '0', 7, 0, V_CREATOR, '');
				end if;
			
			ELSE
			UPDATE ER_PATPROT SET FK_CODELST_PTST_SURVIVAL = V_PTST_SURVIVAL, FK_CODELST_PTST_DTH_STDREL = (select pk_codelst from er_codelst where upper(codelst_type)=upper('PTST_DTH_STDREL') and upper(codelst_desc)=upper(V_TO_SURVIVAL_STATUS)) , LAST_MODIFIED_DATE = SYSDATE ,LAST_MODIFIED_BY = V_CREATOR  where FK_PER = V_COL900 AND FK_STUDY = V_STUDYID;
			END IF;
        END IF;
		
		--UPDATE ER_PATSTUDYSTAT SET PATSTUDYSTAT_REASON= V_REASON_NUM, LAST_MODIFIED_BY = V_CREATOR WHERE FK_PER = V_COL900 AND FK_STUDY = V_STUDYID AND FK_CODELST_STAT = V_WITHRWN_STATUS;
 	   commit; 
	    
    END LOOP;
CLOSE C_PATIENT_RECORDS;

--Track Patch where DB version major is 403
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,403,25,'25_01_SUB_MIGRT.sql',sysdate,'v9.3.0 #712');

V_TRIG := 'ALTER TRIGGER EPAT.PERSON_AU0 ENABLE';
EXECUTE IMMEDIATE V_TRIG;

COMMIT;

DBMS_OUTPUT.PUT_LINE('Migration Successful');

EXCEPTION WHEN OTHERS then
	DBMS_OUTPUT.PUT_LINE('MIGRATION FAILED AT COL900='||V_COL900||' AND ID='||V_STUDYID||' DUE TO:'||' '||SQLERRM);
	EXECUTE IMMEDIATE V_TRIG_1;

COMMIT;
		
END;
/