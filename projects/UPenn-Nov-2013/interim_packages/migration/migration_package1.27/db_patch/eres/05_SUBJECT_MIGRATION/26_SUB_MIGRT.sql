SET SERVEROUTPUT ON;

DECLARE
V_STATUS_DATE    	DATE;
V_STAT            	VARCHAR2(4000);
V_STAT_RAW         	VARCHAR2(4000);
V_PK_FORMSLINEAR    NUMBER;
V_ID    			NUMBER;
V_COL900    		NUMBER;
V_CREATED_ON    	DATE;
V_CREATOR    		NUMBER;
V_ENROLLED    		NUMBER;
V_PATFAC    		NUMBER;
V_PK_CODELST    	NUMBER;
V_PK_PATSTUDYSTAT   NUMBER;
V_STD_DTH_REL		NUMBER;
V_REASON			NUMBER;
BEGIN

SELECT NVL((SELECT PK_USER  FROM ER_USER WHERE UPPER(USR_FIRSTNAME) = UPPER('DATA') AND UPPER(USR_LASTNAME) = UPPER('MIGRATION') AND USR_TYPE= 'N'),0) INTO V_CREATOR FROM DUAL;

SELECT PK_CODELST INTO V_ENROLLED FROM ER_CODELST WHERE UPPER(CODELST_TYPE) = UPPER('patStatus') AND UPPER(CODELST_DESC)=UPPER('Enrolled');

FOR i IN (select distinct STAT_RAW,STAT, PK_FORMSLINEAR, ID, COL900, CREATED_ON from (select a.CREATED_ON as CREATED_ON,ID, COL900,PK_FORMSLINEAR,case trim(REGEXP_SUBSTR(COL21, '[^[]+')||REGEXP_SUBSTR(COL22, '[^[]+')||REGEXP_SUBSTR(COL23, '[^[]+')||REGEXP_SUBSTR(COL26, '[^[]+')||REGEXP_SUBSTR(COL30, '[^[]+')) when    'Completed StudyDisease Progression'    then    'Completed Study'
when    'Completed StudyPI Recommended'    then    'Completed Study'
when    'Completed StudySAE/AE'    then    'Completed Study'
when    'Enrolled'    then    'Completed Study'
when    'EnrolledActive'    then    'Completed Study'
when    'Unknown'    then    'Completed Study'
when    'Deceased'    then    'Deceased'
when    'Deceased'    then    'Deceased'
when    'DeceasedDeathFrom Study Disease'    then    'Deceased - From Study Disease'
when    'DeceasedDeathFrom Study Drug Toxicity'    then    'Deceased - From Study Drug Toxicity'
when    'DeceasedDisease ProgressionFrom Study Disease'    then    'Deceased - From Study Disease'
when    'DeceasedFrom Study Disease'    then    'Deceased - From Study Disease'
when    'DeceasedFrom Study Drug Toxicity'    then    'Deceased - From Study Drug Toxicity'
when    'DeceasedPI Recommended'    then    'Deceased'
when    'DeceasedPI RecommendedFrom Study Disease'    then    'Deceased'
when    'DeceasedPI RecommendedFrom Study Disease'    then    'Deceased - From Study Disease'
when    'DeceasedSAE/AE'    then    'Deceased'
when    'DeceasedSAE/AEFrom Study Disease'    then    'Deceased - From Study Disease'
when    'DeceasedSubject Decision'    then    'Deceased'
when    'DeceasedSubject DecisionFrom Study Disease'    then    'Deceased - From Study Disease'
when    'Enrolled'    then    'Deceased - From Study Disease'
when    'DeceasedDisease Progression'    then    'Deceased'
when    'DeceasedDeathNonRelated'    then    'Deceased - Non-Related'
when    'DeceasedDisease ProgressionNonRelated'    then    'Deceased - Non-Related'
when    'DeceasedNonRelated'    then    'Deceased - Non-Related'
when    'DeceasedSAE/AENonRelated'    then    'Deceased - Non-Related'
when    'Enrolled'    then    'Enrolled'
when    'Enrolled'    then    'Enrolled'
when    'Unknown'    then    'Enrolled'
when    'EnrolledActive'    then    'Enrolled - Active'
when    'Enrolled'    then    'Enrolled - Active Treatment'
when    'EnrolledActive Treatment'    then    'Enrolled - Active Treatment'
when    'In Protocol Followup'    then    'In Protocol Follow-up'
when    'In Protocol FollowupDisease Progression'    then    'In Protocol Follow-up'
when    'EnrolledActive Treatment'    then    'Long-term Follow-up'
when    'Lost to Followup'    then    'Lost to Follow-up'
when    'Lost to FollowupSubject Decision'    then    'Lost to Follow-up'
when    'EnrolledActive TreatmentSubject Decision'    then    'Withdrawn'
when    'WithdrawnFrom Protocol'    then    'Withdrawn - From Protocol'
when    'WithdrawnFrom ProtocolDeath'    then    'Withdrawn - From Protocol'
when    'WithdrawnFrom ProtocolDisease Progression'    then    'Withdrawn - From Protocol'
when    'WithdrawnFrom ProtocolPI Recommended'    then    'Withdrawn - From Protocol'
when    'WithdrawnFrom ProtocolSAE/AE'    then    'Withdrawn - From Protocol'
when    'Enrolled'    then    'Withdrawn - From Protocol'
when    'WithdrawnFrom ProtocolSubject Decision'    then    'Withdrawn - From Protocol'
when    'WithdrawnFrom ProtocolSubject Relocated'    then    'Withdrawn - From Protocol'
when    'WithdrawnFrom Treatment'    then    'Withdrawn - From Treatment'
when    'WithdrawnFrom TreatmentDisease Progression'    then    'Withdrawn - From Treatment'
when    'WithdrawnFrom TreatmentPI Recommended'    then    'Withdrawn - From Treatment'
when    'WithdrawnFrom TreatmentSAE/AE'    then    'Withdrawn - From Treatment'
when    'WithdrawnFrom TreatmentSubject Decision'    then    'Withdrawn - From Treatment'
when    'WithdrawnFrom TreatmentSubject Relocated'    then    'Withdrawn - From Treatment'
else '0' end as STAT, trim(REGEXP_SUBSTR(COL21, '[^[]+')||REGEXP_SUBSTR(COL22, '[^[]+')||REGEXP_SUBSTR(COL23, '[^[]+')||REGEXP_SUBSTR(COL26, '[^[]+')||REGEXP_SUBSTR(COL30, '[^[]+')) as STAT_RAW
from er_formslinear a,er_patprot b where fk_form=295 AND COL900 IS NOT NULL AND ID IS NOT NULL  and col900=b.fk_per and id=b.fk_study))

LOOP

V_STATUS_DATE:= NULL;
V_PK_CODELST:= 0;
V_PATFAC:= 0;
V_STAT:= i.STAT;
V_PK_FORMSLINEAR:= i.PK_FORMSLINEAR;
V_ID:= i.ID;
V_COL900:= i.COL900;
V_CREATED_ON := i.CREATED_ON;
V_STAT_RAW := i.STAT_RAW;
--V_LAST_MODIFIED_DATE:= i.LAST_MODIFIED_DATE

-----------------------------------------
IF V_STAT_RAW    = 'DeceasedDeathFrom Study Disease' THEN 

SELECT PK_CODELST INTO V_STD_DTH_REL FROM ER_CODELST WHERE CODELST_TYPE = 'ptst_dth_stdrel' AND CODELST_DESC= 'Disease Progression';

ELSIF V_STAT_RAW = 'DeceasedDeathFrom Study Drug Toxicity' THEN 

SELECT PK_CODELST INTO V_STD_DTH_REL FROM ER_CODELST WHERE CODELST_TYPE = 'ptst_dth_stdrel' AND CODELST_DESC= 'Related';

ELSIF V_STAT_RAW = 'DeceasedDisease ProgressionFrom Study Disease' THEN 

SELECT PK_CODELST INTO V_STD_DTH_REL FROM ER_CODELST WHERE CODELST_TYPE = 'ptst_dth_stdrel' AND CODELST_DESC= 'Disease Progression';

ELSIF V_STAT_RAW = 'DeceasedFrom Study Disease' THEN 

SELECT PK_CODELST INTO V_STD_DTH_REL FROM ER_CODELST WHERE CODELST_TYPE = 'ptst_dth_stdrel' AND CODELST_DESC= 'Disease Progression';

ELSIF V_STAT_RAW = 'DeceasedFrom Study Drug Toxicity' THEN 

SELECT PK_CODELST INTO V_STD_DTH_REL FROM ER_CODELST WHERE CODELST_TYPE = 'ptst_dth_stdrel' AND CODELST_DESC= 'Related';

--ELSIF V_STAT_RAW = 'DeceasedPI RecommendedFrom Study Disease' THEN 

--SELECT PK_CODELST INTO V_STD_DTH_REL FROM ER_CODELST WHERE CODELST_TYPE = 'ptst_dth_stdrel' AND CODELST_DESC= 'Disease Progression';

ELSIF V_STAT_RAW = 'DeceasedPI RecommendedFrom Study Disease' THEN 

SELECT PK_CODELST INTO V_STD_DTH_REL FROM ER_CODELST WHERE CODELST_TYPE = 'ptst_dth_stdrel' AND CODELST_DESC= 'Disease Progression';

ELSIF V_STAT_RAW = 'DeceasedSAE/AEFrom Study Disease' THEN 

SELECT PK_CODELST INTO V_STD_DTH_REL FROM ER_CODELST WHERE CODELST_TYPE = 'ptst_dth_stdrel' AND CODELST_DESC= 'Disease Progression';

ELSIF V_STAT_RAW = 'DeceasedSubject DecisionFrom Study Disease' THEN 

SELECT PK_CODELST INTO V_STD_DTH_REL FROM ER_CODELST WHERE CODELST_TYPE = 'ptst_dth_stdrel' AND CODELST_DESC= 'Disease Progression';

ELSIF V_STAT_RAW = 'Enrolled' THEN 

SELECT PK_CODELST INTO V_STD_DTH_REL FROM ER_CODELST WHERE CODELST_TYPE = 'ptst_dth_stdrel' AND CODELST_DESC= 'Disease Progression';

ELSIF V_STAT_RAW = 'DeceasedDisease Progression' THEN 

SELECT PK_CODELST INTO V_STD_DTH_REL FROM ER_CODELST WHERE CODELST_TYPE = 'ptst_dth_stdrel' AND CODELST_DESC= 'Disease Progression';

ELSIF V_STAT_RAW = 'DeceasedDeathNon-Related' THEN 

SELECT PK_CODELST INTO V_STD_DTH_REL FROM ER_CODELST WHERE CODELST_TYPE = 'ptst_dth_stdrel' AND CODELST_DESC= 'Unrelated';

ELSIF V_STAT_RAW = 'DeceasedDisease ProgressionNon-Related' THEN 

SELECT PK_CODELST INTO V_STD_DTH_REL FROM ER_CODELST WHERE CODELST_TYPE = 'ptst_dth_stdrel' AND CODELST_DESC= 'Unrelated';

ELSIF V_STAT_RAW = 'DeceasedNon-Related' THEN 

SELECT PK_CODELST INTO V_STD_DTH_REL FROM ER_CODELST WHERE CODELST_TYPE = 'ptst_dth_stdrel' AND CODELST_DESC= 'Unrelated';

ELSIF V_STAT_RAW = 'DeceasedSAE/AENon-Related' THEN 

SELECT PK_CODELST INTO V_STD_DTH_REL FROM ER_CODELST WHERE CODELST_TYPE = 'ptst_dth_stdrel' AND CODELST_DESC= 'Unrelated';

ELSE
V_STD_DTH_REL:= 0;
END IF;

-----------------------------------------




------------------------------------------
IF V_STAT_RAW = 'WithdrawnFrom ProtocolDisease Progression' THEN

SELECT NVL((SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE = 'withdrawnP' AND CODELST_DESC='Disease Progression'), 0) INTO V_REASON FROM DUAL;

ELSIF V_STAT_RAW = 'WithdrawnFrom ProtocolPI Recommended' THEN

SELECT NVL((SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE = 'withdrawnP' AND CODELST_DESC='PI Decision'), 0) INTO V_REASON FROM DUAL;

ELSIF V_STAT_RAW = 'WithdrawnFrom ProtocolSAE/AE' THEN

SELECT NVL((SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE = 'withdrawnP' AND CODELST_DESC='SAE/AE'), 0) INTO V_REASON FROM DUAL;

ELSIF V_STAT_RAW = 'Enrolled' THEN

SELECT NVL((SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE = 'withdrawnP' AND CODELST_DESC='Subject Decision'), 0) INTO V_REASON FROM DUAL;

ELSIF V_STAT_RAW = 'WithdrawnFrom ProtocolSubject Decision' THEN

SELECT NVL((SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE = 'withdrawnP' AND CODELST_DESC='Subject Decision'), 0) INTO V_REASON FROM DUAL;

ELSIF V_STAT_RAW = 'WithdrawnFrom ProtocolSubject Relocated' THEN

SELECT NVL((SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE = 'withdrawnP' AND CODELST_DESC='Subject Relocated'), 0) INTO V_REASON FROM DUAL;

ELSIF V_STAT_RAW = 'WithdrawnFrom TreatmentDisease Progression' THEN

SELECT NVL((SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE = 'withdrawnT' AND CODELST_DESC='Disease Progression'), 0) INTO V_REASON FROM DUAL; 

ELSIF V_STAT_RAW = 'WithdrawnFrom TreatmentPI Recommended' THEN

SELECT NVL((SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE = 'withdrawnT' AND CODELST_DESC='PI Decision'), 0) INTO V_REASON FROM DUAL;

ELSIF V_STAT_RAW = 'WithdrawnFrom TreatmentSAE/AE' THEN

SELECT NVL((SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE = 'withdrawnT' AND CODELST_DESC='SAE/AE'), 0) INTO V_REASON FROM DUAL;

ELSIF V_STAT_RAW = 'WithdrawnFrom TreatmentSubject Decision' THEN

SELECT NVL((SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE = 'withdrawnT' AND CODELST_DESC='Subject Decision'), 0) INTO V_REASON FROM DUAL;

ELSIF V_STAT_RAW = 'WithdrawnFrom TreatmentSubject Relocated' THEN

SELECT NVL((SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE = 'withdrawnT' AND CODELST_DESC='Subject Relocated'), 0) INTO V_REASON FROM DUAL;

ELSE
V_REASON := 0;
END IF;
 



------------------------------------------




IF V_STAT= 'Completed Study' THEN 

SELECT CASE WHEN to_date(COL28, 'mm/dd/yyyy') IS  NULL THEN CREATED_ON ELSE to_date(COL28, 'mm/dd/yyyy') END INTO V_STATUS_DATE FROM ER_FORMSLINEAR WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR;

ELSIF V_STAT= 'Deceased' THEN 

SELECT CASE WHEN to_date(COL29, 'mm/dd/yyyy') IS  NULL THEN CREATED_ON ELSE to_date(COL29, 'mm/dd/yyyy') END INTO V_STATUS_DATE FROM ER_FORMSLINEAR WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR;

ELSIF V_STAT= 'Deceased - From Study Disease' THEN 

SELECT CASE WHEN to_date(COL29, 'mm/dd/yyyy') IS  NULL THEN CREATED_ON ELSE to_date(COL29, 'mm/dd/yyyy') END INTO V_STATUS_DATE FROM ER_FORMSLINEAR WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR;

ELSIF V_STAT= 'Deceased - From Study Drug Toxicity' THEN 

SELECT CASE WHEN to_date(COL29, 'mm/dd/yyyy') IS  NULL THEN CREATED_ON ELSE to_date(COL29, 'mm/dd/yyyy') END INTO V_STATUS_DATE FROM ER_FORMSLINEAR WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR;

ELSIF V_STAT= 'Deceased - Non-Related' THEN 

SELECT CASE WHEN to_date(COL29, 'mm/dd/yyyy') IS  NULL THEN CREATED_ON ELSE to_date(COL29, 'mm/dd/yyyy') END INTO V_STATUS_DATE FROM ER_FORMSLINEAR WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR;

ELSIF V_STAT= 'Enrolled' THEN 

SELECT CASE WHEN to_date(COL14, 'mm/dd/yyyy') IS  NULL THEN CREATED_ON ELSE to_date(COL14, 'mm/dd/yyyy') END INTO V_STATUS_DATE FROM ER_FORMSLINEAR WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR;

ELSIF V_STAT= 'Enrolled - Active' THEN 

SELECT CASE WHEN to_date(COL14, 'mm/dd/yyyy') IS  NULL THEN CREATED_ON ELSE to_date(COL14, 'mm/dd/yyyy') END INTO V_STATUS_DATE FROM ER_FORMSLINEAR WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR;

ELSIF V_STAT= 'Enrolled - Active Treatment' THEN 

SELECT NVL((SELECT CASE WHEN to_date(COL15, 'mm/dd/yyyy') IS  NULL THEN to_date(COL14, 'mm/dd/yyyy') ELSE to_date(COL15, 'mm/dd/yyyy') END FROM ER_FORMSLINEAR WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR), NULL) INTO V_STATUS_DATE FROM DUAL;
    IF V_STATUS_DATE IS NULL THEN
        SELECT CREATED_ON INTO V_STATUS_DATE FROM ER_FORMSLINEAR WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR;
    ELSE
        NULL;
    END IF;

ELSIF V_STAT= 'In Protocol Follow-up' THEN 

SELECT CASE WHEN to_date(COL27, 'mm/dd/yyyy') IS  NULL THEN CREATED_ON ELSE to_date(COL27, 'mm/dd/yyyy') END INTO V_STATUS_DATE FROM ER_FORMSLINEAR WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR;

ELSIF V_STAT= 'Long-term Follow-up' THEN 

SELECT CASE WHEN to_date(COL27, 'mm/dd/yyyy') IS  NULL THEN CREATED_ON ELSE to_date(COL27, 'mm/dd/yyyy') END INTO V_STATUS_DATE FROM ER_FORMSLINEAR WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR;

ELSIF V_STAT= 'Lost to Follow-up' THEN 

SELECT CASE WHEN to_date(COL32, 'mm/dd/yyyy') IS  NULL THEN CREATED_ON ELSE to_date(COL32, 'mm/dd/yyyy') END INTO V_STATUS_DATE FROM ER_FORMSLINEAR WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR;

ELSIF V_STAT= 'Screening' THEN     

SELECT CREATED_ON INTO V_STATUS_DATE FROM ER_FORMSLINEAR WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR;

ELSIF V_STAT= 'Transferred' THEN 

SELECT CASE WHEN to_date(COL31, 'mm/dd/yyyy') IS  NULL THEN CREATED_ON ELSE to_date(COL31, 'mm/dd/yyyy') END INTO V_STATUS_DATE FROM ER_FORMSLINEAR WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR;

ELSIF V_STAT= 'Unknown' THEN 

SELECT CREATED_ON INTO V_STATUS_DATE FROM ER_FORMSLINEAR WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR;

ELSIF V_STAT= 'Withdrawn' THEN 

SELECT NVL((SELECT CASE WHEN to_date(COL25, 'mm/dd/yyyy') IS  NULL THEN to_date(COL24, 'mm/dd/yyyy') ELSE to_date(COL25, 'mm/dd/yyyy') END FROM ER_FORMSLINEAR WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR), NULL) INTO V_STATUS_DATE FROM DUAL; 
    IF V_STATUS_DATE IS NULL THEN
        SELECT CREATED_ON INTO V_STATUS_DATE FROM ER_FORMSLINEAR WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR;
    ELSE
        NULL;
    END IF;

ELSIF V_STAT= 'Withdrawn - From Protocol' THEN 

SELECT CASE WHEN to_date(COL25, 'mm/dd/yyyy') IS  NULL THEN CREATED_ON ELSE to_date(COL25, 'mm/dd/yyyy') END INTO V_STATUS_DATE FROM ER_FORMSLINEAR WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR;

ELSIF V_STAT= 'Withdrawn - From Protocol - Disease Progression' THEN 

SELECT CASE WHEN to_date(COL25, 'mm/dd/yyyy') IS  NULL THEN CREATED_ON ELSE to_date(COL25, 'mm/dd/yyyy') END INTO V_STATUS_DATE FROM ER_FORMSLINEAR WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR;

ELSIF V_STAT= 'Withdrawn - From Protocol - PI Recommended' THEN 

SELECT CASE WHEN to_date(COL25, 'mm/dd/yyyy') IS  NULL THEN CREATED_ON ELSE to_date(COL25, 'mm/dd/yyyy') END INTO V_STATUS_DATE FROM ER_FORMSLINEAR WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR;

ELSIF V_STAT= 'Withdrawn - From Protocol - SAE/AE' THEN 

SELECT CASE WHEN to_date(COL25, 'mm/dd/yyyy') IS  NULL THEN CREATED_ON ELSE to_date(COL25, 'mm/dd/yyyy') END INTO V_STATUS_DATE FROM ER_FORMSLINEAR WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR;

ELSIF V_STAT= 'Withdrawn - From Protocol - Subject Decision' THEN 

SELECT CASE WHEN to_date(COL25, 'mm/dd/yyyy') IS  NULL THEN CREATED_ON ELSE to_date(COL25, 'mm/dd/yyyy') END INTO V_STATUS_DATE FROM ER_FORMSLINEAR WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR;

ELSIF V_STAT= 'Withdrawn - From Protocol - Subject Relocated' THEN 

SELECT CASE WHEN to_date(COL25, 'mm/dd/yyyy') IS  NULL THEN CREATED_ON ELSE to_date(COL25, 'mm/dd/yyyy') END INTO V_STATUS_DATE FROM ER_FORMSLINEAR WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR;

ELSIF V_STAT= 'Withdrawn - From Treatment' THEN 

SELECT CASE WHEN to_date(COL24, 'mm/dd/yyyy') IS  NULL THEN CREATED_ON ELSE to_date(COL24, 'mm/dd/yyyy') END INTO V_STATUS_DATE FROM ER_FORMSLINEAR WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR;

ELSIF V_STAT= 'Withdrawn - From Treatment - Disease Progression' THEN 

SELECT CASE WHEN to_date(COL24, 'mm/dd/yyyy') IS  NULL THEN CREATED_ON ELSE to_date(COL24, 'mm/dd/yyyy') END INTO V_STATUS_DATE FROM ER_FORMSLINEAR WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR;

ELSIF V_STAT= 'Withdrawn - From Treatment - PI Recommended' THEN 

SELECT CASE WHEN to_date(COL24, 'mm/dd/yyyy') IS  NULL THEN CREATED_ON ELSE to_date(COL24, 'mm/dd/yyyy') END INTO V_STATUS_DATE FROM ER_FORMSLINEAR WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR;

ELSIF V_STAT= 'Withdrawn - From Treatment - SAE/AE' THEN 

SELECT CASE WHEN to_date(COL24, 'mm/dd/yyyy') IS  NULL THEN CREATED_ON ELSE to_date(COL24, 'mm/dd/yyyy') END INTO V_STATUS_DATE FROM ER_FORMSLINEAR WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR;

ELSIF V_STAT= 'Withdrawn - From Treatment - Subject Decision' THEN 

SELECT CASE WHEN to_date(COL24, 'mm/dd/yyyy') IS  NULL THEN CREATED_ON ELSE to_date(COL24, 'mm/dd/yyyy') END INTO V_STATUS_DATE FROM ER_FORMSLINEAR WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR;

ELSIF V_STAT= 'Withdrawn - From Treatment - Subject Relocated' THEN 

SELECT CASE WHEN to_date(COL24, 'mm/dd/yyyy') IS  NULL THEN CREATED_ON ELSE to_date(COL24, 'mm/dd/yyyy') END INTO V_STATUS_DATE FROM ER_FORMSLINEAR WHERE PK_FORMSLINEAR = V_PK_FORMSLINEAR;

ELSE
    NULL;
END IF;

SELECT COUNT(*) INTO V_PATFAC FROM ER_PATSTUDYSTAT WHERE FK_STUDY= V_ID AND FK_PER= V_COL900;

SELECT NVL((SELECT PK_CODELST FROM ER_CODELST WHERE UPPER(CODELST_TYPE) = UPPER('patStatus') AND UPPER(CODELST_DESC)=UPPER(V_STAT)), 0) INTO V_PK_CODELST FROM DUAL;

IF V_PATFAC = 0 THEN

    IF V_PK_CODELST <> 0 THEN

	IF V_REASON = 0 THEN
	
    INSERT INTO ER_PATSTUDYSTAT (PK_PATSTUDYSTAT, FK_CODELST_STAT, FK_PER, FK_STUDY, PATSTUDYSTAT_DATE, CREATOR, CREATED_ON, CURRENT_STAT)
    VALUES (SEQ_ER_PATSTUDYSTAT.NEXTVAL, V_PK_CODELST, V_COL900, V_ID, V_STATUS_DATE, V_CREATOR, SYSDATE, 1);
	
	ELSE
	
	INSERT INTO ER_PATSTUDYSTAT (PK_PATSTUDYSTAT, FK_CODELST_STAT, FK_PER, FK_STUDY, PATSTUDYSTAT_DATE, CREATOR, CREATED_ON, CURRENT_STAT,PATSTUDYSTAT_REASON)
    VALUES (SEQ_ER_PATSTUDYSTAT.NEXTVAL, V_PK_CODELST, V_COL900, V_ID, V_STATUS_DATE, V_CREATOR, SYSDATE, 1, V_REASON);
	
	END IF;
    
    ELSE
	
	IF V_REASON = 0 THEN
    
    INSERT INTO ER_PATSTUDYSTAT (PK_PATSTUDYSTAT, FK_CODELST_STAT, FK_PER, FK_STUDY, PATSTUDYSTAT_DATE, CREATOR, CREATED_ON, CURRENT_STAT)
    VALUES (SEQ_ER_PATSTUDYSTAT.NEXTVAL, V_ENROLLED, V_COL900, V_ID, V_CREATED_ON, V_CREATOR, SYSDATE, 1);
	
	ELSE
	
	INSERT INTO ER_PATSTUDYSTAT (PK_PATSTUDYSTAT, FK_CODELST_STAT, FK_PER, FK_STUDY, PATSTUDYSTAT_DATE, CREATOR, CREATED_ON, CURRENT_STAT, PATSTUDYSTAT_REASON)
    VALUES (SEQ_ER_PATSTUDYSTAT.NEXTVAL, V_ENROLLED, V_COL900, V_ID, V_CREATED_ON, V_CREATOR, SYSDATE, 1, V_REASON);
	
	END IF;
    
    END IF;

ELSIF V_PATFAC = 1 AND V_PK_CODELST <> 0 THEN

IF V_REASON = 0 THEN

UPDATE ER_PATSTUDYSTAT SET FK_CODELST_STAT= V_PK_CODELST, PATSTUDYSTAT_DATE = COALESCE(PATSTUDYSTAT_DATE,V_STATUS_DATE), LAST_MODIFIED_BY = V_CREATOR WHERE FK_PER = V_COL900 AND FK_STUDY = V_ID;

ELSE

UPDATE ER_PATSTUDYSTAT SET FK_CODELST_STAT= V_PK_CODELST, PATSTUDYSTAT_DATE = COALESCE(PATSTUDYSTAT_DATE,V_STATUS_DATE), LAST_MODIFIED_BY = V_CREATOR, PATSTUDYSTAT_REASON = V_REASON WHERE FK_PER = V_COL900 AND FK_STUDY = V_ID;

END IF;

ELSIF V_PATFAC > 1 AND V_PK_CODELST <> 0 THEN

SELECT NVL((SELECT MAX(PK_PATSTUDYSTAT) FROM ER_PATSTUDYSTAT WHERE FK_STUDY = V_ID AND FK_PER = V_COL900 AND CURRENT_STAT = 1), NULL) INTO V_PK_PATSTUDYSTAT FROM DUAL;
    IF V_PK_PATSTUDYSTAT IS NULL THEN 
        SELECT MAX(PK_PATSTUDYSTAT) INTO V_PK_PATSTUDYSTAT FROM ER_PATSTUDYSTAT WHERE FK_STUDY = V_ID AND FK_PER = V_COL900;
    ELSE
        NULL;
    END IF;

IF V_REASON = 0 THEN	
	
UPDATE ER_PATSTUDYSTAT SET FK_CODELST_STAT= V_PK_CODELST, PATSTUDYSTAT_DATE = COALESCE(PATSTUDYSTAT_DATE,V_STATUS_DATE) , LAST_MODIFIED_BY = V_CREATOR WHERE PK_PATSTUDYSTAT = V_PK_PATSTUDYSTAT;

ELSE

UPDATE ER_PATSTUDYSTAT SET FK_CODELST_STAT= V_PK_CODELST, PATSTUDYSTAT_DATE = COALESCE(PATSTUDYSTAT_DATE,V_STATUS_DATE) , LAST_MODIFIED_BY = V_CREATOR, PATSTUDYSTAT_REASON = V_REASON WHERE PK_PATSTUDYSTAT = V_PK_PATSTUDYSTAT;

END IF;

END IF;

IF V_STD_DTH_REL <> 0 THEN
	UPDATE ER_PATPROT SET FK_CODELST_PTST_DTH_STDREL = V_STD_DTH_REL WHERE FK_STUDY = V_ID AND FK_PER = V_COL900;
	
ELSE
	NULL;
END IF;

END LOOP;

--Track Patch where DB version major is 403
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,403,26,'26_SUB_MIGRT.sql',sysdate,'v9.3.0 #712');

COMMIT;

EXCEPTION WHEN OTHERS THEN
ROLLBACK;
DBMS_OUTPUT.PUT_LINE (SQLERRM);

END;
/