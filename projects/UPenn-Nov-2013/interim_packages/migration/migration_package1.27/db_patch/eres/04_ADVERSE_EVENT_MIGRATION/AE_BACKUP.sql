delete from esch.sch_adverseve where creator = (select pk_user from er_user where usr_firstname = 'Data' and usr_lastname = 'Migration' and usr_type = 'N');

delete from eres.er_moredetails where creator = (select pk_user from er_user where usr_firstname = 'Data' and usr_lastname = 'Migration' and usr_type = 'N') and MD_MODNAME = 'advtype';

--Track Patch where DB version major is 403
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,403,24,'24_AE_MIGRT.sql',sysdate,'v9.3.0 #712');

commit;
/