CREATE TABLE ERES.DM_USERSFORMDATA AS SELECT DISTINCT TRIM(FIRST_NAME) as FIRST_NAME, TRIM(LAST_NAME) as LAST_NAME FROM DM_USERSFORMDATATEMP WHERE FIRST_NAME IS NOT NULL AND LAST_NAME IS NOT NULL;
ALTER table ERES.DM_USERSFORMDATA ADD (USER_STATUS NUMBER, REMARKS VARCHAR2(4000));

--Track Patch where DB version major is 403
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,403,2,'02_USR_MIGRT.sql',sysdate,'v9.3.0 #712');
COMMIT;
/