/////*********This readMe is specific to Migration_Package**********////


Before running the migration scripts, copy the file ExpToFormDao.class from this _JavaClass folder into the following directory on the eResearch server, overwriting the existing file with the same name:

${ERESEARCH_JBOSS}/server/eresearch/deploy/velos.ear/velos-ejb-eres.jar/com/velos/eres/business/common/

Overwrite "patienttabs.jsp" with the same file at following location:
 
${ERESEARCH_JBOSS}/server/eresearch/deploy/velos.ear/velos.war/jsp

where ${ERESEARCH_JBOSS} is the root of the eResearch JBoss (e.g. it could be /usr/local/eResearch_jboss510). Then delete these two JBoss-generated temporary directories.

${ERESEARCH_JBOSS}/server/eresearch/tmp
${ERESEARCH_JBOSS}/server/eresearch/work

Please confirm JAVA_OPTS in ${ERESEARCH_JBOSS}/bin/run.conf

JAVA_OPTS = "Xms512m -Xmx2048m -XX:MaxPermSize=1024m  --- if these values are less than the mentioned values then increase the memory at given location.

And then RESTART eResearch.

*********************************************************************************************************************************************************************

Please execute patch ''01_migrationPrepPatch.sql'' before migration process as this script will check following field value availability:

1) Primary Purpose: Ancillary
2) Primary Purpose: Early Detection
3) Repair functionality of More AE Details field i.e. Report Type and Duration.

EXECUTION STEPS:

Open SQL PLUS terminal and log in as eres/eres123. After connected, every script starting from should be executed SEQUENTIALLY AND INDIVIDUALLY as following:

1) Write in terminal '' @/(directory where you have stored the script)/MIGRATION_v1.27/01_migrationPrepPatch.sql; ''

Note: Double course is not the part of command. Only from ''@'' to '';'' should be write. For e.g. -->  SQLPLUS>@/home/MIGRATION_v1.27/01_migrationPrepPatch.sql;

2) After completed same execution, please follow readme in following hierarchy:
	2.1) Excute User Migration Scripts
	2.2) Execute Org/Grp migration Scripts
	2.3) Execute Protocol Migration Scripts
	2.4) Execute AE migration Scripts
	2.5) Execute Subject Migration Scripts

3) After execution of Subject Migration scripts, go to link ''(your domain name).com/velos/jsp/exportForm.jsp'' and click submit. When it shows result ''STARTING EXPORT EXPORT FINISHED PHEW!!!'' , it means More Subject Details form has been migrated.

NOTE# IF MORE SUBJECT DETAIL MIGRATION FAILED FOR ANY REASON THEN PLEASE EXECUTE SUB_BACKUP.sql SQL SCRIPT LOCATED IN ''/MIGRATION_v1.27/SUBJECT_MIGRATION/'' AS IT WILL RESTORE MORE SUBJECT FORM RESPONSES. YOU ONLY NEED TO RUN ''26_SUB_MIGRT.sql'' AND THEN REPEAT STEP ''5.)'' IN EXECUTION STEPS.

