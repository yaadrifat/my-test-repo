set define off; 

UPDATE ER_CTRLTAB SET CTRL_VALUE = 'v9.2.0 #693.01-UP1' 
where CTRL_KEY = 'app_version' ; 

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,292,0,'00_er_version.sql',sysdate,'v9.2.0 #693.01-UP1');

commit;

