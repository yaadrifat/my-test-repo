set define off;

declare
v_exists NUMBER := 0;
v_fk_account NUMBER := [fk_account];
v_pk_pagecustom NUMBER := 0;
begin
	
	select count(pk_pagecustom) into v_exists from er_pagecustom where PAGECUSTOM_PAGE = 'patient' and fk_account = v_fk_account;
	if (v_exists > 0) then
		select pk_pagecustom into v_pk_pagecustom from er_pagecustom where PAGECUSTOM_PAGE = 'patient' and fk_account = v_fk_account;

		delete from er_pagecustomflds where PAGECUSTOMFLDS_FIELD = 'pnotes' and fk_pagecustom = v_pk_pagecustom;
		commit;
	end if;
	
	select count(pk_pagecustom) into v_exists from er_pagecustom where PAGECUSTOM_PAGE = 'patstudystatus' and fk_account = v_fk_account;
	if (v_exists > 0) then
		select pk_pagecustom into v_pk_pagecustom from er_pagecustom where PAGECUSTOM_PAGE = 'patstudystatus' and fk_account = v_fk_account;

		update er_pagecustomflds set PAGECUSTOMFLDS_ATTRIBUTE = null, PAGECUSTOMFLDS_MANDATORY = '1', 
		PAGECUSTOMFLDS_LABEL = null where PAGECUSTOMFLDS_FIELD = 'statusdate';
		commit;

		delete from er_pagecustomflds where PAGECUSTOMFLDS_FIELD = 'reason' and fk_pagecustom = v_pk_pagecustom;
		commit;
	end if;
	
	select count(pk_pagecustom) into v_exists from er_pagecustom where PAGECUSTOM_PAGE = 'studysummary' and fk_account = v_fk_account;
	if (v_exists > 0) then
		select pk_pagecustom into v_pk_pagecustom from er_pagecustom where PAGECUSTOM_PAGE = 'studysummary' and fk_account = v_fk_account;
		
		update er_pagecustomflds set PAGECUSTOMFLDS_ATTRIBUTE = '0', PAGECUSTOMFLDS_LABEL = null where PAGECUSTOMFLDS_FIELD = 'studytype';
		commit;
	end if;
end;
/