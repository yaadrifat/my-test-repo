set define off;

update er_study set FK_CODELST_TYPE = null where FK_CODELST_TYPE in (
select pk_codelst FROM ER_CODELST WHERE CODELST_TYPE = 'study_type' AND CODELST_SUBTYP in('healthSResearch', 'basicScienceTx', 
'treatment', 'supportive', 'retroAnalysis'));

commit;