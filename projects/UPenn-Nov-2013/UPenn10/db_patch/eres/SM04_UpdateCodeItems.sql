set define off;

UPDATE ER_CODELST SET CODELST_HIDE='Y' WHERE CODELST_TYPE = 'study_type' AND CODELST_SUBTYP='ancillary';

UPDATE ER_CODELST SET CODELST_DESC='Other' WHERE CODELST_TYPE = 'study_type' AND CODELST_SUBTYP='study';

commit;

DELETE FROM ER_CODELST WHERE CODELST_TYPE = 'study_type' AND CODELST_SUBTYP = 'healthSResearch';
DELETE FROM ER_CODELST WHERE CODELST_TYPE = 'study_type' AND CODELST_SUBTYP = 'basicScienceTx';
DELETE FROM ER_CODELST WHERE CODELST_TYPE = 'study_type' AND CODELST_SUBTYP = 'treatment';
DELETE FROM ER_CODELST WHERE CODELST_TYPE = 'study_type' AND CODELST_SUBTYP = 'supportive';
DELETE FROM ER_CODELST WHERE CODELST_TYPE = 'study_type' AND CODELST_SUBTYP = 'retroAnalysis';

commit;

UPDATE ER_CODELST SET  CODELST_DESC='Others' WHERE CODELST_TYPE = 'study_type' AND CODELST_SUBTYP='study';
UPDATE ER_CODELST SET CODELST_HIDE='N' WHERE CODELST_TYPE = 'study_type' AND CODELST_SUBTYP='ancillary';
UPDATE ER_CODELST SET  CODELST_HIDE='N' WHERE CODELST_TYPE = 'study_type' AND CODELST_SUBTYP='behavioral';
UPDATE ER_CODELST SET  CODELST_HIDE='N' WHERE CODELST_TYPE = 'study_type' AND CODELST_SUBTYP='companion';
UPDATE ER_CODELST SET  CODELST_HIDE='N' WHERE CODELST_TYPE = 'study_type' AND CODELST_SUBTYP='compassionUse';
UPDATE ER_CODELST SET  CODELST_HIDE='N' WHERE CODELST_TYPE = 'study_type' AND CODELST_SUBTYP='correlative';
UPDATE ER_CODELST SET  CODELST_HIDE='N' WHERE CODELST_TYPE = 'study_type' AND CODELST_SUBTYP='earlyDetection';
UPDATE ER_CODELST SET  CODELST_HIDE='N' WHERE CODELST_TYPE = 'study_type' AND CODELST_SUBTYP='epidemiological';
UPDATE ER_CODELST SET  CODELST_HIDE='N' WHERE CODELST_TYPE = 'study_type' AND CODELST_SUBTYP='studyTypeNone';
UPDATE ER_CODELST SET  CODELST_HIDE='N' WHERE CODELST_TYPE = 'study_type' AND CODELST_SUBTYP='observational';
UPDATE ER_CODELST SET  CODELST_HIDE='N' WHERE CODELST_TYPE = 'study_type' AND CODELST_SUBTYP='qol';
UPDATE ER_CODELST SET  CODELST_HIDE='N' WHERE CODELST_TYPE = 'study_type' AND CODELST_SUBTYP='standardofCare';
UPDATE ER_CODELST SET  CODELST_HIDE='N' WHERE CODELST_TYPE = 'study_type' AND CODELST_SUBTYP='therapeutic';
UPDATE ER_CODELST SET  CODELST_HIDE='N' WHERE CODELST_TYPE = 'study_type' AND CODELST_SUBTYP='tissueBankLab';

commit;

UPDATE ER_CODELST SET  CODELST_DESC='Deceased' WHERE CODELST_TYPE = 'patient_status' AND CODELST_SUBTYP='dead';
UPDATE ER_CODELST SET  CODELST_DESC='Deceased' WHERE CODELST_TYPE = 'ptst_survival' AND CODELST_SUBTYP='dead';

commit;

UPDATE ER_CODELST SET codelst_hide = 'Y' WHERE CODELST_TYPE = 'country' AND (CODELST_SUBTYP <> 'unknown' AND CODELST_SUBTYP <> 'US');

commit; 