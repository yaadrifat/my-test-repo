var moreSubjectDetailsFormJS ={
	sites : [],
	anatomicSites : [], 
	showHideSites: {}
};

moreSubjectDetailsFormJS.showHideSites = function (){
	var b_site_cnt = document.getElementById(formFieldMappingsJS.getColumnSysId('b_site_cnt'));
	var bodySiteCnt = b_site_cnt.value;
	if (!bodySiteCnt){
		bodySiteCnt = 0;
	}

	for (var i=1; i<=5; i++){
		if (i <= bodySiteCnt){
			document.getElementById('site_'+i+'_TR').style.display = "inline";
			document.getElementById('anatomic_site_'+i+'_TR').style.display = "inline";
		} else {
			document.getElementById('site_'+i+'_TR').style.display = "none";
			document.getElementById('anatomic_site_'+i+'_TR').style.display = "none";
		}
	}
};

$j(document).ready(function() {
	formFieldMappingsJS.loadFormFieldMappings();

	var jsArrColumns = formFieldMappingsJS.jsArrColumns;

	for (var i=1; i<=5; i++){
		var site = document.getElementById(formFieldMappingsJS.getColumnSysId('site_'+ (i)));
		var parentTR = site.parentNode.parentNode.parentNode;
		parentTR.id ='site_'+i+'_TR';
		moreSubjectDetailsFormJS.sites[i-1] = site;
		
		var anatomic_site = document.getElementById(formFieldMappingsJS.getColumnSysId('anatomic_site_'+ (i)));
		var parentTR = anatomic_site.parentNode.parentNode.parentNode;
		parentTR.id ='anatomic_site_'+i+'_TR';
		moreSubjectDetailsFormJS.anatomicSites[i-1] = anatomic_site;
	}

	var b_site_cnt = document.getElementById(formFieldMappingsJS.getColumnSysId('b_site_cnt'));
	if (!b_site_cnt.addEventListener) {
		b_site_cnt.attachEvent('onchange', moreSubjectDetailsFormJS.showHideSites);
	} else {
		document.querySelector('#'+b_site_cnt.id).addEventListener('change', moreSubjectDetailsFormJS.showHideSites);
	}
	moreSubjectDetailsFormJS.showHideSites();
});