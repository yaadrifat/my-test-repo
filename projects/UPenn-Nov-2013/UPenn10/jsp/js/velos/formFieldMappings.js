formFieldMappingsJS = {
	jsArrColumns: [],
	metadataReady: false,
	createLoadingDialog: {},
	loadFormFieldMappings: {},
	getColumnSysId: {},
	getColumnMapCol: {}
};

formFieldMappingsJS.createLoadingDialog = function(){
	var loadingPanel = document.getElementById('loadingPanel');
	if (!loadingPanel){
		var loadingPanel = document.createElement('div');
		loadingPanel.id = 'loadingPanel';
		loadingPanel.name = 'loadingPanel';
		loadingPanel.style.display = 'none';
		loadingPanel.innerHTML = '<img class="asIsImage" src="../images/jpg/loading_pg.gif" />';
		document.body.appendChild(loadingPanel);
	}
	$j('#loadingPanel').dialog( {
		height: 60,
		width: 250,
		title: 'Loading...',
		resizable:false, 
		closeOnEscape: false 
		} 
	).parent().find('.ui-dialog-titlebar-close').hide();
	$j("#loadingPanel").dialog("open"); 
};

formFieldMappingsJS.loadFormFieldMappings = function(){
	if (document.getElementsByName("formId")[0]){
		var formId = (document.getElementsByName("formId")[0]).value;

		formFieldMappingsJS.createLoadingDialog();
		jQuery.ajax({
			url:"getFormMetadata.jsp?formId=" + formId,
			async: false,
			cache: false,
			success: function(resp) {
				formFieldMappingsJS.jsArrColumns = [];
				
				for (var i=0; i < (resp.arrColumns).length; i++){
					var jsObj = resp.arrColumns[i];
					formFieldMappingsJS.jsArrColumns[jsObj.colUID]= jsObj;
				}
				formFieldMappingsJS.metadataReady = true;
				setTimeout(function(){
					jQuery("#loadingPanel").dialog('close');
				}, 2000);
			}
		});
		while (!formFieldMappingsJS.metadataReady){
			setTimeout (function(){
				if (!$j("#loadingPanel").dialog("isOpen")){
					jQuery("#loadingPanel").dialog({ modal: true });
				}
			}, 2000);
		}
	}
};

formFieldMappingsJS.getColumnSysId = function(colUId){
	var colSysId = '';
	if(colUId){
		var jsObj = formFieldMappingsJS.jsArrColumns[colUId];
		if (jsObj){
			colSysId = jsObj.colSysId;
		}
	}
	return colSysId;
};

formFieldMappingsJS.getColumnMapCol = function(colUId){
	var colMapCol = '';
	if(colUId){
		var jsObj = formFieldMappingsJS.jsArrColumns[colUId];
		if (jsObj){
			colMapCol = jsObj.colMapCol;
		}
	}
	return colMapCol;
};
