/////*********This readMe is specific to v9.3.0 build#UPenn5 (based on v9.3.0 build #696)**********////
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
1. Please copy the resource bundle files in eResearch_jboss510\conf folder.
2. Please refer to doc folder for the configurations and customizations documentation.
3. Remove following 3 extraneous files from folder server\eresearch\deploy\velos.ear\velos.war\jsp
	a. morePatientStudyDetails.jsp
	b. morePatientStudyDetailsInclude.jsp 
	c. studyInclude.jsp
4. Please note the lableBundle switch "Config_StudyScreen_Switch" is now removed and Integrated Study screen will be available from everywhere in the application.
   Please remove corresponding configurations from lableBundle_custom.properties file.
5. Please note the Integrated Patient Demographics screen are introduced in the application but no enhancements are yet received for the same.
