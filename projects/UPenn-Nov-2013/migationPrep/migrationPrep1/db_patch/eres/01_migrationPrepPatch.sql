SET DEFINE OFF;
SET SERVEROUTPUT ON;

DECLARE
  v_item_exists number := 0;  
BEGIN

select count(*) into v_item_exists from ERES.ER_CODELST where 
CODELST_TYPE = 'studyPurpose' and CODELST_SUBTYP = 'ancillary';
if (v_item_exists = 0) then
 Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ)
 Values
   (SEQ_ER_CODELST.nextval, NULL, 'studyPurpose', 'ancillary', 'Ancillary', 
    'Y', 150);
  commit;
  dbms_output.put_line('One row inserted');
else
  dbms_output.put_line('Row already exists');
end if;

select count(*) into v_item_exists from ERES.ER_CODELST where 
CODELST_TYPE = 'studyPurpose' and CODELST_SUBTYP = 'earlyDetection';
if (v_item_exists = 0) then
 Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ)
 Values
   (SEQ_ER_CODELST.nextval, NULL, 'studyPurpose', 'earlyDetection', 'Early Detection', 
    'Y', 151);
  commit;
  dbms_output.put_line('One row inserted');
else
  dbms_output.put_line('Row already exists');
end if;

END;
/

set define off;

update er_codelst 
set codelst_custom_col1 = '<select name="alternateId"><option value="">Select An Option</option><option value="followUpRep">Follow-up Report</option><option value="initialRep">Initial Report</option></select>'
where codelst_type = 'advtype' and codelst_subtyp = 'rep_typ';

commit;

update er_codelst 
set codelst_custom_col1 = '<select name="alternateId"><option value="">Select An Option</option><option value="hours">Hours</option><option value="days">Days</option><option value="minutes">Minutes</option><option value="unknown">Unknown</option><option value="na">N/A</option></select>'
where codelst_type = 'advtype' and codelst_subtyp = 'ae_duration';

commit;
