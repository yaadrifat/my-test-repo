/////*********This readMe is specific to v9.2.0 build#UPenn3**********////
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
1. Please copy the resource bundle files in eResearch_jboss510\conf folder.
2. Please refer to doc folder for the configurations and customizations documentation.
3. Remove following 2 extraneous files from folder server\eresearch\deploy\velos.ear\velos.war\jsp
	a. morePatientStudyDetails.jsp
	b. morePatientStudyDetailsInclude.jsp 