set define off;

update er_codelst set codelst_custom_col1 = 'eth_prim,eth_second' where codelst_type = 'ethnicity';

commit;

UPDATE ER_CODELST SET  CODELST_DESC='Hispanic', CODELST_CUSTOM_COL1='eth_prim' WHERE CODELST_TYPE = 'ethnicity' AND CODELST_SUBTYP='hispanic';
UPDATE ER_CODELST SET  CODELST_CUSTOM_COL1='eth_prim' WHERE CODELST_TYPE = 'ethnicity' AND CODELST_SUBTYP='nonhispanic';
UPDATE ER_CODELST SET  CODELST_CUSTOM_COL1='eth_prim' WHERE CODELST_TYPE = 'ethnicity' AND CODELST_SUBTYP='unknown';
UPDATE ER_CODELST SET  CODELST_CUSTOM_COL1='eth_prim' WHERE CODELST_TYPE = 'ethnicity' AND CODELST_SUBTYP='notreported';

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,299,10,'03_UpdateExistingtEthRecords.sql',sysdate,'v9.3.0 #700');

commit;