/////*********This readMe is specific to v9.3.0 build#UPenn6 (based on v9.3.0 build #700)**********////
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
1. Please copy the resource bundle files in eResearch_jboss510\conf folder.
2. Copy the 'jsp' files in \velos.ear\velos.war\jsp folder.
3. Copy the 'aeScreen.js' in \velos.ear\velos.war\jsp\js\velosCustom
4. Replace '[fk_account]' in '06_UPenn_Configurations.sql' file with actual Account Id.