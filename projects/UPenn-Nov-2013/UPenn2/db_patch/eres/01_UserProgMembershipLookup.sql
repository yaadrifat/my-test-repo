set define off;

create or replace view eres.cust_userProgMembership
as
select pk_user, fk_account, usr_firstname, usr_lastname, usr_firstname || ' ' || usr_lastname uname,
usr_type, t.md_modelementpk as PK_moreDetails, t.md_modelementdata as progMembership
from er_user, 
(select fk_modpk, md_modelementpk, md_modelementdata  from er_moredetails where md_modname ='user'
and md_modelementpk = (select pk_codelst from er_codelst where er_codelst.codelst_type='user' and er_codelst.codelst_subtyp ='progMembership'))t
where er_user.pk_user= t.fk_modpk(+);

Declare

lkpID INTEGER DEFAULT 0;
lkpvwID INTEGER DEFAULT 0;
lkpcolID INTEGER DEFAULT 0;
lkpvwcolID INTEGER DEFAULT 0;
lkpvwcolSeq INTEGER DEFAULT 0;


BEGIN

	SELECT MAX(PK_LKPLIB)+1 INTO lkpID FROM ER_LKPLIB;

	SELECT MAX(PK_LKPVIEW)+1 INTO lkpvwID FROM ER_LKPVIEW;

	SELECT max(pk_LKPCOL)+1 into lkpcolID FROM er_lkpcol;

	SELECT MAX(PK_LKPVIEWCOL)+1 INTO lkpvwcolID FROM ER_LKPVIEWCOL;
	lkpvwcolSeq :=1; 		
		
	-- INSERTING into ER_LKPLIB
	INSERT INTO ER_LKPLIB (PK_LKPLIB,LKPTYPE_NAME,LKPTYPE_VERSION,LKPTYPE_DESC,LKPTYPE_TYPE,FK_ACCOUNT,FK_SITE) 
	VALUES (lkpID,'dynReports',null,'User Program Membership','dyn_a',null,null);			
	
	-- INSERTING into ER_LKPVIEW
	INSERT INTO ER_LKPVIEW (PK_LKPVIEW,LKPVIEW_NAME,LKPVIEW_DESC,FK_LKPLIB,LKPVIEW_RETDATA,LKPVIEW_RETDISP,LKPVIEW_ISACTIVE,LKPVIEW_IGNOREFILTER,LKPVIEW_FILTER,LKPVIEW_KEYWORD) 
	VALUES (lkpvwID,'User Program Membership',null,lkpID,null,null,null,null,'fk_account=[:ACCID]',null); 

	-- INSERTING into ER_LKPCOL and ER_LKPVIEWCOL
	INSERT INTO ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD) 
	VALUES (lkpcolID, lkpID,'pk_user','pk_user','number',null,'cust_userProgMembership','pk_user');
	INSERT INTO ER_LKPVIEWCOL ( PK_LKPVIEWCOL, FK_LKPCOL, LKPVIEW_IS_SEARCH, LKPVIEW_SEQ, FK_LKPVIEW, LKPVIEW_DISPLEN, LKPVIEW_IS_DISPLAY ) 
	VALUES ( lkpvwcolID, lkpcolID, 'Y', lkpvwcolSeq, lkpvwID, '10%', 'N');
	lkpvwcolID := lkpvwcolID+1; 
	lkpvwcolSeq := lkpvwcolSeq +1;
	lkpcolID := lkpcolID+1;

	INSERT INTO ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD) 
	VALUES (lkpcolID, lkpID,'usr_firstname','First Name','varchar2',null,'cust_userProgMembership','usr_firstname');
	INSERT INTO ER_LKPVIEWCOL ( PK_LKPVIEWCOL, FK_LKPCOL, LKPVIEW_IS_SEARCH, LKPVIEW_SEQ, FK_LKPVIEW, LKPVIEW_DISPLEN, LKPVIEW_IS_DISPLAY ) 
	VALUES ( lkpvwcolID, lkpcolID, 'Y', lkpvwcolSeq, lkpvwID, '10%', 'N');
	lkpvwcolID := lkpvwcolID+1; 
	lkpvwcolSeq := lkpvwcolSeq +1;
	lkpcolID := lkpcolID+1;

	INSERT INTO ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD) 
	VALUES (lkpcolID, lkpID,'usr_lastname','Last Name','varchar2',null,'cust_userProgMembership','usr_lastname');
	INSERT INTO ER_LKPVIEWCOL ( PK_LKPVIEWCOL, FK_LKPCOL, LKPVIEW_IS_SEARCH, LKPVIEW_SEQ, FK_LKPVIEW, LKPVIEW_DISPLEN, LKPVIEW_IS_DISPLAY ) 
	VALUES ( lkpvwcolID, lkpcolID, 'Y', lkpvwcolSeq, lkpvwID, '10%', 'N');
	lkpvwcolID := lkpvwcolID+1; 
	lkpvwcolSeq := lkpvwcolSeq +1;			
	lkpcolID := lkpcolID+1;

	INSERT INTO ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD) 
	VALUES (lkpcolID, lkpID,'uname','User Name','varchar2',null,'cust_userProgMembership','uname');
	INSERT INTO ER_LKPVIEWCOL ( PK_LKPVIEWCOL, FK_LKPCOL, LKPVIEW_IS_SEARCH, LKPVIEW_SEQ, FK_LKPVIEW, LKPVIEW_DISPLEN, LKPVIEW_IS_DISPLAY ) 
	VALUES ( lkpvwcolID, lkpcolID, 'Y', lkpvwcolSeq, lkpvwID, '10%', 'Y');
	lkpvwcolID := lkpvwcolID+1; 
	lkpvwcolSeq := lkpvwcolSeq +1;			
	lkpcolID := lkpcolID+1;

	INSERT INTO ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD) 
	VALUES (lkpcolID, lkpID,'usr_type','User Type','varchar2',null,'cust_userProgMembership','usr_type');
	INSERT INTO ER_LKPVIEWCOL ( PK_LKPVIEWCOL, FK_LKPCOL, LKPVIEW_IS_SEARCH, LKPVIEW_SEQ, FK_LKPVIEW, LKPVIEW_DISPLEN, LKPVIEW_IS_DISPLAY ) 
	VALUES ( lkpvwcolID, lkpcolID, 'Y', lkpvwcolSeq, lkpvwID, '10%', 'N');
	lkpvwcolID := lkpvwcolID+1; 
	lkpvwcolSeq := lkpvwcolSeq +1;
	lkpcolID := lkpcolID+1;
	
	INSERT INTO ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD) 
	VALUES (lkpcolID, lkpID,'PK_moreDetails','More Detail PK','number',null,'cust_userProgMembership','PK_moreDetails');
	INSERT INTO ER_LKPVIEWCOL ( PK_LKPVIEWCOL, FK_LKPCOL, LKPVIEW_IS_SEARCH, LKPVIEW_SEQ, FK_LKPVIEW, LKPVIEW_DISPLEN, LKPVIEW_IS_DISPLAY ) 
	VALUES ( lkpvwcolID, lkpcolID, 'Y', lkpvwcolSeq, lkpvwID, '10%', 'N');
	lkpvwcolID := lkpvwcolID+1; 
	lkpvwcolSeq := lkpvwcolSeq +1;
	lkpcolID := lkpcolID+1;
	
	INSERT INTO ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD) 
	VALUES (lkpcolID, lkpID,'progMembership','Program Membership','varchar2',null,'cust_userProgMembership','progMembership');
	INSERT INTO ER_LKPVIEWCOL ( PK_LKPVIEWCOL, FK_LKPCOL, LKPVIEW_IS_SEARCH, LKPVIEW_SEQ, FK_LKPVIEW, LKPVIEW_DISPLEN, LKPVIEW_IS_DISPLAY ) 
	VALUES ( lkpvwcolID, lkpcolID, 'Y', lkpvwcolSeq, lkpvwID, '10%', 'Y');
	lkpvwcolID := lkpvwcolID+1; 
	lkpvwcolSeq := lkpvwcolSeq +1;			
	lkpcolID := lkpcolID+1;
	
	INSERT INTO ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD) 
	VALUES (lkpcolID, lkpID,'FK_ACCOUNT','FK_ACCOUNT','number',null,'cust_userProgMembership','LKP_PK');
	INSERT INTO ER_LKPVIEWCOL ( PK_LKPVIEWCOL, FK_LKPCOL, LKPVIEW_IS_SEARCH, LKPVIEW_SEQ, FK_LKPVIEW, LKPVIEW_DISPLEN, LKPVIEW_IS_DISPLAY ) 
	VALUES ( lkpvwcolID, lkpcolID, 'Y', lkpvwcolSeq, lkpvwID, '10%', 'N');	

	commit;
END;
/ 

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,292,1,'01_UserProgMembershipLookup.sql',sysdate,'v9.2.0 #693.02-UP2');

commit;
