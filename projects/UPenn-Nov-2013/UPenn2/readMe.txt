/////*********This readMe is specific to v9.2.0 build#UPenn2**********////
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
1. Database patch eres\01_UserProgMembershipLookup.sql is a UPenn specific patch. It should not be given to other customers. 
2. Please copy the resource bundle files in eResearch_jboss510\conf folder.
3. Please refer to doc folder for the configurations and customizations documentation.