--Create er_lkpview entry for sponsor lookup

INSERT INTO ER_LKPVIEW (PK_LKPVIEW, LKPVIEW_NAME, FK_LKPLIB, LKPVIEW_KEYWORD)
VALUES ((( Select MAX(PK_LKPVIEW) from er_lkpview)+1), 'StudyTeam Organization', grab pk_of_previous query , 'studyOrgs');

commit;