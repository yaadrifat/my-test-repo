--create er_lkplib entry for sponsor lookup

INSERT INTO ER_LKPLIB (PK_LKPLIB, LKPTYPE_NAME, LKPTYPE_DESC, LKPTYPE_TYPE)
VALUES ( (( Select MAX(PK_LKPLIB) from er_lkplib)+1), 'StudyOrgs','Study Team Organizations' , 'studyOrgs');
 
commit;