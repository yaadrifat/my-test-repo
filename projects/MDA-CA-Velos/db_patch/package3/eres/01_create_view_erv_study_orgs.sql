CREATE OR REPLACE FORCE VIEW "ERES"."ERV_STUDY_ORGS" ("FK_ACCOUNT", "ORGANIZATION", "ORG_TYPE", "FK_CODELST", "FK_STUDY", "SPONSORID")
                                 AS
 SELECT es.fk_account           AS fk_account,
    es.site_name                 AS organization,
    ec.codelst_desc              AS org_type,
    ess.fk_codelst_studysitetype AS fk_codelst,
    ess.fk_study                 AS fk_study,
    es.site_id                   AS sponsorid
  FROM er_studysites ess,
    er_site es,
    er_codelst ec
  WHERE ess.fk_site               =es.pk_site
  AND ess.fk_codelst_studysitetype=ec.pk_codelst;
  
  commit;