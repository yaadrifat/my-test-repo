--create entry for sponsor lookup field in er_codelst table.

INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1)
VALUES (SEQ_ER_CODELST.NEXTVAL, 'evtaddlcode', 'sponsor', 'Sponsor','N',1, 'lookup','{lookupPK:pk_of_er_lkpview table,selection:"single",mapping:[{source:"ORGANIZATION",target:"alternateId"}]}');

commit;