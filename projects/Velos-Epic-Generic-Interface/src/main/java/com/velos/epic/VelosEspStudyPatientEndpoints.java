package com.velos.epic;

import org.apache.camel.CamelContext;
import org.apache.camel.ExchangePattern;
import org.apache.camel.ProducerTemplate;
import org.apache.cxf.message.MessageContentsList;
import org.apache.log4j.Logger;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.integration.MessageChannel;
import org.springframework.integration.core.PollableChannel;

import com.velos.epic.StringToXMLGregorianCalendar;
import com.velos.integration.webservice.VelosEspCreateAndEnrollPatientEndpoint;
import com.velos.integration.webservice.VelosEspEnrollPatientToStudyEndpoint;
import com.velos.integration.webservice.VelosEspGetStudyEndpoint;
import com.velos.integration.webservice.VelosEspSearchPatientEndpoint;
import com.velos.integration.webservice.VelosEspStudyEndpoint;
import com.velos.integration.webservice.VelosEspSystemAdministrationEndpoint;
import com.velos.outbound.servlet.EpicServlet;
import com.velos.services.EnrollPatientToStudyResponse;
import com.velos.services.ObjectInfo;
import com.velos.services.OperationException;
import com.velos.services.OperationException_Exception;
import com.velos.services.OperationRolledBackException;
import com.velos.services.OperationRolledBackException_Exception;
import com.velos.services.Patient;
import com.velos.services.PatientEnrollmentDetails;
import com.velos.services.PatientIdentifier;
import com.velos.services.PatientSearch;
import com.velos.services.PatientSearchResponse;
import com.velos.services.PatientStudyStatusIdentifier;
import com.velos.services.ResponseHolder;
import com.velos.services.SimpleIdentifier;
import com.velos.services.Study;
import com.velos.services.StudyIdentifier;


public class VelosEspStudyPatientEndpoints extends StringToXMLGregorianCalendar implements
VelosEspSystemAdministrationEndpoint, VelosEspGetStudyEndpoint,VelosEspCreateAndEnrollPatientEndpoint,
VelosEspEnrollPatientToStudyEndpoint,VelosEspSearchPatientEndpoint {
	private static Logger logger = Logger.getLogger(VelosEspStudyPatientEndpoints.class);   
	private CamelContext context;

	public VelosEspStudyPatientEndpoints() {
		configureCamel();
	}
	public CamelContext getContext() {
		return context;
	}

	public void setContext(CamelContext context) {
		this.context = context;
	}

	private CamelContext configureCamel() {
		ClassPathXmlApplicationContext springContext = null;
		try{
		if (this.context == null) {
		springContext= new ClassPathXmlApplicationContext(
					"camel-config.xml");
			this.setContext((CamelContext) springContext.getBean("camel"));
		}
		}finally{
			springContext.close();
		}
		return getContext();
	}
	



	//Get Study Details
	@Override
	public Study getStudy(StudyIdentifier studyIdentifier)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList list = (MessageContentsList) producer.sendBody(
				"cxf:bean:espGetStudyEndpoint", ExchangePattern.InOut,
				studyIdentifier);
		return (Study) list.get(0);

	}

	@Override
	public ObjectInfo getObjectInfoFromOID(SimpleIdentifier arg0)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList list = (MessageContentsList) producer.sendBody(
				"cxf:bean:espSysAdminEndpoint", ExchangePattern.InOut, arg0);
		return (ObjectInfo) list.get(0);
	}
	

	//Get Study Summary
	/*@Override
	public StudySummary getStudySummary(StudyIdentifier studyIdentifier)
			throws OperationException_Exception {
		
		System.out.println("Get sutdy Summary");
		ProducerTemplate producer = context.createProducerTemplate();
		System.out.println("Get sutdy Summary2");
		MessageContentsList list = (MessageContentsList) producer.sendBody(
				"cxf:bean:espStudyEndpoint", ExchangePattern.InOut,
				studyIdentifier);
		System.out.println("Get sutdy Summary3");
		return (StudySummary) list.get(0);

	}*/

	


	//Enroll Patient To Study
	@Override
	public ResponseHolder enrollPatientToStudy(
			PatientIdentifier patientIdentifier,
			StudyIdentifier studyIdentifier,
			PatientEnrollmentDetails patientEnrollmentDetails)
			throws OperationException_Exception,
			OperationRolledBackException_Exception {
		
		MessageContentsList list = null;
		
		 /*  MessageChannel channelXml = ((AbstractApplicationContext) context).getBean("input-xml", MessageChannel.class);
	        PollableChannel output = ((AbstractApplicationContext) context).getBean("output", PollableChannel.class);
	        
		*/
		ProducerTemplate producer = context.createProducerTemplate();

		System.out.println("Patient Enrollement Status3");
		MessageContentsList inpList = new MessageContentsList();
		inpList.add(patientIdentifier);
		inpList.add(studyIdentifier);
		inpList.add(patientEnrollmentDetails);
		System.out.println("Patient Enrollement Status4");
		list = (MessageContentsList) producer.sendBody(
				"cxf:bean:espEnrollPatientToStudy", ExchangePattern.InOut,
				inpList);

		System.out.println("Patient Enrollement Status5");

		return (ResponseHolder) list.get(0);
	}
	@Override
	public ResponseHolder createAndEnrollPatient(Patient patient,
			StudyIdentifier studyIdentifier,
			PatientEnrollmentDetails patientEnrollmentDetails)
			/*throws OperationException_Exception,
			OperationRolledBackException_Exception */{

		MessageContentsList list = null;
		try{
		ProducerTemplate producer = context.createProducerTemplate();


		MessageContentsList inpList = new MessageContentsList();
		inpList.add(patient);
		inpList.add(studyIdentifier);
		inpList.add(patientEnrollmentDetails);
		System.out.println("Crate Patient &&  Enrollement Status");
		list = (MessageContentsList) producer.sendBody(
				"cxf:bean:espCreateAndEnrollPatientEndpoint", ExchangePattern.InOut,
				inpList);
		
		
		}catch(Exception e){
			//e.printStackTrace();
		}
		return (ResponseHolder) list.get(0);
	}
	@Override
	public PatientSearchResponse searchPatient(PatientSearch patientSearch)
			throws OperationException_Exception {
		try
		{logger.info("inside searchPatient");
		
			ProducerTemplate producer = context.createProducerTemplate();
			logger.info("inside searchPatient before sendBody");
			MessageContentsList list = (MessageContentsList) producer.sendBody(
					"cxf:bean:espSearchPatientEndpoint", ExchangePattern.InOut,
					patientSearch);
			logger.info("inside searchPatient after sendBody");
			return (PatientSearchResponse) list.get(0);
		}
		catch(Exception e)
		{
			
			logger.info("inside catch");
			logger.info("Error Message while updatePatient : " + e.getCause().getMessage());
			logger.info(e.getMessage());
		}
		
		return null;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/*@Override
	public ResponseHolder enrollPatientToStudy(
			
			PatientIdentifier patientIdentifier,
			StudyIdentifier studyIdentifier,
			PatientEnrollmentDetails patientEnrollmentDetails) throws OperationException, OperationRolledBackException
					 {
		
		MessageContentsList list = null;
		

		ProducerTemplate producer = context.createProducerTemplate();
		
		System.out.println("Patient Enrollement Status3");
		MessageContentsList inpList = new MessageContentsList();
		inpList.add(patientIdentifier);
		inpList.add(studyIdentifier);
		inpList.add(patientEnrollmentDetails);
		System.out.println("Patient Enrollement Status1");
		list = (MessageContentsList) producer.sendBody(
				"cxf:bean:espEnrollPatientToStudy", ExchangePattern.InOut,
				inpList);
		
		
		
		System.out.println("Patient Enrollement Status2");
		return (ResponseHolder) list.get(0);
		


	}*/
}
