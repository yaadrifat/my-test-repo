package com.velos.epic;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.velos.integration.mapping.ProtocolKeys;
import com.velos.integration.mapping.VelosKeys;
import com.velos.services.PatientDataBean;

public class PatientEnrollmentUtil {
	private static Logger logger = Logger.getLogger(PatientEnrollmentUtil.class);   
	StudyStatusMessage studyStatusMessage =new StudyStatusMessage();
	
	public void patientEnrollment(Map<String, String>resultMap){
		
		
		//String siteName=resultMap.get("siteName");
		String studyNumber=resultMap.get("studyNumber");
		String patientId=resultMap.get("patientId");
		String firstName=resultMap.get("firstName");
		String lastName=resultMap.get("lastName");
		String dob=resultMap.get("dob");

		try{
			List<PatientDataBean> patientDataList=studyStatusMessage.searchPatientHelper(resultMap);
			
			

		System.out.println("Patient data list size -------------->"+patientDataList.size());

			int pdlSize=patientDataList.size();

			

		if(pdlSize == 0){

				logger.info("\n************PatienId not Exist so patient create and enroll to study *****************\n");
				studyStatusMessage.createEnrollPatient(resultMap);
			}else{
				logger.info("**************Patient Existed so enroll to study***************");
				studyStatusMessage.enrollPatientStudyHelper(resultMap);
			}

		}catch(Exception e){

		}

	}

}
