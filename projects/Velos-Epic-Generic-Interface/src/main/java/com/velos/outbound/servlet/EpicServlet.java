package com.velos.outbound.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.velos.epic.StudyStatusMessage;
import com.velos.epic.XmlProcess;
import com.velos.epic.service.EpicEndpointClient;
import com.velos.integration.espclient.VelosEspClient;
import com.velos.integration.espclient.VelosEspMethods;
import com.velos.integration.gateway.ProtocolEndpoint;
import com.velos.integration.mapping.EndpointKeys;
import com.velos.integration.mapping.ProtocolKeys;
import com.velos.integration.mapping.VelosKeys;
import com.velos.services.StudyStatus;


/**
 * Servlet implementation class EpicServlet
 */
public class EpicServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(EpicServlet.class);   
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EpicServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response){
		logger.info("********EpicServlet Service Started***************");
		System.out.println("***********EpicServlet Service started*************");
		try{
			String uri = request.getRequestURI();
			System.out.println("uri=="+uri);
			//OutboundService serviceObj = new OutboundServiceImpl();
			String studyId = null;
			Map<VelosKeys,String> resultMap=new HashMap<VelosKeys,String>();
			System.out.println("*****epic********");
			EpicEndpointClient eepc=new EpicEndpointClient();
			//serviceObj.sendRetrieveProtocolDefResponse();
			System.out.println("StudyId--->"+studyId);
			/*String patientid = request.getParameter("patientid");
			String estudynumber = request.getParameter("estudynumber");
			String exPatientId =request.getParameter("exPatientId");

			 */
			if(uri.equals("/velos-patient-interface/RetrieveProtocol.do")){
				
			String studyNumber = request.getParameter("studynumber");
			Map<VelosKeys,Object> resultMap1=new HashMap<VelosKeys,Object>();
			
			resultMap1.put(ProtocolKeys.StudyId,studyNumber);
			try{
				eepc.sendRequest(resultMap1);
			}catch(Exception e){
				
			}

			}else if(uri.equals("/velos-patient-interface/EnrollPatient.do")){

				studyId=request.getParameter("studyId");
				resultMap.put(ProtocolKeys.StudyId,studyId);
				String patientId=request.getParameter("patientId");
				resultMap.put(ProtocolKeys.PatientID, patientId);
				System.out.println("StudyId--->"+studyId);
				System.out.println("Incoming Values ---->"+resultMap);

				VelosEspClient client = new VelosEspClient();
				resultMap.put(EndpointKeys.Endpoint, EndpointKeys.Epic.toString());
				
				Map<VelosKeys, Object> dataMap = client.handleRequest(VelosEspMethods.PatientDetails,resultMap);
				
				resultMap.put(ProtocolKeys.OID,(String) dataMap.get(ProtocolKeys.OID));
				
				Map<VelosKeys, Object> dataMap1 = client.handleRequest(VelosEspMethods.PatientStudyGetStudyPatientStatusHistory,resultMap);
                
	            resultMap.put(ProtocolKeys.StudyPatOID,(String) dataMap1.get(ProtocolKeys.OID));	
	            System.out.println("StudyPatStatus="+dataMap1.get(ProtocolKeys.StudyPatStatus));
	            
	            Map<VelosKeys, Object> dataMap2 = client.handleRequest(VelosEspMethods.StudyPatientStatus,resultMap);
			
				System.out.println("Patient Data --->"+dataMap.isEmpty());
				System.out.println(dataMap);
				dataMap.put(ProtocolKeys.StudyId,studyId);
				dataMap.put(ProtocolKeys.StudyPatStatus,dataMap1.get(ProtocolKeys.StudyPatStatus));
				dataMap.put(ProtocolKeys.StudyPatId,dataMap2.get(ProtocolKeys.StudyPatId));


				eepc.enrollPatientRequest(dataMap);

			}else if(uri.equals("/velos-patient-interface/AlertProtocol.do")){
				studyId=request.getParameter("studyId");
				resultMap.put(ProtocolKeys.StudyId,studyId);
				String patientId=request.getParameter("patientId");
				resultMap.put(ProtocolKeys.PatientID, patientId);
				


				System.out.println("StudyId--->"+studyId);
				System.out.println("Incoming Values ---->"+resultMap);

				VelosEspClient client = new VelosEspClient();
				resultMap.put(EndpointKeys.Endpoint, EndpointKeys.Epic.toString());
				
				logger.info("ResultMap ---->"+resultMap);
				
				Map<VelosKeys, Object> dataMap = client.handleRequest(VelosEspMethods.PatientDetails,resultMap);
				
				resultMap.put(ProtocolKeys.OID,(String) dataMap.get(ProtocolKeys.OID));
				
				Map<VelosKeys, Object> dataMap1 = client.handleRequest(VelosEspMethods.PatientStudyGetStudyPatientStatusHistory,resultMap);
                
	            resultMap.put(ProtocolKeys.StudyPatOID,(String) dataMap1.get(ProtocolKeys.OID));	
	            
	            Map<VelosKeys, Object> dataMap2 = client.handleRequest(VelosEspMethods.StudyPatientStatus,resultMap);
			
				System.out.println("Patient Data --->"+dataMap.isEmpty());
				System.out.println(dataMap);
				dataMap.put(ProtocolKeys.StudyId,studyId);
				dataMap.put(ProtocolKeys.StudyPatStatus,dataMap1.get(ProtocolKeys.StudyPatStatus));
				dataMap.put(ProtocolKeys.StudyPatId,dataMap2.get(ProtocolKeys.StudyPatId));

				

	eepc.alertProtocolState(dataMap);

			}


			logger.info("Study Number --->"+studyId);

		}catch(Exception e){

			e.printStackTrace();
		}
		logger.info("********EpicServlet Service Ended***************");
	}


}
