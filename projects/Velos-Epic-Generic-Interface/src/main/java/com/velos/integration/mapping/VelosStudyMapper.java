package com.velos.integration.mapping;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.velos.epic.XmlProcess;
import com.velos.services.GetStudyPatientStatusHistoryResponse;
import com.velos.services.GetStudyStatusesResponse;
import com.velos.services.GetStudySummaryResponse;
import com.velos.services.NvPair;
import com.velos.services.StudyStatus;
import com.velos.services.StudyStatuses;
import com.velos.services.StudySummary;
import com.velos.services.UserIdentifier;

public class VelosStudyMapper {
	
	private static Logger logger = Logger.getLogger(VelosStudyMapper.class); 
	
	
	private Map<VelosKeys, Object> dataMap = new HashMap<VelosKeys, Object>();
	private String endpoint = null;
	Properties prop=null;
	public VelosStudyMapper(String endpoint) {
		this.endpoint = endpoint;
		prop= new Properties();
		try {
			prop.load(this.getClass().getClassLoader().getResourceAsStream("epic.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	//StudySummary
	public Map<VelosKeys, Object> mapStudySummary(GetStudySummaryResponse resp) {
		if (EndpointKeys.Epic.toString().equals(endpoint)) {
			mapStudySummaryForEpic(resp);
		}
		return dataMap;
	}
	
	//StudyStatusResponse
	public Map<VelosKeys, Object> mapStudyStatuses(GetStudyStatusesResponse resp) {
		if (EndpointKeys.Epic.toString().equals(endpoint)) {
			mapStudyStatusesForEpic(resp);
		}
		return dataMap;
	}
	//StudyStatusResponse
	private void mapStudyStatusesForEpic(GetStudyStatusesResponse resp) {
		StudyStatuses studyStatues = resp.getStudyStatuses();
		List<StudyStatus>  studyStatusList= studyStatues.getStudyStatus();
		System.out.println("studyStatusList=="+studyStatusList);
		if(studyStatusList!=null && !studyStatusList.isEmpty()){
			for(StudyStatus studyStatus:studyStatusList){
				String code = studyStatus.getStatus().getCode();
				String stdyStatus = prop.getProperty("vesp_study_status");
				if(code!=null && code.trim().equalsIgnoreCase(stdyStatus)){
					dataMap.put(ProtocolKeys.StudyStatuses, code);
				}else{
					//
				}
			}
		}
	}
	
	public Map<VelosKeys, Object> mapStudySummary(StudySummary summary) {
		if (EndpointKeys.Epic.toString().equals(endpoint)) {
			mapStudySummaryForEpic(summary);
		}
		return dataMap;
	}
	private void mapStudySummaryForEpic(StudySummary summary) {
		logger.info("\n**********************mapStudySummaryForEpic _ Method*****************\n");
		dataMap.put(ProtocolKeys.Title, MapUtil.escapeSpecial(summary.getStudyTitle()));
		dataMap.put(ProtocolKeys.Text, MapUtil.escapeSpecial(summary.getSummaryText()));
		dataMap.put(ProtocolKeys.StudyNumber, MapUtil.escapeSpecial(summary.getStudyNumber()));
		/*List<NvPair> moreList = summary.getMoreStudyDetails();
		for (NvPair more : moreList) {
			if (ProtocolKeys.IrbNumber.toString().equalsIgnoreCase(more.getKey())) {
				dataMap.put(ProtocolKeys.IrbNumber, more.getValue());
			} else if (ProtocolKeys.Irb.toString().equalsIgnoreCase(more.getKey())) {
				dataMap.put(ProtocolKeys.Irb, more.getValue());
			} else if (ProtocolKeys.NctNumber.toString().equalsIgnoreCase(more.getKey())) {
				dataMap.put(ProtocolKeys.NctNumber, more.getValue());
			} else if (ProtocolKeys.Nct.toString().equalsIgnoreCase(more.getKey())) {
				dataMap.put(ProtocolKeys.Nct, more.getValue());
			}
		}*/
		         
		       
				 String NctNumber =summary.getNctNumber();
		         
				 if(NctNumber != null){
		        	
					 dataMap.put(ProtocolKeys.NctNumber,NctNumber.substring(3));
		         } else if(NctNumber == null){
		        	 dataMap.put(ProtocolKeys.NctNumber,"");
		         }
		         logger.info("NCTNumber --->"+ NctNumber);
		
		         
		UserIdentifier userIdentifier= summary.getStudyContact();
		System.out.println("userIdentifier=="+userIdentifier);
		if(userIdentifier!=null){
			dataMap.put(ProtocolKeys.LastName, isEmpty(userIdentifier.getLastName()));
			dataMap.put(ProtocolKeys.FirstName, isEmpty(userIdentifier.getFirstName()));
			dataMap.put(ProtocolKeys.StudyContact,isEmpty( userIdentifier.getUserLoginName()));
		
		} else if(userIdentifier == null){
			dataMap.put(ProtocolKeys.LastName, "");
			dataMap.put(ProtocolKeys.FirstName,"");
			dataMap.put(ProtocolKeys.StudyContact,"");
		}
		
		
		UserIdentifier userIdentifier1= summary.getPrincipalInvestigator();
		System.out.println("userIdentifier1=="+userIdentifier1);
		logger.info("userIdentifier1====>"+userIdentifier1);
		
		if(userIdentifier1!=null){
			logger.info("***********UserIdentifer is not null*******");
			dataMap.put(ProtocolKeys.PILastName, isEmpty(userIdentifier1.getLastName()));
			dataMap.put(ProtocolKeys.PIFirstName, isEmpty(userIdentifier1.getFirstName()));
			String pIContact = userIdentifier1.getUserLoginName();
			System.out.println("PI Contact =========>>>"+pIContact);
			dataMap.put(ProtocolKeys.PIContact,isEmpty(pIContact));
			System.out.println("PI Contact =========>>>"+pIContact);
		} else if(userIdentifier1 == null){
			logger.info("***********UserIdentifer1 is null*******");
			dataMap.put(ProtocolKeys.PILastName,"");
			dataMap.put(ProtocolKeys.PIFirstName, "");
			dataMap.put(ProtocolKeys.PIContact,"");
			
		}
		logger.info("\n**********************mapStudySummaryForEpic _ Method========>\n"+dataMap);
		
	
	}
	//StudySummary
	private void mapStudySummaryForEpic(GetStudySummaryResponse resp) {
		logger.info("\n**********************mapStudySummaryForEpic _ Method*****************\n");
		
		StudySummary summary = resp.getStudySummary();
		String NctNumber =summary.getNctNumber();
		UserIdentifier userIdentifier= summary.getStudyContact();
		UserIdentifier userIdentifier1= summary.getPrincipalInvestigator();
		
		if(prop.containsKey("outbound_study"+".tittle")
				&& !"".equals(prop.getProperty("outbound_study"+".tittle")
						.toString().trim())) {
			dataMap.put(ProtocolKeys.Title, MapUtil.escapeSpecial(summary.getStudyTitle()));
		}

		if(prop.containsKey("outbound_study"+".text")
				&& !"".equals(prop.getProperty("outbound_study"+".text")
						.toString().trim())) {
			dataMap.put(ProtocolKeys.Text, MapUtil.escapeSpecial(summary.getSummaryText()));
		}
		
		if(prop.containsKey("outbound_study"+".studynumber")
				&& !"".equals(prop.getProperty("outbound_study"+".studynumber")
						.toString().trim())) {
			dataMap.put(ProtocolKeys.StudyNumber, MapUtil.escapeSpecial(summary.getStudyNumber()));
		}
		
		if(prop.containsKey("outbound_study"+".nctnumber")
				&& !"".equals(prop.getProperty("outbound_study"+".nctnumber")
						.toString().trim())) {
			if(NctNumber != null){
				dataMap.put(ProtocolKeys.NctNumber,isEmpty(NctNumber.substring(3)));
			}
		}
		
		if(userIdentifier!=null){
			if(prop.containsKey("outbound_study"+".studycontact")
					&& !"".equals(prop.getProperty("outbound_study"+".studycontact")
							.toString().trim())) {
				String userLoginName = userIdentifier.getUserLoginName();
				dataMap.put(ProtocolKeys.StudyContact,isEmpty(userLoginName));
			}
			if(prop.containsKey("outbound_study"+".studycontact_firstname")
					&& !"".equals(prop.getProperty("outbound_study"+".studycontact_firstname")
							.toString().trim())) {
				dataMap.put(ProtocolKeys.FirstName, isEmpty(userIdentifier.getFirstName()));
			}
			if(prop.containsKey("outbound_study"+".studycontact_lastname")
					&& !"".equals(prop.getProperty("outbound_study"+".studycontact_lastname")
							.toString().trim())) {
				String pILastName = userIdentifier.getLastName();
				dataMap.put(ProtocolKeys.LastName, isEmpty(pILastName));

			}
		}else if(userIdentifier == null){
			dataMap.put(ProtocolKeys.LastName, "");
			dataMap.put(ProtocolKeys.FirstName,"");
			dataMap.put(ProtocolKeys.StudyContact,"");
		}
		
		if(userIdentifier1!=null){
			logger.info("***********UserIdentifer is not null*******");
			
			if(prop.containsKey("outbound_study"+".pIcontact")
					&& !"".equals(prop.getProperty("outbound_study"+".pIcontact")
							.toString().trim())) {
				String pIContact = userIdentifier1.getUserLoginName();
				dataMap.put(ProtocolKeys.PIContact,isEmpty(pIContact));
			}
			if(prop.containsKey("outbound_study"+".pIcontact_pIfirstname")
					&& !"".equals(prop.getProperty("outbound_study"+".pIcontact_pIfirstname")
							.toString().trim())) {
				dataMap.put(ProtocolKeys.PIFirstName,isEmpty(userIdentifier1.getFirstName()));
			}
			if(prop.containsKey("outbound_study"+".pIcontact_pIlastname")
					&& !"".equals(prop.getProperty("outbound_study"+".pIcontact_pIlastname")
							.toString().trim())) {
				String pILastName = userIdentifier1.getFirstName();
				dataMap.put(ProtocolKeys.PILastName,isEmpty(pILastName));

			}
		}else if(userIdentifier1 == null){
			logger.info("***********UserIdentifer1 is null*******");
			dataMap.put(ProtocolKeys.PILastName,"");
			dataMap.put(ProtocolKeys.PIFirstName, "");
			dataMap.put(ProtocolKeys.PIContact,"");
			
		}
		
		logger.info("NCTNumber --->"+ NctNumber);
		logger.info("\n**********************mapStudySummaryForEpic _ Method========>\n"+dataMap);
	}
	
	public String isEmpty(String s){
		if(s == null){
			return s="";
		}
		return s;
	   }
}
