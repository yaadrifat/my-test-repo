package com.velos.integration.core.messaging;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class PatientOutboundClientThread implements Runnable {

	private static Logger logger = Logger.getLogger(OutboundClientThread.class);

	@Override
	public void run() {
		logger.info("Inside run method : ");

		ClassPathXmlApplicationContext context= new ClassPathXmlApplicationContext(
				"ctms-config.xml");
	
		PatientOutboundClient patientClient = (PatientOutboundClient) context
				.getBean("patientOutboundClient");
		logger.info("PatientOutboundClient");
		try {
			patientClient.run();
		} catch (Exception e) {
			logger.info("error : " + e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			context.close();
		}
	}
}
