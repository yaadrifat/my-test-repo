package com.velos.integration.mapping;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.velos.epic.service.EpicEndpointClient;
import com.velos.services.AddStudyPatientStatusResponse;
import com.velos.services.CompletedAction;
import com.velos.services.CrudAction;
import com.velos.services.EnrollPatientToStudyResponse;
import com.velos.services.GetStudyPatientStatusHistoryResponse;
import com.velos.services.GetStudyPatientStatusResponse;
import com.velos.services.GetStudyPatientsResponse;
import com.velos.services.Issues;
import com.velos.services.ResponseHolder;
import com.velos.services.Results;
import com.velos.services.StudyPatient;
import com.velos.services.StudyPatientStatus;
import com.velos.services.StudyPatientStatuses;

public class VelosStudyPatientMapper {
	private static Logger logger = Logger.getLogger(VelosStudyPatientMapper.class);   
	private String endpoint = null;
	private Map<VelosKeys, Object> dataMap = new HashMap<VelosKeys, Object>();
	
	public VelosStudyPatientMapper(String endpoint) {
		this.endpoint = endpoint;
	}
	
	public Map<VelosKeys, Object> mapStudyPatientList(GetStudyPatientsResponse resp) {
		logger.info("\n**********VelosStudyPatientMapper********\n");
		if (EndpointKeys.Epic.toString().equals(endpoint)) {
			mapStudyPatientListForEpic(resp);
		}
		return dataMap;
	}
	private void mapStudyPatientListForEpic(GetStudyPatientsResponse resp) {
		List< Map<VelosKeys, Object> > dataStudyPatientList = new ArrayList< Map<VelosKeys, Object> >();
		List<StudyPatient> list = resp.getStudyPatient();
		for (StudyPatient studyPatient : list) {
			Map<VelosKeys, Object> map = new HashMap<VelosKeys, Object>();
			map.put(ProtocolKeys.Mrn, studyPatient.getPatientIdentifier().getPatientId());
			map.put(ProtocolKeys.StudyPatId, studyPatient.getStudyPatId());
			dataStudyPatientList.add(map);
		}
		dataMap.put(ProtocolKeys.StudyPatientList, dataStudyPatientList);
	}
	
	public Map<VelosKeys, Object> mapEnrollPatientToStudy(EnrollPatientToStudyResponse resp) {
		logger.info("\n**********mapEnrollPatientToStudy_Method********\n");
		if (EndpointKeys.Epic.toString().equals(endpoint)) {
			mapEnrollPatientToStudyForEpic(resp);
		}
		return dataMap;
	}
		
	private void mapEnrollPatientToStudyForEpic(EnrollPatientToStudyResponse resp) {
		System.out.println("mapEnrollPatientToStudyForEpic");
		ResponseHolder holder = resp.getResponse();
		Results results = holder.getResults();
		System.out.println("results="+results);
		Issues issues = holder.getIssues();
		System.out.println("issues="+issues);
		if(results!=null){
		List<CompletedAction> resultList = results.getResult();
			if(resultList!=null && !resultList.isEmpty()){
				for (CompletedAction action : resultList) {
					CrudAction crud = action.getAction();
					dataMap.put(ProtocolKeys.CompletedAction, crud.name());
				}
			}else{
				dataMap.put(ProtocolKeys.FaultString, "Patient is already linked to this study or Patient idendifier id not valid");
			}
		}else{
			dataMap.put(ProtocolKeys.FaultString, "Patient is already linked to this study or Patient idendifier id not valid");
		}
		logger.info("\n**********mapEnrollPatientToStudy_Method ============>\n"+dataMap);
	}
	public Map<VelosKeys, Object> mapAddStudyPatientStatus(AddStudyPatientStatusResponse resp) {
		logger.info("\n**********VelosStudyPatientMapper********\n");
		if (EndpointKeys.Epic.toString().equals(endpoint)) {
			mapAddStudyPatientStatusForEpic(resp);
		}
		return dataMap;
	}
	private void mapAddStudyPatientStatusForEpic(AddStudyPatientStatusResponse resp) {
		ResponseHolder holder = resp.getResponse();
		Results results = holder.getResults();
		List<CompletedAction> resultList = results.getResult();
		for (CompletedAction action : resultList) {
			CrudAction crud = action.getAction();
			dataMap.put(ProtocolKeys.CompletedAction, crud.name());
		}
		logger.info("\n**********mapAddStudyPatientStatusForEpic_Method ============>\n"+dataMap);
	}

	public Map<VelosKeys, Object> mapStudyPatientStatusHistory(GetStudyPatientStatusHistoryResponse resp) {
		logger.info("\n**********VelosStudyPatientMapper********\n");
		if (EndpointKeys.Epic.toString().equals(endpoint)) {
			mapStudyPatientStatusHistoryForEpic(resp);
		}
		return dataMap;
	}
	public Map<VelosKeys, Object> mapStudyPatientStatus(GetStudyPatientStatusResponse resp) {
		logger.info("\n**********VelosStudyPatientMapper********\n");
		if (EndpointKeys.Epic.toString().equals(endpoint)) {
			mapStudyPatientStatusForEpic(resp);
		}
		return dataMap;
	}
	
	private void mapStudyPatientStatusForEpic(GetStudyPatientStatusResponse resp) {
		
		String patStudyId=resp.getPatientEnrollmentDetails().getPatientStudyId();
		
		if(patStudyId != null){
		dataMap.put(ProtocolKeys.StudyPatId,patStudyId);
		}
		logger.info("\n**********mapStudyPatientStatusForEpic_Method ============>\n"+dataMap);
	}
	

	private void mapStudyPatientStatusHistoryForEpic(GetStudyPatientStatusHistoryResponse resp) {
		StudyPatientStatuses patientStudyStatus= resp.getPatientStudyStatuses();
		List<StudyPatientStatus> studyPatientStatusList= patientStudyStatus.getStudyPatientStatus();
		if(studyPatientStatusList!=null && !studyPatientStatusList.isEmpty()){
			StudyPatientStatus studyPatientStatus = studyPatientStatusList.get(0);
			String studyPatStatus=studyPatientStatus.getStudyPatStatus().getCode();
			logger.info("StudyPatientStatus ====>"+studyPatStatus);
			/*if(studyPatStatus.equalsIgnoreCase("enrolled")){
				studyPatStatus="Active";
			}*/
			dataMap.put(ProtocolKeys.StudyPatStatus,studyPatStatus );
			dataMap.put(ProtocolKeys.OID,studyPatientStatus.getStudyPatStatId().getOID());
		}
		
		logger.info("\n**********mapStudyPatientStatusHistoryForEpic_Method ============>\n"+dataMap);
	}
	
	
}
