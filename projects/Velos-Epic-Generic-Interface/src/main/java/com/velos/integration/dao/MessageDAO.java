package com.velos.integration.dao;

import javax.sql.DataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;

public class MessageDAO {
	
	private static Logger logger = Logger.getLogger(MessageDAO.class);
	
	private DataSource eresDataSource;
	private JdbcTemplate jdbcTemplate;
	
	public void setEresDataSource(DataSource eresDataSource) {
		this.eresDataSource = eresDataSource;
		this.jdbcTemplate = new JdbcTemplate(eresDataSource);
		logger.info("*******Jdbc Connection created successfully");	}
	
	public boolean saveMessageDetails(String messageType,String requestXML,String responseXML) {
		System.out.println("saveMessageDetails");
		
		String requestxml=requestXML.replace("\"","");
		String responsexml=responseXML.replace("\"","");

		logger.info("Audit RequestXML "+requestxml);
		logger.info("Audit RespnseXML "+responsexml);
		boolean isSuccess = false;
		try{
			String sql = "INSERT INTO VGDB_EPICMESSAGE_AUDIT_DETAILS"
					+"(PK_AUDIT,MESSAGE_TYPE,REQUEST_MESSAGE,RESPONSE_MESSAGE,CREATION_DATE)"
					+" VALUES(nextval('audit_seq'),?,?,?,current_timestamp)";
			jdbcTemplate.update(sql,new Object[]{messageType,requestxml,responsexml});
			isSuccess = true;
		}catch(Exception e){
			e.printStackTrace();
			isSuccess = false;
		}
		return isSuccess;
	}
	
}
