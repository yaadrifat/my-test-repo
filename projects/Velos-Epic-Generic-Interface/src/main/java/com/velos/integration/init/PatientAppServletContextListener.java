
package com.velos.integration.init;

	import javax.servlet.ServletContextEvent;
	import javax.servlet.ServletContextListener;

	import org.apache.log4j.Logger;
	import org.springframework.context.ApplicationContext;

	import com.velos.integration.core.messaging.PatientOutboundClientThread;

	public class PatientAppServletContextListener implements ServletContextListener {

		private static Logger logger = Logger.getLogger(PatientAppServletContextListener.class.getName());
		
		private ApplicationContext context;

		@Override
		public void contextDestroyed(ServletContextEvent arg0) {}

		@Override
		public void contextInitialized(ServletContextEvent arg0) {
			logger.info("Initiating Core");
			outBoundMessaging();
		}
		
		void outBoundMessaging() {
			(new Thread(new PatientOutboundClientThread())).start();
		}

	}
