package com.velos.integration.espclient;

import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.integration.Message;
import org.springframework.integration.MessageChannel;
import org.springframework.integration.MessageHandlingException;
import org.springframework.integration.core.MessagingTemplate;
import org.springframework.integration.core.PollableChannel;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.integration.xml.source.DomSourceFactory;
import org.springframework.stereotype.Component;
import org.springframework.ws.soap.SoapFaultDetail;
import org.springframework.ws.soap.SoapFaultDetailElement;
import org.springframework.ws.soap.client.SoapFaultClientException;
import org.springframework.ws.soap.security.wss4j.Wss4jSecurityInterceptor;
import org.w3c.dom.NodeList;

import com.velos.integration.mapping.EndpointKeys;
import com.velos.integration.mapping.MapUtil;
import com.velos.integration.mapping.ProtocolKeys;
import com.velos.integration.mapping.VelosKeys;


@Component
public class VelosEspClient {
	
	private static Logger logger = Logger.getLogger(VelosEspClient.class);  
	public static final String HARDCODED_ORG = "Velos";
	//public static final String HARDCODED_ORG = "C C Cancer Center";
	public static final String HARDCODED_PATSTUDYSTAT = "ACTIVITY_EXITED";
	Properties prop=null;
	
	public VelosEspClient(){
		System.out.println("VelosEspClient cons");
		try {
			prop = new Properties();
			prop.load(this.getClass().getClassLoader()
					.getResourceAsStream("config.properties"));
		} catch (IOException e) {
			
		}
	}

	private static final String FAULT_STR = "Fault";
	
	private static final String getStudystatusesTemplate=" <ser:getStudyStatuses xmlns:ser=\"http://velos.com/services/\">"
			+ "<StudyIdentifier>"
			+ "<studyNumber>%s</studyNumber>"
			+ "</StudyIdentifier>"
			+ "</ser:getStudyStatuses>";
	
	private static final String createPatientRequestTemplate = "<ser:createPatient xmlns:ser=\"http://velos.com/services/\">"+
			"<Patient>"+
			"<patientDemographics>"+
			"<dateOfBirth>%s</dateOfBirth>"+
			"<firstName>%s</firstName><lastName>%s</lastName><middleName>%s</middleName>"+
			"<address1>%s</address1>"+
			"<city>%s</city>"+
			"<state>%s</state>"+
			"<zipCode>%s</zipCode>"+
			"<country>%s</country>"+
			"<organization><siteName>%s</siteName></organization>"+
			"<patFacilityId>%s</patFacilityId>"+
			//"<patientCode>%s</patientCode>"+//not required
			"<survivalStatus><code>A</code></survivalStatus>"+
			"</patientDemographics>"+
			"</Patient>"+
			"</ser:createPatient>";
			
private static final String getPatientDetailsTemplate = "<ser:getPatientDetails xmlns:ser=\"http://velos.com/services/\">"+
			"<PatientIdentifier>"+
			"<OID>%s</OID>"+
			"<organizationId>"
			+"<siteName>%s</siteName>"
			+"</organizationId>"+
			"<patientId>%s</patientId>"+
			"</PatientIdentifier>"+
			"</ser:getPatientDetails>";

	private static final String studyPatStatusHistoryTemplate =" <ser:getStudyPatientStatusHistory xmlns:ser=\"http://velos.com/services/\">"+
			"<StudyIdentifier><studyNumber>%s</studyNumber></StudyIdentifier>"+	
			"<PatientIdentifier><OID>%s</OID></PatientIdentifier>"+
			"</ser:getStudyPatientStatusHistory>";
			
	
	private static final String studyRequestTemplate =
			"<ser:getStudySummary xmlns:ser=\"http://velos.com/services/\">" +
					"<StudyIdentifier>" +
					"   <studyNumber>%s</studyNumber>" +
					"</StudyIdentifier>" +
					"</ser:getStudySummary>";
	
	private static final String studyCalendarListRequestTemplate =
			"<ser:getStudyCalendarList xmlns:ser=\"http://velos.com/services/\">" +
					"<StudyIdentifier>" +
					"<studyNumber>%s</studyNumber>" +
					"</StudyIdentifier>" +
					"</ser:getStudyCalendarList>";
	
	private static final String studyCalendarRequestTemplate =
			"<ser:getStudyCalendar xmlns:ser=\"http://velos.com/services/\">" +
					"<CalendarIdentifier><PK>%s</PK></CalendarIdentifier>" +
					"</ser:getStudyCalendar>";
	
	private static final String studyPatientRequestTemplate =
			"<ser:getStudyPatients xmlns:ser=\"http://velos.com/services/\">"+
					"<StudyIdentifier>"+
					"<studyNumber>%s</studyNumber>"+
					"</StudyIdentifier>"+
					"</ser:getStudyPatients>";
	
	private static final String addStudyPatientStatusRequestTemplate =
			"<ser:addStudyPatientStatus xmlns:ser=\"http://velos.com/services/\">"+
					"<PatientIdentifier>"+
					"<organizationId><siteName>%s</siteName></organizationId>"+
					"<patientId>%s</patientId>"+
					"</PatientIdentifier>"+
					"<StudyIdentifier><studyNumber>%s</studyNumber></StudyIdentifier>"+
					"<PatientStudyStatus>"+
					"<currentStatus>true</currentStatus>"+
					"<enrollingSite><siteName>%s</siteName></enrollingSite>"+
					//"<patientStudyId>%s</patientStudyId>"+
					"<enrolledBy><userLoginName>%s</userLoginName></enrolledBy>"+
					"<status><code>%s</code><type>patStatus</type></status>"+
					"<statusDate>%s</statusDate>"+
					"</PatientStudyStatus>"+
					"</ser:addStudyPatientStatus>";
	
	private static final String pendingEnrollPatientRequestTemplate =
			"<ser:enrollPatientToStudy xmlns:ser=\"http://velos.com/services/\">"+
					"<PatientIdentifier>"+
					"<OID>%s</OID>"+
					"<organizationId><siteName>%s</siteName></organizationId>"+
					//"<patientId>%s</patientId>"+
					"</PatientIdentifier>"+
					"<StudyIdentifier><studyNumber>%s</studyNumber></StudyIdentifier>"+
					"<PatientEnrollmentDetails>"+
					"<currentStatus>true</currentStatus>"+
					"<enrolledBy><userLoginName>%s</userLoginName></enrolledBy>"+
					"<enrollingSite><siteName>%s</siteName></enrollingSite>"+
					//"<patientStudyId>%s</patientStudyId>"+
					"<status><code>enrolled</code><type>patStatus</type></status>"+
					"<statusDate>%s</statusDate>"+
					"</PatientEnrollmentDetails>"+
					"</ser:enrollPatientToStudy>";
/*	
	private static final String patDemogSearchPatientRequestTemplate1 =
			"<ser:searchPatient xmlns:ser=\"http://velos.com/services/\"><PatientSearch>"
					+ "<exactSearch>true</exactSearch>"
					+ "<patFirstName>%s</patFirstName><patLastName>%s</patLastName>"
					+ "<patOrganization><siteName>%s</siteName></patOrganization>"
					+ "<patientID>%s</patientID>"
					+"</PatientSearch></ser:searchPatient>";*/

	private static final String patDemogSearchPatientRequestTemplate1 =
			"<ser:searchPatient xmlns:ser=\"http://velos.com/services/\"><PatientSearch>"
					+ "<patFirstName>%s</patFirstName><patLastName>%s</patLastName>"
					+ "<patOrganization><siteName>%s</siteName></patOrganization>"
					//+ "<patientFacilityID>%s</patientFacilityID>"
					+ "<patientID>%s</patientID>"
					+ "</PatientSearch></ser:searchPatient>";
	
	private static final String patDemogSearchPatientRequestTemplate2 =
			"<ser:searchPatient xmlns:ser=\"http://velos.com/services/\"><PatientSearch>"
					//+ "<exactSearch>true</exactSearch>"
					+ "<patDateofBirth>%s</patDateofBirth>"
					+ "<patFirstName>%s</patFirstName><patLastName>%s</patLastName>"
					+ "<patOrganization><siteName>%s</siteName></patOrganization>"
					//+ "<patientFacilityID>%s</patientFacilityID>"
					+ "<patientID>%s</patientID>"
					+ "</PatientSearch></ser:searchPatient>";
	private static final String studyPatientSatusTemplate = "<ser:getStudyPatientStatus xmlns:ser=\"http://velos.com/services/\">"
			+ "<PatientStudyStatusIdentifier>"
			+ "<OID>%s</OID>"
			+ "</PatientStudyStatusIdentifier>"
			+ "</ser:getStudyPatientStatus>";
	
	
	
    
    public static void main(String[] args) throws Exception {
    	//VelosEspClient client = new VelosEspClient();
    	//Map<VelosKeys, String> requestMap = new HashMap<VelosKeys, String>();
    	//requestMap.put(ProtocolKeys.StudyNumber, "Adam-1");
    	
    	// Test study summary
    	//client.testStudySummary(requestMap);
    	
    	//getFormattedCurrentDate();
    	System.out.println("result=>"+(Math.random()*100));
    	// Test study patients
    	//client.testStudyPatient(requestMap);
    	
    	// Test study calendar
    	//client.testStudyCalendar(requestMap);
    	
    	// Test add new patient study status
    	//requestMap.put(ProtocolKeys.PatientId, "9998");
    	//requestMap.put(ProtocolKeys.SiteName, HARDCODED_ORG);
    	//String patStudyStat = EpicMaps.getEpicToVelosPatStudyStatMap().get(HARDCODED_PATSTUDYSTAT);
    	//if (null != patStudyStat){
	    // 	requestMap.put(ProtocolKeys.StudyPatStatus, patStudyStat);
	    //	client.testAddStudyPatientStatus(requestMap);
    	//}

    	// Test enroll patient to study
    	//requestMap.put(ProtocolKeys.PatientId, "444");
    	//requestMap.put(ProtocolKeys.SiteName, HARDCODED_ORG);
    	//client.testEnrollPatient(requestMap);
    	
    	// Test search patient
    	//requestMap.put(ProtocolKeys.Dob, "19861202");
    	//requestMap.put(ProtocolKeys.LastName, "Krystal");
    	//requestMap.put(ProtocolKeys.PatientID, "444");
    	//requestMap.put(ProtocolKeys.SiteName, HARDCODED_ORG);
    	//client.testSearchPatient(requestMap);
    }
    
    private void testStudySummary(Map<VelosKeys, String> requestMap) {
    	Map<VelosKeys, Object> dataMap = handleRequest(VelosEspMethods.StudyGetStudySummary, requestMap);
    	for (VelosKeys key : dataMap.keySet()) {
    		System.out.println("key="+key+"; value="+dataMap.get(key));
    	}
    }
    
    private void testAddStudyPatientStatus(Map<VelosKeys, String> requestMap) {
		handleRequest(VelosEspMethods.StudyPatAddStudyPatientStatus, requestMap);
	}

    private void testEnrollPatient(Map<VelosKeys, String> requestMap) {
		handleRequest(VelosEspMethods.StudyPatEnrollPatientToStudy, requestMap);
	}
	
	@SuppressWarnings("unchecked")
    private void testStudyPatient(Map<VelosKeys, String> requestMap) {
    	Map<VelosKeys, Object> dataMap = handleStudyPatientRequest(requestMap);
		List<Map<VelosKeys, Object>> studyPatientMap = (List<Map<VelosKeys, Object>>)dataMap.get(ProtocolKeys.StudyPatientList);
    	for (Map<VelosKeys, Object> map : studyPatientMap) {
    		System.out.println("study patient ID: "+map.get(ProtocolKeys.StudyPatId));
    	}
    }
    
    @SuppressWarnings("unchecked")
	private void testStudyCalendar(Map<VelosKeys, String> requestMap) {
    	Map<VelosKeys, Object> studyDataMap = handleStudyRequest(requestMap);
    	System.out.println("Study title: "+studyDataMap.get(ProtocolKeys.Title));
    	Map<VelosKeys, Object> calendarListDataMap = handleStudyCalendarListRequest(requestMap);
		List< Map<VelosKeys, Object> > calendarIdList = (List< Map<VelosKeys, Object> >)
				calendarListDataMap.get(ProtocolKeys.CalendarIdList);
    	for (Map<VelosKeys, Object> calendarMap : calendarIdList) {
    		System.out.println("Cal_Id="+calendarMap.get(ProtocolKeys.CalendarId)
    				+" Cal_Name="+calendarMap.get(ProtocolKeys.CalendarName));
    		requestMap.put(ProtocolKeys.CalendarId,
    				String.valueOf(calendarMap.get(ProtocolKeys.CalendarId)));
    		Map<VelosKeys, Object> calendarDataMap = handleStudyCalendarRequest(requestMap);
			List<Map<VelosKeys, Object>> visitList = (List<Map<VelosKeys, Object>>)
					calendarDataMap.get(ProtocolKeys.VisitList);
    		for (Map<VelosKeys, Object> visitMap : visitList) {
    			System.out.println("Visit_Name="+visitMap.get(ProtocolKeys.VisitName));
    			List<Map<VelosKeys, Object>> eventList = (List<Map<VelosKeys, Object>>)
    					visitMap.get(ProtocolKeys.EventList);
    			if (eventList == null) { continue; }
    			for (Map<VelosKeys, Object> eventMap : eventList) {
    				System.out.println("Event_name="+eventMap.get(ProtocolKeys.EventName));
    				System.out.println("Event_coverage="+eventMap.get(ProtocolKeys.CoverageType));
    			}
    		}
    	}
    }
    
    private void testSearchPatient(Map<VelosKeys, String> requestMap) {
    	Map<VelosKeys, Object> dataMap = handleRequest(VelosEspMethods.PatDemogSearchPatient, requestMap);
    	for (VelosKeys key : dataMap.keySet()) {
    		System.out.println("key="+key+"; value="+dataMap.get(key));
    	}
    }
	
	public Map<VelosKeys, Object> handleRequest(VelosEspMethods method, Map<VelosKeys, String> requestMap)  {
    	if (requestMap == null) { return null; }
		MessageChannel channel = null;
		String body = null;
        @SuppressWarnings("resource")
		ClassPathXmlApplicationContext context = null;
        boolean isAlertProtocolRequest = false;
		try{
              context= new ClassPathXmlApplicationContext("velos-espclient-req.xml");
		
        ArrayList<String> argList = new ArrayList<String>();
        // Call Velos WS
		logger.info("********VelosEspClient handleRequest************");
		String siteName = prop.getProperty("velos.organization");
		String userId = prop.getProperty("velos.userID");
		logger.info("userId="+userId);
		logger.info("siteName=="+siteName);
		//System.out.println("StudyStatuses=="+VelosEspMethods.StudyStat);
		logger.info("method=="+method);
		switch(method) {
		
		case StudyGetStudySummary:
	        channel = context.getBean("studyRequests", MessageChannel.class);
	        System.out.println("StudySummary");
	        body = String.format(studyRequestTemplate, requestMap.get(ProtocolKeys.StudyNumber));

			//logger.info("Soap Boady ---->"+body.toString());
			
			break;
		case StudyCalGetStudyCalendar:
	        channel = context.getBean("studyCalendarRequests", MessageChannel.class);
	        body = String.format(studyCalendarRequestTemplate, requestMap.get(ProtocolKeys.CalendarId));
			break;
		case StudyCalGetStudyCalendarList:
	        channel = context.getBean("studyCalendarRequests", MessageChannel.class);
	        body = String.format(studyCalendarListRequestTemplate, requestMap.get(ProtocolKeys.StudyNumber));
			break;
		case StudyPatGetStudyPatients:
	        channel = context.getBean("studyPatientRequests", MessageChannel.class);
	        body = String.format(studyPatientRequestTemplate, requestMap.get(ProtocolKeys.StudyNumber));
			break;
		case StudyPatAddStudyPatientStatus:
			isAlertProtocolRequest = true;
	        channel = context.getBean("studyPatientRequests", MessageChannel.class);
	        //argList.add(HARDCODED_ORG);
	        argList.add(siteName);
			argList.add(requestMap.get(ProtocolKeys.PatientId));
			argList.add(requestMap.get(ProtocolKeys.StudyNumber));
	        //argList.add(HARDCODED_ORG);
			argList.add(siteName);
			//argList.add(requestMap.get(ProtocolKeys.PatientId)+"-"+((int)(Math.random()*100)));
			//argList.add(requestMap.get(ProtocolKeys.StudyPatId));
			argList.add(userId);
			argList.add(requestMap.get(ProtocolKeys.StudyPatStatus));
			argList.add(this.getFormattedCurrentDate());
	        body = String.format(addStudyPatientStatusRequestTemplate, argList.toArray());
			break;
		case StudyPatEnrollPatientToStudy:
	        channel = context.getBean("studyPatientRequests", MessageChannel.class);
	        //argList.add(HARDCODED_ORG);
	        argList.add(requestMap.get(ProtocolKeys.OID));
	        argList.add(siteName);
			//argList.add(requestMap.get(ProtocolKeys.PatientId));
			argList.add(requestMap.get(ProtocolKeys.StudyNumber));
	        //argList.add(HARDCODED_ORG);
			//argList.add(requestMap.get(ProtocolKeys.PatientId)+"-"+((int)(Math.random()*100)));
			argList.add(userId);
			argList.add(siteName);
			argList.add(this.getFormattedCurrentDate());
	        body = String.format(pendingEnrollPatientRequestTemplate, argList.toArray());
	        

			//logger.info("Soap Boady ---->"+body.toString());
			
			break;
		case PatDemogSearchPatient:
			channel = context.getBean("patientDemographicsRequests", MessageChannel.class);
			String formattedDob = requestMap.get(ProtocolKeys.Dob);
			System.out.println("formattedDob=="+formattedDob);
			if (formattedDob != null) {
				argList.add(formattedDob);
			}
			argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.FirstName)));
			argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.LastName)));
			//argList.add(MapUtil.nullToEmpty(requestMap.get(mapPatientDetailsForEpicProtocolKeys.SiteName)));
			//argList.add(MapUtil.nullToEmpty(HARDCODED_ORG));
			argList.add(MapUtil.nullToEmpty(siteName));
			argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.PatientFacilityId)));
			//argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.PatientID)));
			
			
			body = String.format(formattedDob == null ? patDemogSearchPatientRequestTemplate1 : 
					patDemogSearchPatientRequestTemplate2, argList.toArray());

			//logger.info("Soap Boady ---->"+body.toString());
			
			break;
		case PatientStudyGetStudyPatientStatusHistory:
			
			channel = context.getBean("studyPatientStatusHistoryRequests", MessageChannel.class);
		/*	argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.FirstName)));
			argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.LastName)));*/
			argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.StudyId)));
			argList.add(requestMap.get(ProtocolKeys.OID));
			//argList.add(MapUtil.nullToEmpty(siteName));
			//argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.SiteName)));
			//argList.add(MapUtil.nullToEmpty(HARDCODED_ORG));
			//argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.PatientID)));
			
			body = String.format(studyPatStatusHistoryTemplate, argList.toArray());

			//logger.info("Soap Boady ---->"+body.toString());
			
			break;
		case CreatePatient:
			//createPatientRequests
			channel = context.getBean("createPatientRequests", MessageChannel.class);
			argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.Dob)));
			argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.FirstName)));
			argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.LastName)));
			argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.MiddleName)));
			argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.StreetAddressLine)));
			argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.City)));
			argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.State)));
			argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.PostalCode)));
			argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.Country)));
			argList.add(MapUtil.nullToEmpty(siteName));
			argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.PatientFacilityId)));
			//argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.PatientFacilityId)));
			body = String.format(createPatientRequestTemplate, argList.toArray());
			
			//logger.info("Soap Boady ---->"+body.toString());
			
			break;
		case PatientDetails:
			channel = context.getBean("getPatientDetailsRequests", MessageChannel.class);
			argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.OID)));
			argList.add(MapUtil.nullToEmpty(siteName));
			argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.PatientID)));
			body = String.format(getPatientDetailsTemplate, argList.toArray());
			
			///logger.info("Soap Boady ---->"+body.toString());
			
			break;	
		
		case StudyPatientStatus:
			channel = context.getBean("getStudyPatientStatus", MessageChannel.class);
			argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.StudyPatOID)));
			body = String.format(studyPatientSatusTemplate, argList.toArray());
			

			//logger.info("Soap Boady ---->"+body.toString());
			
			break;
		
		case StudyGetStudyStat:
			System.out.println("********StudyStat********");
 			 logger.info("***********StudyStat******************");
			 channel = context.getBean("studyStatusRequests", MessageChannel.class);
		     body = String.format(getStudystatusesTemplate, requestMap.get(ProtocolKeys.StudyNumber));
		     logger.info("StudyStatuses request="+body);
			 break;
			 
		default:
			System.out.println("******default**********");
			logger.info("***************default***************");
			channel = context.getBean("studyStatusRequests", MessageChannel.class);
		    body = String.format(getStudystatusesTemplate, requestMap.get(ProtocolKeys.StudyNumber));
		    logger.info("StudyStatuses request="+body);
			break;
		}
		
		}
		System.out.println("Sent:"+body.toString());
		logger.info("Sent Request ---->"+body.toString());
        MessagingTemplate messagingTemplate = new MessagingTemplate();
        Message<?> message = null;
        try {
        	message = messagingTemplate.sendAndReceive(
                channel, MessageBuilder.withPayload(body).build());
        } catch(MessageHandlingException e) {
        	e.printStackTrace();
        	message = extractOperationException(e);
        	System.out.println("message="+message);
        	DOMSource source = (DOMSource)new DomSourceFactory().createSource(message.getPayload());
        	if (FAULT_STR.equals(source.getNode().getLocalName())) {
        		NodeList nodeList = source.getNode().getChildNodes();
        		for (int iX = 0; iX < nodeList.getLength(); iX++) {
        			if (ProtocolKeys.FaultString.toString().equals(nodeList.item(iX).getLocalName())) {
        				Map<VelosKeys, Object> retMap = new HashMap<VelosKeys, Object>();
        				retMap.put(ProtocolKeys.FaultString, nodeList.item(iX).getTextContent());
        				return retMap;
        			}
        		}
        	}
        }finally{
        	context.close();
        }
        
        System.out.println("Got :"+message.getPayload());
        
        logger.info("Got Response---------->:"+message.getPayload());
        
        Map<VelosKeys, Object> dataMap=transformMessageToMap(message, requestMap.get(EndpointKeys.Endpoint));
        // Transform XML to object to Map
    	//return transformMessageToMap(message, requestMap.get(EndpointKeys.Endpoint));
        
        System.out.println("Patient DATA MAP -->"+dataMap);
        //System.out.println("erroList="+dataMap.containsKey(ProtocolKeys.ErrorList));
        if(isAlertProtocolRequest){
	        if(dataMap!=null && dataMap.containsKey(ProtocolKeys.ErrorList)){
	        	ArrayList errorList = (ArrayList)dataMap.get(ProtocolKeys.ErrorList);
	        	if(errorList==null || errorList.isEmpty()){
	        		dataMap.put(ProtocolKeys.FaultString, "Patient is not enrolled to this study number...");
	        	}
	        }
        }else{
        	//System.out.println();
        }
        return dataMap;
	}

	private final TransformerFactory tfactory = TransformerFactory.newInstance();
	
	private Message<?> extractOperationException(MessageHandlingException messageException) {
    	SoapFaultClientException se = (SoapFaultClientException) messageException.getCause();
    	SoapFaultDetail detail = se.getSoapFault().getFaultDetail();
    	StringWriter writer = new StringWriter();
    	Source source = null;
        Result result = new StreamResult(writer);
        if (detail == null || detail.getDetailEntries() == null) {
        	source = se.getSoapFault().getSource(); 
        } else {
            SoapFaultDetailElement elem = detail.getDetailEntries().next();
        	source = elem.getSource();
        }
        Transformer xform = null;
        try {
        	xform = tfactory.newTransformer();
            xform.transform(source, result);
        } catch(Exception e1) {
        	e1.printStackTrace();
        }
        System.out.println(writer.toString());
        Message<?> message = MessageBuilder.withPayload(writer.toString()).build();
		return message;
	}
	
    public Map<VelosKeys, Object> handleStudyRequest(Map<VelosKeys, String> requestMap) {
    	return handleRequest(VelosEspMethods.StudyGetStudySummary, requestMap);
    }
    
    public Map<VelosKeys, Object> handleStudyCalendarRequest(Map<VelosKeys, String> requestMap) {
    	return handleRequest(VelosEspMethods.StudyCalGetStudyCalendar, requestMap);
    }
    
    public Map<VelosKeys, Object> handleStudyPatientRequest(Map<VelosKeys, String> requestMap) {
    	return handleRequest(VelosEspMethods.StudyPatGetStudyPatients, requestMap);
    }
    
    public Map<VelosKeys, Object> handleStudyCalendarListRequest(Map<VelosKeys, String> requestMap) {
    	return handleRequest(VelosEspMethods.StudyCalGetStudyCalendarList, requestMap);
    }
    
    private Map<VelosKeys, Object> transformMessageToMap(Message<?> message, String endpoint) {
    	System.out.println("transformMessageToMap starts="+message);
    	System.out.println("transformMessageToMap endpoint"+endpoint);
        @SuppressWarnings("resource")
		ClassPathXmlApplicationContext context = null;
        VelosMessageHandler messageHandler = null;
        try{
		context= new ClassPathXmlApplicationContext("velos-espclient-resp.xml");
        System.out.println("transformMessageToMap starts1");
        MessageChannel channelXml = context.getBean("input-xml", MessageChannel.class);
        PollableChannel output = context.getBean("output", PollableChannel.class);
        // Transform XML to object
        channelXml.send(message);
        Message<?> reply = output.receive();
        System.out.println("transformMessageToMap starts2="+reply);
        // Transform object to map
        messageHandler = context.getBean(VelosMessageHandler.class);
        messageHandler.setEndpoint(endpoint);
        messageHandler.handleMessage(reply);
        System.out.println("transformMessageToMap starts3");
        }finally{
        	context.close();
        }
        return messageHandler.getDataMap();
    }
    
    /*private String getFormattedCurrentDate() {
    	SimpleDateFormat f = new SimpleDateFormat("MM-dd-yyyy'T'HH:mm:ss");
    	return f.format(Calendar.getInstance().getTime());
    }
    */
    private String getFormattedCurrentDate() {
    	SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
    	return f.format(Calendar.getInstance().getTime());
    }
    @Bean
    public Wss4jSecurityInterceptor wssBean() {
    	String userId = prop.getProperty("velos.userID");
    	String password = prop.getProperty("velos.password");
    	Wss4jSecurityInterceptor wssBean = new Wss4jSecurityInterceptor();
    	wssBean.setSecurementActions("UsernameToken");
    	wssBean.setSecurementUsername(userId);
    	wssBean.setSecurementPassword(password);
    	wssBean.setSecurementPasswordType("PasswordText");
    	return wssBean;
    }
    
}
