package com.velos.integration.webservice;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import com.velos.services.ObjectInfo;
import com.velos.services.OperationException_Exception;
import com.velos.services.SimpleIdentifier;

@WebService(targetNamespace = "http://velos.com/services/", name = "SystemAdministrationSEI")
public interface VelosEspSystemAdministrationEndpoint {

	@WebResult(name = "ObjectInfo", targetNamespace = "")
	@RequestWrapper(localName = "getObjectInfoFromOID", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetObjectInfoFromOID")
	@WebMethod
	@ResponseWrapper(localName = "getObjectInfoFromOIDResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetObjectInfoFromOIDResponse")
	public com.velos.services.ObjectInfo getObjectInfoFromOID(
			@WebParam(name = "arg0", targetNamespace = "") com.velos.services.SimpleIdentifier arg0)
			throws OperationException_Exception;

}
