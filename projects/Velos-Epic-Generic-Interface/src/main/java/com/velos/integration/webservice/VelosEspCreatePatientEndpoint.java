package com.velos.integration.webservice;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;
import com.velos.services.OperationException_Exception;

@WebService(targetNamespace = "http://velos.com/services/", name = "PatientDemographicsSEI")
public interface VelosEspCreatePatientEndpoint {
	@WebResult(name = "Response", targetNamespace = "")
	@RequestWrapper(localName = "createPatient", targetNamespace = "http://velos.com/services/", className = "com.velos.services.CreatePatient")
	@WebMethod
	@ResponseWrapper(localName = "createPatientResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.CreatePatientResponse")
	public com.velos.services.ResponseHolder createPatient(
			@WebParam(name = "Patient", targetNamespace = "") com.velos.services.Patient patient)
			throws OperationException_Exception;
}
