package com.velos.integration.mapping;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.log4j.Logger;

import scala.annotation.meta.getter;
import scala.sys.Prop;

import com.ibm.icu.text.SimpleDateFormat;
import com.velos.outbound.servlet.EpicServlet;
import com.velos.services.CompletedAction;
import com.velos.services.CreatePatientResponse;
import com.velos.services.GetPatientDetailsResponse;
import com.velos.services.Issue;
import com.velos.services.Issues;
import com.velos.services.Patient;
import com.velos.services.PatientDataBean;
import com.velos.services.PatientDemographics;
import com.velos.services.PatientSearchResponse;
import com.velos.services.ResponseHolder;
import com.velos.services.Results;
import com.velos.services.SearchPatientResponse;

public class VelosPatientMapper {
	
	private static Logger logger = Logger.getLogger(VelosPatientMapper.class);

	private Map<VelosKeys, Object> dataMap = new HashMap<VelosKeys, Object>();
	private String endpoint = null;
	Properties prop=null;

	public VelosPatientMapper(String endpoint) {
		this.endpoint = endpoint;
		prop =new Properties();
		try {
			prop.load(this.getClass().getClassLoader()
					.getResourceAsStream("epic.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	//SearchPatient
	public Map<VelosKeys, Object> mapSearchPatient(SearchPatientResponse resp) {
		logger.info("\n***********VelosPatientMapper*********\n");
		if (EndpointKeys.Epic.toString().equals(endpoint)) {
			mapSearchPatientHelperForEpic(resp);
		}
		return dataMap;
	}

	//SearchPatient
	private void mapSearchPatientHelperForEpic(SearchPatientResponse resp) {
		logger.info("\n************mapSearchPatientHelperForEpic********\n");
		PatientSearchResponse searchResp = resp.getPatientSearchResponse();

		int totalCount = searchResp.getTotalCount();
		if (totalCount < 1) {
			dataMap.put(ProtocolKeys.FaultString, ProtocolKeys.PatientNotFoundMsg.toString());
			return;
		}
		if (totalCount > 1) {
			dataMap.put(ProtocolKeys.FaultString, ProtocolKeys.MultiplePatientsFoundMsg.toString());
			return;
		}
		dataMap.put(ProtocolKeys.TotalCount, searchResp.getTotalCount());
		List<PatientDataBean> patList = searchResp.getPatDataBean();

		for (PatientDataBean patBean : patList) {

			if(prop.containsKey("outbound_patient"+".patientId")
					&& !"".equals(prop.getProperty("outbound_patient"+".patientId")
							.toString().trim())) {
				dataMap.put(ProtocolKeys.PatientID, patBean.getPatientIdentifier().getPatientId());
				dataMap.put(ProtocolKeys.PatientID, patBean.getPatientIdentifier().getPatientId());
			}


			if(prop.containsKey("outbound_patient"+".firstname")
					&& !"".equals(prop.getProperty("outbound_patient"+".firstname")
							.toString().trim())) {
				dataMap.put(ProtocolKeys.FirstName, patBean.getPatFirstName());
			}

			if(prop.containsKey("outbound_patient"+".lastname")
					&& !"".equals(prop.getProperty("outbound_patient"+".lastname")
							.toString().trim())) {
				dataMap.put(ProtocolKeys.LastName, patBean.getPatLastName());
			}
			if(prop.containsKey("outbound_patient"+".dob")
					&& !"".equals(prop.getProperty("outbound_patient"+".dob")
							.toString().trim())) {
				dataMap.put(ProtocolKeys.Dob, patBean.getPatDateofBirth());
			}
			if(prop.containsKey("outbound_patient"+".OID")
					&& !"".equals(prop.getProperty("outbound_patient"+".OID")
							.toString().trim())) {
				dataMap.put(ProtocolKeys.OID, patBean.getPatientIdentifier().getOID());
			}

			dataMap.put(ProtocolKeys.TotalCount, 1);
		}

		logger.info("\nSearch Patient Data Map===>"+dataMap);
	}

	//Get Patient Details
	public Map<VelosKeys, Object> mapPatientDetails(GetPatientDetailsResponse resp) throws ParseException {
		logger.info("\n***********VelosPatientMapper*********\n");
		if (EndpointKeys.Epic.toString().equals(endpoint)) {
			mapGetPatientDetailsHelperforEpic(resp);
		}
		return dataMap;
	}

	//Get Patient Details
	private void mapGetPatientDetailsHelperforEpic(
			GetPatientDetailsResponse resp) throws ParseException {
		logger.info("\n*********mapGetPatientDetailsHelperforEpic**********\n");
		Patient patient = resp.getPatientDetails();
		PatientDemographics patientDemo= patient.getPatientDemographics();

		if(prop.containsKey("outbound_patient"+".patientId")
				&& !"".equals(prop.getProperty("outbound_patient"+".patientFacilityId")
						.toString().trim())) {
			dataMap.put(ProtocolKeys.PatientFacilityId,patientDemo.getPatFacilityId());
		}

		if(prop.containsKey("outbound_patient"+".firstname")
				&& !"".equals(prop.getProperty("outbound_patient"+".firstname")
						.toString().trim())) {
			dataMap.put(ProtocolKeys.FirstName,patientDemo.getFirstName());
		}

		if(prop.containsKey("outbound_patient"+".lastname")
				&& !"".equals(prop.getProperty("outbound_patient"+".lastname")
						.toString().trim())) {
			dataMap.put(ProtocolKeys.LastName,patientDemo.getLastName());
		}

		if(prop.containsKey("outbound_patient"+".middlename")
				&& !"".equals(prop.getProperty("outbound_patient"+".middlename")
						.toString().trim())) {

			dataMap.put(ProtocolKeys.MiddleName,patientDemo.getMiddleName());
		}

		if(prop.containsKey("outbound_patient"+".dob")
				&& !"".equals(prop.getProperty("outbound_patient"+".dob")
						.toString().trim())) {
			Date date =toDate(patientDemo.getDateOfBirth());
			logger.info("Date  -------->"+date);
			String dateformat=prop.getProperty("outbound_patientdob.format");
			SimpleDateFormat sdf=new SimpleDateFormat(dateformat);
			//Date input=sdf.parse(sdf.format(date));
			String Dob = sdf.format(date);
			logger.info("Epic Date =====>"+Dob);
			dataMap.put(ProtocolKeys.Dob,Dob);
		}
		if(prop.containsKey("outbound_patient"+".OID")
				&& !"".equals(prop.getProperty("outbound_patient"+".OID")
						.toString().trim())) {
			dataMap.put(ProtocolKeys.OID,patientDemo.getPatientIdentifier().getOID());
		}
		if(prop.containsKey("outbound_patient"+".address1")
				&& !"".equals(prop.getProperty("outbound_patient"+".address1")
						.toString().trim())) {
			dataMap.put(ProtocolKeys.StreetAddressLine,patientDemo.getAddress1());
		}
		if(prop.containsKey("outbound_patient"+".city")
				&& !"".equals(prop.getProperty("outbound_patient"+".city")
						.toString().trim())) {
			dataMap.put(ProtocolKeys.City, patientDemo.getCity());
		}

		if(prop.containsKey("outbound_patient"+".state")
				&& !"".equals(prop.getProperty("outbound_patient"+".state")
						.toString().trim())) {

			dataMap.put(ProtocolKeys.State,patientDemo.getState());
		}

		if(prop.containsKey("outbound_patient"+".zipCode")
				&& !"".equals(prop.getProperty("outbound_patient"+".zipCode")
						.toString().trim())) {

			dataMap.put(ProtocolKeys.PostalCode,patientDemo.getZipCode());
		}

		if(prop.containsKey("outbound_patient"+".country")
				&& !"".equals(prop.getProperty("outbound_patient"+".country")
						.toString().trim())) {

			dataMap.put(ProtocolKeys.Country,patientDemo.getCountry());
		}

		if(prop.containsKey("outbound_patient"+".address2")
				&& !"".equals(prop.getProperty("outbound_patient"+".address2")
						.toString().trim())) {

			dataMap.put(ProtocolKeys.Address,patientDemo.getAddress2());
		}

		if(prop.containsKey("outbound_patient"+".county")
				&& !"".equals(prop.getProperty("outbound_patient"+".county")
						.toString().trim())) {

			dataMap.put(ProtocolKeys.County,patientDemo.getCounty());
		}

		if(prop.containsKey("outbound_patient"+".emailid")
				&& !"".equals(prop.getProperty("outbound_patient"+".email")
						.toString().trim())) {

			dataMap.put(ProtocolKeys.EmailId,patientDemo.getEMail());
		}

		if(prop.containsKey("outbound_patient"+".homephoneno")
				&& !"".equals(prop.getProperty("outbound_patient"+".homephoneno")
						.toString().trim())) {

			dataMap.put(ProtocolKeys.HomePhoneNo,patientDemo.getHomePhone());
		}

		if(prop.containsKey("outbound_patient"+".workphoneno")
				&& !"".equals(prop.getProperty("outbound_patient"+".workphoneno")
						.toString().trim())) {

			dataMap.put(ProtocolKeys.WorkPhoneNo,patientDemo.getWorkPhone());
		}

		if(prop.containsKey("outbound_patient"+".ssn")
				&& !"".equals(prop.getProperty("outbound_patient"+".ssn")
						.toString().trim())) {

			dataMap.put(ProtocolKeys.Ssn,patientDemo.getSSN());
		}

		logger.info("\nPatientDetailsForEpic =======>"+dataMap);
	}

	public static Date toDate(XMLGregorianCalendar calendar){
		if(calendar == null) {
			return null;
		}
		return calendar.toGregorianCalendar().getTime();
	}

	//Create Patient
	public Map<VelosKeys, Object> mapCreatePatient(CreatePatientResponse resp) {
		logger.info("\n***********VelosPatientMapper*********\n");
		if (EndpointKeys.Epic.toString().equals(endpoint)) {
			mapCreatePatientForEpic(resp);
		}
		return dataMap;
	}
	//Create Patient
	private void mapCreatePatientForEpic(CreatePatientResponse resp) {
		logger.info("**********mapCreatePatientForEpic_Method********");
		ResponseHolder patientResp = resp.getResponse();
		Results results = patientResp.getResults();
		Issues issues = patientResp.getIssues();

		if(issues!=null){
			List<Issue> issueList = issues.getIssue();
			if(issueList!=null && !issueList.isEmpty()){
				for(Issue i:issueList){
					dataMap.put(ProtocolKeys.FaultString, i.getType());
					return;
				}
			}
		}
		if(results!=null){
			List<CompletedAction> resultList= results.getResult();
			if(resultList!=null && !resultList.isEmpty()){
				CompletedAction action = resultList.get(0);
				dataMap.put(ProtocolKeys.OID,action.getObjectId().getOID());
				dataMap.put(ProtocolKeys.PK,action.getObjectId().getPK());
				//dataMap.put(ProtocolKeys.PatientId,action.getObjectId().get);
			}
		}
	}
}
