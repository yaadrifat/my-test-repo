package com.velos.integration.webservice;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import com.velos.services.OperationException_Exception;
import com.velos.services.OperationRolledBackException_Exception;
import com.velos.services.ResponseHolder;



@WebService(targetNamespace="http://velos.com/services/",name="StudyPatientSEI")
public interface VelosEspEnrollPatientToStudyEndpoint {
	@WebResult(name="ResponseHolder", targetNamespace ="")
	@RequestWrapper(localName ="enrollPatientToStudy" ,targetNamespace ="http://velos.com/services/",className = "com.velos.services.EnrollPatientToStudy")
	@WebMethod
	@ResponseWrapper(localName="enrollPatientToStudyResponse", targetNamespace = "http://velos.com/services/",className = "com.velos.services.EnrollPatientToStudyResponse")

	public ResponseHolder enrollPatientToStudy(
			@WebParam( name ="PatientIdentifier",targetNamespace = "") com.velos.services.PatientIdentifier patientIdentifier, 
			@WebParam( name ="StudyIdentifier",targetNamespace = "") com.velos.services.StudyIdentifier studyIdentifier,
			@WebParam( name ="PatientEnrollmentDetails",targetNamespace = "")com.velos.services.PatientEnrollmentDetails patientEnrollmentDetails
			
			) throws OperationException_Exception, OperationRolledBackException_Exception;
}
