package com.velos.integration.notifications;

public class OperationCustomException extends Exception {

	/**
	 * @author msurendra
	 */
	
	private String responseType; 
	
	private static final long serialVersionUID = 1L;

	public OperationCustomException(){
		super();

	}

	public OperationCustomException(String message){
		super(message);
	}

	public OperationCustomException(String message,String responseType){
		super(message);
		this.responseType=responseType;
	}
	
	
	
	public String getResponseType() {
		return responseType;
	}

	public void setResponseType(String responseType) {
		this.responseType = responseType;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
