
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for addScheduleEventStatusResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="addScheduleEventStatusResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="addScheduleEventStatus" type="{http://velos.com/services/}responseHolder" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "addScheduleEventStatusResponse", propOrder = {
    "addScheduleEventStatus"
})
public class AddScheduleEventStatusResponse {

    protected ResponseHolder addScheduleEventStatus;

    /**
     * Gets the value of the addScheduleEventStatus property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseHolder }
     *     
     */
    public ResponseHolder getAddScheduleEventStatus() {
        return addScheduleEventStatus;
    }

    /**
     * Sets the value of the addScheduleEventStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseHolder }
     *     
     */
    public void setAddScheduleEventStatus(ResponseHolder value) {
        this.addScheduleEventStatus = value;
    }

}
