
package com.velos.services;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for multipleChoices complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="multipleChoices">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="autoSequence" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="choice" type="{http://velos.com/services/}multipleChoice" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="columnCount" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="hideResponses" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="topAlignResponses" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "multipleChoices", propOrder = {
    "autoSequence",
    "choice",
    "columnCount",
    "hideResponses",
    "topAlignResponses"
})
public class MultipleChoices
    extends ServiceObject
{

    protected boolean autoSequence;
    @XmlElement(nillable = true)
    protected List<MultipleChoice> choice;
    protected Integer columnCount;
    protected boolean hideResponses;
    protected boolean topAlignResponses;

    /**
     * Gets the value of the autoSequence property.
     * 
     */
    public boolean isAutoSequence() {
        return autoSequence;
    }

    /**
     * Sets the value of the autoSequence property.
     * 
     */
    public void setAutoSequence(boolean value) {
        this.autoSequence = value;
    }

    /**
     * Gets the value of the choice property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the choice property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getChoice().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MultipleChoice }
     * 
     * 
     */
    public List<MultipleChoice> getChoice() {
        if (choice == null) {
            choice = new ArrayList<MultipleChoice>();
        }
        return this.choice;
    }

    /**
     * Gets the value of the columnCount property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getColumnCount() {
        return columnCount;
    }

    /**
     * Sets the value of the columnCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setColumnCount(Integer value) {
        this.columnCount = value;
    }

    /**
     * Gets the value of the hideResponses property.
     * 
     */
    public boolean isHideResponses() {
        return hideResponses;
    }

    /**
     * Sets the value of the hideResponses property.
     * 
     */
    public void setHideResponses(boolean value) {
        this.hideResponses = value;
    }

    /**
     * Gets the value of the topAlignResponses property.
     * 
     */
    public boolean isTopAlignResponses() {
        return topAlignResponses;
    }

    /**
     * Sets the value of the topAlignResponses property.
     * 
     */
    public void setTopAlignResponses(boolean value) {
        this.topAlignResponses = value;
    }

}
