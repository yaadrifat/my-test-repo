
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getCurrentPatientScheduleResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getCurrentPatientScheduleResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CurrentPatientSchedule" type="{http://velos.com/services/}patientSchedule" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getCurrentPatientScheduleResponse", propOrder = {
    "currentPatientSchedule"
})
@XmlRootElement(name="getCurrentPatientScheduleResponse", namespace = "http://velos.com/services/")
public class GetCurrentPatientScheduleResponse {

    @XmlElement(name = "CurrentPatientSchedule")
    protected PatientSchedule currentPatientSchedule;

    /**
     * Gets the value of the currentPatientSchedule property.
     * 
     * @return
     *     possible object is
     *     {@link PatientSchedule }
     *     
     */
    public PatientSchedule getCurrentPatientSchedule() {
        return currentPatientSchedule;
    }

    /**
     * Sets the value of the currentPatientSchedule property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientSchedule }
     *     
     */
    public void setCurrentPatientSchedule(PatientSchedule value) {
        this.currentPatientSchedule = value;
    }

}
