<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_MngPat_New%><%--<%=LC.Pat_Patient%> >> New*****--%></title>
<style>
.pageInst {
	position: relative;
	left: 650px;
	top: 40px;
	width: 550px;
	height: 80px;
	background: #FFFF99;
	border-width: 1px;
	border-style: solid;
	border-color: #FFBD32;
	visibility: hidden; over;
	overflow: auto;
}

.studyInst{
  height:0px;
  left:0;
  overflow:visible;
  position:relative;
  text-align:left;
  top:0;
  visibility:hidden;
  width:600px;
}

.transparent {
	border-width: 1px;
	border-color: red;
	width: 100%;
	height: 100%;
	top: 130px;
	left: 150px;
	z-index: 998;
	visibility: visible;
	filter: alpha(opacity = 55);
	opacity: .55;
	background-color: #EEE;
	position: absolute;
}

</style>
</head>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<!-- Gopu : import the objects for customized field -->
<%@ page import="org.json.*,com.velos.remoteservice.demographics.*,com.velos.eres.service.util.*,com.velos.eres.web.user.ConfigFacade,com.velos.eres.web.user.ConfigObject, com.velos.eres.web.user.ConfigDetailsObject,com.velos.eres.business.common.CommonDAO"%>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="js/velos/patsearch.js" type="text/javascript"></script>
<SCRIPT LANGUAGE="JavaScript">
var dd1, delay;

function setDIVContent(){
	$("wsDIV").innerHTML="<DIV id='indicator' style='float:left;width:100%;top:5%'><img width='5%' src='../images/indicator.gif' align='absmiddle'>&nbsp;<font size='2'><%=MC.M_FetchingData_PlsWait%><%--Fetching Data,Please Wait...*****--%></font></DIV>"
}

function fetchData()
{
	org=$("patientForm").orgDD;
	var orgDisp=org.options[org.selectedIndex].text;
	//orgId=org.options[org.selectedIndex].text;
	orgId=org.value;
	pat=document.getElementById("patid");
	if ((pat.value.length==0) || (orgId.length==0))
	{
	alert("<%=MC.M_PatId_OrgMndt%>");/*alert("Patient Id & Organization both are mandatory.Please fix and try again.");*****/
	pat.focus();
	return false;
	}
	setDIVContent();
	site=$("siteId");
	site.value=org.value;


new Ajax.Request('/eres/jsp/wsresultDemo.jsp?MRN='+pat.value+'&hCFacility='+orgId,{asynchronous:true,
	onSuccess:function(transport)
	{
	 if (transport.status==200)
	 {
		 document.getElementById("trans").style.visibility="hidden";
	 	document.getElementById("wsDIV").innerHTML=transport.responseText;
	  	var fname=document.getElementById("wsPatFName").value;
	 	var lname=document.getElementById("wsPatLName").value

	  	if (fname.length>0) {
	  	fname=fname=fname.replace("'","`");

	 	}
	 	if (lname.length>0) {
	 		lname=lname.replace("'","`");

	 	}


	 	updateSourceField(document.getElementById("patfname"), fname);
	 	updateSourceField(document.getElementById("patlname"), lname);
	 	updateSourceField(document.getElementById("patdob"),document.getElementById("wsPatDOB").value);
	 	updateSourceField(document.getElementById("otherdthCause"), document.getElementById("wsPatCauseOfDeathOther").value);


	 	updateSourceField(document.getElementById("patientForm").patrace, dereferenceCode(document.getElementById("wsPatRace").value, "race"));
		updateSourceField(document.getElementById("patientForm").patethnicity, dereferenceCode(document.getElementById("wsPatEthnicity").value, "ethnicity"));
		updateSourceField(document.getElementById("patientForm").patsex, dereferenceCode(document.getElementById("wsPatSex").value, "gender"));

		updateSourceField(document.getElementById('aliasname'),pat.value);
		updateSourceField(document.getElementById('patadd1'),document.getElementById("wsPatAddress1").value);
		updateSourceField(document.getElementById('patadd2'),document.getElementById("wsPatAddress2").value);
		updateSourceField(document.getElementById('patcity'),document.getElementById("wsPatCity").value);
		updateSourceField(document.getElementById('patstate'),document.getElementById("wsPatState").value);
		updateSourceField(document.getElementById('patcounty'),document.getElementById("wsPatCounty").value);
		updateSourceField(document.getElementById('patcountry'),document.getElementById("wsPatCountry").value);
		updateSourceField(document.getElementById('patzip'),document.getElementById("wsPatZip").value);



	  	if (document.getElementById("wsPatId").value.length>0) {
			document.getElementById("trans").style.visibility="hidden";
		  //Select the organization for botton DD based on what was selected above
	  	for (var i=0;i<site.options.length;i++)
		{
			if (site.options[i].text==orgDisp)
	 		{
	  			site.selectedIndex=i;
	  			break;
	 		}
		}
	//end selecting organization

	 	}

	 }
	},
	onLoading:function(){
	 	$("wsDIV").style.visibility="visible";
	}
	});
}

function toggleOrg()
{
	if (!document.getElementById("trans")){
		return;
	}
	if (!document.getElementById("orgDD")){
		document.getElementById("trans").style.visibility="hidden";
		return;
	}

	if ((document.getElementById("orgDD").value=='0')){
		document.getElementById("trans").style.visibility="hidden";
		document.getElementById("getPatientAnchor").style.visibility="hidden";
		$("wsDIV").style.visibility="hidden";
	}
	else{
		document.getElementById("trans").style.visibility="visible";
		document.getElementById("getPatientAnchor").style.visibility="visible";
	}
}




function startDate1(delay1) {
  var adate, date, amonth;
  delay = delay1;
  adate = new Date();
  date = adate.getDate();
  amonth = adate.getMonth()+1;

  if (amonth == 1) date += " January";
  else if (amonth == 2) date += " February";
  else if (amonth == 3) date += " March";
  else if (amonth == 4) date += " April";
  else if (amonth == 5) date += " May";
  else if (amonth == 6) date += " June";
  else if (amonth == 7) date += " July";
  else if (amonth == 8) date += " August";
  else if (amonth == 9) date += " September";
  else if (amonth == 10) date += " October";
  else if (amonth == 11) date += " November";
  else if (amonth == 12) date += " December";
  date += " " + adate.getFullYear();
  document.atime21.date.value = date;
  dd1 = setTimeout("startDate1(delay)",delay1);
}




function openSpecialityWindow(formobj) {

	specialityIds = formobj.selSpecialityIds.value;

	if(specialityIds=="null")
	{
		 specialityIds="";

	}
	windowName=window.open("multiselectcodepopup.jsp?selectedIds="+specialityIds+"&openerForm=patient&hidEleName=selSpecialityIds&eleName=txtSpeciality&codeType=prim_sp&ptitle=<%=LC.L_Speciality%>&heading=<%=LC.L_Mng_Pats%>","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=650,height=300");
	/*windowName=window.open("multiselectcodepopup.jsp?selectedIds="+specialityIds+"&openerForm=patient&hidEleName=selSpecialityIds&eleName=txtSpeciality&codeType=prim_sp&ptitle=Speciality&heading=Manage Patients","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=650,height=300");*****/

	windowName.focus();


}


function openEthnicityWindow(formobj) {
	addEthnicityIds = formobj.selAddEthnicityIds.value;
	windowName=window.open("multiselectcodepopup.jsp?selectedIds="+addEthnicityIds+"&openerForm=patient&hidEleName=selAddEthnicityIds&eleName=txtAddEthnicity&codeType=ethnicity&ptitle=<%=LC.L_Ethnicity%>&heading=<%=LC.L_Mng_Pats%>","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=650,height=300");
	/*windowName=window.open("multiselectcodepopup.jsp?selectedIds="+addEthnicityIds+"&openerForm=patient&hidEleName=selAddEthnicityIds&eleName=txtAddEthnicity&codeType=ethnicity&ptitle=Ethnicity&heading=Manage Patients","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=650,height=300");*****/
	windowName.focus();
}


function openRaceWindow(formobj) {
	addRaceIds = formobj.selAddRaceIds.value;
	windowName=window.open("multiselectcodepopup.jsp?selectedIds="+addRaceIds+"&openerForm=patient&hidEleName=selAddRaceIds&eleName=txtAddRace&codeType=race&ptitle=<%=LC.L_Race%>&heading=<%=LC.L_Mng_Pats%>","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=650,height=320");
	/*windowName=window.open("multiselectcodepopup.jsp?selectedIds="+addRaceIds+"&openerForm=patient&hidEleName=selAddRaceIds&eleName=txtAddRace&codeType=race&ptitle=Race&heading=Manage Patients","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=650,height=320");*****/
	windowName.focus();
}


function  validate(formobj,autogen){


     if (autogen != 1) {
     if(!(validate_col('Pat ID',formobj.patID))) return false
     }


   //Virendra: Fixed Bug No. 4729, Added '\' validation for PatientID, Firstname, Lastname
   var patIDval;
   var patfnameval;
   var patlnameval;

   if(formobj.patID){
	   patIDval = formobj.patID.value;
   }
   if(formobj.patfname){
	   patfnameval = formobj.patfname.value;
   }
   if(formobj.patlname){
	   patlnameval = formobj.patlname.value;
   }
	
	 

 	 if(formobj.patID &&(patIDval.indexOf("%")!=-1 || patIDval.indexOf("\\")!=-1  ))
	  {
 		var paramArray = ["%,\\"];
		alert(getLocalizedMessageString("M_SplCharNotAllowed", paramArray));/*alert("Special Character(%,\\) not allowed for this Field");*****/
		  formobj.patID.focus();
		  return false;
	  }
 	//Modified to not allowed the special character "," in "Patient ID"
	 if(formobj.patID && patIDval.indexOf(",") != -1 ){
		var paramArray = [","];
		alert(getLocalizedMessageString("M_SplCharNotAllowed", paramArray));/*alert("Special Character(,) not allowed for this Field");*****/
		formobj.patID.focus();
		return false;
	 } 
 	 if( formobj.patfname && patfnameval.indexOf("\\")!=-1  )
	  {
 		var paramArray = ["\\"];
		alert(getLocalizedMessageString("M_SplCharNotAllowed", paramArray));/*alert("Special Character(\\) not allowed for this Field");*****/
		  formobj.patfname.focus();
		  return false;
	  }
 	if( formobj.patlname && patlnameval.indexOf("\\")!=-1  )
	  {
 		var paramArray = ["\\"];
		alert(getLocalizedMessageString("M_SplCharNotAllowed", paramArray));/*alert("Special Character(\\) not allowed for this Field");*****/
		  formobj.patlname.focus();
		  return false;
	  }





// Modified by Gopu on 8th March 2005 for fixed the bug no. 2036 make the "First Name" field in Patient Demographic as non-Mandatory
   	 if (document.getElementById('pgcustompatfname')) {
	   if (!(validate_col('First Name',formobj.patfname))) return false
	 }

	 if (document.getElementById('pgcustompatlname')) {
	   if (!(validate_col('Last Name',formobj.patlname))) return false
	 }



	 if (document.getElementById('pgcustompatdob')) {
	     if (!(validate_col('dob',formobj.patdob))) return false
	 }

	 //gender mandatory/non-mand implementation
	 if (document.getElementById('pgcustompatgender')) {
	     if (!(validate_col('patgender',formobj.patgender))) return false
	 }




	//ethnicity mandatory/non-mand implementation
	 if (document.getElementById('pgcustompatethnicity')) {
	     if (!(validate_col('patethnicity',formobj.patethnicity))) return false
	 }


	  //Additional mandatory/non-mand implementation
	 if (document.getElementById('pgcustompatadditional1')) {
	     if (!(validate_col('Additional',formobj.txtAddEthnicity))) return false
	 }




	 //race mandatory/non-mand implementation
	 if (document.getElementById('pgcustompatrace')) {
	     if (!(validate_col('patrace',formobj.patrace))) return false
	 }


	  //Additional mandatory/non-mand implementation
	 if (document.getElementById('pgcustompatadditional2')) {
	     if (!(validate_col('Additional',formobj.txtAddRace))) return false
	 }

     if (formobj.patdob && !(validate_date(formobj.patdob))) return false

     if (formobj.patorganization && !(validate_col('organization',formobj.patorganization))) return false

     if (formobj.patstatus && !(validate_col('pat status',formobj.patstatus))) return false


	 if (document.getElementById('pgcustompatfacid')) {
	     if (!(validate_col('Pat Facility Id',formobj.patFacilityID))) return false
	 }


	 if (document.getElementById('pgcustompatprov')) {
	     if (!(validate_col('Provider',formobj.patregbyname))) return false
	 }

	 if (document.getElementById('pgcustompatifoth')) {
	     if (!(validate_col('if other',formobj.phyOther))) return false
	 }

	 if (document.getElementById('pgcustompatdod')) {
	     if (!(validate_col('dod',formobj.patdeathdate))) return false
	 }
	 //KM
	 //cod mandatory/non-mand implementation
	 if (document.getElementById('pgcustompatcod')) {
	     if (!(validate_col('dthCause',formobj.dthCause))) return false
	 }

	 if (document.getElementById('pgcustompatspeccause')) {
	     if (!(validate_col('speccause',formobj.otherdthCause))) return false
	 }

	 if (document.getElementById('pgcustompatspeccause')) {
	     if (!(validate_col('speccause',formobj.otherdthCause))) return false
	 }






	 // Added by JM on 040805
     if (!(validate_date(formobj.patdeathdate))) return false


     // Added bY Gopu on 07th April 2005 DOB Validation DOB should not be greater than Today's date
	 // modified by GOpu for fixing the bug no. 2117
	    /*
	    da= new Date();
	    var str1,str2;
	    str1=parseInt(da.getMonth());
	    str2=str1+1;
		todate = str2+'/'+da.getDate()+'/'+da.getFullYear();
		*/

	//JM: #4025, #4026
	todate = formatDate(new Date(),calDateFormat);

	if (formobj.patdob && formobj.patdob.value != null && formobj.patdob.value !=''){
		if (CompareDates(formobj.patdob.value,todate,'>')){
		  	alert ("<%=MC.M_BirthDtNotGrt_Today%>");/*alert ("Date of Birth should not be greater than Today's Date");*****/
			formobj.patdob.focus();
			return false;
		}
	}


	// Added by Gopu on 08th April 2005 Validation for Death Date should not allow future date

	if (formobj.patdeathdate && formobj.patdeathdate.value != null && formobj.patdeathdate.value !=''){
		if (CompareDates(formobj.patdeathdate.value,todate,'>')){
			alert ("<%=MC.M_DthDtNotGtr_TodayDt%>");/*alert ("Death Date should not be greater than Today's Date");*****/
			formobj.patdeathdate.focus();
			return false;
		}

	// Added by Gopu on 08th April 2005 Validation for Death Date should not be less than Birth Date

		if (formobj.patdob && formobj.patdob.value != null && formobj.patdob.value !=''){
			if (CompareDates(formobj.patdob.value,formobj.patdeathdate.value,'>')){
				alert ("<%=MC.M_DthDtNotLess_BirthDt%>");/*alert ("Death Date should not be less than Birth Date");*****/
				formobj.patdeathdate.focus();
				return false;
			}
		}

	}



	//check for death stat

	var patselstat;
	 var deadstat;
	 var deathdate;

if(formobj.patstatus){
	patselstat = formobj.patstatus.value;
}


if(formobj.deadStatPk){
	 deadstat =   formobj.deadStatPk.value;
}

if(formobj.patdeathdate){
	deathdate = formobj.patdeathdate.value;
}



	 if(deadstat == patselstat)
	 {
			if (deathdate == null || deathdate == '' )
			{
				 alert("<%=MC.M_PlsEtr_PatDod%>");/*alert("Please enter <%=LC.Pat_Patient%>'s Date of Death");*****/
				 return false;
			}
	 }

	 if (deathdate != null && deathdate != '' )
	 {
	 	if ( deadstat != patselstat)
	 	{
	 		alert("<%=MC.M_YouEtrPat_DodPlsChg%>");/*alert("You have entered <%=LC.Pat_Patient%>'s Date of Death. Please change the <%=LC.Pat_Patient%>'s Status to 'Dead'");*****/
	 		return false;
	 	}
	}


	 if(formobj.dthCause && formobj.dthCause.value != null && formobj.dthCause.value != '')
	 {
	  if ( deadstat != patselstat)
	 	{
	 		alert("<%=MC.M_EtrPatDthCause_StatDead%>");/*alert("You have entered <%=LC.Pat_Patient%>'s Cause of Death. Please change the <%=LC.Pat_Patient%>'s Status to 'Dead'");*****/
	 		return false;
	 	}
	 }

	// Added by Gopu on 08th April 2005 for Death date should not allow future date added

	if (formobj.patdeathdate && deathdate != null && deathdate !=''){
		if (CompareDates(deathdate,todate,'>')){
		  	alert ("<%=MC.M_DthDtNotGtr_TodayDt%>");/*alert ("Death Date should not be greater than Today's Date");*****/
			formobj.patdeathdate.focus();
			return false;
		}
	}

	if (!(validate_col('e-Signature',formobj.eSign))) return false
	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }


   }
</SCRIPT>
<script src="js/velos/date.js" type="text/javascript"></script>
<script src="js/velos/dateformatSetting.js" type="text/javascript"></script>
<script src="js/velos/patSearchUtil.js" type="text/javascript"></script>
<SCRIPT language="JavaScript1.1">
function openwin1() {
    windowName = window.open("usersearchdetails.jsp?fname=&lname=&from=patient","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
	windowName.focus();
}

var patStat="";
function patStatEMR()
{
	$j("#patstatus").val(<%=request.getParameter("patstatus")%>);
}

function asyncValidate()
{

 valChangeReturn=0;
 if ((document.getElementById("patfname").value.length>0) && (document.getElementById("patlname").value.length>0) && (document.getElementById("patdob").value.length>0) ) {
 ajaxvalidate('patient~fldob:siteId',0,'fldobMessage','','<%=MC.M_PatWithSameNameDobAldy%><%--A <%=LC.Pat_Patient%> already exists with same First Name,Last Name and Date Of Birth.*****--%>','patfname:patlname:patdob:accountId:patid');
 }
 if (document.getElementById("patid").value.length>0) {
 ajaxvalidate('patient:patid',0,'ajaxPatIdMessage','','<%=MC.M_PatId_AldyExst%><%--This <%=LC.Pat_Patient%> ID already exists for the selected organization or it\'s  parent/child organizations.*****--%>','siteId') ;
 }
}


</SCRIPT>


<%@ page language = "java" import = "com.velos.eres.service.util.EJBUtil,com.velos.eres.business.common.*,com.velos.esch.business.common.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB, com.velos.eres.web.user.UserJB"%>

<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="commonB" scope="page" class="com.velos.eres.web.common.CommonJB" />
<jsp:useBean id="userBFromSession" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="siteB" scope="request" class="com.velos.eres.web.site.SiteJB"/>
<jsp:useBean id="accB" scope="request" class="com.velos.eres.web.account.AccountJB"/>
<jsp:useBean id="userSiteB" scope="page" class="com.velos.eres.web.userSite.UserSiteJB" />
<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id ="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>


<% String src;
boolean hasAccess = false;
src= request.getParameter("srcmenu");
String selectedTab = request.getParameter("selectedTab");
String page1 = request.getParameter("page");

page1 = "quick";
String lastName="";
 String firstName="";
String specialName="";

String accCode="";

%>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<body onload="javascript:if (typeof(document.patient.patID) !='undefined') document.patient.patID.focus();">

<DIV class="BrowserBotN"  id="div1">
<%

   HttpSession tSession = request.getSession(true);
	String wsEnabled="";
   if (sessionmaint.isValidSession(tSession))	{
	String uName = (String) tSession.getValue("userName");
        String userIdFromSession = (String) tSession.getAttribute("userId");

	userBFromSession = (UserJB) tSession.getAttribute("currentUser");
	String acc = (String) tSession.getValue("accountId");

	accB.setAccId(EJBUtil.stringToNum(acc));
	accB.getAccountDetails();


	//Added by Manimaran for the Enh.#GL7.
	String autoGenPatient = accB.getAutoGenPatient();
	if (autoGenPatient == null)
		autoGenPatient ="";


	wsEnabled=accB.getWSEnabled();
	wsEnabled=(wsEnabled==null)?"":wsEnabled;
	if (wsEnabled.length()==0) wsEnabled="N";

	accCode=accB.getAccName();
	accCode=(accCode==null)?"":accCode;


	CodeDao  cdOther = new CodeDao();
	int otherDthCause = cdOther.getCodeId("pat_dth_cause","other");

	CodeDao cdperdth = new CodeDao();
	int deadPerStat = cdperdth.getCodeId("patient_status","dead");

	String pageMode= request.getParameter("pageMode");

	if(pageMode == null)
		pageMode = "initial";

		//modifed by Gopu for MAHI Enahncement on 24th Feb 2005
		//Check for page customized mandatory field exists for the page by calling
		//the method populateObject([AccountId], [module/page]


		ConfigFacade cFacade=ConfigFacade.getConfigFacade();
		HashMap hashPgCustFld = new HashMap();



		ConfigDetailsObject cdoPgField = cFacade.populateObject(Integer.parseInt(acc), "patient");
		//if customized fields exists then put the field name and its value in hashmap
		//with key as the field name and value is the position at which the values can be referred to


		if (cdoPgField.getPcfField()!= null && cdoPgField.getPcfField().size() > 0) {
			for (int i=0;i<cdoPgField.getPcfField().size();i++){
				hashPgCustFld.put((String)cdoPgField.getPcfField().get(i), String.valueOf(i));

		}
	}


	String studyId = null;
	String statDesc = null;
	String statid = null;
	String studyVer = null;


	String disableStr ="";
	String readOnlyStr ="";



	studyId = request.getParameter("studyId");

	statDesc = request.getParameter("statDesc");

	statid = request.getParameter("statid");

	studyVer = request.getParameter("studyVer");

	boolean withSelect = true;
	String dthCause ="";

	 CodeDao cd10 = new CodeDao();


  cd10.getCodeValues("pat_dth_cause");
 String dDthCause = "";




	int pageRight = 0;
	String mode = null;

	String parPage = "";
	String patCode="";
	int personPK = 0;
	String patientID = "";
	String organization = "";
	String patOrg = "";
	int orgRight = 0;
	String addEthnicityNames="";
	String addRaceNames="";
    String addEthnicityIds="";
    String addRaceIds="";

	 int usrGroup = 0;
	  boolean completeDet = false;
	  String inputType = "text";
	  String displayStar = "";
	  int patDataDetail = 0;

	personPK = EJBUtil.stringToNum(request.getParameter("pkey"));

	//make study dropdown

	ArrayList arStudyId = new ArrayList();
	ArrayList arStudyNum = new ArrayList();

	StudyDao sd = studyB.getUserStudies(userIdFromSession, "dummy",0,"active");
	arStudyId = sd.getStudyIds();
	arStudyNum = sd.getStudyNumbers();

	//create study dropdown

	String strStudyId = "";


	if (!(personPK==0)) {
    		person.setPersonPKId(personPK);
	    	person.getPersonDetails();

    		patientID = person.getPersonPId();
	    	patientID = (patientID==null)?"":patientID;

    		organization =   person.getPersonLocation() ;
	    	organization = (organization==null)?"":organization;

		siteB.setSiteId(EJBUtil.stringToNum(organization)) ;
		siteB.getSiteDetails();
		patOrg = siteB.getSiteName();

		//check whether the user has right to edit the patients of the patient org
        orgRight = userSiteB.getUserPatientFacilityRight(EJBUtil.stringToNum(userIdFromSession),personPK);

	}

   	if(page1.equals("enrollPatientsearch")){

		patCode=request.getParameter("patCode");

	}

 	mode = request.getParameter("mode");


	usrGroup =EJBUtil.stringToNum( userBFromSession.getUserGrpDefault());


	if (mode.equals("M"))	{


		patDataDetail = person.getPatientCompleteDetailsAccessRight( EJBUtil.stringToNum(userIdFromSession),usrGroup,personPK );


        if(patDataDetail!=0)
        {
		if( patDataDetail >= 4 )
    		{

			completeDet = true;
			inputType = "text";
			displayStar = "";
		}
		else
		{

			completeDet = false;
			inputType = "hidden";
			displayStar = "<font color = blue>*</font>";
		}
                }

	}


	GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");


  pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MPATIENTS"));


 if ((mode.equals("M") && (pageRight >= 4 && orgRight >= 4)) || (mode.equals("N") && (pageRight == 5  || pageRight == 7 ) ))	{

  String special_id="";
  String fname = "";

  String lname = "";

  String aname = "";

  String dob = "";
  String gender = "";

  String ethnicity = "";
  String race = "";

  String deathDate = "";
  String regBy = "";
  String regByName = "";
	String status = "";
	String selstd = "";

  String dGender = "";
	String patstatus = "";
	String patdeathdate = "";
	String patadd1 = "";
	String patadd2 = "";
	String patcity = "";
	String patstate = "";
	String patcounty = "";
	String patzip = "";
	String patcountry = "";
	String pathphone = "";
	String patbphone = "";
	String emailID = "";
	String ssn = "";
	String mname = "";
	String maritalStat = "";
  String dEthnicity = "";
  String dRace = "";
  StringBuffer dSites = new StringBuffer();
  StringBuffer dSitesRestricted = new StringBuffer();
  String dStatus = "";
  String phyOther="";
  String orgOther="";
  String orgDD="";
  String selSpecialityIds="";
special_id=request.getParameter("specialityIds");

	String patientFacilityId  = "";

selSpecialityIds=special_id;

ArrayList siteIdsRestricted;
ArrayList siteNamesRestricted;
ArrayList siteAltIdsRestricted;

  CodeDao cd1 = new CodeDao();
  CodeDao cd4 = new CodeDao();
  CodeDao cd5 = new CodeDao();
  CodeDao cd8 = new CodeDao();

  cd1.getCodeValues("gender");
  cd4.getCodeValues("ethnicity","primary");
  cd5.getCodeValues("race");
  cd8.getCodeValues("patient_status");

   int deadStatPk = 0;

  deadStatPk = cd8.getCodeId("patient_status","dead");

  addEthnicityIds = person.getPersonAddEthnicity();

 	 if (addEthnicityIds==null) addEthnicityIds="";

    if(addEthnicityIds.equals(""))
	{
		addEthnicityNames="";
	}

	if (addEthnicityIds.length()>0) addEthnicityNames = cd8.getCodeLstNames(addEthnicityIds);
	if(addEthnicityNames.equals("error"))
	{
	  addEthnicityNames="";
	}


 	addRaceIds = person.getPersonAddRace();
	if (addRaceIds==null) addRaceIds="";

    if(addRaceIds.equals(""))
	{
		addRaceNames="";

	}

       if(special_id==null)
        {


            specialName="";

        }
        else if(special_id!=null)
	{
	if(special_id.length()>0){specialName=cd8.getCodeLstNames(special_id);
        if(specialName.equals("error"))
		{
			 specialName="";
		}
        }
	}
	if (addRaceIds.length()>0) addRaceNames = cd8.getCodeLstNames(addRaceIds);

      	if(addRaceNames.equals("error"))
	{
			addRaceNames="";
	}
  dthCause = person.getPatDthCause();





 //show sites for which the user has new right i.e right to add a patient
  UserSiteDao userSiteDao = userSiteB.getSitesWithNewRight(EJBUtil.stringToNum(acc),EJBUtil.stringToNum(userIdFromSession));



  ArrayList siteIds = userSiteDao.getUserSiteSiteIds();
  ArrayList siteDescs = userSiteDao.getUserSiteNames() ;


  String siteId = "";
  int len = siteIds.size();
  boolean exists = false;

  /**
   * Shows logged in user's primary organization, default selection

   */
		String userSiteId = "";
		int primSiteId = 0;
		int userId = 0;
		String siteName = "";

    userId = EJBUtil.stringToNum(userIdFromSession);
	/*userB.setUserId(userId);
	userB.getUserDetails();*/
	userB=(com.velos.eres.web.user.UserJB)tSession.getAttribute("currentUser");

	userSiteId = userB.getUserSiteId();
	userSiteId=(userSiteId==null)?"":userSiteId;
	primSiteId= EJBUtil.stringToNum(userSiteId);

	siteB.setSiteId(primSiteId);
	siteB.getSiteDetails();
	siteName = siteB.getSiteName();



	////KM
	String orgAtt ="";
	if (hashPgCustFld.containsKey("org")) {
		int fldNumOrg= Integer.parseInt((String)hashPgCustFld.get("org"));
		orgAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumOrg));
		if(orgAtt == null) orgAtt ="";
	}

	//int cnt =0 ;
	String onChange="onchange=\"asyncValidate()\"";

	if(!"0".equals(orgAtt)) {
	if(orgAtt.equals("1") || orgAtt.equals("2")) //KM-Disabling dropdown based on custom data.
	    dSites.append("<SELECT NAME=patorganization id='siteId' disabled class='readonly-input' "+ onChange +">") ;
	else
		dSites.append("<SELECT NAME=patorganization id='siteId' "+ onChange +">") ;


	for (int counter = 0; counter <= len-1 ; counter++) {
		siteId =  (String)siteIds.get(counter);
		if(userSiteId.equals(siteId)) {
			dSites.append("<OPTION value = "+ primSiteId +" selected>" + siteName+"</OPTION>");

		} else {
			dSites.append("<OPTION value = "+ siteId +">" + siteDescs.get(counter)+"</OPTION>");
		}
	}
	dSites.append("<OPTION value='' >"+LC.L_Select_AnOption/*Select an Option*****/+"</OPTION>");
	dSites.append("</SELECT>");

	if (wsEnabled.equals("Y"))
	{
		userSiteDao.getSitesWithNewRightRestricted(EJBUtil.stringToNum(acc),EJBUtil.stringToNum(userIdFromSession));
		siteIdsRestricted=userSiteDao.getUserSiteSiteIds();
		siteAltIdsRestricted=userSiteDao.getUserSiteAltIds();
		siteNamesRestricted=userSiteDao.getUserSiteNames();
		len=siteIdsRestricted.size();
		String onChangeRestricted="onchange=\"toggleOrg()\"";
		dSitesRestricted.append("<SELECT NAME=orgDD id='orgDD' "+ onChangeRestricted +">") ;
		dSitesRestricted.append("<OPTION value='' selected='selected'>"+LC.L_Select_AnOption/*Select an Option*****/+"</OPTION>");
		for (int counter = 0; counter <= len-1 ; counter++) {
			siteId =  (String)siteAltIdsRestricted.get(counter);

			dSitesRestricted.append("<OPTION value = "+ siteId +">" + siteNamesRestricted.get(counter)+"</OPTION>");

		}
		dSitesRestricted.append("<OPTION value='0' >"+LC.L_Other/*Other*****/+"</OPTION>");
		dSitesRestricted.append("</SELECT>");
		orgDD=dSitesRestricted.toString();
	}
  /*
  check if the patient organization is already in the dropdown
  for (int counter = 0; counter <= len-1 ; counter++) {
	siteId =  (String)siteIds.get(counter);
  	if (siteId.equals(organization)) {
		exists = true;
		break;
	}
  }*/


 /* dSites.append("<SELECT NAME=patorganization >") ;
	for (int counter = 0; counter <= len-1 ; counter++) {
		siteId =  (String)siteIds.get(counter);
		if (siteId.equals(organization)) {
			dSites.append("<OPTION value = "+ siteId +" selected>" + siteDescs.get(counter)+"</OPTION>");
		} else {
			dSites.append("<OPTION value = "+ siteId +">" + siteDescs.get(counter)+"</OPTION>");
		}
	}
	if (!(organization.equals(""))) {
		if (!(exists)) {
		    dSites.append("<OPTION value = "+ organization +" selected>" + patOrg+"</OPTION>");
		}
	} else {
		dSites.append("<OPTION value='' selected>Select an Option</OPTION>");
	}
	dSites.append("</SELECT>");*/
 	}
%>


<%
  if (mode.equals("M")) {

	   dob =  person.getPersonDob();
	   dob = (dob==null)?"":dob;

	   fname  =  person.getPersonFname();
	   fname = (fname==null)?"":fname;

	  gender  =  person.getPersonGender();
	  gender = (gender==null)?"":gender;

	 lname =  person.getPersonLname();
	 lname = (lname==null)?"":lname;

	 aname = person.getPersonAlias();
	 aname = (aname==null)?"":aname;

	 ethnicity =  person.getPersonEthnicity();
	 ethnicity = (ethnicity==null)?"":ethnicity;

	 race =  person.getPersonRace();
	 race = (race==null)?"":race;

	status =   person.getPersonStatus();
	status = (status==null)?"":status;

	deathDate = person.getPersonDeathDate();
	deathDate = (deathDate==null)?"":deathDate;


	regBy = person.getPersonRegBy();
	regBy = (regBy==null)?"":regBy;

	patientFacilityId = person.getPatientFacilityId();

	if (StringUtil.isEmpty(patientFacilityId ))
		patientFacilityId  = "";

	phyOther=person.getPhyOther();
	phyOther=(phyOther==null)?"":phyOther;
	orgOther=person.getOrgOther();
	orgOther=(orgOther==null)?"":orgOther;

	//get reg by name

        if(regBy!="")
        {

        userB.setUserId(EJBUtil.stringToNum(regBy));
	userB.getUserDetails();
        lastName=userB.getUserLastName();
        firstName=userB.getUserFirstName();
        }






        if((lastName=="") && (firstName==""))
        {
        regByName = "";

        }

         else if((firstName!=null)&&(lastName==null))
        {
             regByName=firstName;

        }
        else if((lastName!=null)&&(firstName==null))
        {
            regByName=lastName;

        }
        else if((firstName!=null) && (lastName!=null))
        {

        regByName = userB.getUserLastName() + ", " + userB.getUserFirstName();

        }





	int userPrimOrg = EJBUtil.stringToNum(userB.getUserSiteId());



	String enrollId = (String) tSession.getValue("enrollId");

page1="enrollPatientsearch";

	if(! page1.equals("enrollPatientsearch"))
	{
%>
	<jsp:include page="patienttabs.jsp" flush="true">
	<jsp:param name="pkey" value="<%=personPK%>"/>
	<jsp:param name="patientCode" value="<%=patientID%>"/>
	<jsp:param name="patProtId" value="<%=enrollId%>"/>
	<jsp:param name="studyId" value="<%=studyId%>"/>
	<jsp:param name="fromPage" value="demographics"/>
	<jsp:param name="page" value="<%=page1%>"/>
	</jsp:include>

<%
 }	//not from enrollpatient search
   }
   else{


   		completeDet = true; //all data is visible
		displayStar = ""; // no star will be visible
		//regBy = userIdFromSession;
		//regByName = uName;
   }

	if(pageMode.equals("final")){

	 	 dthCause = request.getParameter("dthCause");
		 patientID = request.getParameter("patID");

		 dob = request.getParameter("patdob");

		 fname = request.getParameter("patfname");

 		 lname = request.getParameter("patlname");
 		 aname = request.getParameter("aliasname");

		 gender = request.getParameter("patgender");
         ethnicity = request.getParameter("patethnicity");
		 race = request.getParameter("patrace");

		 status = request.getParameter("patstatus");
		 addEthnicityNames = request.getParameter("txtAddEthnicity");
		 addRaceNames = request.getParameter("txtAddRace");
		 regByName  = request.getParameter("patregbyname");

		 phyOther  = request.getParameter("phyOther");
		 deathDate = request.getParameter("patdeathdate");
		 selstd = request.getParameter("selstudyId");


		 }

	if (pageMode.equals("patientEMR")) {
		dthCause = request.getParameter("dthCause");
		patientFacilityId = request.getParameter("patID");
		patientID = request.getParameter("patientID");
		dob = request.getParameter("patdob");
		fname = request.getParameter("patfname");
		lname = request.getParameter("patlname");
		mname = request.getParameter("patmname");
		aname = request.getParameter("aliasname");
		gender = request.getParameter("patgender");
		ethnicity = request.getParameter("patethnicity");
		race = request.getParameter("patrace");
		dStatus = request.getParameter("patstatus");
		addEthnicityNames = request.getParameter("txtAddEthnicity");
		addRaceNames = request.getParameter("txtAddRace");
		deathDate = request.getParameter("patdeathdate");
		ssn = request.getParameter("patssn");
		maritalStat =  request.getParameter("maritalStat");
		patdeathdate = request.getParameter("patdeathdate");
		patadd1 = request.getParameter("patadd1");
		patadd2 = request.getParameter("patadd2");
		patcity = request.getParameter("patcity");
		patstate = request.getParameter("patstate");
		patcounty = request.getParameter("patcounty");
		patzip = request.getParameter("patzip");
		patcountry = request.getParameter("patcountry");
		pathphone = request.getParameter("pathphone");
		patbphone = request.getParameter("patbphone");
		addRaceIds = request.getParameter("codeAddRace");
		addEthnicityIds = request.getParameter("codeAddEthnicity");
		emailID = request.getParameter("emailID");
	}


//////////KM
	   String surStatAtt="";
	   String gendAtt ="";
	   String ethAtt ="";
	   String raceAtt ="";

	   if (hashPgCustFld.containsKey("survivestat")) {
		  int fldNumSurStat= Integer.parseInt((String)hashPgCustFld.get("survivestat"));
		  surStatAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumSurStat));
		  if(surStatAtt == null) surStatAtt ="";
	   }

	  if (hashPgCustFld.containsKey("gender")) {
			int fldNumGender= Integer.parseInt((String)hashPgCustFld.get("gender"));
			gendAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumGender));
			if(gendAtt == null) gendAtt ="";
	  }

	  if (hashPgCustFld.containsKey("ethnicity")) {
			int fldNumEth= Integer.parseInt((String)hashPgCustFld.get("ethnicity"));
			ethAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumEth));
			if(ethAtt == null) ethAtt ="";
	  }

	  if (hashPgCustFld.containsKey("race")) {
			int fldNumRace= Integer.parseInt((String)hashPgCustFld.get("race"));
            raceAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumRace));

			if(raceAtt == null) raceAtt ="";
	  }



	 if(pageMode.equals("final")){

		if(!"0".equals(gendAtt)) {
			if(gendAtt.equals("1"))
				dGender = cd1.toPullDown("patgender", EJBUtil.stringToNum(gender),"disabled class='readonly-input'");
			else
				dGender = cd1.toPullDown("patgender", EJBUtil.stringToNum(gender));
		}

		if(!"0".equals(ethAtt)) {
			if(ethAtt.equals("1"))
			    dEthnicity = cd4.toPullDown("patethnicity", EJBUtil.stringToNum(ethnicity),"disabled class='readonly-input'");
			else
			    dEthnicity = cd4.toPullDown("patethnicity", EJBUtil.stringToNum(ethnicity));
		}

		if(!"0".equals(raceAtt)) {
			if(raceAtt.equals("1"))
				dRace = cd5.toPullDown("patrace", EJBUtil.stringToNum(race),"disabled class='readonly-input'");
			else
				dRace = cd5.toPullDown("patrace", EJBUtil.stringToNum(race));
		}

		if(!"0".equals(surStatAtt)) {
			if(surStatAtt.equals("1"))
	  			dStatus = cd8.toPullDown("patstatus", EJBUtil.stringToNum(status),"disabled class='readonly-input'");
		  	else
				dStatus = cd8.toPullDown("patstatus", EJBUtil.stringToNum(status));
		}

	}
	else	{


		// To change the fields into readonly format use the ("disabled class='readonly-input'")
		//dGender = cd1.toPullDown("patgender", EJBUtil.stringToNum(gender),"disabled class='readonly-input'");
		
		if(!"0".equals(gendAtt)) {
			//dGender = cd1.toPullDown("patgender");
	        if(gendAtt.equals("1"))
				dGender = cd1.toPullDown("patgender", EJBUtil.stringToNum(gender),"disabled class='readonly-input'");
			else if(gendAtt.equals("2"))
				dGender = cd1.toPullDown("patgender", EJBUtil.stringToNum(gender),"disabled class='readonly-input'");
			else 
				dGender = cd1.toPullDown("patgender", EJBUtil.stringToNum(gender));
		}

		if(!"0".equals(ethAtt)) {
			if(ethAtt.equals("1"))
			    dEthnicity = cd4.toPullDown("patethnicity", EJBUtil.stringToNum(ethnicity),"disabled class='readonly-input'");
			else if(ethAtt.equals("2"))
			    dEthnicity = cd4.toPullDown("patethnicity", EJBUtil.stringToNum(ethnicity),"disabled class='readonly-input'");
			else 
				 dEthnicity = cd4.toPullDown("patethnicity", EJBUtil.stringToNum(ethnicity));
		}

		//dEthnicity = cd4.toPullDown("patethnicity");
		//dRace = cd5.toPullDown("patrace");

		if(!"0".equals(raceAtt)) {
			if(raceAtt.equals("1")) //KM
				dRace = cd5.toPullDown("patrace", EJBUtil.stringToNum(race),"disabled class='readonly-input'");
			else if(raceAtt.equals("2"))
				dRace = cd5.toPullDown("patrace", EJBUtil.stringToNum(race),"disabled class='readonly-input'");
			else 
				dRace = cd5.toPullDown("patrace", EJBUtil.stringToNum(race));
		}

		//dStatus = cd8.toPullDown("patstatus", EJBUtil.stringToNum(status));
        //dStatus = cd8.toPullDown("patstatus");

		int aliveStatus = 0;
		CodeDao cd = new CodeDao();
		aliveStatus = cd.getCodeId("patient_status","A");

		if(!"0".equals(surStatAtt)) {
			if(surStatAtt.equals("1")) //KM
				dStatus = cd8.toPullDown("patstatus", aliveStatus,"disabled class='readonly-input'");
			else
				dStatus = cd8.toPullDown("patstatus", aliveStatus,"disabled class='readonly-input'");
		}


	}
    selstd = request.getParameter("selstudyId");

	if(selstd == null || selstd.equals("") || selstd.equals("null"))
	selstd = "0";

	strStudyId = EJBUtil.createPullDown("selstudyId", EJBUtil.stringToNum(selstd), arStudyId , arStudyNum )	;

	//dDthCause = cd10.toPullDown("dthCause",EJBUtil.stringToNum(dthCause),withSelect);



		CodeDao cdaodth = new CodeDao();
		cdaodth.getCodeValues("pat_dth_cause");
		ArrayList causeId = new ArrayList();
		causeId = cdaodth.getCId();

		ArrayList causeDesc = new ArrayList();
		causeDesc = cdaodth.getCDesc();
		int selDthCause = 0;
		StringBuffer strCause = new StringBuffer();

		Integer dthIdTemp = null;
		int dthIdSelTemp = 0;

		selDthCause = EJBUtil.stringToNum(dthCause);



	  ////KM

       String codAtt =""; //Added by Manimaran to disable the dropdown based on custom value.
	   if (hashPgCustFld.containsKey("cod")) {

		int fldNumCod= Integer.parseInt((String)hashPgCustFld.get("cod"));
        codAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumCod));

		if(codAtt == null) codAtt ="";

     }

	 if(!"0".equals(codAtt)) {
		  if(codAtt.equals("1"))
	 	     strCause.append("<SELECT NAME='dthCause' disabled class='readonly-input'>") ;
		  else
			 strCause.append("<SELECT NAME='dthCause' >") ;
      ////

		  strCause.append("<OPTION value=''> "+LC.L_Select_AnOption/*Select an Option*****/+" </OPTION>") ;

			for (int dcounter = 0; dcounter  < causeId.size()  ; dcounter++){
				dthIdTemp = (Integer) causeId.get(dcounter);
				if (selDthCause == dthIdTemp.intValue())
				{
					strCause.append("<OPTION value = "+ causeId.get(dcounter)+" SELECTED  >" + causeDesc.get(dcounter)+ "</OPTION>");
				}
				else
				{
					strCause.append("<OPTION value = "+ causeId.get(dcounter)+">" + causeDesc.get(dcounter)+ "</OPTION>");
				}
	
			}
		strCause.append("</SELECT>");
	}
	dDthCause = strCause.toString();



%>
<Form name="patient" id="patientForm" method="post" action="updatequickpatientdetails.jsp" onSubmit ="if (validate(document.patient,<%=autoGenPatient%>)==false) {setValidateFlag('false');return false;} else {setValidateFlag('true'); return true;}">
<input type="hidden" name="srcmenu" Value="<%=src%>">
<input type="hidden" name="pataccount" Value="<%=acc%>">
<input type="hidden" name="pkey" Value="<%=personPK%>">
<input type="hidden" name="page" Value="<%=page1%>">
<Input type="hidden" name="mode" value=<%=mode%>>
<Input type="hidden" name="patCode" value=<%=patCode%>>
<Input type="hidden" name="studyId" value=<%=studyId%>>
<Input type="hidden" name="statDesc" value=<%=statDesc%>>
<Input type="hidden" name="statid" value=<%=statid%>>
<Input type="hidden" name="studyVer" value=<%=studyVer%>>
<Input type="hidden" name="deadStatPk" value=<%=deadStatPk%>>
<input type="hidden" id="accountId" name="accountId" value="<%=acc%>">

<Input type="hidden" name="ssn" value=<%=ssn%>>
<Input type="hidden" name="mname" value=<%=mname%>>
<input type="hidden" id="maritalStat" name="maritalStat" value="<%=maritalStat%>">
				
<input type="hidden" id="patadd1" name="patadd1" size = 30  MAXLENGTH = 100 Value="<%=patadd1%>">
<input type="hidden" id="patadd2" name="patadd2" size = 30  MAXLENGTH = 100 Value="<%=patadd2%>">
<input type="hidden" id="patcity" name="patcity" size = 20  Value="<%=patcity%>">
<input type="hidden" id="patstate" name="patstate" Value="<%=patstate%>">
<input type="hidden" id="patcounty" name="patcounty" size = 20 Value="<%=patcounty%>">
<input type="hidden" id="patzip" name="patzip" Value="<%=patzip%>">
<input type="hidden" id="patcountry" name="patcountry" size = 30 Value="<%=patcountry%>">
<input type="hidden" id="pathphone" name="pathphone" size = 30 Value="<%=pathphone%>">
<input type="hidden" id="patbphone" name="patbphone" size = 30  Value="<%=patbphone%>">
<input type="hidden" id="patemail" name="patemail" size = 30  Value="<%=emailID%>">
<%// code modified by Gupta Maddala 102407 %>
 <input type=hidden id="aliasname" name="aliasname" size = 20 MAXLENGTH = 20 value='<%=aname%>'>


<input type="hidden" name="selSpecialityIds" value="<%=selSpecialityIds%>">


<input type="hidden" name="selAddEthnicityIds" value="<%=addEthnicityIds%>">
<input type="hidden" name="selAddRaceIds" value="<%=addRaceIds%>">
<input type="hidden" name="deadPerStat" value="<%=deadPerStat%>">
<%if(pageMode.equals("patientEMR")){ %>
			<input type="hidden" name="pageMode" value=<%=pageMode%>>
			<%} else { %>
			<input type="hidden" name="pageMode" value="final">
			<%} %>


<DIV id="studyDIV" class="studyInst comments" >
	<p class="sectionHeadings" id="studyNumberMessage"></p>
</DIV>
<table cellspacing="0" cellpadding="0" class="basetbl midAlign bgcolorFix" border="0">
	<tr>
		<td colspan="5">
			<p><%=LC.L_Patient_Details%><%--Patient Details*****--%></p>
		</td>
	</tr>
  <tr>
      <%

	if (hashPgCustFld.containsKey("patid")) {

		int fldNum= Integer.parseInt((String)hashPgCustFld.get("patid"));
		String patMand = ((String)cdoPgField.getPcfMandatory().get(fldNum));
		String patLable = ((String)cdoPgField.getPcfLabel().get(fldNum));
		String patAtt = ((String)cdoPgField.getPcfAttribute().get(fldNum));

		if(patAtt == null) patAtt ="";
		if(patMand == null) patMand ="";


		if(!patAtt.equals("0")) {
		%>
		<td>
			<%if(patLable !=null){%> 
				<%=patLable%>
			<%} else {%> 
				<%=LC.L_Patient_Id%><%--<%=LC.Pat_Patient%> ID*****--%>
			<%}

			if (patMand.equals("1")) {
			%>
			   <FONT class="Mandatory" id="pgcustompatid">*  </FONT>  
			  	<%if(autoGenPatient.equals("1")){ %>
				(<%=LC.L_SysHypenGen%><%--system-generated*****--%>)
				<%} 
			}
	
		    if(patAtt.equals("1")) {
			   disableStr = "disabled class='readonly-input'"; }
		    else if (patAtt.equals("2") ) {
			 	readOnlyStr = "readonly"; }
	  		 %>
    	</td>

	    <td colspan="5">
			<input type="text" name="patID" id="patid" size = 20 MAXLENGTH = 20 value='<%=patientID%>'   <%=disableStr%>  <%=readOnlyStr%> onblur="if (document.getElementById('siteId').value.length>0) {ajaxvalidate('patient:'+this.id,0,'ajaxPatIdMessage','','<%=MC.M_PatId_AldyExst%><%--This <%=LC.Pat_Patient%> ID already exists for the selected organization or it\'s  parent/child organizations.*****--%>','siteId')}">
			<% if (wsEnabled.equals("Y")){ %>
			 <%=orgDD %><A id="getPatientAnchor" href="javascript:void(0);" onClick=" return fetchData();"><%=LC.L_Get_Pat%><%--Get <%=LC.Pat_Patient%>*****--%></A>
			<%} %>
			<span id="ajaxPatIdMessage"></span>
	    </td>
		<%}
	} else {%>
  	<td> <%=LC.L_Patient_Id%><%--<%=LC.Pat_Patient%> ID*****--%> <FONT class="Mandatory" id="pgcustompatid">*  </FONT>  <%if(autoGenPatient.equals("1")){ %>
			(<%=LC.L_SysHypenGen%><%--system-generated*****--%>)
		<%}%>
	</td>
    <td colspan="5">
	<input type="text" name="patID" id="patid" size = 20 MAXLENGTH = 20 value='<%=patientID%>'   onblur="if (document.getElementById('siteId').value.length>0) {ajaxvalidate('patient:'+this.id,0,'ajaxPatIdMessage','','<%=MC.M_PatId_AldyExst%><%--This <%=LC.Pat_Patient%> ID already exists for the selected organization or it\'s  parent/child organizations.*****--%>','siteId')}">
	<% if (wsEnabled.equals("Y")){ %>
	 <%=orgDD %><A id="getPatientAnchor" href="javascript:void(0);" onClick=" return fetchData();"><%=LC.L_Get_Pat%><%--Get <%=LC.Pat_Patient%>*****--%></A>
	<%} %>
	<span id="ajaxPatIdMessage"></span>
    </td>

  <%}%>

  </tr>




  <tr>

	 <%if (hashPgCustFld.containsKey("fname")) {

			int fldNumFname= Integer.parseInt((String)hashPgCustFld.get("fname"));
			String fnameMand = ((String)cdoPgField.getPcfMandatory().get(fldNumFname));
			String fnameLable = ((String)cdoPgField.getPcfLabel().get(fldNumFname));
			String fnameAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumFname));

			disableStr ="";//KM-050208
			readOnlyStr ="";

			if(fnameAtt == null) fnameAtt ="";
			if(fnameMand == null) fnameMand ="";

			if(!fnameAtt.equals("0")) {
			if(fnameLable !=null){
			%>
			<td>
			<%=fnameLable%>
			<%} else {%> <td>
			<%=LC.L_First_Name%><%--First Name*****--%>
			<%}

			if (fnameMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatfname">* </FONT>
			<% }
		%>

	       <%if(fnameAtt.equals("1")) {
			 disableStr = "disabled class='readonly-input'"; }
		   else if (fnameAtt.equals("2") ) {
			 readOnlyStr = "readonly"; }
  		   %>

    </td>
	<td colspan="5">
	<input type=<%=inputType%> id="patfname" name="patfname" size = 20 MAXLENGTH = 30 value='<%=fname%>'  <%=disableStr%>  <%=readOnlyStr%> onblur="if (document.getElementById('siteId').value.length>0) {valChangeReturn=0;ajaxvalidate('patient~fldob:patfname',0,'fldobMessage','','<%=MC.M_PatWithSameNameDobAldy%><%--A <%=LC.Pat_Patient%> already exists with same First Name,Last Name and Date Of Birth.*****--%>','patfname:patlname:patdob:accountId:patid')}"><%=displayStar%>
    </td>
	<%}} else {%>
		<td> <%=LC.L_First_Name%><%--First Name*****--%></td>
	<td colspan="5">
	<input type=<%=inputType%> id="patfname" name="patfname" size = 20 MAXLENGTH = 30 value='<%=fname%>'  onblur="if (document.getElementById('siteId').value.length>0) {valChangeReturn=0;ajaxvalidate('patient~fldob:patfname',0,'fldobMessage','','<%=MC.M_PatWithSameNameDobAldy%><%--A <%=LC.Pat_Patient%> already exists with same First Name,Last Name and Date Of Birth.*****--%>','patfname:patlname:patdob:accountId:patid')}"><%=displayStar%>
    </td>


	<%}%>

    </tr>
  <tr>


	   <%if (hashPgCustFld.containsKey("lname")) {

			int fldNumLname= Integer.parseInt((String)hashPgCustFld.get("lname"));
			String lnameMand = ((String)cdoPgField.getPcfMandatory().get(fldNumLname));
			String lnameLable = ((String)cdoPgField.getPcfLabel().get(fldNumLname));
			String lnameAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumLname));


			disableStr ="";
			readOnlyStr ="";

			if(lnameAtt == null) lnameAtt ="";
			if(lnameMand == null) lnameMand ="";


			if(!lnameAtt.equals("0")) {
			if(lnameLable !=null){
			%>
			<td >

			<%=lnameLable%>
			<%} else {%> <td >
			<%=LC.L_Last_Name%><%--Last Name*****--%>
			<%}

			if (lnameMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatlname">* </FONT>
			<% }
		 %>

	       <%if(lnameAtt.equals("1")) {
			 disableStr = "disabled class='readonly-input'"; }
		   else if (lnameAtt.equals("2") ) {
			 readOnlyStr = "readonly"; }
		 %>



    </td>
    <td  colspan="5">
		<input type=<%=inputType%> id="patlname" name="patlname" size = 20 MAXLENGTH = 20  <%=disableStr%>  <%=readOnlyStr%>  value='<%=lname%>' onblur="if (document.getElementById('siteId').value.length>0) {valChangeReturn=0;ajaxvalidate('patient~fldob:patlname',0,'fldobMessage','','<%=MC.M_PatWithSameNameDobAldy%><%--A <%=LC.Pat_Patient%> already exists with same First Name,Last Name and Date Of Birth.*****--%>','patfname:patlname:patdob:accountId:patid')}"><%=displayStar%><span id="fldobMessage"></span>
    </td>
<%}} else {%>
	<td ><%=LC.L_Last_Name%><%--Last Name*****--%> </td>
    <td colspan="5">
		<input type=<%=inputType%> id="patlname" name="patlname" size = 20 MAXLENGTH = 20    value='<%=lname%>' onblur="if (document.getElementById('siteId').value.length>0) {valChangeReturn=0;ajaxvalidate('patient~fldob:patlname',0,'fldobMessage','','<%=MC.M_PatWithSameNameDobAldy%><%--A <%=LC.Pat_Patient%> already exists with same First Name,Last Name and Date Of Birth.*****--%>','patfname:patlname:patdob:accountId:patid')}"><%=displayStar%><span id="fldobMessage"></span>
    </td>


<%}%>

 </tr>

 <tr>

	   <%

		if (hashPgCustFld.containsKey("dob")) {
			
			int fldNum= Integer.parseInt((String)hashPgCustFld.get("dob"));
			String dobMand = ((String)cdoPgField.getPcfMandatory().get(fldNum));
			String dobLable = ((String)cdoPgField.getPcfLabel().get(fldNum));
			String dobAtt = ((String)cdoPgField.getPcfAttribute().get(fldNum));


			disableStr ="";
			readOnlyStr ="";
			if(dobAtt == null) dobAtt ="";
			if(dobMand == null) dobMand ="";


			if(!dobAtt.equals("0")) {
				if(dobLable !=null){
				%> <td >
				<%=dobLable%>
				<%} else {%> <td >
				<%=LC.L_Date_OfBirth%><%--Date of Birth*****--%>
				<%}
				if (dobMand.equals("1")) {%>
				   <FONT class="Mandatory" id="pgcustompatdob">* </FONT>
				<%} //KM-050208-3465

				if(dobAtt.equals("1")) {
					 disableStr = "disabled class='readonly-input'"; 
				} else if (dobAtt.equals("2") ) {
					 readOnlyStr = "readonly"; 
				}
			 %>
    </td>
    <td colspan="5">
	
<%-- INF-20084 Datepicker-- AGodara --%>
	<% if (completeDet){%>
		<%if (StringUtil.isEmpty(dobAtt)){ %>
			<input type="<%=inputType%>" name="patdob" id="patdob" class="datefield" size = 10 MAXLENGTH=11	value="<%=dob%>" onchange="valChangeReturn=0;ajaxvalidate('patient~fldob:patdob',0,'fldobMessage','','<%=MC.M_PatWithSameNameDobAldy%><%--A <%=LC.Pat_Patient%> already exists with same First Name,Last Name and Date Of Birth.*****--%>','patfname:patlname:patdob:accountId:patid')">
		<%} else if(dobAtt.equals("1") || dobAtt.equals("2")){%>
			<input type=<%=inputType%> id="patdob" name="patdob" <%=readOnlyStr%> <%=disableStr%> size = 10 MAXLENGTH = 11 value='<%=dob%>' onchange="valChangeReturn=0;ajaxvalidate('patient~fldob:patdob',0,'fldobMessage','','<%=MC.M_PatWithSameNameDobAldy%><%--A <%=LC.Pat_Patient%> already exists with same First Name,Last Name and Date Of Birth.*****--%>','patfname:patlname:patdob:accountId:patid')"><%=displayStar%>
		<%}%>
	<%} else{%>
		<input type=<%=inputType%> id="patdob" name="patdob" <%=readOnlyStr%> <%=disableStr%> size = 10 MAXLENGTH = 11 value='<%=dob%>' onchange="valChangeReturn=0;ajaxvalidate('patient~fldob:patdob',0,'fldobMessage','','<%=MC.M_PatWithSameNameDobAldy%><%--A <%=LC.Pat_Patient%> already exists with same First Name,Last Name and Date Of Birth.*****--%>','patfname:patlname:patdob:accountId:patid')"><%=displayStar%>	
	<%} %>
	</td>
  
  <%}
   } else {%>
	<td> <%=LC.L_Date_OfBirth%><%--Date of Birth*****--%>  <FONT class="Mandatory" id="pgcustompatdob">* </FONT>  </td>
    <td colspan="5">
		<input type=<%=inputType%> id="patdob" name="patdob" class="datefield" size = 10 MAXLENGTH = 11 value='<%=dob%>' onchange="valChangeReturn=0;ajaxvalidate('patient~fldob:patdob',0,'fldobMessage','','<%=MC.M_PatWithSameNameDobAldy%><%--A <%=LC.Pat_Patient%> already exists with same First Name,Last Name and Date Of Birth.*****--%>','patfname:patlname:patdob:accountId:patid')"><%=displayStar%>
	</td>


  <%}%>

 </tr>


   <tr>
   <%!String gendMand=null;%>


       <%
		   if (hashPgCustFld.containsKey("gender")) {

			int fldNumGender= Integer.parseInt((String)hashPgCustFld.get("gender"));
			//String
            gendMand = ((String)cdoPgField.getPcfMandatory().get(fldNumGender));
			String gendLable = ((String)cdoPgField.getPcfLabel().get(fldNumGender));
			gendAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumGender));


			if(gendAtt == null) gendAtt ="";
			if(gendMand == null) gendMand ="";


			if(!gendAtt.equals("0")) {
			if(gendLable !=null){
			%> <td >
			<%=gendLable%>
			<%} else {%> <td >
			<%=LC.L_Gender%><%--Gender*****--%>
			<%}

			if (gendMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatgender">* </FONT>
			<% }
		 %>

    </td>
    <td >
		<%=dGender%>  <%if (gendAtt.equals("1")) { %> <input type="hidden" name="patgender" value="">  <%}
		else if(gendAtt.equals("2")){%>
		<input type="hidden" name="patgender" value="<%=EJBUtil.stringToNum(gender)%>"><%}
		
		%><!--KM-->

    </td>
	<%} else if (gendAtt.equals("0")){

	%>
	<input type="hidden" name="patgender" value=""> <!--KM-->

	<%
	}
	 } else {%>
		<td><%=LC.L_Gender%><%--Gender*****--%> </td> <td> <%=dGender%>  </td>

	<%}%>
	<td></td><td></td><td></td>
	</tr>



     <tr>

       <%if (hashPgCustFld.containsKey("ethnicity")) {
			int fldNumEth= Integer.parseInt((String)hashPgCustFld.get("ethnicity"));
			String ethMand = ((String)cdoPgField.getPcfMandatory().get(fldNumEth));
			String ethLable = ((String)cdoPgField.getPcfLabel().get(fldNumEth));
			ethAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumEth));

			if(ethAtt == null) ethAtt ="";
			if(ethMand == null) ethMand ="";

			if(!ethAtt.equals("0")) {
			if(ethLable !=null){
			%><td width="15%">
			<%=ethLable%>
			<%} else {%> <td width="15%">
			<%=LC.L_Prim_Ethnicity%><%--Primary Ethnicity*****--%>
			<%}

			if (ethMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatethnicity">* </FONT>
			<% }
		 %>
    </td>
    <td width="34%" >
		<%=dEthnicity%> <%if(ethAtt.equals("1")) {%><input type="hidden" name="patethnicity" value=""> <%}
		else if(ethAtt.equals("2")){%>
		<input type="hidden" name="patethnicity" value="<%=EJBUtil.stringToNum(ethnicity)%>"><%}
		
		%><!--KM-->
    </td>

	<%} else if(ethAtt.equals("0")) {%>

	<input type="hidden" name="patethnicity" value=""> <!--KM-->


	<%}} else {%>
	<td width="15%" > <%=LC.L_Prim_Ethnicity%><%--Primary Ethnicity*****--%> </td>
    <td width="34%">
		<%=dEthnicity%>
    </td>

	<% }%>




	 <%if (hashPgCustFld.containsKey("additional1")) {


			int fldNumAdd1= Integer.parseInt((String)hashPgCustFld.get("additional1"));
			String add1Mand = ((String)cdoPgField.getPcfMandatory().get(fldNumAdd1));
			String add1Lable = ((String)cdoPgField.getPcfLabel().get(fldNumAdd1));
			String add1Att = ((String)cdoPgField.getPcfAttribute().get(fldNumAdd1));


			disableStr ="";
			if(add1Att == null) add1Att ="";
			if(add1Mand == null) add1Mand ="";



			if(!add1Att.equals("0")) {
			if(add1Lable !=null){
			%><td width="15%">
			<%=add1Lable%>
			<%} else {%> <td width="15%">
			<%=LC.L_Additional%><%--Additional*****--%>
			<%}

			if (add1Mand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatadditional1">* </FONT>
			<% }
		 %>


		   <%if(add1Att.equals("1")) {
			 disableStr = "disabled class='readonly-input'"; }
		     //else if (add1Att.equals("2") ) {
			 //readOnlyStr = "readonly"; }
		 %>


	</td>
	<td width="35%">
		<input type="text" name="txtAddEthnicity" size = 30 MAXLENGTH = 500  <%=disableStr%> Readonly value='<%=addEthnicityNames%>'>
		<%if(!add1Att.equals("1") &&  !add1Att.equals("2") ) {%>
		<A href="#" onClick="openEthnicityWindow(document.patient)"><%=LC.L_Select_Ethnicity%><%--Select Ethnicity*****--%></A>
		<%}%>

	</td>
	<%}} else { //KM-if custom data is not available

		%>
	<td width="15%">
			<%=LC.L_Additional%><%--Additional*****--%> </td>
	<td width="35%">
		<input type="text" name="txtAddEthnicity" size = 30 MAXLENGTH = 500  Readonly value='<%=addEthnicityNames%>'>
		<A href="#" onClick="openEthnicityWindow(document.patient)"><%=LC.L_Select_Ethnicity%><%--Select Ethnicity*****--%></A>	</td>

	<%}%>
  </tr>

 <tr>

       <%if (hashPgCustFld.containsKey("race")) {
			int fldNumRace= Integer.parseInt((String)hashPgCustFld.get("race"));

			String raceMand = ((String)cdoPgField.getPcfMandatory().get(fldNumRace));
			String raceLable = ((String)cdoPgField.getPcfLabel().get(fldNumRace));
			raceAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumRace));


			if(raceAtt == null) raceAtt ="";if(raceMand == null) raceMand ="";




			if(!raceAtt.equals("0")) {
			if(raceLable !=null){
			%>  <td >
			<%=raceLable%>
			<%} else {%> <td >
			<%=LC.L_Prim_Race%><%--Primary Race*****--%>
			<%}
			if (raceMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatrace">* </FONT>
			<%
		 }%>
    </td>
    <td>
		<%=dRace%> <%if(raceAtt.equals("1")) {%><input type="hidden" name="patrace" value=""> <%}
		else if(raceAtt.equals("2")){%>
		<input type="hidden" name="patrace" value="<%=EJBUtil.stringToNum(race)%>"><%}
		%><!--KM-->
    </td>
	<%} else if(raceAtt.equals("0")){%>
	<input type="hidden" name="patrace" value=""> <!--KM-->

	<%}} else { //KM-if custom data is not available
	%><td > <%=LC.L_Prim_Race%><%--Primary Race*****--%> </td>
    <td>
		<%=dRace%>
    </td>


	<%}%>


	<%if (hashPgCustFld.containsKey("additional2")) {


			int fldNumAdd2= Integer.parseInt((String)hashPgCustFld.get("additional2"));
			String add2Mand = ((String)cdoPgField.getPcfMandatory().get(fldNumAdd2));
			String add2Lable = ((String)cdoPgField.getPcfLabel().get(fldNumAdd2));
			String add2Att = ((String)cdoPgField.getPcfAttribute().get(fldNumAdd2));

			disableStr ="";
			if(add2Att == null) add2Att ="";if(add2Mand == null) add2Mand ="";




			if(!add2Att.equals("0")) {
			if(add2Lable !=null){
			%>
				<td >
			<%=add2Lable%>
			<%} else {%> <td >
			<%=LC.L_Additional%><%--Additional*****--%>
			<%}

			if (add2Mand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatadditional2">* </FONT>
			<%
		 }%>


		  <%if(add2Att.equals("1")) {
			 disableStr = "disabled class='readonly-input'"; }
		     //else if (add2Att.equals("2") ) {
			 //readOnlyStr = "readonly"; }
		 %>


	</td>
	<td >
		<input type="text" name="txtAddRace" size =30 MAXLENGTH = 500  <%=disableStr%> Readonly value='<%=addRaceNames%>'>

		 <%if(!add2Att.equals("1")  &&  !add2Att.equals("2") ) { %>

		<A href="#" onClick="openRaceWindow(document.patient)"><%=LC.L_Select_Race%><%--Select Race*****--%></A>

         <%}%>

	</td>
	<%}} else { //KM-if custom data is not available

	%>
		<td > <%=LC.L_Additional%><%--Additional*****--%> </td>  <td > <input type="text" name="txtAddRace" size =30 MAXLENGTH = 500  Readonly value='<%=addRaceNames%>'>
		<A href="#" onClick="openRaceWindow(document.patient)"><%=LC.L_Select_Race%><%--Select Race*****--%></A> </td>


  <%}%>

  </tr>
 </table>




  <DIV id="overlay" style="z-index:20;">
<table cellspacing="0" cellpadding="0" border="0"   class="basetbl midAlign bgcolorFix">
	<tr height="7"><td colspan="5"></td></tr>
	<tr>
		<td colspan="5">
			<p><%=LC.L_Reg_Dets%><%--Registration Details*****--%></p>
		</td>
	</tr>
	<tr>
	 <%if (hashPgCustFld.containsKey("org")) {
			int fldNumOrg= Integer.parseInt((String)hashPgCustFld.get("org"));
			String orgMand = ((String)cdoPgField.getPcfMandatory().get(fldNumOrg));
			String orgLable = ((String)cdoPgField.getPcfLabel().get(fldNumOrg));
			orgAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumOrg));


			if(orgAtt == null) orgAtt ="";
			if(orgMand == null) orgMand ="";


			if(!orgAtt.equals("0")) {
			if(orgLable !=null){
			%> <td width="15%">
			<%=orgLable%>
			<%} else {%> <td width="15%">
			<%=LC.L_Organization%><%--Organization*****--%>
			<%}

			if (orgMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatorg">* </FONT>
			<% } %>  <!--KM-050208-->

    </td>
    <td width="35%" >
		<%=dSites%>
		<%if(orgAtt.equals("1")) {%>
			<input type="hidden" name="patorganization" value="">
		<%} else if(orgAtt.equals("2")) {%>
			<input type="hidden" name="patorganization" value="<%=primSiteId%>">
		<%}%>
    </td>
	<%} else if(orgAtt.equals("0")) {%>

	<input type="hidden" name="patorganization" value=""> <!--KM-->
	<%}} else {%> <!-- KM-if custom data is not available.. -->

	<td width="15%"><%=LC.L_Organization%><%--Organization*****--%> <FONT class="Mandatory" id="pgcustompatorg">* </FONT> </td>
    <td  width="35%">

	<%=dSites%>

    <%}%>



<%if (hashPgCustFld.containsKey("patfacid")) {

			int fldNumPfid= Integer.parseInt((String)hashPgCustFld.get("patfacid"));
			String pfMand = ((String)cdoPgField.getPcfMandatory().get(fldNumPfid));
			String pfLable = ((String)cdoPgField.getPcfLabel().get(fldNumPfid));
			String pfAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumPfid));

			disableStr ="";
			readOnlyStr ="";
			if(pfAtt == null) pfAtt ="";
			if(pfMand == null) pfMand ="";


			if(!pfAtt.equals("0")) {
			if(pfLable !=null){
			%><td width="15%">
			<%=pfLable%>
			<%} else {%> <td width="15%">
			<%=LC.L_PatFacility_ID%><%--<%=LC.Pat_Patient%> Facility ID*****--%>
			<%}

			if (pfMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatfacid">* </FONT>
			<% }
		 %>

		  <%if(pfAtt.equals("1")) {
			 disableStr = "disabled class='readonly-input'"; }
		    else if (pfAtt.equals("2") ) {
			 readOnlyStr = "readonly"; }
		 %>



    </td>
    <td width="34%">
		<input type="text" name="patFacilityID" size = 30 MAXLENGTH = 20 <%=disableStr%>  <%=readOnlyStr%>  value='<%=patientFacilityId%>'>
	</td>
    	<% }}

	else { //KM-if custom data is not available
		 %>

		<td width="15%"> <%=LC.L_PatFacility_ID%><%--<%=LC.Pat_Patient%> Facility ID*****--%>   </td>
    <td  width="34%">
		<input type="text" name="patFacilityID" size = 30 MAXLENGTH = 20   value='<%=patientFacilityId%>'>
	</td>


  <%}%>

</tr>

	<tr>



	   <%if (hashPgCustFld.containsKey("provider")) {


			int fldNumProv= Integer.parseInt((String)hashPgCustFld.get("provider"));

			String provMand = ((String)cdoPgField.getPcfMandatory().get(fldNumProv));
			String provLable = ((String)cdoPgField.getPcfLabel().get(fldNumProv));
			String provAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumProv));

			disableStr ="";
			if(provAtt == null) provAtt ="";
			if(provMand == null) provMand ="";




			if(!provAtt.equals("0")) {
			if(provLable !=null){
			%><td >
			<%=provLable%>
			<%} else {%> <td >
			<%=LC.L_Provider%><%--Provider*****--%>
			<%}
			if (provMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatprov">* </FONT>
			<%
		 }%>



		  <%if(provAtt.equals("1")) {
			 disableStr = "disabled class='readonly-input'"; }
		     //else if (provAtt.equals("2") ) {//KM
			 //readOnlyStr = "readonly"; }
		 %>


    </td>
    <td >
    	<input type=hidden name=patregby value='<%=regBy%>'>
    	<input type=text name=patregbyname value='<%=regByName%>' <%=disableStr%> readonly>

		  <%if(!provAtt.equals("1") && !provAtt.equals("2")) {%>
		  <A HREF=# onClick=openwin1()><%=LC.L_Select_User%><%--Select User*****--%></A>
		  <%}%>
    </td>
	<%
		 }} else { //KM-if custom value is not available
	 %>
		<td > <%=LC.L_Provider%><%--Provider*****--%> </td> <td >
    	<input type=hidden name=patregby value='<%=regBy%>'>
    	<input type=text name=patregbyname value='<%=regByName%>' readonly> <A HREF=# onClick=openwin1()><%=LC.L_Select_User%><%--Select User*****--%></A> </td>

	 <%}%>





  <%if (hashPgCustFld.containsKey("ifother")) {


			int fldNumIf= Integer.parseInt((String)hashPgCustFld.get("ifother"));

			String ifOthMand = ((String)cdoPgField.getPcfMandatory().get(fldNumIf));
			String ifOthLable = ((String)cdoPgField.getPcfLabel().get(fldNumIf));
			String ifOthAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumIf));


			disableStr ="";
			readOnlyStr ="";
			if(ifOthAtt == null) ifOthAtt ="";
			if(ifOthMand == null) ifOthMand ="";


			if(!ifOthAtt.equals("0")) {
			if(ifOthLable !=null){
			%> <td>
			<%=ifOthLable%>
			<%} else {%> <td >
			<%=LC.L_If_Other%><%--If Other*****--%>
			<%}
			if (ifOthMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatifoth">* </FONT>
			<%
		 }%>


		  <%if(ifOthAtt.equals("1")) {
			 disableStr = "disabled class='readonly-input'"; }
		     else if (ifOthAtt.equals("2") ) {
			 readOnlyStr = "readonly"; }
		 %>



  </td><td ><input type="text" name="phyOther" size = 30 <%=disableStr%>  <%=readOnlyStr%> value="<%=phyOther%>"></td>

  <%}} else { //KM-if custom value is not available

  %>
	<td > <%=LC.L_If_Other%><%--If Other*****--%>  </td><td ><input type="text" name="phyOther" size = 30 value="<%=phyOther%>"></td>

  <%}%>

  </tr>
  <tr>


	 <%if (hashPgCustFld.containsKey("survivestat")) {
			int fldNumSurStat= Integer.parseInt((String)hashPgCustFld.get("survivestat"));
			String surStatMand = ((String)cdoPgField.getPcfMandatory().get(fldNumSurStat));
			String surStatLable = ((String)cdoPgField.getPcfLabel().get(fldNumSurStat));
			surStatAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumSurStat));

			if(surStatAtt == null) surStatAtt ="";
			if(surStatMand == null) surStatMand ="";

			if (!surStatAtt.equals("0")) {
			if(surStatLable !=null){
			%><td >
			<%=surStatLable%>
			<%} else {%> <td >
			<%=LC.L_Survival_Status%><%--Survival Status*****--%>
			<%}

			if (surStatMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatsstate">* </FONT>
      <%}%>

    </td>
    <td>
	<%=dStatus%>
	<%if (surStatAtt.equals("1")) {%>
		<input type="hidden" name="patstatus" value="">
	<%} else if (surStatAtt.equals("2")) {%>
		<input type="hidden" name="patstatus" value="">
	<%}%>
    </td>
	<%} else if(surStatAtt.equals("0")) { %>
	<input type="hidden" name="patstatus" value="">

	<%}} else {%>
	   <td > <%=LC.L_Survival_Status%><%--Survival Status*****--%> <FONT class="Mandatory" id="pgcustompatsstate">* </FONT> </td>
    <td >
	<%=dStatus%>
    </td>

    <%}%>


 <%if (hashPgCustFld.containsKey("dod")) {

			int fldNumDod= Integer.parseInt((String)hashPgCustFld.get("dod"));

			String dodMand = ((String)cdoPgField.getPcfMandatory().get(fldNumDod));
			String dodLable = ((String)cdoPgField.getPcfLabel().get(fldNumDod));
			String dodAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumDod));


			disableStr ="";
			readOnlyStr ="";
			if(dodAtt == null) dodAtt ="";
			if(dodMand == null) dodMand ="";


			if(!dodAtt.equals("0")) {
			if(dodLable !=null){
			%>  <td >
			<%=dodLable%>
			<%} else {%> <td >
			 <%=LC.L_Date_OfDeath%><%--Date of Death*****--%>
			<%}
			if (dodMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatdod">* </FONT>
			<%
		 }%>

		 <%if(dodAtt.equals("1")) {
			 disableStr = "disabled class='readonly-input'"; }
		     else if (dodAtt.equals("2") ) {
			 readOnlyStr = "readonly"; }
		 %>



    </td>
    <td >
<%-- INF-20084 Datepicker-- AGodara --%>	
		<%if (completeDet){%>
			<%if (StringUtil.isEmpty(dodAtt)){ %>
				<input type="text" name="patdeathdate" class="datefield" size = 15 MAXLENGTH = 50	value = <%=deathDate%>>
			<%} else if(dodAtt.equals("1") || dodAtt.equals("2")) {%>
				<input type=<%=inputType%> name="patdeathdate"  <%=disableStr%> <%=readOnlyStr%> size = 10 MAXLENGTH = 11 value="<%=deathDate%>" <%=disableStr%> ><%=displayStar%>
			<%}
		}else{%>
			<input type=<%=inputType%> name="patdeathdate" size = 10 MAXLENGTH = 11 value="<%=deathDate%>" <%=disableStr%> readonly ><%=displayStar%>
		<%}%>
    </td>
	<%}}else {
		//KM-if custom value is not available
	%> 
		<td> <%=LC.L_Date_OfDeath%><%--Date of Death*****--%> </td> 
		<td>
	<% if (completeDet){ %>
			<input type=<%=inputType%> name="patdeathdate" class="datefield" size = 10 MAXLENGTH = 11 value="<%=deathDate%>"  ><%=displayStar%>
	<%}else{%>
	
			<input type=<%=inputType%> name="patdeathdate" size = 10 MAXLENGTH = 11 value="<%=deathDate%>"  ><%=displayStar%>
	<% }}%>
	<td>
  </tr>
  <%// May Enhn%>
  <tr>


   <%if (hashPgCustFld.containsKey("cod")) {


			int fldNumCod= Integer.parseInt((String)hashPgCustFld.get("cod"));

			String codMand = ((String)cdoPgField.getPcfMandatory().get(fldNumCod));
			String codLable = ((String)cdoPgField.getPcfLabel().get(fldNumCod));
			codAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumCod));


			if(codAtt == null) codAtt ="";
			if(codMand == null) codMand ="";


			if(!codAtt.equals("0")) {
			if(codLable !=null){
			%><td >
			<%=codLable%>
			<%} else {%> <td >
			 <%=LC.L_Death_Cause%><%--Cause of Death*****--%>
			<%}
			if (codMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatcod">* </FONT>
			<%
		 }%>


 </td><td ><%=dDthCause%> <%if(codAtt.equals("1")) {%><input type="hidden" name="dthCause" value=""> <%}%><!--KM--></td>

  <%}}
  else {  //KM-if backend data is not available
  %>
     <td >  <%=LC.L_Death_Cause%><%--Cause of Death*****--%></td><td ><%=dDthCause%></td>
  <%}%>



<%

String odthCause=request.getParameter("othDthCause");

if(odthCause==null) { odthCause=""; } %> <!--JM: 01.31.2006 #2495 "REASON OF DEATH" changed to "SPECIFY CAUSE"--> <!--<td width="15%">REASON OF DEATH</td><td width="35%"><input type = text name="otherdthCause" size = 30 value="<%--=odthCause--%>"></td> -->



<%if (hashPgCustFld.containsKey("speccause")) {


			int fldNumSpec= Integer.parseInt((String)hashPgCustFld.get("speccause"));
			String specMand = ((String)cdoPgField.getPcfMandatory().get(fldNumSpec));
			String specLable = ((String)cdoPgField.getPcfLabel().get(fldNumSpec));
			String specAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumSpec));


			disableStr ="";
			readOnlyStr ="";
			if(specAtt == null) specAtt ="";
			if(specMand == null) specMand ="";



			if(!specAtt.equals("0")) {
			if(specLable !=null){
			%>  <td >
			<%=specLable%>
			<%} else {%> <td >
			  <%=LC.L_Specify_Cause%><%--Specify Cause*****--%>
			<%}
			if (specMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatspeccause">* </FONT>
			<%
		 }%>


	     <%if(specAtt.equals("1")) {
			 disableStr = "disabled class='readonly-input'"; }
		   else if (specAtt.equals("2") ) {
			 readOnlyStr = "readonly"; }
		 %>



	</td><td ><input type = text name="otherdthCause" size = 30  <%=disableStr%>  <%=readOnlyStr%> value="<%=odthCause%>"></td>
	<%}} else {  //KM-if backend data is not available
	%>
          <td > <%=LC.L_Specify_Cause%><%--Specify Cause*****--%>  </td><td ><input type = text name="otherdthCause" size = 30 value="<%=odthCause%>"></td>

	<%}%>

  </tr>
  <%//-- May Enhn%>
<tr height="7"><td colspan="5"></td></tr>
  </table>
  </DIV>
	<BR>
  <table cellspacing="1" cellpadding="0" align="center" class="basetbl outline midAlign bgcolorFix">
<tr height="7"><td colspan="5"></td></tr>
<tr>
	<td width="65%" >
 <%=MC.M_SpecDeptAcc_Dgraphics%><%--Specify groups/departments with access to edit <%=LC.Pat_Patient_Lower%>'s demographics.
		If blank, all groups have access*****--%> &nbsp;
</td><td width="35%">
		<input type=text name=txtSpeciality  size=15 readOnly value="<%=specialName%>">


		<A href="#" onClick="openSpecialityWindow(document.patient)"><%=LC.L_Select_Speciality_Upper%><%--SELECT SPECIALITY*****--%></A>

</td>


	</tr>

<tr>	<td width="100%" align="center" colspan="5" id="selectStudyLabel"><br>
	<%=MC.M_SelEtr_DetsPat%><%--Select a <%=LC.Std_Study_Lower%> to enter screening/enrollment details for this <%=LC.Pat_Patient_Lower%>*****--%>:  &nbsp;<%=strStudyId%></td></tr>

  </table>

	<BR>


	<% String showSubmit ="";
if ((mode.equals("M") && (pageRight >= 6 && orgRight >=6)) || (mode.equals("N") && (pageRight == 5  || pageRight == 7 ) ))
{	showSubmit ="Y";} else {showSubmit ="N";}
%>
	<jsp:include page="submitBar.jsp" flush="true">
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="patientForm"/>
			<jsp:param name="showDiscard" value="N"/>
			<jsp:param name="showSubmit" value="<%=showSubmit%>"/>
	</jsp:include>


<br>

</Form>

<%

	} //end of if body for page right

else

{

%>

<jsp:include page="accessdenied.jsp" flush="true"/>

<%

} //end of else body for page right

}//end of if body for session

else

{

%>

<jsp:include page="timeout.html" flush="true"/>

<%

}

%>



<div class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>



</div>


<DIV id="wsDIV" class="pageInst" >
</DIV>

<%
String accId = (String) tSession.getValue("accountId");
int accIdNum = EJBUtil.stringToNum(accId);
SettingsDao settingsDao =
	commonB.retrieveSettings(accIdNum, 1);
final String ACCELARATED_ENROLLMENT_CONFIG = "ACCELARATED_ENROLLMENT_CONFIG";
boolean isAccelaratedEnrollOn = false;
for (int x = 0; x<settingsDao.getSettingKeyword().size(); x++){
	String settingKey = (String)settingsDao.getSettingKeyword().get(x);
	if(ACCELARATED_ENROLLMENT_CONFIG.equalsIgnoreCase(settingKey)){
		isAccelaratedEnrollOn = true;
		break;
	}
}


if (wsEnabled.equals("Y") || isAccelaratedEnrollOn){ %>
<div class ="transparent" id = "trans"></div>
<%CommonDAO commonDao=commonB.getCommonHandle();
commonDao.getMappingData("\'race\',\'gender\',\'ethnicity\'",accCode);
ArrayList mappingType=commonDao.getMapping_type();
ArrayList mappingSubTyp=commonDao.getMapping_subTypList();
ArrayList mappingLMapList=commonDao.getMapping_lMapList();
ArrayList mappingRMapList=commonDao.getMapping_rMapList();
%>
<DIV style="visibility:hidden">
<%for (int i=0;i<mappingType.size();i++) {
	String mappingTypeStr=(String)mappingType.get(i);
	mappingTypeStr=(mappingTypeStr==null)?"":mappingTypeStr;
	String mappingSubTypeStr=(String)mappingSubTyp.get(i);
	mappingSubTypeStr=(mappingSubTypeStr==null)?"":mappingSubTypeStr;
	String mappingLMapStr=(String)mappingLMapList.get(i);
	mappingLMapStr=(mappingLMapStr==null)?"":mappingLMapStr;
	String mappingRMapStr=(String)mappingRMapList.get(i);
	mappingRMapStr=(mappingRMapStr==null)?"":mappingRMapStr;

	if (mappingTypeStr.toLowerCase().equals("race")) {
%>
<input type="text" id="race_<%=mappingRMapStr%>" value='<%=mappingLMapStr%>'>
<%}else if (mappingTypeStr.toLowerCase().equals("gender")){ %>
<input type="text" id="gender_<%=mappingRMapStr%>" value='<%=mappingLMapStr%>'>
<%}else if (mappingTypeStr.toLowerCase().equals("ethnicity")){ %>
<input type="text" id="ethnicity_<%=mappingRMapStr%>" value='<%=mappingLMapStr%>'>
<%}} %>
</DIV>
<%} %>

<div class ="mainMenu" id = "emenu">


<jsp:include page="getmenu.jsp" flush="true"/>
</div>

</body>

<%
String defaultPatId = request.getParameter("defaultPatId");
String defaultFacility = request.getParameter("defaultFac");
String defaultStudyPK = request.getParameter("defaultStudyPK");
String defaultStudyName = request.getParameter("defaultStudyName");
	%>
<script>
toggleOrg();
patStatEMR();
var defaultStudyPK = "<%=defaultStudyPK%>";
var defaultStudyName = "<%=defaultStudyName%>";
var defaultFacility = "<%=defaultFacility%>";
var defaultPatId = "<%=defaultPatId%>";
<%if (defaultStudyPK != null){%>

if (defaultStudyPK){
	document.getElementById("selectStudyLabel").style.visibility = "hidden";
	document.getElementById("patientForm").selstudyId.style.visibility = "hidden";
	$("studyNumberMessage").innerHTML="<%=MC.M_AddPat_Std%>/*Adding <%=LC.Pat_Patient_Lower%> for <%=LC.Std_Study%>*****/"+": '" + defaultStudyName + "'";
	$("studyDIV").style.visibility = "visible";
	$("studyDIV").style.height="15px";
	updateSourceField(document.getElementById("patientForm").selstudyId, defaultStudyPK);

}
<%} //end study fcondution%>
if (defaultFacility){
	updateSourceField(document.getElementById("patientForm").patorganization, defaultFacility);
}

if (defaultPatId){
	updateSourceField(document.getElementById("patientForm").patid, defaultPatId);
}
</script>
	<%


//if a remotePatNum got sent in, we're going to do a bunch of work
String remotePatStr = request.getParameter("remotePatSearchIndex");

if (remotePatStr != null){
	//can't use EJBUtil.stringToNum to parse this because that
	//method will return 0 for a parse error, which is a valid index number.
	int remotePatInt = -1;
	try{
		remotePatInt = Integer.parseInt(remotePatStr);
	}
	catch(NumberFormatException e){
		e.printStackTrace();
	}
	if (remotePatInt > -1){
%>
<script>

	<%//function added 1/22/2010 by DRM
	//Updates fields in this page to reflect patient selected on another page.
	//Remote serach in patientSearchWorkflowJSON.jsp adds a JSONArray of patients
	//to the session. If the request contains the remotePatSearchIndex parameter, then
	//the page that called this page (for example patientSearchWorkflow.jsp) wants this
	//page to default patient demographics fields from the patList array.

	JSONArray patList = (JSONArray)tSession.getAttribute("jsPatList");
	JSONObject jsPatient = null;

	if (patList != null){
		//tSession.removeAttribute("jsPatList");
		jsPatient = patList.getJSONObject(remotePatInt);
	}


	%>

	var jsPat = <%=jsPatient.toString()%>
	if (jsPat){
		//alert(jsPat);
		updateDemoFromJSON(jsPat);

	}
	function updateDemoFromJSON(jsPatient){
		var transparentDiv = document.getElementById("trans");
		if (transparentDiv){
			transparentDiv.style.visibility="hidden";
		}
		//update fields
		updateSourceField(document.getElementById("patientForm").patid, jsPatient.MRN);
		var patientDOBDate = new Date(jsPatient.DOB);
		//calDateFormat defined in dateformatSetting.js
		updateSourceField(document.getElementById("patientForm").patdob, formatDate(patientDOBDate, calDateFormat));
		updateSourceField(document.getElementById("patientForm").patfname, jsPatient.firstName);
		updateSourceField(document.getElementById("patientForm").patlname, jsPatient.lastName);

		updateSourceField(document.getElementById("patientForm").patgender, dereferenceCode(jsPatient.gender, "gender"));

		updateSourceField(document.getElementById("patientForm").patrace, dereferenceCode(jsPatient.race, "race"));
		updateSourceField(document.getElementById("patientForm").patethnicity, 	dereferenceCode(jsPatient.ethnicity, "ethnicity"));

	};

	</script>
	<%
	}
}
%>
</html>
