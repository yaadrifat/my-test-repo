This is a custom project of Velos eResearch for BMC.

At this time, the base eResearch version is v9.2.x. This project contains 
files which are modified or added on top of the standard eResearch.

-------------------------------------------------------
Perform these:

1) Add these lines to labelBundle_custom.properties

# For ADT Interface
Config_PT_HOST=http://<PT_HOST>:<PT_PORT>
Config_PT_EMR_URL=/PatientEMRServer/PatientEMR
Config_PT_USER=tenseUser
Config_PT_PASSWORD=tensePassword