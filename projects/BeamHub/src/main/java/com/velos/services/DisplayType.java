
package com.velos.services;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for displayType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="displayType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="STUDY"/>
 *     &lt;enumeration value="PATIENT_ENROLLED_TO_SPECIFIC_STUDY"/>
 *     &lt;enumeration value="ACCOUNT"/>
 *     &lt;enumeration value="ALL_STUDIES"/>
 *     &lt;enumeration value="ALL_PATIENTS"/>
 *     &lt;enumeration value="PATIENT_LEVEL_ALL_STUDIES"/>
 *     &lt;enumeration value="PATIENT_LEVEL_ALL_STUDIES_RESTRICTED"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "displayType")
@XmlEnum
public enum DisplayType {

    STUDY,
    PATIENT_ENROLLED_TO_SPECIFIC_STUDY,
    ACCOUNT,
    ALL_STUDIES,
    ALL_PATIENTS,
    PATIENT_LEVEL_ALL_STUDIES,
    PATIENT_LEVEL_ALL_STUDIES_RESTRICTED;

    public String value() {
        return name();
    }

    public static DisplayType fromValue(String v) {
        return valueOf(v);
    }

}
