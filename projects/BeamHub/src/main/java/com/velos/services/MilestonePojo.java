
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for milestonePojo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="milestonePojo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CREATED_ON" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="CREATOR" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="FK_BGTCAL" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="FK_BGTSECTION" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="FK_BUDGET" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="FK_CAL" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="FK_CODELST_MILESTONE_STAT" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="FK_CODELST_RULE" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="FK_EVENTASSOC" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="FK_LINEITEM" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="FK_STUDY" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="FK_VISIT" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="IP_ADD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LAST_CHECKED_ON" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="LAST_MODIFIED_BY" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="LAST_MODIFIED_DATE" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="MILESTONE_ACHIEVEDCOUNT" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="MILESTONE_AMOUNT" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="MILESTONE_COUNT" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="MILESTONE_DATE_FROM" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="MILESTONE_DATE_TO" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="MILESTONE_DELFLAG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MILESTONE_DESCRIPTION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MILESTONE_EVENTSTATUS" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="MILESTONE_HOLDBACK" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="MILESTONE_ISACTIVE" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="MILESTONE_LIMIT" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="MILESTONE_PAYBYUNIT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MILESTONE_PAYDUEBY" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="MILESTONE_PAYFOR" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="MILESTONE_PAYTYPE" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="MILESTONE_STATUS" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="MILESTONE_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MILESTONE_USERSTO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MILESTONE_VISIT" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="PK_MILESTONE" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="RID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "milestonePojo", propOrder = {
    "createdon",
    "creator",
    "fkbgtcal",
    "fkbgtsection",
    "fkbudget",
    "fkcal",
    "fkcodelstmilestonestat",
    "fkcodelstrule",
    "fkeventassoc",
    "fklineitem",
    "fkstudy",
    "fkvisit",
    "ipadd",
    "lastcheckedon",
    "lastmodifiedby",
    "lastmodifieddate",
    "milestoneachievedcount",
    "milestoneamount",
    "milestonecount",
    "milestonedatefrom",
    "milestonedateto",
    "milestonedelflag",
    "milestonedescription",
    "milestoneeventstatus",
    "milestoneholdback",
    "milestoneisactive",
    "milestonelimit",
    "milestonepaybyunit",
    "milestonepaydueby",
    "milestonepayfor",
    "milestonepaytype",
    "milestonestatus",
    "milestonetype",
    "milestoneusersto",
    "milestonevisit",
    "pkmilestone",
    "rid"
})
public class MilestonePojo {

    @XmlElement(name = "CREATED_ON")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar createdon;
    @XmlElement(name = "CREATOR")
    protected Integer creator;
    @XmlElement(name = "FK_BGTCAL")
    protected Integer fkbgtcal;
    @XmlElement(name = "FK_BGTSECTION")
    protected Integer fkbgtsection;
    @XmlElement(name = "FK_BUDGET")
    protected Integer fkbudget;
    @XmlElement(name = "FK_CAL")
    protected Integer fkcal;
    @XmlElement(name = "FK_CODELST_MILESTONE_STAT")
    protected Integer fkcodelstmilestonestat;
    @XmlElement(name = "FK_CODELST_RULE")
    protected Integer fkcodelstrule;
    @XmlElement(name = "FK_EVENTASSOC")
    protected Integer fkeventassoc;
    @XmlElement(name = "FK_LINEITEM")
    protected Integer fklineitem;
    @XmlElement(name = "FK_STUDY")
    protected Integer fkstudy;
    @XmlElement(name = "FK_VISIT")
    protected Integer fkvisit;
    @XmlElement(name = "IP_ADD")
    protected String ipadd;
    @XmlElement(name = "LAST_CHECKED_ON")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar lastcheckedon;
    @XmlElement(name = "LAST_MODIFIED_BY")
    protected Integer lastmodifiedby;
    @XmlElement(name = "LAST_MODIFIED_DATE")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar lastmodifieddate;
    @XmlElement(name = "MILESTONE_ACHIEVEDCOUNT")
    protected Integer milestoneachievedcount;
    @XmlElement(name = "MILESTONE_AMOUNT")
    protected Double milestoneamount;
    @XmlElement(name = "MILESTONE_COUNT")
    protected Integer milestonecount;
    @XmlElement(name = "MILESTONE_DATE_FROM")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar milestonedatefrom;
    @XmlElement(name = "MILESTONE_DATE_TO")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar milestonedateto;
    @XmlElement(name = "MILESTONE_DELFLAG")
    protected String milestonedelflag;
    @XmlElement(name = "MILESTONE_DESCRIPTION")
    protected String milestonedescription;
    @XmlElement(name = "MILESTONE_EVENTSTATUS")
    protected Integer milestoneeventstatus;
    @XmlElement(name = "MILESTONE_HOLDBACK")
    protected Integer milestoneholdback;
    @XmlElement(name = "MILESTONE_ISACTIVE")
    protected Boolean milestoneisactive;
    @XmlElement(name = "MILESTONE_LIMIT")
    protected Integer milestonelimit;
    @XmlElement(name = "MILESTONE_PAYBYUNIT")
    protected String milestonepaybyunit;
    @XmlElement(name = "MILESTONE_PAYDUEBY")
    protected Integer milestonepaydueby;
    @XmlElement(name = "MILESTONE_PAYFOR")
    protected Integer milestonepayfor;
    @XmlElement(name = "MILESTONE_PAYTYPE")
    protected Integer milestonepaytype;
    @XmlElement(name = "MILESTONE_STATUS")
    protected Integer milestonestatus;
    @XmlElement(name = "MILESTONE_TYPE")
    protected String milestonetype;
    @XmlElement(name = "MILESTONE_USERSTO")
    protected String milestoneusersto;
    @XmlElement(name = "MILESTONE_VISIT")
    protected Integer milestonevisit;
    @XmlElement(name = "PK_MILESTONE")
    protected Integer pkmilestone;
    @XmlElement(name = "RID")
    protected Integer rid;

    /**
     * Gets the value of the createdon property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCREATEDON() {
        return createdon;
    }

    /**
     * Sets the value of the createdon property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCREATEDON(XMLGregorianCalendar value) {
        this.createdon = value;
    }

    /**
     * Gets the value of the creator property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCREATOR() {
        return creator;
    }

    /**
     * Sets the value of the creator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCREATOR(Integer value) {
        this.creator = value;
    }

    /**
     * Gets the value of the fkbgtcal property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFKBGTCAL() {
        return fkbgtcal;
    }

    /**
     * Sets the value of the fkbgtcal property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFKBGTCAL(Integer value) {
        this.fkbgtcal = value;
    }

    /**
     * Gets the value of the fkbgtsection property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFKBGTSECTION() {
        return fkbgtsection;
    }

    /**
     * Sets the value of the fkbgtsection property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFKBGTSECTION(Integer value) {
        this.fkbgtsection = value;
    }

    /**
     * Gets the value of the fkbudget property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFKBUDGET() {
        return fkbudget;
    }

    /**
     * Sets the value of the fkbudget property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFKBUDGET(Integer value) {
        this.fkbudget = value;
    }

    /**
     * Gets the value of the fkcal property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFKCAL() {
        return fkcal;
    }

    /**
     * Sets the value of the fkcal property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFKCAL(Integer value) {
        this.fkcal = value;
    }

    /**
     * Gets the value of the fkcodelstmilestonestat property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFKCODELSTMILESTONESTAT() {
        return fkcodelstmilestonestat;
    }

    /**
     * Sets the value of the fkcodelstmilestonestat property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFKCODELSTMILESTONESTAT(Integer value) {
        this.fkcodelstmilestonestat = value;
    }

    /**
     * Gets the value of the fkcodelstrule property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFKCODELSTRULE() {
        return fkcodelstrule;
    }

    /**
     * Sets the value of the fkcodelstrule property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFKCODELSTRULE(Integer value) {
        this.fkcodelstrule = value;
    }

    /**
     * Gets the value of the fkeventassoc property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFKEVENTASSOC() {
        return fkeventassoc;
    }

    /**
     * Sets the value of the fkeventassoc property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFKEVENTASSOC(Integer value) {
        this.fkeventassoc = value;
    }

    /**
     * Gets the value of the fklineitem property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFKLINEITEM() {
        return fklineitem;
    }

    /**
     * Sets the value of the fklineitem property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFKLINEITEM(Integer value) {
        this.fklineitem = value;
    }

    /**
     * Gets the value of the fkstudy property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFKSTUDY() {
        return fkstudy;
    }

    /**
     * Sets the value of the fkstudy property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFKSTUDY(Integer value) {
        this.fkstudy = value;
    }

    /**
     * Gets the value of the fkvisit property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFKVISIT() {
        return fkvisit;
    }

    /**
     * Sets the value of the fkvisit property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFKVISIT(Integer value) {
        this.fkvisit = value;
    }

    /**
     * Gets the value of the ipadd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIPADD() {
        return ipadd;
    }

    /**
     * Sets the value of the ipadd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIPADD(String value) {
        this.ipadd = value;
    }

    /**
     * Gets the value of the lastcheckedon property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLASTCHECKEDON() {
        return lastcheckedon;
    }

    /**
     * Sets the value of the lastcheckedon property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLASTCHECKEDON(XMLGregorianCalendar value) {
        this.lastcheckedon = value;
    }

    /**
     * Gets the value of the lastmodifiedby property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLASTMODIFIEDBY() {
        return lastmodifiedby;
    }

    /**
     * Sets the value of the lastmodifiedby property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLASTMODIFIEDBY(Integer value) {
        this.lastmodifiedby = value;
    }

    /**
     * Gets the value of the lastmodifieddate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLASTMODIFIEDDATE() {
        return lastmodifieddate;
    }

    /**
     * Sets the value of the lastmodifieddate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLASTMODIFIEDDATE(XMLGregorianCalendar value) {
        this.lastmodifieddate = value;
    }

    /**
     * Gets the value of the milestoneachievedcount property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMILESTONEACHIEVEDCOUNT() {
        return milestoneachievedcount;
    }

    /**
     * Sets the value of the milestoneachievedcount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMILESTONEACHIEVEDCOUNT(Integer value) {
        this.milestoneachievedcount = value;
    }

    /**
     * Gets the value of the milestoneamount property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getMILESTONEAMOUNT() {
        return milestoneamount;
    }

    /**
     * Sets the value of the milestoneamount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setMILESTONEAMOUNT(Double value) {
        this.milestoneamount = value;
    }

    /**
     * Gets the value of the milestonecount property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMILESTONECOUNT() {
        return milestonecount;
    }

    /**
     * Sets the value of the milestonecount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMILESTONECOUNT(Integer value) {
        this.milestonecount = value;
    }

    /**
     * Gets the value of the milestonedatefrom property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getMILESTONEDATEFROM() {
        return milestonedatefrom;
    }

    /**
     * Sets the value of the milestonedatefrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMILESTONEDATEFROM(XMLGregorianCalendar value) {
        this.milestonedatefrom = value;
    }

    /**
     * Gets the value of the milestonedateto property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getMILESTONEDATETO() {
        return milestonedateto;
    }

    /**
     * Sets the value of the milestonedateto property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMILESTONEDATETO(XMLGregorianCalendar value) {
        this.milestonedateto = value;
    }

    /**
     * Gets the value of the milestonedelflag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMILESTONEDELFLAG() {
        return milestonedelflag;
    }

    /**
     * Sets the value of the milestonedelflag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMILESTONEDELFLAG(String value) {
        this.milestonedelflag = value;
    }

    /**
     * Gets the value of the milestonedescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMILESTONEDESCRIPTION() {
        return milestonedescription;
    }

    /**
     * Sets the value of the milestonedescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMILESTONEDESCRIPTION(String value) {
        this.milestonedescription = value;
    }

    /**
     * Gets the value of the milestoneeventstatus property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMILESTONEEVENTSTATUS() {
        return milestoneeventstatus;
    }

    /**
     * Sets the value of the milestoneeventstatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMILESTONEEVENTSTATUS(Integer value) {
        this.milestoneeventstatus = value;
    }

    /**
     * Gets the value of the milestoneholdback property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMILESTONEHOLDBACK() {
        return milestoneholdback;
    }

    /**
     * Sets the value of the milestoneholdback property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMILESTONEHOLDBACK(Integer value) {
        this.milestoneholdback = value;
    }

    /**
     * Gets the value of the milestoneisactive property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMILESTONEISACTIVE() {
        return milestoneisactive;
    }

    /**
     * Sets the value of the milestoneisactive property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMILESTONEISACTIVE(Boolean value) {
        this.milestoneisactive = value;
    }

    /**
     * Gets the value of the milestonelimit property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMILESTONELIMIT() {
        return milestonelimit;
    }

    /**
     * Sets the value of the milestonelimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMILESTONELIMIT(Integer value) {
        this.milestonelimit = value;
    }

    /**
     * Gets the value of the milestonepaybyunit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMILESTONEPAYBYUNIT() {
        return milestonepaybyunit;
    }

    /**
     * Sets the value of the milestonepaybyunit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMILESTONEPAYBYUNIT(String value) {
        this.milestonepaybyunit = value;
    }

    /**
     * Gets the value of the milestonepaydueby property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMILESTONEPAYDUEBY() {
        return milestonepaydueby;
    }

    /**
     * Sets the value of the milestonepaydueby property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMILESTONEPAYDUEBY(Integer value) {
        this.milestonepaydueby = value;
    }

    /**
     * Gets the value of the milestonepayfor property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMILESTONEPAYFOR() {
        return milestonepayfor;
    }

    /**
     * Sets the value of the milestonepayfor property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMILESTONEPAYFOR(Integer value) {
        this.milestonepayfor = value;
    }

    /**
     * Gets the value of the milestonepaytype property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMILESTONEPAYTYPE() {
        return milestonepaytype;
    }

    /**
     * Sets the value of the milestonepaytype property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMILESTONEPAYTYPE(Integer value) {
        this.milestonepaytype = value;
    }

    /**
     * Gets the value of the milestonestatus property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMILESTONESTATUS() {
        return milestonestatus;
    }

    /**
     * Sets the value of the milestonestatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMILESTONESTATUS(Integer value) {
        this.milestonestatus = value;
    }

    /**
     * Gets the value of the milestonetype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMILESTONETYPE() {
        return milestonetype;
    }

    /**
     * Sets the value of the milestonetype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMILESTONETYPE(String value) {
        this.milestonetype = value;
    }

    /**
     * Gets the value of the milestoneusersto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMILESTONEUSERSTO() {
        return milestoneusersto;
    }

    /**
     * Sets the value of the milestoneusersto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMILESTONEUSERSTO(String value) {
        this.milestoneusersto = value;
    }

    /**
     * Gets the value of the milestonevisit property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMILESTONEVISIT() {
        return milestonevisit;
    }

    /**
     * Sets the value of the milestonevisit property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMILESTONEVISIT(Integer value) {
        this.milestonevisit = value;
    }

    /**
     * Gets the value of the pkmilestone property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPKMILESTONE() {
        return pkmilestone;
    }

    /**
     * Sets the value of the pkmilestone property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPKMILESTONE(Integer value) {
        this.pkmilestone = value;
    }

    /**
     * Gets the value of the rid property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRID() {
        return rid;
    }

    /**
     * Sets the value of the rid property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRID(Integer value) {
        this.rid = value;
    }

}
