
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getStudyCalBudget complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getStudyCalBudget">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StudyIdentifier" type="{http://velos.com/services/}studyIdentifier" minOccurs="0"/>
 *         &lt;element name="CalendarNameIdentifier" type="{http://velos.com/services/}calendarNameIdentifier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getStudyCalBudget", propOrder = {
    "studyIdentifier",
    "calendarNameIdentifier"
})
public class GetStudyCalBudget {

    @XmlElement(name = "StudyIdentifier")
    protected StudyIdentifier studyIdentifier;
    @XmlElement(name = "CalendarNameIdentifier")
    protected CalendarNameIdentifier calendarNameIdentifier;

    /**
     * Gets the value of the studyIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link StudyIdentifier }
     *     
     */
    public StudyIdentifier getStudyIdentifier() {
        return studyIdentifier;
    }

    /**
     * Sets the value of the studyIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyIdentifier }
     *     
     */
    public void setStudyIdentifier(StudyIdentifier value) {
        this.studyIdentifier = value;
    }

    /**
     * Gets the value of the calendarNameIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link CalendarNameIdentifier }
     *     
     */
    public CalendarNameIdentifier getCalendarNameIdentifier() {
        return calendarNameIdentifier;
    }

    /**
     * Sets the value of the calendarNameIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link CalendarNameIdentifier }
     *     
     */
    public void setCalendarNameIdentifier(CalendarNameIdentifier value) {
        this.calendarNameIdentifier = value;
    }

}
