
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for formField complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="formField">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="align" type="{http://velos.com/services/}alignment" minOccurs="0"/>
 *         &lt;element name="bold" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="browserFld" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="characteristics" type="{http://velos.com/services/}fieldCharacteristics" minOccurs="0"/>
 *         &lt;element name="choices" type="{http://velos.com/services/}multipleChoices" minOccurs="0"/>
 *         &lt;element name="dataType" type="{http://velos.com/services/}dataType" minOccurs="0"/>
 *         &lt;element name="dateValidations" type="{http://velos.com/services/}dateFieldValidations" minOccurs="0"/>
 *         &lt;element name="decimal" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="desc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="displayWidth" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="expandLabel" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="fieldIdentifier" type="{http://velos.com/services/}fieldIdentifier" minOccurs="0"/>
 *         &lt;element name="fieldNameFormat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fieldType" type="{http://velos.com/services/}fieldType" minOccurs="0"/>
 *         &lt;element name="hideLabel" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="instructions" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="italics" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="keyword" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="length" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="lookUpDataValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lookUpDisplayValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lookUpType" type="{http://velos.com/services/}lookupType" minOccurs="0"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numberValidations" type="{http://velos.com/services/}numberFieldValidations" minOccurs="0"/>
 *         &lt;element name="readOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="sameline" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="sequence" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="sortOrder" type="{http://velos.com/services/}sortOrder" minOccurs="0"/>
 *         &lt;element name="systemID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="textValidations" type="{http://velos.com/services/}textFieldValidations" minOccurs="0"/>
 *         &lt;element name="underlined" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="unique" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="uniqueID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="validations" type="{http://velos.com/services/}fieldValidations" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "formField", propOrder = {
    "align",
    "bold",
    "browserFld",
    "characteristics",
    "choices",
    "dataType",
    "dateValidations",
    "decimal",
    "desc",
    "displayWidth",
    "expandLabel",
    "fieldIdentifier",
    "fieldNameFormat",
    "fieldType",
    "hideLabel",
    "instructions",
    "italics",
    "keyword",
    "length",
    "lookUpDataValue",
    "lookUpDisplayValue",
    "lookUpType",
    "name",
    "numberValidations",
    "readOnly",
    "sameline",
    "sequence",
    "sortOrder",
    "systemID",
    "textValidations",
    "underlined",
    "unique",
    "uniqueID",
    "validations"
})
public class FormField
    extends ServiceObject
{

    protected Alignment align;
    protected Boolean bold;
    protected Boolean browserFld;
    protected FieldCharacteristics characteristics;
    protected MultipleChoices choices;
    protected DataType dataType;
    protected DateFieldValidations dateValidations;
    protected Integer decimal;
    protected String desc;
    protected Integer displayWidth;
    protected Boolean expandLabel;
    protected FieldIdentifier fieldIdentifier;
    protected String fieldNameFormat;
    protected FieldType fieldType;
    protected Boolean hideLabel;
    protected String instructions;
    protected Boolean italics;
    protected String keyword;
    protected int length;
    protected String lookUpDataValue;
    protected String lookUpDisplayValue;
    protected LookupType lookUpType;
    protected String name;
    protected NumberFieldValidations numberValidations;
    protected Boolean readOnly;
    protected Boolean sameline;
    protected Integer sequence;
    protected SortOrder sortOrder;
    protected String systemID;
    protected TextFieldValidations textValidations;
    protected Boolean underlined;
    protected Boolean unique;
    protected String uniqueID;
    protected FieldValidations validations;

    /**
     * Gets the value of the align property.
     * 
     * @return
     *     possible object is
     *     {@link Alignment }
     *     
     */
    public Alignment getAlign() {
        return align;
    }

    /**
     * Sets the value of the align property.
     * 
     * @param value
     *     allowed object is
     *     {@link Alignment }
     *     
     */
    public void setAlign(Alignment value) {
        this.align = value;
    }

    /**
     * Gets the value of the bold property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBold() {
        return bold;
    }

    /**
     * Sets the value of the bold property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBold(Boolean value) {
        this.bold = value;
    }

    /**
     * Gets the value of the browserFld property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBrowserFld() {
        return browserFld;
    }

    /**
     * Sets the value of the browserFld property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBrowserFld(Boolean value) {
        this.browserFld = value;
    }

    /**
     * Gets the value of the characteristics property.
     * 
     * @return
     *     possible object is
     *     {@link FieldCharacteristics }
     *     
     */
    public FieldCharacteristics getCharacteristics() {
        return characteristics;
    }

    /**
     * Sets the value of the characteristics property.
     * 
     * @param value
     *     allowed object is
     *     {@link FieldCharacteristics }
     *     
     */
    public void setCharacteristics(FieldCharacteristics value) {
        this.characteristics = value;
    }

    /**
     * Gets the value of the choices property.
     * 
     * @return
     *     possible object is
     *     {@link MultipleChoices }
     *     
     */
    public MultipleChoices getChoices() {
        return choices;
    }

    /**
     * Sets the value of the choices property.
     * 
     * @param value
     *     allowed object is
     *     {@link MultipleChoices }
     *     
     */
    public void setChoices(MultipleChoices value) {
        this.choices = value;
    }

    /**
     * Gets the value of the dataType property.
     * 
     * @return
     *     possible object is
     *     {@link DataType }
     *     
     */
    public DataType getDataType() {
        return dataType;
    }

    /**
     * Sets the value of the dataType property.
     * 
     * @param value
     *     allowed object is
     *     {@link DataType }
     *     
     */
    public void setDataType(DataType value) {
        this.dataType = value;
    }

    /**
     * Gets the value of the dateValidations property.
     * 
     * @return
     *     possible object is
     *     {@link DateFieldValidations }
     *     
     */
    public DateFieldValidations getDateValidations() {
        return dateValidations;
    }

    /**
     * Sets the value of the dateValidations property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateFieldValidations }
     *     
     */
    public void setDateValidations(DateFieldValidations value) {
        this.dateValidations = value;
    }

    /**
     * Gets the value of the decimal property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDecimal() {
        return decimal;
    }

    /**
     * Sets the value of the decimal property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDecimal(Integer value) {
        this.decimal = value;
    }

    /**
     * Gets the value of the desc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesc() {
        return desc;
    }

    /**
     * Sets the value of the desc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesc(String value) {
        this.desc = value;
    }

    /**
     * Gets the value of the displayWidth property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDisplayWidth() {
        return displayWidth;
    }

    /**
     * Sets the value of the displayWidth property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDisplayWidth(Integer value) {
        this.displayWidth = value;
    }

    /**
     * Gets the value of the expandLabel property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isExpandLabel() {
        return expandLabel;
    }

    /**
     * Sets the value of the expandLabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExpandLabel(Boolean value) {
        this.expandLabel = value;
    }

    /**
     * Gets the value of the fieldIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link FieldIdentifier }
     *     
     */
    public FieldIdentifier getFieldIdentifier() {
        return fieldIdentifier;
    }

    /**
     * Sets the value of the fieldIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link FieldIdentifier }
     *     
     */
    public void setFieldIdentifier(FieldIdentifier value) {
        this.fieldIdentifier = value;
    }

    /**
     * Gets the value of the fieldNameFormat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFieldNameFormat() {
        return fieldNameFormat;
    }

    /**
     * Sets the value of the fieldNameFormat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFieldNameFormat(String value) {
        this.fieldNameFormat = value;
    }

    /**
     * Gets the value of the fieldType property.
     * 
     * @return
     *     possible object is
     *     {@link FieldType }
     *     
     */
    public FieldType getFieldType() {
        return fieldType;
    }

    /**
     * Sets the value of the fieldType property.
     * 
     * @param value
     *     allowed object is
     *     {@link FieldType }
     *     
     */
    public void setFieldType(FieldType value) {
        this.fieldType = value;
    }

    /**
     * Gets the value of the hideLabel property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isHideLabel() {
        return hideLabel;
    }

    /**
     * Sets the value of the hideLabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHideLabel(Boolean value) {
        this.hideLabel = value;
    }

    /**
     * Gets the value of the instructions property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInstructions() {
        return instructions;
    }

    /**
     * Sets the value of the instructions property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInstructions(String value) {
        this.instructions = value;
    }

    /**
     * Gets the value of the italics property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isItalics() {
        return italics;
    }

    /**
     * Sets the value of the italics property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setItalics(Boolean value) {
        this.italics = value;
    }

    /**
     * Gets the value of the keyword property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKeyword() {
        return keyword;
    }

    /**
     * Sets the value of the keyword property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKeyword(String value) {
        this.keyword = value;
    }

    /**
     * Gets the value of the length property.
     * 
     */
    public int getLength() {
        return length;
    }

    /**
     * Sets the value of the length property.
     * 
     */
    public void setLength(int value) {
        this.length = value;
    }

    /**
     * Gets the value of the lookUpDataValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLookUpDataValue() {
        return lookUpDataValue;
    }

    /**
     * Sets the value of the lookUpDataValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLookUpDataValue(String value) {
        this.lookUpDataValue = value;
    }

    /**
     * Gets the value of the lookUpDisplayValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLookUpDisplayValue() {
        return lookUpDisplayValue;
    }

    /**
     * Sets the value of the lookUpDisplayValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLookUpDisplayValue(String value) {
        this.lookUpDisplayValue = value;
    }

    /**
     * Gets the value of the lookUpType property.
     * 
     * @return
     *     possible object is
     *     {@link LookupType }
     *     
     */
    public LookupType getLookUpType() {
        return lookUpType;
    }

    /**
     * Sets the value of the lookUpType property.
     * 
     * @param value
     *     allowed object is
     *     {@link LookupType }
     *     
     */
    public void setLookUpType(LookupType value) {
        this.lookUpType = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the numberValidations property.
     * 
     * @return
     *     possible object is
     *     {@link NumberFieldValidations }
     *     
     */
    public NumberFieldValidations getNumberValidations() {
        return numberValidations;
    }

    /**
     * Sets the value of the numberValidations property.
     * 
     * @param value
     *     allowed object is
     *     {@link NumberFieldValidations }
     *     
     */
    public void setNumberValidations(NumberFieldValidations value) {
        this.numberValidations = value;
    }

    /**
     * Gets the value of the readOnly property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isReadOnly() {
        return readOnly;
    }

    /**
     * Sets the value of the readOnly property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReadOnly(Boolean value) {
        this.readOnly = value;
    }

    /**
     * Gets the value of the sameline property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSameline() {
        return sameline;
    }

    /**
     * Sets the value of the sameline property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSameline(Boolean value) {
        this.sameline = value;
    }

    /**
     * Gets the value of the sequence property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSequence() {
        return sequence;
    }

    /**
     * Sets the value of the sequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSequence(Integer value) {
        this.sequence = value;
    }

    /**
     * Gets the value of the sortOrder property.
     * 
     * @return
     *     possible object is
     *     {@link SortOrder }
     *     
     */
    public SortOrder getSortOrder() {
        return sortOrder;
    }

    /**
     * Sets the value of the sortOrder property.
     * 
     * @param value
     *     allowed object is
     *     {@link SortOrder }
     *     
     */
    public void setSortOrder(SortOrder value) {
        this.sortOrder = value;
    }

    /**
     * Gets the value of the systemID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSystemID() {
        return systemID;
    }

    /**
     * Sets the value of the systemID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSystemID(String value) {
        this.systemID = value;
    }

    /**
     * Gets the value of the textValidations property.
     * 
     * @return
     *     possible object is
     *     {@link TextFieldValidations }
     *     
     */
    public TextFieldValidations getTextValidations() {
        return textValidations;
    }

    /**
     * Sets the value of the textValidations property.
     * 
     * @param value
     *     allowed object is
     *     {@link TextFieldValidations }
     *     
     */
    public void setTextValidations(TextFieldValidations value) {
        this.textValidations = value;
    }

    /**
     * Gets the value of the underlined property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUnderlined() {
        return underlined;
    }

    /**
     * Sets the value of the underlined property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUnderlined(Boolean value) {
        this.underlined = value;
    }

    /**
     * Gets the value of the unique property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUnique() {
        return unique;
    }

    /**
     * Sets the value of the unique property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUnique(Boolean value) {
        this.unique = value;
    }

    /**
     * Gets the value of the uniqueID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUniqueID() {
        return uniqueID;
    }

    /**
     * Sets the value of the uniqueID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUniqueID(String value) {
        this.uniqueID = value;
    }

    /**
     * Gets the value of the validations property.
     * 
     * @return
     *     possible object is
     *     {@link FieldValidations }
     *     
     */
    public FieldValidations getValidations() {
        return validations;
    }

    /**
     * Sets the value of the validations property.
     * 
     * @param value
     *     allowed object is
     *     {@link FieldValidations }
     *     
     */
    public void setValidations(FieldValidations value) {
        this.validations = value;
    }

}
