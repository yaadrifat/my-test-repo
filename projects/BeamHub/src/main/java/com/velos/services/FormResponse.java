
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for formResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="formResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="formFieldResponses" type="{http://velos.com/services/}formFieldResponses" minOccurs="0"/>
 *         &lt;element name="formFillDt" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="formId" type="{http://velos.com/services/}formIdentifier" minOccurs="0"/>
 *         &lt;element name="formStatus" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="formVersion" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "formResponse", propOrder = {
    "formFieldResponses",
    "formFillDt",
    "formId",
    "formStatus",
    "formVersion"
})
@XmlSeeAlso({
    StudyFormResponse.class,
    PatientFormResponse.class,
    AccountFormResponse.class,
    StudyPatientFormResponse.class
})
public class FormResponse
    extends ServiceObject
{

    protected FormFieldResponses formFieldResponses;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar formFillDt;
    protected FormIdentifier formId;
    protected Code formStatus;
    protected Integer formVersion;

    /**
     * Gets the value of the formFieldResponses property.
     * 
     * @return
     *     possible object is
     *     {@link FormFieldResponses }
     *     
     */
    public FormFieldResponses getFormFieldResponses() {
        return formFieldResponses;
    }

    /**
     * Sets the value of the formFieldResponses property.
     * 
     * @param value
     *     allowed object is
     *     {@link FormFieldResponses }
     *     
     */
    public void setFormFieldResponses(FormFieldResponses value) {
        this.formFieldResponses = value;
    }

    /**
     * Gets the value of the formFillDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFormFillDt() {
        return formFillDt;
    }

    /**
     * Sets the value of the formFillDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFormFillDt(XMLGregorianCalendar value) {
        this.formFillDt = value;
    }

    /**
     * Gets the value of the formId property.
     * 
     * @return
     *     possible object is
     *     {@link FormIdentifier }
     *     
     */
    public FormIdentifier getFormId() {
        return formId;
    }

    /**
     * Sets the value of the formId property.
     * 
     * @param value
     *     allowed object is
     *     {@link FormIdentifier }
     *     
     */
    public void setFormId(FormIdentifier value) {
        this.formId = value;
    }

    /**
     * Gets the value of the formStatus property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getFormStatus() {
        return formStatus;
    }

    /**
     * Sets the value of the formStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setFormStatus(Code value) {
        this.formStatus = value;
    }

    /**
     * Gets the value of the formVersion property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFormVersion() {
        return formVersion;
    }

    /**
     * Sets the value of the formVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFormVersion(Integer value) {
        this.formVersion = value;
    }

}
