
package com.velos.services;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for bgtLiniItemDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="bgtLiniItemDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="budgetLineItemInfo" type="{http://velos.com/services/}budgetLineItemPojo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "bgtLiniItemDetail", propOrder = {
    "budgetLineItemInfo"
})
public class BgtLiniItemDetail {

    @XmlElement(nillable = true)
    protected List<BudgetLineItemPojo> budgetLineItemInfo;

    /**
     * Gets the value of the budgetLineItemInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the budgetLineItemInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBudgetLineItemInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BudgetLineItemPojo }
     * 
     * 
     */
    public List<BudgetLineItemPojo> getBudgetLineItemInfo() {
        if (budgetLineItemInfo == null) {
            budgetLineItemInfo = new ArrayList<BudgetLineItemPojo>();
        }
        return this.budgetLineItemInfo;
    }

}
