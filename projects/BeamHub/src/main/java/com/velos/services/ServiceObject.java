
package com.velos.services;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for serviceObject complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="serviceObject">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "serviceObject")
@XmlSeeAlso({
    AccountFormResponses.class,
    StudyPatientFormResponses.class,
    NumberRange.class,
    FormFieldResponses.class,
    FormFieldResponse.class,
    StudyFormResponses.class,
    PatientFormResponses.class,
    FormResponse.class,
    FieldValidations.class
})
public abstract class ServiceObject implements Serializable {


}
