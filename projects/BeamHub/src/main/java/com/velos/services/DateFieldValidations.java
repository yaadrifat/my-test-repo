
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for dateFieldValidations complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="dateFieldValidations">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}fieldValidations">
 *       &lt;sequence>
 *         &lt;element name="defaultDateToCurrent" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="futureDateAllowed" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="overrideDateValidation" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "dateFieldValidations", propOrder = {
    "defaultDateToCurrent",
    "futureDateAllowed",
    "overrideDateValidation"
})
public class DateFieldValidations
    extends FieldValidations
{

    protected boolean defaultDateToCurrent;
    protected boolean futureDateAllowed;
    protected boolean overrideDateValidation;

    /**
     * Gets the value of the defaultDateToCurrent property.
     * 
     */
    public boolean isDefaultDateToCurrent() {
        return defaultDateToCurrent;
    }

    /**
     * Sets the value of the defaultDateToCurrent property.
     * 
     */
    public void setDefaultDateToCurrent(boolean value) {
        this.defaultDateToCurrent = value;
    }

    /**
     * Gets the value of the futureDateAllowed property.
     * 
     */
    public boolean isFutureDateAllowed() {
        return futureDateAllowed;
    }

    /**
     * Sets the value of the futureDateAllowed property.
     * 
     */
    public void setFutureDateAllowed(boolean value) {
        this.futureDateAllowed = value;
    }

    /**
     * Gets the value of the overrideDateValidation property.
     * 
     */
    public boolean isOverrideDateValidation() {
        return overrideDateValidation;
    }

    /**
     * Sets the value of the overrideDateValidation property.
     * 
     */
    public void setOverrideDateValidation(boolean value) {
        this.overrideDateValidation = value;
    }

}
