
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for searchStudyResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="searchStudyResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StudySearchResults" type="{http://velos.com/services/}studySearchResults" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "searchStudyResponse", propOrder = {
    "studySearchResults"
})
public class SearchStudyResponse {

    @XmlElement(name = "StudySearchResults")
    protected StudySearchResults studySearchResults;

    /**
     * Gets the value of the studySearchResults property.
     * 
     * @return
     *     possible object is
     *     {@link StudySearchResults }
     *     
     */
    public StudySearchResults getStudySearchResults() {
        return studySearchResults;
    }

    /**
     * Sets the value of the studySearchResults property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudySearchResults }
     *     
     */
    public void setStudySearchResults(StudySearchResults value) {
        this.studySearchResults = value;
    }

}
