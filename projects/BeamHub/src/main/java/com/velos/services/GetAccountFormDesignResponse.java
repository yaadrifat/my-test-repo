
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getAccountFormDesignResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getAccountFormDesignResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LinkedFormDesign" type="{http://velos.com/services/}linkedFormDesign" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getAccountFormDesignResponse", propOrder = {
    "linkedFormDesign"
})
public class GetAccountFormDesignResponse {

    @XmlElement(name = "LinkedFormDesign")
    protected LinkedFormDesign linkedFormDesign;

    /**
     * Gets the value of the linkedFormDesign property.
     * 
     * @return
     *     possible object is
     *     {@link LinkedFormDesign }
     *     
     */
    public LinkedFormDesign getLinkedFormDesign() {
        return linkedFormDesign;
    }

    /**
     * Sets the value of the linkedFormDesign property.
     * 
     * @param value
     *     allowed object is
     *     {@link LinkedFormDesign }
     *     
     */
    public void setLinkedFormDesign(LinkedFormDesign value) {
        this.linkedFormDesign = value;
    }

}
