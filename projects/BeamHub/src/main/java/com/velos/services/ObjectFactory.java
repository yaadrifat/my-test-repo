
package com.velos.services;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.velos.services package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _UpdatePatientFormResponseResponse_QNAME = new QName("http://velos.com/services/", "updatePatientFormResponseResponse");
    private final static QName _CreatePatientFormResponseResponse_QNAME = new QName("http://velos.com/services/", "createPatientFormResponseResponse");
    private final static QName _GetAccountFormResponse_QNAME = new QName("http://velos.com/services/", "getAccountFormResponse");
    private final static QName _UpdateStudyPatientScheduleFormResponse_QNAME = new QName("http://velos.com/services/", "updateStudyPatientScheduleFormResponse");
    private final static QName _GetStudyPatientScheduleFormResponse_QNAME = new QName("http://velos.com/services/", "getStudyPatientScheduleFormResponse");
    private final static QName _GetListOfPatientFormResponsesResponse_QNAME = new QName("http://velos.com/services/", "getListOfPatientFormResponsesResponse");
    private final static QName _UpdateStudyFormResponseResponse_QNAME = new QName("http://velos.com/services/", "updateStudyFormResponseResponse");
    private final static QName _GetListOfStudyFormResponses_QNAME = new QName("http://velos.com/services/", "getListOfStudyFormResponses");
    private final static QName _RemoveAccountFormResponse_QNAME = new QName("http://velos.com/services/", "removeAccountFormResponse");
    private final static QName _UpdateAccountFormResponseResponse_QNAME = new QName("http://velos.com/services/", "updateAccountFormResponseResponse");
    private final static QName _CreateStudyPatientFormResponse_QNAME = new QName("http://velos.com/services/", "createStudyPatientFormResponse");
    private final static QName _RemoveStudyPatientScheduleFormResponseResponse_QNAME = new QName("http://velos.com/services/", "removeStudyPatientScheduleFormResponseResponse");
    private final static QName _GetListOfStudyFormResponsesResponse_QNAME = new QName("http://velos.com/services/", "getListOfStudyFormResponsesResponse");
    private final static QName _CreateStudyPatientScheduleFormResponse_QNAME = new QName("http://velos.com/services/", "createStudyPatientScheduleFormResponse");
    private final static QName _UpdateStudyPatientFormResponseResponse_QNAME = new QName("http://velos.com/services/", "updateStudyPatientFormResponseResponse");
    private final static QName _GetStudyPatientScheduleFormResponseResponse_QNAME = new QName("http://velos.com/services/", "getStudyPatientScheduleFormResponseResponse");
    private final static QName _GetAccountFormResponseResponse_QNAME = new QName("http://velos.com/services/", "getAccountFormResponseResponse");
    private final static QName _GetListOfStudyPatientFormResponsesResponse_QNAME = new QName("http://velos.com/services/", "getListOfStudyPatientFormResponsesResponse");
    private final static QName _GetListOfAccountFormResponsesResponse_QNAME = new QName("http://velos.com/services/", "getListOfAccountFormResponsesResponse");
    private final static QName _UpdateAccountFormResponse_QNAME = new QName("http://velos.com/services/", "updateAccountFormResponse");
    private final static QName _CreateStudyPatientFormResponseResponse_QNAME = new QName("http://velos.com/services/", "createStudyPatientFormResponseResponse");
    private final static QName _CreatePatientFormResponse_QNAME = new QName("http://velos.com/services/", "createPatientFormResponse");
    private final static QName _RemoveStudyFormResponseResponse_QNAME = new QName("http://velos.com/services/", "removeStudyFormResponseResponse");
    private final static QName _RemovePatientFormResponse_QNAME = new QName("http://velos.com/services/", "removePatientFormResponse");
    private final static QName _GetPatientFormResponseResponse_QNAME = new QName("http://velos.com/services/", "getPatientFormResponseResponse");
    private final static QName _GetListOfAccountFormResponses_QNAME = new QName("http://velos.com/services/", "getListOfAccountFormResponses");
    private final static QName _CreateStudyPatientScheduleFormResponseResponse_QNAME = new QName("http://velos.com/services/", "createStudyPatientScheduleFormResponseResponse");
    private final static QName _GetStudyPatientFormResponse_QNAME = new QName("http://velos.com/services/", "getStudyPatientFormResponse");
    private final static QName _GetStudyFormResponseResponse_QNAME = new QName("http://velos.com/services/", "getStudyFormResponseResponse");
    private final static QName _UpdateStudyFormResponse_QNAME = new QName("http://velos.com/services/", "updateStudyFormResponse");
    private final static QName _UpdateStudyPatientScheduleFormResponseResponse_QNAME = new QName("http://velos.com/services/", "updateStudyPatientScheduleFormResponseResponse");
    private final static QName _GetListOfStudyPatientFormResponses_QNAME = new QName("http://velos.com/services/", "getListOfStudyPatientFormResponses");
    private final static QName _RemoveStudyPatientFormResponseResponse_QNAME = new QName("http://velos.com/services/", "removeStudyPatientFormResponseResponse");
    private final static QName _GetPatientFormResponse_QNAME = new QName("http://velos.com/services/", "getPatientFormResponse");
    private final static QName _OperationException_QNAME = new QName("http://velos.com/services/", "OperationException");
    private final static QName _CreateStudyFormResponse_QNAME = new QName("http://velos.com/services/", "createStudyFormResponse");
    private final static QName _GetListOfPatientFormResponses_QNAME = new QName("http://velos.com/services/", "getListOfPatientFormResponses");
    private final static QName _CreateAccountFormResponseResponse_QNAME = new QName("http://velos.com/services/", "createAccountFormResponseResponse");
    private final static QName _UpdatePatientFormResponse_QNAME = new QName("http://velos.com/services/", "updatePatientFormResponse");
    private final static QName _RemoveStudyFormResponse_QNAME = new QName("http://velos.com/services/", "removeStudyFormResponse");
    private final static QName _GetStudyPatientFormResponseResponse_QNAME = new QName("http://velos.com/services/", "getStudyPatientFormResponseResponse");
    private final static QName _CreateAccountFormResponse_QNAME = new QName("http://velos.com/services/", "createAccountFormResponse");
    private final static QName _RemoveStudyPatientFormResponse_QNAME = new QName("http://velos.com/services/", "removeStudyPatientFormResponse");
    private final static QName _UpdateStudyPatientFormResponse_QNAME = new QName("http://velos.com/services/", "updateStudyPatientFormResponse");
    private final static QName _RemoveStudyPatientScheduleFormResponse_QNAME = new QName("http://velos.com/services/", "removeStudyPatientScheduleFormResponse");
    private final static QName _CreateStudyFormResponseResponse_QNAME = new QName("http://velos.com/services/", "createStudyFormResponseResponse");
    private final static QName _RemovePatientFormResponseResponse_QNAME = new QName("http://velos.com/services/", "removePatientFormResponseResponse");
    private final static QName _GetStudyFormResponse_QNAME = new QName("http://velos.com/services/", "getStudyFormResponse");
    private final static QName _RemoveAccountFormResponseResponse_QNAME = new QName("http://velos.com/services/", "removeAccountFormResponseResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.velos.services
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreateStudyPatientFormResponse }
     * 
     */
    public CreateStudyPatientFormResponse createCreateStudyPatientFormResponse() {
        return new CreateStudyPatientFormResponse();
    }

    /**
     * Create an instance of {@link RemoveAccountFormResponse }
     * 
     */
    public RemoveAccountFormResponse createRemoveAccountFormResponse() {
        return new RemoveAccountFormResponse();
    }

    /**
     * Create an instance of {@link UpdateAccountFormResponseResponse }
     * 
     */
    public UpdateAccountFormResponseResponse createUpdateAccountFormResponseResponse() {
        return new UpdateAccountFormResponseResponse();
    }

    /**
     * Create an instance of {@link GetListOfStudyFormResponsesResponse }
     * 
     */
    public GetListOfStudyFormResponsesResponse createGetListOfStudyFormResponsesResponse() {
        return new GetListOfStudyFormResponsesResponse();
    }

    /**
     * Create an instance of {@link RemoveStudyPatientScheduleFormResponseResponse }
     * 
     */
    public RemoveStudyPatientScheduleFormResponseResponse createRemoveStudyPatientScheduleFormResponseResponse() {
        return new RemoveStudyPatientScheduleFormResponseResponse();
    }

    /**
     * Create an instance of {@link GetListOfStudyFormResponses }
     * 
     */
    public GetListOfStudyFormResponses createGetListOfStudyFormResponses() {
        return new GetListOfStudyFormResponses();
    }

    /**
     * Create an instance of {@link UpdateStudyFormResponseResponse }
     * 
     */
    public UpdateStudyFormResponseResponse createUpdateStudyFormResponseResponse() {
        return new UpdateStudyFormResponseResponse();
    }

    /**
     * Create an instance of {@link GetListOfPatientFormResponsesResponse }
     * 
     */
    public GetListOfPatientFormResponsesResponse createGetListOfPatientFormResponsesResponse() {
        return new GetListOfPatientFormResponsesResponse();
    }

    /**
     * Create an instance of {@link GetStudyPatientScheduleFormResponse }
     * 
     */
    public GetStudyPatientScheduleFormResponse createGetStudyPatientScheduleFormResponse() {
        return new GetStudyPatientScheduleFormResponse();
    }

    /**
     * Create an instance of {@link CreatePatientFormResponseResponse }
     * 
     */
    public CreatePatientFormResponseResponse createCreatePatientFormResponseResponse() {
        return new CreatePatientFormResponseResponse();
    }

    /**
     * Create an instance of {@link GetAccountFormResponse }
     * 
     */
    public GetAccountFormResponse createGetAccountFormResponse() {
        return new GetAccountFormResponse();
    }

    /**
     * Create an instance of {@link UpdateStudyPatientScheduleFormResponse }
     * 
     */
    public UpdateStudyPatientScheduleFormResponse createUpdateStudyPatientScheduleFormResponse() {
        return new UpdateStudyPatientScheduleFormResponse();
    }

    /**
     * Create an instance of {@link UpdatePatientFormResponseResponse }
     * 
     */
    public UpdatePatientFormResponseResponse createUpdatePatientFormResponseResponse() {
        return new UpdatePatientFormResponseResponse();
    }

    /**
     * Create an instance of {@link RemoveStudyFormResponseResponse }
     * 
     */
    public RemoveStudyFormResponseResponse createRemoveStudyFormResponseResponse() {
        return new RemoveStudyFormResponseResponse();
    }

    /**
     * Create an instance of {@link GetListOfAccountFormResponsesResponse }
     * 
     */
    public GetListOfAccountFormResponsesResponse createGetListOfAccountFormResponsesResponse() {
        return new GetListOfAccountFormResponsesResponse();
    }

    /**
     * Create an instance of {@link CreatePatientFormResponse }
     * 
     */
    public CreatePatientFormResponse createCreatePatientFormResponse() {
        return new CreatePatientFormResponse();
    }

    /**
     * Create an instance of {@link CreateStudyPatientFormResponseResponse }
     * 
     */
    public CreateStudyPatientFormResponseResponse createCreateStudyPatientFormResponseResponse() {
        return new CreateStudyPatientFormResponseResponse();
    }

    /**
     * Create an instance of {@link UpdateAccountFormResponse }
     * 
     */
    public UpdateAccountFormResponse createUpdateAccountFormResponse() {
        return new UpdateAccountFormResponse();
    }

    /**
     * Create an instance of {@link GetListOfStudyPatientFormResponsesResponse }
     * 
     */
    public GetListOfStudyPatientFormResponsesResponse createGetListOfStudyPatientFormResponsesResponse() {
        return new GetListOfStudyPatientFormResponsesResponse();
    }

    /**
     * Create an instance of {@link GetAccountFormResponseResponse }
     * 
     */
    public GetAccountFormResponseResponse createGetAccountFormResponseResponse() {
        return new GetAccountFormResponseResponse();
    }

    /**
     * Create an instance of {@link GetStudyPatientScheduleFormResponseResponse }
     * 
     */
    public GetStudyPatientScheduleFormResponseResponse createGetStudyPatientScheduleFormResponseResponse() {
        return new GetStudyPatientScheduleFormResponseResponse();
    }

    /**
     * Create an instance of {@link UpdateStudyPatientFormResponseResponse }
     * 
     */
    public UpdateStudyPatientFormResponseResponse createUpdateStudyPatientFormResponseResponse() {
        return new UpdateStudyPatientFormResponseResponse();
    }

    /**
     * Create an instance of {@link CreateStudyPatientScheduleFormResponse }
     * 
     */
    public CreateStudyPatientScheduleFormResponse createCreateStudyPatientScheduleFormResponse() {
        return new CreateStudyPatientScheduleFormResponse();
    }

    /**
     * Create an instance of {@link GetListOfStudyPatientFormResponses }
     * 
     */
    public GetListOfStudyPatientFormResponses createGetListOfStudyPatientFormResponses() {
        return new GetListOfStudyPatientFormResponses();
    }

    /**
     * Create an instance of {@link UpdateStudyFormResponse }
     * 
     */
    public UpdateStudyFormResponse createUpdateStudyFormResponse() {
        return new UpdateStudyFormResponse();
    }

    /**
     * Create an instance of {@link UpdateStudyPatientScheduleFormResponseResponse }
     * 
     */
    public UpdateStudyPatientScheduleFormResponseResponse createUpdateStudyPatientScheduleFormResponseResponse() {
        return new UpdateStudyPatientScheduleFormResponseResponse();
    }

    /**
     * Create an instance of {@link RemoveStudyPatientFormResponseResponse }
     * 
     */
    public RemoveStudyPatientFormResponseResponse createRemoveStudyPatientFormResponseResponse() {
        return new RemoveStudyPatientFormResponseResponse();
    }

    /**
     * Create an instance of {@link GetStudyPatientFormResponse }
     * 
     */
    public GetStudyPatientFormResponse createGetStudyPatientFormResponse() {
        return new GetStudyPatientFormResponse();
    }

    /**
     * Create an instance of {@link CreateStudyPatientScheduleFormResponseResponse }
     * 
     */
    public CreateStudyPatientScheduleFormResponseResponse createCreateStudyPatientScheduleFormResponseResponse() {
        return new CreateStudyPatientScheduleFormResponseResponse();
    }

    /**
     * Create an instance of {@link GetStudyFormResponseResponse }
     * 
     */
    public GetStudyFormResponseResponse createGetStudyFormResponseResponse() {
        return new GetStudyFormResponseResponse();
    }

    /**
     * Create an instance of {@link GetListOfAccountFormResponses }
     * 
     */
    public GetListOfAccountFormResponses createGetListOfAccountFormResponses() {
        return new GetListOfAccountFormResponses();
    }

    /**
     * Create an instance of {@link RemovePatientFormResponse }
     * 
     */
    public RemovePatientFormResponse createRemovePatientFormResponse() {
        return new RemovePatientFormResponse();
    }

    /**
     * Create an instance of {@link GetPatientFormResponseResponse }
     * 
     */
    public GetPatientFormResponseResponse createGetPatientFormResponseResponse() {
        return new GetPatientFormResponseResponse();
    }

    /**
     * Create an instance of {@link RemoveStudyPatientScheduleFormResponse }
     * 
     */
    public RemoveStudyPatientScheduleFormResponse createRemoveStudyPatientScheduleFormResponse() {
        return new RemoveStudyPatientScheduleFormResponse();
    }

    /**
     * Create an instance of {@link RemoveAccountFormResponseResponse }
     * 
     */
    public RemoveAccountFormResponseResponse createRemoveAccountFormResponseResponse() {
        return new RemoveAccountFormResponseResponse();
    }

    /**
     * Create an instance of {@link GetStudyFormResponse }
     * 
     */
    public GetStudyFormResponse createGetStudyFormResponse() {
        return new GetStudyFormResponse();
    }

    /**
     * Create an instance of {@link CreateStudyFormResponseResponse }
     * 
     */
    public CreateStudyFormResponseResponse createCreateStudyFormResponseResponse() {
        return new CreateStudyFormResponseResponse();
    }

    /**
     * Create an instance of {@link RemovePatientFormResponseResponse }
     * 
     */
    public RemovePatientFormResponseResponse createRemovePatientFormResponseResponse() {
        return new RemovePatientFormResponseResponse();
    }

    /**
     * Create an instance of {@link RemoveStudyPatientFormResponse }
     * 
     */
    public RemoveStudyPatientFormResponse createRemoveStudyPatientFormResponse() {
        return new RemoveStudyPatientFormResponse();
    }

    /**
     * Create an instance of {@link UpdateStudyPatientFormResponse }
     * 
     */
    public UpdateStudyPatientFormResponse createUpdateStudyPatientFormResponse() {
        return new UpdateStudyPatientFormResponse();
    }

    /**
     * Create an instance of {@link RemoveStudyFormResponse }
     * 
     */
    public RemoveStudyFormResponse createRemoveStudyFormResponse() {
        return new RemoveStudyFormResponse();
    }

    /**
     * Create an instance of {@link UpdatePatientFormResponse }
     * 
     */
    public UpdatePatientFormResponse createUpdatePatientFormResponse() {
        return new UpdatePatientFormResponse();
    }

    /**
     * Create an instance of {@link CreateAccountFormResponseResponse }
     * 
     */
    public CreateAccountFormResponseResponse createCreateAccountFormResponseResponse() {
        return new CreateAccountFormResponseResponse();
    }

    /**
     * Create an instance of {@link CreateAccountFormResponse }
     * 
     */
    public CreateAccountFormResponse createCreateAccountFormResponse() {
        return new CreateAccountFormResponse();
    }

    /**
     * Create an instance of {@link GetStudyPatientFormResponseResponse }
     * 
     */
    public GetStudyPatientFormResponseResponse createGetStudyPatientFormResponseResponse() {
        return new GetStudyPatientFormResponseResponse();
    }

    /**
     * Create an instance of {@link OperationException }
     * 
     */
    public OperationException createOperationException() {
        return new OperationException();
    }

    /**
     * Create an instance of {@link GetPatientFormResponse }
     * 
     */
    public GetPatientFormResponse createGetPatientFormResponse() {
        return new GetPatientFormResponse();
    }

    /**
     * Create an instance of {@link GetListOfPatientFormResponses }
     * 
     */
    public GetListOfPatientFormResponses createGetListOfPatientFormResponses() {
        return new GetListOfPatientFormResponses();
    }

    /**
     * Create an instance of {@link CreateStudyFormResponse }
     * 
     */
    public CreateStudyFormResponse createCreateStudyFormResponse() {
        return new CreateStudyFormResponse();
    }

    /**
     * Create an instance of {@link StudyIdentifier }
     * 
     */
    public StudyIdentifier createStudyIdentifier() {
        return new StudyIdentifier();
    }

    /**
     * Create an instance of {@link StudyFormResponse }
     * 
     */
    public StudyFormResponse createStudyFormResponse() {
        return new StudyFormResponse();
    }

    /**
     * Create an instance of {@link SimpleIdentifier }
     * 
     */
    public SimpleIdentifier createSimpleIdentifier() {
        return new SimpleIdentifier();
    }

    /**
     * Create an instance of {@link NumberFieldValidations }
     * 
     */
    public NumberFieldValidations createNumberFieldValidations() {
        return new NumberFieldValidations();
    }

    /**
     * Create an instance of {@link VisitIdentifier }
     * 
     */
    public VisitIdentifier createVisitIdentifier() {
        return new VisitIdentifier();
    }

    /**
     * Create an instance of {@link AccountFormResponses }
     * 
     */
    public AccountFormResponses createAccountFormResponses() {
        return new AccountFormResponses();
    }

    /**
     * Create an instance of {@link StudyFormResponseIdentifier }
     * 
     */
    public StudyFormResponseIdentifier createStudyFormResponseIdentifier() {
        return new StudyFormResponseIdentifier();
    }

    /**
     * Create an instance of {@link StudyPatientFormResponses }
     * 
     */
    public StudyPatientFormResponses createStudyPatientFormResponses() {
        return new StudyPatientFormResponses();
    }

    /**
     * Create an instance of {@link FormResponse }
     * 
     */
    public FormResponse createFormResponse() {
        return new FormResponse();
    }

    /**
     * Create an instance of {@link Results }
     * 
     */
    public Results createResults() {
        return new Results();
    }

    /**
     * Create an instance of {@link NumberRange }
     * 
     */
    public NumberRange createNumberRange() {
        return new NumberRange();
    }

    /**
     * Create an instance of {@link EventIdentifier }
     * 
     */
    public EventIdentifier createEventIdentifier() {
        return new EventIdentifier();
    }

    /**
     * Create an instance of {@link PatientIdentifier }
     * 
     */
    public PatientIdentifier createPatientIdentifier() {
        return new PatientIdentifier();
    }

    /**
     * Create an instance of {@link PatientFormResponse }
     * 
     */
    public PatientFormResponse createPatientFormResponse() {
        return new PatientFormResponse();
    }

    /**
     * Create an instance of {@link FormIdentifier }
     * 
     */
    public FormIdentifier createFormIdentifier() {
        return new FormIdentifier();
    }

    /**
     * Create an instance of {@link StudyPatientScheduleFormResponse }
     * 
     */
    public StudyPatientScheduleFormResponse createStudyPatientScheduleFormResponse() {
        return new StudyPatientScheduleFormResponse();
    }

    /**
     * Create an instance of {@link DateFieldValidations }
     * 
     */
    public DateFieldValidations createDateFieldValidations() {
        return new DateFieldValidations();
    }

    /**
     * Create an instance of {@link Code }
     * 
     */
    public Code createCode() {
        return new Code();
    }

    /**
     * Create an instance of {@link FormFieldResponses }
     * 
     */
    public FormFieldResponses createFormFieldResponses() {
        return new FormFieldResponses();
    }

    /**
     * Create an instance of {@link Issues }
     * 
     */
    public Issues createIssues() {
        return new Issues();
    }

    /**
     * Create an instance of {@link FieldValidations }
     * 
     */
    public FieldValidations createFieldValidations() {
        return new FieldValidations();
    }

    /**
     * Create an instance of {@link StudyPatientFormResponseIdentifier }
     * 
     */
    public StudyPatientFormResponseIdentifier createStudyPatientFormResponseIdentifier() {
        return new StudyPatientFormResponseIdentifier();
    }

    /**
     * Create an instance of {@link AccountFormResponse }
     * 
     */
    public AccountFormResponse createAccountFormResponse() {
        return new AccountFormResponse();
    }

    /**
     * Create an instance of {@link FormFieldResponse }
     * 
     */
    public FormFieldResponse createFormFieldResponse() {
        return new FormFieldResponse();
    }

    /**
     * Create an instance of {@link Issue }
     * 
     */
    public Issue createIssue() {
        return new Issue();
    }

    /**
     * Create an instance of {@link StudyFormResponses }
     * 
     */
    public StudyFormResponses createStudyFormResponses() {
        return new StudyFormResponses();
    }

    /**
     * Create an instance of {@link FieldIdentifier }
     * 
     */
    public FieldIdentifier createFieldIdentifier() {
        return new FieldIdentifier();
    }

    /**
     * Create an instance of {@link ResponseHolder }
     * 
     */
    public ResponseHolder createResponseHolder() {
        return new ResponseHolder();
    }

    /**
     * Create an instance of {@link PatientProtocolIdentifier }
     * 
     */
    public PatientProtocolIdentifier createPatientProtocolIdentifier() {
        return new PatientProtocolIdentifier();
    }

    /**
     * Create an instance of {@link AccountIdentifier }
     * 
     */
    public AccountIdentifier createAccountIdentifier() {
        return new AccountIdentifier();
    }

    /**
     * Create an instance of {@link PatientFormResponses }
     * 
     */
    public PatientFormResponses createPatientFormResponses() {
        return new PatientFormResponses();
    }

    /**
     * Create an instance of {@link ScheduleEventIdentifier }
     * 
     */
    public ScheduleEventIdentifier createScheduleEventIdentifier() {
        return new ScheduleEventIdentifier();
    }

    /**
     * Create an instance of {@link AccountFormResponseIdentifier }
     * 
     */
    public AccountFormResponseIdentifier createAccountFormResponseIdentifier() {
        return new AccountFormResponseIdentifier();
    }

    /**
     * Create an instance of {@link CalendarIdentifier }
     * 
     */
    public CalendarIdentifier createCalendarIdentifier() {
        return new CalendarIdentifier();
    }

    /**
     * Create an instance of {@link PatientFormResponseIdentifier }
     * 
     */
    public PatientFormResponseIdentifier createPatientFormResponseIdentifier() {
        return new PatientFormResponseIdentifier();
    }

    /**
     * Create an instance of {@link StudyPatientFormResponse }
     * 
     */
    public StudyPatientFormResponse createStudyPatientFormResponse() {
        return new StudyPatientFormResponse();
    }

    /**
     * Create an instance of {@link CompletedAction }
     * 
     */
    public CompletedAction createCompletedAction() {
        return new CompletedAction();
    }

    /**
     * Create an instance of {@link OrganizationIdentifier }
     * 
     */
    public OrganizationIdentifier createOrganizationIdentifier() {
        return new OrganizationIdentifier();
    }

    /**
     * Create an instance of {@link TextFieldValidations }
     * 
     */
    public TextFieldValidations createTextFieldValidations() {
        return new TextFieldValidations();
    }

    /**
     * Create an instance of {@link FieldNameValIdentifier }
     * 
     */
    public FieldNameValIdentifier createFieldNameValIdentifier() {
        return new FieldNameValIdentifier();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdatePatientFormResponseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "updatePatientFormResponseResponse")
    public JAXBElement<UpdatePatientFormResponseResponse> createUpdatePatientFormResponseResponse(UpdatePatientFormResponseResponse value) {
        return new JAXBElement<UpdatePatientFormResponseResponse>(_UpdatePatientFormResponseResponse_QNAME, UpdatePatientFormResponseResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreatePatientFormResponseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "createPatientFormResponseResponse")
    public JAXBElement<CreatePatientFormResponseResponse> createCreatePatientFormResponseResponse(CreatePatientFormResponseResponse value) {
        return new JAXBElement<CreatePatientFormResponseResponse>(_CreatePatientFormResponseResponse_QNAME, CreatePatientFormResponseResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAccountFormResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "getAccountFormResponse")
    public JAXBElement<GetAccountFormResponse> createGetAccountFormResponse(GetAccountFormResponse value) {
        return new JAXBElement<GetAccountFormResponse>(_GetAccountFormResponse_QNAME, GetAccountFormResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateStudyPatientScheduleFormResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "updateStudyPatientScheduleFormResponse")
    public JAXBElement<UpdateStudyPatientScheduleFormResponse> createUpdateStudyPatientScheduleFormResponse(UpdateStudyPatientScheduleFormResponse value) {
        return new JAXBElement<UpdateStudyPatientScheduleFormResponse>(_UpdateStudyPatientScheduleFormResponse_QNAME, UpdateStudyPatientScheduleFormResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStudyPatientScheduleFormResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "getStudyPatientScheduleFormResponse")
    public JAXBElement<GetStudyPatientScheduleFormResponse> createGetStudyPatientScheduleFormResponse(GetStudyPatientScheduleFormResponse value) {
        return new JAXBElement<GetStudyPatientScheduleFormResponse>(_GetStudyPatientScheduleFormResponse_QNAME, GetStudyPatientScheduleFormResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetListOfPatientFormResponsesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "getListOfPatientFormResponsesResponse")
    public JAXBElement<GetListOfPatientFormResponsesResponse> createGetListOfPatientFormResponsesResponse(GetListOfPatientFormResponsesResponse value) {
        return new JAXBElement<GetListOfPatientFormResponsesResponse>(_GetListOfPatientFormResponsesResponse_QNAME, GetListOfPatientFormResponsesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateStudyFormResponseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "updateStudyFormResponseResponse")
    public JAXBElement<UpdateStudyFormResponseResponse> createUpdateStudyFormResponseResponse(UpdateStudyFormResponseResponse value) {
        return new JAXBElement<UpdateStudyFormResponseResponse>(_UpdateStudyFormResponseResponse_QNAME, UpdateStudyFormResponseResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetListOfStudyFormResponses }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "getListOfStudyFormResponses")
    public JAXBElement<GetListOfStudyFormResponses> createGetListOfStudyFormResponses(GetListOfStudyFormResponses value) {
        return new JAXBElement<GetListOfStudyFormResponses>(_GetListOfStudyFormResponses_QNAME, GetListOfStudyFormResponses.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveAccountFormResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "removeAccountFormResponse")
    public JAXBElement<RemoveAccountFormResponse> createRemoveAccountFormResponse(RemoveAccountFormResponse value) {
        return new JAXBElement<RemoveAccountFormResponse>(_RemoveAccountFormResponse_QNAME, RemoveAccountFormResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateAccountFormResponseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "updateAccountFormResponseResponse")
    public JAXBElement<UpdateAccountFormResponseResponse> createUpdateAccountFormResponseResponse(UpdateAccountFormResponseResponse value) {
        return new JAXBElement<UpdateAccountFormResponseResponse>(_UpdateAccountFormResponseResponse_QNAME, UpdateAccountFormResponseResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateStudyPatientFormResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "createStudyPatientFormResponse")
    public JAXBElement<CreateStudyPatientFormResponse> createCreateStudyPatientFormResponse(CreateStudyPatientFormResponse value) {
        return new JAXBElement<CreateStudyPatientFormResponse>(_CreateStudyPatientFormResponse_QNAME, CreateStudyPatientFormResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveStudyPatientScheduleFormResponseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "removeStudyPatientScheduleFormResponseResponse")
    public JAXBElement<RemoveStudyPatientScheduleFormResponseResponse> createRemoveStudyPatientScheduleFormResponseResponse(RemoveStudyPatientScheduleFormResponseResponse value) {
        return new JAXBElement<RemoveStudyPatientScheduleFormResponseResponse>(_RemoveStudyPatientScheduleFormResponseResponse_QNAME, RemoveStudyPatientScheduleFormResponseResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetListOfStudyFormResponsesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "getListOfStudyFormResponsesResponse")
    public JAXBElement<GetListOfStudyFormResponsesResponse> createGetListOfStudyFormResponsesResponse(GetListOfStudyFormResponsesResponse value) {
        return new JAXBElement<GetListOfStudyFormResponsesResponse>(_GetListOfStudyFormResponsesResponse_QNAME, GetListOfStudyFormResponsesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateStudyPatientScheduleFormResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "createStudyPatientScheduleFormResponse")
    public JAXBElement<CreateStudyPatientScheduleFormResponse> createCreateStudyPatientScheduleFormResponse(CreateStudyPatientScheduleFormResponse value) {
        return new JAXBElement<CreateStudyPatientScheduleFormResponse>(_CreateStudyPatientScheduleFormResponse_QNAME, CreateStudyPatientScheduleFormResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateStudyPatientFormResponseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "updateStudyPatientFormResponseResponse")
    public JAXBElement<UpdateStudyPatientFormResponseResponse> createUpdateStudyPatientFormResponseResponse(UpdateStudyPatientFormResponseResponse value) {
        return new JAXBElement<UpdateStudyPatientFormResponseResponse>(_UpdateStudyPatientFormResponseResponse_QNAME, UpdateStudyPatientFormResponseResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStudyPatientScheduleFormResponseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "getStudyPatientScheduleFormResponseResponse")
    public JAXBElement<GetStudyPatientScheduleFormResponseResponse> createGetStudyPatientScheduleFormResponseResponse(GetStudyPatientScheduleFormResponseResponse value) {
        return new JAXBElement<GetStudyPatientScheduleFormResponseResponse>(_GetStudyPatientScheduleFormResponseResponse_QNAME, GetStudyPatientScheduleFormResponseResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAccountFormResponseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "getAccountFormResponseResponse")
    public JAXBElement<GetAccountFormResponseResponse> createGetAccountFormResponseResponse(GetAccountFormResponseResponse value) {
        return new JAXBElement<GetAccountFormResponseResponse>(_GetAccountFormResponseResponse_QNAME, GetAccountFormResponseResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetListOfStudyPatientFormResponsesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "getListOfStudyPatientFormResponsesResponse")
    public JAXBElement<GetListOfStudyPatientFormResponsesResponse> createGetListOfStudyPatientFormResponsesResponse(GetListOfStudyPatientFormResponsesResponse value) {
        return new JAXBElement<GetListOfStudyPatientFormResponsesResponse>(_GetListOfStudyPatientFormResponsesResponse_QNAME, GetListOfStudyPatientFormResponsesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetListOfAccountFormResponsesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "getListOfAccountFormResponsesResponse")
    public JAXBElement<GetListOfAccountFormResponsesResponse> createGetListOfAccountFormResponsesResponse(GetListOfAccountFormResponsesResponse value) {
        return new JAXBElement<GetListOfAccountFormResponsesResponse>(_GetListOfAccountFormResponsesResponse_QNAME, GetListOfAccountFormResponsesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateAccountFormResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "updateAccountFormResponse")
    public JAXBElement<UpdateAccountFormResponse> createUpdateAccountFormResponse(UpdateAccountFormResponse value) {
        return new JAXBElement<UpdateAccountFormResponse>(_UpdateAccountFormResponse_QNAME, UpdateAccountFormResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateStudyPatientFormResponseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "createStudyPatientFormResponseResponse")
    public JAXBElement<CreateStudyPatientFormResponseResponse> createCreateStudyPatientFormResponseResponse(CreateStudyPatientFormResponseResponse value) {
        return new JAXBElement<CreateStudyPatientFormResponseResponse>(_CreateStudyPatientFormResponseResponse_QNAME, CreateStudyPatientFormResponseResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreatePatientFormResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "createPatientFormResponse")
    public JAXBElement<CreatePatientFormResponse> createCreatePatientFormResponse(CreatePatientFormResponse value) {
        return new JAXBElement<CreatePatientFormResponse>(_CreatePatientFormResponse_QNAME, CreatePatientFormResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveStudyFormResponseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "removeStudyFormResponseResponse")
    public JAXBElement<RemoveStudyFormResponseResponse> createRemoveStudyFormResponseResponse(RemoveStudyFormResponseResponse value) {
        return new JAXBElement<RemoveStudyFormResponseResponse>(_RemoveStudyFormResponseResponse_QNAME, RemoveStudyFormResponseResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemovePatientFormResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "removePatientFormResponse")
    public JAXBElement<RemovePatientFormResponse> createRemovePatientFormResponse(RemovePatientFormResponse value) {
        return new JAXBElement<RemovePatientFormResponse>(_RemovePatientFormResponse_QNAME, RemovePatientFormResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPatientFormResponseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "getPatientFormResponseResponse")
    public JAXBElement<GetPatientFormResponseResponse> createGetPatientFormResponseResponse(GetPatientFormResponseResponse value) {
        return new JAXBElement<GetPatientFormResponseResponse>(_GetPatientFormResponseResponse_QNAME, GetPatientFormResponseResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetListOfAccountFormResponses }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "getListOfAccountFormResponses")
    public JAXBElement<GetListOfAccountFormResponses> createGetListOfAccountFormResponses(GetListOfAccountFormResponses value) {
        return new JAXBElement<GetListOfAccountFormResponses>(_GetListOfAccountFormResponses_QNAME, GetListOfAccountFormResponses.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateStudyPatientScheduleFormResponseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "createStudyPatientScheduleFormResponseResponse")
    public JAXBElement<CreateStudyPatientScheduleFormResponseResponse> createCreateStudyPatientScheduleFormResponseResponse(CreateStudyPatientScheduleFormResponseResponse value) {
        return new JAXBElement<CreateStudyPatientScheduleFormResponseResponse>(_CreateStudyPatientScheduleFormResponseResponse_QNAME, CreateStudyPatientScheduleFormResponseResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStudyPatientFormResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "getStudyPatientFormResponse")
    public JAXBElement<GetStudyPatientFormResponse> createGetStudyPatientFormResponse(GetStudyPatientFormResponse value) {
        return new JAXBElement<GetStudyPatientFormResponse>(_GetStudyPatientFormResponse_QNAME, GetStudyPatientFormResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStudyFormResponseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "getStudyFormResponseResponse")
    public JAXBElement<GetStudyFormResponseResponse> createGetStudyFormResponseResponse(GetStudyFormResponseResponse value) {
        return new JAXBElement<GetStudyFormResponseResponse>(_GetStudyFormResponseResponse_QNAME, GetStudyFormResponseResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateStudyFormResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "updateStudyFormResponse")
    public JAXBElement<UpdateStudyFormResponse> createUpdateStudyFormResponse(UpdateStudyFormResponse value) {
        return new JAXBElement<UpdateStudyFormResponse>(_UpdateStudyFormResponse_QNAME, UpdateStudyFormResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateStudyPatientScheduleFormResponseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "updateStudyPatientScheduleFormResponseResponse")
    public JAXBElement<UpdateStudyPatientScheduleFormResponseResponse> createUpdateStudyPatientScheduleFormResponseResponse(UpdateStudyPatientScheduleFormResponseResponse value) {
        return new JAXBElement<UpdateStudyPatientScheduleFormResponseResponse>(_UpdateStudyPatientScheduleFormResponseResponse_QNAME, UpdateStudyPatientScheduleFormResponseResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetListOfStudyPatientFormResponses }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "getListOfStudyPatientFormResponses")
    public JAXBElement<GetListOfStudyPatientFormResponses> createGetListOfStudyPatientFormResponses(GetListOfStudyPatientFormResponses value) {
        return new JAXBElement<GetListOfStudyPatientFormResponses>(_GetListOfStudyPatientFormResponses_QNAME, GetListOfStudyPatientFormResponses.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveStudyPatientFormResponseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "removeStudyPatientFormResponseResponse")
    public JAXBElement<RemoveStudyPatientFormResponseResponse> createRemoveStudyPatientFormResponseResponse(RemoveStudyPatientFormResponseResponse value) {
        return new JAXBElement<RemoveStudyPatientFormResponseResponse>(_RemoveStudyPatientFormResponseResponse_QNAME, RemoveStudyPatientFormResponseResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPatientFormResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "getPatientFormResponse")
    public JAXBElement<GetPatientFormResponse> createGetPatientFormResponse(GetPatientFormResponse value) {
        return new JAXBElement<GetPatientFormResponse>(_GetPatientFormResponse_QNAME, GetPatientFormResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OperationException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "OperationException")
    public JAXBElement<OperationException> createOperationException(OperationException value) {
        return new JAXBElement<OperationException>(_OperationException_QNAME, OperationException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateStudyFormResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "createStudyFormResponse")
    public JAXBElement<CreateStudyFormResponse> createCreateStudyFormResponse(CreateStudyFormResponse value) {
        return new JAXBElement<CreateStudyFormResponse>(_CreateStudyFormResponse_QNAME, CreateStudyFormResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetListOfPatientFormResponses }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "getListOfPatientFormResponses")
    public JAXBElement<GetListOfPatientFormResponses> createGetListOfPatientFormResponses(GetListOfPatientFormResponses value) {
        return new JAXBElement<GetListOfPatientFormResponses>(_GetListOfPatientFormResponses_QNAME, GetListOfPatientFormResponses.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateAccountFormResponseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "createAccountFormResponseResponse")
    public JAXBElement<CreateAccountFormResponseResponse> createCreateAccountFormResponseResponse(CreateAccountFormResponseResponse value) {
        return new JAXBElement<CreateAccountFormResponseResponse>(_CreateAccountFormResponseResponse_QNAME, CreateAccountFormResponseResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdatePatientFormResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "updatePatientFormResponse")
    public JAXBElement<UpdatePatientFormResponse> createUpdatePatientFormResponse(UpdatePatientFormResponse value) {
        return new JAXBElement<UpdatePatientFormResponse>(_UpdatePatientFormResponse_QNAME, UpdatePatientFormResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveStudyFormResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "removeStudyFormResponse")
    public JAXBElement<RemoveStudyFormResponse> createRemoveStudyFormResponse(RemoveStudyFormResponse value) {
        return new JAXBElement<RemoveStudyFormResponse>(_RemoveStudyFormResponse_QNAME, RemoveStudyFormResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStudyPatientFormResponseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "getStudyPatientFormResponseResponse")
    public JAXBElement<GetStudyPatientFormResponseResponse> createGetStudyPatientFormResponseResponse(GetStudyPatientFormResponseResponse value) {
        return new JAXBElement<GetStudyPatientFormResponseResponse>(_GetStudyPatientFormResponseResponse_QNAME, GetStudyPatientFormResponseResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateAccountFormResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "createAccountFormResponse")
    public JAXBElement<CreateAccountFormResponse> createCreateAccountFormResponse(CreateAccountFormResponse value) {
        return new JAXBElement<CreateAccountFormResponse>(_CreateAccountFormResponse_QNAME, CreateAccountFormResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveStudyPatientFormResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "removeStudyPatientFormResponse")
    public JAXBElement<RemoveStudyPatientFormResponse> createRemoveStudyPatientFormResponse(RemoveStudyPatientFormResponse value) {
        return new JAXBElement<RemoveStudyPatientFormResponse>(_RemoveStudyPatientFormResponse_QNAME, RemoveStudyPatientFormResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateStudyPatientFormResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "updateStudyPatientFormResponse")
    public JAXBElement<UpdateStudyPatientFormResponse> createUpdateStudyPatientFormResponse(UpdateStudyPatientFormResponse value) {
        return new JAXBElement<UpdateStudyPatientFormResponse>(_UpdateStudyPatientFormResponse_QNAME, UpdateStudyPatientFormResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveStudyPatientScheduleFormResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "removeStudyPatientScheduleFormResponse")
    public JAXBElement<RemoveStudyPatientScheduleFormResponse> createRemoveStudyPatientScheduleFormResponse(RemoveStudyPatientScheduleFormResponse value) {
        return new JAXBElement<RemoveStudyPatientScheduleFormResponse>(_RemoveStudyPatientScheduleFormResponse_QNAME, RemoveStudyPatientScheduleFormResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateStudyFormResponseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "createStudyFormResponseResponse")
    public JAXBElement<CreateStudyFormResponseResponse> createCreateStudyFormResponseResponse(CreateStudyFormResponseResponse value) {
        return new JAXBElement<CreateStudyFormResponseResponse>(_CreateStudyFormResponseResponse_QNAME, CreateStudyFormResponseResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemovePatientFormResponseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "removePatientFormResponseResponse")
    public JAXBElement<RemovePatientFormResponseResponse> createRemovePatientFormResponseResponse(RemovePatientFormResponseResponse value) {
        return new JAXBElement<RemovePatientFormResponseResponse>(_RemovePatientFormResponseResponse_QNAME, RemovePatientFormResponseResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStudyFormResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "getStudyFormResponse")
    public JAXBElement<GetStudyFormResponse> createGetStudyFormResponse(GetStudyFormResponse value) {
        return new JAXBElement<GetStudyFormResponse>(_GetStudyFormResponse_QNAME, GetStudyFormResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveAccountFormResponseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "removeAccountFormResponseResponse")
    public JAXBElement<RemoveAccountFormResponseResponse> createRemoveAccountFormResponseResponse(RemoveAccountFormResponseResponse value) {
        return new JAXBElement<RemoveAccountFormResponseResponse>(_RemoveAccountFormResponseResponse_QNAME, RemoveAccountFormResponseResponse.class, null, value);
    }

}
