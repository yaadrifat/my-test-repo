
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for formDesign complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="formDesign">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="ESignRequired" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="formDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="formIdentifier" type="{http://velos.com/services/}formIdentifier" minOccurs="0"/>
 *         &lt;element name="formLinkedTo" type="{http://velos.com/services/}linkedTo" minOccurs="0"/>
 *         &lt;element name="formName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="formSharedWith" type="{http://velos.com/services/}sharedWith" minOccurs="0"/>
 *         &lt;element name="formStatus" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="sections" type="{http://velos.com/services/}formSections" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "formDesign", propOrder = {
    "eSignRequired",
    "formDesc",
    "formIdentifier",
    "formLinkedTo",
    "formName",
    "formSharedWith",
    "formStatus",
    "sections"
})
@XmlSeeAlso({
    LinkedFormDesign.class
})
public class FormDesign
    extends ServiceObject
{

    @XmlElement(name = "ESignRequired")
    protected boolean eSignRequired;
    protected String formDesc;
    protected FormIdentifier formIdentifier;
    protected LinkedTo formLinkedTo;
    protected String formName;
    protected SharedWith formSharedWith;
    protected Code formStatus;
    protected FormSections sections;

    /**
     * Gets the value of the eSignRequired property.
     * 
     */
    public boolean isESignRequired() {
        return eSignRequired;
    }

    /**
     * Sets the value of the eSignRequired property.
     * 
     */
    public void setESignRequired(boolean value) {
        this.eSignRequired = value;
    }

    /**
     * Gets the value of the formDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormDesc() {
        return formDesc;
    }

    /**
     * Sets the value of the formDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormDesc(String value) {
        this.formDesc = value;
    }

    /**
     * Gets the value of the formIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link FormIdentifier }
     *     
     */
    public FormIdentifier getFormIdentifier() {
        return formIdentifier;
    }

    /**
     * Sets the value of the formIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link FormIdentifier }
     *     
     */
    public void setFormIdentifier(FormIdentifier value) {
        this.formIdentifier = value;
    }

    /**
     * Gets the value of the formLinkedTo property.
     * 
     * @return
     *     possible object is
     *     {@link LinkedTo }
     *     
     */
    public LinkedTo getFormLinkedTo() {
        return formLinkedTo;
    }

    /**
     * Sets the value of the formLinkedTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link LinkedTo }
     *     
     */
    public void setFormLinkedTo(LinkedTo value) {
        this.formLinkedTo = value;
    }

    /**
     * Gets the value of the formName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormName() {
        return formName;
    }

    /**
     * Sets the value of the formName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormName(String value) {
        this.formName = value;
    }

    /**
     * Gets the value of the formSharedWith property.
     * 
     * @return
     *     possible object is
     *     {@link SharedWith }
     *     
     */
    public SharedWith getFormSharedWith() {
        return formSharedWith;
    }

    /**
     * Sets the value of the formSharedWith property.
     * 
     * @param value
     *     allowed object is
     *     {@link SharedWith }
     *     
     */
    public void setFormSharedWith(SharedWith value) {
        this.formSharedWith = value;
    }

    /**
     * Gets the value of the formStatus property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getFormStatus() {
        return formStatus;
    }

    /**
     * Sets the value of the formStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setFormStatus(Code value) {
        this.formStatus = value;
    }

    /**
     * Gets the value of the sections property.
     * 
     * @return
     *     possible object is
     *     {@link FormSections }
     *     
     */
    public FormSections getSections() {
        return sections;
    }

    /**
     * Sets the value of the sections property.
     * 
     * @param value
     *     allowed object is
     *     {@link FormSections }
     *     
     */
    public void setSections(FormSections value) {
        this.sections = value;
    }

}
