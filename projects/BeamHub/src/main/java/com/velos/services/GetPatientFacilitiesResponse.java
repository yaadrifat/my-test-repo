
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getPatientFacilitiesResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getPatientFacilitiesResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PatientFacilities" type="{http://velos.com/services/}patientOrganizations" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getPatientFacilitiesResponse", propOrder = {
    "patientFacilities"
})
public class GetPatientFacilitiesResponse {

    @XmlElement(name = "PatientFacilities")
    protected PatientOrganizations patientFacilities;

    /**
     * Gets the value of the patientFacilities property.
     * 
     * @return
     *     possible object is
     *     {@link PatientOrganizations }
     *     
     */
    public PatientOrganizations getPatientFacilities() {
        return patientFacilities;
    }

    /**
     * Sets the value of the patientFacilities property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientOrganizations }
     *     
     */
    public void setPatientFacilities(PatientOrganizations value) {
        this.patientFacilities = value;
    }

}
