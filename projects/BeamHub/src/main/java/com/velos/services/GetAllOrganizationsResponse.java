
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getAllOrganizationsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getAllOrganizationsResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UserOrganizations" type="{http://velos.com/services/}organizations" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getAllOrganizationsResponse", propOrder = {
    "userOrganizations"
})
public class GetAllOrganizationsResponse {

    @XmlElement(name = "UserOrganizations")
    protected Organizations userOrganizations;

    /**
     * Gets the value of the userOrganizations property.
     * 
     * @return
     *     possible object is
     *     {@link Organizations }
     *     
     */
    public Organizations getUserOrganizations() {
        return userOrganizations;
    }

    /**
     * Sets the value of the userOrganizations property.
     * 
     * @param value
     *     allowed object is
     *     {@link Organizations }
     *     
     */
    public void setUserOrganizations(Organizations value) {
        this.userOrganizations = value;
    }

}
