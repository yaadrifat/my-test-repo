
package com.velos.services;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for studySummary complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="studySummary">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="agentDevice" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="blinding" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="diseaseSites" type="{http://velos.com/services/}code" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="division" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="estimatedBeginDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="isCtrpReportable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="isFdaRegulated" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="isInvestigatorHeldIndIde" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="isStdDefPublic" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="isStdDesignPublic" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="isStdDetailPublic" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="isStdSponsorInfoPublic" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="keywords" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="moreStudyDetails" type="{http://velos.com/services/}nvPair" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="nationalSampleSize" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="nciTrialIdentifier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nctNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="objective" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PIMajorAuthor" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="parentIdentifier" type="{http://velos.com/services/}studyIdentifier" minOccurs="0"/>
 *         &lt;element name="phase" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="principalInvestigator" type="{http://velos.com/services/}userIdentifier" minOccurs="0"/>
 *         &lt;element name="principalInvestigatorOther" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="randomization" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="researchType" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="resubmissionChecklistLink" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="specificSite1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="specificSite2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="specificSite3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sponsorContact" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sponsorID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sponsorName" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="sponsorNameOther" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sponsorOtherInfo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="studyAuthor" type="{http://velos.com/services/}userIdentifier" minOccurs="0"/>
 *         &lt;element name="studyCatLink" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="studyContact" type="{http://velos.com/services/}userIdentifier" minOccurs="0"/>
 *         &lt;element name="studyDuration" type="{http://velos.com/services/}duration" minOccurs="0"/>
 *         &lt;element name="studyIndIdeNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="studyIndIdes" type="{http://velos.com/services/}studyIndIdes" minOccurs="0"/>
 *         &lt;element name="studyLinkedTo" type="{http://velos.com/services/}studyIdentifier" minOccurs="0"/>
 *         &lt;element name="studyNIHGrants" type="{http://velos.com/services/}studyNIHGrants" minOccurs="0"/>
 *         &lt;element name="studyNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="studyPurpose" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="studyScope" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="studyTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="studyType" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="summaryText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="therapeuticArea" type="{http://velos.com/services/}code" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "studySummary", propOrder = {
    "agentDevice",
    "blinding",
    "diseaseSites",
    "division",
    "estimatedBeginDate",
    "isCtrpReportable",
    "isFdaRegulated",
    "isInvestigatorHeldIndIde",
    "isStdDefPublic",
    "isStdDesignPublic",
    "isStdDetailPublic",
    "isStdSponsorInfoPublic",
    "keywords",
    "moreStudyDetails",
    "nationalSampleSize",
    "nciTrialIdentifier",
    "nctNumber",
    "objective",
    "piMajorAuthor",
    "parentIdentifier",
    "phase",
    "principalInvestigator",
    "principalInvestigatorOther",
    "randomization",
    "researchType",
    "resubmissionChecklistLink",
    "specificSite1",
    "specificSite2",
    "specificSite3",
    "sponsorContact",
    "sponsorID",
    "sponsorName",
    "sponsorNameOther",
    "sponsorOtherInfo",
    "studyAuthor",
    "studyCatLink",
    "studyContact",
    "studyDuration",
    "studyIndIdeNum",
    "studyIndIdes",
    "studyLinkedTo",
    "studyNIHGrants",
    "studyNumber",
    "studyPurpose",
    "studyScope",
    "studyTitle",
    "studyType",
    "summaryText",
    "therapeuticArea"
})
public class StudySummary
    extends ServiceObject
{

    protected String agentDevice;
    protected Code blinding;
    @XmlElement(nillable = true)
    protected List<Code> diseaseSites;
    protected Code division;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar estimatedBeginDate;
    protected Boolean isCtrpReportable;
    protected Boolean isFdaRegulated;
    protected Boolean isInvestigatorHeldIndIde;
    protected Boolean isStdDefPublic;
    protected Boolean isStdDesignPublic;
    protected Boolean isStdDetailPublic;
    protected Boolean isStdSponsorInfoPublic;
    protected String keywords;
    @XmlElement(nillable = true)
    protected List<NvPair> moreStudyDetails;
    protected Integer nationalSampleSize;
    protected String nciTrialIdentifier;
    protected String nctNumber;
    protected String objective;
    @XmlElement(name = "PIMajorAuthor")
    protected boolean piMajorAuthor;
    protected StudyIdentifier parentIdentifier;
    protected Code phase;
    protected UserIdentifier principalInvestigator;
    protected String principalInvestigatorOther;
    protected Code randomization;
    protected Code researchType;
    protected String resubmissionChecklistLink;
    protected String specificSite1;
    protected String specificSite2;
    protected String specificSite3;
    protected String sponsorContact;
    protected String sponsorID;
    protected Code sponsorName;
    protected String sponsorNameOther;
    protected String sponsorOtherInfo;
    protected UserIdentifier studyAuthor;
    protected String studyCatLink;
    protected UserIdentifier studyContact;
    protected Duration studyDuration;
    protected String studyIndIdeNum;
    protected StudyIndIdes studyIndIdes;
    protected StudyIdentifier studyLinkedTo;
    protected StudyNIHGrants studyNIHGrants;
    protected String studyNumber;
    protected Code studyPurpose;
    protected Code studyScope;
    protected String studyTitle;
    protected Code studyType;
    protected String summaryText;
    protected Code therapeuticArea;

    /**
     * Gets the value of the agentDevice property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentDevice() {
        return agentDevice;
    }

    /**
     * Sets the value of the agentDevice property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentDevice(String value) {
        this.agentDevice = value;
    }

    /**
     * Gets the value of the blinding property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getBlinding() {
        return blinding;
    }

    /**
     * Sets the value of the blinding property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setBlinding(Code value) {
        this.blinding = value;
    }

    /**
     * Gets the value of the diseaseSites property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the diseaseSites property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDiseaseSites().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Code }
     * 
     * 
     */
    public List<Code> getDiseaseSites() {
        if (diseaseSites == null) {
            diseaseSites = new ArrayList<Code>();
        }
        return this.diseaseSites;
    }

    /**
     * Gets the value of the division property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getDivision() {
        return division;
    }

    /**
     * Sets the value of the division property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setDivision(Code value) {
        this.division = value;
    }

    /**
     * Gets the value of the estimatedBeginDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEstimatedBeginDate() {
        return estimatedBeginDate;
    }

    /**
     * Sets the value of the estimatedBeginDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEstimatedBeginDate(XMLGregorianCalendar value) {
        this.estimatedBeginDate = value;
    }

    /**
     * Gets the value of the isCtrpReportable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsCtrpReportable() {
        return isCtrpReportable;
    }

    /**
     * Sets the value of the isCtrpReportable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsCtrpReportable(Boolean value) {
        this.isCtrpReportable = value;
    }

    /**
     * Gets the value of the isFdaRegulated property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsFdaRegulated() {
        return isFdaRegulated;
    }

    /**
     * Sets the value of the isFdaRegulated property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsFdaRegulated(Boolean value) {
        this.isFdaRegulated = value;
    }

    /**
     * Gets the value of the isInvestigatorHeldIndIde property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsInvestigatorHeldIndIde() {
        return isInvestigatorHeldIndIde;
    }

    /**
     * Sets the value of the isInvestigatorHeldIndIde property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsInvestigatorHeldIndIde(Boolean value) {
        this.isInvestigatorHeldIndIde = value;
    }

    /**
     * Gets the value of the isStdDefPublic property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsStdDefPublic() {
        return isStdDefPublic;
    }

    /**
     * Sets the value of the isStdDefPublic property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsStdDefPublic(Boolean value) {
        this.isStdDefPublic = value;
    }

    /**
     * Gets the value of the isStdDesignPublic property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsStdDesignPublic() {
        return isStdDesignPublic;
    }

    /**
     * Sets the value of the isStdDesignPublic property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsStdDesignPublic(Boolean value) {
        this.isStdDesignPublic = value;
    }

    /**
     * Gets the value of the isStdDetailPublic property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsStdDetailPublic() {
        return isStdDetailPublic;
    }

    /**
     * Sets the value of the isStdDetailPublic property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsStdDetailPublic(Boolean value) {
        this.isStdDetailPublic = value;
    }

    /**
     * Gets the value of the isStdSponsorInfoPublic property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsStdSponsorInfoPublic() {
        return isStdSponsorInfoPublic;
    }

    /**
     * Sets the value of the isStdSponsorInfoPublic property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsStdSponsorInfoPublic(Boolean value) {
        this.isStdSponsorInfoPublic = value;
    }

    /**
     * Gets the value of the keywords property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKeywords() {
        return keywords;
    }

    /**
     * Sets the value of the keywords property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKeywords(String value) {
        this.keywords = value;
    }

    /**
     * Gets the value of the moreStudyDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the moreStudyDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMoreStudyDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NvPair }
     * 
     * 
     */
    public List<NvPair> getMoreStudyDetails() {
        if (moreStudyDetails == null) {
            moreStudyDetails = new ArrayList<NvPair>();
        }
        return this.moreStudyDetails;
    }

    /**
     * Gets the value of the nationalSampleSize property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNationalSampleSize() {
        return nationalSampleSize;
    }

    /**
     * Sets the value of the nationalSampleSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNationalSampleSize(Integer value) {
        this.nationalSampleSize = value;
    }

    /**
     * Gets the value of the nciTrialIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNciTrialIdentifier() {
        return nciTrialIdentifier;
    }

    /**
     * Sets the value of the nciTrialIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNciTrialIdentifier(String value) {
        this.nciTrialIdentifier = value;
    }

    /**
     * Gets the value of the nctNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNctNumber() {
        return nctNumber;
    }

    /**
     * Sets the value of the nctNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNctNumber(String value) {
        this.nctNumber = value;
    }

    /**
     * Gets the value of the objective property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObjective() {
        return objective;
    }

    /**
     * Sets the value of the objective property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObjective(String value) {
        this.objective = value;
    }

    /**
     * Gets the value of the piMajorAuthor property.
     * 
     */
    public boolean isPIMajorAuthor() {
        return piMajorAuthor;
    }

    /**
     * Sets the value of the piMajorAuthor property.
     * 
     */
    public void setPIMajorAuthor(boolean value) {
        this.piMajorAuthor = value;
    }

    /**
     * Gets the value of the parentIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link StudyIdentifier }
     *     
     */
    public StudyIdentifier getParentIdentifier() {
        return parentIdentifier;
    }

    /**
     * Sets the value of the parentIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyIdentifier }
     *     
     */
    public void setParentIdentifier(StudyIdentifier value) {
        this.parentIdentifier = value;
    }

    /**
     * Gets the value of the phase property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getPhase() {
        return phase;
    }

    /**
     * Sets the value of the phase property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setPhase(Code value) {
        this.phase = value;
    }

    /**
     * Gets the value of the principalInvestigator property.
     * 
     * @return
     *     possible object is
     *     {@link UserIdentifier }
     *     
     */
    public UserIdentifier getPrincipalInvestigator() {
        return principalInvestigator;
    }

    /**
     * Sets the value of the principalInvestigator property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserIdentifier }
     *     
     */
    public void setPrincipalInvestigator(UserIdentifier value) {
        this.principalInvestigator = value;
    }

    /**
     * Gets the value of the principalInvestigatorOther property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrincipalInvestigatorOther() {
        return principalInvestigatorOther;
    }

    /**
     * Sets the value of the principalInvestigatorOther property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrincipalInvestigatorOther(String value) {
        this.principalInvestigatorOther = value;
    }

    /**
     * Gets the value of the randomization property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getRandomization() {
        return randomization;
    }

    /**
     * Sets the value of the randomization property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setRandomization(Code value) {
        this.randomization = value;
    }

    /**
     * Gets the value of the researchType property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getResearchType() {
        return researchType;
    }

    /**
     * Sets the value of the researchType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setResearchType(Code value) {
        this.researchType = value;
    }

    /**
     * Gets the value of the resubmissionChecklistLink property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResubmissionChecklistLink() {
        return resubmissionChecklistLink;
    }

    /**
     * Sets the value of the resubmissionChecklistLink property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResubmissionChecklistLink(String value) {
        this.resubmissionChecklistLink = value;
    }

    /**
     * Gets the value of the specificSite1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpecificSite1() {
        return specificSite1;
    }

    /**
     * Sets the value of the specificSite1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpecificSite1(String value) {
        this.specificSite1 = value;
    }

    /**
     * Gets the value of the specificSite2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpecificSite2() {
        return specificSite2;
    }

    /**
     * Sets the value of the specificSite2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpecificSite2(String value) {
        this.specificSite2 = value;
    }

    /**
     * Gets the value of the specificSite3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpecificSite3() {
        return specificSite3;
    }

    /**
     * Sets the value of the specificSite3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpecificSite3(String value) {
        this.specificSite3 = value;
    }

    /**
     * Gets the value of the sponsorContact property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSponsorContact() {
        return sponsorContact;
    }

    /**
     * Sets the value of the sponsorContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSponsorContact(String value) {
        this.sponsorContact = value;
    }

    /**
     * Gets the value of the sponsorID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSponsorID() {
        return sponsorID;
    }

    /**
     * Sets the value of the sponsorID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSponsorID(String value) {
        this.sponsorID = value;
    }

    /**
     * Gets the value of the sponsorName property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getSponsorName() {
        return sponsorName;
    }

    /**
     * Sets the value of the sponsorName property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setSponsorName(Code value) {
        this.sponsorName = value;
    }

    /**
     * Gets the value of the sponsorNameOther property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSponsorNameOther() {
        return sponsorNameOther;
    }

    /**
     * Sets the value of the sponsorNameOther property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSponsorNameOther(String value) {
        this.sponsorNameOther = value;
    }

    /**
     * Gets the value of the sponsorOtherInfo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSponsorOtherInfo() {
        return sponsorOtherInfo;
    }

    /**
     * Sets the value of the sponsorOtherInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSponsorOtherInfo(String value) {
        this.sponsorOtherInfo = value;
    }

    /**
     * Gets the value of the studyAuthor property.
     * 
     * @return
     *     possible object is
     *     {@link UserIdentifier }
     *     
     */
    public UserIdentifier getStudyAuthor() {
        return studyAuthor;
    }

    /**
     * Sets the value of the studyAuthor property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserIdentifier }
     *     
     */
    public void setStudyAuthor(UserIdentifier value) {
        this.studyAuthor = value;
    }

    /**
     * Gets the value of the studyCatLink property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStudyCatLink() {
        return studyCatLink;
    }

    /**
     * Sets the value of the studyCatLink property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStudyCatLink(String value) {
        this.studyCatLink = value;
    }

    /**
     * Gets the value of the studyContact property.
     * 
     * @return
     *     possible object is
     *     {@link UserIdentifier }
     *     
     */
    public UserIdentifier getStudyContact() {
        return studyContact;
    }

    /**
     * Sets the value of the studyContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserIdentifier }
     *     
     */
    public void setStudyContact(UserIdentifier value) {
        this.studyContact = value;
    }

    /**
     * Gets the value of the studyDuration property.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getStudyDuration() {
        return studyDuration;
    }

    /**
     * Sets the value of the studyDuration property.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setStudyDuration(Duration value) {
        this.studyDuration = value;
    }

    /**
     * Gets the value of the studyIndIdeNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStudyIndIdeNum() {
        return studyIndIdeNum;
    }

    /**
     * Sets the value of the studyIndIdeNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStudyIndIdeNum(String value) {
        this.studyIndIdeNum = value;
    }

    /**
     * Gets the value of the studyIndIdes property.
     * 
     * @return
     *     possible object is
     *     {@link StudyIndIdes }
     *     
     */
    public StudyIndIdes getStudyIndIdes() {
        return studyIndIdes;
    }

    /**
     * Sets the value of the studyIndIdes property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyIndIdes }
     *     
     */
    public void setStudyIndIdes(StudyIndIdes value) {
        this.studyIndIdes = value;
    }

    /**
     * Gets the value of the studyLinkedTo property.
     * 
     * @return
     *     possible object is
     *     {@link StudyIdentifier }
     *     
     */
    public StudyIdentifier getStudyLinkedTo() {
        return studyLinkedTo;
    }

    /**
     * Sets the value of the studyLinkedTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyIdentifier }
     *     
     */
    public void setStudyLinkedTo(StudyIdentifier value) {
        this.studyLinkedTo = value;
    }

    /**
     * Gets the value of the studyNIHGrants property.
     * 
     * @return
     *     possible object is
     *     {@link StudyNIHGrants }
     *     
     */
    public StudyNIHGrants getStudyNIHGrants() {
        return studyNIHGrants;
    }

    /**
     * Sets the value of the studyNIHGrants property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyNIHGrants }
     *     
     */
    public void setStudyNIHGrants(StudyNIHGrants value) {
        this.studyNIHGrants = value;
    }

    /**
     * Gets the value of the studyNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStudyNumber() {
        return studyNumber;
    }

    /**
     * Sets the value of the studyNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStudyNumber(String value) {
        this.studyNumber = value;
    }

    /**
     * Gets the value of the studyPurpose property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getStudyPurpose() {
        return studyPurpose;
    }

    /**
     * Sets the value of the studyPurpose property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setStudyPurpose(Code value) {
        this.studyPurpose = value;
    }

    /**
     * Gets the value of the studyScope property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getStudyScope() {
        return studyScope;
    }

    /**
     * Sets the value of the studyScope property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setStudyScope(Code value) {
        this.studyScope = value;
    }

    /**
     * Gets the value of the studyTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStudyTitle() {
        return studyTitle;
    }

    /**
     * Sets the value of the studyTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStudyTitle(String value) {
        this.studyTitle = value;
    }

    /**
     * Gets the value of the studyType property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getStudyType() {
        return studyType;
    }

    /**
     * Sets the value of the studyType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setStudyType(Code value) {
        this.studyType = value;
    }

    /**
     * Gets the value of the summaryText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSummaryText() {
        return summaryText;
    }

    /**
     * Sets the value of the summaryText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSummaryText(String value) {
        this.summaryText = value;
    }

    /**
     * Gets the value of the therapeuticArea property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getTherapeuticArea() {
        return therapeuticArea;
    }

    /**
     * Sets the value of the therapeuticArea property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setTherapeuticArea(Code value) {
        this.therapeuticArea = value;
    }

}
