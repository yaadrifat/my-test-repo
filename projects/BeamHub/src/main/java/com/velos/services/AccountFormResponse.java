
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for accountFormResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="accountFormResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}formResponse">
 *       &lt;sequence>
 *         &lt;element name="accountIdentifier" type="{http://velos.com/services/}accountIdentifier" minOccurs="0"/>
 *         &lt;element name="formFilledFormId" type="{http://velos.com/services/}accountFormResponseIdentifier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "accountFormResponse", propOrder = {
    "accountIdentifier",
    "formFilledFormId"
})
public class AccountFormResponse
    extends FormResponse
{

    protected AccountIdentifier accountIdentifier;
    protected AccountFormResponseIdentifier formFilledFormId;

    /**
     * Gets the value of the accountIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link AccountIdentifier }
     *     
     */
    public AccountIdentifier getAccountIdentifier() {
        return accountIdentifier;
    }

    /**
     * Sets the value of the accountIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountIdentifier }
     *     
     */
    public void setAccountIdentifier(AccountIdentifier value) {
        this.accountIdentifier = value;
    }

    /**
     * Gets the value of the formFilledFormId property.
     * 
     * @return
     *     possible object is
     *     {@link AccountFormResponseIdentifier }
     *     
     */
    public AccountFormResponseIdentifier getFormFilledFormId() {
        return formFilledFormId;
    }

    /**
     * Sets the value of the formFilledFormId property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountFormResponseIdentifier }
     *     
     */
    public void setFormFilledFormId(AccountFormResponseIdentifier value) {
        this.formFilledFormId = value;
    }

}
