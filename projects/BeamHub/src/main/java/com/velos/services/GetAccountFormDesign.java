
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getAccountFormDesign complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getAccountFormDesign">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FormIdentifier" type="{http://velos.com/services/}formIdentifier" minOccurs="0"/>
 *         &lt;element name="AccountIdentifier" type="{http://velos.com/services/}accountIdentifier" minOccurs="0"/>
 *         &lt;element name="IncludeFormatting" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="FormName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getAccountFormDesign", propOrder = {
    "formIdentifier",
    "accountIdentifier",
    "includeFormatting",
    "formName"
})
public class GetAccountFormDesign {

    @XmlElement(name = "FormIdentifier")
    protected FormIdentifier formIdentifier;
    @XmlElement(name = "AccountIdentifier")
    protected AccountIdentifier accountIdentifier;
    @XmlElement(name = "IncludeFormatting")
    protected boolean includeFormatting;
    @XmlElement(name = "FormName")
    protected String formName;

    /**
     * Gets the value of the formIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link FormIdentifier }
     *     
     */
    public FormIdentifier getFormIdentifier() {
        return formIdentifier;
    }

    /**
     * Sets the value of the formIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link FormIdentifier }
     *     
     */
    public void setFormIdentifier(FormIdentifier value) {
        this.formIdentifier = value;
    }

    /**
     * Gets the value of the accountIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link AccountIdentifier }
     *     
     */
    public AccountIdentifier getAccountIdentifier() {
        return accountIdentifier;
    }

    /**
     * Sets the value of the accountIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountIdentifier }
     *     
     */
    public void setAccountIdentifier(AccountIdentifier value) {
        this.accountIdentifier = value;
    }

    /**
     * Gets the value of the includeFormatting property.
     * 
     */
    public boolean isIncludeFormatting() {
        return includeFormatting;
    }

    /**
     * Sets the value of the includeFormatting property.
     * 
     */
    public void setIncludeFormatting(boolean value) {
        this.includeFormatting = value;
    }

    /**
     * Gets the value of the formName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormName() {
        return formName;
    }

    /**
     * Sets the value of the formName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormName(String value) {
        this.formName = value;
    }

}
