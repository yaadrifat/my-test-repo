
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for studyINDIDEInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="studyINDIDEInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="accessType" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="exemptINDIDE" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="expandAccess" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="grantor" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="holder" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="numberINDIDE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="programCode" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="typeINDIDE" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "studyINDIDEInfo", propOrder = {
    "accessType",
    "exemptINDIDE",
    "expandAccess",
    "grantor",
    "holder",
    "numberINDIDE",
    "programCode",
    "typeINDIDE"
})
public class StudyINDIDEInfo {

    protected Code accessType;
    protected Integer exemptINDIDE;
    protected Integer expandAccess;
    protected Code grantor;
    protected Code holder;
    protected String numberINDIDE;
    protected Code programCode;
    protected Integer typeINDIDE;

    /**
     * Gets the value of the accessType property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getAccessType() {
        return accessType;
    }

    /**
     * Sets the value of the accessType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setAccessType(Code value) {
        this.accessType = value;
    }

    /**
     * Gets the value of the exemptINDIDE property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getExemptINDIDE() {
        return exemptINDIDE;
    }

    /**
     * Sets the value of the exemptINDIDE property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setExemptINDIDE(Integer value) {
        this.exemptINDIDE = value;
    }

    /**
     * Gets the value of the expandAccess property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getExpandAccess() {
        return expandAccess;
    }

    /**
     * Sets the value of the expandAccess property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setExpandAccess(Integer value) {
        this.expandAccess = value;
    }

    /**
     * Gets the value of the grantor property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getGrantor() {
        return grantor;
    }

    /**
     * Sets the value of the grantor property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setGrantor(Code value) {
        this.grantor = value;
    }

    /**
     * Gets the value of the holder property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getHolder() {
        return holder;
    }

    /**
     * Sets the value of the holder property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setHolder(Code value) {
        this.holder = value;
    }

    /**
     * Gets the value of the numberINDIDE property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumberINDIDE() {
        return numberINDIDE;
    }

    /**
     * Sets the value of the numberINDIDE property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumberINDIDE(String value) {
        this.numberINDIDE = value;
    }

    /**
     * Gets the value of the programCode property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getProgramCode() {
        return programCode;
    }

    /**
     * Sets the value of the programCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setProgramCode(Code value) {
        this.programCode = value;
    }

    /**
     * Gets the value of the typeINDIDE property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTypeINDIDE() {
        return typeINDIDE;
    }

    /**
     * Sets the value of the typeINDIDE property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTypeINDIDE(Integer value) {
        this.typeINDIDE = value;
    }

}
