
package com.velos.services;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for issueTypes.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="issueTypes">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="MULTIPLE_OBJECTS_FOUND"/>
 *     &lt;enumeration value="CODE_NOT_FOUND"/>
 *     &lt;enumeration value="DATA_VALIDATION"/>
 *     &lt;enumeration value="DUPLICATE_USER"/>
 *     &lt;enumeration value="INVALID_SORT_BY_OPTION"/>
 *     &lt;enumeration value="INVALID_ORDER_BY_OPTION"/>
 *     &lt;enumeration value="INVALID_SITE_OF_SERVICE"/>
 *     &lt;enumeration value="INVALID_FACILITY_ID"/>
 *     &lt;enumeration value="INVALID_EVENT_CATEGORY"/>
 *     &lt;enumeration value="EVENT_CATEGORY_DOES_NOT_EXIST"/>
 *     &lt;enumeration value="SCHEDULE_NOT_FOUND"/>
 *     &lt;enumeration value="STUDY_AUTHOR_NOT_FOUND"/>
 *     &lt;enumeration value="STUDY_PRINCIPAL_INVESTIGATOR_NOT_FOUND"/>
 *     &lt;enumeration value="STUDY_COORDINATOR_NOT_FOUND"/>
 *     &lt;enumeration value="STUDY_NUMBER_EXISTS"/>
 *     &lt;enumeration value="STUDY_NUMBER_NOT_FOUND"/>
 *     &lt;enumeration value="STUDY_NUMBER_SIZE_ISSUE"/>
 *     &lt;enumeration value="STUDY_NOT_FOUND"/>
 *     &lt;enumeration value="STUDY_ORG_NOT_FOUND"/>
 *     &lt;enumeration value="STUDY_ERROR_REMOVE_ORGANIZATION"/>
 *     &lt;enumeration value="STUDY_ERROR_REMOVE_ORGANIZATION_CURRENT_STUDYSTATUS"/>
 *     &lt;enumeration value="STUDY_ERROR_REMOVE_ORGANIZATION_ENROLLED_PATIENT"/>
 *     &lt;enumeration value="STUDY_ERROR_CREATE_ORGANIZATION"/>
 *     &lt;enumeration value="STUDY_ERROR_UPDATE_ORGANIZATION"/>
 *     &lt;enumeration value="STUDY_ERROR_CREATE_STATUS"/>
 *     &lt;enumeration value="STUDY_ERROR_UPDATE_STATUS"/>
 *     &lt;enumeration value="STUDY_ERROR_CREATE_TEAM_MEMBER"/>
 *     &lt;enumeration value="STUDY_ERROR_UPDATE_TEAM_MEMBER"/>
 *     &lt;enumeration value="STUDY_ERROR_REMOVE_TEAM_MEMBER"/>
 *     &lt;enumeration value="STUDY_REMOVE_DATA_MANAGER"/>
 *     &lt;enumeration value="STUDY_ERROR_CREATE_MSD"/>
 *     &lt;enumeration value="STUDY_ERROR_UPDATE_MSD"/>
 *     &lt;enumeration value="NOT_IMPLEMENTED"/>
 *     &lt;enumeration value="UNKNOWN_THROWABLE"/>
 *     &lt;enumeration value="STUDY_ERROR_TEAM_MEMBER_ALREADY_EXISTS"/>
 *     &lt;enumeration value="ENROLLMENT_NOTIFICATION_USER_NOT_FOUND"/>
 *     &lt;enumeration value="GET_APPROVE_ENROLLMENT_NOTIFICATION_USER_NOT_FOUND"/>
 *     &lt;enumeration value="GET_ASSIGNED_TO_USERID_NOT_FOUND"/>
 *     &lt;enumeration value="GET_DOCUMENTED_BY_USERID_NOT_FOUND"/>
 *     &lt;enumeration value="STUDY_TEAM_AUTHORIZATION"/>
 *     &lt;enumeration value="GROUP_NOT_FOUND"/>
 *     &lt;enumeration value="GROUP_AUTHORIZATION"/>
 *     &lt;enumeration value="ORGANIZATION_AUTHORIZATION"/>
 *     &lt;enumeration value="ORGANIZATION_NOT_FOUND"/>
 *     &lt;enumeration value="STUDY_ERROR_CREATE_DUPLICATE_ORGANIZATION"/>
 *     &lt;enumeration value="STUDY_STATUS_ORG_NOT_IN_STUDY"/>
 *     &lt;enumeration value="USER_NOT_FOUND"/>
 *     &lt;enumeration value="SYSTEM_USER_NOT_FOUND"/>
 *     &lt;enumeration value="DOES_NOT_HAVE_RIGHTS_TO_KILL_SESSION"/>
 *     &lt;enumeration value="USER_SESSION_NOT_KILLED"/>
 *     &lt;enumeration value="USER_NOT_LOGGED_IN"/>
 *     &lt;enumeration value="USER_HIDDEN"/>
 *     &lt;enumeration value="HEART_BEAT_NOT_FOUND"/>
 *     &lt;enumeration value="DATABASE_CONNECTION_NOT_FOUND"/>
 *     &lt;enumeration value="EXECPTION_WHILE_READING_MANIFEST_FILE"/>
 *     &lt;enumeration value="EXECPTION_WHILE_GETTING_ATTRIBUTES_FROM_MANIFEST_FILE"/>
 *     &lt;enumeration value="EXECPTION_GETTING_REMOTE_MONITORING_SERVICES"/>
 *     &lt;enumeration value="PATIENT_NOT_FOUND"/>
 *     &lt;enumeration value="PATIENT_DATA_AUTHORIZATION"/>
 *     &lt;enumeration value="STUDIES_FOR_PATIENT_NOT_FOUND"/>
 *     &lt;enumeration value="PATIENT_SCHEDULE_NOT_FOUND"/>
 *     &lt;enumeration value="PATIENT_ALREADY_REGISTERED_TO_THIS_ORGANIZATION"/>
 *     &lt;enumeration value="EXECPTION_GETTING_REMOTE_PATIENT_DEMOGRAPHICS_SERVICES"/>
 *     &lt;enumeration value="STUDY_STATUS_NOT_FOUND"/>
 *     &lt;enumeration value="CALENDAR_NOT_FOUND"/>
 *     &lt;enumeration value="CALENDAR_NOT_ACTIVE"/>
 *     &lt;enumeration value="EVENT_NOT_FOUND"/>
 *     &lt;enumeration value="EVENT_STATUS_IDENTIFIER_NOT_FOUND"/>
 *     &lt;enumeration value="VISIT_NOT_FOUND"/>
 *     &lt;enumeration value="FORM_FIELD_NOT_FOUND"/>
 *     &lt;enumeration value="CURRENCY_NOT_FOUND"/>
 *     &lt;enumeration value="ERROR_CREATING_STUDY_CALENDAR"/>
 *     &lt;enumeration value="ERROR_CREATING_STUDY_CALENDAR_EVENT"/>
 *     &lt;enumeration value="ERROR_CREATING_STUDY_CALENDAR_EVENT_COST"/>
 *     &lt;enumeration value="STUDY_CALENDAR_ERROR_CREATE_EVENT"/>
 *     &lt;enumeration value="PATIENT_SCHEDULE_DOES_NOT_EXIST"/>
 *     &lt;enumeration value="CALENDAR_NAME_ALREADY_EXISTS"/>
 *     &lt;enumeration value="VISIT_NAME_ALREADY_EXISTS"/>
 *     &lt;enumeration value="VISIT_INTERVAL_EXCEEDS_CALENDAR_DURATION"/>
 *     &lt;enumeration value="VISIT_IDENTIFIER_INVALID"/>
 *     &lt;enumeration value="EVENT_IDENTIFIER_INAVLID"/>
 *     &lt;enumeration value="DUPLICATE_VISIT_NAME"/>
 *     &lt;enumeration value="DUPLICATE_EVENT_NAME"/>
 *     &lt;enumeration value="PATIENT_ID_ALREADY_EXISTS"/>
 *     &lt;enumeration value="PATIENT_ALREADY_EXISTS"/>
 *     &lt;enumeration value="PATIENT_IDENTIFIER_INVALID"/>
 *     &lt;enumeration value="PATIENT_ERROR_CREATE_DEMOGRAPHICS"/>
 *     &lt;enumeration value="PATIENT_ERROR_ADD_ORGANIZATION"/>
 *     &lt;enumeration value="STUDY_CALENDAR_ERROR_REMOVE_VISIT"/>
 *     &lt;enumeration value="STUDY_IDENTIFIER_INVALID"/>
 *     &lt;enumeration value="BUDGET_NOT_FOUND"/>
 *     &lt;enumeration value="ERROR_REMOVING_STUDY_CALENDAR_EVENT"/>
 *     &lt;enumeration value="ERROR_REMOVING_STUDY_CALENDAR"/>
 *     &lt;enumeration value="CALENDAR_ACTIVE_NOT_UPDATEABLE"/>
 *     &lt;enumeration value="PATIENT_ERROR_UPDATE_DEMOGRAPHICS"/>
 *     &lt;enumeration value="ERROR_CREATE_PATIENT_ENROLLMENT"/>
 *     &lt;enumeration value="ERROR_CREATE_MILESTONE"/>
 *     &lt;enumeration value="ERROR_CREATE_BUDGET"/>
 *     &lt;enumeration value="ERROR_CREATE_VERSION"/>
 *     &lt;enumeration value="PATIENT_ALREADY_ENROLLED"/>
 *     &lt;enumeration value="DUPLICATE_ORGANIZATION"/>
 *     &lt;enumeration value="ERROR_UPDATING_STUDY_CALENDAR"/>
 *     &lt;enumeration value="ERROR_UPDATING_STUDY_CALENDAR_EVENT"/>
 *     &lt;enumeration value="INVALID_EVENT_DURATION"/>
 *     &lt;enumeration value="INVALID_VISIT_DURATION"/>
 *     &lt;enumeration value="INVALID_FILTER_FIELD_ID"/>
 *     &lt;enumeration value="FORM_NOT_FOUND"/>
 *     &lt;enumeration value="FORM_RESPONSE_DOES_NOT_EXIST"/>
 *     &lt;enumeration value="EVENT_COST_NOT_FOUND"/>
 *     &lt;enumeration value="ERROR_REMOVING_EVENT_COST"/>
 *     &lt;enumeration value="ERROR_UPDATING_EVENT_COST"/>
 *     &lt;enumeration value="CALENDAR_STATUS_NOT_UPDATEABLE"/>
 *     &lt;enumeration value="FORM_TYPE_MISMATCH"/>
 *     &lt;enumeration value="FORM_DESIGN_AUTHORIZATION"/>
 *     &lt;enumeration value="PATIENT_NOT_ONSTUDY"/>
 *     &lt;enumeration value="STUDY_PATIENT_NOT_FOUND"/>
 *     &lt;enumeration value="PATIENT_PROTOCOL_INACTIVE"/>
 *     &lt;enumeration value="FORM_ACCESS_AUTHORIZATION"/>
 *     &lt;enumeration value="FORM_RESPONSE_NOT_FOUND"/>
 *     &lt;enumeration value="FORM_RESPONSE_DOES_NOT_EXIT"/>
 *     &lt;enumeration value="FORM_RESPONSE_LOCKDOWN"/>
 *     &lt;enumeration value="CRF_NOT_LINKED_WITH_CALENDAR_EVENT"/>
 *     &lt;enumeration value="STUDY_PATIENT_NOT_ON_CALENDAR"/>
 *     &lt;enumeration value="PATIENT_SCHEDULE_EVENT_NOT_FOUND"/>
 *     &lt;enumeration value="PATIENT_SCHEDULE_IDENTIFIER_NOT_FOUND"/>
 *     &lt;enumeration value="SINGLE_ENTRY_FORM_RESPONSE_ALREADY_EXISTS"/>
 *     &lt;enumeration value="PATIENT_STATUS_LOCKDOWN"/>
 *     &lt;enumeration value="ERROR_CREATING_FORM_RESPONSE"/>
 *     &lt;enumeration value="ERROR_UPDATING_USER_ACCOUNT_STATUS"/>
 *     &lt;enumeration value="EXECPTION_GETTING_REMOTE_USER_SERVICES"/>
 *     &lt;enumeration value="PATIENT_ORGANIZATION_ACCESS"/>
 *     &lt;enumeration value="PATIENT_FACILITY_ID_ALREADY_EXISTS"/>
 *     &lt;enumeration value="SPECIALITY_ACCESS_AUTHORIZATION"/>
 *     &lt;enumeration value="PATIENT_ERROR_UPDATE_ORGANIZATION"/>
 *     &lt;enumeration value="ERROR_REMOVING_STUDY_PATIENT_STATUS"/>
 *     &lt;enumeration value="STUDY_PATIENT_STATUS_NOT_FOUND"/>
 *     &lt;enumeration value="ERROR_UPDATING_STUDY_PATIENT_STATUS"/>
 *     &lt;enumeration value="ERROR_UPDATING_FORM_RESPONSE"/>
 *     &lt;enumeration value="CODE_LIST_TYPE_NOT_FOUND"/>
 *     &lt;enumeration value="EVENT_ERROR_CREATE_STATUS"/>
 *     &lt;enumeration value="ACCOUNT_NOT_FOUND"/>
 *     &lt;enumeration value="USER_ACCOUNT_NOT_MATCHED"/>
 *     &lt;enumeration value="INVALID_ESIGNATURE"/>
 *     &lt;enumeration value="MILESTONE_STATUS_NOT_FOUND"/>
 *     &lt;enumeration value="DUPLICATE_BUDGET_NAME"/>
 *     &lt;enumeration value="DUPLICATE_STUDYVER_NUMBER"/>
 *     &lt;enumeration value="BUDGET_ALREADY_EXISTS"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "issueTypes")
@XmlEnum
public enum IssueTypes {

    MULTIPLE_OBJECTS_FOUND,
    CODE_NOT_FOUND,
    DATA_VALIDATION,
    DUPLICATE_USER,
    INVALID_SORT_BY_OPTION,
    INVALID_ORDER_BY_OPTION,
    INVALID_SITE_OF_SERVICE,
    INVALID_FACILITY_ID,
    INVALID_EVENT_CATEGORY,
    EVENT_CATEGORY_DOES_NOT_EXIST,
    SCHEDULE_NOT_FOUND,
    STUDY_AUTHOR_NOT_FOUND,
    STUDY_PRINCIPAL_INVESTIGATOR_NOT_FOUND,
    STUDY_COORDINATOR_NOT_FOUND,
    STUDY_NUMBER_EXISTS,
    STUDY_NUMBER_NOT_FOUND,
    STUDY_NUMBER_SIZE_ISSUE,
    STUDY_NOT_FOUND,
    STUDY_ORG_NOT_FOUND,
    STUDY_ERROR_REMOVE_ORGANIZATION,
    STUDY_ERROR_REMOVE_ORGANIZATION_CURRENT_STUDYSTATUS,
    STUDY_ERROR_REMOVE_ORGANIZATION_ENROLLED_PATIENT,
    STUDY_ERROR_CREATE_ORGANIZATION,
    STUDY_ERROR_UPDATE_ORGANIZATION,
    STUDY_ERROR_CREATE_STATUS,
    STUDY_ERROR_UPDATE_STATUS,
    STUDY_ERROR_CREATE_TEAM_MEMBER,
    STUDY_ERROR_UPDATE_TEAM_MEMBER,
    STUDY_ERROR_REMOVE_TEAM_MEMBER,
    STUDY_REMOVE_DATA_MANAGER,
    STUDY_ERROR_CREATE_MSD,
    STUDY_ERROR_UPDATE_MSD,
    NOT_IMPLEMENTED,
    UNKNOWN_THROWABLE,
    STUDY_ERROR_TEAM_MEMBER_ALREADY_EXISTS,
    ENROLLMENT_NOTIFICATION_USER_NOT_FOUND,
    GET_APPROVE_ENROLLMENT_NOTIFICATION_USER_NOT_FOUND,
    GET_ASSIGNED_TO_USERID_NOT_FOUND,
    GET_DOCUMENTED_BY_USERID_NOT_FOUND,
    STUDY_TEAM_AUTHORIZATION,
    GROUP_NOT_FOUND,
    GROUP_AUTHORIZATION,
    ORGANIZATION_AUTHORIZATION,
    ORGANIZATION_NOT_FOUND,
    STUDY_ERROR_CREATE_DUPLICATE_ORGANIZATION,
    STUDY_STATUS_ORG_NOT_IN_STUDY,
    USER_NOT_FOUND,
    SYSTEM_USER_NOT_FOUND,
    DOES_NOT_HAVE_RIGHTS_TO_KILL_SESSION,
    USER_SESSION_NOT_KILLED,
    USER_NOT_LOGGED_IN,
    USER_HIDDEN,
    HEART_BEAT_NOT_FOUND,
    DATABASE_CONNECTION_NOT_FOUND,
    EXECPTION_WHILE_READING_MANIFEST_FILE,
    EXECPTION_WHILE_GETTING_ATTRIBUTES_FROM_MANIFEST_FILE,
    EXECPTION_GETTING_REMOTE_MONITORING_SERVICES,
    PATIENT_NOT_FOUND,
    PATIENT_DATA_AUTHORIZATION,
    STUDIES_FOR_PATIENT_NOT_FOUND,
    PATIENT_SCHEDULE_NOT_FOUND,
    PATIENT_ALREADY_REGISTERED_TO_THIS_ORGANIZATION,
    EXECPTION_GETTING_REMOTE_PATIENT_DEMOGRAPHICS_SERVICES,
    STUDY_STATUS_NOT_FOUND,
    CALENDAR_NOT_FOUND,
    CALENDAR_NOT_ACTIVE,
    EVENT_NOT_FOUND,
    EVENT_STATUS_IDENTIFIER_NOT_FOUND,
    VISIT_NOT_FOUND,
    FORM_FIELD_NOT_FOUND,
    CURRENCY_NOT_FOUND,
    ERROR_CREATING_STUDY_CALENDAR,
    ERROR_CREATING_STUDY_CALENDAR_EVENT,
    ERROR_CREATING_STUDY_CALENDAR_EVENT_COST,
    STUDY_CALENDAR_ERROR_CREATE_EVENT,
    PATIENT_SCHEDULE_DOES_NOT_EXIST,
    CALENDAR_NAME_ALREADY_EXISTS,
    VISIT_NAME_ALREADY_EXISTS,
    VISIT_INTERVAL_EXCEEDS_CALENDAR_DURATION,
    VISIT_IDENTIFIER_INVALID,
    EVENT_IDENTIFIER_INAVLID,
    DUPLICATE_VISIT_NAME,
    DUPLICATE_EVENT_NAME,
    PATIENT_ID_ALREADY_EXISTS,
    PATIENT_ALREADY_EXISTS,
    PATIENT_IDENTIFIER_INVALID,
    PATIENT_ERROR_CREATE_DEMOGRAPHICS,
    PATIENT_ERROR_ADD_ORGANIZATION,
    STUDY_CALENDAR_ERROR_REMOVE_VISIT,
    STUDY_IDENTIFIER_INVALID,
    BUDGET_NOT_FOUND,
    ERROR_REMOVING_STUDY_CALENDAR_EVENT,
    ERROR_REMOVING_STUDY_CALENDAR,
    CALENDAR_ACTIVE_NOT_UPDATEABLE,
    PATIENT_ERROR_UPDATE_DEMOGRAPHICS,
    ERROR_CREATE_PATIENT_ENROLLMENT,
    ERROR_CREATE_MILESTONE,
    ERROR_CREATE_BUDGET,
    ERROR_CREATE_VERSION,
    PATIENT_ALREADY_ENROLLED,
    DUPLICATE_ORGANIZATION,
    ERROR_UPDATING_STUDY_CALENDAR,
    ERROR_UPDATING_STUDY_CALENDAR_EVENT,
    INVALID_EVENT_DURATION,
    INVALID_VISIT_DURATION,
    INVALID_FILTER_FIELD_ID,
    FORM_NOT_FOUND,
    FORM_RESPONSE_DOES_NOT_EXIST,
    EVENT_COST_NOT_FOUND,
    ERROR_REMOVING_EVENT_COST,
    ERROR_UPDATING_EVENT_COST,
    CALENDAR_STATUS_NOT_UPDATEABLE,
    FORM_TYPE_MISMATCH,
    FORM_DESIGN_AUTHORIZATION,
    PATIENT_NOT_ONSTUDY,
    STUDY_PATIENT_NOT_FOUND,
    PATIENT_PROTOCOL_INACTIVE,
    FORM_ACCESS_AUTHORIZATION,
    FORM_RESPONSE_NOT_FOUND,
    FORM_RESPONSE_DOES_NOT_EXIT,
    FORM_RESPONSE_LOCKDOWN,
    CRF_NOT_LINKED_WITH_CALENDAR_EVENT,
    STUDY_PATIENT_NOT_ON_CALENDAR,
    PATIENT_SCHEDULE_EVENT_NOT_FOUND,
    PATIENT_SCHEDULE_IDENTIFIER_NOT_FOUND,
    SINGLE_ENTRY_FORM_RESPONSE_ALREADY_EXISTS,
    PATIENT_STATUS_LOCKDOWN,
    ERROR_CREATING_FORM_RESPONSE,
    ERROR_UPDATING_USER_ACCOUNT_STATUS,
    EXECPTION_GETTING_REMOTE_USER_SERVICES,
    PATIENT_ORGANIZATION_ACCESS,
    PATIENT_FACILITY_ID_ALREADY_EXISTS,
    SPECIALITY_ACCESS_AUTHORIZATION,
    PATIENT_ERROR_UPDATE_ORGANIZATION,
    ERROR_REMOVING_STUDY_PATIENT_STATUS,
    STUDY_PATIENT_STATUS_NOT_FOUND,
    ERROR_UPDATING_STUDY_PATIENT_STATUS,
    ERROR_UPDATING_FORM_RESPONSE,
    CODE_LIST_TYPE_NOT_FOUND,
    EVENT_ERROR_CREATE_STATUS,
    ACCOUNT_NOT_FOUND,
    USER_ACCOUNT_NOT_MATCHED,
    INVALID_ESIGNATURE,
    MILESTONE_STATUS_NOT_FOUND,
    DUPLICATE_BUDGET_NAME,
    DUPLICATE_STUDYVER_NUMBER,
    BUDGET_ALREADY_EXISTS;

    public String value() {
        return name();
    }

    public static IssueTypes fromValue(String v) {
        return valueOf(v);
    }

}
