
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getListOfStudyFormResponsesResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getListOfStudyFormResponsesResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Response" type="{http://velos.com/services/}studyFormResponses" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getListOfStudyFormResponsesResponse", propOrder = {
    "response"
})
public class GetListOfStudyFormResponsesResponse {

    @XmlElement(name = "Response")
    protected StudyFormResponses response;

    /**
     * Gets the value of the response property.
     * 
     * @return
     *     possible object is
     *     {@link StudyFormResponses }
     *     
     */
    public StudyFormResponses getResponse() {
        return response;
    }

    /**
     * Sets the value of the response property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyFormResponses }
     *     
     */
    public void setResponse(StudyFormResponses value) {
        this.response = value;
    }

}
