
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getListOfAccountFormResponsesResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getListOfAccountFormResponsesResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="accountFormResponse" type="{http://velos.com/services/}accountFormResponses" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getListOfAccountFormResponsesResponse", propOrder = {
    "accountFormResponse"
})
public class GetListOfAccountFormResponsesResponse {

    protected AccountFormResponses accountFormResponse;

    /**
     * Gets the value of the accountFormResponse property.
     * 
     * @return
     *     possible object is
     *     {@link AccountFormResponses }
     *     
     */
    public AccountFormResponses getAccountFormResponse() {
        return accountFormResponse;
    }

    /**
     * Sets the value of the accountFormResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountFormResponses }
     *     
     */
    public void setAccountFormResponse(AccountFormResponses value) {
        this.accountFormResponse = value;
    }

}
