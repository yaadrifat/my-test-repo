
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import com.velos.services.SimpleIdentifier;


/**
 * <p>Java class for bgtCalNameIdentifier complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="bgtCalNameIdentifier">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}simpleIdentifier">
 *       &lt;sequence>
 *         &lt;element name="bgtCalName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "bgtCalNameIdentifier", propOrder = {
    "bgtCalName"
})
public class BgtCalNameIdentifier
    extends SimpleIdentifier
{

    protected String bgtCalName;

    /**
     * Gets the value of the bgtCalName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBgtCalName() {
        return bgtCalName;
    }

    /**
     * Sets the value of the bgtCalName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBgtCalName(String value) {
        this.bgtCalName = value;
    }

}
