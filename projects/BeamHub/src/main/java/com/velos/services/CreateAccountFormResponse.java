
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createAccountFormResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createAccountFormResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccountFormResponse" type="{http://velos.com/services/}accountFormResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createAccountFormResponse", propOrder = {
    "accountFormResponse"
})
public class CreateAccountFormResponse {

    @XmlElement(name = "AccountFormResponse")
    protected AccountFormResponse accountFormResponse;

    /**
     * Gets the value of the accountFormResponse property.
     * 
     * @return
     *     possible object is
     *     {@link AccountFormResponse }
     *     
     */
    public AccountFormResponse getAccountFormResponse() {
        return accountFormResponse;
    }

    /**
     * Sets the value of the accountFormResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountFormResponse }
     *     
     */
    public void setAccountFormResponse(AccountFormResponse value) {
        this.accountFormResponse = value;
    }

}
