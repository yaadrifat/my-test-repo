
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for addEventsToStudyCalendarVisit complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="addEventsToStudyCalendarVisit">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CalendarIdentifier" type="{http://velos.com/services/}calendarIdentifier" minOccurs="0"/>
 *         &lt;element name="VisitName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VisitIdentifer" type="{http://velos.com/services/}visitIdentifier" minOccurs="0"/>
 *         &lt;element name="Events" type="{http://velos.com/services/}calendarEvents" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "addEventsToStudyCalendarVisit", propOrder = {
    "calendarIdentifier",
    "visitName",
    "visitIdentifer",
    "events"
})
public class AddEventsToStudyCalendarVisit {

    @XmlElement(name = "CalendarIdentifier")
    protected CalendarIdentifier calendarIdentifier;
    @XmlElement(name = "VisitName")
    protected String visitName;
    @XmlElement(name = "VisitIdentifer")
    protected VisitIdentifier visitIdentifer;
    @XmlElement(name = "Events")
    protected CalendarEvents events;

    /**
     * Gets the value of the calendarIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link CalendarIdentifier }
     *     
     */
    public CalendarIdentifier getCalendarIdentifier() {
        return calendarIdentifier;
    }

    /**
     * Sets the value of the calendarIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link CalendarIdentifier }
     *     
     */
    public void setCalendarIdentifier(CalendarIdentifier value) {
        this.calendarIdentifier = value;
    }

    /**
     * Gets the value of the visitName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVisitName() {
        return visitName;
    }

    /**
     * Sets the value of the visitName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVisitName(String value) {
        this.visitName = value;
    }

    /**
     * Gets the value of the visitIdentifer property.
     * 
     * @return
     *     possible object is
     *     {@link VisitIdentifier }
     *     
     */
    public VisitIdentifier getVisitIdentifer() {
        return visitIdentifer;
    }

    /**
     * Sets the value of the visitIdentifer property.
     * 
     * @param value
     *     allowed object is
     *     {@link VisitIdentifier }
     *     
     */
    public void setVisitIdentifer(VisitIdentifier value) {
        this.visitIdentifer = value;
    }

    /**
     * Gets the value of the events property.
     * 
     * @return
     *     possible object is
     *     {@link CalendarEvents }
     *     
     */
    public CalendarEvents getEvents() {
        return events;
    }

    /**
     * Sets the value of the events property.
     * 
     * @param value
     *     allowed object is
     *     {@link CalendarEvents }
     *     
     */
    public void setEvents(CalendarEvents value) {
        this.events = value;
    }

}
