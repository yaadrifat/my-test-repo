
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updateAccountFormResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updateAccountFormResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FormResponseIdentifier" type="{http://velos.com/services/}accountFormResponseIdentifier" minOccurs="0"/>
 *         &lt;element name="AccountFormResponse" type="{http://velos.com/services/}accountFormResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateAccountFormResponse", propOrder = {
    "formResponseIdentifier",
    "accountFormResponse"
})
public class UpdateAccountFormResponse {

    @XmlElement(name = "FormResponseIdentifier")
    protected AccountFormResponseIdentifier formResponseIdentifier;
    @XmlElement(name = "AccountFormResponse")
    protected AccountFormResponse accountFormResponse;

    /**
     * Gets the value of the formResponseIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link AccountFormResponseIdentifier }
     *     
     */
    public AccountFormResponseIdentifier getFormResponseIdentifier() {
        return formResponseIdentifier;
    }

    /**
     * Sets the value of the formResponseIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountFormResponseIdentifier }
     *     
     */
    public void setFormResponseIdentifier(AccountFormResponseIdentifier value) {
        this.formResponseIdentifier = value;
    }

    /**
     * Gets the value of the accountFormResponse property.
     * 
     * @return
     *     possible object is
     *     {@link AccountFormResponse }
     *     
     */
    public AccountFormResponse getAccountFormResponse() {
        return accountFormResponse;
    }

    /**
     * Sets the value of the accountFormResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountFormResponse }
     *     
     */
    public void setAccountFormResponse(AccountFormResponse value) {
        this.accountFormResponse = value;
    }

}
