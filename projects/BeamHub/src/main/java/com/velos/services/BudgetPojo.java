
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for budgetPojo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="budgetPojo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BUDGET_CALFLAG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BUDGET_COMBFLAG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BUDGET_CREATOR" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="BUDGET_CURRENCY" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="BUDGET_DELFLAG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BUDGET_DESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BUDGET_RIGHTS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BUDGET_RIGHTSCOPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BUDGET_SITEFLAG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BUDGET_TEMPLATE" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="BUDGET_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="budgetCalendars" type="{http://velos.com/services/}budgetCalList" minOccurs="0"/>
 *         &lt;element name="PK_CODELST_STATUS" type="{http://velos.com/services/}code" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "budgetPojo", propOrder = {
    "budgetcalflag",
    "budgetcombflag",
    "budgetcreator",
    "budgetcurrency",
    "budgetdelflag",
    "budgetdesc",
    "budgetrights",
    "budgetrightscope",
    "budgetsiteflag",
    "budgettemplate",
    "budgettype",
    "budgetCalendars",
    "pkcodelststatus"
})
public class BudgetPojo {

    @XmlElement(name = "BUDGET_CALFLAG")
    protected String budgetcalflag;
    @XmlElement(name = "BUDGET_COMBFLAG")
    protected String budgetcombflag;
    @XmlElement(name = "BUDGET_CREATOR")
    protected Integer budgetcreator;
    @XmlElement(name = "BUDGET_CURRENCY")
    protected Code budgetcurrency;
    @XmlElement(name = "BUDGET_DELFLAG")
    protected String budgetdelflag;
    @XmlElement(name = "BUDGET_DESC")
    protected String budgetdesc;
    @XmlElement(name = "BUDGET_RIGHTS")
    protected String budgetrights;
    @XmlElement(name = "BUDGET_RIGHTSCOPE")
    protected String budgetrightscope;
    @XmlElement(name = "BUDGET_SITEFLAG")
    protected String budgetsiteflag;
    @XmlElement(name = "BUDGET_TEMPLATE")
    protected Code budgettemplate;
    @XmlElement(name = "BUDGET_TYPE")
    protected String budgettype;
    protected BudgetCalList budgetCalendars;
    @XmlElement(name = "PK_CODELST_STATUS")
    protected Code pkcodelststatus;

    /**
     * Gets the value of the budgetcalflag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBUDGETCALFLAG() {
        return budgetcalflag;
    }

    /**
     * Sets the value of the budgetcalflag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBUDGETCALFLAG(String value) {
        this.budgetcalflag = value;
    }

    /**
     * Gets the value of the budgetcombflag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBUDGETCOMBFLAG() {
        return budgetcombflag;
    }

    /**
     * Sets the value of the budgetcombflag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBUDGETCOMBFLAG(String value) {
        this.budgetcombflag = value;
    }

    /**
     * Gets the value of the budgetcreator property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBUDGETCREATOR() {
        return budgetcreator;
    }

    /**
     * Sets the value of the budgetcreator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBUDGETCREATOR(Integer value) {
        this.budgetcreator = value;
    }

    /**
     * Gets the value of the budgetcurrency property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getBUDGETCURRENCY() {
        return budgetcurrency;
    }

    /**
     * Sets the value of the budgetcurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setBUDGETCURRENCY(Code value) {
        this.budgetcurrency = value;
    }

    /**
     * Gets the value of the budgetdelflag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBUDGETDELFLAG() {
        return budgetdelflag;
    }

    /**
     * Sets the value of the budgetdelflag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBUDGETDELFLAG(String value) {
        this.budgetdelflag = value;
    }

    /**
     * Gets the value of the budgetdesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBUDGETDESC() {
        return budgetdesc;
    }

    /**
     * Sets the value of the budgetdesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBUDGETDESC(String value) {
        this.budgetdesc = value;
    }

    /**
     * Gets the value of the budgetrights property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBUDGETRIGHTS() {
        return budgetrights;
    }

    /**
     * Sets the value of the budgetrights property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBUDGETRIGHTS(String value) {
        this.budgetrights = value;
    }

    /**
     * Gets the value of the budgetrightscope property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBUDGETRIGHTSCOPE() {
        return budgetrightscope;
    }

    /**
     * Sets the value of the budgetrightscope property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBUDGETRIGHTSCOPE(String value) {
        this.budgetrightscope = value;
    }

    /**
     * Gets the value of the budgetsiteflag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBUDGETSITEFLAG() {
        return budgetsiteflag;
    }

    /**
     * Sets the value of the budgetsiteflag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBUDGETSITEFLAG(String value) {
        this.budgetsiteflag = value;
    }

    /**
     * Gets the value of the budgettemplate property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getBUDGETTEMPLATE() {
        return budgettemplate;
    }

    /**
     * Sets the value of the budgettemplate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setBUDGETTEMPLATE(Code value) {
        this.budgettemplate = value;
    }

    /**
     * Gets the value of the budgettype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBUDGETTYPE() {
        return budgettype;
    }

    /**
     * Sets the value of the budgettype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBUDGETTYPE(String value) {
        this.budgettype = value;
    }

    /**
     * Gets the value of the budgetCalendars property.
     * 
     * @return
     *     possible object is
     *     {@link BudgetCalList }
     *     
     */
    public BudgetCalList getBudgetCalendars() {
        return budgetCalendars;
    }

    /**
     * Sets the value of the budgetCalendars property.
     * 
     * @param value
     *     allowed object is
     *     {@link BudgetCalList }
     *     
     */
    public void setBudgetCalendars(BudgetCalList value) {
        this.budgetCalendars = value;
    }

    /**
     * Gets the value of the pkcodelststatus property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getPKCODELSTSTATUS() {
        return pkcodelststatus;
    }

    /**
     * Sets the value of the pkcodelststatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setPKCODELSTSTATUS(Code value) {
        this.pkcodelststatus = value;
    }

}
