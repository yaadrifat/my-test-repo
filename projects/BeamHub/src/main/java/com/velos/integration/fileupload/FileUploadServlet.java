package com.velos.integration.fileupload;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.postgresql.copy.CopyManager;
import org.postgresql.core.BaseConnection;

import com.velos.integration.dao.MessageDAO;

/**
 * Servlet implementation class FileUploadServlet
 */
public class FileUploadServlet extends HttpServlet {
	
	private static Logger logger = Logger.getLogger(FileUploadServlet.class);
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FileUploadServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		Properties prop = new Properties();
		try {
			prop.load(this.getClass().getClassLoader()
					.getResourceAsStream("config.properties"));
			prop.load(this.getClass().getClassLoader()
					.getResourceAsStream("vgdb.properties"));
		} catch (IOException e) {
			logger.error("Error Loading config.properties:", e);
		}
		
		Connection dbConnection = null;
		Statement statement = null;
		try {
			dbConnection = getDBConnection(prop);
			String sql = "update formresponse set status='Exporting to CSV' where status='Not Exported'";
			statement = dbConnection.createStatement();
			int rowsUpdated = statement.executeUpdate(sql);
			logger.info("Rows Updated = "+rowsUpdated);
			if(rowsUpdated==0){
				String sqlUp = "select COUNT(*) from formresponse where status='Exporting to CSV'";
				ResultSet rs = statement.executeQuery(sqlUp);
				rs.next();
				rowsUpdated = rs.getInt(1);
				rs.close();
			}
			if(rowsUpdated>0){
				long rowsExported = exportDataTOCSVFile(dbConnection,statement,prop);
				if(rowsExported>0){
					logger.info("Data exported to CSV file");
					String sqlUpdate="update formresponse set status='Exported to CSV' where status='Exporting to CSV'";
					int rowsUpdated2 = statement.executeUpdate(sqlUpdate);
					logger.info("Rows Updated = "+rowsUpdated2);
					FTPUtil.connectToFTP(prop);
					out.println("<h1>Form Responses Uploaded successfully</h1>");
				}
			}else{
				logger.info("No Data available to export to CSV file");
				out.println("<h1>No Data available to Upload</h1>");
			}
		} catch (Exception e) {
			//e.printStackTrace();
			logger.error("Error : ",e);
			out.println("<h1>Failed to Upload Data</h1>");
		}finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (dbConnection != null) {
				try {
					dbConnection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	private static Connection getDBConnection(Properties prop) {
		Connection dbConnection = null;
		String DB_DRIVER = prop.getProperty("dataSource.driverClassName");
		String DB_CONNECTION = prop.getProperty("dataSource.url");
		String DB_USER = prop.getProperty("dataSource.username");
		String DB_PASSWORD = prop.getProperty("dataSource.password");
		try {
			Class.forName(DB_DRIVER);
		} catch (ClassNotFoundException e) {
			logger.info(e.getMessage());
		}
		try {
			dbConnection = DriverManager.getConnection(
                              DB_CONNECTION, DB_USER,DB_PASSWORD);
			return dbConnection;
		} catch (SQLException e) {
			logger.info(e.getMessage());
		}
		return dbConnection;
	}
	
	private long exportDataTOCSVFile(Connection dbConnection,Statement statement,Properties prop) throws SQLException, IOException{
		long rowsExported = 0;
		String catBase = System.getProperty("catalina.base");
		String dateFormat = getFormattedCurrentDate("yyyyMMMddHHmmss");
		/*String csvPath = prop.getProperty("CSVFilePath");
		String localDirPath = catBase+csvPath;
		logger.info("Local CSV file Directory = "+localDirPath);*/
		String localDirPath = catBase+File.separator+"formresponses";
		File file = new File(localDirPath);
		file.mkdirs();
		String fileName = "formresponse-"+dateFormat+".csv";
		String filePath = localDirPath+File.separator+fileName;
		FileOutputStream fileOutputStream = new FileOutputStream(filePath);
		String query = "SELECT * FROM formresponse where status='Exporting to CSV'";
		CopyManager copyManager = new CopyManager((BaseConnection) dbConnection);
		rowsExported = copyManager.copyOut("COPY (" + query + ") TO STDOUT WITH (FORMAT CSV)", fileOutputStream);
		logger.info("Rows Exported = "+rowsExported);
		return rowsExported;
	}

	private String getFormattedCurrentDate(String format) {
    	SimpleDateFormat f = new SimpleDateFormat(format);
    	return f.format(Calendar.getInstance().getTime());
    }

}
