package com.velos.integration.incomingmessage.bean;

import com.velos.services.StudyPatientFormResponse;

public class IncomingMessageBean {
	private int primaryKey;
	private String studyNumber;
	private String patientId;
	private String formName;
	private StudyPatientFormResponse formRes;
	private String xmlMessage;
	private String status;
	public int getPrimaryKey() {
		return primaryKey;
	}
	public void setPrimaryKey(int primaryKey) {
		this.primaryKey = primaryKey;
	}
	public String getStudyNumber() {
		return studyNumber;
	}
	public void setStudyNumber(String studyNumber) {
		this.studyNumber = studyNumber;
	}
	public String getPatientId() {
		return patientId;
	}
	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}
	public String getFormName() {
		return formName;
	}
	public void setFormName(String formName) {
		this.formName = formName;
	}
	public StudyPatientFormResponse getFormRes() {
		return formRes;
	}
	public void setFormRes(StudyPatientFormResponse formRes) {
		this.formRes = formRes;
	}
	public String getXmlMessage() {
		return xmlMessage;
	}
	public void setXmlMessage(String xmlMessage) {
		this.xmlMessage = xmlMessage;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	

}
