package com.velos.integration.espclient;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import com.velos.services.OperationException_Exception;

@WebService(targetNamespace = "http://velos.com/services/", name = "FormSEI")
public interface VelosEspGetListOfStudyPatientForms {

	 @WebResult(name = "FormList", targetNamespace = "")
	    @RequestWrapper(localName = "getListOfStudyPatientForms", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetListOfStudyPatientForms")
	    @WebMethod
	    @ResponseWrapper(localName = "getListOfStudyPatientFormsResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetListOfStudyPatientFormsResponse")
	    public com.velos.services.FormList getListOfStudyPatientForms(
	        @WebParam(name = "PatientIdentifier", targetNamespace = "")
	        com.velos.services.PatientIdentifier patientIdentifier,
	        @WebParam(name = "StudyIdentifier", targetNamespace = "")
	        com.velos.services.StudyIdentifier studyIdentifier,
	        @WebParam(name = "maxNumberOfResults", targetNamespace = "")
	        int maxNumberOfResults,
	        @WebParam(name = "formHasResponses", targetNamespace = "")
	        boolean formHasResponses
	    ) throws OperationException_Exception;
}
