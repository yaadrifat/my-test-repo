package com.velos.integration.espclient;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import com.velos.services.OperationException_Exception;

@WebService(targetNamespace = "http://velos.com/services/", name = "StudyCalendarSEI")
public interface VelosEspGetStudyCalendarEnpdoint {
	 @WebResult(name = "StudyCalendar", targetNamespace = "")
	    @RequestWrapper(localName = "getStudyCalendar", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetStudyCalendar")
	    @WebMethod
	    @ResponseWrapper(localName = "getStudyCalendarResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetStudyCalendarResponse")
	    public com.velos.services.StudyCalendar getStudyCalendar(
	        @WebParam(name = "CalendarIdentifier", targetNamespace = "")
	        com.velos.services.CalendarIdentifier calendarIdentifier,
	        @WebParam(name = "StudyIdentifier", targetNamespace = "")
	        com.velos.services.StudyIdentifier studyIdentifier,
	        @WebParam(name = "CalendarName", targetNamespace = "")
	        java.lang.String calendarName
	    ) throws OperationException_Exception;
}
