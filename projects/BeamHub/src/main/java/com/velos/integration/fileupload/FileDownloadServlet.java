package com.velos.integration.fileupload;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.postgresql.copy.CopyManager;
import org.postgresql.core.BaseConnection;

import com.velos.integration.dao.MessageDAO;

/**
 * Servlet implementation class FileUploadServlet
 */
public class FileDownloadServlet extends HttpServlet {
	
	private static Logger logger = Logger.getLogger(FileDownloadServlet.class);
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FileDownloadServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		Properties prop = new Properties();
		
		try {
			prop.load(this.getClass().getClassLoader()
					.getResourceAsStream("config.properties"));
			prop.load(this.getClass().getClassLoader()
					.getResourceAsStream("vgdb.properties"));
		} catch (IOException e) {
			logger.error("Error Loading config.properties:", e);
		}
		
		try {
			FTPUtil.downloadFromFTP(prop);
			out.println("<h1>Form Responses downloaded successfully</h1>");
		} catch (Exception e) {
			logger.error("Error : ",e);
			out.println("<h1>Failed to download Form Responses</h1>");
		}
		
	}

}
