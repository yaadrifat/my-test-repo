package com.velos.integration.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.Properties;

import org.apache.log4j.Logger;


public class PropertiesUtil {

	private static Logger logger = Logger.getLogger(PropertiesUtil.class);

	private Properties prop;

	public PropertiesUtil() {
		logger.info("**************Loading Properties");
		getAllProperties();
	}

	public Properties getAllProperties() {

		
		if (prop == null) {
			logger.info("Checking prop = "+prop);
			prop = loadProperties();
			
		}
		return prop;
	}
	
	public Properties loadProperties(){
		
		Properties prop = new Properties();
		
		try {
			logger.info("**************Loading Properties Again");
			prop.load(this.getClass().getClassLoader()
					.getResourceAsStream("config.properties"));
			prop.load(this.getClass().getClassLoader()
					.getResourceAsStream("mapping.properties"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			logger.error("Error Loading config.properties:", e);
		}

		ArrayList<String> propList = new ArrayList<String>();
		for (Entry<Object, Object> e : prop.entrySet()) {

			if ("select".equals(e.getValue().toString())) {
				propList.add(e.getKey().toString());
			}
		}
		logger.info("Prop List = " + propList);

		for (String list : propList) {
			logger.info("Property file = " + list);
			try {
				prop.load(this.getClass().getClassLoader()
						.getResourceAsStream(list));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.error("Error Loading All properties files:", e);
			}
		}
		logger.info("Properties = " + prop);
	
	return prop;
	}

}
