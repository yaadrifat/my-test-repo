package com.velos.integration.forms.bean;

public class FormsBean {

	private String formName;
	private String formType;
	private String formOid;
	
	public String getFormName() {
		return formName;
	}
	public void setFormName(String formName) {
		this.formName = formName;
	}
	public String getFormType() {
		return formType;
	}
	public void setFormType(String formType) {
		this.formType = formType;
	}
	public String getFormOid() {
		return formOid;
	}
	public void setFormOid(String formOid) {
		this.formOid = formOid;
	}

}
