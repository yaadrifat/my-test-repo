package com.velos.integration.dao;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import com.velos.integration.forms.bean.FormsBean;
import com.velos.integration.forms.bean.FormsFieldsBean;
import com.velos.services.StudyPatientFormResponse;

public class MessageDAO {
	
	private static Logger logger = Logger.getLogger(MessageDAO.class);
	
	private DataSource eresDataSource;
	private JdbcTemplate jdbcTemplate;
	
	public void setEresDataSource(DataSource eresDataSource) {
		this.eresDataSource = eresDataSource;
		this.jdbcTemplate = new JdbcTemplate(eresDataSource);
		logger.info("*******Jdbc Connection created successfully");	}
	
	public boolean saveMessageDetails(String StudyNumber,String BroadCasting_Site,String Participating_Site,String Component_Type,String Component_Name,String Status,String response_message) {
				
		boolean isSuccess = false;
		try{
			String sql = "INSERT INTO BeamHub_AUDIT_DETAILS"
						+"(PK_MSG_AUDIT,STUDYNUMBER,BROADCASTING_SITE,PARTICIPATING_SITE,COMPONENT_TYPE,COMPONENT_NAME,STATUS,RESPONSE_MESSAGE,CREATION_DATE)"
						+" VALUES(nextval('seq_BeamHub_AUDIT_DETAILS'),?,?,?,?,?,?,?,current_timestamp)";

			jdbcTemplate.update(sql,new Object[]{StudyNumber,BroadCasting_Site,Participating_Site,Component_Type,Component_Name,Status,response_message});
			isSuccess = true;
		}catch(Exception e){
			logger.error("Database Error : ",e);
			isSuccess = false;
		}
		if(isSuccess){
			logger.info("//Successfully Saved in DB");
		}
		return isSuccess;
	}
	
	public boolean saveFormResponseDetails(String StudyNumber,String PatientID,String formName,String request_message,String response_message) {
		boolean isSuccess = false;
		try{
			String sql = "INSERT INTO BeamForms_AUDIT_DETAILS"
						+"(PK_MSG_AUDIT,STUDYNUMBER,PATIENTID,FORMNAME,REQUEST_MESSAGE,RESPONSE_MESSAGE,CREATION_DATE)"
						+" VALUES(nextval('seq_BeamForms_AUDIT_DETAILS'),?,?,?,?,?,current_timestamp)";

			jdbcTemplate.update(sql,new Object[]{StudyNumber,PatientID,formName,request_message,response_message});
			isSuccess = true;
		}catch(Exception e){
			logger.error("Database Error : ",e);
			isSuccess = false;
		}
		if(isSuccess){
			logger.info("//Successfully Saved in DB");
		}
		return isSuccess;
	}
	
	public FormsBean getFormsConfiguration(String formName) throws Exception ,IOException{
		FormsBean fb =new FormsBean();
		try{
			String sql="select * from forms where formname='"+formName+"'";
			Map row=jdbcTemplate.queryForMap(sql);
			fb.setFormName((String)row.get("formname"));
			fb.setFormType((String)row.get("formtype"));
			fb.setFormOid((String)row.get("formoid"));
		} catch (Exception e) {
			logger.error("Error While getting Forms Configuration");
			e.printStackTrace();
			throw e;
		}
		return fb;
	}
	
	public List<FormsFieldsBean> getFormFieldsConfiguration(String formName) throws Exception ,IOException{
		List<FormsFieldsBean> formsFieldsList=new ArrayList<FormsFieldsBean>();
		try{
			String sql="select * from forms_fields where formname='"+formName+"'";
			List<Map> rows=jdbcTemplate.queryForList(sql);
			for(Map row:rows)
			{
				FormsFieldsBean ffb =new FormsFieldsBean();
				ffb.setFormName((String)row.get("formname"));
				ffb.setFieldName((String)row.get("fieldname"));
				ffb.setFieldId((String)row.get("fieldid"));
				ffb.setFieldOid((String)row.get("fieldoid"));
				ffb.setDataDictionaryName((String)row.get("datadictionaryname"));
				ffb.setControlType((String)row.get("controltype"));
				formsFieldsList.add(ffb);
			}
		} catch (Exception e) {
			logger.error("Error While getting Forms Fields Configuration");
			e.printStackTrace();
			throw e;
		}
		return formsFieldsList;
	}

	public String getFormFieldsMapConfiguration(String formName,String dictionaryName,String originalValue) throws Exception ,IOException{
		String mappedValue ="";
		try{
			String sql="select mappedvalue from forms_fields_mapping_values where formname='"+formName+"' and datadictionaryname='"+dictionaryName+"' and originalvalue='"+originalValue+"'";
			mappedValue=(String) jdbcTemplate.queryForObject(sql, String.class);
		}catch(EmptyResultDataAccessException ere){
			logger.info("No Mapping value found for "+originalValue);
			return originalValue;
		}catch (Exception e) {
			logger.error("Error While getting Forms Fields Mapping Configuration");
			e.printStackTrace();
			throw e;
		}
		return mappedValue;
	}
	
	public boolean saveformresponse(String StudyNumber,String patientID,String formName,String receivedXml,StudyPatientFormResponse formRes) {
		boolean isSuccess = false;
		try{
			String sql = "INSERT INTO formresponse"
					+"(PK_MSG_AUDIT,STUDYNUMBER,PATIENT_ID,FORMNAME,FORMRESPONSEDATA,RECEIVED_MESSAGE,STATUS,CREATION_DATE)"
					+" VALUES(nextval('seq_formresponse'),?,?,?,?,?,'Not Exported',current_timestamp)";
	        ByteArrayOutputStream bos = new ByteArrayOutputStream();
	        ObjectOutputStream oos = new ObjectOutputStream(bos);
	        oos.writeObject(formRes);
	        oos.flush();
	        oos.close();
	        bos.close();
	        byte[] data = bos.toByteArray();
			jdbcTemplate.update(sql,new Object[]{StudyNumber,patientID,formName,data,receivedXml});
			isSuccess = true;
		}catch(Exception e){
			logger.error("Database Error : ",e);
			isSuccess = false;
		}
		if(isSuccess){
			logger.info("//Successfully Saved in DB");
		}
		return isSuccess;
	} 
	
	public boolean updateMessageStatus(String studyNumber,String patientID,String formName,String receivedXml,String status) {
		boolean isSuccess = false;
		try{
			String sql = "Update target_formresponse set status = '"+status+"' where "
					+"STUDYNUMBER='"+studyNumber+"' and PATIENT_ID='"+patientID+"' and FORMNAME='"+formName+"' and RECEIVED_MESSAGE='"+receivedXml+"'";
			int rowsUpdated = jdbcTemplate.update(sql,new Object[]{});
			logger.info("Rows Updated = "+rowsUpdated);
			isSuccess = true;
		}catch(Exception e){
			logger.error("Database Error : ",e);
			isSuccess = false;
		}
		if(isSuccess){
			logger.info("//Successfully Saved in DB");
		}
		return isSuccess;
	} 

}
