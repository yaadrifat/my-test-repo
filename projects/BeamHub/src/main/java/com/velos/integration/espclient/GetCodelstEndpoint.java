package com.velos.integration.espclient;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import com.velos.services.OperationException_Exception;
@WebService(targetNamespace = "http://velos.com/services/", name = "SystemAdministrationSEI")
public interface GetCodelstEndpoint {
    @WebResult(name = "Codes", targetNamespace = "")
    @RequestWrapper(localName = "getCodeList", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetCodeList")
    @WebMethod
    @ResponseWrapper(localName = "getCodeListResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetCodeListResponse")
    public com.velos.services.Codes getCodeList(
        @WebParam(name = "type", targetNamespace = "")
        java.lang.String type
    ) throws OperationException_Exception;
}
