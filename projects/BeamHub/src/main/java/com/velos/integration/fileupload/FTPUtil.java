package com.velos.integration.fileupload;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.net.ftp.FTPSClient;
import org.apache.log4j.Logger;
import org.postgresql.copy.CopyManager;
import org.postgresql.core.BaseConnection;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class FTPUtil {
	
	private static Logger logger = Logger.getLogger(FTPUtil.class);

	public static void uploadDirectory(FTPClient ftpClient,
	        String remoteDirPath, String localParentDir, String remoteParentDir)
	        throws IOException {
	 
	    logger.info("LISTING directory: " + localParentDir);
	 
	    File localDir = new File(localParentDir);
	    File[] subFiles = localDir.listFiles();
	    if (subFiles != null && subFiles.length > 0) {
	        for (File item : subFiles) {
	            String remoteFilePath = remoteDirPath + "/" + remoteParentDir
	                    + "/" + item.getName();
	            logger.info("Item Name = "+item.getName());
	            if (remoteParentDir.equals("")) {
	                remoteFilePath = remoteDirPath + "/" + item.getName();
	            }
	            logger.info("Remote File Path = "+remoteFilePath);
	 
	            if (item.isFile()) {
	                String localFilePath = item.getAbsolutePath();
	                logger.info("localFilePath = "+localFilePath);
	                String fileName = item.getName();
	                logger.info("fileName = "+fileName);
	                logger.info("-----------------");
	                logger.info("About to upload the file: " + localFilePath);
	                boolean uploaded = uploadSingleFile(ftpClient,
	                        localFilePath, remoteFilePath);
	                String reply = ftpClient.getReplyString();
	        		logger.info("Reply : " + reply);
	                if (uploaded) {
	                    logger.info("UPLOADED a file to: "
	                            + remoteFilePath);
	                    /*File delFile = new File(localFilePath);
	                    boolean deleted = delFile.delete();
	                    if(deleted){
	                    	logger.info("File Removed : "+localFilePath);
	                    }*/
	                    
	            		String catBase = System.getProperty("catalina.base");
	                    File destinationFolder =  new File(catBase+File.separatorChar+"filesuploaded");
	                    if (!destinationFolder.exists())
	                    {
	                        destinationFolder.mkdirs();
	                    }
	                    if(item.renameTo
	                       (new File(destinationFolder,item.getName())))
	                    {
	                        item.delete();
	                    }
	                    else
	                    {
	                        logger.info("Failed to move the file");
	                    }
	                } else {
	                    logger.info("COULD NOT upload the file: "
	                            + localFilePath);
	                }
	            } else {
	                // create directory on the server
	                boolean created = ftpClient.makeDirectory(remoteFilePath);
	                String reply = ftpClient.getReplyString();
	        		logger.info("Reply : " + reply);
	                if (created) {
	                    logger.info("CREATED the directory: "
	                            + remoteFilePath);
	                } else {
	                    logger.info("COULD NOT create the directory: "
	                            + remoteFilePath);
	                }
	 
	                // upload the sub directory
	                logger.info("Remote Parent Dir = "+remoteParentDir);
	                logger.info("Dir Item Name = "+item.getName());
	                String parent = remoteParentDir + "/" + item.getName();
	                if (remoteParentDir.equals("")) {
	                    parent = item.getName();
	                }
	                logger.info("After Remote Parent Dir = "+remoteParentDir);

	                localParentDir = item.getAbsolutePath();
	                logger.info("Local Parent Dir = "+localParentDir);
	                uploadDirectory(ftpClient, remoteDirPath, localParentDir,
	                        parent);
	            }
	        }
	    }
	}
	
	public static boolean uploadSingleFile(FTPClient ftpClient,
	        String localFilePath, String remoteFilePath) throws IOException {
	    File localFile = new File(localFilePath);
	 
	    InputStream inputStream = new FileInputStream(localFile);
	    try {
	        ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
	        return ftpClient.storeFile(remoteFilePath, inputStream);
	    } finally {
	        inputStream.close();
	    }
	}
	
	public static void connectToFTP(Properties prop) throws Exception{
		
		String catBase = System.getProperty("catalina.base");
		/*String csvPath = prop.getProperty("CSVFilePath");
		String localDirPath = catBase+csvPath;
		logger.info("Local CSV file Directory = "+localDirPath);*/
		String localDirPath = catBase+File.separator+"formresponses";
		String remoteDirPath = prop.getProperty("FTP_Location");
		String host = prop.getProperty("FTP_HostName");
		int port = Integer.parseInt(prop.getProperty("FTP_Port"));
		String username = prop.getProperty("FTP_UserName");
		String password = prop.getProperty("FTP_Password");
		FTPSClient ftpClient = new FTPSClient(false);
	      // Connect to host
	      ftpClient.connect(host, port);
	      int reply = ftpClient.getReplyCode();
	      if (FTPReply.isPositiveCompletion(reply)) {
	    	  // Login
	    	  if (ftpClient.login(username, password)) {
	    		  // Set protection buffer size
	    		  ftpClient.execPBSZ(0);
	    		  // Set data channel protection to private
	    		  ftpClient.execPROT("P");
	    		  // Enter local passive mode
	    		  ftpClient.enterLocalPassiveMode();
	    		  // Store file on host
	    		  FTPUtil.uploadDirectory(ftpClient, remoteDirPath, localDirPath, "");
	    		  // Logout
	    		  ftpClient.logout();
	         } else {
	          logger.info("FTP login failed");
	         }
	        // Disconnect
	    	ftpClient.disconnect();
	    } else {
	        logger.error("FTP connect to host failed");
	    }
	}
	
	public static void downloadDirectory(FTPClient ftpClient, String parentDir,
	        String currentDir, String saveDir) throws IOException {
	    String dirToList = parentDir;
	    if (!currentDir.equals("")) {
	        dirToList += "/" + currentDir;
	    }
	 
	    FTPFile[] subFiles = ftpClient.listFiles(dirToList);
	 
	    if (subFiles != null && subFiles.length > 0) {
	        for (FTPFile aFile : subFiles) {
	            String currentFileName = aFile.getName();
	            logger.info("currentFileName = "+currentFileName);
	            if (currentFileName.equals(".") || currentFileName.equals("..")) {
	                // skip parent directory and the directory itself
	                continue;
	            }
	            String filePath = parentDir + "/" + currentDir + "/"
	                    + currentFileName;
	            if (currentDir.equals("")) {
	                filePath = parentDir + "/" + currentFileName;
	            }
	 
	            String newDirPath = saveDir + parentDir + File.separator
	                    + currentDir + File.separator + currentFileName;
	            if (currentDir.equals("")) {
	                newDirPath = saveDir + parentDir + File.separator
	                          + currentFileName;
	            }
	 
	            if (aFile.isDirectory()) {
	                // create the directory in saveDir
	                File newDir = new File(newDirPath);
	                boolean created = newDir.mkdirs();
	                if (created) {
	                    logger.info("CREATED the directory: " + newDirPath);
	                } else {
	                    logger.info("COULD NOT create the directory: " + newDirPath);
	                }
	 
	                // download the sub directory
	                downloadDirectory(ftpClient, dirToList, currentFileName,
	                        saveDir);
	            } else {
	                // download the file
	                boolean success = downloadSingleFile(ftpClient, filePath,
	                        newDirPath);
	                if (success) {
	                    logger.info("DOWNLOADED the file: " + filePath);
	                } else {
	                    logger.info("COULD NOT download the file: "
	                            + filePath);
	                }
	            }
	        }
	    }
	}
	
	public static boolean downloadSingleFile(FTPClient ftpClient,
	        String remoteFilePath, String savePath) throws IOException {
	    /*File downloadFile = new File(savePath);*/
	     
	    /*File parentDir = downloadFile.getParentFile();
	    if (!parentDir.exists()) {
	        parentDir.mkdir();
	    }*/
	    /*downloadFile.mkdirs();*/
	    logger.info("remoteFilePath = "+remoteFilePath);
	    //logger.info("savePath = "+savePath);
	    
	    OutputStream outputStream = new BufferedOutputStream(
	            new FileOutputStream(savePath));
	    try {
	        ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
	        return ftpClient.retrieveFile(remoteFilePath, outputStream);
	    } catch (IOException ex) {
	        throw ex;
	    } finally {
	        if (outputStream != null) {
	            outputStream.close();
	        }
	    }
	}
	
	public static void downloadFromFTP(Properties prop) throws Exception{
		
		String catBase = System.getProperty("catalina.base");
		String saveDirPath=catBase+File.separator+"filesdownloaded"+File.separator;
		String processedFilePath = catBase+File.separator+"filesimported"+File.separator;
		File downloadFile = new File(saveDirPath);
        downloadFile.mkdirs();
		String remoteDirPath = prop.getProperty("FTP_Location");
		String host = prop.getProperty("FTP_HostName");
		int port = Integer.parseInt(prop.getProperty("FTP_Port"));
		String username = prop.getProperty("FTP_UserName");
		String password = prop.getProperty("FTP_Password");
		FTPSClient ftpClient = new FTPSClient(false);
	      // Connect to host
	      ftpClient.connect(host, port);
	      int reply = ftpClient.getReplyCode();
	      if (FTPReply.isPositiveCompletion(reply)) {
	    	  // Login
	    	  if (ftpClient.login(username, password)) {
	    		  // Set protection buffer size
	    		  ftpClient.execPBSZ(0);
	    		  // Set data channel protection to private
	    		  ftpClient.execPROT("P");
	    		  // Enter local passive mode
	    		  ftpClient.enterLocalPassiveMode();
	    		  FTPFile[] files = ftpClient.listFiles(remoteDirPath);
	              if (files != null && files.length > 0) {
	             	 for (FTPFile file : files) {
	             		 logger.info("-----------------");
	             		 String fileName = file.getName();                
	             		 logger.info("Filename = "+fileName);    
	             		 String remoteFilePath =remoteDirPath+fileName;
	             		 String localFilePath = saveDirPath+fileName;
	             		 logger.info("RemoteFilePath = "+remoteFilePath);
	             		 File processedFile = new File(processedFilePath+fileName);
	             		 if(processedFile.exists()){
	             			 logger.info("File Already Downloaded and Processed");
	             			 continue;
	             		 }
	             		 boolean isFileDownloaded = FTPUtil.downloadSingleFile(ftpClient,remoteFilePath,localFilePath);
	             		 if(isFileDownloaded){
	             			 logger.info("File Downloaded successfully......");
	             			 logger.info("Importing data from csv file");
	             			 boolean fileImported = importDataFromCSV(saveDirPath,fileName,prop);
	             			 if(!fileImported){
	             				logger.error("Failed to import data from CSV File");
	             			 }else{
	             				 logger.info("Data imported to DB");
	             				//Move the uploaded file to other directory and delete it from original location
	     	            		//logger.info("Catalina Base  = " + catBase);
	             				File item = new File(localFilePath);
	     	                    File destinationFolder =  new File(catBase+File.separatorChar+"filesimported");
	     	                    if (!destinationFolder.exists())
	     	                    {
	     	                        destinationFolder.mkdirs();
	     	                    }
	     	                    // moving it to a new location
	     	                    if(item.renameTo
	     	                       (new File(destinationFolder,item.getName())))
	     	                    {
	     	                        // if file copied successfully then delete the original file
	     	                        item.delete();
	     	                    }
	     	                    else
	     	                    {
	     	                        logger.info("Failed to move the Imported file");
	     	                    }
	             			 }
	             		  }
	             	  }
	               }else{
	             	 logger.info("Data Not available to download");
	               }
	    		  // Logout
	    		  ftpClient.logout();
	         } else {
	          logger.info("FTP login failed");
	         }
	        // Disconnect
	    	ftpClient.disconnect();
	    } else {
	        logger.error("FTP connect to host failed");
	    }
	}
	
	public static boolean importDataFromCSV(String saveDirPath,String fileName,Properties prop) {
        try{
        	Connection  connection = getDBConnection(prop);
        	importDataTODB(connection,(saveDirPath+fileName));
        	//createFormResponses(connection);
        }catch(Exception e){
            //e.printStackTrace();
            logger.error("Import Failed : ",e);
            return false;
        }
		return true;
    }
	
	private static Connection getDBConnection(Properties prop) throws Exception {
		Connection dbConnection = null;
		String DB_DRIVER = prop.getProperty("dataSource.driverClassName");
		String DB_CONNECTION = prop.getProperty("dataSource.url");
		String DB_USER = prop.getProperty("dataSource.username");
		String DB_PASSWORD = prop.getProperty("dataSource.password");
		Class.forName(DB_DRIVER);
		dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER,DB_PASSWORD);
		return dbConnection;
	}
	
	private static void importDataTODB(Connection connection,String filePath) throws SQLException, IOException{
		FileInputStream fileInputStream = new FileInputStream(filePath);
		CopyManager copyManager = new CopyManager((BaseConnection) connection);
		long rowsImported = copyManager.copyIn("COPY target_formresponse FROM STDIN WITH (FORMAT CSV)", fileInputStream);
		logger.info("Rows Imported = "+rowsImported);
	}

	private static void createFormResponses(Connection  connection){
		ApplicationContext context = new ClassPathXmlApplicationContext("ctms-config.xml");
	}
	
}
