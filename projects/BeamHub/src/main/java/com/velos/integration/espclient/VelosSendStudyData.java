package com.velos.integration.espclient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.velos.integration.dao.MessageDAO;
import com.velos.integration.mapping.EndpointKeys;
import com.velos.integration.mapping.ProtocolKeys;
import com.velos.integration.mapping.VelosKeys;
import com.velos.integration.notifications.EmailNotification;
import com.velos.integration.util.CodelstsCache;
import com.velos.integration.util.PropertiesUtil;
import com.velos.services.BudgetDetail;
import com.velos.services.MilestoneVDAPojo;
import com.velos.services.Code;
import com.velos.services.Study;
import com.velos.services.StudyCalendar;
import com.velos.services.StudyVersion;

public class VelosSendStudyData {

	public static Logger logger = Logger.getLogger(VelosSendStudyData.class);

	public EmailNotification emailNotification;

	private VelosEspClientCamel camelClient;

	private MessageDAO messageDao;

	private PropertiesUtil propertiesUtil;

	public PropertiesUtil getPropertiesUtil() {
		return propertiesUtil;
	}

	public void setPropertiesUtil(PropertiesUtil propertiesUtil) {
		this.propertiesUtil = propertiesUtil;
	}

	public EmailNotification getEmailNotification() {
		return emailNotification;
	}

	public void setEmailNotification(EmailNotification emailNotification) {
		this.emailNotification = emailNotification;
	}


	public VelosEspClientCamel getCamelClient() {
		return camelClient;
	}

	public void setCamelClient(VelosEspClientCamel camelClient) {
		this.camelClient = camelClient;
	}

	public MessageDAO getMessageDao() {
		return messageDao;
	}

	public void setMessageDao(MessageDAO messageDao) {
		this.messageDao = messageDao;
	}

	private CodelstsCache codelstsCache;
	
	public CodelstsCache getCodelstsCache() {
		return codelstsCache;
	}

	public void setCodelstsCache(CodelstsCache codelstsCache) {
		this.codelstsCache = codelstsCache;
	}

	public void sendStudyPackage(String studyNumber, String studyStatus,String packageType) {

		Properties prop = propertiesUtil.getAllProperties();
		String broadCastingSite = "";
		if (prop.containsKey("BroadCastingSite") && !"".equals(prop.getProperty("BroadCastingSite").trim())) {
			broadCastingSite = prop.getProperty("BroadCastingSite").trim();
			logger.info("BroadCasting site = "+broadCastingSite);
		}
		
		Map<String, Object> codelstMap = codelstsCache.getParticipatingCodelsts();
		logger.info("Codelsts From Cache = "+codelstMap);
		
		if(!"".equals(broadCastingSite) && broadCastingSite!=null && !"null".equals(broadCastingSite)){
			String endpoint = EndpointKeys.CTXpress.toString();
			Map<VelosKeys, Object> requestMap = new HashMap<VelosKeys, Object>();
			requestMap.put(EndpointKeys.Endpoint, endpoint);
			requestMap.put(ProtocolKeys.StudyNumber, studyNumber);
			requestMap.put(ProtocolKeys.StudyStatus, studyStatus);
			requestMap.put(ProtocolKeys.BroadCastingSite, broadCastingSite);
			requestMap.put(ProtocolKeys.PropObj, prop);
			requestMap.put(ProtocolKeys.PackageType, packageType);
			requestMap.put(ProtocolKeys.ParticipatingCodelsts, codelstMap);

			Map<VelosKeys, Object> studyDataMap = camelClient.handleRequest(
					VelosEspMethods.StudyGetStudy, requestMap);

			if (studyDataMap.get(ProtocolKeys.FaultString) != null) {
				//throw Exception
				@SuppressWarnings("unchecked")
				List<String> errorList = (List<String>) studyDataMap.get(ProtocolKeys.ErrorList);
				logger.error("ErrorList : "+errorList);
				for(String error : errorList){
					logger.error("Error = "+error);
				}
			}

			if (studyDataMap.get(ProtocolKeys.FaultString) == null) {
				Study study = (Study)studyDataMap.get(ProtocolKeys.StudyObj);
				String docByFname = (String) studyDataMap.get(ProtocolKeys.DocumentedByFirstName);
				String docByLname = (String) studyDataMap.get(ProtocolKeys.DocumentedByLastName);
				String docByLoginName = (String) studyDataMap.get(ProtocolKeys.DocumentedByLoginName);
				logger.info("DocumentedBy FirstName = "+docByFname);
				logger.info("DocumentedBy LastName = "+docByLname);
				logger.info("DocumentedBy LoginName = "+docByLoginName);

				requestMap.put(ProtocolKeys.DocumentedByFirstName, docByFname);
				requestMap.put(ProtocolKeys.DocumentedByLastName,docByLname);
				requestMap.put(ProtocolKeys.DocumentedByLoginName,docByLoginName);

				Map<VelosKeys, Object> emailMap = camelClient.handleRequest(VelosEspMethods.StudyGetDocumentedBy, requestMap);
				logger.info("EmailMap = "+emailMap);
				String email = "";
				if(emailMap!=null){
					email = (String) emailMap.get(ProtocolKeys.EmailID);
					requestMap.put(ProtocolKeys.EmailID,email);
				}
				logger.info("Email = "+email);

				//Get List of organizations added to a study
				@SuppressWarnings("unchecked")
				List<String> organizationList = (List<String>) studyDataMap
				.get(ProtocolKeys.OrganizationList);
				logger.info("List = " + organizationList);
				List<String>  orgList = new ArrayList<String>();
				logger.info("Organizations other than BroadCasting site : "+broadCastingSite);

				for (String orgName : organizationList) {
					if(!orgName.equals(broadCastingSite) && prop.containsKey(orgName)){
						logger.info("OrgName = " + orgName);		
						orgList.add(orgName);
					}
				}

				logger.info("OrgList Size = "+orgList.size());
				logger.info("OrgList  = "+orgList);

				//If no organizations added other than 'CTXpress'
				if(orgList==null || orgList.isEmpty()){
					//Needs to send notification to sender
					logger.info("There are no organizations added to send the study Package");
					logger.info("Email ID = "+email);
					logger.info("EMail Obj = "+emailNotification);
					String response = "There are no participating organizations added to study team to send package";
					logger.info("Request Map = "+requestMap);
					sendNotification(requestMap,response,packageType);
					logger.info("Study Number = "+requestMap.get(ProtocolKeys.StudyNumber));
					String bSite = (String)requestMap.get(ProtocolKeys.BroadCastingSite);
					String studyNum = (String)requestMap.get(ProtocolKeys.StudyNumber);
					if(packageType.equals("StudyPackage")){
						messageDao.saveMessageDetails(studyNum,bSite,"All Participating Sites", packageType,"All","Failed", response);
					}else{
						messageDao.saveMessageDetails(studyNum,bSite,"All Participating Sites", packageType,"All","Failed", response);
					}
				}

				if(orgList!=null && !orgList.isEmpty()){

					requestMap.put(ProtocolKeys.StudyObj, study);
					if(packageType.equals("StudyPackage")){
						logger.info("---------------Get Calendar Starts----------------");
						//Get Calendars from BroadCasting Site
						getCalendars(requestMap);
						logger.info("---------------Get Calendar Ends----------------");
					}else{
						logger.info("---------------Get Amendments Starts----------------");
						logger.info("---------------Get Amendments Ends----------------");
					}

					for(String org : orgList){
						logger.info("Org Name = "+org);
						requestMap.put(ProtocolKeys.OrganizationName,org);
						logger.info("Request Map Data = "+requestMap);
						if(prop.containsKey(org)){
							if(packageType.equals("StudyPackage")){
								logger.info("------Sending Pkg to org = "+org);
								sendPackage(requestMap,packageType);
							}else{
								logger.info("******Sending Amendments to org = "+org);
							}
						}else{
							logger.info("Package/Amendments sending not configured for organization : "+org);
						}
					}
				}
			}
		}else if("".equals(broadCastingSite) || broadCastingSite==null || "null".equals(broadCastingSite)){
			logger.info("BroadCasting site is not configured.Configure BroadCastingSite.");
		}
	}

	private void sendPackage(Map<VelosKeys, Object> requestMap,String packageType) {
		searchStudy(requestMap,packageType);
	}

	private void getCalendars(Map<VelosKeys, Object> requestMap){
		logger.info("Executing Calendars");
		Map<VelosKeys, Object> studyCalListMap = camelClient.handleRequest(
				VelosEspMethods.StudyCalGetStudyCalendarList, requestMap);
		//check if studyCalListMap is null or not
		if (studyCalListMap.get(ProtocolKeys.FaultString) == null) {
			List<String> calNameList = (List<String>)studyCalListMap.get(ProtocolKeys.CalendarNameList);
			logger.info("Set calendar Name list");
			requestMap.put(ProtocolKeys.CalendarNameList,calNameList);
			logger.info("calNameList = "+calNameList);
			//logger.info("calNameListSize = "+calNameList.size());
			if(calNameList!=null && !calNameList.isEmpty()){
				//call to get the calendar data
				Map<VelosKeys, Object> studyCalObjListMap = camelClient.handleRequest(VelosEspMethods.StudyCalGetStudyCalendar, requestMap);
				List<StudyCalendar> studyCalObjList = (List<StudyCalendar>)studyCalObjListMap.get(ProtocolKeys.CalObjList);
				logger.info("studyCalObjList = "+studyCalObjList);
				logger.info("Set cal obj list");
				requestMap.put(ProtocolKeys.CalObjList,studyCalObjList);
			}
		}
		logger.info("Returned Calendars");
	}

	

	//Search Study
	private void searchStudy(Map<VelosKeys, Object> requestMap,String packageType){
		Map<VelosKeys, Object> searchOrgMap = camelClient.handleRequest(VelosEspMethods.StudySearchOrgStudy, requestMap);
		logger.info("SearchOrgMap = "+searchOrgMap);
		if(searchOrgMap!=null && searchOrgMap.get(ProtocolKeys.StudyAlreadyExists) != null){
			logger.info("Error = "+searchOrgMap.get(ProtocolKeys.StudyAlreadyExists));
			//			logger.info("Email = "+requestMap.get(ProtocolKeys.EmailID));
			if(packageType.equals("Amendments")){
				String partSiteStudyNumber = (String)searchOrgMap.get(ProtocolKeys.ParticipatingSite_StudyNumber);
				requestMap.put(ProtocolKeys.ParticipatingSite_StudyNumber, partSiteStudyNumber);
				
			}
		}else if(searchOrgMap!=null && searchOrgMap.get(ProtocolKeys.STUDY_NOT_FOUND) != null){
			logger.info("Error = "+searchOrgMap.get(ProtocolKeys.STUDY_NOT_FOUND));
			if(packageType.equals("StudyPackage")){
				createStudy(requestMap);
			}
			else{
				logger.info("Study Not found.Cannot Send Amendments");
			}
		}
	}

	private void createStudy(Map<VelosKeys, Object> requestMap){

		Map<VelosKeys, Object> createOrgMap = camelClient.handleRequest(VelosEspMethods.StudyCreateOrgStudy, requestMap);

		logger.info("createOrgMap = "+createOrgMap);

		if(createOrgMap !=null &&  createOrgMap.get(ProtocolKeys.FaultString) == null ){		
			String partSiteStudyNumber = (String)createOrgMap.get(ProtocolKeys.ParticipatingSite_StudyNumber);
			logger.info("ParticipatingSite Study Number = "+partSiteStudyNumber);
			if(partSiteStudyNumber!=null && !"".equals(partSiteStudyNumber)){
				requestMap.put(ProtocolKeys.ParticipatingSite_StudyNumber, partSiteStudyNumber);
				logger.info("Added Participating Site Number to requestMap");
			}
			if (createOrgMap.get(ProtocolKeys.FaultString) != null) {
				//Send email notification with errors
				logger.error("Study Creation Failed");
				//logger.info("Email = "+requestMap.get(ProtocolKeys.EmailID));

			}
			else if(createOrgMap.get(ProtocolKeys.FaultString) == null && createOrgMap.get(ProtocolKeys.STUDY_CREATED)!=null){
				logger.info("Study created and sending calendars");
				List<StudyCalendar> studyCalObjList = (List<StudyCalendar>)requestMap.get(ProtocolKeys.CalObjList);
				logger.info("studyCalObjList = "+studyCalObjList);
				if(studyCalObjList!=null && !studyCalObjList.isEmpty()){
					createCalendars(requestMap);
				}else{
					logger.info("No Calendars added to study");
				}
			}
		}else{
			logger.info(createOrgMap);
			logger.info("Study creation failed due to other issues ===>"+requestMap.get(ProtocolKeys.StudyNumber));
		}
	}


	//Create Study Components
	private void createCalendars(Map<VelosKeys, Object> requestMap){
		camelClient.handleRequest(VelosEspMethods.StudyCalCreateStudyCalendar, requestMap);
	}


	private void sendNotification(Map<VelosKeys, Object> requestMap,String status,String packageType){
		logger.info("Calling Notification");
		List<String> errorList = new ArrayList<String>();
		errorList.add(status);
		if(packageType.equals("StudyPackage")){
			emailNotification.sendNotification(requestMap,"Study Package fails for Study Number : ","All Participating Sites",packageType,"All","Failed",errorList);
		}else{
			emailNotification.sendNotification(requestMap,"Study Amendments fails for Study Number : ","All Participating Sites",packageType,"All","Failed",errorList);
		}
	}

}