package com.velos.integration.espclient;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import com.velos.services.OperationException_Exception;

@WebService(targetNamespace = "http://velos.com/services/", name = "FormResponseSEI")
public interface VelosEspCreateStudyPatientFormResponse {
    @WebResult(name = "Response", targetNamespace = "")
    @RequestWrapper(localName = "createStudyPatientFormResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.CreateStudyPatientFormResponse")
    @WebMethod
    @ResponseWrapper(localName = "createStudyPatientFormResponseResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.CreateStudyPatientFormResponseResponse")
    public com.velos.services.ResponseHolder createStudyPatientFormResponse(
        @WebParam(name = "StudyPatientFormResponse", targetNamespace = "")
        com.velos.services.StudyPatientFormResponse studyPatientFormResponse
    ) throws OperationException_Exception;
}
