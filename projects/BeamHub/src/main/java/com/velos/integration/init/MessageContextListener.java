package com.velos.integration.init;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;
import org.quartz.Scheduler;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

public class MessageContextListener implements ServletContextListener {
	
private static Logger logger = Logger.getLogger(MessageContextListener.class.getName());
	
	private ApplicationContext context1;

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		logger.info("ServletContextListener destroyed");
		try
        {
            logger.info("Context is : " + context1);
            Scheduler scheduler1 = (Scheduler) ((AbstractApplicationContext) context1).getBean("quartzSchedulerFactory");                      
            logger.info("Scheduler is : " + scheduler1);
            scheduler1.shutdown(true);
            logger.info("Started scheduler1 shutdown: ");
            // Sleep for a bit so that we don't get any errors
            Thread.sleep(10000); 
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
		logger.info("Scheduler Shutdown complete");
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		logger.debug("ServletContextListener started");
		context1 = new ClassPathXmlApplicationContext("sch-config.xml");
	}

}
