package com.velos.integration.espclient;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import com.velos.services.OperationException_Exception;

@WebService(targetNamespace = "http://velos.com/services/", name = "StudySEI")
public interface VelosEspGetStudyEndpoint {
	
	@WebResult(name = "Study", targetNamespace = "")
    @RequestWrapper(localName = "getStudy", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetStudy")
    @WebMethod
    @ResponseWrapper(localName = "getStudyResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetStudyResponse")
    public com.velos.services.Study getStudy(
        @WebParam(name = "StudyIdentifier", targetNamespace = "")
        com.velos.services.StudyIdentifier studyIdentifier
    ) throws OperationException_Exception;

}
