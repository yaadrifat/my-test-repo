package com.velos.integration.espclient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.velos.integration.dao.MessageDAO;
import com.velos.integration.incomingmessage.bean.IncomingMessageBean;
import com.velos.integration.mapping.EndpointKeys;
import com.velos.integration.mapping.ProtocolKeys;
import com.velos.integration.mapping.VelosKeys;
import com.velos.integration.notifications.EmailNotification;
import com.velos.integration.util.CodelstsCache;
import com.velos.integration.util.PropertiesUtil;
import com.velos.services.BudgetDetail;
import com.velos.services.MilestoneVDAPojo;
import com.velos.services.Code;
import com.velos.services.Study;
import com.velos.services.StudyCalendar;
import com.velos.services.StudyVersion;

public class VelosSendFormRespose {

	public static Logger logger = Logger.getLogger(VelosSendFormRespose.class);

	public EmailNotification emailNotification;

	private VelosEspClientCamel camelClient;

	private MessageDAO messageDao;

	private PropertiesUtil propertiesUtil;

	public PropertiesUtil getPropertiesUtil() {
		return propertiesUtil;
	}

	public void setPropertiesUtil(PropertiesUtil propertiesUtil) {
		this.propertiesUtil = propertiesUtil;
	}

	public EmailNotification getEmailNotification() {
		return emailNotification;
	}

	public void setEmailNotification(EmailNotification emailNotification) {
		this.emailNotification = emailNotification;
	}


	public VelosEspClientCamel getCamelClient() {
		return camelClient;
	}

	public void setCamelClient(VelosEspClientCamel camelClient) {
		this.camelClient = camelClient;
	}

	public MessageDAO getMessageDao() {
		return messageDao;
	}

	public void setMessageDao(MessageDAO messageDao) {
		this.messageDao = messageDao;
	}

	private CodelstsCache codelstsCache;
	
	public CodelstsCache getCodelstsCache() {
		return codelstsCache;
	}

	public void setCodelstsCache(CodelstsCache codelstsCache) {
		this.codelstsCache = codelstsCache;
	}

	public void sendFormResponse(String formOID,String formResponseOID) {

		Properties prop = propertiesUtil.getAllProperties();
		String patOrg = "";
		if (prop.containsKey("PatientOrganization") && !"".equals(prop.getProperty("PatientOrganization").trim())) {
			patOrg = prop.getProperty("PatientOrganization").trim();
			logger.info("PatientOrganization = "+patOrg);
		}else if("".equals(patOrg) || patOrg==null || "null".equals(patOrg)){
			logger.info("PatientOrganization is not configured.Configure PatientOrganization.");
			//return "PatientOrganization is not configured.Configure PatientOrganization";
		}
		
		Map<String, Object> codelstMap = codelstsCache.getParticipatingCodelsts();
		logger.info("Codelsts From Cache = "+codelstMap);
		
			String endpoint = EndpointKeys.CTXpress.toString();
			Map<VelosKeys, Object> requestMap1 = new HashMap<VelosKeys, Object>();
			requestMap1.put(EndpointKeys.Endpoint, endpoint);
			requestMap1.put(ProtocolKeys.SiteName, patOrg);
			requestMap1.put(ProtocolKeys.PropObj, prop);
			requestMap1.put(ProtocolKeys.ParticipatingCodelsts, codelstMap);
			requestMap1.put(ProtocolKeys.FormOID, formOID);
			requestMap1.put(ProtocolKeys.FormResponseOID, formResponseOID);

			//camelClient.handleRequest(VelosEspMethods.SendFormResponse, requestMap);
			camelClient.sendFormResponse(requestMap1);
			//return status;
	}
	
	
	public void sendFormResponseToeRes(String formOID,String formResponseOID,String inXml) {

		Properties prop = propertiesUtil.getAllProperties();
		String patOrg = "";
		if (prop.containsKey("PatientOrganization") && !"".equals(prop.getProperty("PatientOrganization").trim())) {
			patOrg = prop.getProperty("PatientOrganization").trim();
			logger.info("PatientOrganization = "+patOrg);
		}else if("".equals(patOrg) || patOrg==null || "null".equals(patOrg)){
			logger.info("PatientOrganization is not configured.Configure PatientOrganization.");
			//return "PatientOrganization is not configured.Configure PatientOrganization";
		}
		
		Map<String, Object> codelstMap = codelstsCache.getParticipatingCodelsts();
		logger.info("Codelsts From Cache = "+codelstMap);
		
			String endpoint = EndpointKeys.CTXpress.toString();
			Map<VelosKeys, Object> requestMap2 = new HashMap<VelosKeys, Object>();
			requestMap2.put(EndpointKeys.Endpoint, endpoint);
			requestMap2.put(ProtocolKeys.SiteName, patOrg);
			requestMap2.put(ProtocolKeys.PropObj, prop);
			requestMap2.put(ProtocolKeys.ParticipatingCodelsts, codelstMap);
			requestMap2.put(ProtocolKeys.FormOID, formOID);
			requestMap2.put(ProtocolKeys.FormResponseOID, formResponseOID);

			//camelClient.handleRequest(VelosEspMethods.SendFormResponse, requestMap);
			camelClient.sendFormResponseToeRes(requestMap2,inXml);
			//return status;
	}
	
	public void createFormsIneRes(List<IncomingMessageBean> incomingMessages){
		
		Properties prop = propertiesUtil.getAllProperties();
		String patOrg = "";
		if (prop.containsKey("Target_PatientOrganization") && !"".equals(prop.getProperty("Target_PatientOrganization").trim())) {
			patOrg = prop.getProperty("Target_PatientOrganization").trim();
			logger.info("PatientOrganization = "+patOrg);
		}else if("".equals(patOrg) || patOrg==null || "null".equals(patOrg)){
			logger.info("PatientOrganization is not configured.Configure PatientOrganization.");
			//return "PatientOrganization is not configured.Configure PatientOrganization";
		}
		
		String endpoint = EndpointKeys.CTXpress.toString();
		Map<VelosKeys, Object> requestMap3 = new HashMap<VelosKeys, Object>();
		requestMap3.put(EndpointKeys.Endpoint, endpoint);
		requestMap3.put(ProtocolKeys.SiteName, patOrg);
		camelClient.createFormresponses(requestMap3,incomingMessages);
	}
	
	
	private void sendNotification(Map<VelosKeys, Object> requestMap,String status,String packageType){
		logger.info("Calling Notification");
		List<String> errorList = new ArrayList<String>();
		errorList.add(status);
		if(packageType.equals("StudyPackage")){
			emailNotification.sendNotification(requestMap,"Study Package fails for Study Number : ","All Participating Sites",packageType,"All","Failed",errorList);
		}else{
			emailNotification.sendNotification(requestMap,"Study Amendments fails for Study Number : ","All Participating Sites",packageType,"All","Failed",errorList);
		}
	}

}