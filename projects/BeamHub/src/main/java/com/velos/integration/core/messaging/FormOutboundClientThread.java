package com.velos.integration.core.messaging;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Properties;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.jms.TopicSubscriber;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class FormOutboundClientThread implements Runnable {

	private static Logger logger = Logger
			.getLogger(FormOutboundClientThread.class);

	private ApplicationContext context;

	private FormOutboundClient client;

	public void terminate() {

		if (client != null) {
			logger.info("calling terminate");
			client.terminate();
		}

		if (context != null) {
			((AbstractApplicationContext) context).close();
			logger.info("Form Application Context Closed");
		}
	}

	public void run() {
		logger.info("Inside run method : ");
		/*
		 * try { Thread.sleep(6000); } catch (InterruptedException e1) { // TODO
		 * Auto-generated catch block e1.printStackTrace(); }
		 * logger.info("After sleeping 6s loading ctms-config.xml");
		 */
		context = new ClassPathXmlApplicationContext("ctms-config.xml");
		// ApplicationContext context =
		// ApplicationContextProvider.getApplicationContext();
		logger.info("Context = " + context);
		client = (FormOutboundClient) context.getBean("formoutboundClient");
		logger.info("FormOutboundClient Bean = " + client);
		logger.info("FormOutboundClient");
		try {
			client.run();
		} catch (Exception e) {
			logger.info("error : " + e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
