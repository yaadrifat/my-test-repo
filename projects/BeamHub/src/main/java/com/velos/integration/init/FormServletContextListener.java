package com.velos.integration.init;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import com.velos.integration.core.messaging.FormOutboundClientThread;

public class FormServletContextListener implements ServletContextListener {

	private static Logger logger = Logger
			.getLogger(FormServletContextListener.class.getName());

	private Thread thread;

	private FormOutboundClientThread runnable;

	public void contextDestroyed(ServletContextEvent arg0) {
		logger.info("FormTopic Context Destroyed");
		// logger.info("Thread count = "+Thread.activeCount());
		logger.info("Stopping thread: " + thread.getName());
		if (runnable != null) {
			runnable.terminate();
		}
		if (thread != null) {

			try {
				thread.join();
				logger.info("Thread " + thread.getName()
						+ " successfully stopped.");
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// logger.info("Thread count = "+Thread.activeCount());
	}

	public void contextInitialized(ServletContextEvent arg0) {
		logger.info("Initiating Core");
		outBoundMessaging();
	}

	void outBoundMessaging() {
		// (new Thread(new OutboundClientThread())).start();
		// logger.info("Thread count = "+Thread.activeCount());
		runnable = new FormOutboundClientThread();
		thread = new Thread(runnable);
		thread.setName("FormTopicThread");
		logger.info("Starting thread: " + thread.getName());
		thread.start();
		logger.info("FormTopic Background process successfully started.");
	}

}
