package com.velos.integration.forms.bean;

public class FormsFieldsBean {
	
	private String formName;
	private String fieldName;
	private String fieldId;
	private String fieldOid;
	private String dataDictionaryName;
	private String controlType;
	
	public String getFormName() {
		return formName;
	}
	public void setFormName(String formName) {
		this.formName = formName;
	}
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public String getFieldId() {
		return fieldId;
	}
	public void setFieldId(String fieldId) {
		this.fieldId = fieldId;
	}
	public String getFieldOid() {
		return fieldOid;
	}
	public void setFieldOid(String fieldOid) {
		this.fieldOid = fieldOid;
	}
	public String getDataDictionaryName() {
		return dataDictionaryName;
	}
	public void setDataDictionaryName(String dataDictionaryName) {
		this.dataDictionaryName = dataDictionaryName;
	}
	public String getControlType() {
		return controlType;
	}
	public void setControlType(String controlType) {
		this.controlType = controlType;
	}


}
