package com.velos.integration.incomingmessage.scheduler;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/*
 * Quartz scheduler is used to schedule cron job to update coverage.
 */
public class RunIncomingMsgSchdlr extends QuartzJobBean {

	private static Logger logger = Logger.getLogger(RunIncomingMsgSchdlr.class
			.getName());
	private RunIncomingMsgTask runIncomingMsgTask;

	public RunIncomingMsgTask getRunIncomingMsgTask() {
		return runIncomingMsgTask;
	}

	public void setRunIncomingMsgTask(RunIncomingMsgTask runIncomingMsgTask) {
		this.runIncomingMsgTask = runIncomingMsgTask;
	}

	@Override
	protected void executeInternal(JobExecutionContext arg0)
			throws JobExecutionException {
		try {
			this.getRunIncomingMsgTask().runTask();
		} catch (Exception e) {
			logger.info(" Exception caught in executeInternal method of RunIncomingMsgSchdlr "
					+ e.getMessage());
		}
	}

}
