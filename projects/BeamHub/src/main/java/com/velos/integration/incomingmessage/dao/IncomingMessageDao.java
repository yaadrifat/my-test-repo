package com.velos.integration.incomingmessage.dao;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;

import com.velos.integration.incomingmessage.bean.IncomingMessageBean;
import com.velos.services.FieldIdentifier;
import com.velos.services.FormFieldResponse;
import com.velos.services.FormFieldResponses;
import com.velos.services.StudyPatientFormResponse;

public class IncomingMessageDao {
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplate;

	private static Logger logger = Logger.getLogger(IncomingMessageDao.class);

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	public List<IncomingMessageBean> getIncomingMessages() {
		List<IncomingMessageBean> incomingMessages = new ArrayList<IncomingMessageBean>();
		try {
			String sql = "select * from target_formresponse where status ='Exporting to CSV'";
			List<Map> rows = jdbcTemplate.queryForList(sql);
			for (Map row : rows) {
				IncomingMessageBean incomingMessage = new IncomingMessageBean();
				incomingMessage.setPrimaryKey(((Integer) row
						.get("pk_msg_audit")).intValue());
				incomingMessage.setStudyNumber((String) row.get("studynumber"));
				incomingMessage.setPatientId((String) row.get("patient_id"));
				incomingMessage.setFormName((String) row.get("formname"));
				byte[] reponse = ((byte[]) row.get("formresponsedata"));
	            ByteArrayInputStream bais = new ByteArrayInputStream(reponse);
	            ObjectInputStream ins = new ObjectInputStream(bais);
	            StudyPatientFormResponse spfr =(StudyPatientFormResponse)ins.readObject();
	            incomingMessage.setFormRes(spfr);
				incomingMessage.setXmlMessage((String) row.get("received_message"));
				incomingMessage.setStatus((String) row.get("status"));
				incomingMessages.add(incomingMessage);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return incomingMessages;
	}

}
