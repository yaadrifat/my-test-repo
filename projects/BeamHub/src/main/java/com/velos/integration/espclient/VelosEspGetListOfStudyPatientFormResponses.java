package com.velos.integration.espclient;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;
import com.velos.services.OperationException_Exception;

@WebService(targetNamespace = "http://velos.com/services/", name = "FormResponseSEI")
public interface VelosEspGetListOfStudyPatientFormResponses {
	@WebResult(name = "StudyPatientFormResponse", targetNamespace = "")
    @RequestWrapper(localName = "getListOfStudyPatientFormResponses", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetListOfStudyPatientFormResponses")
    @WebMethod
    @ResponseWrapper(localName = "getListOfStudyPatientFormResponsesResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetListOfStudyPatientFormResponsesResponse")
    public com.velos.services.StudyPatientFormResponses getListOfStudyPatientFormResponses(
        @WebParam(name = "FormIdentifier", targetNamespace = "")
        com.velos.services.FormIdentifier formIdentifier,
        @WebParam(name = "PatientIdentifier", targetNamespace = "")
        com.velos.services.PatientIdentifier patientIdentifier,
        @WebParam(name = "StudyIdentifier", targetNamespace = "")
        com.velos.services.StudyIdentifier studyIdentifier,
        @WebParam(name = "PageNumber", targetNamespace = "")
        int pageNumber,
        @WebParam(name = "PageSize", targetNamespace = "")
        int pageSize
    ) throws OperationException_Exception;

}
