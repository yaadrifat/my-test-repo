package com.velos.integration.incomingmessage.scheduler;

import java.util.List;

import org.apache.log4j.Logger;

import com.velos.integration.espclient.VelosSendFormRespose;
import com.velos.integration.espclient.VelosSendStudyData;
import com.velos.integration.incomingmessage.bean.IncomingMessageBean;
import com.velos.integration.incomingmessage.dao.IncomingMessageDao;

public class RunIncomingMsgTask {

	private IncomingMessageDao incomingMessageDao;

	public IncomingMessageDao getIncomingMessageDao() {
		return incomingMessageDao;
	}

	public void setIncomingMessageDao(IncomingMessageDao incomingMessageDao) {
		this.incomingMessageDao = incomingMessageDao;
	}

	public static Logger getLogger() {
		return logger;
	}

	public static void setLogger(Logger logger) {
		RunIncomingMsgTask.logger = logger;
	}

	private VelosSendFormRespose velosSendFormResponse;
	
	
	
	private static Logger logger = Logger.getLogger(RunIncomingMsgTask.class
			.getName());

	public void runTask() {
		try {
			List<IncomingMessageBean> incomingMessages = this
					.getIncomingMessageDao().getIncomingMessages();
			if(incomingMessages!=null && !incomingMessages.isEmpty()){
				velosSendFormResponse.createFormsIneRes(incomingMessages);
			}else if(incomingMessages.isEmpty()){
				//logger.info("No data available to Process");
			}
		} catch (Exception e) {
			logger.info("Exception caught in runTask() method of RunIncomingMsgTask : "
					+ e.getMessage());
		}
	}

	public VelosSendFormRespose getVelosSendFormResponse() {
		return velosSendFormResponse;
	}

	public void setVelosSendFormResponse(VelosSendFormRespose velosSendFormResponse) {
		this.velosSendFormResponse = velosSendFormResponse;
	}

}
