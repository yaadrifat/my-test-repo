
CREATE OR REPLACE VIEW adt_interface_view AS 
 SELECT vg.mrn AS "DOCID",
    vg.pk_vgdb_message_adt AS "PK_ID",
        CASE
            WHEN date_part('year'::text, age(vg.dob::date::timestamp with time zone)) >= 1::double precision THEN concat(date_part('year'::text, age(vg.dob::date::timestamp with time zone)), ' years')
            WHEN date_part('year'::text, age(vg.dob::date::timestamp with time zone)) < 1::double precision AND date_part('month'::text, age(vg.dob::date::timestamp with time zone)) > 1::double precision THEN concat(date_part('month'::text, age(vg.dob::date::timestamp with time zone)), ' months')
            WHEN date_part('year'::text, age(vg.dob::date::timestamp with time zone)) < 1::double precision AND date_part('month'::text, age(vg.dob::date::timestamp with time zone)) < 1::double precision THEN concat(date_part('day'::text, age(vg.dob::date::timestamp with time zone)), ' days')
            ELSE NULL::text
        END AS "AGE_DTL",
    date_part('year'::text, age(vg.dob::date::timestamp with time zone)) AS "AGE",
    concat(vg.firstname::text, vg.lastname::text, vg.middlename::text) AS "FULLNAME",
    vg.mrn AS "PERSON_CODE",

    vg.lastname AS "LASTNAME",
    vg.firstname AS "FIRSTNAME",
    vg.middlename AS "MIDDLENAME",
    vg.gender_eresearch AS "GENDER",
    vg.race_eresearch AS "RACE",
    vg.address1 AS "ADDRESS1",
    vg.address2 AS "ADDRESS2",
    vg.city AS "CITY",
    vg.state AS "STATE",
    vg.zipcode AS "ZIPCODE",
    vg.country AS "COUNTRY",
    vg.county AS "COUNTY",
    vg.homephoneno AS "HOME_PHONE",
    vg.workphoneno AS "WORK_PHONE",
    vg.ethnic_eresearch AS "ETHNICITY",
    vg.maritialstatus_eresearch AS "MARITAL_STATUS",
    vg.messagedate AS "CURRENTDATE",
    vg.dob::date AS "DOB",
    vg.emailid  AS "EMAIL_ID",
    vg.deathdate::date AS "DEATH_DATE"
   FROM vgdb_message_adt vg
     JOIN ( SELECT vgdb_message_adt.mrn,
            max(vgdb_message_adt.pk_vgdb_message_adt) AS maxpktableid
           FROM vgdb_message_adt
          GROUP BY vgdb_message_adt.mrn) groupedtt ON vg.mrn::text = groupedtt.mrn::text AND vg.pk_vgdb_message_adt = groupedtt.maxpktableid;

