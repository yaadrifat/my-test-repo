
----  vgdb_message table ----
insert into vgdb_message (pk_msg , message_type , message_data_table , email_notification , realtime_notification) values(nextval('seq_vgdb_message'),'ADT','vgdb_message_ADT',6,1);
insert into vgdb_message (pk_msg , message_type , message_data_table , email_notification , realtime_notification) values(nextval('seq_vgdb_message'),'LAB','vgdb_message_LAB',6,1);


----vgdb_message_segment-----
insert into vgdb_message_segment (pk_msg_sgmnt, message_type, segment_name, is_mandatory, repeated, repeated_segment_table, segment_sequence, segment_type) values(nextval('SEQ_VGDB_MESSAGE_SEGMENT'),'ADT','MSH',1,1,null,null,1);
insert into vgdb_message_segment (pk_msg_sgmnt, message_type, segment_name, is_mandatory, repeated, repeated_segment_table, segment_sequence, segment_type) values(nextval('SEQ_VGDB_MESSAGE_SEGMENT'),'ADT','PID',1,1,null,null,1);
insert into vgdb_message_segment (pk_msg_sgmnt, message_type, segment_name, is_mandatory, repeated, repeated_segment_table, segment_sequence, segment_type) values(nextval('SEQ_VGDB_MESSAGE_SEGMENT'),'ADT','EVN',0,1,null,null,1);
insert into vgdb_message_segment (pk_msg_sgmnt, message_type, segment_name, is_mandatory, repeated, repeated_segment_table, segment_sequence, segment_type) values(nextval('SEQ_VGDB_MESSAGE_SEGMENT'),'ADT','IN1',0,1,null,null,1);
insert into vgdb_message_segment (pk_msg_sgmnt, message_type, segment_name, is_mandatory, repeated, repeated_segment_table, segment_sequence, segment_type) values(nextval('SEQ_VGDB_MESSAGE_SEGMENT'),'ADT','IN2',0,1,null,null,1);
insert into vgdb_message_segment (pk_msg_sgmnt, message_type, segment_name, is_mandatory, repeated, repeated_segment_table, segment_sequence, segment_type) values(nextval('SEQ_VGDB_MESSAGE_SEGMENT'),'ADT','PV1',0,1,null,null,1);
insert into vgdb_message_segment (pk_msg_sgmnt, message_type, segment_name, is_mandatory, repeated, repeated_segment_table, segment_sequence, segment_type) values(nextval('SEQ_VGDB_MESSAGE_SEGMENT'),'ADT','PV2',0,1,null,null,1);
insert into vgdb_message_segment (pk_msg_sgmnt, message_type, segment_name, is_mandatory, repeated, repeated_segment_table, segment_sequence, segment_type) values(nextval('SEQ_VGDB_MESSAGE_SEGMENT'),'ADT','ZAH',0,1,null,null,1);




---- vgdb_message_response ----

insert into vgdb_message_response values(nextval('SEQ_VGDB_MESSAGE_RESPONSE'),'ACK','MSH','Message Type','/MSH-9-1','0','ACK');
insert into vgdb_message_response values(nextval('SEQ_VGDB_MESSAGE_RESPONSE'),'ACK','MSH','Sending Application','/MSH-3-1','2','/MSH-5-1');
insert into vgdb_message_response values(nextval('SEQ_VGDB_MESSAGE_RESPONSE'),'ACK','MSH','Sending Facility','/MSH-4-1','2','/MSH-6-1');
insert into vgdb_message_response values(nextval('SEQ_VGDB_MESSAGE_RESPONSE'),'ACK','MSH','Receiving Application','/MSH-5-1','2','/MSH-3-1');
insert into vgdb_message_response values(nextval('SEQ_VGDB_MESSAGE_RESPONSE'),'ACK','MSH','Receiving Facility','/MSH-6-1','2','/MSH-4-1');
insert into vgdb_message_response values(nextval('SEQ_VGDB_MESSAGE_RESPONSE'),'ACK','MSH','MessageID','/MSH-10-1','1',null);
insert into vgdb_message_response values(nextval('SEQ_VGDB_MESSAGE_RESPONSE'),'ACK','MSH','VersionID','/MSH-12-1','2','/MSH-12-1');
insert into vgdb_message_response values(nextval('SEQ_VGDB_MESSAGE_RESPONSE'),'ACK','MSA','AcknowledgementCode','/MSA-1-1','1',null);
insert into vgdb_message_response values(nextval('SEQ_VGDB_MESSAGE_RESPONSE'),'ACK','MSA','InputMessageID','/MSA-2-1','2','/MSH-10-1');
insert into vgdb_message_response values(nextval('SEQ_VGDB_MESSAGE_RESPONSE'),'ACK','MSA','AcknowledgementText','/MSA-3-1','1',null); 


------vgdb_message_field_mapping Values----------

insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'gender_eResearch','F','female');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'gender_eResearch','M','male');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'gender_eResearch','U','unknown');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'maritalstatus_eResearch','4','divorced');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'maritalstatus_eResearch','2','married');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'maritalstatus_eResearch','1','single');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'maritalstatus_eResearch','5','separated');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'maritalstatus_eResearch','6','other');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'maritalstatus_eResearch','3','widowed');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'ethnic_eResearch','N','nonhispanic');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'ethnic_eResearch','Y','hispanic');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'ethnic_eResearch','Null','unknown');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'race_eResearch','1','race_white');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'race_eResearch','2','race_blkafr');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'race_eResearch','3','race_asian');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'race_eResearch','4','race_indala');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'race_eResearch','5','race_unknown');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'race_eResearch','6','race_unknown');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'race_eResearch','7','race_notrep');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'race_eResearch','8','race_hwnisl');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'race_eResearch','9','race_unknown');

insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'education_eResearch','1','lesshigh_w/oGED');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'education_eResearch','2','lesshigh_w/oGED');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'education_eResearch','3','lesshigh_w/oGED');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'education_eResearch','4','high_ged');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'education_eResearch','5','some_college');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'education_eResearch','6','college_degree');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'education_eResearch','7','unknown');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'education_eResearch','8','unknown');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'education_eResearch','9','unknown');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'education_eResearch','99','unknown');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'employment_eResearch','1','fullTime');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'employment_eResearch','2','partTime');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'employment_eResearch','3','unemployed');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'employment_eResearch','4','selfEmployed');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'employment_eResearch','5','retired');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'employment_eResearch','6','active_military');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'employment_eResearch','7','other');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'employment_eResearch','8','other');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'employment_eResearch','9','other');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'employment_eResearch','10','other');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'employment_eResearch','11','other');

insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','1','cuban');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','2','Dominican');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','3','Mexican,Mexican');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','4','Mex_Americ');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','5','Americ_Chicano');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','6','Mexicano');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','7','Chicano');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','8','La Raza');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','9','Mex_Amer_Indian');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','10','Puerto Rican');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','11','Salvadoran');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','12','Central America');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','13','Costa Rican');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','14','Nicaraguan');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','15','Panamanian');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','16','Cent_Ame_Indian');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','17','Belize');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','18','South American');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','19','Argentinean');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','20','Bolivian');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','21','Chilean');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','22','Ecuadorian');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','23','Paraguayan');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','24','Peruvian');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','25','Uruguayan');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','26','Venezuelan');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','27','Sou_Amer_Indian');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','28','Criollo');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','29','Guyana');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','30','African');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','31','Botswanan');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','32','Ethiopian');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','33','Liberian');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','34','Namibian');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','35','Nigerian');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','36','Zairean');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','37','African American');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','38','American');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','39','Asian');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','40','Bangladeshi');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','41','Bhutanese');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','42','Burmese');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','43','Hmong');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','44','Indonesian');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','45','Madagascar');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','46','Malaysian');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','47','Maldivian');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','48','Nepalese');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','49','Pakistani');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','50','Singaporean');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','51','Sri Lankan');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','52','Taiwanese');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','53','Thai');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','54','Asian Indian');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','55','Brazilian');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','56','Cambodian');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','57','CapeVerdean');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','58','Caribbean Islan');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','59','Barbadian');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','60','Dominican Islan');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','61','Jamaican');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','62','Trinidadian');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','63','Tobagoan');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','64','West Indian');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','65','Chinese');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','66','Columbian');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','67','European');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','68','English');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','69','French');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','70','German');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','71','Irish');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','72','Italian');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','73','Scottish');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','74','Filipino');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','75','Guatemalan');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','76','Haitian');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','77','Honduran');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','78','Japanese');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','79','Korean');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','80','Laotian');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','81','MidEast_N.Afric');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','82','Assyrian');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','83','Egyptian');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','84','Iranian');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','85','Iraqi');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','86','Lebanese');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','87','Palestinian');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','88','Syrian');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','89','Afghanistani');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','90','Israeli');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','91','Portuguese');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','92','Canarian');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','93','Russian');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','94','Eastern Europea');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','95','Armenian');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','96','Polish');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','97','Vietnamese');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','98','Other');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'additionalethnicity_eResearch','99','Unknown');



-----vgdb_message_fields---

insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','MSH','Delimiter','delimiter',1,'1',null,null,null,null,1,'/MSH-1');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','MSH','EventId','eventid',1,'1',null,null,null,null,5,'/MSH-9-2');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','MSH','EncodingCharacter','encodingcharacter',1,'1',null,null,null,null,4,'/MSH-2');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','MSH','SendingApplication','sendingapplication',1,'1',null,null,null,null,50,'/MSH-3-1');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','MSH','AncillaryDepartment','ancillarydepartment',1,'1',null,null,null,null,50,'/MSH-4-1');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','MSH','ReceivingApplication','receivingapplication',0,'1',null,null,null,null,50,'/MSH-5-1');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','MSH','ReceivingFacility','receivingfacility',0,'1',null,null,null,null,50,'/MSH-6-1');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','MSH','DateorTime_of_Message','datetimeofmessage',1,'1',null,null,null,null,50,'/MSH-7-1');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','MSH','MessageType','messagetype',1,'1',null,null,null,null,5,'/MSH-9-1');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','MSH','MessageControlID','messagecontrolid',1,'1',null,null,null,null,50,'/MSH-10-1');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','MSH','VersionID','versionid',1,'1',null,null,null,null,10,'/MSH-12-1');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','MRN','mrn',1,'1',null,null,null,null,50,'/PID-4-1');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','LastName','lastname',1,'1',null,null,null,null,50,'/PID-5-1');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','FirstName','firstname',0,'1',null,null,null,null,50,'/PID-5-2');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','MiddleName','middlename',0,'1',null,null,null,null,50,'/PID-5-3');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','DOB','dob',0,'1',null,null,null,null,8,'/PID-7-1');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','Gender','gender',0,'1',null,null,null,null,8,'/PID-8-1');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','PrimaryRace','primaryrace',0,'1',null,null,null,null,1,'/PID-10-1');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','MaritalStatus','maritalstatus',0,'1',null,null,null,null,8,'/PID-16-1');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','PrimaryEthnicity','primaryethnicity',1,'1',null,null,null,null,1,'/PID-22-1');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','City','city',1,'1',null,null,null,null,50,'/PID-11-3');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','Address1','address1',1,'1',null,null,null,null,50,'/PID-11-1');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','Address2','address2',0,'1',null,null,null,null,50,'/PID-11-2');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','State','state',0,'1',null,null,null,null,50,'/PID-11-4');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','ZipCode','zipcode',0,'1',null,null,null,null,25,'/PID-11-5');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','Country','country',0,'1',null,null,null,null,50,'/PID-11-6');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','County','county',0,'1',null,null,null,null,50,'/PID-11-9');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','HomePhoneNo','homephoneno',0,'1',null,null,null,null,25,'/PID-13-1');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','WorkPhoneNo','workphoneno',0,'1',null,null,null,null,25,'/PID-14-1');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','DeathDate','DeathDate',0,'1',null,null,null,null,8,'/PID-29-1');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','ZAH','EmailID','emailid',0,'1',null,null,null,null,50,'/ZAH-36-1');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','ZAH','AdditionalEthnicity','additionalethnicity',0,'1',null,null,null,null,50,'/ZAH-27');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','ZAH','EmploymentStatus','employmentstatus',0,'1',null,null,null,null,50,'/ZPI-9');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','Ethnic_eResearch','ethnic_eResearch',0,'1',null,null,'ethnic_eResearch','PrimaryEthnicity',5,null);
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','MaritalStatus_eResearch','maritalstatus_eResearch',0,'1',null,null,'maritalstatus_eResearch','MaritalStatus',5,null);
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','Gender_eResearch','gender_eResearch',0,'1',null,null,'gender_eResearch','Gender',5,null);
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','Race_eResearch','race_eResearch',0,'1',null,null,'race_eResearch','PrimaryRace',5,null);
					
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','Education_eResearch','education_eResearch',0,'1',null,null,'education_eResearch','Education',0,null);
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','EmploymentStatus_eResearch','employmentstatus_eResearch',0,'1',null,null,'employmentstatus_eResearch','EmploymentStatus',0,null);

		







