--  vgdb_message table --
create table vgdb_message (pk_msg INTEGER primary key, message_type VARCHAR(20), message_data_table VARCHAR(50), email_notification INTEGER, realtime_notification INTEGER );
COMMENT ON COLUMN vgdb_message.pk_msg IS 'Primary key for the table.';
COMMENT ON COLUMN vgdb_message.message_type IS 'This column is used to identify type of the message for which row is configured.';
COMMENT ON COLUMN vgdb_message.message_data_table IS 'This column is used to identify the primary message_data table.';
COMMENT ON COLUMN vgdb_message.email_notification IS 'This column is used to identify whether email notification for failed messages are required. 1 for AR only, 2 for AE only, 3 for AR and AE both, 4 for AR only periodic, 5 for AE only periodic, 6 for AR and AE both periodic.';
COMMENT ON COLUMN vgdb_message.realtime_notification IS 'This column is used to identify whether real-time notification are required.';

CREATE SEQUENCE  SEQ_VGDB_MESSAGE  MINVALUE 1 MAXVALUE 9999999999999999 INCREMENT BY 1 START WITH 1 CACHE 100  NO CYCLE ;

-- vgdb_message_segment --

create table vgdb_message_segment (pk_msg_sgmnt INTEGER primary key, message_type VARCHAR(20), segment_name VARCHAR(10), is_mandatory INTEGER, repeated VARCHAR(2), repeated_segment_table VARCHAR(50), segment_sequence INTEGER, segment_type INTEGER);
COMMENT ON COLUMN vgdb_message_segment.pk_msg_sgmnt IS 'Primary key for the table.';
COMMENT ON COLUMN vgdb_message_segment.message_type IS 'This column is used to identify type of the message for which segments are configured.';
COMMENT ON COLUMN vgdb_message_segment.segment_name IS 'This column is used to identify the segment name for which this row is configured.';
COMMENT ON COLUMN vgdb_message_segment.is_mandatory IS 'This column is used to identify whether the segment is mandatory or not.';
COMMENT ON COLUMN vgdb_message_segment.repeated IS 'This column is used identify how many times a segment may repeat. Integer is used for fixed number of repeats and * is used for any number of repeats.';
COMMENT ON COLUMN vgdb_message_segment.repeated_segment_table IS 'This column is used identify the table name for repeated segment. Foreign Key will be the primary Key of the main message_data table.';
COMMENT ON COLUMN vgdb_message_segment.segment_sequence IS 'This column is used to identify the sequence in which segments should occur in the message. If segments can occur in any order, this column will be null.';
COMMENT ON COLUMN vgdb_message_segment.segment_type IS 'This column is used to identify the segment type. 1 for request message, 2 for response message, 3 for batch header, 4 for batch tail';


CREATE SEQUENCE  SEQ_VGDB_MESSAGE_SEGMENT  MINVALUE 1 MAXVALUE 999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 100   NO CYCLE ;


-- vgdb_message_fields --

create table vgdb_message_fields (pk_msg_flds INTEGER primary key, message_type VARCHAR(20), segment_name VARCHAR(10), field_name VARCHAR(30),column_name VARCHAR(30), is_mandatory INTEGER, repeated VARCHAR(5), repeated_column VARCHAR(30), repeated_delimiter VARCHAR(5), mapping_type VARCHAR(30), mapping_field  VARCHAR(30), max_length INTEGER,terser_path VARCHAR(30));

COMMENT ON COLUMN vgdb_message_fields.pk_msg_flds IS 'Primary key for the table.';
COMMENT ON COLUMN vgdb_message_fields.message_type IS 'This column is used to identify type of the message for which fields are configured.';
COMMENT ON COLUMN vgdb_message_fields.segment_name IS 'This column is used to identify the segment name for which fields are configured.';
COMMENT ON COLUMN vgdb_message_fields.field_name IS 'This column is used to identify the field name for which this row is configured.';
COMMENT ON COLUMN vgdb_message_fields.column_name IS 'This column is used to identify the corresponding column in message_data table';
COMMENT ON COLUMN vgdb_message_fields.is_mandatory IS 'This column is used to identify whether the field is mandatory or not.';
COMMENT ON COLUMN vgdb_message_fields.repeated IS 'This column is used identify how many times a field may repeat. Integer is used for fixed number of repeats and * is used for any number of repeats.';
COMMENT ON COLUMN vgdb_message_fields.repeated_column IS 'This column is used identify how to save the repeated column. If a new column name is provided all the repeated values after the first instance will go in that column';
COMMENT ON COLUMN vgdb_message_fields.repeated_delimiter IS 'This column is used identify how to concatenate the repeated field, ex - comma, colon. If it is configured as null, all the repeated values will go in new columns marked as <name of repeated_column>_<sequence>';
COMMENT ON COLUMN vgdb_message_fields.mapping_type IS 'This column is used to identify whether the field has a mapping type.';
COMMENT ON COLUMN vgdb_message_fields.mapping_field IS 'This column is used to identify the corresponding field of the mapped value.';
COMMENT ON COLUMN vgdb_message_fields.max_length IS 'This column is used to identify the maximum field length for message validity. Set it to null if this validation is not required';
COMMENT ON COLUMN vgdb_message_fields.terser_path IS 'This column is used to get the field value by using the terser path';

CREATE SEQUENCE  SEQ_VGDB_MESSAGE_FIELDS  MINVALUE 1 MAXVALUE 99999999999999999 INCREMENT BY 1 START WITH 1 CACHE 100   NO CYCLE ;




--vgdb_message_audit_details--

create table vgdb_message_audit_details (pk_msg_audit INTEGER primary key, message_type VARCHAR(20), event_id VARCHAR(20), message_id VARCHAR(50), request_message 
TEXT, response_message TEXT, acknowledge_type VARCHAR(2), creator VARCHAR(30), creation_date DATE,mrn varchar(50));
COMMENT ON COLUMN vgdb_message_audit_details.pk_msg_audit IS 'Primary key for the table.';
COMMENT ON COLUMN vgdb_message_audit_details.message_type IS 'This column stores type of the message for which row will be created.';
COMMENT ON COLUMN vgdb_message_audit_details.event_id IS 'This column stores type of the event for which row will be created.';
COMMENT ON COLUMN vgdb_message_audit_details.message_id IS 'This column stores message id of the message.';
COMMENT ON COLUMN vgdb_message_audit_details.request_message IS 'This column stores the requested message.';
COMMENT ON COLUMN vgdb_message_audit_details.response_message IS 'This column stores the response message.';
COMMENT ON COLUMN vgdb_message_audit_details.acknowledge_type IS 'This column stores the Acknowledge type. It can be AA, AR or AE';
COMMENT ON COLUMN vgdb_message_audit_details.creator IS 'This column is used for Audit Trail. The Creator identifies the user who created this row';
COMMENT ON COLUMN vgdb_message_audit_details.creation_date IS 'This column is used for Audit Trail. Stores the date on which this row was created.';

CREATE SEQUENCE  SEQ_VGDB_MESSAGE_AUDIT_DETAILS  MINVALUE 1 MAXVALUE 99999999999999999 INCREMENT BY 1 START WITH 1 CACHE 100   NO CYCLE ;



--vgdb_field_mapping_values--

create table vgdb_field_mapping_values (pk_fld_map_vls INTEGER primary key, mapping_type VARCHAR(30), orignal_value VARCHAR(50), mapped_value VARCHAR(50));
COMMENT ON COLUMN vgdb_field_mapping_values.pk_fld_map_vls IS 'Primary key for the table.';
COMMENT ON COLUMN vgdb_field_mapping_values.mapping_type IS 'This column is used to identify mapping type.';
COMMENT ON COLUMN vgdb_field_mapping_values.orignal_value IS 'This column stores the orignal value of the field';
COMMENT ON COLUMN vgdb_field_mapping_values.mapped_value IS 'This column stores the mapped value of the field';

CREATE SEQUENCE  SEQ_VGDB_FIELD_MAPPING_VALUES  MINVALUE 1 MAXVALUE 999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 100   NO CYCLE ;


--vgdb_message_response--

create table vgdb_message_response (pk_msg_rsp INTEGER primary key, message_type VARCHAR(20), segment_name VARCHAR(10), field_name VARCHAR(30), tensor_field VARCHAR(1000), field_from VARCHAR(30), field_value VARCHAR(1000));
COMMENT ON COLUMN vgdb_message_response.pk_msg_rsp IS 'Primary key for the table.';
COMMENT ON COLUMN vgdb_message_response.message_type IS 'This column is used to identify type of the message for which fields are configured.';
COMMENT ON COLUMN vgdb_message_response.segment_name IS 'This column is used to identify the segment name for which fields are configured.';
COMMENT ON COLUMN vgdb_message_response.field_name IS 'This column is used to identify the field name for which this row is configured.';
COMMENT ON COLUMN vgdb_message_response.tensor_field IS 'This column is used to identify the field';
COMMENT ON COLUMN vgdb_message_response.field_from IS 'This column is used to identify whether the response field is static/dynamic/request field 0/1/2 respectively.';
COMMENT ON COLUMN vgdb_message_response.field_value IS 'This column is used to configure the value for field_from column. If field_from is static a constant value will be configured in this field which will be used for all the messages, for example - Velos. If dynamic then the value will come from code and this column will be set to null. If request and request field will be mentioned in this field, for ex - MSH-10-1)';


CREATE SEQUENCE  SEQ_VGDB_MESSAGE_RESPONSE  MINVALUE 1 MAXVALUE 9999999999999999 INCREMENT BY 1 START WITH 1 CACHE 100  NO CYCLE ;




--vgdb_message_adt--


CREATE TABLE vgdb_message_adt
(
  pk_vgdb_message_adt integer NOT NULL,
  eventId varchar(50),
  delimiter varchar(1),
  encodingcharacter varchar(4),
  sendingapplication varchar(50),
  ancillarydepartment varchar(50),
  receivingapplication varchar(50),
  receivingfacility varchar(50),
  datetimeofmessage varchar(50),
  messagetype varchar(3),
  messagecontrolid varchar(50),
  versionid varchar(10),
  mrn varchar(50),
  lastname varchar(250),
  firstname varchar(250),
  middlename varchar(250),
  dob varchar(8),
  gender varchar(50),
  race varchar(50),
  maritalstatus varchar(50),
  ethnicgroup varchar(50),
  street varchar(250),
  city varchar(50),
  state varchar(25),
  zipcode varchar(50),
  country varchar(50),
  county varchar(50),
  homephoneno varchar(25),
  workphoneno varchar(25),
  deathdate varchar(8),
  priormrn varchar(50),
  gender_eresearch varchar(50),
  gender_appb varchar(8),
  race_eresearch varchar(50),
  ethnic_eresearch varchar(50),
  maritialstatus_eresearch varchar(50),
  messagedate varchar(25),
  lastmodifydate character varying(55),
  messagestatus character varying(55),
  CONSTRAINT vgdb_message_adt_pkey PRIMARY KEY (pk_vgdb_message_adt));
 

alter table vgdb_message_adt rename street to address1 
alter table vgdb_message_adt add address2 varchar(250)

CREATE SEQUENCE  SEQ_VGDB_MESSAGE_ADT  MINVALUE 1 MAXVALUE 9999999999999999 INCREMENT BY 1 START WITH 1 CACHE 100  NO CYCLE ;

