<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title><%=MC.M_MngPat_PatSearch%><%--Manage Patients >> Patient Search*****--%></title>
	</head>
<body onload="refreshPaginator();onLoad();">
<jsp:include page="panel.jsp" flush="true"/>
<jsp:include page="ui-include.jsp" flush="true"/>

<%
	HttpSession tSessionEMR = request.getSession(true);
	int pageRightEMR = 0;
	GrpRightsJB grpRightsEMR = (GrpRightsJB) tSessionEMR
			.getValue("GRights");
	pageRightEMR = Integer.parseInt(grpRightsEMR
			.getFtrRightsByValue("MPATIENTS"));
	%>
	
	<script>
var mycounter = 0;
//km
var pageRight = '';
var type = 0;
function isAccessibleFor(pageRight,  type) {
    switch(type) {
    case 'V': return ((pageRight & 0x0004) >> 2) == 1;
    case 'E': return ((pageRight & 0x0002) >> 1) == 1;
    case 'N': return  (pageRight & 0x0001) == 1;
    }
    return false;
}

function escapeShowTitle(title) {
	title = title.replace(/'/g, "`");
    title = escape(title);
    return title;
}

function escapeShowTitleCurrentPage(title) {
	title = title.replace(/'/g, "`");
    return title;
}
function confirmBox(name){
	var paramArray = [name];
    if (confirm(getLocalizedMessageString("M_WantDel_Pat",paramArray)))/*if (confirm("Do you want to delete <%=LC.Pat_Patient%> " + name+"?" ))*****/
	return true;
    else
	return false;
    
}//km
//Added by Gopu for May-June 2006 Enhancement (#P1)
function openOrgpopupView(patId,viewCompRight) {
	var viewCRight ;
	var rtStr;
	
	viewCRight = parseInt(viewCompRight);
	
	if (viewCRight <=0)
	{
		rtStr = "nx";
	}
	else
	{
		
		rtStr = "dx";
	}
	
	windowName = window.open("patOrgview.jsp?patId="+patId+"&c="+rtStr,"patient","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=300,height=150,left=400,top=400");
	windowName.focus();
}
function callAllPatientEMR()
{
	
	var study = document.getElementById("studyId").value;
	var organization =  document.getElementById("siteId").value;
	var specialty = document.getElementById("speciality").value;
	var provider = document.getElementById("regby").value;
	
	if(study!=0)
		{
		alert("Study is not a valid search criteria for EMR Search.")
		return;
		}		
	
	if(organization!=0)
	{
	alert("Organization is not a valid search criteria for EMR Search.")
	return;
	}	
	if(specialty!=0)
	{
	alert("Specialty is not a valid search criteria for EMR Search.")
	return;
	}	
	if(provider!="")
	{
	alert("Provider is not a valid search criteria for EMR Search.")
	return;
	}	
	paginate_pat.runFilter();
}
otherLink=function(elCell, oRecord, oColumn, oData){
var htmlStr="";
htmlStr="<div align=\"center\" ><A HREF=\"javascript:void(0);\" onClick=\"openOrgpopupView('"+oRecord.getData("PK_PERSON")+"',"+oRecord.getData("RIGHT_MASK")+");\"><img title=\"<%=LC.L_View%>\" src=\"./images/View.gif\" border =\"0\"><%//=LC.L_View%><%--View*****--%></A></div>";
elCell.innerHTML=htmlStr;
} 
var L_Yes = '<%=LC.L_Yes%>';
var L_No = '<%=LC.L_No%>';

onAStudyLink=function(elCell, oRecord, oColumn, oData){
var sCounter=0,pMRI=$('pMRI').value,status="";
sCounter=oRecord.getData("COUNT");

 if (pMRI > 0) 
	{ 
	 
        if(sCounter>0)
       {
       	   status= L_Yes+" (" + sCounter + ")" ;
       }
        else
       {
       	   status=L_No ;
       } 	
	
	var htmlStr="<table style='border-style:none' border='0'><tr>";  	
	htmlStr+="<td width='25%' style='border-style:none'>"+status+"</td>";

	var pcode=escape(encodeString(oRecord.getData("PERSON_CODE")));

	htmlStr+="<td width='25%' style='border-style:none'>";
	htmlStr=htmlStr+" <A href=\"patientstudies.jsp?srcmenu="+$('srcmenu').value+"&selectedTab=3&mode=M&patientCode="+pcode+"&pkey="+oRecord.getData("PK_PERSON")+"&page=patient&linkto=adverse\"><img border=\"0\" title=\"<%=LC.L_Adverse_Event%>\" alt=\"<%=LC.L_Adverse_Event%>\" src=\"./images/UnexpectedEvent.gif\" /><%--AE*****--%></A>";
	htmlStr+="</td>";
	htmlStr+="<td width='25%' style='border-style:none'>";
	htmlStr=htmlStr+" <A href=\"patientstudies.jsp?srcmenu="+$('srcmenu').value+"&selectedTab=3&mode=M&patientCode="+pcode+"&pkey="+oRecord.getData("PK_PERSON")+"&page=patient&linkto=schedule&generate=N&visit=1\"><img border=\"0\" title=\"<%=LC.L_Schedule%>\" alt=\"<%=LC.L_Schedule%>\" src=\"./images/Schedule.gif\" /><%--SCH*****--%></A>";
	htmlStr+="</td>";
	htmlStr+="<td width='25%' style='border-style:none'>";
    htmlStr=htmlStr+" <A href=\"patientstudies.jsp?srcmenu="+$('srcmenu').value+"&selectedTab=3&mode=M&patientCode="+pcode+"&pkey="+oRecord.getData("PK_PERSON")+"&page=patient&linkto=forms&calledFrom=S\"><%=LC.L_Form_Upper%><%--FORM*****--%></A>";
    htmlStr+="</td>";
    htmlStr+="</tr></table>";
}
elCell.innerHTML=htmlStr;
}
lastName=function(elCell, oRecord, oColumn, oData)
{
	 var htmlStr="";
	 htmlStr=escapeShowTitleCurrentPage(oData);
	elCell.innerHTML=htmlStr;
}

firstName=function(elCell, oRecord, oColumn, oData)
{
	 var htmlStr="";
	 htmlStr=escapeShowTitleCurrentPage(oData);
	elCell.innerHTML=htmlStr;
}
perIdLink=function(elCell, oRecord, oColumn, oData)
{
  var htmlStr="";
	var result = isAccessibleFor(<%=pageRightEMR%>,'N');
	if(result)
		{

 
  var patHPhone="",patBPhone="";
//  var url="patientdetails.jsp?srcmenu="+$('srcmenu').value+"&selectedTab=1&mode=M&pkey="+oRecord.getData("PK_PERSON")+"&patientCode="+escape(pcode)+"&page=patientEnroll&generate=N&studyVer=null";
  var url="patientdetailsquick.jsp?srcmenu="+$('srcmenu').value+"&selectedTab=1";
  //Parse the phone numbers
  var patCode = ""

  var patCode = oRecord.getData("PERSON_CODE") == null ? "" : oRecord.getData("PERSON_CODE");
  var mode ="N";
  var patID = oRecord.getData("PERSON_CODE") == null ? "" : oRecord.getData("PERSON_CODE");
  var patdob = oRecord.getData("DOB_DESC") == null ? "" : oRecord.getData("DOB_DESC");
  var patfname = escapeShowTitle(oRecord.getData("FIRSTNAME") == null ? "" : oRecord.getData("FIRSTNAME"));
  var patlname = escapeShowTitle(oRecord.getData("LASTNAME") == null ? "" : oRecord.getData("LASTNAME"));
  var patmname = escapeShowTitle(oRecord.getData("MIDDLENAME") == null ? "" : oRecord.getData("MIDDLENAME"));
  var patgender = oRecord.getData("GENDER_CODE") == null ? "" : oRecord.getData("GENDER_CODE");
  var patethnicity = oRecord.getData("ETHNICITY_CODE") == null ? "" : oRecord.getData("ETHNICITY_CODE");
  var patrace = oRecord.getData("RACE_CODE") == null ? "" : oRecord.getData("RACE_CODE");
  var patstatus = oRecord.getData("SURVIVAL_STATUS_CODE") == null ? "" : oRecord.getData("SURVIVAL_STATUS_CODE");
  var txtAddEthnicity = oRecord.getData("ADDL_ETHNICITY_DESC") == null ? "" : oRecord.getData("ADDL_ETHNICITY_DESC");
  var txtAddRace = oRecord.getData("ADDL_RACE_DESC") == null ? "" : oRecord.getData("ADDL_RACE_DESC");
  var codeAddEthnicity = oRecord.getData("ADDL_ETHNICITY_CODE") == null ? "" : oRecord.getData("ADDL_ETHNICITY_CODE");
  var codeAddRace = oRecord.getData("ADDL_RACE_CODE") == null ? "" : oRecord.getData("ADDL_RACE_CODE");
  var patdeathdate = oRecord.getData("DEATH_DATE_DESC") == null ? "" : oRecord.getData("DEATH_DATE_DESC");
  var patadd1 =  escapeShowTitle(oRecord.getData("ADDRESS1") == null ? "" : oRecord.getData("ADDRESS1"));
  var patadd2 =  escapeShowTitle(oRecord.getData("ADDRESS2") == null ? "" : oRecord.getData("ADDRESS2"));
  var patcity =  escapeShowTitle(oRecord.getData("CITY") == null ? "" : oRecord.getData("CITY"));
  var patstate =  oRecord.getData("STATE") == null ? "" : oRecord.getData("STATE");
  var patcounty =  oRecord.getData("COUNTY") == null ? "" : oRecord.getData("COUNTY");
  var patzip =  oRecord.getData("ZIPCODE") == null ? "" : oRecord.getData("ZIPCODE");
  var patcountry =  oRecord.getData("COUNTRY") == null ? "" : oRecord.getData("COUNTRY");
  var pathphone =  oRecord.getData("HOME_PHONE") == null ? "" : oRecord.getData("HOME_PHONE");
  var patbphone =  oRecord.getData("WORK_PHONE") == null ? "" : oRecord.getData("WORK_PHONE");
  var patssn =  oRecord.getData("SSN") == null ? "" : oRecord.getData("SSN");
  var maritalStat =  oRecord.getData("MARITAL_STATUS_CODE") == null ? "" : oRecord.getData("MARITAL_STATUS_CODE");
  var pageMode = "patientEMR";
  url = url + "&patCode=" + patCode + "&mode="+ mode +"&patID="+patID+"&patdob="+patdob+"&patfname="+patfname+"&patmname="+patmname+
  "&patlname="+patlname+"&patgender="+patgender+"&patethnicity="+patethnicity+"&patrace="+patrace+"&patstatus="+patstatus+
  "&txtAddEthnicity="+txtAddEthnicity+"&txtAddRace="+txtAddRace+"&codeAddEthnicity="+codeAddEthnicity+"&codeAddRace="+codeAddRace+
  "&patdeathdate="+patdeathdate+"&pageMode="+pageMode+"&patadd1="+patadd1+"&patadd2="+patadd2+"&patcity="+patcity+"&patstate="+patstate+
  "&patstate="+patstate+"&patcounty="+patcounty+"&patzip="+patzip+"&patcountry="+patcountry+"&pathphone="+pathphone+"&patbphone="+patbphone+
  "&patssn="+patssn+"&maritalStat="+maritalStat;
  //var hPhone=oRecord.getData("MASK_PHPHONE")||"";
 // var bPhone=oRecord.getData("MASK_PBPHONE")||"";
 //Parse the Address
 var patAddress1="",patAddress2="",pcity="",pstate="",pzip="",pcountry="";

  /*htmlStr="<A href='"+url+"' onmouseover=\"return overlib('<tr><td><font size=2><b><%=LC.L_Home_Phone%><%--Home Phone*****--%> : </b></font><font size=1>"+hPhone+"</font></td></tr><tr><td><font size=2><b><%=LC.L_Work_Phone%><%--Work Phone*****--%>: </b></font><font size=1>"+bPhone+
	"</font></td></tr><tr><td><font size=2><b><%=LC.L_Address%><%--Address*****--%>: </b></font></td></tr><tr><td><font size=1>"+patAddress1+"</font></td></tr><tr><td><font size=1>"+patAddress2+
	"</font></td></tr><tr><td><font size=1>"+pcity+"&nbsp;<font size=1>"+pstate+
	"</font>&nbsp;<font size=1>"+pzip+"</font></td></tr><tr><td><font size=1>"+pcountry+
	"</font></td></tr><tr><td><font size=2><b><%=LC.L_Notes%><%--Notes*****--%>: </b></font><font size=1>"+personNotes+"</font></td></tr>',CAPTION,'"+getLocalizedMessageString('L_Pat_Hyp',paramArray)<%--Patient - "+patientName+"*****--%>+"',RIGHT,ABOVE);\" onmouseout=\"return nd();\" ><img src=\"./images/View.gif\" border=\"0\" align=\"left\"/></A>";
	*/
	
	htmlStr="<A href='"+url+"' onmouseover=\"\" onmouseout=\"\" >"+oData+"</A>";
		}
	else
		{
		document.getElementById("registerPatientMessage").innerHTML="";
		htmlStr=escapeShowTitle(oData);
		}

 elCell.innerHTML=htmlStr;
}				

genderData=function(elCell, oRecord, oColumn, oData)
{
var htmlStr="";
if (oData && oData.length>0)
htmlStr=oData;
else
htmlStr="<%=LC.L_Not_Specified%>";/*htmlStr="Not Specified";*****/
 elCell.innerHTML=htmlStr;
}				
				
delLink=function(elCell, oRecord, oColumn, oData)
{
var htmlStr="";
var per=oRecord.getData("PK_PERSON");
if (!per) per="";
var gn=$('groupName').value;
if ((per.length>0) && (gn.toLowerCase()=='admin'))
{
	//KM-#3984
	var perCode = oRecord.getData("PERSON_CODE");
	if (perCode.lastIndexOf("\\")) {
		perCode = perCode.replace("\\","\\\\");
	}
htmlStr="<div align=\"center\" ><A title=\"<%=LC.L_Delete%><%--Delete*****--%>\" href=\"deletepatstudy.jsp?srcmenu="+$('srcmenu').value+"&patientId="+per+"&patientCode="+escape(encodeString(oRecord.getData("PERSON_CODE")))+"&delId=patient\" onclick=\"return confirmBox('"+perCode+"');\">"+
"<img src=\"./images/delete.gif\" border=\"0\" align=\"center\"/></A></div>";
//htmlStr="<A title=\"Delete\" href=\"deletepatstudy.jsp?patProtId="+oRecord.getData("PK_PATPROT")+"&srcmenu="+$('srcMenu').value+"&patientId="+per+"&patientCode="+oRecord.getData("PER_CODE")+"&studyNo="+oRecord.getData("STUDY_NUMBER")+"&studyId="+oRecord.getData("FK_STUDY")+"&delId=studypat\"" onClick=\"return confirmBox('"+oRecord.getData("PER_CODE")+"','"+oRecord.getData("STUDY_NUMBER")+"')\"><img src=\"./images/delete.gif\" border=\"0\" align=\"left\"/></A>";

}
elCell.innerHTML=htmlStr;
}

	myCustomFormatter = function(elCell, oRecord, oColumn, oData) {
			var data = oData;
			var record = oRecord;
			var currentDataStr = record.getId("pk_person");
			var pkey = record.getData("PK_PERSON");
			elCell.innerHTML = "<A href=patientstudies.jsp?srcmenu="
					+ document.getElementById('srcmenu').value
					+ "&selectedTab=3&mode=M&patientCode=" + data + "&pkey="
					+ pkey + "&page=patient>" + data + "</A>";
		};
		var paginate_pat;
		$E.addListener(window, "load", function() {

			paginate_pat = new VELOS.Paginator('allPatientEMR', {
				sortDir : "asc",
				sortKey : "PERSON_CODE",
				defaultParam : "userId,accountId,grpId",
				filterParam : "patCode,patage,patName,gender,pstat",
				dataTable : 'serverpagination',
				navigation : true
			});
			paginate_pat.runFilter();
		});

		function callAllPatient() {
			document.forms["patient"].action = "allPatient.jsp?fromEMR=1";
			document.forms["patient"].submit();
		}

 </script>

<title><%=LC.L_All_Patients%><%--All Patients*****--%>
</title>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id ="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>

  
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>


<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.DateUtil,com.velos.eres.business.common.*,java.text.*"%>

<%@ page language = "java" import="com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.person.*,com.velos.eres.service.util.*,com.velos.eres.web.user.*,com.velos.eres.web.grpRights.GrpRightsJB"%>

<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="usrSite" scope="page" class="com.velos.eres.web.userSite.UserSiteJB" />
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="groupB" scope="page" class="com.velos.eres.web.group.GroupJB"/><!--km-->

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<script>
	var refreshPaginator = function() {
			try {
				paginate_pat = new VELOS.Paginator('allPatientEMR', {
					sortDir : "asc",
					sortKey : "PERSON_CODE",
					defaultParam : "userId,accountId,grpId",
					filterParam : "patCode,patage,patName,gender,pstat",
					dataTable : 'serverpagination',
					navigation : false,
					noIndexGeneration : true
				});
				paginate_pat.runFilter();
			} catch (e) {
			}
		
	}
</script>
<body class="yui-skin-sam" style="auto;">

<% String src;
src= request.getParameter("srcmenu");
BrowserRows br = new BrowserRows();

String module="allPatient" ;


  HttpSession tSession = request.getSession(true);

  if (sessionmaint.isValidSession(tSession))
{
	//Added by Manimaran to give access right for default admin group to the delete link 

	String userId = (String) tSession.getValue("userId");

   	int usrId = EJBUtil.stringToNum(userId);
   	
   	int grpId=0;
   	
   	String protocolManagementRight = (String) tSession.getValue("protocolManagementRight");
		
	int protocolManagementRightInt = 0;
	protocolManagementRightInt = EJBUtil.stringToNum(protocolManagementRight);
	

	userB.setUserId(usrId);

	userB.getUserDetails();

	String defGroup = userB.getUserGrpDefault();
	
	grpId=EJBUtil.stringToNum(defGroup);

	groupB.setGroupId(grpId);

	groupB.getGroupDetails();

	String groupName = groupB.getGroupName();
	
	//Get User's saved searches 
	BrowserDao bDao=new BrowserDao();
	bDao.getBrowserSavedSearch(module,userId);
	ArrayList searchNameList=bDao.getSearchNameList();
	ArrayList searchCriteriaList=bDao.getSearchCriteriaList();
	ArrayList defaultList=bDao.getSearchDefaultList();
	
/*	String defaultValue="";
	
	int indx= defaultList.indexOf("Y");
	if (indx>=0) defaultValue=((String)searchCriteriaList.get(indx));
	
	String searchDD=EJBUtil.createPullDownWithStr("searchDD",defaultValue,searchCriteriaList,searchNameList);
	
	*/
	
	
	

	//km
   	int pageRight = 0;
	GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");		
	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MPATIENTS"));

 if (pageRight>0) {

	Calendar cal = new GregorianCalendar();
	String studyId = "";
	String selectedTab = "" ; 
   	selectedTab=request.getParameter("selectedTab");
	String userIdFromSession = (String) tSession.getValue("userId");	
	String uName = (String) tSession.getValue("userName");	
	UserJB user = (UserJB) tSession.getValue("currentUser");

   	String siteId = user.getUserSiteId();
   	String accountId = user.getUserAccountId();
   	String active_status="" ;
	String dGender="" ;
    	String gender="" ;
	String genderVal="" ;
	String pstat="" ;
    	String age ="" ;
	String patName = "";
	String group = "";
	
	

	String ageFilter ="" ;
 	String patCode = "";
 	String regBy ="", ptxt="All",atxt="All",gtxt="All",rtxt="",rbytxt="All" , pattxt="All";
	String spltxt = "All" ;
	
	String selSite = "";
	//Changes made for the filter :- 6November
	String orgId = "";  //for passing the org id 
	String orgtxt = ""; //for passing the org name
	String stdyId = "" ; //for passing the study id 
	String stdytxt = "" ; //for passing the study name
	String patstatId = ""  ; //for passing the patstatusid
	String regByname = "";
	String deathdt = "";

	
 	int lowLimit=0,highLimit=0 ;
	patCode = request.getParameter("patCode");
	if (patCode==null)	 patCode="";
	gender = request.getParameter("patgender");
	genderVal = request.getParameter("patgender");
	if(gender == null || "".equals( gender))
			gender = request.getParameter("gender");
			if (gender == null)
				gender = "";
			genderVal = gender;
	int patDataDetail = 0;
	int personPK = 0;
	int usrGroup = 0;
	userB.setUserId(EJBUtil.stringToNum(userIdFromSession));
	userB.getUserDetails();
	usrGroup =EJBUtil.stringToNum( userB.getUserGrpDefault());
	personPK = EJBUtil.stringToNum(request.getParameter("pkey"));
	 

	regBy =  request.getParameter("patregby");
	selSite =  request.getParameter("dPatSite");
	
	Date dt1 = new java.util.Date();
	Date dt2= new java.util.Date();
	Format formatter;
        //formatter = new SimpleDateFormat("yyyy/MM/dd");
	
	String minDate="";
	String maxDate="";

//Added by Gopu to fix the bugzilla issue #2786 
	int inSite = 0;
	String uOrg = "";
	StringBuffer sbSite = new StringBuffer();
//
	
	if (! StringUtil.isEmpty(regBy))
	{
		UserJB userReg = new UserJB();
		userReg.setUserId(EJBUtil.stringToNum(regBy));
		userReg.getUserDetails();
		
		regByname =   userReg.getUserFirstName() + " " + userReg.getUserLastName();
	}
			
	if (regBy==null) 	regBy="" ; 

		
	
	
	//JM: 30May05

	if( (request.getParameter("spltxt")) != null && (request.getParameter("spltxt").length()> 0))
	spltxt = request.getParameter("spltxt");
	
	String spltxtName = request.getParameter("speciality");
	if (spltxtName ==null || spltxtName.equals("")) spltxt="All";

	
	 patName = request.getParameter("patName");
	 if ( patName == null)
	 {	
	 	patName = "";
		pattxt ="" ;
	}
	else
	{
		pattxt =   patName   ;
	}
	
	
	// JM : 04May05
	if (orgId.equals("0")) 
	{
		
		 siteId ="0"; 		 
	}	

	

	if (   (!  ( orgId == null ))  && ( !orgId.equals("null")) && (!(orgId.equals("")     )) )
	{
		
		 siteId = orgId; 		
	}
	
	
	else  if (     !(selSite == null) &&  (  !  (  selSite.trim() ).equals("") )  )
	{
		 siteId = selSite;		
	}
		
	
	
	pstat= request.getParameter("patstatus");
	
	if(pstat == null || "".equals(pstat))
		pstat = request.getParameter("pstat");
	if ((pstat == null) || (pstat.equals("null"))) {
		pstat = "";
	} else if ((patstatId != null)
	&& (!(patstatId.equals("null")))
	&& (!patstatId.equals(""))) {
		if (!patstatId.equals("0"))
			pstat = patstatId;
	}

	 //for study and patient name
	 String  studyIdStr = "";
	 int idStudy = 0 ;
	 studyIdStr = request.getParameter("dStudy");
	 if ( studyId != null )
	 {
	 	idStudy = EJBUtil.stringToNum(studyIdStr);		
	}
	 if ( ( stdyId != null )  && ( !stdyId.equals("null"))    && (!(stdyId.equals("")     )) )
	{
		idStudy = EJBUtil.stringToNum(stdyId);
	}
	
	
	 UserSiteDao usd = new UserSiteDao ();
	 ArrayList arSiteid = new ArrayList();
	 ArrayList arSitedesc = new ArrayList();
	 String ddSite = "";
	 
	 usd = usrSite.getSitesWithViewRight(EJBUtil.stringToNum(accountId),EJBUtil.stringToNum(userId));
	 
	 arSiteid = usd.getUserSiteSiteIds();	 
	 arSitedesc = usd.getUserSiteNames();	 
 	 //6 November
		 
	
		if ( ( orgtxt == null )  || ( orgtxt.equals("null"  )   )    || (orgtxt.equals(""))   )
		{
			//take the orgtxt from session else take it from the parameters set on the Go 	
	
			for(int cnt = 0 ; cnt < arSiteid.size() ; cnt++)
			{
				if(       siteId.equals(  arSiteid.get(cnt).toString() )   )  
				{
					orgtxt = arSitedesc.get(cnt).toString() ;				
					break ; 
					
				}
			}
			
			
		}


//Commented by Gopu to fix the bugzilla issue #2786
	//ddSite = EJBUtil.createPullDownOrg("dPatSite",EJBUtil.stringToNum(siteId),arSiteid,arSitedesc);	
	
	//Added by Gopu to fix the bugzilla Issue #2786
 	 sbSite.append("<SELECT NAME='siteId' id='siteId'>") ;	
	 sbSite.append("<option selected value='' >"+LC.L_All/*All*****/+"</option>") ;	 		
		if (arSiteid.size() > 0)
		{
			for (int counter = 0; counter < arSiteid.size()  ; counter++)
			{
				//iSite = (Integer)arSiteid.get(counter);
				inSite = Integer.parseInt((String)arSiteid.get(counter));
				if(inSite == EJBUtil.stringToNum(selSite)){			
					sbSite.append("<OPTION value = "+ inSite+" selected>" + arSitedesc.get(counter)+ "</OPTION>");
				}else{						
				sbSite.append("<OPTION value = "+ inSite+">" + arSitedesc.get(counter)+ "</OPTION>");
				}
			
			}
		}

		sbSite.append("</SELECT>");
		 ddSite  = sbSite.toString();
	
	///////////////
	
	// JM: 24May05 for speciality search criteria
	
	String ddSpeciality="";
	CodeDao cd = new CodeDao();
	cd.getCodeValues("prim_sp");
	
	String speciality=request.getParameter("speciality");
	if (speciality==null) speciality="";		

	if (speciality.equals("")){
	ddSpeciality=cd.toPullDown("speciality id='speciality'");
	}
	else{
	ddSpeciality=cd.toPullDown("speciality id='speciality'",EJBUtil.stringToNum(speciality),true);
	}
	

	 
	 CodeDao cd1 = new CodeDao();
	 CodeDao cd2 = new CodeDao();
	 CodeDao cd3 = new CodeDao();
     
	 boolean withSelect = true;
     
     	String dStatus="";
        cd1.getCodeValues("gender");
	
	if (EJBUtil.isEmpty(genderVal))
	   dGender=cd1.toPullDown("gender id='gender'");
	else
  	dGender=cd1.toPullDown("gender id='gender'",EJBUtil.stringToNum(genderVal),withSelect);
    
  
	
	int pstatusId = 0 ;
	
	cd2.getCodeValues("patient_status");
	
	
	if (pstat.equals(""))	
	{
		dStatus = cd2.toPullDown("pstat id='pstat'");
		rtxt = "All" ;		
	} 
	else
	{
  	    dStatus = cd2.toPullDown("pstat id='pstat'",EJBUtil.stringToNum(pstat),withSelect);
	}
	
 //Patint Age    
	String dAge="<select size='1' name='patage' id='patage'>"; 
	age = request.getParameter("patage");
	if(age == null)
		age = "";
	if (age.equals(""))	 
     dAge = dAge + " <option selected value='' SELECTED>"+LC.L_All/*All*****/+"</option>" ;
	else
		dAge = dAge + " <option selected value=''>"+ LC.L_All/*All*****/+ "</option>";
	if (age.equals("0,15"))	 
	   dAge = dAge +  "<option value='0,15' SELECTED>"+LC.L_0_15/*0-15*****/+"</option>";
	else
   	   dAge = dAge +  "<option value='0,15'>"+LC.L_0_15/*0-15*****/+"</option>";
	if (age.equals("16,30"))	   
	   dAge = dAge +  " <option value='16,30' SELECTED>"+LC.L_16_30/*16-30*****/+"</option>";
	 else
      dAge = dAge +  " <option value='16,30'>"+LC.L_16_30/*16-30*****/+"</option>";
	  
	if (age.equals("31,45"))  
		dAge = dAge +   "<option value='31,45' SELECTED>"+LC.L_31_45/*31-45*****/+"</option>";
	else
		dAge = dAge +   "<option value='31,45'>"+LC.L_31_45/*31-45*****/+"</option>";

	if (age.equals("46,60"))  
		dAge = dAge + " <option value='46,60' SELECTED>"+LC.L_46_60/*46-60*****/+"</option>";
		else
		dAge = dAge + " <option value='46,60'>"+LC.L_46_60/*46-60*****/+"</option>";
	if (age.equals("61,75"))  
		dAge = dAge + "<option value='61,75' SELECTED>"+LC.L_61_75/*61-75*****/+"</option>";
	else
		dAge = dAge + "<option value='61,75'>"+LC.L_61_75/*61-75*****/+"</option>";	

	if (age.equals("76,Over"))		
		//Modified by Manimaran to fix the Bug2597
		dAge = dAge +  " <option value='76,Over' SELECTED>"+LC.L_76_Over/*76 &amp; Over*****/+" </option> </select>" ;
	else
		dAge = dAge +  " <option value='76,Over'>"+LC.L_76_Over/*76 &amp; Over*****/+" </option> </select>" ;
		
//end Patient age	
	
	
	//study for the patient search
	
	 String ddstudy ="";
//   	StudyDao studyDao = studyB.getUserStudies(userId, "dStudy",idStudy,"active");
//JM: 02/11/2005 modified 
	StudyDao studyDao = studyB.getUserStudies(userId, "studyId id='studyId' STYLE='WIDTH:177px' ",idStudy,"activeForEnrl");
	ddstudy = studyDao.getStudyDropDown();
	
	ArrayList studyIds = studyDao.getStudyIds();
	ArrayList studyNumbers = studyDao.getStudyNumbers();
	
	
	

				
    			  	   
			
	   
%>
 
 
 

<input type="hidden" id="srcmenu" name="srcmenu" Value="<%=src%>">
<input type="hidden" name="searchFrom" Value="search">
<input type="hidden" name="dstudyId" Value=<%=studyId%>>

<input type="hidden" id="accountId" Value=<%=accountId%>>
<input type="hidden" id="userId"  name="userId"  Value=<%=userId%>>
<input type="hidden" id="grpId" name="grpId" Value=<%=grpId%>>
<input type="hidden" id="pMRI" name="pMRI" Value=<%=protocolManagementRightInt%>>
<input type="hidden" id="groupName" name="groupName" Value=<%=groupName%>>


<Form name="patient" METHOD="POST" onsubmit="return false;"> 
 <DIV class="tabDefTopN" id="div1" style="padding:0px;">
  	    <jsp:include page="mgpatienttabs.jsp" flush="true"/>
 </div>
 <DIV class="tabFormTopN tabFormTopN_PS" id="div2">
  <table width="100%" cellspacing="0" cellpadding="0" border="0" class="basetbl midalign outline">
	<td  colspan="7"><b><%=LC.L_Search_By%><%--Search By*****--%></b></td>
	</tr>
	<tr >
	<td  width="15%"> <%=LC.L_Patient_Id%><%--Patient ID*****--%>: </td>
	<td > <Input type=text name="patCode" id="patCode"  size="15" value = "<%=patCode%>"> </td>
	<td  align="right"><%=LC.L_Age%><%--Age*****--%>:&nbsp;</td>	
	<td  ><%=dAge%></td>	 	
	<td  width="10%" align="right"><%=LC.L_Organization%><%--Organization*****--%>:&nbsp;</td>
	<td  colspan="2"><%=ddSite%></td>	
	</tr>
	<tr >
   	<td  width="15%"> <%=LC.L_Pat_Name%><%--Patient Name*****--%>: <img src="../images/jpg/help.jpg" onmouseover="return overlib('<%=MC.M_SrchFname_Mname%><%--Search on First Name,Last Name or Middle Name.*****--%>',CAPTION,'<%=LC.L_Search_Keyword_S%><%--Search Keyword(s)*****--%>');" onmouseout="return nd();"></td>		
	<td  > <Input type=text name="patName" id="patName"  size="15" value = "<%=patName%>"> </td>
	<td  align="right"> <%=LC.L_Gender%><%--Gender*****--%>:&nbsp;</td>
	<td  ><%=dGender%></td>	
	<td  width="10%" align="right"><%=LC.L_Specialty%><%--Specialty*****--%>:&nbsp;</td>
	<td  colspan="2"><%=ddSpeciality%></td>
	</tr>
	<tr >
	<!-- commented for fix the bug #2394 >
	<!-- td class=tdDefault >Registration Status</td-->		
	<td  width="15%"><%=LC.L_Survival_Status%><%--Survival Status*****--%>:</td>	
	<td  ><%=dStatus%></td>
	<td  align="right"><%=LC.L_Study%><%--Study*****--%>:&nbsp;</td>	
	<td  ><%=ddstudy%></td>
	<td   width="10%" align="right"><%=LC.L_Provider%><%--Provider*****--%>:&nbsp;</td>
	<td><Input type="hidden" name="patregby" size="15" value = "<%=regBy%>">
	<input type=text name="regby" id="regby" value="<%=regByname%>"></td>
	<td align="center"><button type="submit" onClick="callAllPatient()"><%=LC.L_Search%></button></td>
 	</tr>
   
  </table>
</div>

<DIV class="tabFormBotN tabFormBotN_P_1" id="div3" style="width:99%;overflow:auto;position:absolute;border-bottom: 20%;">
<div id="serverpagination">	<P class="defComments" align="center">
<%=MC.M_PlsSrch_ViewList%><%--Please specify Search criteria to view a list of matching Patients*****--%>
</P></div>
<input type="hidden" name="selectedTab" Value=<%=selectedTab%>> 	  
<BR/>
 <table width="50%" cellspacing="6" cellpadding="6">
    <tr>
    	<td width="5%"></td>
    	<td width="95%"></td>
    </tr>
    
	<tr>
		<td></td>
		<td> <A href="#" onclick="callAllPatientEMR()">Continue to search in EMR</A></p></td>
	</tr>
</table>



</DIV>


    	



  </Form>

 		

<% } 

// Rohit Bug No: 3980
//end of if body for page right
else{
%>

<jsp:include page="accessdenied.jsp" flush="true"/>
<%
} //end of if body for session

}
else{

%>

  <jsp:include page="timeout.html" flush="true"/>

  <%

}



%>

   <div> 

    <jsp:include page="bottompanel.jsp" flush="true"/>

  </div>

<div class ="mainMenu" id = "emenu">

 <jsp:include page="getmenu.jsp" flush="true"/>

</div> 



</body>

</html>
