
----  vgdb_message table ----
insert into vgdb_message (pk_msg , message_type , message_data_table , email_notification , realtime_notification) values(nextval('seq_vgdb_message'),'ADT','vgdb_message_ADT',2,1);
insert into vgdb_message (pk_msg , message_type , message_data_table , email_notification , realtime_notification) values(nextval('seq_vgdb_message'),'LAB','vgdb_message_LAB',2,1);




----vgdb_message_segment-----
insert into vgdb_message_segment (pk_msg_sgmnt, message_type, segment_name, is_mandatory, repeated, repeated_segment_table, segment_sequence, segment_type) values(nextval('SEQ_VGDB_MESSAGE_SEGMENT'),'ADT','MSH',1,1,null,null,1);
insert into vgdb_message_segment (pk_msg_sgmnt, message_type, segment_name, is_mandatory, repeated, repeated_segment_table, segment_sequence, segment_type) values(nextval('SEQ_VGDB_MESSAGE_SEGMENT'),'ADT','PID',1,1,null,null,1);
insert into vgdb_message_segment (pk_msg_sgmnt, message_type, segment_name, is_mandatory, repeated, repeated_segment_table, segment_sequence, segment_type) values(nextval('SEQ_VGDB_MESSAGE_SEGMENT'),'ADT','EVN',0,1,null,null,1);
insert into vgdb_message_segment (pk_msg_sgmnt, message_type, segment_name, is_mandatory, repeated, repeated_segment_table, segment_sequence, segment_type) values(nextval('SEQ_VGDB_MESSAGE_SEGMENT'),'ADT','MRG',0,1,null,null,1);
insert into vgdb_message_segment (pk_msg_sgmnt, message_type, segment_name, is_mandatory, repeated, repeated_segment_table, segment_sequence, segment_type) values(nextval('SEQ_VGDB_MESSAGE_SEGMENT'),'ADT','PV1',0,1,null,null,1);
insert into vgdb_message_segment (pk_msg_sgmnt, message_type, segment_name, is_mandatory, repeated, repeated_segment_table, segment_sequence, segment_type) values(nextval('SEQ_VGDB_MESSAGE_SEGMENT'),'ADT','PV2',0,1,null,null,1);
insert into vgdb_message_segment (pk_msg_sgmnt, message_type, segment_name, is_mandatory, repeated, repeated_segment_table, segment_sequence, segment_type) values(nextval('SEQ_VGDB_MESSAGE_SEGMENT'),'ADT','DG1',0,1,null,null,1);




---- vgdb_message_response ----

insert into vgdb_message_response values(nextval('SEQ_VGDB_MESSAGE_RESPONSE'),'ACK','MSH','Message Type','/MSH-9-1','0','ACK');
insert into vgdb_message_response values(nextval('SEQ_VGDB_MESSAGE_RESPONSE'),'ACK','MSH','Sending Application','/MSH-3-1','2','/MSH-5-1');
insert into vgdb_message_response values(nextval('SEQ_VGDB_MESSAGE_RESPONSE'),'ACK','MSH','Sending Facility','/MSH-4-1','2','/MSH-6-1');
insert into vgdb_message_response values(nextval('SEQ_VGDB_MESSAGE_RESPONSE'),'ACK','MSH','Receiving Application','/MSH-5-1','2','/MSH-3-1');
insert into vgdb_message_response values(nextval('SEQ_VGDB_MESSAGE_RESPONSE'),'ACK','MSH','Receiving Facility','/MSH-6-1','2','/MSH-4-1');
insert into vgdb_message_response values(nextval('SEQ_VGDB_MESSAGE_RESPONSE'),'ACK','MSH','MessageID','/MSH-10-1','1',null);
insert into vgdb_message_response values(nextval('SEQ_VGDB_MESSAGE_RESPONSE'),'ACK','MSH','VersionID','/MSH-12-1','2','/MSH-12-1');
insert into vgdb_message_response values(nextval('SEQ_VGDB_MESSAGE_RESPONSE'),'ACK','MSA','AcknowledgementCode','/MSA-1-1','1',null);
insert into vgdb_message_response values(nextval('SEQ_VGDB_MESSAGE_RESPONSE'),'ACK','MSA','InputMessageID','/MSA-2-1','2','/MSH-10-1');
insert into vgdb_message_response values(nextval('SEQ_VGDB_MESSAGE_RESPONSE'),'ACK','MSA','AcknowledgementText','/MSA-3-1','1',null); 



------vgdb_message_field_mapping Values----------

insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'gender_eResearch','F','female');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'gender_eResearch','M','male');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'gender_eResearch','U','unknown');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'maritalstatus_eResearch','D','divorced');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'maritalstatus_eResearch','M','married');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'maritalstatus_eResearch','S','single');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'maritalstatus_eResearch','L','lifePartner');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'maritalstatus_eResearch','P','separated');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'maritalstatus_eResearch','U','unKnown');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'maritalstatus_eResearch','W','widowed');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'maritalstatus_eResearch','Y','commonLaw');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'ethnic_eResearch','2','nonhispanic');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'ethnic_eResearch','1','hispanic');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'ethnic_eResearch','3','notreported');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'ethnic_eResearch','4','declined');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'race_eResearch','I','race_indala');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'race_eResearch','C','race_white');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'race_eResearch','H','race_hwnisl');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'race_eResearch','B','race_blkafr');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'race_eResearch','O','race_asian');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'race_eResearch','D','race_notrep');
insert into vgdb_field_mapping_values(pk_fld_map_vls, mapping_type , orignal_value , mapped_value) values(nextval('SEQ_VGDB_FIELD_MAPPING_VALUES'),'race_eResearch','Z','race_UNK');



				

-----vgdb_message_fields---

insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','MSH','Delimiter','delimiter',0,'1',null,null,null,null,1,'/MSH-1');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','MSH','EventId','eventid',0,'1',null,null,null,null,5,'/MSH-9-2');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','MSH','EncodingCharacters','encodingcharacters',0,'1',null,null,null,null,4,'/MSH-2');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','MSH','SendingApplication','sendingapplication',0,'1',null,null,null,null,50,'/MSH-3-1');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','MSH','AncillaryDepartment','ancillarydepartment',0,'1',null,null,null,null,50,'/MSH-4-1');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','MSH','ReceivingApplication','receivingapplication',0,'1',null,null,null,null,50,'/MSH-5-1');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','MSH','ReceivingFacility','receivingfacility',0,'1',null,null,null,null,50,'/MSH-6-1');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','MSH','DateorTime_of_Message','datetimeofmessage',0,'1',null,null,null,null,50,'/MSH-7-1');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','MSH','MessageType','messagetype',1,'1',null,null,null,null,5,'/MSH-9-1');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','MSH','MessageControlID','messagecontrolid',0,'1',null,null,null,null,50,'/MSH-10-1');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','MSH','VersionID','versionid',0,'1',null,null,null,null,10,'/MSH-12-1');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','Mrn','mrn',1,'1',null,null,null,null,50,'/PID-3-1');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','LastName','lastname',1,'1',null,null,null,null,50,'/PID-5-1');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','FirstName','firstname',1,'1',null,null,null,null,50,'/PID-5-2');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','MiddleName','middlename',0,'1',null,null,null,null,50,'/PID-5-3');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','DOB','dob',1,'1',null,null,null,null,8,'/PID-7-1');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','Gender','gender',1,'1',null,null,null,null,8,'/PID-8-1');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','Race','race',1,'1',null,null,null,null,1,'/PID-10-1');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','MaritalStatus','maritalstatus',0,'1',null,null,null,null,8,'/PID-16-1');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','EthnicGroup','ethnicgroup',1,'1',null,null,null,null,1,'/PID-22-1');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','City','city',1,'1',null,null,null,null,50,'/PID-11-3');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','Address1','address1',1,'1',null,null,null,null,50,'/PID-11-1');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','Address2','address2',0,'1',null,null,null,null,50,'/PID-11-2');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','State','state',1,'1',null,null,null,null,50,'/PID-11-4');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','ZipCode','zipcode',1,'1',null,null,null,null,25,'/PID-11-5');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','Country','country',1,'1',null,null,null,null,50,'/PID-11-6');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','County','county',1,'1',null,null,null,null,50,'/PID-11-9');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','HomePhoneNo','homephoneno',1,'1',null,null,null,null,25,'/PID-13-1');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','WorkPhoneNo','workphoneno',0,'1',null,null,null,null,25,'/PID-14-1');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','DeathDate','DeathDate',0,'1',null,null,null,null,8,'/PID-29-1');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','MRG','PriorMRN','priormrn',0,'1',null,null,null,null,50,'/MRG-1-1');
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','Ethnic_eResearch','ethnic_eResearch',0,'1',null,null,'ethnic_eResearch','EthnicGroup',5,null);
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','MaritalStatus_eResearch','maritalstatus_eResearch',0,'1',null,null,'maritalstatus_eResearch','MaritalStatus',5,null);
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','Gender_eResearch','gender_eResearch',0,'1',null,null,'gender_eResearch','Gender',5,null);
insert into vgdb_message_fields(pk_msg_flds , message_type , segment_name , field_name ,column_name , is_mandatory , repeated , repeated_column, repeated_delimiter, mapping_type, mapping_field , max_length,terser_path) values(nextval('SEQ_VGDB_MESSAGE_FIELDS'),'ADT','PID','Race_eResearch','race_eResearch',0,'1',null,null,'race_eResearch','Race',5,null);
					

		
													
