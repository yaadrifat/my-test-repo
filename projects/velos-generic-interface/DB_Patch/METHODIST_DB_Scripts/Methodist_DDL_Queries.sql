
--vgdb_message_adt--


CREATE TABLE vgdb_message_adt
(
  pk_vgdb_message_adt integer NOT NULL,
  eventId varchar(50),
  delimiter varchar(8),
  encodingcharacters varchar(10),
  sendingapplication varchar(50),
  ancillarydepartment varchar(50),
  receivingapplication varchar(50),
  receivingfacility varchar(50),
  datetimeofmessage varchar(50),
  messagetype varchar(10),
  messagecontrolid varchar(50),
  versionid varchar(10),
  mrn varchar(50),
  lastname varchar(250),
  firstname varchar(250),
  middlename varchar(250),
  dob varchar(25),
  gender varchar(10),
  race varchar(20),
  maritalstatus varchar(8),
  ethnicgroup varchar(10),
  address1 varchar(250),
  address2 varchar(250),
  city varchar(50),
  state varchar(25),
  zipcode varchar(50),
  country varchar(50),
  county varchar(50),
  homephoneno varchar(25),
  workphoneno varchar(25),
  deathdate varchar(25),
  priormrn varchar(50),
  gender_eresearch varchar(50),
  race_eresearch varchar(50),
  ethnic_eresearch varchar(50),
  maritialstatus_eresearch varchar(50),
  messagedate varchar(25),
  lastmodifydate character varying(55),
  messagestatus character varying(10),
  CONSTRAINT vgdb_message_adt_pkey PRIMARY KEY (pk_vgdb_message_adt));
 

CREATE SEQUENCE  SEQ_VGDB_MESSAGE_ADT  MINVALUE 1 MAXVALUE 9999999999999999 INCREMENT BY 1 START WITH 1 CACHE 100  NO CYCLE ;
