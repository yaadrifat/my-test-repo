package com.velos.integration.hl7.dao;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.JdbcTemplate;
import com.velos.integration.hl7.bean.FieldMapBean;
import com.velos.integration.hl7.bean.MessageBean;
import com.velos.integration.hl7.bean.MessageFieldBean;
import com.velos.integration.hl7.bean.ResponseMessageBean;
import com.velos.integration.hl7.bean.SegmentBean;



public class PatientConfiguration {


	private static Logger logger = Logger.getLogger(PatientConfiguration.class);

	private DataSource vgdbDataSource;
	private JdbcTemplate jdbcTemplate;

	public DataSource getVgdbDataSource() {
		return vgdbDataSource;
	}

	public void setVgdbDataSource(DataSource vgdbDataSource) {
		this.vgdbDataSource = vgdbDataSource;
		this.jdbcTemplate = new JdbcTemplate(vgdbDataSource);
		logger.info("Jdbc Template created successfully");
	}



	@Cacheable("messageconfiguration")
	public List<MessageBean> getMessageConfigurationBean() throws Exception ,IOException{

		List<MessageBean> messagelist=new ArrayList<MessageBean>();
		try{

			System.out.println("executing getMessageConfigurationBean() Method");

			String sql="select * from vgdb_message";
			List<Map> rows=jdbcTemplate.queryForList(sql);
			logger.info("Query for Messages :"+sql);
			for(Map row:rows)
			{
				MessageBean mb =new MessageBean();
				mb.setMesssageType((String)row.get("message_type"));
				mb.setMessageDataTable((String)row.get("message_data_table"));
				mb.setEmailNotification((Integer)row.get("email_notification"));
				mb.setRealTimeNotfication((Integer)row.get("realtime_notification"));

				//logger.info("Messages Data "+mb.getMesssageType());

				messagelist.add(mb);
				System.out.println("MessageBean");
			}


		} catch (Exception e) {
			throw new Exception(
					"Failed to Load Message Bean Configuration"+e.getMessage());
		}
		return messagelist;
	}

	@Cacheable("segmentconfiguration")
	public List<SegmentBean> getSegmentConfigurationBean() throws Exception,IOException {

		List<SegmentBean> segmentlist=new ArrayList<SegmentBean>();
		try{


			System.out.println("executing getSegmentConfigurationBean() Method");


			String sql="select * from vgdb_message_segment";
			List<Map> rows=jdbcTemplate.queryForList(sql);
			logger.info("Query for Segments :"+sql);

			for(Map row:rows)
			{
				SegmentBean sb =new SegmentBean();
				sb.setMessageType((String)row.get("message_type"));
				sb.setSegmentName((String)row.get("segment_name"));
				sb.setRepeatedSegmentTable((String)row.get("repeated_segment_table"));
				sb.setRepeated((String)row.get("repeated"));
				sb.setSegmentType((Integer)row.get("segment_type"));
				sb.setSegmentSequence((Integer)row.get("segment_sequence"));
				sb.setIsMandatory((Integer)row.get("is_mandatory"));
				segmentlist.add(sb);
			}

		}catch (Exception ex) {

			throw new Exception(
					"Failed To load Message Segment Bean Configuration"+ex.getMessage());

		}
		return segmentlist;
	}


	@Cacheable("messageFieldsConfiguration")
	public List<MessageFieldBean> getMessageFieldsConfiguration() throws Exception,IOException{

		List<MessageFieldBean> messagefields=new ArrayList<MessageFieldBean>();
		try{

			MessageFieldBean messagefield;

			String sql="select * from vgdb_message_fields";
			List<Map> rows=jdbcTemplate.queryForList(sql);

			for(Map row:rows){


				messagefield=new MessageFieldBean();

				messagefield.setMessageType((String)row.get("message_type"));
				messagefield.setSegmentName((String)row.get("segment_name"));
				messagefield.setFieldName((String)row.get("field_name"));
				messagefield.setRepeated((String)row.get("repeated"));
				messagefield.setColumn_name((String)row.get("column_name"));
				messagefield.setRepeatedColumn((String)row.get("repeated_column"));
				messagefield.setIsMandatory(Integer.parseInt(row.get("is_mandatory").toString()));
				messagefield.setRepeatedDelimiter((String)row.get("repeated_delimiter"));
				messagefield.setMappingType((String)row.get("mapping_type"));
				messagefield.setMappingField((String)row.get("mapping_field"));
				messagefield.setMaxLength(Integer.parseInt(row.get("max_length").toString()));
				messagefield.setTerserPath((String)row.get("terser_path"));

				messagefields.add(messagefield);


			}


		} catch (Exception e) {
			throw new Exception(
					"Failed to Load MessageFieldBean Configuration"+e.getMessage());
		}
		return messagefields;

	}

	@Cacheable("fieldmappingconfiguration")
	public List<FieldMapBean> getFieldmap() throws Exception,IOException {

		List<FieldMapBean> fieldmap=new ArrayList<FieldMapBean>();
		try{


			FieldMapBean fieldMapBean;

			String sql="select * from vgdb_field_mapping_values";
			List<Map> rows=jdbcTemplate.queryForList(sql);

			for(Map row:rows){

				fieldMapBean=new FieldMapBean();
				fieldMapBean.setMappingType((String)row.get("mapping_type"));
				fieldMapBean.setMappedValue((String)row.get("mapped_value"));
				fieldMapBean.setOriginalValue((String)row.get("orignal_value"));

				fieldmap.add(fieldMapBean);


			}


		} catch (Exception e) {
			throw new Exception(
					"Failed to Load Mapping Bean Configuration"+e.getMessage());
		}
		return fieldmap;
	}

	@Cacheable("ResponseMessageBeanConfiguration")
	public List<ResponseMessageBean> getResponseMessageBeanList() throws Exception{
		List<ResponseMessageBean> resmesbeanlist=new ArrayList<ResponseMessageBean>();
		try{

			String sql="select * from vgdb_message_response";
			List<Map> rows=jdbcTemplate.queryForList(sql);


			for(Map row:rows){
				ResponseMessageBean rmb=new ResponseMessageBean();
				rmb.setMessageType((String)row.get("message_type"));
				rmb.setSegmentName((String)row.get("segment_name"));
				rmb.setFieldName((String)row.get("field_name"));
				rmb.setFieldValue((String)row.get("field_value"));
				rmb.setTerserPath((String)row.get("tensor_field"));
				rmb.setFieldFrom(Integer.parseInt((String)row.get("field_from")));

				resmesbeanlist.add(rmb);
			}



		}catch(Exception e){
			throw new Exception(
					"Failed to Load Response Bean Configuration"+e.getMessage());

		}
		return resmesbeanlist;
	}




}
