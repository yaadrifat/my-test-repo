package com.velos.integration.adt.processor;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.camel.CamelContext;
import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import ca.uhn.hl7v2.model.Message;

import com.velos.integration.adt.dao.ADTDao;
import com.velos.integration.hl7.dao.MessageDao;
import com.velos.integration.hl7.notifications.EmailNotification;
import com.velos.integration.hl7.notifications.HL7CustomException;
import com.velos.integration.hl7.processor.MessageProcess;
import com.velos.integration.hl7.util.StringToXMLGregorianCalendar;
import com.velos.services.Code;
import com.velos.services.OrganizationIdentifier;
import com.velos.services.PatientDataBean;
import com.velos.services.PatientSearch;

public class ADTProcess extends MessageProcess {

	private static Logger logger = Logger.getLogger(ADTProcess.class);

	private ADTDao adtDao;
	private ADTPostProcessorHelper aDTPostProcessorHelper;
	Properties prop1 = new Properties();

	private MessageDao messagedao = new MessageDao();

	Properties prop = new Properties();

	public ADTProcess(MessageProcess messageProcess) throws HL7CustomException {
		super(messageProcess);
		adtDao = new ADTDao(messageProcess.getMessageDao());
		aDTPostProcessorHelper = new ADTPostProcessorHelper();

		try {
			prop.load(this.getClass().getClassLoader()
					.getResourceAsStream("adt.properties"));
			prop1.load(this.getClass().getClassLoader()
					.getResourceAsStream("ackcodes.properties"));

		} catch (Exception e) {
			throw new HL7CustomException(e.getMessage(),
					prop1.getProperty("AcknowledgementReject"));
		}
	}

	public ADTDao getAdtDao() {
		return adtDao;
	}

	public void setAdtDao(ADTDao adtDao) {
		this.adtDao = adtDao;
	}

	public boolean preProcessMerge(String tableName,
			Map<String, String> resultMap) throws HL7CustomException {

		try {
			logger.info("**************Update pre Mrg Record****************");
			StringBuffer sqlMerge = new StringBuffer("select ");
			String delim = "";
			StringBuffer whrSqlMerge = new StringBuffer();

			String mrnField = "";

			int i = 0;
			for (Entry<Object, Object> e : prop.entrySet()) {

				if ("select".equals(e.getValue().toString())
						|| e.getKey().toString().contains("mrg.")) {
					sqlMerge.append(delim);

					sqlMerge.append(e.getKey().toString().substring(4));

					delim = ", ";
					i++;
				}
			}

			sqlMerge.append(" from " + tableName + " where ");

			sqlMerge.append(" pk_" + tableName + " = (select max(pk_"
					+ tableName + ") from " + tableName + " where "
					+ prop.getProperty("merg.mrnFieldName") + " = '"
					+ resultMap.get(prop.getProperty("merg.mergeField")) + "')");

			logger.info("sqlMergeQuery  " + sqlMerge);
			HashMap<String, String> record = adtDao.getRecord(sqlMerge
					.toString());

			int count = 0;

			if (record != null && record.size() >= 1) {
				// pick the latest record with priorMRN
				// if record != null then check record Map values with resultMap
				// values
				// if it is equal then update the record in vgdb

				for (Entry<Object, Object> e : prop.entrySet()) {
					if ("select".equals(e.getValue().toString())
							|| e.getKey().toString().contains("mrg.")) {

						String result = resultMap.get(e.getKey().toString()
								.substring(4));
						String recordVal = record.get(e.getKey().toString()
								.substring(4));

						if (result != null && recordVal != null
								&& result.equalsIgnoreCase(recordVal)) {

							count = count + 1;
						}

					}
				}

				// if all Patient Demographics matches then update the record
				// with New MRN
				if (count == i) {
					logger.info("**************Update Mrg Record****************");

					try {

						mrnField = prop.getProperty("merg.mrnFieldName");
						sqlMerge = new StringBuffer();
						sqlMerge.append("update "
								+ tableName
								+ " set "
								+ mrnField
								+ " ='"
								+ resultMap.get(mrnField)
								+ "',"
								+ prop.getProperty("merg.lastmodifydate")
								+ " ='"
								+ messagedao.getDate()
								+ "',"
								+ prop.getProperty("merg.msgstatuscol")
								+ "='"
								+ prop.getProperty("merg.msgstatus")
								+ "' where "
								+ mrnField
								+ " ='"
								+ resultMap.get(prop
										.getProperty("merg.mergeField")) + "'");
						adtDao.updateRecord(sqlMerge.toString());
						logger.info("Updated the query with New MRN ");

					} catch (Exception e) {
						e.printStackTrace();
						throw new HL7CustomException(e.getMessage(),
								prop1.getProperty("AcknowledgementError"));
					}

				}

				else {

					// Patient demographics are not matches, so send error
					// notification
					// stating with
					// mrn, firstname, lastname, dob, race, gender details from
					// both the
					// current and previous record. are not matches
					// emailNotification.sendMail("Patient Demographics doesnot match for prior MRN");
					throw new HL7CustomException("",
							"Patient Demographics doesnot match for prior MRN",
							prop1.getProperty("AcknowledgementReject"));

				}
			}
			// if record Map == null means there is no Patient Demographics with
			// PriorMRN
			if (record == null) {

				// check all vgdb records with current Patient Demographics
				// if it matches send Notification Demographics matches buts MRN
				// doesn't match

				sqlMerge = new StringBuffer("select ");
				delim = "";
				sqlMerge.append(prop.getProperty("merg.mrnFieldName"));
				sqlMerge.append(" from " + tableName + " where ");

				for (Entry<Object, Object> e : prop.entrySet()) {
					if ("select".equals(e.getValue().toString())
							|| e.getKey().toString().contains("mrg.")) {

						sqlMerge.append(delim);
						sqlMerge.append(e.getKey().toString().substring(4)
								+ " = '"
								+ resultMap.get(e.getKey().toString()
										.substring(4)) + "'");
						delim = " AND ";
					}

				}

				sqlMerge.append(" order by pk_" + tableName + " desc ");

				logger.info("Select Query with patient demographics  "
						+ sqlMerge);
				record = adtDao.getRecord(sqlMerge.toString());

				if (record != null) {
					// Send you email notification here stating that no previous
					// record
					// exist for this merge message.
					// With notification you need to send the details also, that
					// for
					// which ADT merge message this notification is generated.
					// You
					// need
					// to create template for that
					// It will include things like mrn, firstname, lastname,
					// dob,
					// race,
					// gender. Please take Ram's help on how the notification
					// should
					// look like.
					// emailNotification.sendMail("Patient Demographics are matches but Prior MRN doesn't match");
					logger.info("Patient Demographics are matches but Prior MRN doesn't match");
					throw new HL7CustomException(
							"",
							"Patient Demographics are matches but Prior MRN doesn't match",
							prop1.getProperty("AcknowledgementReject"));

				} else {
					// emailNotification.sendMail("Neither Patient Demographics nor MRN matches with any previous record");
					logger.info("Neither Patient Demographics nor MRN matches with any previous record");
					throw new HL7CustomException(
							"",
							"Neither Patient Demographics nor MRN matches with any previous record",
							prop1.getProperty("AcknowledgementReject"));

					// Both Demographics and MRN mismatch. Send notification
					// accordingly.

				}

			}

		} catch (HL7CustomException e) {
			throw e;
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return true;
	}

	public boolean preProcessUpdate(String tableName,
			Map<String, String> resultMap) throws HL7CustomException {
		try {
			StringBuffer sqlUpdate = new StringBuffer(
					"select count(*) as count");

			sqlUpdate.append(" from " + tableName + " where "
					+ prop.getProperty("upd.mrnFieldName") + " = "
					+ resultMap.get(prop.getProperty("upd.mrnFieldName")));

			HashMap<String, String> record = adtDao.getRecord(sqlUpdate
					.toString());

			if (record.containsKey("count")
					&& (Integer.parseInt(record.get("count")) == 0)) {
				// Send Error notification as no previous records for update
				// event.
				return false;
			}
		} catch (HL7CustomException e) {
			throw e;
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return true;
	}

	public boolean preProcess(String tableName, Map<String, String> resultMap,
			Message input) throws HL7CustomException {
		// Apply your pre process logic here. This will include merging patients
		// in VGDB.
		logger.info("****************Message Pre Process*******************");

		try {

			if ("Y".equals(prop.getProperty("preMergeReq").toString())) {
				if (prop.containsKey("mergeEvents")
						&& resultMap.containsKey("eventid")
						&& (prop.getProperty("mergeEvents").toString()
								.contains(resultMap.get("eventid")))) {

					return preProcessMerge(tableName, resultMap);
				}
			}
			if ("Y".equals(prop.getProperty("preUpdateReq").toString())) {
				if (prop.containsKey("updateEvents")
						&& resultMap.containsKey("eventid")
						&& (prop.getProperty("updateEvents").toString()
								.contains(resultMap.get("eventid")))) {
					return preProcessUpdate(tableName, resultMap);
				}
			}
		} catch (HL7CustomException e) {
			throw e;
		}
		return true;
	}

	public boolean postProcess(String tablename, Map<String, String> resultMap)
			throws Exception {
		// Apply you post process logic here. This will include updating/merging
		// patients in eResearch
		// int count=0;
		logger.info("****************ADT Post Process*******************");
		logger.info("****************ADT Post Process*******************");
		if ("Y".equals(prop.getProperty("postMergeReq").toString())) {
			if (prop.containsKey("mergeEvents")
					&& resultMap.containsKey("eventid")
					&& (prop.getProperty("mergeEvents").toString()
							.contains(resultMap.get("eventid")))) {
				System.out.println("****PostProcess***");
				List<PatientDataBean> patientDataList = aDTPostProcessorHelper
						.searchPatientHelper(resultMap, prop, "mrg_eResearch",
								1);
				
				if (patientDataList == null) {
					// search with MRN,
					patientDataList = aDTPostProcessorHelper
							.searchPatientHelper(resultMap, prop,
									"mrg_eResearch", 2);
					// comparing eresearch Mrn value with Incoming Msg Mrn
					boolean result=compareEresValues(patientDataList,
							resultMap, 2);
					//MRN doesn't match if it returns false
					//compare PatientDemographics
					if (result == false) {
						patientDataList = aDTPostProcessorHelper
								.searchPatientHelper(resultMap, prop,
										"mrg_eResearch", 3);

						// compare eresearch PatientDemographics with resultMap PD
						//Both MRN and Patient Demographics doesn,t Matches 
						//if it returns false 
						//throw error response
						result = compareEresValues(patientDataList,
								resultMap,3);
						if(result == false){
							throw new HL7CustomException("",
									"Neither Patient Demographics nor MRN matches"
											+ " with any previous record","AR");
						}
					}

					// if record exist throw error with
					// eResearch and current message demographics.
					// else search with demographics,
					/*
					 * patientDataList = aDTPostProcessorHelper
					 * .searchPatientHelper(resultMap, prop, "mrg_eResearch",
					 * 3);
					 */
					// if record exist throw
					// error with eResearch and current message demographics.
				} else {

					// If Configuration says throw error and notification for
					// multiple records check if multiple records and take
					// appropriate actions.
					// If no errors call update patient facility with new
					// mrn.
					if ("Y".equals(prop.get("multipleMergUpdates").toString()) || ("N".equals(prop.get("multipleMergUpdates").toString()) && patientDataList.size() == 1)) {
						System.out.println("****PostProcess-2***");
						aDTPostProcessorHelper.updatePatientFacilityHelper(
								resultMap, prop, "mrg_eResearch",
								patientDataList);
						//Merge Success
					} else if ("N".equals(prop.get("multipleMergUpdates").toString()) && patientDataList.size() > 1) {
						throw new HL7CustomException(
								"",
								"Multiple Merge Records are not allowed to Update",
								"AR");
					}
				}
			}
		}
		logger.info("****************prop.getProperty(postUpdateReq).toString()*******************" + prop.getProperty("postUpdateReq").toString());
		logger.info("****************prop.containsKey(updateEvents)*******************" + prop.containsKey("updateEvents"));
		logger.info("****************resultMap.containsKey(eventid)*******************" + resultMap.containsKey("eventid"));
		logger.info("****************(prop.getProperty(updateEvents).toString().contains(resultMap.get(eventid))*******************" + prop.getProperty("updateEvents").toString().contains(resultMap.get("eventid")));
		if ("Y".equals(prop.getProperty("postUpdateReq").toString())) {
			if (prop.containsKey("updateEvents")
					&& resultMap.containsKey("eventid")
					&& (prop.getProperty("updateEvents").toString()
							.contains(resultMap.get("eventid")))) {
				List<PatientDataBean> patientDataList = aDTPostProcessorHelper
						.searchPatientHelper(resultMap, prop, "upd_eResearch",
								2);
				logger.info("****************patientDataList*******************" + patientDataList.size());
				if (patientDataList != null) {
					// If Configuration says throw error and notification for
					// multiple records check if multiple records and take
					// appropriate actions.
					// If no errors call update patient facility with new
					// demographics details
					if ("Y".equals(prop.get("multipleUpdates").toString()) || ("N".equals(prop.get("multipleUpdates").toString()) && patientDataList.size() == 1)) {
						aDTPostProcessorHelper.updatePatientHelper(resultMap,
								prop, "upd_eResearch", patientDataList);
					} else if ("N".equals(prop.get("multipleUpdates").toString()) && patientDataList.size() > 1) {
						throw new HL7CustomException(
								"",
								"Multiple Update Records are not allowed to Update",
								"AR");
					}
				}
			}
		}
		return true;
	}

	@SuppressWarnings("null")
	private boolean compareEresValues(List<PatientDataBean> patientDataList,
			Map<String, String> resultMap, int searchType)
					throws HL7CustomException, Exception {
		boolean compresult=false;
		StringBuffer eresDemg =null;
		StringBuffer reslutMapDemg =null;
		String rsltMapMrn = resultMap
				.get(prop.getProperty("merg.mrnFieldName"));

		for (Entry<Object, Object> e : prop.entrySet()) {
			if(e.getKey().toString().contains("cmpr_eResearch.")) {
				String rsltMapDmg = resultMap.get(e.getValue().toString());
				reslutMapDemg.append(rsltMapDmg);
				reslutMapDemg.append("\n");
			}
		}
		
		//comparing eResearch MRN's with Incoming Msg MRN
		//if it matches throw error response
		//if it not matches return false
		if (searchType == 2) {
			for (PatientDataBean pdb : patientDataList) {

				// Comparing eResearchMrn with resultMap Mrn 
				//if it matches sending error with demographics
				if (pdb.getPatientFacilityId().equals(rsltMapMrn)) {
					eresDemg.append(pdb.getPatFirstName());
					eresDemg.append("\n");
					eresDemg.append(pdb.getPatLastName());
					eresDemg.append("\n");
					eresDemg.append(pdb.getGender().toString());
					eresDemg.append("\n");

					throw new HL7CustomException("",
							"MRN Matches \n InComing Message Demographics\n"
									+ reslutMapDemg
									+ "\neRsearch Demographics\n " + eresDemg,
							"AR");
				}

			}
			return compresult;
			
		}
		// comparing eResearch patient demographics with resultMap demographics
		// if patdemographics matches send error response with eres patdemogrphics
		// and Incoming Msg Demographics
		// if PatDemographics doesn,t matches return false
		if (searchType == 3) {
			StringBuffer patDemg=null;
			Class cls = Class.forName("com.velos.services.PatientDataBean");
			Object obj = cls.newInstance();
			Class parameterTypes[] ={};
			Method method;
			Object resultVal;
			int count=0;
			for(PatientDataBean pdb : patientDataList){
				
				for (Entry<Object, Object> e : prop.entrySet()) {
					
					if (prop.containsKey("cmpr_eResearch." +e.getKey().toString().substring(15))
							&& !"".equals(prop.getProperty("cmpr_eResearch" +e.getKey().toString().substring(15).trim()))) {

						method = cls.getMethod("get"+e.getKey().toString().substring(15).trim()+"()", parameterTypes);
						resultVal = method.invoke(pdb, null);

						if(resultVal.toString().equalsIgnoreCase(resultMap.get(e.getValue().toString()))){
							count=count+1;
						}
					}
				}
				//Number of Patient Demographic fields to compare in CTMS DB, config in adt.properties file
				int i=Integer.parseInt(prop.getProperty("numberoffieldstocompare"));
				if(count == i ){
					eresDemg.append("\nPatientDemographics\n");
					eresDemg.append(pdb.getPatFirstName());
					eresDemg.append("\n");
					eresDemg.append(pdb.getPatLastName());
					eresDemg.append("\n");
					eresDemg.append(pdb.getGender().toString());
					eresDemg.append("\n");
					eresDemg.append(pdb.getPatDateofBirth());
					eresDemg.append("\n");
					eresDemg.append(pdb.getRace().toString());

					patDemg.append(eresDemg+"\n");
					compresult=true;
				}
				count=0;
			}

			if(compresult == true){
				throw new HL7CustomException("","eResearchDemographics\n"+ patDemg
						+"\n"+"InComing Msg Demg\n"+reslutMapDemg,"AR");
			}

		}

		return false;

	}
}
