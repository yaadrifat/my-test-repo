package com.velos.integration.hl7.notifications;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.springframework.mail.MailParseException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.apache.log4j.Logger;

public class EmailNotification {
	private static Logger logger = Logger
			.getLogger(EmailNotification.class.getName());
	
	private JavaMailSender mailSender;
	private SimpleMailMessage simpleMailMessage;

	public void setSimpleMailMessage(SimpleMailMessage simpleMailMessage) {
		this.simpleMailMessage = simpleMailMessage;
	}

	public void setMailSender(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}

	public void sendMail(String content,String errorMsg,String msgdate,String mrn,String msgId) throws IOException, HL7CustomException {
		logger.info("Creating Mime Message");
		MimeMessage message = mailSender.createMimeMessage();
		logger.info("Mime Message got created");
		InputStream input=null;

		try{
			Properties prop = new Properties();

			input = this.getClass().getClassLoader()
					.getResourceAsStream("email.properties");			
			prop.load(input);
			logger.info("Setting message Parameters");
			String enviornment = prop.getProperty("enviornment");   
			MimeMessageHelper helper = new MimeMessageHelper(message, true);

			helper.setFrom(simpleMailMessage.getFrom());
			helper.setTo(simpleMailMessage.getTo());
			helper.setSubject(enviornment + " : " 
					+ simpleMailMessage.getSubject() +mrn+" } - Message Id "+"{ "+msgId+" }");
			
			helper.setText(String.format(
					simpleMailMessage.getText(),"","\nThe incoming message at { "+msgdate+" } "
							+ "for patient { "+mrn+" } caused errors."
							+ " \n\n See below for description.\n"
							+ "\nError summary: { "+errorMsg+" }\n\n"+"The details are below :\n\n{ "
							+ content+" }\n"));		
			logger.info("Message Parameters set");
			/*
			String fileLoc = new File(System.getProperty("catalina.base"))+ "/logs/coverage.log";
			logger.info("Log File location : " + fileLoc);
			File file = new File(fileLoc);			
			helper.addAttachment("coverage.log", file);*/
			mailSender.send(message);
			logger.info("Message sent");
		}catch (Exception e) {
			throw new HL7CustomException(e.getMessage(),"AE");
		}finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}
}
