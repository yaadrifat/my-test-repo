package com.velos.integration.hl7.client;

import java.io.IOException;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import ca.uhn.hl7v2.HL7Exception;

import com.velos.integration.hl7.dao.MessageConfiguration;


public class CacheClient {
	public static void main(String... args) throws IOException, Exception {
        ApplicationContext context = new FileSystemXmlApplicationContext(
                "/src/main/webapp/WEB-INF/protocol-ws-servlet.xml");
        MessageConfiguration product = (MessageConfiguration) context.getBean("patientConfiguration");
 
        // calling getProduct method first time.
        System.out.println("First Time"+product.getMessageConfigurationBean());
 
        // calling getProduct method second time. This time, method will not
        // execute.
        System.out.println("Second Time"+product.getMessageConfigurationBean());
 
        
    }
}
