package com.velos.integration.hl7.processor;

import java.util.HashMap;

import com.velos.integration.adt.processor.ADTProcess;

public class MessageProcessFactory {
	
	private HashMap<String, MessageProcess> cache = new HashMap<String, MessageProcess>();

	public MessageProcess getMessageProcessInstance(
			MessageProcess messageProcess, String type) throws Exception {
		
		if (cache.isEmpty())
			createCache(messageProcess);
		return getFromCache(messageProcess, type);
	}

	public MessageProcess getFromCache(MessageProcess messageProcess,
			String type) throws Exception {
		System.out.println("Type=="+type);
		if ("ADT".equals(type)) {
			if (!cache.containsKey(type))
				cache.put(type, new ADTProcess(messageProcess));
			return cache.get(type);
		} else {
			if (!cache.containsKey("GENERIC"))
				cache.put(type, messageProcess);
			return cache.get("GENERIC");
		}

	}

	public void createCache(MessageProcess messageProcess) throws Exception {
		cache.put("ADT", new ADTProcess(messageProcess));
		cache.put("GENERIC", messageProcess);
	}

}
