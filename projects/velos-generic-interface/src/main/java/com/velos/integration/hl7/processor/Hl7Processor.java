package com.velos.integration.hl7.processor;

import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;

import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.PipeParser;
import ca.uhn.hl7v2.util.Terser;

import com.velos.integration.hl7.bean.MessageBean;
import com.velos.integration.hl7.bean.MessageFieldBean;
import com.velos.integration.hl7.bean.SegmentBean;
import com.velos.integration.hl7.dao.MessageConfiguration;
import com.velos.integration.hl7.dao.MessageDao;
import com.velos.integration.hl7.notifications.HL7CustomException;
import com.velos.integration.hl7.util.MessageValidationUtil;

public class Hl7Processor implements Processor {

	private static Logger logger = Logger.getLogger(Hl7Processor.class);

	private MessageProcess messageProcess;
	private MessageValidationUtil validationUtil;
	private MessageConfiguration messageConfiguration;
	private MessageDao messageDao;
	private MessageProcessFactory messageProcessFactory;
	Properties prop=new Properties();
		
	public MessageProcessFactory getMessageProcessFactory() {
		return messageProcessFactory;
	}

	public void setMessageProcessFactory(MessageProcessFactory messageProcessFactory) {
		this.messageProcessFactory = messageProcessFactory;
	}

	public MessageDao getMessageDao() {
		return messageDao;
	}

	public void setMessageDao(MessageDao messageDao) {
		this.messageDao = messageDao;
	}

	public void setValidationUtil(MessageValidationUtil validationUtil) {
		this.validationUtil = validationUtil;
	}
	public MessageConfiguration getMessageConfiguration() {
		return messageConfiguration;
	}

	public void setMessageConfiguration(MessageConfiguration messageConfiguration) {
		this.messageConfiguration = messageConfiguration;
	}

	public MessageProcess getMessageProcess() {
		return messageProcess;
	}

	public void setMessageProcess(MessageProcess messageProcess) {
		this.messageProcess = messageProcess;
	}


	@Override
	public void process(Exchange exchange) throws Exception {
		String messageString = exchange.getIn().getBody(String.class);
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream("ackcodes.properties");
		prop.load(inputStream);

		logger.info("\nIncoming Message ::\n"+messageString);

		Message output = null;
		Message input=null; 
		String messageType=null;
		try{
			
				// Message Pre-Process
			//Commented By Aman  
			//mpreprocess.preProcess(messageString);
			
			//List<String> segList=mpreprocess.segmentList(messageString);
			PipeParser pipeParser = new PipeParser();
			pipeParser.setValidationContext(new HL7ValidationContext());
			input = pipeParser.parse(messageString);
			
			Terser t=new Terser(input);

			messageType=t.get("/MSH-9-1");
			messageProcess = messageProcessFactory.getMessageProcessInstance(messageProcess, messageType);
			System.out.println("messageProcess=="+messageProcess);
			//Validation Starts
			List<MessageBean> messageBeanList = messageConfiguration.getMessageConfigurationBean();
			List<SegmentBean> segmentBeanList = messageConfiguration.getSegmentConfigurationBean();
			List<MessageFieldBean> messageFieldBeanList = messageConfiguration.getMessageFieldsConfiguration();
			validationUtil.validateMessage(messageString,messageType,messageBeanList,messageFieldBeanList, segmentBeanList);
			//Validation ends

			
			
			//Based on message type, load the Message Type configurations. & Message Processing Starts
			output=messageProcess.processMessage(input);
			if(output!=null){
				messageDao.insertMessageAudit(messageType,input,output);
				exchange.getOut().setBody(output);
			}
		} catch (HL7CustomException e) {
			logger.error("\nError while parsing the incoming message :: \n");
			e.printStackTrace();
			String emailNotfMsg=e.getEmailNotfMsg();
			String errMessage=null;
			if(emailNotfMsg!=null){
				errMessage=emailNotfMsg;
			}else if(emailNotfMsg==null || emailNotfMsg.trim().length()>0) {
				errMessage=e.getLocalizedMessage();
			}
			
			Message out=getMessageProcess().getFinalException(messageString,errMessage,e.getResponeType());
			exchange.getOut().setBody(out);

		}catch(Exception e){
			e.printStackTrace();
			System.out.println("ERRORMESSAGE----->"+e.getMessage());
			Message out=getMessageProcess().getFinalException(messageString,e.getMessage(),prop.getProperty("AcknowledgementError"));
			exchange.getOut().setBody(out);
		}finally{
			inputStream.close();
		}
		
		
		
		//Convert the String message to Hapi Message.

		//If message converts successfully, get the Message Header segment and identify the message type.

		//Based on message type, load the configurations. 

		//Parse the message, using reflection. For any missing segment, missing field as per the cardinality in the configuration we need to mark this message as 'AR'.
		//In this case throw an exception and in catch of that, generate a response with AR. Response segments for a message type will also come from the configuration.
		//Please note we might need to send an email notification also for a failed message, so please keep the code flexible such that you can that the notification component.

		//If message is parsed successfully, persist the message in the appropriate table/tables. And generate a successful response based on the configured segments.

		//Persist the message in Audit table for both the failed and passed messages.

		//All the above steps you can do using multiple class, multiple functions anyway you feel comfortable. At each step log the message with appropriate texts.

		//Please note I havn't yet specified what to do if string message fails to convert to Hapi Message. That is 'AE' response. I am still thinking how to handle that case.
		//Till then you can go ahead and start developing the other things.
	}


}
