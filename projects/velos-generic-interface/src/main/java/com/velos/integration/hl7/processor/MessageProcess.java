package com.velos.integration.hl7.processor;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.io.IOException;
import org.apache.log4j.Logger;
import com.velos.integration.hl7.ackmappings.AckResponseType;

import com.velos.integration.hl7.bean.MessageBean;
import com.velos.integration.hl7.bean.ResponseMessageBean;
import com.velos.integration.hl7.dao.MessageConfiguration;
import com.velos.integration.hl7.dao.MessageDao;
import com.velos.integration.hl7.notifications.EmailNotification;
import com.velos.integration.hl7.notifications.HL7CustomException;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.DefaultModelClassFactory;
import ca.uhn.hl7v2.util.Terser;

public class MessageProcess {

	private static Logger logger = Logger.getLogger(Hl7Processor.class);

	private ComparisonUtil comparisionutil;
	private MessageConfiguration messageConfiguration;
	private Message output;
	private InterfaceUtil interfaceutil;
	private MessageDao messageDao;
	private EmailNotification emailNotification;
	Properties prop = new Properties();
	Map<String, Object> messageMap;
	InputStream inputStream;

	
	public ComparisonUtil getComparisionutil() {
		return comparisionutil;
	}

	public void setComparisionutil(ComparisonUtil comparisionutil) {
		this.comparisionutil = comparisionutil;
	}

	public InterfaceUtil getInterfaceutil() {
		return interfaceutil;
	}

	public void setInterfaceutil(InterfaceUtil interfaceutil) {
		this.interfaceutil = interfaceutil;
	}

	public MessageConfiguration getMessageConfiguration() {
		return messageConfiguration;
	}

	public void setMessageConfiguration(
			MessageConfiguration messageConfiguration) {
		this.messageConfiguration = messageConfiguration;
	}

	public MessageDao getMessageDao() {
		return messageDao;
	}

	public void setMessageDao(MessageDao messageDao) {
		this.messageDao = messageDao;
	}
	
	public MessageProcess()
	{

	}
	
	public EmailNotification getEmailNotification() {
		return emailNotification;
		
	}

	public void setEmailNotification(EmailNotification emailNotification) {
		this.emailNotification = emailNotification;
	}
	

	public MessageProcess(MessageProcess messageProcess) {
		this.comparisionutil = messageProcess.comparisionutil;
		this.messageConfiguration = messageProcess.messageConfiguration;
		this.output = messageProcess.output;
		this.interfaceutil = messageProcess.interfaceutil;
		this.messageDao = messageProcess.messageDao;
		this.emailNotification=messageProcess.emailNotification;
	}

	public Message processMessage(Message input)  throws Exception {
		InputStream inputStream = null;
		try {
			 inputStream = getClass().getClassLoader().getResourceAsStream("ackcodes.properties");
			prop.load(inputStream);


			if (input != null) {

				Terser t = new Terser(input);
				String messageType = t.get("/MSH-9-1");
			
				boolean flag=comparisionutil.messageTypeComparison(messageType);

				if (flag == true) {

					logger.info("-----Processing " + messageType+ " Message type-----");

					messageMap=loadMsgTypeConfiguration( messageType,input);

					if(messageMap.containsValue(messageType)){
						persistMessage(input, messageMap);
					}

					logger.info("Message Map------->"+messageMap);
					output = getResponseMessage(input,
							prop.getProperty("AckAccept"),
							prop.getProperty("AcknowledgementAccept"));

				} 
			}

		} catch (HL7CustomException e) {
			logger.error("Error While Checking message Type");

			throw e;
		}finally{
			inputStream.close();
		}
		return output;
	}





	public boolean preProcess(String tablename, Map<String, String> resultMap,Message input)
			throws Exception {
		return true;
	}

	public boolean postProcess(String tablename, Map<String, String> resultMap)
			throws Exception {
		return true;
	}




	public boolean persistMessage(Message input, Map<String, Object> messageMap) throws Exception
	{
		// mttablecreation.createTable(messageType);
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream("adt.properties");
		prop.load(inputStream);

		String tablename = (String) messageMap.get("tablename");

		Map<String, Map> finalMap;
		try {
			finalMap = interfaceutil.getFieldValue(input,
					messageMap);


			Map<String, String> resultMap = finalMap.get("ResultMap");

			System.out.println("Result Map size--->"+resultMap.size());
			Map<String, String[]> repeatedValueMap = finalMap
					.get("RepeatedValueMap");
			Map<String, String> repSegtableName = finalMap
					.get("RepSegTableNameMap");

			//mpreprocess.preProcess(input,tablename,segList,resultMap);



			boolean flag = preProcess(tablename, resultMap,input);

			if (flag == true) {
				// method returning parent table Sequence ID
				int parseqId = messageDao.insertMessageRecord(tablename, resultMap);

				int repeatsegNo = Integer.parseInt(repSegtableName.get("RepeatedNo"));

				logger.info("Repeated Seg No -->" + repeatsegNo);

				if (repeatsegNo >= 2) {
					String repSegTableName = repSegtableName.get("RepSegTableName");
					messageDao.insertRepeatedSegmentValue(repSegTableName,
							repeatedValueMap, parseqId, tablename);
					logger.info("values inserted into repeated segment table");
				}

			}

			flag = postProcess(tablename, resultMap);

			return true;

		} catch (HL7CustomException e) {

			throw e;
		}finally{
			inputStream.close();
		}
	}



	// Generating Response Message
	public Message getResponseMessage(Message input, String ackmessage,
			String ackCode)  throws HL7CustomException,IOException {
		Message out = null;

		logger.info("\nGenerating Response Message");

		System.out.println("Ack Code  ==>" + ackCode);
		InputStream inputStream = null ;
		try {
			 inputStream = getClass().getClassLoader()
					.getResourceAsStream("ackcodes.properties");
			prop.load(inputStream);
			Terser t1 = new Terser(input);
			String version = t1.get("/MSH-12-1");
			// Make an ACK of the same version as the inbound message:
			String ackClassName = DefaultModelClassFactory
					.getVersionPackageName(version) + "message.ACK";
			Class ackClass = Class.forName(ackClassName);
			out = (Message) ackClass.newInstance();
			Terser t = new Terser(out);
			List<ResponseMessageBean> messageBeanList = messageConfiguration
					.getResponseMessageBeanList();

			t.set("/MSH-1", AckResponseType.Pipe.toString());
			t.set("/MSH-2", AckResponseType.EncodingCharacters.toString());
			t.set("/MSH-2", "^~\\&");

			for (ResponseMessageBean responseMsgBean : messageBeanList) {

				int fieldFrom = responseMsgBean.getFieldFrom();
				if (fieldFrom == 0) {
					t.set(responseMsgBean.getTerserPath(),
							responseMsgBean.getFieldValue());
				} else if (fieldFrom == 1) {
					if (responseMsgBean.getFieldName().equals("MessageID")) {
						t.set(responseMsgBean.getTerserPath(),
								AckResponseType.ResponseMessageType.toString()
								+ t1.get("/MSH-10"));
					} else if (responseMsgBean.getFieldName().equals(
							"AcknowledgementCode")) {
						t.set(responseMsgBean.getTerserPath(), ackCode);
					} else {
						t.set(responseMsgBean.getTerserPath(), ackmessage);
					}
				} else if (fieldFrom == 2) {
					t.set(responseMsgBean.getTerserPath(),
							t1.get(responseMsgBean.getFieldValue()));
				}
			}
			logger.info("\nResponse message :: \n" + out);

		} catch (Exception e) {

			logger.error("Error While Generating Response Messages");
			throw new HL7CustomException(e.getMessage(),"",prop.getProperty("AcknowledgementReject"));
		}finally{
			inputStream.close();
		}
		return out;
	}




	//Loading Message type configuration
	public Map<String, Object> loadMsgTypeConfiguration(String messageType,Message input)  throws HL7CustomException,IOException{
		messageMap = new HashMap<String, Object>();
		try {
			for (MessageBean mb : getMessageConfiguration()
					.getMessageConfigurationBean()) {

				// Checking request message type and from config to load
				// configurations


				if (mb.getMesssageType().equalsIgnoreCase(messageType)) {

					logger.info("\nMessageType--->" + messageType);

					System.out.println("MessageType---->"+messageType);

					// Loading the Message Type Configurations
					messageMap.put("messagetype", mb.getMesssageType());
					messageMap.put("tablename",
							mb.getMessageDataTable());
					messageMap.put("email", mb.getEmailNotification());
					messageMap.put("realtime",
							mb.getRealTimeNotfication());

					//persistMessage(input, messageMap);

					break;
				}// if ends

			}
		}  catch (Exception e) {
			throw new HL7CustomException(e.getMessage(),"",prop.getProperty("AcknowledgementReject"));
		}

		return messageMap;

	}




	// Generating ErrorResponse
	public Message getErrorResponse(String message,String errorMsg,String respType) throws HL7CustomException, IOException
	{
		
		logger.info("\nGenerating Error Response" + errorMsg);

		String[] segments1 = message.split("\r");
				
		String[] segments2 = segments1[0].split("\\|");

		String sendingApplication = segments2[3];
		String sendingFacility = segments2[4];
		String receivingApplication = segments2[5];
		String receivingFacility = segments2[6];
		String version = segments2[11];
		String messageId = segments2[9];

	
		Message out = null;
		InputStream inputStream = null;
		try {

			 inputStream = getClass().getClassLoader()
					.getResourceAsStream("ackcodes.properties");
			prop.load(inputStream);

			String ackClassName = DefaultModelClassFactory
					.getVersionPackageName(version) + "message.ACK";
			System.out.println("AckClassName--->" + ackClassName);

			System.out.println("AckCodes---->"
					+ prop.getProperty("AcknowledgementError"));
			Class ackClass = Class.forName(ackClassName);
			out = (Message) ackClass.newInstance();

			Terser t = new Terser(out);
			t.set("/MSH-1", AckResponseType.Pipe.toString());
			t.set("/MSH-2", AckResponseType.EncodingCharacters.toString());
			t.set("/MSH-3-1", receivingApplication);
			t.set("/MSH-4-1", receivingFacility);
			t.set("/MSH-5-1", sendingApplication);
			t.set("/MSH-6-1", sendingFacility);
			t.set("/MSH-9-1", "ACK");
			t.set("/MSH-10-1", AckResponseType.ResponseMessageType.toString()
					+ messageId);
			t.set("/MSH-12-1", version);
			t.set("/MSA-1-1",respType);
			t.set("/MSA-2-1", messageId);
			t.set("/MSA-3-1", errorMsg);


			/*
			if (errorMsg == null || errorMsg.contains("Primitive value")
					|| errorMsg.contains("org.postgresql.util.PSQLException")) {


			if (errorMsg == null || errorMsg.contains("ValidationException")  ||errorMsg.contains("Primitive value")
					|| errorMsg.contains("org.postgresql.util.PSQLException") || errorMsg.contains("Validation failed")) {

				t.set("/MSA-1-1", prop.getProperty("AcknowledgementError"));

			} else {

				t.set("/MSA-1-1", prop.getProperty("AcknowledgementReject"));
			}*/

		} catch (Exception e) {
			logger.error("Error While Generating Error Response");
			throw new HL7CustomException(e.getMessage(),"",prop.getProperty("AcknowledgementReject"));
		}finally{
		
				inputStream.close();
			
		}

		System.out.println("Error OutPut---->" + out);

		return out;
	}



	//Catching Exceptions from all Classes
	public Message getFinalException(String messageStrings,String errorMsg,String responseType) throws Exception{
		logger.error("\nError while parsing the incoming message :: \n");
		logger.error("Error Message  "+errorMsg);
		System.out.println("Error Message  "+errorMsg);
		Message out=null;
		InputStream inputStream = null;
		try {
			 inputStream = getClass().getClassLoader()
					.getResourceAsStream("adt.properties");
			prop.load(inputStream);

			out=getErrorResponse(messageStrings,errorMsg,responseType);
			Terser t=new Terser(out);
			String msgId=t.get("/MSA-2-1");
			String mrn = null, msgdate=null, temp=null;
			System.out.println("MESSAGE-->"+out);
			String[] segments1 = messageStrings.split("\r");
			for(String seg:segments1){
				if(seg.startsWith("PID")){
					String[] field=seg.split("\\|");
					 temp=field[Integer.parseInt(prop.getProperty("pid_mrn_field_positionNo"))];
					 mrn=temp.split("[^A-Za-z0-9]")[0];
					 break;
				}
			}
			messageDao.insertMessageAudit(messageStrings,out,mrn);
			msgdate = messageDao.getDate();
			emailNotification.sendMail(out.toString(),errorMsg, msgdate, mrn, msgId);
		} catch (HL7CustomException e) {
			logger.error("\nFinalException\n "+out);
			e.printStackTrace();
			throw e;
		}finally{
			 inputStream.close();
		}

		return out;
	}



}
