
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getStudyCalendarSummaryResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getStudyCalendarSummaryResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CalendarSummary" type="{http://velos.com/services/}calendarSummary" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getStudyCalendarSummaryResponse", propOrder = {
    "calendarSummary"
})
public class GetStudyCalendarSummaryResponse {

    @XmlElement(name = "CalendarSummary")
    protected CalendarSummary calendarSummary;

    /**
     * Gets the value of the calendarSummary property.
     * 
     * @return
     *     possible object is
     *     {@link CalendarSummary }
     *     
     */
    public CalendarSummary getCalendarSummary() {
        return calendarSummary;
    }

    /**
     * Sets the value of the calendarSummary property.
     * 
     * @param value
     *     allowed object is
     *     {@link CalendarSummary }
     *     
     */
    public void setCalendarSummary(CalendarSummary value) {
        this.calendarSummary = value;
    }

}
