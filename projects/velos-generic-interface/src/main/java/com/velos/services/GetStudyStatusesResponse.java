
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getStudyStatusesResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getStudyStatusesResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StudyStatuses" type="{http://velos.com/services/}studyStatuses" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getStudyStatusesResponse", propOrder = {
    "studyStatuses"
})
public class GetStudyStatusesResponse {

    @XmlElement(name = "StudyStatuses")
    protected StudyStatuses studyStatuses;

    /**
     * Gets the value of the studyStatuses property.
     * 
     * @return
     *     possible object is
     *     {@link StudyStatuses }
     *     
     */
    public StudyStatuses getStudyStatuses() {
        return studyStatuses;
    }

    /**
     * Sets the value of the studyStatuses property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyStatuses }
     *     
     */
    public void setStudyStatuses(StudyStatuses value) {
        this.studyStatuses = value;
    }

}
