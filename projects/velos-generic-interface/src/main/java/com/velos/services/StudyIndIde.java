
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for studyIndIde complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="studyIndIde">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="exempt" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="expandedAccess" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="expandedAccessType" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="grantor" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="holderType" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="indIdeNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="indIdeType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="parentIdentifier" type="{http://velos.com/services/}studyIdentifier" minOccurs="0"/>
 *         &lt;element name="programCode" type="{http://velos.com/services/}code" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "studyIndIde", propOrder = {
    "exempt",
    "expandedAccess",
    "expandedAccessType",
    "grantor",
    "holderType",
    "indIdeNumber",
    "indIdeType",
    "parentIdentifier",
    "programCode"
})
public class StudyIndIde
    extends ServiceObject
{

    protected boolean exempt;
    protected boolean expandedAccess;
    protected Code expandedAccessType;
    protected Code grantor;
    protected Code holderType;
    protected String indIdeNumber;
    protected String indIdeType;
    protected StudyIdentifier parentIdentifier;
    protected Code programCode;

    /**
     * Gets the value of the exempt property.
     * 
     */
    public boolean isExempt() {
        return exempt;
    }

    /**
     * Sets the value of the exempt property.
     * 
     */
    public void setExempt(boolean value) {
        this.exempt = value;
    }

    /**
     * Gets the value of the expandedAccess property.
     * 
     */
    public boolean isExpandedAccess() {
        return expandedAccess;
    }

    /**
     * Sets the value of the expandedAccess property.
     * 
     */
    public void setExpandedAccess(boolean value) {
        this.expandedAccess = value;
    }

    /**
     * Gets the value of the expandedAccessType property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getExpandedAccessType() {
        return expandedAccessType;
    }

    /**
     * Sets the value of the expandedAccessType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setExpandedAccessType(Code value) {
        this.expandedAccessType = value;
    }

    /**
     * Gets the value of the grantor property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getGrantor() {
        return grantor;
    }

    /**
     * Sets the value of the grantor property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setGrantor(Code value) {
        this.grantor = value;
    }

    /**
     * Gets the value of the holderType property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getHolderType() {
        return holderType;
    }

    /**
     * Sets the value of the holderType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setHolderType(Code value) {
        this.holderType = value;
    }

    /**
     * Gets the value of the indIdeNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndIdeNumber() {
        return indIdeNumber;
    }

    /**
     * Sets the value of the indIdeNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndIdeNumber(String value) {
        this.indIdeNumber = value;
    }

    /**
     * Gets the value of the indIdeType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndIdeType() {
        return indIdeType;
    }

    /**
     * Sets the value of the indIdeType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndIdeType(String value) {
        this.indIdeType = value;
    }

    /**
     * Gets the value of the parentIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link StudyIdentifier }
     *     
     */
    public StudyIdentifier getParentIdentifier() {
        return parentIdentifier;
    }

    /**
     * Sets the value of the parentIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyIdentifier }
     *     
     */
    public void setParentIdentifier(StudyIdentifier value) {
        this.parentIdentifier = value;
    }

    /**
     * Gets the value of the programCode property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getProgramCode() {
        return programCode;
    }

    /**
     * Sets the value of the programCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setProgramCode(Code value) {
        this.programCode = value;
    }

}
