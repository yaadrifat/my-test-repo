
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getStudyTeamMembersResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getStudyTeamMembersResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StudyTeamMembers" type="{http://velos.com/services/}studyTeamMembers" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getStudyTeamMembersResponse", propOrder = {
    "studyTeamMembers"
})
public class GetStudyTeamMembersResponse {

    @XmlElement(name = "StudyTeamMembers")
    protected StudyTeamMembers studyTeamMembers;

    /**
     * Gets the value of the studyTeamMembers property.
     * 
     * @return
     *     possible object is
     *     {@link StudyTeamMembers }
     *     
     */
    public StudyTeamMembers getStudyTeamMembers() {
        return studyTeamMembers;
    }

    /**
     * Sets the value of the studyTeamMembers property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyTeamMembers }
     *     
     */
    public void setStudyTeamMembers(StudyTeamMembers value) {
        this.studyTeamMembers = value;
    }

}
