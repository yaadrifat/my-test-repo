
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getStudySummaryResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getStudySummaryResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StudySummary" type="{http://velos.com/services/}studySummary" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getStudySummaryResponse", propOrder = {
    "studySummary"
})
public class GetStudySummaryResponse {

    @XmlElement(name = "StudySummary")
    protected StudySummary studySummary;

    /**
     * Gets the value of the studySummary property.
     * 
     * @return
     *     possible object is
     *     {@link StudySummary }
     *     
     */
    public StudySummary getStudySummary() {
        return studySummary;
    }

    /**
     * Sets the value of the studySummary property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudySummary }
     *     
     */
    public void setStudySummary(StudySummary value) {
        this.studySummary = value;
    }

}
