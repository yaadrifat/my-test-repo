package com.velos.eres.web.intercept;

import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.FormCompareUtilityDao;
import com.velos.eres.service.util.SessionMaint;
import com.velos.esch.service.util.StringUtil;
import com.velos.eres.web.formLib.FormLibJB;
import com.velos.eres.web.patStudyStat.PatStudyStatJB;
import com.velos.eres.service.util.DateUtil;

public class LINDFTFormInterceptorJB extends InterceptorJB {
	@Override
	public void handle(Object... args) {
		if (args == null || args.length < 1) { return; }
		HttpServletRequest request = (HttpServletRequest)args[0];
		HttpSession tSession = request.getSession(true);
		
		SessionMaint sessionmaint = new SessionMaint();
		if (!sessionmaint.isValidSession(tSession)){
			return;
		}		
		
		String userId = (String)tSession.getAttribute("userId");
		System.out.println("userId="+tSession.getAttribute("userId"));
		String studyId = request.getParameter("patStudyId");
		if (StringUtil.isEmpty(studyId) || StringUtil.stringToNum(studyId) <= 0){
			return;
		}
		
		String patientId = request.getParameter("formPatient");
		String patientStudyStatusDate = DateUtil.getCurrentDate();
		String ipAdd = (String)tSession.getValue("ipAdd");
		String usr = (String)tSession.getValue("userId");
		int screenFailureReason = 0;
		int saved = 0;
		
		String formDispLocation = request.getParameter("formDispLocation");
		if (StringUtil.isEmpty(formDispLocation)){
			return;
		}
		System.out.println("formDispLocation="+formDispLocation);
				
		if ("SP".equals(formDispLocation)){  
			String formId = "";
			String formPullDown = request.getParameter("formPullDown");
			System.out.println("formPullDown=" + formPullDown);
			if (StringUtil.isEmpty(formPullDown)){
				return;
			} else {
			 	StringTokenizer strTokenizer = new StringTokenizer(formPullDown,"*");
	 	     	formId = strTokenizer.nextToken();
	 	     	//entryChar = strTokenizer.nextToken();
				//numEntries = EJBUtil.stringToNum(strTokenizer.nextToken());
			}
			System.out.println("formPullDown="+formPullDown);
			
			if (StringUtil.isEmpty(formId)){
				return;
			} else {
				FormLibJB formLibB = new FormLibJB();
				formLibB.setFormLibId(StringUtil.stringToNum(formId));
				formLibB.getFormLibDetails();
				String formName = formLibB.getFormLibName();
				
				System.out.println("formName="+formName);
				if(StringUtil.isEmpty(formName) || !formName.contains("Eligibility with PI Override")) { //Form name "incExcTestForm"; replaced equals by contains
					return;
				}	
			
				FormCompareUtilityDao formCUDao = new FormCompareUtilityDao();
				formCUDao.setFormId(StringUtil.stringToNum(formId));
				formCUDao.getFormFieldMap();

				ArrayList arrColSysIds = formCUDao.getColSysIds();
				ArrayList arrColUIds = formCUDao.getColUIds();
				String colSystemId = "";
				String piOvrrideReq = "";
				String dataEntryDate = "";

				int colFound=0;
				for (int i =0; i < arrColUIds.size(); i++){
					if ("eligibility".equals(arrColUIds.get(i))){
						colSystemId = (String) arrColSysIds.get(i);
						System.out.println("colSystemId="+colSystemId);
						colFound++;
					}
					if ("piOvrrideReq".equals(arrColUIds.get(i))){
						piOvrrideReq = (String) arrColSysIds.get(i);
						System.out.println("piOvrrideReq="+request.getParameter("piOvrrideReq"));
						colFound++;
					}
					if ("er_def_date_01".equals(arrColUIds.get(i))){
						dataEntryDate = (String) arrColSysIds.get(i);
						System.out.println("dataEntryDate="+request.getParameter(dataEntryDate));
						dataEntryDate = request.getParameter(dataEntryDate);
						colFound++;
					}
					if (colFound == 3) break;
				}

				if (!StringUtil.isEmpty(colSystemId)){
					String colValue = request.getParameter(colSystemId);
					colValue = (StringUtil.isEmpty(colValue))? "" : colValue;
					
					PatStudyStatJB patStudyStatCurrentB = new PatStudyStatJB();
					patStudyStatCurrentB.getPatStudyCurrentStatus(studyId, patientId);
					int currentStatus = StringUtil.stringToNum((String)patStudyStatCurrentB.getPatStudyStat());
					System.out.println("currentStatus="+currentStatus);					

					CodeDao cdDao = new CodeDao();
					PatStudyStatJB patStudyStatB = new PatStudyStatJB();
					patStudyStatB.setStartDate(dataEntryDate);
					patStudyStatB.setStudyId(studyId);
					patStudyStatB.setPatientId(patientId);
	   				patStudyStatB.setCurrentStat("1");
	   				patStudyStatB.setIpAdd(ipAdd);
	   				patStudyStatB.setCreator(usr);
					patStudyStatB.setPatProtObj(null);
					
					if(colValue.equals("eligible")) {
						cdDao = new CodeDao();
						int eligCritMetCodeId = cdDao.getCodeId("patStatus", "patStatus_86"); //Eligibility criteria met
						if (eligCritMetCodeId <= 0 || currentStatus == eligCritMetCodeId){
							return;
						}
						System.out.println("eligCritMetCodeId"+eligCritMetCodeId);
						patStudyStatB.setPatStudyStat(""+eligCritMetCodeId);
						patStudyStatB.setPatStudyStatReason(""+screenFailureReason); 
						saved = patStudyStatB.setPatStudyStatDetails();
					} else if(colValue.equals("notEligible") && request.getParameter(piOvrrideReq)==null){
						cdDao = new CodeDao();
						int screenFailureCodeId1 = cdDao.getCodeId("patStatus", "scrfail"); //Screen failure
						screenFailureReason = cdDao.getCodeId("scrfail", "inclusionExclu"); //reason for failure pk
						if (screenFailureCodeId1 <= 0 || currentStatus == screenFailureCodeId1){
							return;
						}
						patStudyStatB.setPatStudyStat(""+screenFailureCodeId1);
						patStudyStatB.setPatStudyStatReason(""+screenFailureReason); 
						saved = patStudyStatB.setPatStudyStatDetails();
					}else if(colValue.equals("notEligible") && request.getParameter(piOvrrideReq)!=null){
						int screenFailureCodeId1 = cdDao.getCodeId("patStatus", "scrfail"); //Screen failure
						int screenFailureCodeId2 = cdDao.getCodeId("patStatus", "patStatus_10"); //PI override requested "patStatus_101"
						if (screenFailureCodeId1 <= 0 || screenFailureCodeId2 <= 0 || currentStatus == screenFailureCodeId2){
							return;
						}
						
						if(currentStatus != screenFailureCodeId1){
							cdDao = new CodeDao();
							screenFailureReason = cdDao.getCodeId("scrfail", "inclusionExclu"); //reason for failure pk
							patStudyStatB.setPatStudyStat(""+screenFailureCodeId1);
							patStudyStatB.setPatStudyStatReason(""+screenFailureReason); 
							saved = patStudyStatB.setPatStudyStatDetails();
						}
						screenFailureReason = 0;
						patStudyStatB = new PatStudyStatJB();
						patStudyStatB.setStartDate(dataEntryDate);
						patStudyStatB.setPatStudyStat(""+screenFailureCodeId2);
						patStudyStatB.setPatStudyStatReason(""); 
						patStudyStatB.setStudyId(studyId);
						patStudyStatB.setPatientId(patientId);
		   				patStudyStatB.setCurrentStat("1");
		   				patStudyStatB.setIpAdd(ipAdd);
		   				patStudyStatB.setCreator(usr);
						patStudyStatB.setPatProtObj(null);
						saved = patStudyStatB.setPatStudyStatDetails();
					}
					
					//write logic to add two statuses if failed
					//write logic to make sure duplicate statuses are not created
				}
			}
		}
	}
}