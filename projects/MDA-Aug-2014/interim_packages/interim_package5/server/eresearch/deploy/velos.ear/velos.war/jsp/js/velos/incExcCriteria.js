incExcCriteriaJS={
		inclusionFields: [],
		exclusionFields: [],
		piOvrrideReq: [],
		setSelectedValue: {},
		checkInclusionExclusion: {}
};

incExcCriteriaJS.setSelectedValue= function(selectObj, valueToSet) {
    for (var i = 0; i < selectObj.options.length; i++) {
        if (selectObj.options[i].text== valueToSet) {
            selectObj.options[i].selected = true;
            return;
        }
    }
};

incExcCriteriaJS.getSelectedValue= function(selectObj) {
    for (var i = 0; i < selectObj.options.length; i++) {
        if (selectObj.options[i].selected == true) {
            return selectObj.options[i].text;
        }
    }
};

incExcCriteriaJS.checkInclusionExclusion = function() {
	var include = true;
	var incYes = 0;

	for (var incFld = 0; incFld < (incExcCriteriaJS.inclusionFields).length; incFld++){
		var fldJSON = incExcCriteriaJS.inclusionFields[incFld];
		
		var fldOptions = document.getElementsByName(fldJSON.colSysId);
		if (fldOptions.length > 1){
			for (var optIndex = 0; optIndex < fldOptions.length; optIndex++){
				if (fldOptions[optIndex].checked){
					switch(fldOptions[optIndex].id){
					case 'Yes':
					case 'N/A':
						incYes++;
						break;
					case 'No':
						include = false;
						optIndex = fldOptions.length;
						incFld = (incExcCriteriaJS.inclusionFields).length;
						break; 
					default:
						break;
					}
				}
			}
		}
	}
	var excNo = 0;
	for (var excFld = 0; excFld < (incExcCriteriaJS.exclusionFields).length; excFld++){
		var fldJSON = incExcCriteriaJS.exclusionFields[excFld];
		
		var fldOptions = document.getElementsByName(fldJSON.colSysId);
		if (fldOptions.length > 0){
			for (var optIndex = 0; optIndex < fldOptions.length; optIndex++){
				if (fldOptions[optIndex].checked){
					switch(fldOptions[optIndex].id){
					case 'Yes':
						include = false;
						optIndex = fldOptions.length;
						excFld = (incExcCriteriaJS.exclusionFields).length;
						break;
					case 'No':
					case 'N/A':
						excNo++;
						break;
					case 'Request':
						//alert("Found!");
						break;
					default:
						break;
					}
				}
			}
		}
	}
	
	if (incYes == (incExcCriteriaJS.inclusionFields).length  && excNo == (incExcCriteriaJS.exclusionFields).length)
		return true;
	else 
		return false;
};

$j(document).ready(function(){
	formFieldMappingsJS.loadFormFieldMappings();
	
	var jsArrColumns = [];
	var indxInc = 0;
	var indxExc = 0;
	var indxPI = 0;
	
	var eligibility;
	
	jsArrColumns = formFieldMappingsJS.jsArrColumns;
	
	if(formFieldMappingsJS.getColumnSysId('eligibility')){
		eligibility = document.getElementById(formFieldMappingsJS.getColumnSysId('eligibility'));
	}
	
	for (oKey in jsArrColumns){
		var fldJSON = jsArrColumns[oKey];
		if (oKey.substr(0,4) == 'INC_'){
			incExcCriteriaJS.inclusionFields[indxInc] = jsArrColumns[oKey];
			++indxInc;
		}
		if (oKey.substr(0,4) == 'EXC_'){
			incExcCriteriaJS.exclusionFields[indxExc] = jsArrColumns[oKey];
			++indxExc;
		}

		var fldOptions = document.getElementsByName(fldJSON.colSysId);
		if (fldOptions.length > 1){
			for (var optIndex = 0; optIndex < fldOptions.length; optIndex++){
				var suffix = (fldOptions[optIndex].id).replace("/","")

				if (oKey.substr(0,4) == 'INC_'){
					fldOptions[optIndex].className = ("incRadio"+suffix);
				}
				if (oKey.substr(0,4) == 'EXC_'){
					fldOptions[optIndex].className = ("excRadio"+suffix);
				}
			}
		}
	}
	
	//PI Override checkbox and label
	var piOverrideReqCheck = document.getElementById("Request");
	var piOvrrideReq_label = document.getElementById(formFieldMappingsJS.getColumnSysId('piOvrrideReq')+'_id'); //Hardcoded '_id';
	//By default	
	var selValue = incExcCriteriaJS.getSelectedValue(eligibility);
	if(selValue == "Patient did not meet inclusion/exclusion criteria and is not eligible") {
		piOverrideReqCheck.parentNode.style.display = "inline";
		piOvrrideReq_label.style.display = "inline";
	} else {
		piOverrideReqCheck.parentNode.style.display = "none";
		piOvrrideReq_label.style.display = "none";
	}
	
	//INC radio
	$j("[class^=incRadio]").change(function(){
		if (incExcCriteriaJS.checkInclusionExclusion()) {
			incExcCriteriaJS.setSelectedValue(eligibility, "Patient met all inclusion/exclusion criteria and is eligible");
			piOverrideReqCheck.parentNode.style.display = "none";
			piOvrrideReq_label.style.display = "none";
		} else {
			incExcCriteriaJS.setSelectedValue(eligibility, "Patient did not meet inclusion/exclusion criteria and is not eligible");
			piOverrideReqCheck.parentNode.style.display = "inline";
			piOvrrideReq_label.style.display = "inline";
		}
	});
	
	//EXC radio
	$j("[class^=excRadio]").change(function(){
		if (incExcCriteriaJS.checkInclusionExclusion()) {
			incExcCriteriaJS.setSelectedValue(eligibility, "Patient met all inclusion/exclusion criteria and is eligible");
			piOverrideReqCheck.parentNode.style.display = "none";
			piOvrrideReq_label.style.display = "none";
		} else {
			incExcCriteriaJS.setSelectedValue(eligibility, "Patient did not meet inclusion/exclusion criteria and is not eligible");
			piOverrideReqCheck.parentNode.style.display = "inline";
			piOvrrideReq_label.style.display = "inline";
		}
	});
});