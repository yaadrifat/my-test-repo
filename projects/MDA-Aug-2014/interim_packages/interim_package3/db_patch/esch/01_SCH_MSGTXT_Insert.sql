set define off;

--STARTS INSERTING RECORD INTO SCH_MSGTXT TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from SCH_MSGTXT
    where MSGTXT_TYPE = 'lindPIOver';

  if (v_record_exists = 0) then
      INSERT INTO SCH_MSGTXT(PK_MSGTXT , MSGTXT_TYPE, MSGTXT_SHORT, 
      MSGTXT_LONG, 
      FK_ACCOUNT, RID, CREATOR, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON , 
      IP_ADD, MSGTXT_SUBJECT) 
      VALUES((select Max(PK_MSGTXT)+1 from SCH_MSGTXT),'lindPIOver','MSG short',
      'Dear Velos eResearch Member,

A request for PI Override has been submitted for:

Study Number: ~1~
Patient Study ID: ~2~
Date: ~3~

Sent from Velos eResearch.

------------------------------------------------------------------------------------------------------------------------------
This email transmission and any documents, files or previous email messages attached to it may contain information that is confidential or legally privileged. If you are not the intended recipient or a person responsible for delivering this transmission to the intended recipient you are hereby notified that you must not read this transmission and that any disclosure, copying, printing, distribution or use of this transmission is strictly prohibited. If you have received this transmission in error, please immediately notify the sender by telephone or return email and delete the original transmission and its attachments without reading or saving in any manner.
------------------------------------------------------------------------------------------------------------------------------',
      '','','','',SYSDATE,SYSDATE,
      '','PI Override Requested');
	commit;
  end if;
end;
/
