create or replace 
TRIGGER ER_PATSTUDYSTAT_AI_LINDFT0 
AFTER INSERT ON ER_PATSTUDYSTAT
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW 
declare
	v_piOverride NUMBER := 0;
	v_msgtemplate VARCHAR2(4000);
	v_msgsubject VARCHAR2(100);
	v_fparam VARCHAR2(4000);
	v_mail VARCHAR2(4000);

	v_studyNumber VARCHAR2(100) := null;
	v_patStudyId VARCHAR2(100) := null;
	v_pk_user NUMBER := null;
	v_sendEmail NUMBER := 1;
BEGIN
	select count(*) into v_piOverride
    from er_codelst
    where pk_codelst = :new.fk_codelst_stat
    and codelst_type='patStatus' and codelst_subtyp='patStatus_10';

	if (v_piOverride = 1) then
		begin
			select STUDY_NUMBER into v_studyNumber from er_study where pk_study = :new.fk_study;
			select PATPROT_PATSTDID into v_patStudyId from er_patprot where fk_study = :new.fk_study and fk_per = :new.fk_per and PATPROT_STAT = 1;
			select pk_user into v_pk_user from er_user where USR_LASTNAME='Email User' and USR_FIRSTNAME='PI-Override' and USR_TYPE='N';

			EXCEPTION WHEN NO_DATA_FOUND THEN
		        --RAISE_APPLICATION_ERROR (-20102, 'Exception occurred in ER_PATSTUDYSTAT_AI_LINDFT0 :'||SQLERRM);
				v_sendEmail := 0;
		end;
		if (v_sendEmail > 0) then
			begin
				v_msgtemplate   :=    PKG_COMMON.SCH_GETMAILMSG('lindPIOver');
				v_msgsubject := 'PI Override Requested';
		
				v_fparam :=   v_studyNumber||'~'|| v_patStudyId ||'~'|| TO_CHAR(:new.PATSTUDYSTAT_DATE, pkg_dateutil.f_get_dateformat()) ;
				v_mail :=    PKG_COMMON.SCH_GETMAIL(v_msgtemplate,v_fparam);

				INSERT INTO SCH_DISPATCHMSG (PK_MSG ,MSG_SENDON ,MSG_STATUS  ,
					MSG_TYPE, MSG_TEXT,FK_PAT,MSG_SUBJECT) values(SCH_DISPATCHMSG_SEQ1.NEXTVAL,SYSDATE ,0,'U',
					v_mail,NVL(v_pk_user,''),NVL(v_msgsubject,''));
			end;
		end if;
	end if;
END;
/