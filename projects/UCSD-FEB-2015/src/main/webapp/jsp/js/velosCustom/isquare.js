var isquare = {
	createBeachhead: {}
};

isquare.createBeachhead = function (args){
	var beacHeadId = '';
	var location = args.location;
	var isqBeachheads = args.isquareBeachheads;
	var params = args.isquareParams; 
	var locIndex = args.locationIndex;
	
	var beachHeadArray = null;
	try {
		var locationObj = JSON.parse(isqBeachheads);
		//console.log ('locationObj '+locationObj);
		beachHeadArray = locationObj[location];
		var beachHeadObj = beachHeadArray[locIndex];
		//console.log ('beachHeadArray '+beachHeadArray);
		beacHeadId = beachHeadObj["_id"]; 
	} catch (e) {
		console.log(isqBeachheads + ' ' + e);
	}

	console.log('beacHeadId '+ beacHeadId);
	if (beacHeadId && beacHeadId.length > 0){
		var isquareURL = args.isquareURL;	
		var sessUserId = params["loggedInUser"];
	
		var scriptTag = document.createElement('script');
		scriptTag.onload = function() {
			define('global', {
				bHeadId: beacHeadId,
				mainTarget: location,
				params: params,
				loggedInUser: sessUserId
			});
		};
		scriptTag.id = beacHeadId;
		scriptTag.type = "text/javascript";
		scriptTag.src = isquareURL+"/require";
		scriptTag.setAttribute("data-main", isquareURL+"/main");
		document.getElementsByTagName('head')[0].appendChild(scriptTag);
	}
};

isquare.addOneMoreBeachhead = function(args){
	var beacHeadId = '';
	var location = args.location;
	var isqBeachheads = args.isquareBeachheads;
	var params = args.isquareParams; 
	var locIndex = args.locationIndex;
	
	var beachHeadArray = null;
	try {
		var locationObj = JSON.parse(isqBeachheads);
		console.log ('locationObj '+locationObj);
		beachHeadArray = locationObj[location];
		var beachHeadObj = beachHeadArray[locIndex];
		console.log ('beachHeadArray '+beachHeadArray);
		beacHeadId = beachHeadObj["_id"]; 
	} catch (e) {
		console.log(isqBeachheads + ' ' + e);
	}

	console.log('beacHeadId '+ beacHeadId);
	if (beacHeadId && beacHeadId.length > 0){
		require.undef('global');
		define('global', {
		  bHeadId: beacHeadId,
	      mainTarget: location,
	      params: params
	    });
		require(['global', 'router/MainBeachHeadRouter'], function(global, MainBeachHeadRouter){
			console.log(global.bHeadId);
			if (global.bHeadId){
				MainBeachHeadRouter.initialize(global.bHeadId);
			}
		});
	}
};