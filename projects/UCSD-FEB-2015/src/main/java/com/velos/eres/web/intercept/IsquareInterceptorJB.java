package com.velos.eres.web.intercept;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import com.velos.eres.service.util.LC;

public class IsquareInterceptorJB extends InterceptorJB {
	public static final String ISQ_URL_NOT_CONFIGURED_STR = "[ISQ_URL_NOT_CONFIGURED]";
	public static final String ISQ_CRED_NOT_CONFIGURED_STR = "[ISQ_CRED_NOT_CONFIGURED]";
	public static final String UTF8_STR = "UTF-8";
	public static final String GET_STR = "GET";
	
	public void handle(Object...args) {
		if(ISQ_URL_NOT_CONFIGURED_STR.equals(LC.ISQ_URL) || ISQ_CRED_NOT_CONFIGURED_STR.equals(LC.ISQ_AUTH)){
			return;
		}
		try {
			StringBuffer fullUrl = new StringBuffer(LC.ISQ_URL);
			fullUrl.append("/login");
			URL url = new URL(fullUrl.toString());
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setUseCaches(false);
			conn.setConnectTimeout(15000);
			conn.setReadTimeout(15000);
			conn.setRequestMethod(GET_STR);
	        
			//Set basic authentication in the header
			String userPassword = LC.ISQ_AUTH;//"admin:password123";    
			String encoding = new sun.misc.BASE64Encoder().encode(userPassword.getBytes()); 
			conn.setRequestProperty("Authorization", "Basic " + encoding);
			conn.setRequestProperty("Product", "VelosER");

			String respStr = readResponseContent(conn);
			LC.ISQ_TOKEN = respStr;
			System.out.println("respStr="+respStr);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

    private String readResponseContent(HttpURLConnection conn) throws IOException {
        StringBuffer sb = new StringBuffer();
        char[] buffer = new char[1];
        try {
            if (conn.getInputStream() != null) {
                InputStreamReader reader = new InputStreamReader(conn.getInputStream(), UTF8_STR);
                int iRead = 0;
                while ( true ) {
                    iRead = reader.read(buffer);
                    if (iRead < 0) break;
                    sb.append(buffer);
                }
            }
        } catch (IOException e) {} 
        finally {
            try { conn.getInputStream().close(); } catch(IOException e) {}
        }
        return sb.toString();
    }
	
}
