package com.velos.integration.coverage.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.velos.integration.coverage.dao.CoverageDataDAOImpl;
import com.velos.integration.coverage.dao.CoverageDataFromCA;


public class TestCoverageDataDAO {
	
	private static Logger logger = Logger.getLogger(TestCoverageDataDAO.class.getName());

	private static ApplicationContext context;
	private static List<CoverageDataFromCA> list;

	public static void main(String[] args) {
	
		context = new ClassPathXmlApplicationContext("camel-config.xml");
		CoverageDataDAOImpl bean = (CoverageDataDAOImpl) context
				.getBean("coverageDataDAO");

		list = new ArrayList<CoverageDataFromCA>();
		try {
			if (bean != null) {

				list = bean.getAllCoverageDataFromCA();

				if (list != null || list.size() != 0) {
					//bean.updateCoverageTypeInCTMS(list);
				}

				logger.info("Method getAllCoverageDataFromCA's returned List size :  " + list.size());

			}

		} catch (Exception e) {
			logger.info("Exception caught in TestCoverageDataDAO : " + e.getMessage());
		}

	}

}
