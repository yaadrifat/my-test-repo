package com.velos.integration.espclient;

import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.integration.Message;
import org.springframework.integration.MessageChannel;
import org.springframework.integration.MessageHandlingException;
import org.springframework.integration.core.MessagingTemplate;
import org.springframework.integration.core.PollableChannel;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.integration.xml.source.DomSourceFactory;
import org.springframework.stereotype.Component;
import org.springframework.ws.soap.SoapFaultDetail;
import org.springframework.ws.soap.SoapFaultDetailElement;
import org.springframework.ws.soap.client.SoapFaultClientException;
import org.springframework.ws.soap.security.wss4j.Wss4jSecurityInterceptor;
import org.w3c.dom.NodeList;

import com.velos.integration.mapping.EndpointKeys;
import com.velos.integration.mapping.MapUtil;
import com.velos.integration.mapping.ProtocolKeys;
import com.velos.integration.mapping.VelosKeys;


@Component
public class VelosEspClient {
	
	public static final String HARDCODED_ORG = "Velos";
	//public static final String HARDCODED_ORG = "C C Cancer Center";
	public static final String HARDCODED_PATSTUDYSTAT = "ACTIVITY_EXITED";
	
	private static final String FAULT_STR = "Fault";
	
	private static final String studyRequestTemplate =
			"<ser:getStudySummary xmlns:ser=\"http://velos.com/services/\">" +
					"<StudyIdentifier>" +
					"   <studyNumber>%s</studyNumber>" +
					"</StudyIdentifier>" +
					"</ser:getStudySummary>";
	
	private static final String studyCalendarListRequestTemplate =
			"<ser:getStudyCalendarList xmlns:ser=\"http://velos.com/services/\">" +
					"<StudyIdentifier>" +
					"<studyNumber>%s</studyNumber>" +
					"</StudyIdentifier>" +
					"</ser:getStudyCalendarList>";
	
	private static final String studyCalendarRequestTemplate =
			"<ser:getStudyCalendar xmlns:ser=\"http://velos.com/services/\">" +
					"<CalendarIdentifier><PK>%s</PK></CalendarIdentifier>" +
					"</ser:getStudyCalendar>";
	
	private static final String studyPatientRequestTemplate =
			"<ser:getStudyPatients xmlns:ser=\"http://velos.com/services/\">"+
					"<StudyIdentifier>"+
					"<studyNumber>%s</studyNumber>"+
					"</StudyIdentifier>"+
					"</ser:getStudyPatients>";
	
	private static final String addStudyPatientStatusRequestTemplate =
			"<ser:addStudyPatientStatus xmlns:ser=\"http://velos.com/services/\">"+
					"<PatientIdentifier>"+
					"<organizationId><siteName>%s</siteName></organizationId>"+
					"<patientId>%s</patientId>"+
					"</PatientIdentifier>"+
					"<StudyIdentifier><studyNumber>%s</studyNumber></StudyIdentifier>"+
					"<PatientStudyStatus>"+
					"<currentStatus>true</currentStatus>"+
					"<enrollingSite><siteName>%s</siteName></enrollingSite>"+
					"<patientStudyId>%s</patientStudyId>"+
					"<enrolledBy><userLoginName>ihuang</userLoginName></enrolledBy>"+
					"<status><code>%s</code><type>patStatus</type></status>"+
					"<statusDate>%s</statusDate>"+
					"</PatientStudyStatus>"+
					"</ser:addStudyPatientStatus>";
	
	private static final String pendingEnrollPatientRequestTemplate =
			"<ser:enrollPatientToStudy xmlns:ser=\"http://velos.com/services/\">"+
					"<PatientIdentifier>"+
					"<organizationId><siteName>%s</siteName></organizationId>"+
					"<patientId>%s</patientId>"+
					"</PatientIdentifier>"+
					"<StudyIdentifier><studyNumber>%s</studyNumber></StudyIdentifier>"+
					"<PatientEnrollmentDetails>"+
					"<currentStatus>true</currentStatus>"+
					"<enrollingSite><siteName>%s</siteName></enrollingSite>"+
					"<patientStudyId>%s</patientStudyId>"+
					"<status><code>enroll_pending</code><type>patStatus</type></status>"+
					"<statusDate>%s</statusDate>"+
					"</PatientEnrollmentDetails>"+
					"</ser:enrollPatientToStudy>";
	
	private static final String patDemogSearchPatientRequestTemplate1 =
			"<ser:searchPatient xmlns:ser=\"http://velos.com/services/\"><PatientSearch>"
					+ "<exactSearch>true</exactSearch>"
					+ "<patFirstName>%s</patFirstName><patLastName>%s</patLastName>"
					+ "<patOrganization><siteName>%s</siteName></patOrganization>"
					+ "<patientID>%s</patientID>"
					+"</PatientSearch></ser:searchPatient>";

	private static final String patDemogSearchPatientRequestTemplate2 =
			"<ser:searchPatient xmlns:ser=\"http://velos.com/services/\"><PatientSearch>"
					+ "<exactSearch>true</exactSearch>"
					+ "<patDateofBirth>%s</patDateofBirth>"
					+ "<patFirstName>%s</patFirstName><patLastName>%s</patLastName>"
					+ "<patOrganization><siteName>%s</siteName></patOrganization>"
					+ "<patientID>%s</patientID>"
					+"</PatientSearch></ser:searchPatient>";
    
    public static void main(String[] args) throws Exception {
    	VelosEspClient client = new VelosEspClient();
    	Map<VelosKeys, String> requestMap = new HashMap<VelosKeys, String>();
    	requestMap.put(ProtocolKeys.StudyNumber, "Adam-1");
    	
    	// Test study summary
    	client.testStudySummary(requestMap);
    	
    	// Test study patients
    	//client.testStudyPatient(requestMap);
    	
    	// Test study calendar
    	//client.testStudyCalendar(requestMap);
    	
    	// Test add new patient study status
    	//requestMap.put(ProtocolKeys.PatientId, "9998");
    	//requestMap.put(ProtocolKeys.SiteName, HARDCODED_ORG);
    	//String patStudyStat = EpicMaps.getEpicToVelosPatStudyStatMap().get(HARDCODED_PATSTUDYSTAT);
    	//if (null != patStudyStat){
	    // 	requestMap.put(ProtocolKeys.StudyPatStatus, patStudyStat);
	    //	client.testAddStudyPatientStatus(requestMap);
    	//}

    	// Test enroll patient to study
    	//requestMap.put(ProtocolKeys.PatientId, "444");
    	//requestMap.put(ProtocolKeys.SiteName, HARDCODED_ORG);
    	//client.testEnrollPatient(requestMap);
    	
    	// Test search patient
    	//requestMap.put(ProtocolKeys.Dob, "19861202");
    	//requestMap.put(ProtocolKeys.LastName, "Krystal");
    	//requestMap.put(ProtocolKeys.PatientID, "444");
    	//requestMap.put(ProtocolKeys.SiteName, HARDCODED_ORG);
    	//client.testSearchPatient(requestMap);
    }
    
    private void testStudySummary(Map<VelosKeys, String> requestMap) {
    	Map<VelosKeys, Object> dataMap = handleRequest(VelosEspMethods.StudyGetStudySummary, requestMap);
    	for (VelosKeys key : dataMap.keySet()) {
    		System.out.println("key="+key+"; value="+dataMap.get(key));
    	}
    }
    
    private void testAddStudyPatientStatus(Map<VelosKeys, String> requestMap) {
		handleRequest(VelosEspMethods.StudyPatAddStudyPatientStatus, requestMap);
	}

    private void testEnrollPatient(Map<VelosKeys, String> requestMap) {
		handleRequest(VelosEspMethods.StudyPatEnrollPatientToStudy, requestMap);
	}
	
	@SuppressWarnings("unchecked")
    private void testStudyPatient(Map<VelosKeys, String> requestMap) {
    	Map<VelosKeys, Object> dataMap = handleStudyPatientRequest(requestMap);
		List<Map<VelosKeys, Object>> studyPatientMap = (List<Map<VelosKeys, Object>>)dataMap.get(ProtocolKeys.StudyPatientList);
    	for (Map<VelosKeys, Object> map : studyPatientMap) {
    		System.out.println("study patient ID: "+map.get(ProtocolKeys.StudyPatId));
    	}
    }
    
    @SuppressWarnings("unchecked")
	private void testStudyCalendar(Map<VelosKeys, String> requestMap) {
    	Map<VelosKeys, Object> studyDataMap = handleStudyRequest(requestMap);
    	System.out.println("Study title: "+studyDataMap.get(ProtocolKeys.Title));
    	Map<VelosKeys, Object> calendarListDataMap = handleStudyCalendarListRequest(requestMap);
		List< Map<VelosKeys, Object> > calendarIdList = (List< Map<VelosKeys, Object> >)
				calendarListDataMap.get(ProtocolKeys.CalendarIdList);
    	for (Map<VelosKeys, Object> calendarMap : calendarIdList) {
    		System.out.println("Cal_Id="+calendarMap.get(ProtocolKeys.CalendarId)
    				+" Cal_Name="+calendarMap.get(ProtocolKeys.CalendarName));
    		requestMap.put(ProtocolKeys.CalendarId,
    				String.valueOf(calendarMap.get(ProtocolKeys.CalendarId)));
    		Map<VelosKeys, Object> calendarDataMap = handleStudyCalendarRequest(requestMap);
			List<Map<VelosKeys, Object>> visitList = (List<Map<VelosKeys, Object>>)
					calendarDataMap.get(ProtocolKeys.VisitList);
    		for (Map<VelosKeys, Object> visitMap : visitList) {
    			System.out.println("Visit_Name="+visitMap.get(ProtocolKeys.VisitName));
    			List<Map<VelosKeys, Object>> eventList = (List<Map<VelosKeys, Object>>)
    					visitMap.get(ProtocolKeys.EventList);
    			if (eventList == null) { continue; }
    			for (Map<VelosKeys, Object> eventMap : eventList) {
    				System.out.println("Event_name="+eventMap.get(ProtocolKeys.EventName));
    				System.out.println("Event_coverage="+eventMap.get(ProtocolKeys.CoverageType));
    			}
    		}
    	}
    }
    
    private void testSearchPatient(Map<VelosKeys, String> requestMap) {
    	Map<VelosKeys, Object> dataMap = handleRequest(VelosEspMethods.PatDemogSearchPatient, requestMap);
    	for (VelosKeys key : dataMap.keySet()) {
    		System.out.println("key="+key+"; value="+dataMap.get(key));
    	}
    }
	
	public Map<VelosKeys, Object> handleRequest(VelosEspMethods method, Map<VelosKeys, String> requestMap)  {
    	if (requestMap == null) { return null; }
		MessageChannel channel = null;
		String body = null;
        @SuppressWarnings("resource")
		ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("velos-espclient-req.xml");
        ArrayList<String> argList = new ArrayList<String>();
		
        // Call Velos WS
		switch(method) {
		case StudyGetStudySummary:
	        channel = context.getBean("studyRequests", MessageChannel.class);
	        body = String.format(studyRequestTemplate, requestMap.get(ProtocolKeys.StudyNumber));
			break;
		case StudyCalGetStudyCalendar:
	        channel = context.getBean("studyCalendarRequests", MessageChannel.class);
	        body = String.format(studyCalendarRequestTemplate, requestMap.get(ProtocolKeys.CalendarId));
			break;
		case StudyCalGetStudyCalendarList:
	        channel = context.getBean("studyCalendarRequests", MessageChannel.class);
	        body = String.format(studyCalendarListRequestTemplate, requestMap.get(ProtocolKeys.StudyNumber));
			break;
		case StudyPatGetStudyPatients:
	        channel = context.getBean("studyPatientRequests", MessageChannel.class);
	        body = String.format(studyPatientRequestTemplate, requestMap.get(ProtocolKeys.StudyNumber));
			break;
		case StudyPatAddStudyPatientStatus:
	        channel = context.getBean("studyPatientRequests", MessageChannel.class);
	        argList.add(HARDCODED_ORG);
			argList.add(requestMap.get(ProtocolKeys.PatientId));
			argList.add(requestMap.get(ProtocolKeys.StudyNumber));
	        argList.add(HARDCODED_ORG);
			argList.add(requestMap.get(ProtocolKeys.PatientId)+"-"+((int)(Math.random()*100)));
			argList.add(requestMap.get(ProtocolKeys.StudyPatStatus));
			argList.add(this.getFormattedCurrentDate());
	        body = String.format(addStudyPatientStatusRequestTemplate, argList.toArray());
			break;
		case StudyPatEnrollPatientToStudy:
	        channel = context.getBean("studyPatientRequests", MessageChannel.class);
	        argList.add(HARDCODED_ORG);
			argList.add(requestMap.get(ProtocolKeys.PatientId));
			argList.add(requestMap.get(ProtocolKeys.StudyNumber));
	        argList.add(HARDCODED_ORG);
			argList.add(requestMap.get(ProtocolKeys.PatientId)+"-"+((int)(Math.random()*100)));
			argList.add(this.getFormattedCurrentDate());
	        body = String.format(pendingEnrollPatientRequestTemplate, argList.toArray());
			break;
		case PatDemogSearchPatient:
			channel = context.getBean("patientDemographicsRequests", MessageChannel.class);
			String formattedDob = MapUtil.formatDateOfBirth(requestMap.get(ProtocolKeys.Dob));
			if (formattedDob != null) {
				argList.add(formattedDob);
			}
			argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.FirstName)));
			argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.LastName)));
			//argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.SiteName)));
			argList.add(MapUtil.nullToEmpty(HARDCODED_ORG));
			argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.PatientID)));
			
			
			body = String.format(formattedDob == null ? patDemogSearchPatientRequestTemplate1 : 
					patDemogSearchPatientRequestTemplate2, argList.toArray());
			break;
		default:
			break;
		}
		
		System.out.println("Sent:"+body.toString());
        MessagingTemplate messagingTemplate = new MessagingTemplate();
        Message<?> message = null;
        try {
        	message = messagingTemplate.sendAndReceive(
                channel, MessageBuilder.withPayload(body).build());
        } catch(MessageHandlingException e) {
        	message = extractOperationException(e);
        	DOMSource source = (DOMSource)new DomSourceFactory().createSource(message.getPayload());
        	if (FAULT_STR.equals(source.getNode().getLocalName())) {
        		NodeList nodeList = source.getNode().getChildNodes();
        		for (int iX = 0; iX < nodeList.getLength(); iX++) {
        			if (ProtocolKeys.FaultString.toString().equals(nodeList.item(iX).getLocalName())) {
        				Map<VelosKeys, Object> retMap = new HashMap<VelosKeys, Object>();
        				retMap.put(ProtocolKeys.FaultString, nodeList.item(iX).getTextContent());
        				return retMap;
        			}
        		}
        	}
        }
        
        System.out.println("Got :"+message.getPayload());
        
        // Transform XML to object to Map
    	return transformMessageToMap(message, requestMap.get(EndpointKeys.Endpoint));
	}

	private final TransformerFactory tfactory = TransformerFactory.newInstance();
	
	private Message<?> extractOperationException(MessageHandlingException messageException) {
    	SoapFaultClientException se = (SoapFaultClientException) messageException.getCause();
    	SoapFaultDetail detail = se.getSoapFault().getFaultDetail();
    	StringWriter writer = new StringWriter();
    	Source source = null;
        Result result = new StreamResult(writer);
        if (detail == null || detail.getDetailEntries() == null) {
        	source = se.getSoapFault().getSource(); 
        } else {
            SoapFaultDetailElement elem = detail.getDetailEntries().next();
        	source = elem.getSource();
        }
        Transformer xform = null;
        try {
        	xform = tfactory.newTransformer();
            xform.transform(source, result);
        } catch(Exception e1) {
        	e1.printStackTrace();
        }
        System.out.println(writer.toString());
        Message<?> message = MessageBuilder.withPayload(writer.toString()).build();
		return message;
	}
	
    public Map<VelosKeys, Object> handleStudyRequest(Map<VelosKeys, String> requestMap) {
    	return handleRequest(VelosEspMethods.StudyGetStudySummary, requestMap);
    }
    
    public Map<VelosKeys, Object> handleStudyCalendarRequest(Map<VelosKeys, String> requestMap) {
    	return handleRequest(VelosEspMethods.StudyCalGetStudyCalendar, requestMap);
    }
    
    public Map<VelosKeys, Object> handleStudyPatientRequest(Map<VelosKeys, String> requestMap) {
    	return handleRequest(VelosEspMethods.StudyPatGetStudyPatients, requestMap);
    }
    
    public Map<VelosKeys, Object> handleStudyCalendarListRequest(Map<VelosKeys, String> requestMap) {
    	return handleRequest(VelosEspMethods.StudyCalGetStudyCalendarList, requestMap);
    }
    
    private Map<VelosKeys, Object> transformMessageToMap(Message<?> message, String endpoint) {
        @SuppressWarnings("resource")
		ClassPathXmlApplicationContext context = 
				new ClassPathXmlApplicationContext("velos-espclient-resp.xml");
        MessageChannel channelXml = context.getBean("input-xml", MessageChannel.class);
        PollableChannel output = context.getBean("output", PollableChannel.class);
        
        // Transform XML to object
        channelXml.send(message);
        Message<?> reply = output.receive();
        
        // Transform object to map
        VelosMessageHandler messageHandler = context.getBean(VelosMessageHandler.class);
        messageHandler.setEndpoint(endpoint);
        messageHandler.handleMessage(reply);
        return messageHandler.getDataMap();
    }
    
    private String getFormattedCurrentDate() {
    	SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    	return f.format(Calendar.getInstance().getTime());
    }
    
    @Bean
    public Wss4jSecurityInterceptor wssBean() {
    	Wss4jSecurityInterceptor wssBean = new Wss4jSecurityInterceptor();
    	wssBean.setSecurementActions("UsernameToken");
    	wssBean.setSecurementUsername("ihuang");
    	wssBean.setSecurementPassword("velos123");
    	wssBean.setSecurementPasswordType("PasswordText");
    	return wssBean;
    }
    
}
