package com.velos.integration.coverage.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import com.velos.integration.coverage.dao.CoverageDataDAO;

public class CoverageDataDAOImpl implements CoverageDataDAO {

	private static Logger logger = Logger.getLogger(CoverageDataDAOImpl.class
			.getName());

	private JdbcTemplate jdbcTemplate;
	List<CoverageDataFromCA> list = new ArrayList<CoverageDataFromCA>();

	public void setEschDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		logger.info("Datasource and jdbcTemplate created successfully");
	}

	@Override
	public List<CoverageDataFromCA> getAllCoverageDataFromCA() {

		String sql = "SELECT * FROM COVERAGE_ANALYSIS_DATA ";

		if (this.jdbcTemplate != null) {

			@SuppressWarnings("unchecked")
			List<Map<String, Object>> rows = this.jdbcTemplate
			.queryForList(sql);
			for (Map<String, Object> resultSet : rows) {

				CoverageDataFromCA covCa = new CoverageDataFromCA();

				covCa.setPk((BigDecimal) resultSet.get("PK"));
				covCa.setStudyNumber((String) resultSet.get("STUDY_NUMBER"));
				covCa.setCalendarName((String) resultSet.get("CALENDAR_NAME"));
				covCa.setCptCode((String) resultSet.get("CPT_CODE"));
				covCa.setEventName((String) resultSet.get("EVENT_NAME"));
				covCa.setVisitName((String) resultSet.get("VISIT_NAME"));
				covCa.setReason((String) resultSet.get("REASON"));
				covCa.setCoverageType((String) resultSet.get("COVERAGE_TYPE"));
				covCa.setCoverageNotes((String) resultSet
						.get("COVERAGES_NOTES"));
				covCa.setSocCharge((BigDecimal) resultSet.get("SOC_CHARGE"));
				covCa.setPrsCharge((BigDecimal) resultSet.get("PRS_CHARGE"));
				covCa.setHccCharge((BigDecimal) resultSet.get("HCC_CHARGE"));
				covCa.setModifiedDate((Timestamp) resultSet
						.get("MODIFIED_DATE"));

				list.add(covCa);
			}
		}

		return list;
	}

	/*
	 * This method is used to update coverage_type field in event_assoc table
	 * for given study_number, calendar_name, visit_name and event_name. This
	 * method will receive information from CA apllication's database table.
	 */

	@Override
	public void updateCoverageTypeInCTMS(List<CoverageDataFromCA> objList) {

		for (int j = 0; j < objList.size(); j++) {

			if (this.jdbcTemplate != null) {

				try {

					/*
					 * This query checks if study_name and calendar_name is
					 * associated and calendar status is either
					 * "work in progress"(W) or "offline for editing"(O) or
					 * "Reactivated"(R). If study_number or calendar_name is not
					 * present in CTMS database it will log message saying
					 * "study_number or calendar_name is not present in CTMS database"
					 */

					String sql = "select es.study_number, es.pk_study, ea.chain_id, ea.name, sc.codelst_subtyp"
							+ " from er_study es,  ESCH.event_assoc ea, sch_codelst sc"
							+ " where es.study_number ="
							+ "'"
							+ (objList.get(j)).getStudyNumber()
							+ "'"
							+ " and es.pk_study = ea.chain_id "
							+ " and ea.name = "
							+ "'"
							+ (objList.get(j)).getCalendarName()
							+ "'"
							+ " and ea.event_calassocto='P'"
							+ "  and ea.fk_codelst_calstat = sc.pk_codelst";

					@SuppressWarnings("unchecked")
					List<Map<String, Object>> rows = jdbcTemplate
					.queryForList(sql);

					if (rows.size() == 0) {
						logger.info("Study_number or calendar_name is not present in CTMS database");
					} else {

						Timestamp ts = new Timestamp(System.currentTimeMillis());
						String insertSql = "INSERT INTO ESCH.coverage_log_data VALUES ("
								+ (objList.get(j)).getPk()
								+ ","
								+ "'Calendar Status is active or deactivated..can not update coverage_type field'"
								+ ", "
								+ "to_timestamp('"
								+ ts
								+ "' , 'yyyy/MM/dd hh24:mi:ss.FF')" + ")";

						for (Map<String, Object> rs : rows) {
							logger.info("Status of calendar is : " + "  "
									+ (String) rs.get("study_number") + "  "
									+ (String) rs.get("name") + "  "
									+ (String) rs.get("codelst_subtyp"));

							String code = ((String) rs.get("codelst_subtyp"))
									.trim();

							if (code.equals("A") || code.equals("D")) {
								logger.info("Can not update coverage type because calendar status is active or deactived");
								int result = jdbcTemplate.update(insertSql);
								logger.info("Result from insert query is : "
										+ result);
							} else {

								String selectSql = " select ea.event_id"
										+ " from ESCH.event_assoc ea, sch_protocol_visit spv"
										+ " where chain_id = ("
										+ " select ea.event_id"
										+ " from er_study es,  ESCH.event_assoc ea"
										+ " where es.study_number ="
										+ "'"
										+ (objList.get(j)).getStudyNumber()
										+ "'"
										+ " and es.pk_study = ea.chain_id"
										+ " and ea.name = "
										+ "'"
										+ (objList.get(j)).getCalendarName()
										+ "'"
										+ " and ea.event_calassocto='P')"
										+ " and fk_visit = spv.pk_protocol_visit"
										+ " and spv.visit_name=" + "'"
										+ (objList.get(j)).getVisitName() + "'"
										+ " and ea.name= " + "'"
										+ (objList.get(j)).getEventName() + "'";

								int eventId = this.jdbcTemplate
										.queryForInt(selectSql);

								logger.info("EventId is : " + eventId);

								int coverageType = 0;
								if (((objList.get(j)).getCoverageType())
										.equals("q0")) {
									coverageType = 290;
								} else if (((objList.get(j)).getCoverageType())
										.equals("q1")) {
									coverageType = 290;
								} else if (((objList.get(j)).getCoverageType())
										.equals("r0")) {
									coverageType = 290;
								}

								if (eventId != 0) {
									String updateSql = " update esch.event_assoc"
											+ " set fk_codelst_covertype= "
											+ coverageType
											+ " where event_id = " + eventId;

									Timestamp ts1 = new Timestamp(
											System.currentTimeMillis());

									int result1 = this.jdbcTemplate
											.update(updateSql);
									logger.info("Record updated successfully : "
											+ result1);

									String insertSql1 = "INSERT INTO ESCH.coverage_log_data VALUES ("
											+ (objList.get(j)).getPk()

											+ ","
											+ "'Record updated successfully for this eventId : "
											+ eventId
											+ "'"
											+ ", "
											+ "to_timestamp('"
											+ ts1
											+ "' , 'yyyy/MM/dd hh24:mi:ss.FF')"
											+ ")";

									int result2 = jdbcTemplate
											.update(insertSql1);
									logger.info("Result from insert query for log table : "
											+ result2);

								} 
							}
						}

					}

				} catch (Exception e) {
					
					Timestamp ts2 = new Timestamp(
							System.currentTimeMillis());
					logger.info("Record does not exist with given study_number, calendar_name, visit_name and event_name");
					String insertSql2 = "INSERT INTO ESCH.coverage_log_data VALUES ("
							+ (objList.get(j)).getPk()

							+ ","
							+ "'Record does not exist with given combination study_number, calendar_name, visit_name and event_name '"
							+ ", "
							+ "to_timestamp('"
							+ ts2
							+ "' , 'yyyy/MM/dd hh24:mi:ss.FF')"
							+ ")";

					int result3 = jdbcTemplate
							.update(insertSql2);
					
					logger.info("Result from insert query for log table : "
							+ result3);					
					
				}
			}

		}

	}
}

	