package com.velos.integration.espclient;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class XmlConfiguration {
	
	@Bean
    public Jaxb2Marshaller marshaller() {
		Jaxb2Marshaller m = new Jaxb2Marshaller();
		String[] packagesToScan = {"com.velos.services"};
		m.setPackagesToScan(packagesToScan);
        return m;
    }

}
