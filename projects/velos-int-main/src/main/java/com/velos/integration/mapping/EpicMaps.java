package com.velos.integration.mapping;

import java.util.HashMap;
import java.util.Map;

public class EpicMaps {
	private static final HashMap<String, String> velosToEpicPatStudyStatMap;
	private static final HashMap<String, String> epicToVelosPatStudyStatMap;
	
	static
    {
		velosToEpicPatStudyStatMap = new HashMap<String, String>();
		velosToEpicPatStudyStatMap.put("enrolled", "PROCESS_ACTIVE");
		velosToEpicPatStudyStatMap.put("scrfail", "PROCESS_SUSPENDED");
		velosToEpicPatStudyStatMap.put("sComplete", "PROCESS_COMPLETED");
		velosToEpicPatStudyStatMap.put("offstudy", "PROCESS_EXITED");
		velosToEpicPatStudyStatMap.put("offtreat", "ACTIVITY_EXITED");
		
		epicToVelosPatStudyStatMap = new HashMap<String, String>();
		epicToVelosPatStudyStatMap.put("PROCESS_ACTIVE", "enrolled");
		epicToVelosPatStudyStatMap.put("PROCESS_SUSPENDED", "scrfail");
		epicToVelosPatStudyStatMap.put("PROCESS_COMPLETED", "sComplete");
		epicToVelosPatStudyStatMap.put("PROCESS_EXITED", "offstudy");
		epicToVelosPatStudyStatMap.put("ACTIVITY_EXITED", "offtreat");
    }
	
	public static Map<String, String> getVelosToEpicPatStudyStatusMap() {
		return velosToEpicPatStudyStatMap;
	}
	
	public static Map<String, String> getEpicToVelosPatStudyStatMap() {
		return epicToVelosPatStudyStatMap;
	}
}
