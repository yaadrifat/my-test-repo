package com.velos.integration.messaging;
import java.util.HashMap;
import java.util.Map;

import com.velos.integration.espclient.VelosEspClientForMsgClient;
import com.velos.integration.mapping.ProtocolKeys;
import com.velos.integration.mapping.VelosKeys;
import com.velos.services.PatientStudyStatusIdentifier;
import com.velos.services.SimpleIdentifier;

public class StudyMessageProcessor {
	public void processStudyMessage(CustomHandler handler) {
		SimpleIdentifier simpleIdentifier  = (SimpleIdentifier) handler.getParentOID().get(IdentifierConstants.SIMPLE_IDENTIFIER); 
		System.out.println("In StudyMessageProcessor: OID="+simpleIdentifier.getOID());
	 
		loop_a: for(Change change: handler.getChanges()) {
			if(change == null) { continue; }
			String module = change.getModule();
			String action = change.getAction(); 

			if(module.equalsIgnoreCase("patstudy_status")) {
				PatientStudyStatusIdentifier studyStatusIdentifier = (PatientStudyStatusIdentifier) change.getIdentifier(); 
				String statusCodeDesc = change.getAdditionalTags().get("statusCodeDesc");
				String statusSubType = change.getAdditionalTags().get("statusSubType");
				String timeStamp = change.getTimeStamp();
				System.out.println("    timeStamp="+timeStamp
						+" statusSubType="+statusSubType+" statusCodeDesc="+statusCodeDesc);
				break loop_a; 
			}
		}
		
		//TODO - Call VelosEspClientForMsgClient to collect more info
		Map<VelosKeys, String> requestMap = new HashMap<VelosKeys, String>();
		requestMap.put(ProtocolKeys.OID, simpleIdentifier.getOID());
		VelosEspClientForMsgClient espClient = new VelosEspClientForMsgClient();
		Map<VelosKeys, Object> resultMap = new HashMap<VelosKeys, Object>();
		//resultMap.putAll(espClient.testPatStudyRequest(requestMap));  // <= not yet moved from Spring WS to CXF
		
		//TODO - Call Customer WS to send info
		
	}
}
