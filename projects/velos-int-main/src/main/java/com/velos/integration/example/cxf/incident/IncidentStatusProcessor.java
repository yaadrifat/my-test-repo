package com.velos.integration.example.cxf.incident;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class IncidentStatusProcessor implements Processor {
	
    public void process(Exchange exchange) throws Exception {
        // set reply
        OutputStatusIncident output = new OutputStatusIncident();
        output.setStatus("IN PROGRESS...");
        exchange.getOut().setBody(output);
    }

}
