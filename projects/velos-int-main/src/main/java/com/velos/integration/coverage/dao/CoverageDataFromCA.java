package com.velos.integration.coverage.dao;

import java.math.BigDecimal;
import java.sql.Timestamp;

/*
 * This class represents sql table data that will be populated by 
 * CA(MD Anderson) for coverage analysis field update.
 */
public class CoverageDataFromCA {

	private BigDecimal pk;	
	private String studyNumber;
	private String calendarName;
	private String visitName;
	private String eventName;
	private String cptCode;
	private String coverageType;
	private String coverageNotes;
	private String reason;
	private BigDecimal prsCharge;
	private BigDecimal socCharge;
	private BigDecimal hccCharge;	
	private Timestamp modifiedDate;
	
	

	public Timestamp getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	
	public BigDecimal getPk() {
		return pk;
	}

	public void setPk(BigDecimal pk) {
		this.pk = pk;
	}

	public BigDecimal getPrsCharge() {
		return prsCharge;
	}

	public void setPrsCharge(BigDecimal prsCharge) {
		this.prsCharge = prsCharge;
	}

	public BigDecimal getSocCharge() {
		return socCharge;
	}

	public void setSocCharge(BigDecimal socCharge) {
		this.socCharge = socCharge;
	}

	public BigDecimal getHccCharge() {
		return hccCharge;
	}

	public void setHccCharge(BigDecimal hccCharge) {
		this.hccCharge = hccCharge;
	}
	
	public String getStudyNumber() {
		return studyNumber;
	}

	public void setStudyNumber(String studyNumber) {
		this.studyNumber = studyNumber;
	}

	public String getCalendarName() {
		return calendarName;
	}

	public void setCalendarName(String calendarName) {
		this.calendarName = calendarName;
	}

	public String getVisitName() {
		return visitName;
	}

	public void setVisitName(String visitName) {
		this.visitName = visitName;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getCptCode() {
		return cptCode;
	}

	public void setCptCode(String cptCode) {
		this.cptCode = cptCode;
	}

	public String getCoverageType() {
		return coverageType;
	}

	public void setCoverageType(String coverageType) {
		this.coverageType = coverageType;
	}

	public String getCoverageNotes() {
		return coverageNotes;
	}

	public void setCoverageNotes(String coverageNotes) {
		this.coverageNotes = coverageNotes;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}
}
