package com.velos.integration.messaging;

import java.util.HashMap;
import java.util.Map;

import com.velos.services.SimpleIdentifier;

public class Change {
	private String action; 
	private String module; 
	private String timeStamp; 
	private SimpleIdentifier identifier; 
	//Other Tags then above four comes under additional Tags 
	private Map<String, String> additionalTags = new HashMap<String, String>();
	
	public String getAction() {
		return action;
	}
	
	public void setAction(String action) {
		this.action = action;
	}
	
	public String getModule() {
		return module;
	}
	
	public void setModule(String module) {
		this.module = module;
	}
	
	public String getTimeStamp() {
		return timeStamp;
	}
	
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	
	public SimpleIdentifier getIdentifier() {
		return identifier;
	}
	
	public void setIdentifier(SimpleIdentifier identifier) {
		this.identifier = identifier;
	}
	
	public Map<String, String> getAdditionalTags() {
		return additionalTags;
	}
	
	public void setAdditionalTags(Map<String, String> additionalTags) {
		this.additionalTags = additionalTags;
	} 
}
