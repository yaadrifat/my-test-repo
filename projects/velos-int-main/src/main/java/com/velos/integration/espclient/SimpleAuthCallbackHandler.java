package com.velos.integration.espclient;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;

import org.apache.ws.security.WSPasswordCallback;

public class SimpleAuthCallbackHandler implements CallbackHandler {

	public void handle(Callback[] callbacks) {
		WSPasswordCallback pc = (WSPasswordCallback) callbacks[0];
		if (pc.getIdentifier().equals("ihuang") || pc.getIdentifier().equals("kchetal") || pc.getIdentifier().equals("hvchahal")) {
			pc.setPassword("velos123");
		}
	}
}
