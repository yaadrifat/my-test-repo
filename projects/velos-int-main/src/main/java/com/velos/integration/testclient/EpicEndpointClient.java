package com.velos.integration.testclient;

import java.io.StringReader;
import java.io.StringWriter;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.addressing.client.ActionCallback;
import org.springframework.ws.soap.addressing.version.Addressing10;

import com.velos.integration.espclient.VelosEspClient;
import com.velos.integration.mapping.ProtocolKeys;
import com.velos.integration.mapping.VelosKeys;

@Component
public class EpicEndpointClient {
	
    private static final String protocolRequestTemplate =
    		"<urn:"+ProtocolKeys.RetrieveProtocol+" root=\"1234\" extension=\"%s\" xmlns:urn=\"urn:hl7-org:v3\" />";
    
    private static final String patientRequestTemplate =
    		"<urn:"+ProtocolKeys.RetrievePatient+" root=\"1234\" extension=\"%s\" xmlns:urn=\"urn:hl7-org:v3\" />";
    
	private static final String AlertProtocolRequestTemplate =
			"<urn:AlertProtocolState xmlns:urn=\"urn:hl7-org:v3\" >"+
					"<urn:processState>%s</urn:processState>"+
					"<urn:patient>"+
					"<candidateID root=\"1.3.6.1.4.1.12559.11.1.4.1.2\" extension=\"%s\" assigningAuthorityName=\"%s\"/>"+
					"<urn:name>"+
					"<urn:given>%s</urn:given>"+
					"<urn:family>%s</urn:family>"+
					"</urn:name>"+
					"<urn:dob value=\"%s\"></urn:dob>"+
					"</urn:patient>"+
					"<urn:study>"+
					"<urn:id root=\"8.3.5.6.7.78\" extension=\"%s\" assigningAuthorityName=\"%s\"/>"+
					"</urn:study>"+
					"</urn:AlertProtocolState>";
 
    public static void main(String[] args) throws Exception {
    	Map<VelosKeys, String> requestMap = new HashMap<VelosKeys, String>();
    	requestMap.put(ProtocolKeys.ProcessState, "a");
    	requestMap.put(ProtocolKeys.PatientId, "b");
    	requestMap.put(ProtocolKeys.SiteName, "c");
    	requestMap.put(ProtocolKeys.FirstName, "d");
    	requestMap.put(ProtocolKeys.LastName, "e");
    	requestMap.put(ProtocolKeys.Dob, "19770101");
    	requestMap.put(ProtocolKeys.SiteName, VelosEspClient.HARDCODED_ORG);
    	requestMap.put(ProtocolKeys.StudyNumber, "Adam-1");
    	EpicEndpointClient client = new EpicEndpointClient();
    	client.handleRequest(ProtocolKeys.AlertProtocolState, requestMap);
    }
    
	public Map<VelosKeys, Object> handleRequest(ProtocolKeys request, Map<VelosKeys, String> requestMap)  {
		if (requestMap == null) { return null; }
		String body = null;
		@SuppressWarnings("resource")
		ClassPathXmlApplicationContext context = 
				new ClassPathXmlApplicationContext("epic-testclient-req.xml");
		switch (request) {
		case RetrieveProtocol:
			body = String.format(protocolRequestTemplate, requestMap.get(ProtocolKeys.StudyNumber));
			break;
		case RetrievePatient:
			body = String.format(patientRequestTemplate, requestMap.get(ProtocolKeys.StudyNumber));
			break;
		case AlertProtocolState:
			ArrayList<String> argList = new ArrayList<String>();
			argList.add(requestMap.get(ProtocolKeys.ProcessState));
			argList.add(requestMap.get(ProtocolKeys.PatientId));
			argList.add(requestMap.get(ProtocolKeys.SiteName));
			argList.add(requestMap.get(ProtocolKeys.FirstName));
			argList.add(requestMap.get(ProtocolKeys.LastName));
			argList.add(requestMap.get(ProtocolKeys.Dob));
			argList.add(requestMap.get(ProtocolKeys.StudyNumber));
			argList.add(requestMap.get(ProtocolKeys.SiteName));
			body = String.format(AlertProtocolRequestTemplate, argList.toArray());
			break;
		default: body = ""; break;
		}
		
		WebServiceTemplate webServiceTemplate = (WebServiceTemplate)context.getBean("webServiceTemplate");
		StreamSource source = new StreamSource(new StringReader(body));
		StringWriter writer = new StringWriter();
		StreamResult result = new StreamResult(writer);
		webServiceTemplate.sendSourceAndReceiveToResult(source, wasHeader(), result);
        System.out.println("Got this response\n"+writer.toString());
        return null;
	}
	
	private static ResourceBundle bundle = ResourceBundle.getBundle("epic-testclient-req");
	
	public static String getProperty(String key) {
	    return bundle.getString(key);
	}
	
	@Bean
	public ActionCallback wasHeader() {
		ActionCallback header = null;
		try {
			URI action = new URI("urn:ihe:qrph:rpe:2009:AlertProtocolState");
			Addressing10 version = new Addressing10();
			String scheme = getProperty("ws.scheme");
			String host = getProperty("ws.host");
			String port = getProperty("ws.port");
			String context = getProperty("ws.context");
			if (context != null && !context.startsWith("/")) {
				context = "/" + context;
			}
			URI to = null;
			int portInt = -1;
			try {
				portInt = Integer.parseInt(port);
			} catch(NumberFormatException e) {
				portInt = 80;
			}
			to = (new URL(scheme, host, portInt, context)).toURI();
			header = new ActionCallback(action, version, to);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return header;
	}
    
}
