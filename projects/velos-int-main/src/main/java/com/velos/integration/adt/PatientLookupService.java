package com.velos.integration.adt;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v24.datatype.HD;
import ca.uhn.hl7v2.model.v24.segment.MSH;
import ca.uhn.hl7v2.parser.DefaultXMLParser;
import ca.uhn.hl7v2.parser.XMLParser;

public class PatientLookupService {
    public Message lookupPatient(Message input) throws HL7Exception {
    	//encode message in XML
    	XMLParser xmlParser = new DefaultXMLParser();
    	String ackMessageInXML = xmlParser.encode(input);
    	//System.out.println(ackMessageInXML);
 
        // Extract NINTENDO in test data
    	MSH msh = (MSH)input.get("MSH");  // Make the version in MSH line the same as version (v24) of MSH import
    	HD hd = msh.getMsh4_SendingFacility();
    	String sendingFacility = hd.getComponent(0).encode();
    	System.out.println("sendingFacility="+sendingFacility);
    	
        return null;
    }
}
