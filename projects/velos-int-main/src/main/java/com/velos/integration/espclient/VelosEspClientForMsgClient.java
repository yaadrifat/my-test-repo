package com.velos.integration.espclient;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.datatype.XMLGregorianCalendar;

import org.springframework.stereotype.Component;

import com.velos.integration.mapping.ProtocolKeys;
import com.velos.integration.mapping.VelosKeys;
import com.velos.services.Code;
import com.velos.services.Issue;
import com.velos.services.ObjectInfo;
import com.velos.services.OperationException_Exception;
import com.velos.services.PatientIdentifier;
import com.velos.services.PatientProtocolIdentifier;
import com.velos.services.PatientStudyStatusIdentifier;
import com.velos.services.SimpleIdentifier;
import com.velos.services.StudyIdentifier;
import com.velos.services.StudyPatientStatus;
import com.velos.services.StudyPatientStatuses;

@Component
public class VelosEspClientForMsgClient extends VelosEspClientCamel {
		
	public static void main(String[] args) {
		VelosEspClientForMsgClient client = new VelosEspClientForMsgClient();
		client.configureCamel();
		
		// Create test data
		Map<VelosKeys, String> requestMap = new HashMap<VelosKeys, String>();
		requestMap.put(ProtocolKeys.OID, "63c259bc-10da-41c6-9552-d83a819495aa");
		
		// Test study summary
		client.testPatStudyRequest(requestMap);
	}

	public Map<VelosKeys, Object> testPatStudyRequest(
			Map<VelosKeys, String> requestMap) {
		SimpleIdentifier simpleIdentifier = new SimpleIdentifier();
		simpleIdentifier.setOID(requestMap.get(ProtocolKeys.OID));
		// simpleIdentifier.setPK(Integer.parseInt(requestMap.get(ProtocolKeys.PK)));
		ObjectInfo objectInfo;
		try {
			objectInfo = getObjectInfoFromOID(simpleIdentifier);
			System.out.println(objectInfo.getTableName());
			System.out.println(objectInfo.getTablePk());
			PatientProtocolIdentifier patientProtocolIdentifier = getPatientStudyInfo(objectInfo
					.getTablePk());
			System.out.println("---------------");
			PatientIdentifier patientIdentifier = patientProtocolIdentifier.getPatientIdentifier();
			StudyIdentifier studyIdentifier = patientProtocolIdentifier.getStudyIdentifier();
			StudyPatientStatuses studyPatientStatuses = getStudyPatientStatusHistory(studyIdentifier, patientIdentifier);
			List<StudyPatientStatus> studyPatientStatusList = studyPatientStatuses.getStudyPatientStatus();
			
			int maxPk =0;
			int secondMaxPk =0;
			Map<Integer, Map> studyPatientMap = new HashMap<Integer, Map>();
			
			for(StudyPatientStatus studyPatientStatus : studyPatientStatusList)
			{
				Map<String, String> studyPatientRow = new HashMap<String, String>();
				int pk;
				PatientStudyStatusIdentifier studyPatStatId = studyPatientStatus.getStudyPatStatId();
				Code status = studyPatientStatus.getStudyPatStatus();
				Code reason = studyPatientStatus.getStatusReason();
				XMLGregorianCalendar date = studyPatientStatus.getStatusDate();
				if(status != null)
				studyPatientRow.put("status", status.getCode());
				if(reason != null)
				studyPatientRow.put("reason", reason.getCode());
				if(date != null)
				studyPatientRow.put("date", date.toString());
				pk = studyPatStatId.getPK();
				studyPatientMap.put(pk, studyPatientRow);	
				if (pk > maxPk)
				{
					secondMaxPk = maxPk;
					maxPk = pk;		
				}
				else if(pk > secondMaxPk)
				{
					secondMaxPk = pk;
				}
				
			}
			
			Map<String, String> newStatus = studyPatientMap.get(maxPk);
			Map<String, String> oldStatus = studyPatientMap.get(secondMaxPk);
			System.out.println("Patient Number : " + patientIdentifier.getPatientId());
			System.out.println("Study Number : " + studyIdentifier.getStudyNumber());
			System.out.println("Old Status : " + oldStatus.get("status"));
			System.out.println("New Status : " + newStatus.get("status"));
			System.out.println("Date of Status Change : " + newStatus.get("date"));
			System.out.println("Reason : " + newStatus.get("reason"));
			System.out.println("Finish");
		} catch (OperationException_Exception e) {
			List<Issue> issueList = e.getFaultInfo().getIssues().getIssue();
			if (issueList == null) { return null; }
			for (Issue issue : issueList) {
				System.out.println(issue.getMessage());
			}
		}

		// VelosStudyMapper mapper = new
		// VelosStudyMapper(requestMap.get(EndpointKeys.Endpoint));
		// return mapper.mapStudySummary(studySummary);
		return null;
	}


}
