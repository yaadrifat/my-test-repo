package com.velos.integration.messaging;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.velos.services.PatientStudyStatusIdentifier;
import com.velos.services.SimpleIdentifier;


public class CustomHandler extends DefaultHandler {
/*
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<changes>
    <change>
        <action>CREATE</action>
        <statusSubType>infConsent</statusSubType>
        <statusCodeDesc>Informed Consent Signed</statusCodeDesc>
        <identifier>
            <OID>74b5fd8c-6538-41bd-9941-ec16437827c7</OID>
        </identifier>
        <module>patstudy_status</module>
        <timeStamp>JAN-28-2014 21:53:40</timeStamp>
    </change>
    <parentIdentifier>
        <id>
            <OID>f2f7c692-6fa6-4365-a7ab-9061003b399a</OID>
        </id>
    </parentIdentifier>
</changes>
*/
	
	private Map<String, SimpleIdentifier> parentOID = new HashMap<String, SimpleIdentifier>();
	 
	 
	private boolean parentIdentifier = false;
	private List<Change> changes =  new ArrayList<Change>();
	
	private boolean isChange = false; 
	private boolean isOID = false; 
	private boolean isId = false; 
	private boolean isAction = false; 
	private boolean isModule = false; 
	private boolean isTimeStamp = false;

	private String OID = ""; 
	
	// change elements 
	private String action = ""; 
	private String module = ""; 
	private String timeStamp = ""; 
	private Map<String, String> additionalInfoMap = new HashMap<String, String>(); 
	private String additionalInfoTag = null; 
	
	
	// StudyIdentifier additional information
	private String studyNumber = "";
	
	
	public Map getParentOID()
	{
		return parentOID; 
	}
	
	
	public List<Change> getChanges() {
		return changes;
	}

	
	public void startElement(String uri, String localName,
			String qName, Attributes attributes)
	throws SAXException {

		//System.out.println("Start Element :" + qName);

		if (qName.equalsIgnoreCase("parentIdentifier")) {
			parentIdentifier = true;
			return; 
		}
		
		if(qName.equalsIgnoreCase("change"))
		{
			isChange = true; 
			return;
		}


		if (qName.equalsIgnoreCase("OID")) {
			isOID = true;
			return;
		}
		
		if(qName.equalsIgnoreCase("id"))
		{
			isId = true; 
			return;
		}
		
		if(isChange)
		{
			if(qName.equalsIgnoreCase("action"))
			{
				isAction = true; 
				return; 
			}
			if(qName.equalsIgnoreCase("module"))
			{
				isModule = true; 
				return; 
				
			}
			if(qName.equalsIgnoreCase("timeStamp"))
			{
				isTimeStamp = true; 
				return; 
			}
			
			
			if(qName.equalsIgnoreCase("identifier"))
			{
				return; 
			}
			
			additionalInfoTag = qName; 
			
		}

	}

	public void endElement(String uri, String localName,
			String qName)
	throws SAXException {

		if(qName.equalsIgnoreCase("parentIdentifier"))
		{
			
				parentIdentifier = false; 
		}
		
		if(qName.equalsIgnoreCase("id"))
		{
			isId = false; 
			SimpleIdentifier identifier = new SimpleIdentifier(); 
			identifier.setOID(OID); 
			parentOID.put(IdentifierConstants.SIMPLE_IDENTIFIER, identifier ); 
		}
	
		if(qName.equalsIgnoreCase("change"))
		{
			isChange = false; 
			Change change = new Change(); 
			change.setAction(action); 
			change.setModule(module); 
			PatientStudyStatusIdentifier identifier = new PatientStudyStatusIdentifier(); 
			identifier.setOID(OID); 
			change.setIdentifier(identifier); 
		
			change.setTimeStamp(timeStamp); 
			change.setAdditionalTags(additionalInfoMap); 
			changes.add(change); 
			additionalInfoMap = new HashMap<String, String>(); 
		}
			
	}

	public void characters(char ch[], int start, int length)
	throws SAXException {
		
		if(isOID)
		{
			 OID =  new String(ch, start, length); 
			 isOID = false; 			
		}
			
		if(isAction)
		{
			action = new String(ch, start, length); 
			isAction = false; 
		}
			
		if(isModule)
		{
			module = new String(ch, start, length); 
			isModule = false;
		}
		
		if(isTimeStamp)
		{
			timeStamp =  new String(ch, start, length);
			isTimeStamp = false; 
		}
			
		if(additionalInfoTag != null)
		{
			additionalInfoMap.put(additionalInfoTag, new String(ch, start, length)); 
			additionalInfoTag = null; 				
		}
	}
}
