package com.velos.integration.mapping;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.velos.services.PatientDataBean;
import com.velos.services.PatientSearchResponse;
import com.velos.services.SearchPatientResponse;

public class VelosPatientMapper {
	private String endpoint = null;
	private Map<VelosKeys, Object> dataMap = new HashMap<VelosKeys, Object>();
	
	public VelosPatientMapper(String endpoint) {
		this.endpoint = endpoint;
	}
	
	public Map<VelosKeys, Object> mapSearchPatient(SearchPatientResponse resp) {
		if (EndpointKeys.Epic.toString().equals(endpoint)) {
			mapSearchPatientForEpic(resp);
		}
		return dataMap;
	}
	
	private void mapSearchPatientForEpic(SearchPatientResponse resp) {
		PatientSearchResponse searchResp = resp.getPatientSearchResponse();
		int totalCount = searchResp.getTotalCount();
		if (totalCount < 1) {
			dataMap.put(ProtocolKeys.FaultString, ProtocolKeys.PatientNotFoundMsg.toString());
			return;
		}
		if (totalCount > 1) {
			dataMap.put(ProtocolKeys.FaultString, ProtocolKeys.MultiplePatientsFoundMsg.toString());
			return;
		}
		dataMap.put(ProtocolKeys.TotalCount, searchResp.getTotalCount());
		List<PatientDataBean> patList = searchResp.getPatDataBean();
		for (PatientDataBean patBean : patList) {
			dataMap.put(ProtocolKeys.PatientID, patBean.getPatientIdentifier().getPatientId());
			dataMap.put(ProtocolKeys.FirstName, patBean.getPatFirstName());
			dataMap.put(ProtocolKeys.LastName, patBean.getPatLastName());
			dataMap.put(ProtocolKeys.Dob, patBean.getPatDateofBirth());
			dataMap.put(ProtocolKeys.TotalCount, 1);
		}
	}

}
