package com.velos.integration.testclient;

import java.util.HashMap;
import java.util.Map;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.integration.Message;
import org.springframework.integration.MessageChannel;
import org.springframework.integration.core.MessagingTemplate;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.stereotype.Component;

import com.velos.integration.mapping.ProtocolKeys;
import com.velos.integration.mapping.VelosKeys;

@Component
public class VelosEndpointClient {
	
    private static final String protocolRequestTemplate =
    		"<urn:RetrieveProtocolDefRequest root=\"1234\" xmlns:urn=\"urn:hl7-org:v3\" ><urn:query root=\"\" extension=\"%s\"/></urn:RetrieveProtocolDefRequest>";
    
    private static final String patientRequestTemplate =
    		"<urn:"+ProtocolKeys.RetrievePatient+" root=\"1234\" extension=\"%s\" xmlns:urn=\"urn:hl7-org:v3\" />";
    
    public static void main(String[] args) throws Exception {
    	Map<VelosKeys, String> requestMap = new HashMap<VelosKeys, String>();
    	requestMap.put(ProtocolKeys.StudyNumber, "Adam-1");
    	VelosEndpointClient client = new VelosEndpointClient();
    	client.handleRequest(ProtocolKeys.RetrieveProtocol, requestMap);
    }
    
	public Map<VelosKeys, Object> handleRequest(ProtocolKeys request, Map<VelosKeys, String> requestMap)  {
		if (requestMap == null) { return null; }
		MessageChannel channel = null;
		String body = null;
		@SuppressWarnings("resource")
		ClassPathXmlApplicationContext context = 
				new ClassPathXmlApplicationContext("velos-testclient-req.xml");
		switch (request) {
		case RetrieveProtocol:
			channel = context.getBean("protocolRequests", MessageChannel.class);
			body = String.format(protocolRequestTemplate, requestMap.get(ProtocolKeys.StudyNumber));
			break;
		case RetrievePatient:
			channel = context.getBean("protocolRequests", MessageChannel.class);
			body = String.format(patientRequestTemplate, requestMap.get(ProtocolKeys.StudyNumber));
			break;
		default: body = ""; break;
		}
        MessagingTemplate messagingTemplate = new MessagingTemplate();
        Message<?> message = messagingTemplate.sendAndReceive(
                channel, MessageBuilder.withPayload(body).build());
        System.out.println("Got this response\n"+message.getPayload());
        return null;
	}
    
}
