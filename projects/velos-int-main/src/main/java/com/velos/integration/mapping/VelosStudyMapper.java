package com.velos.integration.mapping;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.velos.services.GetStudySummaryResponse;
import com.velos.services.NvPair;
import com.velos.services.StudySummary;

public class VelosStudyMapper {
	private String endpoint = null;
	private Map<VelosKeys, Object> dataMap = new HashMap<VelosKeys, Object>();
	
	public VelosStudyMapper(String endpoint) {
		this.endpoint = endpoint;
	}
	
	public Map<VelosKeys, Object> mapStudySummary(GetStudySummaryResponse resp) {
		if (EndpointKeys.Epic.toString().equals(endpoint)) {
			mapStudySummaryForEpic(resp);
		}
		return dataMap;
	}
	
	public Map<VelosKeys, Object> mapStudySummary(StudySummary summary) {
		if (EndpointKeys.Epic.toString().equals(endpoint)) {
			mapStudySummaryForEpic(summary);
		}
		return dataMap;
	}
	
	private void mapStudySummaryForEpic(GetStudySummaryResponse resp) {
		mapStudySummaryForEpic(resp.getStudySummary());
	}
	
	private void mapStudySummaryForEpic(StudySummary summary) {
		dataMap.put(ProtocolKeys.Title, MapUtil.escapeSpecial(summary.getStudyTitle()));
		dataMap.put(ProtocolKeys.Text, MapUtil.escapeSpecial(summary.getSummaryText()));
		dataMap.put(ProtocolKeys.IdExtension, MapUtil.escapeSpecial(summary.getStudyNumber()));
		List<NvPair> moreList = summary.getMoreStudyDetails();
		for (NvPair more : moreList) {
			if (ProtocolKeys.IrbNumber.toString().equalsIgnoreCase(more.getKey())) {
				dataMap.put(ProtocolKeys.IrbNumber, more.getValue());
			} else if (ProtocolKeys.Irb.toString().equalsIgnoreCase(more.getKey())) {
				dataMap.put(ProtocolKeys.Irb, more.getValue());
			} else if (ProtocolKeys.NctNumber.toString().equalsIgnoreCase(more.getKey())) {
				dataMap.put(ProtocolKeys.NctNumber, more.getValue());
			} else if (ProtocolKeys.Nct.toString().equalsIgnoreCase(more.getKey())) {
				dataMap.put(ProtocolKeys.Nct, more.getValue());
			}
		}
	}
}
