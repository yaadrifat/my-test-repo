
package com.velos.services;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for cost complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cost">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="cost" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="costType" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="currency" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="eventCostIdentifier" type="{http://velos.com/services/}eventCostIdentifier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cost", propOrder = {
    "cost",
    "costType",
    "currency",
    "eventCostIdentifier"
})
public class Cost
    extends ServiceObject
{

    protected BigDecimal cost;
    protected Code costType;
    protected Code currency;
    protected EventCostIdentifier eventCostIdentifier;

    /**
     * Gets the value of the cost property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCost() {
        return cost;
    }

    /**
     * Sets the value of the cost property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCost(BigDecimal value) {
        this.cost = value;
    }

    /**
     * Gets the value of the costType property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getCostType() {
        return costType;
    }

    /**
     * Sets the value of the costType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setCostType(Code value) {
        this.costType = value;
    }

    /**
     * Gets the value of the currency property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getCurrency() {
        return currency;
    }

    /**
     * Sets the value of the currency property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setCurrency(Code value) {
        this.currency = value;
    }

    /**
     * Gets the value of the eventCostIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link EventCostIdentifier }
     *     
     */
    public EventCostIdentifier getEventCostIdentifier() {
        return eventCostIdentifier;
    }

    /**
     * Sets the value of the eventCostIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link EventCostIdentifier }
     *     
     */
    public void setEventCostIdentifier(EventCostIdentifier value) {
        this.eventCostIdentifier = value;
    }

}
