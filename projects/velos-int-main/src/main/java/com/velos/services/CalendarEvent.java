
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for calendarEvent complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="calendarEvent">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="calendarEventSummary" type="{http://velos.com/services/}calendarEventSummary" minOccurs="0"/>
 *         &lt;element name="costs" type="{http://velos.com/services/}costs" minOccurs="0"/>
 *         &lt;element name="parentIdentifier" type="{http://velos.com/services/}parentIdentifier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "calendarEvent", propOrder = {
    "calendarEventSummary",
    "costs",
    "parentIdentifier"
})
public class CalendarEvent
    extends ServiceObject
{

    protected CalendarEventSummary calendarEventSummary;
    protected Costs costs;
    protected ParentIdentifier parentIdentifier;

    /**
     * Gets the value of the calendarEventSummary property.
     * 
     * @return
     *     possible object is
     *     {@link CalendarEventSummary }
     *     
     */
    public CalendarEventSummary getCalendarEventSummary() {
        return calendarEventSummary;
    }

    /**
     * Sets the value of the calendarEventSummary property.
     * 
     * @param value
     *     allowed object is
     *     {@link CalendarEventSummary }
     *     
     */
    public void setCalendarEventSummary(CalendarEventSummary value) {
        this.calendarEventSummary = value;
    }

    /**
     * Gets the value of the costs property.
     * 
     * @return
     *     possible object is
     *     {@link Costs }
     *     
     */
    public Costs getCosts() {
        return costs;
    }

    /**
     * Sets the value of the costs property.
     * 
     * @param value
     *     allowed object is
     *     {@link Costs }
     *     
     */
    public void setCosts(Costs value) {
        this.costs = value;
    }

    /**
     * Gets the value of the parentIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link ParentIdentifier }
     *     
     */
    public ParentIdentifier getParentIdentifier() {
        return parentIdentifier;
    }

    /**
     * Sets the value of the parentIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link ParentIdentifier }
     *     
     */
    public void setParentIdentifier(ParentIdentifier value) {
        this.parentIdentifier = value;
    }

}
