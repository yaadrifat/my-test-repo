package com.velos.eres.web.patient;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.velos.epat.audit.web.AuditRowEpatJB;
import com.velos.eres.business.common.SettingsDao;
import com.velos.eres.business.common.StudyStatusDao;
import com.velos.eres.business.perId.impl.PerIdBean;
import com.velos.eres.business.person.impl.PersonBean;
import com.velos.eres.business.pref.impl.PrefBean;
import com.velos.eres.service.util.MC;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.common.CommonJB;
import com.velos.eres.web.perId.PerIdJB;
import com.velos.eres.web.person.PersonJB;
import com.velos.eres.web.pref.PrefJB;
import com.velos.eres.web.site.SiteJB;
import com.velos.eres.web.study.StudyJB;
import com.velos.eres.web.studyStatus.StudyStatusJB;

public class PatientAction extends ActionSupport  {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1296958176689894517L;
	private HttpServletRequest request = null;
	private HttpServletResponse response = null;
	
	public static final String USER_FK = "userFK";
	public static final String DATA_NOT_SAVED = "dataNotSaved";
	public static final String DATA_NOT_DELETED = "dataNotDeleted";
	public static final String ESIGN_DOES_NOT_MATCH = "esignDoesNotMatch";
	private Map errorMap = null;
	private Collection errorAction = null;


	public PatientAction() {
		ActionContext ac = ActionContext.getContext();
		request = (HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
		response = (HttpServletResponse)ac.get(ServletActionContext.HTTP_RESPONSE);
	}
	
	public void setErrorMap(Map errorMap) {
		this.errorMap = errorMap;
	}

	public Map getErrorMap() {
		return errorMap;
	}

	public HashMap<String, Object> loadPatientEnrollParameterMap(){
		HashMap<String, Object> args = new HashMap<String, Object>();
		
		HttpSession tSession = request.getSession(true);

		String eSign = request.getParameter("eSign");
		args.put("eSign", eSign);
		
		String oldESign = (String) tSession.getAttribute("eSign");
		args.put("oldESign", oldESign);
		
		if(!oldESign.equals(eSign)) {
			throw new IllegalArgumentException(ESIGN_DOES_NOT_MATCH);
		}

		String ipAdd = (String) tSession.getAttribute("ipAdd");
		args.put("ipAdd", ipAdd);
		
		String userId = (String) tSession.getAttribute("userId");
		args.put("userId", userId);
		
		String accId=(String)tSession.getAttribute("accountId");
		args.put("accountId", accId);
		
		String mode  = request.getParameter("mode");
		mode=(StringUtil.isEmpty(mode))?"":mode;
		args.put("mode", mode);

		String selstudyId  = request.getParameter("selstudyId");
		selstudyId=(StringUtil.isEmpty(selstudyId))?"":selstudyId;
		args.put("selstudyId", selstudyId);

		String organization = request.getParameter("patorganization");
		organization=(StringUtil.isEmpty(organization))? null:organization;
		args.put("patorganization", organization);

		String add1 = request.getParameter("patadd1");
		add1=(StringUtil.isEmpty(add1))?"":add1;
		args.put("patadd1", add1);
		
		String add2 = request.getParameter("patadd2");
		add2=(StringUtil.isEmpty(add2))?"":add2;
		args.put("patadd2", add2);
		
		String city = request.getParameter("patcity");
		city=(StringUtil.isEmpty(city))?"":city;
		args.put("patcity", city);
		
		String state = request.getParameter("patstate");
		state=(StringUtil.isEmpty(state))?"":state;
		args.put("patstate", state); // Modified for Teaugo ADT interface on 3Oct2016 Surendra
		
		String zip = request.getParameter("patzip");
		zip=(StringUtil.isEmpty(zip))?"":zip;
		args.put("patzip", zip);
		
		String country = request.getParameter("patcountry");
		country=(StringUtil.isEmpty(country))?"":country;
		args.put("patcountry", country);
		
		String county = request.getParameter("patcounty");
		county=(StringUtil.isEmpty(county))?"":county;
		args.put("patcounty", county);
		
		String bphone = request.getParameter("patbphone");
		bphone=(StringUtil.isEmpty(bphone))?"":bphone;
		args.put("patbphone", bphone);
		
		String hphone= request.getParameter("pathphone");
		hphone=(StringUtil.isEmpty(hphone))?"":hphone;
		args.put("pathphone", hphone);
		
		String email = request.getParameter("patemail");
		email=(StringUtil.isEmpty(email))?"":email;
		args.put("patemail", email);

		String statDesc= request.getParameter("statDesc");
		statDesc=(StringUtil.isEmpty(statDesc))?"":statDesc;
		args.put("statDesc", statDesc);

		String statid= request.getParameter("statid");
		statid=(StringUtil.isEmpty(statid))?"":statid;
		args.put("statid", statid);

		String studyVer= request.getParameter("studyVer");
		studyVer=(StringUtil.isEmpty(studyVer))?"":studyVer;
		args.put("studyVer", studyVer);
		
		String othDthCause = request.getParameter("otherdthCause");
		othDthCause=(StringUtil.isEmpty(othDthCause))?"":othDthCause;
		args.put("othDthCause", othDthCause);
		
		String patientID = request.getParameter("patID");
		patientID=(StringUtil.isEmpty(patientID))?"":patientID;
		args.put("patID", patientID);
		
		String patientFacilityId = request.getParameter("patFacilityID");
		if (StringUtil.isEmpty(patientFacilityId )){
			patientFacilityId  = patientID ;
		}
		args.put("patFacilityID", patientFacilityId);
		
		String fname = request.getParameter("patfname");
		args.put("patfname", fname);

		String lname = request.getParameter("patlname");
		args.put("patlname", lname);

		String aname = request.getParameter("aliasname");
		if(aname==null) aname="";
		args.put("aliasname", aname);

		String dob = request.getParameter("patdob");
		args.put("patdob", dob);
		
		String patmname =request.getParameter("patmname"); //Added for Teaugo ADT Interface on  3Oct2016 by Surendra
		args.put("patmname", patmname);
		
		String maritalStat =request.getParameter("maritalStat"); //Added for Teaugo ADT Interface on  3Oct2016 by Surendra
		args.put("maritalStat", maritalStat);
		
		String patssn =request.getParameter("patssn"); //Added for Teaugo ADT Interface on  3Oct2016 by Surendra
		args.put("patssn", patssn);
		System.out.println("patient Action SSN 1 ================>"+patssn);
		String gender = request.getParameter("patgender");
		if (gender.equals("")) gender = null;
		args.put("patgender", gender);

		String ethnicity = request.getParameter("patethnicity");
		if (ethnicity.equals("")) ethnicity = null;
		args.put("patethnicity", ethnicity);

		String race = request.getParameter("patrace");
		if(race!=null){
			if (race.equals("")) race = null;
		}
		args.put("patrace", race);

		String regBy = request.getParameter("patregby");
		args.put("patregby", regBy);

		String patDeathDate = request.getParameter("patdeathdate");
		args.put("patdeathdate", patDeathDate);
		
		String status = request.getParameter("patstatus");
		if (status.equals("")) status= null;
		args.put("patstatus", status);
		
		String account = request.getParameter("pataccount");
		args.put("pataccount", account);
		
		String phyOther=request.getParameter("phyOther");
		args.put("phyOther", phyOther);
		
		String specialityIds = request.getParameter("selSpecialityIds");
		if(specialityIds.equals("null")){
		   specialityIds="";
		}
		args.put("selSpecialityIds", specialityIds);

		String addEthnicityIds = request.getParameter("selAddEthnicityIds");
		if(addEthnicityIds.equals("")){
			addEthnicityIds=null;
		}
		args.put("selAddEthnicityIds", addEthnicityIds);

		String dthCause = request.getParameter("dthCause");
		args.put("dthCause", dthCause);
		
		String addRaceIds = request.getParameter("selAddRaceIds");
		if(addRaceIds.equals("")){
			addRaceIds=null;
		}
		args.put("selAddRaceIds", addRaceIds);
		return args;
	}
	
	public HashMap<String, Object> loadPatientDemogParameterMap(){
		HashMap<String, Object> args = new HashMap<String, Object>();
		
		HttpSession tSession = request.getSession(true);

		String eSign = request.getParameter("eSign");
		args.put("eSign", eSign);
		
		String oldESign = (String) tSession.getAttribute("eSign");
		args.put("oldESign", oldESign);
		
		if(!oldESign.equals(eSign)) {
			throw new IllegalArgumentException(ESIGN_DOES_NOT_MATCH);
		}

		String ipAdd = (String) tSession.getAttribute("ipAdd");
		args.put("ipAdd", ipAdd);

		String usr = (String) tSession.getAttribute("userId");
		args.put("userId", usr);

		String mode = request.getParameter("mode");
		args.put("mode", mode);
		
		String patientID = request.getParameter("patID");
		args.put("patID", patientID);

		String fname = request.getParameter("patfname");
		args.put("patfname", fname);

		String mname = request.getParameter("patmname");
		args.put("patmname", mname);

		String lname = request.getParameter("patlname");
		args.put("patlname", lname);

		String dob = request.getParameter("patdob");
		args.put("patdob", dob);

		String gender = request.getParameter("patgender");
		if (gender.equals("")) gender = null;
		args.put("patgender", gender);

		String maritalStatus = request.getParameter("patmarital");
		if (maritalStatus.equals(""))    maritalStatus  = null;
		args.put("patmarital", maritalStatus);

		String bloodGroup = request.getParameter("patblood");
		if (bloodGroup.equals("")) bloodGroup = null;
		args.put("patblood", bloodGroup);

		String ethnicity = request.getParameter("patethnicity");
		if (ethnicity.equals("")) ethnicity = null;
		args.put("patethnicity", ethnicity);

		String race = request.getParameter("patrace");
		if (race.equals("")) race = null;
		args.put("patrace", race);

		String ssn = request.getParameter("patssn");
		args.put("patssn", ssn);

		String organization = request.getParameter("patorganization");
		if (organization.equals("")) organization = null;
		args.put("patorganization", organization);

		String patDeathDate = request.getParameter("patdeathdate");
		args.put("patdeathdate", patDeathDate);

		String add1 = request.getParameter("patadd1");
		args.put("patadd1", add1);

		String add2 = request.getParameter("patadd2");
		args.put("patadd2", add2);

		String city = request.getParameter("patcity");
		args.put("patcity", city);

		String state = request.getParameter("patstate");
		args.put("patstate", state);

		String zip = request.getParameter("patzip");
		args.put("patzip", zip);

		String country = request.getParameter("patcountry");
		args.put("patcountry", country);

		String county = request.getParameter("patcounty");
		args.put("patcounty", county);

		String bphone = request.getParameter("patbphone");
		args.put("patbphone", bphone);

		String hphone= request.getParameter("pathphone");
		args.put("pathphone", hphone);

		String email = request.getParameter("patemail");
		args.put("patemail", email);

		String employment = request.getParameter("patemp");
		if (employment.equals("")) employment = null;
		args.put("patemp", employment);

		String education  = request.getParameter("patedu");
		if (education.equals("")) education = null;
		args.put("patedu", education);

		String status = request.getParameter("patstatus");
		if (status.equals("")) status= null;
		args.put("patstatus", status);

		String notes = request.getParameter("patnotes");
		args.put("patnotes", notes);

		String account = request.getParameter("pataccount");
		args.put("pataccount", account);

		String timeZone = request.getParameter("timeZone");
		if (timeZone.equals("")) timeZone = null;
		args.put("timeZone", timeZone);
			   
		String phDetailIStr = request.getParameter("phDetailI");
		phDetailIStr = StringUtil.isEmpty(phDetailIStr)? "" : phDetailIStr;
		args.put("phDetailI", phDetailIStr);

		String SSN_NA = request.getParameter("SSN_NA");
		if (StringUtil.isEmpty(SSN_NA)){
			SSN_NA = "0";
		}
		args.put("SSN_NA", SSN_NA);
		
		String dthCause = request.getParameter("dthCause");
		args.put("dthCause", dthCause);

		String othDthCause = request.getParameter("othDthCause");
		args.put("othDthCause", othDthCause);
		
		String addEthnicityIds = request.getParameter("selAddEthnicityIds");
		if(addEthnicityIds.equals("")){
			addEthnicityIds=null;
		}
		args.put("selAddEthnicityIds", addEthnicityIds);

		String addRaceIds = request.getParameter("selAddRaceIds");		
		if(addRaceIds.equals("")){
			addRaceIds=null;
		}
		args.put("selAddRaceIds", addRaceIds);

		return args;
	}

	public int updatePatient(){
		HashMap<String, Object> args =  new HashMap<String, Object>();
		try{
			args = loadPatientDemogParameterMap();
		} catch (IllegalArgumentException e){
			if (ESIGN_DOES_NOT_MATCH.equals(e.getMessage())){
				return -4;
			}
		}

		int success = updatePatientData(args);
		return success;
	}

	private int updatePatientData(HashMap<String, Object> args){
		int success = 0;
		int saved = 0;
		String ipAdd = (String) args.get("ipAdd");
		String usr = (String) args.get("userId");
		String acc = (String) args.get("accountId");
		int personPK  = 0;
		CommonJB commonB = new CommonJB();

		String studyId  = (String) args.get("selstudyId");

		StudyJB studyB = new StudyJB(); 
		studyB.setId(StringUtil.stringToNum(studyId));
		studyB.getStudyDetails();
		String studyNum = studyB.getStudyNumber();
		studyNum=(studyNum==null)?"":studyNum;
		
		String organization = (String) args.get("patorganization");

		String add1 = (String) args.get("patadd1");
		String add2 = (String) args.get("patadd2");
		String city = (String) args.get("patcity");
		String state = (String) args.get("patstate");
		String zip = (String) args.get("patzip");
		String country = (String) args.get("patcountry");
		String county = (String) args.get("patcounty");
		String bphone = (String) args.get("patbphone");
		String hphone= (String) args.get("pathphone");
		String email = (String) args.get("patemail");
		String patientID = (String) args.get("patID");
		String SSN_NA = (String) args.get("SSN_NA");
		String account = (String) args.get("pataccount");
		String bloodGroup =  (String) args.get("patblood");
		String fname = (String) args.get("patfname");
		String lname = (String) args.get("patlname");
		String mname = (String) args.get("patmname");
		String aname = (String) args.get("aliasname");
		String dob = (String) args.get("patdob");
		String ssn = (String) args.get("patssn");
		String patDeathDate = (String) args.get("patdeathdate");
		String education = (String) args.get("patedu");
		String employment = (String) args.get("patemp");
		String gender = (String) args.get("patgender");
		String maritalStatus = (String) args.get("patmarital");
		String notes = (String) args.get("patnotes");
		String ethnicity = (String) args.get("patethnicity");
		String race = (String) args.get("patrace");
		String status = (String) args.get("patstatus");
		String timeZone = (String) args.get("timeZone");
		String addEthnicityIds = (String) args.get("selAddEthnicityIds");
		String addRaceIds = (String) args.get("selAddRaceIds");
		String dthCause = (String) args.get("dthCause");
		String othDthCause = (String) args.get("othDthCause");
		
		String phDetailIStr = (String) args.get("phDetailI");
		boolean phDetailI = (phDetailIStr != null && phDetailIStr.equals("true"))? true:false;

		PersonJB person = new PersonJB();

		String mode = (String) args.get("mode");
		if (mode.equals("M")){
			personPK = StringUtil.stringToNum(request.getParameter("pkey"));
			person.setPersonPKId(personPK);
			person.getPersonDetails();
		}
		if(mode.equals("M")){
			String preorg = request.getParameter("preorg");
		}
		boolean codeExists = false;
		patientID = patientID.trim();
		String oldPersonId = person.getPersonPId().trim().toUpperCase();
		
		if((!mode.equals("M")) || (mode.equals("M") && !(oldPersonId.equals(patientID.trim().toUpperCase())))  ) { // || (mode.equals("M")) && !(preorg.equals(organization))
			PrefJB prefB = new PrefJB();
			codeExists = prefB.patientCodeExistsOneOrg(patientID, organization);
		}
		
		if (codeExists){
			success = -2;
		} else {
			person.setPersonMultiBirth(SSN_NA)			;//for SM
			
			person.setPersonPId(patientID);
			person.setPersonAccount(account);
			person.setPersonLocation(organization);
		    person.setPersonBloodGr(bloodGroup);
			if (phDetailI){
				person.setPersonDob(dob);
				person.setPersonFname(fname);
				person.setPersonLname(lname);
				person.setPersonMname(mname);
				person.setPersonSSN(ssn);
				person.setPersonAddress1(add1);
				person.setPersonAddress2(add2);
				person.setPersonCity(city);
				person.setPersonCounty(county);
				person.setPersonState(state);
				person.setPersonCountry(country);
				person.setPersonZip(zip);
				
				person.setPersonEmail(email);
				person.setPersonHphone(hphone);
				person.setPersonBphone(bphone);
				
				person.setPersonDeathDate(patDeathDate);
			}
		   
		    person.setPersonEducation(education);
		    person.setPersonEmployment(employment);
	 	    person.setPersonGender(gender);
	  	    person.setPersonMarital(maritalStatus);
		    person.setPersonNotes(notes);
		    person.setPersonEthnicity(ethnicity);
		    person.setPersonRace(race);
		    person.setPersonStatus(status);	   
		    person.setTimeZoneId(timeZone);

			//person.setPersonRegDate(regDate);
			//person.setPersonRegBy(regBy);
			//	person.setPhyOther(phyOther);
			//	person.setPersonSplAccess(specialityIds);

			person.setPersonAddEthnicity(addEthnicityIds);
			person.setPersonAddRace(addRaceIds);
			person.setPatDthCause(dthCause);
			if(othDthCause!=null )
				person.setDthCauseOther(othDthCause);
			else
				person.setDthCauseOther("");

			PersonBean pers = null;

			if (mode.equals("M")){
				person.setModifiedBy(usr);
				person.setIpAdd(ipAdd);
	
				AuditRowEpatJB auditJB = new AuditRowEpatJB();
				int lastRaid = auditJB.findLatestRaidForPatient(person.getPersonPKId());
	
				saved = person.updatePerson();
				HttpSession tSession = request.getSession(true);
				tSession.setAttribute("personPK", ""+personPK);
				tSession.setAttribute("patientCode", patientID);

				String remarks = request.getParameter("remarks");
				if (!StringUtil.isEmpty(remarks)){
					int userId = StringUtil.stringToNum((String)tSession.getAttribute("userId"));
					auditJB.setReasonForChangeOfPatient(person.getPersonPKId(), userId, remarks, lastRaid);
				}
			} else {
				person.setCreator(usr);
				person.setIpAdd(ipAdd);
				pers = person.setPersonDetails();

				if (pers !=null){
					saved = 0;
					personPK =  pers.getPersonPKId();
					HttpSession tSession = request.getSession(true);
					tSession.setAttribute("personPK", ""+personPK);
					tSession.setAttribute("patientCode", patientID);
				} else {
					saved = -1;
				}
			}
		}

		return success;
	}

	private int createPatient(){
		HashMap<String, Object> args =  new HashMap<String, Object>();
		try{
			args = loadPatientEnrollParameterMap();
		} catch (IllegalArgumentException e){
			if (ESIGN_DOES_NOT_MATCH.equals(e.getMessage())){
				return -4;
			}
		}

		int success = createPatientData(args);
		return success;
	}

	private int createPatientData(HashMap<String, Object> args){
		int success = 0;
		
		String ipAdd = (String) args.get("ipAdd");
		String usr = (String) args.get("userId");
		String acc = (String) args.get("accountId");
		int personPK  = 0;
		CommonJB commonB = new CommonJB();

		String studyId  = (String) args.get("selstudyId");

		StudyJB studyB = new StudyJB(); 
		studyB.setId(StringUtil.stringToNum(studyId));
		studyB.getStudyDetails();
		String studyNum = studyB.getStudyNumber();
		studyNum=(studyNum==null)?"":studyNum;
		
		String organization = (String) args.get("patorganization");

		String add1 = (String) args.get("patadd1");
		String add2 = (String) args.get("patadd2");
		String city = (String) args.get("patcity");
		String state = (String) args.get("patstate");
		String zip = (String) args.get("patzip");
		String country = (String) args.get("patcountry");
		String county = (String) args.get("patcounty");
		String bphone = (String) args.get("patbphone");
		String hphone= (String) args.get("pathphone");
		String email = (String) args.get("patemail");

		//Check if the Organization study combination is open for enrolling
		StudyStatusDao statDao=null;
		int iStudyId=StringUtil.stringToNum(studyId);
		int index=-1,stopEnrollCount=0;
		String studyAccrualFlag="";
		ArrayList statList=null;
		if (studyId.length()>0)
		{
			SettingsDao settingsDao=commonB.retrieveSettings(iStudyId,3);
			ArrayList settingsValue=settingsDao.getSettingValue();
			ArrayList settingsKeyword=settingsDao.getSettingKeyword();
			index=settingsKeyword.indexOf("STUDY_ACCRUAL_FLAG");
			if (index>=0)
			{
				studyAccrualFlag=(String)settingsValue.get(index);
				studyAccrualFlag=(studyAccrualFlag==null)?"":studyAccrualFlag;
			}
			else
			{
				studyAccrualFlag="D";
			}
			if ( (studyAccrualFlag.equals("D")) || (studyAccrualFlag.length()==0) ) {
				//Disable this for now, with Default settings everything will work as it was in ver 6.2
				//stopEnrollCount=studyStatB.getCountByOrgStudyStat(StringUtil.stringToNum(studyId),StringUtil.stringToNum(organization),"'active_cls'","A");
				stopEnrollCount=0;
			} else if (studyAccrualFlag.equals("O")){
				 StudyStatusJB studyStatB = new StudyStatusJB();
				 statDao=studyStatB.getDAOByOrgStudyStat(StringUtil.stringToNum(studyId),StringUtil.stringToNum(organization),"'active_cls','active','prmnt_cls'","A");
				 statList=statDao.getStudyStatList();
				 if (statList.size()==0) stopEnrollCount=0;
				
				 if (statList.indexOf("active_cls")>=0)
					 stopEnrollCount=1;
				
				 if (statList.indexOf("active")<0)
					 stopEnrollCount=1;
				
				 if (statList.indexOf("prmnt_cls")>0)
				 stopEnrollCount=1;
			}
		}

		//End check for Organization study combination is open for enrolling

		SiteJB siteB = new SiteJB();
		siteB.setSiteId(StringUtil.stringToNum(organization));
		siteB.getSiteDetails();
		String siteName = siteB.getSiteName();
		int cnt = studyB.getOrgAccessForStudy(StringUtil.stringToNum(usr), StringUtil.stringToNum(studyId), StringUtil.stringToNum(organization) );
		if((cnt>0) || (studyId.length()==0)){
			String mode = (String) args.get("mode");
			String pkey = (String) args.get("pkey");
			String statDesc= (String) args.get("statDesc");
			String statid= (String) args.get("statid");
			String studyVer= (String) args.get("studyVer");
			String othDthCause = (String) args.get("otherdthCause");
			String patientID = (String) args.get("patID");
			String patientFacilityId = (String) args.get("patFacilityID");
			if (StringUtil.isEmpty(patientFacilityId )){
				patientFacilityId  = patientID ;
			}
			String fname = (String) args.get("patfname");
			String lname = (String) args.get("patlname");
			String mname = (String) args.get("patmname"); // Added for Teaugo ADT Interface on  3Oct2016 by Surendra
			String maritalStat = (String) args.get("maritalStat"); // Added for Teaugo ADT Interface on  3Oct2016 by Surendra
			String aname = (String) args.get("aliasname");
			String dob = (String) args.get("patdob");
			String ssn = (String) args.get("patssn");// Added for Teaugo ADT Interface on  27Dec2016 by Surendra
			String gender = (String) args.get("patgender");
			String ethnicity = (String) args.get("patethnicity");
			String race = (String) args.get("patrace");
			String regBy = (String) args.get("patregby");
			String patDeathDate = (String) args.get("patdeathdate");
			String status = (String) args.get("patstatus");
			String account = (String) args.get("pataccount");
			String phyOther=(String) args.get("phyOther");
			String specialityIds = (String) args.get("selSpecialityIds");			
			String addEthnicityIds = (String) args.get("selAddEthnicityIds");
			String dthCause = (String) args.get("dthCause");
			String addRaceIds = (String) args.get("selAddRaceIds");

			PersonJB personB = new PersonJB();

			if (mode.equals("M")){
				//out.print("key" +args.get("pkey"));
				personPK = StringUtil.stringToNum((String)args.get("pkey"));
				personB.setPersonPKId(personPK);
				personB.getPersonDetails();
			}
			boolean codeExists = false;
			patientID = patientID.trim();
			if((!mode.equals("M")) || (mode.equals("M") && !(personB.getPersonPId().equals(patientID)))) {
				PrefJB prefB = new PrefJB();
				codeExists = prefB.patientCodeExistsOneOrg(patientID, organization);
			}
			if(codeExists){
				success = -2;
			} else if (stopEnrollCount>0){ 
				success = -3;
			} else {
			   	personB.setPersonPId(patientID);
				personB.setPersonAccount(account);
				personB.setPersonLocation(organization);
		        personB.setPersonDob(dob);
		        personB.setPersonFname(fname);
				personB.setPersonGender(gender);
				personB.setPersonMname(mname); // Added for Teaugo ADT Interface on  3Oct2016 by Surendra
				personB.setPersonMarital(maritalStat); // Added for Teaugo ADT Interface on  3Oct2016 by Surendra
				System.out.println("MIDDLE NAME ACTION CLASS =============>"+mname);
				System.out.println("Patient Action SSN =============>"+ssn);
				personB.setPersonSSN(ssn);
		        personB.setPersonLname(lname);
		        personB.setPersonAlias(aname);
		        personB.setPersonEthnicity(ethnicity);
		        personB.setPersonRace(race);
		    	personB.setPersonStatus(status);
				personB.setPersonDeathDate(patDeathDate);
				personB.setPersonRegBy(regBy);
				personB.setPhyOther(phyOther);
				personB.setPersonSplAccess(specialityIds);
				personB.setPersonAddEthnicity(addEthnicityIds);
				personB.setPersonAddRace(addRaceIds);
				personB.setPatDthCause(dthCause);
				personB.setPatientFacilityId(patientFacilityId);
	
				//set Address information
				personB.setPersonAddress1(add1);
			    personB.setPersonAddress2(add2);
				personB.setPersonCity(city);
				personB.setPersonCountry(country);
				personB.setPersonCounty(county);
	
				personB.setPersonEmail(email);
				personB.setPersonHphone(hphone);
				personB.setPersonBphone(bphone);
				personB.setPersonState(state);

				personB.setPersonZip(zip);
	
				//End address information
	
				if(othDthCause!=null )
					personB.setDthCauseOther(othDthCause);
				else
					personB.setDthCauseOther("");

				PersonBean pers = null;
				if (mode.equals("M")){
					personB.setModifiedBy(usr);
					personB.setIpAdd(ipAdd);
					success = personB.updatePerson();
				}else{
					//Added by Gopu for July-August'06 Enhancement (U2) - Admin Settings for Patient Time zone
					SettingsDao settingsDao=commonB.getSettingsInstance();
					int modname=1;
					String keyword="ACC_PAT_TZ";
					settingsDao.retrieveSettings(StringUtil.stringToNum(acc),modname,keyword);
					ArrayList setvalues=settingsDao.getSettingValue();
					String setvalue="";
					if(setvalues!=null){
						setvalue=(setvalues.get(0)==null)?setvalue:(setvalues.get(0)).toString();
					}
					if (!(setvalue.equals(""))){
						 personB.setTimeZoneId(setvalue);
					}
					//Commented by Manimaran to fix the Bug2956
					//personB.setModifiedBy(usr);
					personB.setCreator(usr);
					personB.setIpAdd(ipAdd);
					pers = personB.setPersonDetails();
					personB.setPersonPKId(pers.getPersonPKId());
					pers=null;
					pers = personB.getPersonDetails();
					if (pers !=null){
						success = 0;
						personPK =  pers.getPersonPKId();
						success = personPK;
						patientID = pers.getPersonPId();
						HttpSession tSession = request.getSession(true);
						tSession.setAttribute("personPK", ""+personPK);
						tSession.setAttribute("patientCode", patientID);
						tSession.setAttribute("studyId", studyId);
					}else{
						success = -1;
					}
				}
			}
		}
		return success;
	}
	
	private int createUpdateMorePatientDetails(String createOrUpdate){
		int success = -1;
		
		try{
			HttpSession tSession = request.getSession(true);
			String creator=(String) tSession.getAttribute("userId");
			String ipAdd=(String)tSession.getAttribute("ipAdd");

			String perId = null;
			if ("CREATE".equals(createOrUpdate)){
				perId = (String) tSession.getAttribute("personPK");
			} else if ("UPDATE".equals(createOrUpdate)){
				perId = request.getParameter("perId");
			}

			ArrayList arRecordType = new ArrayList();
			ArrayList arAlternateId = new ArrayList();
			ArrayList arAlternateValues = new ArrayList();
			ArrayList arId = new ArrayList();
			//String[] arPerIdType = request.getParameterValues("perIdType");

			Enumeration params = request.getParameterNames();
			while (params.hasMoreElements()) {
			    String param = (String)params.nextElement();
			    if (param.startsWith("alternateId") && (!param.endsWith("Checks"))) {
			    	String paramVal = request.getParameter(param);
			    	if (null != paramVal){
			    		String mdId = param.substring(param.indexOf("alternateId")+("alternateId".length()));
			    		arAlternateId.add(mdId);
			    		arAlternateValues.add(paramVal);
			    		paramVal = request.getParameter("recordType"+mdId);
				    	if (null != paramVal){
				    		arRecordType.add(paramVal);
				    	}
				    	paramVal = request.getParameter("id"+mdId);
				    	if (null != paramVal){
				    		arId.add(paramVal);
				    	}
			      	}
			    }
			}



			PerIdBean psk = new PerIdBean();

			ArrayList alPerIds = arAlternateValues;

				int len = alPerIds.size();

				for ( int j = 0 ; j< len ; j++)
				{
					PerIdBean pskTemp = new PerIdBean();

					if   ( ( !(StringUtil.isEmpty ((String)arAlternateValues.get(j))) &&  "N".equals((String)arRecordType.get(j))) || "M".equals((String)arRecordType.get(j)))
					{
						pskTemp.setId(StringUtil.stringToNum((String)arId.get(j)));
						pskTemp.setPerId(perId) ;
						pskTemp.setPerIdType((String)arAlternateId.get(j));
						pskTemp.setAlternatePerId((String)arAlternateValues.get(j));
						pskTemp.setRecordType((String)arRecordType.get(j));
						if ("N".equals((String)arRecordType.get(j)))
						{
							pskTemp.setCreator(creator);
						}else
						{
							pskTemp.setModifiedBy(creator);
						}
						pskTemp.setIpAdd(ipAdd);
						psk.setPerIdStateKeepers(pskTemp);
					}
				}

			PerIdJB altId = new PerIdJB();
			success = altId.createMultiplePerIds(psk);

		} catch (Exception e){
			success = 0;
		}
		return success;
	}
	
	public String updatePatientEnrollScreen(){
		int success = 0;
		try{
			success = createPatient();
			
			if (success < 0){
				Map<String, String> errorMap = new HashMap<String, String>();

				switch(success){
					case -1:
						errorMap.put(DATA_NOT_SAVED, MC.M_DataNotSvd_Succ);
						break;
					case -2:
						//The Patient ID you have entered already exists for the selected organization. Please click on back and enter a new ID.
						errorMap.put(DATA_NOT_SAVED, MC.M_PatIdExst_OrgEtrNew);
						break;
					case -3:
						//Enrollment is not permitted at this time. The study may not be open at this organization, may be closed or temporarily on hold. Please contact the <%=LC.Std_Study%> Administrator for more information.
						errorMap.put(DATA_NOT_SAVED, MC.M_EnrlNotPrmtd_CnctStdAdmin);
						break;
					case -4:
						errorMap.put(ESIGN_DOES_NOT_MATCH, MC.M_EtrWrgEsign_Svg);
						break;
					default:
						errorMap.put(DATA_NOT_SAVED, MC.M_DataNotSvd_Succ);
						break;
				}
				this.errorMap=errorMap;
				return INPUT;
			}
			
			success = createUpdateMorePatientDetails("CREATE");
			
			if (success < 0){
				Map<String, String> errorMap = new HashMap<String, String>();
				errorMap.put(DATA_NOT_SAVED, "More Patient Details not saved!");
				this.errorMap=errorMap;
				return INPUT;
			}

			// -- Add a hook for customization for after a patient update succeeds
			// -- End of hook
			
		} catch (Exception e){
			e.printStackTrace();
			Map<String, String> errorMap = new HashMap<String, String>();
			errorMap.put(DATA_NOT_SAVED, MC.M_DataNotSvd_Succ);
			this.errorMap=errorMap;
			return INPUT;
		}
		this.errorMap = new HashMap<String, Object>();
		return SUCCESS;
	}
	
	public String updatePatientDemogScreen(){
		int success = -1;
		try{
			success = updatePatient();
			
			if (success < 0){
				Map<String, String> errorMap = new HashMap<String, String>();

				switch(success){
					case -1:
						errorMap.put(DATA_NOT_SAVED, MC.M_DataNotSvd_Succ);
						break;
					case -2:
						//The Patient ID you have entered already exists for the selected organization. Please click on back and enter a new ID.
						errorMap.put(DATA_NOT_SAVED, MC.M_PatIdExst_OrgEtrNew);
						break;
					case -3:
						//Enrollment is not permitted at this time. The study may not be open at this organization, may be closed or temporarily on hold. Please contact the <%=LC.Std_Study%> Administrator for more information.
						errorMap.put(DATA_NOT_SAVED, MC.M_EnrlNotPrmtd_CnctStdAdmin);
						break;
					case -4:
						errorMap.put(ESIGN_DOES_NOT_MATCH, MC.M_EtrWrgEsign_Svg);
						break;
					default:
						errorMap.put(DATA_NOT_SAVED, MC.M_DataNotSvd_Succ);
						break;
				}
				this.errorMap=errorMap;
				return INPUT;
				
			}
			
			success = createUpdateMorePatientDetails("UPDATE");
			
			if (success < 0){
				Map<String, String> errorMap = new HashMap<String, String>();
				errorMap.put(DATA_NOT_SAVED, "More Patient Details not saved!");
				this.errorMap=errorMap;
				return INPUT;
			}
		} catch (Exception e){
			e.printStackTrace();
			Map<String, String> errorMap = new HashMap<String, String>();
			errorMap.put(DATA_NOT_SAVED, MC.M_DataNotSvd_Succ);
			this.errorMap=errorMap;
			return INPUT;
		}
		this.errorMap = new HashMap<String, Object>();
		return SUCCESS;
	}
}