package com.velos.services.searchpatient;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.business.common.PatientDao;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.services.AbstractService;
import com.velos.services.CodeNotFoundException;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.OrganizationIdentifier;
import com.velos.services.model.PatientDataBean;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.PatientSearch;
import com.velos.services.model.PatientSearchResponse;
import com.velos.services.util.CodeCache;

public class SearchPatientDAO extends CommonDAO
implements Serializable{
	private static Logger logger = Logger.getLogger(SearchPatientServiceImpl.class.getName());
	/**
	 * 
	 */
	private static final long serialVersionUID = 4963096598046398734L;
	public PatientSearchResponse searchPatient (PatientSearch paramPatientIdentifier, Map<String, Object> parameters) throws OperationException{
		  //Added by Surendra for Teaugo ADT Interface 18Aug2016
		if(paramPatientIdentifier.isDistinctFacilityRequired()){
			return searchPatientWithDistinctFacilityId(paramPatientIdentifier, parameters);
		} else {
		Connection conn = getConnection();		
		PreparedStatement pstmt = null;
		StringBuffer sbSelect = new StringBuffer();
	    StringBuffer sbFrom = new StringBuffer();
	    StringBuffer sbWhere = new StringBuffer();
	    int requestedParameterCount = EJBUtil.stringToNum(parameters.get("requestedParameterCount").toString());
	   // int requestedRecordCount = 50;
	    int pageSize = 50;
	    int pageNumber = 0;
	    UserBean usrBean = (UserBean)parameters.get("callingUser");   
	    ObjectMapService objectMapService = (ObjectMapService) parameters.get("objectMapService");
	    String siteId = ""; //parameters.get("siteId").toString();
	    ArrayList usrSiteId = (ArrayList)parameters.get("arUsrSiteid");
		int userId = usrBean.getUserId();

		Format formatter = new SimpleDateFormat("yyyy/MM/dd");
		//String defGroup = userB.getUserGrpDefault();		
		String defGroup = usrBean.getUserGrpDefault();
		int grpId=EJBUtil.stringToNum(defGroup);
		int accountId = EJBUtil.stringToNum(usrBean.getUserAccountId());
		int codelstGender = 0;
		int codelstsurvivalStat = 0;
		int codelstPatSpecialty = 0;
		int fieldMasking = 0;

		boolean isExactSearch = false;
		
		CodeCache codeCache = CodeCache.getInstance();
		 
		int patRecordCount =0;
		try{
			if (logger.isDebugEnabled()) logger.debug("Requested Page Size " + paramPatientIdentifier.getPageSize());
			if(EJBUtil.stringToNum(paramPatientIdentifier.getPageSize()) > 0){
				if(EJBUtil.stringToNum(paramPatientIdentifier.getPageSize()) <= 50){
					pageSize = EJBUtil.stringToNum(paramPatientIdentifier.getPageSize());
				} else {
					pageSize =50;
				}
			}			
		} catch (Exception ex){			
			ex.printStackTrace();

			((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.DATA_VALIDATION, "Invalid Value for Requested Page Size: " + paramPatientIdentifier.getPageSize()));
			returnConnection(conn);
			/*((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.DATA_VALIDATION, "Invalid Value for Requested Record Count: " + paramPatientIdentifier.getRequestedRecordCount()));
			returnConnection(conn);
*/
	        throw new OperationException();
		}
		try{
			if(paramPatientIdentifier.isExactSearch()){
				isExactSearch = true;
			} else {
				isExactSearch = false;
			}
		} catch (Exception ex){
			ex.printStackTrace();
			((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.DATA_VALIDATION, "Invalid Value for ExactSearch: " + paramPatientIdentifier.isExactSearch()));
			returnConnection(conn);
	        throw new OperationException();
		}
		
		
		// ---- page Number check
		try
		{
			if (logger.isDebugEnabled()) logger.debug("Requested page number " + paramPatientIdentifier.getPageNumber());
			if(paramPatientIdentifier.getPageNumber() == null || (EJBUtil.stringToNum(paramPatientIdentifier.getPageNumber()) <= 0))
			{
				pageNumber = 1;
			}
			else
			{
				pageNumber = EJBUtil.stringToNum(paramPatientIdentifier.getPageNumber());
			}	
		}
		catch (Exception ex){			
			ex.printStackTrace();
			((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.DATA_VALIDATION, "Invalid Value for Page Number: " + paramPatientIdentifier.getPageNumber()));
	        throw new OperationException();
		}
		/*try{
			if (logger.isDebugEnabled()) logger.debug("Requested page number " + paramPatientIdentifier.getPageNumber());
			if(EJBUtil.stringToNum(paramPatientIdentifier.getPageNumber()) > 0){
				if(EJBUtil.stringToNum(paramPatientIdentifier.getPageNumber()) <= 50){
					pageNumber = EJBUtil.stringToNum(paramPatientIdentifier.getPageNumber());
				} else {
					pageNumber =1;
				}
			}			
		} catch (Exception ex){			
			ex.printStackTrace();
			((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.DATA_VALIDATION, "Invalid Value for Requested Record Count: " + paramPatientIdentifier.getRequestedRecordCount()));
	        throw new OperationException();
		}*/
	//	sbSelect.append("SELECT DISTINCT EP.PERSON_CODE, EP.PERSON_FNAME, EP.PERSON_LNAME, TO_CHAR(EP.PERSON_DOB, 'YYYY/MM/DD') AS PERSON_DOB,  EP.FK_CODELST_PSTAT, EP.FK_CODELST_GENDER ");
		sbSelect.append("SELECT DISTINCT EP.PK_PERSON, EP.PERSON_CODE, EP.PERSON_FNAME, EP.PERSON_LNAME, TO_CHAR(EP.PERSON_DOB, 'YYYY/MM/DD') AS PERSON_DOB,  EP.FK_CODELST_PSTAT, EP.FK_CODELST_GENDER,(SELECT SITE_ALTID FROM ER_SITE WHERE pk_site = EP.FK_SITE) AS SITE_ALTID, (SELECT SITE_NAME FROM ER_SITE WHERE pk_site = EP.FK_SITE) AS SITE_NAME, EP.FK_SITE, EP.FK_CODELST_RACE,fac.pat_facilityid ");
		
		/*sbSelect.append("SELECT DISTINCT EP.PERSON_CODE, EP.PERSON_FNAME, EP.PERSON_LNAME, TO_CHAR(EP.PERSON_DOB, 'YYYY/MM/DD') AS PERSON_DOB,  (SELECT CODELST_SUBTYP FROM ERES.ER_CODELST WHERE PK_CODELST = EP.FK_CODELST_PSTAT) AS SURVSTATUSCODE, " + 
						"(SELECT CODELST_DESC FROM ERES.ER_CODELST WHERE PK_CODELST = EP.FK_CODELST_PSTAT) AS SURVSTATUSDESC, (SELECT CODELST_SUBTYP FROM ERES.ER_cODELST WHERE PK_CODELST = EP.FK_CODELST_GENDER) AS GENDERCODE, " +
						"(SELECT CODELST_DESC FROM ERES.ER_cODELST WHERE PK_CODELST = EP.FK_CODELST_GENDER) AS GENDERDESC  ");
		*/
		
		PatientDao pdao = new PatientDao();
		int minPHIRight = pdao.getViewPHIRight(userId, grpId);			
		
		if (minPHIRight >= 4)
	    {
	      sbSelect.append(", 4 as right_mask");
	    }
	    else
	    {
	      sbSelect.append(", pkg_studystat.f_get_patientright(" + userId + ", " + grpId + ", pk_person ) right_mask");
	    }
		
		sbFrom.append(" FROM EPAT.PERSON EP, ER_PATFACILITY fac, ER_USERSITE usr ");
			
		sbWhere.append(" WHERE usr.fk_site = fac.fk_site AND usersite_right>=4 AND fac.patfacility_accessright > 0 AND fk_per = pk_person AND EP.fk_account = " + accountId + " and fk_user = " + userId);
		
		if((!EJBUtil.isEmpty(paramPatientIdentifier.getPatientID())) && (!(paramPatientIdentifier.getPatientID().equals("?")))){	
			if(isExactSearch){
				sbWhere.append(" AND ( lower(person_code) = lower('" + paramPatientIdentifier.getPatientID() + "') or  lower(fac.pat_facilityid) = lower('" + paramPatientIdentifier.getPatientID() + "') )  ");
			} else {
				sbWhere.append(" AND ( lower(person_code) LIKE lower('%" + paramPatientIdentifier.getPatientID() + "%') or  lower(fac.pat_facilityid) LIKE lower('%" + paramPatientIdentifier.getPatientID() + "%') )  ");
			}
			requestedParameterCount++;
		}
		
		//Added By Aman
		if((!EJBUtil.isEmpty(paramPatientIdentifier.getPatientFacilityID())) && (!(paramPatientIdentifier.getPatientFacilityID().equals("?")))){	
			if(isExactSearch){
				sbWhere.append(" AND  lower(fac.pat_facilityid) = lower('" + paramPatientIdentifier.getPatientFacilityID() + "')   ");
			} else {
				sbWhere.append(" AND  lower(fac.pat_facilityid) LIKE lower('%" + paramPatientIdentifier.getPatientFacilityID() + "%')   ");
			}
			requestedParameterCount++;
		}
		
		if((paramPatientIdentifier.getPatDateofBirth() != null) && (!paramPatientIdentifier.getPatDateofBirth().toString().equals("?")) && (!EJBUtil.isEmpty(paramPatientIdentifier.getPatDateofBirth().toString()))) {
			try{
				if (logger.isDebugEnabled()) logger.debug("DOB recieved: " + paramPatientIdentifier.getPatDateofBirth());
				String personDOB = formatter.format(paramPatientIdentifier.getPatDateofBirth());
				if (logger.isDebugEnabled()) logger.debug("DOB Formatted: " + personDOB);
				sbWhere.append(" AND EP.PERSON_DOB = TO_DATE('" + personDOB + "', 'YYYY/MM/DD') ");
			requestedParameterCount++;
			} catch (Exception ex){
				((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.DATA_VALIDATION, "Invalid Date of Birth"));
				returnConnection(conn);
		        throw new OperationException();
			}
		}
		if(!EJBUtil.isEmpty(paramPatientIdentifier.getPatFirstName())){
			if(isExactSearch){
				sbWhere.append(" AND LOWER(EP.PERSON_FNAME) = LOWER(('" + paramPatientIdentifier.getPatFirstName() + "')) ");
			} else {
				sbWhere.append(" AND LOWER(EP.PERSON_FNAME) LIKE LOWER(('%" + paramPatientIdentifier.getPatFirstName() + "%')) ");
			}
			requestedParameterCount++;
		}
		if(!EJBUtil.isEmpty(paramPatientIdentifier.getPatLastName())){
			if(isExactSearch){
				sbWhere.append(" AND LOWER(EP.PERSON_LNAME) = LOWER(('" + paramPatientIdentifier.getPatLastName() + "')) ");
			} else {
				sbWhere.append(" AND LOWER(EP.PERSON_LNAME) LIKE LOWER(('%" + paramPatientIdentifier.getPatLastName() + "%')) ");
			}
			requestedParameterCount++; 
		}

		if ((paramPatientIdentifier.getGender() != null) && (paramPatientIdentifier.getGender().getCode() != null)) {
		      try
		      {
		    	  sbWhere.append(" and ep.fk_codelst_gender  = " + AbstractService.dereferenceCodeStr(paramPatientIdentifier.getGender(), "gender", usrBean));
		    	  requestedParameterCount++;
		      }
		      catch (CodeNotFoundException e) {
		        ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
		          new Issue(
		          IssueTypes.DATA_VALIDATION, 
		          "Gender Code Not Found: " + paramPatientIdentifier.getGender().getCode()));
				returnConnection(conn);
		        throw new OperationException();
		      }
		    }
		
		//Added By Aman
		if ((paramPatientIdentifier.getRace() != null) && (paramPatientIdentifier.getRace().getCode() != null)) {
		      try
		      {
		    	  sbWhere.append(" and ep.fk_codelst_race  = " + AbstractService.dereferenceCodeStr(paramPatientIdentifier.getRace(), "race", usrBean));
		    	  requestedParameterCount++;
		      }
		      catch (CodeNotFoundException e) {
		        ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
		          new Issue(
		          IssueTypes.DATA_VALIDATION, 
		          "Race Code Not Found: " + paramPatientIdentifier.getRace().getCode()));
				returnConnection(conn);
		        throw new OperationException();
		      }
		    }
		
		
		if(paramPatientIdentifier.getPatSpecialty() != null && (paramPatientIdentifier.getPatSpecialty().getCode() != null)) {
			try
	          {
				sbWhere.append("  and  ',' || fac.patfacility_splaccess || ',' like ('%," + AbstractService.dereferenceCodeStr(paramPatientIdentifier.getPatSpecialty(), "prim_sp", usrBean) + ",%')");
				requestedParameterCount++;
	          }
	          catch (CodeNotFoundException cde) {
	            ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
	              new Issue(
	              IssueTypes.DATA_VALIDATION, 
	              "Specialty Code Not Found: " + paramPatientIdentifier.getPatSpecialty().getCode()));
				returnConnection(conn);
	            throw new OperationException();
	          }
		}
		
		
		if ((paramPatientIdentifier.getPatSurvivalStat() != null) && (paramPatientIdentifier.getPatSurvivalStat().getCode() != null))
	    {
	      try
	      {
	    	  sbWhere.append("  and  ep.fk_codelst_pstat = " + AbstractService.dereferenceCodeStr(paramPatientIdentifier.getPatSurvivalStat(), "patient_status", usrBean));
	    	  requestedParameterCount++;
	      } catch (CodeNotFoundException e) {
	        ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
	          new Issue(
	          IssueTypes.DATA_VALIDATION, 
	          "Survival Status Code Not Found: " + paramPatientIdentifier.getPatSurvivalStat().getCode()));
			returnConnection(conn);
	        throw new OperationException();
	      }
	    }

		if (logger.isDebugEnabled()) logger.debug("site id: "+ usrSiteId);
		if(usrSiteId != null && usrSiteId.size() > 0){
			for(int cnt = 0 ; cnt < usrSiteId.size() ; cnt++){
				//if(!siteId.equals("")){
				if(siteId.equals("")){
					siteId = usrSiteId.get(cnt).toString();
				} else {
					siteId = siteId + "," + usrSiteId.get(cnt).toString();
				}	
			}				
			sbWhere.append("  AND fac.fk_site IN ( " + siteId + ")");
		} 
		   
		String sortColumn ="";
		System.out.println(" --  u r in serach DAO method ---- ");
		if(paramPatientIdentifier.getSortBy()== null)
		{
			System.out.println(" -- inside the NULL case for sorting ====");
			sortColumn ="EP.PERSON_CODE";
		}	
		else if(paramPatientIdentifier.getSortBy().equals(PatientSearch.sorting.PatFirstName))
		{
			sortColumn ="EP.PERSON_FNAME"; 
		}
		else if(paramPatientIdentifier.getSortBy().equals(PatientSearch.sorting.PatLastName))
		{
			sortColumn ="EP.PERSON_LNAME";
		}
		else if(paramPatientIdentifier.getSortBy().equals(PatientSearch.sorting.PatientID))
		{
			sortColumn ="EP.PERSON_CODE";
		}
		else if(paramPatientIdentifier.getSortBy().equals(PatientSearch.sorting.Age))
		{
			sortColumn ="TO_CHAR(EP.PERSON_DOB, 'YYYY/MM/DD')";
		}
		else if(paramPatientIdentifier.getSortBy().equals(PatientSearch.sorting.Gender))
		{
			sortColumn ="EP.FK_CODELST_GENDER";
		}
		else if(paramPatientIdentifier.getSortBy().equals(PatientSearch.sorting.SurvivalStatus))
		{
			sortColumn ="ep.fk_codelst_pstat";
		}
		/*else
		{
			((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.INVALID_SORTBY_FIELD, "The sortBy value is not a valid field name"));
	        throw new OperationException();
		}*/	
		
		String sortOrder = "";
		if(paramPatientIdentifier.getSortOrder()== null)
		{
			sortOrder ="ASC";
		}	
		else if(paramPatientIdentifier.getSortOrder().equals(PatientSearch.ordering.ASCENDING))
		{
			sortOrder ="ASC"; 
		}
		else if(paramPatientIdentifier.getSortOrder().equals(PatientSearch.ordering.DESCENDING))
		{
			sortOrder ="DESC";
		}
		System.out.println(" --pageSize -- "+pageSize); 
		System.out.println(" --pageNumber -- "+pageNumber); 
		int uperLimit=pageSize*pageNumber;
    	int lowerLimit=(uperLimit-pageSize)+1;
		
		//String mainsql = "SELECT * from (" +  sbSelect.toString() + sbFrom.toString() + sbWhere.toString() + " ORDER BY EP.PERSON_CODE ) WHERE ROWNUM <=  " + requestedRecordCount ;
		String mainsql = "SELECT * from (select x.*, rownum row_num from (" +  sbSelect.toString() + sbFrom.toString() + sbWhere.toString() + " ORDER BY "+sortColumn +" "+ sortOrder+") x)WHERE row_num between " + lowerLimit+" and "+uperLimit ;
		//String recordCountSql = "SELECT COUNT(*) AS TOTALRECORDS from (" + sbSelect.toString() + sbFrom.toString() + sbWhere.toString() + " ORDER BY EP.PERSON_CODE )" ;
		String recordCountSql = "SELECT COUNT(*) AS TOTALRECORDS from (" + sbSelect.toString() + sbFrom.toString() + sbWhere.toString() + " ORDER BY "+sortColumn+" )" ;
		if (logger.isDebugEnabled()) logger.debug("mainsql: "  + mainsql);
		System.out.println("mainsql: "  + mainsql);
		int count = 0;
		int returnedPageSize = 0;
		boolean pageRangeError=false;
		if(requestedParameterCount> 0){
			try{			
		      pstmt = conn.prepareStatement(mainsql);
		      
		      ResultSet rsPatData = pstmt.executeQuery();
		      List<PatientDataBean> lstPatientDataBean = new ArrayList<PatientDataBean>();
		      if(rsPatData != null){
			      while (rsPatData.next())
			      {
			    	  ++count;
			    	  String sitePK = rsPatData.getString("FK_SITE");
		    		  String siteName = rsPatData.getString("SITE_NAME");
		    		  String siteAltID = rsPatData.getString("SITE_ALTID");
		    		  OrganizationIdentifier org = new OrganizationIdentifier(); ;
			    	  fieldMasking = rsPatData.getInt("right_mask");
			    	  PatientDataBean objPatientDataBean = new PatientDataBean();	
			    	  PatientIdentifier patIdentifier = new PatientIdentifier(); 
			    	  patIdentifier.setOID(objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PERSON, rsPatData.getInt("PK_PERSON")).getOID()); 
			    	  patIdentifier.setPK(rsPatData.getInt("PK_PERSON"));
			    	  
			    	  patIdentifier.setPatientId(rsPatData.getString("PERSON_CODE"));
			    	  objPatientDataBean.setGender(codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_GENDER,rsPatData.getString("FK_CODELST_GENDER"), accountId));
			    	  objPatientDataBean.setPatSurvivalStat(codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_PATIENT_SURVIVAl_STATUS , rsPatData.getString("FK_CODELST_PSTAT"), accountId));
			    	  //Race and Patient Facility Id added by Aman
			    	  objPatientDataBean.setRace(codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_RACE , rsPatData.getString("FK_CODELST_RACE"), accountId));
			    	  org.setOID(objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, (StringUtil.stringToInteger(sitePK))).getOID());
			    	  org.setPK((StringUtil.stringToInteger(sitePK)));
		    		  org.setSiteName(siteName);
		    		  org.setSiteAltId(siteAltID);
		    		  patIdentifier.setOrganizationId(org);
		    		  objPatientDataBean.setPatientIdentifier(patIdentifier);
			    	  
			    	  if(fieldMasking < 4){
				    	  objPatientDataBean.setPatFirstName("****");
				    	  objPatientDataBean.setPatLastName("****");			    	  
				    	  objPatientDataBean.setPatDateofBirth("****");
			    	  } else {
			    		  objPatientDataBean.setPatFirstName(rsPatData.getString("PERSON_FNAME"));
				    	  objPatientDataBean.setPatLastName(rsPatData.getString("PERSON_LNAME"));			    	  
				    	  objPatientDataBean.setPatDateofBirth(rsPatData.getString("PERSON_DOB"));
			    	  }
			    	  lstPatientDataBean.add(objPatientDataBean);		   
			      }
			      try{
				      rsPatData.close();
				      rsPatData= null;
				      pstmt.close();
				      pstmt = null;
			      } catch (SQLException ex){
			    	  if (logger.isDebugEnabled()) logger.debug("Exception while closing Resulset - " + ex);
			      }
		      }
		      returnedPageSize = count;      
		      if (logger.isDebugEnabled()) logger.debug("CountSQL: " + recordCountSql);
		      pstmt = conn.prepareStatement(recordCountSql);
		      rsPatData = pstmt.executeQuery();
		      if(rsPatData != null){
		    	  if(rsPatData.next()){
		    		  patRecordCount = rsPatData.getInt("TOTALRECORDS");
		    	  }
		    	  try{
				      rsPatData.close();
				      rsPatData= null;
				      pstmt.close();
				      pstmt = null;
			      } catch (SQLException ex){
			    	  if (logger.isDebugEnabled()) logger.debug("Exception while closing Resulset - " + ex);
			      }
		      }
		    
		      if(count == 0 && patRecordCount > 0)
		      {
		    	  pageRangeError=true;
		    	  throw new OperationException();
			  }	  
		      if (logger.isDebugEnabled()) logger.debug("Response size" + lstPatientDataBean.size());
		      PatientSearchResponse patSearchResponse = new PatientSearchResponse();
		      patSearchResponse.setPatDataBean(lstPatientDataBean);
		   //   patSearchResponse.setPatRecordCount(patRecordCount);
		      patSearchResponse.setTotalCount(patRecordCount);
		      patSearchResponse.setPageNumber(pageNumber);
		      patSearchResponse.setPageSize(returnedPageSize);
		      return patSearchResponse;
			}catch(Exception ex){
				if(pageRangeError)
				{
					 // ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.PAGE_NUMBER_OUT_OF_RANGE, "Value for pageNumber is out of range " ));
				    	
				}
				else
				{
					((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.UNKNOWN_THROWABLE, "Fault Occurred while processing: " + ex));
			        
				}
				throw new OperationException();

				/*((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.UNKNOWN_THROWABLE, "Fault Occurred while processing: " + ex));
		        throw new OperationException();*/
			} finally {
				returnConnection(conn);

			}
		} else {
			((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.DATA_VALIDATION, "Minimum 1 parameter must be specified in Search Criteria"));
			returnConnection(conn);
	        throw new OperationException();
		}
	}
	}
	
	
	//Added By Surendra to return distinct records based on patient facility id
	public PatientSearchResponse searchPatientWithDistinctFacilityId (PatientSearch paramPatientIdentifier, Map<String, Object> parameters) throws OperationException{
		  

		  
		Connection conn = getConnection();		
		PreparedStatement pstmt = null;
		StringBuffer sbSelect = new StringBuffer();
	    StringBuffer sbFrom = new StringBuffer();
	    StringBuffer sbWhere = new StringBuffer();
	    int requestedParameterCount = EJBUtil.stringToNum(parameters.get("requestedParameterCount").toString());
	   // int requestedRecordCount = 50;
	    int pageSize = 50;
	    int pageNumber = 0;
	    UserBean usrBean = (UserBean)parameters.get("callingUser");   
	    ObjectMapService objectMapService = (ObjectMapService) parameters.get("objectMapService");
	    String siteId = ""; //parameters.get("siteId").toString();
	    ArrayList usrSiteId = (ArrayList)parameters.get("arUsrSiteid");
		int userId = usrBean.getUserId();

		Format formatter = new SimpleDateFormat("yyyy/MM/dd");
		//String defGroup = userB.getUserGrpDefault();		
		String defGroup = usrBean.getUserGrpDefault();
		int grpId=EJBUtil.stringToNum(defGroup);
		int accountId = EJBUtil.stringToNum(usrBean.getUserAccountId());
		int codelstGender = 0;
		int codelstsurvivalStat = 0;
		int codelstPatSpecialty = 0;
		int fieldMasking = 0;

		boolean isExactSearch = false;
		
		CodeCache codeCache = CodeCache.getInstance();
		 
		int patRecordCount =0;
		try{
			if (logger.isDebugEnabled()) logger.debug("Requested Page Size " + paramPatientIdentifier.getPageSize());
			if(EJBUtil.stringToNum(paramPatientIdentifier.getPageSize()) > 0){
				if(EJBUtil.stringToNum(paramPatientIdentifier.getPageSize()) <= 50){
					pageSize = EJBUtil.stringToNum(paramPatientIdentifier.getPageSize());
				} else {
					pageSize =50;
				}
			}			
		} catch (Exception ex){			
			ex.printStackTrace();

			((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.DATA_VALIDATION, "Invalid Value for Requested Page Size: " + paramPatientIdentifier.getPageSize()));
			returnConnection(conn);
			/*((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.DATA_VALIDATION, "Invalid Value for Requested Record Count: " + paramPatientIdentifier.getRequestedRecordCount()));
			returnConnection(conn);
*/
	        throw new OperationException();
		}
		try{
			if(paramPatientIdentifier.isExactSearch()){
				isExactSearch = true;
			} else {
				isExactSearch = false;
			}
		} catch (Exception ex){
			ex.printStackTrace();
			((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.DATA_VALIDATION, "Invalid Value for ExactSearch: " + paramPatientIdentifier.isExactSearch()));
			returnConnection(conn);
	        throw new OperationException();
		}
		
		
		// ---- page Number check
		try
		{
			if (logger.isDebugEnabled()) logger.debug("Requested page number " + paramPatientIdentifier.getPageNumber());
			if(paramPatientIdentifier.getPageNumber() == null || (EJBUtil.stringToNum(paramPatientIdentifier.getPageNumber()) <= 0))
			{
				pageNumber = 1;
			}
			else
			{
				pageNumber = EJBUtil.stringToNum(paramPatientIdentifier.getPageNumber());
			}	
		}
		catch (Exception ex){			
			ex.printStackTrace();
			((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.DATA_VALIDATION, "Invalid Value for Page Number: " + paramPatientIdentifier.getPageNumber()));
	        throw new OperationException();
		}
		/*try{
			if (logger.isDebugEnabled()) logger.debug("Requested page number " + paramPatientIdentifier.getPageNumber());
			if(EJBUtil.stringToNum(paramPatientIdentifier.getPageNumber()) > 0){
				if(EJBUtil.stringToNum(paramPatientIdentifier.getPageNumber()) <= 50){
					pageNumber = EJBUtil.stringToNum(paramPatientIdentifier.getPageNumber());
				} else {
					pageNumber =1;
				}
			}			
		} catch (Exception ex){			
			ex.printStackTrace();
			((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.DATA_VALIDATION, "Invalid Value for Requested Record Count: " + paramPatientIdentifier.getRequestedRecordCount()));
	        throw new OperationException();
		}*/
	//	sbSelect.append("SELECT DISTINCT EP.PERSON_CODE, EP.PERSON_FNAME, EP.PERSON_LNAME, TO_CHAR(EP.PERSON_DOB, 'YYYY/MM/DD') AS PERSON_DOB,  EP.FK_CODELST_PSTAT, EP.FK_CODELST_GENDER ");
		sbSelect.append("SELECT DISTINCT EP.PK_PERSON, EP.PERSON_CODE, EP.PERSON_FNAME, EP.PERSON_LNAME, TO_CHAR(EP.PERSON_DOB, 'YYYY/MM/DD') AS PERSON_DOB,  EP.FK_CODELST_PSTAT, EP.FK_CODELST_GENDER,(SELECT SITE_ALTID FROM ER_SITE WHERE pk_site = EP.FK_SITE) AS SITE_ALTID, (SELECT SITE_NAME FROM ER_SITE WHERE pk_site = EP.FK_SITE) AS SITE_NAME, EP.FK_SITE ");
		
		/*sbSelect.append("SELECT DISTINCT EP.PERSON_CODE, EP.PERSON_FNAME, EP.PERSON_LNAME, TO_CHAR(EP.PERSON_DOB, 'YYYY/MM/DD') AS PERSON_DOB,  (SELECT CODELST_SUBTYP FROM ERES.ER_CODELST WHERE PK_CODELST = EP.FK_CODELST_PSTAT) AS SURVSTATUSCODE, " + 
						"(SELECT CODELST_DESC FROM ERES.ER_CODELST WHERE PK_CODELST = EP.FK_CODELST_PSTAT) AS SURVSTATUSDESC, (SELECT CODELST_SUBTYP FROM ERES.ER_cODELST WHERE PK_CODELST = EP.FK_CODELST_GENDER) AS GENDERCODE, " +
						"(SELECT CODELST_DESC FROM ERES.ER_cODELST WHERE PK_CODELST = EP.FK_CODELST_GENDER) AS GENDERDESC  ");
		*/
		
		PatientDao pdao = new PatientDao();
		int minPHIRight = pdao.getViewPHIRight(userId, grpId);			
		
		if (minPHIRight >= 4)
	    {
	      sbSelect.append(", 4 as right_mask");
	    }
	    else
	    {
	      sbSelect.append(", pkg_studystat.f_get_patientright(" + userId + ", " + grpId + ", pk_person ) right_mask");
	    }
		
		sbFrom.append(" FROM EPAT.PERSON EP, ER_PATFACILITY fac, ER_USERSITE usr ");
			
		sbWhere.append(" WHERE usr.fk_site = fac.fk_site AND usersite_right>=4 AND fac.patfacility_accessright > 0 AND fk_per = pk_person AND EP.fk_account = " + accountId + " and fk_user = " + userId);
		
		if((!EJBUtil.isEmpty(paramPatientIdentifier.getPatientID())) && (!(paramPatientIdentifier.getPatientID().equals("?")))){	
			if(isExactSearch){
				sbWhere.append(" AND ( lower(person_code) = lower('" + paramPatientIdentifier.getPatientID() + "') or  lower(fac.pat_facilityid) = lower('" + paramPatientIdentifier.getPatientID() + "') )  ");
			} else {
				sbWhere.append(" AND ( lower(person_code) LIKE lower('%" + paramPatientIdentifier.getPatientID() + "%') or  lower(fac.pat_facilityid) LIKE lower('%" + paramPatientIdentifier.getPatientID() + "%') )  ");
			}
			requestedParameterCount++;
		}
		
		
		if((!EJBUtil.isEmpty(paramPatientIdentifier.getPatientFacilityID())) && (!(paramPatientIdentifier.getPatientFacilityID().equals("?")))){	
			if(isExactSearch){
				sbWhere.append(" AND  lower(fac.pat_facilityid) = lower('" + paramPatientIdentifier.getPatientID() + "')   ");
			} else {
				sbWhere.append(" AND  lower(fac.pat_facilityid) LIKE lower('%" + paramPatientIdentifier.getPatientID() + "%')   ");
			}
			requestedParameterCount++;
		}
		
		// Added By Aman
		if ((!EJBUtil.isEmpty(paramPatientIdentifier.getPatientFacilityID()))
				&& (!(paramPatientIdentifier.getPatientFacilityID().equals("?")))) {
			if (isExactSearch) {
				sbWhere.append(" AND  lower(fac.pat_facilityid) = lower('"
						+ paramPatientIdentifier.getPatientFacilityID()
						+ "')   ");
			} else {
				sbWhere.append(" AND  lower(fac.pat_facilityid) LIKE lower('%"
						+ paramPatientIdentifier.getPatientFacilityID()
						+ "%')   ");
			}
			requestedParameterCount++;
		}
		//Added By Surendra for ADT Interface 18Aug2016
				if((!EJBUtil.isEmpty(paramPatientIdentifier.getPatientFacilityID())) && (!(paramPatientIdentifier.getPatientFacilityID().equals("?")))){	
					if(isExactSearch){
						sbWhere.append(" AND  lower(fac.pat_facilityid) = lower('" + paramPatientIdentifier.getPatientFacilityID() + "')   ");
					} else {
						sbWhere.append(" AND  lower(fac.pat_facilityid) LIKE lower('%" + paramPatientIdentifier.getPatientFacilityID() + "%')   ");
					}
					requestedParameterCount++;
				}
				//--------END--
		if((paramPatientIdentifier.getPatDateofBirth() != null) && (!paramPatientIdentifier.getPatDateofBirth().toString().equals("?")) && (!EJBUtil.isEmpty(paramPatientIdentifier.getPatDateofBirth().toString()))) {
			try{
				if (logger.isDebugEnabled()) logger.debug("DOB recieved: " + paramPatientIdentifier.getPatDateofBirth());
				String personDOB = formatter.format(paramPatientIdentifier.getPatDateofBirth());
				if (logger.isDebugEnabled()) logger.debug("DOB Formatted: " + personDOB);
				sbWhere.append(" AND EP.PERSON_DOB = TO_DATE('" + personDOB + "', 'YYYY/MM/DD') ");
			requestedParameterCount++;
			} catch (Exception ex){
				((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.DATA_VALIDATION, "Invalid Date of Birth"));
				returnConnection(conn);
		        throw new OperationException();
			}
		}
		if(!EJBUtil.isEmpty(paramPatientIdentifier.getPatFirstName())){
			if(isExactSearch){
				sbWhere.append(" AND LOWER(EP.PERSON_FNAME) = LOWER(('" + paramPatientIdentifier.getPatFirstName() + "')) ");
			} else {
				sbWhere.append(" AND LOWER(EP.PERSON_FNAME) LIKE LOWER(('%" + paramPatientIdentifier.getPatFirstName() + "%')) ");
			}
			requestedParameterCount++;
		}
		if(!EJBUtil.isEmpty(paramPatientIdentifier.getPatLastName())){
			if(isExactSearch){
				sbWhere.append(" AND LOWER(EP.PERSON_LNAME) = LOWER(('" + paramPatientIdentifier.getPatLastName() + "')) ");
			} else {
				sbWhere.append(" AND LOWER(EP.PERSON_LNAME) LIKE LOWER(('%" + paramPatientIdentifier.getPatLastName() + "%')) ");
			}
			requestedParameterCount++; 
		}

		if ((paramPatientIdentifier.getGender() != null) && (paramPatientIdentifier.getGender().getCode() != null)) {
		      try
		      {
		    	  sbWhere.append(" and ep.fk_codelst_gender  = " + AbstractService.dereferenceCodeStr(paramPatientIdentifier.getGender(), "gender", usrBean));
		    	  requestedParameterCount++;
		      }
		      catch (CodeNotFoundException e) {
		        ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
		          new Issue(
		          IssueTypes.DATA_VALIDATION, 
		          "Gender Code Not Found: " + paramPatientIdentifier.getGender().getCode()));
				returnConnection(conn);
		        throw new OperationException();
		      }
		    }
		
		// Added By Aman
		if ((paramPatientIdentifier.getRace() != null)
				&& (paramPatientIdentifier.getRace().getCode() != null)) {
			try {
				sbWhere.append(" and ep.fk_codelst_race  = "
						+ AbstractService.dereferenceCodeStr(
								paramPatientIdentifier.getRace(), "race",
								usrBean));
				requestedParameterCount++;
			} catch (CodeNotFoundException e) {
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues()
						.add(new Issue(IssueTypes.DATA_VALIDATION,
								"Race Code Not Found: "
										+ paramPatientIdentifier.getRace()
												.getCode()));
				returnConnection(conn);
				throw new OperationException();
			}
		}

		//Added By Surendra for Teaugo ADT Interface 18Aug2016
		if ((paramPatientIdentifier.getRace() != null) && (paramPatientIdentifier.getRace().getCode() != null)) {
		      try
		      {
		    	  sbWhere.append(" and ep.fk_codelst_race  = " + AbstractService.dereferenceCodeStr(paramPatientIdentifier.getRace(), "race", usrBean));
		    	  requestedParameterCount++;
		      }
		      catch (CodeNotFoundException e) {
		        ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
		          new Issue(
		          IssueTypes.DATA_VALIDATION, 
		          "Race Code Not Found: " + paramPatientIdentifier.getRace().getCode()));
				returnConnection(conn);
		        throw new OperationException();
		      }
		    }
		
		if(paramPatientIdentifier.getPatSpecialty() != null && (paramPatientIdentifier.getPatSpecialty().getCode() != null)) {
			try
	          {
				sbWhere.append("  and  ',' || fac.patfacility_splaccess || ',' like ('%," + AbstractService.dereferenceCodeStr(paramPatientIdentifier.getPatSpecialty(), "prim_sp", usrBean) + ",%')");
				requestedParameterCount++;
	          }
	          catch (CodeNotFoundException cde) {
	            ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
	              new Issue(
	              IssueTypes.DATA_VALIDATION, 
	              "Specialty Code Not Found: " + paramPatientIdentifier.getPatSpecialty().getCode()));
				returnConnection(conn);
	            throw new OperationException();
	          }
		}
		
		
		if ((paramPatientIdentifier.getPatSurvivalStat() != null) && (paramPatientIdentifier.getPatSurvivalStat().getCode() != null))
	    {
	      try
	      {
	    	  sbWhere.append("  and  ep.fk_codelst_pstat = " + AbstractService.dereferenceCodeStr(paramPatientIdentifier.getPatSurvivalStat(), "patient_status", usrBean));
	    	  requestedParameterCount++;
	      } catch (CodeNotFoundException e) {
	        ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
	          new Issue(
	          IssueTypes.DATA_VALIDATION, 
	          "Survival Status Code Not Found: " + paramPatientIdentifier.getPatSurvivalStat().getCode()));
			returnConnection(conn);
	        throw new OperationException();
	      }
	    }

		if (logger.isDebugEnabled()) logger.debug("site id: "+ usrSiteId);
		if(usrSiteId != null && usrSiteId.size() > 0){
			for(int cnt = 0 ; cnt < usrSiteId.size() ; cnt++){
				//if(!siteId.equals("")){
				if(siteId.equals("")){
					siteId = usrSiteId.get(cnt).toString();
				} else {
					siteId = siteId + "," + usrSiteId.get(cnt).toString();
				}	
			}				
			sbWhere.append("  AND fac.fk_site IN ( " + siteId + ")");
		} 
		   
		String sortColumn ="";
		System.out.println(" --  u r in serach DAO method ---- ");
		if(paramPatientIdentifier.getSortBy()== null)
		{
			System.out.println(" -- inside the NULL case for sorting ====");
			sortColumn ="EP.PERSON_CODE";
		}	
		else if(paramPatientIdentifier.getSortBy().equals(PatientSearch.sorting.PatFirstName))
		{
			sortColumn ="EP.PERSON_FNAME"; 
		}
		else if(paramPatientIdentifier.getSortBy().equals(PatientSearch.sorting.PatLastName))
		{
			sortColumn ="EP.PERSON_LNAME";
		}
		else if(paramPatientIdentifier.getSortBy().equals(PatientSearch.sorting.PatientID))
		{
			sortColumn ="EP.PERSON_CODE";
		}
		else if(paramPatientIdentifier.getSortBy().equals(PatientSearch.sorting.Age))
		{
			sortColumn ="TO_CHAR(EP.PERSON_DOB, 'YYYY/MM/DD')";
		}
		else if(paramPatientIdentifier.getSortBy().equals(PatientSearch.sorting.Gender))
		{
			sortColumn ="EP.FK_CODELST_GENDER";
		}
		else if(paramPatientIdentifier.getSortBy().equals(PatientSearch.sorting.SurvivalStatus))
		{
			sortColumn ="ep.fk_codelst_pstat";
		}
		/*else
		{
			((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.INVALID_SORTBY_FIELD, "The sortBy value is not a valid field name"));
	        throw new OperationException();
		}*/	
		
		String sortOrder = "";
		if(paramPatientIdentifier.getSortOrder()== null)
		{
			sortOrder ="ASC";
		}	
		else if(paramPatientIdentifier.getSortOrder().equals(PatientSearch.ordering.ASCENDING))
		{
			sortOrder ="ASC"; 
		}
		else if(paramPatientIdentifier.getSortOrder().equals(PatientSearch.ordering.DESCENDING))
		{
			sortOrder ="DESC";
		}
		System.out.println(" --pageSize -- "+pageSize); 
		System.out.println(" --pageNumber -- "+pageNumber); 
		int uperLimit=pageSize*pageNumber;
    	int lowerLimit=(uperLimit-pageSize)+1;
		
		//String mainsql = "SELECT * from (" +  sbSelect.toString() + sbFrom.toString() + sbWhere.toString() + " ORDER BY EP.PERSON_CODE ) WHERE ROWNUM <=  " + requestedRecordCount ;
		String mainsql = "SELECT * from (select x.*, rownum row_num from (" +  sbSelect.toString() + sbFrom.toString() + sbWhere.toString() + " ORDER BY "+sortColumn +" "+ sortOrder+") x)WHERE row_num between " + lowerLimit+" and "+uperLimit ;
		//String recordCountSql = "SELECT COUNT(*) AS TOTALRECORDS from (" + sbSelect.toString() + sbFrom.toString() + sbWhere.toString() + " ORDER BY EP.PERSON_CODE )" ;
		String recordCountSql = "SELECT COUNT(*) AS TOTALRECORDS from (" + sbSelect.toString() + sbFrom.toString() + sbWhere.toString() + " ORDER BY "+sortColumn+" )" ;
		if (logger.isDebugEnabled()) logger.debug("mainsql: "  + mainsql);
		System.out.println("mainsql: "  + mainsql);
		int count = 0;
		int returnedPageSize = 0;
		boolean pageRangeError=false;
		if(requestedParameterCount> 0){
			try{			
		      pstmt = conn.prepareStatement(mainsql);
		      
		      ResultSet rsPatData = pstmt.executeQuery();
		      List<PatientDataBean> lstPatientDataBean = new ArrayList<PatientDataBean>();
		      if(rsPatData != null){
			      while (rsPatData.next())
			      {
			    	  ++count;
			    	  String sitePK = rsPatData.getString("FK_SITE");
		    		  String siteName = rsPatData.getString("SITE_NAME");
		    		  String siteAltID = rsPatData.getString("SITE_ALTID");
		    		  OrganizationIdentifier org = new OrganizationIdentifier(); ;
			    	  fieldMasking = rsPatData.getInt("right_mask");
			    	  PatientDataBean objPatientDataBean = new PatientDataBean();	
			    	  PatientIdentifier patIdentifier = new PatientIdentifier(); 
			    	  patIdentifier.setOID(objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PERSON, rsPatData.getInt("PK_PERSON")).getOID()); 
			    	  patIdentifier.setPK(rsPatData.getInt("PK_PERSON"));
			    	  
			    	  patIdentifier.setPatientId(rsPatData.getString("PERSON_CODE"));
			    	  objPatientDataBean.setGender(codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_GENDER,rsPatData.getString("FK_CODELST_GENDER"), accountId));
			    	  objPatientDataBean.setPatSurvivalStat(codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_PATIENT_SURVIVAl_STATUS , rsPatData.getString("FK_CODELST_PSTAT"), accountId));
			    	  //Race and Patient Facility Id added by Surendra Teaugo ADT Interface 18Aug2016
			    	  objPatientDataBean.setRace(codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_RACE , rsPatData.getString("FK_CODELST_RACE"), accountId));
			    	  objPatientDataBean.setPatientFacilityId(rsPatData.getString("PAT_FACILITYID"));
			    
			    	  org.setOID(objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, (StringUtil.stringToInteger(sitePK))).getOID());
			    	  org.setPK((StringUtil.stringToInteger(sitePK)));
		    		  org.setSiteName(siteName);
		    		  org.setSiteAltId(siteAltID);
		    		  patIdentifier.setOrganizationId(org);
		    		  objPatientDataBean.setPatientIdentifier(patIdentifier);
			    	  
			    	  if(fieldMasking < 4){
				    	  objPatientDataBean.setPatFirstName("****");
				    	  objPatientDataBean.setPatLastName("****");			    	  
				    	  objPatientDataBean.setPatDateofBirth("****");
			    	  } else {
			    		  objPatientDataBean.setPatFirstName(rsPatData.getString("PERSON_FNAME"));
				    	  objPatientDataBean.setPatLastName(rsPatData.getString("PERSON_LNAME"));			    	  
				    	  objPatientDataBean.setPatDateofBirth(rsPatData.getString("PERSON_DOB"));
			    	  }
			    	  lstPatientDataBean.add(objPatientDataBean);		   
			      }
			      try{
				      rsPatData.close();
				      rsPatData= null;
				      pstmt.close();
				      pstmt = null;
			      } catch (SQLException ex){
			    	  if (logger.isDebugEnabled()) logger.debug("Exception while closing Resulset - " + ex);
			      }
		      }
		      returnedPageSize = count;      
		      if (logger.isDebugEnabled()) logger.debug("CountSQL: " + recordCountSql);
		      pstmt = conn.prepareStatement(recordCountSql);
		      rsPatData = pstmt.executeQuery();
		      if(rsPatData != null){
		    	  if(rsPatData.next()){
		    		  patRecordCount = rsPatData.getInt("TOTALRECORDS");
		    	  }
		    	  try{
				      rsPatData.close();
				      rsPatData= null;
				      pstmt.close();
				      pstmt = null;
			      } catch (SQLException ex){
			    	  if (logger.isDebugEnabled()) logger.debug("Exception while closing Resulset - " + ex);
			      }
		      }
		    
		      if(count == 0 && patRecordCount > 0)
		      {
		    	  pageRangeError=true;
		    	  throw new OperationException();
			  }	  
		      if (logger.isDebugEnabled()) logger.debug("Response size" + lstPatientDataBean.size());
		      PatientSearchResponse patSearchResponse = new PatientSearchResponse();
		      patSearchResponse.setPatDataBean(lstPatientDataBean);
		   //   patSearchResponse.setPatRecordCount(patRecordCount);
		      patSearchResponse.setTotalCount(patRecordCount);
		      patSearchResponse.setPageNumber(pageNumber);
		      patSearchResponse.setPageSize(returnedPageSize);
		      return patSearchResponse;
			}catch(Exception ex){
				if(pageRangeError)
				{
					 // ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.PAGE_NUMBER_OUT_OF_RANGE, "Value for pageNumber is out of range " ));
				    	
				}
				else
				{
					((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.UNKNOWN_THROWABLE, "Fault Occurred while processing: " + ex));
			        
				}
				throw new OperationException();

				/*((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.UNKNOWN_THROWABLE, "Fault Occurred while processing: " + ex));
		        throw new OperationException();*/
			} finally {
				returnConnection(conn);

			}
		} else {
			((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.DATA_VALIDATION, "Minimum 1 parameter must be specified in Search Criteria"));
			returnConnection(conn);
	        throw new OperationException();
		}
	}
	
	//Bug Fix : 16357
    public static boolean ifOrgExists(int sitePK) throws OperationException{
    	
    	String sql = "select count(*) as Count from er_site where pk_site = ?";
    	PreparedStatement pstmt  = null;
    	Connection conn =  null;
    	boolean flag = false;
    	try{
    		conn = getConnection();
    		pstmt = conn.prepareStatement(sql);
        	pstmt.setInt(1, sitePK);
        	
        	ResultSet rs = pstmt.executeQuery();
        			
        	while(rs.next()){
        		if(rs.getInt("Count") ==1)
				   flag = true;
				else 
				   flag =  false;
        	}
        	return flag;
    	}catch(Throwable t){
    		t.printStackTrace();
    		throw new OperationException();
    	}
    	
    	finally{
    		try{
    			if(pstmt!=null){
    				pstmt.close();
    			}
    		}catch(Exception e){
    			
    		}
    		try{
    			if(conn!=null){
    				conn.close();
    			}
    		}catch(Exception e){
    			
    		}
    	}
    }
}
