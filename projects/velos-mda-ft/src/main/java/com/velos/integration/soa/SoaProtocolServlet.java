package com.velos.integration.soa;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class SoaProtocolServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	public static final String CHARSET = "UTF-8";
	public static final String EMPTY_STR = "";
	public static final String STUDY_NUMBER_STR = "studyNumber";
	public static final String VERSION_STR = "version";
	public static final String SUBMISSION_DATE_STR = "submissionDate";
	public static final String TYPE_STR = "type";
	public static final String TYPE_CAT_TRIGGER = "Presubmission";
	public static final String TYPE_PROTOCOL = "Protocol";
	public static final String TYPE_AMENDMENT = "Amendment";
	public static final String VALID_USERNAME_STR = "valid-username";
	public static final String VALID_PASSWORD_STR = "valid-password";
	public static final String VALID_TOKEN_STR = "valid-token";
	public static final String USERNAME_STR = "username";
	public static final String PASSWORD_STR = "password";
	public static final String TOKEN_STR = "token";
	
	public void doGet(HttpServletRequest httpservletrequest,
			HttpServletResponse httpservletresponse) throws ServletException, IOException {
		processServlet(httpservletrequest,httpservletresponse);
	}

	public void doPost(HttpServletRequest httpservletrequest,
			HttpServletResponse httpservletresponse) throws ServletException, IOException {
		processServlet(httpservletrequest,httpservletresponse);
	}

	private static ResourceBundle bundle = ResourceBundle.getBundle("soa-protocol");
	
	private void processServlet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding(CHARSET);
		response.setCharacterEncoding(CHARSET);

		// Security check
		HttpSession session = request.getSession(false);
		String validUsername = bundle.getString(VALID_USERNAME_STR);
		String validPassword = bundle.getString(VALID_PASSWORD_STR);
		String validToken = bundle.getString(VALID_TOKEN_STR);
		String username = request.getParameter(USERNAME_STR);
		String password = request.getParameter(PASSWORD_STR);
		String token = request.getParameter(TOKEN_STR);
		if (username == null || (password == null && token == null) ||
				!username.equals(validUsername)) {
			showForbiddenMessage(response);
			return;
		}
		if ((password != null && !password.equals(validPassword)) || 
				(token != null && !token.equals(validToken))) {
			showForbiddenMessage(response);
			return;
		}
		callMdaClient(request, response);
	}
	
	private Map<String, String> processCatTrigger(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException  {
        ServletOutputStream out = response.getOutputStream();
        Map<String, String> requestMap = new HashMap<String, String>();
		requestMap.put(SoaProtocolClient.SubmissionTypeKey, TYPE_CAT_TRIGGER);
		requestMap.put(SoaProtocolClient.ActionKey, EMPTY_STR);
		requestMap.put(SoaProtocolClient.InterfaceVersionKey, EMPTY_STR);
		return requestMap;
	}
	
	private Map<String, String> processAmendment(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException  {
        ServletOutputStream out = response.getOutputStream();
        Map<String, String> requestMap = new HashMap<String, String>();
		String version = request.getParameter(VERSION_STR);
		if (version == null) {
			out.println("errorMsg=Invalid data A1");
			return null;
		}
		String minorVersion = null;
		int minorVerInt = 0;
		try {
			minorVersion = version.substring(version.indexOf(".")+1);
			minorVerInt = Integer.parseInt(minorVersion);
		} catch(Exception e) {
			out.println("errorMsg=Invalid data A2");
			return null;
		}
		String action = EMPTY_STR;
		if (minorVerInt == 1) {
			action = "add";
		} else if (minorVerInt > 1) {
			action = "update";
		}
		requestMap.put(SoaProtocolClient.SubmissionTypeKey, TYPE_AMENDMENT);
		requestMap.put(SoaProtocolClient.ActionKey, action);
		requestMap.put(SoaProtocolClient.InterfaceVersionKey, version);
		return requestMap;
	}
	
	private Map<String, String> processProtocol(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException  {
        ServletOutputStream out = response.getOutputStream();
        Map<String, String> requestMap = new HashMap<String, String>();
		String version = request.getParameter(VERSION_STR);
		if (version == null) {
			out.println("errorMsg=Invalid data P1");
			return null;
		}
		String majorVersion = null;
		int majorVerInt = 0;
		try {
			if (version.indexOf(".") < 0) {
				majorVersion = version;
				majorVerInt = Integer.parseInt(majorVersion);
			} else {
				majorVersion = version.substring(0, version.indexOf("."));
				majorVerInt = Integer.parseInt(majorVersion);
			}
		} catch(Exception e) {
			out.println("errorMsg=Invalid data P2");
			return null;
		}
		String action = EMPTY_STR;
		if (majorVerInt == 1) {
			action = "add";
		} else if (majorVerInt > 1) {
			action = "update";
		}
		requestMap.put(SoaProtocolClient.SubmissionTypeKey, TYPE_PROTOCOL);
		requestMap.put(SoaProtocolClient.ActionKey, action);
		requestMap.put(SoaProtocolClient.InterfaceVersionKey, version);
		return requestMap;
	}
	
	private void callMdaClient(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
        ServletOutputStream out = response.getOutputStream();
		Map<String, String> requestMap = null;
		String studyNumber = request.getParameter(STUDY_NUMBER_STR);
		System.out.println("studyNumber="+studyNumber);
		if (studyNumber == null) {
			out.println("errorMsg=Invalid data 1");
			return;
		}
		String submissionDate = request.getParameter(SUBMISSION_DATE_STR);
		if (submissionDate == null) {
			out.println("errorMsg=Invalid data 2");
			return;
		}
		String type = request.getParameter(TYPE_STR);
		if (type == null) {
			out.println("errorMsg=Invalid data 3");
			return;
		}		
		if (TYPE_PROTOCOL.equals(type)) {
			requestMap = processProtocol(request, response);
		} else if (TYPE_CAT_TRIGGER.equals(type)) {
			requestMap = processCatTrigger(request, response);
		} else if (TYPE_AMENDMENT.equals(type)) {
			requestMap = processAmendment(request, response);
		}
		if (requestMap == null) { return; }
		requestMap.put(SoaProtocolClient.StudyNumberKey, studyNumber);
		requestMap.put(SoaProtocolClient.SubmissionDateKey, submissionDate);
		SoaProtocolClient mdaClient = new SoaProtocolClient();
		Map<String, Object> resultMap = null;
        try {
        	resultMap = mdaClient.handleRequest(requestMap);
    		System.out.println("Received:"+resultMap.get(SoaProtocolClient.GetStudyResponseKey));
            out.println("errorMsg=success");
        } catch (Exception e) {
        	e.printStackTrace();
            out.println("errorMsg=failed");
        }
	}
	
    private void showForbiddenMessage(HttpServletResponse resp) {
        try {
            ServletOutputStream out = resp.getOutputStream();
            out.println("<html><head><title>Forbidden</title></head>");
            out.println("<body>");
            out.println("<h1>Forbidden</h1>");
            out.println("<p>You do not have permission to access this server or file.</p>");
            out.println("</body>");
            out.println("</html>");
        } catch (IOException e) {
        }
    }

}
