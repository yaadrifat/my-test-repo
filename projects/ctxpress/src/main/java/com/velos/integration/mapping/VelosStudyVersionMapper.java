package com.velos.integration.mapping;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.velos.integration.util.CodelstsCache;
import com.velos.services.Code;
import com.velos.services.Study;
import com.velos.services.StudyStatusHistory;
import com.velos.services.StudyVersion;
import com.velos.services.Versions;

public class VelosStudyVersionMapper {
	private static Logger logger = Logger.getLogger(VelosStudyVersionMapper.class);
	
	private Map<VelosKeys, Object> dataMap = null;
	List<StudyVersion> createStudyVersList = null;
	Properties prop = null;
	private String endpoint = null;



	public VelosStudyVersionMapper(String endpoint){
		this.endpoint = endpoint;
	}


	public Map<VelosKeys, Object> mapGetStudyVersOrgList(Versions versions,Map<VelosKeys, Object> requestMap) {
		if (EndpointKeys.CTXpress.toString().equals(endpoint)) {
			mapForGetStudyVers(versions,requestMap);
		}
		return dataMap;
	}



	public Map<VelosKeys, Object> mapCreateStudyVersOrgList(Map<VelosKeys, Object> dataMap) {

		if (EndpointKeys.CTXpress.toString().equals(endpoint)) {
			mapCrtStudyVersOrgList(dataMap);
		}

		return dataMap;
	}

	
	
	
	public Map<VelosKeys, Object> mapCrtStudyVersOrgList(Map<VelosKeys, Object> dataMap) {
		logger.info("******** Mapping for Create Study Versions *********");

		List<StudyVersion> studyVersionList = null;
		String orgName = null;
		String partVersStatus= null;
		String BroadVersStatus = null;
		String versStatus = null;
		String versCatgCode = null;
		String versType = null ;
		Code code = null;
		String brodversStatDesc = null;
		String catgcode =null;
		String typeCode = null;


		prop = (Properties)dataMap.get(ProtocolKeys.PropObj);
		studyVersionList = (List<StudyVersion>) dataMap.get(ProtocolKeys.CreateStudyVersList);
		orgName = ((String)dataMap.get(ProtocolKeys.OrganizationName)).trim();
		createStudyVersList =new ArrayList<StudyVersion>();




		try{
			logger.info("****Config.properties for versions ****");
			BroadVersStatus =  prop.getProperty("version.status.subtype").trim();
			brodversStatDesc = prop.getProperty("version.status.desc").trim();

			logger.info("BroadCasting Versions Status ==>"+BroadVersStatus);
			logger.info("BroadCasting Versions Description ==>"+brodversStatDesc);

			logger.info(orgName +".properties for versions");
			partVersStatus =  prop.getProperty(orgName+"_verstatus.Code").trim();
			logger.info("Participant Versions Status ==>"+brodversStatDesc);

		}catch(Exception e){
			logger.info("Participant or Broad casting Versions Status values are not mapped in properties files");
		}


		for(StudyVersion sv : studyVersionList){

			List<StudyStatusHistory> stdStatusHist = null;

			try{
				
				stdStatusHist = sv.getStudyStatusHistory();
				
			}catch(Exception e){
				logger.info(" Version Study Status History is Not defined for Version :: "+sv.getStudyVerIdentifier().getSTUDYVERNUMBER());	
			}
			

			for(StudyStatusHistory ssh : stdStatusHist){
				
				versStatus = null;
				
				try{
					versStatus = ssh.getPKCODELSTSTAT().getCode();

				}catch(Exception e){}

				if(versStatus != null){
					if(versStatus.equals(BroadVersStatus)){

						logger.info("Version Object ===> "+sv);
						logger.info("Version Number ===>"+sv.getStudyVerIdentifier().getSTUDYVERNUMBER());
						logger.info("Version Status ===>"+versStatus);

						sv.setSTUDYVERSTATUS(partVersStatus);	

						try{
							Code versionCategory = sv.getSTUDYVERCATEGORY();
							if(versionCategory!=null){
								versCatgCode = versionCategory.getCode();
								String verCatType = versionCategory.getType();
								String verCatDesc = versionCategory.getDescription();
								Code mappedCategory = CodelstsCache.getMappedCode(dataMap,"vercategory",verCatType,versCatgCode,verCatDesc);
								sv.setSTUDYVERCATEGORY(mappedCategory);
							}
						}catch(Exception e){
							logger.info(" Version Category is Not defined for Version :: "+sv.getStudyVerIdentifier().getSTUDYVERNUMBER());	
						}

						/*if(versCatgCode != null && !"".equals(versCatgCode)){
							logger.info("Version Categories Codes ===>"+versCatgCode);
							try{
								if(prop.containsKey(orgName+"_vercategory."+versCatgCode) && !"".equals(prop.getProperty(orgName+"_vercategory."+versCatgCode).trim())){
							 catgcode =prop.getProperty(orgName+"_vercategory."+versCatgCode).trim();
								}else if(versCatgCode != null){
									catgcode=versCatgCode;
								}
							}catch(Exception e){
								logger.info("Study Version Categories values are not mapped properly in "+orgName +" properties file");
							}
							if(catgcode == null || catgcode.equals("")){
								logger.info("Versions Category Codelst Sub Type mismatch pleace check in"+orgName +" . Properties files");
							}

							code = new Code();
							code.setCode(catgcode);
							sv.setSTUDYVERCATEGORY(code);
						}*/
						

						try{
							Code codeVerType = sv.getSTUDYVERTYPE();
							if(codeVerType!=null){
								versType = codeVerType.getCode();
								String type = codeVerType.getType();
								String verTypeDesc = codeVerType.getDescription();
								Code mappedVerType = CodelstsCache.getMappedCode(dataMap,"versType",type,versType,verTypeDesc);
								sv.setSTUDYVERTYPE(mappedVerType);
							}
							
						}catch(Exception e){
							logger.info("Study Version Type is Not defined for Version :: "+sv.getStudyVerIdentifier().getSTUDYVERNUMBER());	
						}
						

						/*if(versType != null && !"".equals(versType)){
							logger.info("Study Version Type Codes ===>"+versType);
							
							try{
								if(prop.containsKey(orgName+"_versType."+versType) && !"".equals(prop.getProperty(orgName+"_versType."+versType).trim())){
							 typeCode = prop.getProperty(orgName+"_versType."+versType).trim();
								}else if(versType != null) {
									typeCode=versType;
								}
							
						}catch(Exception e){
							logger.info("Study Version Type values are not mapped properly in "+orgName +" properties file");
						}

							if(typeCode == null || typeCode.equals("")){
								logger.info("Versions Types Codelst Sub Type mismatch pleace check in"+orgName +" . Properties files");
							}

							code = new Code();
							code.setCode(typeCode);
							sv.setSTUDYVERTYPE(code);
						}*/
						

						if(ssh !=null){
							code = new Code();
							code.setCode(partVersStatus);
							ssh.setPKCODELSTSTAT(code);
						}
						createStudyVersList.add(sv);
					}
				}
			}
		}
		dataMap.put(ProtocolKeys.CreateStudyVersfinalList,createStudyVersList);
		
		return dataMap;
	}	


	public Map<VelosKeys, Object> mapForGetStudyVers(Versions versions,Map<VelosKeys, Object> requestMap){
		logger.info("******* Getting Study Versions ***********");
		
		List<StudyVersion> studyVersList = null;
		String versStatus = null;
		
		
		createStudyVersList =new ArrayList<StudyVersion>();
		prop = (Properties)requestMap.get(ProtocolKeys.PropObj);
		dataMap = new HashMap<VelosKeys, Object>();
		


		try{
			studyVersList=versions.getVersion();
		}catch(Exception e){

		}

		if(studyVersList != null && !studyVersList.isEmpty()){

			for(StudyVersion sv : studyVersList){

				List<StudyStatusHistory> stdStatusHist = null;
				stdStatusHist = sv.getStudyStatusHistory();

				for(StudyStatusHistory ssh : stdStatusHist){
					versStatus = null;
					try{
						versStatus = ssh.getPKCODELSTSTAT().getCode();
					}catch(Exception e){

					}

					if(versStatus != null){
						if(versStatus.equals(prop.getProperty("version.status.subtype"))){

							logger.info("Version Object ===> "+sv);
							logger.info("Version Number ===>"+sv.getStudyVerIdentifier().getSTUDYVERNUMBER());
							logger.info("Version Status ===>"+versStatus);

							createStudyVersList.add(sv);
						}
					}
				}
			}
		}
		logger.info("GetVersions from Broadcasting site Completed");
		dataMap.put(ProtocolKeys.CreateStudyVersList,createStudyVersList);

		return dataMap;
	}
}
