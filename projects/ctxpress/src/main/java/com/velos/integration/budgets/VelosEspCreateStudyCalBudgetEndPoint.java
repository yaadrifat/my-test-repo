package com.velos.integration.budgets;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import com.velos.services.OperationException_Exception;

@WebService(targetNamespace = "http://velos.com/services/", name = "BudgetSEI")
public interface VelosEspCreateStudyCalBudgetEndPoint {
	
	@WebResult(name = "Response", targetNamespace = "")
    @RequestWrapper(localName = "createStudyCalBudget", targetNamespace = "http://velos.com/services/", className = "com.velos.services.CreateStudyCalBudget")
    @WebMethod
    @ResponseWrapper(localName = "createStudyCalBudgetResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.CreateStudyCalBudgetResponse")
    public com.velos.services.ResponseHolder createStudyCalBudget(
        @WebParam(name = "Budget", targetNamespace = "")
        com.velos.services.BudgetDetail budget
    ) throws OperationException_Exception;

}
