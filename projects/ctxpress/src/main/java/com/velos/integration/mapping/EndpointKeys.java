package com.velos.integration.mapping;

public enum EndpointKeys implements VelosKeys {
	// Convention: Each enum type should begin with upper case and use camel case for the rest.
	//             The corresponding string can be in any case to match the business need.
	//             If the string contains a period, use _ for that in the corresponding enum type.
	Endpoint("endpoint"),
	CTXpress("ctxpress"),
	VelosRemote("velosRemote");
	
	EndpointKeys(String key) {
		this.key = key;
	}
	
	private String key;
	
	@Override
	public String toString() {
		return key;
	}
}
