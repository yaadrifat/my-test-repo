package com.velos.integration.espclient;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import com.velos.services.OperationException_Exception;
import com.velos.services.OperationRolledBackException_Exception;

@WebService(targetNamespace="http://velos.com/services/", name="StudySEI")
public interface VelosEspAddStudyStatus {
    @WebResult(name = "Response", targetNamespace = "")
    @RequestWrapper(localName = "addStudyStatus", targetNamespace = "http://velos.com/services/", className = "com.velos.services.AddStudyStatus")
    @WebMethod
    @ResponseWrapper(localName = "addStudyStatusResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.AddStudyStatusResponse")
    public com.velos.services.ResponseHolder addStudyStatus(
        @WebParam(name = "StudyIdentifier", targetNamespace = "")
        com.velos.services.StudyIdentifier studyIdentifier,
        @WebParam(name = "StudyStatus", targetNamespace = "")
        com.velos.services.StudyStatus studyStatus
    ) throws OperationException_Exception, OperationRolledBackException_Exception;
}
