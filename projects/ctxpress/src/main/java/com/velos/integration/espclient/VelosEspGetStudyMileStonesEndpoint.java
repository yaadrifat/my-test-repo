package com.velos.integration.espclient;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import com.velos.services.OperationException_Exception;

@WebService(targetNamespace = "http://velos.com/services/", name = "MilestoneSEI")
public interface VelosEspGetStudyMileStonesEndpoint {
	@WebResult(name = "Milestones", targetNamespace = "")
    @RequestWrapper(localName = "getStudyMilestones", targetNamespace = "http://velos.com/services/", className = "com.velos.milestones.services.GetStudyMilestones")
    @WebMethod
    @ResponseWrapper(localName = "getStudyMilestonesResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.milestones.services.GetStudyMilestonesResponse")
    public com.velos.services.Milestone getStudyMilestones(
        @WebParam(name = "StudyIdentifier", targetNamespace = "")
        com.velos.services.StudyIdentifier studyIdentifier
    ) throws com.velos.services.OperationException_Exception;
}
