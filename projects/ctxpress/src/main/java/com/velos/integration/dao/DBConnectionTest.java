package com.velos.integration.dao;

import java.sql.*;

public class DBConnectionTest {

	public static void main(String args[]) {
		try {
			Class.forName("org.postgresql.Driver");

			Connection con = DriverManager.getConnection(
					"jdbc:postgresql://172.16.2.177:5432/CTXpress", "postgres", "postgres");

			Statement stmt = con.createStatement();

			/*ResultSet rs = stmt.executeQuery("select * from vgdb_message");
			
			while (rs.next())
				System.out.println(rs.getInt(1) + "  " + rs.getString(2) + "  "
						+ rs.getString(3));*/
		
			
			int rows =  stmt.executeUpdate("INSERT INTO CTXpress_AUDIT_DETAILS VALUES(nextval('seq_CTXpress_AUDIT_DETAILS'),'BR','st','PTS','cal_Type','Cal_1','success','req_msg',current_timestamp)");
			
			System.out.println("Rows Inserted = "+rows);

			con.close();

		} catch (Exception e) {
			System.out.println(e);
		}

	}
}