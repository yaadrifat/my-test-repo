package com.velos.integration.espclient;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import com.velos.services.OperationException_Exception;
@WebService(targetNamespace = "http://velos.com/services/", name = "MilestoneSEI")
public interface VelosEspCreateMileStonesEndpoint {
	 @WebResult(name = "Response", targetNamespace = "")
	    @RequestWrapper(localName = "createMMilestones", targetNamespace = "http://velos.com/services/", className = "com.velos.milestones.services.CreateMMilestones")
	    @WebMethod
	    @ResponseWrapper(localName = "createMMilestonesResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.milestones.services.CreateMMilestonesResponse")
	    public com.velos.services.ResponseHolder createMMilestones(
	        @WebParam(name = "Milestones", targetNamespace = "")
	        com.velos.services.MilestoneList milestones
	    ) throws OperationException_Exception;

}
