package com.velos.integration.budgets;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.camel.CamelContext;
import org.apache.camel.ExchangePattern;
import org.apache.camel.ProducerTemplate;
import org.apache.cxf.message.MessageContentsList;
import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.velos.integration.dao.MessageDAO;
import com.velos.integration.espclient.VelosEspClientCamel;
import com.velos.integration.mapping.EndpointKeys;
import com.velos.integration.mapping.ProtocolKeys;
import com.velos.integration.mapping.VelosKeys;
import com.velos.integration.mapping.VelosStudyCalBudgetMapper;
import com.velos.integration.notifications.EmailNotification;
import com.velos.integration.versions.VersionsImpl;
import com.velos.services.BudgetDetail;
import com.velos.services.CalendarNameIdentifier;
import com.velos.services.Code;
import com.velos.services.CompletedAction;
import com.velos.services.CrudAction;
import com.velos.services.Issue;
import com.velos.services.Issues;
import com.velos.services.OperationException_Exception;
import com.velos.services.ResponseHolder;
import com.velos.services.Results;
import com.velos.services.SimpleIdentifier;
import com.velos.services.StudyIdentifier;

public class StudyCalBudgetsImpl implements VelosEspCreateStudyCalBudgetEndPoint,VelosEspGetStudyCalBudgetEndPoint{
	private static Logger logger = Logger.getLogger(StudyCalBudgetsImpl.class);

	VelosEspClientCamel velosEspClientCamel;
	MessageDAO messageDao ;
	EmailNotification emailNotification;
	


	List<BudgetDetail> budgetDetailList = null;
	BudgetDetail budgetDetail = null;
	Properties prop =null;
	VelosStudyCalBudgetMapper velosStudyCalBudgetMapper;


	public VelosStudyCalBudgetMapper getVelosStudyCalBudgetMapper() {
		return velosStudyCalBudgetMapper;
	}
	public void setVelosStudyCalBudgetMapper(
			VelosStudyCalBudgetMapper velosStudyCalBudgetMapper) {
		this.velosStudyCalBudgetMapper = velosStudyCalBudgetMapper;
	}
	public VelosEspClientCamel getVelosEspClientCamel() {
		return velosEspClientCamel;
	}
	public void setVelosEspClientCamel(VelosEspClientCamel velosEspClientCamel) {
		this.velosEspClientCamel = velosEspClientCamel;
	}

	public EmailNotification getEmailNotification() {
		return emailNotification;
	}
	public void setEmailNotification(EmailNotification emailNotification) {
		this.emailNotification = emailNotification;
	}
	public MessageDAO getMessageDao() {
		return messageDao;
	}
	public void setMessageDao(MessageDAO messageDao) {
		this.messageDao = messageDao;
	}

	protected CamelContext context;
	public CamelContext getContext() {
		return context;
	}
	public void setContext(CamelContext context) {
		this.context = context;
	}
	//private static Logger logger = Logger.getLogger(VelosEspClientCamel.class);

/*	public void configure(){
		if (this.context != null) { return; }
		//logger.info("****Getting camel context and creating Beans");
		@SuppressWarnings("resource")
		ClassPathXmlApplicationContext springContext =
		new ClassPathXmlApplicationContext("camel-config.xml");
		this.setContext((CamelContext)springContext.getBean("camel"));		
	}
*/

	public Map<VelosKeys, Object> getStudyCalBudgets(Map<VelosKeys, Object> requestMap ){

		String bdgtStat = null;
		String bdgtType = null;
		String brdBudgStat = null;
		String brdBudgType = null;
		Map<VelosKeys,Object> dataMap = null;
		String gettingCalBudgt = null;
		
		budgetDetailList = new ArrayList<BudgetDetail>();

		prop=(Properties) requestMap.get(ProtocolKeys.PropObj);

		String studyNumber = (String) requestMap.get(ProtocolKeys.StudyNumber);

		StudyIdentifier studyIdentifier=new StudyIdentifier();
		studyIdentifier.setStudyNumber(studyNumber);

		CalendarNameIdentifier calendarNameIdentifier; 

		try{

			bdgtStat = prop.getProperty("budget.budget_stat.subtype");
			bdgtType = prop.getProperty("budget.budget_stat.type");

		}catch(Exception e){
			logger.error("BroadCasting site Budget status subtypes are not mapped");
		}


		List<String> calNameList =	(List<String>) requestMap.get(ProtocolKeys.CalendarNameList);


		for(String calNamlist :calNameList){
			calendarNameIdentifier	= new CalendarNameIdentifier();
			calendarNameIdentifier.setCalendarName(calNamlist);
			calendarNameIdentifier.setCalendarAssocTo("P");
			logger.info("Getting Budget for Calendar = "+calNamlist);
			gettingCalBudgt = calNamlist;
			
			try {
				budgetDetail = velosEspClientCamel.getStudyCalBudget( studyIdentifier,calendarNameIdentifier);

				if(budgetDetail != null){

					brdBudgStat =budgetDetail.getBudgetInfo().getPKCODELSTSTATUS().getCode();
					brdBudgType = budgetDetail.getBudgetInfo().getPKCODELSTSTATUS().getType();
					logger.info("Budget Status = "+brdBudgStat);
					logger.info("Budget Status Type = "+brdBudgType);

					if(bdgtStat.equals(brdBudgStat) && brdBudgType.equals(bdgtType) ){
						logger.info("******* Adding Study Cal Budgets in List *******");
					
					logger.info("Budget Name===>"+budgetDetail.getBudgetIdentifier().getBudgetName());	
					
						budgetDetail.setStudyIdentifier(studyIdentifier);
						budgetDetail.setCalendarIdentifier(calendarNameIdentifier);
						budgetDetailList.add(budgetDetail);
						logger.info("****************************************************************");
					logger.info("BudgetList is Size ==>"+budgetDetailList.size());	
					}


				}else if(budgetDetail == null){
					logger.info("***Budget Object is null value***");
				}


			} catch (OperationException_Exception e){
				//e.printStackTrace();
				logger.error("**ERROR OCCURED GETTING Budgets**");
				dataMap = new HashMap<VelosKeys, Object>();
				List<Issue> issueList = e.getFaultInfo().getIssues().getIssue();
				List<String> errorList = new ArrayList<String>();
				for(Issue issue : issueList){
					logger.error("Issue Type = "+issue.getType());
					logger.error("Message = "+issue.getMessage());
					dataMap.put(ProtocolKeys.FaultString, issue.getMessage());
					//errorList.add("Issue Type = "+issue.getType()+" Message = "+issue.getMessage());
					//logger.info("Got operaton exception with issue: "+issue.getMessage());
					errorList.add(issue.getMessage());
				}
				dataMap.put(ProtocolKeys.ErrorList, errorList);
				if (issueList == null) { return null; }
				velosEspClientCamel.saveDB(requestMap,"","Budget",gettingCalBudgt,"Getting Calendar Budget Failed",errorList);
				//return dataMap;
			} catch (Exception e) {
				//e.printStackTrace();
				logger.error("Error in Get StudyCalBudget :",e);
				//return dataMap;
			}
		}

		requestMap.put(ProtocolKeys.CreateBudgetDetailsList,budgetDetailList);


		return requestMap;
	}

	
	
	
	public Map<VelosKeys, Object>	createStudyCalBudgets (Map<VelosKeys, Object> requestMap ){

		ResponseHolder responseHolder = null;
		String budgetIdentName = null;
		Map<VelosKeys, Object> dataMap = null;
	VelosStudyCalBudgetMapper velosStudyCalBudgetMapper = new VelosStudyCalBudgetMapper((String)requestMap.get(EndpointKeys.Endpoint));
		
	velosStudyCalBudgetMapper.mapCreateStudyCalBudget(requestMap);
		
		String orgName = ((String)requestMap.get(ProtocolKeys.OrganizationName)).trim();
		
		List<BudgetDetail> budgetDetailList = (List<BudgetDetail>) requestMap.get(ProtocolKeys.FinalCreateBudgetDetailList);

		
		for (BudgetDetail bdgtDetail :budgetDetailList){
			try{
				
			budgetIdentName = bdgtDetail.getBudgetIdentifier().getBudgetName();
			logger.info("Budget Name = "+budgetIdentName);
		 responseHolder =	velosEspClientCamel.createStudyCalBudget(bdgtDetail,orgName);
			
		
		logger.info("After Calling Create Org Study Cal Budgets");
		Issues issues = responseHolder.getIssues();
		Results results = responseHolder.getResults();
		if(issues!=null && !"".equals(issues)){
			List<Issue> issueList = issues.getIssue();
			List<String> errorList = new ArrayList<String>();
			if(!issueList.isEmpty()){
				logger.info("Issues Found");
				logger.info("IssueList = "+issueList);
				for(Issue issue : issueList){
					logger.info("Issue = "+issue);
					logger.info("Issue Type = "+issue.getType());
					logger.error("Issue Message = "+issue.getMessage());
					errorList.add(issue.getMessage());
				}
				emailNotification.sendNotification(requestMap,budgetIdentName+" creation fails for study : ",orgName,"StudyCalBudget",budgetIdentName,"Failed",errorList);
				velosEspClientCamel.saveDB(requestMap,orgName,"StudyCalBudget",budgetIdentName,"Failed",errorList);
			}
		}
		if(results!=null){
			logger.info("***Study Calendar Budgets  Create Results***");
			List<CompletedAction> actionList = results.getResult();
			if(!actionList.isEmpty()){
				for(CompletedAction action : actionList){
					CrudAction crudAction = action.getAction();
					logger.info("Action = "+crudAction.value());
					requestMap.put(ProtocolKeys.STUDY_CAL_BUDGET_CREATED,crudAction.value());
					SimpleIdentifier si = action.getObjectId();
					logger.info("OID = "+si.getOID());
					logger.info("Pk = "+si.getPK());
					logger.info("Object name = "+action.getObjectName());
				}
				logger.info("----------------------------------------------");
				String bSite = (String)requestMap.get(ProtocolKeys.BroadCastingSite);
				String studyNum = (String)requestMap.get(ProtocolKeys.StudyNumber);
				messageDao.saveMessageDetails(studyNum,bSite,orgName,"StudyCalBudget",budgetIdentName,"Created", "StudyCalBudget Created");
			}
		}
	} catch (OperationException_Exception e) {
		//e.printStackTrace();
		logger.info("***ERROR OCCURED creating study Calendar Budget Versions****");
		dataMap = new HashMap<VelosKeys, Object>();
		List<Issue> issueList = e.getFaultInfo().getIssues().getIssue();
		List<String> errorList = new ArrayList<String>();
		for(Issue issue : issueList){
			logger.info(issue.getType());
			logger.info(issue.getMessage());
			dataMap.put(ProtocolKeys.FaultString, issue.getMessage());
			//errorList.add("Issue Type = "+issue.getType()+". Message = "+issue.getMessage());
			logger.info("Issue Type = "+issue.getType()+". Message = "+issue.getMessage());
			errorList.add(issue.getMessage());
			logger.info("Got operaton exception with issue: "+issue.getMessage());
		}
		dataMap.put(ProtocolKeys.ErrorList, errorList);
		logger.info("------------------------------------------------------");
		if (issueList == null) { return null; }
		logger.info("Create Study Calendar Budget Failed = "+budgetIdentName);
		logger.info("Study Calendar Budget Creation Failed for organization  "+orgName +"="+budgetIdentName);
		emailNotification.sendNotification(dataMap,budgetIdentName+" creation fails for study : ",orgName,"StudyCalBudget",budgetIdentName,"Failed",errorList);
		velosEspClientCamel.saveDB(requestMap,orgName,"StudyCalBudget",budgetIdentName,"Failed",errorList);

	}finally{
		
	}
		}
		return dataMap;
	}





	public void getBudgets() throws OperationException_Exception{
		StudyIdentifier studyIdentifier=new StudyIdentifier();
		CalendarNameIdentifier calendarNameIdentifier=new CalendarNameIdentifier();
		studyIdentifier.setStudyNumber("CTX_Test_3");
		calendarNameIdentifier.setCalendarName("abc_copy");
		calendarNameIdentifier.setCalendarAssocTo("P");
		BudgetDetail budDetail =	getStudyCalBudget( studyIdentifier,calendarNameIdentifier);
		System.out.println("Psingh::"+budDetail.getBudgetIdentifier().getBudgetName());


	}


	/*public static void main(String[] args) {

		StudyCalBudgetsImpl obj=new StudyCalBudgetsImpl();
		obj.configure();
		try {
			obj.getBudgets();
		} catch (OperationException_Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


*/


	public BudgetDetail getStudyCalBudget(StudyIdentifier studyIdentifier,
			CalendarNameIdentifier calendarNameIdentifier)
					throws OperationException_Exception {
		MessageContentsList inputList=new MessageContentsList();
		inputList.add(studyIdentifier);
		inputList.add(calendarNameIdentifier);
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList list = null;

		list = (MessageContentsList) producer.sendBody("espGetBudgetEndpoint", 
				ExchangePattern.InOut,inputList );

		return (BudgetDetail) list.get(0);
	}
	public ResponseHolder createStudyCalBudget(BudgetDetail budget)
			throws OperationException_Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
