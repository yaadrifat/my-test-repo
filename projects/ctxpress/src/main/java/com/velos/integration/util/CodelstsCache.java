package com.velos.integration.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.velos.integration.dao.MessageDAO;
import com.velos.integration.espclient.VelosEspClientCamel;
import com.velos.integration.mapping.ProtocolKeys;
import com.velos.integration.mapping.VelosKeys;
import com.velos.integration.notifications.EmailNotification;
import com.velos.services.Code;
import com.velos.services.Codes;

public class CodelstsCache {
	
	private static Logger logger = Logger.getLogger(CodelstsCache.class);

	private VelosEspClientCamel camelClient;

	private Map<String, Object> codelstCache = new HashMap<String, Object> ();
	
	private static EmailNotification emailNotification;

	private static MessageDAO messageDao;

	public EmailNotification getEmailNotification() {
		return emailNotification;
	}

	public void setEmailNotification(EmailNotification emailNotification) {
		this.emailNotification = emailNotification;
	}

	public MessageDAO getMessageDao() {
		return messageDao;
	}

	public void setMessageDao(MessageDAO messageDao) {
		this.messageDao = messageDao;
	}

	public CodelstsCache(VelosEspClientCamel camelClient) {
		this.camelClient=camelClient;
		getParticipatingCodelsts();
	}

	public Map<String, Object> getParticipatingCodelsts() {
		if (codelstCache.isEmpty()) {
			logger.info("|||||||||||||GETTING CODELSTS|||||||||||||||||");
			codelstCache = createCodelstCache();
		}
		return codelstCache;
	}
	
	public Map<String, Object> createCodelstCache(){
		
		Properties prop = new Properties();
		try {
			prop.load(this.getClass().getClassLoader()
					.getResourceAsStream("config.properties"));
		} catch (IOException e) {
			logger.error("Error Loading config.properties:", e);
		}

		ArrayList<String> siteList = new ArrayList<String>();
		for (Entry<Object, Object> e : prop.entrySet()) {
			if ("ParticipatingSite".equals(e.getValue().toString())) {
				siteList.add(e.getKey().toString());
			}
		}
		logger.info("ParticpatingSite List = " + siteList);

		ArrayList<String> successList = new ArrayList<String>();
		ArrayList<String> failedList = new ArrayList<String>();
		for (String orgName : siteList) {
			Map<String, Object> codeMap = cacheParticipatingCodelsts(orgName);
			if(codeMap!=null){
				successList.add(orgName);
			}else{
				failedList.add(orgName);
			}
		}
		if(!successList.isEmpty()){
			logger.info("The following organizations codelsts are cached : ");
			logger.info(successList);
		}
		if(!failedList.isEmpty()){
			logger.fatal("Failed to cache codelsts for the following organizations : ");
			logger.fatal(failedList);
		}

		logger.info("FinalOrgCodeMap = "+codelstCache);
		return codelstCache;
	}
	
	public Map<String, Object> cacheParticipatingCodelsts(String orgName){
		Map<String, Object> codeMap = camelClient.callGetCodelsts(orgName);
		if(codeMap!=null){
			codelstCache.put(orgName, codeMap);
		}else{
			logger.fatal("Failed to get codelsts for organization : "+orgName);
		}
		return codeMap;
	}
	
	public static Code getMappedCode(Map<VelosKeys, Object> requestMap,String mappingConstant,String codeType,String broadSubType,String broadDesc){
		
		/*logger.info("Codelst Type = "+codeType);
		logger.info("Codelst SubType = "+broadSubType);
		logger.info("Codelst Desc = "+broadDesc);*/
		String orgName = ((String)requestMap.get(ProtocolKeys.OrganizationName)).trim();
    	Properties prop = (Properties)requestMap.get(ProtocolKeys.PropObj);
    	Code mappedCode = null;
    	if(!isEmpty(broadSubType) && !isEmpty(broadDesc)){
			mappedCode = new Code();
			if (prop.containsKey(orgName+"_"+mappingConstant+"."+broadSubType) && !"".equals(prop.getProperty(orgName+"_"+mappingConstant+"."+broadSubType).trim())) {
				String mappedCodelstSubtyp = prop.getProperty(orgName+"_"+mappingConstant+"."+broadSubType).trim();
				mappedCode = checkParticipatingCodelst(requestMap,orgName,codeType,mappedCodelstSubtyp,broadDesc);
			}else{
				mappedCode = checkParticipatingCodelst(requestMap,orgName,codeType,broadSubType,broadDesc);
			}
		}else{
			String resMsg = null;
			if(isEmpty(broadSubType)){
				resMsg = "Broadcasting Site codelst subtype is null or empty";
			}else if(isEmpty(broadDesc)){
				resMsg = "Broadcasting Site codelst description is null or empty";
			}
			logger.info(resMsg);
			logger.info("Codelst Type = "+codeType);
			logger.info("Codelst SubType = "+broadSubType);
			logger.info("Codelst Desc = "+broadDesc);
			mappedCode = getUnknownMapping(requestMap,prop,orgName,codeType,broadSubType,broadDesc,resMsg,"1");
		}
		return mappedCode;
	}
	
	private static Code checkParticipatingCodelst(Map<VelosKeys, Object> requestMap,String orgName,String codeType,String broadSubType,String broadDesc){
		
		Map<String, Object> codeMap = (Map<String, Object>) requestMap.get(ProtocolKeys.ParticipatingCodelsts);
		Map<String, Object> orgCodeMap = (Map<String, Object>)codeMap.get(orgName);
		
		Properties prop = (Properties)requestMap.get(ProtocolKeys.PropObj);
		String finalSubType = null;
		Code mappedCode = null;
		
		Codes codes = (Codes) orgCodeMap.get(codeType);
		if(codes!=null){
			List<Code> list = codes.getCodes();
			for(Code code : list){
				/*logger.info("Code Type = "+code.getType());
				logger.info("Code SubType = "+code.getCode());
				logger.info("Code Desc = "+code.getDescription());
				System.out.println("----------------------------------");*/
				String partSubType = code.getCode();
				String partDesc = code.getDescription();
				if(!isEmpty(partSubType) && !isEmpty(partDesc)){
					if(partSubType.equals(broadSubType) && partDesc.equalsIgnoreCase(broadDesc)){
						finalSubType = partSubType;
						mappedCode = new Code();
						mappedCode.setCode(partSubType);
						mappedCode.setDescription(partDesc);
						mappedCode.setType(codeType);
					}
				}
			}
			if(mappedCode==null){
				String resMsg = "Broadcasting Site Codelst Doesn't match with Participating Site";
				logger.info(resMsg);
				logger.info("Codelst Type = "+codeType);
				logger.info("Codelst SubType = "+broadSubType);
				logger.info("Codelst Desc = "+broadDesc);
				mappedCode = getUnknownMapping(requestMap,prop,orgName,codeType,broadSubType,broadDesc,resMsg,"2");
			}
		}
		return mappedCode;
	}
	
	private static Code getUnknownMapping(Map<VelosKeys, Object> requestMap,Properties prop,String orgName,String codeType,String broadSubType,String broadDesc,String resMsg,String notfType){
		Code mappedCode = null;
		String studyNumber = (String)requestMap.get(ProtocolKeys.StudyNumber);
		String bSite = (String)requestMap.get(ProtocolKeys.BroadCastingSite);
		String componentName = "Codelst Type="+codeType+"\nCodelst SubType="+broadSubType+"\nCodelst Description="+broadDesc;
		//messageDao.saveMessageDetails(studyNumber,bSite,orgName, "CodelstMapping",componentName,"Mapped to Unknown",resMsg);
		//emailNotification.sendCodelstNotification(requestMap,resMsg,orgName,componentName,"Mapped to Unknown",notfType);
		if (prop.containsKey(orgName+"_UnknownSubType") && !"".equals(prop.getProperty(orgName+"_UnknownSubType").trim())) {
			String mappedCodelstSubtyp = prop.getProperty(orgName+"_UnknownSubType").trim();
			String mappedDesc = prop.getProperty(orgName+"_UnknownDesc").trim();
			mappedCode = new Code();
			mappedCode.setType(codeType);
			mappedCode.setCode(mappedCodelstSubtyp);
			mappedCode.setDescription(mappedDesc);
		}
		return mappedCode;
	}
	
	public static boolean isEmpty(String strParam) {
        if ((!(strParam == null)) && (!(strParam.trim()).equals(""))
                && ((strParam.trim()).length() > 0)) {
            return false;
        } else {
            return true;
        }
    }
	
}
