package com.velos.integration.versions;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.camel.CamelContext;
import org.apache.camel.CamelExecutionException;
import org.apache.camel.ExchangePattern;
import org.apache.camel.ProducerTemplate;
import org.apache.cxf.message.MessageContentsList;
import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.velos.integration.dao.MessageDAO;
import com.velos.integration.espclient.VelosEspClientCamel;
import com.velos.integration.mapping.EndpointKeys;
import com.velos.integration.mapping.ProtocolKeys;
import com.velos.integration.mapping.VelosKeys;
import com.velos.integration.mapping.VelosStudyVersionMapper;
import com.velos.integration.notifications.EmailNotification;
import com.velos.services.CompletedAction;
import com.velos.services.CrudAction;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.Issues;
import com.velos.services.OperationException_Exception;
import com.velos.services.ResponseHolder;
import com.velos.services.Results;
import com.velos.services.SimpleIdentifier;
import com.velos.services.Study;
import com.velos.services.StudyAppendix;
import com.velos.services.StudyIdentifier;
import com.velos.services.StudyStatusHistory;
import com.velos.services.StudyVersion;
import com.velos.services.Versions;

public class VersionsImpl  {

	private static Logger logger = Logger.getLogger(VersionsImpl.class);
	private EmailNotification emailNotification;
	private MessageDAO messageDao;
	private VelosEspClientCamel velosEspClientCamel;  
	protected CamelContext context;
	VelosStudyVersionMapper mapper = null;


	public VersionsImpl(){
		configureCamel();
	}
	public MessageDAO getMessageDao() {
		return messageDao;
	}
	public void setMessageDao(MessageDAO messageDao) {
		this.messageDao = messageDao;
	}

	public EmailNotification getEmailNotification() {
		return emailNotification;
	}
	public void setEmailNotification(EmailNotification emailNotification) {
		logger.info("/////Setting email obj");
		logger.info("Obj in constructor = "+emailNotification);
		this.emailNotification = emailNotification;
	}
	public VelosEspClientCamel getVelosEspClientCamel() {
		return velosEspClientCamel;
	}
	public void setVelosEspClientCamel(VelosEspClientCamel velosEspClientCamel) {
		this.velosEspClientCamel = velosEspClientCamel;
	}
	public CamelContext getContext() {
		return context;
	}

	public void setContext(CamelContext context) {
		this.context = context;
	}


	public void configureCamel() {
		if (this.context != null) { return; }
		//logger.info("****Getting camel context and creating Beans");
		@SuppressWarnings("resource")
		ClassPathXmlApplicationContext springContext =new ClassPathXmlApplicationContext("camel-config.xml");
		this.setContext((CamelContext)springContext.getBean("camel"));		
	}

	public Map<VelosKeys, Object> getStudyVersionsDef(Map<VelosKeys, Object> requestMap){
		logger.info("*****VersionsImpl getStudyVersionsDef ******");
		StudyIdentifier studyIdentifier = new StudyIdentifier();
		studyIdentifier.setStudyNumber((String)requestMap.get(ProtocolKeys.StudyNumber));
		Versions versions = new Versions();
		Map<VelosKeys,Object> dataMap = null;
		
		try {
			versions =getStudyVersions(studyIdentifier);

		} catch (OperationException_Exception e) {
			//e.printStackTrace();
			logger.error("**ERROR OCCURED GETTING Study VERSIONS**");
			dataMap = new HashMap<VelosKeys, Object>();
			List<Issue> issueList = e.getFaultInfo().getIssues().getIssue();
			List<String> errorList = new ArrayList<String>();
			for(Issue issue : issueList){
				logger.error("Issue Type = "+issue.getType());
				logger.error("Message = "+issue.getMessage());
				dataMap.put(ProtocolKeys.FaultString, issue.getMessage());
				errorList.add("Issue Type = "+issue.getType()+" Message = "+issue.getMessage());
				//logger.info("Got operaton exception with issue: "+issue.getMessage());
			}
			dataMap.put(ProtocolKeys.ErrorList, errorList);
			if (issueList == null) { return null; }
			return dataMap;
		} catch (Exception e) {
			//e.printStackTrace();
			logger.error("Error :",e);
			return dataMap;
		}
		
	
		
		if (dataMap == null && versions != null){
			logger.info("****Executing StudyVersion mapper****");
			VelosStudyVersionMapper mapper = new VelosStudyVersionMapper((String)requestMap.get(EndpointKeys.Endpoint));
			return mapper.mapGetStudyVersOrgList(versions,requestMap);
		} 

		return dataMap;

	}


	//Create Versions for Participants
	public Map<VelosKeys, Object> createVersionsDef(Map<VelosKeys, Object> requestMap){


		mapper = new VelosStudyVersionMapper((String)requestMap.get(EndpointKeys.Endpoint));
		mapper.mapCreateStudyVersOrgList(requestMap);

		Map<VelosKeys, Object> dataMap = null;
		String orgName = ((String)requestMap.get(ProtocolKeys.OrganizationName)).trim();
		try{

			StudyIdentifier studyIdentifier = new StudyIdentifier();
			String partSiteStudyNumber = (String)requestMap.get(ProtocolKeys.ParticipatingSite_StudyNumber);
			logger.info("Adding Participating Study Number to Versions="+partSiteStudyNumber);
			studyIdentifier.setStudyNumber(partSiteStudyNumber);

			String versNumber = null;

			List<StudyVersion> studyVersionList = (List<StudyVersion>) requestMap.get(ProtocolKeys.CreateStudyVersfinalList);
			List<StudyVersion> creatStudyVersList = null;
			for(StudyVersion studyVersion : studyVersionList){
				if(studyVersion != null){
					creatStudyVersList = new ArrayList<StudyVersion>();
					creatStudyVersList.add(studyVersion);
					Versions  versions = new Versions();
					versions.setStudyIdentifier(studyIdentifier);
					versions.getVersion().addAll(creatStudyVersList);
					versNumber = studyVersion.getStudyVerIdentifier().getSTUDYVERNUMBER();

					try {
						ResponseHolder responseHolder= createVersions(versions,orgName);
						logger.info("After Calling Create Org Study Versions");
						Issues issues = responseHolder.getIssues();
						Results results = responseHolder.getResults();
						if(issues!=null && !"".equals(issues)){
							List<Issue> issueList = issues.getIssue();
							if(!issueList.isEmpty()){
								logger.info("Issues Found");
								logger.info("IssueList = "+issueList);
								for(Issue issue : issueList){
									logger.info("Issue = "+issue);
									logger.info("Issue Type = "+issue.getType());
									logger.info("Issue Message = "+issue.getMessage());
								}
							}
						}
						if(results!=null){
							logger.info("Study Versions Create Results");
							List<CompletedAction> actionList = results.getResult();
							for(CompletedAction action : actionList){
								CrudAction crudAction = action.getAction();
								logger.info("Action = "+crudAction.value());
								requestMap.put(ProtocolKeys.VERSIONS_CREATED,crudAction.value());
								SimpleIdentifier si = action.getObjectId();
								logger.info("OID = "+si.getOID());
								logger.info("Pk = "+si.getPK());
								logger.info("Object name = "+action.getObjectName());
							}

							String bSite = (String)requestMap.get(ProtocolKeys.BroadCastingSite);
							String studyNum = (String)requestMap.get(ProtocolKeys.StudyNumber);
							messageDao.saveMessageDetails(studyNum,bSite,orgName,"Versions",versNumber,"Created", "Versions Created");
						}
					} catch (OperationException_Exception e) {
						//e.printStackTrace();
						logger.info("ERROR OCCURED creating study Versions");
						dataMap = new HashMap<VelosKeys, Object>();
						List<Issue> issueList = e.getFaultInfo().getIssues().getIssue();
						List<String> errorList = new ArrayList<String>();
						for(Issue issue : issueList){
							logger.info(issue.getType());
							logger.info(issue.getMessage());
							dataMap.put(ProtocolKeys.FaultString, issue.getMessage());
							//errorList.add("Issue Type = "+issue.getType()+". Message = "+issue.getMessage());
							logger.info("Issue Type = "+issue.getType()+". Message = "+issue.getMessage());
							errorList.add(issue.getMessage());
							logger.info("Got operaton exception with issue: "+issue.getMessage());
						}
						dataMap.put(ProtocolKeys.ErrorList, errorList);
						if (issueList == null) { return null; }
						logger.info("Create Version Failed = "+versNumber);
						logger.info("Versions Creation Failed for organization  "+orgName +"="+versNumber);
						emailNotification.sendNotification(requestMap,versNumber+" creation fails for study : ",orgName,"Versions",versNumber,"Failed",errorList);
						velosEspClientCamel.saveDB(requestMap,orgName,"Versions",versNumber,"Failed",errorList);

					}finally{

					}
				}
			}
			logger.info("All versions sent to participant site :: "+ orgName);
		}catch(Exception e){
			logger.info("Create version Exception for ===>"+orgName+"",e);
		}
		return dataMap;
	}



	public ResponseHolder createVersions(Versions versions,String orgName)
			throws OperationException_Exception {
		logger.info("****Versions Impl Create studyVersions "+ orgName +" from Endpoint ****");
		
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList list = null;
		try{
			list = (MessageContentsList) producer.sendBody("cxf:bean:espCreate"+orgName+"StudyVersionsEndpoint", 
					ExchangePattern.InOut, versions);        
		}catch(CamelExecutionException e) {
			//	logger.info(e.getCause());
			//	logger.info(e.getMessage());
			if (e.getCause() instanceof OperationException_Exception) {
				//		logger.info("throwing Exception");
				throw(OperationException_Exception)e.getCause();
			}
			e.printStackTrace();
			return null;
		}
			logger.info("Created  Versions on ::  "+orgName);
		return (ResponseHolder) list.get(0);


	}


	public Versions getStudyVersions(StudyIdentifier studyIdentifier)
			throws OperationException_Exception {
		logger.info("****Versions Impl Get studyVersions from ctxpress Endpoint ****");
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList list = null;
		MessageContentsList inpList = new MessageContentsList();
		inpList.add(studyIdentifier);
		inpList.add(true);
		try{
			list = (MessageContentsList) producer.sendBody("cxf:bean:espGetStudyVersionEndpoint", 
					ExchangePattern.InOut,inpList);
		}catch(CamelExecutionException e) {
			//	logger.info(e.getCause());
			//	logger.info(e.getMessage());
			if (e.getCause() instanceof OperationException_Exception) {
				//		logger.info("throwing Exception");
				throw(OperationException_Exception)e.getCause();
			}
			e.printStackTrace();
			return null;
		}
		logger.info("***Returned Versions from Ctxpress***");
		return (Versions) list.get(0);
	}
	
	
	
	
	
	
	/*public static void main(String args[]){

	VersionsImpl vI = new VersionsImpl();
	vI.configureCamel();
	Map<VelosKeys, Object> requestMap = new HashMap<VelosKeys,Object>();

	requestMap =vI.getVersions("Amitabcdef");
		Versions version =(Versions) requestMap.get(ProtocolKeys.CalendarName);
	System.out.println("Version StudyIdentifier =====>"+version.getStudyIdentifier());
}*/



	/*public Map<VelosKeys, Object> getVersions(String studyNumber){

		StudyIdentifier studyIdentifier = new StudyIdentifier();
		//studyIdentifier.setStudyNumber((String)requestMap.get(ProtocolKeys.StudyNumber));

		studyIdentifier.setStudyNumber(studyNumber);

		Versions versions = null;
		Map<VelosKeys, Object> dataMap = new HashMap<VelosKeys, Object>();
		try {
			versions = getStudyVersions(studyIdentifier);

			//System.out.println("Versions StudyIdentifier ====>"+versions.getStudyIdentifier().getStudyNumber());
			System.out.println("Versions ====>"+versions.getVersion());
			List<StudyVersion> studyVersList = versions.getVersion();


			dataMap.put(ProtocolKeys.StudyNumber,versions.getStudyIdentifier().getStudyNumber());


			dataMap.put(ProtocolKeys.VERSION,studyVersList);

			//List<StudyStatusHistory> stdStatHist = versions.getStdStatusHistory();


			//	dataMap.put(ProtocolKeys.STUDYSTATUSHISTORY,stdStatHist);


			XMLGregorianCalendar stdVerDate = null;

				for(StudyStatusHistory ssh : stdStatHist){

				stdVerDate = ssh.getSTATUSDATE();
			}

			//Date stdVersDate = toDate(stdVerDate);

			//dataMap.put(ProtocolKeys.VERSTATUSDATE,  stdVersDate);


			int leng = studyVersList.size();

			System.out.println("Length of the Versions ====>"+ leng);

			for(StudyVersion sv :studyVersList){

				sv.getStudyVerIdentifier();

				//System.out.println("StudyVersionNumber ========> "+sv.getSTUDYVERNUMBER());

				dataMap =	  getVersionDef(sv,dataMap);
				System.out.println("*****************************************************");
				break;
				
				  System.out.println("IPAddress ==========>"+sv.getIPADD());

				  System.out.println("StudyversionStatus ========> "+sv.getSTUDYVERSTATUS());
				  System.out.println("StudyVersionNumber ========> "+sv.getSTUDYVERNUMBER());
				  System.out.println("StudyVersionNotes =========>"+sv.getSTUDYVERNOTES());
				 
			}

		//	createVersionsDefs(dataMap);
			//dataMap.put(ProtocolKeys.CalendarName, versions);

		} catch (OperationException_Exception e) {

			//e.printStackTrace();
			//logger.info("ERROR OCCURED Getting Email ID for Documented By");
			dataMap = new HashMap<VelosKeys, Object>();
			List<Issue> issueList = e.getFaultInfo().getIssues().getIssue();
			List<String> errorList = new ArrayList<String>();
			for(Issue issue : issueList){
				IssueTypes issueTypes = issue.getType();
				String value = issueTypes.value();
				//	logger.info("Issue value = "+value);
				//	logger.info("Issue Type = "+issue.getType());
				//	logger.info("Message = "+issue.getMessage());
				dataMap.put(ProtocolKeys.FaultString, issue.getMessage());

				errorList.add(issue.getMessage());
				//	logger.info("Got operaton exception with issue: "+issue.getMessage());
			}

			dataMap.put(ProtocolKeys.ErrorList, errorList);

			if (issueList == null) { 
				return null;
			}
			return dataMap;

		}

		return dataMap;
	}*/


	/*public Map<VelosKeys, Object> getVersionDef(StudyVersion  studyVersion,Map<VelosKeys, Object> dataMap){

		//dataMap.put(ProtocolKeys.CalendarName, value)
		String  studyVerCategoryCode = studyVersion.getSTUDYVERCATEGORY().getCode();

		dataMap.put(ProtocolKeys.STUDYVERCATEGORY,studyVerCategoryCode);

		//dataMap.put(ProtocolKeys.STUDYVERNUMBER,studyVersion.getSTUDYVERNUMBER());

		dataMap.put(ProtocolKeys.STUDYVERSTATUS,studyVersion.getSTUDYVERSTATUS());

		//dataMap.put(ProtocolKeys.STUDYVERTYPECODE, studyVersion.getSTUDYVERTYPE().getCode());

		dataMap.put(ProtocolKeys.STUDYAPPENDIX,studyVersion.getStudyAppendix());
		System.out.println("StudyVersionCategory =====> "+studyVersion.getSTUDYVERCATEGORY().getCode());
		System.out.println("StudyVersion Category description ====> "+studyVersion.getSTUDYVERCATEGORY().getDescription());
		System.out.println("StudyVersionNumber ====> "+studyVersion.getStudyVerIdentifier().getSTUDYVERNUMBER());
		System.out.println("StudyVersion Status ======> "+studyVersion.getSTUDYVERSTATUS());
		//	System.out.println("StudyVersion Type ======> "+studyVersion.getSTUDYVERTYPE().getCode());
		List<StudyAppendix> studyApendxList = studyVersion.getStudyAppendix();

		for(StudyAppendix studyAppendix:studyApendxList){
			System.out.println(studyAppendix.getSTUDYAPNDXDESC());
			System.out.println(studyAppendix.getSTUDYAPNDXFILE());
			System.out.println(studyAppendix.getSTUDYAPNDXFILEOBJ());
			System.out.println(studyAppendix.getSTUDYAPNDXFILESIZE());
			System.out.println(studyAppendix.getSTUDYAPNDXPUBFLAG());
			System.out.println(studyAppendix.getSTUDYAPNDXTYPE());
			System.out.println(studyAppendix.getSTUDYAPNDXURI());
		}

		return dataMap;
	}*/
	
	/*
	public void createVersionsDefs(Map<VelosKeys, Object> dataMap){
		String studyNumber = (String) dataMap.get(ProtocolKeys.StudyNumber);
		Versions versions = new Versions();
		StudyIdentifier studyIdentifier = new StudyIdentifier();
		dataMap.get(ProtocolKeys.STUDYSTATUSHISTORY);

		studyIdentifier.setStudyNumber(studyNumber);

		versions.setStudyIdentifier(studyIdentifier);
		List<StudyStatusHistory> sshlist = new ArrayList<StudyStatusHistory>();

		sshlist= (List<StudyStatusHistory>) dataMap.get(ProtocolKeys.STUDYSTATUSHISTORY);
		StudyVersion stdVers = new StudyVersion();
		List<StudyVersion> studyVersList = (List<StudyVersion>) dataMap.get(ProtocolKeys.VERSION);
		versions.setVersion(studyVersList);
		String orgName ="UTHSCSA";

		try {
			createVersions(versions,orgName);

			System.out.println("************************Version Creation Completed**********************");
		} catch (OperationException_Exception e) {
			System.out.println("******************Create Version Exception************");
			e.printStackTrace();
		}
	}*/


/*	public Versions getStudyVers(StudyIdentifier studyIdentifier) throws OperationException_Exception{

		try {
			configureCamel();
			return getStudyVersions(studyIdentifier);
		} catch (OperationException_Exception e) {
			throw e;
		}
	}*/
	

}
