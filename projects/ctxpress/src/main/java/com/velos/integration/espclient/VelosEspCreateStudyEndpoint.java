package com.velos.integration.espclient;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import com.velos.services.OperationException_Exception;
import com.velos.services.OperationRolledBackException_Exception;

@WebService(targetNamespace="http://velos.com/services/", name="StudySEI")
public interface VelosEspCreateStudyEndpoint {

	@WebResult(name = "Response", targetNamespace = "")
    @RequestWrapper(localName = "createStudy", targetNamespace = "http://velos.com/services/", className = "com.velos.services.CreateStudy")
    @WebMethod
    @ResponseWrapper(localName = "createStudyResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.CreateStudyResponse")
    public com.velos.services.ResponseHolder createStudy(
        @WebParam(name = "Study", targetNamespace = "")
        com.velos.services.Study study,
        @WebParam(name = "createNonSystemUsers", targetNamespace = "")
        boolean createNonSystemUsers
    ) throws OperationException_Exception, OperationRolledBackException_Exception;

}
