package com.velos.integration.versions;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import com.velos.services.OperationException_Exception;

@WebService(targetNamespace = "http://velos.com/services/", name = "VersionSEI")
public interface VelosEspCreateStudyVersionsEndpoint {
	
	  @WebResult(name = "Response", targetNamespace = "")
	    @RequestWrapper(localName = "createVersions", targetNamespace = "http://velos.com/services/", className = "com.velos.services.CreateVersions")
	    @WebMethod
	    @ResponseWrapper(localName = "createVersionsResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.CreateVersionsResponse")
	    public com.velos.services.ResponseHolder createVersions(
	        @WebParam(name = "Versions", targetNamespace = "")
	        com.velos.services.Versions versions
	    ) throws OperationException_Exception;

}
