package com.velos.integration.mapping;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.ibm.icu.util.BuddhistCalendar;
import com.velos.integration.budgets.StudyCalBudgetsImpl;
import com.velos.integration.util.CodelstsCache;
import com.velos.services.BgtCalPojo;
import com.velos.services.BudgetCalList;
import com.velos.services.BudgetDetail;
import com.velos.services.BudgetLineItemPojo;
import com.velos.services.BudgetSectionPojo;
import com.velos.services.Code;
import com.velos.services.StudyIdentifier;
import com.velos.services.Versions;

public class VelosStudyCalBudgetMapper {
	
	String orgName = null;

	private static Logger logger = Logger.getLogger(VelosStudyCalBudgetMapper.class);


	private Map<VelosKeys, Object> dataMap = null;
	Properties prop = null;
	private String endpoint = null;


	public VelosStudyCalBudgetMapper(String endpoint) {
		this.endpoint = endpoint;
	}

	public Map<VelosKeys, Object> mapCreateStudyCalBudget(Map<VelosKeys, Object> dataMap) {
		if (EndpointKeys.CTXpress.toString().equals(endpoint)) {
			mapForCreateStudyCalBudget(dataMap);
		}
		return dataMap;
	}



	public Map<VelosKeys, Object> mapForCreateStudyCalBudget(Map<VelosKeys, Object> dataMap) {
		logger.info("**VelosStudyCalBudgetMapper for Creating Budget**");
		String bdgtStat = null;
		String bdgtType = null;

		String partbdgtStat = null;
		String partbdgtStatType = null;
		String partbdgtStatDesc = null;
		String partbdgtCurrency = null ;
		String partBdgtTemplate = null;
		String partCstBdtTempCategory = null;
		String partCstBdtTempCostType = null;
		String partCstBdtTempStatus = null;


		String brodCstBdgtCurrency = null;
		String brodCstBdgtTemplate = null;
		String brodCstBdtTempCategory = null;
		String brodCstBdtTempCostType = null;
		String brodCstBdtTempStatus = null ;
		String brodCsBdtStatuType = null;
		prop=(Properties) dataMap.get(ProtocolKeys.PropObj);



		Code code = null;

		List<BudgetDetail> mappedbudgetDetailList = new ArrayList<BudgetDetail>();

		List<BudgetDetail> budgetDetailList = (List<BudgetDetail>) dataMap.get(ProtocolKeys.CreateStudyCalBudgetList);

		orgName = ((String)dataMap.get(ProtocolKeys.OrganizationName)).trim();
		try{

			bdgtStat = prop.getProperty("budget.budget_stat.subtype").trim();
			bdgtType = prop.getProperty("budget.budget_stat.type").trim();

			partbdgtStat = prop.getProperty(orgName+"_budgetstat.Code");
			partbdgtStatType = prop.getProperty(orgName+"_budgetstat.Type");
			partbdgtStatDesc = prop.getProperty(orgName+"_budgetstat.Description");
		}catch(Exception e){
			logger.error("BroadCasting site Budget status subtypes are not mapped",e);
		}

		if(budgetDetailList != null && budgetDetailList.size() != 0){
			for(BudgetDetail budgtDetail : budgetDetailList){
				
				String partSiteStudyNumber = (String)dataMap.get(ProtocolKeys.ParticipatingSite_StudyNumber);
				logger.info("Adding Participating Study Number to Budgets="+partSiteStudyNumber);
				StudyIdentifier studyIdentifier = new StudyIdentifier();
				studyIdentifier.setStudyNumber(partSiteStudyNumber);
				
				budgtDetail.setStudyIdentifier(studyIdentifier);
				
				brodCstBdtTempStatus  =	budgtDetail.getBudgetInfo().getPKCODELSTSTATUS().getCode().trim();
				brodCsBdtStatuType = 	budgtDetail.getBudgetInfo().getPKCODELSTSTATUS().getType().trim();

				if(bdgtStat.equals(brodCstBdtTempStatus) &&
						bdgtType.equals(brodCsBdtStatuType)){

					//Participating Site Status
					code = new Code();
					code.setCode(partbdgtStat);
					code.setType(partbdgtStatType);
					code.setDescription(partbdgtStatDesc);
					budgtDetail.getBudgetInfo().setPKCODELSTSTATUS(code);


					//Budget template Currency
					Code budgetTempCurrency = budgtDetail.getBgtTemplate().getBudgetInfo().getBUDGETCURRENCY();
					if(budgetTempCurrency!=null){
						brodCstBdgtCurrency = budgtDetail.getBgtTemplate().getBudgetInfo().getBUDGETCURRENCY().getCode();
						String currencyType = budgetTempCurrency.getType();
						String currencyDesc = budgetTempCurrency.getDescription();
						Code mappedTempCurrency = CodelstsCache.getMappedCode(dataMap,"budgtcurrency",currencyType,brodCstBdgtCurrency,currencyDesc);
						budgtDetail.getBgtTemplate().getBudgetInfo().setBUDGETCURRENCY(mappedTempCurrency);
					}
					/*
					if(brodCstBdgtCurrency != null && !"".equals(brodCstBdgtCurrency)){
					try{
						if(prop.containsKey(orgName+"_budgtcurrency."+brodCstBdgtCurrency) && !"".equals(prop.getProperty(orgName+"_budgtcurrency."+brodCstBdgtCurrency).trim())){
						partbdgtCurrency = prop.getProperty(orgName+"_budgtcurrency."+brodCstBdgtCurrency).trim();
						}else{
							partbdgtCurrency = brodCstBdgtCurrency; 	
						}
					}catch(Exception e){
						logger.info("***Participating site Budget Currency Mappings are missing***",e);
					}
					code = new Code();
					code.setCode(partbdgtCurrency);
					budgtDetail.getBgtTemplate().getBudgetInfo().setBUDGETCURRENCY(code);
				}*/
					try{
						Code budgetTemplate = budgtDetail.getBgtTemplate().getBudgetInfo().getBUDGETTEMPLATE();
						if(budgetTemplate!=null){
							brodCstBdgtTemplate = budgtDetail.getBgtTemplate().getBudgetInfo().getBUDGETTEMPLATE().getCode();
							String templateType = budgetTemplate.getType();
							String templateDesc = budgetTemplate.getDescription();
							Code mappedTemplate = CodelstsCache.getMappedCode(dataMap,"budgettemplate",templateType,brodCstBdgtTemplate,templateDesc);
							budgtDetail.getBgtTemplate().getBudgetInfo().setBUDGETTEMPLATE(mappedTemplate);
						}
					}catch(Exception e){
						logger.info("**BroadCast BudgetTemplate is Null Value ***",e);
					}

					/*if(brodCstBdgtTemplate != null && !"".equals(brodCstBdgtTemplate)){
						try{
							if(prop.containsKey(orgName+"_budgettemplate."+brodCstBdgtTemplate)){
								partBdgtTemplate = prop.getProperty(orgName+"_budgettemplate."+brodCstBdgtTemplate).trim(); 
							}else if(brodCstBdgtTemplate  != null) {
								partBdgtTemplate = brodCstBdgtTemplate;
							}
						}catch(Exception e){
							logger.info("***Participating site Budget Template is missing***",e);
						}
						code = new Code();
						code.setCode(partBdgtTemplate);
						budgtDetail.getBgtTemplate().getBudgetInfo().setBUDGETTEMPLATE(code);

					}*/


					BudgetCalList budgetCalList =(BudgetCalList) budgtDetail.getBgtTemplate().getBudgetInfo().getBudgetCalendars();

					//needs to check null value
					List<BgtCalPojo> bdgtCalPojoList = budgetCalList.getBudgetCalendar();
					if(bdgtCalPojoList != null && bdgtCalPojoList.size() != 0){
						for (BgtCalPojo budgetCalPojo : bdgtCalPojoList){

							List<BudgetSectionPojo> budgetSectPojoList = budgetCalPojo.getBudgetSections().getBudgetSectionInfo();

							for(BudgetSectionPojo budgtsectPojo :budgetSectPojoList){
								if( budgtsectPojo.getLineItems() != null ){

									List<BudgetLineItemPojo> budgtLineItemPojoList= budgtsectPojo.getLineItems().getBudgetLineItemInfo();

									if(budgtLineItemPojoList != null && budgtLineItemPojoList.size() != 0){

										for(BudgetLineItemPojo budgtLineItemPojo :budgtLineItemPojoList){

											Code codelstCategory = budgtLineItemPojo.getPKCODELSTCATEGORY();
											if(codelstCategory!=null){
												brodCstBdtTempCategory = budgtLineItemPojo.getPKCODELSTCATEGORY().getCode();
												String categoryType = codelstCategory.getType();
												String categoryDesc = codelstCategory.getDescription();
												Code mappedCodelstCategory = CodelstsCache.getMappedCode(dataMap,"budgetcategory",categoryType,brodCstBdtTempCategory,categoryDesc);
												budgtLineItemPojo.setPKCODELSTCATEGORY(mappedCodelstCategory);
											}
											/*if(brodCstBdtTempCategory != null && !"".equals(brodCstBdtTempCategory)){
												try{
													if(prop.containsKey(orgName+"_budgetcategory."+brodCstBdtTempCategory) && !"".equals( prop.getProperty(orgName+"_budgetcategory."+brodCstBdtTempCategory).trim())){

														partCstBdtTempCategory = prop.getProperty(orgName+"_budgetcategory."+brodCstBdtTempCategory).trim();
													}else if(brodCstBdtTempCategory != null){
														partCstBdtTempCategory = brodCstBdtTempCategory;		 
													}

												}catch(Exception e){
													logger.info("**Partcipating BudgetTemplate Category SubTypes are mapped ***",e);
												}

												code = new Code();
												code.setCode(partCstBdtTempCategory);
												budgtLineItemPojo.setPKCODELSTCATEGORY(code);
											}*/

											Code costType = budgtLineItemPojo.getPKCODELSTCOSTTYPE();
											if(costType!=null){
												brodCstBdtTempCostType = budgtLineItemPojo.getPKCODELSTCOSTTYPE().getCode();
												String type = costType.getType();
												String costTypeDesc = costType.getDescription();
												Code mappedCostType = CodelstsCache.getMappedCode(dataMap,"budgetTempCost",type,brodCstBdtTempCostType,costTypeDesc);
												budgtLineItemPojo.setPKCODELSTCOSTTYPE(mappedCostType);
											}
											/*if(brodCstBdtTempCostType != null && !"".equals(brodCstBdtTempCostType)){
												try{
													if(prop.containsKey(orgName+"_budgetTempCost."+brodCstBdtTempCostType) && !"".equals(prop.getProperty(orgName+"_budgetTempCost."+brodCstBdtTempCostType).trim())){
														partCstBdtTempCostType = prop.getProperty(orgName+"_budgetTempCost."+brodCstBdtTempCostType).trim();
													} else {
														partCstBdtTempCostType = 	brodCstBdtTempCostType; 
													}
												}catch(Exception e){
													logger.info("**Partcipating BudgetTemplate Cost Type subTypes not mapped ***",e);
												}
												code = new Code();
												code.setCode(partCstBdtTempCostType);
												budgtLineItemPojo.setPKCODELSTCOSTTYPE(code);
											}*/

										}
									}
								}

							}


						}
					}
					//Budget Template status "Save as Template"

					Code 	brodCstBdtTempStatusCode = budgtDetail.getBgtTemplate().getBudgetInfo().getPKCODELSTSTATUS();
					if(brodCstBdtTempStatusCode!=null){
						brodCstBdtTempStatus =	budgtDetail.getBgtTemplate().getBudgetInfo().getPKCODELSTSTATUS().getCode();
						String brodCstBdtTempStatusType = brodCstBdtTempStatusCode.getType();
						String brodCstBdtTempStatusDesc = brodCstBdtTempStatusCode.getDescription();
						Code mappedBrodCstBdtTempStatusCode = CodelstsCache.getMappedCode(dataMap,"budgetTempStat",brodCstBdtTempStatusType,brodCstBdtTempStatus,brodCstBdtTempStatusDesc);
						budgtDetail.getBgtTemplate().getBudgetInfo().setPKCODELSTSTATUS(mappedBrodCstBdtTempStatusCode);
					}
					
					/*if(brodCstBdtTempStatus != null && !"".equals(brodCstBdtTempStatus)){
						try{
							if(prop.containsKey(orgName+"_budgetTempStat."+brodCstBdtTempStatus) && !"".equals(prop.getProperty(orgName+"_budgetTempStat."+brodCstBdtTempStatus).trim())){	
								partCstBdtTempStatus = prop.getProperty(orgName+"_budgetTempStat."+brodCstBdtTempStatus).trim();
							}else{
								partCstBdtTempStatus = brodCstBdtTempStatus;
							}
						}catch(Exception e){
							logger.info("**Partcipating BudgetTemplate Status Type subTypes not mapped ***");
						}
						code =new Code();
						code.setCode(partCstBdtTempStatus);
						budgtDetail.getBgtTemplate().getBudgetInfo().setPKCODELSTSTATUS(code);
					}*/


					//Budget Info

					// Budget Info Currency
					Code bgtInfoCurrency = budgtDetail.getBudgetInfo().getBUDGETCURRENCY();
					if(bgtInfoCurrency!=null){
						brodCstBdgtCurrency = budgtDetail.getBudgetInfo().getBUDGETCURRENCY().getCode();
						String bgtInfoCurrencyType = bgtInfoCurrency.getType();
						String bgtInfoCurrencyDesc  =bgtInfoCurrency.getDescription();
						Code mappedBgtInfoCurrency = CodelstsCache.getMappedCode(dataMap,"budgtcurrency",bgtInfoCurrencyType,brodCstBdgtCurrency,bgtInfoCurrencyDesc);
						budgtDetail.getBudgetInfo().setBUDGETCURRENCY(mappedBgtInfoCurrency);
					}
					/*if(brodCstBdgtCurrency != null && !"".equals(brodCstBdgtCurrency)){
						try{
							if(prop.containsKey(orgName+"_budgtcurrency."+brodCstBdgtCurrency) && !"".equals(prop.getProperty(orgName+"_budgtcurrency."+brodCstBdgtCurrency).trim())){
								partbdgtCurrency = prop.getProperty(orgName+"_budgtcurrency."+brodCstBdgtCurrency).trim();
							}else{
								partbdgtCurrency = 	brodCstBdgtCurrency;
							}
						}catch(Exception e){
							logger.info("***Participating site Budget Currency Mappings are missing***",e);
						}
						code = new Code();
						code.setCode(partbdgtCurrency);
						budgtDetail.getBudgetInfo().setBUDGETCURRENCY(code);
					}*/

					//BudgetCalendarsList
					budgetCalList =(BudgetCalList) budgtDetail.getBudgetInfo().getBudgetCalendars();

					//Budget Calendar


					List<BgtCalPojo> bdgtCalPojoLists = budgetCalList.getBudgetCalendar();

					for (BgtCalPojo budgetCalPojo : bdgtCalPojoLists){


						//Bugdet Sections List
						List<BudgetSectionPojo> budgetSectPojoList = budgetCalPojo.getBudgetSections().getBudgetSectionInfo();

						for(BudgetSectionPojo budgtsectPojo :budgetSectPojoList){
							if( budgtsectPojo.getLineItems() != null ){
								//Budget Section Line Item 
								List<BudgetLineItemPojo> budgtLineItemPojoList= budgtsectPojo.getLineItems().getBudgetLineItemInfo();

								for(BudgetLineItemPojo budgtLineItemPojo :budgtLineItemPojoList){

									Code category= budgtLineItemPojo.getPKCODELSTCATEGORY();
									if(category!=null){
										brodCstBdtTempCategory = budgtLineItemPojo.getPKCODELSTCATEGORY().getCode();
										String categoryType = category.getType();
										String categoryDesc = category.getDescription();
										Code mappedCategory = CodelstsCache.getMappedCode(dataMap,"budgetcategory",categoryType,brodCstBdtTempCategory,categoryDesc);
										budgtLineItemPojo.setPKCODELSTCATEGORY(mappedCategory);
									}
									/*if(brodCstBdtTempCategory != null && !"".equals(brodCstBdtTempCategory)){
										try{
											if(prop.containsKey(orgName+"_budgetcategory."+brodCstBdtTempCategory) && !"".equals(prop.getProperty(orgName+"_budgetcategory."+brodCstBdtTempCategory).trim())){
												partCstBdtTempCategory = prop.getProperty(orgName+"_budgetcategory."+brodCstBdtTempCategory).trim();
											}else {
												partCstBdtTempCategory = brodCstBdtTempCategory; 		 
											}
										}catch(Exception e){
											logger.info("**Partcipating Budget Category Type subTypes not mapped ***",e);
										}
										code = new Code();
										code.setCode(partCstBdtTempCategory);
										budgtLineItemPojo.setPKCODELSTCATEGORY(code);
									}*/
									Code bdtTempCost = budgtLineItemPojo.getPKCODELSTCOSTTYPE();
									if(bdtTempCost!=null){
										brodCstBdtTempCostType = budgtLineItemPojo.getPKCODELSTCOSTTYPE().getCode();
										String bdtTempCostType = bdtTempCost.getType();
										String bdtTempCostDesc = bdtTempCost.getDescription();
										Code mappedBdtTempCost = CodelstsCache.getMappedCode(dataMap,"budgetTempCost",bdtTempCostType,brodCstBdtTempCostType,bdtTempCostDesc);
										budgtLineItemPojo.setPKCODELSTCOSTTYPE(mappedBdtTempCost);
									}
									/*if(brodCstBdtTempCostType != null && !"".equals(brodCstBdtTempCostType)){
										try{
											if(prop.containsKey(orgName+"_budgetTempCost."+brodCstBdtTempCostType) && !"".equals(prop.getProperty(orgName+"_budgetTempCost."+brodCstBdtTempCostType).trim())){
												partCstBdtTempCostType = prop.getProperty(orgName+"_budgetTempCost."+brodCstBdtTempCostType).trim();
											}else{
												partCstBdtTempCostType = brodCstBdtTempCostType; 
											}
										}catch(Exception e){
											logger.info("**Partcipating BudgetTemplate Cost Type subTypes not mapped ***",e);
										}
										code = new Code();
										code.setCode(partCstBdtTempCostType);
										budgtLineItemPojo.setPKCODELSTCOSTTYPE(code);
									}*/
								}
							}
						}
					}
				}else{
					logger.info("*** BroadCast Site Budget Status codelst type/Subtype miss matching ***");
				}
				mappedbudgetDetailList.add(budgtDetail);
			}
		}else{
			logger.info("Budget Data List Size is Zero");
		}
		dataMap.put(ProtocolKeys.FinalCreateBudgetDetailList, mappedbudgetDetailList);


		return dataMap;
	}
}
