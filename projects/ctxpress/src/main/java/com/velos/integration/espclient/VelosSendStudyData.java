package com.velos.integration.espclient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.velos.integration.dao.MessageDAO;
import com.velos.integration.mapping.EndpointKeys;
import com.velos.integration.mapping.ProtocolKeys;
import com.velos.integration.mapping.VelosKeys;
import com.velos.integration.notifications.EmailNotification;
import com.velos.integration.util.CodelstsCache;
import com.velos.integration.util.PropertiesUtil;
import com.velos.services.BudgetDetail;
import com.velos.services.MilestoneVDAPojo;
import com.velos.services.Code;
import com.velos.services.Study;
import com.velos.services.StudyCalendar;
import com.velos.services.StudyVersion;

public class VelosSendStudyData {

	public static Logger logger = Logger.getLogger(VelosSendStudyData.class);

	public EmailNotification emailNotification;

	private VelosEspClientCamel camelClient;

	private MessageDAO messageDao;

	private PropertiesUtil propertiesUtil;

	public PropertiesUtil getPropertiesUtil() {
		return propertiesUtil;
	}

	public void setPropertiesUtil(PropertiesUtil propertiesUtil) {
		this.propertiesUtil = propertiesUtil;
	}

	public EmailNotification getEmailNotification() {
		return emailNotification;
	}

	public void setEmailNotification(EmailNotification emailNotification) {
		this.emailNotification = emailNotification;
	}


	public VelosEspClientCamel getCamelClient() {
		return camelClient;
	}

	public void setCamelClient(VelosEspClientCamel camelClient) {
		this.camelClient = camelClient;
	}

	public MessageDAO getMessageDao() {
		return messageDao;
	}

	public void setMessageDao(MessageDAO messageDao) {
		this.messageDao = messageDao;
	}

	private CodelstsCache codelstsCache;
	
	public CodelstsCache getCodelstsCache() {
		return codelstsCache;
	}

	public void setCodelstsCache(CodelstsCache codelstsCache) {
		this.codelstsCache = codelstsCache;
	}

	public void sendStudyPackage(String studyNumber, String studyStatus,String packageType) {

		Properties prop = propertiesUtil.getAllProperties();
		String broadCastingSite = "";
		if (prop.containsKey("BroadCastingSite") && !"".equals(prop.getProperty("BroadCastingSite").trim())) {
			broadCastingSite = prop.getProperty("BroadCastingSite").trim();
			logger.info("BroadCasting site = "+broadCastingSite);
		}
		
		Map<String, Object> codelstMap = codelstsCache.getParticipatingCodelsts();
		logger.info("Codelsts From Cache = "+codelstMap);
		
		if(!"".equals(broadCastingSite) && broadCastingSite!=null && !"null".equals(broadCastingSite)){
			String endpoint = EndpointKeys.CTXpress.toString();
			Map<VelosKeys, Object> requestMap = new HashMap<VelosKeys, Object>();
			requestMap.put(EndpointKeys.Endpoint, endpoint);
			requestMap.put(ProtocolKeys.StudyNumber, studyNumber);
			requestMap.put(ProtocolKeys.StudyStatus, studyStatus);
			requestMap.put(ProtocolKeys.BroadCastingSite, broadCastingSite);
			requestMap.put(ProtocolKeys.PropObj, prop);
			requestMap.put(ProtocolKeys.PackageType, packageType);
			requestMap.put(ProtocolKeys.ParticipatingCodelsts, codelstMap);

			Map<VelosKeys, Object> studyDataMap = camelClient.handleRequest(
					VelosEspMethods.StudyGetStudy, requestMap);

			if (studyDataMap.get(ProtocolKeys.FaultString) != null) {
				//throw Exception
				@SuppressWarnings("unchecked")
				List<String> errorList = (List<String>) studyDataMap.get(ProtocolKeys.ErrorList);
				logger.error("ErrorList : "+errorList);
				for(String error : errorList){
					logger.error("Error = "+error);
				}
			}

			if (studyDataMap.get(ProtocolKeys.FaultString) == null) {
				Study study = (Study)studyDataMap.get(ProtocolKeys.StudyObj);
				String docByFname = (String) studyDataMap.get(ProtocolKeys.DocumentedByFirstName);
				String docByLname = (String) studyDataMap.get(ProtocolKeys.DocumentedByLastName);
				String docByLoginName = (String) studyDataMap.get(ProtocolKeys.DocumentedByLoginName);
				logger.info("DocumentedBy FirstName = "+docByFname);
				logger.info("DocumentedBy LastName = "+docByLname);
				logger.info("DocumentedBy LoginName = "+docByLoginName);

				requestMap.put(ProtocolKeys.DocumentedByFirstName, docByFname);
				requestMap.put(ProtocolKeys.DocumentedByLastName,docByLname);
				requestMap.put(ProtocolKeys.DocumentedByLoginName,docByLoginName);

				Map<VelosKeys, Object> emailMap = camelClient.handleRequest(VelosEspMethods.StudyGetDocumentedBy, requestMap);
				logger.info("EmailMap = "+emailMap);
				String email = "";
				if(emailMap!=null){
					email = (String) emailMap.get(ProtocolKeys.EmailID);
					requestMap.put(ProtocolKeys.EmailID,email);
				}
				logger.info("Email = "+email);

				//Get List of organizations added to a study
				@SuppressWarnings("unchecked")
				List<String> organizationList = (List<String>) studyDataMap
				.get(ProtocolKeys.OrganizationList);
				logger.info("List = " + organizationList);
				List<String>  orgList = new ArrayList<String>();
				logger.info("Organizations other than BroadCasting site : "+broadCastingSite);

				for (String orgName : organizationList) {
					if(!orgName.equals(broadCastingSite) && prop.containsKey(orgName)){
						logger.info("OrgName = " + orgName);		
						orgList.add(orgName);
					}
				}

				logger.info("OrgList Size = "+orgList.size());
				logger.info("OrgList  = "+orgList);

				//If no organizations added other than 'CTXpress'
				if(orgList==null || orgList.isEmpty()){
					//Needs to send notification to sender
					logger.info("There are no organizations added to send the study Package or Amendments");
					logger.info("Email ID = "+email);
					logger.info("EMail Obj = "+emailNotification);
					String response = "There are no participating organizations added to study team to send package or Amendments";
					logger.info("Request Map = "+requestMap);
					sendNotification(requestMap,response,packageType);
					logger.info("Study Number = "+requestMap.get(ProtocolKeys.StudyNumber));
					String bSite = (String)requestMap.get(ProtocolKeys.BroadCastingSite);
					String studyNum = (String)requestMap.get(ProtocolKeys.StudyNumber);
					if(packageType.equals("StudyPackage")){
						messageDao.saveMessageDetails(studyNum,bSite,"All Participating Sites", packageType,"All","Failed", response);
					}else{
						messageDao.saveMessageDetails(studyNum,bSite,"All Participating Sites", packageType,"All","Failed", response);
					}
				}

				if(orgList!=null && !orgList.isEmpty()){

					requestMap.put(ProtocolKeys.StudyObj, study);
					if(packageType.equals("StudyPackage")){
						logger.info("---------------Get Calendar Starts----------------");
						//Get Calendars from BroadCasting Site
						getCalendars(requestMap);
						logger.info("---------------Get Calendar Ends----------------");

						logger.info("---------------Get Budget Starts----------------");
						getBudgets(requestMap);
						logger.info("---------------Get Budget Ends----------------");

						logger.info("---------------Get Milestones Starts----------------");
						getMilestones(requestMap);
						logger.info("---------------Get Milestones Ends----------------");					 

						logger.info("---------------Get Version Starts----------------");
						//GetVersions from BroadCasting Site
						getVersions(requestMap);
						logger.info("---------------Get Version Ends----------------");
					}else{
						logger.info("---------------Get Amendments Starts----------------");
						getVersionAmendments(requestMap);
						logger.info("---------------Get Amendments Ends----------------");
					}

					String  status = checkStudyComponentStatus(requestMap,packageType);

					if(status.equals("success")){
						for(String org : orgList){
							logger.info("Org Name = "+org);
							requestMap.put(ProtocolKeys.OrganizationName,org);
							logger.info("Request Map Data = "+requestMap);
							if(prop.containsKey(org)){
								if(packageType.equals("StudyPackage")){
									logger.info("------Sending Pkg to org = "+org);
									sendPackage(requestMap,packageType);
								}else{
									logger.info("******Sending Amendments to org = "+org);
									sendAmendmentDocuments(requestMap,packageType);
								}
							}else{
								logger.info("Package/Amendments sending not configured for organization : "+org);
							}
						}
					}else{
						//Send email notification if atleast one of all components are not added 
						logger.error("Component Status = "+status);
						//String email = (String)requestMap.get(ProtocolKeys.EmailID);
						logger.info("Email ID = "+email);
						sendNotification(requestMap,status,packageType);
						String bSite = (String)requestMap.get(ProtocolKeys.BroadCastingSite);
						String studyNum = (String)requestMap.get(ProtocolKeys.StudyNumber);
						if(packageType.equals("StudyPackage")){
							messageDao.saveMessageDetails(studyNum,bSite,"All Participating sites",packageType,"All","Failed", status);
						}else{
							messageDao.saveMessageDetails(studyNum,bSite,"All Participating sites",packageType,"All","Failed", status);
						}
					}
				}
			}
		}else if("".equals(broadCastingSite) || broadCastingSite==null || "null".equals(broadCastingSite)){
			logger.info("BroadCasting site is not configured.Configure BroadCastingSite.");
		}
	}

	private void sendPackage(Map<VelosKeys, Object> requestMap,String packageType) {
		searchStudy(requestMap,packageType);
	}

	private void getCalendars(Map<VelosKeys, Object> requestMap){
		logger.info("Executing Calendars");
		Map<VelosKeys, Object> studyCalListMap = camelClient.handleRequest(
				VelosEspMethods.StudyCalGetStudyCalendarList, requestMap);
		//check if studyCalListMap is null or not
		if (studyCalListMap.get(ProtocolKeys.FaultString) == null) {
			List<String> calNameList = (List<String>)studyCalListMap.get(ProtocolKeys.CalendarNameList);
			logger.info("Set calendar Name list");
			requestMap.put(ProtocolKeys.CalendarNameList,calNameList);
			logger.info("calNameList = "+calNameList);
			//logger.info("calNameListSize = "+calNameList.size());
			if(calNameList!=null && !calNameList.isEmpty()){
				//call to get the calendar data
				Map<VelosKeys, Object> studyCalObjListMap = camelClient.handleRequest(VelosEspMethods.StudyCalGetStudyCalendar, requestMap);
				List<StudyCalendar> studyCalObjList = (List<StudyCalendar>)studyCalObjListMap.get(ProtocolKeys.CalObjList);
				logger.info("studyCalObjList = "+studyCalObjList);
				logger.info("Set cal obj list");
				requestMap.put(ProtocolKeys.CalObjList,studyCalObjList);
			}
		}
		logger.info("Returned Calendars");
	}

	private String checkStudyComponentStatus(Map<VelosKeys, Object> requestMap,String packageType) {
		String status = "success";
		String failed="";
		Properties prop = (Properties)requestMap.get(ProtocolKeys.PropObj);
		if(packageType.equals("StudyPackage")){
		//Checking Versions Component.
		List<StudyVersion> studyVersList = (List<StudyVersion>) requestMap.get(ProtocolKeys.CreateStudyVersList);
		logger.info("Version List = "+studyVersList);
		if(studyVersList == null || studyVersList.isEmpty()){
			failed = failed+"There should be atleast 'one Version' with status '"+prop.getProperty("version.status.desc").trim()+"' to send the Package"+"\n";
		}
		
		//Checking Calendar Component.
		List<String> calNameList = (List<String>)requestMap.get(ProtocolKeys.CalendarNameList);
		logger.info("calNameList = "+calNameList);
		//logger.info("calNameListSize = "+calNameList.size());
		if(calNameList==null || calNameList.isEmpty()){
			failed = failed+"There should be atleast 'one calendar' with status '"+prop.getProperty("calendar.status.desc").trim()+"' to send the Package."+"\n";
		}

		//Checking Budget Component.
		List<BudgetDetail> budgetDetailList = (List<BudgetDetail>) requestMap.get(ProtocolKeys.CreateBudgetDetailsList);
		logger.info("Budget List = "+budgetDetailList);
		if(budgetDetailList == null || budgetDetailList.isEmpty()){
			failed = failed+"There should be atleast 'one Budget' with Status 'Send in Study Package' to send the Package."+"\n";
		}

		//Checking Milestone Component.
		List<MilestoneVDAPojo> mileList = (List<MilestoneVDAPojo>)requestMap.get(ProtocolKeys.MilestoneList);
		logger.info("Miletone List = "+mileList);
		if(mileList==null || mileList.isEmpty()){
			failed = failed+"There should be atleast 'one Milestone' with status '"+prop.getProperty("Milestone.status.desc").trim()+"' to send the Package."+"\n";
		}
		}else{
			//Checking Amendments Component.
			List<StudyVersion> studyVersList = (List<StudyVersion>) requestMap.get(ProtocolKeys.CreateStudyVersAmendmentList);
			logger.info("Amendments List = "+studyVersList);
			if(studyVersList == null || studyVersList.isEmpty()){
				failed = failed+"There should be atleast 'one Amendments' with status '"+prop.getProperty("Amendments.status.desc").trim()+"' to send Version Amendments"+"\n";
			}
		}
		logger.info("Component Status = "+failed);
		if(!"".equals(failed)){
			return failed;
		}
		logger.info("Status = "+status);
		return status;
	}


	//Search Study
	private void searchStudy(Map<VelosKeys, Object> requestMap,String packageType){
		Map<VelosKeys, Object> searchOrgMap = camelClient.handleRequest(VelosEspMethods.StudySearchOrgStudy, requestMap);
		logger.info("SearchOrgMap = "+searchOrgMap);
		if(searchOrgMap!=null && searchOrgMap.get(ProtocolKeys.StudyAlreadyExists) != null){
			logger.info("Error = "+searchOrgMap.get(ProtocolKeys.StudyAlreadyExists));
			//			logger.info("Email = "+requestMap.get(ProtocolKeys.EmailID));
			if(packageType.equals("Amendments")){
				String partSiteStudyNumber = (String)searchOrgMap.get(ProtocolKeys.ParticipatingSite_StudyNumber);
				requestMap.put(ProtocolKeys.ParticipatingSite_StudyNumber, partSiteStudyNumber);
				createVersionAmendments(requestMap);
			}
		}else if(searchOrgMap!=null && searchOrgMap.get(ProtocolKeys.STUDY_NOT_FOUND) != null){
			logger.info("Error = "+searchOrgMap.get(ProtocolKeys.STUDY_NOT_FOUND));
			if(packageType.equals("StudyPackage")){
				createStudy(requestMap);
			}
			else{
				logger.info("Study Not found.Cannot Send Amendments");
			}
		}
	}

	private void createStudy(Map<VelosKeys, Object> requestMap){

		Map<VelosKeys, Object> createOrgMap = camelClient.handleRequest(VelosEspMethods.StudyCreateOrgStudy, requestMap);

		logger.info("createOrgMap = "+createOrgMap);

		if(createOrgMap !=null &&  createOrgMap.get(ProtocolKeys.FaultString) == null ){		
			
			String partSiteStudyNumber = (String)createOrgMap.get(ProtocolKeys.ParticipatingSite_StudyNumber);
			logger.info("ParticipatingSite Study Number = "+partSiteStudyNumber);
			if(partSiteStudyNumber!=null && !"".equals(partSiteStudyNumber)){
				requestMap.put(ProtocolKeys.ParticipatingSite_StudyNumber, partSiteStudyNumber);
				logger.info("Added Participating Site Number to requestMap");
			}
			
			logger.info("****** Create Versions Starts After Create Study *******");
			//Create Versions
			if(createOrgMap.get(ProtocolKeys.FaultString) == null && createOrgMap.get(ProtocolKeys.STUDY_CREATED)!=null ){
				logger.info("Study Created  and sending Versions");
				createVersions(requestMap);
			}
			if (createOrgMap.get(ProtocolKeys.FaultString) != null) {
				//Send email notification with errors
				logger.error("Study Creation Failed");
				//				logger.info("Email = "+requestMap.get(ProtocolKeys.EmailID));

			}
			else if(createOrgMap.get(ProtocolKeys.FaultString) == null && createOrgMap.get(ProtocolKeys.STUDY_CREATED)!=null){

				logger.info("Study created and sending calendars");

				createCalendars(requestMap);
			}

			logger.info("**Create Study calendar Budget Starts***");
			if(createOrgMap.get(ProtocolKeys.FaultString) == null && requestMap.get(ProtocolKeys.FaultString) == null ){

				logger.info("**Study & calendar Created Sending  Budgets***");
				createBudget(requestMap);
			}else {
				logger.info("Calendar creation failed due to issues in Study or calendar creations ===>"+requestMap.get(ProtocolKeys.StudyNumber));
				logger.info(requestMap);
			}
			logger.info("***Create Study Calendar Budget Ends ***");		

			//Create Milestones
			if(createOrgMap.get(ProtocolKeys.FaultString) == null && requestMap.get(ProtocolKeys.CALENDER_CREATED) != null ){
				logger.info("Creating Milestones");
				createMilestones(requestMap);
			}else{
				logger.info(requestMap);
				logger.info("*** calendar Creation Creation failed ***");
			}
		}else{
			logger.info(createOrgMap);
			logger.info("Study creation failed due to other issues ===>"+requestMap.get(ProtocolKeys.StudyNumber));
		}
	}


	//Create Study Components
	private void createCalendars(Map<VelosKeys, Object> requestMap){
		camelClient.handleRequest(VelosEspMethods.StudyCalCreateStudyCalendar, requestMap);
	}


	private void createBudget(Map<VelosKeys, Object> requestMap){

		camelClient.handleRequest(VelosEspMethods.StudyCreateStudyCalBudgets, requestMap);

	}


	private void createMilestones(Map<VelosKeys, Object> requestMap){
		camelClient.handleRequest(VelosEspMethods.StudyCalCreateMilestones, requestMap);
	}


	private void createVersions(Map<VelosKeys, Object> requestMap){

		camelClient.handleRequest(VelosEspMethods.StudyVersCreateStudyVersions, requestMap);
	}

	private void sendNotification(Map<VelosKeys, Object> requestMap,String status,String packageType){
		logger.info("Calling Notification");
		List<String> errorList = new ArrayList<String>();
		errorList.add(status);
		if(packageType.equals("StudyPackage")){
			emailNotification.sendNotification(requestMap,"Study Package fails for Study Number : ","All Participating Sites",packageType,"All","Failed",errorList);
		}else{
			emailNotification.sendNotification(requestMap,"Study Amendments fails for Study Number : ","All Participating Sites",packageType,"All","Failed",errorList);
		}
	}

	public Map<VelosKeys, Object>  getVersions(Map<VelosKeys, Object> requestMap){

		logger.info("Executing Versions started for study =====>"+requestMap.get(ProtocolKeys.StudyNumber));

		Map<VelosKeys, Object> studyVersListMap = camelClient.handleRequest(VelosEspMethods.StudyVersGetStudyVersList,requestMap);

		//requestMap.put(ProtocolKeys.StudyVersList,studyVersListMap);

		logger.info("BroadCast StudyVersionList=====>" + studyVersListMap);

		if(studyVersListMap != null && !studyVersListMap.isEmpty()){

			requestMap.put(ProtocolKeys.CreateStudyVersList,studyVersListMap.get(ProtocolKeys.CreateStudyVersList));
		}
		return requestMap;
	}

	private void getMilestones(Map<VelosKeys, Object> requestMap){
		logger.info("Getting Milestones");
		Map<VelosKeys, Object> milestoneListMap = camelClient.handleRequest(
				VelosEspMethods.StudyCalGetMilestones, requestMap);
		if (milestoneListMap.get(ProtocolKeys.FaultString) == null) {
			List<MilestoneVDAPojo> milestoneList = (List<MilestoneVDAPojo>)milestoneListMap.get(ProtocolKeys.MilestoneList);
			logger.info("MilestoneList = "+milestoneList);
			if(milestoneList!=null && !milestoneList.isEmpty()){
				logger.info("Set Milestone list");
				requestMap.put(ProtocolKeys.MilestoneList,milestoneList);
			}
		}
	}


	public Map<VelosKeys, Object>  getBudgets(Map<VelosKeys, Object> requestMap){
		logger.info("Getting Budgets started for Study Number ===> "+requestMap.get(ProtocolKeys.StudyNumber));

		try{
			Map<VelosKeys, Object> studyCalBudgetsListMap = camelClient.handleRequest(VelosEspMethods.StudyGetStudyCalBudgets,requestMap);


			if(studyCalBudgetsListMap != null && !studyCalBudgetsListMap.isEmpty()){
				logger.info("**Adding create Budget List into Map ****");
				List<BudgetDetail> budgetDetailList = (List<BudgetDetail>) studyCalBudgetsListMap.get(ProtocolKeys.CreateBudgetDetailsList);


				logger.info("*** After Budget Get ***");
				if(budgetDetailList != null && !budgetDetailList.isEmpty()){
					requestMap.put(ProtocolKeys.CreateStudyCalBudgetList,budgetDetailList);
				}else{
					logger.info("*** BudgetDetail Object is Empty/null ****");
				}


				logger.info("*** Get Budget List Completed***");
			}else if(studyCalBudgetsListMap == null || studyCalBudgetsListMap.isEmpty()){
				logger.info("******Get Budget List is empty from BroadCasting Site*****");
			}


		}catch (Exception e)
		{
			logger.info("Exception in GetBudgets in Velos Send Study Data");
		}
		return requestMap;

	}
	
	

	public void sendAmendmentDocuments(Map<VelosKeys, Object> requestMap,String packageType) {
		searchStudy(requestMap,packageType);
	}

	public void  getVersionAmendments(Map<VelosKeys, Object> requestMap){
		logger.info("Executing Amendments started for study ="+requestMap.get(ProtocolKeys.StudyNumber));
		Map<VelosKeys, Object> studyVersAmndListMap = camelClient.handleRequest(VelosEspMethods.StudyVersGetStudyVersAmndList,requestMap);
		logger.info("BroadCast StudyVersionAmendmentsList =" + studyVersAmndListMap);
		if(studyVersAmndListMap != null && studyVersAmndListMap.get(ProtocolKeys.ErrorList) == null){
			requestMap.put(ProtocolKeys.CreateStudyVersAmendmentList,studyVersAmndListMap.get(ProtocolKeys.CreateStudyVersAmendmentList));
		}
	}

	private void createVersionAmendments(Map<VelosKeys, Object> requestMap){
		camelClient.handleRequest(VelosEspMethods.StudyVersCreateStudyVersAmendment, requestMap);
	}


}