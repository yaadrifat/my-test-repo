package com.velos.integration.versions;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import com.velos.services.OperationException_Exception;

@WebService(targetNamespace = "http://velos.com/services/", name = "VersionSEI")
public interface VelosEspGetStudyVersionsEndpoint {
	 @WebResult(name = "Versions", targetNamespace = "")
	    @RequestWrapper(localName = "getStudyVersions", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetStudyVersions")
	    @WebMethod
	    @ResponseWrapper(localName = "getStudyVersionsResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetStudyVersionsResponse")
	 	public com.velos.services.Versions getStudyVersions(
		    @WebParam(name = "StudyIdentifier", targetNamespace = "")
		    com.velos.services.StudyIdentifier studyIdentifier,
		    @WebParam(name = "CurrentStatus", targetNamespace = "")
		    boolean currentStatus
		) throws OperationException_Exception;
}
