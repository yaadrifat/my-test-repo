package com.velos.integration.authentication;

import java.io.IOException;
import java.util.Properties;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;

import org.apache.log4j.Logger;
import org.apache.ws.security.WSPasswordCallback;

import com.velos.integration.util.PropertiesUtil;

public class UTMBAuthCallbackHandler implements CallbackHandler {

	private static Logger logger = Logger
			.getLogger(UTMBAuthCallbackHandler.class);

	private PropertiesUtil propertiesUtil;

	public PropertiesUtil getPropertiesUtil() {
		return propertiesUtil;
	}

	public void setPropertiesUtil(PropertiesUtil propertiesUtil) {
		this.propertiesUtil = propertiesUtil;
	}
	
	public void handle(Callback[] callbacks) {
		/*Properties prop = new Properties();
		try {
			prop.load(this.getClass().getClassLoader()
					.getResourceAsStream("config.properties"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		Properties prop = propertiesUtil.getAllProperties();
		logger.info("inside UTMB AuthCallbackHandler");
		WSPasswordCallback pc = (WSPasswordCallback) callbacks[0];
		if (pc.getIdentifier().equals(prop.getProperty("UTMB.userID"))) {
			pc.setPassword(prop.getProperty("UTMB.password"));
		}
	}
}
