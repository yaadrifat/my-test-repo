package com.velos.integration.authentication;

import java.io.IOException;
import java.util.Properties;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;

import org.apache.log4j.Logger;
import org.apache.ws.security.WSPasswordCallback;

import com.velos.integration.util.PropertiesUtil;

public class UTHSCHAuthCallbackHandler implements CallbackHandler {

	private static Logger logger = Logger
			.getLogger(UTHSCHAuthCallbackHandler.class);

	private PropertiesUtil propertiesUtil;

	public PropertiesUtil getPropertiesUtil() {
		return propertiesUtil;
	}

	public void setPropertiesUtil(PropertiesUtil propertiesUtil) {
		this.propertiesUtil = propertiesUtil;
	}

	public void handle(Callback[] callbacks) {

		Properties prop = propertiesUtil.getAllProperties();
		logger.info("inside UTHSCH AuthCallbackHandler");
/*		logger.info("user - " + prop.getProperty("UTHSCH.userID"));
		logger.info("password - " + prop.getProperty("UTHSCH.password"));*/
		WSPasswordCallback pc = (WSPasswordCallback) callbacks[0];
		if (pc.getIdentifier().equals(prop.getProperty("UTHSCH.userID"))) {
			pc.setPassword(prop.getProperty("UTHSCH.password"));
		}
	}
}
