package com.velos.integration.mapping;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.log4j.Logger;

import com.velos.integration.util.CodelstsCache;
import com.velos.integration.util.StringToXMLGregorianCalendar;
import com.velos.services.BgtCalNameIdentifier;
import com.velos.services.BgtSectionNameIdentifier;
import com.velos.services.CalendarNameIdentifier;
import com.velos.services.EventNameIdentfier;
import com.velos.services.LineItemNameIdentifier;
import com.velos.services.MilestoneVDAPojo;
import com.velos.services.Milestonee;
import com.velos.services.BudgetIdentifier;
import com.velos.services.Code;
import com.velos.services.StudyCalendar;
import com.velos.services.VisitNameIdentifier;


public class VelosMilestoneMapper {
	
	private static Logger logger = Logger.getLogger(VelosMilestoneMapper.class);
	private String endpoint = null;
	private Map<VelosKeys, Object> dataMap = null;
	Properties prop = null;
	
	public VelosMilestoneMapper(String endpoint) {
		this.endpoint = endpoint;
	}
	
	//Added By Rajasekhar
	public Map<VelosKeys, Object> getMappedOrgMilestones(List<MilestoneVDAPojo> milestoneObjList,Map<VelosKeys, Object> requestMap) throws Exception {
		if (EndpointKeys.CTXpress.toString().equals(endpoint)) {
			mapOrgMilestones(milestoneObjList,requestMap);
		}
		return dataMap;
	}
	
	private void mapOrgMilestones(List<MilestoneVDAPojo> milestoneObjList,Map<VelosKeys, Object> requestMap) throws Exception {
		
		dataMap = new HashMap<VelosKeys, Object>();
		
		prop = (Properties)requestMap.get(ProtocolKeys.PropObj);
		
		
		String orgName = ((String)requestMap.get(ProtocolKeys.OrganizationName)).trim();
    	logger.info("OrganizationName = "+orgName);
    	
    	logger.info("*******Getting Mapped Milestone Data for org = "+orgName);
    	List<Milestonee> mappedMilestoneList = null;
    	if(!milestoneObjList.isEmpty()){
    		mappedMilestoneList = new ArrayList<Milestonee>();
    		
    		for(MilestoneVDAPojo milestoneVDAPojo : milestoneObjList){
    			logger.info("-----------");
    			Milestonee mappedMileStone = new Milestonee();
    			
    			String milestoneCategory = milestoneVDAPojo.getMSRULCATEGORY();
    			logger.info("Milestone Type = "+milestoneCategory);
    			
    			String milestoneType = null;
    			
    			//Milestone Type
    			if(milestoneCategory.equals("Patient Milestone")){
    				milestoneType="PM";
    				mappedMileStone.setMILESTONETYPE("PM");;
    			}else if(milestoneCategory.equals("Visit Milestone")){
    				milestoneType="VM";
    				mappedMileStone.setMILESTONETYPE("VM");;
    			}else if(milestoneCategory.equals("Event Milestone")){
    				milestoneType="EM";
    				mappedMileStone.setMILESTONETYPE("EM");;
    			}else if(milestoneCategory.equals("Study Milestone")){
    				milestoneType="SM";
    				mappedMileStone.setMILESTONETYPE("SM");;
    			}else if(milestoneCategory.equals("Additional Milestone")){
    				milestoneType="AM";
    				mappedMileStone.setMILESTONETYPE("AM");;
    			}else{
    				logger.info("MileStone Type not found");
    			}
    			
    			//Budget Name
    			String budgetName = milestoneVDAPojo.getBUDGETNAME();
    			if(budgetName!=null && !"".equals(budgetName)){
    				BudgetIdentifier budgetIdentifier = new BudgetIdentifier();
    					budgetIdentifier.setBudgetName(budgetName);;
    				mappedMileStone.setBudgetIdentifier(budgetIdentifier);
    			}
    			
    			//Budget Section Name
    			String budgetSectionName = milestoneVDAPojo.getBUDGETSECTIONNAME();
    			if(budgetSectionName!=null && !"".equals(budgetSectionName)){
    				BgtSectionNameIdentifier budgetSectionIdentifier = new BgtSectionNameIdentifier();
    					budgetSectionIdentifier.setBgtSectionName(budgetSectionName);
    				mappedMileStone.setBgtSectionNameIdentifier(budgetSectionIdentifier);
    			}
    			
    			//Line Item
    			String lineItemName = milestoneVDAPojo.getLINEITEMNAME();
    			if(lineItemName!=null && !"".equals(lineItemName)){
    				LineItemNameIdentifier LineItemNameIdentifier = new LineItemNameIdentifier();
    				LineItemNameIdentifier.setLineItemName(lineItemName);
    				mappedMileStone.setLineItemNameIdentifier(LineItemNameIdentifier);
    			}
    			
    			//Calendar Name
    			String calendarName =  milestoneVDAPojo.getMSRULPROTCAL();
    			String calendarAssocTo = milestoneVDAPojo.getMSPROTCALASSOCTO();
    			if(calendarName!=null && !"".equals(calendarName) && calendarAssocTo!=null && !"".equals(calendarAssocTo)){
    				CalendarNameIdentifier calIdentifier = new  CalendarNameIdentifier();
    					calIdentifier.setCalendarAssocTo(calendarAssocTo);
    					calIdentifier.setCalendarName(calendarName);
    				mappedMileStone.setCalendarNameIdent(calIdentifier);
    			}
    			
    			//BudgetCalNameIdentifier
    			if(calendarName!=null && !"".equals(calendarName) && calendarAssocTo!=null && !"".equals(calendarAssocTo)){
    				BgtCalNameIdentifier bgtCalNameIdentifier = new  BgtCalNameIdentifier();
    					bgtCalNameIdentifier.setBgtCalName(calendarName);
    				mappedMileStone.setBgtCalNameIdentifier(bgtCalNameIdentifier);
    			}
    			
    			//Visit Name
    			String visitName = milestoneVDAPojo.getMILESTONEVISIT();
    			if(visitName!=null && !"".equals(visitName)){
    				VisitNameIdentifier visitNameIdentifier = new VisitNameIdentifier();
    					visitNameIdentifier.setVisitName(visitName);
    				mappedMileStone.setVisitNameIdent(visitNameIdentifier);
    			}
    			
    			//Event Name
    			String eventName = milestoneVDAPojo.getEVENTNAME();
    			if(eventName!=null && !"".equals(eventName)){
    				EventNameIdentfier eventIdentifier = new EventNameIdentfier();
    					eventIdentifier.setEventName(eventName);
    				mappedMileStone.setEventNameIdent(eventIdentifier);
    			}
    			
    			//MileStone Rule
    			String mileRuleSubType = milestoneVDAPojo.getMILERULESUBTYP();
    			String milstoneRuleDesc = milestoneVDAPojo.getMSRULMSRULE();
    			if(mileRuleSubType!=null && !"".equals(mileRuleSubType)){
					Code mappedMilestoneRule = CodelstsCache.getMappedCode(requestMap,milestoneType+"_milestoneRule",milestoneType,mileRuleSubType,milstoneRuleDesc);
					mappedMileStone.setPKCODELSTRULE(mappedMilestoneRule);
				}
    			
    			
    			   			
    			//Event Status
    			String eventSubType = milestoneVDAPojo.getMILEVISITSUBTYP();
    			String eventStatus = milestoneVDAPojo.getMSRULEVENTSTAT();
    			if(eventSubType!=null && !"".equals(eventSubType)){
    				eventSubType=eventSubType.trim();
					Code mappedMileEventStatus = CodelstsCache.getMappedCode(requestMap,"milestone_eventStatus","eventstatus",eventSubType,eventStatus);
					mappedMileStone.setMILESTONEEVENTSTATUS(mappedMileEventStatus);
				}
    			
    			//PatientCount
    			String patientCount = milestoneVDAPojo.getMSRULPTCOUNT();
    			if(patientCount!=null && !"".equals(patientCount)){
    				Integer count = Integer.valueOf(patientCount);
    				mappedMileStone.setMILESTONECOUNT(count);
    			}
    			
    			//Patient Status OR StudyStatus
    			String patientStatusSubType = milestoneVDAPojo.getMILEPSSUBTYP();
    			String patientStatus = milestoneVDAPojo.getMSRULPTSTATUS();
    			if(patientStatusSubType!=null && !"".equals(patientStatusSubType)){
    				if(!milestoneType.equals("SM")){
    					Code mappedPatientStatus = CodelstsCache.getMappedCode(requestMap,"milestone_patientStatus","patStatus",patientStatusSubType,patientStatus);
    					mappedMileStone.setMILESTONEPATSTUDYSTAT(mappedPatientStatus); //Bug#23900
    				}else{
    					Code mappedPatientStatus = CodelstsCache.getMappedCode(requestMap,"milestone_studyStatus","studystat",patientStatusSubType,patientStatus);
    					mappedMileStone.setMILESTONEPATSTUDYSTAT(mappedPatientStatus);
    				}
				}
    			
    			
    			//Milestone Amount
    			String milestoneAmount = milestoneVDAPojo.getMILESTONEAMOUNT();
    			if(milestoneAmount!=null && !"".equals(milestoneAmount)){
    				//Double amount = Double.valueOf(milestoneAmount); 
    				mappedMileStone.setMILESTONEAMOUNT(milestoneAmount); //Bug#24116
    			}
    			
    			//HoldBack
    			String milestoneHoldBack = milestoneVDAPojo.getMILESTONEHOLDBACK();
    			if(milestoneHoldBack!=null && !"".equals(milestoneHoldBack)){
    				//Integer holdBack = Integer.valueOf(milestoneHoldBack); 
    				mappedMileStone.setMILESTONEHOLDBACK(milestoneHoldBack); //Bug#24116
    			}
    			
    			//Limit
    			String milestoneLimit = milestoneVDAPojo.getMSRULLIMIT();
    			if(milestoneLimit!=null && !"".equals(milestoneLimit)){
    				Integer limit = Integer.valueOf(milestoneLimit);
    				mappedMileStone.setMILESTONELIMIT(limit);
    			}
    			
    			//PaymentType
    			String paymentSubType = milestoneVDAPojo.getMILEPAYSUBTYP();
    			String paymentType = milestoneVDAPojo.getMSRULPAYTYPE();
    			if(paymentSubType!=null && !"".equals(paymentSubType)){
					Code mappedPaymentType = CodelstsCache.getMappedCode(requestMap,"paymentType","milepaytype",paymentSubType,paymentType);
					mappedMileStone.setMILESTONEPAYTYPE(mappedPaymentType);
				}
    			
    			//PaymentFor
    			String paymentForSubType = milestoneVDAPojo.getMILEPAYFRSUBTYPE();
    			String paymentFor = milestoneVDAPojo.getMSRULPAYFOR();
    			if(paymentForSubType!=null && !"".equals(paymentForSubType)){
					Code mappedPaymentFor = CodelstsCache.getMappedCode(requestMap,"paymentFor","milePayfor",paymentForSubType,paymentFor);
					mappedMileStone.setMILESTONEPAYFOR(mappedPaymentFor);
				}
    			
    			//Date From
    			XMLGregorianCalendar fromDate = milestoneVDAPojo.getMILESTONEDATEFROM();
    			if(fromDate!=null && !"".equals(fromDate)){
       		 		/*XMLGregorianCalendar dateFrom = StringToXMLGregorianCalendar
       		 				.convertStringDateToXmlGregorianCalendar(fromDate,"yyyy-MM-dd",true);
       		 		mappedMileStone.setMILESTONEDATEFROM(dateFrom);*/
    				mappedMileStone.setMILESTONEDATEFROM(fromDate);
    			}
    			
    			//Date To
    			XMLGregorianCalendar toDate = milestoneVDAPojo.getMILESTONEDATETO();
    			if(toDate!=null && !"".equals(toDate)){
       		 		/*XMLGregorianCalendar dateTo = StringToXMLGregorianCalendar
       		 				.convertStringDateToXmlGregorianCalendar(toDate,"yyyy-MM-dd",true);
       		 		mappedMileStone.setMILESTONEDATETO(dateTo);*/
    				mappedMileStone.setMILESTONEDATETO(toDate);
    			}
    			
    			try{
    			//Milestone Status
    			String milestoneStatus = milestoneVDAPojo.getMSRULSTATUS();
    			String mileStatusSubType = milestoneVDAPojo.getMILESTATSUBTYP();
    			
    			String mappedCode = (prop.getProperty(orgName + "_milestoneStatus.Code")).trim();
				String mappedDesc = (prop.getProperty(orgName + "_milestoneStatus.Description")).trim();
				String mappedType = (prop.getProperty(orgName + "_milestoneStatus.Type")).trim();
				
				Code mappedMilestoneStatus = new Code();
				mappedMilestoneStatus.setCode(mappedCode);
				mappedMilestoneStatus.setDescription(mappedDesc);
				mappedMilestoneStatus.setType(mappedType);
				mappedMileStone.setPKCODELSTMILESTONESTAT(mappedMilestoneStatus);
    			}catch(Exception e){
    				logger.fatal("MileStone Status mapping not configured for participating site  "+orgName);
    				throw new Exception("MileStone Status mapping not configured for participating site  "+orgName);
    			}
			
				//Milestone Description
				String milestoneDesc = milestoneVDAPojo.getMILESTONEDESCRIPTION();
				if(milestoneDesc!=null && !"".equals(milestoneDesc)){
       		 		mappedMileStone.setMILESTONEDESCRIPTION(milestoneDesc);
    			}
    			
				//Delete Flag
				mappedMileStone.setMILESTONEDELFLAG("N");
				
				if(calendarName!=null){
					List<String> calCreatedList = (List<String>)requestMap.get(ProtocolKeys.CalendarCreatedList);
					if(calCreatedList!=null && !calCreatedList.isEmpty()){
						if((calCreatedList.contains((orgName+"_"+calendarName)))){
							mappedMilestoneList.add(mappedMileStone);
						}
					}					
				}else{   			
					mappedMilestoneList.add(mappedMileStone);
				}
				
			}
    		logger.info("Mapped Milestone obj List = "+mappedMilestoneList);
    		dataMap.put(ProtocolKeys.MappedMilestoneList,mappedMilestoneList);
    	}
    	
    	
    	
	}

}
