package com.velos.integration.authentication;

import java.io.IOException;
import java.util.Properties;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;

import org.apache.log4j.Logger;
import org.apache.ws.security.WSPasswordCallback;
import org.springframework.context.ApplicationContext;

import com.velos.integration.init.ApplicationContextProvider;
import com.velos.integration.notifications.EmailNotification;
import com.velos.integration.util.PropertiesUtil;

public class UTHSCSAAuthCallbackHandler implements CallbackHandler {

	private static Logger logger = Logger.getLogger(UTHSCSAAuthCallbackHandler.class);
	
	private PropertiesUtil propertiesUtil;

	public PropertiesUtil getPropertiesUtil() {
		return propertiesUtil;
	}

	public void setPropertiesUtil(PropertiesUtil propertiesUtil) {
		this.propertiesUtil = propertiesUtil;
	}
	
	public void handle(Callback[] callbacks) {
		/*Properties prop = new Properties();
		try {
			prop.load(this.getClass().getClassLoader()
					.getResourceAsStream("config.properties"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
	/*	ApplicationContext appcontext = ApplicationContextProvider.getApplicationContext();
		logger.info("Application COntext = "+appcontext);
		
		EmailNotification notificationObj =(EmailNotification)appcontext.getBean("emailNotification");
		logger.info("EmailNotification Obj = "+notificationObj);
		
		PropertiesUtil propertiesUtil =(PropertiesUtil)appcontext.getBean("propertiesUtil");
		logger.info("*PropertiesUtil Obj = "+propertiesUtil);*/
		
		Properties prop = propertiesUtil.getAllProperties();
		
		logger.info("inside UTHSCSA AuthCallbackHandler");
		WSPasswordCallback pc = (WSPasswordCallback) callbacks[0];
		if (pc.getIdentifier().equals(prop
				.getProperty("UTHSCSA.userID"))) {
			pc.setPassword(prop
					.getProperty("UTHSCSA.password"));
		}
	}
}
