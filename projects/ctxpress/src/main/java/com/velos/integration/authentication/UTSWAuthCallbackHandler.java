package com.velos.integration.authentication;

import java.io.IOException;
import java.util.Properties;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;

import org.apache.log4j.Logger;
import org.apache.ws.security.WSPasswordCallback;

import com.velos.integration.util.PropertiesUtil;

public class UTSWAuthCallbackHandler implements CallbackHandler {

	private static Logger logger = Logger
			.getLogger(UTSWAuthCallbackHandler.class);

	private PropertiesUtil propertiesUtil;

	public PropertiesUtil getPropertiesUtil() {
		return propertiesUtil;
	}

	public void setPropertiesUtil(PropertiesUtil propertiesUtil) {
		this.propertiesUtil = propertiesUtil;
	}

	public void handle(Callback[] callbacks) {

		Properties prop = propertiesUtil.getAllProperties();
		logger.info("inside UTSW AuthCallbackHandler");
		WSPasswordCallback pc = (WSPasswordCallback) callbacks[0];
		if (pc.getIdentifier().equals(prop.getProperty("UTSW.userID"))) {
			pc.setPassword(prop.getProperty("UTSW.password"));
		}
	}
}
