package com.velos.integration.espclient;

import java.io.IOException;
import java.util.Properties;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;

import org.apache.log4j.Logger;
import org.apache.ws.security.WSPasswordCallback;

public class UTHSCSAAuthCallbackHandler implements CallbackHandler {

	private static Logger logger = Logger.getLogger(UTHSCSAAuthCallbackHandler.class);
	public void handle(Callback[] callbacks) {
		Properties prop = new Properties();
		try {
			prop.load(this.getClass().getClassLoader()
					.getResourceAsStream("config.properties"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.info("inside UTHSCSAAuthCallbackHandler");
		logger.info("user - " + prop
				.getProperty("UTHSCSA.userID"));
		logger.info("password - " + prop
				.getProperty("UTHSCSA.password"));
		WSPasswordCallback pc = (WSPasswordCallback) callbacks[0];
		if (pc.getIdentifier().equals(prop
				.getProperty("UTHSCSA.userID"))) {
			pc.setPassword(prop
					.getProperty("UTHSCSA.password"));
		}
	}
}
