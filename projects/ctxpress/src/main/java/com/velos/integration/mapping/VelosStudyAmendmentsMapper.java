package com.velos.integration.mapping;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.log4j.Logger;

import com.velos.integration.util.CodelstsCache;
import com.velos.services.Code;
import com.velos.services.GetStudyCalendarListResponse;
import com.velos.services.StudyAppendix;
import com.velos.services.StudyCalendar;
import com.velos.services.StudyStatusHistory;
import com.velos.services.StudyVersion;
import com.velos.services.StudyVersionIdentifier;

public class VelosStudyAmendmentsMapper {

	private static Logger logger = Logger.getLogger(VelosStudyAmendmentsMapper.class);
	private String endpoint = null;
	private Map<VelosKeys, Object> dataMap = null;
	Properties prop = null;
	
	public VelosStudyAmendmentsMapper(String endpoint) {
		this.endpoint = endpoint;
	}
	
	public Map<VelosKeys, Object> mapOrgStudyAmendments(List<StudyVersion> studyAmendList,Map<VelosKeys, Object> requestMap) throws Exception {
		if (EndpointKeys.CTXpress.toString().equals(endpoint)) {
			mapForOrgStudyAmendments(studyAmendList,requestMap);
		}
		return dataMap;
	}
	
	private void mapForOrgStudyAmendments(List<StudyVersion> studyAmendList,Map<VelosKeys, Object> requestMap) throws Exception {
		
		dataMap = new HashMap<VelosKeys, Object>();
		prop = (Properties)requestMap.get(ProtocolKeys.PropObj);
		String orgName = ((String)requestMap.get(ProtocolKeys.OrganizationName)).trim();
    	
    	logger.info("*******Getting Mapped Amendments Data for org = "+orgName);
    	
 		String partAmendStatus= null;
		String broadAmendStatus = null;
		String versStatus = null;
		String versCatgCode = null;
		String versType = null ;
		Code code = null;
		String brodAmendStatDesc = null;
		String catgcode =null;
		String typeCode = null;
		String amendError = "Amendment Status mapping not found for participating site "+orgName;
		try{
			broadAmendStatus =  prop.getProperty("Amendments.status.subtype").trim();
			brodAmendStatDesc = prop.getProperty("Amendments.status.desc").trim();
			partAmendStatus =  prop.getProperty(orgName+"_amendmentStatus.Code").trim();
			if("".equals(broadAmendStatus) || "".equals(brodAmendStatDesc) || "".equals(partAmendStatus)){
				throw new Exception(amendError);
			}
		}catch(Exception e){
			logger.fatal(amendError);
			throw new Exception(amendError);
		}
			
    	List<StudyVersion> mappedAmendList = null;
    	if(studyAmendList!=null && !studyAmendList.isEmpty()){
    		mappedAmendList = new ArrayList<StudyVersion>();
     		for(StudyVersion sv : studyAmendList){
     			StudyVersion sa = new StudyVersion();
     			sa.setSTUDYVERSTATUS(partAmendStatus);
				Code versionCategory = sv.getSTUDYVERCATEGORY();
				if(versionCategory!=null){
					versCatgCode= versionCategory.getCode();
					String verCatType = versionCategory.getType();
					String verCatDesc = versionCategory.getDescription();
					Code mappedCategory = CodelstsCache.getMappedCode(requestMap,"vercategory",verCatType,versCatgCode,verCatDesc);
					sa.setSTUDYVERCATEGORY(mappedCategory);
				}
			
				Code versionType = sv.getSTUDYVERTYPE();
				if(versionType!=null){
					versType = versionType.getCode();
					String type = versionType.getType();
					String verTypeDesc = versionType.getDescription();
					Code mappedVerType = CodelstsCache.getMappedCode(requestMap,"versType",type,versType,verTypeDesc);
					sa.setSTUDYVERTYPE(mappedVerType);
				}

    			List<StudyStatusHistory> stdStatusHist = sv.getStudyStatusHistory();
    			List<StudyStatusHistory> mappedStatusHist = new ArrayList<StudyStatusHistory>();
    			for(StudyStatusHistory ssh : stdStatusHist){
    				Code statCode = ssh.getPKCODELSTSTAT();
    				if(statCode!=null){
    					versStatus = statCode.getCode();
    				}

    				if(versStatus != null){
    						String statusDesc = ssh.getPKCODELSTSTAT().getDescription();
							XMLGregorianCalendar statusEndDate = ssh.getSTATUSENDDATE();
							if(versStatus.equals(broadAmendStatus)){
									code = new Code();
									code.setCode(partAmendStatus);
									StudyStatusHistory mappedStatus = new StudyStatusHistory();
									mappedStatus.setPKCODELSTSTAT(code);
									mappedStatus.setSTATUSDATE(ssh.getSTATUSDATE());
									mappedStatus.setSTATUSNOTES(ssh.getSTATUSNOTES());
									mappedStatusHist.add(mappedStatus);
									sa.getStudyStatusHistory().addAll(mappedStatusHist);
							}
    				}
    			}
    			
    			XMLGregorianCalendar verDate = sv.getSTUDYVERDATE();
    			if(verDate!=null && !"".equals(verDate)){
       		 		sa.setSTUDYVERDATE(verDate);
    			}
    			
    			String verNotes = sv.getSTUDYVERNOTES();
    			if(verNotes!=null && !"".equals(verNotes)){
					sa.setSTUDYVERNOTES(verNotes);
				}
    			
    			StudyVersionIdentifier svIdentifier = sv.getStudyVerIdentifier();
    			if(svIdentifier!=null){
    				String verNumber = svIdentifier.getSTUDYVERNUMBER();
    				if(verNumber!=null && !"".equals(verNumber)){
    					StudyVersionIdentifier svI = new StudyVersionIdentifier();
    					svI.setSTUDYVERNUMBER(verNumber);
    					sa.setStudyVerIdentifier(svI);
    				}
    			}
    			
    			List<StudyAppendix> studyAppendList = sv.getStudyAppendix();
    			sa.getStudyAppendix().addAll(studyAppendList);
    			
    			mappedAmendList.add(sa);
    		}
    	}
		dataMap.put(ProtocolKeys.CreateStudyVersAmendmentList,mappedAmendList);
	}
}
