package com.velos.integration.budgets;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import com.velos.services.OperationException_Exception;


@WebService(targetNamespace = "http://velos.com/services/", name = "BudgetSEI")
public interface VelosEspGetStudyCalBudgetEndPoint {
	  @WebResult(name = "Budget", targetNamespace = "")
	    @RequestWrapper(localName = "getStudyCalBudget", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetStudyCalBudget")
	    @WebMethod
	    @ResponseWrapper(localName = "getStudyCalBudgetResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetStudyCalBudgetResponse")
	    public com.velos.services.BudgetDetail getStudyCalBudget(
	        @WebParam(name = "StudyIdentifier", targetNamespace = "")
	        com.velos.services.StudyIdentifier studyIdentifier,
	        @WebParam(name = "CalendarNameIdentifier", targetNamespace = "")
	        com.velos.services.CalendarNameIdentifier calendarNameIdentifier
	    ) throws OperationException_Exception;

}
