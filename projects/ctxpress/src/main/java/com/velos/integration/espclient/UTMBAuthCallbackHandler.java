package com.velos.integration.espclient;

import java.io.IOException;
import java.util.Properties;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;

import org.apache.log4j.Logger;
import org.apache.ws.security.WSPasswordCallback;

public class UTMBAuthCallbackHandler implements CallbackHandler {

	private static Logger logger = Logger
			.getLogger(UTMBAuthCallbackHandler.class);

	public void handle(Callback[] callbacks) {
		Properties prop = new Properties();
		try {
			prop.load(this.getClass().getClassLoader()
					.getResourceAsStream("config.properties"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.info("inside UTMBAuthCallbackHandler");
		logger.info("user - " + prop.getProperty("UTMB.userID"));
		logger.info("password - " + prop.getProperty("UTMB.password"));
		WSPasswordCallback pc = (WSPasswordCallback) callbacks[0];
		if (pc.getIdentifier().equals(prop.getProperty("UTMB.userID"))) {
			pc.setPassword(prop.getProperty("UTMB.password"));
		}
	}
}
