package com.velos.integration.notifications;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.mail.MailParseException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.apache.log4j.Logger;

import com.velos.integration.mapping.ProtocolKeys;
import com.velos.integration.mapping.VelosKeys;

public class EmailNotification {
	private static Logger logger = Logger
			.getLogger(EmailNotification.class.getName());

	private JavaMailSender mailSender;
	private SimpleMailMessage simpleMailMessage;
	private SimpleMailMessage smMessage;
	Properties prop = new Properties();
	InputStream input,input1;


	public void setSimpleMailMessage(SimpleMailMessage simpleMailMessage) {
		this.simpleMailMessage = simpleMailMessage;
	}

	public SimpleMailMessage getSmMessage() {
		return smMessage;
	}

	public void setSmMessage(SimpleMailMessage smMessage) {
		this.smMessage = smMessage;
	}

	public void setMailSender(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}
/*
	//Error Email Notification
	public void sendMail(String content,String errorMsg) throws IOException {
		logger.info("Error Email Notification");

		MimeMessage errormessage=mailSender.createMimeMessage();



		try{
			input=this.getClass().getClassLoader().getResourceAsStream("email.properties");
			prop.load(input);
			logger.info("Message Parameters set");
			String enviornment = prop.getProperty("enviornment");  
			logger.info("Message Parameters set1");
			MimeMessageHelper helper = new MimeMessageHelper(errormessage, true);
			logger.info("Message Parameters set2");
			if(!"".equals(prop.getProperty("mail.from")) && prop.containsKey("mail.from")){
				helper.setFrom(smMessage.getFrom());
			}
			helper.setTo(smMessage.getTo());

			logger.info("Message Parameters set3");
			helper.setSubject(enviornment + " : "+ smMessage.getSubject());
			logger.info("Message Parameters set4");

			helper.setText(String.format(
					smMessage.getText(),"","\n"+ prop.getProperty("mail.msgbody3").toString().trim()+"\n\n"
							+ content+"\n\n\n"+errorMsg));		
			logger.info("Message Parameters set5");

			mailSender.send(errormessage);
			logger.info("*******Email sent successfully ********");

		}catch(Exception e){
			e.printStackTrace();
			
		}


	}
*/

/*	//Success Email Notification
	public void sendMail(String content,String errorMsg,String msgdate,String mrn,String msgId) throws IOException {
		logger.info("Success Email Notification");
		
		logger.info("Creating Mime Message");
		MimeMessage message = mailSender.createMimeMessage();
		logger.info("Mime Message got created");


		try{

			input1 = this.getClass().getClassLoader()
					.getResourceAsStream("email.properties");			
			prop.load(input1);

			logger.info("Setting message Parameters");
			String enviornment = prop.getProperty("enviornment");   
			MimeMessageHelper helper = new MimeMessageHelper(message, true);

			System.out.println("Message Id--->"+msgId);

			//helper.setFrom(simpleMailMessage.getFrom());
			if(!"".equals(prop.getProperty("mail.from")) && prop.containsKey("mail.from")){
				helper.setFrom(simpleMailMessage.getFrom());
			}
			helper.setTo(simpleMailMessage.getTo());
			helper.setSubject(enviornment + " : " 
					+ simpleMailMessage.getSubject() +mrn+" } - Message Id "+"{ "+msgId+" }");

			helper.setText(String.format(
					simpleMailMessage.getText(),"","\n"+ prop.getProperty("mail.msgbody").toString().trim()+"\n\n"
							+ content+"\n\n\n"+errorMsg));		
			logger.info("Message Parameters set5");
			
			String fileLoc = new File(System.getProperty("catalina.base"))+ "/logs/coverage.log";
			logger.info("Log File location : " + fileLoc);
			File file = new File(fileLoc);			
			helper.addAttachment("coverage.log", file);
			mailSender.send(message);
			logger.info("Email sent successfully");
		}catch (Exception e) {
			e.printStackTrace();
			
		}finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}*/
	
	public void sendNotification(Map<VelosKeys, Object> requestMap,String mailSubject,String participatingSite,String componentType,String componentName,String status,List<String> errorList){
		
		logger.info("sendNotification()");
		try{
			input=this.getClass().getClassLoader().getResourceAsStream("email.properties");
			prop.load(input);
		
		String email = (String)requestMap.get(ProtocolKeys.EmailID);
		logger.info("Email ID = "+email);
		//logger.info("EMail Obj = "+emailNotification);

		String studyNum = (String)requestMap.get(ProtocolKeys.StudyNumber);
		
		if(email!=null && !"".equals(email)){
			
		String bSite = (String)requestMap.get(ProtocolKeys.BroadCastingSite);
		
		StringBuffer msgText = new StringBuffer();
		if (prop.containsKey("mail.msgText") && !"".equals(prop.getProperty("mail.msgText").trim())) {
			String msgTextData = prop.getProperty("mail.msgText").trim();
			msgText.append(msgTextData);
		}
		msgText.append("\n");
		if (prop.containsKey("mail.StudyNumber") && !"".equals(prop.getProperty("mail.StudyNumber").trim())) {
			String mailStudy = prop.getProperty("mail.StudyNumber").trim();
			msgText.append(mailStudy+" "+studyNum);
		}
		msgText.append("\n");
		if (prop.containsKey("mail.BroadcastingSite") && !"".equals(prop.getProperty("mail.BroadcastingSite").trim())) {
			String mailBSite = prop.getProperty("mail.BroadcastingSite").trim();
			msgText.append(mailBSite+" "+bSite);
		}
		msgText.append("\n");
		if (prop.containsKey("mail.ParticipatingSite") && !"".equals(prop.getProperty("mail.ParticipatingSite").trim())) {
			String mailPSite = prop.getProperty("mail.ParticipatingSite").trim();
			msgText.append(mailPSite+" "+participatingSite);
		}
		msgText.append("\n");
		if (prop.containsKey("mail.ComponentType") && !"".equals(prop.getProperty("mail.ComponentType").trim())) {
			String mailCType = prop.getProperty("mail.ComponentType").trim();
			msgText.append(mailCType+" "+componentType);
		}
		msgText.append("\n");
		if (prop.containsKey("mail.ComponentName") && !"".equals(prop.getProperty("mail.ComponentName").trim())) {
			String mailCName = prop.getProperty("mail.ComponentName").trim();
			msgText.append(mailCName+" "+componentName);
		}
		msgText.append("\n");
		if (prop.containsKey("mail.ComponentStatus") && !"".equals(prop.getProperty("mail.ComponentStatus").trim())) {
			String mailCStatus = prop.getProperty("mail.ComponentStatus").trim();
			msgText.append(mailCStatus+" "+status);
		}
		msgText.append("\n");
		StringBuffer sb = null;
		if(errorList!=null && !errorList.isEmpty()){
			sb = new StringBuffer();
			for(String error : errorList){
				sb.append(error);
				sb.append("\n");
			}
		}
		if(sb!=null){
			msgText.append("Error Description : "+sb.toString());
		}
		if (prop.containsKey("mail.Otherinfo") && !"".equals(prop.getProperty("mail.Otherinfo"))) {
			String info1 = prop.getProperty("mail.Otherinfo");
			msgText.append(info1);
		}
		String errorMsg = "";
		errorMsg = msgText.toString();
		/*if(emailNotification!=null){
			if(email!=null && !"".equals(email)){
				emailNotification.sendMail(mailSubject+studyNum,errorMsg,email);
			}
		}*/
		logger.info("Sending Email");
			sendMail(mailSubject+studyNum,errorMsg,email);
		}
		else{
		}
		}catch(Exception e){
			logger.error("Error : ",e);
		}
		
	}
	//Error Email Notification
	public void sendMail(String subject,String errorMsg,String studyCoordinators){
		//logger.info("*******Study Contacts Error Email Notification ********");

		MimeMessage errormessage=mailSender.createMimeMessage();
		List<String> list = null;

		String studyContMsg= "";


		try{
			input=this.getClass().getClassLoader().getResourceAsStream("email.properties");
			prop.load(input);
			String environment = prop.getProperty("environment");  
			MimeMessageHelper helper = new MimeMessageHelper(errormessage, true);
			if(!"".equals(prop.getProperty("mail.from")) && prop.containsKey("mail.from")){
				helper.setFrom(smMessage.getFrom());
			}
			String to [] = new String[100];
			to =smMessage.getTo();

			if(studyCoordinators == null){
				//logger.info("*******No Study Contacts Error Email Notification ********");
				studyContMsg = prop.getProperty("mail.nostudycontacts");
				helper.setTo(to);

			}else if(studyCoordinators != null || !"".equals(studyCoordinators)){
				//logger.info("*******Study Contacts Error Email Notification starts ********");
				if(studyCoordinators.contains("@")){ 

					list = new ArrayList<String>(Arrays.asList(to));

					for(String a :studyCoordinators.split(",")){
						if(a.contains("@")){
							list.add(a);
						}else {
							studyContMsg += a+"\n";
						}
					}
					String emailId[]=new String[list.size()];

					for(int i=0; i<list.size();i++){
						emailId[i] = list.get(i);
						logger.info("Email ID = "+emailId[i]);
					}
					
					helper.setTo(emailId);

				} else if(!studyCoordinators.contains("@")){
					studyContMsg = studyCoordinators;
					helper.setTo(to);
				}
			}
//			helper.setSubject(enviornment + " : "+ smMessage.getSubject());
			helper.setSubject(environment + " : "+ subject);

			/*helper.setText(String.format(
					smMessage.getText(),"","\n"+ prop.getProperty("mail.msgbody").toString().trim()+"\n\n"
							+studyContMsg+"\n\n"+errorMsg));*/	
			helper.setText(String.format(smMessage.getText(),"","\n"+errorMsg));

			mailSender.send(errormessage);
			logger.info("Email Sent successfully");
		}catch(Exception e){
			//e.printStackTrace();
			logger.error("Email Sent Failed : ", e);
		}


	}
	
	public void sendCodelstNotification(Map<VelosKeys, Object> requestMap,String mailSubject,String participatingSite,String componentName,String status,String notfType){
		
		logger.info("sendCodelstNotification()");
		try{
			input=this.getClass().getClassLoader().getResourceAsStream("email.properties");
			prop.load(input);
			
			String enableNotification = null;
			
			if (prop.containsKey("Enable_Notification_For_CodelstMismatch") && !"".equals(prop.getProperty("Enable_Notification_For_CodelstMismatch").trim())) {
				enableNotification = prop.getProperty("Enable_Notification_For_CodelstMismatch").trim();
			}
		
			String studyNum = (String)requestMap.get(ProtocolKeys.StudyNumber);
			
		if(enableNotification!=null && !"".equals(enableNotification) && enableNotification.equalsIgnoreCase("Y")){
			
			String bSite = (String)requestMap.get(ProtocolKeys.BroadCastingSite);
	
			StringBuffer msgText = new StringBuffer();
			if (prop.containsKey("mail.codelstText") && !"".equals(prop.getProperty("mail.codelstText").trim())) {
				String msgTextData = prop.getProperty("mail.codelstText").trim();
				msgText.append(msgTextData);
			}
			msgText.append("\n");
			msgText.append(componentName);
			msgText.append("\n");
			if (prop.containsKey("mail.StudyNumber") && !"".equals(prop.getProperty("mail.StudyNumber").trim())) {
				String mailStudy = prop.getProperty("mail.StudyNumber").trim();
				msgText.append(mailStudy+" "+studyNum);
			}
			msgText.append("\n");
			if (prop.containsKey("mail.BroadcastingSite") && !"".equals(prop.getProperty("mail.BroadcastingSite").trim())) {
				String mailBSite = prop.getProperty("mail.BroadcastingSite").trim();
				msgText.append(mailBSite+" "+bSite);
			}
			msgText.append("\n");
			if (prop.containsKey("mail.ParticipatingSite") && !"".equals(prop.getProperty("mail.ParticipatingSite").trim())) {
				String mailPSite = prop.getProperty("mail.ParticipatingSite").trim();
				msgText.append(mailPSite+" "+participatingSite);
			}
			msgText.append("\n");
			if (prop.containsKey("mail.ComponentStatus") && !"".equals(prop.getProperty("mail.ComponentStatus").trim())) {
				String mailCStatus = prop.getProperty("mail.ComponentStatus").trim();
				msgText.append(mailCStatus+" "+status);
			}
			msgText.append("\n");
			if (prop.containsKey("codelst.Otherinfo") && !"".equals(prop.getProperty("codelst.Otherinfo"))) {
				String info = prop.getProperty("codelst.Otherinfo");
				msgText.append(info);
			}
			String errorMsg = "";
			errorMsg = msgText.toString();
		
			logger.info("Sending Email");
				sendCodelstMail(mailSubject,errorMsg,notfType,participatingSite);
		}else{
			logger.info("Notification For Codelst Mismatch is Turned OFF");
		}
		}catch(Exception e){
			logger.error("Error : ",e);
		}
		
	}
	
	public void sendCodelstMail(String subject,String errorMsg,String notfType,String participatingSite){

	MimeMessage errormessage=mailSender.createMimeMessage();
	try{
		input=this.getClass().getClassLoader().getResourceAsStream("email.properties");
		prop.load(input);
		String environment = prop.getProperty("environment");  
		MimeMessageHelper helper = new MimeMessageHelper(errormessage, true);
		helper.setSubject(environment + " : "+ subject);
		helper.setText(String.format(smMessage.getText(),"","\n"+errorMsg));
		if(!"".equals(prop.getProperty("mail.from")) && prop.containsKey("mail.from")){
			helper.setFrom(smMessage.getFrom());
		}
		String toBroadEmail = null;
		
		if (prop.containsKey("mail.to.BroadCastingSite") && !"".equals(prop.getProperty("mail.to.BroadCastingSite").trim())) {
			toBroadEmail = prop.getProperty("mail.to.BroadCastingSite").trim();
		}
		
		logger.info("Broadcast Email ID = "+toBroadEmail);
		if(notfType.equals("1")){
			if(toBroadEmail!=null && !"".equals(toBroadEmail)){
				helper.setTo(toBroadEmail);
				mailSender.send(errormessage);
				logger.info("Email Sent successfully");
			}else{
				logger.error("Notification Failed.Broadcasting email is null");
			}
		}else if(notfType.equals("2")){
			String toPartEmail = null;
			String partEmailkey = "mail.to.ParticipatingSite."+participatingSite;
			if (prop.containsKey(partEmailkey) && !"".equals(prop.getProperty(partEmailkey).trim())) {
				toPartEmail = prop.getProperty(partEmailkey).trim();
			}
			 
			logger.info("Participant Email ID = "+toPartEmail);
			if(toPartEmail!=null && !"".equals(toPartEmail)){
				helper.setTo(toPartEmail);
				if(toBroadEmail!=null && !"".equals(toBroadEmail)){
					helper.setCc(toBroadEmail);
				}
				mailSender.send(errormessage);
				logger.info("Email Sent successfully");
			}else{
				logger.error("Notification Failed.Participating email is null for organization : "+participatingSite);
			}
		}
	}catch(Exception e){
		//e.printStackTrace();
		logger.error("Email Sent Failed : ", e);
	}


}

/*
	//Success Email Notification
	public void sendMail(String content,String errorMsg,String msgdate,String mrn,String msgId,String studyCoordinators) throws IOException {
		logger.info("Creating Mime Message");
		MimeMessage message = mailSender.createMimeMessage();
		logger.info("Mime Message got created");
		System.out.println("Sending email Notification");
		List<String> list = null;
		String studyContMsg="";
		try{

			input1 = this.getClass().getClassLoader()
					.getResourceAsStream("email.properties");			
			prop.load(input1);

			logger.info("Setting message Parameters");
			String enviornment = prop.getProperty("enviornment");   
			MimeMessageHelper helper = new MimeMessageHelper(message, true);

			System.out.println("Message Id--->"+msgId);

			//helper.setFrom(simpleMailMessage.getFrom());
			if(!"".equals(prop.getProperty("mail.from")) && prop.containsKey("mail.from")){
				helper.setFrom(simpleMailMessage.getFrom());
			}
			//helper.setTo(simpleMailMessage.getTo());
			String to [] = new String[100];
			to =simpleMailMessage.getTo();
			System.out.println("studyCoordinators Object =======>*"+studyCoordinators);




			if(studyCoordinators == null){
				logger.info("*******No Study Contacts ********");
				studyContMsg = prop.getProperty("mail.nostudycontacts");
				helper.setTo(to);
			}else if(studyCoordinators != null || !"".equals(studyCoordinators) ){
				logger.info("*******Study Contacts Email notification satarts ********");
				if(studyCoordinators.contains("@")){ 

					list = new ArrayList<String>(Arrays.asList(to));

					for(String a :studyCoordinators.split(",")){
						if(a.contains("@")){
							list.add(a);
						}else {
							studyContMsg += a+"\n";
						}

					}
					String emailId[]=new String[list.size()];

					for(int i=0; i<list.size();i++){
						emailId[i] = list.get(i);
					}



					helper.setTo(emailId);
				} else if(!studyCoordinators.contains("@")){

					studyContMsg = studyCoordinators;
					helper.setTo(to);

				}
			}

			helper.setSubject(enviornment + " : " 
					+ simpleMailMessage.getSubject() +mrn+" } - Message Id "+"{ "+msgId+" }");

			helper.setText(String.format(
					simpleMailMessage.getText(),"","\n"+ prop.getProperty("mail.msgbody").toString().trim()+"\n\n"
							+studyContMsg+"\n\n"+ content+"\n\n\n"+errorMsg));		
			logger.info("Message Parameters set5");
			mailSender.send(message);
			logger.info("Email Sent successfully");
		}catch (Exception e) {
			e.printStackTrace();
					}finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}


*/
}
