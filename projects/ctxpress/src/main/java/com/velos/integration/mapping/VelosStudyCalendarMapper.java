package com.velos.integration.mapping;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.log4j.Logger;

import com.velos.integration.util.CodelstsCache;
import com.velos.services.CalendarEvent;
import com.velos.services.CalendarEventSummary;
import com.velos.services.CalendarEvents;
import com.velos.services.CalendarSummary;
import com.velos.services.CalendarVisit;
import com.velos.services.CalendarVisitSummary;
import com.velos.services.CalendarVisits;
import com.velos.services.Code;
import com.velos.services.Cost;
import com.velos.services.Costs;
import com.velos.services.Duration;
import com.velos.services.GetStudyCalendarListResponse;
import com.velos.services.GetStudyCalendarResponse;
import com.velos.services.OrganizationIdentifier;
import com.velos.services.Study;
import com.velos.services.StudyCalendar;
import com.velos.services.StudyCalendars;
import com.velos.services.StudyCalendarsList;
import com.velos.services.StudyIdentifier;
import com.velos.services.TimeUnits;
import com.velos.services.UserIdentifier;

public class VelosStudyCalendarMapper {
	
	private static Logger logger = Logger.getLogger(VelosStudyCalendarMapper.class);
	private String endpoint = null;
	private Map<VelosKeys, Object> dataMap = null;
	Properties prop = null;
	
	public VelosStudyCalendarMapper(String endpoint) {
		this.endpoint = endpoint;
		/*prop = new Properties();
		try {
			prop.load(this.getClass().getClassLoader()
					.getResourceAsStream("mapping.properties"));

			prop.load(this.getClass().getClassLoader()
					.getResourceAsStream("config.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}*/
	}
	
	public Map<VelosKeys, Object> mapStudyCalendarList(GetStudyCalendarListResponse resp) {
		if (EndpointKeys.CTXpress.toString().equals(endpoint)) {
			mapStudyCalendarListForEpic(resp);
		}
		return dataMap;
	}
	
	private void mapStudyCalendarListForEpic(GetStudyCalendarListResponse resp) {
		List< Map<VelosKeys, Object> > dataList = new ArrayList< Map<VelosKeys, Object> >();
		StudyCalendarsList list = resp.getStudyCalendarList();
		for (StudyCalendars calendar : list.getStudyCalendars()) {
			if (calendar.getCalendarStatus() == null) { continue; }
			if (!ProtocolKeys.LetterA.toString().equals(calendar.getCalendarStatus().getCode())) {
				continue;
			}
			Map<VelosKeys, Object> map = new HashMap<VelosKeys, Object>();
			map.put(ProtocolKeys.CalendarName, MapUtil.escapeSpecial(calendar.getCalendarName()));
			map.put(ProtocolKeys.CalendarId, calendar.getCalendarIdentifier().getPK());
			dataList.add(map);
		}
		dataMap.put(ProtocolKeys.CalendarIdList, dataList);
	}

	public Map<VelosKeys, Object> mapStudyCalendar(GetStudyCalendarResponse resp) {
		if (EndpointKeys.CTXpress.toString().equals(endpoint)) {
			mapStudyCalendarForEpic(resp);
		}
		return dataMap;
	}
	
	private void mapStudyCalendarForEpic(GetStudyCalendarResponse resp) {
		List< Map<VelosKeys, Object> > dataVisitList = new ArrayList< Map<VelosKeys, Object> >();
		Duration calDuration = resp.getStudyCalendar().getCalendarSummary().getCalendarDuration();
		dataMap.put(ProtocolKeys.CalendarDuration, calDuration.getValue());
		dataMap.put(ProtocolKeys.CalendarDurUnit, calDuration.getUnit().toString());
		logger.info("Cal duration is "+calDuration.getValue()+" "+calDuration.getUnit().toString());
		CalendarVisits list = resp.getStudyCalendar().getVisits();
		for (CalendarVisit visit : list.getVisit()) {
			CalendarEvents eventList = visit.getEvents();
			List< Map<VelosKeys, Object> > dataEventList = new ArrayList< Map<VelosKeys, Object> >();
			for (CalendarEvent event : eventList.getEvent()) {
				Map<VelosKeys, Object> map = new HashMap<VelosKeys, Object>();
				map.put(ProtocolKeys.EventName, MapUtil.escapeSpecial(event.getCalendarEventSummary().getEventName()));
				map.put(ProtocolKeys.EventCpt, event.getCalendarEventSummary().getCPTCode());
				map.put(ProtocolKeys.EventSequence, event.getCalendarEventSummary().getSequence());
				if (event.getCalendarEventSummary().getCoverageType() != null) {
					map.put(ProtocolKeys.CoverageType, 
							event.getCalendarEventSummary().getCoverageType().getCode());
				}
				dataEventList.add(map);
			}
			Map<VelosKeys, Object> map = new HashMap<VelosKeys, Object>();
			map.put(ProtocolKeys.VisitName, MapUtil.escapeSpecial(visit.getCalendarVisitSummary().getVisitName()));
			map.put(ProtocolKeys.EventList, dataEventList);
			map.put(ProtocolKeys.VisitWindowBefore, visit.getCalendarVisitSummary().getVisitWindowBefore().getValue());
			map.put(ProtocolKeys.VisitWindowAfter, visit.getCalendarVisitSummary().getVisitWindowAfter().getValue());
			map.put(ProtocolKeys.VisitDisplacement, 
					visit.getCalendarVisitSummary().getAbsoluteInterval() == null ? 
							0 : visit.getCalendarVisitSummary().getAbsoluteInterval().getValue());
			dataVisitList.add(map);
		}
		dataMap.put(ProtocolKeys.VisitList, dataVisitList);
	}

	//Added By Rajasekhar
	public Map<VelosKeys, Object> mapOrgStudyCalList(List<StudyCalendar> calList,Map<VelosKeys, Object> requestMap) throws Exception {
		if (EndpointKeys.CTXpress.toString().equals(endpoint)) {
			mapForOrgStudyCalList(calList,requestMap);
		}
		return dataMap;
	}
	
	private void mapForOrgStudyCalList(List<StudyCalendar> calList,Map<VelosKeys, Object> requestMap) throws Exception {
		
		dataMap = new HashMap<VelosKeys, Object>();
		
		prop = (Properties)requestMap.get(ProtocolKeys.PropObj);
		
		String orgName = ((String)requestMap.get(ProtocolKeys.OrganizationName)).trim();
    	logger.info("OrganizationName = "+orgName);
    	
    	logger.info("*******Getting Mapped Calendar Data for org = "+orgName);
    	
    	List<StudyCalendar> mappedCalList = null;
    	if(!calList.isEmpty()){
    		mappedCalList = new ArrayList<StudyCalendar>();
    		for(StudyCalendar studyCal : calList){
    			StudyCalendar mappedStudyCalendar = new StudyCalendar();
    			StudyIdentifier mappedStudyIdentifier = new StudyIdentifier();
    			StudyIdentifier studyIdentifier = studyCal.getStudyIdentifier();
    			
    			if (studyIdentifier != null) {
    				logger.info("Study Number ="+studyIdentifier.getStudyNumber());
    				String partSiteStudyNumber = (String)requestMap.get(ProtocolKeys.ParticipatingSite_StudyNumber);
    				logger.info("Adding Participating Study Number ="+partSiteStudyNumber);
    				mappedStudyIdentifier.setStudyNumber(partSiteStudyNumber);
    			}
    			
    			//Set Study Identifier
    			mappedStudyCalendar.setStudyIdentifier(mappedStudyIdentifier);
    			
    			CalendarSummary calendarSummary = studyCal.getCalendarSummary();
    			logger.info("Calendary Summary = "+calendarSummary);
    			CalendarSummary mappedCalendarSummary = null;
    			if(calendarSummary!=null){
    				mappedCalendarSummary = new CalendarSummary();
    				String calDesc = calendarSummary.getCalendarDescription();
    				logger.info("Cal Desc = "+calDesc);
    				if(calDesc!=null && !"".equals(calDesc)){
    					mappedCalendarSummary.setCalendarDescription(calDesc);
    				}
    				Duration duration = calendarSummary.getCalendarDuration();
    				logger.info("Cal Duration = "+duration);
    				if(duration!=null){
    					int value = duration.getValue();
    					logger.info("Duration Value = "+value);
    					TimeUnits timeUnits = duration.getUnit();
    					logger.info("Time Units = "+timeUnits);
    					mappedCalendarSummary.setCalendarDuration(duration);
    				}
    				
    				String calName = calendarSummary.getCalendarName();
    				logger.info("******Calendar name = "+calName);
    				if(calName!=null && !"".equals(calName)){
    					mappedCalendarSummary.setCalendarName(calName);
    				}
    				
    				Code calStatusCode = calendarSummary.getCalendarStatus();
    				if(calStatusCode!=null){
    					String code = calStatusCode.getCode();
    					String desc = calStatusCode.getDescription();
    					String type = calStatusCode.getType();
    				}
    				
    				try{
    				Code mappedCalStatusCode = new Code();
    				String mappedCode = (prop.getProperty(orgName + "_calstatus.Code")).trim();
    				String mappedDesc = (prop.getProperty(orgName + "_calstatus.Description")).trim();
    				String mappedType = (prop.getProperty(orgName + "_calstatus.Type")).trim();
    				   				
    				mappedCalStatusCode.setCode(mappedCode);
    				mappedCalStatusCode.setDescription(mappedDesc);
    				mappedCalStatusCode.setType(mappedType);
    				mappedCalendarSummary.setCalendarStatus(mappedCalStatusCode);
    				}catch(Exception e){
    					logger.fatal("Calendar Status mapping not configured for participating site  "+orgName);
    					throw new Exception("Calendar Status mapping not configured for participating site  "+orgName);
    				}
    				
    				Code calTypeCode = calendarSummary.getCalendarType();
    				Code mappedCalTypeCode = null;
    				logger.info("Cal type Code = "+calTypeCode);
    				if(calTypeCode!=null && !"".equals(calTypeCode)){
    					String code = calTypeCode.getCode();
    					logger.info("Cal code value = "+code);
    					String desc = calTypeCode.getDescription();
    					logger.info("Cal Type Desc = "+desc);
    					String type = calTypeCode.getType();
    					logger.info("Cal Type = "+type);
    					if (prop.containsKey(orgName + "_calCategorySubType."+code) && !"".equals(prop.getProperty(orgName + "_calCategorySubType."+code).trim())) {
    						if (prop.containsKey(orgName + "_calCategoryType."+type) && !"".equals(prop.getProperty(orgName + "_calCategoryType."+type).trim())) {
    							mappedCalTypeCode = new Code();
    							String mappedCalCategorySubType = prop.getProperty(orgName + "_calCategorySubType."+code).trim();
    							String mappedCalCategoryType = prop.getProperty(orgName + "_calCategoryType."+type).trim();
    							logger.info("MappedCalCategorySubType = "+mappedCalCategorySubType);
    							logger.info("MappedCalCategoryType = "+mappedCalCategoryType);
    							mappedCalTypeCode.setCode(mappedCalCategorySubType);
    							mappedCalTypeCode.setType(mappedCalCategoryType);
    							mappedCalTypeCode.setDescription(desc);
    							mappedCalendarSummary.setCalendarType(mappedCalTypeCode);
    						}
    					}else{
    						mappedCalendarSummary.setCalendarType(calTypeCode);
    					}
    				}
    				
    				UserIdentifier statusChangedBy = calendarSummary.getStatusChangedBy();
    				if(statusChangedBy!=null){
    					String fName = statusChangedBy.getFirstName();
    					logger.info("First Name = "+fName);
    					String lName = statusChangedBy.getLastName();
    					logger.info("Last name = "+lName);
    					String uLogName = statusChangedBy.getUserLoginName();
    					logger.info("User Login Name = "+uLogName);
    					mappedCalendarSummary.setStatusChangedBy(statusChangedBy);
    				}
    				
    				XMLGregorianCalendar calStatusDate = calendarSummary.getStatusDate();
    				if(calStatusDate!=null){
    					int year =calStatusDate.getYear();
    					logger.info("Year = "+year);
    					int month = calStatusDate.getMonth();
    					logger.info("Month = "+month);
    					int day = calStatusDate.getDay();
    					logger.info("Day = "+day);
    					mappedCalendarSummary.setStatusDate(calStatusDate);
    				}
    				
    				String calStatusNotes = calendarSummary.getStatusNotes();
    				logger.info("Cal Status Notes = "+calStatusNotes);
    				if(calStatusNotes!=null && !"".equals(calStatusNotes)){
    					mappedCalendarSummary.setStatusNotes(calStatusNotes);
    				}
    				
    				//Set Mapped  Calendar Summary
    				mappedStudyCalendar.setCalendarSummary(mappedCalendarSummary);
    			}	
    			
    			CalendarEvents calEvents = studyCal.getEvents();
    			logger.info("Library Calendar Events = "+calEvents);
    			if(calEvents!=null){
    				CalendarEvents mappedCalendarEvents = getMappedCalendarEvents(requestMap,calEvents,orgName);
					logger.info("Mapped Libraray Calendar Events = "+mappedCalendarEvents);
					mappedStudyCalendar.setEvents(mappedCalendarEvents);
    			}
  			
    			CalendarVisits calVisits = studyCal.getVisits();
    			logger.info("Cal Visits = "+calVisits);
    			if(calVisits!=null){
    				//mappedStudyCalendar.setVisits(calVisits);
    				CalendarVisits mappedCalVisits = getMappedCalendarVisits(requestMap,calVisits,orgName);
    				logger.info("Mapped Calendar Visits = "+mappedCalVisits);
    				mappedStudyCalendar.setVisits(mappedCalVisits);
    			}
    			logger.info("Adding Mapped Study Calendar to List ");
    			mappedCalList.add(mappedStudyCalendar);
     		}
    		logger.info("Mapped Cal obj List = "+mappedCalList);
    		dataMap.put(ProtocolKeys.CalObjList,mappedCalList);
    	}
    	
    	
	}

	private CalendarVisits getMappedCalendarVisits(Map<VelosKeys, Object> requestMap,CalendarVisits calVisits,String orgName){
		
		logger.info("Getting Mapped Calendar Vists");
		CalendarVisits mappedCalVisits = new CalendarVisits();
		List<CalendarVisit> visitList = calVisits.getVisit();
		List<CalendarVisit> mappedVisitList = new ArrayList<CalendarVisit>();
   	 	
		if(visitList!=null && !visitList.isEmpty()){
			for(CalendarVisit visit : visitList){
				//Visit start
				CalendarVisit mappedVisit = null;
				if(visit!=null){
					mappedVisit = new CalendarVisit();
    				
					//visitSummary
					CalendarVisitSummary visitSummary = visit.getCalendarVisitSummary();
					CalendarVisitSummary mappedVisitSummary =null;
					if(visitSummary!=null){
						mappedVisitSummary = new CalendarVisitSummary();
						String visitName = visitSummary.getVisitName();
						if(visitName != null && !"".equals(visitName)){
							mappedVisitSummary.setVisitName(visitName);
						}
						
						Integer visitSequence = visitSummary.getVisitSequence();
						mappedVisitSummary.setVisitSequence(visitSequence);
						
						String visitDescription =  visitSummary.getVisitDescription();
						if(visitDescription != null && !"".equals(visitDescription)){
							mappedVisitSummary.setVisitDescription(visitDescription);
						}
						
						Boolean noIntervalDefined = visitSummary.isNoIntervalDefined();
						mappedVisitSummary.setNoIntervalDefined(noIntervalDefined);
						
						Duration absoluteInterval = visitSummary.getAbsoluteInterval();
						if(absoluteInterval!=null){
							TimeUnits timeUnits = absoluteInterval.getUnit();
							Integer value = absoluteInterval.getValue();
							int checkValue = (int)value;
							/*if(checkValue!=0){
								mappedVisitSummary.setAbsoluteInterval(absoluteInterval);
							}*/
							if(timeUnits==null){
								Duration duration = new Duration();
									duration.setUnit(TimeUnits.DAY);
									duration.setValue(value);
								mappedVisitSummary.setAbsoluteInterval(duration);
							}else{
								mappedVisitSummary.setAbsoluteInterval(absoluteInterval);
							}
						}
						
						Duration visitWindowAfter = visitSummary.getVisitWindowAfter();
						if(visitWindowAfter!=null){
							TimeUnits timeUnits = visitWindowAfter.getUnit();
							Integer value = visitWindowAfter.getValue();
							int checkValue = (int)value;
							/*if(checkValue!=0){
								mappedVisitSummary.setVisitWindowAfter(visitWindowAfter);
							}*/
							if(timeUnits==null){
								Duration duration = new Duration();
									duration.setUnit(TimeUnits.DAY);
									duration.setValue(value);
								mappedVisitSummary.setVisitWindowAfter(duration);
							}else{
								mappedVisitSummary.setVisitWindowAfter(visitWindowAfter);
							}
						}
						
						Duration visitWindowBefore = visitSummary.getVisitWindowBefore();
						if(visitWindowAfter!=null){
							TimeUnits timeUnits = visitWindowBefore.getUnit();
							Integer value = visitWindowBefore.getValue();
							int checkValue = (int)value;
							/*if(checkValue!=0){
								mappedVisitSummary.setVisitWindowBefore(visitWindowBefore);
							}*/
							if(timeUnits==null){
								Duration duration = new Duration();
									duration.setUnit(TimeUnits.DAY);
									duration.setValue(value);
								mappedVisitSummary.setVisitWindowBefore(duration);
							}else{
								mappedVisitSummary.setVisitWindowBefore(visitWindowBefore);
							}
						}
						
						String relativeVisitName = visitSummary.getRelativeVisitName();
						if(relativeVisitName != null && !"".equals(relativeVisitName)){
							mappedVisitSummary.setRelativeVisitName(relativeVisitName);
						}
						
						Duration  relativeVisitInterval= visitSummary.getRelativeVisitInterval();
						if(relativeVisitInterval!=null){
							TimeUnits timeUnits = relativeVisitInterval.getUnit();
							Integer value = relativeVisitInterval.getValue();
							int checkValue = (int)value;
							/*if(checkValue!=0){
								mappedVisitSummary.setRelativeVisitInterval(relativeVisitInterval);
							}*/
							if(timeUnits==null){
								Duration duration = new Duration();
									duration.setUnit(TimeUnits.DAY);
									duration.setValue(value);
								mappedVisitSummary.setRelativeVisitInterval(duration);
							}else{
								mappedVisitSummary.setRelativeVisitInterval(relativeVisitInterval);
							}
						}
						
						mappedVisit.setCalendarVisitSummary(mappedVisitSummary);
						//End Visit Summary
					}
					//Calendar Events
					CalendarEvents  calendarEvents = visit.getEvents();
					if(calendarEvents!=null){
						//Getting Mapped Calendar Events
						CalendarEvents mappedCalendarEvents = getMappedCalendarEvents(requestMap,calendarEvents,orgName);
						mappedVisit.setEvents(mappedCalendarEvents);
					}
					
					//Set CalendarVisit to list
					mappedVisitList.add(mappedVisit);			
				}
			}
    	}
		logger.info("Set All Mapped Visits");
		mappedCalVisits.getVisit().addAll(mappedVisitList);
		
		return mappedCalVisits;
	}
	
	private CalendarEvents getMappedCalendarEvents(Map<VelosKeys, Object> requestMap,CalendarEvents calendarEvents,String orgName){
	
		CalendarEvents mappedCalendarEvents = new CalendarEvents();
		List<CalendarEvent> eventList = calendarEvents.getEvent();
		List<CalendarEvent> mappedEventList = new ArrayList<CalendarEvent>();
   	 	
		if(eventList!=null && !eventList.isEmpty()){
			for(CalendarEvent event : eventList){
				CalendarEvent mappedEvent = null;
				if(event!=null){
					mappedEvent = new CalendarEvent();
					
					CalendarEventSummary eventSummary = event.getCalendarEventSummary();
					CalendarEventSummary mappedEventSummary = null;
					if(eventSummary!=null){
						mappedEventSummary = new CalendarEventSummary();
						String eventName = eventSummary.getEventName();
						if(eventName != null && !"".equals(eventName)){
							mappedEventSummary.setEventName(eventName);
						}
						
						Integer eventSequence = eventSummary.getSequence();
						mappedEventSummary.setSequence(eventSequence);
						
						String eventDescription = eventSummary.getDescription();
						if(eventDescription != null && !"".equals(eventDescription)){
							mappedEventSummary.setDescription(eventDescription);
						}
						
						String cptCode = eventSummary.getCPTCode();
						if(cptCode != null && !"".equals(cptCode)){
							mappedEventSummary.setCPTCode(cptCode);
						}
						
						Duration eventDuration = eventSummary.getEventDuration();
						if(eventDuration!=null){
							TimeUnits timeUnits = eventDuration.getUnit();
							Integer value = eventDuration.getValue();
							int checkValue = (int)value;
							/*if(checkValue!=0){
								mappedEventSummary.setEventDuration(eventDuration);
							}*/
							if(timeUnits==null){
								Duration duration = new Duration();
									duration.setUnit(TimeUnits.DAY);
									duration.setValue(value);
								mappedEventSummary.setEventDuration(duration);
							}else{
								mappedEventSummary.setEventDuration(eventDuration);
							}
						}
						
						Duration eventWindowAfter = eventSummary.getEventWindowAfter();
						if(eventWindowAfter!=null){
							TimeUnits timeUnits = eventWindowAfter.getUnit();
							Integer value = eventWindowAfter.getValue();
							int checkValue = (int)value;
							/*if(checkValue!=0){
								mappedEventSummary.setEventWindowAfter(eventWindowAfter);
							}*/
							if(timeUnits==null){
								Duration duration = new Duration();
									duration.setUnit(TimeUnits.DAY);
									duration.setValue(value);
								mappedEventSummary.setEventWindowAfter(duration);
							}else{
								mappedEventSummary.setEventWindowAfter(eventWindowAfter);
							}
						}
						
						Duration eventWindowBefore = eventSummary.getEventWindowBefore();
						if(eventWindowBefore!=null){
							TimeUnits timeUnits = eventWindowBefore.getUnit();
							Integer value = eventWindowBefore.getValue();
							int checkValue = (int)value;
							/*if(checkValue!=0){
								mappedEventSummary.setEventWindowBefore(eventWindowBefore);
							}*/
							if(timeUnits==null){
								Duration duration = new Duration();
									duration.setUnit(TimeUnits.DAY);
									duration.setValue(value);
								mappedEventSummary.setEventWindowBefore(duration);
							}else{
								mappedEventSummary.setEventWindowBefore(eventWindowBefore);
							}
						}
						
						String eventNotes = eventSummary.getNotes();
						if(eventNotes != null && !"".equals(eventNotes)){
							mappedEventSummary.setNotes(eventNotes);
						}
						
						OrganizationIdentifier siteOfService = eventSummary.getSiteOfService();
						if(siteOfService!=null){
							String siteAltId = siteOfService.getSiteAltId();
							if(siteAltId != null && !"".equals(siteAltId)){
								
							}
							String siteName = siteOfService.getSiteName();
							if(siteName != null && !"".equals(siteName)){
								
							}
							//set
						}
						
						
						OrganizationIdentifier facility = eventSummary.getFacility();
						if(facility!=null){
							String siteAltId = facility.getSiteAltId();
							if(siteAltId != null && !"".equals(siteAltId)){
								
							}
							String siteName = facility.getSiteName();
							if(siteName != null && !"".equals(siteName)){
								
							}
							//set
						}
						
						//Bug#23877
						String coverageNotes = eventSummary.getCoverageNotes();
						if(coverageNotes!=null && !"".equals(coverageNotes)){
							mappedEventSummary.setCoverageNotes(coverageNotes);							
						}
						
						Code coverageTypeCode = eventSummary.getCoverageType();
						if(coverageTypeCode!=null){
							String coverageSubType = coverageTypeCode.getCode();
							String coverageDesc = coverageTypeCode.getDescription();
							String coverageType = coverageTypeCode.getType();
							Code mappedCoverageTypeCode = CodelstsCache.getMappedCode(requestMap,"coverageType",coverageType,coverageSubType,coverageDesc);
							mappedEventSummary.setCoverageType(mappedCoverageTypeCode);
						}
						mappedEvent.setCalendarEventSummary(mappedEventSummary);
					}
					//Costs
					Costs costs = event.getCosts();	
					if(costs!=null){
						//mappedEvent.setCosts(costs);
						Costs mappedCosts = getMappedCosts(requestMap,costs,orgName);
						mappedEvent.setCosts(mappedCosts);
					}
				}
				mappedEventList.add(mappedEvent);
			}
		}
		mappedCalendarEvents.getEvent().addAll(mappedEventList);
    	
		return mappedCalendarEvents;
	}
	
	private Costs getMappedCosts(Map<VelosKeys, Object> requestMap,Costs costs,String orgName){
		
		logger.info("Getting Mapped Calendar Event Costs");
		Costs mappedCosts = new Costs();
		List<Cost> costList = costs.getCost();
		List<Cost> mappedCostList = new ArrayList<Cost>();
   	 	
		if(costList!=null && !costList.isEmpty()){
			for(Cost cost : costList){
				Cost mappedCost = null;
				if(cost!=null){
					mappedCost = new Cost();
					
					BigDecimal costValue = cost.getCost();
					mappedCost.setCost(costValue);
					
					Code costType = cost.getCostType();
					if(costType!=null){
						String costSubType = costType.getCode();
						String costDesc = costType.getDescription();
						String costCodelstType = costType.getType();
						Code mappedCostType = CodelstsCache.getMappedCode(requestMap,"costType",costCodelstType,costSubType,costDesc);
						mappedCost.setCostType(mappedCostType);
					}
					
					Code currency = cost.getCurrency();
					if(currency!=null){
						String currencySubType = currency.getCode();
						String currencyDesc = currency.getDescription();
						String currencyCodelstType = currency.getType();
						Code mappedCurrency = CodelstsCache.getMappedCode(requestMap,"currency",currencyCodelstType,currencySubType,currencyDesc);
						mappedCost.setCurrency(mappedCurrency);
					}
				}
				mappedCostList.add(mappedCost);
			}
		}
		logger.info("Add All Mapped Costs");
		mappedCosts.getCost().addAll(mappedCostList);
    	
		return mappedCosts;	
		
	}
	
}
