package com.velos.integration.espclient;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import com.velos.services.OperationException_Exception;

@WebService(targetNamespace = "http://velos.com/services/", name = "StudyCalendarSEI")
public interface VelosEspGetStudyCalendarListEndpoint {

	 @WebResult(name = "StudyCalendarList", targetNamespace = "")
	    @RequestWrapper(localName = "getStudyCalendarList", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetStudyCalendarList")
	    @WebMethod
	    @ResponseWrapper(localName = "getStudyCalendarListResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetStudyCalendarListResponse")
	    public com.velos.services.StudyCalendarsList getStudyCalendarList(
	        @WebParam(name = "StudyIdentifier", targetNamespace = "")
	        com.velos.services.StudyIdentifier studyIdentifier
	    ) throws OperationException_Exception;
}
