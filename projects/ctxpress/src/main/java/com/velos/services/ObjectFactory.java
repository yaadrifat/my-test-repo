
package com.velos.services;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.velos.services package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetStudyCalBudgetResponse_QNAME = new QName("http://velos.com/services/", "getStudyCalBudgetResponse");
    private final static QName _OperationException_QNAME = new QName("http://velos.com/services/", "OperationException");
    private final static QName _CreateStudyCalBudgetResponse_QNAME = new QName("http://velos.com/services/", "createStudyCalBudgetResponse");
    private final static QName _GetBudgetStatusResponse_QNAME = new QName("http://velos.com/services/", "getBudgetStatusResponse");
    private final static QName _GetStudyCalBudget_QNAME = new QName("http://velos.com/services/", "getStudyCalBudget");
    private final static QName _GetBudgetStatus_QNAME = new QName("http://velos.com/services/", "getBudgetStatus");
    private final static QName _CreateStudyCalBudget_QNAME = new QName("http://velos.com/services/", "createStudyCalBudget");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.velos.services
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreateStudyCalBudget }
     * 
     */
    public CreateStudyCalBudget createCreateStudyCalBudget() {
        return new CreateStudyCalBudget();
    }

    /**
     * Create an instance of {@link GetBudgetStatus }
     * 
     */
    public GetBudgetStatus createGetBudgetStatus() {
        return new GetBudgetStatus();
    }

    /**
     * Create an instance of {@link GetStudyCalBudget }
     * 
     */
    public GetStudyCalBudget createGetStudyCalBudget() {
        return new GetStudyCalBudget();
    }

    /**
     * Create an instance of {@link OperationException }
     * 
     */
    public OperationException createOperationException() {
        return new OperationException();
    }

    /**
     * Create an instance of {@link GetStudyCalBudgetResponse }
     * 
     */
    public GetStudyCalBudgetResponse createGetStudyCalBudgetResponse() {
        return new GetStudyCalBudgetResponse();
    }

    /**
     * Create an instance of {@link GetBudgetStatusResponse }
     * 
     */
    public GetBudgetStatusResponse createGetBudgetStatusResponse() {
        return new GetBudgetStatusResponse();
    }

    /**
     * Create an instance of {@link CreateStudyCalBudgetResponse }
     * 
     */
    public CreateStudyCalBudgetResponse createCreateStudyCalBudgetResponse() {
        return new CreateStudyCalBudgetResponse();
    }

    /**
     * Create an instance of {@link BgtSectionDetail }
     * 
     */
    public BgtSectionDetail createBgtSectionDetail() {
        return new BgtSectionDetail();
    }

    /**
     * Create an instance of {@link BudgetIdentifier }
     * 
     */
    public BudgetIdentifier createBudgetIdentifier() {
        return new BudgetIdentifier();
    }

    /**
     * Create an instance of {@link StudyIdentifier }
     * 
     */
    public StudyIdentifier createStudyIdentifier() {
        return new StudyIdentifier();
    }

    /**
     * Create an instance of {@link UserIdentifier }
     * 
     */
    public UserIdentifier createUserIdentifier() {
        return new UserIdentifier();
    }

    /**
     * Create an instance of {@link Issue }
     * 
     */
    public Issue createIssue() {
        return new Issue();
    }

    /**
     * Create an instance of {@link SimpleIdentifier }
     * 
     */
    public SimpleIdentifier createSimpleIdentifier() {
        return new SimpleIdentifier();
    }

    /**
     * Create an instance of {@link VisitIdentifier }
     * 
     */
    public VisitIdentifier createVisitIdentifier() {
        return new VisitIdentifier();
    }

    /**
     * Create an instance of {@link BgtLiniItemDetail }
     * 
     */
    public BgtLiniItemDetail createBgtLiniItemDetail() {
        return new BgtLiniItemDetail();
    }

    /**
     * Create an instance of {@link ResponseHolder }
     * 
     */
    public ResponseHolder createResponseHolder() {
        return new ResponseHolder();
    }

    /**
     * Create an instance of {@link LineItemNameIdentifier }
     * 
     */
    public LineItemNameIdentifier createLineItemNameIdentifier() {
        return new LineItemNameIdentifier();
    }

    /**
     * Create an instance of {@link Results }
     * 
     */
    public Results createResults() {
        return new Results();
    }

    /**
     * Create an instance of {@link EventNameIdentfier }
     * 
     */
    public EventNameIdentfier createEventNameIdentfier() {
        return new EventNameIdentfier();
    }

    /**
     * Create an instance of {@link CalendarIdentifier }
     * 
     */
    public CalendarIdentifier createCalendarIdentifier() {
        return new CalendarIdentifier();
    }

    /**
     * Create an instance of {@link GroupIdentifier }
     * 
     */
    public GroupIdentifier createGroupIdentifier() {
        return new GroupIdentifier();
    }

    /**
     * Create an instance of {@link Code }
     * 
     */
    public Code createCode() {
        return new Code();
    }

    /**
     * Create an instance of {@link BudgetDetail }
     * 
     */
    public BudgetDetail createBudgetDetail() {
        return new BudgetDetail();
    }

    /**
     * Create an instance of {@link BudgetStatus }
     * 
     */
    public BudgetStatus createBudgetStatus() {
        return new BudgetStatus();
    }

    /**
     * Create an instance of {@link CompletedAction }
     * 
     */
    public CompletedAction createCompletedAction() {
        return new CompletedAction();
    }

    /**
     * Create an instance of {@link OrganizationIdentifier }
     * 
     */
    public OrganizationIdentifier createOrganizationIdentifier() {
        return new OrganizationIdentifier();
    }

    /**
     * Create an instance of {@link BudgetPojo }
     * 
     */
    public BudgetPojo createBudgetPojo() {
        return new BudgetPojo();
    }

    /**
     * Create an instance of {@link VisitNameIdentifier }
     * 
     */
    public VisitNameIdentifier createVisitNameIdentifier() {
        return new VisitNameIdentifier();
    }

    /**
     * Create an instance of {@link BudgetCalList }
     * 
     */
    public BudgetCalList createBudgetCalList() {
        return new BudgetCalList();
    }

    /**
     * Create an instance of {@link Issues }
     * 
     */
    public Issues createIssues() {
        return new Issues();
    }

    /**
     * Create an instance of {@link BudgetLineItemPojo }
     * 
     */
    public BudgetLineItemPojo createBudgetLineItemPojo() {
        return new BudgetLineItemPojo();
    }

    /**
     * Create an instance of {@link BgtCalPojo }
     * 
     */
    public BgtCalPojo createBgtCalPojo() {
        return new BgtCalPojo();
    }

    /**
     * Create an instance of {@link BgtTemplate }
     * 
     */
    public BgtTemplate createBgtTemplate() {
        return new BgtTemplate();
    }

    /**
     * Create an instance of {@link BudgetSectionPojo }
     * 
     */
    public BudgetSectionPojo createBudgetSectionPojo() {
        return new BudgetSectionPojo();
    }

    /**
     * Create an instance of {@link BudgetCalIdentifier }
     * 
     */
    public BudgetCalIdentifier createBudgetCalIdentifier() {
        return new BudgetCalIdentifier();
    }

    /**
     * Create an instance of {@link CalendarNameIdentifier }
     * 
     */
    public CalendarNameIdentifier createCalendarNameIdentifier() {
        return new CalendarNameIdentifier();
    }

    /**
     * Create an instance of {@link BgtSectionNameIdentifier }
     * 
     */
    public BgtSectionNameIdentifier createBgtSectionNameIdentifier() {
        return new BgtSectionNameIdentifier();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStudyCalBudgetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "getStudyCalBudgetResponse")
    public JAXBElement<GetStudyCalBudgetResponse> createGetStudyCalBudgetResponse(GetStudyCalBudgetResponse value) {
        return new JAXBElement<GetStudyCalBudgetResponse>(_GetStudyCalBudgetResponse_QNAME, GetStudyCalBudgetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OperationException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "OperationException")
    public JAXBElement<OperationException> createOperationException(OperationException value) {
        return new JAXBElement<OperationException>(_OperationException_QNAME, OperationException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateStudyCalBudgetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "createStudyCalBudgetResponse")
    public JAXBElement<CreateStudyCalBudgetResponse> createCreateStudyCalBudgetResponse(CreateStudyCalBudgetResponse value) {
        return new JAXBElement<CreateStudyCalBudgetResponse>(_CreateStudyCalBudgetResponse_QNAME, CreateStudyCalBudgetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBudgetStatusResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "getBudgetStatusResponse")
    public JAXBElement<GetBudgetStatusResponse> createGetBudgetStatusResponse(GetBudgetStatusResponse value) {
        return new JAXBElement<GetBudgetStatusResponse>(_GetBudgetStatusResponse_QNAME, GetBudgetStatusResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStudyCalBudget }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "getStudyCalBudget")
    public JAXBElement<GetStudyCalBudget> createGetStudyCalBudget(GetStudyCalBudget value) {
        return new JAXBElement<GetStudyCalBudget>(_GetStudyCalBudget_QNAME, GetStudyCalBudget.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBudgetStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "getBudgetStatus")
    public JAXBElement<GetBudgetStatus> createGetBudgetStatus(GetBudgetStatus value) {
        return new JAXBElement<GetBudgetStatus>(_GetBudgetStatus_QNAME, GetBudgetStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateStudyCalBudget }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "createStudyCalBudget")
    public JAXBElement<CreateStudyCalBudget> createCreateStudyCalBudget(CreateStudyCalBudget value) {
        return new JAXBElement<CreateStudyCalBudget>(_CreateStudyCalBudget_QNAME, CreateStudyCalBudget.class, null, value);
    }

}
