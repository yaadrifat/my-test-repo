
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for studyVersionIdentifier complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="studyVersionIdentifier">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}simpleIdentifier">
 *       &lt;sequence>
 *         &lt;element name="STUDYVER_NUMBER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "studyVersionIdentifier", propOrder = {
    "studyvernumber"
})
public class StudyVersionIdentifier
    extends SimpleIdentifier
{

    @XmlElement(name = "STUDYVER_NUMBER")
    protected String studyvernumber;

    /**
     * Gets the value of the studyvernumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTUDYVERNUMBER() {
        return studyvernumber;
    }

    /**
     * Sets the value of the studyvernumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTUDYVERNUMBER(String value) {
        this.studyvernumber = value;
    }

}
