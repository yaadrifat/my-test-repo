
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for milestoneVDAPojo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="milestoneVDAPojo">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}simpleIdentifier">
 *       &lt;sequence>
 *         &lt;element name="BUDGET_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BUDGET_SECTION_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CALENDAR_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CREATED_ON" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CREATOR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CREATOR_FK" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EVENT_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FK_ACCOUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FK_BGTCAL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FK_BGTSECTION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FK_BUDGET" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FK_EVENTASSOC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FK_LINEITEM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FK_STUDY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FK_VISIT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IP_ADD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LAST_MODIFIED_BY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LAST_MODIFIED_BY_FK" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LAST_MODIFIED_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LINEITEM_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MILEPAYFR_SUBTYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MILEPAY_SUBTYP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MILEPS_SUBTYP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MILERULE_SUBTYP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MILESTAT_SUBTYP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MILESTONE_ACHIEVEDCOUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MILESTONE_AMOUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MILESTONE_DATE_FROM" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="MILESTONE_DATE_TO" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="MILESTONE_DESCRIPTION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MILESTONE_DESC_CALCULATED" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MILESTONE_EVENTSTATUS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MILESTONE_HOLDBACK" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MILESTONE_ISACTIVE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MILESTONE_PAYBYUNIT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MILESTONE_PAYDUEBY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MILESTONE_USERSTO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MILESTONE_VISIT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MILEVISIT_SUBTYP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MSRUL_CATEGORY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MSRUL_EVENT_STAT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MSRUL_LIMIT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MSRUL_MS_RULE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MSRUL_PAY_FOR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MSRUL_PAY_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MSRUL_PROT_CAL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MSRUL_PT_COUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MSRUL_PT_STATUS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MSRUL_STATUS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MSRUL_STUDY_STATUS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MSRUL_VISIT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MS_PROT_CALASSOCTO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PK_MILESTONE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="STUDY_NUMBER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="STUDY_TITLE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "milestoneVDAPojo", propOrder = {
    "budgetname",
    "budgetsectionname",
    "calendarname",
    "createdon",
    "creator",
    "creatorfk",
    "eventname",
    "fkaccount",
    "fkbgtcal",
    "fkbgtsection",
    "fkbudget",
    "fkeventassoc",
    "fklineitem",
    "fkstudy",
    "fkvisit",
    "ipadd",
    "lastmodifiedby",
    "lastmodifiedbyfk",
    "lastmodifieddate",
    "lineitemname",
    "milepayfrsubtype",
    "milepaysubtyp",
    "milepssubtyp",
    "milerulesubtyp",
    "milestatsubtyp",
    "milestoneachievedcount",
    "milestoneamount",
    "milestonedatefrom",
    "milestonedateto",
    "milestonedescription",
    "milestonedesccalculated",
    "milestoneeventstatus",
    "milestoneholdback",
    "milestoneisactive",
    "milestonepaybyunit",
    "milestonepaydueby",
    "milestoneusersto",
    "milestonevisit",
    "milevisitsubtyp",
    "msrulcategory",
    "msruleventstat",
    "msrullimit",
    "msrulmsrule",
    "msrulpayfor",
    "msrulpaytype",
    "msrulprotcal",
    "msrulptcount",
    "msrulptstatus",
    "msrulstatus",
    "msrulstudystatus",
    "msrulvisit",
    "msprotcalassocto",
    "pkmilestone",
    "rid",
    "studynumber",
    "studytitle"
})
public class MilestoneVDAPojo
    extends SimpleIdentifier
{

    @XmlElement(name = "BUDGET_NAME")
    protected String budgetname;
    @XmlElement(name = "BUDGET_SECTION_NAME")
    protected String budgetsectionname;
    @XmlElement(name = "CALENDAR_NAME")
    protected String calendarname;
    @XmlElement(name = "CREATED_ON")
    protected String createdon;
    @XmlElement(name = "CREATOR")
    protected String creator;
    @XmlElement(name = "CREATOR_FK")
    protected String creatorfk;
    @XmlElement(name = "EVENT_NAME")
    protected String eventname;
    @XmlElement(name = "FK_ACCOUNT")
    protected String fkaccount;
    @XmlElement(name = "FK_BGTCAL")
    protected String fkbgtcal;
    @XmlElement(name = "FK_BGTSECTION")
    protected String fkbgtsection;
    @XmlElement(name = "FK_BUDGET")
    protected String fkbudget;
    @XmlElement(name = "FK_EVENTASSOC")
    protected String fkeventassoc;
    @XmlElement(name = "FK_LINEITEM")
    protected String fklineitem;
    @XmlElement(name = "FK_STUDY")
    protected String fkstudy;
    @XmlElement(name = "FK_VISIT")
    protected String fkvisit;
    @XmlElement(name = "IP_ADD")
    protected String ipadd;
    @XmlElement(name = "LAST_MODIFIED_BY")
    protected String lastmodifiedby;
    @XmlElement(name = "LAST_MODIFIED_BY_FK")
    protected String lastmodifiedbyfk;
    @XmlElement(name = "LAST_MODIFIED_DATE")
    protected String lastmodifieddate;
    @XmlElement(name = "LINEITEM_NAME")
    protected String lineitemname;
    @XmlElement(name = "MILEPAYFR_SUBTYPE")
    protected String milepayfrsubtype;
    @XmlElement(name = "MILEPAY_SUBTYP")
    protected String milepaysubtyp;
    @XmlElement(name = "MILEPS_SUBTYP")
    protected String milepssubtyp;
    @XmlElement(name = "MILERULE_SUBTYP")
    protected String milerulesubtyp;
    @XmlElement(name = "MILESTAT_SUBTYP")
    protected String milestatsubtyp;
    @XmlElement(name = "MILESTONE_ACHIEVEDCOUNT")
    protected String milestoneachievedcount;
    @XmlElement(name = "MILESTONE_AMOUNT")
    protected String milestoneamount;
    @XmlElement(name = "MILESTONE_DATE_FROM")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar milestonedatefrom;
    @XmlElement(name = "MILESTONE_DATE_TO")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar milestonedateto;
    @XmlElement(name = "MILESTONE_DESCRIPTION")
    protected String milestonedescription;
    @XmlElement(name = "MILESTONE_DESC_CALCULATED")
    protected String milestonedesccalculated;
    @XmlElement(name = "MILESTONE_EVENTSTATUS")
    protected String milestoneeventstatus;
    @XmlElement(name = "MILESTONE_HOLDBACK")
    protected String milestoneholdback;
    @XmlElement(name = "MILESTONE_ISACTIVE")
    protected String milestoneisactive;
    @XmlElement(name = "MILESTONE_PAYBYUNIT")
    protected String milestonepaybyunit;
    @XmlElement(name = "MILESTONE_PAYDUEBY")
    protected String milestonepaydueby;
    @XmlElement(name = "MILESTONE_USERSTO")
    protected String milestoneusersto;
    @XmlElement(name = "MILESTONE_VISIT")
    protected String milestonevisit;
    @XmlElement(name = "MILEVISIT_SUBTYP")
    protected String milevisitsubtyp;
    @XmlElement(name = "MSRUL_CATEGORY")
    protected String msrulcategory;
    @XmlElement(name = "MSRUL_EVENT_STAT")
    protected String msruleventstat;
    @XmlElement(name = "MSRUL_LIMIT")
    protected String msrullimit;
    @XmlElement(name = "MSRUL_MS_RULE")
    protected String msrulmsrule;
    @XmlElement(name = "MSRUL_PAY_FOR")
    protected String msrulpayfor;
    @XmlElement(name = "MSRUL_PAY_TYPE")
    protected String msrulpaytype;
    @XmlElement(name = "MSRUL_PROT_CAL")
    protected String msrulprotcal;
    @XmlElement(name = "MSRUL_PT_COUNT")
    protected String msrulptcount;
    @XmlElement(name = "MSRUL_PT_STATUS")
    protected String msrulptstatus;
    @XmlElement(name = "MSRUL_STATUS")
    protected String msrulstatus;
    @XmlElement(name = "MSRUL_STUDY_STATUS")
    protected String msrulstudystatus;
    @XmlElement(name = "MSRUL_VISIT")
    protected String msrulvisit;
    @XmlElement(name = "MS_PROT_CALASSOCTO")
    protected String msprotcalassocto;
    @XmlElement(name = "PK_MILESTONE")
    protected String pkmilestone;
    @XmlElement(name = "RID")
    protected String rid;
    @XmlElement(name = "STUDY_NUMBER")
    protected String studynumber;
    @XmlElement(name = "STUDY_TITLE")
    protected String studytitle;

    /**
     * Gets the value of the budgetname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBUDGETNAME() {
        return budgetname;
    }

    /**
     * Sets the value of the budgetname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBUDGETNAME(String value) {
        this.budgetname = value;
    }

    /**
     * Gets the value of the budgetsectionname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBUDGETSECTIONNAME() {
        return budgetsectionname;
    }

    /**
     * Sets the value of the budgetsectionname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBUDGETSECTIONNAME(String value) {
        this.budgetsectionname = value;
    }

    /**
     * Gets the value of the calendarname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCALENDARNAME() {
        return calendarname;
    }

    /**
     * Sets the value of the calendarname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCALENDARNAME(String value) {
        this.calendarname = value;
    }

    /**
     * Gets the value of the createdon property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCREATEDON() {
        return createdon;
    }

    /**
     * Sets the value of the createdon property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCREATEDON(String value) {
        this.createdon = value;
    }

    /**
     * Gets the value of the creator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCREATOR() {
        return creator;
    }

    /**
     * Sets the value of the creator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCREATOR(String value) {
        this.creator = value;
    }

    /**
     * Gets the value of the creatorfk property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCREATORFK() {
        return creatorfk;
    }

    /**
     * Sets the value of the creatorfk property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCREATORFK(String value) {
        this.creatorfk = value;
    }

    /**
     * Gets the value of the eventname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEVENTNAME() {
        return eventname;
    }

    /**
     * Sets the value of the eventname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEVENTNAME(String value) {
        this.eventname = value;
    }

    /**
     * Gets the value of the fkaccount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFKACCOUNT() {
        return fkaccount;
    }

    /**
     * Sets the value of the fkaccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFKACCOUNT(String value) {
        this.fkaccount = value;
    }

    /**
     * Gets the value of the fkbgtcal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFKBGTCAL() {
        return fkbgtcal;
    }

    /**
     * Sets the value of the fkbgtcal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFKBGTCAL(String value) {
        this.fkbgtcal = value;
    }

    /**
     * Gets the value of the fkbgtsection property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFKBGTSECTION() {
        return fkbgtsection;
    }

    /**
     * Sets the value of the fkbgtsection property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFKBGTSECTION(String value) {
        this.fkbgtsection = value;
    }

    /**
     * Gets the value of the fkbudget property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFKBUDGET() {
        return fkbudget;
    }

    /**
     * Sets the value of the fkbudget property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFKBUDGET(String value) {
        this.fkbudget = value;
    }

    /**
     * Gets the value of the fkeventassoc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFKEVENTASSOC() {
        return fkeventassoc;
    }

    /**
     * Sets the value of the fkeventassoc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFKEVENTASSOC(String value) {
        this.fkeventassoc = value;
    }

    /**
     * Gets the value of the fklineitem property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFKLINEITEM() {
        return fklineitem;
    }

    /**
     * Sets the value of the fklineitem property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFKLINEITEM(String value) {
        this.fklineitem = value;
    }

    /**
     * Gets the value of the fkstudy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFKSTUDY() {
        return fkstudy;
    }

    /**
     * Sets the value of the fkstudy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFKSTUDY(String value) {
        this.fkstudy = value;
    }

    /**
     * Gets the value of the fkvisit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFKVISIT() {
        return fkvisit;
    }

    /**
     * Sets the value of the fkvisit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFKVISIT(String value) {
        this.fkvisit = value;
    }

    /**
     * Gets the value of the ipadd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIPADD() {
        return ipadd;
    }

    /**
     * Sets the value of the ipadd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIPADD(String value) {
        this.ipadd = value;
    }

    /**
     * Gets the value of the lastmodifiedby property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLASTMODIFIEDBY() {
        return lastmodifiedby;
    }

    /**
     * Sets the value of the lastmodifiedby property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLASTMODIFIEDBY(String value) {
        this.lastmodifiedby = value;
    }

    /**
     * Gets the value of the lastmodifiedbyfk property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLASTMODIFIEDBYFK() {
        return lastmodifiedbyfk;
    }

    /**
     * Sets the value of the lastmodifiedbyfk property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLASTMODIFIEDBYFK(String value) {
        this.lastmodifiedbyfk = value;
    }

    /**
     * Gets the value of the lastmodifieddate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLASTMODIFIEDDATE() {
        return lastmodifieddate;
    }

    /**
     * Sets the value of the lastmodifieddate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLASTMODIFIEDDATE(String value) {
        this.lastmodifieddate = value;
    }

    /**
     * Gets the value of the lineitemname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLINEITEMNAME() {
        return lineitemname;
    }

    /**
     * Sets the value of the lineitemname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLINEITEMNAME(String value) {
        this.lineitemname = value;
    }

    /**
     * Gets the value of the milepayfrsubtype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMILEPAYFRSUBTYPE() {
        return milepayfrsubtype;
    }

    /**
     * Sets the value of the milepayfrsubtype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMILEPAYFRSUBTYPE(String value) {
        this.milepayfrsubtype = value;
    }

    /**
     * Gets the value of the milepaysubtyp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMILEPAYSUBTYP() {
        return milepaysubtyp;
    }

    /**
     * Sets the value of the milepaysubtyp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMILEPAYSUBTYP(String value) {
        this.milepaysubtyp = value;
    }

    /**
     * Gets the value of the milepssubtyp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMILEPSSUBTYP() {
        return milepssubtyp;
    }

    /**
     * Sets the value of the milepssubtyp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMILEPSSUBTYP(String value) {
        this.milepssubtyp = value;
    }

    /**
     * Gets the value of the milerulesubtyp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMILERULESUBTYP() {
        return milerulesubtyp;
    }

    /**
     * Sets the value of the milerulesubtyp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMILERULESUBTYP(String value) {
        this.milerulesubtyp = value;
    }

    /**
     * Gets the value of the milestatsubtyp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMILESTATSUBTYP() {
        return milestatsubtyp;
    }

    /**
     * Sets the value of the milestatsubtyp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMILESTATSUBTYP(String value) {
        this.milestatsubtyp = value;
    }

    /**
     * Gets the value of the milestoneachievedcount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMILESTONEACHIEVEDCOUNT() {
        return milestoneachievedcount;
    }

    /**
     * Sets the value of the milestoneachievedcount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMILESTONEACHIEVEDCOUNT(String value) {
        this.milestoneachievedcount = value;
    }

    /**
     * Gets the value of the milestoneamount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMILESTONEAMOUNT() {
        return milestoneamount;
    }

    /**
     * Sets the value of the milestoneamount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMILESTONEAMOUNT(String value) {
        this.milestoneamount = value;
    }

    /**
     * Gets the value of the milestonedatefrom property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getMILESTONEDATEFROM() {
        return milestonedatefrom;
    }

    /**
     * Sets the value of the milestonedatefrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMILESTONEDATEFROM(XMLGregorianCalendar value) {
        this.milestonedatefrom = value;
    }

    /**
     * Gets the value of the milestonedateto property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getMILESTONEDATETO() {
        return milestonedateto;
    }

    /**
     * Sets the value of the milestonedateto property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMILESTONEDATETO(XMLGregorianCalendar value) {
        this.milestonedateto = value;
    }

    /**
     * Gets the value of the milestonedescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMILESTONEDESCRIPTION() {
        return milestonedescription;
    }

    /**
     * Sets the value of the milestonedescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMILESTONEDESCRIPTION(String value) {
        this.milestonedescription = value;
    }

    /**
     * Gets the value of the milestonedesccalculated property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMILESTONEDESCCALCULATED() {
        return milestonedesccalculated;
    }

    /**
     * Sets the value of the milestonedesccalculated property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMILESTONEDESCCALCULATED(String value) {
        this.milestonedesccalculated = value;
    }

    /**
     * Gets the value of the milestoneeventstatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMILESTONEEVENTSTATUS() {
        return milestoneeventstatus;
    }

    /**
     * Sets the value of the milestoneeventstatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMILESTONEEVENTSTATUS(String value) {
        this.milestoneeventstatus = value;
    }

    /**
     * Gets the value of the milestoneholdback property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMILESTONEHOLDBACK() {
        return milestoneholdback;
    }

    /**
     * Sets the value of the milestoneholdback property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMILESTONEHOLDBACK(String value) {
        this.milestoneholdback = value;
    }

    /**
     * Gets the value of the milestoneisactive property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMILESTONEISACTIVE() {
        return milestoneisactive;
    }

    /**
     * Sets the value of the milestoneisactive property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMILESTONEISACTIVE(String value) {
        this.milestoneisactive = value;
    }

    /**
     * Gets the value of the milestonepaybyunit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMILESTONEPAYBYUNIT() {
        return milestonepaybyunit;
    }

    /**
     * Sets the value of the milestonepaybyunit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMILESTONEPAYBYUNIT(String value) {
        this.milestonepaybyunit = value;
    }

    /**
     * Gets the value of the milestonepaydueby property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMILESTONEPAYDUEBY() {
        return milestonepaydueby;
    }

    /**
     * Sets the value of the milestonepaydueby property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMILESTONEPAYDUEBY(String value) {
        this.milestonepaydueby = value;
    }

    /**
     * Gets the value of the milestoneusersto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMILESTONEUSERSTO() {
        return milestoneusersto;
    }

    /**
     * Sets the value of the milestoneusersto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMILESTONEUSERSTO(String value) {
        this.milestoneusersto = value;
    }

    /**
     * Gets the value of the milestonevisit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMILESTONEVISIT() {
        return milestonevisit;
    }

    /**
     * Sets the value of the milestonevisit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMILESTONEVISIT(String value) {
        this.milestonevisit = value;
    }

    /**
     * Gets the value of the milevisitsubtyp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMILEVISITSUBTYP() {
        return milevisitsubtyp;
    }

    /**
     * Sets the value of the milevisitsubtyp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMILEVISITSUBTYP(String value) {
        this.milevisitsubtyp = value;
    }

    /**
     * Gets the value of the msrulcategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMSRULCATEGORY() {
        return msrulcategory;
    }

    /**
     * Sets the value of the msrulcategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMSRULCATEGORY(String value) {
        this.msrulcategory = value;
    }

    /**
     * Gets the value of the msruleventstat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMSRULEVENTSTAT() {
        return msruleventstat;
    }

    /**
     * Sets the value of the msruleventstat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMSRULEVENTSTAT(String value) {
        this.msruleventstat = value;
    }

    /**
     * Gets the value of the msrullimit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMSRULLIMIT() {
        return msrullimit;
    }

    /**
     * Sets the value of the msrullimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMSRULLIMIT(String value) {
        this.msrullimit = value;
    }

    /**
     * Gets the value of the msrulmsrule property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMSRULMSRULE() {
        return msrulmsrule;
    }

    /**
     * Sets the value of the msrulmsrule property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMSRULMSRULE(String value) {
        this.msrulmsrule = value;
    }

    /**
     * Gets the value of the msrulpayfor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMSRULPAYFOR() {
        return msrulpayfor;
    }

    /**
     * Sets the value of the msrulpayfor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMSRULPAYFOR(String value) {
        this.msrulpayfor = value;
    }

    /**
     * Gets the value of the msrulpaytype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMSRULPAYTYPE() {
        return msrulpaytype;
    }

    /**
     * Sets the value of the msrulpaytype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMSRULPAYTYPE(String value) {
        this.msrulpaytype = value;
    }

    /**
     * Gets the value of the msrulprotcal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMSRULPROTCAL() {
        return msrulprotcal;
    }

    /**
     * Sets the value of the msrulprotcal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMSRULPROTCAL(String value) {
        this.msrulprotcal = value;
    }

    /**
     * Gets the value of the msrulptcount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMSRULPTCOUNT() {
        return msrulptcount;
    }

    /**
     * Sets the value of the msrulptcount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMSRULPTCOUNT(String value) {
        this.msrulptcount = value;
    }

    /**
     * Gets the value of the msrulptstatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMSRULPTSTATUS() {
        return msrulptstatus;
    }

    /**
     * Sets the value of the msrulptstatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMSRULPTSTATUS(String value) {
        this.msrulptstatus = value;
    }

    /**
     * Gets the value of the msrulstatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMSRULSTATUS() {
        return msrulstatus;
    }

    /**
     * Sets the value of the msrulstatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMSRULSTATUS(String value) {
        this.msrulstatus = value;
    }

    /**
     * Gets the value of the msrulstudystatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMSRULSTUDYSTATUS() {
        return msrulstudystatus;
    }

    /**
     * Sets the value of the msrulstudystatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMSRULSTUDYSTATUS(String value) {
        this.msrulstudystatus = value;
    }

    /**
     * Gets the value of the msrulvisit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMSRULVISIT() {
        return msrulvisit;
    }

    /**
     * Sets the value of the msrulvisit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMSRULVISIT(String value) {
        this.msrulvisit = value;
    }

    /**
     * Gets the value of the msprotcalassocto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMSPROTCALASSOCTO() {
        return msprotcalassocto;
    }

    /**
     * Sets the value of the msprotcalassocto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMSPROTCALASSOCTO(String value) {
        this.msprotcalassocto = value;
    }

    /**
     * Gets the value of the pkmilestone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPKMILESTONE() {
        return pkmilestone;
    }

    /**
     * Sets the value of the pkmilestone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPKMILESTONE(String value) {
        this.pkmilestone = value;
    }

    /**
     * Gets the value of the rid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRID() {
        return rid;
    }

    /**
     * Sets the value of the rid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRID(String value) {
        this.rid = value;
    }

    /**
     * Gets the value of the studynumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTUDYNUMBER() {
        return studynumber;
    }

    /**
     * Sets the value of the studynumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTUDYNUMBER(String value) {
        this.studynumber = value;
    }

    /**
     * Gets the value of the studytitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTUDYTITLE() {
        return studytitle;
    }

    /**
     * Sets the value of the studytitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTUDYTITLE(String value) {
        this.studytitle = value;
    }

}
