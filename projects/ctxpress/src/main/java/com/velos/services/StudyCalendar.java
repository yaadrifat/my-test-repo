
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for studyCalendar complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="studyCalendar">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="calendarIdentifier" type="{http://velos.com/services/}calendarIdentifier" minOccurs="0"/>
 *         &lt;element name="calendarSummary" type="{http://velos.com/services/}calendarSummary" minOccurs="0"/>
 *         &lt;element name="events" type="{http://velos.com/services/}calendarEvents" minOccurs="0"/>
 *         &lt;element name="studyIdentifier" type="{http://velos.com/services/}studyIdentifier" minOccurs="0"/>
 *         &lt;element name="visits" type="{http://velos.com/services/}calendarVisits" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "studyCalendar", propOrder = {
    "calendarIdentifier",
    "calendarSummary",
    "events",
    "studyIdentifier",
    "visits"
})
public class StudyCalendar
    extends ServiceObject
{

    protected CalendarIdentifier calendarIdentifier;
    protected CalendarSummary calendarSummary;
    protected CalendarEvents events;
    protected StudyIdentifier studyIdentifier;
    protected CalendarVisits visits;

    /**
     * Gets the value of the calendarIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link CalendarIdentifier }
     *     
     */
    public CalendarIdentifier getCalendarIdentifier() {
        return calendarIdentifier;
    }

    /**
     * Sets the value of the calendarIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link CalendarIdentifier }
     *     
     */
    public void setCalendarIdentifier(CalendarIdentifier value) {
        this.calendarIdentifier = value;
    }

    /**
     * Gets the value of the calendarSummary property.
     * 
     * @return
     *     possible object is
     *     {@link CalendarSummary }
     *     
     */
    public CalendarSummary getCalendarSummary() {
        return calendarSummary;
    }

    /**
     * Sets the value of the calendarSummary property.
     * 
     * @param value
     *     allowed object is
     *     {@link CalendarSummary }
     *     
     */
    public void setCalendarSummary(CalendarSummary value) {
        this.calendarSummary = value;
    }

    /**
     * Gets the value of the events property.
     * 
     * @return
     *     possible object is
     *     {@link CalendarEvents }
     *     
     */
    public CalendarEvents getEvents() {
        return events;
    }

    /**
     * Sets the value of the events property.
     * 
     * @param value
     *     allowed object is
     *     {@link CalendarEvents }
     *     
     */
    public void setEvents(CalendarEvents value) {
        this.events = value;
    }

    /**
     * Gets the value of the studyIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link StudyIdentifier }
     *     
     */
    public StudyIdentifier getStudyIdentifier() {
        return studyIdentifier;
    }

    /**
     * Sets the value of the studyIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyIdentifier }
     *     
     */
    public void setStudyIdentifier(StudyIdentifier value) {
        this.studyIdentifier = value;
    }

    /**
     * Gets the value of the visits property.
     * 
     * @return
     *     possible object is
     *     {@link CalendarVisits }
     *     
     */
    public CalendarVisits getVisits() {
        return visits;
    }

    /**
     * Sets the value of the visits property.
     * 
     * @param value
     *     allowed object is
     *     {@link CalendarVisits }
     *     
     */
    public void setVisits(CalendarVisits value) {
        this.visits = value;
    }

}
