
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for searchStudy complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="searchStudy">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StudySearch" type="{http://velos.com/services/}studySearch" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "searchStudy", propOrder = {
    "studySearch"
})
public class SearchStudy {

    @XmlElement(name = "StudySearch")
    protected StudySearch studySearch;

    /**
     * Gets the value of the studySearch property.
     * 
     * @return
     *     possible object is
     *     {@link StudySearch }
     *     
     */
    public StudySearch getStudySearch() {
        return studySearch;
    }

    /**
     * Sets the value of the studySearch property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudySearch }
     *     
     */
    public void setStudySearch(StudySearch value) {
        this.studySearch = value;
    }

}
