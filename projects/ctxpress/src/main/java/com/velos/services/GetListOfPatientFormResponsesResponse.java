
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getListOfPatientFormResponsesResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getListOfPatientFormResponsesResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PatientFormResponse" type="{http://velos.com/services/}patientFormResponses" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getListOfPatientFormResponsesResponse", propOrder = {
    "patientFormResponse"
})
public class GetListOfPatientFormResponsesResponse {

    @XmlElement(name = "PatientFormResponse")
    protected PatientFormResponses patientFormResponse;

    /**
     * Gets the value of the patientFormResponse property.
     * 
     * @return
     *     possible object is
     *     {@link PatientFormResponses }
     *     
     */
    public PatientFormResponses getPatientFormResponse() {
        return patientFormResponse;
    }

    /**
     * Sets the value of the patientFormResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientFormResponses }
     *     
     */
    public void setPatientFormResponse(PatientFormResponses value) {
        this.patientFormResponse = value;
    }

}
