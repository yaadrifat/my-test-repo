
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for textFieldValidations complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="textFieldValidations">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}fieldValidations">
 *       &lt;sequence>
 *         &lt;element name="defaultResponse" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="editBoxLines" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="fieldLength" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="maxCharAllowed" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "textFieldValidations", propOrder = {
    "defaultResponse",
    "editBoxLines",
    "fieldLength",
    "maxCharAllowed"
})
@XmlSeeAlso({
    NumberFieldValidations.class
})
public class TextFieldValidations
    extends FieldValidations
{

    protected String defaultResponse;
    protected Integer editBoxLines;
    protected Integer fieldLength;
    protected Integer maxCharAllowed;

    /**
     * Gets the value of the defaultResponse property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultResponse() {
        return defaultResponse;
    }

    /**
     * Sets the value of the defaultResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultResponse(String value) {
        this.defaultResponse = value;
    }

    /**
     * Gets the value of the editBoxLines property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getEditBoxLines() {
        return editBoxLines;
    }

    /**
     * Sets the value of the editBoxLines property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setEditBoxLines(Integer value) {
        this.editBoxLines = value;
    }

    /**
     * Gets the value of the fieldLength property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFieldLength() {
        return fieldLength;
    }

    /**
     * Sets the value of the fieldLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFieldLength(Integer value) {
        this.fieldLength = value;
    }

    /**
     * Gets the value of the maxCharAllowed property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxCharAllowed() {
        return maxCharAllowed;
    }

    /**
     * Sets the value of the maxCharAllowed property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxCharAllowed(Integer value) {
        this.maxCharAllowed = value;
    }

}
