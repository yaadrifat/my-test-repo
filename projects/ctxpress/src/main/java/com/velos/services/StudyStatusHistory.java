
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for studyStatusHistory complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="studyStatusHistory">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PK_CODELST_STAT" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="RECORD_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="STATUS_CUSTOM1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="STATUS_DATE" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="STATUS_END_DATE" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="STATUS_ISCURRENT" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="STATUS_NOTES" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="studyVerStatIdent" type="{http://velos.com/services/}studyVerStatusIdentifier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "studyStatusHistory", propOrder = {
    "pkcodelststat",
    "recordtype",
    "statuscustom1",
    "statusdate",
    "statusenddate",
    "statusiscurrent",
    "statusnotes",
    "studyVerStatIdent"
})
public class StudyStatusHistory {

    @XmlElement(name = "PK_CODELST_STAT")
    protected Code pkcodelststat;
    @XmlElement(name = "RECORD_TYPE")
    protected String recordtype;
    @XmlElement(name = "STATUS_CUSTOM1")
    protected String statuscustom1;
    @XmlElement(name = "STATUS_DATE")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar statusdate;
    @XmlElement(name = "STATUS_END_DATE")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar statusenddate;
    @XmlElement(name = "STATUS_ISCURRENT")
    protected Integer statusiscurrent;
    @XmlElement(name = "STATUS_NOTES")
    protected String statusnotes;
    protected StudyVerStatusIdentifier studyVerStatIdent;

    /**
     * Gets the value of the pkcodelststat property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getPKCODELSTSTAT() {
        return pkcodelststat;
    }

    /**
     * Sets the value of the pkcodelststat property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setPKCODELSTSTAT(Code value) {
        this.pkcodelststat = value;
    }

    /**
     * Gets the value of the recordtype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRECORDTYPE() {
        return recordtype;
    }

    /**
     * Sets the value of the recordtype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRECORDTYPE(String value) {
        this.recordtype = value;
    }

    /**
     * Gets the value of the statuscustom1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTATUSCUSTOM1() {
        return statuscustom1;
    }

    /**
     * Sets the value of the statuscustom1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTATUSCUSTOM1(String value) {
        this.statuscustom1 = value;
    }

    /**
     * Gets the value of the statusdate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSTATUSDATE() {
        return statusdate;
    }

    /**
     * Sets the value of the statusdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSTATUSDATE(XMLGregorianCalendar value) {
        this.statusdate = value;
    }

    /**
     * Gets the value of the statusenddate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSTATUSENDDATE() {
        return statusenddate;
    }

    /**
     * Sets the value of the statusenddate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSTATUSENDDATE(XMLGregorianCalendar value) {
        this.statusenddate = value;
    }

    /**
     * Gets the value of the statusiscurrent property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSTATUSISCURRENT() {
        return statusiscurrent;
    }

    /**
     * Sets the value of the statusiscurrent property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSTATUSISCURRENT(Integer value) {
        this.statusiscurrent = value;
    }

    /**
     * Gets the value of the statusnotes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTATUSNOTES() {
        return statusnotes;
    }

    /**
     * Sets the value of the statusnotes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTATUSNOTES(String value) {
        this.statusnotes = value;
    }

    /**
     * Gets the value of the studyVerStatIdent property.
     * 
     * @return
     *     possible object is
     *     {@link StudyVerStatusIdentifier }
     *     
     */
    public StudyVerStatusIdentifier getStudyVerStatIdent() {
        return studyVerStatIdent;
    }

    /**
     * Sets the value of the studyVerStatIdent property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyVerStatusIdentifier }
     *     
     */
    public void setStudyVerStatIdent(StudyVerStatusIdentifier value) {
        this.studyVerStatIdent = value;
    }

}
