
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getStudyCalendarListResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getStudyCalendarListResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StudyCalendarList" type="{http://velos.com/services/}studyCalendarsList" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getStudyCalendarListResponse", propOrder = {
    "studyCalendarList"
})
public class GetStudyCalendarListResponse {

    @XmlElement(name = "StudyCalendarList")
    protected StudyCalendarsList studyCalendarList;

    /**
     * Gets the value of the studyCalendarList property.
     * 
     * @return
     *     possible object is
     *     {@link StudyCalendarsList }
     *     
     */
    public StudyCalendarsList getStudyCalendarList() {
        return studyCalendarList;
    }

    /**
     * Sets the value of the studyCalendarList property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyCalendarsList }
     *     
     */
    public void setStudyCalendarList(StudyCalendarsList value) {
        this.studyCalendarList = value;
    }

}
