
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for bgtCalPojo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="bgtCalPojo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BGTCAL_CLINICFLAG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BGTCAL_CLINICOHEAD" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="BGTCAL_DELFLAG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BGTCAL_DISCOUNT" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="BGTCAL_DISCOUNTFLAG" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="BGTCAL_EXCLDSOCFLAG" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="BGTCAL_FRGBENEFIT" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="BGTCAL_FRGFLAG" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="BGTCAL_INDIRECTCOST" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="BGTCAL_PROTTYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BGTCAL_SPONSORFLAG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BGTCAL_SPONSOROHEAD" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="BGTCAL_SP_OVERHEAD" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="BGTCAL_SP_OVERHEAD_FLAG" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="budgetCalIdent" type="{http://velos.com/services/}budgetCalIdentifier" minOccurs="0"/>
 *         &lt;element name="budgetSections" type="{http://velos.com/services/}bgtSectionDetail" minOccurs="0"/>
 *         &lt;element name="calNameIdent" type="{http://velos.com/services/}calendarNameIdentifier" minOccurs="0"/>
 *         &lt;element name="GRAND_DISC_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GRAND_DISC_TOTALCOST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GRAND_FRINGE_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GRAND_FRINGE_TOTALCOST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GRAND_IND_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GRAND_IND_TOTALCOST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GRAND_RES_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GRAND_RES_SPONSOR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GRAND_RES_TOTALCOST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GRAND_RES_VARIANCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GRAND_RES_VIARIANCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GRAND_SALARY_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GRAND_SALARY_TOTALCOST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GRAND_SOC_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GRAND_SOC_SPONSOR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GRAND_SOC_TOTALCOST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GRAND_SOC_VARIANCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GRAND_TOTAL_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GRAND_TOTAL_SPONSOR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GRAND_TOTAL_TOTALCOST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GRAND_TOTAL_VARIANCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "bgtCalPojo", propOrder = {
    "bgtcalclinicflag",
    "bgtcalclinicohead",
    "bgtcaldelflag",
    "bgtcaldiscount",
    "bgtcaldiscountflag",
    "bgtcalexcldsocflag",
    "bgtcalfrgbenefit",
    "bgtcalfrgflag",
    "bgtcalindirectcost",
    "bgtcalprottype",
    "bgtcalsponsorflag",
    "bgtcalsponsorohead",
    "bgtcalspoverhead",
    "bgtcalspoverheadflag",
    "budgetCalIdent",
    "budgetSections",
    "calNameIdent",
    "granddisccost",
    "granddisctotalcost",
    "grandfringecost",
    "grandfringetotalcost",
    "grandindcost",
    "grandindtotalcost",
    "grandrescost",
    "grandressponsor",
    "grandrestotalcost",
    "grandresvariance",
    "grandresviariance",
    "grandsalarycost",
    "grandsalarytotalcost",
    "grandsoccost",
    "grandsocsponsor",
    "grandsoctotalcost",
    "grandsocvariance",
    "grandtotalcost",
    "grandtotalsponsor",
    "grandtotaltotalcost",
    "grandtotalvariance"
})
public class BgtCalPojo {

    @XmlElement(name = "BGTCAL_CLINICFLAG")
    protected String bgtcalclinicflag;
    @XmlElement(name = "BGTCAL_CLINICOHEAD")
    protected Double bgtcalclinicohead;
    @XmlElement(name = "BGTCAL_DELFLAG")
    protected String bgtcaldelflag;
    @XmlElement(name = "BGTCAL_DISCOUNT")
    protected Double bgtcaldiscount;
    @XmlElement(name = "BGTCAL_DISCOUNTFLAG")
    protected Integer bgtcaldiscountflag;
    @XmlElement(name = "BGTCAL_EXCLDSOCFLAG")
    protected Integer bgtcalexcldsocflag;
    @XmlElement(name = "BGTCAL_FRGBENEFIT")
    protected Double bgtcalfrgbenefit;
    @XmlElement(name = "BGTCAL_FRGFLAG")
    protected Integer bgtcalfrgflag;
    @XmlElement(name = "BGTCAL_INDIRECTCOST")
    protected Double bgtcalindirectcost;
    @XmlElement(name = "BGTCAL_PROTTYPE")
    protected String bgtcalprottype;
    @XmlElement(name = "BGTCAL_SPONSORFLAG")
    protected String bgtcalsponsorflag;
    @XmlElement(name = "BGTCAL_SPONSOROHEAD")
    protected Double bgtcalsponsorohead;
    @XmlElement(name = "BGTCAL_SP_OVERHEAD")
    protected Double bgtcalspoverhead;
    @XmlElement(name = "BGTCAL_SP_OVERHEAD_FLAG")
    protected Integer bgtcalspoverheadflag;
    protected BudgetCalIdentifier budgetCalIdent;
    protected BgtSectionDetail budgetSections;
    protected CalendarNameIdentifier calNameIdent;
    @XmlElement(name = "GRAND_DISC_COST")
    protected String granddisccost;
    @XmlElement(name = "GRAND_DISC_TOTALCOST")
    protected String granddisctotalcost;
    @XmlElement(name = "GRAND_FRINGE_COST")
    protected String grandfringecost;
    @XmlElement(name = "GRAND_FRINGE_TOTALCOST")
    protected String grandfringetotalcost;
    @XmlElement(name = "GRAND_IND_COST")
    protected String grandindcost;
    @XmlElement(name = "GRAND_IND_TOTALCOST")
    protected String grandindtotalcost;
    @XmlElement(name = "GRAND_RES_COST")
    protected String grandrescost;
    @XmlElement(name = "GRAND_RES_SPONSOR")
    protected String grandressponsor;
    @XmlElement(name = "GRAND_RES_TOTALCOST")
    protected String grandrestotalcost;
    @XmlElement(name = "GRAND_RES_VARIANCE")
    protected String grandresvariance;
    @XmlElement(name = "GRAND_RES_VIARIANCE")
    protected String grandresviariance;
    @XmlElement(name = "GRAND_SALARY_COST")
    protected String grandsalarycost;
    @XmlElement(name = "GRAND_SALARY_TOTALCOST")
    protected String grandsalarytotalcost;
    @XmlElement(name = "GRAND_SOC_COST")
    protected String grandsoccost;
    @XmlElement(name = "GRAND_SOC_SPONSOR")
    protected String grandsocsponsor;
    @XmlElement(name = "GRAND_SOC_TOTALCOST")
    protected String grandsoctotalcost;
    @XmlElement(name = "GRAND_SOC_VARIANCE")
    protected String grandsocvariance;
    @XmlElement(name = "GRAND_TOTAL_COST")
    protected String grandtotalcost;
    @XmlElement(name = "GRAND_TOTAL_SPONSOR")
    protected String grandtotalsponsor;
    @XmlElement(name = "GRAND_TOTAL_TOTALCOST")
    protected String grandtotaltotalcost;
    @XmlElement(name = "GRAND_TOTAL_VARIANCE")
    protected String grandtotalvariance;

    /**
     * Gets the value of the bgtcalclinicflag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBGTCALCLINICFLAG() {
        return bgtcalclinicflag;
    }

    /**
     * Sets the value of the bgtcalclinicflag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBGTCALCLINICFLAG(String value) {
        this.bgtcalclinicflag = value;
    }

    /**
     * Gets the value of the bgtcalclinicohead property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getBGTCALCLINICOHEAD() {
        return bgtcalclinicohead;
    }

    /**
     * Sets the value of the bgtcalclinicohead property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setBGTCALCLINICOHEAD(Double value) {
        this.bgtcalclinicohead = value;
    }

    /**
     * Gets the value of the bgtcaldelflag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBGTCALDELFLAG() {
        return bgtcaldelflag;
    }

    /**
     * Sets the value of the bgtcaldelflag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBGTCALDELFLAG(String value) {
        this.bgtcaldelflag = value;
    }

    /**
     * Gets the value of the bgtcaldiscount property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getBGTCALDISCOUNT() {
        return bgtcaldiscount;
    }

    /**
     * Sets the value of the bgtcaldiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setBGTCALDISCOUNT(Double value) {
        this.bgtcaldiscount = value;
    }

    /**
     * Gets the value of the bgtcaldiscountflag property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBGTCALDISCOUNTFLAG() {
        return bgtcaldiscountflag;
    }

    /**
     * Sets the value of the bgtcaldiscountflag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBGTCALDISCOUNTFLAG(Integer value) {
        this.bgtcaldiscountflag = value;
    }

    /**
     * Gets the value of the bgtcalexcldsocflag property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBGTCALEXCLDSOCFLAG() {
        return bgtcalexcldsocflag;
    }

    /**
     * Sets the value of the bgtcalexcldsocflag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBGTCALEXCLDSOCFLAG(Integer value) {
        this.bgtcalexcldsocflag = value;
    }

    /**
     * Gets the value of the bgtcalfrgbenefit property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getBGTCALFRGBENEFIT() {
        return bgtcalfrgbenefit;
    }

    /**
     * Sets the value of the bgtcalfrgbenefit property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setBGTCALFRGBENEFIT(Double value) {
        this.bgtcalfrgbenefit = value;
    }

    /**
     * Gets the value of the bgtcalfrgflag property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBGTCALFRGFLAG() {
        return bgtcalfrgflag;
    }

    /**
     * Sets the value of the bgtcalfrgflag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBGTCALFRGFLAG(Integer value) {
        this.bgtcalfrgflag = value;
    }

    /**
     * Gets the value of the bgtcalindirectcost property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getBGTCALINDIRECTCOST() {
        return bgtcalindirectcost;
    }

    /**
     * Sets the value of the bgtcalindirectcost property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setBGTCALINDIRECTCOST(Double value) {
        this.bgtcalindirectcost = value;
    }

    /**
     * Gets the value of the bgtcalprottype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBGTCALPROTTYPE() {
        return bgtcalprottype;
    }

    /**
     * Sets the value of the bgtcalprottype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBGTCALPROTTYPE(String value) {
        this.bgtcalprottype = value;
    }

    /**
     * Gets the value of the bgtcalsponsorflag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBGTCALSPONSORFLAG() {
        return bgtcalsponsorflag;
    }

    /**
     * Sets the value of the bgtcalsponsorflag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBGTCALSPONSORFLAG(String value) {
        this.bgtcalsponsorflag = value;
    }

    /**
     * Gets the value of the bgtcalsponsorohead property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getBGTCALSPONSOROHEAD() {
        return bgtcalsponsorohead;
    }

    /**
     * Sets the value of the bgtcalsponsorohead property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setBGTCALSPONSOROHEAD(Double value) {
        this.bgtcalsponsorohead = value;
    }

    /**
     * Gets the value of the bgtcalspoverhead property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getBGTCALSPOVERHEAD() {
        return bgtcalspoverhead;
    }

    /**
     * Sets the value of the bgtcalspoverhead property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setBGTCALSPOVERHEAD(Double value) {
        this.bgtcalspoverhead = value;
    }

    /**
     * Gets the value of the bgtcalspoverheadflag property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBGTCALSPOVERHEADFLAG() {
        return bgtcalspoverheadflag;
    }

    /**
     * Sets the value of the bgtcalspoverheadflag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBGTCALSPOVERHEADFLAG(Integer value) {
        this.bgtcalspoverheadflag = value;
    }

    /**
     * Gets the value of the budgetCalIdent property.
     * 
     * @return
     *     possible object is
     *     {@link BudgetCalIdentifier }
     *     
     */
    public BudgetCalIdentifier getBudgetCalIdent() {
        return budgetCalIdent;
    }

    /**
     * Sets the value of the budgetCalIdent property.
     * 
     * @param value
     *     allowed object is
     *     {@link BudgetCalIdentifier }
     *     
     */
    public void setBudgetCalIdent(BudgetCalIdentifier value) {
        this.budgetCalIdent = value;
    }

    /**
     * Gets the value of the budgetSections property.
     * 
     * @return
     *     possible object is
     *     {@link BgtSectionDetail }
     *     
     */
    public BgtSectionDetail getBudgetSections() {
        return budgetSections;
    }

    /**
     * Sets the value of the budgetSections property.
     * 
     * @param value
     *     allowed object is
     *     {@link BgtSectionDetail }
     *     
     */
    public void setBudgetSections(BgtSectionDetail value) {
        this.budgetSections = value;
    }

    /**
     * Gets the value of the calNameIdent property.
     * 
     * @return
     *     possible object is
     *     {@link CalendarNameIdentifier }
     *     
     */
    public CalendarNameIdentifier getCalNameIdent() {
        return calNameIdent;
    }

    /**
     * Sets the value of the calNameIdent property.
     * 
     * @param value
     *     allowed object is
     *     {@link CalendarNameIdentifier }
     *     
     */
    public void setCalNameIdent(CalendarNameIdentifier value) {
        this.calNameIdent = value;
    }

    /**
     * Gets the value of the granddisccost property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGRANDDISCCOST() {
        return granddisccost;
    }

    /**
     * Sets the value of the granddisccost property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGRANDDISCCOST(String value) {
        this.granddisccost = value;
    }

    /**
     * Gets the value of the granddisctotalcost property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGRANDDISCTOTALCOST() {
        return granddisctotalcost;
    }

    /**
     * Sets the value of the granddisctotalcost property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGRANDDISCTOTALCOST(String value) {
        this.granddisctotalcost = value;
    }

    /**
     * Gets the value of the grandfringecost property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGRANDFRINGECOST() {
        return grandfringecost;
    }

    /**
     * Sets the value of the grandfringecost property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGRANDFRINGECOST(String value) {
        this.grandfringecost = value;
    }

    /**
     * Gets the value of the grandfringetotalcost property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGRANDFRINGETOTALCOST() {
        return grandfringetotalcost;
    }

    /**
     * Sets the value of the grandfringetotalcost property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGRANDFRINGETOTALCOST(String value) {
        this.grandfringetotalcost = value;
    }

    /**
     * Gets the value of the grandindcost property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGRANDINDCOST() {
        return grandindcost;
    }

    /**
     * Sets the value of the grandindcost property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGRANDINDCOST(String value) {
        this.grandindcost = value;
    }

    /**
     * Gets the value of the grandindtotalcost property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGRANDINDTOTALCOST() {
        return grandindtotalcost;
    }

    /**
     * Sets the value of the grandindtotalcost property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGRANDINDTOTALCOST(String value) {
        this.grandindtotalcost = value;
    }

    /**
     * Gets the value of the grandrescost property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGRANDRESCOST() {
        return grandrescost;
    }

    /**
     * Sets the value of the grandrescost property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGRANDRESCOST(String value) {
        this.grandrescost = value;
    }

    /**
     * Gets the value of the grandressponsor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGRANDRESSPONSOR() {
        return grandressponsor;
    }

    /**
     * Sets the value of the grandressponsor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGRANDRESSPONSOR(String value) {
        this.grandressponsor = value;
    }

    /**
     * Gets the value of the grandrestotalcost property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGRANDRESTOTALCOST() {
        return grandrestotalcost;
    }

    /**
     * Sets the value of the grandrestotalcost property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGRANDRESTOTALCOST(String value) {
        this.grandrestotalcost = value;
    }

    /**
     * Gets the value of the grandresvariance property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGRANDRESVARIANCE() {
        return grandresvariance;
    }

    /**
     * Sets the value of the grandresvariance property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGRANDRESVARIANCE(String value) {
        this.grandresvariance = value;
    }

    /**
     * Gets the value of the grandresviariance property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGRANDRESVIARIANCE() {
        return grandresviariance;
    }

    /**
     * Sets the value of the grandresviariance property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGRANDRESVIARIANCE(String value) {
        this.grandresviariance = value;
    }

    /**
     * Gets the value of the grandsalarycost property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGRANDSALARYCOST() {
        return grandsalarycost;
    }

    /**
     * Sets the value of the grandsalarycost property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGRANDSALARYCOST(String value) {
        this.grandsalarycost = value;
    }

    /**
     * Gets the value of the grandsalarytotalcost property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGRANDSALARYTOTALCOST() {
        return grandsalarytotalcost;
    }

    /**
     * Sets the value of the grandsalarytotalcost property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGRANDSALARYTOTALCOST(String value) {
        this.grandsalarytotalcost = value;
    }

    /**
     * Gets the value of the grandsoccost property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGRANDSOCCOST() {
        return grandsoccost;
    }

    /**
     * Sets the value of the grandsoccost property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGRANDSOCCOST(String value) {
        this.grandsoccost = value;
    }

    /**
     * Gets the value of the grandsocsponsor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGRANDSOCSPONSOR() {
        return grandsocsponsor;
    }

    /**
     * Sets the value of the grandsocsponsor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGRANDSOCSPONSOR(String value) {
        this.grandsocsponsor = value;
    }

    /**
     * Gets the value of the grandsoctotalcost property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGRANDSOCTOTALCOST() {
        return grandsoctotalcost;
    }

    /**
     * Sets the value of the grandsoctotalcost property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGRANDSOCTOTALCOST(String value) {
        this.grandsoctotalcost = value;
    }

    /**
     * Gets the value of the grandsocvariance property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGRANDSOCVARIANCE() {
        return grandsocvariance;
    }

    /**
     * Sets the value of the grandsocvariance property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGRANDSOCVARIANCE(String value) {
        this.grandsocvariance = value;
    }

    /**
     * Gets the value of the grandtotalcost property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGRANDTOTALCOST() {
        return grandtotalcost;
    }

    /**
     * Sets the value of the grandtotalcost property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGRANDTOTALCOST(String value) {
        this.grandtotalcost = value;
    }

    /**
     * Gets the value of the grandtotalsponsor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGRANDTOTALSPONSOR() {
        return grandtotalsponsor;
    }

    /**
     * Sets the value of the grandtotalsponsor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGRANDTOTALSPONSOR(String value) {
        this.grandtotalsponsor = value;
    }

    /**
     * Gets the value of the grandtotaltotalcost property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGRANDTOTALTOTALCOST() {
        return grandtotaltotalcost;
    }

    /**
     * Sets the value of the grandtotaltotalcost property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGRANDTOTALTOTALCOST(String value) {
        this.grandtotaltotalcost = value;
    }

    /**
     * Gets the value of the grandtotalvariance property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGRANDTOTALVARIANCE() {
        return grandtotalvariance;
    }

    /**
     * Sets the value of the grandtotalvariance property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGRANDTOTALVARIANCE(String value) {
        this.grandtotalvariance = value;
    }

}
