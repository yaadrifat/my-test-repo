
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createMMilestones complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createMMilestones">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Milestones" type="{http://velos.com/services/}milestoneList" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createMMilestones", propOrder = {
    "milestones"
})
public class CreateMMilestones {

    @XmlElement(name = "Milestones")
    protected MilestoneList milestones;

    /**
     * Gets the value of the milestones property.
     * 
     * @return
     *     possible object is
     *     {@link MilestoneList }
     *     
     */
    public MilestoneList getMilestones() {
        return milestones;
    }

    /**
     * Sets the value of the milestones property.
     * 
     * @param value
     *     allowed object is
     *     {@link MilestoneList }
     *     
     */
    public void setMilestones(MilestoneList value) {
        this.milestones = value;
    }

}
