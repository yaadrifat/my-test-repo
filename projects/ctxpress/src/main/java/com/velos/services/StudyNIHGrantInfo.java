
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for studyNIHGrantInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="studyNIHGrantInfo">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="fundMech" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="instCode" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="nihGrantSerial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="programCode" type="{http://velos.com/services/}code" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "studyNIHGrantInfo", propOrder = {
    "fundMech",
    "instCode",
    "nihGrantSerial",
    "programCode"
})
public class StudyNIHGrantInfo
    extends ServiceObject
{

    protected Code fundMech;
    protected Code instCode;
    protected String nihGrantSerial;
    protected Code programCode;

    /**
     * Gets the value of the fundMech property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getFundMech() {
        return fundMech;
    }

    /**
     * Sets the value of the fundMech property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setFundMech(Code value) {
        this.fundMech = value;
    }

    /**
     * Gets the value of the instCode property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getInstCode() {
        return instCode;
    }

    /**
     * Sets the value of the instCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setInstCode(Code value) {
        this.instCode = value;
    }

    /**
     * Gets the value of the nihGrantSerial property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNihGrantSerial() {
        return nihGrantSerial;
    }

    /**
     * Sets the value of the nihGrantSerial property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNihGrantSerial(String value) {
        this.nihGrantSerial = value;
    }

    /**
     * Gets the value of the programCode property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getProgramCode() {
        return programCode;
    }

    /**
     * Sets the value of the programCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setProgramCode(Code value) {
        this.programCode = value;
    }

}
