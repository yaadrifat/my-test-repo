/* This readMe is specific to  eRes CTX Build#4 */
==============================================================================================================================================




Bugzilla bugs:

			23786 - Order the milestone based on the pk - either ascending or descending

			23769 - Create Milestones: If count tag is null or 0 while creating patient Milestone then incorrect error appears

			23770 -  Hibernate error appears if incorrect value entered in <MILESTONE_TYPE> tag

			23766 - Create Milestone: Milestone gets created irrespective of any data in <MILESTONE_ISACTIVE> and <FK_CODELST_MILESTONE_STAT>

			23773 - Create Milestone: All the mandatory fields for all the milestones should be validated and appropriate error should be displayed. 

			23782 - Create Milestone: All the tags such as <fk_tagname> should be <pk_tagname>

			23899 - Get Study version is getting only the file location and a way to get the file 

			23898 - When we try to create a new version we are getting null pointer exception 
			
Note: To make deployment process easy we have put the database connection in properties file.			

Due to above issue we have made a hot fix(1) to CTX Build#3.
	
This version contains 14 class files 2 scripts read-me, release note and 3 configuration files:


Java class files:


\server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\model\StudyApndxPojo.class
\server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\model\StudyAppendix.class
\server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\model\StudyStatusHistoryPojo.class
\server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\util\HibernateUtil.class
\server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\util\HibernateUtilHelper.class

\server\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\milestone\MilestoneServiceDao.class
\server\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\milestone\MilestoneHandler.class

\server\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\StudyApndxPojo.class
\server\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\StudyAppendix.class
\server\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\StudyStatusHistoryPojo.class

\server\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\util\HibernateUtil.class
\server\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\util\HibernateUtilHelper.class

\server\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\version\VersionServiceDao.class
\server\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\version\VersionServiceImpl.class


2. DB Script files:
		eres:
            			1. db script\ERES\1_hotfix1_er_version.sql
		
		vda:            2. db script\VDA\1_hotfix1_vda_version.sql
			

3. Please update erespool.properties file inside jboss conf directory with following, also, please update credentials according to QA environment :


jdbc-vda.proxool.alias=vda
jdbc-vda.proxool.driver-url=jdbc:oracle:thin:@172.16.3.90:1521:Velos921
jdbc-vda.proxool.driver-class=oracle.jdbc.driver.OracleDriver
jdbc-vda.proxool.maximum-connection-count=2
jdbc-vda.proxool.maximum-connection-count=10
jdbc-vda.proxool.maximum-active-time=120000
jdbc-vda.proxool.maximum-connection-lifetime=120000
jdbc-vda.proxool.statistics=1m,1m,1d
jdbc-vda.proxool.statistics-log-level=DEBUG
jdbc-vda.proxool.house-keeping-test-sql=select sysdate from dual
jdbc-vda.user=vda
jdbc-vda.password=vda123
	
Please comment/remove following tags from hibernate-eres.cfg.xml ,hibernate-esch.cfg.xml,hibernate-vda.cfg.xml files:

<property name="connection.url">jdbc:oracle:thin:@172.16.3.90:1521:Velos921</property> 
<property name="connection.username">esch</property> 
<property name="connection.password">esch123</property> 


	
4. Please remove following jars from eresearch/lib directory:

1. hibernate-annotations-3.4.0.GA
2. hibernate-commons-annotations-3.1.0.GA
3. hibernate-core-3.3.0.SP1
4. hibernate-core-4.3.0.beta1
5. hibernate-jpa-2.0-api-1.0.1.Final




Steps to deploy build:
- Stop the eResearch application.
- execute db patches.
- Clear the tmp and work folders under JBoss as usual and start the eResearch application.



