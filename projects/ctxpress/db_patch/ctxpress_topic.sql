set define off;

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_SETTINGS where SETTINGS_MODNUM = 2894 and SETTINGS_KEYWORD='OUTBOUND_MESSAGE_FREQUENCY';
  if (v_record_exists = 0) then
INSERT INTO ER_SETTINGS
		(SETTINGS_PK, SETTINGS_MODNUM, SETTINGS_MODNAME, SETTINGS_KEYWORD, SETTINGS_VALUE) 
	VALUES (SEQ_ER_SETTINGS.nextval, 2894, 1,'OUTBOUND_MESSAGE_FREQUENCY', '5');
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_MSG_TOPICS where FK_ACCOUNT = 2894 and TOPIC_JNDI = 'topic/study_2894';
  if (v_record_exists = 0) then
INSERT INTO ER_MSG_TOPICS
		(FK_ACCOUNT,TOPIC_NAME,TOPIC_JNDI, ROOT_TABLENAMES, TOPIC_CONNECTION_JNDI,TOPIC_CONN_USERNAME, TOPIC_CONN_PASSWORD)
	 VALUES (2894,'study topic','topic/study_2894','er_study','ConnectionFactory','guest','guest');
	  commit;
  end if;
end;
/
commit;
