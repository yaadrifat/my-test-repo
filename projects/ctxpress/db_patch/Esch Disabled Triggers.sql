--Triggers disabled on event_assoc table
ALTER TRIGGER EVENT_ASSOC_AU2_OUT DISABLE;
ALTER TRIGGER EVENT_ASSOC_AI2_OUT DISABLE;

--Triggers disabled on sch_budget table
ALTER TRIGGER SCH_BUDGET_AU3_OUT DISABLE;
ALTER TRIGGER SCH_BUDGET_AI1_OUT DISABLE;
