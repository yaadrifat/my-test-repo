CREATE TABLE ctxpress_audit_details
(
  pk_msg_audit integer NOT NULL,
  studynumber character varying(20),
  broadcasting_site character varying(50),
  participating_site character varying(50),
  component_type character varying(50),
  component_name character varying(50),
  status character varying(50),
  response_message text,
  creation_date timestamp without time zone,
  CONSTRAINT ctxpress_audit_details_pkey PRIMARY KEY (pk_msg_audit)
)
