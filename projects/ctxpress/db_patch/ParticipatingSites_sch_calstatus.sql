set define off;

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from SCH_CODELST where CODELST_TYPE='calStatStd' and CODELST_SUBTYP = 'SIP';
  if (v_record_exists = 0) then
INSERT INTO SCH_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'calStatStd', 'SIP', 'Sent in Study Package', 'N', 
		(select max(CODELST_SEQ)+1 from SCH_CODELST where CODELST_TYPE='calStatStd'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL,'default_data', NULL);
	  commit;
  end if;
end;
/
commit;
