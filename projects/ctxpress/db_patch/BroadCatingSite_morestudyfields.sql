set define off;

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studyidtype' and CODELST_SUBTYP = 'studyidtype_0';
  if (v_record_exists = 0) then
INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studyidtype', 'studyidtype_0', 'Short Title', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studyidtype'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, NULL,NULL, NULL);
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studyidtype' and CODELST_SUBTYP = 'school';
  if (v_record_exists = 0) then
INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studyidtype', 'school', 'School', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studyidtype'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, NULL,NULL, NULL);
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studyidtype' and CODELST_SUBTYP = 'department';
  if (v_record_exists = 0) then
INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studyidtype', 'department', 'Department', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studyidtype'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, NULL,NULL, NULL);
	  commit;
  end if;
end;
/
commit;

