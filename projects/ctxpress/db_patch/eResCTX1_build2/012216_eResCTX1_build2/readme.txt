/* This readMe is specific to  eRes CTxpress Ver 1.0.0 */
==============================================================================================================================================

1. Budget as web service has been implemented.
2. It can be tested through SOAP-UI tool.
    1.createBudgets
    2.getStudyBudgets	
ExampleEndPointURL:
	http://172.16.2.60:8080/webservices/budgetservice?wsdl 

	
	
This version contains :

1. Replace the "server" directory inside jboss server	
	
2. "Doc" directory:
	1. contains xml input file to get budget and output xml file for end result.
  	2. contains xml input file to create budget and output xml file for end result.

	Also, there is no change in Assumptions and pre requisite document.

Steps to deploy build:
- Stop the eResearch application.
- Clear the tmp and work folders under JBoss as usual and start the eResearch application.


