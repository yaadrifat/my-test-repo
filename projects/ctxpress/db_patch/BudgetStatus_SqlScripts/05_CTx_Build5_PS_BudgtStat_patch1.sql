set define off;

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from SCH_CODELST where CODELST_TYPE='budget_stat' and CODELST_SUBTYP = 'S';
  if (v_record_exists = 0) then
INSERT INTO SCH_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_STUDY_ROLE)
	VALUES(SCH_CODELST_SEQ1.nextval , NULL, 'budget_stat', 'S', 'Sent in Study Package', 'N', 
		(select max(CODELST_SEQ)+1 from SCH_CODELST where CODELST_TYPE='budget_stat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL,'default_data');
	  commit;
  end if;
end;
/
commit;




INSERT INTO track_patches
VALUES(seq_track_patches.nextval,292,0,'05_CTx_Build5_PS_BudgtStat_patch1.sql',sysdate,'v9.2.2 #693f.10');
commit;
