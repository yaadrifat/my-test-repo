/* This readMe is specific to  eRes CTX Build#8 */
==============================================================================================================================================

NOTE: PLEASE TAKE BACKUP OF ESP BEFORE DEPLOYING THIS BUILD.



Bugzilla bugs:

			23891 Uniqueness of the study in Participating not determined by morestudy details CTXStudyNumber
	
			23876 When the number of visits are more than 32 we get the java.util.ArrayList.RangeCheck(ArrayList.java:547) 
	
			24116 When we have decimal in the holdback the milestone is not getting migrated 

			23877 Coverage notes are not getting copied
			
			23869 When Events are more than 40 the calendar is not getting migrated and we get this invalid error 
			
			23900 There are two nodes for milestone status

			24037 When we have a bugdet template and when we try to create a budget it will not let us create a new budget for the same template  
			
			24072 Versions with Attachment are not moving and not sending an error notifications as well 
			
			24074 Sent in Study package - not showing up in the participating site for the version that is send  
			
			24075 Version tab gets moved but the attachment are not getting moved 
			
			24110 Version date gets autopopulated in the participating even though the value is not there 
			
Due to above issue we have made a hot fix(1) to CTX Build#7.
	
This version contains 21 class files 1 db script, read-me, release note files:


Java class files:

server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\eres\service\util\StringUtil.class

server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\model\CalendarEventSummary.class
server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\model\Milestonee.class
server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\model\MilestonePojo.class
server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\model\StudySearch.class
server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\model\StudySummary.class
server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\model\StudySearchResults.class


server\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\budget\BudgetServiceDAO.class


server\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\milestone\MilestoneHandler.class
server\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\milestone\MilestoneServiceDao.class

server\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\CalendarEventSummary.class
server\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\Milestonee.class
server\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\MilestonePojo.class
server\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\StudySearch.class
server\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\StudySummary.class
server\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\StudySearchResults.class



server\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\study\StudyServiceHelper.class
server\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\study\StudyServiceImpl$1.class
server\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\study\StudyServiceImpl.class
server\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\study\StudyService.class

server\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\version\VersionServiceDao.class

server\eresearch\deploy\velos.ear\velos-ejb-esch.jar\com\velos\services\calendar\CalendarEventHelper.class
server\eresearch\deploy\velos.ear\velos-ejb-esch.jar\com\velos\services\calendar\CalendarVisitHelper.class

server\eresearch\deploy\velos.ear\webservices.war\WEB-INF\classes\com\velos\webservices\StudySEI.class
server\eresearch\deploy\velos.ear\webservices.war\WEB-INF\classes\com\velos\webservices\StudyWS.class

server\eresearch\deploy\velos.ear\webservices.war\WEB-INF\classes\com\velos\services\client\StudyClient.class


2. DB Script files:
		eres:
            			1. db script\ERES\08_CTx_Build8_patch1.sql
		
		

Steps to deploy build:
- Stop the eResearch application.
- execute db patches.
- Clear the tmp and work folders under JBoss as usual and start the eResearch application.



