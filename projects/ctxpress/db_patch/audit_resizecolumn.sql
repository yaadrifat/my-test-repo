--Execute this script on the interface postgres database for increasing the audit table column size

SELECT atttypmod FROM pg_attribute
WHERE attrelid = 'ctxpress_audit_details'::regclass
AND attname = 'studynumber';

UPDATE pg_attribute SET atttypmod = 200+4
WHERE attrelid = 'ctxpress_audit_details'::regclass
AND attname = 'studynumber';

UPDATE pg_attribute SET atttypmod = 200+4
WHERE attrelid = 'ctxpress_audit_details'::regclass
AND attname = 'broadcasting_site';

UPDATE pg_attribute SET atttypmod = 200+4
WHERE attrelid = 'ctxpress_audit_details'::regclass
AND attname = 'participating_site';


UPDATE pg_attribute SET atttypmod = 200+4
WHERE attrelid = 'ctxpress_audit_details'::regclass
AND attname = 'component_type';



UPDATE pg_attribute SET atttypmod = 200+4
WHERE attrelid = 'ctxpress_audit_details'::regclass
AND attname = 'component_name';
