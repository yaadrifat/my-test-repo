create or replace
TRIGGER CTXpress_ER_STUDYSTAT_AI1_OUT
BEFORE INSERT ON ER_STUDYSTAT
FOR EACH ROW
DECLARE
fkaccount NUMBER;
pkstudypckg NUMBER;
pkamenddocs NUMBER;
pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SVC_OUTBOUND', pLEVEL  => Plog.LFATAL);
BEGIN
  SELECT fk_account
  INTO fkaccount
  FROM er_user
  WHERE pk_user = :NEW.creator;
  SELECT pk_codelst
  INTO pkstudypckg
  FROM er_codelst
  WHERE codelst_type = 'studystat' AND  codelst_subtyp='Send_Study_Pckg';
  SELECT pk_codelst
  INTO pkamenddocs
  FROM er_codelst
  WHERE codelst_type = 'studystat' AND  codelst_subtyp='Send_Amend_Docs';
IF :NEW.FK_CODELST_STUDYSTAT = pkstudypckg OR :NEW.FK_CODELST_STUDYSTAT = pkamenddocs
THEN
  PKG_MSG_QUEUE.SP_POPULATE_STUDY_STATUS_MSG (
      :NEW.pk_studystat,
      fkaccount,
      'I',
      to_char(:NEW.fk_study),
      :NEW.FK_CODELST_STUDYSTAT
   );
END IF;
  EXCEPTION  WHEN OTHERS THEN
  Plog.FATAL(pCTX,'exception in CTXpress_ER_STUDYSTAT_AI1_OUT ' || SQLERRM);
END;

