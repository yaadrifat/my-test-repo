CREATE OR REPLACE FORCE VIEW "VDA"."VDA_V_MILESTONES_CTX" ("STUDY_NUMBER", "STUDY_TITLE", "MSRUL_CATEGORY", "MILESTONE_AMOUNT", "MSRUL_PT_COUNT", "MILESTONE_VISIT", "MILESTONE_DELFLAG", "MILESTONE_USERSTO", "RID", "IP_ADD", "MSRUL_LIMIT", "MSRUL_PT_STATUS", "MILESTONE_PAYDUEBY", "MILESTONE_PAYBYUNIT", "MSRUL_PAY_TYPE", "MSRUL_PAY_FOR", "MILESTONE_EVENTSTATUS", "MSRUL_STATUS", "MSRUL_PROT_CAL", "MS_PROT_CALASSOCTO", "MILESTONE_ISACTIVE", "MSRUL_VISIT", "MSRUL_MS_RULE", "MSRUL_EVENT_STAT", "MSRUL_STUDY_STATUS", "FK_ACCOUNT", "PK_MILESTONE", "CREATOR", "LAST_MODIFIED_BY", "LAST_MODIFIED_DATE", "CREATED_ON", "FK_STUDY", "EVENT_NAME", "MILESTONE_ACHIEVEDCOUNT", "LAST_CHECKED_ON", "MILESTONE_DESCRIPTION", "FK_BUDGET", "FK_BGTCAL", "FK_BGTSECTION", "FK_LINEITEM", "BUDGET_NAME", "CALENDAR_NAME", "BUDGET_SECTION_NAME", "LINEITEM_NAME", "MILESTONE_DATE_FROM", "MILESTONE_DATE_TO", "MILESTONE_HOLDBACK", "MILESTONE_DESC_CALCULATED", "FK_VISIT", "FK_EVENTASSOC", "LAST_MODIFIED_BY_FK", "CREATOR_FK", "MILERULE_SUBTYP",
  "MILEPS_SUBTYP", "MILEPAY_SUBTYP", "MILEPAYFR_SUBTYPE", "MILESTAT_SUBTYP", "MILEVISIT_SUBTYP")
AS
  SELECT STUDY_NUMBER,
    STUDY_TITLE,
    ERES.F_GEt_Mtype (MILESTONE_TYPE) MSRUL_CATEGORY,
    MILESTONE_AMOUNT,
    MILESTONE_COUNT MSRUL_PT_COUNT,
    MILESTONE_VISIT,
    MILESTONE_DELFLAG,
    MILESTONE_USERSTO,
    m.RID,
    m.IP_ADD,
    MILESTONE_LIMIT MSRUL_LIMIT,
    (SELECT CODELST_DESC
    FROM eres.ER_CODELST
    WHERE PK_CODELST = MILESTONE_STATUS
    AND CODELST_TYPE = 'patStatus'
    ) MSRUL_PT_STATUS,
    MILESTONE_PAYDUEBY,
    MILESTONE_PAYBYUNIT,
    ERES.F_GEt_Codelstdesc (MILESTONE_PAYTYPE) MSRUL_PAY_TYPE,
    ERES.F_GEt_Codelstdesc (MILESTONE_PAYFOR) MSRUL_PAY_FOR,
    MILESTONE_EVENTSTATUS,
    ERES.F_GEt_Codelstdesc (FK_CODELST_MILESTONE_STAT) MSRUL_STATUS,
    DECODE(MILESTONE_TYPE,'AM',
	(SELECT NAME FROM ESCH.EVENT_ASSOC where EVENT_ID=(SELECT CHAIN_ID FROM ESCH.EVENT_ASSOC where event_id=FK_EVENTASSOC))
	,(SELECT NAME FROM ESCH.EVENT_ASSOC WHERE EVENT_ID = FK_CAL
    )) MSRUL_PROT_CAL,
	DECODE(MILESTONE_TYPE,'AM',
	(SELECT EVENT_CALASSOCTO FROM ESCH.EVENT_ASSOC where EVENT_ID=(SELECT CHAIN_ID FROM ESCH.EVENT_ASSOC where event_id=FK_EVENTASSOC))
	,(SELECT EVENT_CALASSOCTO FROM ESCH.EVENT_ASSOC WHERE EVENT_ID = FK_CAL
    )) MS_PROT_CALASSOCTO,
    MILESTONE_ISACTIVE,
    DECODE(MILESTONE_TYPE,'AM',(SELECT visit_name FROM esch.sch_protocol_visit
    WHERE pk_protocol_visit = (select fk_visit from ESCH.EVENT_ASSOC where event_id=FK_EVENTASSOC)),
	(SELECT visit_name  FROM esch.sch_protocol_visit  WHERE pk_protocol_visit = fk_visit
    )) MSRUL_VISIT,
    ERES.F_GEt_Codelstdesc (FK_CODELST_RULE) MSRUL_MS_RULE,
    (SELECT CODELST_DESC
    FROM esch.SCH_CODELST
    WHERE PK_CODELST = MILESTONE_EVENTSTATUS
    ) MSRUL_EVENT_STAT,
    (SELECT CODELST_DESC
    FROM eres.ER_CODELST
    WHERE PK_CODELST = MILESTONE_STATUS
    AND CODELST_TYPE = 'studystat'
    ) MSRUL_STUDY_STATUS,
    s.fk_Account FK_ACCOUNT,
    PK_MILESTONE,
    (SELECT USR_FIRSTNAME
      || ' '
      || USR_LASTNAME
    FROM eres.ER_USER
    WHERE PK_USER = m.CREATOR
    ) CREATOR,
    (SELECT USR_FIRSTNAME
      || ' '
      || USR_LASTNAME
    FROM eres.ER_USER
    WHERE PK_USER = m.LAST_MODIFIED_BY
    ) LAST_MODIFIED_BY,
    m.LAST_MODIFIED_DATE,
    m.CREATED_ON,
    FK_STUDY,
    (SELECT NAME FROM ESCH.EVENT_ASSOC WHERE EVENT_ID = FK_EVENTASSOC
    ) EVENT_NAME,
    MILESTONE_ACHIEVEDCOUNT,
    LAST_CHECKED_ON,
    MILESTONE_DESCRIPTION,
    FK_BUDGET,
    FK_BGTCAL,
    FK_BGTSECTION,
    FK_LINEITEM,
    (SELECT BUDGET_NAME FROM ESCH.SCH_BUDGET WHERE PK_BUDGET=FK_BUDGET
    ) BUDGET_NAME,
    (SELECT name
    FROM ESCH.EVENT_ASSOC
    WHERE EVENT_ID=FK_BGTCAL
    AND chain_id  =pk_study
    ) CALENDAR_NAME,
    (SELECT bgtsection_name
    FROM ESCH.SCH_BGTSECTION
    WHERE PK_BUDGETSEC=FK_BGTSECTION
    ) BUDGET_SECTION_NAME,
    (SELECT lineitem_name FROM ESCH.SCH_LINEITEM WHERE PK_LINEITEM=FK_LINEITEM
    ) LINEITEM_NAME,
    m.MILESTONE_DATE_FROM,
    m.MILESTONE_DATE_TO,
    m.MILESTONE_HOLDBACK,
    ERES.PKG_Milestone_New.f_getMilestoneDesc (PK_MILESTONE) AS MILESTONE_DESC_CALCULATED,
    fk_visit,
    FK_EVENTASSOC,
    m.LAST_MODIFIED_BY LAST_MODIFIED_BY_FK,
    m.CREATOR CREATOR_FK,
    (SELECT er1.codelst_subtyp
    FROM eres.er_codelst er1
    WHERE er1.pk_codelst=FK_CODELST_RULE
    ) MILERULE_SUBTYP,
    (SELECT er2.codelst_subtyp
    FROM eres.er_codelst er2
    WHERE er2.pk_codelst=MILESTONE_STATUS
    ) MILEPS_SUBTYP,
    (SELECT er3.codelst_subtyp
    FROM eres.er_codelst er3
    WHERE er3.pk_codelst=MILESTONE_PAYTYPE
    ) MILEPAY_SUBTYP,
    (SELECT er4.codelst_subtyp
    FROM eres.er_codelst er4
    WHERE er4.pk_codelst=MILESTONE_PAYFOR
    ) MILEPAYFR_SUBTYPE,
    (SELECT er5.codelst_subtyp
    FROM eres.er_codelst er5
    WHERE er5.pk_codelst=FK_CODELST_MILESTONE_STAT
    ) MILESTAT_SUBTYP,
    (SELECT er6.codelst_subtyp
    FROM esch.sch_codelst er6
    WHERE er6.pk_codelst=MILESTONE_EVENTSTATUS
    ) MILEVISIT_SUBTYP
  FROM eres.ER_MILESTONE m,
    eres.er_study s
  WHERE NVL (milestone_delflag, 'N') = 'N'
  AND m.fk_study                     = pk_study;
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."STUDY_NUMBER"
IS
  'The Study Number of the study the milestone is linked with';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."STUDY_TITLE"
IS
  'The study title of the study the milestone is linked with';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."MSRUL_CATEGORY"
IS
  'The milestone rule category';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."MILESTONE_AMOUNT"
IS
  'The amount linked with the milestone';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."MSRUL_PT_COUNT"
IS
  'The patient count attribute of the milestone (if applicable)';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."MILESTONE_VISIT"
IS
  'The visit Name to for visit or event milestones';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."MILESTONE_DELFLAG"
IS
  'The Delete Flag of milestones';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."MILESTONE_USERSTO"
IS
  'The milestone notification users';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."RID"
IS
  'The RID for auditing';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."IP_ADD"
IS
  'The IP_ADD of milestone creator';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."MSRUL_LIMIT"
IS
  'The milestone achievement count limit';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."MSRUL_PT_STATUS"
IS
  'The patient status attribute of the milestone (if applicable)';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."MILESTONE_PAYDUEBY"
IS
  'The milestone payment Due By';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."MILESTONE_PAYBYUNIT"
IS
  'The milestone payment Due Unit';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."MSRUL_PAY_TYPE"
IS
  'The milestone payment type';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."MSRUL_PAY_FOR"
IS
  'The milestone payment for';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."MILESTONE_EVENTSTATUS"
IS
  'The Milestone event status';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."MSRUL_STATUS"
IS
  'The milestone status';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."MSRUL_PROT_CAL"
IS
  'The Calendar attribute of the milestone (if applicable)';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."MS_PROT_CALASSOCTO"
IS
  'The Calendar Associted to in study (if applicable)';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."MILESTONE_ISACTIVE"
IS
  'The check is milestone active';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."MSRUL_VISIT"
IS
  'The Calendar visit attribute of the milestone (if applicable)';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."MSRUL_MS_RULE"
IS
  'The milestone rule description';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."MSRUL_EVENT_STAT"
IS
  'The Calendar event status attribute of the milestone (if applicable)';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."MSRUL_STUDY_STATUS"
IS
  'The Study Status attribute of the milestone (if applicable)';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."FK_ACCOUNT"
IS
  'The account the milestone is linked with';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."PK_MILESTONE"
IS
  'The primary key of the milestone record';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."CREATOR"
IS
  'The user who created the record (Audit)';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."LAST_MODIFIED_BY"
IS
  'The user who last modified the record(Audit)';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."LAST_MODIFIED_DATE"
IS
  'The date the record was last modified on (Audit)';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."CREATED_ON"
IS
  'The user who created the record (Audit)';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."FK_STUDY"
IS
  'The Foreign Key to the study milestone is linked with';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."EVENT_NAME"
IS
  'The event name attribute of the milestone (if applicable)';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."MILESTONE_ACHIEVEDCOUNT"
IS
  'The current milestone achievement count';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."LAST_CHECKED_ON"
IS
  'The timestamp when the milestone achievement was last checked on';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."MILESTONE_DESCRIPTION"
IS
  'The milestone description';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."FK_BUDGET"
IS
  'The foreign key to the Budget (if the milestone was created from a budget)';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."FK_BGTCAL"
IS
  'The foreign key to the Budget Calendar record (if the milestone was created from a budget)';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."FK_BGTSECTION"
IS
  'The foreign key to the Budget Section record Calendar record (if the milestone was created from a budget)';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."FK_LINEITEM"
IS
  'The foreign key to the Budget line item record (if the milestone was created from a budget)';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."BUDGET_NAME"
IS
  'The Budget Name for milestones (if the milestone was created from a budget)';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."CALENDAR_NAME"
IS
  'The Calendar Name for milestones (if the milestone was created from a budget)';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."BUDGET_SECTION_NAME"
IS
  'The Budget Section Name for event milestones (if the milestone was created from a budget)';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."LINEITEM_NAME"
IS
  'The LineItem Name for event milestones (if the milestone was created from a budget)';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."MILESTONE_DATE_FROM"
IS
  'The Start Date for the time-bounded milestones';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."MILESTONE_DATE_TO"
IS
  'The To or End Date for the time-bounded milestones';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."MILESTONE_HOLDBACK"
IS
  'The Holdback amount for the milestone';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."MILESTONE_DESC_CALCULATED"
IS
  'The Milestone description calculated from milestone attributes';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."FK_VISIT"
IS
  'The PK of visit to uniquely identify a visit for visit milestones';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."FK_EVENTASSOC"
IS
  'The PK of event to uniquely identify an Event for event milestones';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."LAST_MODIFIED_BY_FK"
IS
  'The Fk of who last modified milestones';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."CREATOR_FK"
IS
  'The Fk of creator of milestones';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."MILERULE_SUBTYP"
IS
  'The SubType milestone Rules';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."MILEPS_SUBTYP"
IS
  'The SubType of milestone patient or study Status';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."MILEPAY_SUBTYP"
IS
  'The SubType of milestone payment type';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."MILEPAYFR_SUBTYPE"
IS
  'The SubType of milestone payment for';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."MILESTAT_SUBTYP"
IS
  'The SubType of milestone Status';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONES_CTX"."MILEVISIT_SUBTYP"
IS
  'The SubType of milestone visit';
  COMMENT ON TABLE "VDA"."VDA_V_MILESTONES_CTX"
IS
  'This view provides access to the Milestones defined for studies';