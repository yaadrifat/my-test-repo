/* This readMe is specific to  eRes CTxpress Ver 1.0.0 */
==============================================================================================================================================

1. Milestone as web service has been implemented.
2. It can be tested through SOAP-UI tool.
    1.createMMilestones
    2.getStudyMilestones	
ExampleEndPointURL:
	http://172.16.2.60:8080/webservices/MilestoneService?wsdl 

This version contains :
1. Two Hibernate configuration files  & xml file to define end point & database scripts to change ip,port,schema,username and password
   Please pick the files from ereshome and put the following files in the below mentioned location:
	a. hibernate-eres.cfg.xml: jboss conf\hibernate-eres.cfg.xml
	b. hibernate-vda.cfg.xml:  jboss conf\hibernate-vda.cfg.xml
	c. hibernate-esch.cfg.xml:  jboss conf\hibernate-esch.cfg.xml

2. Property file :

	jboss conf\serviceBundle.properties

3.  Hiberante specific Jars files are to be placed in the following location.
	server\eresearch\lib\   

4. Replace the "server" directory inside jboss server	
	
5. One database script  to be executed on VDA Schema : VDA_V_VERSION_CTX.sql
	..\db script\VDA\VDA_V_VERSION_CTX.sql

Steps to deploy build:
- Stop the eResearch application.
- Execute the DB patch in the 'db_patch' folder on the database. 
- Clear the tmp and work folders under JBoss as usual and start the eResearch application.


