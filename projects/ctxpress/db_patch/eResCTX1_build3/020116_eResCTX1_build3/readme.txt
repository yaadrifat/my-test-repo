/* This readMe is specific to  eRes CTxpress Ver 1.0.0 */
==============================================================================================================================================

1. Version as web service has been implemented.
2. It can be tested through SOAP-UI tool.
    1.createVersions
    2.getStudyVersions	
ExampleEndPointURL:
	http://172.16.2.60:8080/webservices/VersionService?wsdl

	
	
This version contains :

1. Replace the "server" directory inside jboss server	
	
2. "Doc" directory:
	1. contains xml input file to get version and output xml file for end result.
  	2. contains xml input file to create version and output xml file for end result.

	Also, there is updated Assumptions and pre requisite document.

3. DB Script:
	
	1. 020116_eResCTX1_build3\db script\ERES\er_version.sql
	2. 020116_eResCTX1_build3\db script\VDA\VDA_V_MILESTONES_CTX.sql
	
4. 1 xml File:
    server\eresearch\deploy\velos.ear\webservices.war\WEB-INF\cxf-beans.xml	
	
5. Please change the database credentials as per QA database server.	
	
Note:

Fix in Create Milestone service: removed pk and fk.
	
Steps to deploy build:
- Stop the eResearch application.
- execute db patches and replace xml file.
- Clear the tmp and work folders under JBoss as usual and start the eResearch application.


