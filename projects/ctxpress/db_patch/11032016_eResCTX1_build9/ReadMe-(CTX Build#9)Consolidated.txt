/* This readMe is specific to  eRes CTX Build#9 */
==============================================================================================================================================



NOTE: PLEASE TAKE BACKUP OF ESP BEFORE DEPLOYING THIS BUILD. This build is consolidated build for all three services(Milestone,Budgets and versions)



Bugzilla bugs:

			24075 - Version tab gets moved but the attachment are not getting moved
			24072 - Versions with Attachment are not moving and not sending an error notifications as well
			24136 - Study with multiple version documents -did not move the version and exception is not written in the database table
			23856 - When the number of visits are more than 40 getStudyCalendar throws the following exception
			
Due to above issue we have made a hot fix(1) to CTX Build#8.
		

		


2. DB Script files:
		eres:
            			1. db script\ERES\09_CTx_Build9_patch1.sql
						
						
		vda:            1. db script\VDA\VDA_V_MILESTONES_CTX.sql
						2. db script\VDA\1_hotfix1_vda_version.sql
		
Please update erespool.properties file inside jboss conf directory with following, also, please update credentials according to QA/New Instance environment :


jdbc-vda.proxool.alias=vda
jdbc-vda.proxool.driver-url=jdbc:oracle:thin:@172.16.3.90:1521:Velos921
jdbc-vda.proxool.driver-class=oracle.jdbc.driver.OracleDriver
jdbc-vda.proxool.maximum-connection-count=2
jdbc-vda.proxool.maximum-connection-count=10
jdbc-vda.proxool.maximum-active-time=120000
jdbc-vda.proxool.maximum-connection-lifetime=120000
jdbc-vda.proxool.statistics=1m,1m,1d
jdbc-vda.proxool.statistics-log-level=DEBUG
jdbc-vda.proxool.house-keeping-test-sql=select sysdate from dual
jdbc-vda.user=vda
jdbc-vda.password=vda123
	
	
4. Please pick the files from ereshome directory and put the following files in jboss's conf directory:
	a. hibernate-eres.cfg.xml: jboss conf\hibernate-eres.cfg.xml
	b. hibernate-vda.cfg.xml:  jboss conf\hibernate-vda.cfg.xml
	c. hibernate-esch.cfg.xml: jboss conf\hibernate-esch.cfg.xml	
	d. serviceBundle.properties: jboss conf\serviceBundle.properties
	
	
4. Please put following jars to eresearch/lib directory of jboss server:

1. c3p0-0.9.2.1
2. hibernate-c3p0-3.3.0.GA
3. mchange-commons-java-0.2.3.4
	

Steps to deploy build:
- Stop the eResearch application.
- execute db patches.
- Clear the tmp and work folders under JBoss as usual and start the eResearch application.



