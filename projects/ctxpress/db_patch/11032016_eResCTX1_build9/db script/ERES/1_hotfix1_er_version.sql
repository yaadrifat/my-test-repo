
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,292,0,'01_CTx_Build1_patch1.sql',sysdate,'v9.2.2 #693f.10');
commit;
/
--build2

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,292,0,'02_CTx_Build2_patch1.sql',sysdate,'v9.2.2 #693f.10');
commit;
/
--build3
DELETE from track_patches where DB_PATCH_NAME = 'er_version.sql'  and APP_VERSION = 'Build#3';
commit	;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,292,0,'03_CTx_Build3_patch1.sql',sysdate,'v9.2.2 #693f.10');
commit;
/
--build4

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,292,0,'04_CTx_Build4_patch1.sql',sysdate,'v9.2.2 #693f.10');
commit;
/

