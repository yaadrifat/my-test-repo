/* This readMe is specific to  eRes CTX Build#5 */ 
==============================================================================================================================================

NOTE: This build is complete build for all three services(Milestone,Budgets and versions)


This version contains class 105 files 2 db script files, read-me, release notes, Doc directory that contains latest Input/Output xmls for Budget and 
version services,requirement and pre-requisite document,ereshome directory that contains configuration files and properties file, 3 jar files and 
1 xml file.


Java class files:

\server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\model\BgtCalNameIdentifier.class
\server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\model\BgtSectionDetail.class
\server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\model\BgtSectionNameIdentifier.class
\server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\model\BudgetCalDetail.class
\server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\model\BudgetDetail.class
\server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\model\CalendarNameIdentifier.class
\server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\model\EventAssocPojo.class
\server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\model\EventNameIdentfier.class
\server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\model\LineItemNameIdentifier.class
\server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\model\Milestone.class
\server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\model\Milestonee.class
\server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\model\MilestoneIdentifier.class
\server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\model\MilestoneList.class
\server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\model\MilestonePojo.class
\server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\model\MilestoneVDAPojo.class
\server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\model\SCHBudgetCalPojo.class
\server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\model\SCHBudgetPojo.class
\server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\model\SCHBudgetSectionPojo.class
\server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\model\SCHLineItemPojo.class
\server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\model\SCHProtocolVisitPojo.class
\server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\model\StudyApndxPojo.class
\server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\model\StudyAppendix.class
\server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\model\StudyStatusHistory.class
\server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\model\StudyStatusHistoryPojo.class
\server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\model\StudyVersion.class
\server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\model\StudyVersionPojo.class
\server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\model\VersionDetail.class
\server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\model\VersionIdentifier.class
\server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\model\Versions.class

\server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\util\CodeCache.class
\server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\util\CTXPressConstants.class
\server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\util\HibernateUtil.class
\server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\util\HibernateUtilHelper.class
\server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\util\JNDINames.class
\server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\util\ObjectLocator.class
\server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\util\SqlDateAdapter.class
\server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\services\IssueTypes.class



\server\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\budget\BudgetHandler.class
\server\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\budget\BudgetService.class
\server\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\budget\BudgetServiceDAO.class
\server\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\budget\BudgetServiceImpl.class

\server\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\map\ObjectMapService.class

\server\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\milestone\MilestoneHandler.class
\server\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\milestone\MilestoneService.class
\server\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\milestone\MilestoneServiceDao.class
\server\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\milestone\MilestoneServiceImpl.class


\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\BgtCalNameIdentifier.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\BgtCalPojo.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\BgtLiniItemDetail.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\BgtSectionDetail.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\BgtSectionNameIdentifier.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\BgtTemplate.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\BudgetCalDetail.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\BudgetCalIdentifier.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\BudgetCalList.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\BudgetDetail.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\BudgetLineItemPojo.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\BudgetPojo.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\BudgetSectionPojo.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\BudgetTempPojo.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\CalendarNameIdentifier.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\EventAssocPojo.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\EventNameIdentfier.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\LineItemNameIdentifier.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\Milestone.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\Milestonee.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\MilestoneIdentifier.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\MilestoneList.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\MilestonePojo.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\MilestoneVDAPojo.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\SCHBudgetCalPojo.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\SCHBudgetPojo.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\SCHBudgetSectionPojo.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\SCHLineItemPojo.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\SCHProtocolVisitPojo.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\StudyApndxPojo.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\StudyAppendix.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\StudyStatusHistory.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\StudyStatusHistoryPojo.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\StudyVersion.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\StudyVersionPojo.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\VersionDetail.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\VersionIdentifier.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\model\Versions.class


\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\util\CodeCache.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\util\CTXPressConstants.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\util\HibernateUtil.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\util\HibernateUtilHelper.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\util\JNDINames.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\util\ObjectLocator.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\util\SqlDateAdapter.class



\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\version\VersionHandler.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\version\VersionService.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\version\VersionServiceDao.class
\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\services\version\VersionServiceImpl.class


\eresearch\deploy\velos.ear\velos-main.jar\com\velos\eres\service\util\SVC.class


\eresearch\deploy\velos.ear\webservices.war\WEB-INF\classes\com\velos\services\client\BudgetClient.class
\eresearch\deploy\velos.ear\webservices.war\WEB-INF\classes\com\velos\services\client\MilestoneClient.class
\eresearch\deploy\velos.ear\webservices.war\WEB-INF\classes\com\velos\services\client\VersionClient.class


\eresearch\deploy\velos.ear\webservices.war\WEB-INF\classes\com\velos\webservices\BudgetSEI.class
\eresearch\deploy\velos.ear\webservices.war\WEB-INF\classes\com\velos\webservices\BudgetWS.class
\eresearch\deploy\velos.ear\webservices.war\WEB-INF\classes\com\velos\webservices\MilestoneSEI.class
\eresearch\deploy\velos.ear\webservices.war\WEB-INF\classes\com\velos\webservices\MilestoneWS.class
\eresearch\deploy\velos.ear\webservices.war\WEB-INF\classes\com\velos\webservices\VersionSEI.class
\eresearch\deploy\velos.ear\webservices.war\WEB-INF\classes\com\velos\webservices\VersionWS.class


\server\eresearch\deploy\velos.ear\webservices.war\WEB-INF\cxf-beans


2. DB Script files:
		eres:
            			1. db script\ERES\05_CTx_Build5_patch1.sql
						
						
		vda:            1. db script\VDA\VDA_V_MILESTONES_CTX.sql
					

3. Please update erespool.properties file inside jboss conf directory with following, also, please update credentials according to QA/New Instance environment :


jdbc-vda.proxool.alias=vda
jdbc-vda.proxool.driver-url=jdbc:oracle:thin:@172.16.3.90:1521:Velos921
jdbc-vda.proxool.driver-class=oracle.jdbc.driver.OracleDriver
jdbc-vda.proxool.maximum-connection-count=2
jdbc-vda.proxool.maximum-connection-count=10
jdbc-vda.proxool.maximum-active-time=120000
jdbc-vda.proxool.maximum-connection-lifetime=120000
jdbc-vda.proxool.statistics=1m,1m,1d
jdbc-vda.proxool.statistics-log-level=DEBUG
jdbc-vda.proxool.house-keeping-test-sql=select sysdate from dual
jdbc-vda.user=vda
jdbc-vda.password=vda123
	
	
4. Please pick the files from ereshome directory and put the following files in jboss's conf directory:
	a. hibernate-eres.cfg.xml: jboss conf\hibernate-eres.cfg.xml
	b. hibernate-vda.cfg.xml:  jboss conf\hibernate-vda.cfg.xml
	c. hibernate-esch.cfg.xml: jboss conf\hibernate-esch.cfg.xml	
	d. serviceBundle.properties: jboss conf\serviceBundle.properties
	
	
4. Please put following jars to eresearch/lib directory of jboss server:

1. c3p0-0.9.2.1
2. hibernate-c3p0-3.3.0.GA
3. mchange-commons-java-0.2.3.4



Steps to deploy build:
- Stop the eResearch application.
- execute db patches.
- Clear the tmp and work folders under JBoss as usual and start the eResearch application.



