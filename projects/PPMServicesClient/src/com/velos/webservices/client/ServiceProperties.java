/**
 * This class deals with the properties, to put into the test cases.
 */
package com.velos.webservices.client;

import java.util.ResourceBundle;

public class ServiceProperties {
	
	private static ResourceBundle bundle= ResourceBundle.
						getBundle("com/velos/webservices/client/Test");
	//Properties for Junit Tests.
	public static final String STUDY_SERVICE_WSDL_URL = "study.service.wsdl.url";
	public static final String PAT_DEMOGRAPHICS_SERVICE_WSDL_URL = "patientdemographics.service.wsdl.url";
	public static final String STUDY_PATIENTS_SERVICE_WSDL_URL = "study.patients.service.wsdl.url";
	public static final String PATIENT_STUDIES_SERVICE_WSDL_URL = "patient.studies.service.wsdl.url";
	public static final String PATIENT_SCHEDULE_SERVICE_WSDL_URL = "patient.schedule.service.wsdl.url";
	public static final String MONITORING_SERVICE_WSDL_URL = "monitoring.service.wsdl.url";
	
	public static final String TEST_STUDY_NUMBER= "studyNo";
	public static final String TEST_STUDY_SYS_ID= "studyId";
	public static final String TEST_LOGIN_USERNAME= "login.userName";
	public static final String TEST_LOGIN_PASSWORD= "login.password";	
	public static final String TEST_ORGNAME= "study.orgname";

	//Properties for Test Study Team Member
	public static final String TEST_USERNAME= "userName";
	public static final String TEST_FIRSTNAME= "firstName";
	public static final String TEST_LASTNAME= "lastName";
	
	public static final String TEST_STUDY_SYS_ID_TO_UPDATE= 
		TEST_STUDY_SYS_ID + "_updated";
	
	public static final String TEST_FIRSTNAME_TO_UPDATE = 
		TEST_FIRSTNAME + "_updated";
	public static final String TEST_LASTNAME_TO_UPDATE=
		TEST_LASTNAME + "_updated";
	
	public static final String TEST_TEAM_ROLE_CODE=
		"teamRoleCode";
	public static final String TEST_TEAM_ROLE_CODE_TYPE= 
		"teamRoleCodeType";
	public static final String TEST_TEAM_ROLE_CODE_TO_UPDATE = 
		"teamRoleCodeToUpdate";
	public static final String TEST_TEAM_ROLE_CODE_TYPE_TO_UPDATE = 
		"teamRoleCodeTypeToUpdate";
	
	public static final String TEST_TEAM_MEMBER_STATUS_CODE_ACTIVE=
		"code.active";
	public static final String TEST_TEAM_MEMBER_STATUS_CODE_DEACTIVED=
		"code.deactived";
	//properties for Test StudySummary
	public static final String TEST_MORE_STUDY_DETAILS_KEY=
		"test.more.study.details.key";
	
	//Properties for Test Study status
	public static final String TEST_STUDY_OUTCOME_CODE="study.outcome.code";
	public static final String TEST_REVIEW_BOARD_CODE="review.board.code";
	public static final String TEST_STATUS_CODE="status.code";
	
	//Properties for Test Study Organization 
	public static final String TEST_STUDY_ORG_TYPE_CODE=
		"study.orgtype.code";
	public static final String TEST_STUDY_ORG_NEWENROLL_NOTIFICTO_LOGNAME_USR1=
		TEST_USERNAME;
	public static final String TEST_STUDY_ORG_APPRENROLL_NOTIFICTO_LOGNAME_USR1 =
		TEST_USERNAME;
	
	//Properties for Test Study Organization Update
	public static final String TEST_STUDY_ORG_NAME_TOUPDATE=
		"study.orgname.toupdate";
	public static final String TEST_STUDY_ORG_TYPE_CODE_TOUPDATE=
		"study.orgtype.code.toupdate";
	
	public static final String MESSAGE_TRAFFIC_FROM_DATE= "message.traffic.from.date";
	public static final String MESSAGE_TRAFFIC_TO_DATE= "message.traffic.to.date";
	public static final String TEST_PATIENT_ID = "test.patient.id";
	
	public static final String MESSAGE_TRAFFIC_FROM_DATE_YYYY= "message.traffic.from.date.yyyy";
	public static final String MESSAGE_TRAFFIC_FROM_DATE_MM= "message.traffic.from.date.mm";
	public static final String MESSAGE_TRAFFIC_FROM_DATE_DD= "message.traffic.from.date.dd";
	
	public static final String MESSAGE_TRAFFIC_TO_DATE_YYYY= "message.traffic.to.date.yyyy";
	public static final String MESSAGE_TRAFFIC_TO_DATE_MM= "message.traffic.to.date.mm";
	public static final String MESSAGE_TRAFFIC_TO_DATE_DD= "message.traffic.to.date.dd";
	
	
	//JNDI properties
	public static final String JNDI_NAMING_FACTORY_INITIAL = "java.naming.factory.initial"; 
	public static final String JNDI_NAMING_PROVIDER_URL = "java.naming.provider.url"; 
	public static final String JNDI_NAMING_FACTORY_URL_PKGS  = "java.naming.factory.url.pkgs";
	
	public static final String STUDY_TOPIC_CONNECTION_FACTORY_JNDI= "study.topic.connectionfactory.jndi"; 
	public static final String STUDY_TOPIC_JNDI = "study.topic.jndi"; 
	public static final String STUDY_TOPIC_USERNAME = "study.topic.username"; 
	public static final String STUDY_TOPIC_PASSWORD = "study.topic.password";
	
	/** Method to read properties as key, and return value
	 *  after reading from ResourceBundle
	 **/
	public static String getProperty(String key) {
	    String value = bundle.getString(key);
	    return value;
	}
	
}
