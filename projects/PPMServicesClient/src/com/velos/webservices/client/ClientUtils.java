package com.velos.webservices.client;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.TrustManager;

import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.configuration.security.ProxyAuthorizationPolicy;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.endpoint.Endpoint;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transport.http.HttpAuthSupplier;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.apache.cxf.ws.security.wss4j.WSS4JOutInterceptor;
import org.apache.ws.security.WSConstants;
import org.apache.ws.security.handler.WSHandlerConstants;

import com.velos.services.StudySEI;
import com.velos.services.StudyService;
import com.velos.webservices.client.ServiceProperties;
import com.velos.webservices.client.SimpleCallbackHandler;

public class ClientUtils {
	




		/**
		 * Utility method to get StudyService for the 
		 *  user logging in.
		 */
		
		public static StudySEI getStudyService() throws MalformedURLException{
			StudyService service = null;
			StudySEI studyService = null;
			//setup the service port
			if (studyService == null){
			// Uncomment following code for GE machine	
//				System.getProperties().put( "proxySet", "true" );
//				System.getProperties().put( "proxyHost", "3.20.109.241" );
//				System.getProperties().put( "proxyPort", "88" );
				URL serviceEndpointWSDL = 
					new URL(ServiceProperties.
								getProperty(
										ServiceProperties.STUDY_SERVICE_WSDL_URL));
				
				service = new StudyService(serviceEndpointWSDL);
				studyService = service.getStudyWSPort(); 
				Client client = ClientProxy.getClient(studyService);
				addSecurityInterceptor(client);
			}
			return studyService;
		}
		

		/**
		 * method to generate test study number.
		 */
		
		@SuppressWarnings("deprecation")
		public static String getNewTestStudyNumber() {
			String strTime= String.valueOf(new Date().getTime());
			String testStudyNumber = "TestStudy"+strTime;
			return testStudyNumber;
		}
		
		//Add Security interceptor
	  	private static void addSecurityInterceptor(Client client){
	  		
	  		// Uncomment following code for GE machine		
//	  		HTTPConduit http = (HTTPConduit) client.getConduit();          
//			ProxyAuthorizationPolicy policy = new ProxyAuthorizationPolicy(); 
//			policy.setUserName("501860659"); 
//			policy.setPassword("velos2010v"); 
//			http.setProxyAuthorization(policy); 
			
			  Endpoint cxfEndpoint = client.getEndpoint();
			Map<String,Object> outProps = new HashMap<String,Object>();
			outProps.put(WSHandlerConstants.ACTION,
					WSHandlerConstants.USERNAME_TOKEN);
			outProps.put(WSHandlerConstants.PASSWORD_TYPE, 
					WSConstants.PW_TEXT);
			outProps.put(WSHandlerConstants.PW_CALLBACK_CLASS, 
					SimpleCallbackHandler.class.getName());
			outProps.put(WSHandlerConstants.USER, 
					ServiceProperties.getProperty(ServiceProperties.TEST_LOGIN_USERNAME));

			WSS4JOutInterceptor wssOut = new WSS4JOutInterceptor(outProps);
			cxfEndpoint.getOutInterceptors().add(wssOut);
	  	}

	}

