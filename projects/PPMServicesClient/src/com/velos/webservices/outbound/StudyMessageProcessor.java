/**
 * Created On Apr 19, 2011
 */
package com.velos.webservices.outbound;

import java.io.PrintStream;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import javax.xml.ws.Holder;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.ge.gehcit.cb.ppm_dt.ObjectFactory;
import com.ge.gehcit.cb.ppm_ws_velos.SPPMSponsor;
import com.ge.gehcit.cb.ppm_ws_velos.SPPMStudyPhase;
import com.ge.gehcit.cb.ppm_ws_velos.SPPMStudyStatus;
import com.ge.gehcit.cb.ppm_ws_velos.SStudyClassification;
import com.ge.gehcit.cb.ppm_ws_velos_study.StudyVELOSSoap;
import com.ge.gehealthcare.StatusCollection;
import com.velos.services.OperationException_Exception;
import com.velos.services.Study;
import com.velos.services.StudyIdentifier;
import com.velos.services.StudySEI;
import com.velos.services.StudyStatus;
import com.velos.services.StudyStatusIdentifier;
import com.velos.webservices.client.ClientUtils;
import com.velos.webservices.ppm.PPMClientUtils;

/**
 * @author Kanwaldeep
 *
 */
public class StudyMessageProcessor {

	/**
	 * 
	 */
	public void processStudyMessage(CustomHandler handler) {
		
		StudyIdentifier studyIdentifier  = (StudyIdentifier) handler.getParentOID().get(IdentifierConstants.STUDY_IDENTIFIER); 
		String studyNumber = studyIdentifier.getStudyNumber(); 
	// check if study number is of interest then process it otherwise discard message and return from this method. 
		
		boolean callStudy = true; 
	 
		a: for(Change change: handler.getChanges())
		{
			
			if(change != null)
			{
				String module = change.getModule();
				String action = change.getAction(); 
				
				if(module.equalsIgnoreCase("study_status"))
				{
					StudyStatusIdentifier studyStatusIdentifier = (StudyStatusIdentifier) change.getIdentifier(); 
					String statusCodeDesc = change.getAdditionalTags().get("statusCodeDesc"); 
					String statusSubType = change.getAdditionalTags().get("statusSubType"); 
					// If status is in list PPM needs update for than continue otherwise don't do anything
					List<String> ppMStatusSubTypes = new ArrayList<String>(); 
					//populate List from table or properties file
					if(ppMStatusSubTypes.contains(statusSubType)) 			callStudy = true; 
					break a; 
				}
			}
		}
		// Now we got study from Velos lets map it to PPM . 
		
		if(callStudy)
		{
		Study study = getStudy(studyIdentifier);
		printVelosStudy(study); 
	 	// Create Study in PPM 
	 	createStudyInPPM(study); 
		}
	}
	
	
	@SuppressWarnings("unused")
	private StudyStatus getStudyStatus(StudyIdentifier studyIdentifier, StudyStatusIdentifier studyStatusIdentifier)
	{
		StudyStatus status = null; 
		try {
			StudySEI studyService= ClientUtils.getStudyService();
			status = studyService.getStudyStatus(studyStatusIdentifier); 
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//StudyStatus status = studyService.
		catch (OperationException_Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return status; 
	}
	
	
	private Study getStudy(StudyIdentifier studyIdentifier)
	{
		Study study = null ;
		try
		{
			StudySEI studyService = ClientUtils.getStudyService(); 
			study = studyService.getStudy(studyIdentifier); 
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (OperationException_Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return study; 
	}
	
	
	private void createStudyInPPM(Study velosStudy)
	{
		StudyVELOSSoap ppmStudyService = PPMClientUtils.getStudyService();
		com.ge.gehcit.cb.ppm_dt.Study ppmStudy =  new com.ge.gehcit.cb.ppm_dt.Study(); 
		ObjectFactory factory = new ObjectFactory(); 
		ppmStudy.setMnemonic(velosStudy.getStudySummary().getStudyNumber()); 
		//phase
		
		SPPMStudyPhase studyPhase = new SPPMStudyPhase(); 
		//look up dictionary map
		//NOTE  - please keep dictionary map in table this is just example .
		//This may or may not be correct as I need to discuss. 
		Map<String, Long> phaseDictionaryMap = new HashMap<String, Long>(); 
		phaseDictionaryMap.put("phase1", new Long(1)); 
		phaseDictionaryMap.put("phase2", new Long(2)); 
		phaseDictionaryMap.put("phase3", new Long(3)); 
		phaseDictionaryMap.put("phase4", new Long(4)); 
		studyPhase.setNumber(phaseDictionaryMap.get(velosStudy.getStudySummary().getPhase().getCode())); 
		ppmStudy.setPhase(studyPhase); 
		
		SStudyClassification classification = new SStudyClassification(); 
		Map<String, Long> classificationDictionaryMap = new HashMap<String, Long>(); 
		classificationDictionaryMap.put("Federal", new Long(1)); 
		classificationDictionaryMap.put("Pharmaceutical", new Long(2)); 
		classificationDictionaryMap.put("Institutional", new Long(3));
		classification.setNumber(classificationDictionaryMap.get(velosStudy.getStudySummary().getSponsorName().getCode()));
		
		ppmStudy.setClassification(factory.createStudyClassification(classification));
		
		//sponsor
		Map<String, Long> sponsorDictionaryMap = new HashMap<String, Long>(); 
		// populate sponsor Dictionary Map from mapping table 
		SPPMSponsor sponsor = new SPPMSponsor(); 
		sponsor.setNumber(new Long(1));
		
		ppmStudy.setSponsor(sponsor); 
	 
		
		//Status
		SPPMStudyStatus status = new SPPMStudyStatus(); 
		status.setNumber(new Long(100)); // velos Study code = 100
		ppmStudy.setStatus(status); 
		
		
		//version
		ppmStudy.setVersion("1"); // not sure about mapping (Dartmouth is supposed to tell us correct mapping). 
		ppmStudy.setName(velosStudy.getStudySummary().getStudyTitle()); 
	
	
		/* ctsStudyId		 */
		
		ppmStudy.setCtmsStudyId(factory.createStudyCtmsStudyId(velosStudy.getStudyIdentifier().getOID()));
		Holder<String> result = new Holder<String>(); 
		Holder<StatusCollection> stHolder = new Holder<StatusCollection>(); 
		ppmStudyService.save(ppmStudy, result, stHolder);
		//	velosSoap.save(ppmStudy, "",result , stHolder);
	
		System.out.println(result.value); 
		System.out.println(stHolder.value.toString());
		 
		
	}
	
	
	public void printVelosStudy(Study study)
	{

		JAXBElement<Study> element = new JAXBElement<Study>(new QName("Study"), Study.class, study); 
	
		// do mapping send to external system 
		JAXBContext context;
		try {
			context = JAXBContext.newInstance("com.velos.services");				
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true); 
			PrintStream p = System.out; 
			m.marshal(element, p);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}

}
