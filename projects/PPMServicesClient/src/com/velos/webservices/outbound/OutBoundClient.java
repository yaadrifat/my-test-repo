/**
 * Created On May 10, 2011
 */
package com.velos.webservices.outbound;

import java.util.Hashtable;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.jms.TopicSubscriber;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.velos.webservices.client.ServiceProperties;

/**
 * @author Kanwaldeep
 *
 */
public class OutBoundClient implements MessageListener, ExceptionListener{
	
	/* (non-Javadoc)
	 * @see javax.jms.ExceptionListener#onException(javax.jms.JMSException)
	 */
	
	private boolean running = true; 
	
	private String connectionFactoryJNDI = "ConnectionFactory"; 
	private String topicJNDI =""; 
	private String userName = "guest"; 
	private String password = "guest"; 
	private String clientID = "client1"; 
	private String subscriberName = "Subscriber1"; 
	
	
	public String getConnectionFactoryJNDI() {
		return connectionFactoryJNDI;
	}

	public void setConnectionFactoryJNDI(String connectionFactoryJNDI) {
		this.connectionFactoryJNDI = connectionFactoryJNDI;
	}

	public String getTopicJNDI() {
		return topicJNDI;
	}

	public void setTopicJNDI(String topicJNDI) {
		this.topicJNDI = topicJNDI;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getClientID() {
		return clientID;
	}

	public void setClientID(String clientID) {
		this.clientID = clientID;
	}

	public String getSubscriberName() {
		return subscriberName;
	}

	public void setSubscriberName(String subscriberName) {
		this.subscriberName = subscriberName;
	}
	
	public void onException(JMSException arg0) {
		arg0.printStackTrace(); 
		
	}

	/* (non-Javadoc)
	 * @see javax.jms.MessageListener#onMessage(javax.jms.Message)
	 */
	public void onMessage(Message msg) {
		System.out.println("got New message"); 
		if (msg instanceof TextMessage) {
            
        	TextMessage xml = (TextMessage) msg; 
        	try {
        		String xmlString = xml.getText();      
        		
        	//	SAX Parser 
    			OutBoundSAXProcessor sax = new OutBoundSAXProcessor(); 
    			sax.processBySAXParser(xmlString); 
        	} catch (JMSException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
        	
			
		}
	}
	
	public void startListening()
	{

		//ConnectionFactory JNDI  
		ConnectionFactory factory = getConnectionFactory(connectionFactoryJNDI); 
		try {
			Connection conn = factory.createConnection(userName, password); 
			//Connection conn = factory.createConnection(); 
			conn.setClientID(clientID); 
			Session session  = conn.createSession(false, Session.AUTO_ACKNOWLEDGE); 
			Topic topic = getTopic(topicJNDI); 
			//MessageConsumer sub = session.createConsumer(topic); 
			TopicSubscriber sub = session.createDurableSubscriber(topic, subscriberName);
			sub.setMessageListener(this); 
			conn.setExceptionListener(this); 
			conn.start(); 
			while(running) // listen messages
			{
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				} 
			}
			
			conn.close(); 
			
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
	}
	

	
	public Object lookup(String JNDIName)
	{
		Hashtable<String, String> env = new Hashtable<String, String>(); 
		InitialContext context = null;
		Object obj = null; 
		try {
			env.put(Context.INITIAL_CONTEXT_FACTORY, ServiceProperties.getProperty(ServiceProperties.JNDI_NAMING_FACTORY_INITIAL)); 
			env.put(Context.PROVIDER_URL, ServiceProperties.getProperty(ServiceProperties.JNDI_NAMING_PROVIDER_URL)); 
			env.put(Context.URL_PKG_PREFIXES,ServiceProperties.getProperty(ServiceProperties.JNDI_NAMING_FACTORY_URL_PKGS)); 
			context = new InitialContext(env);
			obj = context.lookup(JNDIName);
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return  obj; 
	}
	
	
	public ConnectionFactory getConnectionFactory(String connectionFactoryJNDI)
	{
		
		ConnectionFactory factory = (ConnectionFactory) lookup(connectionFactoryJNDI); 		
		return factory; 
	}
	
		
	public Topic getTopic(String topicJNDI){
		Topic topic = (Topic) lookup(topicJNDI); 
		return topic; 
	}
	
	public void close()
	{
		this.running = false; 
	}
}
	
	

