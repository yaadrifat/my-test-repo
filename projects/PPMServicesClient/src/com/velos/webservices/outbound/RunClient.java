/**
 * Created On Apr 6, 2011
 */
package com.velos.webservices.outbound;

import java.net.Authenticator;

import com.velos.webservices.client.ServiceProperties;

/**
 * @author Kanwaldeep
 *
 */
public class RunClient {
	
	
	public static void main(String[] args)
	{
		//Authenticator.setDefault(new SimpleAuthenticator("501860659", "velos2010v")); 
		OutBoundClient client = new OutBoundClient(); 
		client.setTopicJNDI(ServiceProperties.getProperty(ServiceProperties.STUDY_TOPIC_JNDI)); 
		client.setConnectionFactoryJNDI(ServiceProperties.getProperty(ServiceProperties.STUDY_TOPIC_CONNECTION_FACTORY_JNDI)); 
		client.setUserName(ServiceProperties.getProperty(ServiceProperties.STUDY_TOPIC_USERNAME));
		client.setPassword(ServiceProperties.getProperty(ServiceProperties.STUDY_TOPIC_PASSWORD)); 
		
		client.startListening();
	}
	


}
