/**
 * Created On Apr 15, 2011
 */
package com.velos.webservices.outbound;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.velos.services.SimpleIdentifier;
import com.velos.services.StudyIdentifier;
import com.velos.services.StudyStatusIdentifier;

/**
 * @author Kanwaldeep
 *
 */
public class CustomHandler extends DefaultHandler {
	/*
	 * <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<changes>
    <change>
        <action>UPDATE</action>
        <statusSubType>app_GCRC</statusSubType>
        <statusCodeDesc>GCRC - Approved</statusCodeDesc>
        <identifier xsi:type="studyStatusIdentifier" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            <OID>15710a91-6a5d-401f-b10c-e924db39757d</OID>
        </identifier>
        <module>study_status</module>
        <timeStamp>APR-14-2011 12:20:20</timeStamp>
    </change>
    <change>
        <action>CREATE</action>
        <statusSubType>chr_consent</statusSubType>
        <statusCodeDesc>CHR (IRB) Submission - Consent Form</statusCodeDesc>
        <identifier xsi:type="studyStatusIdentifier" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            <OID>60355c78-65fb-4d57-af6d-319ac5fd0948</OID>
        </identifier>
        <module>study_status</module>
        <timeStamp>APR-14-2011 12:20:20</timeStamp>
    </change>
    <parentIdentifier>
        <id xsi:type="studyIdentifier" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            <OID>c40aeca5-9bc5-4083-9096-69c5c7d3b5cf</OID>
            <studyNumber>emaven p1v2</studyNumber>
        </id>
    </parentIdentifier>
</changes>

	 */
	private Map<String, SimpleIdentifier> parentOID = new HashMap<String, SimpleIdentifier>();
	 
	 
	private boolean parentIdentifier = false;
	private List<Change> changes =  new ArrayList<Change>();
	
	private boolean isChange = false; 
	private boolean isOID = false; 
	private boolean isStudyNumber = false; 
	private boolean isId = false; 
	private boolean isAction = false; 
	private boolean isModule = false; 
	private boolean isTimeStamp = false;

	
	private String idType = ""; 
	private String OID = ""; 
	
	// change elements 
	private String action = ""; 
	private String module = ""; 
	private String timeStamp = ""; 
	private Map<String, String> additionalInfoMap = new HashMap<String, String>(); 
	private String additionalInfoTag = null; 
	
	
	// StudyIdentifier additional information
	private String studyNumber = "";
	
	
	public Map getParentOID()
	{
		return parentOID; 
	}
	
	
	public List<Change> getChanges() {
		return changes;
	}

	
	public void startElement(String uri, String localName,
			String qName, Attributes attributes)
	throws SAXException {

		//System.out.println("Start Element :" + qName);

		if (qName.equalsIgnoreCase("parentIdentifier")) {
			parentIdentifier = true;
			return; 
		}
		
		if(qName.equalsIgnoreCase("change"))
		{
			isChange = true; 
			return;
		}


		if (qName.equalsIgnoreCase("OID")) {
			isOID = true;
			return;
		}
		
		if(qName.equalsIgnoreCase("id"))
		{
			idType = attributes.getValue("xsi:type"); 
			isId = true; 
			return;
		}
	
		if(qName.equalsIgnoreCase("studyNumber"))
		{
			isStudyNumber = true; 
			return;
		}
		
		if(isChange)
		{
			if(qName.equalsIgnoreCase("action"))
			{
				isAction = true; 
				return; 
			}
			if(qName.equalsIgnoreCase("module"))
			{
				isModule = true; 
				return; 
				
			}
			if(qName.equalsIgnoreCase("timeStamp"))
			{
				isTimeStamp = true; 
				return; 
			}
			
			
			if(qName.equalsIgnoreCase("identifier"))
			{
				idType = attributes.getValue("xsi:type"); 
				return; 
			}
			
			additionalInfoTag = qName; 
			
		}

	}

	public void endElement(String uri, String localName,
			String qName)
	throws SAXException {

		if(qName.equalsIgnoreCase("parentIdentifier"))
		{
			
				parentIdentifier = false; 
		}
		
		if(qName.equalsIgnoreCase("id"))
		{
			
			isId = false; 
			// Done populating additional Tags for identifier - lets create Identifier Object
			
			if(idType.equalsIgnoreCase(IdentifierConstants.STUDY_IDENTIFIER))
			{
				StudyIdentifier identifier = new StudyIdentifier(); 
				identifier.setOID(OID); 
				identifier.setStudyNumber(studyNumber);
				parentOID.put(IdentifierConstants.STUDY_IDENTIFIER, identifier ); 
			}
		}
		
	
		if(qName.equalsIgnoreCase("change"))
		{
			isChange = false; 
			Change change = new Change(); 
			change.setAction(action); 
			change.setModule(module); 
			if(idType.equalsIgnoreCase(IdentifierConstants.STUDY_STATUS_IDENTIFIER))
			{
				StudyStatusIdentifier identifier = new StudyStatusIdentifier(); 
				identifier.setOID(OID); 
				change.setIdentifier(identifier); 
			}
		
			change.setTimeStamp(timeStamp); 
			change.setAdditionalTags(additionalInfoMap); 
			changes.add(change); 
			additionalInfoMap = new HashMap<String, String>(); 
		}
			
	}

	public void characters(char ch[], int start, int length)
	throws SAXException {
		
		if(isOID)
		{
			 OID =  new String(ch, start, length); 
			 isOID = false; 			
		}
		
		if(isStudyNumber) {
			studyNumber =  new String(ch, start, length);
			isStudyNumber = false; 			
		}
	
			
		if(isAction)
		{
			action = new String(ch, start, length); 
			isAction = false; 
		}
			
		if(isModule)
		{
			module = new String(ch, start, length); 
			isModule = false;
		}
		
		if(isTimeStamp)
		{
			timeStamp =  new String(ch, start, length);
			isTimeStamp = false; 
		}
			
		if(additionalInfoTag != null)
		{
			additionalInfoMap.put(additionalInfoTag, new String(ch, start, length)); 
			additionalInfoTag = null; 				
		}
			

	}
	

}
