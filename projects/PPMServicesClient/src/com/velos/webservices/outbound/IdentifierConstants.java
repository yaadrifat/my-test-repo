/**
 * Created On Apr 7, 2011
 */
package com.velos.webservices.outbound;

/**
 * @author Kanwaldeep
 *
 */
public interface IdentifierConstants {
	
	public static String STUDY_IDENTIFIER ="studyIdentifier"; 
	public static String STUDY_STATUS_IDENTIFIER = "studyStatusIdentifier"; 
	public static String ORGANIZATION_IDENTIFIER = "organizationIdentifier"; 
	public static String USER_IDENTIFIER = "userIdentifier"; 

}
