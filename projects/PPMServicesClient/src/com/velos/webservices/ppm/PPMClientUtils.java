/**
 * Created On May 11, 2011
 */
package com.velos.webservices.ppm;

import java.util.HashMap;
import java.util.Map;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.endpoint.Endpoint;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.ws.security.wss4j.WSS4JOutInterceptor;
import org.apache.ws.security.WSConstants;
import org.apache.ws.security.handler.WSHandlerConstants;

import com.ge.gehcit.cb.ppm_ws_velos_study.StudyVELOS;
import com.ge.gehcit.cb.ppm_ws_velos_study.StudyVELOSSoap;

/**
 * @author Kanwaldeep
 *
 */
public class PPMClientUtils {
	
	
	public static StudyVELOSSoap getStudyService()
	{
		StudyVELOS service = new StudyVELOS();		
		StudyVELOSSoap velosSoap = service.getStudyVELOSSoap(); 
		ClientCallBackHandler clientInterceptor = new ClientCallBackHandler();
		Client client = ClientProxy.getClient(velosSoap); 
		Endpoint cxfEndpoint = client.getEndpoint(); 
		Map<String,Object> outProps = new HashMap<String,Object>();

		outProps.put(WSHandlerConstants.ACTION, WSHandlerConstants.USERNAME_TOKEN);
		// Specify our username
		outProps.put(WSHandlerConstants.USER, "VELOSWS");
		// Password type : plain text
		outProps.put(WSHandlerConstants.PASSWORD_TYPE, WSConstants.PW_TEXT);
		// for hashed password use:
		//properties.setProperty(WSHandlerConstants.PASSWORD_TYPE, WSConstants.PW_DIGEST);
		// Callback used to retrive password for given user.
		outProps.put(WSHandlerConstants.PW_CALLBACK_CLASS, ClientCallBackHandler.class.getName());
		
		WSS4JOutInterceptor wssOut = new WSS4JOutInterceptor(outProps);
		cxfEndpoint.getOutInterceptors().add(wssOut);	
		
		return velosSoap; 
	}

}
