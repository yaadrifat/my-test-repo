package com.velos.webservices.ppm;

import java.io.IOException;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

import org.apache.ws.security.WSPasswordCallback;


public class ClientCallBackHandler implements CallbackHandler {

	public void handle(Callback[] arg0) throws IOException,
			UnsupportedCallbackException {
			
			WSPasswordCallback pc = (WSPasswordCallback) arg0[0]; 
			pc.setPassword("VELOSWS");
	}

}
