set define off;

declare
v_reportpk NUMBER := 0;
begin
	select max(pk_report) + 1 into v_reportpk from er_report;
	if (v_reportpk > 0) then
	commit;
		insert into er_report (pk_report, REP_NAME, REP_DESC, REP_SQL, FK_ACCOUNT, REP_HIDE, REP_COLUMNS, REP_FILTERBY, GENERATE_XML, REP_TYPE, REP_SQL_CLOB, REP_FILTERKEYWORD, REP_FILTERAPPLICABLE) 
		select v_reportpk, s.REP_NAME, s.REP_DESC, s.REP_SQL, s.FK_ACCOUNT, 'N', s.REP_COLUMNS, s.REP_FILTERBY, s.GENERATE_XML, s.REP_TYPE, s.REP_SQL_CLOB, s.REP_FILTERKEYWORD, s.REP_FILTERAPPLICABLE 
		from er_report s where s.pk_report = 181;
		commit;
		
		update er_repxsl set pk_repxsl = v_reportpk , fk_report = v_reportpk where fk_report = 181;
		commit;
		
		delete from er_report where pk_report = 181;
		commit;
	end if;
end;
/