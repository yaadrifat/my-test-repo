SET define OFF;

-- Update into ER_REPORT
DECLARE
  v_record_exists NUMBER := 0;
BEGIN
  SELECT COUNT(*) INTO v_record_exists FROM er_report WHERE pk_report = 181;
  IF (v_record_exists != 0) THEN
    Update ER_REPORT Set REP_FILTERBY = 'Date, Reporting Organization, Enrolling Organization, Treating Organization, Clinical Research Category',
    REP_COLUMNS = 'Disease Site, Newly Registered Patients, Total Patients Newly Enrolled In Interventional Treatment Trials'  where pk_report = 181;
    COMMIT;
    
  UPDATE er_report SET rep_sql = 'SELECT disease_site,
  repsitename,
  enrsitename,
  studyTypes,
  Anatomicsitenull,
  COUNT 
FROM
  ( select codelst_desc as disease_site,
    (select site_name from er_site where pk_site in (:repOrgId)) as repsitename,
     SITE_NAME as enrsitename,
    (SELECT wm_concat(CODELST_DESC) from er_codelst where pk_codelst IN (:studyType) AND  NVL(CODELST_CUSTOM_COL1, ''noninterventional'') <> ''interventional'') as studyTypes,
  (SELECT wm_concat(''[Study: ''||STUDY_NUMBER ||'' & ''||''Patient: ''||(select PER_CODE from er_per where PK_PER = patprt.fk_per)) || '']''
    FROM ER_PATPROT patprt,
      er_patstudystat patst,
      er_study stdy,
      er_studysites stsite
    WHERE stdy.fk_account = :sessAccId
    AND stdy.pk_study     = patprt.fk_study
    AND patprt.FK_PER     = patst.fk_per
    AND stsite.fk_study   =stdy.pk_study
    AND patprt.fk_study   = patst.fk_study
    AND FK_CODELST_STAT   =
      (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp = ''enrolled'')
    AND PATSTUDYSTAT_DATE BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
    AND fk_site_enrolling  = pk_site
    AND stsite.FK_SITE             IN (:repOrgId)
    AND (PATPROT_TREATINGORG IS NULL OR NVL(PATPROT_TREATINGORG,0) IN (:trtOrgId))
    AND PATPROT_OTHR_DIS_CODE is null 
    AND FK_CODELST_TYPE IN
      (SELECT pk_codelst FROM ER_CODELST WHERE pk_codelst IN (:studyType) AND CODELST_CUSTOM_COL1=''interventional''
      )
    ) AS Anatomicsitenull,
  (SELECT count(*)
      FROM ER_PATPROT patprt, er_patstudystat patst,er_study stdy, er_studysites stsite
      WHERE  stdy.fk_account = :sessAccId
      AND stdy.pk_study = patprt.fk_study
      AND patprt.FK_PER = patst.fk_per
      AND stsite.fk_study=stdy.pk_study
      AND patprt.fk_study  = patst.fk_study
      AND FK_CODELST_STAT = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp = ''enrolled'')
      AND PATSTUDYSTAT_DATE BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
      AND fk_site_enrolling = pk_site
      AND stsite.FK_SITE in (:repOrgId)
      AND (PATPROT_TREATINGORG IS NULL OR NVL(PATPROT_TREATINGORG,0) IN (:trtOrgId))
      AND PK_CODELST = f_to_number(patprt.PATPROT_OTHR_DIS_CODE)
      AND FK_CODELST_TYPE in (
      select pk_codelst from ER_CODELST where pk_codelst in (:studyType) and CODELST_CUSTOM_COL1=''interventional''
      )
      ) as count
  FROM er_codelst, er_site site
  WHERE codelst_type = ''disease_site''
  AND PK_SITE in (:enrOrgId)
  ) order by enrsitename '
    WHERE pk_report = 181;
    COMMIT;

UPDATE er_report SET rep_sql_clob = 'SELECT disease_site,
  repsitename,
  enrsitename,
  studyTypes,
  Anatomicsitenull,
  COUNT 
FROM
  ( select codelst_desc as disease_site,
    (select site_name from er_site where pk_site in (:repOrgId)) as repsitename,
     SITE_NAME as enrsitename,
    (SELECT wm_concat(CODELST_DESC) from er_codelst where pk_codelst IN (:studyType) AND  NVL(CODELST_CUSTOM_COL1, ''noninterventional'') <> ''interventional'') as studyTypes,
   (SELECT wm_concat(''[Study: ''||STUDY_NUMBER ||'' & ''||''Patient: ''||(select PER_CODE from er_per where PK_PER = patprt.fk_per)) || '']''
    FROM ER_PATPROT patprt,
      er_patstudystat patst,
      er_study stdy,
      er_studysites stsite
    WHERE stdy.fk_account = :sessAccId
    AND stdy.pk_study     = patprt.fk_study
    AND patprt.FK_PER     = patst.fk_per
    AND stsite.fk_study   =stdy.pk_study
    AND patprt.fk_study   = patst.fk_study
    AND FK_CODELST_STAT   =
      (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp = ''enrolled'')
    AND PATSTUDYSTAT_DATE BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
    AND fk_site_enrolling  = pk_site
    AND stsite.FK_SITE             IN (:repOrgId)
    AND (PATPROT_TREATINGORG IS NULL OR NVL(PATPROT_TREATINGORG,0) IN (:trtOrgId))
    AND PATPROT_OTHR_DIS_CODE is null 
    AND FK_CODELST_TYPE IN
      (SELECT pk_codelst FROM ER_CODELST WHERE pk_codelst IN (:studyType) AND CODELST_CUSTOM_COL1=''interventional''
      )
    ) AS Anatomicsitenull,
  (SELECT count(*)
      FROM ER_PATPROT patprt, er_patstudystat patst,er_study stdy, er_studysites stsite
      WHERE  stdy.fk_account = :sessAccId
      AND stdy.pk_study = patprt.fk_study
      AND patprt.FK_PER = patst.fk_per
      AND stsite.fk_study=stdy.pk_study
      AND  patprt.fk_study  = patst.fk_study
      AND FK_CODELST_STAT = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp = ''enrolled'')
      AND PATSTUDYSTAT_DATE BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
      AND fk_site_enrolling in (pk_site)
      AND stsite.FK_SITE in (:repOrgId)
      AND (PATPROT_TREATINGORG IS NULL OR NVL(PATPROT_TREATINGORG,0) IN (:trtOrgId))
      AND PK_CODELST = f_to_number(patprt.PATPROT_OTHR_DIS_CODE)
      AND FK_CODELST_TYPE in (
      select pk_codelst from ER_CODELST where pk_codelst in (:studyType) and CODELST_CUSTOM_COL1=''interventional''
      )
      ) as count
  FROM er_codelst, er_site
  WHERE codelst_type = ''disease_site''
  AND PK_SITE in (:enrOrgId)
  ) order by enrsitename '
    WHERE pk_report = 181;
    COMMIT;
	
  END IF;
  END;
/

