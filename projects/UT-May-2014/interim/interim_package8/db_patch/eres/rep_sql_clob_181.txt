SELECT disease_site,
  disease_site_subtyp,
  repsitename,
  enrsitename,
  studyTypes,
  Anatomicsitenull,
  SUM(pat_ds_count) COUNT 
FROM
  ( select codelst_desc as disease_site, codelst_subtyp as disease_site_subtyp,
    (select site_name from er_site where pk_site in (:repOrgId)) as repsitename,
     SITE_NAME as enrsitename,
    pkg_util.f_join(cursor(SELECT (CODELST_DESC) from er_codelst where pk_codelst IN (:studyType) AND  NVL(CODELST_CUSTOM_COL1, 'noninterventional') <> 'interventional'), ';') as studyTypes,
    pkg_util.f_join(cursor(SELECT ('[Study: '||STUDY_NUMBER ||' & '||'Patient: '||(select PER_CODE from er_per where PK_PER = patprt.fk_per) || ']')
    FROM ER_PATPROT patprt,
      er_patstudystat patst,
      er_study stdy,
      er_studysites stsite
    WHERE stdy.fk_account = :sessAccId
    AND stdy.pk_study     = patprt.fk_study
    AND patprt.FK_PER     = patst.fk_per
    AND stsite.fk_study   =stdy.pk_study
    AND patprt.fk_study   = patst.fk_study
	AND patprt.patprot_stat = 1
    AND FK_CODELST_STAT   =
      (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp = 'enrolled')
    AND PATSTUDYSTAT_DATE BETWEEN TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT)
    AND fk_site_enrolling  = pk_site
    AND stsite.FK_SITE             IN (:repOrgId)
    AND (PATPROT_TREATINGORG IS NULL OR NVL(PATPROT_TREATINGORG,0) IN (:trtOrgId))
    AND PATPROT_OTHR_DIS_CODE IS NULL
    AND FK_CODELST_TYPE IN
      (SELECT pk_codelst FROM ER_CODELST WHERE pk_codelst IN (:studyType) AND CODELST_CUSTOM_COL1='interventional'
      )
    ), '') AS Anatomicsitenull,
  (SELECT count(*)
      FROM ER_PATPROT patprt, er_patstudystat patst,er_study stdy, er_studysites stsite
      WHERE  stdy.fk_account = :sessAccId
      AND stdy.pk_study = patprt.fk_study
      AND patprt.FK_PER = patst.fk_per
      AND stsite.fk_study=stdy.pk_study
      AND patprt.fk_study  = patst.fk_study
      AND FK_CODELST_STAT = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp = 'enrolled')
      AND PATSTUDYSTAT_DATE BETWEEN TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT)
      AND fk_site_enrolling = pk_site
      AND stsite.FK_SITE in (:repOrgId)
      AND (PATPROT_TREATINGORG IS NULL OR NVL(PATPROT_TREATINGORG,0) IN (:trtOrgId))
      AND (PK_CODELST = f_to_number(patprt.PATPROT_OTHR_DIS_CODE)
	  OR (codelst_subtyp = 'leukemiaother' AND PK_CODELST in (select pk_codelst from er_codelst where codelst_type = 'disease_site' AND codelst_subtyp = 'leukemianototh')))
      AND FK_CODELST_TYPE in (
      select pk_codelst from ER_CODELST where pk_codelst in (:studyType) and CODELST_CUSTOM_COL1='interventional'
      )
      ) as pat_ds_count
  FROM er_site site, 
  (select pk_codelst, codelst_subtyp, codelst_desc from er_codelst
  WHERE codelst_type = 'disease_site' and codelst_subtyp not in ('multiplesites','leukemianototh','buccalcavityan')
  UNION
  select pk_codelst, 'leukemiaother', (select codelst_desc from er_codelst t1 where codelst_type ='disease_site' and codelst_subtyp = 'leukemiaother') 
  from er_codelst WHERE codelst_type ='disease_site' and codelst_subtyp = 'leukemianototh'
  UNION
  select pk_codelst, 'liporalcavitya', (select codelst_desc from er_codelst t1 where codelst_type ='disease_site' and codelst_subtyp = 'liporalcavitya') 
  from er_codelst WHERE codelst_type ='disease_site' and codelst_subtyp = 'buccalcavityan'
  ) temp_er_codelst
  WHERE PK_SITE in (:enrOrgId)
  )
  group by disease_site, disease_site_subtyp, repsitename, enrsitename, studyTypes, Anatomicsitenull
  order by enrsitename, 
  decode(disease_site_subtyp,'liporalcavitya',1,'esophagus',2,'stomach',3,'smallintestine',4,'colon',5,'rectum',6,'anus',7,'liver',8,'pancreas',9,'otherdigestive',10, 
'larynx',11,'lung',12,'otherrespirato',13,'bonesandjoints',14,'softtissue',15, 'melanomaskin',16,'kaposisarcom',17,'mycosisfungoid',18,'otherskin',19,'breastfemale',20,
'cervix',21,'corpusuteri',22,'ovary',23,'othermalegenit',24,'urinarybladder',25,'prostate',26,'othermalegenit',27,'kidney',28,'otherurinary',29,'eyeandorbit',30,
'brainnervoussy',31,'thyroid',32,'otherendocrine',33,'nonhodgkinslym',34,'hodgkinslympho',35,'multiplemyelom',36,'lymphoidleukem',37,'myeloidandmono',38,'leukemiaother',39,'otherhematopo',40,
'unknownsites',41,'illdefinedsite',42)