SET define OFF;

-- Update into ER_REPORT
DECLARE
  v_record_exists NUMBER := 0;
  v_reportPK NUMBER := 0;
BEGIN
	SELECT COUNT(*) INTO v_record_exists FROM er_report WHERE rep_name = 'CCSG Data Table 3';
	IF (v_record_exists != 0) THEN
		SELECT pk_report INTO v_reportPK FROM er_report WHERE rep_name = 'CCSG Data Table 3';
		UPDATE ER_REPORT SET 
			REP_SQL = '',
			FK_ACCOUNT = 0,
			FK_CODELST_TYPE = null,
			IP_ADD = null,
			REP_HIDE = 'N',
			REP_COLUMNS = 'Disease Site, Newly Registered Patients, Total Patients Newly Enrolled in Interventional treatment trials',,
			REP_FILTERBY = 'Date:ReportingOrg:EnrollingOrg:TreatingOrg:StudyType',
			GENERATE_XML = 1,
			REP_TYPE = '',
			REP_FILTERKEYWORD = '[DATEFILTER]D:repOrgId:enrOrgId:trtOrgId:studyType',
			REP_FILTERAPPLICABLE = 'date:repOrgId:enrOrgId:trtOrgId:studyType'
		WHERE PK_REPORT = v_reportPK;
		COMMIT;
	END IF;
END;
/
