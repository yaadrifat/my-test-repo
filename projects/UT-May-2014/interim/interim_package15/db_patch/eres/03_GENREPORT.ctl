LOAD DATA INFILE * INTO TABLE er_report
APPEND
FIELDS TERMINATED BY '|'
(PK_REPORT,
REP_NAME,
REP_DESC,
FK_ACCOUNT,
REP_HIDE,
GENERATE_XML,
REP_TYPE,
txt_file filler char,
"REP_SQL_CLOB" LOBFILE (txt_file) TERMINATED BY EOF NULLIF txt_file = 'NONE')
BEGINDATA
[pk_report_max_plus_one]|CCSG Data Table 4 (UT Specific)|CCSG Data Table 4 (UT Specific)|0|N|1|rep_nci|rep_sql_clob_182.txt
[pk_report_max_plus_two]|CCSG Data Table 3|CCSG Data Table 3|0|N|1|rep_nci|rep_sql_clob_181.txt