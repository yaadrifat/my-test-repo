SET define OFF;

Delete FROM er_repxsl where fk_report in (select pk_report from er_report WHERE rep_name = 'CCSG Data Table 4 (UT Specific)');

COMMIT;

Delete FROM er_report WHERE rep_name = 'CCSG Data Table 4 (UT Specific)';

COMMIT;