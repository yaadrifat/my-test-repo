SET define OFF;

DECLARE
filterID number := 0;
v_exists NUMBER := 0;
begin

	select count(*) into v_exists from er_repfilter where REPFILTER_KEYWORD = 'studyType';
	if (v_exists = 0) then
		select Max(PK_REPFILTER)+1 into filterID from er_repfilter;

		INSERT INTO er_repfilter (PK_REPFILTER,REPFILTER_COLUMN,REPFILTER_VALUESQL,REPFILTER_COLDISPNAME,REPFILTER_KEYWORD,REPFILTER_DISPDIV) 
		VALUES (filterID,'<td><DIV id="studytypeDIV"><FONT class = "comments"><A href="javascript:void(0);" onClick="return openLookup(document.reports,''viewId=6011&form=reports&seperator=,&defaultvalue=[ALL]&keyword=selstudyType|CODEDESC~paramstudyType|CODEPK|[VELHIDE]'',''dfilter=codelst_type=\''study_type\'' '')">Select Clinical Research Category</A> </FONT></DIV> </td>
	       	<td><DIV id="studytypedataDIV"><Input type="text" name="selstudyType" size="20" READONLY value="[ALL]">
	       	<Input type="hidden" name="paramstudyType" value="[ALL]"></DIV>
	       	</td>', 
		'select pk_codelst from  er_codelst Where  codelst_type=''study_type''','Clinical Research Category','studyType','studytypeDIV,studytypedataDIV');
		COMMIT;
	else 
		update er_repfilter set REPFILTER_COLUMN = '<td><DIV id="studytypeDIV"><FONT class = "comments"><A href="javascript:void(0);" onClick="return openLookup(document.reports,''viewId=6011&form=reports&seperator=,&defaultvalue=[ALL]&keyword=selstudyType|CODEDESC~paramstudyType|CODEPK|[VELHIDE]'',''dfilter=codelst_type=\''study_type\'' '')">Select Clinical Research Category</A> </FONT></DIV> </td>
	       	<td><DIV id="studytypedataDIV"><Input type="text" name="selstudyType" size="20" READONLY value="[ALL]">
	       	<Input type="hidden" name="paramstudyType" value="[ALL]"></DIV>
	       	</td>', REPFILTER_COLDISPNAME = 'Clinical Research Category' where REPFILTER_KEYWORD = 'studyType';
		COMMIT;
	end if;
	
	select count(*) into v_exists from er_repfilter where REPFILTER_KEYWORD = 'repOrgId';
	if (v_exists = 0) then
		select Max(PK_REPFILTER)+1 into filterID from er_repfilter;

		INSERT INTO er_repfilter (PK_REPFILTER,REPFILTER_COLUMN,REPFILTER_COLDISPNAME,REPFILTER_KEYWORD,REPFILTER_DISPDIV) 
		VALUES (filterID,'<td><DIV id="repOrgDIV"><FONT class = "comments"><A href="javascript:openLookup(document.reports,''viewId=6014&form=reports&maxselect=1&seperator=,&keyword=selrepOrgId|site_name~paramrepOrgId|pk_site|[VELHIDE]'','''')">Select Reporting Organization</A> </FONT></DIV> </td>
	       	<td><DIV id=''repOrgdataDIV''><Input TYPE="text" NAME="selrepOrgId" SIZE="20" READONLY VALUE=''[SESSSITENAME]''>
	       	<Input TYPE="hidden" NAME="paramrepOrgId" VALUE=''[SESSSITEID]''></DIV>
	       	</td>','Reporting Organization','repOrgId','repOrgDIV,repOrgdataDIV');
		COMMIT;
	end if;
	
	select count(*) into v_exists from er_repfilter where REPFILTER_KEYWORD = 'enrOrgId';
	if (v_exists = 0) then
		select Max(PK_REPFILTER)+1 into filterID from er_repfilter;

		INSERT INTO er_repfilter (PK_REPFILTER,REPFILTER_COLUMN,REPFILTER_VALUESQL,REPFILTER_COLDISPNAME,REPFILTER_KEYWORD,REPFILTER_DISPDIV) 
		VALUES (filterID,'<td><DIV id="enrOrgDIV"><FONT class = "comments"><A href="javascript:openLookup(document.reports,''viewId=6014&form=reports&seperator=,&defaultvalue=[ALL]&keyword=selenrOrgId|site_name~paramenrOrgId|pk_site|[VELHIDE]'','''')">Select Enrolling Organization</A> </FONT></DIV> </td>
	       	<td><DIV id=''enrOrgdataDIV''><Input TYPE="text" NAME="selenrOrgId" SIZE="20" READONLY VALUE=''[ALL]''>
	       	<Input TYPE="hidden" NAME="paramenrOrgId" VALUE=''[ALL]''></DIV>
	       	</td>',
		'SELECT pk_site FROM erv_allusersites_distinct WHERE fk_user = :sessUserId AND site_hidden <> 1','Enrolling Organization','enrOrgId','enrOrgDIV,enrOrgdataDIV');
		COMMIT;
	end if;
	
	select count(*) into v_exists from er_repfilter where REPFILTER_KEYWORD = 'trtOrgId';
	if (v_exists = 0) then
		select Max(PK_REPFILTER)+1 into filterID from er_repfilter;

		INSERT INTO er_repfilter (PK_REPFILTER,REPFILTER_COLUMN,REPFILTER_VALUESQL,REPFILTER_COLDISPNAME,REPFILTER_KEYWORD,REPFILTER_DISPDIV) 
		VALUES (filterID,'<td><DIV id="trtOrgDIV"><FONT class = "comments"><A href="javascript:openLookup(document.reports,''viewId=6014&form=reports&seperator=,&defaultvalue=[ALL]&keyword=seltrtOrgId|site_name~paramtrtOrgId|pk_site|[VELHIDE]'','''')">Select Treating Organization</A> </FONT></DIV> </td>
	       	<td><DIV id=''trtOrgdataDIV''><Input TYPE="text" NAME="seltrtOrgId" SIZE="20" READONLY VALUE=''[ALL]''>
	       	<Input TYPE="hidden" NAME="paramtrtOrgId" VALUE=''[ALL]''></DIV>
	       	</td>',
		'SELECT pk_site FROM erv_allusersites_distinct WHERE fk_user = :sessUserId AND site_hidden <> 1','Treating Organization','trtOrgId','trtOrgDIV,trtOrgdataDIV');
		COMMIT;
	end if;

	select count(*) into v_exists from er_repfilter where REPFILTER_KEYWORD = 'inclValMsgChk';
	if (v_exists = 0) then
		select Max(PK_REPFILTER)+1 into filterID from er_repfilter;

		INSERT INTO er_repfilter (PK_REPFILTER,REPFILTER_COLUMN,REPFILTER_VALUESQL,REPFILTER_COLDISPNAME,REPFILTER_KEYWORD,REPFILTER_DISPDIV) 
		VALUES (filterID,'<td>
	<DIV id="inclValMsgChkDIV">
		<input type="checkbox" name="valCheck" value="0" 
		onClick="this.value = (this.checked)? ''1'' : ''0''; document.getElementById(''paraminclValMsgChk'').value=this.value;"/> Including Validation Messages
		<input type="hidden" id="paraminclValMsgChk" name="paraminclValMsgChk" value="0"/>
	</DIV>
</td>',
		NULL,'Including Validation Messages','inclValMsgChk','inclValMsgChkDIV');
		COMMIT;
	end if;

END;
/


