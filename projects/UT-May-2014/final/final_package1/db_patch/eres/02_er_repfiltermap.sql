DECLARE
filterMID number := 0;
filterFK number := 0;
v_exists NUMBER := 0;
begin

	select PK_REPFILTER into filterFK from er_repfilter where REPFILTER_KEYWORD = 'studyType';
	select count(*) into v_exists from er_repfiltermap where fk_repfilter = filterFK and repfiltermap_repcat = 'data safety monitoring';
	if (v_exists = 0) then
		select Max(PK_REPFILTERMAP)+1 into filterMID from er_repfiltermap;
	
		INSERT INTO er_repfiltermap (pk_repfiltermap,fk_repfilter,repfiltermap_repcat,repfiltermap_seq) 
		VALUES (filterMID,filterFK, 'data safety monitoring',12);

		COMMIT;
	end if;
	
	select PK_REPFILTER into filterFK from er_repfilter where REPFILTER_KEYWORD = 'repOrgId';
	select count(*) into v_exists from er_repfiltermap where fk_repfilter = filterFK and repfiltermap_repcat = 'data safety monitoring';
	if (v_exists = 0) then	
		select Max(PK_REPFILTERMAP)+1 into filterMID from er_repfiltermap;

		INSERT INTO er_repfiltermap (pk_repfiltermap,fk_repfilter,repfiltermap_repcat,repfiltermap_seq) 
		VALUES (filterMID,filterFK, 'data safety monitoring',13);
			
		COMMIT;
	end if;
	
	select PK_REPFILTER into filterFK from er_repfilter where REPFILTER_KEYWORD = 'enrOrgId';
	select count(*) into v_exists from er_repfiltermap where fk_repfilter = filterFK and repfiltermap_repcat = 'data safety monitoring';
	
	if (v_exists = 0) then	
		select Max(PK_REPFILTERMAP)+1 into filterMID from er_repfiltermap;
		
		INSERT INTO er_repfiltermap (pk_repfiltermap,fk_repfilter,repfiltermap_repcat,repfiltermap_seq) 
		VALUES (filterMID,filterFK, 'data safety monitoring',14);
			
		COMMIT;
	end if;

	select PK_REPFILTER into filterFK from er_repfilter where REPFILTER_KEYWORD = 'trtOrgId';
	select count(*) into v_exists from er_repfiltermap where fk_repfilter = filterFK and repfiltermap_repcat = 'data safety monitoring';
	
	if (v_exists = 0) then	
		select Max(PK_REPFILTERMAP)+1 into filterMID from er_repfiltermap;

		INSERT INTO er_repfiltermap (pk_repfiltermap,fk_repfilter,repfiltermap_repcat,repfiltermap_seq) 
		VALUES (filterMID,filterFK, 'data safety monitoring',15);
			
		COMMIT;
	end if;
	
	select PK_REPFILTER into filterFK from er_repfilter where REPFILTER_KEYWORD = 'studyResType';
	select count(*) into v_exists from er_repfiltermap where fk_repfilter = filterFK and repfiltermap_repcat = 'data safety monitoring';
	
	if (v_exists = 0) then	
		select Max(PK_REPFILTERMAP)+1 into filterMID from er_repfiltermap;

		INSERT INTO er_repfiltermap (pk_repfiltermap,fk_repfilter,repfiltermap_repcat,repfiltermap_seq,REPFILTERMAP_COLUMN) 
		VALUES (filterMID,filterFK, 'data safety monitoring',16,'<td><DIV id="restypeDIV"><FONT class = "comments"><A href="javascript:void(0);" onClick="return openLookup(document.reports,''viewId=6011&form=reports&seperator=,&defaultvalue=[ALL]&keyword=selstudyResType|CODEDESC~paramstudyResType|CODEPK|[VELHIDE]'',''dfilter=codelst_type=\''research_type\'' '')">Select Study Source</A> </FONT></DIV> </td>
	       	<td><DIV id="restypedataDIV"><Input TYPE="text" name="selstudyResType" SIZE="20" READONLY value="[ALL]">
	       	<Input TYPE="hidden" name="paramstudyResType" value="[ALL]"></DIV>
	       	</td>');

		COMMIT;
	end if;
	
	select PK_REPFILTER into filterFK from er_repfilter where REPFILTER_KEYWORD = 'inclValMsgChk';
	select count(*) into v_exists from er_repfiltermap where fk_repfilter = filterFK and repfiltermap_repcat = 'data safety monitoring';
	
	if (v_exists = 0) then	
		select Max(PK_REPFILTERMAP)+1 into filterMID from er_repfiltermap;

		INSERT INTO er_repfiltermap (pk_repfiltermap,fk_repfilter,repfiltermap_repcat,repfiltermap_seq) 
		VALUES (filterMID,filterFK, 'data safety monitoring',17);
			
		COMMIT;
	end if;
END;
/


