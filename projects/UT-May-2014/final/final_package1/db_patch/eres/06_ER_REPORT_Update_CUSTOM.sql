SET define OFF;

-- Update into ER_REPORT
DECLARE
	v_pk_report NUMBER := 0;
	v_record_exists NUMBER := 0;
	SQL_CLOB CLOB;
BEGIN
	SELECT COUNT(*) INTO v_record_exists FROM er_report WHERE REP_TYPE = 'rep_nci' AND REP_NAME = 'CCSG Data Table 4 (UT Specific)';
	IF (v_record_exists != 0) THEN
		SELECT MAX(PK_REPORT) INTO v_pk_report FROM er_report WHERE REP_TYPE = 'rep_nci' AND REP_NAME = 'CCSG Data Table 4 (UT Specific)';
		
		Update ER_REPORT Set REP_FILTERBY = 'Date, Reporting Organization, Clinical Research Category, Study Source', 
		REP_FILTERKEYWORD='[DATEFILTER]D:repOrgId:studyType:studyResType:inclValMsgChk', 
		REP_FILTERAPPLICABLE='date:repOrgId:studyType:studyResType:inclValMsgChk',
		REP_COLUMNS = 'Clinical Research Category, Study Source, Specific Funding Source, Anatomic Site, Proto ID, PI Last Name, PI First Name, Program Code, Date Opened, Date Closed, Phase, Primary Purpose, Official Title, Is multi-Inst trial?, Total Targetted Accrual-Entire Study, Total Targetted Accrual-Your Center, Cancer Center: Primary Accrual Institution-12 Mos, Cancer Center: Primary Accrual Institution-To Date, Other Accrual Institution-12 Mos, Other Accrual Institution-To Date'
		where pk_report = v_pk_report;
		COMMIT;
 	END IF;
END;
/
