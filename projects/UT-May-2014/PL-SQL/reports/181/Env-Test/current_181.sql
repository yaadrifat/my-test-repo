SELECT disease_site,
  disease_site_subtyp,
  repsitename,
  enrsitename,
  studyTypes,
  Anatomicsitenull,
  SUM(pat_ds_count) COUNT 
FROM
  ( select codelst_desc as disease_site, codelst_subtyp as disease_site_subtyp,
    (select site_name from er_site where pk_site in (:repOrgId)) as repsitename,
     SITE_NAME as enrsitename,
    null as studyTypes,
    pkg_util.f_join(cursor(SELECT ('[Study: '||STUDY_NUMBER ||' & '||'Patient: '||(select PER_CODE from er_per where PK_PER = patprt.fk_per) || ']')
    FROM ER_PATPROT patprt,
      er_patstudystat patst,
      er_study stdy,
      er_studysites stsite
    WHERE stdy.fk_account = :sessAccId
    AND stdy.pk_study     = patprt.fk_study
    AND patprt.FK_PER     = patst.fk_per
    AND stsite.fk_study   =stdy.pk_study
    AND patprt.fk_study   = patst.fk_study
	AND patprt.patprot_stat = 1
    AND FK_CODELST_STAT = (SELECT cdStat1.pk_codelst FROM ER_CODELST cdStat1 WHERE cdStat1.codelst_subtyp = 'enrolled')
    AND PATSTUDYSTAT_DATE BETWEEN TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT)
    AND fk_site_enrolling  = enrSite.pk_site
    AND stsite.FK_SITE             IN (:repOrgId)
    AND (PATPROT_TREATINGORG IS NULL OR NVL(PATPROT_TREATINGORG,0) IN (:trtOrgId))
    AND PATPROT_OTHR_DIS_CODE IS NULL
    AND FK_CODELST_TYPE IN (SELECT cdStudyType.pk_codelst FROM ER_CODELST cdStudyType WHERE cdStudyType.codelst_type ='study_type' and codelst_subtyp='Intervention')
	AND EXISTS (select * from er_formslinear where fk_form = 404 and id = stdy.pk_study and col2 like 'Treatment%')
    ), '') AS Anatomicsitenull,
	(SELECT count(*)
      FROM ER_PATPROT patprt, er_study stdy, er_studysites stsite
      WHERE  stdy.fk_account = :sessAccId
      AND stdy.pk_study = patprt.fk_study
      AND stsite.fk_study=stdy.pk_study
	  AND patprt.patprot_stat = 1
	  AND EXISTS (select * from er_patstudystat patst 
      where patst.fk_per = patprt.FK_PER and patst.fk_study = patprt.fk_study
      AND patst.FK_CODELST_STAT = (SELECT cdStat1.pk_codelst FROM ER_CODELST cdStat1 WHERE cdStat1.codelst_subtyp = 'enrolled')
      AND patst.PATSTUDYSTAT_DATE BETWEEN TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT))
      AND fk_site_enrolling = enrSite.pk_site
      AND stsite.FK_SITE in (:repOrgId)
      AND (PATPROT_TREATINGORG IS NULL OR NVL(PATPROT_TREATINGORG,0) IN (:trtOrgId))
	  AND (cdDisSite.PK_CODELST = f_to_number(patprt.PATPROT_OTHR_DIS_CODE)
      OR ((cdDisSite.PK_CODELST <> f_to_number(patprt.PATPROT_OTHR_DIS_CODE))
      AND (
      (cdDisSite.PK_CODELST in (select t2.pk_codelst from er_codelst t2 where t2.codelst_type = 'disease_site' AND t2.codelst_subtyp = 'leukemiaother')
      AND f_to_number(patprt.PATPROT_OTHR_DIS_CODE) in (select t2.pk_codelst from er_codelst t2 where t2.codelst_type = 'disease_site' AND t2.codelst_subtyp = 'leukemianototh'))
      OR (cdDisSite.PK_CODELST in (select t2.pk_codelst from er_codelst t2 where t2.codelst_type = 'disease_site' AND t2.codelst_subtyp = 'breastfemale')
      AND f_to_number(patprt.PATPROT_OTHR_DIS_CODE) in (select t2.pk_codelst from er_codelst t2 where t2.codelst_type = 'disease_site' AND t2.codelst_subtyp = 'breastmale'))
	  OR (cdDisSite.PK_CODELST in (select t2.pk_codelst from er_codelst t2 where t2.codelst_type = 'disease_site' AND t2.codelst_subtyp = 'liporalcavitya')
      AND f_to_number(patprt.PATPROT_OTHR_DIS_CODE) in (select t2.pk_codelst from er_codelst t2 where t2.codelst_type = 'disease_site' AND t2.codelst_subtyp = 'buccalcavityan'))
      )))
      AND FK_CODELST_TYPE IN (SELECT cdStudyType.pk_codelst FROM ER_CODELST cdStudyType WHERE cdStudyType.codelst_type ='study_type' and codelst_subtyp='Intervention')
	  AND EXISTS (select * from er_formslinear where fk_form = 404 and id = stdy.pk_study and col2 like 'Treatment%')
    ) as pat_ds_count
  FROM er_site enrSite, 
  (select a.pk_codelst as pk_codelst, a.codelst_subtyp as codelst_subtyp, a.codelst_desc as codelst_desc
  from er_codelst a WHERE a.codelst_type = 'disease_site' and a.codelst_subtyp not in ('multiplesites','leukemianototh','buccalcavityan','breastfemale','breastmale')
  UNION
  select bb.pk_codelst, 'leukemiaother', bb.codelst_desc
  from er_codelst b, er_codelst bb WHERE b.codelst_type = bb.codelst_type and b.codelst_type ='disease_site' and b.codelst_subtyp = 'leukemianototh' and bb.codelst_subtyp = 'leukemiaother'
  UNION
  select cc.pk_codelst, 'liporalcavitya', cc.codelst_desc
  from er_codelst c, er_codelst cc WHERE c.codelst_type = cc.codelst_type and c.codelst_type ='disease_site' and c.codelst_subtyp = 'buccalcavityan' and cc.codelst_subtyp = 'liporalcavitya'
  UNION
  select (select dd.pk_codelst from er_codelst dd where dd.codelst_type ='disease_site' and dd.codelst_subtyp = 'breastfemale') pk_codelst, 'breastfemale', 'Breast' 
  from er_codelst d WHERE d.codelst_type ='disease_site' and d.codelst_subtyp in ('breastfemale','breastmale')
  ) cdDisSite
  WHERE enrSite.PK_SITE in (:enrOrgId)
  )
  group by disease_site, disease_site_subtyp, repsitename, enrsitename, studyTypes, Anatomicsitenull
  order by enrsitename, 
  decode(disease_site_subtyp,'liporalcavitya',1,'esophagus',2,'stomach',3,'smallintestine',4,'colon',5,'rectum',6,'anus',7,'liver',8,'pancreas',9,'otherdigestive',10, 
'larynx',11,'lung',12,'otherrespirato',13,'bonesandjoints',14,'softtissue',15, 'melanomaskin',16,'kaposisarcom',17,'mycosisfungoid',18,'otherskin',19,'breastfemale',20,
'cervix',21,'corpusuteri',22,'ovary',23,'othfemalegenit',24,'prostate',25,'othermalegenit',26,'urinarybladder',27,'kidney',28,'otherurinary',29,'eyeandorbit',30,
'brainnervoussy',31,'thyroid',32,'otherendocrine',33,'nonhodgkinslym',34,'hodgkinslympho',35,'multiplemyelom',36,'lymphoidleukem',37,'myeloidandmono',38,'leukemiaother',39,'otherhematopo',40,
'unknownsites',41,'illdefinedsite',42)