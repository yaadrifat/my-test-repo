SELECT PK_STUDY, STUDY_NUMBER, ':inclValMsgChk' as SHOW_VAL_MSGS,
 (SELECT CODELST_DESC FROM ER_CODELST WHERE PK_CODELST = FK_CODELST_RESTYPE) AS RESEARCH_TYPE,
 NVL(( SELECT CODELST_DESC FROM ER_CODELST WHERE PK_CODELST = FK_CODELST_TYPE), 'Not Entered') AS STUDY_TYPE,
 (SELECT SITE_NAME FROM ER_SITE WHERE PK_SITE IN (STDSITE.FK_SITE)) AS REPSITENAME,
 DECODE(fk_codelst_sponsor,NULL,'', (SELECT CODELST_DESC FROM ER_CODELST WHERE PK_CODELST = FK_CODELST_SPONSOR)) 
 ||DECODE(study_sponsor,NULL,'',', '||study_sponsor)
 || ', '|| pkg_util.f_join((cursor(select col2 from er_formslinear where fk_form = 336 and id = stdy.pk_study)), ',') AS SPECIFIC_FUNDING_SOURCE,
 F_GETDIS_SITE(STUDY_DISEASE_SITE) AS ANATOMIC_SITE,
 (case when stdy.FK_CODELST_RESTYPE in (select pk_codelst from er_codelst Where codelst_type='research_type' and codelst_subtyp in('coop', 'other')) then
 (select studyid_id from er_studyid, er_codelst where fk_study = stdy.pk_study and fk_codelst_idtype = pk_codelst and codelst_type='studyidtype' and codelst_subtyp ='studyidtype_267')
 else (select studyid_id from er_studyid, er_codelst where fk_study = stdy.pk_study and fk_codelst_idtype = pk_codelst and codelst_type='studyidtype' and codelst_subtyp ='irb_number')
 end) AS PROTO_ID,
 (SELECT usr_firstname FROM ER_USER WHERE pk_user = study_prinv) as PI_FIRST_NAME,
 (SELECT USR_LASTNAME FROM ER_USER WHERE pk_user = study_prinv) as PI_LAST_NAME,
 (SELECT SUBSTR(studyid_id, 0, instr(NVL(studyid_id, ''), ' '))
 FROM er_studyid
 WHERE fk_codelst_idtype = (SELECT pk_codelst FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND codelst_subtyp = 'sum4_prg')
 AND fk_study = stdy.pk_study) AS Program_Code,
 TO_CHAR(sstat_a.studystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) AS OPEN_FOR_ENROLL_DT,
 (SELECT TO_CHAR(MIN(sstat_pc.studystat_date),PKG_DATEUTIL.F_GET_DATEFORMAT)
 FROM ER_STUDYSTAT sstat_pc
 WHERE sstat_pc.fk_study = stdy.pk_study
 AND sstat_pc.fk_codelst_studystat = (SELECT pk_codelst FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND codelst_subtyp = 'prmnt_cls')) AS CLOSED_TO_ACCR_DT,
 DECODE(FK_CODELST_PHASE,NULL,'', ( SELECT CODELST_DESC FROM ER_CODELST WHERE PK_CODELST = FK_CODELST_PHASE)) AS PHASE,
 (select DECODE(substr(col2, 0, 3), 'Hea', 'Hsr', substr(col2, 0, 3)) from er_formslinear where fk_form = 404 and id = stdy.pk_study) AS PRIMARY_PURPOSE,
 study_title AS OFFICIAL_TITLE,
 DECODE((SELECT CODELST_DESC FROM ER_CODELST WHERE PK_CODELST = FK_CODELST_SCOPE),NULL,'','Multi Center Study','YES','Single Center Study','NO') AS IS_MULTI_INST_TRIAL,
 NVL(STUDY_NSAMPLSIZE,0) AS ENTIRE_STUDY,
 NVL(studysite_lsamplesize,0) AS YOUR_CENTER,
 NVL(
 (SELECT COUNT(*) FROM ER_PATPROT PATPRT, ER_PATSTUDYSTAT PATST WHERE PATPRT.FK_STUDY = pk_study
 AND PATPRT.FK_STUDY = PATST.FK_STUDY AND PATPRT.FK_PER = PATST.FK_PER AND PATPRT.PATPROT_STAT = 1
 AND PATST.FK_CODELST_STAT = (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE = 'patStatus' AND CODELST_SUBTYP = 'enrolled')
 AND PATST.PATSTUDYSTAT_DATE BETWEEN TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT)
 AND PATPRT.FK_SITE_ENROLLING = STDSITE.FK_SITE),0) AS MOS_PRIMARY,
 NVL(
 (SELECT COUNT(*)
 FROM ER_PATPROT PATPRT, ER_PATSTUDYSTAT PATST WHERE PATPRT.FK_STUDY = pk_study 
 AND PATPRT.FK_STUDY = PATST.FK_STUDY  AND PATPRT.FK_PER = PATST.FK_PER AND PATPRT.PATPROT_STAT = 1
 AND FK_CODELST_STAT = (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE = 'patStatus' AND CODELST_SUBTYP = 'enrolled')
 AND PATPRT.FK_SITE_ENROLLING = STDSITE.FK_SITE),0) AS TO_DATE_PRIMARY,
 CASE WHEN (STDSITE.FK_CODELST_STUDYSITETYPE IS NULL 
 OR STDSITE.FK_CODELST_STUDYSITETYPE NOT IN (SELECT pk_codelst FROM ER_CODELST WHERE CODELST_TYPE = 'studySiteType' AND CODELST_SUBTYP = 'primary')) THEN 0
 ELSE
 NVL(
 (SELECT COUNT(*) FROM ER_PATPROT PATPRT, ER_PATSTUDYSTAT PATST WHERE PATPRT.FK_STUDY = pk_study 
 AND PATPRT.FK_STUDY = PATST.FK_STUDY AND PATPRT.FK_PER = PATST.FK_PER AND PATPRT.PATPROT_STAT = 1
 AND PATST.FK_CODELST_STAT = (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE = 'patStatus' AND CODELST_SUBTYP = 'enrolled')
 AND PATST.PATSTUDYSTAT_DATE BETWEEN TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT)
 AND PATPRT.FK_SITE_ENROLLING <> STDSITE.FK_SITE),0) 
 END AS MOS_OTHERS,
 CASE WHEN (STDSITE.FK_CODELST_STUDYSITETYPE IS NULL 
 OR STDSITE.FK_CODELST_STUDYSITETYPE NOT IN (SELECT pk_codelst FROM ER_CODELST WHERE CODELST_TYPE = 'studySiteType' AND CODELST_SUBTYP = 'primary')) THEN 0
 ELSE
 NVL(
 (SELECT COUNT(*) FROM ER_PATPROT PATPRT, ER_PATSTUDYSTAT PATST WHERE PATPRT.FK_STUDY = pk_study 
 AND PATPRT.FK_STUDY = PATST.FK_STUDY AND PATPRT.FK_PER = PATST.FK_PER AND PATPRT.PATPROT_STAT = 1
 AND PATST.FK_CODELST_STAT = (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE = 'patStatus' AND CODELST_SUBTYP = 'enrolled')
 AND PATPRT.FK_SITE_ENROLLING <> STDSITE.FK_SITE),0) 
 END AS TO_DATE_OTHERS
FROM ER_STUDY stdy, ER_STUDYSTAT sstat_a, 
 ER_STUDYSITES STDSITE, ER_CODELST clist_STUDY_TYPE, ER_CODELST clist_RESEARCH_TYPE
WHERE STDSITE.FK_STUDY = stdy.PK_STUDY AND sstat_a.FK_STUDY = stdy.pk_study 
AND stdy.FK_ACCOUNT = :sessAccId 
AND sstat_a.FK_SITE = STDSITE.FK_SITE AND STDSITE.FK_SITE IN (:repOrgId)
AND (stdy.FK_CODELST_TYPE IS NULL OR (stdy.FK_CODELST_TYPE IN (:studyType)
AND clist_STUDY_TYPE.pk_codelst = stdy.FK_CODELST_TYPE AND clist_STUDY_TYPE.CODELST_TYPE = 'study_type'))
AND (stdy.FK_CODELST_RESTYPE IN (:studyResType) AND clist_RESEARCH_TYPE.pk_codelst = stdy.FK_CODELST_RESTYPE AND clist_RESEARCH_TYPE.CODELST_TYPE = 'research_type')
AND stdy.STUDY_DIVISION = (select pk_codelst from er_codelst where codelst_type = 'study_division' and codelst_subtyp = 'CTRC')
AND sstat_a.fk_codelst_studystat = (SELECT pk_codelst FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND codelst_subtyp = 'active')
AND (sstat_a.STUDYSTAT_DATE <= TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT))
AND NOT EXISTS (SELECT * FROM ER_STUDYSTAT sstat_pc WHERE sstat_pc.FK_STUDY = stdy.pk_study 
AND sstat_pc.fk_codelst_studystat = (SELECT pk_codelst FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND codelst_subtyp = 'prmnt_cls')
AND sstat_pc.STUDYSTAT_DATE < TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT))
ORDER BY decode(clist_STUDY_TYPE.codelst_subtyp, 'Intervention', 1, 'observational', 2, 'Anc_Cor', 3), 
decode(clist_RESEARCH_TYPE.codelst_subtyp, 'coop', 1, 'other', 2, 'insti', 3, 'indus', 4), 
lower(PI_LAST_NAME), lower(PI_FIRST_NAME), SPECIFIC_FUNDING_SOURCE