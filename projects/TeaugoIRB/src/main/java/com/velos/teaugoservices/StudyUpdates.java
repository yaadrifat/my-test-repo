package com.velos.teaugoservices;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "StudyUpdates")
public class StudyUpdates {
//Validate   
	private String irbProtocolNumber;
	
	private String studyNumber;
	private String title;
	private String shortTitle;
	
	public String getIrbProtocolNumber() {
		return irbProtocolNumber;
	}
	public void setIrbProtocolNumber(String irbProtocolNumber) {
		this.irbProtocolNumber = irbProtocolNumber;
	}
	public String getStudyNumber() {
		return studyNumber;
	}
	public void setStudyNumber(String studyNumber) {
		this.studyNumber = studyNumber;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getShortTitle() {
		return shortTitle;
	}
	public void setShortTitle(String shortTitle) {
		this.shortTitle = shortTitle;
	}
	
	private String summary;
	private String fundingType;
	private String fundingSource;
	private String sponsorProtocolNumber;
	private String fundingOtherInformation;
	private String sponsorContact;
	private String studyScope;
	private String phase;

	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	
	public String getSponsorContact() {
		return sponsorContact;
	}
	public String getStudyScope() {
		return studyScope;
	}
	public void setStudyScope(String studyScope) {
		this.studyScope = studyScope;
	}
	public String getPhase() {
		return phase;
	}
	public void setPhase(String phase) {
		this.phase = phase;
	}
	public void setSponsorContact(String sponsorContact) {
		this.sponsorContact = sponsorContact;
	}
	public String getFundingType() {
		return fundingType;
	}
	public void setFundingType(String fundingType) {
		this.fundingType = fundingType;
	}
	public String getFundingSource() {
		return fundingSource;
	}
	public void setFundingSource(String fundingSource) {
		this.fundingSource = fundingSource;
	}
	public String getSponsorProtocolNumber() {
		return sponsorProtocolNumber;
	}
	public void setSponsorProtocolNumber(String sponsorProtocolNumber) {
		this.sponsorProtocolNumber = sponsorProtocolNumber;
	}
	public String getFundingOtherInformation() {
		return fundingOtherInformation;
	}
	public void setFundingOtherInformation(String fundingOtherInformation) {
		this.fundingOtherInformation = fundingOtherInformation;
	}
	
	
	/*private String statusType;
	private String status;
	private String documentedBy;
	private String statusValidFrom;
	private String currentStatus;*/
}