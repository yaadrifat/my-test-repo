package com.velos.teaugoservices;
 
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.log4j.Logger;

import com.velos.integration.espclient.VelosSendStudyData;


 public class GenericExceptionMapper implements ExceptionMapper<Throwable>{
	 
public static Logger logger = Logger.getLogger(GenericExceptionMapper.class);

 
 public Response toResponse(Throwable ex) {
	 logger.info("############# GenericExceptionMapper  ############");
	 //ex.printStackTrace();
	 logger.error("Error :",ex);
	 if(ex instanceof TeaugoServiceException) {
		 TeaugoServiceException serviceException = (TeaugoServiceException)ex;
		 return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(serviceException.getFaultDetails()).build();
	 }else if (!(ex instanceof NullPointerException)){
		 logger.info("#### NOT NullPointerException ####");
		 ServiceResponse response = new ServiceResponse();
			response.setStatus("Failed");
			response.setMessage(ex.getMessage());
		 return Response.status(Status.INTERNAL_SERVER_ERROR).entity(response).build();
	 }else {
		 ServiceResponse response = new ServiceResponse();
			response.setStatus("Failed");
			response.setMessage("Exception OTHER THAN TeaugoServiceException");
		 return Response.status(Status.INTERNAL_SERVER_ERROR).entity(response).build();
	 }
 }
 
}