package com.velos.teaugoservices;

import java.io.IOException;
import java.util.List;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.annotations.SchemaValidation;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.cxf.jaxrs.ext.multipart.Multipart;
import org.apache.cxf.jaxrs.model.wadl.Description;
import org.apache.cxf.jaxrs.model.wadl.Descriptions;
import org.apache.cxf.jaxrs.model.wadl.DocTarget;

import com.sun.istack.NotNull;

public interface FileUploadSEI {

	@POST
	@Path("/uploadStudyDocument")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	// @Produces({"text/xml","text/html"})
	@Produces({ "application/xml", "application/json" })
	public ServiceResponse uploadFile(
			@Multipart(value = "irbProtocolNumber", required = false) String irbProtocolNumber,
			@Multipart(value = "versionNumber", required = false) String versionNumber,
			@Multipart(value = "versionDate", required = false) String versionDate,
			@Multipart(value = "category", required = false) String category,
			@Multipart(value = "type", required = false) String type,
			@Multipart(value = "file", required = false) List<Attachment> attachments,
			@Multipart(value = "description", required = false) String description,
			@Multipart(value = "url", required = false) String url,
			@Multipart(value = "urlDescription", required = false) String urlDescription)
			throws IOException, Exception;

}
