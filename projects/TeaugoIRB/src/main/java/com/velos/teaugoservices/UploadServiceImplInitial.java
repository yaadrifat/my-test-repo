 package com.velos.teaugoservices;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import javax.activation.DataHandler;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.cxf.jaxrs.ext.multipart.Multipart;
import org.apache.cxf.jaxrs.ext.multipart.MultipartBody;

public class UploadServiceImplInitial {

   @POST
   @Path("/uploadFile")
   @Consumes(MediaType.MULTIPART_FORM_DATA)
   @Produces("text/xml")
   //public Response uploadFile(List<Attachment> attachments, @Context HttpServletRequest request) {
   public Response uploadFile(List<Attachment> attachments) {
     System.out.println("File Upload Request Received......");
	   for (Attachment attachment : attachments) {
         DataHandler handler = attachment.getDataHandler();
         try {
            InputStream stream = handler.getInputStream();
            MultivaluedMap<String, String> map = attachment.getHeaders();
            System.out.println("fileName : " + getFileName(map));
            OutputStream out = new FileOutputStream(new File("/home/brajasekharreddy/Documents/iMedRis/sponsor/" + getFileName(map)));

            int read = 0;
            byte[] bytes = new byte[1024];
            while ((read = stream.read(bytes)) != -1) {
               out.write(bytes, 0, read);
            }
            stream.close();
            out.flush();
            out.close();
         } catch (Exception e) {
            e.printStackTrace();
         }
      }

      return Response.ok("file uploaded").build();
   }

   @POST
   @Path("/uploadSingleFile")
   @Consumes(MediaType.MULTIPART_FORM_DATA)
   @Produces("text/xml")
   //public Response uploadFile(List<Attachment> attachments, @Context HttpServletRequest request) {
   public Response uploadFile(Attachment attachment) {
     System.out.println("Single File Upload Request Received......");
         DataHandler handler = attachment.getDataHandler();
         try {
            InputStream stream = handler.getInputStream();
            MultivaluedMap<String, String> map = attachment.getHeaders();
            System.out.println("fileName  : " + getFileName(map));
            OutputStream out = new FileOutputStream(new File("/home/brajasekharreddy/Documents/iMedRis/sponsor/" + getFileName(map)));

            int read = 0;
            byte[] bytes = new byte[1024];
            while ((read = stream.read(bytes)) != -1) {
               out.write(bytes, 0, read);
            }
            stream.close();
            out.flush();
            out.close();
         } catch (Exception e) {
            e.printStackTrace();
         }
      return Response.ok("file uploaded").build();
   }
   
   private String getFileName(MultivaluedMap<String, String> header) {
      String[] contentDisposition = header.getFirst("Content-Disposition").split(";");
      for (String filename : contentDisposition) {
         if ((filename.trim().startsWith("filename"))) {
            String[] name = filename.split("=");
            String exactFileName = name[1].trim().replaceAll("\"", "");
            return exactFileName;
         }
      }
      return "unknown";
   }
   
   
   @POST
   @Path("/uploadSingleFileWithNote")
   @Consumes(MediaType.MULTIPART_FORM_DATA)
   @Produces("text/xml")
   //public String uploadFile(@Multipart("note") String note, @Multipart("upfile") Attachment attachment) throws IOException,Exception {
   public String uploadFile(@Multipart(value = "note",required = false) String note, @Multipart("upfile") Attachment attachment) throws IOException,Exception {

	   System.out.println("Note with Single File Upload Request Received......");
	   String filename = attachment.getContentDisposition().getParameter("filename");
	   System.out.println("FileName from Disposition : "+filename);
	   DataHandler handler = attachment.getDataHandler();
       try {
          InputStream stream = handler.getInputStream();
          MultivaluedMap<String, String> map = attachment.getHeaders();
          System.out.println("fileName  : " + getFileName(map));
          OutputStream out = new FileOutputStream(new File("/home/brajasekharreddy/Documents/iMedRis/sponsor/" + getFileName(map)));

          int read = 0;
          byte[] bytes = new byte[1024];
          while ((read = stream.read(bytes)) != -1) {
             out.write(bytes, 0, read);
          }
          stream.close();
          out.flush();
          out.close();
       } catch (Exception e) {
          e.printStackTrace();
       }
	   
      return "uploaded " + note;  
   }      
   
   
   
   @POST
   @Path("/uploadMultipleFileWithNote")
   @Consumes(MediaType.MULTIPART_FORM_DATA)
   @Produces("text/xml")
   public String uploadFile(@Multipart("note") String note, @Multipart("upfile") List<Attachment> attachments) throws IOException,Exception {
   //public String uploadFile(@Multipart(value = "note",required = false) String note, @Multipart("upfile") List<Attachment> attachments) throws IOException,Exception {
	   
	   System.out.println("Note with Multiple File Upload Request Received......");
	   for (Attachment attachment : attachments) {
         DataHandler handler = attachment.getDataHandler();
         try {
            InputStream stream = handler.getInputStream();
            MultivaluedMap<String, String> map = attachment.getHeaders();
            System.out.println("fileName : " + getFileName(map));
            OutputStream out = new FileOutputStream(new File("/home/brajasekharreddy/Documents/iMedRis/sponsor/" + getFileName(map)));

            int read = 0;
            byte[] bytes = new byte[1024];
            while ((read = stream.read(bytes)) != -1) {
               out.write(bytes, 0, read);
            }
            stream.close();
            out.flush();
            out.close();
         } catch (Exception e) {
            e.printStackTrace();
         }
      }
	   
      return "uploaded " + note;  
   }  
   
   @POST
   @Path("/uploadMultipart")
   @Consumes(MediaType.MULTIPART_FORM_DATA)
   @Produces("text/xml")
   public Response uploadFile(MultipartBody body) {
     System.out.println("MutltipartBody Upload Request Received......");
     List<Attachment> attachments = body.getAllAttachments();
	   for (Attachment attachment : attachments) {
         DataHandler handler = attachment.getDataHandler();
         try {
            InputStream stream = handler.getInputStream();
            MultivaluedMap<String, String> map = attachment.getHeaders();
            System.out.println("fileName : " + getFileName(map));
            OutputStream out = new FileOutputStream(new File("/home/brajasekharreddy/Documents/iMedRis/sponsor/" + getFileName(map)));

            int read = 0;
            byte[] bytes = new byte[1024];
            while ((read = stream.read(bytes)) != -1) {
               out.write(bytes, 0, read);
            }
            stream.close();
            out.flush();
            out.close();
         } catch (Exception e) {
            e.printStackTrace();
         }
      }

      return Response.ok("file uploaded").build();
   }
   
   //This willreceive Note and it will also create a new file for note also but where as 
   //in Attachment for note file will not be created.
   
   @POST
   @Path("/uploadMultipartWithNote")
   @Consumes(MediaType.MULTIPART_FORM_DATA)
   @Produces({"text/xml","text/html"})
   public String uploadFile(@Multipart("note") String note, @Multipart("upfile") MultipartBody body) throws IOException,Exception {
   //public String uploadFile(@Multipart(value = "note",required = false) String note, @Multipart("upfile") MultipartBody body) throws IOException,Exception {
	   
	   System.out.println("Note with MultipartBody File Upload Request Received......");
	   List<Attachment> attachments = body.getAllAttachments();
	   for (Attachment attachment : attachments) {
         DataHandler handler = attachment.getDataHandler();
         try {
            InputStream stream = handler.getInputStream();
            MultivaluedMap<String, String> map = attachment.getHeaders();
            System.out.println("fileName : " + getFileName(map));
            OutputStream out = new FileOutputStream(new File("/home/brajasekharreddy/Documents/iMedRis/sponsor/" + getFileName(map)));

            int read = 0;
            byte[] bytes = new byte[1024];
            while ((read = stream.read(bytes)) != -1) {
               out.write(bytes, 0, read);
            }
            stream.close();
            out.flush();
            out.close();
         } catch (Exception e) {
            e.printStackTrace();
         }
      }
	   
      return "uploaded MultipartBody with Note : " + note;  
   }
   
}