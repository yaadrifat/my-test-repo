package com.velos.teaugoservices;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "TeaugoServiceException")
public class TeaugoServiceException extends Exception implements Serializable{
 
  private ServiceResponse faultDetails;

  public TeaugoServiceException(ServiceResponse faultDetails) {
    this.faultDetails = faultDetails;
  }

  public TeaugoServiceException(String message, ServiceResponse faultDetails) {
    super(message);
    this.faultDetails = faultDetails;
  }

  public ServiceResponse getFaultDetails() {
    return faultDetails;
  }

}
