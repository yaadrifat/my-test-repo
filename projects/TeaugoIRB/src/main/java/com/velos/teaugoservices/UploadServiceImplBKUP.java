 package com.velos.teaugoservices;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import javax.activation.DataHandler;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.cxf.jaxrs.ext.multipart.Multipart;
import org.apache.cxf.jaxrs.ext.multipart.MultipartBody;

public class UploadServiceImplBKUP {

	
	//This will receive Note also and it will also create a new file for note.
	
   @POST
   @Path("/uploadFile")
   @Consumes(MediaType.MULTIPART_FORM_DATA)
   @Produces("text/xml")
   //public Response uploadFile(List<Attachment> attachments, @Context HttpServletRequest request) {
   public Response uploadFile(List<Attachment> attachments) {
     System.out.println("Attachment List : File Upload Request Received......");
	   for (Attachment attachment : attachments) {
         DataHandler handler = attachment.getDataHandler();
         try {
            InputStream stream = handler.getInputStream();
            MultivaluedMap<String, String> map = attachment.getHeaders();
            System.out.println("fileName : " + getFileName(map));
            OutputStream out = new FileOutputStream(new File("/home/brajasekharreddy/Documents/iMedRis/sponsor/" + getFileName(map)));

            int read = 0;
            byte[] bytes = new byte[1024];
            while ((read = stream.read(bytes)) != -1) {
               out.write(bytes, 0, read);
            }
            stream.close();
            out.flush();
            out.close();
         } catch (Exception e) {
            e.printStackTrace();
         }
      }

      return Response.ok("file uploaded").build();
   }
   
   //This will receive Note also and it will also create a new file for note.

   @POST
   @Path("/uploadSingleFile")
   @Consumes(MediaType.MULTIPART_FORM_DATA)
   @Produces("text/xml")
   //public Response uploadFile(List<Attachment> attachments, @Context HttpServletRequest request) {
   public Response uploadFile(Attachment attachment) {
     System.out.println("Attachment : Single File Upload Request Received......");
         DataHandler handler = attachment.getDataHandler();
         try {
            InputStream stream = handler.getInputStream();
            MultivaluedMap<String, String> map = attachment.getHeaders();
            System.out.println("fileName  : " + getFileName(map));
            OutputStream out = new FileOutputStream(new File("/home/brajasekharreddy/Documents/iMedRis/sponsor/" + getFileName(map)));

            int read = 0;
            byte[] bytes = new byte[1024];
            while ((read = stream.read(bytes)) != -1) {
               out.write(bytes, 0, read);
            }
            stream.close();
            out.flush();
            out.close();
         } catch (Exception e) {
            e.printStackTrace();
         }
      return Response.ok("file uploaded").build();
   }
   
   private String getFileName(MultivaluedMap<String, String> header) {
	  //System.out.println("#################################");
	  //System.out.println("Headre First = "+header.getFirst("Content-Disposition"));
      String[] contentDisposition = header.getFirst("Content-Disposition").split(";");
      //System.out.println("After Split = "+Arrays.asList(contentDisposition));
      for (String filename : contentDisposition) {
         if ((filename.trim().startsWith("filename"))) {
            String[] name = filename.split("=");
            //System.out.println("Names = "+Arrays.asList(name));
            String exactFileName = name[1].trim().replaceAll("\"", "");
            //System.out.println("Final File Name = "+exactFileName);
            //System.out.println("#################################");
            return exactFileName;
         }
      }
      //System.out.println("#################################");
      return "unknown";
   }
   
   //This will receive Note and it will NOT create a new file for note.
   
   @POST
   @Path("/uploadSingleFileWithNote")
   @Consumes(MediaType.MULTIPART_FORM_DATA)
   @Produces("text/xml")
   //public String uploadFile(@Multipart("note") String note, @Multipart("upfile") Attachment attachment) throws IOException,Exception {
   //public String uploadFile(@Multipart(value = "note",required = false) String note, @Multipart("upfile") Attachment attachment) throws IOException,Exception {
   public String uploadFile(@Multipart(value = "note",required = false) String note, @Multipart(value = "upfile",required = false) Attachment attachment) throws IOException,Exception {
   
	   System.out.println("Attachment : Note with Single File Upload Request Received......");
		if (attachment != null) {
			String filename = attachment.getContentDisposition().getParameter("filename");
			System.out.println("FileName from Disposition : " + filename);
			DataHandler handler = attachment.getDataHandler();
			try {
				InputStream stream = handler.getInputStream();
				MultivaluedMap<String, String> map = attachment.getHeaders();
				System.out.println("fileName  : " + getFileName(map));
				OutputStream out = new FileOutputStream(new File("/home/brajasekharreddy/Documents/iMedRis/sponsor/"+ getFileName(map)));

				int read = 0;
				byte[] bytes = new byte[1024];
				while ((read = stream.read(bytes)) != -1) {
					out.write(bytes, 0, read);
				}
				stream.close();
				out.flush();
				out.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else{
			System.out.println("***  NO   Attachment  ***");
		}
       System.out.println("Note Data = "+note);
      return "uploaded " + note;  
   }      
   
   
   //This will receive Note and it will NOT create a new file for note.(Use this method)
   
   
   @POST
   @Path("/uploadMultipleFileWithNote")
   @Consumes(MediaType.MULTIPART_FORM_DATA)
   @Produces({"text/xml","text/html"})
   //public String uploadFile(@Multipart("note") String note, @Multipart("upfile") List<Attachment> attachments) throws IOException,Exception {
   //public String uploadFile(@Multipart(value = "note",required = false) String note, @Multipart("upfile") List<Attachment> attachments) throws IOException,Exception {
   public String uploadFile(@Multipart(value = "note",required = false) String note, @Multipart(value = "upfile",required = false) List<Attachment> attachments) throws IOException,Exception {
	   
	   System.out.println("|||||||||||Attachment List : Note with Multiple File Upload Request Received......");
		if (attachments != null && !attachments.isEmpty()) {
			for (Attachment attachment : attachments) {
				/*System.out.println("*****************************************");
				System.out.println("Attachment Content ID = "+attachment.getContentId());
				System.out.println("Attachment Content Type = "+attachment.getContentType());
				System.out.println("Attachment Headers = "+attachment.getHeaders());
				System.out.println("Attachment Headers List = "+attachment.getHeaderAsList("Content-Disposition"));
				System.out.println("------");
				System.out.println("ContentDisposition Type = "+attachment.getContentDisposition().getType());
				System.out.println("ContentDisposition Parameters = "+attachment.getContentDisposition().getParameters());
				System.out.println("------");
				DataHandler handlerT = attachment.getDataHandler();
				System.out.println("DataHandler Content Type = "+ handlerT.getContentType());
				System.out.println("DataHandler Name = "+ handlerT.getName());				
				System.out.println("*****************************************");*/
				
				String filename = attachment.getContentDisposition().getParameter("filename");
				System.out.println("FileName from Disposition : " + filename);
				if(!isEmpty(filename)){
				DataHandler handler = attachment.getDataHandler();
				try {
					InputStream stream = handler.getInputStream();
					System.out.println("Input Stream size = "+stream.available());
					MultivaluedMap<String, String> map = attachment.getHeaders();
					System.out.println("fileName : " + getFileName(map));
					OutputStream out = new FileOutputStream(new File("/home/brajasekharreddy/Documents/iMedRis/sponsor/"+filename));

					int read = 0;
					//byte[] bytes = new byte[1024];
					byte[] bytes = new byte[stream.available()]; //Creating with Stream size
					while ((read = stream.read(bytes)) != -1) {
						out.write(bytes, 0, read);
					}
					stream.close();
					out.flush();
					System.out.println("File Size = "+bytes.length);
					System.out.println("--------------------------------------------");
					System.out.println(bytes.toString());
					System.out.println("--------------------------------------------");
					System.out.println(read);
					System.out.println("--------------------------------------------");
					//Prints the bytes
					//System.out.println(Arrays.toString(bytes));
					//System.out.println("--------------------------------------------");
					//Prints the contents of the file
					//System.out.write(bytes);
					out.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				}else{
					System.out.println("Filename not provided OR File is not Attached");
				}
			}
		}else{
			System.out.println("***  NO   Attachments  ***");
		}
	   System.out.println("Note Data = "+note);
      return "uploaded " + note;  
   }  
   
   //This will receive Note and it will also create a new file for note also but where as 
   //in Attachment for note file will not be created.   
   
   @POST
   @Path("/uploadMultipart")
   @Consumes(MediaType.MULTIPART_FORM_DATA)
   @Produces("text/xml")
   public Response uploadFile(MultipartBody body) {
     System.out.println("MutltipartBody Upload Request Received......");
     List<Attachment> attachments = body.getAllAttachments();
	   for (Attachment attachment : attachments) {
         DataHandler handler = attachment.getDataHandler();
         try {
            InputStream stream = handler.getInputStream();
            MultivaluedMap<String, String> map = attachment.getHeaders();
            System.out.println("fileName : " + getFileName(map));
            OutputStream out = new FileOutputStream(new File("/home/brajasekharreddy/Documents/iMedRis/sponsor/" + getFileName(map)));

            int read = 0;
            byte[] bytes = new byte[1024];
            while ((read = stream.read(bytes)) != -1) {
               out.write(bytes, 0, read);
            }
            stream.close();
            out.flush();
            out.close();
         } catch (Exception e) {
            e.printStackTrace();
         }
      }

      return Response.ok("file uploaded").build();
   }
   
   //This will receive Note and it will also create a new file for note also but where as 
   //in Attachment for note file will not be created.
   
   @POST
   @Path("/uploadMultipartWithNote")
   @Consumes(MediaType.MULTIPART_FORM_DATA)
   @Produces({"text/xml","text/html"})
   public String uploadFile(@Multipart("note") String note, @Multipart("upfile") MultipartBody body) throws IOException,Exception {
   //public String uploadFile(@Multipart(value = "note",required = false) String note, @Multipart("upfile") MultipartBody body) throws IOException,Exception {
	   
	   System.out.println("|||||||||||||Note with MultipartBody File Upload Request Received......");
	   List<Attachment> attachments = body.getAllAttachments();
	   for (Attachment attachment : attachments) {
		   
		   System.out.println("*****************************************");
			System.out.println("Attachment Content ID = "+attachment.getContentId());
			System.out.println("Attachment Content Type = "+attachment.getContentType());
			System.out.println("Attachment Headers = "+attachment.getHeaders());
			System.out.println("Attachment Headers List = "+attachment.getHeaderAsList("Content-Disposition"));
			System.out.println("------");
			System.out.println("ContentDisposition Type = "+attachment.getContentDisposition().getType());
			System.out.println("ContentDisposition Parameters = "+attachment.getContentDisposition().getParameters());
			
							
			System.out.println("*****************************************");
			String filename = attachment.getContentDisposition().getParameter("filename");
			System.out.println("FileName from Disposition : " + filename);
		   
         DataHandler handler = attachment.getDataHandler();
         try {
            InputStream stream = handler.getInputStream();
            MultivaluedMap<String, String> map = attachment.getHeaders();
            System.out.println("fileName : " + getFileName(map));
            OutputStream out = new FileOutputStream(new File("/home/brajasekharreddy/Documents/iMedRis/sponsor/" + getFileName(map)));

            int read = 0;
            byte[] bytes = new byte[1024];
            while ((read = stream.read(bytes)) != -1) {
               out.write(bytes, 0, read);
            }
            stream.close();
            out.flush();
            out.close();
         } catch (Exception e) {
            e.printStackTrace();
         }
      }
	   System.out.println("Note Data = "+note);
      return "uploaded MultipartBody with Note : " + note;  
   }
   
   
   public static boolean isEmpty(String strParam) {
       if ((!(strParam == null)) && (!(strParam.trim()).equals(""))
               && ((strParam.trim()).length() > 0)) {
           return false;
       } else {
           return true;
       }
   }
}