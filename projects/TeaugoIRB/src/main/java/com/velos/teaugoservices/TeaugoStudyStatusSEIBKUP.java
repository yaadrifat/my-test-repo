
package com.velos.teaugoservices;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.annotations.SchemaValidation;
import org.apache.cxf.jaxrs.model.wadl.Description;
import org.apache.cxf.jaxrs.model.wadl.Descriptions;
import org.apache.cxf.jaxrs.model.wadl.DocTarget;

@WebService
@Consumes({"application/xml","application/json"})
@Produces({"application/xml","application/json"}) //WORKING
//@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
//@SchemaValidation
public interface TeaugoStudyStatusSEIBKUP {
	
  @POST
  @Path("/updateStudyStatus")
  //@Consumes("application/xml")
  //@Produces("application/xml")
  //@Produces("application/xml,application/json")
  //@Produces({"application/xml","application/json"})
  //@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
  @WebResult(name = "Response")
  @Descriptions({
		@Description(value = "returns studystaus update ", target = DocTarget.METHOD),
		@Description(value = "status of update", target = DocTarget.RETURN)
  })
  //@SchemaValidation
  //StudyStatus updateStudyStatus(@WebParam(name = "StudyStatus")StudyStatus studyStatus)throws ServiceException;
  ServiceResponse updateStudyStatus(@Description(value = "Study Staus to update") @WebParam(name = "StudyStatus") StudyStatus studyStatus)throws TeaugoServiceException;
  
  }
