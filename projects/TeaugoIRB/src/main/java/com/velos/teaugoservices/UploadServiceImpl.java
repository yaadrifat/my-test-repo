 package com.velos.teaugoservices;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.cxf.jaxrs.ext.multipart.Multipart;
import org.apache.cxf.jaxrs.ext.multipart.MultipartBody;
import org.apache.log4j.Logger;

import com.velos.integration.dao.MessageDAO;
import com.velos.integration.espclient.VelosSendStudyData;
import com.velos.integration.mapping.ProtocolKeys;
import com.velos.integration.mapping.VelosKeys;
import com.velos.integration.notifications.EmailNotification;

public class UploadServiceImpl implements FileUploadSEI {

	
	private String getFileName(MultivaluedMap<String, String> header) {
      String[] contentDisposition = header.getFirst("Content-Disposition").split(";");
      for (String filename : contentDisposition) {
         if ((filename.trim().startsWith("filename"))) {
            String[] name = filename.split("=");
            String exactFileName = name[1].trim().replaceAll("\"", "");
            return exactFileName;
         }
      }
      return "unknown";
   }
   
     
	public static Logger logger = Logger.getLogger(UploadServiceImpl.class);
	
	private VelosSendStudyData velosSendStudyData;

	public VelosSendStudyData getVelosSendStudyData() {
		return velosSendStudyData;
	}

	public void setVelosSendStudyData(VelosSendStudyData velosSendStudyData) {
		this.velosSendStudyData = velosSendStudyData;
	}
	
	private MessageDAO messageDao;
	
	public MessageDAO getMessageDao() {
		return messageDao;
	}

	public void setMessageDao(MessageDAO messageDao) {
		this.messageDao = messageDao;
	}

	private EmailNotification emailNotification;

	
	public EmailNotification getEmailNotification() {
		return emailNotification;
	}

	public void setEmailNotification(EmailNotification emailNotification) {
		this.emailNotification = emailNotification;
	}

   
   //This will receive Note and it will NOT create a new file for note.(Use this method)
   
   
   //public String uploadFile(@Multipart("note") String note, @Multipart("upfile") List<Attachment> attachments) throws IOException,Exception {
   //public String uploadFile(@Multipart(value = "note",required = false) String note, @Multipart("upfile") List<Attachment> attachments) throws IOException,Exception {
   public ServiceResponse uploadFile(String irbProtocolNumber,String versionNumber,String versionDate,String category,String type,List<Attachment> attachments,String description,String url,String urlDescription) throws IOException,Exception {
	   
	   logger.info("||||| File Upload Request Received......");
	   System.out.println("||||| File Upload Request Received......");
	   Map<VelosKeys, Object> frequestMap = null;
	   try{
		   logger.info("Version Number = "+versionNumber);
		frequestMap = new HashMap<VelosKeys, Object>();
		velosSendStudyData.uploadFiles(frequestMap,attachments,irbProtocolNumber,versionNumber,versionDate,category,type,description,url,urlDescription);
	   }catch(Exception e){
			logger.error("Failed to Create version :",e);
			ServiceResponse response = new ServiceResponse();
			response.setStatus("Failed");
			String errMsg="";
			if(e instanceof NullPointerException || isEmpty(e.getMessage())){
				errMsg="Failed to Create Version "+versionNumber +"  for IRB Prtocol Number : "+irbProtocolNumber;
				response.setMessage(errMsg);
			}else{
				errMsg=e.getMessage();
				response.setMessage(errMsg);
			}
			String studyNumber = (String)frequestMap.get(ProtocolKeys.StudyNumber);
			messageDao.saveVersionDetails(irbProtocolNumber,studyNumber,versionNumber,"Failed",errMsg);
			
			try{
			InputStream	input=this.getClass().getClassLoader().getResourceAsStream("email.properties");
			Properties prop = new Properties();
			prop.load(input);
			
			String enableNotification = null;
			
			if (prop.containsKey("Enable_Notification_For_Failure") && !"".equals(prop.getProperty("Enable_Notification_For_Failure").trim())) {
				enableNotification = prop.getProperty("Enable_Notification_For_Failure").trim();
			}
			if ((enableNotification != null) && (!"".equals(enableNotification)) && (enableNotification.equalsIgnoreCase("Y"))){
				String piscemail="";
				//piscemail = velosSendStudyData.getStudyPIContactEmail(frequestMap);
				emailNotification.sendVerFailNotification(frequestMap,piscemail,irbProtocolNumber,versionNumber,category, type);
			}else{
				logger.info("Notification For Failure is Turned OFF");
			}
			}catch(Exception ex){
				logger.error("Error while sending notification",ex);
			}
			
			
			throwTeaugoException(new TeaugoServiceException(response));
		}
		
	   ServiceResponse response = new ServiceResponse();
		response.setStatus("Success");
		response.setMessage("Version : "+versionNumber +" Created for study with IRB Protocol Number : "+irbProtocolNumber);
	
	return response;
	
		//	   logger.info("Note Data = "+note);
      //return "uploaded " + note;  
   }  
   
   public static void throwTeaugoException(TeaugoServiceException serviceException)throws TeaugoServiceException {

		throw new TeaugoServiceException("Fault Occured",serviceException.getFaultDetails());
   }

     
   
   public static boolean isEmpty(String strParam) {
       if ((!(strParam == null)) && (!(strParam.trim()).equals(""))
               && ((strParam.trim()).length() > 0)) {
           return false;
       } else {
           return true;
       }
   }
   
   public ServiceResponse uploadFileInitial(String irbProtocolNumber,String versionNumber,String versionDate,String category,String type,List<Attachment> attachments) throws IOException,Exception {
	   
	   logger.info("|||||||||||Attachment List : Note with Multiple File Upload Request Received......");
		if (attachments != null && !attachments.isEmpty()) {
			for (Attachment attachment : attachments) {
				
				logger.info("*****************************************");
				logger.info("Attachment Content ID = "+attachment.getContentId());
				logger.info("Attachment Content Type = "+attachment.getContentType());
				logger.info("Attachment Headers = "+attachment.getHeaders());
				logger.info("Attachment Headers List = "+attachment.getHeaderAsList("Content-Disposition"));
				logger.info("------");
				logger.info("ContentDisposition Type = "+attachment.getContentDisposition().getType());
				logger.info("ContentDisposition Parameters = "+attachment.getContentDisposition().getParameters());
				logger.info("------");
				DataHandler handlerT = attachment.getDataHandler();
				logger.info("DataHandler Content Type = "+ handlerT.getContentType());
				logger.info("DataHandler Name = "+ handlerT.getName());				
				logger.info("*****************************************");
				
				String filename = attachment.getContentDisposition().getParameter("filename");
				logger.info("FileName from Disposition : " + filename);
				if(!isEmpty(filename)){
				DataHandler handler = attachment.getDataHandler();
				try {
					InputStream stream = handler.getInputStream();
					logger.info("Input Stream size = "+stream.available());
					MultivaluedMap<String, String> map = attachment.getHeaders();
					logger.info("fileName : " + getFileName(map));
					OutputStream out = new FileOutputStream(new File("/home/brajasekharreddy/Documents/iMedRis/sponsor/"+filename));

					int read = 0;
					//byte[] bytes = new byte[1024];
					byte[] bytes = new byte[stream.available()]; //Creating with Stream size
					while ((read = stream.read(bytes)) != -1) {
						out.write(bytes, 0, read);
					}
					stream.close();
					out.flush();
					logger.info("File Size = "+bytes.length);
					logger.info("--------------------------------------------");
					logger.info(bytes.toString());
					logger.info("--------------------------------------------");
					logger.info(read);
					logger.info("--------------------------------------------");
					//Prints the bytes
					//logger.info(Arrays.toString(bytes));
					//logger.info("--------------------------------------------");
					//Prints the contents of the file
					//System.out.write(bytes);
					out.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				}else{
					logger.info("Filename not provided OR File is not Attached");
				}
			}
		}else{
			logger.info("***  NO  Attachments  ***");
		}
		
		logger.info("Uploading Files");
		Map<VelosKeys, Object> frequestMap = new HashMap<VelosKeys, Object>();
		velosSendStudyData.uploadFiles(frequestMap,attachments,irbProtocolNumber,versionNumber,versionDate,category,type,"","","");
		
	   ServiceResponse response = new ServiceResponse();
		response.setStatus("SUCCESS");
		response.setMessage("File Uploaded Successfully ");
	
	return response;
	
		//	   logger.info("Note Data = "+note);
      //return "uploaded " + note;  
   }  

}