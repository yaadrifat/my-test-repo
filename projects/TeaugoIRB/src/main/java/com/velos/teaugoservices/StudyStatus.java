package com.velos.teaugoservices;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "StudyStatus")
public class StudyStatus {
//Validate   
	private String irbProtocolNumber;
	private String statusType;
	private String status;
	private String documentedBy;
	private String statusValidFrom;
	private String currentStatus;
	private String statusValidUntil;

	public String getStatusValidUntil() {
		return statusValidUntil;
	}

	public void setStatusValidUntil(String statusValidUntil) {
		this.statusValidUntil = statusValidUntil;
	}

	public String getIrbProtocolNumber() {
		return irbProtocolNumber;
	}

	public void setIrbProtocolNumber(String irbProtocolNumber) {
		this.irbProtocolNumber = irbProtocolNumber;
	}

	public String getStatusType() {
		return statusType;
	}

	public void setStatusType(String statusType) {
		this.statusType = statusType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDocumentedBy() {
		return documentedBy;
	}

	public void setDocumentedBy(String documentedBy) {
		this.documentedBy = documentedBy;
	}

	public String getStatusValidFrom() {
		return statusValidFrom;
	}

	public void setStatusValidFrom(String statusValidFrom) {
		this.statusValidFrom = statusValidFrom;
	}

	public String getCurrentStatus() {
		return currentStatus;
	}

	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}

}