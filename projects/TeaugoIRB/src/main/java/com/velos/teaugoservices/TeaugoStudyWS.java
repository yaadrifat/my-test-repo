package com.velos.teaugoservices;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.jws.WebParam;
import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.model.wadl.Description;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.PhaseInterceptorChain;
import org.apache.log4j.Logger;

import com.velos.integration.dao.MessageDAO;
import com.velos.integration.espclient.VelosSendStudyData;
import com.velos.integration.mapping.ProtocolKeys;
import com.velos.integration.mapping.VelosKeys;
import com.velos.integration.notifications.EmailNotification;

@WebService(endpointInterface = "com.velos.teaugoservices.TeaugoStudySEI")
public class TeaugoStudyWS implements TeaugoStudySEI {
	
	public static Logger logger = Logger.getLogger(TeaugoStudyWS.class);
	
	private VelosSendStudyData velosSendStudyData;

	public VelosSendStudyData getVelosSendStudyData() {
		return velosSendStudyData;
	}

	public void setVelosSendStudyData(VelosSendStudyData velosSendStudyData) {
		this.velosSendStudyData = velosSendStudyData;
	}
	
	private MessageDAO messageDao;
	
	public MessageDAO getMessageDao() {
		return messageDao;
	}

	public void setMessageDao(MessageDAO messageDao) {
		this.messageDao = messageDao;
	}

	private EmailNotification emailNotification;

	
	public EmailNotification getEmailNotification() {
		return emailNotification;
	}

	public void setEmailNotification(EmailNotification emailNotification) {
		this.emailNotification = emailNotification;
	}



	
	public ServiceResponse updateStudyStatus(StudyStatus studyStatus)throws Exception {
		logger.info("###### Update StudyStatus Request Received.........");
		System.out.println("###### Update StudyStatus Request Received.........");
		Map<VelosKeys, Object> requestMap = null;
		try{
			/*if (studyStatus.getCurrentStatus().equals("y")) {
				studyStatus.setCurrentStatus("TRUE");
			} else if (studyStatus.getCurrentStatus().equals("n")) {
				studyStatus.getCurrentStatus().substring(0, 10);
				studyStatus.setCurrentStatus("FALSE");
			}else if (studyStatus.getCurrentStatus().equals("x")) {
				String message = null;
				message.substring(5);
			}else{
				throw new Exception();
			}
			if(isEmpty(studyStatus.getStatus())){
				throw new Exception("Study Status value is required");
			}*/
			requestMap = new HashMap<VelosKeys, Object>();
			velosSendStudyData.updateStudyStatusineRes(requestMap,studyStatus);
		}catch(Exception e){
			logger.error("Failed to Update Study status :",e);
			//e.printStackTrace();
			//throw new Exception("Failed to add study status");
			ServiceResponse response = new ServiceResponse();
			response.setStatus("Failed");
			String errMsg="";
			if(e instanceof NullPointerException || isEmpty(e.getMessage())){
				errMsg="Failed to add study status";
				response.setMessage(errMsg);
				//throwTeaugoException(new TeaugoServiceException(response));
			}else{
				//ServiceResponse response = new ServiceResponse();
				//response.setStatus("Failed");
				errMsg=e.getMessage();
				response.setMessage(errMsg);
				//throwTeaugoException(new TeaugoServiceException(response));
			}
			String irbNumber = studyStatus.getIrbProtocolNumber();
			String stStatus = studyStatus.getStatus();
			String studyNumber = (String)requestMap.get(ProtocolKeys.StudyNumber);
			messageDao.saveStatusDetails(irbNumber,studyNumber,stStatus,"Failed",errMsg);
			
			try{
				InputStream	input=this.getClass().getClassLoader().getResourceAsStream("email.properties");
				Properties prop = new Properties();
				prop.load(input);
				
				String enableNotification = null;
				
				if (prop.containsKey("Enable_Notification_For_Failure") && !"".equals(prop.getProperty("Enable_Notification_For_Failure").trim())) {
					enableNotification = prop.getProperty("Enable_Notification_For_Failure").trim();
				}
				if ((enableNotification != null) && (!"".equals(enableNotification)) && (enableNotification.equalsIgnoreCase("Y"))){
					String piscemail="";
					//piscemail = velosSendStudyData.getStudyPIContactEmail(requestMap);
					emailNotification.sendStatusFailNotification(requestMap,piscemail,studyStatus);
				}else{
					logger.info("Notification For Failure is Turned OFF");
				}
				}catch(Exception ex){
					logger.error("Error while sending notification",ex);
				}
			
			throwTeaugoException(new TeaugoServiceException(response));
			//return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(response).build();
			//throwException(response);
		//return response;
		}
		ServiceResponse response = new ServiceResponse();
			response.setStatus("Success");
			response.setMessage("Status Added Successfully");
		
		return response;
		//return Response.status(Response.Status.OK).entity(response).build();
	}
	
	//For ExceptionMapper Provider
	public static void throwTeaugoException(TeaugoServiceException serviceException)throws TeaugoServiceException {

			throw new TeaugoServiceException("Fault Occured",serviceException.getFaultDetails());
	}
	
	/*//Without Exception Mapper
	public static void throwTeaugoException(TeaugoServiceException serviceException)throws TeaugoServiceException {
		Message message = PhaseInterceptorChain.getCurrentMessage();
		HttpServletRequest servletRequest = (HttpServletRequest) message
				.get("HTTP.REQUEST");
		System.out.println("CONTENT TYPE : " + servletRequest.getContentType());
		if (servletRequest.getContentType().equals("application/json")
				|| servletRequest.getContentType().equals("application/xml")) {
			javax.ws.rs.core.Response.ResponseBuilder builder = Response
					.status(javax.ws.rs.core.Response.Status.NOT_ACCEPTABLE);
			// builder.type("application/json");
			builder.entity(serviceException.getFaultDetails());
			throw new WebApplicationException(builder.build());
		} else {
			throw new TeaugoServiceException("Fault Occured",serviceException.getFaultDetails());
		}
	}*/
	
	public static boolean isEmpty(String strParam) {
        if ((!(strParam == null)) && (!(strParam.trim()).equals(""))
                && ((strParam.trim()).length() > 0)) {
            return false;
        } else {
            return true;
        }
    }
	
	
	
	/*public static void throwException(ServiceResponse serviceException)throws Exception {
		Message message = PhaseInterceptorChain.getCurrentMessage();
		HttpServletRequest servletRequest = (HttpServletRequest) message
				.get("HTTP.REQUEST");
		System.out.println("CONTENT TYPE : " + servletRequest.getContentType());
		if (servletRequest.getContentType().equals("application/json")
				|| servletRequest.getContentType().equals("application/xml")) {
			javax.ws.rs.core.Response.ResponseBuilder builder = Response
					.status(javax.ws.rs.core.Response.Status.NOT_ACCEPTABLE);
			// builder.type("application/json");
			builder.entity(serviceException);
			throw new WebApplicationException(builder.build());
		} else {
			throw new Exception(serviceException.getMessage());
		}
	}*/
	
	

	/*public StudyStatus updateStudyStatus(StudyStatus studyStatus)throws ServiceException {
		if (studyStatus.getCurrentStatus().equals("y")) {
			studyStatus.setCurrentStatus("TRUE");
			velosSendStudyData.sendStudyPackage(studyStatus);
			return studyStatus;
		} else if (studyStatus.getCurrentStatus().equals("n")) {
			studyStatus.setCurrentStatus("FALSE");
			velosSendStudyData.sendStudyPackage(studyStatus);
			return studyStatus;
		} else {
			ServiceExceptionDetails serviceFaultDetailArray[] = new ServiceExceptionDetails[2];
			ServiceExceptionDetails serviceExceptionDetails = new ServiceExceptionDetails();
			serviceExceptionDetails.setFaultCode("100");
			serviceExceptionDetails.setFaultMessage("Status is not correct");
			serviceFaultDetailArray[0] = serviceExceptionDetails;
			ServiceExceptionDetails serviceExceptionDetails2 = new ServiceExceptionDetails();
			serviceExceptionDetails2.setFaultCode("200");
			serviceExceptionDetails2.setFaultMessage("2-Status is not correct");
			serviceFaultDetailArray[1] = serviceExceptionDetails2;
			throwException(new ServiceException(serviceFaultDetailArray));
			return null;
		}
	}

	// You can move this method into some utility class so that you can reuse
	// for all the services
	public static void throwException(ServiceException serviceException)
			throws ServiceException {
		Message message = PhaseInterceptorChain.getCurrentMessage();
		HttpServletRequest servletRequest = (HttpServletRequest) message
				.get("HTTP.REQUEST");
		System.out.println("CONTENT TYPE : " + servletRequest.getContentType());
		if (servletRequest.getContentType().equals("application/json")
				|| servletRequest.getContentType().equals("application/xml")) {
			javax.ws.rs.core.Response.ResponseBuilder builder = Response
					.status(javax.ws.rs.core.Response.Status.NOT_ACCEPTABLE);
			// builder.type("application/json");
			builder.entity(serviceException.getFaultDetails());
			throw new WebApplicationException(builder.build());
		} else {
			throw new ServiceException("Fault Message",
					serviceException.getFaultDetails());
		}
	}*/
	
	public ServiceResponse updateStudySummary(StudyUpdates studyUpdates)throws TeaugoServiceException{
		
		logger.info("###### Summary Request Received.........");
		System.out.println("###### Summary Request Received.........");
		Map<VelosKeys, Object> requestMap3 = null;
		try{
			requestMap3 = new HashMap<VelosKeys, Object>();
			velosSendStudyData.updateStudyineRes(requestMap3,studyUpdates);
		}catch(Exception e){
			logger.error("Failed to Update Study Summary :",e);
			ServiceResponse response = new ServiceResponse();
			response.setStatus("Failed");
			String errMsg="";
			if(e instanceof NullPointerException || isEmpty(e.getMessage())){
				errMsg="Failed to Update study";
				response.setMessage(errMsg);
			}else{
				errMsg=e.getMessage();
				response.setMessage(errMsg);
			}
			String irbNumber = studyUpdates.getIrbProtocolNumber();
			String studyNumber = (String)requestMap3.get(ProtocolKeys.StudyNumber);
			messageDao.saveStatusDetails(irbNumber,studyNumber,"","Failed",errMsg);
			
			try{
				InputStream	input=this.getClass().getClassLoader().getResourceAsStream("email.properties");
				Properties prop = new Properties();
				prop.load(input);
				
				String enableNotification = null;
				
				if (prop.containsKey("Enable_Notification_For_Failure") && !"".equals(prop.getProperty("Enable_Notification_For_Failure").trim())) {
					enableNotification = prop.getProperty("Enable_Notification_For_Failure").trim();
				}
				if ((enableNotification != null) && (!"".equals(enableNotification)) && (enableNotification.equalsIgnoreCase("Y"))){
					String piscemail="";
					//piscemail = velosSendStudyData.getStudyPIContactEmail(requestMap3);
					emailNotification.sendSummaryFailNotification(requestMap3,piscemail,studyUpdates);
				}else{
					logger.info("Notification For Failure is Turned OFF");
				}
				}catch(Exception ex){
					logger.error("Error while sending notification",ex);
				}
			
			throwTeaugoException(new TeaugoServiceException(response));
			}
		

		
		ServiceResponse response = new ServiceResponse();
		response.setStatus("Success");
		response.setMessage("Study Summary Updated Successfully");
	
	return response;	
	}

}