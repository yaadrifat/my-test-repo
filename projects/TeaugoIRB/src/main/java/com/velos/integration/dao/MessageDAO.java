package com.velos.integration.dao;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import scala.util.Properties;

import com.velos.integration.core.messaging.OutboundMessageBean;
import com.velos.integration.mapping.ProtocolKeys;
import com.velos.integration.mapping.VelosKeys;
import com.velos.services.StudyPatientFormResponse;

public class MessageDAO {
	
	private static Logger logger = Logger.getLogger(MessageDAO.class);
	
	private DataSource eresDataSource;
	private JdbcTemplate jdbcTemplate;
	
	public void setEresDataSource(DataSource eresDataSource) {
		this.eresDataSource = eresDataSource;
		this.jdbcTemplate = new JdbcTemplate(eresDataSource);
		logger.info("*******Jdbc Connection created successfully");	}
	
	public boolean saveMessageDetails(String StudyNumber,String BroadCasting_Site,String Participating_Site,String Component_Type,String Component_Name,String Status,String response_message) {
				
		boolean isSuccess = false;
		try{
			String sql = "INSERT INTO BeamHub_AUDIT_DETAILS"
						+"(PK_MSG_AUDIT,STUDYNUMBER,BROADCASTING_SITE,PARTICIPATING_SITE,COMPONENT_TYPE,COMPONENT_NAME,STATUS,RESPONSE_MESSAGE,CREATION_DATE)"
						+" VALUES(nextval('seq_BeamHub_AUDIT_DETAILS'),?,?,?,?,?,?,?,current_timestamp)";

			jdbcTemplate.update(sql,new Object[]{StudyNumber,BroadCasting_Site,Participating_Site,Component_Type,Component_Name,Status,response_message});
			isSuccess = true;
		}catch(Exception e){
			logger.error("Database Error : ",e);
			isSuccess = false;
		}
		if(isSuccess){
			logger.info("//Successfully Saved in DB");
		}
		return isSuccess;
	}
	
	public boolean saveFormResponseDetails(String StudyNumber,String PatientID,String formName,String request_message,String response_message) {
		boolean isSuccess = false;
		try{
			String sql = "INSERT INTO BeamForms_AUDIT_DETAILS"
						+"(PK_MSG_AUDIT,STUDYNUMBER,PATIENTID,FORMNAME,REQUEST_MESSAGE,RESPONSE_MESSAGE,CREATION_DATE)"
						+" VALUES(nextval('seq_BeamForms_AUDIT_DETAILS'),?,?,?,?,?,current_timestamp)";

			jdbcTemplate.update(sql,new Object[]{StudyNumber,PatientID,formName,request_message,response_message});
			isSuccess = true;
		}catch(Exception e){
			logger.error("Database Error : ",e);
			isSuccess = false;
		}
		if(isSuccess){
			logger.info("//Successfully Saved in DB");
		}
		return isSuccess;
	}
	
	/*public FormsBean getFormsConfiguration(String formName,String studyNumber,String folder) throws Exception ,IOException{
		FormsBean fb =new FormsBean();
		try{
			String sql="select * from forms where formname='"+formName+"' and studynumber='"+studyNumber+"' AND folder='"+folder+"'";
			Map row=jdbcTemplate.queryForMap(sql);
			fb.setFormName((String)row.get("formname"));
			fb.setFormType((String)row.get("formtype"));
			fb.setFormOid((String)row.get("formoid"));
			fb.setStudyNumber((String)row.get("studynumber"));
			fb.setFolder((String)row.get("folder"));
			fb.setStudyEventOID((String)row.get("studyeventoid"));
			fb.setStudyEventRepeatKey((String)row.get("studyeventrepeatkey"));
		} catch (Exception e) {
			logger.error("Error While getting Forms Configuration");
			//e.printStackTrace();
			throw e;
		}
		return fb;
	}
	
	public List<FormsFieldsBean> getFormFieldsConfiguration(String formName,String studyNumber) throws Exception ,IOException{
		List<FormsFieldsBean> formsFieldsList=new ArrayList<FormsFieldsBean>();
		try{
			String sql="select * from forms_fields where formname='"+formName+"' and studynumber='"+studyNumber+"'";
			List<Map> rows=jdbcTemplate.queryForList(sql);
			for(Map row:rows)
			{
				FormsFieldsBean ffb =new FormsFieldsBean();
				ffb.setFormName((String)row.get("formname"));
				ffb.setFieldName((String)row.get("fieldname"));
				ffb.setFieldId((String)row.get("fieldid"));
				ffb.setFieldOid((String)row.get("fieldoid"));
				ffb.setDataDictionaryName((String)row.get("datadictionaryname"));
				ffb.setControlType((String)row.get("controltype"));
				formsFieldsList.add(ffb);
			}
		} catch (Exception e) {
			logger.error("Error While getting Forms Fields Configuration");
			//e.printStackTrace();
			throw e;
		}
		return formsFieldsList;
	}

	public String getFormFieldsMapConfiguration(String studyNumber,String dictionaryName,String originalValue) throws Exception ,IOException{
		String mappedValue ="";
		try{
			String sql="select mappedvalue from forms_fields_mapping_values where studynumber='"+studyNumber+"' and datadictionaryname='"+dictionaryName+"' and originalvalue='"+originalValue+"'";
			mappedValue=(String) jdbcTemplate.queryForObject(sql, String.class);
		}catch(EmptyResultDataAccessException ere){
			logger.info("No Mapping value found for "+originalValue);
			return originalValue;
		}catch (Exception e) {
			logger.error("Error While getting Forms Fields Mapping Configuration");
			//e.printStackTrace();
			throw e;
		}
		return mappedValue;
	}
	
	public boolean saveformresponse(String StudyNumber,String patientID,String formName,String receivedXml,StudyPatientFormResponse formRes) {
		boolean isSuccess = false;
		try{
			String sql = "INSERT INTO formresponse"
					+"(PK_MSG_AUDIT,STUDYNUMBER,PATIENT_ID,FORMNAME,FORMRESPONSEDATA,RECEIVED_MESSAGE,STATUS,CREATION_DATE)"
					+" VALUES(nextval('seq_formresponse'),?,?,?,?,?,'Not Exported',current_timestamp)";
	        ByteArrayOutputStream bos = new ByteArrayOutputStream();
	        ObjectOutputStream oos = new ObjectOutputStream(bos);
	        oos.writeObject(formRes);
	        oos.flush();
	        oos.close();
	        bos.close();
	        byte[] data = bos.toByteArray();
			jdbcTemplate.update(sql,new Object[]{StudyNumber,patientID,formName,data,receivedXml});
			isSuccess = true;
		}catch(Exception e){
			logger.error("Database Error : ",e);
			isSuccess = false;
		}
		if(isSuccess){
			logger.info("//Successfully Saved in DB");
		}
		return isSuccess;
	} 
	
	public boolean updateMessageStatus(String studyNumber,String patientID,String formName,String receivedXml,String status) {
		boolean isSuccess = false;
		try{
			String sql = "Update target_formresponse set status = '"+status+"' where "
					+"STUDYNUMBER='"+studyNumber+"' and PATIENT_ID='"+patientID+"' and FORMNAME='"+formName+"' and RECEIVED_MESSAGE='"+receivedXml+"'";
			int rowsUpdated = jdbcTemplate.update(sql,new Object[]{});
			logger.info("Rows Updated = "+rowsUpdated);
			isSuccess = true;
		}catch(Exception e){
			logger.error("Database Error : ",e);
			isSuccess = false;
		}
		if(isSuccess){
			logger.info("//Successfully Saved in DB");
		}
		return isSuccess;
	}*/ 
	
	public boolean saveStatusDetails(String irbNumber,String studyNumber,String studyStatus,String status,String response_message) {
		
		boolean isSuccess = false;
		try{
			String sql = "INSERT INTO IRB_AUDIT_DETAILS"
						+"(PK_MSG_AUDIT,MESSAGE_TYPE,IRBNUMBER,STUDYNUMBER,STUDYSTATUS,STATUS,RESPONSE,CREATION_DATE)"
						+" VALUES(nextval('seq_IRB_AUDIT_DETAILS'),'INBOUND',?,?,?,?,?,current_timestamp)";

			jdbcTemplate.update(sql,new Object[]{irbNumber,studyNumber,studyStatus,status,response_message});
			isSuccess = true;
		}catch(Exception e){
			logger.error("Database Error : ",e);
			isSuccess = false;
		}
		if(isSuccess){
			logger.info("//Successfully Saved in DB");
		}
		return isSuccess;
	}
	
	public boolean saveVersionDetails(String irbNumber,String studyNumber,String versionNumber,String status,String response_message) {
		
		boolean isSuccess = false;
		try{
			String sql = "INSERT INTO IRB_AUDIT_DETAILS"
						+"(PK_MSG_AUDIT,MESSAGE_TYPE,IRBNUMBER,STUDYNUMBER,VERSIONNUMBER,STATUS,RESPONSE,CREATION_DATE)"
						+" VALUES(nextval('seq_IRB_AUDIT_DETAILS'),'INBOUND',?,?,?,?,?,current_timestamp)";

			jdbcTemplate.update(sql,new Object[]{irbNumber,studyNumber,versionNumber,status,response_message});
			isSuccess = true;
		}catch(Exception e){
			logger.error("Database Error : ",e);
			isSuccess = false;
		}
		if(isSuccess){
			logger.info("//Successfully Saved in DB");
		}
		return isSuccess;
	}

	public boolean insertOutboundMessageDetails(String module,String studyNumber,String status,String action,String timestamp,String statusOID) {
		
		boolean isSuccess = false;
		try{
			String sql = "INSERT INTO vgdb_outbound_message"
						+"(pk_vgdb_outbound_message,message_type,process_flag,module_name,studynumber,status,action,trigger_timestamp,record_creation_date,count,oid)"
						+" VALUES(nextval('seq_vgdb_outbound_message'),'OUTBOUND','false',?,?,?,?,?,current_timestamp,0,?)";

			jdbcTemplate.update(sql,new Object[]{module,studyNumber,status,action,timestamp,statusOID});
			isSuccess = true;
		}catch(Exception e){
			logger.error("Database Error : ",e);
			logger.error("Failed to add to vgdb_outbound_message for Study Number="+studyNumber +" Module="+module+" Status="+status+" Action="+action+" TimeStamp="+timestamp);
			isSuccess = false;
		}
		if(isSuccess){
			logger.info("//Successfully Saved in DB");
		}
		return isSuccess;
	}

	public List<OutboundMessageBean> getOutboundMessageBean(String moduleName,int outboundMsgCount) {

		List<OutboundMessageBean> messagelist = null;
		OutboundMessageBean omb = null;

		try {
			messagelist = 	new ArrayList<OutboundMessageBean>();
			String sql = "select * from vgdb_outbound_message where process_flag = 'false' and module_name = '"+moduleName+"'  and count < " + outboundMsgCount;
			logger.info("Getting Outbound Message Query ==>" + sql);
			List<Map> rows = jdbcTemplate.queryForList(sql);
			for (Map row : rows) {
				omb = new OutboundMessageBean();
				omb.setMessageType((String) row.get("message_type"));
				omb.setAction((String) row.get("action"));
				omb.setModuleName((String) row.get("module_name"));
				omb.setProcessFlag((String) row.get("process_flag"));
				omb.setStudyNumber((String) row.get("studynumber"));
				omb.setStatus((String) row.get("status"));
				omb.setMsgTimeStamp(row.get("trigger_timestamp"));
				omb.setRecordCreationDate(row.get("record_creation_date"));
				omb.setOID((String) row.get("oid"));
				omb.setPk_vgdb_outbound_message(((Integer)row.get("pk_vgdb_outbound_message")).intValue());
				omb.setCount(((Integer)row.get("count")).intValue());

				messagelist.add(omb);
			}
			return messagelist;
		} catch (Exception e) {
			logger.info("************OutBoundMessageBean Dao Exception block ************", e);
		} 
		return messagelist;
	}
	
	public void updateOutboundMessageInfo(OutboundMessageBean omb) {

		String studyId = null;
		Date msgtimeStamp = null;
		Date recordCreationtimeStamp = null;
		String query = null;

		try {
			studyId = omb.getStudyNumber();
			int pk_out_msg = omb.getPk_vgdb_outbound_message();

			query = "update vgdb_outbound_message set process_flag='true' where process_flag = 'false' and studyNumber='" + studyId + "'and pk_vgdb_outbound_message = " + pk_out_msg;
			
			jdbcTemplate.update(query);
		} catch (Exception e) {
			logger.error("Failed to update outbound message info : ",e);
		}
	}
	
	public boolean saveStatusDetails(OutboundMessageBean omb,String status,String response_message,String request) {
		
		boolean isSuccess = false;
		try{
			
			int pk_out_msg = omb.getPk_vgdb_outbound_message();
			String studyNumber = omb.getStudyNumber();
			String oid = omb.getOID();
			
			String sql = "INSERT INTO IRB_AUDIT_DETAILS"
						+"(PK_MSG_AUDIT,MESSAGE_TYPE,fk_vgdb_outbound_message,STUDYNUMBER,OID,STATUS,RESPONSE,CREATION_DATE,request)"
						+" VALUES(nextval('seq_IRB_AUDIT_DETAILS'),'OUTBOUND',?,?,?,?,?,current_timestamp,?)";

			jdbcTemplate.update(sql,new Object[]{pk_out_msg,studyNumber,oid,status,response_message,request});
			isSuccess = true;
		}catch(Exception e){
			logger.error("Database Error : ",e);
			isSuccess = false;
		}
		if(isSuccess){
			logger.info("//Successfully Saved in DB");
		}
		return isSuccess;
	}

	public int getCount(int pk_id, int flag) {
		int i = 0;
		String sql = null;
		try {
			if (flag == 0) {
				sql = " select count from vgdb_outbound_message where pk_vgdb_outbound_message ="+ pk_id;
				i = jdbcTemplate.queryForInt(sql);
			}
			if (flag > 0) {
				sql = "update vgdb_outbound_message set count = "+flag+" where pk_vgdb_outbound_message ="+ pk_id;
				i = jdbcTemplate.update(sql);
			}
		} catch (Exception e) {
			logger.info("Query = "+sql);
			logger.error("Failed to Get/Update count ",e);
		}
		return i;

	}
}
