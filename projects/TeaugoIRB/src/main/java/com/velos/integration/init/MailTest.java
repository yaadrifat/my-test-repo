package com.velos.integration.init;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;



public class MailTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try{
			
			// Recipient's email ID needs to be mentioned.
			String to = "brajasekharreddy@chn.aithent.com,msurendra@chn.aithent.com";
			//String cc=prop.getProperty("mail.cc");
			// Sender's email ID needs to be mentioned
			final String from= "notifications@velos.com";
			final String password= "velos1234";
			
			String host ="smtp.gmail.com";
			String port ="465";
			String authentication="true";
			
			Properties properties = System.getProperties();
			properties.put("mail.smtp.host", host);
			properties.put("mail.smtp.port", port);
			//Session session = Session.getDefaultInstance(properties);
			properties.put("mail.smtp.auth",authentication);
			properties.setProperty("mail.debug", "true");
			
			properties.put("mail.smtp.socketFactory.port", "465");
			properties.put("mail.smtp.socketFactory.class",	"javax.net.ssl.SSLSocketFactory");
	 
			Session session = Session.getDefaultInstance(properties,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(from,password);
					}
				});
			
							
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(to));
			
			String subject1="Mutilple Test Notifications";
			
											
			StringBuilder subject = new StringBuilder();
			subject.append(subject1);
		

			message.setSubject(subject.toString());

			String body1="Test notifications for CTXpress from Sample Test Program";
			
			StringBuilder msgText = new StringBuilder();
			msgText.append(body1);
			

			message.setText(msgText.toString());
			
			Transport.send(message);
			
			
		}
		catch(Exception e){
			e.printStackTrace();
			
		}

	}

}
