package com.velos.integration.core.messaging;

public class OutboundMessageBean {
	private String messageType;
	private String processFlag;
	private String moduleName;
	private String studyNumber;
	private String status;
	private String action;
	private Object recordCreationDate;
	private Object msgTimeStamp;
	private String OID;
	private int pk_vgdb_outbound_message;
	private int count;

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getPk_vgdb_outbound_message() {
		return pk_vgdb_outbound_message;
	}

	public void setPk_vgdb_outbound_message(int pk_vgdb_outbound_message) {
		this.pk_vgdb_outbound_message = pk_vgdb_outbound_message;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getProcessFlag() {
		return processFlag;
	}

	public void setProcessFlag(String processFlag) {
		this.processFlag = processFlag;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getStudyNumber() {
		return studyNumber;
	}

	public void setStudyNumber(String studyNumber) {
		this.studyNumber = studyNumber;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Object getRecordCreationDate() {
		return recordCreationDate;
	}

	public void setRecordCreationDate(Object recordCreationDate) {
		this.recordCreationDate = recordCreationDate;
	}

	public Object getMsgTimeStamp() {
		return msgTimeStamp;
	}

	public void setMsgTimeStamp(Object msgTimeStamp) {
		this.msgTimeStamp = msgTimeStamp;
	}

	public String getOID() {
		return OID;
	}

	public void setOID(String oID) {
		OID = oID;
	}

}
