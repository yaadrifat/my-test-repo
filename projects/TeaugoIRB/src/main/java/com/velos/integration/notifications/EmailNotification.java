package com.velos.integration.notifications;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.mail.MailParseException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring3.SpringTemplateEngine;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.velos.integration.mapping.ProtocolKeys;
import com.velos.integration.mapping.VelosKeys;
import com.velos.teaugoservices.StudyStatus;
import com.velos.teaugoservices.StudyUpdates;

public class EmailNotification {
	private static Logger logger = Logger
			.getLogger(EmailNotification.class.getName());

	private JavaMailSender mailSender;
	private SimpleMailMessage smMessage;
	Properties prop = new Properties();
	InputStream input,input1;



	public SimpleMailMessage getSmMessage() {
		return smMessage;
	}

	public void setSmMessage(SimpleMailMessage smMessage) {
		this.smMessage = smMessage;
	}

	public void setMailSender(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}

	//Error Email Notification
	public void sendMail(String subject,String errorMsg,String studyCoordinators){
		//logger.info("*******Study Contacts Error Email Notification ********");

		MimeMessage errormessage=mailSender.createMimeMessage();
		List<String> list = null;

		String studyContMsg= "";


		try{
			input=this.getClass().getClassLoader().getResourceAsStream("email.properties");
			prop.load(input);
			String environment = prop.getProperty("environment");  
			MimeMessageHelper helper = new MimeMessageHelper(errormessage, true);
			if(!"".equals(prop.getProperty("mail.from")) && prop.containsKey("mail.from")){
				helper.setFrom(smMessage.getFrom());
			}
			String to [] = new String[100];
			to =smMessage.getTo();

			if(studyCoordinators == null){
				//logger.info("*******No Study Contacts Error Email Notification ********");
				studyContMsg = prop.getProperty("mail.nostudycontacts");
				helper.setTo(to);

			}else if(studyCoordinators != null || !"".equals(studyCoordinators)){
				//logger.info("*******Study Contacts Error Email Notification starts ********");
				if(studyCoordinators.contains("@")){ 

					list = new ArrayList<String>(Arrays.asList(to));

					for(String a :studyCoordinators.split(",")){
						if(a.contains("@")){
							list.add(a);
						}else {
							studyContMsg += a+"\n";
						}
					}
					String emailId[]=new String[list.size()];

					for(int i=0; i<list.size();i++){
						emailId[i] = list.get(i);
						logger.info("Email ID = "+emailId[i]);
					}
					
					helper.setTo(emailId);

				} else if(!studyCoordinators.contains("@")){
					studyContMsg = studyCoordinators;
					helper.setTo(to);
				}
			}
//			helper.setSubject(enviornment + " : "+ smMessage.getSubject());
			helper.setSubject(environment + " : "+ subject);

			/*helper.setText(String.format(
					smMessage.getText(),"","\n"+ prop.getProperty("mail.msgbody").toString().trim()+"\n\n"
							+studyContMsg+"\n\n"+errorMsg));*/	
			helper.setText(String.format(smMessage.getText(),"","\n"+errorMsg));

			mailSender.send(errormessage);
			logger.info("Email Sent successfully");
		}catch(Exception e){
			//e.printStackTrace();
			logger.error("Email Sent Failed : ", e);
		}


	}
	
	public void sendCodelstNotification(Map<VelosKeys, Object> requestMap,String mailSubject,String participatingSite,String componentName,String status,String notfType){
		
		logger.info("sendCodelstNotification()");
		try{
			input=this.getClass().getClassLoader().getResourceAsStream("email.properties");
			prop.load(input);
			
			String enableNotification = null;
			
			if (prop.containsKey("Enable_Notification_For_CodelstMismatch") && !"".equals(prop.getProperty("Enable_Notification_For_CodelstMismatch").trim())) {
				enableNotification = prop.getProperty("Enable_Notification_For_CodelstMismatch").trim();
			}
		
			
		if(enableNotification!=null && !"".equals(enableNotification) && enableNotification.equalsIgnoreCase("Y")){
			
			String bSite = (String)requestMap.get(ProtocolKeys.BroadCastingSite);
			String studyNum = (String)requestMap.get(ProtocolKeys.StudyNumber);

			StringBuffer msgText = new StringBuffer();
			if (prop.containsKey("mail.codelstText") && !"".equals(prop.getProperty("mail.codelstText").trim())) {
				String msgTextData = prop.getProperty("mail.codelstText").trim();
				msgText.append(msgTextData);
			}
			msgText.append("\n");
			msgText.append(componentName);
			msgText.append("\n");
			if (prop.containsKey("mail.StudyNumber") && !"".equals(prop.getProperty("mail.StudyNumber").trim())) {
				String mailStudy = prop.getProperty("mail.StudyNumber").trim();
				msgText.append(mailStudy+" "+studyNum);
			}
			msgText.append("\n");
			if (prop.containsKey("mail.BroadcastingSite") && !"".equals(prop.getProperty("mail.BroadcastingSite").trim())) {
				String mailBSite = prop.getProperty("mail.BroadcastingSite").trim();
				msgText.append(mailBSite+" "+bSite);
			}
			msgText.append("\n");
			if (prop.containsKey("mail.ParticipatingSite") && !"".equals(prop.getProperty("mail.ParticipatingSite").trim())) {
				String mailPSite = prop.getProperty("mail.ParticipatingSite").trim();
				msgText.append(mailPSite+" "+participatingSite);
			}
			msgText.append("\n");
			if (prop.containsKey("mail.ComponentStatus") && !"".equals(prop.getProperty("mail.ComponentStatus").trim())) {
				String mailCStatus = prop.getProperty("mail.ComponentStatus").trim();
				msgText.append(mailCStatus+" "+status);
			}
			msgText.append("\n");
			String errorMsg = "";
			errorMsg = msgText.toString();
		
			logger.info("Sending Email");
				sendCodelstMail(mailSubject,errorMsg,notfType,participatingSite);
		}else{
			logger.info("Notification For Codelst Mismatch is Turned OFF");
		}
		}catch(Exception e){
			logger.error("Error : ",e);
		}
		
	}
	
	public void sendCodelstMail(String subject,String errorMsg,String notfType,String participatingSite){

	MimeMessage errormessage=mailSender.createMimeMessage();
	try{
		input=this.getClass().getClassLoader().getResourceAsStream("email.properties");
		prop.load(input);
		String environment = prop.getProperty("environment");  
		MimeMessageHelper helper = new MimeMessageHelper(errormessage, true);
		helper.setSubject(environment + " : "+ subject);
		helper.setText(String.format(smMessage.getText(),"","\n"+errorMsg));
		if(!"".equals(prop.getProperty("mail.from")) && prop.containsKey("mail.from")){
			helper.setFrom(smMessage.getFrom());
		}
		String toBroadEmail = null;
		
		if (prop.containsKey("mail.to.BroadCastingSite") && !"".equals(prop.getProperty("mail.to.BroadCastingSite").trim())) {
			toBroadEmail = prop.getProperty("mail.to.BroadCastingSite").trim();
		}
		
		logger.info("Broadcast Email ID = "+toBroadEmail);
		if(notfType.equals("1")){
			if(toBroadEmail!=null && !"".equals(toBroadEmail)){
				helper.setTo(toBroadEmail);
				mailSender.send(errormessage);
				logger.info("Email Sent successfully");
			}else{
				logger.error("Notification Failed.Broadcasting email is null");
			}
		}else if(notfType.equals("2")){
			String toPartEmail = null;
			String partEmailkey = "mail.to.ParticipatingSite."+participatingSite;
			if (prop.containsKey(partEmailkey) && !"".equals(prop.getProperty(partEmailkey).trim())) {
				toPartEmail = prop.getProperty(partEmailkey).trim();
			}
			 
			logger.info("Participant Email ID = "+toPartEmail);
			if(toPartEmail!=null && !"".equals(toPartEmail)){
				helper.setTo(toPartEmail);
				if(toBroadEmail!=null && !"".equals(toBroadEmail)){
					helper.setCc(toBroadEmail);
				}
				mailSender.send(errormessage);
				logger.info("Email Sent successfully");
			}else{
				logger.error("Notification Failed.Participating email is null for organization : "+participatingSite);
			}
		}
	}catch(Exception e){
		//e.printStackTrace();
		logger.error("Email Sent Failed : ", e);
	}


}

	private SimpleMailMessage funErrorMessage;
	
	
	public SimpleMailMessage getFunErrorMessage() {
		return funErrorMessage;
	}

	public void setFunErrorMessage(SimpleMailMessage funErrorMessage) {
		this.funErrorMessage = funErrorMessage;
	}

	public TemplateEngine emailTemplateEngine() {
        //TemplateEngine templateEngine = new TemplateEngine();
		final SpringTemplateEngine templateEngine = new SpringTemplateEngine();
        // Resolver for TEXT emails
        //templateEngine.addTemplateResolver(textTemplateResolver());
        // Resolver for HTML emails (except the editable one)
        templateEngine.addTemplateResolver(htmlTemplateResolver());
        // Resolver for HTML editable emails (which will be treated as a String)
        //templateEngine.addTemplateResolver(stringTemplateResolver());
        // Message source, internationalization specific to emails
        //templateEngine.setTemplateEngineMessageSource(emailMessageSource());
        return templateEngine;
	}

	public ClassLoaderTemplateResolver htmlTemplateResolver(){
        final ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        //templateResolver.setOrder(Integer.valueOf(2));
        //templateResolver.setResolvablePatterns(Collections.singleton("html/*"));
        templateResolver.setPrefix("/templates/");
        templateResolver.setSuffix(".html");
        //templateResolver.setTemplateMode(TemplateMode.HTML); //3.0.9 version
        templateResolver.setTemplateMode("HTML5"); //2.1.1 version
        templateResolver.setCharacterEncoding("UTF-8");
        templateResolver.setCacheable(false);
		return templateResolver;
    }
	
	
	public void sendFunctionalErrorNotification(Map<VelosKeys, Object> requestMap,String errorMessage){
		
		logger.info("send Functional Error Notification");
		try{
			input=this.getClass().getClassLoader().getResourceAsStream("email.properties");
			prop.load(input);
			
			String enableNotification = null;
			
			if (prop.containsKey("Enable_Notification_For_Functional_Errors") && !"".equals(prop.getProperty("Enable_Notification_For_Functional_Errors").trim())) {
				enableNotification = prop.getProperty("Enable_Notification_For_Functional_Errors").trim();
			}
			if ((enableNotification != null) && (!"".equals(enableNotification)) && (enableNotification.equalsIgnoreCase("Y"))){
				sendFunctionalErrorMail(requestMap,errorMessage);
			}else{
				logger.info("Notification For Functional Errors is Turned OFF");
			}
		}catch(Exception e){
			logger.error("Error : ",e);
		}
	}


	public void sendFunctionalErrorMail(Map<VelosKeys, Object> requestMap,String errorMessage){

	try{
		input=this.getClass().getClassLoader().getResourceAsStream("email.properties");
		prop.load(input);
		String environment = prop.getProperty("environment");
		String interfaceName = prop.getProperty("interfaceName");
		
		String studyNum = (String)requestMap.get(ProtocolKeys.StudyNumber);
		logger.info("StudyNumber : "+studyNum);
		String patientID = (String)requestMap.get(ProtocolKeys.PatientId);
		logger.info("PatientID : "+patientID);
		//String form = ((String)requestMap.get(ProtocolKeys.FormName)).trim();
		//logger.info("Form = "+form);
		
		String template = "FunctionalErrorTemplate";
	     
        // Prepare the evaluation context
        final Context ctx = new Context();
        ctx.setVariable("StudyNumber", studyNum);
        ctx.setVariable("PatientID", patientID);
        //ctx.setVariable("Form", form);
        ctx.setVariable("ErrorMessage",errorMessage);

        String html = this.emailTemplateEngine().process(template, ctx);
        
        String subject = "Failed to send form response"; 
    	
		final MimeMessage mimeMessage = this.mailSender.createMimeMessage();
	    final MimeMessageHelper helper	= new MimeMessageHelper(mimeMessage,"UTF-8");

		helper.setFrom(funErrorMessage.getFrom());
		helper.setTo(funErrorMessage.getTo());
		helper.setCc(funErrorMessage.getCc());
        helper.setSubject(interfaceName+" : "+ environment + " - "+ subject);
		helper.setText(html, true);
		
		mailSender.send(mimeMessage);
		logger.info("Email Sent successfully");
				
	}catch(Exception e){
		//e.printStackTrace();
		logger.error("Email Sent Failed : ", e);
	}
}
	
	
		
	public void sendVerSuccNotification(Map<VelosKeys, Object> requestMap,String piscemail,String irbProtocolNumber,String versionNumber,String category,String type){
		
		logger.info("send Version Success Notification");
		try{
			input=this.getClass().getClassLoader().getResourceAsStream("email.properties");
			prop.load(input);
			
			String enableNotification = null;
			
			if (prop.containsKey("Enable_Notification_For_Success") && !"".equals(prop.getProperty("Enable_Notification_For_Success").trim())) {
				enableNotification = prop.getProperty("Enable_Notification_For_Success").trim();
			}
			if ((enableNotification != null) && (!"".equals(enableNotification)) && (enableNotification.equalsIgnoreCase("Y"))){
				String template = "VersionSuccessTemplate";

		        // Prepare the evaluation context
		        final Context ctx = new Context();
		        ctx.setVariable("IRBNumber", irbProtocolNumber);
		        ctx.setVariable("VersionNumber", versionNumber);
		        ctx.setVariable("VersionType", type);
		        
		        String subject = "Version Sent Successfully";
		        
		        sendVerSuccNotification(requestMap,piscemail,template,ctx,subject);
			}else{
				logger.info("Notification For Success is Turned OFF");
			}
		}catch(Exception ex){
			logger.error("Error : ",ex);
		}
	}
	
	
	public void sendVerSuccNotification(Map<VelosKeys, Object> requestMap,String piscemail,String template,Context ctx,String subject){

		try{
			input=this.getClass().getClassLoader().getResourceAsStream("email.properties");
			prop.load(input);
			String environment = prop.getProperty("environment");
			String interfaceName = prop.getProperty("interfaceName");
			       

	        String html = this.emailTemplateEngine().process(template, ctx);
	        
	        final MimeMessage mimeMessage = this.mailSender.createMimeMessage();
		    final MimeMessageHelper helper	= new MimeMessageHelper(mimeMessage,"UTF-8");

			helper.setFrom(smMessage.getFrom());
			
			String to [] = new String[100];
			to =smMessage.getTo();
			List<String> list = null;
			list = new ArrayList<String>(Arrays.asList(to));
			if(piscemail != null || !"".equals(piscemail)){
				if(piscemail.contains("@")){ 
					for(String a :piscemail.split(",")){
						if(a.contains("@")){
							list.add(a);
						}
					}
				}
			}
			String emailId[]=new String[list.size()];
			for(int i=0; i<list.size();i++){
				emailId[i] = list.get(i);
				logger.info("Email ID = "+emailId[i]);
			}
			helper.setTo(emailId);
				
			/*if(piscemail != null || !"".equals(piscemail)){
				if(piscemail.contains("@")){ 
					list = new ArrayList<String>(Arrays.asList(to));
					for(String a :piscemail.split(",")){
						if(a.contains("@")){
							list.add(a);
						}
					}
					String emailId[]=new String[list.size()];
					for(int i=0; i<list.size();i++){
						emailId[i] = list.get(i);
						logger.info("Email ID = "+emailId[i]);
					}
					helper.setTo(emailId);
				}
			}*/
			
			helper.setCc(smMessage.getCc());
	        helper.setSubject(interfaceName+" : "+ environment + " - "+ subject);
			helper.setText(html, true);
			
			mailSender.send(mimeMessage);
			logger.info("Email Sent successfully");
					
		}catch(Exception ex){
			//e.printStackTrace();
			logger.error("Email Sent Failed : ", ex);
		}
	}

	
	public void sendStatusSuccNotification(Map<VelosKeys, Object> requestMap,String piscemail,StudyStatus stdStatus){
		logger.info("send Study Status Success Notification");
		try{
			input=this.getClass().getClassLoader().getResourceAsStream("email.properties");
			prop.load(input);
			
			String enableNotification = null;
			
			if (prop.containsKey("Enable_Notification_For_Success") && !"".equals(prop.getProperty("Enable_Notification_For_Success").trim())) {
				enableNotification = prop.getProperty("Enable_Notification_For_Success").trim();
			}
			if ((enableNotification != null) && (!"".equals(enableNotification)) && (enableNotification.equalsIgnoreCase("Y"))){
				String template = "StudyStatusSuccess";
				String irbNumber = (String)requestMap.get(ProtocolKeys.IrbNumber);

		        // Prepare the evaluation context
		        final Context ctx = new Context();
		        ctx.setVariable("IRBNumber", irbNumber);
		        
		        String subject = "Study Status Updated Successfully";
		        
		        sendVerSuccNotification(requestMap,piscemail,template,ctx,subject);
			}else{
				logger.info("Notification For Success is Turned OFF");
			}
		}catch(Exception ex){
			logger.error("Error : ",ex);
		}
	}
	
	
	public void sendStatusFailNotification(Map<VelosKeys, Object> requestMap,String piscemail,StudyStatus stdStatus){
		logger.info("send Study Status Fail Notification");
		try{
			input=this.getClass().getClassLoader().getResourceAsStream("email.properties");
			prop.load(input);
			
			String enableNotification = null;
			
			if (prop.containsKey("Enable_Notification_For_Failure") && !"".equals(prop.getProperty("Enable_Notification_For_Failure").trim())) {
				enableNotification = prop.getProperty("Enable_Notification_For_Failure").trim();
			}
			if ((enableNotification != null) && (!"".equals(enableNotification)) && (enableNotification.equalsIgnoreCase("Y"))){
				String template = "StudyStatusFailure";
				String irbNumber = (String)requestMap.get(ProtocolKeys.IrbNumber);

		        // Prepare the evaluation context
		        final Context ctx = new Context();
		        ctx.setVariable("IRBNumber", irbNumber);
		        ctx.setVariable("StudyStatus", stdStatus.getStatus());
		        
		        String subject = "Study Status Update Failed";
		        
		        sendVerSuccNotification(requestMap,piscemail,template,ctx,subject);
			}else{
				logger.info("Notification For Failure is Turned OFF");
			}
		}catch(Exception ex){
			logger.error("Error : ",ex);
		}
	}
	
	public void sendVerFailNotification(Map<VelosKeys, Object> requestMap,String piscemail,String irbProtocolNumber,String versionNumber,String category,String type){
		
		logger.info("send Version Failure Notification");
		try{
			input=this.getClass().getClassLoader().getResourceAsStream("email.properties");
			prop.load(input);
			
			String enableNotification = null;
			
			if (prop.containsKey("Enable_Notification_For_Failure") && !"".equals(prop.getProperty("Enable_Notification_For_Failure").trim())) {
				enableNotification = prop.getProperty("Enable_Notification_For_Failure").trim();
			}
			if ((enableNotification != null) && (!"".equals(enableNotification)) && (enableNotification.equalsIgnoreCase("Y"))){
				String template = "VersionFailureTemplate";

		        // Prepare the evaluation context
		        final Context ctx = new Context();
		        ctx.setVariable("IRBNumber", irbProtocolNumber);
		        ctx.setVariable("VersionNumber", versionNumber);
		        ctx.setVariable("VersionType", type);
		        
		        String subject = "Version Failed to send to eResearch";
		        
		        sendVerSuccNotification(requestMap,piscemail,template,ctx,subject);
			}else{
				logger.info("Notification For Failure is Turned OFF");
			}
		}catch(Exception ex){
			logger.error("Error : ",ex);
		}
	}

	public void sendSummaryFailNotification(Map<VelosKeys, Object> requestMap,String piscemail,StudyUpdates studyUpdates){
		logger.info("send Study Summary Fail Notification");
		try{
			input=this.getClass().getClassLoader().getResourceAsStream("email.properties");
			prop.load(input);
			
			String enableNotification = null;
			
			if (prop.containsKey("Enable_Notification_For_Failure") && !"".equals(prop.getProperty("Enable_Notification_For_Failure").trim())) {
				enableNotification = prop.getProperty("Enable_Notification_For_Failure").trim();
			}
			if ((enableNotification != null) && (!"".equals(enableNotification)) && (enableNotification.equalsIgnoreCase("Y"))){
				String template = "StudyUpdateFailure";

		        // Prepare the evaluation context
		        final Context ctx = new Context();
		        ctx.setVariable("StudyUpdates", studyUpdates);
		        
		        String subject = "Study Summary Update Failed";
		        
		        sendVerSuccNotification(requestMap,piscemail,template,ctx,subject);
			}else{
				logger.info("Notification For Failure is Turned OFF");
			}
		}catch(Exception ex){
			logger.error("Error : ",ex);
		}
	}

	public void sendSummarySuccNotification(Map<VelosKeys, Object> requestMap,String piscemail,StudyUpdates studyUpdates){
		logger.info("send Study Summary Success Notification");
		try{
			input=this.getClass().getClassLoader().getResourceAsStream("email.properties");
			prop.load(input);
			
			String enableNotification = null;
			
			if (prop.containsKey("Enable_Notification_For_Success") && !"".equals(prop.getProperty("Enable_Notification_For_Success").trim())) {
				enableNotification = prop.getProperty("Enable_Notification_For_Success").trim();
			}
			if ((enableNotification != null) && (!"".equals(enableNotification)) && (enableNotification.equalsIgnoreCase("Y"))){
				String template = "StudyUpdateSuccess";

		        // Prepare the evaluation context
		        final Context ctx = new Context();
		        ctx.setVariable("StudyUpdates", studyUpdates);
		        
		        String subject = "Study Summary Updated Successfully";
		        
		        sendVerSuccNotification(requestMap,piscemail,template,ctx,subject);
			}else{
				logger.info("Notification For Success is Turned OFF");
			}
		}catch(Exception ex){
			logger.error("Error : ",ex);
		}
	}

	public void sendOutboundStudyFailNotification(Map<VelosKeys, Object> requestMap,String piscemail,Map<String, String> irbdataMap){
		logger.info("send Outbound Study Fail Notification");
		try{
			input=this.getClass().getClassLoader().getResourceAsStream("email.properties");
			prop.load(input);
			
			String enableNotification = null;
			
			if (prop.containsKey("Enable_Notification_For_Failure") && !"".equals(prop.getProperty("Enable_Notification_For_Failure").trim())) {
				enableNotification = prop.getProperty("Enable_Notification_For_Failure").trim();
			}
			if ((enableNotification != null) && (!"".equals(enableNotification)) && (enableNotification.equalsIgnoreCase("Y"))){
				String template = "OutboundStudyFailure";

		        // Prepare the evaluation context
		        final Context ctx = new Context();
		        ctx.setVariable("StudyNumber", irbdataMap.get("StudyNumber"));
		        ctx.setVariable("PI",irbdataMap.get("PI"));
		        ctx.setVariable("StudyContact",irbdataMap.get("StudyContact"));
		        ctx.setVariable("Title",irbdataMap.get("Title"));
		        ctx.setVariable("Phase",irbdataMap.get("Phase"));
		        ctx.setVariable("Scope",irbdataMap.get("Scope"));
		        ctx.setVariable("StudyStatus",irbdataMap.get("StudyStatus"));
		        ctx.setVariable("StatusValidFrom",irbdataMap.get("StatusValidFrom"));
		        
		        String subject = "Failed to send study to IRB";
		        
		        sendVerSuccNotification(requestMap,piscemail,template,ctx,subject);
			}else{
				logger.info("Notification For Failure is Turned OFF");
			}
		}catch(Exception ex){
			logger.error("Error : ",ex);
		}
	}

	public void sendOutboundStudyNotification(Map<VelosKeys, Object> requestMap,String piscemail,Map<String, String> irbdataMap){
		logger.info("Send Outbound Study Notification");
		try{
			input=this.getClass().getClassLoader().getResourceAsStream("email.properties");
			prop.load(input);
			
			String enableNotification = null;
			
			if (prop.containsKey("Enable_Notification_For_Success") && !"".equals(prop.getProperty("Enable_Notification_For_Success").trim())) {
				enableNotification = prop.getProperty("Enable_Notification_For_Success").trim();
			}
			if ((enableNotification != null) && (!"".equals(enableNotification)) && (enableNotification.equalsIgnoreCase("Y"))){
				String template = "OutboundStudySuccess";

		        // Prepare the evaluation context
		        final Context ctx = new Context();
		        ctx.setVariable("StudyNumber", irbdataMap.get("StudyNumber"));
		        ctx.setVariable("PI",irbdataMap.get("PI"));
		        ctx.setVariable("StudyContact",irbdataMap.get("StudyContact"));
		        ctx.setVariable("Title",irbdataMap.get("Title"));
		        ctx.setVariable("Phase",irbdataMap.get("Phase"));
		        ctx.setVariable("Scope",irbdataMap.get("Scope"));
		        ctx.setVariable("StudyStatus",irbdataMap.get("StudyStatus"));
		        ctx.setVariable("StatusValidFrom",irbdataMap.get("StatusValidFrom"));
		        
		        String subject = "Study sent to IRB";
		        
		        sendVerSuccNotification(requestMap,piscemail,template,ctx,subject);
			}else{
				logger.info("Notification For Success is Turned OFF");
			}
		}catch(Exception ex){
			logger.error("Error : ",ex);
		}
	}
	
}
