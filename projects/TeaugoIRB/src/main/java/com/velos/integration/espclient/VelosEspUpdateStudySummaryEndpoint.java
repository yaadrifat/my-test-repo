package com.velos.integration.espclient;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import com.velos.services.OperationException_Exception;
import com.velos.services.OperationRolledBackException_Exception;
import com.velos.services.StudyIdentifier;
import com.velos.services.StudySummary;

@WebService(targetNamespace="http://velos.com/services/", name="StudySEI")
public interface VelosEspUpdateStudySummaryEndpoint {
	
	@WebResult(name = "Response", targetNamespace = "")
    @RequestWrapper(localName = "updateStudySummary", targetNamespace = "http://velos.com/services/", className = "com.velos.services.UpdateStudySummary")
    @WebMethod
    @ResponseWrapper(localName = "updateStudySummaryResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.UpdateStudySummaryResponse")
    public com.velos.services.ResponseHolder updateStudySummary(
        @WebParam(name = "StudyIdentifier", targetNamespace = "")
        com.velos.services.StudyIdentifier studyIdentifier,
        @WebParam(name = "StudySummary", targetNamespace = "")
        com.velos.services.StudySummary studySummary,
        @WebParam(name = "createNonSystemUsers", targetNamespace = "")
        boolean createNonSystemUsers
    ) throws OperationException_Exception, OperationRolledBackException_Exception;
	
}
