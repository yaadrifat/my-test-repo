package com.velos.integration.espclient;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.log4j.Logger;

import com.velos.integration.core.messaging.OutboundMessageBean;
import com.velos.integration.mapping.EndpointKeys;
import com.velos.integration.mapping.ProtocolKeys;
import com.velos.integration.mapping.VelosKeys;
import com.velos.integration.util.PropertiesUtil;
import com.velos.services.BudgetDetail;
import com.velos.services.MilestoneVDAPojo;
import com.velos.services.Code;
import com.velos.services.Study;
import com.velos.services.StudyCalendar;
import com.velos.services.StudyVersion;
import com.velos.teaugoservices.StudyStatus;
import com.velos.teaugoservices.StudyUpdates;

public class VelosSendStudyData {

	public static Logger logger = Logger.getLogger(VelosSendStudyData.class);


	private VelosEspClientCamel camelClient;

	private PropertiesUtil propertiesUtil;

	public PropertiesUtil getPropertiesUtil() {
		return propertiesUtil;
	}

	public void setPropertiesUtil(PropertiesUtil propertiesUtil) {
		this.propertiesUtil = propertiesUtil;
	}

	
	public VelosEspClientCamel getCamelClient() {
		return camelClient;
	}

	public void setCamelClient(VelosEspClientCamel camelClient) {
		this.camelClient = camelClient;
	}

	

	

	public void updateStudyStatusineRes(Map<VelosKeys, Object> requestMap,StudyStatus studyStatus) throws Exception {

		Properties prop = propertiesUtil.getAllProperties();
		String organizationName = "";
		if (prop.containsKey("Organization_Name") && !"".equals(prop.getProperty("Organization_Name").trim())) {
			organizationName = prop.getProperty("Organization_Name").trim();
			logger.info("Organization_Name = "+organizationName);
		}
		
		String endpoint = EndpointKeys.CTXpress.toString();
		//Map<VelosKeys, Object> requestMap = new HashMap<VelosKeys, Object>();
		requestMap.put(EndpointKeys.Endpoint, endpoint);
		requestMap.put(ProtocolKeys.IrbNumber, studyStatus.getIrbProtocolNumber());
		requestMap.put(ProtocolKeys.StudyStatus, studyStatus);
		//requestMap.put(ProtocolKeys.BroadCastingSite, broadCastingSite);
		requestMap.put(ProtocolKeys.PropObj, prop);
		requestMap.put(ProtocolKeys.OrganizationName, organizationName);
		
		logger.info("Trying to add status");
		
		
		validateStudyStatusData(requestMap,studyStatus);
		
		searchStudyIRB(requestMap);
		camelClient.createOrgAmendStudyStatus(requestMap);

	}



	//Search Study
	private void searchStudyIRB(Map<VelosKeys, Object> requestMap) throws Exception{
		Map<VelosKeys, Object> searchOrgMap = camelClient.handleRequest(VelosEspMethods.StudySearchOrgStudy, requestMap);
		logger.info("SearchOrgMap = "+searchOrgMap);
		if(searchOrgMap!=null && searchOrgMap.get(ProtocolKeys.StudyAlreadyExists) != null){
			logger.info("Error = "+searchOrgMap.get(ProtocolKeys.StudyAlreadyExists));
			//			logger.info("Email = "+requestMap.get(ProtocolKeys.EmailID));
			String partSiteStudyNumber = (String)searchOrgMap.get(ProtocolKeys.StudyNumber);
			requestMap.put(ProtocolKeys.StudyNumber, partSiteStudyNumber);
			//camelClient.createOrgAmendStudyStatus(requestMap);
			
		}else if(searchOrgMap!=null && searchOrgMap.get(ProtocolKeys.STUDY_NOT_FOUND) != null){
			logger.info("Error = "+searchOrgMap.get(ProtocolKeys.STUDY_NOT_FOUND));
			throw new Exception("Study not found with IRB Protocol Number : "+(String)requestMap.get(ProtocolKeys.IrbNumber));
		}
	}
	
	public void uploadFiles(Map<VelosKeys, Object> requestMap,List<Attachment> attachments,String irbProtocolNumber,String versionNumber,String versionDate,String category,String type,String description,String url,String urlDescription) throws Exception {

		Properties prop = propertiesUtil.getAllProperties();
		String organizationName = "";
		if (prop.containsKey("Organization_Name") && !"".equals(prop.getProperty("Organization_Name").trim())) {
			organizationName = prop.getProperty("Organization_Name").trim();
			logger.info("Organization_Name = "+organizationName);
		}
		
		String endpoint = EndpointKeys.CTXpress.toString();
		//Map<VelosKeys, Object> requestMap = new HashMap<VelosKeys, Object>();
		requestMap.put(EndpointKeys.Endpoint, endpoint);
		requestMap.put(ProtocolKeys.IrbNumber, irbProtocolNumber);
		//requestMap.put(ProtocolKeys.BroadCastingSite, broadCastingSite);
		requestMap.put(ProtocolKeys.PropObj, prop);
		requestMap.put(ProtocolKeys.OrganizationName, organizationName);
		
		logger.info("Trying to upload files");
		
		validateVersionData(requestMap,irbProtocolNumber,versionNumber,versionDate,category);
		
		searchStudyIRB(requestMap);
		
		camelClient.callCreateStudyVersion(requestMap,attachments,irbProtocolNumber,versionNumber,versionDate,category,type,description,url,urlDescription);

	}
	
	private void searchStudyForversion(Map<VelosKeys, Object> requestMap) throws Exception{
		Map<VelosKeys, Object> searchOrgMap = camelClient.handleRequest(VelosEspMethods.StudySearchOrgStudy, requestMap);
		logger.info("SearchOrgMap = "+searchOrgMap);
		if(searchOrgMap!=null && searchOrgMap.get(ProtocolKeys.StudyAlreadyExists) != null){
			logger.info("Error = "+searchOrgMap.get(ProtocolKeys.StudyAlreadyExists));
			//			logger.info("Email = "+requestMap.get(ProtocolKeys.EmailID));
			String partSiteStudyNumber = (String)searchOrgMap.get(ProtocolKeys.StudyNumber);
			requestMap.put(ProtocolKeys.StudyNumber, partSiteStudyNumber);
			//camelClient.createOrgAmendStudyStatus(requestMap);
			
		}else if(searchOrgMap!=null && searchOrgMap.get(ProtocolKeys.STUDY_NOT_FOUND) != null){
			logger.info("Error = "+searchOrgMap.get(ProtocolKeys.STUDY_NOT_FOUND));
			throw new Exception("Study not found with IRB Protocol Number : "+(String)requestMap.get(ProtocolKeys.IrbNumber));
		}
	}
	
	public String getStudyPIContactEmail(Map<VelosKeys, Object> requestMap) throws Exception{
		return camelClient.getStudyPIContactEmail(requestMap);
	}
	
	
	public void updateStudyineRes(Map<VelosKeys, Object> requestMap,StudyUpdates studyUpdates) throws Exception {

		Properties prop = propertiesUtil.getAllProperties();
		String organizationName = "";
		if (prop.containsKey("Organization_Name") && !"".equals(prop.getProperty("Organization_Name").trim())) {
			organizationName = prop.getProperty("Organization_Name").trim();
			logger.info("Organization_Name = "+organizationName);
		}
		
		String endpoint = EndpointKeys.CTXpress.toString();
		//Map<VelosKeys, Object> requestMap = new HashMap<VelosKeys, Object>();
		requestMap.put(EndpointKeys.Endpoint, endpoint);
		requestMap.put(ProtocolKeys.IrbNumber, studyUpdates.getStudyNumber());
		requestMap.put(ProtocolKeys.PropObj, prop);
		requestMap.put(ProtocolKeys.OrganizationName, organizationName);
		requestMap.put(ProtocolKeys.StudyNumber, studyUpdates.getStudyNumber());
		
		validateStudySummaryData(requestMap,studyUpdates);
		
		searchStudy(requestMap);
		
		Map<VelosKeys, Object> studyDataMap = camelClient.handleRequest(VelosEspMethods.StudyGetStudySummary, requestMap);
		if (studyDataMap!=null && studyDataMap.get(ProtocolKeys.FaultString) == null && studyDataMap.get(ProtocolKeys.ErrorList) == null) {

			camelClient.callupdateStudySummary(requestMap,studyUpdates,studyDataMap);
		}

	}

	private void searchStudy(Map<VelosKeys, Object> requestMap) throws Exception{
		//Map<VelosKeys, Object> searchOrgMap = camelClient.handleRequest(VelosEspMethods.StudySearchOrgStudy, requestMap);
		Map<VelosKeys, Object> searchOrgMap = camelClient.callSearchStudy(requestMap);
		logger.info("SearchOrgMap = "+searchOrgMap);
		if(searchOrgMap!=null && searchOrgMap.get(ProtocolKeys.StudyAlreadyExists) != null){
			logger.info("Error = "+searchOrgMap.get(ProtocolKeys.StudyAlreadyExists));
			//			logger.info("Email = "+requestMap.get(ProtocolKeys.EmailID));
			String partSiteStudyNumber = (String)searchOrgMap.get(ProtocolKeys.StudyNumber);
			requestMap.put(ProtocolKeys.StudyNumber, partSiteStudyNumber);
			//camelClient.createOrgAmendStudyStatus(requestMap);
			
		}else if(searchOrgMap!=null && searchOrgMap.get(ProtocolKeys.STUDY_NOT_FOUND) != null){
			logger.info("Error = "+searchOrgMap.get(ProtocolKeys.STUDY_NOT_FOUND));
			throw new Exception("Study not found with Study Number : "+(String)requestMap.get(ProtocolKeys.IrbNumber));
		}
	}

	
	public void sendStudyDataToIRB(String studyNumber, String studyStatus) throws Exception {

		Properties prop = propertiesUtil.getAllProperties();
		String organizationName = "";
		if (prop.containsKey("Organization_Name") && !"".equals(prop.getProperty("Organization_Name").trim())) {
			organizationName = prop.getProperty("Organization_Name").trim();
			logger.info("Organization_Name = "+organizationName);
		}
		
		String endpoint = EndpointKeys.CTXpress.toString();
		Map<VelosKeys, Object> requestMap = new HashMap<VelosKeys, Object>();
		requestMap.put(EndpointKeys.Endpoint, endpoint);
		requestMap.put(ProtocolKeys.IrbNumber,studyNumber);
		requestMap.put(ProtocolKeys.PropObj, prop);
		requestMap.put(ProtocolKeys.OrganizationName, organizationName);
		requestMap.put(ProtocolKeys.StudyNumber,studyNumber);
		
		
		searchStudy(requestMap);
		
		Map<VelosKeys, Object> studyDataMap = camelClient.handleRequest(VelosEspMethods.StudyGetStudySummary, requestMap);
		if (studyDataMap!=null && studyDataMap.get(ProtocolKeys.FaultString) == null && studyDataMap.get(ProtocolKeys.ErrorList) == null) {

			//camelClient.callupdateStudySummary(requestMap,studyUpdates,studyDataMap);
		}
		logger.info("OUTBOUND STUDY");

	}
	
	public void sendStudyDataToIRB(String studyNumber,OutboundMessageBean omb) throws Exception {

		Properties prop = propertiesUtil.getAllProperties();
		String organizationName = "";
		if (prop.containsKey("Organization_Name") && !"".equals(prop.getProperty("Organization_Name").trim())) {
			organizationName = prop.getProperty("Organization_Name").trim();
			logger.info("Organization_Name = "+organizationName);
		}
		
		String endpoint = EndpointKeys.CTXpress.toString();
		Map<VelosKeys, Object> requestMap = new HashMap<VelosKeys, Object>();
		requestMap.put(EndpointKeys.Endpoint, endpoint);
		requestMap.put(ProtocolKeys.PropObj, prop);
		requestMap.put(ProtocolKeys.OrganizationName, organizationName);
		requestMap.put(ProtocolKeys.StudyNumber,studyNumber);
		
		Map<VelosKeys, Object> studyDataMap = camelClient.handleRequest(VelosEspMethods.StudyGetStudy, requestMap);
		if (studyDataMap!=null && studyDataMap.get(ProtocolKeys.FaultString) == null && studyDataMap.get(ProtocolKeys.ErrorList) == null) {

			camelClient.sendStudyToIRB(requestMap,studyDataMap,omb);
		}else{
			throw new Exception("Failed to get the study");
		}

	}
	
	

/*	
	protected void validate(StudyStatus object)
			throws Exception{
				//check for any validation issues on the incoming study object
		getDataValidationIssues(object);
				List<Issue> validationIssues = getDataValidationIssues(object);
				if (validationIssues.size() > 0){
					response.getIssues().addAll(validationIssues);
					if (logger.isDebugEnabled()) logger.debug("Validation Issues found for object " + object.toString());
					throw new ValidationException();
				}
	}
	



protected static void getDataValidationIssues(StudyStatus  study) throws Exception{
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		
		Set<ConstraintViolation<StudyStatus>> constraintViolations = validator.validate(study);
		
		StringBuilder violationMsg = new StringBuilder();
		for (ConstraintViolation<StudyStatus> violation : constraintViolations){
			
			violationMsg.append("Field: " + violation.getPropertyPath().toString());
			violationMsg.append(" issue: " + violation.getMessage());
		}
		
		throw new Exception(violationMsg.toString());
	}*/
	
	private void validateVersionData(Map<VelosKeys, Object> requestMap,String irbProtocolNumber,String versionNumber,String versionDate,String category) throws Exception {
		
		logger.info("Validating Version Data");
		
		if(isEmpty(versionNumber)){
			throw new Exception("versionNumber is required");
		}
		if(isEmpty(irbProtocolNumber)){
			throw new Exception("irbProtocolNumber is required");
		}
		if(isEmpty(category)){
			throw new Exception("category is required");
		}
		if(!isEmpty(versionDate)){
			Properties prop = (Properties)requestMap.get(ProtocolKeys.PropObj);
			String dateFormat = "MM/dd/yyyy";
			if (prop.containsKey("IRB_DateFormat") && !"".equals(prop.getProperty("IRB_DateFormat").trim())) {
				dateFormat = prop.getProperty("IRB_DateFormat").trim();
			}
			if(!isValidDate(versionDate,dateFormat)){
				throw new Exception("Invalid Version date.It should be in '"+dateFormat+"' Format");
			}
		}
	}
	
	private void validateStudyStatusData(Map<VelosKeys, Object> requestMap,StudyStatus studyStatus) throws Exception {
		
		logger.info("Validating Data");
	
		String current = studyStatus.getCurrentStatus();
		String docBy = studyStatus.getDocumentedBy();
		String irbNumber = studyStatus.getIrbProtocolNumber();
		String status = studyStatus.getStatus();
		String statusType = studyStatus.getStatusType();
		String validFrom = studyStatus.getStatusValidFrom();
		String validUntil = studyStatus.getStatusValidUntil();
		if(isEmpty(current)){
			throw new Exception("currentStatus is required");
		}else if(!(current.equals("false") || current.equals("true"))){
			throw new Exception("currentStatus value should be either 'true' or 'false'");
		}
		if(isEmpty(docBy)){
			throw new Exception("documentedBy is required");
		}
		if(isEmpty(irbNumber)){
			throw new Exception("irbProtocolNumber is required");
		}
		if(isEmpty(status)){
			throw new Exception("status is required");
		}
		if(isEmpty(statusType)){
			throw new Exception("statusType is required");
		}
		if(isEmpty(validFrom)){
			throw new Exception("statusValidFrom is required");
		}else{
			Properties prop = (Properties)requestMap.get(ProtocolKeys.PropObj);
			String dateFormat = "MM/dd/yyyy";
			if (prop.containsKey("IRB_DateFormat") && !"".equals(prop.getProperty("IRB_DateFormat").trim())) {
				dateFormat = prop.getProperty("IRB_DateFormat").trim();
			}
			if(!isValidDate(validFrom,dateFormat)){
				throw new Exception("Invalid 'statusValidFrom' date.It should be in '"+dateFormat+"' Format");
			}
		}
		if(!isEmpty(validUntil)){
			Properties prop = (Properties)requestMap.get(ProtocolKeys.PropObj);
			String dateFormat = "MM/dd/yyyy";
			if (prop.containsKey("IRB_DateFormat") && !"".equals(prop.getProperty("IRB_DateFormat").trim())) {
				dateFormat = prop.getProperty("IRB_DateFormat").trim();
			}
			if(!isValidDate(validUntil,dateFormat)){
				throw new Exception("Invalid 'statusValidUntil' date.It should be in '"+dateFormat+"' Format");
			}
		}
	}
	
	
private void validateStudySummaryData(Map<VelosKeys, Object> requestMap,StudyUpdates studyUpdates) throws Exception {
		
		logger.info("Validating Data");
	
		String irbNumber = studyUpdates.getIrbProtocolNumber();
		String studyNumber = studyUpdates.getStudyNumber();
		String title = studyUpdates.getTitle();
		
		if(isEmpty(irbNumber)){
			throw new Exception("irbProtocolNumber is required");
		}
		if(isEmpty(studyNumber)){
			throw new Exception("studyNumber is required");
		}
		if(isEmpty(title)){
			throw new Exception("title is required");
		}
	}


	public static boolean isEmpty(String strParam) {
        if ((!(strParam == null)) && (!(strParam.trim()).equals(""))
                && ((strParam.trim()).length() > 0)) {
            return false;
        } else {
            return true;
        }
    }
	
	public static boolean isValidDate(String validFrom,String dateFormat){
		
		boolean valid = true;
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		sdf.setLenient(false);
		try {
			//if not valid, it will throw ParseException
			Date date = sdf.parse(validFrom);
			logger.info("Valid Date = "+date);
		}catch(Exception e){
			valid = false;
		}
		return valid;
	}
	
}