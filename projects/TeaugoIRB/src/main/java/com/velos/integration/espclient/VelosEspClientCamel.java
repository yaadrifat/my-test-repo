package com.velos.integration.espclient;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;
import java.util.Set;

import javax.activation.DataHandler;
import javax.jws.WebParam;
import javax.ws.rs.core.MultivaluedMap;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.apache.camel.CamelContext;
import org.apache.camel.CamelExecutionException;
import org.apache.camel.ExchangePattern;
import org.apache.camel.ProducerTemplate;
import org.apache.commons.io.IOUtils;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.cxf.message.MessageContentsList;
import org.apache.cxf.ws.security.wss4j.WSS4JOutInterceptor;
import org.apache.log4j.Logger;
import org.apache.ws.security.WSConstants;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.sun.mail.iap.Protocol;
import com.velos.integration.util.StringToXMLGregorianCalendar;
import com.velos.integration.core.messaging.OutboundMessageBean;
import com.velos.integration.dao.MessageDAO;
import com.velos.integration.mapping.EndpointKeys;
import com.velos.integration.mapping.ProtocolKeys;
import com.velos.integration.mapping.VelosKeys;
import com.velos.integration.notifications.EmailNotification;
import com.velos.services.BudgetDetail;
import com.velos.services.CalendarIdentifier;
import com.velos.services.CalendarNameIdentifier;
import com.velos.services.ChangesList;
import com.velos.services.Codes;
import com.velos.services.EventNameIdentfier;
import com.velos.services.Milestone;
import com.velos.services.MilestoneList;
import com.velos.services.MilestoneVDAPojo;
import com.velos.services.Milestonee;
import com.velos.services.CalendarSummary;
import com.velos.services.CalendarVisits;
import com.velos.services.Code;
import com.velos.services.CompletedAction;
import com.velos.services.CrudAction;
import com.velos.services.FormIdentifier;
import com.velos.services.GroupIdentifier;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.Issues;
import com.velos.services.NonSystemUser;
import com.velos.services.NvPair;
import com.velos.services.ObjectInfo;
import com.velos.services.OperationException_Exception;
import com.velos.services.OperationRolledBackException_Exception;
import com.velos.services.OrganizationIdentifier;
import com.velos.services.ParentIdentifier;
import com.velos.services.PatientIdentifier;
import com.velos.services.PatientProtocolIdentifier;
import com.velos.services.ResponseHolder;
import com.velos.services.Results;
import com.velos.services.SearchStudyResponse;
import com.velos.services.SimpleIdentifier;
import com.velos.services.Study;
import com.velos.services.StudyAppendix;
import com.velos.services.StudyCalendar;
import com.velos.services.StudyCalendars;
import com.velos.services.StudyCalendarsList;
import com.velos.services.StudyIdentifier;
import com.velos.services.StudyOrganization;
import com.velos.services.StudyOrganizations;
import com.velos.services.StudyPatientFormResponse;
import com.velos.services.StudyPatientFormResponses;
import com.velos.services.StudyPatientStatuses;
import com.velos.services.StudySearch;
import com.velos.services.StudySearchResults;
import com.velos.services.StudyStatus;
import com.velos.services.StudyStatusHistory;
import com.velos.services.StudyStatuses;
import com.velos.services.StudySummary;
import com.velos.services.StudyTeamIdentifier;
import com.velos.services.StudyTeamMember;
import com.velos.services.StudyTeamMembers;
import com.velos.services.StudyVersion;
import com.velos.services.StudyVersionIdentifier;
import com.velos.services.User;
import com.velos.services.UserIdentifier;
import com.velos.services.UserSearch;
import com.velos.services.UserSearchResults;
import com.velos.services.UserStatus;
import com.velos.services.UserType;
import com.velos.services.VisitNameIdentifier;
import com.velos.services.Versions;
import com.velos.teaugoservices.StudyUpdates;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
//@Component
public class VelosEspClientCamel{

	private static Logger logger = Logger.getLogger(VelosEspClientCamel.class);


	private EmailNotification emailNotification;

	private MessageDAO messageDao;

	public EmailNotification getEmailNotification() {
		return emailNotification;
	}

	public void setEmailNotification(EmailNotification emailNotification) {
		this.emailNotification = emailNotification;
	}

	public MessageDAO getMessageDao() {
		return messageDao;
	}

	public void setMessageDao(MessageDAO messageDao) {
		this.messageDao = messageDao;
	}

	public void configureCamel() {
		if (this.context != null) { return; }
		logger.info("****Getting camel context and creating Beans");
		@SuppressWarnings("resource")
		ClassPathXmlApplicationContext springContext =
		new ClassPathXmlApplicationContext("camel-config.xml");
		this.setContext((CamelContext)springContext.getBean("camel"));		
	}

	protected CamelContext context;

	public CamelContext getContext() {
		return context;
	}

	public void setContext(CamelContext context) {
		this.context = context;
	}
	
	private List codeTypeList;

	public List getCodeTypeList() {
		return codeTypeList;
	}

	public void setCodeTypeList(List codeTypeList) {
		this.codeTypeList = codeTypeList;
	}

	public Map<VelosKeys, Object> handleRequest(VelosEspMethods method, Map<VelosKeys, Object> requestMap) throws Exception  {
		if (requestMap == null) { return null; }
		//this.configureCamel();

		// Call Velos WS
		switch(method) {
		case StudyGetStudySummary:
			return callGetStudySummary(requestMap);
		case SysAdminGetObjectInfoFromOID:
			//return callGetObjectInfoFromOID(requestMap);
		case StudyGetStudy:
			return callGetStudy(requestMap);
		case StudySearchOrgStudy:
			return callSearchOrgStudy(requestMap);
		case StudyCreateOrgStudy:
			//return callCreateOrgStudy(requestMap);
		case StudyCalGetStudyCalendarList:
			//return callGetStudyCalendarList(requestMap);
		case StudyCalGetStudyCalendar:
			//return callGetStudyCalendar(requestMap);
		case StudyCalCreateStudyCalendar:
			//return callCreateOrgStudyCalendar(requestMap);
		case StudyGetDocumentedBy:
			//return callGetDocumentedBy(requestMap);
		case StudyCalGetMilestones:
			//return callGetMilestones(requestMap);
		case StudyCalCreateMilestones:
			//return callCreateMilestones(requestMap);
		case StudyVersGetStudyVersList :
			//return callGetStudyVersList(requestMap);
		case StudyVersCreateStudyVersions:
			//return callCreateOrgStudyVersions(requestMap);
		case StudyGetStudyCalBudgets:
			//return callGetStudyCalBudgets(requestMap);
		case StudyCreateStudyCalBudgets:
			//return callCreateOrgStudyCalBudgets(requestMap);
		case StudyVersGetStudyVersAmndList:
			return callGetStudyVersAmndList(requestMap);
		case StudyVersCreateStudyVersAmendment:
			//return callCreateStudyVersAmendment(requestMap);

		default:
			break;
		}
		return null;
	}




	//Method for searching a study in participating Organizations
	public Map<VelosKeys, Object> callSearchOrgStudy(Map<VelosKeys, Object> requestMap) throws Exception {

		StudyIdentifier studyIdentifier = new StudyIdentifier();
		studyIdentifier.setStudyNumber((String)requestMap.get(ProtocolKeys.StudyNumber));

		String orgName = ((String)requestMap.get(ProtocolKeys.OrganizationName)).trim();
		logger.info("OrganizationName = "+orgName);

		String irbNumber = (String)requestMap.get(ProtocolKeys.IrbNumber);
		logger.info("IRB Number = "+irbNumber);
		Properties prop = (Properties)requestMap.get(ProtocolKeys.PropObj);
		
		String ctxStudySubType = null;		
		Map<VelosKeys, Object> dataMap = null;
		Study study = null;
		StudySearchResults studySearchResults = null;
		String studyNumberInSummary = null;
		try {
			ctxStudySubType = prop.getProperty("IRBNumber_subtype").trim();
			logger.info("IRB SubType = "+ctxStudySubType);
			
			if(ctxStudySubType!=null && !"".equals(ctxStudySubType)){
				StudySearch studySearch = new StudySearch();
				studySearch.setMoreStudyDetails(irbNumber);
				studySearch.setMsdCodeSubType(ctxStudySubType);
			
				//study = getOrgStudy(studyIdentifier,orgName);
				studySearchResults = searchOrgStudy(studySearch,orgName);
				logger.info("StudySearchResults = "+studySearchResults);
				List<StudySearch> studySearchList = studySearchResults.getStudySearch();
				for(StudySearch studyresult : studySearchList){
					studyNumberInSummary = studyresult.getStudyNumber();
					logger.info("Studies found in site "+orgName);
					logger.info("Study Number = "+studyNumberInSummary);
				}
				logger.info("List = "+studySearchResults.getStudySearch());
			}else{
				throw new Exception();
			}
		} catch (OperationException_Exception e) {
			//e.printStackTrace();
			logger.error("Error occured Searching study in Participating site");
			dataMap = new HashMap<VelosKeys, Object>();
			List<Issue> issueList = e.getFaultInfo().getIssues().getIssue();
			List<String> errorList = new ArrayList<String>();
			for(Issue issue : issueList){
				IssueTypes issueTypes = issue.getType();
				String value = issueTypes.value();
				logger.info("Issue value = "+value);
				logger.info("Issue Type = "+issue.getType());
				logger.info("Message = "+issue.getMessage());
				dataMap.put(ProtocolKeys.FaultString, issue.getMessage());
				if(issue.getType().value().equals("STUDY_NOT_FOUND")){
					logger.info("Adding STUDY_NOT_FOUND to Map");
					dataMap.put(ProtocolKeys.STUDY_NOT_FOUND, "STUDY_NOT_FOUND in "+orgName);
					logger.info("Study Not Found in "+orgName);
				}
				errorList.add(issue.getMessage());
				//logger.info("Got operaton exception with issue: "+issue.getMessage());
			}
			dataMap.put(ProtocolKeys.ErrorList, errorList);
			if (issueList == null) { return null; }
			return dataMap;
		} catch (Exception e) {
			//e.printStackTrace();
			logger.error("Error :",e);
			if(ctxStudySubType== null || "".equals(ctxStudySubType)){
				String ctxError = "Study Number mapping is not Configured "+orgName;
				logger.fatal(ctxError);
				//messageDao.saveMessageDetails(studyNumber,(String)requestMap.get(ProtocolKeys.BroadCastingSite),orgName, "Study",studyNumber,"Failed",ctxError);
			}
			throw e;
			//return dataMap;
		}

		logger.info(dataMap);
		logger.info(study);

		//if (dataMap == null && study != null ){
		if (dataMap == null && studySearchResults != null ){
			logger.info("Null map = "+dataMap);
			dataMap = new HashMap<VelosKeys, Object>();
			logger.error("Study Already exists in "+orgName);
			//Check for study number in MSD then send StudyAlreadyExists
			dataMap.put(ProtocolKeys.StudyAlreadyExists,"StudyAlreadyExists in "+orgName);
			
			List<String> errorList = new ArrayList<String>();
			//logger.error("Study Already exists-sent as package");
			errorList.add("Study Already exists with studyNumber = "+studyNumberInSummary);
			
			dataMap.put(ProtocolKeys.StudyNumber,studyNumberInSummary);
			logger.info(dataMap);
			return dataMap;
		}
		return dataMap;
	}



	/*// Get Versions List
		public Map<VelosKeys, Object> callGetStudyVersList(Map<VelosKeys, Object> requestMap){

			StudyIdentifier studyIdentifier = new StudyIdentifier();
	    	studyIdentifier.setStudyNumber((String)requestMap.get(ProtocolKeys.StudyNumber));
	    	VersionsImpl versionsImpl = new VersionsImpl();
	    	Versions versions = new Versions();
	    	Map<VelosKeys, Object> dataMap = null;

	    	try {
				versions =versionsImpl.getStudyVersions(studyIdentifier);

			} catch (OperationException_Exception e) {
				//e.printStackTrace();
	    		logger.error("ERROR OCCURED GETTING Versions");
	    		dataMap = new HashMap<VelosKeys, Object>();
				List<Issue> issueList = e.getFaultInfo().getIssues().getIssue();
				List<String> errorList = new ArrayList<String>();
				for(Issue issue : issueList){
					logger.error("Issue Type = "+issue.getType());
					logger.error("Message = "+issue.getMessage());
					dataMap.put(ProtocolKeys.FaultString, issue.getMessage());
					errorList.add("Issue Type = "+issue.getType()+" Message = "+issue.getMessage());
					//logger.info("Got operaton exception with issue: "+issue.getMessage());
				}
				dataMap.put(ProtocolKeys.ErrorList, errorList);
				if (issueList == null) { return null; }
				return dataMap;
			} catch (Exception e) {
				//e.printStackTrace();
				logger.error("Error :",e);
				return dataMap;
			}

	    	if (dataMap == null && versions != null){
	    		logger.info("****Executing StudyVersion mapper****");
	    	VelosStudyVersionMapper mapper = new VelosStudyVersionMapper((String)requestMap.get(EndpointKeys.Endpoint));
	    	return mapper.mapGetStudyVersOrgList(versions,requestMap);
	    	}

			return dataMap;

		}*/




	/* @Bean
	public WSS4JOutInterceptor wssInterceptor() {
		Properties prop = new Properties();
		try {
			prop.load(this.getClass().getClassLoader()
					.getResourceAsStream("config.properties"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Map<String, Object> props = new HashMap<String, Object>();
		props.put(WSHandlerConstants.ACTION, WSHandlerConstants.USERNAME_TOKEN);
		props.put(WSHandlerConstants.PASSWORD_TYPE, WSConstants.PW_TEXT);
		props.put(WSHandlerConstants.USER, prop.getProperty("velos.userID"));
		props.put(WSHandlerConstants.PW_CALLBACK_CLASS,
				SimpleAuthCallbackHandler.class.getName());
		WSS4JOutInterceptor wssBean = new WSS4JOutInterceptor(props);
		return wssBean;
	}
	 */

	public void saveDB(Map<VelosKeys, Object> requestMap,String participatingSite,String componentType,String componentName,String status,List<String> errorList){

		logger.info("saveDB()");
		String bSite = (String)requestMap.get(ProtocolKeys.BroadCastingSite);
		String studyNum = (String)requestMap.get(ProtocolKeys.StudyNumber);
		String errorMsg = "";
		StringBuffer sb = null;
		if(errorList!=null && !errorList.isEmpty()){
			sb = new StringBuffer();
			for(String error : errorList){
				sb.append(error);
				sb.append("\n");
			}
		}
		if(sb!=null){
			errorMsg = sb.toString();
		}
		//messageDao.saveMessageDetails(studyNum,bSite,participatingSite, componentType,componentName,status,errorMsg);
		//messageDao.saveMessageDetails(studyNum,bSite,orgName, "Calendar",calCreating,"Failed", errorList.toString(), "Test User");
	}



	//Method for searching study
	public StudySearchResults searchStudy(StudySearch studySearch)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList list = null;
		try{
			list = (MessageContentsList) producer.sendBody("espSearchStudyEndpoint", 
					ExchangePattern.InOut, studySearch);
		}catch(CamelExecutionException e) {
			logger.info(e.getCause());
			logger.info(e.getMessage());
			if (e.getCause() instanceof OperationException_Exception) {
				logger.info("throwing Exception");
				throw(OperationException_Exception)e.getCause();
			}
			e.printStackTrace();
			return null;
		}catch (Exception e) {
			//e.printStackTrace();
			logger.error("Error :",e);
			return null;
		}
		logger.info("Search Study");
		return (StudySearchResults) list.get(0);
	}

	//Method for searching study in participating sites
	public StudySearchResults searchOrgStudy(StudySearch studySearch,String orgName)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList list = null;
		try{
			list = (MessageContentsList) producer.sendBody("espSearchStudyEndpoint", 
					ExchangePattern.InOut, studySearch);
		}catch(CamelExecutionException e) {
			logger.error(e.getCause());
			logger.error(e.getMessage());
			if (e.getCause() instanceof OperationException_Exception) {
				logger.info("throwing Exception");
				throw(OperationException_Exception)e.getCause();
			}else{
				throw e;
			}
		}
		logger.info("Search Study");
		return (StudySearchResults) list.get(0);
	}
	

	public Map<VelosKeys, Object> callGetStudyVersAmndList(Map<VelosKeys, Object> requestMap){

		String studyNumber = (String)requestMap.get(ProtocolKeys.StudyNumber);
		Properties prop = (Properties)requestMap.get(ProtocolKeys.PropObj);
		StudyIdentifier studyIdentifier = new StudyIdentifier();
		studyIdentifier.setStudyNumber(studyNumber);
		
		Versions versions = null;
		Map<VelosKeys,Object> dataMap = null;
		List<StudyVersion> createVersAmndList = null;
		String amendStatusSubType = null;
		String amendStatusDesc = null;
		List<String> statusHistoryList = null;
		try {
			amendStatusSubType = prop.getProperty("Amendments.status.subtype").trim();
			amendStatusDesc = prop.getProperty("Amendments.status.desc").trim();
			if("".equals(amendStatusSubType) || "".equals(amendStatusDesc)){
				throw new Exception("Version Amendment Status Mappings Not configured");
			}	
			
			versions = getStudyVersionAmendments(studyIdentifier);
			if(versions!=null){
				createVersAmndList = new ArrayList<StudyVersion>();
				List<StudyVersion> studyVersAmndList = versions.getVersion();
				if(studyVersAmndList != null && !studyVersAmndList.isEmpty()){
					for(StudyVersion sv : studyVersAmndList){
						List<StudyStatusHistory> stdStatusHist = sv.getStudyStatusHistory();
						for(StudyStatusHistory ssh : stdStatusHist){
							String	versStatus = ssh.getPKCODELSTSTAT().getCode();
							XMLGregorianCalendar statusEndDate = ssh.getSTATUSENDDATE();
							if(versStatus.equals(amendStatusSubType)){
								createVersAmndList.add(sv);
							}
						}
					}
				}
			}
		} catch (OperationException_Exception e) {
			//e.printStackTrace();
			logger.error("**ERROR OCCURED GETTING Study VERSIONS Amendments**");
			dataMap = new HashMap<VelosKeys, Object>();
			List<Issue> issueList = e.getFaultInfo().getIssues().getIssue();
			List<String> errorList = new ArrayList<String>();
			for(Issue issue : issueList){
				logger.error("Issue Type = "+issue.getType());
				logger.error("Message = "+issue.getMessage());
				dataMap.put(ProtocolKeys.FaultString, issue.getMessage());
				errorList.add(issue.getMessage());
			}
			dataMap.put(ProtocolKeys.ErrorList, errorList);
			if (issueList == null) { return null; }
			return dataMap;
		} catch (Exception e) {
			//e.printStackTrace();
			logger.error("Getting Amendments  Failed :",e);
			if(amendStatusSubType==null || amendStatusDesc==null || e.getMessage().equals("Version Amendment Status Mappings Not configured")){
				String errorMsg = "Configure Version Amendment Status Mappings in config.properties file ";
				//messageDao.saveMessageDetails(studyNumber,(String)requestMap.get(ProtocolKeys.BroadCastingSite)," ", "Amendment Versions","Getting Amendments Failed","Failed", errorMsg);
			}
			return dataMap;
		}
		if(createVersAmndList!=null && !createVersAmndList.isEmpty()){
			dataMap = new HashMap<VelosKeys, Object>();
			dataMap.put(ProtocolKeys.CreateStudyVersAmendmentList,createVersAmndList);
		}else{
			dataMap = new HashMap<VelosKeys, Object>();
		}
		return dataMap;
	}
	
	
	public Versions getStudyVersionAmendments(StudyIdentifier studyIdentifier)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList list = null;
		MessageContentsList inpList = new MessageContentsList();
		inpList.add(studyIdentifier);
		inpList.add(true);
		try{
			list = (MessageContentsList) producer.sendBody("cxf:bean:espGetStudyVersionEndpoint", 
					ExchangePattern.InOut,inpList);
		}catch(CamelExecutionException e) {
			logger.error(e.getCause());
			logger.error(e.getMessage());
			if (e.getCause() instanceof OperationException_Exception) {
				logger.info("throwing Exception");
				throw(OperationException_Exception)e.getCause();
			}else{
				throw e;
			}
		}
		return (Versions) list.get(0);
	}
	

	
	public void createOrgAmendStudyStatus(Map<VelosKeys, Object> requestMap) throws Exception {

		Study study = (Study)requestMap.get(ProtocolKeys.StudyObj);
		String bSite = (String)requestMap.get(ProtocolKeys.BroadCastingSite);
		String studyNumber = (String)requestMap.get(ProtocolKeys.StudyNumber);
		String orgName = ((String)requestMap.get(ProtocolKeys.OrganizationName)).trim();
		StudyIdentifier studyIdentifier = new StudyIdentifier();
		String partSiteStudyNumber = (String)requestMap.get(ProtocolKeys.ParticipatingSite_StudyNumber);
		//studyIdentifier.setStudyNumber(partSiteStudyNumber);
		studyIdentifier.setStudyNumber(studyNumber);
		
		com.velos.teaugoservices.StudyStatus stdStatus = (com.velos.teaugoservices.StudyStatus)requestMap.get(ProtocolKeys.StudyStatus);

		Map<VelosKeys, Object> dataMap = null;
		try {
			StudyStatus studyStatus = getMappedStudyStatus(requestMap);
			
			ResponseHolder responseHolder = createAmendStudyStatus(studyIdentifier,studyStatus,orgName);
			Issues issues = responseHolder.getIssues();
			Results results = responseHolder.getResults();
			if(issues!=null && !"".equals(issues)){
				List<Issue> issueList = issues.getIssue();
				if(!issueList.isEmpty()){
					logger.fatal("***** Failed to add Study Status *****");
					logger.info("Issues Found");
					logger.info("IssueList = "+issueList);
					for(Issue issue : issueList){
						logger.info("Issue = "+issue);
						logger.info("Issue Type = "+issue.getType());
						logger.info("Issue Message = "+issue.getMessage());
					}
				}
			}
			if(results!=null){
				List<CompletedAction> actionList = results.getResult();
				if(!actionList.isEmpty()){
				for(CompletedAction action : actionList){
					CrudAction crudAction = action.getAction();
					logger.info("Action = "+crudAction.value());
					SimpleIdentifier si = action.getObjectId();
					logger.info("OID = "+si.getOID());
					logger.info("Pk = "+si.getPK());
					logger.info("Object name = "+action.getObjectName());
				}
				
				String irbNumber = stdStatus.getIrbProtocolNumber();
				String stStatus = stdStatus.getStatus();
				messageDao.saveStatusDetails(irbNumber,studyNumber,stStatus,"Success","Study status updated");
				
				try{
					InputStream	input=this.getClass().getClassLoader().getResourceAsStream("email.properties");
					Properties prop = new Properties();
					prop.load(input);
					
					String enableNotification = null;
					
					if (prop.containsKey("Enable_Notification_For_Success") && !"".equals(prop.getProperty("Enable_Notification_For_Success").trim())) {
						enableNotification = prop.getProperty("Enable_Notification_For_Success").trim();
					}
					if ((enableNotification != null) && (!"".equals(enableNotification)) && (enableNotification.equalsIgnoreCase("Y"))){
						String piscemail="";
						//piscemail = getStudyPIContactEmail(requestMap);
						emailNotification.sendStatusSuccNotification(requestMap,piscemail,stdStatus);
					}else{
						logger.info("Notification For Success is Turned OFF");
					}
					}catch(Exception ex){
						logger.error("Error while sending notification",ex);
					}
				
				}
			}
			//messageDao.saveMessageDetails(studyNumber,bSite,orgName,"Amendments Study Status",studyNumber,"Created", "Study Status Created for Study = "+partSiteStudyNumber);
		} catch (OperationException_Exception e) {
			//e.printStackTrace();
			logger.error("ERROR OCCURED Creating Study Status in Participating org = "+orgName);
			List<Issue> issueList = e.getFaultInfo().getIssues().getIssue();
			List<String> errorList = new ArrayList<String>();
			for(Issue issue : issueList){
				logger.info("Issue Type = "+issue.getType());
				logger.info("Issue Message = "+issue.getMessage());
				errorList.add(issue.getMessage());
				logger.error("Got operaton exception with issue: "+issue.getMessage());
				if(issue.getType().value().equals("GET_DOCUMENTED_BY_USERID_NOT_FOUND")){
					String err = "DocumentedBy user id not found";
					logger.error(err);
					throw new Exception(err);
				}
			}
			throw new Exception(getErrorList(errorList));
			//saveDB(requestMap,orgName,"Amendments Study Status",studyNumber,"Failed",errorList);
		} catch (Exception e) {
			logger.error("Error :",e);
			throw e;
		}
	}
	
		
	public ResponseHolder createAmendStudyStatus(StudyIdentifier studyIdentifier,StudyStatus studyStatus,String orgName)
			throws OperationException_Exception,OperationRolledBackException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList inpList = new MessageContentsList();
		inpList.add(studyIdentifier);
		inpList.add(studyStatus);
		MessageContentsList list = null;
		try{
			list = (MessageContentsList) producer.sendBody("espAddStudyStatus", 
					ExchangePattern.InOut,inpList);
		}catch(CamelExecutionException e) {
			logger.error(e.getCause());
			logger.error(e.getMessage());
			if (e.getCause() instanceof OperationException_Exception) {
				logger.info("throwing Exception");
				throw(OperationException_Exception)e.getCause();
			}else{
				throw e;
			}
		}
		logger.info("Created Study Status in Org = "+orgName);
		return (ResponseHolder) list.get(0);
	}
	
	public StudyStatus getMappedStudyStatus(Map<VelosKeys, Object> requestMap) throws Exception{
		StudyStatus mappedStudyStatus = new StudyStatus();
		UserIdentifier documentedBy = null;
		Properties prop = (Properties)requestMap.get(ProtocolKeys.PropObj);
		String orgName = ((String)requestMap.get(ProtocolKeys.OrganizationName)).trim();
    	logger.info("OrganizationName = "+orgName);
    	com.velos.teaugoservices.StudyStatus stdStatus = (com.velos.teaugoservices.StudyStatus)requestMap.get(ProtocolKeys.StudyStatus);
    	logger.info("Study Status Changed = "+stdStatus);
    	
    	OrganizationIdentifier mappedOrgIdentifier = new OrganizationIdentifier();
		mappedOrgIdentifier.setSiteName(orgName);
		
		mappedStudyStatus.setOrganizationId(mappedOrgIdentifier);
		
		/*if (prop.containsKey(orgName + ".userID") && !"".equals(prop.getProperty(orgName + ".userID").trim())) {
			documentedBy = new UserIdentifier();
			String userName = prop.getProperty(orgName + ".userID").trim();
			documentedBy.setUserLoginName(userName);
		}*/
		documentedBy = new UserIdentifier();
		documentedBy.setUserLoginName(stdStatus.getDocumentedBy());
		
		mappedStudyStatus.setDocumentedBy(documentedBy);

		String studyStatus = stdStatus.getStatus();
		Code mappedStatus = getMappedCode(requestMap,"StudyStatus",studyStatus);
		mappedStudyStatus.setStatus(mappedStatus);

		String statusType = stdStatus.getStatusType();
		Code mappedStatusType = getMappedCode(requestMap,"StatusType",statusType);
		mappedStudyStatus.setStatusType(mappedStatusType);
				
		String DATE_FORMAT_NOW = "yyyy-MM-dd";
		//Date date = new Date();
		
		String dateFormat = "MM/dd/yyyy";
		if (prop.containsKey("IRB_DateFormat") && !"".equals(prop.getProperty("IRB_DateFormat").trim())) {
			dateFormat = prop.getProperty("IRB_DateFormat").trim();
		}
		
		SimpleDateFormat sdfT = new SimpleDateFormat(dateFormat);
		sdfT.setLenient(false);
		Date date = sdfT.parse(stdStatus.getStatusValidFrom());
		logger.info("Valid From Date = "+date);
		
		
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
		String stringDate = sdf.format(date);
		logger.info("Formatted From Date = "+stringDate);

		XMLGregorianCalendar validFromDate = StringToXMLGregorianCalendar
				.convertStringDateToXmlGregorianCalendar(stringDate,
						"yyyy-MM-dd", true);
		
		mappedStudyStatus.setValidFromDate(validFromDate);
		
		String toDate = stdStatus.getStatusValidUntil();
		if(!isEmpty(toDate)){
			Date udate = sdfT.parse(toDate);
			logger.info("Valid Until Date = "+udate);
			
			String ustringDate = sdf.format(udate);
			logger.info("Formatted Until Date = "+ustringDate);

			XMLGregorianCalendar validUntilDate = StringToXMLGregorianCalendar
					.convertStringDateToXmlGregorianCalendar(ustringDate,
							"yyyy-MM-dd", true);
			mappedStudyStatus.setValidUntilDate(validUntilDate);
		}
		
		//mappedStudyStatus.setCurrentForStudy(false);
		boolean defStatus = false;
		String statusC = stdStatus.getCurrentStatus();
		
		if(statusC.equals("false") || statusC.equals("true")){
			defStatus=Boolean.parseBoolean(statusC);
		}
		
		mappedStudyStatus.setCurrentForStudy(defStatus);
		return mappedStudyStatus;
		
	}


	/*
	 public Map<String, Object> callGetCodelsts(String orgName) {
		
		String codeType=null;
		Map<VelosKeys, Object> dataMap = null;
		Map<String,Object> codeMap = null;
		try{
						
			List<String> typeList = getCodeTypeList();
			logger.info("List = "+typeList);
			
			codeMap = new HashMap<String,Object>();
			logger.info("Getting codelsts for org = "+orgName);
			for(String type : typeList){
				try{
					codeType=type;
					logger.info("***************Type = "+type+"*****************");
					Codes codes = getCodelsts(orgName,type);
					if(codes!=null){
						List<Code> list = codes.getCodes();
						for(Code code : list){
							logger.info("Code SubType="+code.getCode()+" Code Desc="+code.getDescription());
							logger.info("Code Type = "+code.getType());
							logger.info("Code SubType = "+code.getCode());
							logger.info("Code Desc = "+code.getDescription());
						}
						codeMap.put(type, codes);
					}
				}catch (OperationException_Exception e) {
					logger.error("ERROR OCCURED getting Codelsts for "+orgName +"of Type = "+codeType);
					dataMap = new HashMap<VelosKeys, Object>();
					List<Issue> issueList = e.getFaultInfo().getIssues().getIssue();
					List<String> errorList = new ArrayList<String>();
					for(Issue issue : issueList){
						logger.info(issue.getType());
						logger.info(issue.getMessage());
						dataMap.put(ProtocolKeys.FaultString, issue.getMessage());
						logger.error("Issue Type = "+issue.getType()+". Message = "+issue.getMessage());
						errorList.add(issue.getMessage());
						logger.error("Got operaton exception with issue: "+issue.getMessage());
					}
					dataMap.put(ProtocolKeys.ErrorList, errorList);
					if (issueList == null) { return null; }
					return null;
				}
			}
		}catch (Exception e) {
			logger.error("Error :",e);
			return null;
		}
		logger.info("CodeMap = "+codeMap);
		return codeMap;
	}
	
	public Codes getCodelsts(String orgName,String codeType)
			throws OperationException_Exception {
	
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList list = null;
		try{
			list = (MessageContentsList) producer.sendBody(orgName+"_getCodelsts", 
					ExchangePattern.InOut, codeType);        
		}catch(CamelExecutionException e) {
				logger.error(e.getCause());
				logger.error(e.getMessage());
			if (e.getCause() instanceof OperationException_Exception) {
				throw(OperationException_Exception)e.getCause();
			}else{
				throw e;
			}
		}
		return (Codes) list.get(0);
	}getStudy
*/
	
	public Map<VelosKeys, Object> callGetStudy(Map<VelosKeys, Object> requestMap) {

		StudyIdentifier studyIdentifier = new StudyIdentifier();
		studyIdentifier.setStudyNumber((String)requestMap.get(ProtocolKeys.StudyNumber));

		Map<VelosKeys, Object> dataMap = null;
		Study study = null;
		try {
			study = getStudy(studyIdentifier);
			dataMap = new HashMap<VelosKeys, Object>();
			dataMap.put(ProtocolKeys.StudyObj, study);
			logger.info("Study = "+study);
		} catch (OperationException_Exception e) {
			//e.printStackTrace();
			logger.error("ERROR OCCURED GETTING STUDY");
			dataMap = new HashMap<VelosKeys, Object>();
			List<Issue> issueList = e.getFaultInfo().getIssues().getIssue();
			List<String> errorList = new ArrayList<String>();
			for(Issue issue : issueList){
				logger.error("Issue Type = "+issue.getType());
				logger.error("Message = "+issue.getMessage());
				dataMap.put(ProtocolKeys.FaultString, issue.getMessage());
				errorList.add("Issue Type = "+issue.getType()+" Message = "+issue.getMessage());
				//logger.info("Got operaton exception with issue: "+issue.getMessage());
			}
			dataMap.put(ProtocolKeys.ErrorList, errorList);
			if (issueList == null) { return null; }
			return dataMap;
		} catch (Exception e) {
			//e.printStackTrace();
			logger.error("Error :",e);
			return dataMap;
		}

		/*if (dataMap == null && study != null){
			logger.info("Executing mapper");
			VelosStudyMapper mapper = new VelosStudyMapper((String)requestMap.get(EndpointKeys.Endpoint));
			return mapper.mapStudyOrgList(study,requestMap);
		}*/
		return dataMap;
	}

	public Study getStudy(StudyIdentifier request)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		logger.info("Get study from ");
		MessageContentsList list = null;
		try{
			list = (MessageContentsList) producer.sendBody("cxf:bean:espGetStudyEndpoint", 
					ExchangePattern.InOut, request);
		}catch(CamelExecutionException e) {
			logger.info(e.getCause());
			logger.info(e.getMessage());
			if (e.getCause() instanceof OperationException_Exception) {
				logger.info("throwing Exception");
				throw(OperationException_Exception)e.getCause();
			}else{
				throw e;
			}
		}
			/*e.printStackTrace();
			return null;
		}catch (Exception e) {
			//e.printStackTrace();
			logger.error("Error :",e);
			return null;
		}*/
		logger.info("Returned Organization Study");
		return (Study) list.get(0);
	}
	
	public String getStudyPIContactEmail(Map<VelosKeys, Object> requestMap) throws Exception {
		// TODO Auto-generated method stub
		
		//Map<VelosKeys, Object> studyDataMap = handleRequest(VelosEspMethods.StudyGetStudy, requestMap);
		
		Map<VelosKeys, Object> studyDataMap = handleRequest(VelosEspMethods.StudyGetStudySummary, requestMap);
		
		String piscemail="";
		String piEmail="";
		String scEmail="";

		if (studyDataMap!=null && studyDataMap.get(ProtocolKeys.FaultString) == null && studyDataMap.get(ProtocolKeys.ErrorList) == null) {
			//Study study = (Study)studyDataMap.get(ProtocolKeys.StudyObj);
			//StudySummary summary = study.getStudySummary();
			
			StudySummary summary = (StudySummary)studyDataMap.get(ProtocolKeys.StudyObj);
			
			
			UserIdentifier pi = summary.getPrincipalInvestigator();
			if(pi!=null){
				logger.info("PI Pk = "+pi.getPK());
				logger.info("PI FirstName = "+pi.getFirstName());
				logger.info("PI LastName = "+pi.getLastName());
				logger.info("PI Login = "+pi.getUserLoginName());
				try {
					piEmail = getUserEmail(requestMap,pi.getPK());
				} catch (Exception e) {
					logger.error("Failed to get PI Email ID");
				}
			}
			
			UserIdentifier studyContact = summary.getStudyContact();
			if(studyContact!=null){
				logger.info("StudyContact Pk = "+studyContact.getPK());
				logger.info("StudyContact FirstName = "+studyContact.getFirstName());
				logger.info("StudyContact LastName = "+studyContact.getLastName());
				logger.info("StudyContact Login = "+studyContact.getUserLoginName());
				
				try {
					scEmail = getUserEmail(requestMap,studyContact.getPK());
				} catch (Exception e) {
					logger.error("Failed to get StudyContact Email ID");
				}
			}
		}
		
		if(piEmail != null || !"".equals(piEmail)){
			if(piEmail.contains("@")){ 
				piscemail=piEmail;
			}
		}
		if(scEmail != null || !"".equals(scEmail)){
			if(scEmail.contains("@")){ 
				piscemail=piscemail+","+scEmail;
			}
		}
		
		logger.info("PI SC Email = "+piscemail);

		return piscemail;	
		
	}
	
	public String getUserEmail(Map<VelosKeys, Object> requestMap,int pk) throws Exception {

		String studyNum = (String)requestMap.get(ProtocolKeys.StudyNumber);
		
		String userMail="";
				
		/*String firstName = ((String)requestMap.get(ProtocolKeys.DocumentedByFirstName)).trim();
		logger.info("firstName = "+firstName);
		String lastName = ((String)requestMap.get(ProtocolKeys.DocumentedByLastName)).trim();
		logger.info("lastName = "+lastName);
		String loginName = ((String)requestMap.get(ProtocolKeys.DocumentedByLoginName)).trim();
		logger.info("DocumnetedBy LoginName = "+loginName);*/

		GroupIdentifier groupIdentifier = new GroupIdentifier();
		groupIdentifier.setGroupName("");

		UserSearch userSearch = new UserSearch();
		//userSearch.setFirstName(firstName);
		//userSearch.setLastName(lastName);
		//userSearch.setLoginName(loginName);
		userSearch.setPageNumber(1);
		userSearch.setPageSize(1000);
		userSearch.setGroup(groupIdentifier);
		userSearch.setUserPK(pk);

		Map<VelosKeys, Object> dataMap = null;

		UserSearchResults userSearchResults = null;
		try {

			userSearchResults = searchUser(userSearch);
			logger.info("UserSearch Results = "+userSearchResults);

			Long totalCount = userSearchResults.getTotalCount();
			logger.info("Total Count = "+totalCount);
			logger.info("Page Size = "+userSearchResults.getPageSize());

			String emailErrorMsg = null;
			if(totalCount == 0){
				emailErrorMsg = "No user found for the search criteria to get Email ID";
				logger.error(emailErrorMsg);
				throw new Exception(emailErrorMsg);
				//messageDao.saveMessageDetails(studyNum,"null","null", "DocumentedByEmailID","null","Getting Email Failed", emailErrorMsg);
			}
			if(totalCount == 1){
				dataMap = new HashMap<VelosKeys, Object>();
				List<User> users = userSearchResults.getUser();
				for(User user:users){
					logger.info("*****Email Users*****");
					logger.info("user = "+user);
					logger.info("User Pk = "+user.getPK());
					logger.info("User FirstName = "+user.getFirstName());
					logger.info("User Last name = "+user.getLastName());
					logger.info("User Login = "+user.getUserLoginName());
					logger.info("User Org = "+user.getOrganization().getSiteName());
					userMail = user.getEmail();
					logger.info("User Email = "+userMail);
					logger.info("***************************");
					if(!"".equals(userMail) && !"null".equals(userMail) && userMail!=null){
						return userMail;
					}
					else{
						emailErrorMsg = "There is no email id for the user";
						logger.fatal(emailErrorMsg);
						throw new Exception(emailErrorMsg);
						//messageDao.saveMessageDetails(studyNum,"null","null", "DocumentedByEmailID","null","Getting Email Failed", emailErrorMsg);
					}
				}
			}
			if(totalCount >= 2){
				emailErrorMsg = "Multiple users found for the search criteria for DocumentedBy Users";
				logger.fatal(emailErrorMsg);
				throw new Exception(emailErrorMsg);
				//messageDao.saveMessageDetails(studyNum,"null","null", "DocumentedByEmailID","null","Getting Email Failed", emailErrorMsg);
			}
			

		} catch (OperationException_Exception e) {
			//e.printStackTrace();
			logger.info("ERROR OCCURED Getting Email ID");
			dataMap = new HashMap<VelosKeys, Object>();
			List<Issue> issueList = e.getFaultInfo().getIssues().getIssue();
			List<String> errorList = new ArrayList<String>();
			for(Issue issue : issueList){
				IssueTypes issueTypes = issue.getType();
				String value = issueTypes.value();
				logger.info("Issue value = "+value);
				logger.info("Issue Type = "+issue.getType());
				logger.info("Message = "+issue.getMessage());
				dataMap.put(ProtocolKeys.FaultString, issue.getMessage());

				errorList.add(issue.getMessage());
				logger.info("Got operaton exception with issue: "+issue.getMessage());
			}
			dataMap.put(ProtocolKeys.ErrorList, errorList);
			if (issueList == null) { return null; }
			throw e;
			//return dataMap;
		} catch (Exception e) {
			//e.printStackTrace();
			//logger.error("Error :",e);
			throw e;
			//return dataMap;
		}

		return userMail;
	}

	
	public com.velos.services.UserSearchResults searchUser(UserSearch userSearch) throws OperationException_Exception{
		ProducerTemplate producer = context.createProducerTemplate();
		logger.info("Getting User");
		MessageContentsList list = null;
		try{
			list = (MessageContentsList) producer.sendBody("cxf:bean:espSearchUserEndpoint", 
					ExchangePattern.InOut, userSearch);
		}catch(CamelExecutionException e) {
			logger.info(e.getCause());
			logger.info(e.getMessage());
			if (e.getCause() instanceof OperationException_Exception) {
				logger.info("throwing Exception");
				throw(OperationException_Exception)e.getCause();
			}else{
				throw e;
			}
		}
		logger.info("Returned Users based on organization");
		return (UserSearchResults) list.get(0);
	}
	
	public static Code getMappedCode(Map<VelosKeys, Object> requestMap,String constant,String value){
		Properties prop = (Properties)requestMap.get(ProtocolKeys.PropObj);
		logger.info("Orginal Value = "+value);
		Code mappedCode = new Code();
		if(isEmpty(value)){
			mappedCode.setCode(value);
		}else{
			String repValue=value.trim().replace(" ", "_");
			if (prop.containsKey(constant+"_"+repValue) && !"".equals(prop.getProperty(constant+"_"+repValue).trim())) {
				String mappedValue = prop.getProperty(constant+"_"+repValue).trim();
				logger.info("Mapped Value  = "+mappedValue);
				mappedCode.setCode(mappedValue);
			}else{
				mappedCode.setCode(value);
			}
		}
		return mappedCode;
	}
	
	
	
	public static boolean isEmpty(String strParam) {
        if ((!(strParam == null)) && (!(strParam.trim()).equals(""))
                && ((strParam.trim()).length() > 0)) {
            return false;
        } else {
            return true;
        }
    }
	
	
	public String getErrorList(List<String> errorList){

		String errorMsg = "";
		StringBuffer sb = null;
		if(errorList!=null && !errorList.isEmpty()){
			sb = new StringBuffer();
			for(String error : errorList){
				sb.append(error);
				sb.append("\n");
			}
		}
		if(sb!=null){
			errorMsg = sb.toString();
		}
		return errorMsg;
	}
	
	
	 public StudyVersion getMappedVersion(Map<VelosKeys, Object> requestMap,List<Attachment> attachments,String irbProtocolNumber,String versionNumber,String versionDate,String category,String type,String description,String url,String urlDescription) throws Exception {
		
		Properties prop = (Properties)requestMap.get(ProtocolKeys.PropObj);
		String orgName = ((String)requestMap.get(ProtocolKeys.OrganizationName)).trim();
    	
    	logger.info("*******Getting Mapped Amendments Data for org = "+orgName);
    	
 		String partAmendStatus= null;
		String broadAmendStatus = null;
		String versStatus = null;
		String versCatgCode = null;
		String versType = null ;
		Code code = null;
		String brodAmendStatDesc = null;
		String catgcode =null;
		String typeCode = null;
		String amendError = "Amendment Status mapping not found for participating site "+orgName;
		try{
			partAmendStatus =  prop.getProperty("VersionStatus_subtype").trim();
			
		}catch(Exception e){
			logger.fatal(amendError);
			throw new Exception(amendError);
		}
			
     			StudyVersion sa = new StudyVersion();
     			
     			sa.setSTUDYVERSTATUS(partAmendStatus);
     			
     			Code mappedCategory = getMappedCode(requestMap,"Category",category);
				sa.setSTUDYVERCATEGORY(mappedCategory);

				if(!isEmpty(type)){
					Code mappedVerType = getMappedCode(requestMap,"VersionType",type);
					sa.setSTUDYVERTYPE(mappedVerType);
				}

    			List<StudyStatusHistory> mappedStatusHist = new ArrayList<StudyStatusHistory>();
    			
    						
				code = new Code();
				code.setCode(partAmendStatus);
				StudyStatusHistory mappedStatus = new StudyStatusHistory();
				mappedStatus.setPKCODELSTSTAT(code);
				//mappedStatus.setSTATUSDATE(ssh.getSTATUSDATE());
				//mappedStatus.setSTATUSNOTES(ssh.getSTATUSNOTES());
				mappedStatusHist.add(mappedStatus);
				sa.getStudyStatusHistory().addAll(mappedStatusHist);
				
				
				if(!isEmpty(versionDate)){					
					String DATE_FORMAT_NOW = "yyyy-MM-dd";
					//Date date = new Date();
					
					String dateFormat = "MM/dd/yyyy";
					if (prop.containsKey("IRB_DateFormat") && !"".equals(prop.getProperty("IRB_DateFormat").trim())) {
						dateFormat = prop.getProperty("IRB_DateFormat").trim();
					}
					
					SimpleDateFormat sdfT = new SimpleDateFormat(dateFormat);
					sdfT.setLenient(false);
					Date date = sdfT.parse(versionDate);
					logger.info("Valid Date = "+date);
					
					
					SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
					String stringDate = sdf.format(date);
					logger.info("Formatted Valid Date = "+stringDate);

					XMLGregorianCalendar verDate = StringToXMLGregorianCalendar
							.convertStringDateToXmlGregorianCalendar(stringDate,
									"yyyy-MM-dd", true);
										
					sa.setSTUDYVERDATE(verDate);
				}
    			
				//sa.setSTUDYVERNOTES(verNotes);
    			
				StudyVersionIdentifier svI = new StudyVersionIdentifier();
    					svI.setSTUDYVERNUMBER(versionNumber);
    			sa.setStudyVerIdentifier(svI);
    			
    			/*List<StudyAppendix> studyAppendList = new ArrayList<StudyAppendix>();
    			
    			
    			StudyAppendix studyAppendix = new StudyAppendix();
    			studyAppendix.setSTUDYAPNDXFILEOBJ();
    			studyAppendix.setSTUDYAPNDXPUBFLAG("N");
    			studyAppendix.setSTUDYAPNDXTYPE("file");
    			studyAppendix.setSTUDYAPNDXURI(value);
    			
    			studyAppendList.add(studyAppendix);
    			*/
    			
    			List<StudyAppendix> studyAppendList = getStudyAppendixList(attachments,description);
    			if (studyAppendList != null && !studyAppendList.isEmpty()) {
    				sa.getStudyAppendix().addAll(studyAppendList);
    			}
    			if(!isEmpty(url)){
    				List<StudyAppendix> urlList = new ArrayList<StudyAppendix>();
        			
        			StudyAppendix saurl = new StudyAppendix();
        			saurl.setSTUDYAPNDXPUBFLAG("N");
        			saurl.setSTUDYAPNDXTYPE("url");
        			saurl.setSTUDYAPNDXURI(url);
        			saurl.setSTUDYAPNDXDESC(urlDescription);
        			urlList.add(saurl);
        			sa.getStudyAppendix().addAll(urlList);
    			}
    			
    			return sa;
    			
	}

	
	
	
	
	public Map<VelosKeys, Object> callCreateStudyVersion(Map<VelosKeys, Object> requestMap, List<Attachment> attachments,String irbProtocolNumber, String versionNumber, String versionDate,String category, String type,String description,String url,String urlDescription) throws Exception {

		Map<VelosKeys, Object> dataMap = null;
		String orgName = ((String) requestMap.get(ProtocolKeys.OrganizationName)).trim();
		String bSite = (String) requestMap.get(ProtocolKeys.BroadCastingSite);
		String studyNumber = (String) requestMap.get(ProtocolKeys.StudyNumber);

		try {

			String versNumber = versionNumber;
			String versCategory = null;

			StudyIdentifier studyIdentifier = new StudyIdentifier();
			studyIdentifier.setStudyNumber(studyNumber);

			Versions versions = new Versions();
			versions.setStudyIdentifier(studyIdentifier);

			StudyVersion creatStudyVersList = getMappedVersion(requestMap,attachments, irbProtocolNumber, versionNumber, versionDate,category, type,description,url,urlDescription);
			versions.getVersion().add(creatStudyVersList);

			try {
				ResponseHolder responseHolder = createVersionAmendments(versions);
				Issues issues = responseHolder.getIssues();
				Results results = responseHolder.getResults();
				if (issues != null && !"".equals(issues)) {
					List<Issue> issueList = issues.getIssue();
					List<String> errorList = new ArrayList<String>();
					if (!issueList.isEmpty()) {
						logger.info("Issues Found");
						logger.info("IssueList = " + issueList);
						for (Issue issue : issueList) {
							logger.error("Issue = " + issue);
							logger.error("Issue Type = " + issue.getType());
							logger.error("Issue Message = "	+ issue.getMessage());
							errorList.add(issue.getMessage());
							if(issue.getType().value().equals("DUPLICATE_STUDYVER_NUMBER")){
								String err = issue.getMessage();
								logger.error(err);
								throw new Exception(err);
							}
						}
						logger.error("Version Creation Failed =" + versNumber);
						throw new Exception(getErrorList(errorList));
						// emailNotification.sendNotification(requestMap,versNumber+" creation fails for study : ",orgName,"Amendment Versions",versNumber,"Failed",errorList);
						// saveDB(requestMap,orgName,"Amendment Versions",versNumber,"Failed",errorList);
					}
				}
				if (results != null) {
					List<CompletedAction> actionList = results.getResult();
					if (!actionList.isEmpty()) {
						for (CompletedAction action : actionList) {
							CrudAction crudAction = action.getAction();
							SimpleIdentifier si = action.getObjectId();
						}
						logger.info("********* VERSION CREATED");
						messageDao.saveVersionDetails(irbProtocolNumber,studyNumber,versionNumber,"Success","Version Created");
						
						try{
							InputStream	input=this.getClass().getClassLoader().getResourceAsStream("email.properties");
							Properties prop = new Properties();
							prop.load(input);
							
							String enableNotification = null;
							
							if (prop.containsKey("Enable_Notification_For_Success") && !"".equals(prop.getProperty("Enable_Notification_For_Success").trim())) {
								enableNotification = prop.getProperty("Enable_Notification_For_Success").trim();
							}
							if ((enableNotification != null) && (!"".equals(enableNotification)) && (enableNotification.equalsIgnoreCase("Y"))){
								String piscemail="";
								//piscemail = getStudyPIContactEmail(requestMap);
								emailNotification.sendVerSuccNotification(requestMap,piscemail,irbProtocolNumber,versionNumber,category, type);
							}else{
								logger.info("Notification For Success is Turned OFF");
							}
						}catch(Exception ex){
							logger.error("Error while sending notification",ex);
						}
						
						// messageDao.saveMessageDetails(studyNumber,bSite,orgName,"Amendment Versions",versNumber,"Created",
						// "Amendment Created");
					}
				}
			} catch (OperationException_Exception e) {
				// e.printStackTrace();
				dataMap = new HashMap<VelosKeys, Object>();
				List<Issue> issueList = e.getFaultInfo().getIssues().getIssue();
				List<String> errorList = new ArrayList<String>();
				for (Issue issue : issueList) {
					logger.error(issue.getType());
					logger.error(issue.getMessage());
					dataMap.put(ProtocolKeys.FaultString, issue.getMessage());
					// errorList.add("Issue Type = "+issue.getType()+". Message = "+issue.getMessage());
					logger.error("Issue Type = " + issue.getType()+ ". Message = " + issue.getMessage());
					errorList.add(issue.getMessage());
					logger.error("Got operaton exception with issue: "+ issue.getMessage());
				}
				dataMap.put(ProtocolKeys.ErrorList, errorList);
				if (issueList == null) {
					return null;
				}
				logger.error("Versions Creation Failed =" + versNumber);
				if (errorList.isEmpty()) {
					errorList.add(e.getMessage());
				}
				// emailNotification.sendNotification(requestMap,versNumber+" creation fails for study : ",orgName,"Amendment Versions",versNumber,"Failed",errorList);
				// saveDB(requestMap,orgName,"Amendment Versions",versNumber,"Failed",errorList);
				throw new Exception(getErrorList(errorList));
			}

		} catch (Exception e) {
			logger.error("Create Amendment version Exception " , e);
			// messageDao.saveMessageDetails(studyNumber,bSite,orgName,
			// "Amendment Versions","Creating Amendments Failed","Failed",
			// e.getMessage());
			throw e;
		}

		return dataMap;
	}


	public ResponseHolder createVersionAmendments(Versions versions)
			throws OperationException_Exception {
	
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList list = null;
		try{
			list = (MessageContentsList) producer.sendBody("cxf:bean:espCreateStudyVersionsEndpoint", 
					ExchangePattern.InOut, versions);        
		}catch(CamelExecutionException e) {
				logger.error(e.getCause());
				logger.error(e.getMessage());
			if (e.getCause() instanceof OperationException_Exception) {
				throw(OperationException_Exception)e.getCause();
			}else{
				throw e;
			}
		}
		return (ResponseHolder) list.get(0);
	}
	
	public List<StudyAppendix> getStudyAppendixList(List<Attachment> attachments,String description) throws Exception{
		
		List<StudyAppendix> studyAppendList = null;
		
		if (attachments != null && !attachments.isEmpty()) {
			studyAppendList = new ArrayList<StudyAppendix>();
			for (Attachment attachment : attachments) {
				logger.info("*****************************************");
				logger.info("Attachment Content ID = "+attachment.getContentId());
				logger.info("Attachment Content Type = "+attachment.getContentType());
				logger.info("Attachment Headers = "+attachment.getHeaders());
				logger.info("Attachment Headers List = "+attachment.getHeaderAsList("Content-Disposition"));
				logger.info("------");
				logger.info("ContentDisposition Type = "+attachment.getContentDisposition().getType());
				logger.info("ContentDisposition Parameters = "+attachment.getContentDisposition().getParameters());
				logger.info("------");
				DataHandler handlerT = attachment.getDataHandler();
				logger.info("DataHandler Content Type = "+ handlerT.getContentType());
				logger.info("DataHandler Name = "+ handlerT.getName());				
				logger.info("*****************************************");
				
				String filename = attachment.getContentDisposition().getParameter("filename");
				logger.info("FileName from Disposition : " + filename);
				if(!isEmpty(filename)){
				DataHandler handler = attachment.getDataHandler();
				try {
					InputStream stream = handler.getInputStream();
					logger.info("Input Stream size = "+stream.available());
					MultivaluedMap<String, String> map = attachment.getHeaders();
					logger.info("fileName : " + getFileName(map));
					/*String catBase = System.getProperty("catalina.base");
					String fileLoc = catBase+File.separatorChar+"filesreceived";
                    File destinationFolder =  new File(fileLoc);
                    if (!destinationFolder.exists())
                    {
                        destinationFolder.mkdirs();
                    }*/
					//OutputStream out = new FileOutputStream(new File("/home/brajasekharreddy/Documents/iMedRis/sponsor/"+filename));
                    //OutputStream out = new FileOutputStream(new File(fileLoc+File.separator+filename));
                    
					//int read = 0;
					//byte[] bytes = new byte[1024];
					//byte[] bytes = new byte[stream.available()]; //Creating with Stream size
					//while ((read = stream.read(bytes)) != -1) {
						//out.write(bytes, 0, read);
					//}
					byte[] bytes = IOUtils.toByteArray(stream);
					stream.close();
					//out.flush();
					
					StudyAppendix studyAppendix = new StudyAppendix();
					studyAppendix.setSTUDYAPNDXFILEOBJ(bytes);
					studyAppendix.setSTUDYAPNDXPUBFLAG("N");
					studyAppendix.setSTUDYAPNDXTYPE("file");
					studyAppendix.setSTUDYAPNDXURI(filename);
					studyAppendix.setSTUDYAPNDXDESC(description);
					
					studyAppendList.add(studyAppendix);
					
					logger.info("File Size = "+bytes.length);
					logger.info("--------------------------------------------");
					logger.info(bytes.toString());
					logger.info("--------------------------------------------");
					//logger.info(read);
					//logger.info("--------------------------------------------");
					//Prints the bytes
					//logger.info(Arrays.toString(bytes));
					//logger.info("--------------------------------------------");
					//Prints the contents of the file
					//System.out.write(bytes);
					//out.close();
				} catch (Exception e) {
					//e.printStackTrace();
					logger.error("Failed to upload files ",e);
					throw new Exception("Failed to create version");
				}
				}else{
					logger.info("Filename not provided OR File is not Attached");
				}
			}
		}else{
			logger.info("***  NO  Attachments  ***");
		}
		
		return studyAppendList;
		
	}


	private String getFileName(MultivaluedMap<String, String> header) {
	      String[] contentDisposition = header.getFirst("Content-Disposition").split(";");
	      for (String filename : contentDisposition) {
	         if ((filename.trim().startsWith("filename"))) {
	            String[] name = filename.split("=");
	            String exactFileName = name[1].trim().replaceAll("\"", "");
	            return exactFileName;
	         }
	      }
	      return "unknown";
	   }
	
	public Map<VelosKeys, Object> callGetStudySummary(Map<VelosKeys, Object> requestMap) {
		StudyIdentifier studyIdentifier = new StudyIdentifier();
		studyIdentifier.setStudyNumber((String)requestMap.get(ProtocolKeys.StudyNumber));

		Map<VelosKeys, Object> dataMap = null;
		StudySummary studySummary = null;
		try {
			studySummary = getStudySummary(studyIdentifier);
			dataMap = new HashMap<VelosKeys, Object>();
			dataMap.put(ProtocolKeys.StudyObj, studySummary);
			logger.info("Study Summary= "+studySummary);
		} catch (OperationException_Exception e) {
			//e.printStackTrace();
			logger.info("ERROR OCCURED");
			dataMap = new HashMap<VelosKeys, Object>();
			List<Issue> issueList = e.getFaultInfo().getIssues().getIssue();
			List<String> errorList = new ArrayList<String>();
			for(Issue issue : issueList){
				logger.info(issue.getType());
				logger.info(issue.getMessage());
				dataMap.put(ProtocolKeys.FaultString, issue.getMessage());
				errorList.add(issue.getMessage());
				logger.info("Got operaton exception with issue: "+issue.getMessage());
			}
			dataMap.put(ProtocolKeys.ErrorList, errorList);
			if (issueList == null) { return null; }
			return dataMap;
		} catch (Exception e) {
			logger.error("Error getting study Summary", e);
			return dataMap;
		}

		return dataMap;
	}
	

	public StudySummary getStudySummary(StudyIdentifier request)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList list = null;
		try{
			list = (MessageContentsList) producer.sendBody("cxf:bean:espGetStudySummaryEndpoint", 
					ExchangePattern.InOut, request);
		}catch(CamelExecutionException e) {
			logger.info(e.getCause());
			logger.info(e.getMessage());
			if (e.getCause() instanceof OperationException_Exception) {
				logger.info("throwing Exception");
				throw(OperationException_Exception)e.getCause();
			}else{
				throw e;
			}
		}
		logger.info("Returned Summary");
		return (StudySummary) list.get(0);
	}

	public Map<VelosKeys, Object> callSearchStudy(Map<VelosKeys, Object> requestMap) throws Exception {

		StudyIdentifier studyIdentifier = new StudyIdentifier();
		studyIdentifier.setStudyNumber((String)requestMap.get(ProtocolKeys.StudyNumber));

		String orgName = ((String)requestMap.get(ProtocolKeys.OrganizationName)).trim();
		logger.info("OrganizationName = "+orgName);

		String irbNumber = (String)requestMap.get(ProtocolKeys.IrbNumber);
		logger.info("Study Number = "+irbNumber);
		Properties prop = (Properties)requestMap.get(ProtocolKeys.PropObj);
		
		String ctxStudySubType = null;		
		Map<VelosKeys, Object> dataMap = null;
		Study study = null;
		StudySearchResults studySearchResults = null;
		String studyNumberInSummary = null;
		try {
			
				StudySearch studySearch = new StudySearch();
				studySearch.setStudyNumber(irbNumber);
			
				//study = getOrgStudy(studyIdentifier,orgName);
				studySearchResults = searchOrgStudy(studySearch,orgName);
				logger.info("StudySearchResults = "+studySearchResults);
				List<StudySearch> studySearchList = studySearchResults.getStudySearch();
				for(StudySearch studyresult : studySearchList){
					studyNumberInSummary = studyresult.getStudyNumber();
					logger.info("Studies found in site "+orgName);
					logger.info("Study Number = "+studyNumberInSummary);
				}
				logger.info("List = "+studySearchResults.getStudySearch());
		} catch (OperationException_Exception e) {
			//e.printStackTrace();
			logger.error("Error occured Searching study in Participating site");
			dataMap = new HashMap<VelosKeys, Object>();
			List<Issue> issueList = e.getFaultInfo().getIssues().getIssue();
			List<String> errorList = new ArrayList<String>();
			for(Issue issue : issueList){
				IssueTypes issueTypes = issue.getType();
				String value = issueTypes.value();
				logger.info("Issue value = "+value);
				logger.info("Issue Type = "+issue.getType());
				logger.info("Message = "+issue.getMessage());
				dataMap.put(ProtocolKeys.FaultString, issue.getMessage());
				if(issue.getType().value().equals("STUDY_NOT_FOUND")){
					logger.info("Adding STUDY_NOT_FOUND to Map");
					dataMap.put(ProtocolKeys.STUDY_NOT_FOUND, "STUDY_NOT_FOUND in "+orgName);
					logger.info("Study Not Found in "+orgName);
				}
				errorList.add(issue.getMessage());
				//logger.info("Got operaton exception with issue: "+issue.getMessage());
			}
			dataMap.put(ProtocolKeys.ErrorList, errorList);
			if (issueList == null) { return null; }
			return dataMap;
		} catch (Exception e) {
			//e.printStackTrace();
			logger.error("Error :",e);
			if(ctxStudySubType== null || "".equals(ctxStudySubType)){
				String ctxError = "Study Number mapping is not Configured "+orgName;
				logger.fatal(ctxError);
				//messageDao.saveMessageDetails(studyNumber,(String)requestMap.get(ProtocolKeys.BroadCastingSite),orgName, "Study",studyNumber,"Failed",ctxError);
			}
			throw e;
			//return dataMap;
		}

		logger.info(dataMap);
		logger.info(study);

		//if (dataMap == null && study != null ){
		if (dataMap == null && studySearchResults != null ){
			logger.info("Null map = "+dataMap);
			dataMap = new HashMap<VelosKeys, Object>();
			logger.error("Study Already exists in "+orgName);
			//Check for study number in MSD then send StudyAlreadyExists
			dataMap.put(ProtocolKeys.StudyAlreadyExists,"StudyAlreadyExists in "+orgName);
			
			List<String> errorList = new ArrayList<String>();
			//logger.error("Study Already exists-sent as package");
			errorList.add("Study Already exists with studyNumber = "+studyNumberInSummary);
			
			dataMap.put(ProtocolKeys.StudyNumber,studyNumberInSummary);
			logger.info(dataMap);
			return dataMap;
		}
		return dataMap;
	}

	public Map<VelosKeys, Object> callupdateStudySummary(Map<VelosKeys, Object> requestMap,StudyUpdates studyUpdates,Map<VelosKeys, Object> studyDataMap) throws Exception {
		
		String studyNumber = (String)requestMap.get(ProtocolKeys.StudyNumber);
		
		StudyIdentifier studyIdentifier = new StudyIdentifier();
		studyIdentifier.setStudyNumber(studyNumber);

		Map<VelosKeys, Object> dataMap = null;
	
		try {
			
			StudySummary summary = (StudySummary)studyDataMap.get(ProtocolKeys.StudyObj);
			StudySummary mappedSummary = getMappedStudySummary(requestMap,summary,studyUpdates);
			
			ResponseHolder responseHolder = updateStudySummary(studyIdentifier,mappedSummary);
			Issues issues = responseHolder.getIssues();
			Results results = responseHolder.getResults();
			if (issues != null && !"".equals(issues)) {
				List<Issue> issueList = issues.getIssue();
				List<String> errorList = new ArrayList<String>();
				if (!issueList.isEmpty()) {
					logger.info("Issues Found");
					logger.info("IssueList = " + issueList);
					for (Issue issue : issueList) {
						logger.error("Issue = " + issue);
						logger.error("Issue Type = " + issue.getType());
						logger.error("Issue Message = "	+ issue.getMessage());
						errorList.add(issue.getMessage());
					}
					throw new Exception(getErrorList(errorList));
					// emailNotification.sendNotification(requestMap,versNumber+" creation fails for study : ",orgName,"Amendment Versions",versNumber,"Failed",errorList);
					// saveDB(requestMap,orgName,"Amendment Versions",versNumber,"Failed",errorList);
				}
			}
			if (results != null) {
				List<CompletedAction> actionList = results.getResult();
				if (!actionList.isEmpty()) {
					for (CompletedAction action : actionList) {
						CrudAction crudAction = action.getAction();
						SimpleIdentifier si = action.getObjectId();
					}
						messageDao.saveVersionDetails(studyUpdates.getIrbProtocolNumber(),studyNumber,"","Success","Study Updated");
						
						try{
							InputStream	input=this.getClass().getClassLoader().getResourceAsStream("email.properties");
							Properties prop = new Properties();
							prop.load(input);
							
							String enableNotification = null;
							
							if (prop.containsKey("Enable_Notification_For_Success") && !"".equals(prop.getProperty("Enable_Notification_For_Success").trim())) {
								enableNotification = prop.getProperty("Enable_Notification_For_Success").trim();
							}
							if ((enableNotification != null) && (!"".equals(enableNotification)) && (enableNotification.equalsIgnoreCase("Y"))){
								String piscemail="";
								//piscemail = getStudyPIContactEmail(requestMap);
								emailNotification.sendSummarySuccNotification(requestMap,piscemail,studyUpdates);
							}else{
								logger.info("Notification For Success is Turned OFF");
							}
						}catch(Exception ex){
							logger.error("Error while sending notification",ex);
						}
						
						// messageDao.saveMessageDetails(studyNumber,bSite,orgName,"Amendment Versions",versNumber,"Created",
						// "Amendment Created");
					}
				}
			} catch (OperationException_Exception e) {
				// e.printStackTrace();
				dataMap = new HashMap<VelosKeys, Object>();
				List<Issue> issueList = e.getFaultInfo().getIssues().getIssue();
				List<String> errorList = new ArrayList<String>();
				for (Issue issue : issueList) {
					logger.error(issue.getType());
					logger.error(issue.getMessage());
					dataMap.put(ProtocolKeys.FaultString, issue.getMessage());
					// errorList.add("Issue Type = "+issue.getType()+". Message = "+issue.getMessage());
					logger.error("Issue Type = " + issue.getType()+ ". Message = " + issue.getMessage());
					errorList.add(issue.getMessage());
					logger.error("Got operaton exception with issue: "+ issue.getMessage());
				}
				dataMap.put(ProtocolKeys.ErrorList, errorList);
				if (issueList == null) {
					return null;
				}
				logger.error("Summary Update Failed ");
				if (errorList.isEmpty()) {
					errorList.add(e.getMessage());
				}
				// emailNotification.sendNotification(requestMap,versNumber+" creation fails for study : ",orgName,"Amendment Versions",versNumber,"Failed",errorList);
				// saveDB(requestMap,orgName,"Amendment Versions",versNumber,"Failed",errorList);
				throw new Exception(getErrorList(errorList));
		} catch (Exception ex) {
			logger.error("Study Summary Exception " , ex);
			// messageDao.saveMessageDetails(studyNumber,bSite,orgName,
			// "Amendment Versions","Creating Amendments Failed","Failed",
			// e.getMessage());
			throw ex;
		}
		

		return dataMap;
	}

	
	public ResponseHolder updateStudySummary(StudyIdentifier studyIdentifier,StudySummary studySummary)
			throws OperationException_Exception,OperationRolledBackException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList inpList = new MessageContentsList();
		inpList.add(studyIdentifier);
		inpList.add(studySummary);
		inpList.add(true);
		MessageContentsList list = null;
		try{
			list = (MessageContentsList) producer.sendBody("espUpdateSummaryEndpoint", 
					ExchangePattern.InOut,inpList);
		}catch(CamelExecutionException e) {
			logger.error(e.getCause());
			logger.error(e.getMessage());
			if (e.getCause() instanceof OperationException_Exception) {
				logger.info("throwing Exception");
				throw(OperationException_Exception)e.getCause();
			}else{
				throw e;
			}
		}
		logger.info("Updated Study Summary ");
		return (ResponseHolder) list.get(0);
	}
	
	public StudySummary getMappedStudySummary(Map<VelosKeys, Object> requestMap,StudySummary summary,StudyUpdates studyUpdates){
		
		Properties prop = (Properties)requestMap.get(ProtocolKeys.PropObj);
		
		summary.setStudyTitle(studyUpdates.getTitle());
		
		String irbSummary = studyUpdates.getSummary();
		String irbSponsorname = studyUpdates.getFundingType();
		String irbSponsorContact = studyUpdates.getSponsorContact();
		String irbScope = studyUpdates.getStudyScope();
		String irbPhase = studyUpdates.getPhase();
		String irbSponsorOther = studyUpdates.getFundingSource();
		String irbSponsorID = studyUpdates.getSponsorProtocolNumber();
		String irbSponsorOtherInfo = studyUpdates.getFundingOtherInformation();
		
		if(!isEmpty(irbSponsorOther)){
			summary.setSponsorNameOther(irbSponsorOther);
		}
		
		if(!isEmpty(irbSponsorID)){
			summary.setSponsorID(irbSponsorID);
		}
		
		if(!isEmpty(irbSponsorOtherInfo)){
			summary.setSponsorOtherInfo(irbSponsorOtherInfo);
		}		
		
		if(!isEmpty(irbSummary)){
			summary.setSummaryText(irbSummary);
		}
		
		if(!isEmpty(irbSponsorname)){
			Code mappedSponsor = getMappedCode(requestMap,"Sponsor",irbSponsorname);
			summary.setSponsorName(mappedSponsor);
		}
		
		if(!isEmpty(irbSponsorContact)){
			summary.setSponsorContact(irbSponsorContact);
		}
		if(!isEmpty(irbScope)){
			Code mappedScope = getMappedCode(requestMap,"StudyScope",irbScope);
			summary.setStudyScope(mappedScope);
		}
		if(!isEmpty(irbPhase)){
			Code mappedPhase = getMappedCode(requestMap,"Phase",irbPhase);
			summary.setPhase(mappedPhase);
		}
		
		List<NvPair> mappedMoreList = new ArrayList<NvPair>();
	
		String irbSubType = prop.getProperty("IRBNumber_subtype").trim();
		NvPair irb = new NvPair();
			irb.setKey(irbSubType);
			irb.setValue(studyUpdates.getIrbProtocolNumber());
		mappedMoreList.add(irb);
		
		String shortTitle="";
		
		if (prop.containsKey("ShortTitle_subtype") && !"".equals(prop.getProperty("ShortTitle_subtype").trim())) {
			shortTitle = prop.getProperty("ShortTitle_subtype").trim();
			NvPair shTitle = new NvPair();
				shTitle.setKey(shortTitle);
				shTitle.setValue(studyUpdates.getShortTitle());
			mappedMoreList.add(shTitle);
		}
		
		
		summary.getMoreStudyDetails().addAll(mappedMoreList);
		logger.info("MSD List ="+mappedMoreList);
		
		return summary;
	}
		
	public ChangesList getOutboundMessages(String fromDate,String formatToDate) throws Exception{

		Map<VelosKeys, Object> dataMap = new HashMap<VelosKeys, Object> ();

		ChangesList msglist = null;
		try {
			/*SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			String fromDate = sdf.format("2019-07-24T10:25:00");*/
			//String fromDate = "2019-07-24T12:25:00";
			//logger.info("Formatted Valid Date = "+fromDate);

			/*XMLGregorianCalendar validFromDate = StringToXMLGregorianCalendar
					.convertStringDateToXmlGregorianCalendar(stringDate,
							"yyyy-MM-dd'T'HH:mm:ss", true);*/
			XMLGregorianCalendar validFromDate = stringToXMLGregorianCalendar(fromDate);
			
			/*SimpleDateFormat simpleDateForm = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		    Date todate = new Date();
		    String formatToDate = simpleDateForm.format(todate);*/
		    
		    /*XMLGregorianCalendar validToDate = StringToXMLGregorianCalendar
					.convertStringDateToXmlGregorianCalendar(formatToDate,
							"yyyy-MM-dd'T'HH:mm:ss", true);*/
		    XMLGregorianCalendar validToDate = stringToXMLGregorianCalendar(formatToDate);

		    
		    String moduleName = "study_status";
		    
		    String calledFrom = "TeaugoIRB";
		    
		    logger.info("********OUTBOUND MESSAGE Request parameters");
		    logger.info("validFromDate = "+validFromDate);
		    logger.info("validToDate = "+validToDate);
		    logger.info("moduleName = "+moduleName);
		    logger.info("calledFrom = "+calledFrom);
		    
		    msglist =  getOutboundMessages(validFromDate,validToDate,moduleName,calledFrom);
		    
		    logger.info("Msg List = "+msglist);
		    
		    msglist.getChanges();
			
		} catch (OperationException_Exception e) {
			//e.printStackTrace();
			logger.error("**ERROR OCCURED GETTING OUTBOUND Messages**");
			dataMap = new HashMap<VelosKeys, Object>();
			List<Issue> issueList = e.getFaultInfo().getIssues().getIssue();
			List<String> errorList = new ArrayList<String>();
			for(Issue issue : issueList){
				logger.error("Issue Type = "+issue.getType());
				logger.error("Message = "+issue.getMessage());
				dataMap.put(ProtocolKeys.FaultString, issue.getMessage());
				errorList.add(issue.getMessage());
			}
			dataMap.put(ProtocolKeys.ErrorList, errorList);
			if (issueList == null) { return null; }
			throw new Exception("Failed to get Outbound messages from eSP");
		} catch (Exception e) {
			//e.printStackTrace();
			logger.error("Getting OUTBOUND Messages  Failed :",e);
			throw new Exception("Failed to get Outbound messages from eSP");
		}

		return  msglist;
	}
	
	
	public ChangesList getOutboundMessages(javax.xml.datatype.XMLGregorianCalendar fromDate,
	        javax.xml.datatype.XMLGregorianCalendar toDate,
	        java.lang.String moduleName,
	        java.lang.String calledFrom
	    ) throws OperationException_Exception{
		MessageContentsList inputList=new MessageContentsList();
		inputList.add(fromDate);
		inputList.add(toDate);
		inputList.add(moduleName);
		inputList.add(calledFrom);
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList list = null;
		try{
			list = (MessageContentsList) producer.sendBody("espGetOutboundMessageEndpoint", 
					ExchangePattern.InOut,inputList );

		}	catch(CamelExecutionException e) {
			logger.info(e.getCause());
			logger.info(e.getMessage());
			if (e.getCause() instanceof OperationException_Exception) {
				logger.info("throwing Exception");
				throw(OperationException_Exception)e.getCause();
			}else{
				throw e;
			}
		}
		return (ChangesList) list.get(0);
	}


	public XMLGregorianCalendar stringToXMLGregorianCalendar(String s)
			throws ParseException, DatatypeConfigurationException {
		XMLGregorianCalendar result = null;
		Date date;
		SimpleDateFormat simpleDateFormat;
		GregorianCalendar gregorianCalendar;

		simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		date = simpleDateFormat.parse(s);
		gregorianCalendar = (GregorianCalendar) GregorianCalendar.getInstance();
		gregorianCalendar.setTime(date);
		result = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
		result.setTimezone(DatatypeConstants.FIELD_UNDEFINED );
		result.setMillisecond(DatatypeConstants.FIELD_UNDEFINED);
		
		return result;
	}
	
	public void sendStudyToIRB(Map<VelosKeys, Object> requestMap,Map<VelosKeys, Object> studyDataMap,OutboundMessageBean omb) throws Exception{
		
		Properties prop = (Properties)requestMap.get(ProtocolKeys.PropObj);
		
		String studyNumber = (String)requestMap.get(ProtocolKeys.StudyNumber);
		logger.info("***********Study Data to IRB ");
		logger.info("Study Number = "+studyNumber);
		Study study = (Study)studyDataMap.get(ProtocolKeys.StudyObj);
		
		StudySummary stdSummary = study.getStudySummary();
		
		String piName="";
		UserIdentifier pi = stdSummary.getPrincipalInvestigator();
		if(pi!=null){
			/*String piFname = pi.getFirstName();
			String piLName = pi.getLastName();
			piName = piFname+" "+piLName;*/
			//piName=pi.getUserLoginName();
			try {
				piName = getUserEmail(requestMap,pi.getPK());
			} catch (Exception e) {
				logger.error("Failed to get PI Email ID");
			}
		}
		logger.info("PI Email = "+piName);
		
		String scName="";
		UserIdentifier sc = stdSummary.getStudyContact();
		if(sc!=null){
			/*String scFname = sc.getFirstName();
			String scLName = sc.getLastName();
			scName = scFname+" "+scLName;*/
			scName=sc.getUserLoginName();
			try {
				scName = getUserEmail(requestMap,sc.getPK());
			} catch (Exception e) {
				logger.error("Failed to get Study Contact Email ID");
			}
		}
		logger.info("Study Contact = "+scName);
		
		String title = stdSummary.getStudyTitle();
		title = title.replaceAll("&", "&amp;");
		title = title.replaceAll("<", "&lt;");
		logger.info("Study Title = "+title);
		
		String phase="";
		Code stdPhase = stdSummary.getPhase();
		if(stdPhase!=null){
			phase=stdPhase.getDescription();
		}
		logger.info("Study Phase = "+phase);
		
		String scope="";
		Code stdScope = stdSummary.getStudyScope();
		if(stdScope!=null){
			scope=stdScope.getDescription();
		}
		logger.info("Study Scope = "+scope);
		
		String statustoirb = "";
		String statusValidFrom="";
		StudyStatuses studyStatuses=study.getStudyStatuses();
		List<StudyStatus> statusList = studyStatuses.getStudyStatus();
		for(StudyStatus status : statusList){
			Code statusCode = status.getStatus();
			String statusDesc =	statusCode.getDescription();
			String statusSubTyp = statusCode.getCode();
			logger.info("Status = "+statusDesc);
			logger.info("Status subtype= "+statusSubTyp);
			if(statusSubTyp.equals(prop.getProperty("study.status").trim())){
				statustoirb = statusDesc;
				statusValidFrom = status.getValidFromDate().toString();
				logger.info("Status to IRB = "+statusDesc);
				logger.info("statusValidFrom = "+statusValidFrom);
				break;
			}
		}
		
		
		Map<String, String> irbdataMap= new HashMap<String,String>();
		irbdataMap.put("StudyNumber", studyNumber);
		irbdataMap.put("PI", piName);
		irbdataMap.put("StudyContact", scName);
		irbdataMap.put("Title", title);
		irbdataMap.put("Phase", phase);
		irbdataMap.put("Scope", scope);
		irbdataMap.put("StudyStatus", statustoirb);
		irbdataMap.put("StatusValidFrom", statusValidFrom);
		
		String irbFields = prop.getProperty("IRBFields");
		String[] irbArr = irbFields.split(",");
		logger.info("IRB Fields :"+Arrays.asList(irbArr));
		ArrayList<String> xmlIPlist = new ArrayList<String>();
		for(String irbField : irbArr){
			xmlIPlist.add(irbdataMap.get(irbField));
		}
		logger.info("XML Input List = "+xmlIPlist);
		

		
		
		String irbURL = prop.getProperty("IRB_URL");
		
		//sendToIRB(requestMap,omb,irbRequestXML,irbURL);
		
		sendStudyToeIRB(prop,omb,xmlIPlist,irbdataMap);
		
		/*String sessonToken = checkIRBLogin(prop,irbURL);
		if(!isEmpty(sessonToken)){
			IRBResponse irbResponse = sendIRBStudyXml(prop,irbURL,sessonToken,xmlIPlist);
			if (irbResponse.getStatus().equals("Success")) {
				
			} else {
				logger.error("Failed to send study to IRB = "+irbResponse.getResponse());
			}
			logoff(prop,irbURL,sessonToken);
		}else{
			String errMsg="Failed to establish an authenticated session and to get the session token from IRB";
			throw new Exception(errMsg);
		}*/
		
	}
	
	public void sendStudyToeIRB(Properties prop,OutboundMessageBean omb,ArrayList<String> xmlIPlist,Map<String, String> irbdataMap) throws Exception{
		int outboundMsgCount = 0;
		int dbCount = 0;
		int pk = 0;
		try {
			outboundMsgCount = Integer.parseInt(prop.getProperty("outboundMsgCount"));
			pk = omb.getPk_vgdb_outbound_message();
			logger.info("VGDB PK = "+pk);
			logger.info("No of times to process outbound Message = "+outboundMsgCount);
			
			dbCount = messageDao.getCount(pk,0);
			logger.info("Database Count = "+dbCount);
			if(dbCount < outboundMsgCount) {
				String irbURL = prop.getProperty("IRB_URL");
				String sessionToken = checkIRBLogin(prop, irbURL,omb);
				logger.info("Session Token = " +sessionToken);
				if (!isEmpty(sessionToken)) {
					IRBResponse irbResponse = sendIRBStudyXml(prop, irbURL, sessionToken, xmlIPlist,omb,irbdataMap);
					/*if (irbResponse.getStatus().equals("Success")) {
						messageDao.saveStatusDetails(omb,"Success",irbResponse.getResponse());
						messageDao.updateOutboundMessageInfo(omb);
					} else {
						logger.error("Failed to send study to IRB = "+ irbResponse.getResponse());
					}*/
					logoff(prop, irbURL, sessionToken);
				} else {
					String errMsg = "Failed to establish an authenticated session and to get the session token from IRB";
					String irbLoginXML = prop.getProperty("IRB_Login_XML");
					String storeName = prop.getProperty("IRB_storeName");
			    	String irbUsername = prop.getProperty("IRB_userName");
			    	String irbPassword = prop.getProperty("IRB_password");
			    	ArrayList<String> loginIPlist = new ArrayList<String>();
			    	loginIPlist.add(storeName);
			    	loginIPlist.add(irbUsername);
			    	loginIPlist.add(irbPassword);
					String irbRequestXML = String.format(irbLoginXML, loginIPlist.toArray());
					messageDao.saveStatusDetails(omb,"Failed",errMsg,irbRequestXML);
					throw new Exception(errMsg);
				}
			}else{
				logger.info("Message already processed "+outboundMsgCount+" times");
			}
		} catch (Exception e) {
			logger.info("!!!!!!!!!!Send email for failure");
			String piscemail="";
			//piscemail = velosSendStudyData.getStudyPIContactEmail(requestMap);
			Map<VelosKeys, Object> requestMap = null;
			emailNotification.sendOutboundStudyFailNotification(requestMap,piscemail,irbdataMap);

			logger.error("Failure ", e);
			if(dbCount < outboundMsgCount) {
				dbCount = dbCount+1;
				messageDao.getCount(pk,dbCount);
				sendStudyToeIRB(prop,omb,xmlIPlist,irbdataMap);
			}else{
				throw e;
			}
		}
	}


    public String checkIRBLogin(Properties prop,String irbURL,OutboundMessageBean omb) throws Exception{
    	
    	String sessonToken = null;
    	String irbLoginXML = prop.getProperty("IRB_Login_XML");
    	String storeName = prop.getProperty("IRB_storeName");
    	String irbUsername = prop.getProperty("IRB_userName");
    	String irbPassword = prop.getProperty("IRB_password");
    	
    	String eTagName = "soap12:Envelope";
		if (prop.containsKey("ElementTagName") && !"".equals(prop.getProperty("ElementTagName").trim())) {
			eTagName = prop.getProperty("ElementTagName").trim();
			logger.info("Element Tag name = "+eTagName);
		}
    	
    	ArrayList<String> loginIPlist = new ArrayList<String>();
    	loginIPlist.add(storeName);
    	loginIPlist.add(irbUsername);
    	loginIPlist.add(irbPassword);
		
		String irbRequestXML = String.format(irbLoginXML, loginIPlist.toArray());
		//logger.info("IRB Login Request : \n"+irbRequestXML);
		String errMsg="Failed to establish an authenticated session and to get the session token from IRB";
		try {
			IRBResponse irbResponse = sendToIRB(irbRequestXML,irbURL);
			if (irbResponse.getStatus().equals("Success")) {
				//sessonToken="dsdsdd";
				//Parse the Response XML and set sesion token
				sessonToken=getSessionToken(irbResponse.getResponse(),eTagName);
				messageDao.saveStatusDetails(omb,"Success",irbResponse.getResponse(),irbRequestXML);
			} else {
				logger.error("Failed to get Session");
				throw new Exception(irbResponse.getResponse());
			}
		} catch (Exception e) {
			logger.error("Failed to login",e);
			messageDao.saveStatusDetails(omb,"Failed",e.getMessage(),irbRequestXML);
			throw new Exception(errMsg);
		}
		return sessonToken;
    }
    
    
    public IRBResponse sendIRBStudyXml(Properties prop,String irbURL,String sessonToken,ArrayList<String> xmlIPlist,OutboundMessageBean omb,Map<String, String> irbdataMap) throws Exception{
    	
		String irbStudyXML = prop.getProperty("IRB_Study_XML");
		String irbStudyRequestXML = String.format(irbStudyXML, xmlIPlist.toArray());
		logger.info("IRB Study Request XML = "+irbStudyRequestXML);
		
		ArrayList<String> finalIPlist = new ArrayList<String>();
		finalIPlist.add(sessonToken);
		finalIPlist.add(irbStudyRequestXML);
		
		String finalXML = prop.getProperty("IRB_Final_XML");
		String irbRequestXML = String.format(finalXML,finalIPlist.toArray());
		logger.info("IRB Final Request XML = "+irbRequestXML);
		
    	
		String errMsg="Failed to send study to IRB";
		IRBResponse irbResponse = null;
		try {
			irbResponse = sendToIRB(irbRequestXML,irbURL);
			if (irbResponse.getStatus().equals("Success")) {
				logger.info("Study Sent successfully to IRB");
				String piscemail="";
				//piscemail = getStudyPIContactEmail(requestMap);
				Map<VelosKeys, Object> requestMap = null;
				emailNotification.sendOutboundStudyNotification(requestMap,piscemail,irbdataMap);
				
				messageDao.saveStatusDetails(omb,"Success",irbResponse.getResponse(),irbRequestXML);
				messageDao.updateOutboundMessageInfo(omb);
			} else {
				logger.error("Failed to send study to IRB = "+ irbResponse.getResponse());
				throw new Exception(irbResponse.getResponse());
			}
		} catch (Exception e) {
			logger.error("Failed to send study to IRB",e);
			messageDao.saveStatusDetails(omb,"Failed",e.getMessage(),irbRequestXML);
			throw new Exception(errMsg);
		}
		return irbResponse;
    }
    
    public IRBResponse logoff(Properties prop,String irbURL,String sessonToken) throws Exception{
    	
		String irbLogoffXML = prop.getProperty("IRB_Logoff_XML");
		String irbRequestXML = String.format(irbLogoffXML, sessonToken);
		logger.info("IRB LogOff Request XML = "+irbRequestXML);
    	
		String errMsg="Failed to LogOff session";
		IRBResponse irbResponse = null;
		try {
			irbResponse = sendToIRB(irbRequestXML,irbURL);
			if (irbResponse.getStatus().equals("Success")) {
				logger.info("IRB Session logoff successful");
			} else {
				logger.error("Failed to logoff session = "+irbResponse.getResponse());
			}
		} catch (Exception e) {
			logger.error(errMsg,e);
		}
		return irbResponse;
    }
    
    
	public IRBResponse sendToIRB(String input, String irbURL) throws Exception {

		IRBResponse irbResponse = new IRBResponse();
		URL url = new URL(irbURL);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setDoOutput(true);
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Content-Type", "text/xml");
		conn.setDoInput(true);

		OutputStream os = conn.getOutputStream();
		os.write(input.getBytes());
		os.flush();

		Scanner scanner;
		String result;
		logger.info("Response Code = " + conn.getResponseCode());
		if (conn.getResponseCode() != 200) {
			scanner = new Scanner(conn.getErrorStream());
			result = "Error From Server \n";
			logger.error(result);
			irbResponse.setStatus("Failed");;
		} else {
			scanner = new Scanner(conn.getInputStream());
			result = "Response From Server \n";
			logger.info(result);
			irbResponse.setStatus("Success");;
		}
		scanner.useDelimiter("\\Z");

		String response = scanner.next();
		
		irbResponse.setResponse(response);

		logger.info("*******Response from IRB**************=\n" + response);
		scanner.close();
		conn.disconnect();
		// messageDao.saveStatusDetails(omb,"Success",response);
		return irbResponse;
	}
	
	public String sendToIRB(Map<VelosKeys, Object> requestMap,OutboundMessageBean omb,String input,String irbURL) throws Exception{
		
		String status="SUCCESS";
		//try {
			
			//String wURL=(String)prop.getProperty("irbURL").trim();
			//String Username =(String)prop.getProperty("RWS_UserName").trim();
			//String password =(String)prop.getProperty("RWS_Password").trim();;
			
	
			URL url = new URL(irbURL);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "text/xml");
			conn.setDoInput(true);

			/*String userpass = Username + ":"	+ password;
			String basicAuth = "Basic "
					+ javax.xml.bind.DatatypeConverter
							.printBase64Binary(userpass.getBytes());
			conn.setRequestProperty("Authorization", basicAuth);*/
	
			//String input = sb.toString();
			OutputStream os = conn.getOutputStream();
			os.write(input.getBytes());
			os.flush();

			Scanner scanner;
			String result;
			logger.info("Response Code = "+conn.getResponseCode());
			if (conn.getResponseCode() != 200) {
				scanner = new Scanner(conn.getErrorStream());
				result = "Error From Server \n";
				logger.error(result);
			} else {
				scanner = new Scanner(conn.getInputStream());
				result = "Response From Server \n";
				logger.info(result);
			}
			scanner.useDelimiter("\\Z");
			
			String response=scanner.next(); 
			
			logger.info("*******Response from IRB**************=\n"+response);	
			scanner.close();
			conn.disconnect();
			
			messageDao.saveStatusDetails(omb,"Success",response,input);
			
		return status;
	}
	
	public String getCharacterDataFromElement(Element e) {
	    Node child = e.getFirstChild();
	    if (child instanceof CharacterData) {
	    	CharacterData cd = (CharacterData) child;
	    	return cd.getData();
	    }
	    return "";
	}
	
	public String getSessionToken(String studyXml,String eTagName) throws ParserConfigurationException, SAXException, IOException{
		
		
		/*String studyXml = "<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
							+"<soap12:Body>"
							+"<LoginResponse xmlns=\"http://clickcommerce.com/Extranet/WebServices\">"
							+"<LoginResult>string</LoginResult>"
							+" </LoginResponse>"
							+"</soap12:Body>"
							+"</soap12:Envelope>";*/
		logger.info("Getting session Token ");
		String sessionToken=null;
		DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(studyXml));

			Document doc = db.parse(is);
			NodeList nodes = doc.getElementsByTagName(eTagName);
			for (int i = 0; i < nodes.getLength(); i++) {
				Element element = (Element) nodes.item(i);

		
				NodeList title = element.getElementsByTagName("LoginResult");
					Element loginResult = (Element) title.item(0);
					sessionToken=getCharacterDataFromElement(loginResult);
			      
				logger.info("LoginResult : " +sessionToken);
			}
			return sessionToken;
	}



}

