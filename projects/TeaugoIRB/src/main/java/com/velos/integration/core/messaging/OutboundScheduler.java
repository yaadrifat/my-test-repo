package com.velos.integration.core.messaging;

import com.velos.integration.dao.MessageDAO;
import com.velos.integration.espclient.VelosEspClientCamel;
import com.velos.integration.espclient.VelosEspMethods;
import com.velos.integration.espclient.VelosSendStudyData;
import com.velos.integration.mapping.EndpointKeys;
import com.velos.integration.mapping.ProtocolKeys;
import com.velos.integration.mapping.VelosKeys;
import com.velos.integration.notifications.EmailNotification;
import com.velos.integration.util.PropertiesUtil;
import com.velos.services.Change;
import com.velos.services.Changes;
import com.velos.services.ChangesList;
import com.velos.services.OutboundIdentifier;
import com.velos.services.OutboundPatientProtocolIdentifier;
import com.velos.services.OutboundStudyIdentifier;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.StringWriter;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;
import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class OutboundScheduler{
  private static Logger logger = Logger.getLogger(OutboundScheduler.class);
  Properties prop = null;
  ClassPathXmlApplicationContext epicContext = null;
  ClassLoader classLoader = null;
  PropertiesConfiguration outBoundProp = null;
  
  private VelosEspClientCamel camelClient;

	public VelosEspClientCamel getCamelClient() {
		return camelClient;
	}

	public void setCamelClient(VelosEspClientCamel camelClient) {
		this.camelClient = camelClient;
	}
  
	
	private MessageDAO messageDao;

	public MessageDAO getMessageDao() {
		return messageDao;
	}

	public void setMessageDao(MessageDAO messageDao) {
		this.messageDao = messageDao;
	}
	
	private VelosSendStudyData velosSendStudyData;
	
	public VelosSendStudyData getVelosSendStudyData() {
		return velosSendStudyData;
	}
	public void setVelosSendStudyData(VelosSendStudyData velosSendStudyData) {
		this.velosSendStudyData = velosSendStudyData;
	}
	
  public OutboundScheduler() throws IOException { prop = new Properties();
    prop.load(getClass().getClassLoader().getResourceAsStream("config.properties"));
  }
  
  public void run() throws Exception {
    Thread.sleep(6000L);
    logger.info("**************** Schedular Start **************");
    String fromDate = getPropertyValue("outbound.properties", "last_outbound_from_time");
    String toDate = getPropertyValue("outbound.properties", "last_outbound_to_time");
    try{
    	ChangesList msgs = camelClient.getOutboundMessages(fromDate,toDate);
    	logger.info("Returned messages = "+msgs);
    	String status = null;
    	String timeStamp = null;
    	String statusOID = null;
    	String action = null;
        String module = null;
        String oid = null;

    	if(msgs!=null){
    		List<Changes> msgList = msgs.getChanges();
    		logger.info("Changes Messages list ="+msgList);
    		if(msgList!=null && !msgList.isEmpty()){
    			for(Changes msg : msgList){
    				List<Change> chngList = msg.getChange();
    				for(Change msgData : chngList){
    					logger.info("Change XmL data ");
    					action = msgData.getAction().value();
    					module = msgData.getModule();
    					status = msgData.getStatusSubType();
    					timeStamp = msgData.getTimeStamp();
    					logger.info("Action = "+action);
    					logger.info("Module = "+module);
    					logger.info("Status = "+status);
    					logger.info("Status Desc= "+msgData.getStatusCodeDesc());
    					logger.info("Timestamp = "+timeStamp);
    					statusOID = msgData.getIdentifier().getOID();
    					logger.info("Change/Status OID = "+statusOID);
    				}
    				OutboundIdentifier outId = msg.getParentIdentifier().getId();
    				logger.info("OutboundIdentifier = "+outId);
    				OutboundStudyIdentifier outSid = outId.getStudyIdentifier();
    				logger.info("OutboundStudyIdentifier = "+outSid);
    				String studyNumb=null;
    				if(outSid!=null){
    					studyNumb = outId.getStudyIdentifier().getStudyNumber();
    					logger.info("Study Number = "+studyNumb);
    					String parentOID = outId.getOID();
    					logger.info("Parent OID = "+parentOID);
    				}
    				if(studyNumb==null){
    					studyNumb = outId.getStudyNumber();
    					logger.info("StudyNumber from OutboundIdentifier = "+outId.getStudyNumber());
    				}
    				if(status.equals(prop.getProperty("study.status").trim())){
    					messageDao.insertOutboundMessageDetails(module,studyNumb,status,action,timeStamp,statusOID);
    					//convertObjectToXml(msg);
					}else{
						String pkgStatus = "Incorrect Status to send StudyData to IRB";
						logger.error(pkgStatus);
					}
    			}
    		}else{
    			logger.info("||||||||NO OUTBOUND Messages from eSP");
    		}
    		updateOuboundProperties(toDate);
    	}//End of msgs loop
    }catch(Exception e){
    	logger.error("Failed to get OUTBOUND Messages from eSP",e);
    }
    
    List<OutboundMessageBean> outbondMsgList = null;
    int count = 0;
    int outboundMsgCount = Integer.parseInt(prop.getProperty("outboundMsgCount"));
    outbondMsgList = messageDao.getOutboundMessageBean("study_status",outboundMsgCount);
    if(outbondMsgList!=null && !outbondMsgList.isEmpty()){
    	for (OutboundMessageBean omb : outbondMsgList){
    	  String studyNumber = null;
    	  String status = null;
    	  String module = null;
    	  String action = null;
    	  String oid = null;
          int pk_outbound_message = 0;
          String processedFlag="false";
          try
          {
            studyNumber = omb.getStudyNumber();
            status = omb.getStatus();
            module = omb.getModuleName();
            action = omb.getAction();
            oid = omb.getOID();
            pk_outbound_message = omb.getPk_vgdb_outbound_message();
            logger.info("||||||Processing VGDB Outbound message");
            logger.info("Pk = "+pk_outbound_message);
			logger.info("StudyNumber = "+studyNumber);
			logger.info("Status = "+status);
            logger.info("Action = "+action);
			logger.info("Module = "+module);
            velosSendStudyData.sendStudyDataToIRB(studyNumber,omb);
            processedFlag="true";
            //messageDao.updateOutboundMessageInfo(omb);
          }catch (Exception e) {
        	  processedFlag="false";
        	  logger.error("Failed to send Outbound Message : ",e);
        	  messageDao.saveStatusDetails(omb,"Failed",e.getMessage(),"");
          }
    	}
    }else{
    	logger.info("Outbound messages not available in VGDB");
    }
  }
  

  
  private void convertObjectToXml(Changes changes) throws JAXBException {
	// TODO Auto-generated method stub
	  JAXBContext contextObj = JAXBContext.newInstance(Changes.class);  
	  
	    Marshaller marshallerObj = contextObj.createMarshaller();  
	    marshallerObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);  
	  
	    StringWriter s = new StringWriter(); 
	    
	    //marshallerObj.marshal(changes, s); 
	    //marshallerObj.marshal(changes, s);
	    
	  //If we DO NOT have JAXB annotated class
        JAXBElement<Changes> jaxbElement = new JAXBElement<Changes>( new QName("", "changes"),Changes.class,changes);
	    marshallerObj.marshal(jaxbElement, s);
		 
		logger.info("Changes XML M =\n"+s.toString());
  }

  public void updateOuboundProperties(String schStartDate) throws Exception{
    FileOutputStream out = null;
    try{
      logger.info(" *********Updating Outbound Properties");
      classLoader = getClass().getClassLoader();
      out = new FileOutputStream(classLoader.getResource("outbound.properties").getFile());
      outBoundProp.setProperty("last_outbound_from_time", schStartDate);
      logger.info("Updated_Outbound_From_Date = " + schStartDate);
      
      String updatedDate = incrementDatebyMinutes(schStartDate,1);
      outBoundProp.setProperty("last_outbound_to_time", updatedDate);
      logger.info("Updated_Outbound_TO_Date = " + updatedDate);
      
      outBoundProp.save(out);
    }
    catch (Exception e) {
      logger.info("Error in update OutboundProperties = " + e.getMessage());
      throw e;
    } finally {
      if (out != null)
        out.close();
    }
  }
  
  
  public String incrementDatebyMinutes(String s,int m) throws ParseException{
		
		logger.info("Inital Time = "+s);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		Date date = simpleDateFormat.parse(s);
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		
		cal.add(Calendar.MINUTE, m);
		
		Date incTime = cal.getTime();
		String stringDate = simpleDateFormat.format(incTime);
		logger.info("Increment Time = "+stringDate);
		return stringDate;
	}
  
  public String getPropertyValue(String propertyFileName, String keyName) throws ConfigurationException {
    String keyList = null;
    try {
      classLoader = getClass().getClassLoader();
      File file = new File(classLoader.getResource(propertyFileName).getFile());
      outBoundProp = new PropertiesConfiguration(file);
      outBoundProp.setReloadingStrategy(new FileChangedReloadingStrategy());
      keyList = (String)outBoundProp.getProperty(keyName);
    } catch (Exception e) {
      e.printStackTrace();
      logger.info("Get Property Value = " + e.getMessage());
    }
    return keyList;
  }
    
  public String getDate() throws Exception{
    try {
      SimpleDateFormat simpleDateForm = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
      Date todate = new Date();
      return simpleDateForm.format(todate);
    }
    catch (Exception e) {
      logger.info("Error in updateOutboundProperties  getDate() = " + e.getMessage());
      throw e;
    }
  }
  
}