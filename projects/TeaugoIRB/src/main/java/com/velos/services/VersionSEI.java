package com.velos.services;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 2.7.12
 * 2017-04-10T12:14:46.993+05:30
 * Generated source version: 2.7.12
 * 
 */
@WebService(targetNamespace = "http://velos.com/services/", name = "VersionSEI")
@XmlSeeAlso({ObjectFactory.class})
public interface VersionSEI {

    @WebResult(name = "Response", targetNamespace = "")
    @RequestWrapper(localName = "createVersions", targetNamespace = "http://velos.com/services/", className = "com.velos.services.CreateVersions")
    @WebMethod
    @ResponseWrapper(localName = "createVersionsResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.CreateVersionsResponse")
    public com.velos.services.ResponseHolder createVersions(
        @WebParam(name = "Versions", targetNamespace = "")
        com.velos.services.Versions versions
    ) throws OperationException_Exception;

    @WebResult(name = "Versions", targetNamespace = "")
    @RequestWrapper(localName = "getStudyVersions", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetStudyVersions")
    @WebMethod
    @ResponseWrapper(localName = "getStudyVersionsResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetStudyVersionsResponse")
    public com.velos.services.Versions getStudyVersions(
        @WebParam(name = "StudyIdentifier", targetNamespace = "")
        com.velos.services.StudyIdentifier studyIdentifier,
        @WebParam(name = "CurrentStatus", targetNamespace = "")
        boolean currentStatus
    ) throws OperationException_Exception;
}
