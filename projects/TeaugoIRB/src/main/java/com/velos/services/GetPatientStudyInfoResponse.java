
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getPatientStudyInfoResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getPatientStudyInfoResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PatientStudy" type="{http://velos.com/services/}patientProtocolIdentifier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getPatientStudyInfoResponse", propOrder = {
    "patientStudy"
})
public class GetPatientStudyInfoResponse {

    @XmlElement(name = "PatientStudy")
    protected PatientProtocolIdentifier patientStudy;

    /**
     * Gets the value of the patientStudy property.
     * 
     * @return
     *     possible object is
     *     {@link PatientProtocolIdentifier }
     *     
     */
    public PatientProtocolIdentifier getPatientStudy() {
        return patientStudy;
    }

    /**
     * Sets the value of the patientStudy property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientProtocolIdentifier }
     *     
     */
    public void setPatientStudy(PatientProtocolIdentifier value) {
        this.patientStudy = value;
    }

}
