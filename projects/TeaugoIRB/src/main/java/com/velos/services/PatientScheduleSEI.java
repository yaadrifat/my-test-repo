package com.velos.services;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 2.2.9
 * Fri Jan 16 16:33:50 PST 2015
 * Generated source version: 2.2.9
 * 
 */
 
@WebService(targetNamespace = "http://velos.com/services/", name = "PatientScheduleSEI")
@XmlSeeAlso({ObjectFactory.class})
public interface PatientScheduleSEI {

    @WebResult(name = "CurrentPatientSchedule", targetNamespace = "")
    @RequestWrapper(localName = "getCurrentPatientSchedule", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetCurrentPatientSchedule")
    @WebMethod
    @ResponseWrapper(localName = "getCurrentPatientScheduleResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetCurrentPatientScheduleResponse")
    public com.velos.services.PatientSchedule getCurrentPatientSchedule(
        @WebParam(name = "PatientIdentifier", targetNamespace = "")
        com.velos.services.PatientIdentifier patientIdentifier,
        @WebParam(name = "StudyIdentifier", targetNamespace = "")
        com.velos.services.StudyIdentifier studyIdentifier,
        @WebParam(name = "startDate", targetNamespace = "")
        javax.xml.datatype.XMLGregorianCalendar startDate,
        @WebParam(name = "endDate", targetNamespace = "")
        javax.xml.datatype.XMLGregorianCalendar endDate
    ) throws OperationException_Exception;

    @WebResult(name = "getSitesOfService", targetNamespace = "")
    @RequestWrapper(localName = "getSitesOfService", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetSitesOfService")
    @WebMethod
    @ResponseWrapper(localName = "getSitesOfServiceResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetSitesOfServiceResponse")
    public com.velos.services.SitesOfService getSitesOfService() throws OperationException_Exception;

    @WebResult(name = "response", targetNamespace = "")
    @RequestWrapper(localName = "createMEventStatus", targetNamespace = "http://velos.com/services/", className = "com.velos.services.CreateMEventStatus")
    @WebMethod
    @ResponseWrapper(localName = "createMEventStatusResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.CreateMEventStatusResponse")
    public com.velos.services.ResponseHolder createMEventStatus(
        @WebParam(name = "MEventStatuses", targetNamespace = "")
        com.velos.services.MEventStatuses mEventStatuses
    ) throws OperationException_Exception;

    @WebResult(name = "addScheduleEventStatus", targetNamespace = "")
    @RequestWrapper(localName = "addScheduleEventStatus", targetNamespace = "http://velos.com/services/", className = "com.velos.services.AddScheduleEventStatus")
    @WebMethod
    @ResponseWrapper(localName = "addScheduleEventStatusResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.AddScheduleEventStatusResponse")
    public com.velos.services.ResponseHolder addScheduleEventStatus(
        @WebParam(name = "EventIdentifier", targetNamespace = "")
        com.velos.services.EventIdentifier eventIdentifier,
        @WebParam(name = "EventStatus", targetNamespace = "")
        com.velos.services.EventStatus eventStatus
    ) throws OperationException_Exception;

    @WebResult(name = "addUnscheduledEvents", targetNamespace = "")
    @RequestWrapper(localName = "addUnscheduledEvents", targetNamespace = "http://velos.com/services/", className = "com.velos.services.AddUnscheduledEvents")
    @WebMethod
    @ResponseWrapper(localName = "addUnscheduledEventsResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.AddUnscheduledEventsResponse")
    public com.velos.services.ResponseHolder addUnscheduledEvents(
        @WebParam(name = "ScheduleIdentifier", targetNamespace = "")
        com.velos.services.PatientProtocolIdentifier scheduleIdentifier,
        @WebParam(name = "StudyIdentifier", targetNamespace = "")
        com.velos.services.StudyIdentifier studyIdentifier,
        @WebParam(name = "PatientIdentifier", targetNamespace = "")
        com.velos.services.PatientIdentifier patientIdentifier,
        @WebParam(name = "Events", targetNamespace = "")
        com.velos.services.MultipleEvents events,
        @WebParam(name = "VisitIdentifier", targetNamespace = "")
        com.velos.services.VisitIdentifier visitIdentifier
    ) throws OperationException_Exception;

    @WebResult(name = "PatientSchedule", targetNamespace = "")
    @RequestWrapper(localName = "getPatientSchedule", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetPatientSchedule")
    @WebMethod
    @ResponseWrapper(localName = "getPatientScheduleResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetPatientScheduleResponse")
    public com.velos.services.PatientSchedule getPatientSchedule(
        @WebParam(name = "ScheduleIdentifier", targetNamespace = "")
        com.velos.services.PatientProtocolIdentifier scheduleIdentifier
    ) throws OperationException_Exception;

    @WebResult(name = "updatemeventstatus", targetNamespace = "")
    @RequestWrapper(localName = "updateMEventStatus", targetNamespace = "http://velos.com/services/", className = "com.velos.services.UpdateMEventStatus")
    @WebMethod
    @ResponseWrapper(localName = "updateMEventStatusResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.UpdateMEventStatusResponse")
    public com.velos.services.ResponseHolder updateMEventStatus(
        @WebParam(name = "ScheduleEventStatuses", targetNamespace = "")
        com.velos.services.ScheduleEventStatuses scheduleEventStatuses
    ) throws OperationException_Exception;

    @WebResult(name = "PatientSchedules", targetNamespace = "")
    @RequestWrapper(localName = "getPatientScheduleList", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetPatientScheduleList")
    @WebMethod
    @ResponseWrapper(localName = "getPatientScheduleListResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetPatientScheduleListResponse")
    public com.velos.services.PatientSchedules getPatientScheduleList(
        @WebParam(name = "PatientIdentifier", targetNamespace = "")
        com.velos.services.PatientIdentifier patientIdentifier,
        @WebParam(name = "StudyIdentifier", targetNamespace = "")
        com.velos.services.StudyIdentifier studyIdentifier
    ) throws OperationException_Exception;

    @WebResult(name = "addUnscheduledAdminEvents", targetNamespace = "")
    @RequestWrapper(localName = "addUnscheduledAdminEvents", targetNamespace = "http://velos.com/services/", className = "com.velos.services.AddUnscheduledAdminEvents")
    @WebMethod
    @ResponseWrapper(localName = "addUnscheduledAdminEventsResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.AddUnscheduledAdminEventsResponse")
    public com.velos.services.ResponseHolder addUnscheduledAdminEvents(
        @WebParam(name = "ScheduleIdentifier", targetNamespace = "")
        com.velos.services.CalendarIdentifier scheduleIdentifier,
        @WebParam(name = "Events", targetNamespace = "")
        com.velos.services.MultipleEvents events,
        @WebParam(name = "VisitIdentifier", targetNamespace = "")
        com.velos.services.VisitIdentifier visitIdentifier
    ) throws OperationException_Exception;

    @WebResult(name = "addmpatientschedules", targetNamespace = "")
    @RequestWrapper(localName = "addMPatientSchedules", targetNamespace = "http://velos.com/services/", className = "com.velos.services.AddMPatientSchedules")
    @WebMethod
    @ResponseWrapper(localName = "addMPatientSchedulesResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.AddMPatientSchedulesResponse")
    public com.velos.services.MPatientScheduleList addMPatientSchedules(
        @WebParam(name = "PatientSchedules", targetNamespace = "")
        com.velos.services.MPatientSchedules patientSchedules
    ) throws OperationException_Exception;
}
