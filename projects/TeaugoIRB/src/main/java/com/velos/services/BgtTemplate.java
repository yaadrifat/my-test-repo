
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for bgtTemplate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="bgtTemplate">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="budgetIdentifier" type="{http://velos.com/services/}budgetIdentifier" minOccurs="0"/>
 *         &lt;element name="budgetInfo" type="{http://velos.com/services/}budgetPojo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "bgtTemplate", propOrder = {
    "budgetIdentifier",
    "budgetInfo"
})
public class BgtTemplate {

    protected BudgetIdentifier budgetIdentifier;
    protected BudgetPojo budgetInfo;

    /**
     * Gets the value of the budgetIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link BudgetIdentifier }
     *     
     */
    public BudgetIdentifier getBudgetIdentifier() {
        return budgetIdentifier;
    }

    /**
     * Sets the value of the budgetIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link BudgetIdentifier }
     *     
     */
    public void setBudgetIdentifier(BudgetIdentifier value) {
        this.budgetIdentifier = value;
    }

    /**
     * Gets the value of the budgetInfo property.
     * 
     * @return
     *     possible object is
     *     {@link BudgetPojo }
     *     
     */
    public BudgetPojo getBudgetInfo() {
        return budgetInfo;
    }

    /**
     * Sets the value of the budgetInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BudgetPojo }
     *     
     */
    public void setBudgetInfo(BudgetPojo value) {
        this.budgetInfo = value;
    }

}
