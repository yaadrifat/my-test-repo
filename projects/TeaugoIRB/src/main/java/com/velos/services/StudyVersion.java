
package com.velos.services;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for studyVersion complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="studyVersion">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ORIG_STUDY" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="STUDYVER_CATEGORY" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="STUDYVER_DATE" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="STUDYVER_NOTES" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="STUDYVER_STATUS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="STUDYVER_TYPE" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="studyAppendix" type="{http://velos.com/services/}studyAppendix" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="studyStatusHistory" type="{http://velos.com/services/}studyStatusHistory" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="studyVerIdentifier" type="{http://velos.com/services/}studyVersionIdentifier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "studyVersion", propOrder = {
    "origstudy",
    "studyvercategory",
    "studyverdate",
    "studyvernotes",
    "studyverstatus",
    "studyvertype",
    "studyAppendix",
    "studyStatusHistory",
    "studyVerIdentifier"
})
public class StudyVersion {

    @XmlElement(name = "ORIG_STUDY")
    protected Integer origstudy;
    @XmlElement(name = "STUDYVER_CATEGORY")
    protected Code studyvercategory;
    @XmlElement(name = "STUDYVER_DATE")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar studyverdate;
    @XmlElement(name = "STUDYVER_NOTES")
    protected String studyvernotes;
    @XmlElement(name = "STUDYVER_STATUS")
    protected String studyverstatus;
    @XmlElement(name = "STUDYVER_TYPE")
    protected Code studyvertype;
    @XmlElement(nillable = true)
    protected List<StudyAppendix> studyAppendix;
    @XmlElement(nillable = true)
    protected List<StudyStatusHistory> studyStatusHistory;
    protected StudyVersionIdentifier studyVerIdentifier;

    /**
     * Gets the value of the origstudy property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getORIGSTUDY() {
        return origstudy;
    }

    /**
     * Sets the value of the origstudy property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setORIGSTUDY(Integer value) {
        this.origstudy = value;
    }

    /**
     * Gets the value of the studyvercategory property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getSTUDYVERCATEGORY() {
        return studyvercategory;
    }

    /**
     * Sets the value of the studyvercategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setSTUDYVERCATEGORY(Code value) {
        this.studyvercategory = value;
    }

    /**
     * Gets the value of the studyverdate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSTUDYVERDATE() {
        return studyverdate;
    }

    /**
     * Sets the value of the studyverdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSTUDYVERDATE(XMLGregorianCalendar value) {
        this.studyverdate = value;
    }

    /**
     * Gets the value of the studyvernotes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTUDYVERNOTES() {
        return studyvernotes;
    }

    /**
     * Sets the value of the studyvernotes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTUDYVERNOTES(String value) {
        this.studyvernotes = value;
    }

    /**
     * Gets the value of the studyverstatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTUDYVERSTATUS() {
        return studyverstatus;
    }

    /**
     * Sets the value of the studyverstatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTUDYVERSTATUS(String value) {
        this.studyverstatus = value;
    }

    /**
     * Gets the value of the studyvertype property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getSTUDYVERTYPE() {
        return studyvertype;
    }

    /**
     * Sets the value of the studyvertype property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setSTUDYVERTYPE(Code value) {
        this.studyvertype = value;
    }

    /**
     * Gets the value of the studyAppendix property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the studyAppendix property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStudyAppendix().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StudyAppendix }
     * 
     * 
     */
    public List<StudyAppendix> getStudyAppendix() {
        if (studyAppendix == null) {
            studyAppendix = new ArrayList<StudyAppendix>();
        }
        return this.studyAppendix;
    }

    /**
     * Gets the value of the studyStatusHistory property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the studyStatusHistory property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStudyStatusHistory().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StudyStatusHistory }
     * 
     * 
     */
    public List<StudyStatusHistory> getStudyStatusHistory() {
        if (studyStatusHistory == null) {
            studyStatusHistory = new ArrayList<StudyStatusHistory>();
        }
        return this.studyStatusHistory;
    }

    /**
     * Gets the value of the studyVerIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link StudyVersionIdentifier }
     *     
     */
    public StudyVersionIdentifier getStudyVerIdentifier() {
        return studyVerIdentifier;
    }

    /**
     * Sets the value of the studyVerIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyVersionIdentifier }
     *     
     */
    public void setStudyVerIdentifier(StudyVersionIdentifier value) {
        this.studyVerIdentifier = value;
    }

}
