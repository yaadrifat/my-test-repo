
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getCodeTypesResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getCodeTypesResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CodeTypes" type="{http://velos.com/services/}codeTypes" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getCodeTypesResponse", propOrder = {
    "codeTypes"
})
public class GetCodeTypesResponse {

    @XmlElement(name = "CodeTypes")
    protected CodeTypes codeTypes;

    /**
     * Gets the value of the codeTypes property.
     * 
     * @return
     *     possible object is
     *     {@link CodeTypes }
     *     
     */
    public CodeTypes getCodeTypes() {
        return codeTypes;
    }

    /**
     * Sets the value of the codeTypes property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeTypes }
     *     
     */
    public void setCodeTypes(CodeTypes value) {
        this.codeTypes = value;
    }

}
