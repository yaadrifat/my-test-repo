
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for studyCalendars complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="studyCalendars">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="calendarIdentifier" type="{http://velos.com/services/}calendarIdentifier" minOccurs="0"/>
 *         &lt;element name="calendarName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="calendarStatus" type="{http://velos.com/services/}code" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "studyCalendars", propOrder = {
    "calendarIdentifier",
    "calendarName",
    "calendarStatus"
})
public class StudyCalendars
    extends ServiceObject
{

    protected CalendarIdentifier calendarIdentifier;
    protected String calendarName;
    protected Code calendarStatus;

    /**
     * Gets the value of the calendarIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link CalendarIdentifier }
     *     
     */
    public CalendarIdentifier getCalendarIdentifier() {
        return calendarIdentifier;
    }

    /**
     * Sets the value of the calendarIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link CalendarIdentifier }
     *     
     */
    public void setCalendarIdentifier(CalendarIdentifier value) {
        this.calendarIdentifier = value;
    }

    /**
     * Gets the value of the calendarName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCalendarName() {
        return calendarName;
    }

    /**
     * Sets the value of the calendarName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCalendarName(String value) {
        this.calendarName = value;
    }

    /**
     * Gets the value of the calendarStatus property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getCalendarStatus() {
        return calendarStatus;
    }

    /**
     * Sets the value of the calendarStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setCalendarStatus(Code value) {
        this.calendarStatus = value;
    }

}
