
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getStudyCalendarResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getStudyCalendarResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StudyCalendar" type="{http://velos.com/services/}studyCalendar" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getStudyCalendarResponse", propOrder = {
    "studyCalendar"
})
public class GetStudyCalendarResponse {

    @XmlElement(name = "StudyCalendar")
    protected StudyCalendar studyCalendar;

    /**
     * Gets the value of the studyCalendar property.
     * 
     * @return
     *     possible object is
     *     {@link StudyCalendar }
     *     
     */
    public StudyCalendar getStudyCalendar() {
        return studyCalendar;
    }

    /**
     * Sets the value of the studyCalendar property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyCalendar }
     *     
     */
    public void setStudyCalendar(StudyCalendar value) {
        this.studyCalendar = value;
    }

}
