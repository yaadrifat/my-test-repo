
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updateStudyFormResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updateStudyFormResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FormResponseIdentifier" type="{http://velos.com/services/}studyFormResponseIdentifier" minOccurs="0"/>
 *         &lt;element name="StudyFormResponse" type="{http://velos.com/services/}studyFormResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateStudyFormResponse", propOrder = {
    "formResponseIdentifier",
    "studyFormResponse"
})
public class UpdateStudyFormResponse {

    @XmlElement(name = "FormResponseIdentifier")
    protected StudyFormResponseIdentifier formResponseIdentifier;
    @XmlElement(name = "StudyFormResponse")
    protected StudyFormResponse studyFormResponse;

    /**
     * Gets the value of the formResponseIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link StudyFormResponseIdentifier }
     *     
     */
    public StudyFormResponseIdentifier getFormResponseIdentifier() {
        return formResponseIdentifier;
    }

    /**
     * Sets the value of the formResponseIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyFormResponseIdentifier }
     *     
     */
    public void setFormResponseIdentifier(StudyFormResponseIdentifier value) {
        this.formResponseIdentifier = value;
    }

    /**
     * Gets the value of the studyFormResponse property.
     * 
     * @return
     *     possible object is
     *     {@link StudyFormResponse }
     *     
     */
    public StudyFormResponse getStudyFormResponse() {
        return studyFormResponse;
    }

    /**
     * Sets the value of the studyFormResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyFormResponse }
     *     
     */
    public void setStudyFormResponse(StudyFormResponse value) {
        this.studyFormResponse = value;
    }

}
