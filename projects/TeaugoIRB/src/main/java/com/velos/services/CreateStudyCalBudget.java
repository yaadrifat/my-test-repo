
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createStudyCalBudget complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createStudyCalBudget">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Budget" type="{http://velos.com/services/}budgetDetail" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createStudyCalBudget", propOrder = {
    "budget"
})
public class CreateStudyCalBudget {

    @XmlElement(name = "Budget")
    protected BudgetDetail budget;

    /**
     * Gets the value of the budget property.
     * 
     * @return
     *     possible object is
     *     {@link BudgetDetail }
     *     
     */
    public BudgetDetail getBudget() {
        return budget;
    }

    /**
     * Sets the value of the budget property.
     * 
     * @param value
     *     allowed object is
     *     {@link BudgetDetail }
     *     
     */
    public void setBudget(BudgetDetail value) {
        this.budget = value;
    }

}
