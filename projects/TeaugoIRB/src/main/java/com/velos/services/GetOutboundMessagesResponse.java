
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getOutboundMessagesResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getOutboundMessagesResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ChangesList" type="{http://velos.com/services/}changesList" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getOutboundMessagesResponse", propOrder = {
    "changesList"
})
public class GetOutboundMessagesResponse {

    @XmlElement(name = "ChangesList")
    protected ChangesList changesList;

    /**
     * Gets the value of the changesList property.
     * 
     * @return
     *     possible object is
     *     {@link ChangesList }
     *     
     */
    public ChangesList getChangesList() {
        return changesList;
    }

    /**
     * Sets the value of the changesList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangesList }
     *     
     */
    public void setChangesList(ChangesList value) {
        this.changesList = value;
    }

}
