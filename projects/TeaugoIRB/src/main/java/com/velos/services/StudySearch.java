
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for studySearch complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="studySearch">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="currentStudyStatus" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="displayedStudyStatus" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="division" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="moreStudyDetails" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="msdCodeSubType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pageNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="pageSize" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="phase" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="principalInvestigator" type="{http://velos.com/services/}userIdentifier" minOccurs="0"/>
 *         &lt;element name="sortBy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sortOrder" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="studyNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="studyOrganization" type="{http://velos.com/services/}organizationIdentifier" minOccurs="0"/>
 *         &lt;element name="studyTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="therapeuticArea" type="{http://velos.com/services/}code" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "studySearch", propOrder = {
    "currentStudyStatus",
    "displayedStudyStatus",
    "division",
    "moreStudyDetails",
    "msdCodeSubType",
    "pageNumber",
    "pageSize",
    "phase",
    "principalInvestigator",
    "sortBy",
    "sortOrder",
    "studyNumber",
    "studyOrganization",
    "studyTitle",
    "therapeuticArea"
})
public class StudySearch
    extends ServiceObject
{

    protected Code currentStudyStatus;
    protected Code displayedStudyStatus;
    protected Code division;
    protected String moreStudyDetails;
    protected String msdCodeSubType;
    protected Integer pageNumber;
    protected Integer pageSize;
    protected Code phase;
    protected UserIdentifier principalInvestigator;
    protected String sortBy;
    protected String sortOrder;
    protected String studyNumber;
    protected OrganizationIdentifier studyOrganization;
    protected String studyTitle;
    protected Code therapeuticArea;

    /**
     * Gets the value of the currentStudyStatus property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getCurrentStudyStatus() {
        return currentStudyStatus;
    }

    /**
     * Sets the value of the currentStudyStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setCurrentStudyStatus(Code value) {
        this.currentStudyStatus = value;
    }

    /**
     * Gets the value of the displayedStudyStatus property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getDisplayedStudyStatus() {
        return displayedStudyStatus;
    }

    /**
     * Sets the value of the displayedStudyStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setDisplayedStudyStatus(Code value) {
        this.displayedStudyStatus = value;
    }

    /**
     * Gets the value of the division property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getDivision() {
        return division;
    }

    /**
     * Sets the value of the division property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setDivision(Code value) {
        this.division = value;
    }

    /**
     * Gets the value of the moreStudyDetails property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMoreStudyDetails() {
        return moreStudyDetails;
    }

    /**
     * Sets the value of the moreStudyDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMoreStudyDetails(String value) {
        this.moreStudyDetails = value;
    }

    /**
     * Gets the value of the msdCodeSubType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsdCodeSubType() {
        return msdCodeSubType;
    }

    /**
     * Sets the value of the msdCodeSubType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsdCodeSubType(String value) {
        this.msdCodeSubType = value;
    }

    /**
     * Gets the value of the pageNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPageNumber() {
        return pageNumber;
    }

    /**
     * Sets the value of the pageNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPageNumber(Integer value) {
        this.pageNumber = value;
    }

    /**
     * Gets the value of the pageSize property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPageSize() {
        return pageSize;
    }

    /**
     * Sets the value of the pageSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPageSize(Integer value) {
        this.pageSize = value;
    }

    /**
     * Gets the value of the phase property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getPhase() {
        return phase;
    }

    /**
     * Sets the value of the phase property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setPhase(Code value) {
        this.phase = value;
    }

    /**
     * Gets the value of the principalInvestigator property.
     * 
     * @return
     *     possible object is
     *     {@link UserIdentifier }
     *     
     */
    public UserIdentifier getPrincipalInvestigator() {
        return principalInvestigator;
    }

    /**
     * Sets the value of the principalInvestigator property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserIdentifier }
     *     
     */
    public void setPrincipalInvestigator(UserIdentifier value) {
        this.principalInvestigator = value;
    }

    /**
     * Gets the value of the sortBy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSortBy() {
        return sortBy;
    }

    /**
     * Sets the value of the sortBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSortBy(String value) {
        this.sortBy = value;
    }

    /**
     * Gets the value of the sortOrder property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSortOrder() {
        return sortOrder;
    }

    /**
     * Sets the value of the sortOrder property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSortOrder(String value) {
        this.sortOrder = value;
    }

    /**
     * Gets the value of the studyNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStudyNumber() {
        return studyNumber;
    }

    /**
     * Sets the value of the studyNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStudyNumber(String value) {
        this.studyNumber = value;
    }

    /**
     * Gets the value of the studyOrganization property.
     * 
     * @return
     *     possible object is
     *     {@link OrganizationIdentifier }
     *     
     */
    public OrganizationIdentifier getStudyOrganization() {
        return studyOrganization;
    }

    /**
     * Sets the value of the studyOrganization property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganizationIdentifier }
     *     
     */
    public void setStudyOrganization(OrganizationIdentifier value) {
        this.studyOrganization = value;
    }

    /**
     * Gets the value of the studyTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStudyTitle() {
        return studyTitle;
    }

    /**
     * Sets the value of the studyTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStudyTitle(String value) {
        this.studyTitle = value;
    }

    /**
     * Gets the value of the therapeuticArea property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getTherapeuticArea() {
        return therapeuticArea;
    }

    /**
     * Sets the value of the therapeuticArea property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setTherapeuticArea(Code value) {
        this.therapeuticArea = value;
    }

}
