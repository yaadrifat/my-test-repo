
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for milestonee complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="milestonee">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="bgtCalNameIdentifier" type="{http://velos.com/services/}bgtCalNameIdentifier" minOccurs="0"/>
 *         &lt;element name="bgtSectionNameIdentifier" type="{http://velos.com/services/}bgtSectionNameIdentifier" minOccurs="0"/>
 *         &lt;element name="budgetIdentifier" type="{http://velos.com/services/}budgetIdentifier" minOccurs="0"/>
 *         &lt;element name="calendarNameIdent" type="{http://velos.com/services/}calendarNameIdentifier" minOccurs="0"/>
 *         &lt;element name="eventNameIdent" type="{http://velos.com/services/}eventNameIdentfier" minOccurs="0"/>
 *         &lt;element name="lineItemNameIdentifier" type="{http://velos.com/services/}lineItemNameIdentifier" minOccurs="0"/>
 *         &lt;element name="MILESTONE_AMOUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MILESTONE_COUNT" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="MILESTONE_DATE_FROM" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="MILESTONE_DATE_TO" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="MILESTONE_DELFLAG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MILESTONE_DESCRIPTION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MILESTONE_EVENTSTATUS" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="MILESTONE_HOLDBACK" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MILESTONE_LIMIT" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="MILESTONE_PATSTUDY_STAT" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="MILESTONE_PAYFOR" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="MILESTONE_PAYTYPE" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="MILESTONE_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="milestoneIdent" type="{http://velos.com/services/}milestoneIdentifier" minOccurs="0"/>
 *         &lt;element name="PK_CODELST_MILESTONE_STAT" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="PK_CODELST_RULE" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="visitNameIdent" type="{http://velos.com/services/}visitNameIdentifier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "milestonee", propOrder = {
    "bgtCalNameIdentifier",
    "bgtSectionNameIdentifier",
    "budgetIdentifier",
    "calendarNameIdent",
    "eventNameIdent",
    "lineItemNameIdentifier",
    "milestoneamount",
    "milestonecount",
    "milestonedatefrom",
    "milestonedateto",
    "milestonedelflag",
    "milestonedescription",
    "milestoneeventstatus",
    "milestoneholdback",
    "milestonelimit",
    "milestonepatstudystat",
    "milestonepayfor",
    "milestonepaytype",
    "milestonetype",
    "milestoneIdent",
    "pkcodelstmilestonestat",
    "pkcodelstrule",
    "visitNameIdent"
})
public class Milestonee {

    protected BgtCalNameIdentifier bgtCalNameIdentifier;
    protected BgtSectionNameIdentifier bgtSectionNameIdentifier;
    protected BudgetIdentifier budgetIdentifier;
    protected CalendarNameIdentifier calendarNameIdent;
    protected EventNameIdentfier eventNameIdent;
    protected LineItemNameIdentifier lineItemNameIdentifier;
    @XmlElement(name = "MILESTONE_AMOUNT")
    protected String milestoneamount;
    @XmlElement(name = "MILESTONE_COUNT")
    protected Integer milestonecount;
    @XmlElement(name = "MILESTONE_DATE_FROM")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar milestonedatefrom;
    @XmlElement(name = "MILESTONE_DATE_TO")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar milestonedateto;
    @XmlElement(name = "MILESTONE_DELFLAG")
    protected String milestonedelflag;
    @XmlElement(name = "MILESTONE_DESCRIPTION")
    protected String milestonedescription;
    @XmlElement(name = "MILESTONE_EVENTSTATUS")
    protected Code milestoneeventstatus;
    @XmlElement(name = "MILESTONE_HOLDBACK")
    protected String milestoneholdback;
    @XmlElement(name = "MILESTONE_LIMIT")
    protected Integer milestonelimit;
    @XmlElement(name = "MILESTONE_PATSTUDY_STAT")
    protected Code milestonepatstudystat;
    @XmlElement(name = "MILESTONE_PAYFOR")
    protected Code milestonepayfor;
    @XmlElement(name = "MILESTONE_PAYTYPE")
    protected Code milestonepaytype;
    @XmlElement(name = "MILESTONE_TYPE")
    protected String milestonetype;
    protected MilestoneIdentifier milestoneIdent;
    @XmlElement(name = "PK_CODELST_MILESTONE_STAT")
    protected Code pkcodelstmilestonestat;
    @XmlElement(name = "PK_CODELST_RULE")
    protected Code pkcodelstrule;
    protected VisitNameIdentifier visitNameIdent;

    /**
     * Gets the value of the bgtCalNameIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link BgtCalNameIdentifier }
     *     
     */
    public BgtCalNameIdentifier getBgtCalNameIdentifier() {
        return bgtCalNameIdentifier;
    }

    /**
     * Sets the value of the bgtCalNameIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link BgtCalNameIdentifier }
     *     
     */
    public void setBgtCalNameIdentifier(BgtCalNameIdentifier value) {
        this.bgtCalNameIdentifier = value;
    }

    /**
     * Gets the value of the bgtSectionNameIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link BgtSectionNameIdentifier }
     *     
     */
    public BgtSectionNameIdentifier getBgtSectionNameIdentifier() {
        return bgtSectionNameIdentifier;
    }

    /**
     * Sets the value of the bgtSectionNameIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link BgtSectionNameIdentifier }
     *     
     */
    public void setBgtSectionNameIdentifier(BgtSectionNameIdentifier value) {
        this.bgtSectionNameIdentifier = value;
    }

    /**
     * Gets the value of the budgetIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link BudgetIdentifier }
     *     
     */
    public BudgetIdentifier getBudgetIdentifier() {
        return budgetIdentifier;
    }

    /**
     * Sets the value of the budgetIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link BudgetIdentifier }
     *     
     */
    public void setBudgetIdentifier(BudgetIdentifier value) {
        this.budgetIdentifier = value;
    }

    /**
     * Gets the value of the calendarNameIdent property.
     * 
     * @return
     *     possible object is
     *     {@link CalendarNameIdentifier }
     *     
     */
    public CalendarNameIdentifier getCalendarNameIdent() {
        return calendarNameIdent;
    }

    /**
     * Sets the value of the calendarNameIdent property.
     * 
     * @param value
     *     allowed object is
     *     {@link CalendarNameIdentifier }
     *     
     */
    public void setCalendarNameIdent(CalendarNameIdentifier value) {
        this.calendarNameIdent = value;
    }

    /**
     * Gets the value of the eventNameIdent property.
     * 
     * @return
     *     possible object is
     *     {@link EventNameIdentfier }
     *     
     */
    public EventNameIdentfier getEventNameIdent() {
        return eventNameIdent;
    }

    /**
     * Sets the value of the eventNameIdent property.
     * 
     * @param value
     *     allowed object is
     *     {@link EventNameIdentfier }
     *     
     */
    public void setEventNameIdent(EventNameIdentfier value) {
        this.eventNameIdent = value;
    }

    /**
     * Gets the value of the lineItemNameIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link LineItemNameIdentifier }
     *     
     */
    public LineItemNameIdentifier getLineItemNameIdentifier() {
        return lineItemNameIdentifier;
    }

    /**
     * Sets the value of the lineItemNameIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link LineItemNameIdentifier }
     *     
     */
    public void setLineItemNameIdentifier(LineItemNameIdentifier value) {
        this.lineItemNameIdentifier = value;
    }

    /**
     * Gets the value of the milestoneamount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMILESTONEAMOUNT() {
        return milestoneamount;
    }

    /**
     * Sets the value of the milestoneamount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMILESTONEAMOUNT(String value) {
        this.milestoneamount = value;
    }

    /**
     * Gets the value of the milestonecount property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMILESTONECOUNT() {
        return milestonecount;
    }

    /**
     * Sets the value of the milestonecount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMILESTONECOUNT(Integer value) {
        this.milestonecount = value;
    }

    /**
     * Gets the value of the milestonedatefrom property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getMILESTONEDATEFROM() {
        return milestonedatefrom;
    }

    /**
     * Sets the value of the milestonedatefrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMILESTONEDATEFROM(XMLGregorianCalendar value) {
        this.milestonedatefrom = value;
    }

    /**
     * Gets the value of the milestonedateto property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getMILESTONEDATETO() {
        return milestonedateto;
    }

    /**
     * Sets the value of the milestonedateto property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMILESTONEDATETO(XMLGregorianCalendar value) {
        this.milestonedateto = value;
    }

    /**
     * Gets the value of the milestonedelflag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMILESTONEDELFLAG() {
        return milestonedelflag;
    }

    /**
     * Sets the value of the milestonedelflag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMILESTONEDELFLAG(String value) {
        this.milestonedelflag = value;
    }

    /**
     * Gets the value of the milestonedescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMILESTONEDESCRIPTION() {
        return milestonedescription;
    }

    /**
     * Sets the value of the milestonedescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMILESTONEDESCRIPTION(String value) {
        this.milestonedescription = value;
    }

    /**
     * Gets the value of the milestoneeventstatus property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getMILESTONEEVENTSTATUS() {
        return milestoneeventstatus;
    }

    /**
     * Sets the value of the milestoneeventstatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setMILESTONEEVENTSTATUS(Code value) {
        this.milestoneeventstatus = value;
    }

    /**
     * Gets the value of the milestoneholdback property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMILESTONEHOLDBACK() {
        return milestoneholdback;
    }

    /**
     * Sets the value of the milestoneholdback property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMILESTONEHOLDBACK(String value) {
        this.milestoneholdback = value;
    }

    /**
     * Gets the value of the milestonelimit property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMILESTONELIMIT() {
        return milestonelimit;
    }

    /**
     * Sets the value of the milestonelimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMILESTONELIMIT(Integer value) {
        this.milestonelimit = value;
    }

    /**
     * Gets the value of the milestonepatstudystat property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getMILESTONEPATSTUDYSTAT() {
        return milestonepatstudystat;
    }

    /**
     * Sets the value of the milestonepatstudystat property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setMILESTONEPATSTUDYSTAT(Code value) {
        this.milestonepatstudystat = value;
    }

    /**
     * Gets the value of the milestonepayfor property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getMILESTONEPAYFOR() {
        return milestonepayfor;
    }

    /**
     * Sets the value of the milestonepayfor property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setMILESTONEPAYFOR(Code value) {
        this.milestonepayfor = value;
    }

    /**
     * Gets the value of the milestonepaytype property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getMILESTONEPAYTYPE() {
        return milestonepaytype;
    }

    /**
     * Sets the value of the milestonepaytype property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setMILESTONEPAYTYPE(Code value) {
        this.milestonepaytype = value;
    }

    /**
     * Gets the value of the milestonetype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMILESTONETYPE() {
        return milestonetype;
    }

    /**
     * Sets the value of the milestonetype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMILESTONETYPE(String value) {
        this.milestonetype = value;
    }

    /**
     * Gets the value of the milestoneIdent property.
     * 
     * @return
     *     possible object is
     *     {@link MilestoneIdentifier }
     *     
     */
    public MilestoneIdentifier getMilestoneIdent() {
        return milestoneIdent;
    }

    /**
     * Sets the value of the milestoneIdent property.
     * 
     * @param value
     *     allowed object is
     *     {@link MilestoneIdentifier }
     *     
     */
    public void setMilestoneIdent(MilestoneIdentifier value) {
        this.milestoneIdent = value;
    }

    /**
     * Gets the value of the pkcodelstmilestonestat property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getPKCODELSTMILESTONESTAT() {
        return pkcodelstmilestonestat;
    }

    /**
     * Sets the value of the pkcodelstmilestonestat property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setPKCODELSTMILESTONESTAT(Code value) {
        this.pkcodelstmilestonestat = value;
    }

    /**
     * Gets the value of the pkcodelstrule property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getPKCODELSTRULE() {
        return pkcodelstrule;
    }

    /**
     * Sets the value of the pkcodelstrule property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setPKCODELSTRULE(Code value) {
        this.pkcodelstrule = value;
    }

    /**
     * Gets the value of the visitNameIdent property.
     * 
     * @return
     *     possible object is
     *     {@link VisitNameIdentifier }
     *     
     */
    public VisitNameIdentifier getVisitNameIdent() {
        return visitNameIdent;
    }

    /**
     * Sets the value of the visitNameIdent property.
     * 
     * @param value
     *     allowed object is
     *     {@link VisitNameIdentifier }
     *     
     */
    public void setVisitNameIdent(VisitNameIdentifier value) {
        this.visitNameIdent = value;
    }

}
