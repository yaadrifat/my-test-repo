
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import com.velos.services.CalendarIdentifier;


/**
 * <p>Java class for calendarNameIdentifier complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="calendarNameIdentifier">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}calendarIdentifier">
 *       &lt;sequence>
 *         &lt;element name="calendarAssocTo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="calendarName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "calendarNameIdentifier", propOrder = {
    "calendarAssocTo",
    "calendarName"
})
public class CalendarNameIdentifier
    extends CalendarIdentifier
{

    protected String calendarAssocTo;
    protected String calendarName;

    /**
     * Gets the value of the calendarAssocTo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCalendarAssocTo() {
        return calendarAssocTo;
    }

    /**
     * Sets the value of the calendarAssocTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCalendarAssocTo(String value) {
        this.calendarAssocTo = value;
    }

    /**
     * Gets the value of the calendarName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCalendarName() {
        return calendarName;
    }

    /**
     * Sets the value of the calendarName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCalendarName(String value) {
        this.calendarName = value;
    }

}
