package com.velos.services;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 2.2.9
 * Fri Jan 16 16:33:50 PST 2015
 * Generated source version: 2.2.9
 * 
 */
 
@WebService(targetNamespace = "http://velos.com/services/", name = "PatientStudySEI")
@XmlSeeAlso({ObjectFactory.class})
public interface PatientStudySEI {

    @WebResult(name = "PatientStudy", targetNamespace = "")
    @RequestWrapper(localName = "getPatientStudyInfo", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetPatientStudyInfo")
    @WebMethod
    @ResponseWrapper(localName = "getPatientStudyInfoResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetPatientStudyInfoResponse")
    public com.velos.services.PatientProtocolIdentifier getPatientStudyInfo(
        @WebParam(name = "patProtId", targetNamespace = "")
        java.lang.Integer patProtId
    ) throws OperationException_Exception;

    @WebResult(name = "PatientStudy", targetNamespace = "")
    @RequestWrapper(localName = "getPatientStudies", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetPatientStudies")
    @WebMethod
    @ResponseWrapper(localName = "getPatientStudiesResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetPatientStudiesResponse")
    public java.util.List<com.velos.services.PatientStudy> getPatientStudies(
        @WebParam(name = "PatientIdentifier", targetNamespace = "")
        com.velos.services.PatientIdentifier patientIdentifier
    ) throws OperationException_Exception;
}
