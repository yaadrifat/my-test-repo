
/*
 * 
 */

package com.velos.services;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;

/**
 * This class was generated by Apache CXF 2.2.9
 * Fri Jan 16 16:33:48 PST 2015
 * Generated source version: 2.2.9
 * 
 */


@WebServiceClient(name = "LibraryService", 
                  wsdlLocation = "http://50.197.161.40:80/webservices/libraryservice?wsdl",
                  targetNamespace = "http://velos.com/services/") 
public class LibraryService extends Service {

    public final static URL WSDL_LOCATION;
    public final static QName SERVICE = new QName("http://velos.com/services/", "LibraryService");
    public final static QName LibraryWSPort = new QName("http://velos.com/services/", "LibraryWSPort");
    static {
        URL url = null;
        try {
            url = new URL("http://50.197.161.40:80/webservices/libraryservice?wsdl");
        } catch (MalformedURLException e) {
            System.err.println("Can not initialize the default wsdl from http://50.197.161.40:80/webservices/libraryservice?wsdl");
            // e.printStackTrace();
        }
        WSDL_LOCATION = url;
    }

    public LibraryService(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public LibraryService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public LibraryService() {
        super(WSDL_LOCATION, SERVICE);
    }
    

    /**
     * 
     * @return
     *     returns LibrarySEI
     */
    @WebEndpoint(name = "LibraryWSPort")
    public LibrarySEI getLibraryWSPort() {
        return super.getPort(LibraryWSPort, LibrarySEI.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns LibrarySEI
     */
    @WebEndpoint(name = "LibraryWSPort")
    public LibrarySEI getLibraryWSPort(WebServiceFeature... features) {
        return super.getPort(LibraryWSPort, LibrarySEI.class, features);
    }

}
