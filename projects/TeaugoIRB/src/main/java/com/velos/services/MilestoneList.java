
package com.velos.services;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.velos.services.StudyIdentifier;


/**
 * <p>Java class for milestoneList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="milestoneList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="milestone" type="{http://velos.com/services/}milestonee" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="studyIdentifier" type="{http://velos.com/services/}studyIdentifier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "milestoneList", propOrder = {
    "milestone",
    "studyIdentifier"
})
public class MilestoneList {

    @XmlElement(nillable = true)
    protected List<Milestonee> milestone;
    protected StudyIdentifier studyIdentifier;

    /**
     * Gets the value of the milestone property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the milestone property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMilestone().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Milestonee }
     * 
     * 
     */
    public List<Milestonee> getMilestone() {
        if (milestone == null) {
            milestone = new ArrayList<Milestonee>();
        }
        return this.milestone;
    }

    /**
     * Gets the value of the studyIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link StudyIdentifier }
     *     
     */
    public StudyIdentifier getStudyIdentifier() {
        return studyIdentifier;
    }

    /**
     * Sets the value of the studyIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyIdentifier }
     *     
     */
    public void setStudyIdentifier(StudyIdentifier value) {
        this.studyIdentifier = value;
    }

}
