
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updateMEventStatusResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updateMEventStatusResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="updatemeventstatus" type="{http://velos.com/services/}responseHolder" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateMEventStatusResponse", propOrder = {
    "updatemeventstatus"
})
public class UpdateMEventStatusResponse {

    protected ResponseHolder updatemeventstatus;

    /**
     * Gets the value of the updatemeventstatus property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseHolder }
     *     
     */
    public ResponseHolder getUpdatemeventstatus() {
        return updatemeventstatus;
    }

    /**
     * Sets the value of the updatemeventstatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseHolder }
     *     
     */
    public void setUpdatemeventstatus(ResponseHolder value) {
        this.updatemeventstatus = value;
    }

}
