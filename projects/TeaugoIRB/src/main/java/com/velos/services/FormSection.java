
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for formSection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="formSection">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="fields" type="{http://velos.com/services/}formFields" minOccurs="0"/>
 *         &lt;element name="offline" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="repeated" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="secName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="secSequence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sectionIdentifier" type="{http://velos.com/services/}formSectionIdentifier" minOccurs="0"/>
 *         &lt;element name="tabular" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "formSection", propOrder = {
    "fields",
    "offline",
    "repeated",
    "secName",
    "secSequence",
    "sectionIdentifier",
    "tabular"
})
public class FormSection
    extends ServiceObject
{

    protected FormFields fields;
    protected boolean offline;
    protected Integer repeated;
    protected String secName;
    protected String secSequence;
    protected FormSectionIdentifier sectionIdentifier;
    protected boolean tabular;

    /**
     * Gets the value of the fields property.
     * 
     * @return
     *     possible object is
     *     {@link FormFields }
     *     
     */
    public FormFields getFields() {
        return fields;
    }

    /**
     * Sets the value of the fields property.
     * 
     * @param value
     *     allowed object is
     *     {@link FormFields }
     *     
     */
    public void setFields(FormFields value) {
        this.fields = value;
    }

    /**
     * Gets the value of the offline property.
     * 
     */
    public boolean isOffline() {
        return offline;
    }

    /**
     * Sets the value of the offline property.
     * 
     */
    public void setOffline(boolean value) {
        this.offline = value;
    }

    /**
     * Gets the value of the repeated property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRepeated() {
        return repeated;
    }

    /**
     * Sets the value of the repeated property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRepeated(Integer value) {
        this.repeated = value;
    }

    /**
     * Gets the value of the secName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecName() {
        return secName;
    }

    /**
     * Sets the value of the secName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecName(String value) {
        this.secName = value;
    }

    /**
     * Gets the value of the secSequence property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecSequence() {
        return secSequence;
    }

    /**
     * Sets the value of the secSequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecSequence(String value) {
        this.secSequence = value;
    }

    /**
     * Gets the value of the sectionIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link FormSectionIdentifier }
     *     
     */
    public FormSectionIdentifier getSectionIdentifier() {
        return sectionIdentifier;
    }

    /**
     * Sets the value of the sectionIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link FormSectionIdentifier }
     *     
     */
    public void setSectionIdentifier(FormSectionIdentifier value) {
        this.sectionIdentifier = value;
    }

    /**
     * Gets the value of the tabular property.
     * 
     */
    public boolean isTabular() {
        return tabular;
    }

    /**
     * Sets the value of the tabular property.
     * 
     */
    public void setTabular(boolean value) {
        this.tabular = value;
    }

}
