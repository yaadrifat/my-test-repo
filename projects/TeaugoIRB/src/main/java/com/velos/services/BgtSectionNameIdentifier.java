
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import com.velos.services.SimpleIdentifier;


/**
 * <p>Java class for bgtSectionNameIdentifier complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="bgtSectionNameIdentifier">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}simpleIdentifier">
 *       &lt;sequence>
 *         &lt;element name="bgtSectionName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "bgtSectionNameIdentifier", propOrder = {
    "bgtSectionName"
})
public class BgtSectionNameIdentifier
    extends SimpleIdentifier
{

    protected String bgtSectionName;

    /**
     * Gets the value of the bgtSectionName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBgtSectionName() {
        return bgtSectionName;
    }

    /**
     * Sets the value of the bgtSectionName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBgtSectionName(String value) {
        this.bgtSectionName = value;
    }

}
