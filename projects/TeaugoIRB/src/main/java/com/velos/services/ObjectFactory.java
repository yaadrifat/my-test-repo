
package com.velos.services;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.velos.services package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OperationException_QNAME = new QName("http://velos.com/services/", "OperationException");
    private final static QName _GetOutboundMessagesResponse_QNAME = new QName("http://velos.com/services/", "getOutboundMessagesResponse");
    private final static QName _GetOutboundMessages_QNAME = new QName("http://velos.com/services/", "getOutboundMessages");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.velos.services
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetOutboundMessagesResponse }
     * 
     */
    public GetOutboundMessagesResponse createGetOutboundMessagesResponse() {
        return new GetOutboundMessagesResponse();
    }

    /**
     * Create an instance of {@link GetOutboundMessages }
     * 
     */
    public GetOutboundMessages createGetOutboundMessages() {
        return new GetOutboundMessages();
    }

    /**
     * Create an instance of {@link OperationException }
     * 
     */
    public OperationException createOperationException() {
        return new OperationException();
    }

    /**
     * Create an instance of {@link OutboundStudyIdentifier }
     * 
     */
    public OutboundStudyIdentifier createOutboundStudyIdentifier() {
        return new OutboundStudyIdentifier();
    }

    /**
     * Create an instance of {@link StudyIdentifier }
     * 
     */
    public StudyIdentifier createStudyIdentifier() {
        return new StudyIdentifier();
    }

    /**
     * Create an instance of {@link Change }
     * 
     */
    public Change createChange() {
        return new Change();
    }

    /**
     * Create an instance of {@link Issue }
     * 
     */
    public Issue createIssue() {
        return new Issue();
    }

    /**
     * Create an instance of {@link UserIdentifier }
     * 
     */
    public UserIdentifier createUserIdentifier() {
        return new UserIdentifier();
    }

    /**
     * Create an instance of {@link GroupIdentifier }
     * 
     */
    public GroupIdentifier createGroupIdentifier() {
        return new GroupIdentifier();
    }

    /**
     * Create an instance of {@link SimpleIdentifier }
     * 
     */
    public SimpleIdentifier createSimpleIdentifier() {
        return new SimpleIdentifier();
    }

    /**
     * Create an instance of {@link OutboundPatientIdentifier }
     * 
     */
    public OutboundPatientIdentifier createOutboundPatientIdentifier() {
        return new OutboundPatientIdentifier();
    }

    /**
     * Create an instance of {@link OrganizationIdentifier }
     * 
     */
    public OrganizationIdentifier createOrganizationIdentifier() {
        return new OrganizationIdentifier();
    }

    /**
     * Create an instance of {@link OutboundPatientProtocolIdentifier }
     * 
     */
    public OutboundPatientProtocolIdentifier createOutboundPatientProtocolIdentifier() {
        return new OutboundPatientProtocolIdentifier();
    }

    /**
     * Create an instance of {@link Issues }
     * 
     */
    public Issues createIssues() {
        return new Issues();
    }

    /**
     * Create an instance of {@link ChangesList }
     * 
     */
    public ChangesList createChangesList() {
        return new ChangesList();
    }

    /**
     * Create an instance of {@link OutboundIdentifier }
     * 
     */
    public OutboundIdentifier createOutboundIdentifier() {
        return new OutboundIdentifier();
    }

    /**
     * Create an instance of {@link OutboundOrganizationIdentifier }
     * 
     */
    public OutboundOrganizationIdentifier createOutboundOrganizationIdentifier() {
        return new OutboundOrganizationIdentifier();
    }

    /**
     * Create an instance of {@link Changes }
     * 
     */
    public Changes createChanges() {
        return new Changes();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OperationException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "OperationException")
    public JAXBElement<OperationException> createOperationException(OperationException value) {
        return new JAXBElement<OperationException>(_OperationException_QNAME, OperationException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetOutboundMessagesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "getOutboundMessagesResponse")
    public JAXBElement<GetOutboundMessagesResponse> createGetOutboundMessagesResponse(GetOutboundMessagesResponse value) {
        return new JAXBElement<GetOutboundMessagesResponse>(_GetOutboundMessagesResponse_QNAME, GetOutboundMessagesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetOutboundMessages }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "getOutboundMessages")
    public JAXBElement<GetOutboundMessages> createGetOutboundMessages(GetOutboundMessages value) {
        return new JAXBElement<GetOutboundMessages>(_GetOutboundMessages_QNAME, GetOutboundMessages.class, null, value);
    }

}
