package com.velos.webservices.client;

import java.io.PrintStream;
import java.net.MalformedURLException;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;

import com.velos.services.OperationException_Exception;
import com.velos.services.StudyIdentifier;
import com.velos.services.StudyPatient;
import com.velos.services.StudyPatientSEI;

public class StudyPatientsSampleClient{
	
	static StudyPatientSEI studyPatientService = null;
	//Main method to marshal study object and get its XML.
	public static void main(String args[]) throws OperationException_Exception, MalformedURLException, JAXBException{
	
		
		//get patient demographics client code
		studyPatientService= ClientUtils.getStudyPatientsService();
		StudyIdentifier studyId = new StudyIdentifier();
		
		studyId.setStudyNumber(ServiceProperties.getProperty(ServiceProperties.TEST_STUDY_NUMBER));//study number (can be changed)
		List<StudyPatient> lstStudyPatient =  studyPatientService.getStudyPatients(studyId);
		
		
		JAXBContext context = JAXBContext.newInstance(StudyPatient.class);			
	    Marshaller m = context.createMarshaller();
	    m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true); 
	    PrintStream p = System.out; 
	    
	    for(StudyPatient studyPatient :  lstStudyPatient){
	    	JAXBElement<StudyPatient> patientStudyElement = new JAXBElement<StudyPatient>(
	    			new QName(StudyPatient.class.getSimpleName()),
	    			StudyPatient.class, 
	    			studyPatient);
	    	
	    	m.marshal(patientStudyElement, p);
	    }
		
	}

	

}