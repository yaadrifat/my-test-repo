/**
 * 
 */
package com.velos.webservices.client;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.endpoint.Endpoint;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.ws.security.wss4j.WSS4JOutInterceptor;
import org.apache.ws.security.WSConstants;
import org.apache.ws.security.handler.WSHandlerConstants;

import com.velos.services.MonitoringSEI;
import com.velos.services.MonitoringService;
import com.velos.services.PatientDemographicsSEI;
import com.velos.services.PatientDemographicsService;

import com.velos.services.PatientStudySEI;
import com.velos.services.PatientStudyService;
import com.velos.services.StudyCalendarService;
import com.velos.services.StudyPatientSEI;
import com.velos.services.StudyPatientService;
import com.velos.services.StudySEI;
import com.velos.services.StudyService;
import com.velos.services.StudyCalendarSEI;
import com.velos.services.StudyCalendar;

/**
 * @author dylan
 *
 */
public class ClientUtils {
	/**
	 * Utility method to get StudyService for the 
	 *  user logging in.
	 */
	
	public static StudySEI getStudyService() throws MalformedURLException{
		StudyService service = null;
		StudySEI studyService = null;
		//setup the service port
		if (studyService == null){

			URL serviceEndpointWSDL = 
					new URL(ServiceProperties.getProperty(ServiceProperties.SERVER_DOMAIN)+
							ServiceProperties.STUDY_SERVICE_WSDL_URL);
			
			service = new StudyService(serviceEndpointWSDL);
			studyService = service.getPort(StudySEI.class);
					
			//get the cxf endpoint so we can add security interceptor
			Client client = ClientProxy.getClient(studyService);
			addSecurityInterceptor(client);
		}
		return studyService;
	}
	
	/**
	 * Utility method to get PatientDemographicsService for the 
	 *  user logging in.
	 */
	
	public static PatientDemographicsSEI getPatientDemographicsService() throws MalformedURLException{
		PatientDemographicsService service = null;
		PatientDemographicsSEI patientService = null;
		//setup the service port
		if (patientService == null){

			URL serviceEndpointWSDL = 
					new URL(ServiceProperties.getProperty(ServiceProperties.SERVER_DOMAIN)+
							ServiceProperties.PAT_DEMOGRAPHICS_SERVICE_WSDL_URL);
			
			service = new PatientDemographicsService(serviceEndpointWSDL);
			patientService = service.getPort(PatientDemographicsSEI.class);
					
			//get the cxf endpoint so we can add security interceptor
			Client client = ClientProxy.getClient(patientService);
			addSecurityInterceptor(client);
		}
		return patientService;
	}
	
	/**
	 * Utility method to get StudyPatientService for the 
	 *  user logging in.
	 */
	
	public static StudyPatientSEI getStudyPatientsService() throws MalformedURLException{
		StudyPatientService service = null;
		StudyPatientSEI studypatientService = null;
		//setup the service port
		if (studypatientService == null){

			URL serviceEndpointWSDL = 
					new URL(ServiceProperties.getProperty(ServiceProperties.SERVER_DOMAIN)+
							ServiceProperties.STUDY_PATIENT_SERVICE_WSDL_URL);
			
			service = new StudyPatientService(serviceEndpointWSDL);
			studypatientService = service.getPort(StudyPatientSEI.class);
					
			//get the cxf endpoint so we can add security interceptor
			Client client = ClientProxy.getClient(studypatientService);
			addSecurityInterceptor(client);
		}
		return studypatientService;
	}
	/**
	 * Utility method to get StudyPatientService for the 
	 *  user logging in.
	 */
	
	public static PatientStudySEI getPatientStudiesService() throws MalformedURLException{
		PatientStudyService service = null;
		PatientStudySEI patientStudyService = null;
		//setup the service port
		if (patientStudyService == null){

			URL serviceEndpointWSDL = 
					new URL(ServiceProperties.getProperty(ServiceProperties.SERVER_DOMAIN)+
							ServiceProperties.PATIENT_STUDY_SERVICE_WSDL_URL);
			
			service = new PatientStudyService(serviceEndpointWSDL);
			patientStudyService = service.getPort(PatientStudySEI.class);
					
			//get the cxf endpoint so we can add security interceptor
			Client client = ClientProxy.getClient(patientStudyService);
			addSecurityInterceptor(client);
		}
		return patientStudyService;
	}
	/**
	 * Utility method to get PatientSchedule for the 
	 *  user logging in.
	 */
	
	
	public static MonitoringSEI getMonitoringService() throws MalformedURLException{
		MonitoringService service = null;
		MonitoringSEI monitoringService = null;
		//setup the service port
		if (monitoringService == null){

			URL serviceEndpointWSDL = 
					new URL(ServiceProperties.getProperty(ServiceProperties.SERVER_DOMAIN)+
							ServiceProperties.MONITORING_SERVICE_WSDL_URL);
			
			service = new MonitoringService(serviceEndpointWSDL);
			monitoringService = service.getPort(MonitoringSEI.class);
					
			//get the cxf endpoint so we can add security interceptor
			Client client = ClientProxy.getClient(monitoringService);
			addSecurityInterceptor(client);
		}
		return monitoringService;
	}
	
	public static StudyCalendarSEI getStudyCalendar() throws MalformedURLException{
		StudyCalendarSEI studycalendar = null;
		StudyCalendarService service = null; 
		//setup the service port
		if (studycalendar == null){

			URL serviceEndpointWSDL = 
					new URL(ServiceProperties.getProperty(ServiceProperties.SERVER_DOMAIN)+
							ServiceProperties.STUDY_CALENDAR_WSDL_URL);
			
			service = new StudyCalendarService(serviceEndpointWSDL);
			studycalendar = service.getPort(StudyCalendarSEI.class);
					
			//get the cxf endpoint so we can add security interceptor	
				
			
			Client client = ClientProxy.getClient(studycalendar);
			addSecurityInterceptor(client);
		}
		return studycalendar;
	}
	
	/**
	 * method to generate test study number.
	 */
	public static String getNewTestStudyNumber() {
		String strTime= String.valueOf(new Date().getTime());
		String testStudyNumber = "TestStudy"+strTime;
		return testStudyNumber;
	}
	
	//Add Security interceptor
  	private static void addSecurityInterceptor(Client client){
  		
		Endpoint cxfEndpoint = client.getEndpoint();

		Map<String,Object> outProps = new HashMap<String,Object>();
		outProps.put(WSHandlerConstants.ACTION,
				WSHandlerConstants.USERNAME_TOKEN);
		outProps.put(WSHandlerConstants.PASSWORD_TYPE, 
				WSConstants.PW_TEXT);
		outProps.put(WSHandlerConstants.PW_CALLBACK_CLASS, 
				SimpleCallbackHandler.class.getName());
		outProps.put(WSHandlerConstants.USER, 
				ServiceProperties.getProperty(ServiceProperties.TEST_LOGIN_USERNAME));

		WSS4JOutInterceptor wssOut = new WSS4JOutInterceptor(outProps);
		cxfEndpoint.getOutInterceptors().add(wssOut);
  	}
	
	public ClientUtils() {
		super();
	}

}