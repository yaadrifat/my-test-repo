/**
 * Created On Apr 6, 2011
 */
package com.velos.webservices.outbound;

import java.util.Hashtable;

import javax.naming.Context;

import com.velos.webservices.client.ServiceProperties;

/**
 * @author Kanwaldeep
 *
 */
public class StartOutBoundSampleClient {
	
	
	public static void main(String[] args)
	{
		
		//Initial JNDI properties 
		Hashtable<String, String> env = new Hashtable<String, String>();
		//JNDI properties details for getting message from TOPIC 
		env.put(Context.INITIAL_CONTEXT_FACTORY, ServiceProperties.getProperty(ServiceProperties.JNDI_NAMING_FACTORY_INITIAL)); 
		env.put(Context.PROVIDER_URL, ServiceProperties.getProperty(ServiceProperties.JNDI_NAMING_PROVIDER_URL)); 
		env.put(Context.URL_PKG_PREFIXES,ServiceProperties.getProperty(ServiceProperties.JNDI_NAMING_FACTORY_URL_PKGS));
		OutBoundClient client = new OutBoundClient(); 
		client.setJNDIProperties(env); // set JNDI
		
		// To create at Durable Subscription 
		client.setClientID("client1"); // example Client ID 
		client.setUserName(ServiceProperties.getProperty(ServiceProperties.STUDY_TOPIC_USERNAME));  
		client.setPassword(ServiceProperties.getProperty(ServiceProperties.STUDY_TOPIC_PASSWORD));  
		client.setSubscriberName("Subscriber1"); //example subscriber name 
		// end data for Durable Subscription
		
		client.setConnectionFactoryJNDI(ServiceProperties.getProperty(ServiceProperties.STUDY_TOPIC_CONNECTION_FACTORY_JNDI)); 
		client.setTopicJNDI(ServiceProperties.getProperty(ServiceProperties.STUDY_TOPIC_JNDI)); //
		
		client.startListening();
		
		/** you can start multiple number of message listeners depending upon number of TOPICs you are subscribing to by repeating the previous code
		and setting TOPIC specific values.
		*/
	}
	


}
