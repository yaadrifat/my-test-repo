package com.velos.webservices.client;

import java.io.PrintStream;
import java.net.MalformedURLException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;

import com.velos.services.OperationException_Exception;
import com.velos.services.OrganizationIdentifier;
import com.velos.services.PatientDemographics;
import com.velos.services.PatientDemographicsSEI;
import com.velos.services.PatientIdentifier;

public class PatientDemographicsSampleClient{
	
	static PatientDemographicsSEI patientService = null;
	//Main method to marshal study object and get its XML.
	public static void main(String args[]) throws OperationException_Exception, MalformedURLException, JAXBException{
	
		
		//get patient demographics client code
		patientService= ClientUtils.getPatientDemographicsService();
		PatientIdentifier patIdentifier = new PatientIdentifier();
		
		patIdentifier.setPatientId(ServiceProperties.getProperty(ServiceProperties.TEST_PATIENT_ID));//patientd id(can be changed)
		OrganizationIdentifier orgId = new OrganizationIdentifier();
		orgId.setSiteName(ServiceProperties.getProperty(ServiceProperties.TEST_PATIENT_ORGANIZATION_NAME));
		patIdentifier.setOrganizationId(orgId);
		
		PatientDemographics patdemo = patientService.getPatientDemographics(patIdentifier);
		
		JAXBElement<PatientDemographics> patdemoElement = new JAXBElement<PatientDemographics>(
				new QName(PatientDemographics.class.getSimpleName()), 
				PatientDemographics.class, 
				patdemo);
		
		JAXBContext context = JAXBContext.newInstance(PatientDemographics.class);			
	    Marshaller m = context.createMarshaller();
	    m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true); 
	    PrintStream p = System.out; 
		m.marshal(patdemoElement, p);
		
	}

	
}