package com.velos.webservices.client;

import java.io.PrintStream;
import java.net.MalformedURLException;
import java.util.GregorianCalendar;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;

import com.velos.services.IOException_Exception;
import com.velos.services.MessageTraffic;
import com.velos.services.MonitorVersion;
import com.velos.services.MonitoringSEI;
import com.velos.services.OperationException_Exception;
import com.velos.services.OperationRolledBackException_Exception;
import com.velos.services.ResponseHolder;

public class MonitoringServiceSampleClient{
	static MonitoringSEI monitoringService = null;
	
	public static void main(String args[]) 
	throws MalformedURLException, 
	OperationRolledBackException_Exception, 
	OperationException_Exception,
	JAXBException, 
	IOException_Exception, 
	DatatypeConfigurationException{
		
		monitoringService = ClientUtils.getMonitoringService();
		ResponseHolder responseHeartBeat =monitoringService.getHeartBeat();
		JAXBContext context = JAXBContext.newInstance(ResponseHolder.class, MonitorVersion.class, MessageTraffic.class);
		Marshaller m = context.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		
		JAXBElement<ResponseHolder> responseHeartBeatElement = new JAXBElement<ResponseHolder>(
				new QName("Response"), 
				ResponseHolder.class,
				responseHeartBeat);
		
		PrintStream p = System.out;
		//marshalling Heart Beat
		m.marshal(responseHeartBeatElement, p);
		
		//marshalling monitor version
		MonitorVersion monitorVersion= monitoringService.getMonitorVersion();
		JAXBElement<MonitorVersion> monitorVersionElement = new JAXBElement<MonitorVersion>(
				new QName(MonitorVersion.class.getSimpleName()), 
				MonitorVersion.class, 
				monitorVersion);
		
		m.marshal(monitorVersionElement, p);
		
		//form date and to date (can be changed from ServiceProperties)
		Integer fromDate_YYYY=0000;
		Integer fromDate_MM=00;
		Integer fromDate_DD=00;
		Integer toDate_YYYY=0000;
		Integer toDate_MM=00;
		Integer toDate_DD=00;
		
		if(ServiceProperties.getProperty(ServiceProperties.MESSAGE_TRAFFIC_FROM_DATE_YYYY) != null){
			fromDate_YYYY = Integer.valueOf(ServiceProperties.getProperty(ServiceProperties.MESSAGE_TRAFFIC_FROM_DATE_YYYY));
		}
		if(ServiceProperties.getProperty(ServiceProperties.MESSAGE_TRAFFIC_FROM_DATE_MM) != null){
			fromDate_MM = Integer.valueOf(ServiceProperties.getProperty(ServiceProperties.MESSAGE_TRAFFIC_FROM_DATE_MM));
		}
		if(ServiceProperties.getProperty(ServiceProperties.MESSAGE_TRAFFIC_FROM_DATE_DD) != null){
			fromDate_DD = Integer.valueOf(ServiceProperties.getProperty(ServiceProperties.MESSAGE_TRAFFIC_FROM_DATE_DD)); 
		}
		if(ServiceProperties.getProperty(ServiceProperties.MESSAGE_TRAFFIC_FROM_DATE_YYYY) != null){
			toDate_YYYY = Integer.valueOf(ServiceProperties.getProperty(ServiceProperties.MESSAGE_TRAFFIC_TO_DATE_YYYY));
		}
		if(ServiceProperties.getProperty(ServiceProperties.MESSAGE_TRAFFIC_FROM_DATE_MM) != null){
			toDate_MM = Integer.valueOf(ServiceProperties.getProperty(ServiceProperties.MESSAGE_TRAFFIC_TO_DATE_MM));
		}
		if(ServiceProperties.getProperty(ServiceProperties.MESSAGE_TRAFFIC_FROM_DATE_MM) != null){
			toDate_DD = Integer.valueOf(ServiceProperties.getProperty(ServiceProperties.MESSAGE_TRAFFIC_TO_DATE_DD));
		}
		
		XMLGregorianCalendar fromDate = DatatypeFactory.
		newInstance()
		.newXMLGregorianCalendar(
				new GregorianCalendar(
						fromDate_YYYY, fromDate_MM, fromDate_DD));//from date  (can be changed)
		
		XMLGregorianCalendar toDate = DatatypeFactory.
		newInstance().
		newXMLGregorianCalendar(
				new GregorianCalendar(
						toDate_YYYY, toDate_MM, toDate_DD));//to date (can be changed)
//		Date fromDate = new Date("");
//		Date toDate = new Date("");
//		//marshalling message traffic
		MessageTraffic messageTraffic = monitoringService.getMessageTraffic(fromDate, toDate);
		
		JAXBElement<MessageTraffic> messageTrafficElement = new JAXBElement<MessageTraffic>(
				new QName(MessageTraffic.class.getSimpleName()), 
				MessageTraffic.class, 
				messageTraffic);
		m.marshal(messageTrafficElement, p);
	}
}