/**
 * Created On Apr 14, 2011
 */
package com.velos.webservices.outbound;


import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.Attributes;

import com.velos.services.Study;
import com.velos.services.StudyIdentifier;
import com.velos.services.StudyStatus;
import com.velos.services.StudyStatusIdentifier;

/**
 * @author Kanwaldeep
 *
 */
public class OutBoundSAXProcessor {
	

	
	/**This method is used to parse xml by using SAX parser
	 * @param xml
	 */
	public void processBySAXParser(String xml)
	{
		try {
			byte[] byteString = xml.getBytes();
			ByteArrayInputStream in = new ByteArrayInputStream(byteString);
			InputSource inputSource = new InputSource(in);
			SAXParserFactory factory = SAXParserFactory.newInstance();

			SAXParser parser;

			parser = factory.newSAXParser();

			CustomHandler handler = new CustomHandler(); 
			parser.parse(inputSource, handler);
			
			if(handler.getParentOID().size() == 1 && handler.getParentOID().containsKey(IdentifierConstants.STUDY_IDENTIFIER))
			{
				//process Study Messages 
				
				StudyMessageProcessor processor = new StudyMessageProcessor(); 
				processor.processStudyMessage(handler); 
				
			}
				
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
 
    
 
   
  }

}
