package com.velos.webservices.client;

import java.io.PrintStream;
import java.net.MalformedURLException;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;

import com.velos.services.OperationException_Exception;
import com.velos.services.OrganizationIdentifier;
import com.velos.services.PatientIdentifier;
import com.velos.services.PatientStudy;
import com.velos.services.PatientStudySEI;

public class PatientStudiesSampleClient{
	
	static PatientStudySEI patientStudiesService = null;
	//Main method to marshal PatientStudy object and get its XML.
	public static void main(String args[]) throws OperationException_Exception, MalformedURLException, JAXBException{
	
		
		//get PatientStudy client code
		patientStudiesService= ClientUtils.getPatientStudiesService();
		PatientIdentifier patientIdentifier = new PatientIdentifier();
		patientIdentifier.setPatientId(ServiceProperties.getProperty(ServiceProperties.TEST_PATIENT_ID));//patient id(can be changed)
		OrganizationIdentifier orgId = new OrganizationIdentifier();
		orgId.setSiteName(ServiceProperties.getProperty(ServiceProperties.TEST_PATIENT_ORGANIZATION_NAME));
		patientIdentifier.setOrganizationId(orgId);
		
		List<PatientStudy> lstPatientStudies =  patientStudiesService.getPatientStudies(patientIdentifier);
		
		
		JAXBContext context = JAXBContext.newInstance(PatientStudy.class);			
	    Marshaller m = context.createMarshaller();
	    m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true); 
	    PrintStream p = System.out; 
	    
	    for(PatientStudy patientStudy :  lstPatientStudies){
	    	JAXBElement<PatientStudy> patientStudyElement = new JAXBElement<PatientStudy>(
	    			new QName(PatientStudy.class.getSimpleName()), 
	    			PatientStudy.class, 
	    			patientStudy);
	    	
	    	m.marshal(patientStudyElement, p);
	    }
		
	}
}