/**
 * Created On Apr 6, 2011
 */
package com.velos.webservices.outbound;


import java.util.Hashtable;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.jms.TopicSubscriber;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * @author Kanwaldeep
 *
 */
public class OutBoundClient implements MessageListener, ExceptionListener{
	
	private boolean running = true; 
	private String connectionFactoryJNDI = ""; 
	private String topicJNDI = ""; 
	private String userName = ""; 
	private String password = ""; 
	private String clientID = ""; 
	private String subscriberName = ""; 
	private Hashtable<String, String> env = new Hashtable<String, String>(); 
	
	/**This method sets JNDI properties for TOPIC and TopicConnectionFactory lookup
	 * @param Hashtable with JNDI properties
	 */
	public void setJNDIProperties(Hashtable<String, String> env) {
		this.env = env;
	}

	/* (non-Javadoc)
	 * @see javax.jms.ExceptionListener#onException(javax.jms.JMSException)
	 */
	public void onException(JMSException arg0) {
		arg0.printStackTrace(); 
		
	}

	/* (non-Javadoc)
	 * @see javax.jms.MessageListener#onMessage(javax.jms.Message)
	 */
	public void onMessage(Message msg) {
		System.out.println("got New message"); 
		if (msg instanceof TextMessage) {
            
        	TextMessage xml = (TextMessage) msg; 
        	try {
        		String xmlString = xml.getText();
        		
        		//DOM parser
//        		OutBoundDOMMessageProcessor dom = new OutBoundDOMMessageProcessor();            	
//    			dom.processByDOMParser(xmlString);
    			
        		
        		//SAX Parser 
    			OutBoundSAXProcessor sax = new OutBoundSAXProcessor(); 
    			sax.processBySAXParser(xmlString); 
        	} catch (JMSException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
        	
			
		}
	}
	
	/**This method starts listening process to listen to OutBound messages published to TOPIC  
	 */
	public void startListening()
	{

		//ConnectionFactory JNDI  
		ConnectionFactory factory = getConnectionFactory(connectionFactoryJNDI); 
		try {
			Connection conn = factory.createConnection(userName, password); 
			conn.setClientID(clientID); 
			Session session  = conn.createSession(false, Session.AUTO_ACKNOWLEDGE); 
			Topic topic = getTopic(topicJNDI); 
			TopicSubscriber sub = session.createDurableSubscriber(topic, subscriberName);
			sub.setMessageListener(this); 
			conn.setExceptionListener(this); 
			conn.start(); 
			while(running) // listen messages
			{
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				} 
			}
			
			conn.close(); 
			
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
	}
	

	
	/**
	 * @param JNDIName
	 * @return JNDI Object for given JNDI name
	 */
	public Object lookup(String JNDIName)
	{
		 
		InitialContext context = null;
		Object obj = null; 
		try {
			
			context = new InitialContext(env);
			obj = context.lookup(JNDIName);
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return  obj; 
	}
	
	
	/**
	 * @param connectionFactoryJNDI
	 * @return TopicConnectionFactory
	 */
	public ConnectionFactory getConnectionFactory(String connectionFactoryJNDI)
	{
		
		ConnectionFactory factory = (ConnectionFactory) lookup(connectionFactoryJNDI); 		
		return factory; 
	}
	
		
	/**
	 * @param topicJNDI
	 * @return TOPIC  
	 */
	public Topic getTopic(String topicJNDI){
		Topic topic = (Topic) lookup(topicJNDI); 
		return topic; 
	}
	
	public void close()
	{
		this.running = false; 
	}

	/**
	 * @param connectionFactoryJNDI
	 */
	public void setConnectionFactoryJNDI(String connectionFactoryJNDI) {
		this.connectionFactoryJNDI = connectionFactoryJNDI;
	}

	/**Sets TopicJNDI for connection 
	 * @param topicJNDI
	 */
	public void setTopicJNDI(String topicJNDI) {
		this.topicJNDI = topicJNDI;
	}

	/**Sets UserName for connection for durable subscription
	 * @param userName
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**Sets Password for connection for durable subscription
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**Sets clientID for connection for durable subscription
	 * @param clientID
	 */
	public void setClientID(String clientID) {
		this.clientID = clientID;
	}

	/**Sets subscriber Name for durable subscription 
	 * @param subscriberName 
	 */
	public void setSubscriberName(String subscriberName) {
		this.subscriberName = subscriberName;
	}
	
	


}
