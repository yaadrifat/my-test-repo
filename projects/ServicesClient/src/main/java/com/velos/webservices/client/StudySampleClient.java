package com.velos.webservices.client;

import java.io.PrintStream;
import java.net.MalformedURLException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;

import com.velos.services.OperationException_Exception;
import com.velos.services.Study;
import com.velos.services.StudyIdentifier;
import com.velos.services.StudyOrganization;
import com.velos.services.StudySEI;
import com.velos.services.StudyStatus;

public class StudySampleClient {
	

	static StudySEI studyService = null;
	StudySampleClient(){}
	//Main method to marshal study object and get its XML.
	public static void main(String args[]) throws OperationException_Exception, MalformedURLException, JAXBException{
		//get study client code
		studyService= ClientUtils.getStudyService();
		StudyIdentifier studyIdentifier = new StudyIdentifier();
		studyIdentifier.setStudyNumber(ServiceProperties.getProperty(ServiceProperties.TEST_STUDY_NUMBER));//example study number
		Study study =studyService.getStudy(studyIdentifier);
	
		JAXBContext context = JAXBContext.newInstance(Study.class,StudyIdentifier.class,StudyOrganization.class,StudyStatus.class);			
	    Marshaller m = context.createMarshaller();
	    
	    JAXBElement<Study> studyElement = new JAXBElement<Study>(
	    		new QName(Study.class.getSimpleName()), 
	    		Study.class,
	    		study);
	    
	    m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true); 
	    PrintStream p = System.out; 
		m.marshal(studyElement, p);
		
		
	}
}
