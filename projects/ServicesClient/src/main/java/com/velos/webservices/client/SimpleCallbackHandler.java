/**
 * 
 */
package com.velos.webservices.client;
 
import java.io.IOException;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

import org.apache.ws.security.WSPasswordCallback;

/**   
 * @author dylan
 * call back handle class handling authentication of user logging in.
 *
 */
public class SimpleCallbackHandler implements CallbackHandler{
	  public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {

	        WSPasswordCallback pc = (WSPasswordCallback) callbacks[0];
	        //virendra: set login password for logging in user.
	        pc.setPassword(ServiceProperties.getProperty(ServiceProperties.TEST_LOGIN_PASSWORD)); 
	    }

}
