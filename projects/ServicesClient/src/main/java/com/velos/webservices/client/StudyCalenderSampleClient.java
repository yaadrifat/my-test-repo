package com.velos.webservices.client;

import java.io.PrintStream;
import java.net.MalformedURLException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;

import com.velos.services.OperationException_Exception;
import com.velos.services.StudyCalendar;
import com.velos.services.StudyCalendarSEI;
import com.velos.services.StudyIdentifier;
import com.velos.services.GetStudyCalendar;

public class StudyCalenderSampleClient {
	
	static StudyCalendarSEI studycalendar = null; 
	static GetStudyCalendar scalendar = null;
	
	StudyCalenderSampleClient(){}
	//Main method to marshal study calendar object and get its XML.
	public static void main(String args[]) throws OperationException_Exception, MalformedURLException, JAXBException{
		//get study calendar client code
		studycalendar = ClientUtils.getStudyCalendar();
		StudyIdentifier studyIdentifier = new StudyIdentifier();
		studyIdentifier.setStudyNumber(ServiceProperties.getProperty(ServiceProperties.TEST_STUDY_NUMBER)); // study name (can be changed)
		StudyCalendar sc =studycalendar.getStudyCalendar( null, studyIdentifier, 
				ServiceProperties.getProperty(ServiceProperties.STUDY_CALENDAR_NAME));// calendar name ( can be changed)
		 
		JAXBContext context = JAXBContext.newInstance(StudyCalendar.class);	
	    Marshaller m = context.createMarshaller();
	    
	    JAXBElement<StudyCalendar> calendarElement = new JAXBElement<StudyCalendar>(
	    		new QName(GetStudyCalendar.class.getSimpleName()),
	    		StudyCalendar.class,
	    		sc);
	    
	    m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true); 
	    PrintStream p = System.out; 
		m.marshal(calendarElement, p);
	}

}
