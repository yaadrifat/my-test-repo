/**
 * This class deals with the properties, to put into the test cases.
 */
package com.velos.webservices.client;

import java.util.ResourceBundle;

public class ServiceProperties {
	
	private static ResourceBundle bundle= ResourceBundle.
						getBundle("com/velos/webservices/client/Test");
	// Constants
	public static final String SERVER_DOMAIN = "server.domain";
	public static final String BUDGET_SERVICE_WSDL_URL = "/webservices/budgetservice?wsdl";
	public static final String FORM_RESPONSE_SERVICE_WSDL_URL = "/webservices/formresponseservice?wsdl";
	public static final String FORM_SERVICE_WSDL_URL = "/webservices/formservice?wsdl";
	public static final String LIBRARY_SERVICE_WSDL_URL = "/webservices/libraryservice?wsdl";
	public static final String MONITORING_SERVICE_WSDL_URL = "/webservices/monitorservice?wsdl";
	public static final String PAT_DEMOGRAPHICS_SERVICE_WSDL_URL = "/webservices/patientdemographicsservice?wsdl";
	public static final String PATIENT_SCHEDULE_SERVICE_WSDL_URL = "/webservices/patientscheduleservice?wsdl";
	public static final String PATIENT_STUDY_SERVICE_WSDL_URL = "/webservices/patientstudyservice?wsdl";
	public static final String STUDY_CALENDAR_WSDL_URL = "/webservices/studycalendarservice?wsdl";
	public static final String STUDY_MASHUP_WSDL_URL = "/webservices/studymashupservice?wsdl";
	public static final String STUDY_PATIENT_SERVICE_WSDL_URL = "/webservices/studypatientservice?wsdl";
	public static final String STUDY_SERVICE_WSDL_URL = "/webservices/studyservice?wsdl";
	public static final String SYSTEM_ADMINISTRATION_SERVICE_WSDL_URL = "/webservices/systemadministrationservice?wsdl";
	public static final String USER_SERVICE_WSDL_URL = "/webservices/userservice?wsdl";
	
	
	//Properties for Junit Tests.
	public static final String TEST_STUDY_NUMBER= "studyNo";
	public static final String TEST_STUDY_SYS_ID= "studyId";
	public static final String TEST_LOGIN_USERNAME= "login.userName";
	public static final String TEST_LOGIN_PASSWORD= "login.password";	
	public static final String TEST_ORGNAME= "study.orgname";
	
	//Properties for Test Study calendar
	public static final String STUDY_CALENDAR_OID= "study.calendar.oid";
	public static final String STUDY_CALENDAR_NAME= "study.calendar.name";
	public static final String STUDY_CALENDAR_VISIT_NAME= "study.calendar.visit.name";
	public static final String STUDY_CALENDAR_VISIT_EVENT_NAME= "study.calendar.visit.event.name";
	public static final String STUDY_CALENDAR_VISIT_OID= "study.calendar.visit.oid";
	public static final String STUDY_CALENDAR_VISIT_EVENT_OID= "study.calendar.visit.event.oid";
	public static final String STUDY_CALENDAR_CODE_COVERAGE_TYPE = "study.calendar.coverage.type";
	public static final String STUDY_CALENDAR_CODE_COST_TYPE = "study.calendar.cost.type";
	public static final String STUDY_CALENDAR_CODE_COST_CURRENCY = "study.calendar.cost.currency";
	public static final String STUDY_CALENDAR_STATUS_CODE = "study.calendar.status.code";
	public static final String STUDY_CALENDAR_TYPE_CODE = "study.calendar.type.code";
	
	//Properties for Test Create and Enroll Patient
	public static final String PAT_DEMOGRAPHICS_CODE_ETHNICITY = "pat.demographics.ethnicity.code";
	public static final String PAT_DEMOGRAPHICS_CODE_RACE = "pat.demographics.code.race";
	public static final String PAT_DEMOGRAPHICS_CODE_SURVIVALSTATUS = "pat.demographics.code.survivalstatus";
	public static final String PAT_DEMOGRAPHICS_CODE_GENDER = "pat.demographics.code.gender";
	public static final String PAT_ENROLLMENT_PATIENT_STATUS_CODE = "pat.enrollment.patient.status.code";

	//Properties for Test Study Team Member
	public static final String TEST_USERNAME= "userName";
	public static final String TEST_FIRSTNAME= "firstName";
	public static final String TEST_LASTNAME= "lastName";
	
	public static final String TEST_STUDY_SYS_ID_TO_UPDATE= 
		TEST_STUDY_SYS_ID + "_updated";
	
	public static final String TEST_FIRSTNAME_TO_UPDATE = 
		TEST_FIRSTNAME + "_updated";
	public static final String TEST_LASTNAME_TO_UPDATE=
		TEST_LASTNAME + "_updated";
	
	public static final String TEST_TEAM_ROLE_CODE=
		"teamRoleCode";
	public static final String TEST_TEAM_ROLE_CODE_TYPE= 
		"teamRoleCodeType";
	public static final String TEST_TEAM_ROLE_CODE_TO_UPDATE = 
		"teamRoleCodeToUpdate";
	public static final String TEST_TEAM_ROLE_CODE_TYPE_TO_UPDATE = 
		"teamRoleCodeTypeToUpdate";
	
	public static final String TEST_TEAM_MEMBER_STATUS_CODE_ACTIVE=
		"code.active";
	public static final String TEST_TEAM_MEMBER_STATUS_CODE_DEACTIVED=
		"code.deactived";
	//properties for Test StudySummary
	public static final String TEST_MORE_STUDY_DETAILS_KEY=
		"test.more.study.details.key";
	public static final String SUMMARY_PHASE_CODE = "summary.phase.code";
	public static final String SUMMARY_DIVISION_CODE = "summary.division.code";
	public static final String SUMMARY_TAREA_CODE = "summary.tarea.code";
	
	//Properties for Test Study status
	public static final String TEST_STUDY_OUTCOME_CODE="study.outcome.code";
	public static final String TEST_REVIEW_BOARD_CODE="review.board.code";
	public static final String TEST_STATUS_CODE="status.code";
	
	//Properties for Test Study Organization 
	public static final String TEST_STUDY_ORG_TYPE_CODE=
		"study.orgtype.code";
	public static final String TEST_STUDY_ORG_NEWENROLL_NOTIFICTO_LOGNAME_USR1=
		TEST_USERNAME;
	public static final String TEST_STUDY_ORG_APPRENROLL_NOTIFICTO_LOGNAME_USR1 =
		TEST_USERNAME;
	
	//Properties for Test Study Organization Update
	public static final String TEST_STUDY_ORG_NAME_TOUPDATE=
		"study.orgname.toupdate";
	public static final String TEST_STUDY_ORG_TYPE_CODE_TOUPDATE=
		"study.orgtype.code.toupdate";
	
	public static final String MESSAGE_TRAFFIC_FROM_DATE= "message.traffic.from.date";
	public static final String MESSAGE_TRAFFIC_TO_DATE= "message.traffic.to.date";
	public static final String TEST_PATIENT_ID = "test.patient.id";
	public static final String TEST_PATIENT_ORGANIZATION_NAME = "test.patient.organization.name";
	
	public static final String MESSAGE_TRAFFIC_FROM_DATE_YYYY= "message.traffic.from.date.yyyy";
	public static final String MESSAGE_TRAFFIC_FROM_DATE_MM= "message.traffic.from.date.mm";
	public static final String MESSAGE_TRAFFIC_FROM_DATE_DD= "message.traffic.from.date.dd";
	
	public static final String MESSAGE_TRAFFIC_TO_DATE_YYYY= "message.traffic.to.date.yyyy";
	public static final String MESSAGE_TRAFFIC_TO_DATE_MM= "message.traffic.to.date.mm";
	public static final String MESSAGE_TRAFFIC_TO_DATE_DD= "message.traffic.to.date.dd";
	
	
	//JNDI properties
	public static final String JNDI_NAMING_FACTORY_INITIAL = "java.naming.factory.initial"; 
	public static final String JNDI_NAMING_PROVIDER_URL = "java.naming.provider.url"; 
	public static final String JNDI_NAMING_FACTORY_URL_PKGS  = "java.naming.factory.url.pkgs";
	
	public static final String STUDY_TOPIC_CONNECTION_FACTORY_JNDI= "study.topic.connectionfactory.jndi"; 
	public static final String STUDY_TOPIC_JNDI = "study.topic.jndi"; 
	public static final String STUDY_TOPIC_USERNAME = "study.topic.username"; 
	public static final String STUDY_TOPIC_PASSWORD = "study.topic.password";
	
	/** Method to read properties as key, and return value
	 *  after reading from ResourceBundle
	 **/
	public static String getProperty(String key) {
	    String value = bundle.getString(key);
	    return value;
	}
	
}
