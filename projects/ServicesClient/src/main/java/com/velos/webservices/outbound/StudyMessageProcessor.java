/**
 * Created On Apr 19, 2011
 */
package com.velos.webservices.outbound;

import java.io.PrintStream;
import java.net.MalformedURLException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.velos.services.OperationException_Exception;
import com.velos.services.Study;
import com.velos.services.StudyIdentifier;
import com.velos.services.StudySEI;
import com.velos.services.StudyStatus;
import com.velos.services.StudyStatusIdentifier;
import com.velos.webservices.client.ClientUtils;

/**StudyMessageProcessor is responsible for processing all the messages related to Study module.
 * @author Kanwaldeep
 *
 */
public class StudyMessageProcessor {

	/**This method processes Study related message and performs required further actions.
	 *  Any changes specific to integration will go in this method. 
	 *  Modify this method to get mapping between external and velos data fields and do required functions* 
	 */
	public void processStudyMessage(CustomHandler handler) {
		
		StudyIdentifier studyIdentifier  = (StudyIdentifier) handler.getParentOID().get(IdentifierConstants.STUDY_IDENTIFIER); 
		String studyNumber = studyIdentifier.getStudyNumber(); 
	// check if study number is of interest then process it otherwise discard message and return from this method. 
	 
		for(Change change: handler.getChanges())
		{
			
			if(change != null)
			{
				String module = change.getModule();
				String action = change.getAction(); 
				
				if(module.equalsIgnoreCase("study_status"))
				{
					StudyStatusIdentifier studyStatusIdentifier = (StudyStatusIdentifier) change.getIdentifier(); 
					if(action.equalsIgnoreCase("CREATE") )
					{
						// get  from web service  
						//- depending on use case call either getStudy or get study status
						try{
							StudyStatus status = getStudyStatus(studyIdentifier, studyStatusIdentifier); 
							
							JAXBElement<StudyStatus> element = new JAXBElement<StudyStatus>(new QName("StudyStatus"), StudyStatus.class, status); 
						
							// do mapping send to external system 
							JAXBContext context = JAXBContext.newInstance(StudyStatus.class);			
							Marshaller m = context.createMarshaller();
							m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true); 
							PrintStream p = System.out; 
							m.marshal(element, p);
						} catch (JAXBException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					
												
					}else if( action.equalsIgnoreCase("UPDATE"))
					{
						// get  from web service  
						//- depending on use case call either getStudy or get study status
						try{
							StudyStatus status = getStudyStatus(studyIdentifier, studyStatusIdentifier); 
							JAXBElement<StudyStatus> element = new JAXBElement<StudyStatus>(new QName("StudyStatus"), StudyStatus.class, status); 
							// do mapping send to external system 
							JAXBContext context = JAXBContext.newInstance(StudyStatus.class);			
							Marshaller m = context.createMarshaller();
							m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true); 
							PrintStream p = System.out; 
							m.marshal(element, p);
						} catch (JAXBException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}else
					{
						// DELETE action you cannot call getstudystatus on deteted status 
						//code to process message for external system
					 
					}
				}
				
				
			}
			
		}
		
		
	}
	
	
	private StudyStatus getStudyStatus(StudyIdentifier studyIdentifier, StudyStatusIdentifier studyStatusIdentifier)
	{
		StudyStatus status = null; 
		try {
			StudySEI studyService= ClientUtils.getStudyService();
			status = studyService.getStudyStatus(studyStatusIdentifier); 
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//StudyStatus status = studyService.
 catch (OperationException_Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return status; 
	}

}
