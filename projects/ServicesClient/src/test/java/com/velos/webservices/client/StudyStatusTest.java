/**
 * 
 */
package com.velos.webservices.client;

import java.util.Date;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import junit.framework.Assert;

import org.junit.Test;

import com.velos.services.Code;
import com.velos.services.OperationException_Exception;
import com.velos.services.OperationRolledBackException_Exception;
import com.velos.services.OrganizationIdentifier;
import com.velos.services.ResponseHolder;
import com.velos.services.Study;
import com.velos.services.StudyIdentifier;
import com.velos.services.StudySEI;
import com.velos.services.StudyStatus;
import com.velos.services.UserIdentifier;

/**
 * @author dylan
 *
 */
public class StudyStatusTest  {

	/**
	 * We test status create, update and remove in one method
	 * by performing the operation, getting the study, performing 
	 * the operation again.
	 */
	@Test
	public void TestStatus(){
		try{
			DatatypeFactory dFactory = null;
			try {
				dFactory = DatatypeFactory.newInstance();
			} catch (DatatypeConfigurationException e) {
				throw new RuntimeException(e);
			}
			//Test Study Status Begin.
			String studyNumber =  ServiceProperties.getProperty(ServiceProperties.TEST_STUDY_NUMBER);
			StudyIdentifier studyId = new StudyIdentifier();
			studyId.setStudyNumber(studyNumber);
			StudySEI studyService = ClientUtils.getStudyService();
			Study study = null;
			try {
				study = studyService.getStudy(studyId);
				//all studies should have at least the initial status
				Assert.assertTrue(
						"Studies must have at least one status", 
						study.getStudyStatuses().getStudyStatus().size() > 0);
				
			} catch (OperationException_Exception e) {
				Assert.fail(e.getMessage());
			}
			//Get Study Statuses from Study.
			List<StudyStatus> statusesBeforeInsert = study.getStudyStatuses().getStudyStatus();
			
			StudyStatus newStatus = new StudyStatus();
			
			UserIdentifier userId = new UserIdentifier();
			XMLGregorianCalendar meetingDate = TestUtils.dateToXMLGregorian(new Date());
			
			
			
			userId.setUserLoginName(ServiceProperties.getProperty(ServiceProperties.TEST_USERNAME));
			String notes = "This is a test note from our unit tests";
			OrganizationIdentifier orgId = new OrganizationIdentifier();
			orgId.setSiteName(ServiceProperties.getProperty(ServiceProperties.TEST_ORGNAME));
			
			Code studyOutcomeCode = new Code();
			studyOutcomeCode.setCode(ServiceProperties.getProperty(ServiceProperties.TEST_STUDY_OUTCOME_CODE));
			
			Code reviewBoardCode = new Code();
			reviewBoardCode.setCode(ServiceProperties.getProperty(ServiceProperties.TEST_REVIEW_BOARD_CODE));
			
			Code statusCode= new Code();
			Code statusCodeType= new Code();
			
			statusCode.setCode("active");
			statusCodeType.setCode("default");
						
			newStatus.setAssignedTo(userId);
			newStatus.setDocumentedBy(userId);
			newStatus.setCurrentForStudy(true);
			newStatus.setMeetingDate(meetingDate);
			newStatus.setNotes(notes);
			newStatus.setOrganizationId(orgId);
			newStatus.setOutcome(studyOutcomeCode);
			newStatus.setReviewBoard(reviewBoardCode);
			newStatus.setStatus(statusCode);
			newStatus.setStatusType(statusCodeType);
			
			newStatus.setValidFromDate(meetingDate);
			newStatus.setValidUntilDate(meetingDate);
		
			//Add a new status after populating new StudyStatus Object
			ResponseHolder response = 
				studyService.addStudyStatus(studyId, newStatus);
			//Retrieve statuses again to test the new one
			List<StudyStatus> statusesAfterInsert = 
				studyService.getStudyStatuses(studyId).getStudyStatus();
			
			Assert.assertEquals("Study should have one more status", 
					statusesBeforeInsert.size() + 1, statusesAfterInsert.size());
			
			//loop through all status to make sure that we have the new one
			//if we find a match, it will populate into statusFromRempository
			
			StudyStatus statusFromRepository = null;
			for (StudyStatus studyStatus : statusesAfterInsert){
				Assert.assertNotNull("Status must have a status code", studyStatus.getStatus());
				if (isSameStatus(studyStatus, newStatus)){
					statusFromRepository = studyStatus;
					break;
				}
			}
			Assert.assertNotNull(
					"New status not returned from repository", 
					statusFromRepository);
	
			checkStatusFields(statusFromRepository, newStatus);

		}
		catch (OperationRolledBackException_Exception e)
		{
			TestUtils.handleOperationRolledbackException(e);
			Assert.fail(e.getMessage());
		}
		catch (Exception e) {
			Assert.fail(e.getMessage());
		}
		
	}
	
	/**
	 * Check that the fields in two statuses match
	 * @param statusA
	 * @param statusB
	 */
	public void checkStatusFields(StudyStatus statusA, StudyStatus statusB){
		
		//valid from date

		if (statusA.getValidFromDate() != null &&
				!TestUtils.isSameDay(statusA.getValidFromDate(), statusB.getValidFromDate())){
			Assert.fail("Valid From Dates are not the same");
		}
		
		//valid from date
		if (statusA.getValidUntilDate() != null &&
				!TestUtils.isSameDay(statusA.getValidUntilDate(), statusB.getValidUntilDate())){
			Assert.fail("Valid Until Dates are not the same");
		}
		
		//check documented by to user
		if (statusA.getDocumentedBy().getUserLoginName() != null &&
				!statusA.getDocumentedBy().getUserLoginName().
				equalsIgnoreCase(
						statusB.getDocumentedBy().getUserLoginName())){
			Assert.fail("Documented By Users are not the same");
		}
		
		//meeting date
		if (!TestUtils.isSameDay(statusA.getMeetingDate(), statusB.getMeetingDate())){
			Assert.fail("Meeting Dates are not the same");
		}
		
		//notes
		Assert.assertEquals("Notes are not equal", statusA.getNotes(), statusB.getNotes());
		//review board
		if (statusA.getReviewBoard() != null){
			Assert.assertEquals("Review Board Not Same", 
					statusA.getReviewBoard().getCode(), 
					statusB.getReviewBoard().getCode());
		}
		//valid from date
		if (statusA.getValidFromDate() != null &&
				!TestUtils.isSameDay(statusA.getValidFromDate(), statusB.getValidFromDate())){
			Assert.fail("Valid From Date not equal.");
		}
		
		//valid till date
		if (statusA.getValidUntilDate() != null &&
				!TestUtils.isSameDay(statusA.getValidUntilDate(), statusB.getValidUntilDate())){
			Assert.fail("Valid From Date not equal.");
		}
	}
	
	/**
	 * Compares two statuses to tell whether they are the same. Compares
	 * each field individually. Returns false the first time if finds a field
	 * that does not match.
	 * @param statusA
	 * @param statusB
	 * @return statuses are equal
	 */
	public boolean isSameStatus(StudyStatus statusA, StudyStatus statusB){
		//check assigned to user
		if (statusA.getAssignedTo() != null &&
		        statusB.getAssignedTo() != null &&
		        statusA.getAssignedTo().getUserLoginName() != null &&
				!statusA.getAssignedTo().getUserLoginName().
				equalsIgnoreCase(
						statusB.getAssignedTo().getUserLoginName())){
			return false;
		}
		
		//valid from date
		if (statusA.getValidFromDate() != null &&
				!TestUtils.isSameDay(statusA.getValidFromDate(), statusB.getValidFromDate())){
			return false;
		}
		//organization id
		if ((statusA.getOrganizationId() != null && statusB.getOrganizationId() != null) &&
				!statusA.getOrganizationId().getSiteName().equals(
				statusB.getOrganizationId().getSiteName())){
			return false;
		}
		//status code
		if (statusA.getStatus() != null && 
				!statusA.getStatus().getCode().equals(
				statusB.getStatus().getCode())){
			return false;
		}
		
		//status type code
		if (statusA.getStatusType() != null &&
				!statusA.getStatusType().getCode().equals(
				statusB.getStatusType().getCode())){
			return false;
		}
		
		return true;
	}
	

}