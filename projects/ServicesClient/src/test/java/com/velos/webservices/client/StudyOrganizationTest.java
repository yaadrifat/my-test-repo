/**
 * 
 */
package com.velos.webservices.client;

import java.net.MalformedURLException;
import java.util.List;

import junit.framework.Assert;

import org.junit.Test;

import com.velos.services.Code;
import com.velos.services.OperationException_Exception;
import com.velos.services.OperationRolledBackException_Exception;
import com.velos.services.OrganizationIdentifier;
import com.velos.services.ResponseHolder;
import com.velos.services.Study;
import com.velos.services.StudyIdentifier;
import com.velos.services.StudyOrganization;
import com.velos.services.StudySEI;
import com.velos.services.UserIdentifier;

/**
 * @author dylan
 *
 */
public class StudyOrganizationTest {

	/**
	 * Test method for  Study Organization, Add  Update and Delete  
	 */
	static StudySEI studyService = null;
	@Test
	public void TestStudyOrganization(){
		//Test Study Organization begin.
		String studyNumber =  ServiceProperties.getProperty(ServiceProperties.TEST_STUDY_NUMBER);
		StudyIdentifier studyId = new StudyIdentifier();
		studyId.setStudyNumber(studyNumber);
		OrganizationIdentifier orgId = new OrganizationIdentifier();
		StudyOrganization newStudyOrganization =new StudyOrganization();
		//Get Study Service
		List<StudyOrganization> lstStudyOrgBeforeAdd = null;
		try {
			studyService = ClientUtils.getStudyService();
		} catch (MalformedURLException e) {
			Assert.fail("Malformed URL getting service");
		}
		Study studyBeforeAdd = null;

		//get study organization before add operation begin
		try {
			studyBeforeAdd = studyService.getStudy(studyId);
			lstStudyOrgBeforeAdd = studyBeforeAdd.getStudyOrganizations().getStudyOrganizaton();
			Assert.assertNotNull(studyBeforeAdd);
		} catch (OperationException_Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Assert.fail("Failed to get Study");
			Assert.fail(e.getMessage());
		}
		//get Study Organizaion end.
		
		
		//Add Organization begin.
		orgId.setSiteName(ServiceProperties.getProperty(ServiceProperties.TEST_STUDY_ORG_NAME_TOUPDATE));

		newStudyOrganization.setOrganizationIdentifier(orgId);
		Code codeStudyOrgType = new Code();
		codeStudyOrgType.setCode(ServiceProperties.getProperty(ServiceProperties.TEST_STUDY_ORG_TYPE_CODE));
		newStudyOrganization.setType(codeStudyOrgType);
		newStudyOrganization.setLocalSampleSize(10);
		newStudyOrganization.setPublicInformationFlag(true);
		newStudyOrganization.setReportableFlag(true);
		newStudyOrganization.setReviewBasedEnrollmentFlag(true);
		
		//defining two diffrent userIdentifiers to add
		UserIdentifier usr1LogNameNewEnroll = new UserIdentifier();
		usr1LogNameNewEnroll.setUserLoginName(
				ServiceProperties.getProperty(
						ServiceProperties.TEST_STUDY_ORG_NEWENROLL_NOTIFICTO_LOGNAME_USR1));
		newStudyOrganization.getNewEnrollNotificationTo().add(
				usr1LogNameNewEnroll);
		UserIdentifier usr1LogNameApproveEnroll = new UserIdentifier();
		usr1LogNameApproveEnroll.setUserLoginName(
				ServiceProperties.getProperty(
						ServiceProperties.TEST_STUDY_ORG_APPRENROLL_NOTIFICTO_LOGNAME_USR1));
		newStudyOrganization.getApproveEnrollNotificationTo()
				.add(usr1LogNameApproveEnroll);
		
		
		ResponseHolder updateResponse = null;
		//Addding Study Organization after creating and populating StudyOrg object
		try {
			updateResponse = studyService.addStudyOrganization(studyId,
					newStudyOrganization);
			Assert.assertTrue("Issue found adding study organization", updateResponse.getIssues().getIssue().size() == 0);
		} catch (OperationException_Exception e1) {
			Assert.fail("Failed to Add Study Org");
			Assert.fail(e1.getMessage());
			
		} catch (OperationRolledBackException_Exception e1) {
			TestUtils.handleOperationRolledbackException(e1);
			Assert.fail("Failed to Add Study Org");
			Assert.fail(e1.getMessage());
		}
		
		//Getting Study Org in study after adding it to study.
		try {								
			Study studyAfterAdd = 
				studyService.getStudy(studyId);
		
			boolean boolIsStdOrgAdded = false;
			StudyOrganization studyOrgAfterAdd = null;

			List<StudyOrganization> lstStudyOrgAfterAdd = 
				studyAfterAdd.getStudyOrganizations().getStudyOrganizaton();
			Assert.assertEquals(lstStudyOrgBeforeAdd.size()+1, 
					lstStudyOrgAfterAdd.size());
			
			//to check if study org added
			for(int x =0; x < lstStudyOrgAfterAdd.size(); x++){
				studyOrgAfterAdd = lstStudyOrgAfterAdd.get(x);
				boolIsStdOrgAdded = isStudyOrgEqualCompare(studyOrgAfterAdd,
						newStudyOrganization);
				if(boolIsStdOrgAdded)
				{
					break;
				}
				
			}
			Assert.assertTrue(boolIsStdOrgAdded);
			
			//Add Organization end
			
			//update organization begin
			updateResponse = null;
			
			orgId.setSiteName(ServiceProperties.getProperty(ServiceProperties.TEST_STUDY_ORG_NAME_TOUPDATE));
			newStudyOrganization.setOrganizationIdentifier(orgId);
			
			Code codeStudyOrgTypeUpdate = new Code();
			codeStudyOrgTypeUpdate.setCode(
					ServiceProperties.getProperty(
							ServiceProperties.TEST_STUDY_ORG_TYPE_CODE_TOUPDATE));
			newStudyOrganization.setType(codeStudyOrgTypeUpdate);
			
			newStudyOrganization.setLocalSampleSize(100);
			
			newStudyOrganization.setPublicInformationFlag(false);
			newStudyOrganization.setReportableFlag((false));
			newStudyOrganization.setReviewBasedEnrollmentFlag(false);
			
			//defining two diffrent userIdentifiers to add
			UserIdentifier usr1LogNameUpdateNewEnroll = new UserIdentifier();
			usr1LogNameUpdateNewEnroll.setUserLoginName(
					ServiceProperties.getProperty(
							ServiceProperties.TEST_STUDY_ORG_NEWENROLL_NOTIFICTO_LOGNAME_USR1));
			
			UserIdentifier usr1LogNameUpdateApproveEnroll = new UserIdentifier();
			usr1LogNameUpdateApproveEnroll.setUserLoginName(
					ServiceProperties.getProperty(
							ServiceProperties.TEST_STUDY_ORG_APPRENROLL_NOTIFICTO_LOGNAME_USR1));
			
		
			Study studyAfterUpdate= null;
			//Update StudyOrganization Begin
			try {
				updateResponse = studyService.updateStudyOrganization(studyId,
						newStudyOrganization);
				
				Assert.assertNotNull(updateResponse);
				
				studyAfterUpdate = 
					studyService.getStudy(studyId);
				
			} catch (OperationRolledBackException_Exception e1) {
				TestUtils.handleOperationRolledbackException(e1);
				Assert.fail("Failed to update StudyOrganization");
				Assert.fail(e1.getMessage());
			}
			//Checking whether StudyOrg updated or not.
			boolean boolIsStdOrgUpdated= false;
			StudyOrganization stuOrgAfterUpdate = null;
			List<StudyOrganization> lstStudyOrgAfterUpdate = 
				studyAfterUpdate.getStudyOrganizations().getStudyOrganizaton();
			Assert.assertEquals(lstStudyOrgAfterAdd.size(), 
					lstStudyOrgAfterUpdate.size());
			//comparison loop after update, if study updated or not.
			
			for(int x =0; x < lstStudyOrgAfterUpdate.size(); x++){
				stuOrgAfterUpdate = lstStudyOrgAfterUpdate.get(x);
				boolIsStdOrgUpdated = isStudyOrgEqualCompare(stuOrgAfterUpdate,
						newStudyOrganization);
				if(boolIsStdOrgUpdated){
					boolIsStdOrgUpdated = true;
					break;	
				}
								
					
			}
			Assert.assertTrue(boolIsStdOrgUpdated);
			
			//Update Organization end.
			
			//Remove Organization begin
			updateResponse = null;
			Study studyAfterDelete = null;
			if(studyAfterAdd != null){
				try {
					updateResponse = studyService.removeStudyOrganization(studyId,
							newStudyOrganization.getOrganizationIdentifier());
					Assert.assertNotNull(updateResponse);
					studyAfterDelete = 
						studyService.getStudy(studyId);
				} catch (OperationRolledBackException_Exception e) {
					TestUtils.handleOperationRolledbackException(e);
					Assert.fail("Failed to Remove Study Organization.");				
					Assert.fail(e.getMessage());
				}
				if(studyAfterDelete != null ){
					List<StudyOrganization> lstStudyOrgAfterRemove = 
						studyAfterDelete.getStudyOrganizations().getStudyOrganizaton();
					Assert.assertEquals(lstStudyOrgAfterAdd.size()-1,
							lstStudyOrgAfterRemove.size());
				}
			}
			
		} catch (OperationException_Exception e) {

			Assert.fail(e.getMessage());
		}
		
		//Remove StudyOrganization end
	}
	/**
	 * Comparing Study Organization. If compare success,
	 * return true else false.
	 * @param studyOrg1
	 * @param studyOrg2
	 * @return
	 */
	public boolean isStudyOrgEqualCompare(StudyOrganization studyOrg1, 
			StudyOrganization studyOrg2){
		boolean boolIsCompareSuccess = false;
		OrganizationIdentifier OrgId1 = studyOrg1.getOrganizationIdentifier();
		OrganizationIdentifier OrgId2 = studyOrg2.getOrganizationIdentifier();
		
		Code studyOrgType1 = studyOrg1.getType();
		Code studyOrgType2 = studyOrg2.getType();
		
		Integer integerLocalSampleSize1 = studyOrg1.getLocalSampleSize();
		Integer integerLocalSampleSize2 = studyOrg2.getLocalSampleSize();
		
		boolean boolIsSiteInfoPublic1 = studyOrg1.isPublicInformationFlag();
		boolean boolIsSiteInfoPublic2 = studyOrg2.isPublicInformationFlag();
		
		boolean boolIsSiteInfoReportable1 = studyOrg1.isReportableFlag();
		boolean boolIsSiteInfoReportable2 = studyOrg2.isReportableFlag();
		
		boolean boolIsReviewBasedEnrollmentFlag1 =
			studyOrg1.isReviewBasedEnrollmentFlag();
		boolean boolIsReviewBasedEnrollmentFlag2 =
			studyOrg2.isReviewBasedEnrollmentFlag();
		
		List<UserIdentifier> lstUserIdNewEnrollNotificationTo1 =
			studyOrg1.getNewEnrollNotificationTo();
		List<UserIdentifier> lstUserIdNewEnrollNotificationTo2 = 
			studyOrg2.getNewEnrollNotificationTo();
		
		List<UserIdentifier> lstUserIdApproveEnrollNotificationTo1 =
			studyOrg1.getApproveEnrollNotificationTo();
		List<UserIdentifier> lstUserIdApproveEnrollNotificationTo2 =
			studyOrg2.getApproveEnrollNotificationTo();
		
		//compare sitename
		if((OrgId1.getSiteName() != null) && (OrgId2.getSiteName() != null)){
			if(OrgId1.getSiteName().equalsIgnoreCase(OrgId2.getSiteName())){
				boolIsCompareSuccess = true;
			}
			else{ 
				return false;
			}
		}
		
		//compare site type
		if(ifCodeEquals(studyOrgType1, studyOrgType2)){
			boolIsCompareSuccess = true;
		}
		else{ 
			return false;
		}
		//compare local sample size
		if(integerLocalSampleSize1.equals(integerLocalSampleSize2)){
			boolIsCompareSuccess = true;
		}
		else{ 
			return false;
		}
		//compare ifSiteInfoPublic
		if(boolIsSiteInfoPublic1 == boolIsSiteInfoPublic2){
			boolIsCompareSuccess = true;
		}
		else{
			return false;
		}
		//compare if site info reportable
		if(boolIsSiteInfoReportable1 == boolIsSiteInfoReportable2){
			boolIsCompareSuccess = true;
		}
		else{ 
			return false;
		}
		//compare if enrollment review based
		if(boolIsReviewBasedEnrollmentFlag1 == 
			boolIsReviewBasedEnrollmentFlag2){
			boolIsCompareSuccess = true;
		}
		else{ 
			return false;
		}
		
		//compare user Identifiers in list for New Enrollment notification sent to
		
		if(lstUserIdNewEnrollNotificationTo1.size() == 
			lstUserIdNewEnrollNotificationTo2.size()){
			for(int x= 0; x < lstUserIdNewEnrollNotificationTo1.size(); x++ ){
				boolIsCompareSuccess = 
					isUserIdEqual(lstUserIdNewEnrollNotificationTo1.get(x), 
							lstUserIdNewEnrollNotificationTo2.get(x));
				if(boolIsCompareSuccess == false){
					break;
				}
			}
		}
		else{ 
			return false;
		}
		
		//compare user Identifiers in list for Approve Enrollment 
		//notification sent to
		if(lstUserIdApproveEnrollNotificationTo1.size() == 
			lstUserIdApproveEnrollNotificationTo2.size()){
			for(int x= 0; x < lstUserIdNewEnrollNotificationTo1.size(); x++ ){
				boolIsCompareSuccess = 
					isUserIdEqual(lstUserIdApproveEnrollNotificationTo1.get(x), 
							lstUserIdApproveEnrollNotificationTo2.get(x));
				if(boolIsCompareSuccess == false){
					break;
				}
			}
		}
		else{ 
			return false;
		}
		
		return boolIsCompareSuccess;
	}
	/*** 
	 * Compare User Identifiers.
	 * if success, return true, else return false
	 * @param userIdentifier1
	 * @param userIdentifier2
	 * @return
	 */
	public boolean isUserIdEqual(UserIdentifier userIdentifier1,
			UserIdentifier userIdentifier2){
		boolean boolIfUserExists= false;
		
		if(userIdentifier1.getUserLoginName().
				equals(userIdentifier2.getUserLoginName())){
			return true;
		}
		else if(userIdentifier1.getFirstName().
					equalsIgnoreCase(userIdentifier2.getFirstName()) 
					&& (userIdentifier1.getLastName().
							equalsIgnoreCase(userIdentifier2.getLastName()))){
				boolIfUserExists = true;
		}
		
		return boolIfUserExists;
	}
	/**Compare codes, if success, return true
	 * else false  
	 * @param code1
	 * @param code2
	 * @return
	 */
	 public boolean ifCodeEquals(Code code1, Code code2){
		if ((code1 ==null && code2 != null) ||
				((code1 !=null && code2==null))){
			return false;
		}
		boolean boolIfCodeUpdated = false;
		String ccode1 =code1.getCode();
		String ccode2 =code2.getCode();
		if((ccode1 != null)&&(ccode2 != null)){
			if(ccode1.equalsIgnoreCase(ccode2) ){
				boolIfCodeUpdated = true;
			}
		}
		return boolIfCodeUpdated;	
	}
}
