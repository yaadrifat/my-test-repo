package com.velos.webservices.client;

import org.junit.Assert;
import org.junit.Test;

import com.velos.services.Code;
import com.velos.services.OperationException_Exception;
import com.velos.services.OperationRolledBackException_Exception;
import com.velos.services.OrganizationIdentifier;
import com.velos.services.Patient;
import com.velos.services.PatientDemographics;
import com.velos.services.PatientDemographicsSEI;
import com.velos.services.PatientIdentifier;
import com.velos.services.PatientOrganization;
import com.velos.services.PatientOrganizations;
import com.velos.services.ResponseHolder;
import com.velos.services.SpecialityAccess;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;

public class CreatePatientDemographicsTest {
	
	PatientDemographics patientdemographics = new PatientDemographics();
	PatientDemographicsSEI patientDemographicsSEI;
	Patient patient = new Patient();
	PatientIdentifier patientIdentifier = new PatientIdentifier();
	
	
	
	
@Test
 public void createpatientdemographicsTest()
{
	patientdemographics.setAddress1("315 E san Fernando Street");
	patientdemographics.setAddress2("Apt. #25");
	patientdemographics.setCity("San Jose");
	patientdemographics.setCountry("USA");
	patientdemographics.setCounty("Santa Clara");

	GregorianCalendar cal = new GregorianCalendar(); 
	cal.set(Calendar.YEAR, 1954); 
	cal.set(Calendar.MONTH, 7); 
	cal.set(Calendar.DAY_OF_MONTH, 20); 
	XMLGregorianCalendar dateOfBirth = TestUtils.dateToXMLGregorian(cal.getTime());	
	patientdemographics.setDateOfBirth(dateOfBirth);
	
	patientdemographics.setEMail("ravi@gmail.com");
	Code cethnicity = new Code();
	cethnicity.setCode(ServiceProperties.getProperty(ServiceProperties.PAT_DEMOGRAPHICS_CODE_ETHNICITY));
	patientdemographics.setEthnicity(cethnicity);
	patientdemographics.setFirstName("Ravi");
	
    Code cdGender = new Code();
    cdGender.setCode(ServiceProperties.getProperty(ServiceProperties.PAT_DEMOGRAPHICS_CODE_GENDER));
	patientdemographics.setGender(cdGender);
	patientdemographics.setHomePhone("408404484");
	patientdemographics.setLastName("Gupta");
	patientdemographics.setMiddleName("Kumar");
	OrganizationIdentifier oIdentifier = new OrganizationIdentifier();
	oIdentifier.setSiteName(ServiceProperties.getProperty(ServiceProperties.TEST_ORGNAME));
	
	patientdemographics.setOrganization(oIdentifier);
	patientdemographics.setPatFacilityId("123456");
	patientdemographics.setPatientCode("408408");
	PatientIdentifier patientIdentifier = new PatientIdentifier();
	patientIdentifier.setPatientId("408408");
	patientdemographics.setPatientIdentifier(patientIdentifier);
	Code cRace = new Code();
	cRace.setCode(ServiceProperties.getProperty(ServiceProperties.PAT_DEMOGRAPHICS_CODE_RACE));
	patientdemographics.setRace(cRace);
	
	patientdemographics.setState("California");
	Code cSurvivalStatus = new Code();
	cSurvivalStatus.setCode(ServiceProperties.getProperty(ServiceProperties.PAT_DEMOGRAPHICS_CODE_SURVIVALSTATUS));
	patientdemographics.setSurvivalStatus(cSurvivalStatus);
	patientdemographics.setWorkPhone("408484848");
	patientdemographics.setZipCode("95112");
	
	
	patient.setPatientDemographics(patientdemographics);
	
	patient.setPatientIdentifier(patientIdentifier);
	
	PatientOrganization pOrganization = new PatientOrganization();
	pOrganization.setAccess(null);
	pOrganization.setDefault(true);

	pOrganization.setFacilityID(null);
	pOrganization.setOrganizationID(oIdentifier);
	pOrganization.setOtherProvider("XYZ");
	pOrganization.setProvider(null);
	GregorianCalendar cal1 = new GregorianCalendar();
	cal1.set(Calendar.YEAR, 2009);
	cal1.set(Calendar.MONTH, 10);
	cal1.set(Calendar.DAY_OF_MONTH, 10);
	XMLGregorianCalendar registrationDate = TestUtils.dateToXMLGregorian(cal.getTime());	
	pOrganization.setRegistrationDate(registrationDate);
	SpecialityAccess SA = new SpecialityAccess();
	PatientOrganizations patientorganizations = new PatientOrganizations();
	patientorganizations.getOrganization().add(pOrganization);
	patient.setPatientOrganizations(patientorganizations);

	
	ResponseHolder response = null;
	try{
		patientDemographicsSEI = ClientUtils.getPatientDemographicsService();
		try {
			response = patientDemographicsSEI.createPatient(patient);
		} catch (OperationRolledBackException_Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}catch(OperationException_Exception e)
	{
		Assert.assertNotNull(null);
		e.printStackTrace();
	} catch (MalformedURLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}
}


