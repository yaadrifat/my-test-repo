package com.velos.webservices.client;

import java.net.MalformedURLException;
import java.util.List;

import org.junit.Test;

import junit.framework.Assert;

import com.velos.services.Code;
import com.velos.services.OperationException_Exception;
import com.velos.services.OperationRolledBackException_Exception;
import com.velos.services.ResponseHolder;
import com.velos.services.Study;
import com.velos.services.StudyIdentifier;
import com.velos.services.StudySEI;
import com.velos.services.StudyService;
import com.velos.services.StudyTeamMember;
import com.velos.services.UserIdentifier;
import com.velos.services.UserType;

/**
 * @author dylan
 *
 */

public class StudyTeamMemberTest {
	
	//static StudyService service = null;
	static StudySEI studyService = null;
	/**
	 * We test team member create, update and remove in one method
	 * by performing the operation, getting the study, performing 
	 * the operation again.
	 */
	
	@Test
	public void TestStudyTeamMember() {
		
		//Add Study Team Member Begin
		StudyIdentifier studyId = new StudyIdentifier();
		ResponseHolder updateResponse = null;
		boolean boolifUserAlreadyExists = false; 
		Study studyBeforeCreateTeamMember = null;
		String studyNumber =  ServiceProperties.getProperty(ServiceProperties.TEST_STUDY_NUMBER);
	
		studyId.setStudyNumber(studyNumber);
	
		try {
			studyService = ClientUtils.getStudyService();
		} catch (MalformedURLException e1) {
			Assert.fail("Malformed URL getting service");
		}
		//check if study is not null  for create study
		try {
 			studyBeforeCreateTeamMember = studyService.getStudy(studyId);
			Assert.assertTrue(studyBeforeCreateTeamMember != null);
		} catch (OperationException_Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace(); 
		}
		
//		try{
		//adding a new study team member
 		UserIdentifier usrIdToAdd = new UserIdentifier();
 		UserIdentifier usrIdToUpdate = new UserIdentifier();
 		//usrIdToAdd.setSystemId(ServiceProperties.getTestStudySysId());
		usrIdToAdd.setUserLoginName(ServiceProperties.
				getProperty(ServiceProperties.TEST_USERNAME));
		usrIdToAdd.setLastName(ServiceProperties.
				getProperty(ServiceProperties.TEST_LASTNAME));
 		usrIdToAdd.setFirstName(ServiceProperties.
 				getProperty(ServiceProperties.TEST_FIRSTNAME));
		StudyTeamMember stuTeamMember =new StudyTeamMember();
		
		stuTeamMember.setUserId(usrIdToAdd);
		
		stuTeamMember.setUserType(UserType.DEFAULT);
		
		Code teamRoleCode = new Code();
		teamRoleCode.setCode(ServiceProperties.
 				getProperty(ServiceProperties.
				TEST_TEAM_ROLE_CODE));
		Code teamMemberStatusCode = new Code();
		teamMemberStatusCode.setCode("Active");
		stuTeamMember.setTeamRole(teamRoleCode);
		stuTeamMember.setStatus(teamMemberStatusCode);
		
		Study studyBeforeTeamMemberInsert = null;
		
		 //check if study team member to insert is not already present in study
		 try {
			studyBeforeTeamMemberInsert = studyService.getStudy(studyId);
		} catch (OperationException_Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
		
		boolean boolIfTeamMemToAddExists = false;
		Assert.assertNotNull("Study not fetching, the member not already present",
				studyBeforeTeamMemberInsert);
		List <StudyTeamMember> lstStudyTeamMemberFrmStudy =
			studyBeforeTeamMemberInsert.getStudyTeamMembers().getStudyTeamMember();
		for (int x = 0; x < lstStudyTeamMemberFrmStudy.size(); x++ ) {
		
			UserIdentifier usrIdfrmStudyBeforeAdd = 
				lstStudyTeamMemberFrmStudy.get(x).getUserId();
			boolIfTeamMemToAddExists = 
				ifUserAlreadyExists( usrIdToAdd , 
					usrIdfrmStudyBeforeAdd );
			if ( boolIfTeamMemToAddExists ){
				boolIfTeamMemToAddExists = true;
				updateResponse = null;
				 //if study team member already present then remove  it 
				 try {
					 updateResponse = studyService.
					 removeStudyTeamMember(studyId, usrIdToAdd);
					 Assert.assertTrue(updateResponse != null);
					 //list of StudyTeamMEmebers after remove
					 studyBeforeTeamMemberInsert = studyService.
					 								getStudy(studyId);
					 lstStudyTeamMemberFrmStudy =
							 studyBeforeTeamMemberInsert.getStudyTeamMembers()
							 .getStudyTeamMember();
					 
				} catch (Exception e) {
					// TODO Auto-generated catch block
					
					e.printStackTrace();
				}
				
			}
		}
		updateResponse = null; 
		//Add StudyTeamMember
		//Known Issue: Ignoring Study Team Status for Study Team Status. 
		//StudyTeamStatus is not being  pulled back from database as of now.
		try { 
			updateResponse = studyService.addStudyTeamMember(
					studyId, stuTeamMember, true);
			Assert.assertTrue(updateResponse != null);
		}
		catch(Exception e){
			Assert.fail(e.getMessage());
			Assert.fail("Failed to Add StudyTeamMember");
		}
			 
			//check if study team member added successfully
			 Study studyAfterStdyTeamMemAdd = null;
			 //updateResponse = null;
			try {
				studyAfterStdyTeamMemAdd = studyService.getStudy(studyId);
				Assert.assertTrue(studyAfterStdyTeamMemAdd != null);
			} catch (OperationException_Exception e) {
				Assert.fail("Failed to Get Study with Added StudyTeamMember");
				e.printStackTrace();
			}
		
			boolean boolIsTeamMemAdded = false;
			List<StudyTeamMember> lstStudyTeamMemberAfterAdd =
				studyAfterStdyTeamMemAdd.getStudyTeamMembers().getStudyTeamMember();
			
			Assert.assertEquals(
					"Team member list size should increase by one after adding",
					lstStudyTeamMemberAfterAdd.size(), 
					lstStudyTeamMemberFrmStudy.size() + 1);
			
 			for(int x=0; x < lstStudyTeamMemberAfterAdd.size();x++){
				UserIdentifier usrIdAfterAdd = 
					lstStudyTeamMemberAfterAdd.get(x).getUserId();
				UserType userTypeAfterAdd = 
					lstStudyTeamMemberAfterAdd.get(x).getUserType();
				Code teamRoleCodeAfterAdd = 
					lstStudyTeamMemberAfterAdd.get(x).getTeamRole();
				Code teamMemberStatusCodeAfterAdd = 
					lstStudyTeamMemberAfterAdd.get(x).getStatus();
				boolifUserAlreadyExists = 
					ifUserAlreadyExists(
							usrIdToAdd,
							usrIdAfterAdd );
				if(boolifUserAlreadyExists){
					
					if((stuTeamMember.getUserType() != null) 
							&& (stuTeamMember.getStatus() != null) 
							&& (userTypeAfterAdd != null) ){
						
						if(ifCodeUpdated(teamRoleCodeAfterAdd,teamRoleCode) 
								&& ifCodeUpdated(teamMemberStatusCodeAfterAdd,
										teamMemberStatusCode)
								&& (userTypeAfterAdd
										.equals(stuTeamMember.getUserType())) ){
							
								boolIsTeamMemAdded = true;
								break;
						}
				}
			}
		}
			Assert.assertTrue(boolIsTeamMemAdded);
			//Add StudyTeamMember End
			
			//update StudyTeamMember Begin
			usrIdToUpdate.setUserLoginName(ServiceProperties.
					getProperty(ServiceProperties.
							TEST_USERNAME));
			usrIdToUpdate.setLastName(ServiceProperties.
					getProperty(ServiceProperties.
							TEST_LASTNAME)+"_updated");
			usrIdToUpdate.setFirstName(ServiceProperties.
					getProperty(ServiceProperties.
							TEST_FIRSTNAME)+"_updated");
			
			StudyTeamMember stuTeamMemberToUpdate =new StudyTeamMember();
			
			stuTeamMemberToUpdate.setUserId(usrIdToUpdate);
			stuTeamMemberToUpdate.setUserType(UserType.DEFAULT);
			
			Code teamRoleCodetoUpdate = new Code();
			teamRoleCodetoUpdate.setCode(ServiceProperties.
					getProperty(ServiceProperties.
							TEST_TEAM_ROLE_CODE_TO_UPDATE));
			
			Code teamMemberStatusCodeToUpdate = new Code();
			teamMemberStatusCodeToUpdate.setCode("Deactivated");
			
			stuTeamMemberToUpdate.setTeamRole(teamRoleCodetoUpdate);
			stuTeamMemberToUpdate.setStatus(teamMemberStatusCodeToUpdate);
			
			 //if StudyTeamMember  to update already exists
			boolean boolIfTeamMemToUpdateAlreadyExists = false;
			List<StudyTeamMember> lstStudyTeamMemberfrmStudyBeforeUpdate
								= studyAfterStdyTeamMemAdd.getStudyTeamMembers()
								.getStudyTeamMember();
			
			for(int x=0; x < lstStudyTeamMemberfrmStudyBeforeUpdate.size();x++)
			{
				UserIdentifier usrIdBeforeUpdate = 
					lstStudyTeamMemberfrmStudyBeforeUpdate.get(x).getUserId();
				
				UserType userTypeBeforeUpdate = 
				   lstStudyTeamMemberfrmStudyBeforeUpdate.get(x).getUserType();
				
				Code teamRoleCodeBeforeUpdate = 
					lstStudyTeamMemberfrmStudyBeforeUpdate.get(x).getTeamRole();
				Code teamMemberStatusCodeBeforeUpdate = 
					lstStudyTeamMemberfrmStudyBeforeUpdate.get(x).getStatus();
				
					ifUserAlreadyExists(usrIdToUpdate, usrIdBeforeUpdate );
				if(boolIfTeamMemToUpdateAlreadyExists){
						if(ifCodeUpdated(teamRoleCodetoUpdate,teamRoleCodeBeforeUpdate) 
								&& ifCodeUpdated(teamMemberStatusCodeToUpdate,
										teamMemberStatusCodeBeforeUpdate)
								&& userTypeBeforeUpdate.
								equals(stuTeamMemberToUpdate.getUserType())){
									boolIfTeamMemToUpdateAlreadyExists = true;
							//Remove StudyTeamMember Begin
							updateResponse = null;
							try {
								 updateResponse = studyService.removeStudyTeamMember(
										 studyId, usrIdToUpdate);
								 Assert.assertTrue(updateResponse != null);
								 boolIfTeamMemToUpdateAlreadyExists = false;
								 break;
								 
							} catch (Exception e) {
								Assert.fail("Failed to remove StudyTeamMember");
								
								e.printStackTrace();
							}
							boolIfTeamMemToUpdateAlreadyExists = false;
						break;
					
				}
				}
			}
			Assert.assertFalse(boolIfTeamMemToUpdateAlreadyExists);
			updateResponse = null;
			//Remove StudyTeamMember end
			
			//Update StudyTeamMember begin
			Study studyAfterUpdate = null;
			//if StudyTeamMember does exist, update it.
			try {
				updateResponse = studyService.updateStudyTeamMember(
						studyId, 
						stuTeamMemberToUpdate);
				Assert.assertTrue(updateResponse != null);		

				
				//check if StudyTeamMember updated or not.
				studyAfterUpdate = 
						studyService.getStudy(studyId);
			} catch (OperationException_Exception e) {
				Assert.fail(e.getMessage());
				Assert.fail("Failed to Update StudyTeamMember");
			}
			catch (OperationRolledBackException_Exception e) {
				Assert.fail(e.getMessage());
				Assert.fail("Failed to Update StudyTeamMember");
				TestUtils.handleOperationRolledbackException(e);
			}
				
			Boolean boolIsTeamMemUpdated = false;
			
			List<StudyTeamMember> lstStudyTeamMemberAfterUpdate =
						studyAfterUpdate.getStudyTeamMembers().getStudyTeamMember();
			
			//Check whether the team member has been inserted
			for(int x=0; x < lstStudyTeamMemberAfterUpdate.size(); x++){
			
				UserIdentifier usrIdAfterUpdate =
					lstStudyTeamMemberAfterUpdate.get(x).getUserId();
				UserType userTypeAfterUpdate =
					lstStudyTeamMemberAfterUpdate.get(x).getUserType();
				
				Code teamRoleCodeAfterUpdate = 
					lstStudyTeamMemberAfterUpdate.get(x).getTeamRole();
				Code teamMemberStatusCodeAfterUpdate = 
					lstStudyTeamMemberAfterUpdate.get(x).getStatus();
				boolifUserAlreadyExists = 
					ifUserAlreadyExists( 
						usrIdToUpdate, 
						usrIdAfterUpdate );
				
				//If teamMember added, compare fields
				if(boolifUserAlreadyExists){
					if((stuTeamMemberToUpdate.getUserType() != null) 
							&& (stuTeamMember.getStatus() != null) 
							&& (userTypeAfterUpdate != null)){
						
						if(ifCodeUpdated(teamRoleCodetoUpdate,teamRoleCodeAfterUpdate) 
								&& ifCodeUpdated(teamMemberStatusCodeToUpdate,
										teamMemberStatusCodeAfterUpdate)
								&& userTypeAfterUpdate.
								equals(stuTeamMemberToUpdate.getUserType())){
							boolIsTeamMemUpdated = true;
					break;
					}
					}
				}
			}
			Assert.assertTrue(boolIsTeamMemUpdated);
			//Team Member Update End
			
			 //Remove StudyTeamMember
			 Study studyAfterRemoveStdTeamMember = null;
			
			 updateResponse = null;
			 
			 try{
				 updateResponse = studyService.removeStudyTeamMember(studyId, 
						 usrIdToUpdate);
				 Assert.assertTrue(updateResponse != null);
			 	}
				catch(Exception e){
					Assert.fail("Failed to remove StudyTeamMember");
					Assert.fail(e.getMessage());
				}
				try{
					studyAfterRemoveStdTeamMember = studyService.getStudy(studyId);
					Assert.assertTrue(studyAfterRemoveStdTeamMember != null);
					
				} 
				catch (OperationException_Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
				}
					 
					//check if StudyTeamMember removed or not
				
					boolean boolIsTeamMemberExistAfterDelete = false;
					List<StudyTeamMember> lstStudyTeamMemberAfterRemove =
						studyAfterRemoveStdTeamMember.getStudyTeamMembers()
						.getStudyTeamMember();

					
					if(!lstStudyTeamMemberAfterRemove.isEmpty()){
					   for(int y=0; y < lstStudyTeamMemberAfterRemove.size(); y++){
						UserIdentifier usrIdAfterRemove= 
							lstStudyTeamMemberFrmStudy.get(y).getUserId();
						UserType userTypeAfterRemove = 
							lstStudyTeamMemberAfterRemove.get(y).getUserType();
						
						Code teamRoleCodeAfterRemove = 
							lstStudyTeamMemberAfterRemove.get(y).getTeamRole();
						Code teamMemberStatusCodeAfterRemove = 
							lstStudyTeamMemberAfterRemove.get(y).getStatus();
						boolifUserAlreadyExists = 
							ifUserAlreadyExists(usrIdToUpdate, 
									usrIdAfterRemove );
						if(boolifUserAlreadyExists){
							if((stuTeamMemberToUpdate.getUserType() != null) 
									&& (stuTeamMember.getStatus() != null) 
									&& (userTypeAfterRemove != null) ){
								
								if(ifCodeUpdated(teamRoleCodetoUpdate,teamRoleCodeAfterRemove) 
										&& ifCodeUpdated(teamMemberStatusCodeToUpdate,
												teamMemberStatusCodeAfterRemove)
										&& userTypeAfterRemove.
										equals(stuTeamMemberToUpdate.getUserType())){
									
									boolIsTeamMemberExistAfterDelete = true;
									break;
								}
							}
						}
					}
					}
					Assert.assertFalse(boolIsTeamMemberExistAfterDelete);
		//StudyTeamMember Removed
	}



/**
 * Method to check if two User Identifiers are Identical.
 *  If yes, then return true.
 */
public boolean ifUserAlreadyExists(UserIdentifier userIdentifier,
		UserIdentifier userIdentifier1)
{
	boolean boolIfUserExists= false;
	
	if(userIdentifier1.getUserLoginName().
			equals(userIdentifier.getUserLoginName())){
		return true;
	}
	else if(userIdentifier1.getFirstName().
				equalsIgnoreCase(userIdentifier.getFirstName()) 
				&& (userIdentifier1.getLastName().
						equalsIgnoreCase(userIdentifier.getLastName()))){
			boolIfUserExists = true;
	}
	
	return boolIfUserExists;
}
/**
 * Method to check if two Codes are Identical. 
 * If yes, then return true.
 */	
public boolean ifCodeUpdated(Code code1, Code code2){
	boolean boolifCodeUpdated = false;
	String ccode1 =code1.getCode();
	String ccode2 =code2.getCode();
	if(ccode1.equals(ccode2) ){
		boolifCodeUpdated = true;
	}
	return boolifCodeUpdated;	
}
}

