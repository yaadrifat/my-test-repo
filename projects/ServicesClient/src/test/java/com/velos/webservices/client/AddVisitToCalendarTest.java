package com.velos.webservices.client;

import org.junit.Test;

import junit.framework.Assert;

import com.velos.services.AddVisitsToStudyCalendar;
import com.velos.services.CalendarEvents;
import com.velos.services.CalendarIdentifier;
import com.velos.services.CalendarVisit;
import com.velos.services.CalendarVisitSummary;
import com.velos.services.CalendarVisits;
import com.velos.services.Duration;
import com.velos.services.ParentIdentifier;
import com.velos.services.ResponseHolder;
import com.velos.services.StudyCalendarSEI;
import com.velos.services.TimeUnits;
import com.velos.services.VisitIdentifier;

public class AddVisitToCalendarTest {

	AddVisitsToStudyCalendar addVisitsToStudyCalendar = new AddVisitsToStudyCalendar();
	StudyCalendarSEI studyCalendarSEI;
	@Test
	
	public void visitstostudycalendar()
	
	{
		CalendarIdentifier calendarIdentifier = new CalendarIdentifier();
		calendarIdentifier.setOID(ServiceProperties.getProperty(ServiceProperties.STUDY_CALENDAR_OID));
		VisitIdentifier visitIdentifier = new VisitIdentifier();
		visitIdentifier.setOID(ServiceProperties.getProperty(ServiceProperties.STUDY_CALENDAR_VISIT_OID));
		
		
		CalendarVisits calendarVisits = new CalendarVisits();
		CalendarVisit calendarVisit = new CalendarVisit();
		CalendarVisitSummary visitSummary = new CalendarVisitSummary();
	
		
		
		Duration absoluteInterval = new Duration();
		absoluteInterval.setUnit(TimeUnits.DAY);
		visitSummary.setAbsoluteInterval(absoluteInterval);
		
		visitSummary.setNoIntervalDefined(true);
		Duration relativevisitinterval = new Duration();
		relativevisitinterval.setUnit(TimeUnits.DAY);
		visitSummary.setRelativeVisitInterval(relativevisitinterval);
		
		visitSummary.setVisitIdentifier(visitIdentifier);
		visitSummary.setVisitSequence(10);
		
		Duration visitwindowafter = new Duration();
		visitwindowafter.setUnit(TimeUnits.DAY);
		visitSummary.setVisitWindowAfter(visitwindowafter);
		
		Duration visitwindowbefore = new Duration();
		visitwindowbefore.setUnit(TimeUnits.DAY);
		visitSummary.setVisitWindowBefore(visitwindowbefore);
		
		visitSummary.setVisitName(ServiceProperties.getProperty(ServiceProperties.STUDY_CALENDAR_VISIT_NAME));
	calendarVisit.setCalendarVisitSummary(visitSummary); 
		calendarVisits.getVisit().add(calendarVisit);
		
		
		
		
		
		
	
try{
		
		ResponseHolder response = null;
	studyCalendarSEI	 = ClientUtils.getStudyCalendar();
	response = studyCalendarSEI.addVisitsToStudyCalendar(calendarIdentifier, calendarVisits);
		
	}catch(Exception e)
	{
		Assert.assertNotNull(visitIdentifier);
		e.printStackTrace();
	}}}

	
	

