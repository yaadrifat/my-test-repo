package com.velos.webservices.client;

import junit.framework.Assert;

import org.junit.Test;

import com.velos.services.AddEventsToStudyCalendarVisit;
import com.velos.services.CalendarEvent;
import com.velos.services.CalendarEventSummary;
import com.velos.services.CalendarEvents;
import com.velos.services.CalendarIdentifier;
import com.velos.services.Code;
import com.velos.services.Cost;
import com.velos.services.Costs;
import com.velos.services.Duration;
import com.velos.services.EventIdentifier;
import com.velos.services.OperationException;
import com.velos.services.OperationException_Exception;
import com.velos.services.OrganizationIdentifier;
import com.velos.services.ResponseHolder;
import com.velos.services.StudyCalendarSEI;
import com.velos.services.TimeUnits;
import com.velos.services.VisitIdentifier;

import java.math.BigDecimal;
import java.net.MalformedURLException;
import com.velos.services.OperationException_Exception;
import com.velos.services.OperationRolledBackException_Exception;


public class AddEventtoStudyCalendarVisitTest
{  
	StudyCalendarSEI scalendar; 
	AddEventsToStudyCalendarVisit eventToStudycalendarVisit = new AddEventsToStudyCalendarVisit();
@Test
public void eventtostudycalendarvisit()
{
	
	CalendarIdentifier calendarIdentifer = new CalendarIdentifier();
	VisitIdentifier vIdentifier = new VisitIdentifier();
	String VisitName = ServiceProperties.getProperty(ServiceProperties.STUDY_CALENDAR_VISIT_NAME);// visit name (can be changed)
	calendarIdentifer.setOID(ServiceProperties.getProperty(ServiceProperties.STUDY_CALENDAR_OID));
	vIdentifier.setOID(ServiceProperties.getProperty(ServiceProperties.STUDY_CALENDAR_VISIT_OID));
	EventIdentifier eIdentifier = new EventIdentifier();
	eIdentifier.setOID(ServiceProperties.getProperty(ServiceProperties.STUDY_CALENDAR_VISIT_EVENT_OID));
	CalendarEvent calendarEvent = new CalendarEvent();
	CalendarEvents cEvent = new CalendarEvents();
	
	OrganizationIdentifier oIdentifier = new OrganizationIdentifier();
	
	oIdentifier.setSiteName(ServiceProperties.getProperty(ServiceProperties.TEST_ORGNAME));
	
	CalendarEventSummary calendarEventSummary = new CalendarEventSummary();
	calendarEventSummary.setCPTCode("CPTCalendar");
	Costs costs = new Costs();
	Cost cost = new Cost();
	BigDecimal bg = new BigDecimal("1234.4321");
	cost.setCost(bg);
	Code currency = new Code();
	currency.setCode(ServiceProperties.getProperty(ServiceProperties.STUDY_CALENDAR_CODE_COST_CURRENCY));
	
	Code costType = new Code();
	costType.setCode(ServiceProperties.getProperty(ServiceProperties. STUDY_CALENDAR_CODE_COST_TYPE)); 
	
	cost.setCostType(costType); 
	cost.setCurrency(currency);
	costs.getCost().add(cost);
	
	

	Code coverageType = new Code(); 
	try{
		coverageType.setCode(ServiceProperties.getProperty(ServiceProperties.STUDY_CALENDAR_CODE_COVERAGE_TYPE));
		calendarEventSummary.setCoverageType(coverageType);
	}catch(Exception e)
	{
	Assert.assertNotNull(coverageType);
	e.printStackTrace();
	
	}
	Duration eventDuration = new Duration();
	eventDuration.setUnit(TimeUnits.DAY);
	calendarEventSummary.setEventDuration(eventDuration);
	
	calendarEventSummary.setDescription("FirstCalender");
	calendarEventSummary.setEventIdentifier(eIdentifier);
	calendarEventSummary.setEventName(ServiceProperties.getProperty(ServiceProperties.STUDY_CALENDAR_VISIT_EVENT_NAME));// event name (can be changed)
	
	Duration eventWindowAfter = new Duration(); 
	eventWindowAfter.setUnit(TimeUnits.DAY); 
	eventWindowAfter.setValue(2); 
	calendarEventSummary.setEventWindowAfter(eventWindowAfter);
	
	Duration eventWindowBefore = new Duration();
	eventWindowBefore.setUnit(TimeUnits.DAY);
	eventWindowBefore.setValue(1);
	calendarEventSummary.setEventWindowBefore(eventWindowBefore);
	calendarEventSummary.setFacility(oIdentifier);
	calendarEventSummary.setNotes("Velos");	
	calendarEventSummary.setSequence(10);
	calendarEventSummary.setSiteOfService(oIdentifier);
	cEvent.getEvent().add(calendarEvent);
	calendarEvent.setCosts(costs);
	calendarEvent.setCalendarEventSummary(calendarEventSummary);

	
	try{
		
		ResponseHolder response = null;
	scalendar	 = ClientUtils.getStudyCalendar();
	response = scalendar.addEventsToStudyCalendarVisit(calendarIdentifer, VisitName, vIdentifier,cEvent);
		
	}catch(Exception e)
	{
		Assert.assertNotNull(vIdentifier);
		e.printStackTrace();
	}
	
	
	}
}
