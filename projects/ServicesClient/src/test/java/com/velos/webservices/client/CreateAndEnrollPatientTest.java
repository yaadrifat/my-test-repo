package com.velos.webservices.client;

import java.net.MalformedURLException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.xml.datatype.XMLGregorianCalendar;

import org.junit.Assert;
import org.junit.Test;

import com.velos.services.AccessRights;
import com.velos.services.Code;
import com.velos.services.OperationException_Exception;
import com.velos.services.OperationRolledBackException_Exception;
import com.velos.services.OrganizationIdentifier;
import com.velos.services.Patient;
import com.velos.services.PatientDemographics;
import com.velos.services.PatientEnrollmentDetails;
import com.velos.services.PatientIdentifier;
import com.velos.services.PatientOrganization;
import com.velos.services.PatientOrganizations;
import com.velos.services.ResponseHolder;
import com.velos.services.StudyIdentifier;
import com.velos.services.StudyPatientSEI;

public class CreateAndEnrollPatientTest {
	
	
	StudyPatientSEI studyPatientSEI;
	Patient patient = new Patient();
	StudyIdentifier studyIdentifier = new StudyIdentifier();
	PatientEnrollmentDetails patientEnrollmentDetails = new PatientEnrollmentDetails();
	PatientDemographics patientDemographics = new PatientDemographics();
	PatientOrganization patientOrganization = new PatientOrganization();

@Test
public void createAndEnrollPatient()
{
	patientDemographics.setAddress1("201 S 4th Street");
	patientDemographics.setAddress2("Apt.522");
	patientDemographics.setCity("San Jose");
	patientDemographics.setCountry("USA");
	patientDemographics.setCounty("santa Clara");
	GregorianCalendar cal = new GregorianCalendar(); 
	cal.set(Calendar.YEAR, 1985); 
	cal.set(Calendar.MONTH, 7); 
	cal.set(Calendar.DAY_OF_MONTH, 20); 
	XMLGregorianCalendar dateOfBirth = TestUtils.dateToXMLGregorian(cal.getTime());
	patientDemographics.setDateOfBirth(dateOfBirth);
	//patientDemographics.setDeathDate(value);
	patientDemographics.setEMail("abc@velos.com");
	Code codeEthnicity = new Code();
	codeEthnicity.setCode(ServiceProperties.getProperty(ServiceProperties.PAT_DEMOGRAPHICS_CODE_ETHNICITY));
	patientDemographics.setEthnicity(codeEthnicity);
	patientDemographics.setFirstName("Gursimran");
	Code cGender = new Code();
	cGender.setCode(ServiceProperties.getProperty(ServiceProperties.PAT_DEMOGRAPHICS_CODE_GENDER));
	patientDemographics.setGender(cGender);
	patientDemographics.setHomePhone("4088494164");
	patientDemographics.setLastName("chahal");
	patientDemographics.setMiddleName("Singh");
	
	OrganizationIdentifier oIdentifier = new OrganizationIdentifier();
	oIdentifier.setSiteName(ServiceProperties.getProperty(ServiceProperties.TEST_ORGNAME));
	patientDemographics.setOrganization(oIdentifier);
	patientDemographics.setPatFacilityId("48484848");
	patientDemographics.setPatientCode("7041");
	PatientIdentifier patientIdentifier = new PatientIdentifier();
	patientIdentifier.setPatientId("7041");
	Code cRace = new Code();
	cRace.setCode(ServiceProperties.getProperty(ServiceProperties.PAT_DEMOGRAPHICS_CODE_RACE));
	patientDemographics.setRace(cRace);
	
	GregorianCalendar cal1 = new GregorianCalendar(); 
	cal.set(Calendar.YEAR, 2009); 
	cal.set(Calendar.MONTH, 7); 
	cal.set(Calendar.DAY_OF_MONTH, 20); 
	XMLGregorianCalendar registrationDate = TestUtils.dateToXMLGregorian(cal1.getTime());
	patientDemographics.setRegistrationDate(registrationDate);
	patientDemographics.setState("california");
	Code cSurvivalStatus = new Code();
	cSurvivalStatus.setCode(ServiceProperties.getProperty(ServiceProperties.PAT_DEMOGRAPHICS_CODE_SURVIVALSTATUS));
	patientDemographics.setSurvivalStatus(cSurvivalStatus);
	patientDemographics.setWorkPhone("408404408");
	patientDemographics.setZipCode("95112");
	
	patient.setPatientDemographics(patientDemographics);
	
	
	patientOrganization.setAccess(AccessRights.GRANTED);
	patientOrganization.setDefault(true);
	patientOrganization.setFacilityID("123456");
	OrganizationIdentifier OIdentifier = new OrganizationIdentifier();
	OIdentifier.setSiteName(ServiceProperties.getProperty(ServiceProperties.TEST_ORGNAME));
	patientOrganization.setOrganizationID(OIdentifier);
	patientOrganization.setOtherProvider("fgg");
	//UserIdentifier uIdentifier = new UserIdentifier();
	//uIdentifier.setOID(value)
	patientOrganization.setProvider(null);
	patientOrganization.setRegistrationDate(registrationDate);
	//patientOrganization.setSpecialtyAccess(value);
	PatientOrganizations patientorganizations = new PatientOrganizations();
	patientorganizations.getOrganization().add(patientOrganization);
	//patient.setPatientOrganizations(patientorganizations);
	
	
	studyIdentifier.setStudyNumber(ServiceProperties.getProperty(ServiceProperties.TEST_STUDY_NUMBER));
	
	//patientEnrollmentDetails.setAssignedTo(value);
	patientEnrollmentDetails.setCurrentStatus(true);
	//patientEnrollmentDetails.setDateOfDeath(value);
	//patientEnrollmentDetails.setDeathRelatedToTrial(value);
	
	patientEnrollmentDetails.setEnrollingSite(OIdentifier);
	
	//patientEnrollmentDetails.setEvaluationFlag(value);
	//patientEnrollmentDetails.setEvaluationStatus(value);
	//patientEnrollmentDetails.setInevaluationStatus(value);
	//patientEnrollmentDetails.setPatient(value);
	patientEnrollmentDetails.setPatientStudyId("Test 7");
	//UserIdentifier uIdentifier = new UserIdentifier();
	//uIdentifier.s
	//patientEnrollmentDetails.setPhysician(value);
	//patientEnrollmentDetails.setReasonOfDeathRelatedToTrial(value);
	GregorianCalendar cal2 = new GregorianCalendar(); 
	cal.set(Calendar.YEAR, 2011); 
	cal.set(Calendar.MONTH, 7); 
	cal.set(Calendar.DAY_OF_MONTH, 20); 
	XMLGregorianCalendar StatusDate = TestUtils.dateToXMLGregorian(cal2.getTime());
	patientEnrollmentDetails.setStatusDate(StatusDate);
	Code codestatus = new Code();
	codestatus.setCode(ServiceProperties.getProperty(ServiceProperties.PAT_ENROLLMENT_PATIENT_STATUS_CODE));
	patientEnrollmentDetails.setStatus(codestatus);
	//patientEnrollmentDetails.setStatusReason(value);
	
	//patientEnrollmentDetails.setStudyIdentifier(studyIdentifier);
	patientEnrollmentDetails.setSurvivalStatus(cSurvivalStatus);
	
	patientEnrollmentDetails.setTreatingOrganization(OIdentifier);
	
	//patientEnrollmentDetails.setTreatmentLocation(value);
	
	ResponseHolder response = null;
	try{
		studyPatientSEI = ClientUtils.getStudyPatientsService();
		try {
			response = studyPatientSEI.createAndEnrollPatient(patient, studyIdentifier, patientEnrollmentDetails);
		} catch (OperationRolledBackException_Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}catch(OperationException_Exception e)
	{
		Assert.assertNotNull(patient);
		e.printStackTrace();
	} catch (MalformedURLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}
		
	

	
	}
