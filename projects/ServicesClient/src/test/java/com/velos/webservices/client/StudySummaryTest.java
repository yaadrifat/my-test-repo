/**
 * 
 */
package com.velos.webservices.client;

import java.net.MalformedURLException;
import java.util.Date;
import java.util.List;
import javax.xml.datatype.XMLGregorianCalendar;
import junit.framework.Assert;
import org.junit.Test;

import com.velos.services.Code;
import com.velos.services.NvPair;
import com.velos.services.OperationException_Exception;
import com.velos.services.OperationRolledBackException_Exception;
import com.velos.services.OrganizationIdentifier;
import com.velos.services.ResponseHolder;
import com.velos.services.Study;
import com.velos.services.StudyIdentifier;
import com.velos.services.StudyOrganization;
import com.velos.services.StudyOrganizations;
import com.velos.services.StudySEI;
import com.velos.services.StudyStatus;
import com.velos.services.StudyStatuses;
import com.velos.services.StudySummary;
import com.velos.services.StudyTeamMember;
import com.velos.services.StudyTeamMembers;
import com.velos.services.UserIdentifier;
import com.velos.services.UserType;

/**
 * @author dylan
 * Tests StudySummary for a particular study.
 *
 */
public class StudySummaryTest {
	//ClientUtils clientUtils = new ClientUtils();
	// private static String studyNumber = ClientUtils.getNewTestStudyNumber();
	private static String studyNumber = 
		ServiceProperties.getProperty(ServiceProperties.TEST_STUDY_NUMBER); 
	static StudySEI studyService = null;
	
	
	/**
	 * Test method for Create Study. Takes a test study number 
	 * and required values for StudySummary, StudyOrganization, 
	 * StudyTeamMember  and StudyStatus and creates a Study. 
	 * 
	 */
	public void TestCreateStudy(){
		Boolean createNonSystemUsers = true;
		//CreateStudy Begin
		UserIdentifier usrStudyEnteredBy= new UserIdentifier();
		usrStudyEnteredBy.setUserLoginName(ServiceProperties.getProperty(ServiceProperties.TEST_LOGIN_USERNAME));
		
		String testStudyTitle = "Title: " + (new Date()).toString();
		
		Code codePhase = new Code();
		codePhase.setCode(ServiceProperties.getProperty(ServiceProperties.SUMMARY_PHASE_CODE));
		
		Code codeDivCode = new Code();
		codeDivCode.setCode(ServiceProperties.getProperty(ServiceProperties.SUMMARY_DIVISION_CODE));
		
		Code codeTherapeuticArea = new Code();
		codeTherapeuticArea.setCode(ServiceProperties.getProperty(ServiceProperties.SUMMARY_TAREA_CODE));
		
		
		Study newStudy = new Study();
		Study newStudyafterCreate = new Study();
		
		//Set Study summary
        StudySummary summary = new StudySummary();
		summary.setStudyNumber(studyNumber);
		summary.setStudyAuthor(usrStudyEnteredBy);
		summary.setStudyTitle(testStudyTitle);
		summary.setDivision(codeDivCode);
		summary.setTherapeuticArea(codeTherapeuticArea);
		summary.setPhase(codePhase);
		newStudy.setStudySummary(summary);
		
		//Set Study Organization
		OrganizationIdentifier organizationIdentifier = new OrganizationIdentifier();
		organizationIdentifier.setSiteName(ServiceProperties.getProperty(ServiceProperties.TEST_ORGNAME));
		StudyOrganization studyOrganization = new StudyOrganization();
		studyOrganization.setOrganizationIdentifier(organizationIdentifier);
        StudyOrganizations studyOrganizations = new StudyOrganizations();
		studyOrganizations.getStudyOrganizaton().add(studyOrganization);
		newStudy.setStudyOrganizations(studyOrganizations);
		
		//Set Study Status
        StudyStatus studyStatus = new StudyStatus();
		studyStatus.setOrganizationId(organizationIdentifier);
		Code statusCode= new Code();
		Code statusCodeType= new Code();
		statusCode.setCode("active");
		statusCodeType.setCode("default");
		studyStatus.setStatus(statusCode);
		studyStatus.setStatusType(statusCodeType);
		UserIdentifier userId = new UserIdentifier();
		userId.setUserLoginName(ServiceProperties.getProperty(ServiceProperties.TEST_LOGIN_USERNAME));
		studyStatus.setDocumentedBy(userId);
		XMLGregorianCalendar meetingDate = TestUtils.dateToXMLGregorian(new Date());
		studyStatus.setValidFromDate(meetingDate);
		StudyStatuses studyStatuses = new StudyStatuses();
		studyStatuses.getStudyStatus().add(studyStatus);
		newStudy.setStudyStatuses(studyStatuses);
		
		//Set Study Team Member
		UserIdentifier usrIdToAdd = new UserIdentifier();
		usrIdToAdd.setUserLoginName(ServiceProperties.
				getProperty(ServiceProperties.TEST_LOGIN_USERNAME));
        StudyTeamMember studyTeamMember = new StudyTeamMember();
		studyTeamMember.setUserId(usrIdToAdd);
		studyTeamMember.setUserType(UserType.DEFAULT);
		Code teamRoleCode = new Code();
		teamRoleCode.setCode(ServiceProperties.
 				getProperty(ServiceProperties.
				TEST_TEAM_ROLE_CODE));
		Code teamMemberStatusCode = new Code();
		teamMemberStatusCode.setCode("Active");
		studyTeamMember.setTeamRole(teamRoleCode);
		studyTeamMember.setStatus(teamMemberStatusCode);
		StudyTeamMembers studyTeamMembers = new StudyTeamMembers();
		studyTeamMembers.getStudyTeamMember().add(studyTeamMember);
		newStudy.setStudyTeamMembers(studyTeamMembers);
		//Get StudyService
		try {
			studyService = ClientUtils.getStudyService();
		} catch (MalformedURLException e1) {
			Assert.fail("Malformed URL getting service");
		}
		
		ResponseHolder response = null;
		
		
		try { 
			//Create study after StudySummary, StudyOrganization, Team Member, StudyStatus 
			//are being set into new study object.
			response = studyService.createStudy(newStudy, createNonSystemUsers);
		}
		catch(Exception e){
			Assert.assertNotNull(response);
			Assert.fail(e.getMessage());
		}
		
		
		StudyIdentifier studyId = new StudyIdentifier();
		studyId.setStudyNumber(studyNumber);
		//After Study created, fetch it from database 
		//to see whether properties created.
		try {
			newStudyafterCreate = studyService.getStudy(studyId);
		} catch (OperationException_Exception e) {
			// TODO Auto-generated catch block
			Assert.assertNotNull(newStudy);
			e.printStackTrace();
		}
		
		//assert check for check for created study properties , 
		//study title and study phase
		String studyTitleAfterCreate=  newStudyafterCreate.getStudySummary().getStudyTitle();
		if(studyTitleAfterCreate != null && !(studyTitleAfterCreate.equals(""))){
			Assert.assertEquals(studyTitleAfterCreate, testStudyTitle);
			
		}
		String studyPhaseAfterCreate=  newStudyafterCreate.getStudySummary().getPhase().getCode();
		String testStudyPhase = codePhase.getCode();
		if(studyPhaseAfterCreate != null && !(studyPhaseAfterCreate.equals(""))){
			Assert.assertEquals(testStudyPhase, studyPhaseAfterCreate);
			
		}
	}
	

	/**
	 * Method to test StudySummary details of a given study.
	 */
	@Test
	public void TestSummary(){
		String testStudyTitle = "Title: " + (new Date()).toString();
		StudyIdentifier studyId = new StudyIdentifier();
		studyId.setStudyNumber(studyNumber);
		//Test StudySummary begin.
		StudySummary studySummary = null;
		try {
			StudySEI studyService = null;
			try {
				studyService = ClientUtils.getStudyService();
			} catch (MalformedURLException e1) {
				Assert.fail("Malformed URL getting service");
			}
			studySummary = studyService.getStudySummary(studyId);
			//all studies should have at least the initial status
			//Assert.assertTrue( study.getStudyStatuses().size() > 0);
			
			studySummary.setStudyTitle(testStudyTitle);
			//more study details are mapped as name value pairs,
			//where the key is a codelst_subtyp from er_codelst where codelst_type='studyidtype'
			String testMSDKey = ServiceProperties.getProperty(ServiceProperties.TEST_MORE_STUDY_DETAILS_KEY);
			String testMSDValue = "Test cancer center num "   + (new Date()).toString();
			NvPair testMoreStudyDetail = new NvPair();
			testMoreStudyDetail.setKey(testMSDKey);
			testMoreStudyDetail.setValue(testMSDValue);
			
			//look for an NVPair with the test key in the existing
			//summary. If it's there, remove it.
			List<NvPair> existingMSDetails = studySummary.getMoreStudyDetails();
			for (NvPair existingMSDetail : existingMSDetails ){
				if (existingMSDetail.getKey().equals(testMSDKey)){
					existingMSDetails.remove(existingMSDetail);
					break;
				}
			}
			
			studySummary.getMoreStudyDetails().add(testMoreStudyDetail);
			//update study summary after values being set into study summary object.
			try {
				studyService.updateStudySummary(studyId, studySummary, false);
			} catch (OperationRolledBackException_Exception e) {
				TestUtils.handleOperationRolledbackException(e);
				Assert.fail("Failed to Update StudySummary");
				Assert.fail(e.getMessage());
			}
			
			//grab the new summary and test fields
			studySummary = studyService.getStudySummary(studyId);
			
			//check that the more study detail we added is there
			List<NvPair> moreStudyDetails = studySummary.getMoreStudyDetails();
			boolean msdFound = false;
			for (NvPair msdetail : moreStudyDetails){
				if (msdetail.getKey().equals(testMoreStudyDetail.getKey())){
					msdFound = true;
					Assert.assertEquals(testMoreStudyDetail.getValue(), msdetail.getValue());
				}
			}
			
			Assert.assertEquals(testStudyTitle, studySummary.getStudyTitle());
			
		} catch (OperationException_Exception e) {
			Assert.fail(e.getMessage());
		}
	}
}
