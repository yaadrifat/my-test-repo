/**
 * 
 */
package com.velos.webservices.client;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;


import com.velos.services.Issue;
import com.velos.services.OperationRolledBackException_Exception;

/**
 * @author dylan
 * Utility Class for Converting date to XMLGregorianCalendar
 *
 */
public class TestUtils{
	
	public static XMLGregorianCalendar dateToXMLGregorian(Date date){
		//create datatype factory for converting dates to XMLGregorianCalendars
		DatatypeFactory dFactory = null;
		try {
			dFactory = DatatypeFactory.newInstance();
		} catch (DatatypeConfigurationException e) {
			throw new RuntimeException(e);
		}

		//convert dates to XMLGregorianCalendar
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		XMLGregorianCalendar gregorianCal = 
			dFactory.newXMLGregorianCalendar(calendar);
		return gregorianCal;
	}
	//method to check if given two dates are same, if then return true else
	//return false.
	public static boolean isSameDay(XMLGregorianCalendar calendar1, XMLGregorianCalendar calendar2){
		if (calendar1.getYear() != calendar2.getYear()){
			return false;
		}
		if (calendar1.getMonth() != calendar2.getMonth()){
			return false;
		}
		if (calendar1.getDay() != calendar2.getDay()){
			return false;
		}
		return true;
		
	}
	//method to handle OperationRollback Exception and print issues
	public static void handleOperationRolledbackException(OperationRolledBackException_Exception e){
		List<Issue> issueList = e.getFaultInfo().getIssues().getIssue();
		for (Issue issue : issueList){
			System.err.println(issue.getType() + " " + issue.getMessage());
		}
	}
}
