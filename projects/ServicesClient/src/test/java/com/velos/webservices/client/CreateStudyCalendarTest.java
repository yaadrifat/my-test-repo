package com.velos.webservices.client;

import junit.framework.Assert;

import org.junit.Test;

import com.velos.services.CalendarEvent;
import com.velos.services.CalendarEventSummary;
import com.velos.services.CalendarEvents;
import com.velos.services.CalendarIdentifier;
import com.velos.services.CalendarSummary;
import com.velos.services.CalendarVisit;
import com.velos.services.CalendarVisits;
import com.velos.services.Code;
import com.velos.services.Costs;
import com.velos.services.Duration;
import com.velos.services.EventIdentifier;
import com.velos.services.OrganizationIdentifier;
import com.velos.services.ResponseHolder;
import com.velos.services.StudyCalendar;
import com.velos.services.StudyCalendarSEI;
import com.velos.services.StudyIdentifier;
import com.velos.services.TimeUnits;
import com.velos.services.VisitIdentifier;

public class CreateStudyCalendarTest 
{
	StudyCalendarSEI studyCalendarSEI;
	StudyCalendar studycalendar = new StudyCalendar();
@Test
public void createStudyCalendarTest()
{
	CalendarIdentifier calendarIdentifier = new CalendarIdentifier();
	CalendarEvent calendarEvent = new CalendarEvent();
	CalendarEvents cEvent = new CalendarEvents();
	EventIdentifier eIdentifier =new EventIdentifier();
	OrganizationIdentifier oIdentifier = new OrganizationIdentifier();
	oIdentifier.setSiteName(ServiceProperties.getProperty(ServiceProperties.TEST_ORGNAME));
	
	
	CalendarSummary cSummary = new CalendarSummary();
	cSummary.setCalendarDescription("NewCalendarSummary");
	Duration calendarduration = new Duration();
	calendarduration.setUnit(TimeUnits.DAY);
	cSummary.setCalendarDuration(calendarduration);
	cSummary.setCalendarName(ServiceProperties.getProperty(ServiceProperties.STUDY_CALENDAR_NAME));
	
	Code cd = new Code();
	cd.setCode(ServiceProperties.getProperty(ServiceProperties.STUDY_CALENDAR_STATUS_CODE));
	cSummary.setCalendarStatus(cd);
	Code cd1 = new Code();
	cd1.setCode(ServiceProperties.getProperty(ServiceProperties.STUDY_CALENDAR_TYPE_CODE));
	cd1.setType("L");
	cSummary.setCalendarType(cd1);
	
	StudyIdentifier studyidentifier = new StudyIdentifier();
	studyidentifier.setStudyNumber(ServiceProperties.getProperty(ServiceProperties.TEST_STUDY_NUMBER));
	String VisitName = ServiceProperties.getProperty(ServiceProperties.STUDY_CALENDAR_VISIT_NAME);
	
	
	
	
	
	CalendarVisits calendarVisits = new CalendarVisits();
	CalendarVisit calendarVisit = new CalendarVisit();
	VisitIdentifier visitIdentifier = new VisitIdentifier();
	visitIdentifier.setOID(ServiceProperties.getProperty(ServiceProperties.STUDY_CALENDAR_VISIT_OID));
	
	Duration absoluteInterval = new Duration();
	absoluteInterval.setUnit(TimeUnits.DAY);
	
	Duration relativevisitinterval = new Duration();
	relativevisitinterval.setUnit(TimeUnits.DAY);
	
	
	Duration visitwindowafter = new Duration();
	visitwindowafter.setUnit(TimeUnits.DAY);
	
	
	Duration visitwindowbefore = new Duration();
	visitwindowbefore.setUnit(TimeUnits.DAY);
	
	CalendarEventSummary calendarEventSummary = new CalendarEventSummary();
	Duration eventDuration = new Duration();
	eventDuration.setUnit(TimeUnits.DAY);
	calendarEventSummary.setEventDuration(eventDuration);
	
	calendarEventSummary.setDescription("X-Ray Visit Event");
	calendarEventSummary.setEventIdentifier(eIdentifier);
	calendarEventSummary.setEventName(ServiceProperties.getProperty(ServiceProperties.STUDY_CALENDAR_VISIT_EVENT_NAME));
	
	Duration eventWindowAfter = new Duration(); 
	eventWindowAfter.setUnit(TimeUnits.DAY); 
	eventWindowAfter.setValue(2); 
	calendarEventSummary.setEventWindowAfter(eventWindowAfter);
	
	Duration eventWindowBefore = new Duration();
	eventWindowBefore.setUnit(TimeUnits.DAY);
	eventWindowBefore.setValue(1);
	calendarEventSummary.setEventWindowBefore(eventWindowBefore);
	calendarEventSummary.setFacility(oIdentifier);
	calendarEventSummary.setNotes("Velos");	
	calendarEventSummary.setSequence(10);
	calendarEventSummary.setSiteOfService(oIdentifier);
	calendarEvent.setCalendarEventSummary(calendarEventSummary);
	cEvent.getEvent().add(calendarEvent);
	Costs costs = new Costs();
	calendarEvent.setCosts(costs);
	
	
	calendarVisits.getVisit().add(calendarVisit);
	studycalendar.setCalendarIdentifier(calendarIdentifier);
	studycalendar.setCalendarSummary(cSummary);
	studycalendar.setStudyIdentifier(studyidentifier);
	studycalendar.setVisits(calendarVisits);
	
	
try{
		
		@SuppressWarnings("unused")
		ResponseHolder response = null;
	studyCalendarSEI	 = ClientUtils.getStudyCalendar();
	
	calendarIdentifier.setOID(ServiceProperties.getProperty(ServiceProperties.STUDY_CALENDAR_OID));
	
	response = studyCalendarSEI.addEventsToStudyCalendarVisit(calendarIdentifier, VisitName, visitIdentifier,cEvent);
		
	}catch(Exception e)
	{
		Assert.assertNotNull(calendarIdentifier);
		e.printStackTrace();
	}}
}

