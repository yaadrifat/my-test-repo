@echo off

REM set this to a valid JDK!
set JAVA_Home=

if "%JAVA_HOME%" == "" goto usage

set path=./maven/bin;%path%

mvn test

:usage
echo Please open this run.bat in an editor and set JAVA_HOME
goto EOF

:EOF
pause

