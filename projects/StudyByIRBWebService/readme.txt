This is a custom web service created for the University of Michigan's Click Commerce interface.

The service uses all of the eSP service code. However, Michigan had two requirements that are custom:

1. Find studies by IRB number, which is a More Study Details Field

2. Custom authentication.

To build, run:
	mvn clean compile