package com.velos.webservices;

import com.velos.services.OperationException;

public class MultipleStudiesForIRBNumberException extends OperationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2884174688107019314L;

	public MultipleStudiesForIRBNumberException() {
		super();
	}

}
