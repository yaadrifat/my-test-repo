/*Virendra
 * 
 * 
 * 
 */

package com.velos.webservices;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.CommonDAO;
import com.velos.services.OperationException;
import com.velos.services.model.NVPair;
import com.velos.services.model.StudySummary;
import com.velos.services.util.CodeCache;


public class MoreStudyDetailsDAO extends CommonDAO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8410547449723597051L;
	private static Logger logger = Logger.getLogger(MoreStudyDetailsDAO.class);

	private static String studyPKSql = 
		"select study_number " +
		"from er_studyid, er_codelst, er_study " +
		"where er_studyid.fk_codelst_idtype = er_codelst.pk_codelst " +
		"and er_codelst.codelst_type = 'studyidtype' " +
		"and pk_study = fk_study " +
		"and er_codelst.codelst_subtyp = ? " +
		"and studyid_id=? ";
			
	 public static String getStudyNumberFromMSD( // MSD = More Study Details
			 String msdKey, 
			 String msdSearchTerm) 
	 throws 
	 OperationException, MultipleStudiesForIRBNumberException{

	        PreparedStatement pstmt = null;
			Connection conn = null;
	        try
	        {
	        	conn = getConnection();
				if (logger.isDebugEnabled()) logger.debug("com.velos.services.study.StudyStatusDAO.getStudy sql:" + studyPKSql);
				
				pstmt = conn.prepareStatement(studyPKSql);
				pstmt.setString(1, msdKey);
				pstmt.setString(2, msdSearchTerm);
				
				ResultSet rs = pstmt.executeQuery();
				String studyNumber = null;
				int studyCount = 0;
				while(rs.next()) {
				    studyNumber = rs.getString("study_number");
				    studyCount++;
				}
				if (studyCount > 1){
					throw new MultipleStudiesForIRBNumberException();
				}
				
				return studyNumber;
	        }
	        catch (SQLException ex) {
				logger.fatal("MoreStudyDetailsDAOgetStudyIdFromMSD. EXCEPTION IN FETCHING FROM studypk"
						+ ex);
				throw new OperationException(ex);
			} finally {
				try {
					if (pstmt != null)
						pstmt.close();
				} catch (Exception e) {
				}
				try {
					if (conn != null)
						conn.close();
				} catch (Exception e) {
				}

			}

	 }

}
