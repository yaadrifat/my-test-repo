/**
 *
 */
package com.velos.webservices;
   
import javax.annotation.Resource;
import javax.jws.WebService;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.xml.ws.WebServiceContext;

import org.apache.log4j.Logger;

import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.Study;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.study.StudyService;

/**
 * @author dylan
 * 
 */
@WebService(
		serviceName = "StudyService", 
		endpointInterface = "com.velos.webservices.IRBStudySEI", 
		targetNamespace = "http://velos.com/services/study")	  
public class IRBStudyWS implements IRBStudySEI{
	
	private static Logger logger = Logger.getLogger(IRBStudyWS.class.getName());
	
	@Resource
	private WebServiceContext context;
 
 
	public IRBStudyWS() { 
 
	}
         
	public ResponseHolder createStudy(Study study, boolean createNonSystemUsers)
			throws OperationException {

		ResponseHolder response = new ResponseHolder();
		try {
			StudyService service = getStudyRemote();
			response = service.createStudy(study, createNonSystemUsers);
			
		} catch (OperationException e) {
			logger.error("create study", e);
			throw e;
		} catch (Throwable t) {
			logger.error("create study", t);
			response.addIssue(new Issue(IssueTypes.UNKNOWN_THROWABLE));
		}
		return response;
	}
	
	
	public Study getStudy(IRBStudyId studyId) throws OperationException {

	    ResponseHolder response = new ResponseHolder();
		try {
			StudyService service = getStudyRemote();
			StudyIdentifier id = new StudyIdentifier();
			String studyNumber = IRBStudyWS.getStudyNumber(studyId);
			if (studyNumber == null) {
                Issue issue = new Issue(IssueTypes.STUDY_NOT_FOUND, "For IRB study number: " + studyId.getIRBNumber());
                response.addIssue(issue);
			    throw new OperationException(issue.toString());
			}
			id.setStudyNumber(studyNumber);
			Study fetchedStudy = service.getStudy(id);
			context.getMessageContext().put("userN", context.getUserPrincipal().getName());
			return fetchedStudy;
		} catch (OperationException e) {
			logger.error("getStudy", e);
			throw e;
		} catch (Throwable t) {
			logger.error("getStudy", t); 
			throw new OperationException(t);
		}
	 		
	}
	
	/**
	 * Customer M requires identification of the study by IRBNumber, not
	 * Study Number. The IRBStudyId class contains both fields.
	 * Before we fetch the study normally, we'll find the study number
	 * based on IRB Number, add that field to the MichigcanStudyId and 
	 * send that in.
	 * @param studyId
	 * @throws OperationException
	 */
	
	protected static String getStudyNumber(IRBStudyId studyId) throws OperationException{
		String studyNumber = studyId.getStudyNumber();

		if (studyNumber == null || studyNumber.length() == 0){
			String irbNumber = studyId.getIRBNumber();
			// IRB Number is stored as irb_number in More Study Details
			return 
				MoreStudyDetailsDAO.
					getStudyNumberFromMSD(
							"irb_number", 
							irbNumber);
		}
		return null;
	}

	
	/**
	 * 
	 * @return
	 * @throws NamingException
	 */
	private static StudyService getStudyRemote()
		throws OperationException{
		//TODO Should this be a member of this class so that we only get the studyremote once??
		
		StudyService studyRemote = null;
		InitialContext ic;
		try{
			ic = new InitialContext();
			studyRemote =
				(StudyService) ic.lookup(StudyService.class.getName());
		}
		catch(NamingException e){
			throw new OperationException(e);
		}
		return studyRemote;
	}
}
