/**
 * 
 */
package com.velos.webservices;
 
import java.util.Date;
import java.util.Set;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.xml.namespace.QName;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.LogicalHandler;
import javax.xml.ws.handler.LogicalMessageContext;

import org.apache.cxf.message.Message;
import org.apache.cxf.phase.PhaseInterceptorChain;
import org.apache.log4j.Logger;

import com.velos.eres.business.user.impl.UserBean;


/**
 * This jax-ws handler handles logging message activity to svc_session and svc_message tables. It
 * must be added to the jax-ws handler chain. In CXF, this can be done the beans.xml file.
 * 
 * @author dylan
 *
 */
public class VelosTokenMessageHandler implements LogicalHandler<LogicalMessageContext>{
	public static final String VELOS_TOKEN_HEADER_NAME = "VelosToken";
	public static final String VELOS_LOGIN_HEADER_NAME = "VelosUser";
	
	private static Logger logger = Logger.getLogger(VelosTokenMessageHandler.class.getName());

	
	
	public VelosTokenMessageHandler(){
		
	}
	
	/* (non-Javadoc)
	 * @see javax.xml.ws.handler.soap.SOAPHandler#getHeaders()
	 */
	public Set<QName> getHeaders() {
		// TODO Auto-generated method stub
		return null;
	}



	/* (non-Javadoc)
	 * @see javax.xml.ws.handler.Handler#handleFault(javax.xml.ws.handler.MessageContext)
	 */
	public boolean handleFault(LogicalMessageContext context) {

		String errStr = (String)context.get(Constants.ERROR_STRING_KEY);

		return true; 
	}

	/**
	 * Logs messageLog and sessionLog information. 
	 * Currently does not log user identification information.
	 * @see javax.xml.ws.handler.Handler#handleMessage(javax.xml.ws.handler.MessageContext)
	 */
	public boolean handleMessage(LogicalMessageContext context) {
		//This method gets called twice per message. Once before invoking the 
		//web service operation (and before authenticating) and the second
		//time on the way back out.
		//We set SessionLog and MessageLog parameters in both directions.
		try{
		
			Boolean messageOut = (Boolean)context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
			
			if (!messageOut){
				//receiving a message, log the initial info about it
			
				HttpServletRequest request =
					(HttpServletRequest)context.get(MessageContext.SERVLET_REQUEST);
				
				String headerValue = request.getHeader(VELOS_TOKEN_HEADER_NAME);
				String userLogin = request.getHeader(VELOS_LOGIN_HEADER_NAME);
				
				
				//Now we need to find this token in the
				//header table. The call to the database
				//will return an object that contains expiration time, 
				//userlogin name and client ip address that was created
				//for this token.
				
				//If IP and Username from database match what was passed in
				//the headers AND the expiration time from the database is not
				//past, and you can successfully get a UserBean instance for this user,
				//extend the expiration date/time to the period configured
				//for this user (get the user's UserBean instance to find.)
				//and return true.
				if (true){//remove "true" for real logic
					UserBean user = new UserBean();
					Message cxfMessage = PhaseInterceptorChain.getCurrentMessage();
					cxfMessage.put(Constants.USER_BEAN_KEY, user);
					return true;
				}
				
				
				
				//Otherwise, return false 
				return false;
				
				
			}
			return true;
			
		}
		catch(Throwable t){
			logger.error("error creating user session information " 
					 + " " + t); 
			return false;
		}
		
	}

	
	
	
	/* (non-Javadoc)
	 * @see javax.xml.ws.handler.Handler#close(javax.xml.ws.handler.MessageContext)
	 */
	public void close(MessageContext context) {
		//cxf does not appear to be ever calling this method
		
	}
	



}
