/**
 * 
 */
package com.velos.webservices;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import com.velos.services.OperationException;
import com.velos.services.OperationRolledBackException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.Study;

/**
 * @author dylan
 *
 */
@WebService(
		 targetNamespace="http://velos.com/services/study")
public interface IRBStudySEI {

	@WebResult(name = "Response")
	public abstract ResponseHolder createStudy(
			@WebParam(name = "Study") 
			Study study, 
			
			@WebParam(name = "createNonSystemUsers")
			boolean createNonSystemUsers)
			throws OperationException, OperationRolledBackException;

	
	@WebResult(name = "Study")
	public abstract Study getStudy(
			@WebParam(name = "StudyIdentifier")
			IRBStudyId id) 
		throws OperationException;
	

}