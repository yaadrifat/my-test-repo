package com.velos.webservices;

import com.velos.services.model.StudyIdentifier;

/**
 * Customer M needs to use IRBNumber to get studies rather than
 * study number, which is the default. So, we're creating this 
 * class to send IRB Number in. Test.
 * @author dylan
 *
 */
public class IRBStudyId extends StudyIdentifier {

	public IRBStudyId(){
	
	}
	
	protected String IRBNumber; 
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 9035779952305121087L;


	public IRBStudyId(String IRBNumber) {
		super();
		this.IRBNumber = IRBNumber;
	}


	public String getIRBNumber() {
		return IRBNumber;
	}


	public void setIRBNumber(String iRBNumber) {
		IRBNumber = iRBNumber;
	}

	
}
