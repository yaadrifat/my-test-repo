/* Formatted on 4/1/2014 2:41:33 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_STORAGE_SPECIMEN  (View)
--
--  Dependencies:
--   ER_USER (Table)
--   ER_CODELST (Table)
--   ER_SPECIMEN (Table)
--   ER_STORAGE (Table)
--   ER_STORAGE_STATUS (Table)
--   ER_STUDY (Table)
--

CREATE OR REPLACE  VIEW VDA_V_STORAGE_SPECIMEN
(
   FK_ACCOUNT,
   PK_STORAGE,
   STORAGE_ID,
   STORAGE_NAME,
   STORAGE_ALTERNATEID,
   STORAGE_TYPE,
   CHILD_STORAGE_TYPE,
   PARENT_STORAGE_ID,
   PARENT_STORAGE,
   STORAGE_FK_STUDY,
   STORAGE_STUDY_NUMBER,
   STORAGE_STATUS,
   STORAGE_STATUS_DATE,
   STORAGE_TRACKING_USER,
   STORAGE_TRACKING_NO,
   RID,
   CREATOR,
   CREATED_ON,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   PK_SPECIMEN,
   SPEC_ID,
   STORAGE_MULTI_SPECIMEN, IP_ADD
   
)
AS
   SELECT   a.fk_account,
            pk_storage,
            a.storage_id,
            storage_name,
            STORAGE_ALTERNALEID AS STORAGE_ALTERNATEID,
            (SELECT   codelst_desc
               FROM   eres.ER_CODELST
              WHERE   pk_codelst = fk_codelst_storage_type)
               storage_type,
            (SELECT   codelst_desc
               FROM   eres.ER_CODELST
              WHERE   pk_codelst = fk_codelst_child_stype)
               child_storage_type,
            (SELECT   aa.storage_id
               FROM   eres.ER_STORAGE aa
              WHERE   aa.pk_storage = a.fk_storage)
               parent_storage_id,
            (SELECT   aa.storage_name
               FROM   eres.ER_STORAGE aa
              WHERE   aa.pk_storage = a.fk_storage)
               parent_storage,
            b.fk_study storage_fk_study,
            (SELECT   study_number
               FROM   eres.ER_STUDY
              WHERE   pk_study = b.fk_study)
               storage_study_number,
            (SELECT   codelst_desc
               FROM   eres.ER_CODELST
              WHERE   pk_codelst = fk_codelst_storage_status)
               storage_status,
            ss_start_date storage_status_date,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   eres.ER_USER
              WHERE   pk_user = b.fk_user)
               AS Storage_tracking_user,
            b.ss_tracking_number Storage_tracking_no,
            a.rid,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   eres.ER_USER
              WHERE   pk_user = a.creator)
               creator,
            a.created_on,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   eres.ER_USER
              WHERE   pk_user = a.last_modified_by)
               last_modified_by,
            a.last_modified_date,
            (SELECT   pk_specimen
               FROM   eres.er_specimen x
              WHERE   x.fk_storage = a.pk_storage)
               pk_specimen,
            (SELECT   SPEC_ID
               FROM   eres.er_specimen x
              WHERE   x.fk_storage = a.pk_storage)
               SPEC_ID,
            storage_multi_specimen, a.IP_ADD
     FROM   eres.ER_STORAGE a, eres.ER_STORAGE_STATUS b
    WHERE   NVL (storage_istemplate, 0) <> 1 AND a.pk_storage = b.fk_storage
            AND b.pk_storage_status =
                  (SELECT   MAX (c.pk_storage_status)
                     FROM   eres.ER_STORAGE_STATUS c
                    WHERE   a.pk_storage = c.fk_storage
                            AND c.ss_start_date =
                                  (SELECT   MAX (d.ss_start_date)
                                     FROM   eres.ER_STORAGE_STATUS d
                                    WHERE   a.pk_storage = d.fk_storage))
/
COMMENT ON TABLE VDA_V_STORAGE_SPECIMEN IS 'This view provides access to storage units designed in Velos eSample. The view does not include the ones marked as a template.'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.FK_ACCOUNT IS 'The account storage is linked with'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.PK_STORAGE IS 'The Primary Key of the storage record'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.STORAGE_ID IS 'The unique Storage ID'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.STORAGE_NAME IS 'The Storage Name'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.STORAGE_ALTERNATEID IS 'The Storage Alternate ID'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.STORAGE_TYPE IS 'The Storage Type'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.CHILD_STORAGE_TYPE IS 'The Child Storage type - immediate child'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.PARENT_STORAGE_ID IS 'The Parent Storage ID'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.PARENT_STORAGE IS 'The Parent Storage Name'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.STORAGE_FK_STUDY IS 'The FK to the study storage is linked with (through storage status)'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.STORAGE_STUDY_NUMBER IS 'The study number of the study the storage is linked with (through storage status)'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.STORAGE_STATUS IS 'The recent storage status'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.STORAGE_STATUS_DATE IS 'The Storage Status Date'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.STORAGE_TRACKING_USER IS 'The user linked with storage status'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.STORAGE_TRACKING_NO IS 'The tracking number linked with the storage status'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.RID IS 'The unique Row ID for Audit'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.CREATED_ON IS 'The date the record was created on (Audit)'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.LAST_MODIFIED_BY IS 'The user who last modified the record (Audit)'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.LAST_MODIFIED_DATE IS 'The date the record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.PK_SPECIMEN IS 'The Primary Key of the specimen - if storage has a specimen stored in it'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.SPEC_ID IS 'The Specimen ID of the specimen - if storage has a specimen stored in it'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.STORAGE_MULTI_SPECIMEN IS 'Flag to indicate if storage can contain multiple specimens'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.IP_ADD IS 'The IP address of the client machine that initiated the session'
/