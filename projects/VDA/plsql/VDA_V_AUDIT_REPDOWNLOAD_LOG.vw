/* Formatted on 3/18/2014 2:39:10 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_AUDIT_REPDOWNLOAD_LOG  (View)
--
--  Dependencies:
--   ER_USER (Table)
--   ER_USERREPORTLOG (Table)
--

CREATE OR REPLACE FORCE VIEW VDA_V_AUDIT_REPDOWNLOAD_LOG
(
   USER_NAME,
   USR_RPT_LOG_REP_NAME,
   USR_RPT_LOG_TABLE_NAME,
   IP_ADD,
   USR_RPT_LOG_URL,
   USR_RPT_LOG_URLPARAM,
   USR_RPT_LOG_ACCESSTIME,
   USR_RPT_LOG_MODNAME,
   USR_RPT_LOG_FILENAME,
   USR_RPT_LOG_FORMAT,
   USR_RPT_LOG_DOWNLOAD_FLAG
)
AS
   SELECT   (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = FK_USER)
               USER_NAME,
            USR_RPT_LOG_REP_NAME,
            USR_RPT_LOG_TABLE_NAME,
            IP_ADD,
            USR_RPT_LOG_URL,
            USR_RPT_LOG_URLPARAM,
            USR_RPT_LOG_ACCESSTIME,
            USR_RPT_LOG_MODNAME,
            USR_RPT_LOG_FILENAME,
            DECODE (USR_RPT_LOG_FORMAT,
                    NULL, 'Screen Output',
                    'null', 'Screen Output',
                    USR_RPT_LOG_FORMAT)
               USR_RPT_LOG_FORMAT,
            decode(USR_RPT_LOG_DOWNLOAD_FLAG,0,'No','Yes') USR_RPT_LOG_DOWNLOAD_FLAG
     FROM   eres.ER_USERREPORTLOG
/
COMMENT ON TABLE VDA_V_AUDIT_REPDOWNLOAD_LOG IS 'This view provides access to the audit log generated based on user activity on executing reports and/or file downloads.'
/

COMMENT ON COLUMN VDA_V_AUDIT_REPDOWNLOAD_LOG.USER_NAME IS 'The name of the user who accessed a report or downloaded a file'
/

COMMENT ON COLUMN VDA_V_AUDIT_REPDOWNLOAD_LOG.USR_RPT_LOG_REP_NAME IS 'The name of the report or file'
/

COMMENT ON COLUMN VDA_V_AUDIT_REPDOWNLOAD_LOG.USR_RPT_LOG_TABLE_NAME IS 'The source table name'
/

COMMENT ON COLUMN VDA_V_AUDIT_REPDOWNLOAD_LOG.IP_ADD IS 'The IP ADDRESS of the client machine'
/

COMMENT ON COLUMN VDA_V_AUDIT_REPDOWNLOAD_LOG.USR_RPT_LOG_URL IS 'The URL accessed in the Velos eResearch application'
/

COMMENT ON COLUMN VDA_V_AUDIT_REPDOWNLOAD_LOG.USR_RPT_LOG_URLPARAM IS 'The parameters passed to the URL accessed in the Velos eResearch application'
/

COMMENT ON COLUMN VDA_V_AUDIT_REPDOWNLOAD_LOG.USR_RPT_LOG_ACCESSTIME IS 'The time of report access or file download'
/

COMMENT ON COLUMN VDA_V_AUDIT_REPDOWNLOAD_LOG.USR_RPT_LOG_MODNAME IS 'The module name'
/

COMMENT ON COLUMN VDA_V_AUDIT_REPDOWNLOAD_LOG.USR_RPT_LOG_FILENAME IS 'The downloaded file''s name or temporary report file generated'
/

COMMENT ON COLUMN VDA_V_AUDIT_REPDOWNLOAD_LOG.USR_RPT_LOG_FORMAT IS 'The file or report output format'
/

COMMENT ON COLUMN VDA_V_AUDIT_REPDOWNLOAD_LOG.USR_RPT_LOG_DOWNLOAD_FLAG IS 'Flad indicating file download'
/

