/* Formatted on 3/11/2014 3:18:29 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_AE_ALL  (View)
--
--  Dependencies:
--   ER_PATPROT (Table)
--   F_GETDIS_SITE (Function)
--   F_GET_CODELSTDESC (Function)
--   F_GET_YESNO (Function)
--   SCH_ADVERSEVE (Table)
--   SCH_CODELST (Table)
--   VDA_V_STUDY_SUMMARY (View)
--

CREATE OR REPLACE FORCE VIEW VDA_V_AE_ALL
(
   FK_STUDY,
   FK_SITE_ENROLLING,
   STUDY_NUMBER,
   STUDY_TITLE,
   STUDY_PI,
   STUDY_TAREA,
   STUDY_DISEASE_SITE,
   AE_TYPE,
   AE_DESC,
   AE_STDATE,
   AE_ENDDATE,
   AE_GRADE,
   AE_NAME,
   MEDDRA,
   DICTIONARY,
   FK_PER,
   FK_ACCOUNT
)
AS
   SELECT   e.fk_study,
            p.fk_site_enrolling,
            s.study_number,
            s.study_title,
            s.study_pi,
            s.study_tarea,
            s.study_disease_site,
            (SELECT   codelst_desc
               FROM   esch.sch_codelst
              WHERE   pk_codelst = e.fk_codlst_aetype)
               ae_type,
            e.ae_desc,
            e.ae_stdate,
            e.ae_enddate,
            e.ae_grade,
            e.ae_name,
            e.meddra,
            e.dictionary,
            e.fk_per,
            s.fk_account
     FROM   esch.sch_adverseve e,
            eres.er_patprot p,
            VDA.VDA_V_STUDY_SUMMARY s
    WHERE       e.fk_per = p.fk_per
            AND e.fk_study = p.fk_study
            AND p.patprot_stat = 1
            AND e.fk_study = pk_study
/
COMMENT ON TABLE VDA_V_AE_ALL IS 'This view provides access to all adverse events on a study'
/

COMMENT ON COLUMN VDA_V_AE_ALL.FK_ACCOUNT IS 'The account the Adverse Event is linked with'
/

COMMENT ON COLUMN VDA_V_AE_ALL.FK_STUDY IS 'The Foreign Key of the Study record for which the Adverse Event is reported'
/

COMMENT ON COLUMN VDA_V_AE_ALL.FK_SITE_ENROLLING IS 'The Site or Organization Patient is Enrolled for'
/

COMMENT ON COLUMN VDA_V_AE_ALL.STUDY_NUMBER IS 'The Study Number the Adverse Event is reported'
/

COMMENT ON COLUMN VDA_V_AE_ALL.STUDY_TITLE IS 'Title of the Study the Adverse Event is reported on'
/

COMMENT ON COLUMN VDA_V_AE_ALL.STUDY_PI IS 'Principal Investigator of the study'
/

COMMENT ON COLUMN VDA_V_AE_ALL.STUDY_TAREA IS 'Therapeutic Area of the study'
/

COMMENT ON COLUMN VDA_V_AE_ALL.STUDY_DISEASE_SITE IS 'The Disease site associated with the study'
/

COMMENT ON COLUMN VDA_V_AE_ALL.AE_TYPE IS 'Description of the Adverse Event Type'
/

COMMENT ON COLUMN VDA_V_AE_ALL.AE_DESC IS 'Adverse Event''s description'
/

COMMENT ON COLUMN VDA_V_AE_ALL.AE_STDATE IS 'The Start Date of the Adverse Event'
/

COMMENT ON COLUMN VDA_V_AE_ALL.AE_ENDDATE IS 'The End Date of the Adverse Event'
/

COMMENT ON COLUMN VDA_V_AE_ALL.AE_GRADE IS 'The grade of the Adverse Event'
/

COMMENT ON COLUMN VDA_V_AE_ALL.AE_NAME IS 'The name of the Adverse Event'
/

COMMENT ON COLUMN VDA_V_AE_ALL.MEDDRA IS 'The Meddra code linked with the Adverse Event'
/

COMMENT ON COLUMN VDA_V_AE_ALL.DICTIONARY IS 'The Dictionary linked with the Adverse Event'
/

COMMENT ON COLUMN VDA_V_AE_ALL.FK_PER IS 'The Patient for which the Adverse Event is reported'
/


