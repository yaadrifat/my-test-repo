CREATE OR REPLACE FORCE VIEW VDA_V_STUDYSTATUS_TIMELINE
(
   STUDY_NUMBER,
   PK_STUDY,
   STUDY_TITLE,
   FIRST_NOT_ACTIVE,
   FIRST_IRB_APPROVED,
   FIRST_ACTIVE,
   LAST_PERM_CLOSURE
)
AS
   SELECT   study_number,
            pk_study,
            study_title,
            (SELECT   MIN (studystat_date)
               FROM   eres.er_studystat i
              WHERE   i.fk_study = o.pk_study
                      AND fk_codelst_studystat =
                            (SELECT   pk_codelst
                               FROM   eres.er_codelst
                              WHERE   codelst_type = 'studystat'
                                      AND codelst_subtyp = 'not_active'))
               first_not_active,
            (SELECT   MIN (studystat_date)
               FROM   eres.er_studystat i
              WHERE   i.fk_study = o.pk_study
                      AND fk_codelst_studystat =
                            (SELECT   pk_codelst
                               FROM   eres.er_codelst
                              WHERE   codelst_type = 'studystat'
                                      AND codelst_subtyp = 'app_CHR'))
               first_irb_approved,
            (SELECT   MIN (studystat_date)
               FROM   eres.er_studystat i
              WHERE   i.fk_study = o.pk_study
                      AND fk_codelst_studystat =
                            (SELECT   pk_codelst
                               FROM   eres.er_codelst
                              WHERE   codelst_type = 'studystat'
                                      AND codelst_subtyp = 'active'))
               first_active,
            (SELECT   MAX (studystat_date)
               FROM   eres.er_studystat i
              WHERE   i.fk_study = o.pk_study
                      AND fk_codelst_studystat =
                            (SELECT   pk_codelst
                               FROM   eres.er_codelst
                              WHERE   codelst_type = 'studystat'
                                      AND codelst_subtyp = 'prmnt_cls'))
               last_perm_closure
     FROM   eres.er_study o
/
COMMENT ON TABLE VDA_V_STUDYSTATUS_TIMELINE IS 'This view provides access to the study status timeline for predefined trial statuses'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_TIMELINE.STUDY_NUMBER IS 'The Study Number'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_TIMELINE.PK_STUDY IS 'The PK of the study'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_TIMELINE.STUDY_TITLE IS 'The Study Title'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_TIMELINE.FIRST_NOT_ACTIVE IS 'The date study was initiated-first ''Not Active'' or an equivalent status'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_TIMELINE.FIRST_IRB_APPROVED IS 'The date study was IRB Approved-first ''IRB Approved'' or an equivalent status'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_TIMELINE.FIRST_ACTIVE IS 'The date study was Active or Enrolling  -first ''Active/Enrolling'' or an equivalent status'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_TIMELINE.LAST_PERM_CLOSURE IS 'The date study was Closed -last ''Closed or Retired'' or an equivalent status'
/

