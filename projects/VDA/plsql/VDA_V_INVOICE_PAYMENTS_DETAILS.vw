/* Formatted on 6/11/2014 6:25:44 AM (QP5 v5.115.810.9015) */
--
-- VDA_V_INVOICE_PAYMENTS_DETAILS  (View)
--
--  Dependencies:
--   F_GET_CODELSTDESC (Function)
--   ER_MILEPAYMENT_DETAILS (Table)
--   VDA_V_PAYMENTS (View)
--

CREATE OR REPLACE  VIEW VDA_V_INVOICE_PAYMENTS_DETAILS
(
   STUDY_NUMBER,
   STUDY_TITLE,
   MSPAY_DATE,
   MSPAY_TYPE,
   MSPAY_AMT_RECVD,
   MSPAY_DESC,
   MSPAY_COMMENTS,
   PK_MILEPAYMENT,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATED_ON,
   FK_STUDY,
   FK_ACCOUNT,
   FK_INVOICE,
   MP_AMOUNT
)
AS
   SELECT   "STUDY_NUMBER",
            "STUDY_TITLE",
            "MSPAY_DATE",
            "MSPAY_TYPE",
            "MSPAY_AMT_RECVD",
            "MSPAY_DESC",
            "MSPAY_COMMENTS",
            "PK_MILEPAYMENT",
            "CREATOR",
            "LAST_MODIFIED_BY",
            "LAST_MODIFIED_DATE",
            "CREATED_ON",
            "FK_STUDY",
            "FK_ACCOUNT",
            "FK_INVOICE",
            "MP_AMOUNT"
          FROM   VDA_V_PAYMENTS,
            (  SELECT   MP_LINKTO_ID fk_invoice,
                        fk_milepayment,
                        SUM (MP_AMOUNT) MP_AMOUNT
                 FROM   eres.ER_MILEPAYMENT_DETAILS
                WHERE   MP_LINKTO_TYPE = 'I'
             GROUP BY   MP_LINKTO_ID, fk_milepayment) x
    WHERE   PK_MILEPAYMENT = FK_MILEPAYMENT
/
COMMENT ON TABLE VDA_V_INVOICE_PAYMENTS_DETAILS IS 'This view provides access to the payment details information for payments linked with invoices'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.FK_ACCOUNT IS 'The account the milestone is linked with'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.FK_INVOICE IS 'The PK to uniquely identify invoice the payment is linked with'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.MP_AMOUNT IS 'The Payment amount linked with the invoice'
/


COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.STUDY_NUMBER IS 'The Study Number of the study the payment  is linked with'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.STUDY_TITLE IS 'The Study Title of the study the payment is linked with'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.MSPAY_DATE IS 'The payment date'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.MSPAY_TYPE IS 'The payment type'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.MSPAY_AMT_RECVD IS 'The payment amount received'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.MSPAY_DESC IS 'The payment description'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.MSPAY_COMMENTS IS 'The payment comments'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.PK_MILEPAYMENT IS 'The Primary Key of the payment record'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.LAST_MODIFIED_DATE IS 'The date the record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.CREATED_ON IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.FK_STUDY IS 'The Foreign Key to the study milestone is linked with'
/



