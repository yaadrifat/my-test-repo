/* Formatted on 3/11/2014 3:18:36 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_BUDGET_MILESTONES  (View)
--
--  Dependencies:
--   F_GET_CODELSTDESC (Function)
--   F_GET_MTYPE (Function)
--   ERV_BUDGET (View)
--   PKG_DATEUTIL (Synonym)
--   VDA_V_MILESTONES (View)
--

CREATE OR REPLACE FORCE VIEW VDA_V_BUDGET_MILESTONES
(
   STUDY_NUMBER,
   STUDY_TITLE,
   MSRUL_CATEGORY,
   MSRUL_AMOUNT,
   MSRUL_PT_COUNT,
   MSRUL_PT_STATUS,
   MSRUL_LIMIT,
   MSRUL_PAY_TYPE,
   MSRUL_PAY_FOR,
   MSRUL_STATUS,
   MSRUL_PROT_CAL,
   MSRUL_VISIT,
   MSRUL_MS_RULE,
   MSRUL_EVENT_STAT,
   FK_ACCOUNT,
   PK_MILESTONE,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATED_ON,
   FK_STUDY,
   EVENT_NAME,
   MILESTONE_ACHIEVEDCOUNT,
   MILESTONE_ACHIEVEDAMOOUNT,
   LAST_CHECKED_ON,
   MILESTONE_DESCRIPTION,
   BUDGET_NAME,
   LINEITEM_NAME,
   PROT_CALENDAR,
   BGTSECTION_NAME
)
AS
   SELECT   m.STUDY_NUMBER,
            m.STUDY_TITLE,
            MSRUL_CATEGORY,
            MSRUL_AMOUNT,
            MSRUL_PT_COUNT,
            MSRUL_PT_STATUS,
            MSRUL_LIMIT,
            MSRUL_PAY_TYPE,
            MSRUL_PAY_FOR,
            MSRUL_STATUS,
            MSRUL_PROT_CAL,
            MSRUL_VISIT,
            MSRUL_MS_RULE,
            MSRUL_EVENT_STAT,
            FK_ACCOUNT,
            PK_MILESTONE,
            m.CREATOR,
            m.LAST_MODIFIED_BY,
            m.LAST_MODIFIED_DATE,
            m.CREATED_ON,
            m.FK_STUDY,
            EVENT_NAME,
            MILESTONE_ACHIEVEDCOUNT,
            MILESTONE_ACHIEVEDAMOUNT,
            LAST_CHECKED_ON,
            MILESTONE_DESCRIPTION,
            BUDGET_NAME,
            LINEITEM_NAME,
            PROT_CALENDAR,
            BGTSECTION_NAME
     FROM   VDA.VDA_V_MILESTONES m, esch.erv_budget
    WHERE       fk_budget = pk_budget
            AND FK_BGTCAL = pk_bgtcal
            AND FK_BGTSECTION = pk_budgetsec
            AND FK_LINEITEM = pk_lineitem
/
COMMENT ON TABLE VDA_V_BUDGET_MILESTONES IS 'This view provides data on milestones that are created from a budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.STUDY_NUMBER IS 'The Study Number'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.STUDY_TITLE IS 'The Study Title'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.MSRUL_CATEGORY IS 'The Milestone Category'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.MSRUL_AMOUNT IS 'The Milestone amount'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.MSRUL_PT_COUNT IS 'The Milestone Patient Count for the achievement rule'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.MSRUL_PT_STATUS IS 'The Milestone Patient Status for the achievement rule'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.MSRUL_LIMIT IS 'The Milestone Patient Status for the achievement rule'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.MSRUL_PAY_TYPE IS 'The Milestone Payment Type'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.MSRUL_PAY_FOR IS 'The Milestone Payment For'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.MSRUL_STATUS IS 'The Milestone Status for the achievement rule'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.MSRUL_PROT_CAL IS 'The Milestone Calendar for the achievement rule'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.MSRUL_VISIT IS 'The Milestone Visit for the achievement rule'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.MSRUL_MS_RULE IS 'The Milestone achievement rule'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.MSRUL_EVENT_STAT IS 'The Milestone Event Status for achievement the rule'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.FK_ACCOUNT IS 'The account the milestone is linked with'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.PK_MILESTONE IS 'The Primary Key of the Milestone'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.CREATOR IS 'The date the record was created on (Audit)'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.LAST_MODIFIED_DATE IS 'The date the  record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.CREATED_ON IS 'The date the  record was created on (Audit)'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.FK_STUDY IS 'The Foreign Key of the Study the milestone is linked with'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.EVENT_NAME IS 'The Milestone Event Name for achievement the rule'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.MILESTONE_ACHIEVEDCOUNT IS 'The Milestones achieved so far'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.MILESTONE_ACHIEVEDAMOOUNT IS 'The Milestones amount achieved so far'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.LAST_CHECKED_ON IS 'The Last checked date for the milestone achievement'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.MILESTONE_DESCRIPTION IS 'The Milestone description'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.BUDGET_NAME IS 'he name of the budget the MS is linked with'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.LINEITEM_NAME IS 'The name of the line item the MS is linked with'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.PROT_CALENDAR IS 'The name of the budget calendar the MS is linked with'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.BGTSECTION_NAME IS 'The name of the budget section the MS is linked with'
/


