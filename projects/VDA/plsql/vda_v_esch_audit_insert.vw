/* Formatted on 3/11/2014 3:18:57 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_ESCH_AUDIT_INSERT  (View)
--
--  Dependencies:
--   AUDIT_INSERT (Table)
--   AUDIT_ROW (Table)
--

CREATE OR REPLACE FORCE VIEW VDA_V_ESCH_AUDIT_INSERT
(
   USER_NAME,
   TIMESTAMP,
   TABLE_NAME,
   ROW_DATA
)
AS
   SELECT   r.user_name,
            r.timestamp,
            r.table_name,
            c.row_data
     FROM   esch.audit_insert c, esch.audit_row r
    WHERE   r.action = 'I' AND c.raid = r.raid
/
COMMENT ON TABLE VDA_V_ESCH_AUDIT_INSERT IS 'This view provides access to the detail audit trail on Insert actions in ESCH schema'
/

COMMENT ON COLUMN VDA_V_ESCH_AUDIT_INSERT.USER_NAME IS 'The user who triggered the action'
/

COMMENT ON COLUMN VDA_V_ESCH_AUDIT_INSERT.TIMESTAMP IS 'The date and time of the action'
/

COMMENT ON COLUMN VDA_V_ESCH_AUDIT_INSERT.TABLE_NAME IS 'The name of the table'
/

COMMENT ON COLUMN VDA_V_ESCH_AUDIT_INSERT.ROW_DATA IS 'The pipe delimited data at the time of insert, The order of the column is same as the order of the columns in the table'
/


