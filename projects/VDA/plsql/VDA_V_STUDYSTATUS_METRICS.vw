/* Formatted on 3/18/2014 4:08:01 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_STUDYSTATUS_METRICS  (View)
--
--  Dependencies:
--   VDA_V_STUDYSTATUS_TIMELINE (View)
--

CREATE OR REPLACE FORCE VIEW VDA_V_STUDYSTATUS_METRICS
(
   STUDY_NUMBER,
   PK_STUDY,
   STUDY_TITLE,
   FIRST_NOT_ACTIVE,
   FIRST_IRB_APPROVED,
   FIRST_ACTIVE,
   LAST_PERM_CLOSURE,
   DAYS_IRB_APPROVAL,
   DAYS_NOTACTIVE_TO_ACTIVE,
   DAYS_NOTACTIVE_TO_CLOSURE
)
AS
   SELECT   x."STUDY_NUMBER",
            x."PK_STUDY",
            x."STUDY_TITLE",
            x."FIRST_NOT_ACTIVE",
            x."FIRST_IRB_APPROVED",
            x."FIRST_ACTIVE",
            x."LAST_PERM_CLOSURE",
            (first_irb_approved - first_not_active) days_irb_approval,
            (first_active - first_not_active) days_notactive_to_active,
            (last_perm_closure - first_not_active) days_notactive_to_closure
     FROM   VDA_V_STUDYSTATUS_TIMELINE x
/

COMMENT ON TABLE VDA_V_STUDYSTATUS_METRICS IS 'This view provides access to the study status timeline metrics for predefined trial statuses'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_METRICS.STUDY_NUMBER IS 'The Study Number'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_METRICS.PK_STUDY IS 'The PK of the study'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_METRICS.STUDY_TITLE IS 'The Study Title'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_METRICS.FIRST_NOT_ACTIVE IS 'The date study was initiated-first ''Not Active'' or an equivalent status'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_METRICS.FIRST_IRB_APPROVED IS 'The date study was IRB Approved-first ''IRB Approved'' or an equivalent status'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_METRICS.FIRST_ACTIVE IS 'The date study was Active or Enrolling  -first ''Active/Enrolling'' or an equivalent status'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_METRICS.LAST_PERM_CLOSURE IS 'The date study was Closed -last ''Closed or Retired'' or an equivalent status'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_METRICS.DAYS_IRB_APPROVAL IS 'Number of days it took from study initiation to IRB Approval'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_METRICS.DAYS_NOTACTIVE_TO_ACTIVE IS 'Number of days it took from study initiation to Study Active/Enrolling'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_METRICS.DAYS_NOTACTIVE_TO_CLOSURE IS 'Number of days it took from study initiation to Study Closure/Completion'
/
