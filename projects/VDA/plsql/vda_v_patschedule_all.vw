/* Formatted on 3/11/2014 3:19:39 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_PATSCHEDULE_ALL  (View)
--
--  Dependencies:
--   F_GET_DURUNIT (Function)
--   EVENT_ASSOC (Table)
--   SCH_PROTOCOL_VISIT (Table)
--   ER_SITE (Table)
--   VDA_V_PAT_SCHEDULE (View)
--

CREATE OR REPLACE FORCE VIEW VDA_V_PATSCHEDULE_ALL
(
   PAT_STUDY_ID,
   FK_STUDY,
   CALENDAR_PK,
   PTSCH_VISIT,
   PTSCH_SUGSCH_DATE,
   PTSCH_ACTSCH_DATE,
   PTSCH_EVENT_PK,
   PTSCH_EVENT,
   PTSCH_EVENT_STAT,
   SCH_EVENTS1_PK,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATED_ON,
   FK_PER,
   EVENTSTAT_DATE,
   EVENTSTAT_NOTES,
   PATPROT_STAT,
   FK_SITE_ENROLLING,
   EVENT_SEQUENCE,
   SERVICE_SITE_ID,
   FACILITY_ID,
   COVERAGE_TYPE,
   CALENDAR_NAME,
   PTSCH_VISIT_WIN,
   EVENT_WINDOW_START,
   EVENT_WINDOW_END,
   VISIT_NAME,
   SITE_NAME
)
AS
   SELECT   PAT_STUDY_ID,
            FK_STUDY,
            CALENDAR_PK,
            PTSCH_VISIT,
            PTSCH_SUGSCH_DATE,
            PTSCH_ACTSCH_DATE,
            PTSCH_EVENT_PK,
            PTSCH_EVENT,
            PTSCH_EVENT_STAT,
            a.EVENT_ID sch_events1_pk,
            a.CREATOR,
            a.LAST_MODIFIED_BY,
            a.LAST_MODIFIED_DATE,
            a.CREATED_ON,
            FK_PER,
            EVENTSTAT_DATE,
            EVENTSTAT_NOTES,
            PATPROT_STAT,
            fk_site_enrolling,
            a.event_sequence,
            a.service_site_id,
            a.facility_id,
            coverage_type,
            cal.NAME AS calendar_name,
            (   NVL (ev.FUZZY_PERIOD, 0)
             || ' '
             || NVL (eres.F_Get_Durunit (ev.EVENT_DURATIONBEFORE), 'days')
             || ' Before / '
             || NVL (ev.EVENT_FUZZYAFTER, 0)
             || ' '
             || NVL (eres.F_Get_Durunit (ev.EVENT_DURATIONAFTER), 'days')
             || ' After')
               PTSCH_VISIT_WIN,
            NVL (PTSCH_ACTSCH_DATE
                 - DECODE (ev.event_durationbefore,
                           'D',
                           (ev.fuzzy_period * 1),
                           'H',
                           1,
                           'W',
                           (ev.fuzzy_period * 7),
                           'M',
                           (ev.fuzzy_period * 30)), PTSCH_ACTSCH_DATE)
               AS event_window_start,
            NVL (PTSCH_ACTSCH_DATE
                 + DECODE (ev.event_durationafter,
                           'D',
                           (ev.event_fuzzyafter * 1),
                           'H',
                           1,
                           'W',
                           (ev.event_fuzzyafter * 7),
                           'M',
                           (ev.event_fuzzyafter * 30)), PTSCH_ACTSCH_DATE)
               AS event_window_end,
            sch_protocol_visit.visit_name,
            site_name
     FROM   VDA.VDA_V_PAT_SCHEDULE a,
            esch.event_assoc cal,
            esch.event_assoc ev,
            esch.sch_protocol_visit,
            eres.er_site s
    WHERE       cal.EVENT_ID = a.CALENDAR_PK
            AND ev.event_id = a.PTSCH_EVENT_PK
            AND PK_PROTOCOL_VISIT = a.PTSCH_VISIT
            AND s.pk_site = a.FK_SITE_ENROLLING
/
COMMENT ON TABLE VDA_V_PATSCHEDULE_ALL IS 'This view provides access to the patient schedules'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.PAT_STUDY_ID IS 'The Patient Study ID'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.FK_STUDY IS 'The Primary Key of the study schedule is linked with'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.CALENDAR_PK IS 'The Primary Key of the calendar schedule is linked with'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.PTSCH_VISIT IS 'The Scheduled Visit Primary Key'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.PTSCH_SUGSCH_DATE IS 'The suggested date for the scheduled event'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.PTSCH_ACTSCH_DATE IS 'The actual date for the scheduled event'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.PTSCH_EVENT_PK IS 'The PK of the event'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.PTSCH_EVENT IS 'The name of the event'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.PTSCH_EVENT_STAT IS 'The event status'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.SCH_EVENTS1_PK IS 'The Primary Key of the scheduled event'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.LAST_MODIFIED_DATE IS 'The date the  record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.CREATED_ON IS 'The date the record was created on (Audit)'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.FK_PER IS 'The Primary Key of the patient the schedule is linked with'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.EVENTSTAT_DATE IS 'The date of the current event status'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.EVENTSTAT_NOTES IS 'The notes linked with the current event status'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.PATPROT_STAT IS 'The flag to indicate if the schedule is current. 1 indicates current schedule, 0 indicates old or discontinued schedule'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.FK_SITE_ENROLLING IS 'The Primary Key of the enrolling site'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.EVENT_SEQUENCE IS 'The sequenceof the event in the visit'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.SERVICE_SITE_ID IS 'The site of service'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.FACILITY_ID IS 'The facility linked with the event'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.COVERAGE_TYPE IS 'The coverage type of the event'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.CALENDAR_NAME IS 'The Calendar name'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.PTSCH_VISIT_WIN IS 'The scheduled visit window per Calendar'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.EVENT_WINDOW_START IS 'The scheduled event window-start date'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.EVENT_WINDOW_END IS 'The scheduled event window-end  date'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.VISIT_NAME IS 'The scheduled visit'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.SITE_NAME IS 'The patient enrolling site name'
/


