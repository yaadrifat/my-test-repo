/* Formatted on 3/11/2014 3:20:08 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_USERSITES  (View)
--
--  Dependencies:
--   ER_CODELST (Table)
--   ER_USER (Table)
--   ER_USERSITE (Table)
--   ER_SITE (Table)
--

CREATE OR REPLACE FORCE VIEW VDA_V_USERSITES
(
   PK_SITE,
   FK_CODELST_TYPE,
   FK_ACCOUNT,
   SITE_NAME,
   PARENT_SITE,
   SITE_STAT,
   PARENT_SITE_NAME,
   SITETYPE,
   PK_USERSITE,
   FK_USER,
   USERSITE_RIGHT,
   SITE_ALTID,
   USER_NAME
)
AS
   SELECT   A.PK_SITE,
            A.FK_CODELST_TYPE,
            A.FK_ACCOUNT,
            A.SITE_NAME,
            A.SITE_PARENT AS PARENT_SITE,
            A.SITE_STAT,
            (SELECT   SITE_NAME
               FROM   eres.er_site B
              WHERE   B.PK_SITE = A.SITE_PARENT)
               AS PARENT_SITE_NAME,
            (SELECT   C.CODELST_DESC
               FROM   eres.ER_CODELST C
              WHERE   C.PK_CODELST = A.FK_CODELST_TYPE)
               SITETYPE,
            PK_USERSITE,
            FK_USER,
            DECODE (USERSITE_RIGHT, 5, 'NEW', 6, 'EDIT', 7, 'ALL', 'VIEW')
               USERSITE_RIGHT,
            A.SITE_ALTID,
            usr.USR_FIRSTNAME || ' ' || usr.USR_LASTNAME AS user_name
     FROM   eres.ER_SITE A, eres.ER_USERSITE U, eres.ER_USER usr
    WHERE       U.FK_SITE = A.PK_SITE
            AND usr.pk_user = u.fk_user
            AND usersite_right > 0
/
COMMENT ON TABLE VDA_V_USERSITES IS 'This view provides access to the user-organization access'
/

COMMENT ON COLUMN VDA_V_USERSITES.USER_NAME IS 'The user name'
/

COMMENT ON COLUMN VDA_V_USERSITES.PARENT_SITE_NAME IS 'The Parent Site Name (immediate parent)'
/

COMMENT ON COLUMN VDA_V_USERSITES.SITETYPE IS 'The Site Type Description'
/

COMMENT ON COLUMN VDA_V_USERSITES.PK_USERSITE IS 'The PK of usersite record'
/

COMMENT ON COLUMN VDA_V_USERSITES.FK_USER IS 'The FK to user'
/

COMMENT ON COLUMN VDA_V_USERSITES.USERSITE_RIGHT IS 'The right'
/

COMMENT ON COLUMN VDA_V_USERSITES.SITE_ALTID IS 'The site alternate id'
/

COMMENT ON COLUMN VDA_V_USERSITES.PK_SITE IS 'The Primary Key of the site'
/

COMMENT ON COLUMN VDA_V_USERSITES.FK_CODELST_TYPE IS 'The FK to code list for site type'
/

COMMENT ON COLUMN VDA_V_USERSITES.FK_ACCOUNT IS 'The account the site is linked with'
/

COMMENT ON COLUMN VDA_V_USERSITES.SITE_NAME IS 'The Site Name'
/

COMMENT ON COLUMN VDA_V_USERSITES.PARENT_SITE IS 'The FK to Parent Site (immediate parent)'
/

COMMENT ON COLUMN VDA_V_USERSITES.SITE_STAT IS 'The Site Status'
/


