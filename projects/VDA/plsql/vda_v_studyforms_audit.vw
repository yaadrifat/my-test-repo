/* Formatted on 3/11/2014 3:19:54 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_STUDYFORMS_AUDIT  (View)
--
--  Dependencies:
--   ER_STUDY (Table)
--   ER_STUDYFORMS (Table)
--   ER_FORMAUDITCOL (Table)
--   ER_FORMLIB (Table)
--

CREATE OR REPLACE FORCE VIEW VDA_V_STUDYFORMS_AUDIT
(
   PK_FORMAUDITCOL,
   FK_FILLEDFORM,
   FK_FORM,
   FORM_NAME,
   FA_SYSTEMID,
   FA_DATETIME,
   FA_FLDNAME,
   FA_OLDVALUE,
   FA_NEWVALUE,
   FA_MODIFIEDBY_NAME,
   FA_REASON,
   STUDY_NUMBER,
   STUDY_TITLE,
   FK_ACCOUNT
)
AS
   SELECT   PK_FORMAUDITCOL,
            FK_FILLEDFORM,
            FK_FORM,
            form_name,
            FA_SYSTEMID,
            FA_DATETIME,
            FA_FLDNAME,
            FA_OLDVALUE,
            FA_NEWVALUE,
            FA_MODIFIEDBY_NAME,
            FA_REASON,
            study_number,
            study_title,
            b.fk_account
     FROM   eres.er_formauditcol,
            eres.er_formlib b,
            eres.er_studyforms sf,
            eres.er_study
    WHERE       FA_FORMTYPE = 'S'
            AND b.pk_formlib = FK_FORM
            AND sf.pk_studyforms = FK_FILLEDFORM
            AND sf.fk_formlib = FK_FORM
            AND pk_study = fk_study
/
COMMENT ON TABLE VDA_V_STUDYFORMS_AUDIT IS 'This view provides access to the column update level Audit information of account level forms'
/

COMMENT ON COLUMN VDA_V_STUDYFORMS_AUDIT.PK_FORMAUDITCOL IS 'The Primary Key of the Audit Transaction'
/

COMMENT ON COLUMN VDA_V_STUDYFORMS_AUDIT.FK_FILLEDFORM IS 'The FK to the form response (account form)'
/

COMMENT ON COLUMN VDA_V_STUDYFORMS_AUDIT.FK_FORM IS 'The FK to the form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMS_AUDIT.FORM_NAME IS 'The form name'
/

COMMENT ON COLUMN VDA_V_STUDYFORMS_AUDIT.FA_SYSTEMID IS 'The systemid of the field'
/

COMMENT ON COLUMN VDA_V_STUDYFORMS_AUDIT.FA_DATETIME IS 'The date-time of the transaction'
/

COMMENT ON COLUMN VDA_V_STUDYFORMS_AUDIT.FA_FLDNAME IS 'The user defined field name'
/

COMMENT ON COLUMN VDA_V_STUDYFORMS_AUDIT.FA_OLDVALUE IS 'The old value of the field'
/

COMMENT ON COLUMN VDA_V_STUDYFORMS_AUDIT.FA_NEWVALUE IS 'The new value of the field'
/

COMMENT ON COLUMN VDA_V_STUDYFORMS_AUDIT.FA_MODIFIEDBY_NAME IS 'The user who modified the form response data'
/

COMMENT ON COLUMN VDA_V_STUDYFORMS_AUDIT.FA_REASON IS 'The reason for change'
/

COMMENT ON COLUMN VDA_V_STUDYFORMS_AUDIT.STUDY_NUMBER IS 'The study number of the study the form response is linked with'
/

COMMENT ON COLUMN VDA_V_STUDYFORMS_AUDIT.STUDY_TITLE IS 'The title of the study the form response is linked with'
/

COMMENT ON COLUMN VDA_V_STUDYFORMS_AUDIT.FK_ACCOUNT IS 'The account the form response is linked with'
/


