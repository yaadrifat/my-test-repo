/* Formatted on 3/11/2014 3:18:38 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_BUDGET_SECTION  (View)
--
--  Dependencies:
--   F_GET_FSTAT (Function)
--   F_GET_YESNO (Function)
--   ER_STUDY (Table)
--   SCH_BGTCAL (Table)
--   SCH_BGTSECTION (Table)
--   SCH_BUDGET (Table)
--   SCH_CODELST (Table)
--   ER_USER (Table)
--   ER_SITE (Table)
--

CREATE OR REPLACE FORCE VIEW VDA_V_BUDGET_SECTION
(
   BGTDT_NAME,
   BGTDT_VERSION,
   BGTDT_TEMPLATE,
   BGTDT_STUDY_NUM,
   BGTDT_STUDY_TITLE,
   BGTDT_ORGANIZATION,
   BGTDT_STATUS,
   BGTDT_DESCRIPTION,
   BGTDT_CREATED_BY,
   BGTDT_CURRENCY,
   FK_ACCOUNT,
   PK_BUDGET,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATED_ON,
   FK_STUDY,
   BGTCAL_SPONSOROHEAD,
   SPONSOR_OHEAD_FLAG,
   BGTCAL_INDIRECTCOST,
   BGTCAL_FRGBENEFIT,
   FRINGEBFT_FLAG,
   BGTCAL_DISCOUNT,
   DISCNT_FLAG,
   EXCLUD_SOC,
   BUDGET_COMBFLAG,
   PK_BUDGETSEC,
   BGTSECTION_NAME,
   BGTSECTION_SEQUENCE,
   BGTSECTION_TYPE,
   BGTSECTION_PATNO,
   BGTSECTION_NOTES,
   BGTSECTION_PERSONLFLAG,
   BUDGET_VISIT_PK
)
AS
   SELECT   BUDGET_NAME BGTDT_NAME,
            BUDGET_VERSION BGTDT_VERSION,
            NVL ( (SELECT   CODELST_DESC
                     FROM   esch.SCH_CODELST
                    WHERE   PK_CODELST = B.BUDGET_TEMPLATE),
                 (SELECT   BUDGET_NAME
                    FROM   esch.SCH_BUDGET
                   WHERE   PK_BUDGET = B.BUDGET_TEMPLATE))
               BGTDT_TEMPLATE,
            (SELECT   STUDY_NUMBER
               FROM   eres.ER_STUDY
              WHERE   PK_STUDY = FK_STUDY)
               BGTDT_STUDY_NUM,
            (SELECT   STUDY_TITLE
               FROM   eres.ER_STUDY
              WHERE   PK_STUDY = FK_STUDY)
               BGTDT_STUDY_TITLE,
            (SELECT   SITE_NAME
               FROM   eres.ER_SITE
              WHERE   PK_SITE = FK_SITE)
               BGTDT_ORGANIZATION,
            ERES.F_GEt_Fstat (BUDGET_STATUS) BGTDT_STATUS,
            BUDGET_DESC BGTDT_DESCRIPTION,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = BUDGET_CREATOR)
               BGTDT_CREATED_BY,
            (SELECT   CODELST_DESC
               FROM   esch.SCH_CODELST
              WHERE   PK_CODELST = BUDGET_CURRENCY)
               BGTDT_CURRENCY,
            B.FK_ACCOUNT,
            PK_BUDGET,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = B.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = B.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            B.LAST_MODIFIED_DATE,
            B.CREATED_ON,
            fk_study,
            BGTCAL_SPONSOROHEAD,
            ERES.F_GEt_Yesno (BGTCAL_SPONSORFLAG) SPONSOR_OHEAD_FLAG,
            BGTCAL_INDIRECTCOST,
            BGTCAL_FRGBENEFIT,
            ERES.F_GEt_Yesno (BGTCAL_FRGFLAG) FRINGEBFT_FLAG,
            BGTCAL_DISCOUNT,
            ERES.F_GEt_Yesno (BGTCAL_DISCOUNTFLAG) DISCNT_FLAG,
            ERES.F_GEt_Yesno (BGTCAL_EXCLDSOCFLAG) EXCLUD_SOC,
            BUDGET_COMBFLAG,
            pk_budgetsec,
            bgtsection_name,
            bgtsection_sequence,
            bgtsection_type,
            bgtsection_patno,
            bgtsection_notes,
            bgtsection_personlflag,
            fk_visit
     FROM   ESCH.SCH_BUDGET B, ESCH.SCH_BGTCAL C, ESCH.SCH_BGTSECTION S
    WHERE       C.FK_BUDGET = B.PK_BUDGET
            AND (BUDGET_DELFLAG IS NULL OR BUDGET_DELFLAG <> 'Y')
            AND S.fk_bgtcal = C.pk_bgtcal
            AND NVL (BGTSECTION_DELFLAG, 'N') <> 'Y'
            AND NVL (BGTCAL_DELFLAG, 'N') <> 'Y'
/
COMMENT ON TABLE VDA_V_BUDGET_SECTION IS 'This view provides access to the Budget Sections design'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.EXCLUD_SOC IS 'The Exclude Standard of Care cost from totals flag'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.BUDGET_COMBFLAG IS 'The flag to indicate if the budget is a combined budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.PK_BUDGETSEC IS 'The Primary Key of the Budget Section Record'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.BGTSECTION_NAME IS 'The name of the Budget Section'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.BGTSECTION_SEQUENCE IS 'The Sequence of the Section in the Budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.BGTSECTION_TYPE IS 'The Budget Section type'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.BGTSECTION_PATNO IS 'The number of patients the section is applicable for (used in calculating cost)'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.BGTSECTION_NOTES IS 'The notes linked with the section'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.BGTSECTION_PERSONLFLAG IS 'The Flag to indicate if it is a Personnel type section'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.BUDGET_VISIT_PK IS 'The Foreign Key to the visit the Budget line item is linked with'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.BGTDT_NAME IS 'The name of the Budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.BGTDT_VERSION IS 'The version of the Budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.BGTDT_TEMPLATE IS 'The template used for creating the budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.BGTDT_STUDY_NUM IS 'The study number of the study linked with the budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.BGTDT_STUDY_TITLE IS 'The title of the study linked with the budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.BGTDT_ORGANIZATION IS 'The organization linked with the budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.BGTDT_STATUS IS 'The status of the budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.BGTDT_DESCRIPTION IS 'The description of the budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.BGTDT_CREATED_BY IS 'The user who created the Budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.BGTDT_CURRENCY IS 'The currency the budget is created in'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.FK_ACCOUNT IS 'The Account linked with the Budget'''
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.PK_BUDGET IS 'The Primary Key of the main Budget record'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.CREATOR IS 'The user who created the Budget record (Audit)'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.LAST_MODIFIED_BY IS 'The user who last modified the Budget Section record(Audit)'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.LAST_MODIFIED_DATE IS 'The date the Budget Section record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.CREATED_ON IS 'The date the Budget Section record was created on (Audit)'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.FK_STUDY IS 'The Foreign Key to the study the patient study status record is linked with'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.BGTCAL_SPONSOROHEAD IS 'The Sponsor Overhead number'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.SPONSOR_OHEAD_FLAG IS 'The Sponsor Overhead flag'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.BGTCAL_INDIRECTCOST IS 'The indirect cost number to be applied to selected line items'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.BGTCAL_FRGBENEFIT IS 'The Fringe Benefit number to be applied to selected line items'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.FRINGEBFT_FLAG IS 'The Fringe Benefit calculation flag'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.BGTCAL_DISCOUNT IS 'The discount or markup number'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.DISCNT_FLAG IS 'The discount or markup flag '
/


