/* Formatted on 3/11/2014 3:18:47 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_ER_CODELST  (View)
--
--  Dependencies:
--   ER_CODELST (Table)
--

CREATE OR REPLACE FORCE VIEW VDA_V_ER_CODELST
(
   PK_CODELST,
   CODELST_TYPE,
   CODELST_SUBTYP,
   CODELST_DESC,
   CODELST_HIDE,
   CODELST_SEQ,
   CODELST_CUSTOM_COL,
   CODELST_CUSTOM_COL1,
   CODELST_STUDY_ROLE
)
AS
   SELECT   PK_CODELST,
            CODELST_TYPE,
            CODELST_SUBTYP,
            CODELST_DESC,
            CODELST_HIDE,
            CODELST_SEQ,
            CODELST_CUSTOM_COL,
            CODELST_CUSTOM_COL1,
            CODELST_STUDY_ROLE
     FROM   eres.er_codelst
/
COMMENT ON TABLE VDA_V_ER_CODELST IS 'This view provides access to all the codes (eres schema) used in the application. Mostly, these codes are available in the form of ''Dropdown'' data.'
/

COMMENT ON COLUMN VDA_V_ER_CODELST.PK_CODELST IS 'This uniquely identifies the code. Primary key of the table'
/

COMMENT ON COLUMN VDA_V_ER_CODELST.CODELST_TYPE IS 'The code type to categorize the codes'
/

COMMENT ON COLUMN VDA_V_ER_CODELST.CODELST_SUBTYP IS 'The second level categorization of the code. This is in addition to the code type column and is sometimes used in the application for specific purposes'
/

COMMENT ON COLUMN VDA_V_ER_CODELST.CODELST_DESC IS 'The Description of the code. This value is shown in the application instead of the code'
/

COMMENT ON COLUMN VDA_V_ER_CODELST.CODELST_HIDE IS 'A flag (Y/N) to indicate if the value has to be displayed or not. Default N'
/

COMMENT ON COLUMN VDA_V_ER_CODELST.CODELST_SEQ IS 'This number indicates the sequence in which the values for a particular code type/sub-type will be displayed in the application'
/

COMMENT ON COLUMN VDA_V_ER_CODELST.CODELST_CUSTOM_COL IS 'This column can be used to establish relationships between codes or some specific attributes.'
/

COMMENT ON COLUMN VDA_V_ER_CODELST.CODELST_CUSTOM_COL1 IS 'This column can be used to establish relationships between codes or some specific attributes. In code_type=''studyidtype'', this stores the dropdown html structure'
/

COMMENT ON COLUMN VDA_V_ER_CODELST.CODELST_STUDY_ROLE IS 'This is used to store the Study Team Roles that may have access to the code'
/


