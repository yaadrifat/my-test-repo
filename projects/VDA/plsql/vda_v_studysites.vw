/* Formatted on 3/11/2014 3:19:57 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_STUDYSITES  (View)
--
--  Dependencies:
--   ER_CODELST (Table)
--   ER_STUDY (Table)
--   ER_STUDYSITES (Table)
--   ER_USER (Table)
--   GETUSER (Function)
--   ER_SITE (Table)
--

CREATE OR REPLACE FORCE VIEW VDA_V_STUDYSITES
(
   PK_STUDYSITES,
   FK_STUDY,
   FK_SITE,
   STUDYSITE_TYPE,
   STUDYSITE_LSAMPLESIZE,
   IS_STUDYSITE_PUBLIC,
   INCLUDE_IN_REPORTS,
   RID,
   CREATOR,
   LAST_MODIFIED_BY,
   CREATED_ON,
   LAST_MODIFIED_ON,
   IP_ADD,
   STUDYSITE_ENRCOUNT,
   IS_REVIEWBASED_ENR,
   SEND_ENROLLMENT_NOTIFICATION,
   SEND_ENROLLMENT_APPROVAL,
   STUDY_NUMBER,
   SITE_NAME,
   FK_ACCOUNT
)
AS
   SELECT   si.PK_STUDYSITES,
            si.FK_STUDY,
            si.FK_SITE,
            (SELECT   codelst_desc
               FROM   eres.er_codelst
              WHERE   pk_codelst = si.FK_CODELST_STUDYSITETYPE)
               STUDYSITE_TYPE,
            si.STUDYSITE_LSAMPLESIZE,
            DECODE (si.STUDYSITE_PUBLIC,
                    '0',
                    'No',
                    1,
                    'Yes')
               is_studysite_public,
            DECODE (si.STUDYSITE_REPINCLUDE,
                    '0',
                    'No',
                    1,
                    'Yes')
               include_in_reports,
            si.RID,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = si.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = si.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            si.CREATED_ON,
            si.LAST_MODIFIED_ON,
            si.IP_ADD,
            si.STUDYSITE_ENRCOUNT,
            DECODE (si.IS_REVIEWBASED_ENR,
                    '0',
                    'No',
                    1,
                    'Yes')
               IS_REVIEWBASED_ENR,
            eres.GETUSER (si.ENR_NOTIFYTO) send_enrollment_notification,
            eres.GETUSER (si.ENR_STAT_NOTIFYTO) send_enrollment_approval,
            st.study_number,
            (SELECT   site_name
               FROM   eres.er_site
              WHERE   pk_site = si.fk_site)
               site_name,
            st.fk_account
     FROM   eres.er_studysites si, eres.er_study st
    WHERE   st.pk_study = si.fk_study
/
COMMENT ON TABLE VDA_V_STUDYSITES IS 'This view provides information on the sites or organizations associated with a study'
/

COMMENT ON COLUMN VDA_V_STUDYSITES.PK_STUDYSITES IS 'Primary key'
/

COMMENT ON COLUMN VDA_V_STUDYSITES.FK_STUDY IS 'This column identifies the study to which this study site is associated'
/

COMMENT ON COLUMN VDA_V_STUDYSITES.FK_SITE IS 'This column identifies the site'
/

COMMENT ON COLUMN VDA_V_STUDYSITES.STUDYSITE_TYPE IS 'This column identifies the study site type'
/

COMMENT ON COLUMN VDA_V_STUDYSITES.STUDYSITE_LSAMPLESIZE IS 'The local sample size for this study site'
/

COMMENT ON COLUMN VDA_V_STUDYSITES.IS_STUDYSITE_PUBLIC IS 'Flag for  information to be available to the public'
/

COMMENT ON COLUMN VDA_V_STUDYSITES.INCLUDE_IN_REPORTS IS 'Flag for study site to be included in reports'
/

COMMENT ON COLUMN VDA_V_STUDYSITES.RID IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database.'
/

COMMENT ON COLUMN VDA_V_STUDYSITES.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYSITES.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYSITES.CREATED_ON IS 'The date the record was created on (Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYSITES.LAST_MODIFIED_ON IS 'The date the  record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYSITES.IP_ADD IS 'The IP ADDRESS of the client machine (Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYSITES.STUDYSITE_ENRCOUNT IS 'The number of current patients enrolled (with any status) for the study-site combination'
/

COMMENT ON COLUMN VDA_V_STUDYSITES.IS_REVIEWBASED_ENR IS 'Identifies if the site will have review based enrollment.'
/

COMMENT ON COLUMN VDA_V_STUDYSITES.SEND_ENROLLMENT_NOTIFICATION IS 'Identifies the users who will be notified when an ''enrolled'' status is added'
/

COMMENT ON COLUMN VDA_V_STUDYSITES.SEND_ENROLLMENT_APPROVAL IS 'Identifies the users who will be notified when a patient status with code_subtyp = "enroll_appr" or code_subtyp = "enroll_denied" status is added'
/

COMMENT ON COLUMN VDA_V_STUDYSITES.STUDY_NUMBER IS 'The Study number of the associated study'
/

COMMENT ON COLUMN VDA_V_STUDYSITES.SITE_NAME IS 'The name of the associated site'
/

COMMENT ON COLUMN VDA_V_STUDYSITES.FK_ACCOUNT IS 'The account the study site is linked with'
/


