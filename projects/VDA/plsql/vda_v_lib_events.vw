/* Formatted on 3/11/2014 3:19:09 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_LIB_EVENTS  (View)
--
--  Dependencies:
--   F_GET_DURUNIT (Function)
--   EVENT_DEF (Table)
--   SCH_CODELST (Table)
--   SCH_EVENTCOST (Table)
--   SCH_EVENTDOC (Table)
--   SCH_EVENTUSR (Table)
--   ER_USER (Table)
--   ER_SITE (Table)
--

CREATE OR REPLACE FORCE VIEW VDA_V_LIB_EVENTS
(
   LBEVE_CATEGORY,
   LBEVE_EVENT,
   LBEVE_DESCRIPTION,
   LBEVE_CPTCODE,
   LBEVE_DURATION,
   LBEVE_VISIT_WIN_B4,
   LBEVE_VISIT_WIN_AFT,
   LBEVE_NOTES,
   LBEVE_PTMESS_B4,
   LBEVE_PTMESS_AFT,
   LBEVE_PTDAYS_B4,
   LBEVE_PTDAYS_AFT,
   LBEVE_USRMESS_B4,
   LBEVE_USRMESS_AFT,
   LBEVE_USRDAYS_B4,
   LBEVE_USRDAYS_AFT,
   LBEVE_COST_CNT,
   LBEVE_APPNX_CNT,
   LBEVE_RESOURCE_CNT,
   FK_ACCOUNT,
   EVENT_ID,
   CREATOR,
   CREATED_ON,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   EVENT_LIBRARY_TYPE,
   EVENT_LINE_CATEGORY,
   SITE_OF_SERVICE_NAME,
   FACILITY_NAME,
   COVERAGE_TYPE,
   COVERAGE_NOTES
)
AS
   SELECT   (SELECT   NAME
               FROM   ESCH.EVENT_DEF B
              WHERE   A.CHAIN_ID = B.EVENT_ID AND EVENT_TYPE = 'L')
               LBEVE_CATEGORY,
            NAME LBEVE_EVENT,
            DESCRIPTION LBEVE_DESCRIPTION,
            EVENT_CPTCODE LBEVE_CPTCODE,
            DURATION || ' ' || ERES.F_GET_DURUNIT (DURATION_UNIT)
               LBEVE_DURATION,
            FUZZY_PERIOD || ' ' || ERES.F_GET_DURUNIT (EVENT_DURATIONBEFORE)
               LBEVE_VISIT_WIN_B4,
               EVENT_FUZZYAFTER
            || ' '
            || ERES.F_GET_DURUNIT (EVENT_DURATIONAFTER)
               LBEVE_VISIT_WIN_AFT,
            NOTES LBEVE_NOTES,
            PAT_MSGBEFORE LBEVE_PTMESS_B4,
            PAT_MSGAFTER LBEVE_PTMESS_AFT,
            PAT_DAYSBEFORE LBEVE_PTDAYS_B4,
            PAT_DAYSAFTER LBEVE_PTDAYS_AFT,
            USR_MSGBEFORE LBEVE_USRMESS_B4,
            USR_MSGAFTER LBEVE_USRMESS_AFT,
            USR_DAYSBEFORE LBEVE_USRDAYS_B4,
            USR_DAYSAFTER LBEVE_USRDAYS_AFT,
            (SELECT   COUNT ( * )
               FROM   esch.SCH_EVENTCOST
              WHERE   FK_EVENT = EVENT_ID)
               LBEVE_COST_CNT,
            (SELECT   COUNT ( * )
               FROM   esch.SCH_EVENTDOC
              WHERE   FK_EVENT = EVENT_ID)
               LBEVE_APPNX_CNT,
            (SELECT   COUNT ( * )
               FROM   esch.SCH_EVENTUSR
              WHERE   FK_EVENT = EVENT_ID AND EVENTUSR_TYPE = 'R')
               LBEVE_RESOURCE_CNT,
            USER_ID FK_ACCOUNT,
            EVENT_ID,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = A.CREATOR)
               CREATOR,
            CREATED_ON,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = A.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            LAST_MODIFIED_DATE,
            (SELECT   CODELST_DESC
               FROM   esch.SCH_CODELST
              WHERE   PK_CODELST = EVENT_LIBRARY_TYPE)
               EVENT_LIBRARY_TYPE,
            (SELECT   (SELECT   CODELST_DESC
                         FROM   esch.SCH_CODELST
                        WHERE   PK_CODELST = b.EVENT_LINE_CATEGORY)
               FROM   ESCH.EVENT_DEF B
              WHERE   A.CHAIN_ID = B.EVENT_ID AND EVENT_TYPE = 'L')
               EVENT_LINE_CATEGORY,
            (SELECT   site_name
               FROM   eres.er_site
              WHERE   pk_site = SERVICE_SITE_ID)
               SITE_OF_SERVICE_NAME,
            (SELECT   site_name
               FROM   eres.er_site
              WHERE   pk_site = FACILITY_ID)
               FACILITY_NAME,
            (SELECT   CODELST_DESC
               FROM   esch.SCH_CODELST
              WHERE   PK_CODELST = FK_CODELST_COVERTYPE)
               COVERAGE_TYPE
     FROM   ESCH.EVENT_DEF A
    WHERE   EVENT_TYPE = 'E'
/
COMMENT ON TABLE VDA_V_LIB_EVENTS IS 'This view provides access to the Event Library data'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.LBEVE_CATEGORY IS 'The Event category in the Event Library'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.LBEVE_EVENT IS 'The event Name'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.LBEVE_DESCRIPTION IS 'The Event Description'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.LBEVE_CPTCODE IS 'The CPT code linked with the event'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.LBEVE_DURATION IS 'The Event Duration'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.LBEVE_VISIT_WIN_B4 IS 'Visit Window duration specified for before the event date'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.LBEVE_VISIT_WIN_AFT IS 'Visit Window duration specified for after the event date'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.LBEVE_NOTES IS 'Event Notes'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.LBEVE_PTMESS_B4 IS 'Event Message for the patient-before the event'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.LBEVE_PTMESS_AFT IS 'Event Message for the patient-after the event'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.LBEVE_PTDAYS_B4 IS 'Number of Days before the Event the patient Message to be sent out'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.LBEVE_PTDAYS_AFT IS 'Number of Days after the Event the patient Message to be sent out'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.LBEVE_USRMESS_B4 IS 'Event Message for the user-before the event'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.LBEVE_USRMESS_AFT IS 'Event Message for the user-after the event'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.LBEVE_USRDAYS_B4 IS 'Number of Days before the Event the user Message to be sent out'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.LBEVE_USRDAYS_AFT IS 'Number of Days after the Event the user Message to be sent out'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.LBEVE_COST_CNT IS 'Count of cost records linked with the event'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.LBEVE_APPNX_CNT IS 'Count of appendix records linked with the event'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.LBEVE_RESOURCE_CNT IS 'Count of resource records linked with the event'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.FK_ACCOUNT IS 'the account linked with event library'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.EVENT_ID IS 'the Primary Key of the event record'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.CREATOR IS 'The user who created the event  record (Audit)'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.CREATED_ON IS 'The date the event record was created on (Audit)'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.LAST_MODIFIED_BY IS 'The user who last modified the event  record(Audit)'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.LAST_MODIFIED_DATE IS 'The date the event record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.EVENT_LIBRARY_TYPE IS 'The event library type the event is linked with'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.EVENT_LINE_CATEGORY IS 'The default budget category for the event'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.SITE_OF_SERVICE_NAME IS 'The Site of Service (Organization) linked with the event'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.FACILITY_NAME IS 'The Facility linked with the event'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.COVERAGE_TYPE IS 'The default coverage type linked with the event'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.COVERAGE_NOTES IS 'The coverage type notes linked with the event'
/


