/* Formatted on 3/11/2014 3:19:33 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_PAT_TXARM  (View)
--
--  Dependencies:
--   ER_PATPROT (Table)
--   ER_PATTXARM (Table)
--   ER_PER (Table)
--   ER_STUDY (Table)
--   ER_STUDYTXARM (Table)
--   ER_USER (Table)
--

CREATE OR REPLACE FORCE VIEW VDA_V_PAT_TXARM
(
   PATIENT_ID,
   PATIENT_STUDY_ID,
   TREATMENT_NAME,
   DRUG_INFO,
   TREATMENT_STATUS_DATE,
   TREATMENT_END_DATE,
   TREATMENT_NOTES,
   CREATOR,
   CREATED_ON,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   FK_ACCOUNT,
   RID,
   PK_PATTXARM,
   FK_STUDY,
   FK_PER,
   STUDY_NUMBER,
   STUDY_TITLE
)
AS
   SELECT   PER_CODE,
            PATPROT_PATSTDID,
            (SELECT   TX_NAME
               FROM   eres.ER_STUDYTXARM
              WHERE   PK_STUDYTXARM = FK_STUDYTXARM)
               PTARM_TREATMT_NAME,
            TX_DRUG_INFO,
            TX_START_DATE,
            TX_END_DATE,
            NOTES PTARM_NOTES,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = pt.CREATOR)
               CREATOR,
            pt.CREATED_ON,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = pt.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            pt.LAST_MODIFIED_DATE,
            p.FK_ACCOUNT,
            pt.RID,
            PK_PATTXARM,
            fk_study,
            fk_per,
            study_number,
            study_title
     FROM   eres.ER_PATTXARM pt,
            eres.ER_PATPROT pp,
            eres.ER_PER p,
            eres.er_study s
    WHERE       PK_PATPROT = FK_PATPROT
            AND pk_per = fk_per
            AND s.pk_study = fk_study
/
COMMENT ON TABLE VDA_V_PAT_TXARM IS 'This view provides access to patient treatment arm information'
/

COMMENT ON COLUMN VDA_V_PAT_TXARM.PK_PATTXARM IS 'The PK of treatment arm record'
/

COMMENT ON COLUMN VDA_V_PAT_TXARM.FK_STUDY IS 'The FK to the study record'
/

COMMENT ON COLUMN VDA_V_PAT_TXARM.FK_PER IS 'The FK to the patient record'
/

COMMENT ON COLUMN VDA_V_PAT_TXARM.STUDY_NUMBER IS 'The Study Number'
/

COMMENT ON COLUMN VDA_V_PAT_TXARM.STUDY_TITLE IS 'The Study Title'
/

COMMENT ON COLUMN VDA_V_PAT_TXARM.TREATMENT_END_DATE IS 'The Treatment arm end date'
/

COMMENT ON COLUMN VDA_V_PAT_TXARM.TREATMENT_NOTES IS 'The Notes associated with the patient treatment arm'
/

COMMENT ON COLUMN VDA_V_PAT_TXARM.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_TXARM.CREATED_ON IS 'The date the record was created on (Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_TXARM.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_TXARM.LAST_MODIFIED_DATE IS 'The date the record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_TXARM.FK_ACCOUNT IS 'The account the patient is linked with'
/

COMMENT ON COLUMN VDA_V_PAT_TXARM.RID IS 'The unique Row ID for Audit'
/

COMMENT ON COLUMN VDA_V_PAT_TXARM.PATIENT_ID IS 'The Patient ID or Patient Code'
/

COMMENT ON COLUMN VDA_V_PAT_TXARM.PATIENT_STUDY_ID IS 'The Patient Study ID'
/

COMMENT ON COLUMN VDA_V_PAT_TXARM.TREATMENT_NAME IS 'The Treatment name'
/

COMMENT ON COLUMN VDA_V_PAT_TXARM.DRUG_INFO IS 'Drug information linked with the treatment arm'
/

COMMENT ON COLUMN VDA_V_PAT_TXARM.TREATMENT_STATUS_DATE IS 'The status date'
/


