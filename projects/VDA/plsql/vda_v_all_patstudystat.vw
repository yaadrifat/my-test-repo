/* Formatted on 3/11/2014 3:18:32 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_ALL_PATSTUDYSTAT  (View)
--
--  Dependencies:
--   ER_PATPROT (Table)
--   ER_PATSTUDYSTAT (Table)
--   F_GET_CODELSTDESC (Function)
--   ER_STUDY (Table)
--   ER_USER (Table)
--   ER_SITE (Table)
--

CREATE OR REPLACE FORCE VIEW VDA_V_ALL_PATSTUDYSTAT
(
   PSTAT_STATUS,
   PSTAT_REASON,
   PSTAT_STAT_DATE,
   PSTAT_NOTES,
   PSTAT_PAT_STUD_ID,
   PSTAT_ASSIGNED_TO,
   PSTAT_PHYSICIAN,
   PSTAT_TREAT_LOCAT,
   PSTAT_EVAL_FLAG,
   PSTAT_EVAL_STAT,
   PSTAT_INEVAL_STAT,
   PSTAT_SURVIVAL_STAT,
   PSTAT_RANDOM_NUM,
   PSTAT_ENROLLED_BY,
   PSTAT_ENROLL_SITE,
   PSTAT_NEXT_FU_DATE,
   PSTAT_IC_VERS_NUM,
   PSTAT_SCREEN_NUM,
   PSTAT_SCREEN_BY,
   PSTAT_SCREEN_OUTCOME,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATED_ON,
   FK_PER,
   FK_STUDY,
   PSTAT_CURRENT_STAT,
   PATSTUDYSTAT_ENDT,
   STUDY_NUMBER,
   FK_ACCOUNT,
   PK_PATSTUDYSTAT
)
AS
   SELECT   eres.F_GET_CODELSTDESC (FK_CODELST_STAT) PSTAT_STATUS,
            eres.F_GET_CODELSTDESC (PATSTUDYSTAT_REASON) PSTAT_REASON,
            PATSTUDYSTAT_DATE PSTAT_STAT_DATE,
            PATSTUDYSTAT_NOTE PSTAT_NOTES,
            PATPROT_PATSTDID PSTAT_PAT_STUD_ID,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = FK_USERASSTO)
               PSTAT_ASSIGNED_TO,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = PATPROT_PHYSICIAN)
               PSTAT_PHYSICIAN,
            ERES.F_GET_CODELSTDESC (FK_CODELSTLOC) PSTAT_TREAT_LOCAT,
            ERES.F_GET_CODELSTDESC (FK_CODELST_PTST_EVAL_FLAG)
               PSTAT_EVAL_FLAG,
            ERES.F_GET_CODELSTDESC (FK_CODELST_PTST_EVAL) PSTAT_EVAL_STAT,
            ERES.F_GET_CODELSTDESC (FK_CODELST_PTST_INEVAL) PSTAT_INEVAL_STAT,
            ERES.F_GET_CODELSTDESC (FK_CODELST_PTST_SURVIVAL) PSTAT_SURV_STAT,
            PATPROT_RANDOM PSTAT_RANDOM_NUM,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = ER_PATPROT.FK_USER)
               PSTAT_ENROLLED_BY,
            (SELECT   SITE_NAME
               FROM   eres.ER_SITE
              WHERE   PK_SITE = FK_SITE_ENROLLING)
               PSTAT_ENROLL_SITE,
            ER_PATSTUDYSTAT.NEXT_FOLLOWUP_ON PSTAT_NEXT_FU_DATE,
            ER_PATSTUDYSTAT.INFORM_CONSENT_VER PSTAT_IC_VERS_NUM,
            SCREEN_NUMBER PSTAT_SCREEN_NUM,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = SCREENED_BY)
               PSTAT_SCREEN_BY,
            ERES.F_GET_CODELSTDESC (SCREENING_OUTCOME) PSTAT_SCREEN_OUTCOME,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = ER_PATSTUDYSTAT.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = ER_PATSTUDYSTAT.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            ER_PATSTUDYSTAT.LAST_MODIFIED_DATE,
            ER_PATSTUDYSTAT.CREATED_ON,
            ER_PATSTUDYSTAT.fk_per,
            ER_PATSTUDYSTAT.fk_study,
            ER_PATSTUDYSTAT.CURRENT_STAT,
            ER_PATSTUDYSTAT.PATSTUDYSTAT_ENDT,
            STUDY_NUMBER,
            FK_ACCOUNT,
            PK_PATSTUDYSTAT
     FROM   eres.ER_PATSTUDYSTAT, eres.ER_PATPROT, eres.ER_STUDY
    WHERE       ER_PATSTUDYSTAT.FK_PER = ER_PATPROT.FK_PER
            AND ER_PATSTUDYSTAT.FK_STUDY = ER_PATPROT.FK_STUDY
            AND ER_STUDY.PK_STUDY = ER_PATPROT.FK_STUDY
            AND patprot_stat = 1
/
COMMENT ON TABLE VDA_V_ALL_PATSTUDYSTAT IS 'This view provides access to all patient study statuses records'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.STUDY_NUMBER IS 'The Study Number the Patient Study Status is linked with'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.FK_ACCOUNT IS 'The Account the Patient Study status is linked with'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PK_PATSTUDYSTAT IS 'The Primary Key of the patient study status record'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_STATUS IS 'The Patient Study Status description'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_REASON IS 'The Reason the previous Patient Study Status is changed to the new status'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_STAT_DATE IS 'The date this Patient Study Status started on'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_NOTES IS 'The notes linked with the Patient Study Status'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_PAT_STUD_ID IS 'The Patient Study ID (Identifier) assigned to the patient on the study'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_ASSIGNED_TO IS 'The user the patient is assigned to'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_PHYSICIAN IS 'The Physician the patient is assigned to'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_TREAT_LOCAT IS 'The location of the treatment'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_EVAL_FLAG IS 'The evaluation flag linked with the Patient Study Status'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_EVAL_STAT IS 'The evaluation Status linked with the Patient Study Status'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_INEVAL_STAT IS 'The in-evaluation flag linked with the Patient Study Status'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_SURVIVAL_STAT IS 'Patient Survival Status information'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_RANDOM_NUM IS 'Patient randomization number'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_ENROLLED_BY IS 'The user who enrolled the patient'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_ENROLL_SITE IS 'The Site or Organization Patient is Enrolled for'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_NEXT_FU_DATE IS 'The Next follow up date for the Patient'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_IC_VERS_NUM IS 'Patient Informed Consent Version Number'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_SCREEN_NUM IS 'Patient Screening Number'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_SCREEN_BY IS 'The user who screened the patient'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_SCREEN_OUTCOME IS 'The screening outcome'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.CREATOR IS 'The user who created the patient study status record (Audit)'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.LAST_MODIFIED_BY IS 'The user who last modified the patient study status record (Audit)'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.LAST_MODIFIED_DATE IS 'The date the patient study status record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.CREATED_ON IS 'The date the patient study status record was created on (Audit)'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.FK_PER IS 'The Foreign Key to the patient the patient study status record is linked with'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.FK_STUDY IS 'The Foreign Key to the study the patient study status record is linked with'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_CURRENT_STAT IS 'The flag to indicate if this is the current status of the patient on the study'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PATSTUDYSTAT_ENDT IS 'The date this status ended on - auto calculated as a new status with a higher date is added'
/


