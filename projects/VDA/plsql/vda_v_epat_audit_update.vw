/* Formatted on 3/11/2014 3:18:42 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_EPAT_AUDIT_UPDATE  (View)
--
--  Dependencies:
--   AUDIT_COLUMN (Table)
--   AUDIT_ROW (Table)
--

CREATE OR REPLACE FORCE VIEW VDA_V_EPAT_AUDIT_UPDATE
(
   USER_NAME,
   TIMESTAMP,
   TABLE_NAME,
   COLUMN_NAME,
   OLD_VALUE,
   NEW_VALUE,
   REMARKS,
   CAID,
   RID
)
AS
   SELECT   r.user_name,
            r.timestamp,
            r.table_name,
            c.column_name,
            c.old_value,
            c.new_value,
            c.remarks,
            caid,
            r.rid
     FROM   epat.audit_column c, epat.audit_row r
    WHERE   r.action = 'U' AND c.raid = r.raid
/
COMMENT ON TABLE VDA_V_EPAT_AUDIT_UPDATE IS 'This view provides access to the detail audit trail on Update actions in EPAT schema'
/

COMMENT ON COLUMN VDA_V_EPAT_AUDIT_UPDATE.USER_NAME IS 'The user who triggered the action'
/

COMMENT ON COLUMN VDA_V_EPAT_AUDIT_UPDATE.TIMESTAMP IS 'The date and time of the action'
/

COMMENT ON COLUMN VDA_V_EPAT_AUDIT_UPDATE.TABLE_NAME IS 'The name of the table'
/

COMMENT ON COLUMN VDA_V_EPAT_AUDIT_UPDATE.COLUMN_NAME IS 'The name of the column updated by the action'
/

COMMENT ON COLUMN VDA_V_EPAT_AUDIT_UPDATE.OLD_VALUE IS 'The old value of the column'
/

COMMENT ON COLUMN VDA_V_EPAT_AUDIT_UPDATE.NEW_VALUE IS 'The new value of the column'
/

COMMENT ON COLUMN VDA_V_EPAT_AUDIT_UPDATE.REMARKS IS 'The reason for change comments linked with the action'
/

COMMENT ON COLUMN VDA_V_EPAT_AUDIT_UPDATE.CAID IS 'The Primary Key of the audit column change'
/

COMMENT ON COLUMN VDA_V_EPAT_AUDIT_UPDATE.RID IS 'The Primary Key of the audit transaction'
/


