/* Formatted on 3/11/2014 3:19:03 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_GROUP_USERS  (View)
--
--  Dependencies:
--   ER_GRPS (Table)
--   ER_USER (Table)
--   ER_USRGRP (Table)
--

CREATE OR REPLACE FORCE VIEW VDA_V_GROUP_USERS
(
   PK_USRGRP,
   FK_USER,
   FK_GRP,
   RID,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATED_ON,
   IP_ADD,
   USR_LASTNAME,
   USR_FIRSTNAME,
   GROUP_NAME,
   FK_ACCOUNT
)
AS
   SELECT   PK_USRGRP,
            FK_USER,
            FK_GRP,
            g.RID,
            g.CREATOR,
            g.LAST_MODIFIED_BY,
            g.LAST_MODIFIED_DATE,
            g.CREATED_ON,
            g.IP_ADD,
            usr_lastname,
            usr_firstname,
            (SELECT   grp_name
               FROM   eres.er_grps
              WHERE   pk_grp = FK_GRP)
               group_name,
            u.fk_account
     FROM   eres.ER_USRGRP g, eres.er_user u
    WHERE   pk_user = FK_USER
/
COMMENT ON TABLE VDA_V_GROUP_USERS IS 'This provides access the group-user relationship: all users in a group'
/

COMMENT ON COLUMN VDA_V_GROUP_USERS.PK_USRGRP IS 'The Primary key'
/

COMMENT ON COLUMN VDA_V_GROUP_USERS.FK_USER IS 'The PK of er_user'
/

COMMENT ON COLUMN VDA_V_GROUP_USERS.FK_GRP IS 'The PK of er_grps'
/

COMMENT ON COLUMN VDA_V_GROUP_USERS.RID IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database.'
/

COMMENT ON COLUMN VDA_V_GROUP_USERS.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_GROUP_USERS.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_GROUP_USERS.LAST_MODIFIED_DATE IS 'The date the  record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_GROUP_USERS.CREATED_ON IS 'The date the record was created on (Audit)'
/

COMMENT ON COLUMN VDA_V_GROUP_USERS.IP_ADD IS 'The IP ADDRESS of the client machine (Audit)'
/

COMMENT ON COLUMN VDA_V_GROUP_USERS.USR_LASTNAME IS 'The last name of the user'
/

COMMENT ON COLUMN VDA_V_GROUP_USERS.USR_FIRSTNAME IS 'The first name of the user'
/

COMMENT ON COLUMN VDA_V_GROUP_USERS.GROUP_NAME IS 'The name of the group'
/

COMMENT ON COLUMN VDA_V_GROUP_USERS.FK_ACCOUNT IS 'The account the study site is linked with'
/


