/* Formatted on 3/11/2014 3:19:21 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_MORE_PATIENT_DETAILS  (View)
--
--  Dependencies:
--   ER_PER (Table)
--   ER_CODELST (Table)
--   PAT_PERID (Table)
--   ER_USER (Table)
--   ER_SITE (Table)
--

CREATE OR REPLACE FORCE VIEW VDA_V_MORE_PATIENT_DETAILS
(
   FIELD_NAME,
   FIELD_VALUE,
   FK_PER,
   CREATED_ON,
   FK_ACCOUNT,
   PATIENT_ID,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   RID,
   PK_PERID,
   SITE_NAME
)
AS
   SELECT   codelst_desc AS field_name,
            perid_id AS field_value,
            fk_per,
            a.CREATED_ON,
            p.fk_account,
            PER_CODE PATIENT_ID,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   eres.er_user
              WHERE   pk_user = A.CREATOR)
               creator,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   eres.er_user
              WHERE   pk_user = A.LAST_MODIFIED_BY)
               last_modified_by,
            a.LAST_MODIFIED_DATE,
            A.RID,
            PK_PERID,
            (SELECT   site_name
               FROM   eres.ER_SITE
              WHERE   pk_site = p.fk_site)
               site_name
     FROM   epat.pat_perid a, eres.ER_PER p, eres.er_codelst c
    WHERE   pk_per = fk_per AND c.pk_codelst = fk_codelst_idtype
/
COMMENT ON TABLE VDA_V_MORE_PATIENT_DETAILS IS 'This view provides access to the more patient codes or details linked with a patient'
/

COMMENT ON COLUMN VDA_V_MORE_PATIENT_DETAILS.SITE_NAME IS 'The name of the default organization the patient is linked'
/

COMMENT ON COLUMN VDA_V_MORE_PATIENT_DETAILS.CREATED_ON IS 'The date the record was created on (Audit)'
/

COMMENT ON COLUMN VDA_V_MORE_PATIENT_DETAILS.FK_ACCOUNT IS 'The account the patient is linked with'
/

COMMENT ON COLUMN VDA_V_MORE_PATIENT_DETAILS.PATIENT_ID IS 'The Patient ID or Patient Code'
/

COMMENT ON COLUMN VDA_V_MORE_PATIENT_DETAILS.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_MORE_PATIENT_DETAILS.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_MORE_PATIENT_DETAILS.LAST_MODIFIED_DATE IS 'The date the record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_MORE_PATIENT_DETAILS.RID IS 'The Primary Key of the audit transaction'
/

COMMENT ON COLUMN VDA_V_MORE_PATIENT_DETAILS.PK_PERID IS 'The Primary Key of the patient record'
/

COMMENT ON COLUMN VDA_V_MORE_PATIENT_DETAILS.FIELD_NAME IS 'The Name of the Field'
/

COMMENT ON COLUMN VDA_V_MORE_PATIENT_DETAILS.FIELD_VALUE IS 'The value of the Field'
/

COMMENT ON COLUMN VDA_V_MORE_PATIENT_DETAILS.FK_PER IS 'The FK to the patient record'
/


