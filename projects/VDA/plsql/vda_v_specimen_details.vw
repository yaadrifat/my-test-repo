/* Formatted on 4/1/2014 3:23:13 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_SPECIMEN_DETAILS  (View)
--
--  Dependencies:
--   ER_USER (Table)
--   F_GET_CODELSTDESC (Function)
--   ER_CODELST (Table)
--   ER_SITE (Table)
--   ER_SPECIMEN (Table)
--   ER_STORAGE (Table)
--   ER_STUDY (Table)
--   SCH_PROTOCOL_VISIT (Table)
--   SCH_EVENTS1 (Table)
--   PERSON (Table)
--

CREATE OR REPLACE FORCE VIEW VDA_V_SPECIMEN_DETAILS
(
   PK_SPECIMEN,
   SPEC_ID,
   SPEC_ALTERNATE_ID,
   SPEC_DESCRIPTION,
   EXP_SPEC_QTY,
   EXP_QTY_UNITS,
   COLL_SPEC_QTY,
   COLL_QTY_UNITS,
   CURRENT_SPEC_QTY,
   CURRENT_SPEC_QTY_UNITS,
   SPEC_TYPE,
   SPEC_COLLECTION_DATE,
   OWNER,
   SPEC_STORAGE,
   SPEC_KIT_COMPONENT,
   FK_STUDY,
   SPEC_STUDY_NUMBER,
   FK_PER,
   SPEC_PATIENT_CODE,
   SPEC_PATIENT_NAME,
   SPEC_ORGANIZATION,
   VIST_NAME,
   EVENT_DESCRIPTION,
   SPEC_ANATOMIC_SITE,
   SPEC_TISSUE_SIDE,
   SPEC_PATHOLOGY_STAT,
   SPEC_STORAGE_KIT,
   SPEC_NOTES,
   PATHOLOGIST,
   SURGEON,
   COLLECTING_TECHNICIAN,
   PROCESSING_TECHNICIAN,
   SPEC_REMOVAL_TIME,
   SPEC_FREEZE_TIME,
   PARENT_SPEC_ID,
   CREATOR,
   CREATED_ON,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   FK_ACCOUNT,
   RID,
   IP_ADD,
   SPEC_ENVT_CONS,
   SPEC_DISPOSITION
)
AS
   SELECT   a.pk_specimen pk_specimen,
            SPEC_ID,
            SPEC_ALTERNATE_ID,
            SPEC_DESCRIPTION,
            SPEC_EXPECTED_QUANTITY exp_spec_qty,
            (SELECT   codelst_desc
               FROM   eres.ER_CODELST
              WHERE   pk_codelst = SPEC_EXPECTEDQ_UNITS)
               exp_qty_units,
            SPEC_BASE_ORIG_QUANTITY coll_spec_qty,
            (SELECT   codelst_desc
               FROM   eres.ER_CODELST
              WHERE   pk_codelst = SPEC_BASE_ORIGQ_UNITS)
               coll_qty_units,
            SPEC_ORIGINAL_QUANTITY current_spec_qty,
            (SELECT   codelst_desc
               FROM   eres.ER_CODELST
              WHERE   pk_codelst = SPEC_QUANTITY_UNITS)
               current_spec_qty_units,
            (SELECT   codelst_desc
               FROM   eres.ER_CODELST
              WHERE   pk_codelst = SPEC_TYPE)
               SPEC_TYPE,
            SPEC_COLLECTION_DATE,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = SPEC_OWNER_FK_USER)
               AS Owner,
            (SELECT   c.storage_name
               FROM   eres.ER_STORAGE c
              WHERE   c.pk_storage = a.fk_storage)
               spec_storage,
            (SELECT   d.storage_name
               FROM   eres.ER_STORAGE d
              WHERE   d.pk_storage = a.FK_STORAGE_KIT_COMPONENT)
               spec_kit_component,
            a.FK_STUDY fk_study,
            (SELECT   study_number
               FROM   eres.ER_STUDY
              WHERE   pk_study = a.FK_STUDY)
               spec_study_number,
            FK_PER,
            (SELECT   person_code
               FROM   epat.person
              WHERE   PK_person = fk_per)
               AS spec_patient_code,
            (SELECT   person_FNAME || ' ' || person_LNAME
               FROM   epat.person
              WHERE   PK_person = fk_per)
               AS spec_patient_name,
            (SELECT   site_name
               FROM   eres.ER_SITE
              WHERE   pk_site = FK_SITE)
               AS spec_organization,
            (SELECT   visit_name
               FROM   esch.sch_protocol_visit
              WHERE   pk_protocol_visit = fk_visit)
               VIST_NAME,
            (SELECT   description
               FROM   esch.sch_events1
              WHERE   event_id = FK_SCH_EVENTS1)
               Event_Description,
            (SELECT   codelst_desc
               FROM   eres.ER_CODELST
              WHERE   pk_codelst = SPEC_ANATOMIC_SITE)
               SPEC_ANATOMIC_SITE,
            (SELECT   codelst_desc
               FROM   eres.ER_CODELST
              WHERE   pk_codelst = SPEC_TISSUE_SIDE)
               SPEC_TISSUE_SIDE,
            (SELECT   codelst_desc
               FROM   eres.ER_CODELST
              WHERE   pk_codelst = SPEC_PATHOLOGY_STAT)
               SPEC_PATHOLOGY_STAT,
            (SELECT   STORAGE_NAME
               FROM   eres.ER_STORAGE
              WHERE   PK_STORAGE = FK_STORAGE_KIT)
               SPEC_STORAGE_KIT,
            SPEC_NOTES,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = FK_USER_PATH)
               AS Pathologist,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = FK_USER_SURG)
               AS Surgeon,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = FK_USER_COLLTECH)
               AS Collecting_Technician,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = FK_USER_PROCTECH)
               AS Processing_Technician,
            SPEC_REMOVAL_TIME,
            SPEC_FREEZE_TIME,
            (SELECT   aa.SPEC_ID
               FROM   eres.ER_SPECIMEN aa
              WHERE   aa.pk_specimen = a.FK_SPECIMEN)
               parent_spec_id,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = a.CREATOR)
               creator,
            a.CREATED_ON,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = a.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            a.LAST_MODIFIED_DATE,
            FK_ACCOUNT,
            a.RID,
            a.IP_ADD,
            SPEC_ENVT_CONS,
            eres.F_GET_CODELSTDESC (SPEC_DISPOSITION) SPEC_DISPOSITION
     FROM   eres.ER_SPECIMEN a
/
COMMENT ON TABLE VDA_V_SPECIMEN_DETAILS IS 'This view provides access to specimen attributes.'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.PK_SPECIMEN IS 'The Primary Key of the specimen record'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.SPEC_ID IS 'The specimen id'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.SPEC_ALTERNATE_ID IS 'The specimen Alternate ID'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.SPEC_DESCRIPTION IS 'The specimen description'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.EXP_SPEC_QTY IS 'The expected specimen quantity'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.EXP_QTY_UNITS IS 'The expected specimen quantity units'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.COLL_SPEC_QTY IS 'The collected specimen quantity'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.COLL_QTY_UNITS IS 'The collected specimen quantity units'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.CURRENT_SPEC_QTY IS 'The current specimen quantity'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.CURRENT_SPEC_QTY_UNITS IS 'The current specimen quantity units'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.SPEC_TYPE IS 'The specimen type'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.SPEC_COLLECTION_DATE IS 'The specimen collection date'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.OWNER IS 'The specimen owner'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.SPEC_STORAGE IS 'The specimen storage'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.SPEC_KIT_COMPONENT IS 'The specimen kit component'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.FK_STUDY IS 'The FK of study linked with specimen (if linked with a study)'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.SPEC_STUDY_NUMBER IS 'The study number of the study the specimen is linked with'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.FK_PER IS 'The FK of patient the specimen is linked with (if linked with a patient)'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.SPEC_PATIENT_CODE IS 'The patient code of patient the specimen is linked with (if linked with a patient)'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.SPEC_PATIENT_NAME IS 'The patient name of patient the specimen is linked with (if linked with a patient)'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.SPEC_ORGANIZATION IS 'The organization or site the specimen is linked with'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.VIST_NAME IS 'The name of the patient visit the specimen was collected with'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.EVENT_DESCRIPTION IS 'The name of the patient visit event the specimen was collected with'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.SPEC_ANATOMIC_SITE IS 'The specimen anatomic site'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.SPEC_TISSUE_SIDE IS 'The specimen tissue side'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.SPEC_PATHOLOGY_STAT IS 'The pathological status '
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.SPEC_STORAGE_KIT IS 'The storage kit the specimen is linked with'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.SPEC_NOTES IS 'The specimen notes'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.PATHOLOGIST IS 'The pathologist linked with specimen record'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.SURGEON IS 'The surgeon linked with specimen record'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.COLLECTING_TECHNICIAN IS 'The collecting technician linked with specimen record'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.PROCESSING_TECHNICIAN IS 'The processing technician linked with specimen record'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.PARENT_SPEC_ID IS 'The parent specimen ID'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.CREATED_ON IS 'The date the record was created on (Audit)'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.LAST_MODIFIED_BY IS 'The user who last modified the record (Audit)'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.LAST_MODIFIED_DATE IS 'The date the record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.FK_ACCOUNT IS 'The account storage is linked with'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.RID IS 'The unique Row ID for Audit'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.IP_ADD IS 'The IP address of the client machine that initiated the session'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.SPEC_ENVT_CONS IS 'The environmental constraints'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.SPEC_DISPOSITION IS 'The specimen disposition'
/

