/* Formatted on 6/6/2014 2:45:47 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_INVOICE_PAYMENTS  (View)
--
--  Dependencies:
--   ER_INVOICE (Table)
--   ER_INVOICE_DETAIL (Table)
--   ER_MILEPAYMENT_DETAILS (Table)
--   ER_STUDY (Table)
--

CREATE OR REPLACE  VIEW VDA_V_INVOICE_PAYMENTS
(
   PK_INVOICE,
   STUDY_NUMBER,
   STUDY_SPONSOR,
   INV_NUMBER,
   INV_DATE,
   INVOICE_AMOUNT,
   INVOICE_AMOUNT_BALANCE,
   PAYMENTS
)
AS
     SELECT   PK_INVOICE,
              STUDY_NUMBER,
              decode(FK_CODELST_SPONSOR,null,STUDY_SPONSOR,eres.F_GET_CODELSTDESC (FK_CODELST_SPONSOR)) STUDY_SPONSOR, 
              INV_NUMBER,
              INV_DATE,
              (SUM (NVL (AMOUNT_INVOICED, 0))) AS INVOICE_AMOUNT,
              (SUM (NVL (AMOUNT_INVOICED, 0))
               - NVL (
                    (SELECT   SUM (MP_AMOUNT)
                       FROM   eres.ER_MILEPAYMENT_DETAILS
                      WHERE   MP_LINKTO_ID = PK_INVOICE
                              AND MP_LINKTO_TYPE = 'I'),
                    0
                 ))
                 AS INVOICE_AMOUNT_BALANCE,
              NVL (
                 (SELECT   SUM (MP_AMOUNT)
                    FROM   eres.ER_MILEPAYMENT_DETAILS
                   WHERE   MP_LINKTO_ID = PK_INVOICE AND MP_LINKTO_TYPE = 'I'),
                 0
              )
                 AS PAYMENTS
       FROM   eres.ER_STUDY STUDY,
              eres.ER_INVOICE INV,
              eres.ER_INVOICE_DETAIL INVDETAIL
      WHERE       STUDY.PK_STUDY = INV.FK_STUDY
              AND INV.PK_INVOICE = INVDETAIL.FK_INV
              AND INVDETAIL.DETAIL_TYPE = 'H'
   GROUP BY   PK_INVOICE,
              STUDY_NUMBER,
              decode(FK_CODELST_SPONSOR,null,STUDY_SPONSOR,eres.F_GET_CODELSTDESC (FK_CODELST_SPONSOR)),
              INV_NUMBER,
              INV_DATE
/
COMMENT ON TABLE VDA_V_INVOICE_PAYMENTS IS 'This view provides access to invoice and payment summary'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS.PAYMENTS IS 'The payment amount (if any) reconciled with the invoice'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS.PK_INVOICE IS 'Primary Key of Invoice'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS.STUDY_NUMBER IS 'The Study Number'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS.STUDY_SPONSOR IS 'The Study Sponsor (from summary), if study sponsor field is empty, it gets ''if other'''
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS.INV_NUMBER IS 'The Invoice Number'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS.INV_DATE IS 'The invoive Date'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS.INVOICE_AMOUNT IS 'The original invoice amount'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS.INVOICE_AMOUNT_BALANCE IS 'The amount balance for the invoice'
/

