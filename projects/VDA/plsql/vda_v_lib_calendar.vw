/* Formatted on 3/11/2014 3:19:07 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_LIB_CALENDAR  (View)
--
--  Dependencies:
--   F_GET_DURATION (Function)
--   F_GET_DURUNIT (Function)
--   F_GET_SHAREDWITH (Function)
--   ER_CATLIB (Table)
--   EVENT_DEF (Table)
--   SCH_CODELST (Table)
--   ER_USER (Table)
--

CREATE OR REPLACE FORCE VIEW VDA_V_LIB_CALENDAR
(
   LBCAL_TYPE,
   LBCAL_NAME,
   LBCAL_DESC,
   LBCAL_STAT,
   LBCAL_SHARED_WITH,
   LBCAL_DURATION,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATED_ON,
   CALENDAR_PK,
   FK_ACCOUNT
)
AS
   SELECT   (SELECT   CATLIB_NAME
               FROM   ERES.ER_CATLIB
              WHERE   PK_CATLIB = FK_CATLIB)
               LBCAL_TYPE,
            NAME LBCAL_NAME,
            DESCRIPTION LBCAL_DESC,
            --JM: 08Feb2011, D-FIN9
            --ERES.F_GET_FSTAT (STATUS) LBCAL_STAT,
            (SELECT   CODELST_DESC
               FROM   esch.sch_codelst
              WHERE   PK_CODELST = FK_CODELST_CALSTAT)
               LBCAL_STAT,
            ERES.F_GET_SHAREDWITH (CALENDAR_SHAREDWITH) LBCAL_SHARED_WITH,
               ERES.F_GET_DURATION (DURATION_UNIT, DURATION)
            || ' '
            || ERES.F_GET_DURUNIT (DURATION_UNIT)
               LBCAL_DURATION,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = E.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = E.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            LAST_MODIFIED_DATE,
            CREATED_ON,
            EVENT_ID CALENDAR_PK,
            USER_ID FK_ACCOUNT
     FROM   ESCH.EVENT_DEF E
    WHERE   EVENT_TYPE = 'P'
/
COMMENT ON TABLE VDA_V_LIB_CALENDAR IS 'This view provides access to Library Calendars'' summary information'
/

COMMENT ON COLUMN VDA_V_LIB_CALENDAR.LBCAL_TYPE IS 'Calendar Type'
/

COMMENT ON COLUMN VDA_V_LIB_CALENDAR.LBCAL_NAME IS 'Calendar Name'
/

COMMENT ON COLUMN VDA_V_LIB_CALENDAR.LBCAL_DESC IS 'Calendar Description'
/

COMMENT ON COLUMN VDA_V_LIB_CALENDAR.LBCAL_STAT IS 'Calendar Status'
/

COMMENT ON COLUMN VDA_V_LIB_CALENDAR.LBCAL_SHARED_WITH IS 'Shared With-access information'
/

COMMENT ON COLUMN VDA_V_LIB_CALENDAR.LBCAL_DURATION IS 'Calendar Duration'
/

COMMENT ON COLUMN VDA_V_LIB_CALENDAR.CREATOR IS 'The user who created the calendar record (Audit)'
/

COMMENT ON COLUMN VDA_V_LIB_CALENDAR.LAST_MODIFIED_BY IS 'The user who last modified the calendar record(Audit)'
/

COMMENT ON COLUMN VDA_V_LIB_CALENDAR.LAST_MODIFIED_DATE IS 'The date the calendar record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_LIB_CALENDAR.CREATED_ON IS 'The date the calendar record was created on (Audit)'
/

COMMENT ON COLUMN VDA_V_LIB_CALENDAR.CALENDAR_PK IS 'The Primary Key of the Calendar record'
/

COMMENT ON COLUMN VDA_V_LIB_CALENDAR.FK_ACCOUNT IS 'The account the calendar is linked with'
/


