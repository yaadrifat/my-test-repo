/* Formatted on 3/11/2014 3:19:12 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_LIBRARY_FORMS  (View)
--
--  Dependencies:
--   F_CODELST_DESC (Function)
--   F_GET_SHAREDWITH (Function)
--   ER_CATLIB (Table)
--   ER_FORMLIB (Table)
--   ER_USER (Table)
--

CREATE OR REPLACE FORCE VIEW VDA_V_LIBRARY_FORMS
(
   FORM_TYPE,
   FORM_NAME,
   FORM_DESC,
   FORM_STATUS,
   FORM_SHARED_WITH,
   FK_ACCOUNT,
   PK_FORMLIB,
   RID,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATED_ON,
   FORM_ESIGN_REQUIRED
)
AS
   SELECT   (SELECT   CATLIB_NAME
               FROM   eres.ER_CATLIB
              WHERE   PK_CATLIB = FK_CATLIB)
               FORM_TYPE,
            FORM_NAME FORM_NAME,
            FORM_DESC FORM_DESC,
            eres.F_CODELST_DESC (FORM_STATUS) FORM_STATUS,
            eres.F_GET_SHAREDWITH (FORM_SHAREDWITH) FORM_SHARED_WITH,
            FK_ACCOUNT,
            PK_FORMLIB,
            RID,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   eres.ER_USER
              WHERE   pk_user = ER_FORMLIB.CREATOR)
               creator,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   eres.ER_USER
              WHERE   pk_user = ER_FORMLIB.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            LAST_MODIFIED_DATE,
            CREATED_ON,
            DECODE (NVL (FORM_ESIGNREQ, 0),
                    0,
                    'No',
                    1,
                    'Yes')
               FORM_ESIGN_REQUIRED
     FROM   eres.ER_FORMLIB
    WHERE   form_linkto = 'L' AND NVL (record_type, 'N') <> 'D'
/
COMMENT ON TABLE VDA_V_LIBRARY_FORMS IS 'This view provides access to the library forms information'
/

COMMENT ON COLUMN VDA_V_LIBRARY_FORMS.FORM_TYPE IS 'The Form Type'
/

COMMENT ON COLUMN VDA_V_LIBRARY_FORMS.FORM_NAME IS 'The name of the form'
/

COMMENT ON COLUMN VDA_V_LIBRARY_FORMS.FORM_DESC IS 'The form description'
/

COMMENT ON COLUMN VDA_V_LIBRARY_FORMS.FORM_STATUS IS 'The Form Status'
/

COMMENT ON COLUMN VDA_V_LIBRARY_FORMS.FORM_SHARED_WITH IS 'The Form Sharing option'
/

COMMENT ON COLUMN VDA_V_LIBRARY_FORMS.FK_ACCOUNT IS 'The account the site is linked with'
/

COMMENT ON COLUMN VDA_V_LIBRARY_FORMS.PK_FORMLIB IS 'The PK of the form record'
/

COMMENT ON COLUMN VDA_V_LIBRARY_FORMS.RID IS 'The unique Row ID for Audit'
/

COMMENT ON COLUMN VDA_V_LIBRARY_FORMS.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_LIBRARY_FORMS.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_LIBRARY_FORMS.LAST_MODIFIED_DATE IS 'The date the record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_LIBRARY_FORMS.CREATED_ON IS 'The date the record was created on (Audit)'
/

COMMENT ON COLUMN VDA_V_LIBRARY_FORMS.FORM_ESIGN_REQUIRED IS 'Is eSign required for the form response'
/


