/* Formatted on 3/11/2014 3:19:25 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_PAT_ACCRUAL_YEAR  (View)
--
--  Dependencies:
--   F_GET_CODELSTDESC (Function)
--   VDA_V_PAT_ACCRUAL (View)
--

CREATE OR REPLACE FORCE VIEW VDA_V_PAT_ACCRUAL_YEAR
(
   FK_STUDY,
   FK_SITE_ENROLLING,
   PSTAT_ENROLL_SITE,
   PATPROT_ENROLL_YEAR,
   ACCRUAL_COUNT
)
AS
     SELECT   fk_study,
              FK_SITE_ENROLLING,
              PSTAT_ENROLL_SITE,
              EXTRACT (YEAR FROM patprot_enroldt) patprot_enroll_year,
              COUNT ( * ) accrual_count
       FROM   VDA.VDA_V_PAT_ACCRUAL
   GROUP BY   fk_study,
              FK_SITE_ENROLLING,
              PSTAT_ENROLL_SITE,
              EXTRACT (YEAR FROM patprot_enroldt)
/
COMMENT ON TABLE VDA_V_PAT_ACCRUAL_YEAR IS 'This view provides access to the yearly accrual or enrollment counts. Accrual is considered as the enrollment status and enrollment date entered'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_YEAR.FK_STUDY IS 'The Primary Key of the study the accrual is linked with'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_YEAR.FK_SITE_ENROLLING IS 'The Primary Key of the enrolling site'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_YEAR.PSTAT_ENROLL_SITE IS 'The Patient Enrolling Site'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_YEAR.PATPROT_ENROLL_YEAR IS 'The enrollment year'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_YEAR.ACCRUAL_COUNT IS 'The enrollment or accrual count'
/


