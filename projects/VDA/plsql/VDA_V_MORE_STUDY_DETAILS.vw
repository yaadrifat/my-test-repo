/* Formatted on 3/21/2014 3:42:58 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_MORE_STUDY_DETAILS  (View)
--
--  Dependencies:
--   ER_USER (Table)
--   ER_CODELST (Table)
--   ER_STUDY (Table)
--   ER_STUDYID (Table)
--

CREATE OR REPLACE FORCE VIEW VDA_V_MORE_STUDY_DETAILS
(
   FIELD_NAME,
   FIELD_VALUE,
   FK_STUDY,
   CREATED_ON,
   FK_ACCOUNT,
   STUDY_NUMBER,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   RID,
   PK_STUDYID
)
AS
   SELECT   codelst_desc AS field_name,
            studyid_id AS field_value,
            fk_STUDY,
            a.CREATED_ON,
            p.fk_account,
            STUDY_NUMBER AS STUDY_NUMBER,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   eres.er_user
              WHERE   pk_user = A.CREATOR)
               creator,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   eres.er_user
              WHERE   pk_user = A.LAST_MODIFIED_BY)
               last_modified_by,
            a.LAST_MODIFIED_DATE,
            A.RID,
            PK_STUDYID
     FROM   eres.er_studyid a, eres.er_study p, eres.er_codelst c
    WHERE   pk_study = FK_STUDY AND c.pk_codelst = fk_codelst_idtype
/
COMMENT ON TABLE VDA_V_MORE_STUDY_DETAILS IS 'This view provides access to the more study details or IDs linked with a study'
/

COMMENT ON COLUMN VDA_V_MORE_STUDY_DETAILS.FIELD_NAME IS 'The Name of the Field'
/

COMMENT ON COLUMN VDA_V_MORE_STUDY_DETAILS.FIELD_VALUE IS 'The value of the Field'
/

COMMENT ON COLUMN VDA_V_MORE_STUDY_DETAILS.FK_STUDY IS 'The FK to the Study record'
/

COMMENT ON COLUMN VDA_V_MORE_STUDY_DETAILS.CREATED_ON IS 'The date the record was created on (Audit)'
/

COMMENT ON COLUMN VDA_V_MORE_STUDY_DETAILS.FK_ACCOUNT IS 'The account the study is linked with'
/

COMMENT ON COLUMN VDA_V_MORE_STUDY_DETAILS.STUDY_NUMBER IS 'The Study Number'
/

COMMENT ON COLUMN VDA_V_MORE_STUDY_DETAILS.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_MORE_STUDY_DETAILS.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_MORE_STUDY_DETAILS.LAST_MODIFIED_DATE IS 'The date the record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_MORE_STUDY_DETAILS.RID IS 'The Primary Key of the audit transaction'
/

COMMENT ON COLUMN VDA_V_MORE_STUDY_DETAILS.PK_STUDYID IS 'The Primary Key of the more study details record'
/

