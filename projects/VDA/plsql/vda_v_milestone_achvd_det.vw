/* Formatted on 6/9/2014 10:21:36 AM (QP5 v5.115.810.9015) */
--
-- VDA_V_MILESTONE_ACHVD_DET  (View)
--
--  Dependencies:
--   ER_PATPROT (Table)
--   ER_PER (Table)
--   PKG_MILESTONE_NEW (Package)
--   ER_MILEACHIEVED (Table)
--   ER_STUDY (Table)
--   ER_USER (Table)
--

CREATE OR REPLACE FORCE VIEW VDA_V_MILESTONE_ACHVD_DET
(
   PK_MILEACHIEVED,
   MSACH_MILESTONE_DESC,
   MSACH_PATIENT_ID,
   MSACH_PTSTUDY_ID,
   MSACH_ACH_DATE,
   CREATOR,
   CREATED_ON,
   LAST_MODIFIED_DATE,
   LAST_MODIFIED_BY,
   FK_STUDY,
   FK_ACCOUNT,
   STUDY_NUMBER, fk_milestone
)
AS
   SELECT   PK_MILEACHIEVED,
            ERES.PKG_Milestone_New.f_getMilestoneDesc (FK_MILESTONE)
               MSACH_MILESTONE_DESC,
            (SELECT   PER_CODE
               FROM   eres.ER_PER
              WHERE   PK_PER = FK_PER)
               MSACH_PATIENT_ID,
            (SELECT   PATPROT_PATSTDID
               FROM   eres.ER_PATPROT pp
              WHERE       pp.fk_per = a.fk_per
                      AND pp.fk_study = a.fk_study
                      AND pp.patprot_stat = 1
                      AND ROWNUM < 2)
               MSACH_PTSTUDY_ID,
            ACH_DATE MSACH_ACH_DATE,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   eres.ER_USER
              WHERE   pk_user = a.CREATOR)
               creator,
            a.CREATED_ON,
            a.LAST_MODIFIED_DATE,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   eres.ER_USER
              WHERE   pk_user = a.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            fk_study,
            b.fk_account,
            b.STUDY_NUMBER, fk_milestone
     FROM   eres.ER_MILEACHIEVED a, eres.ER_STUDY b
    WHERE   IS_COMPLETE = 1 AND b.pk_study = a.fk_study
/
COMMENT ON TABLE VDA_V_MILESTONE_ACHVD_DET IS 'This view provides access to the milestone achievement data'
/

COMMENT ON COLUMN VDA_V_MILESTONE_ACHVD_DET.PK_MILEACHIEVED IS 'The Primary of the milestones achievement records'
/

COMMENT ON COLUMN VDA_V_MILESTONE_ACHVD_DET.MSACH_MILESTONE_DESC IS 'Milestone description'
/

COMMENT ON COLUMN VDA_V_MILESTONE_ACHVD_DET.MSACH_PATIENT_ID IS 'Patient ID for Patient related milestones'
/

COMMENT ON COLUMN VDA_V_MILESTONE_ACHVD_DET.MSACH_PTSTUDY_ID IS 'Patient Study ID for Patient related milestones'
/

COMMENT ON COLUMN VDA_V_MILESTONE_ACHVD_DET.MSACH_ACH_DATE IS 'milestone achievement date'
/

COMMENT ON COLUMN VDA_V_MILESTONE_ACHVD_DET.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_MILESTONE_ACHVD_DET.CREATED_ON IS 'The date the record was created on (Audit)'
/

COMMENT ON COLUMN VDA_V_MILESTONE_ACHVD_DET.LAST_MODIFIED_DATE IS 'The date the record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_MILESTONE_ACHVD_DET.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_MILESTONE_ACHVD_DET.FK_STUDY IS 'The Foreign Key to the study milestone is linked with'
/

COMMENT ON COLUMN VDA_V_MILESTONE_ACHVD_DET.FK_ACCOUNT IS 'The account the milestone is linked with'
/

COMMENT ON COLUMN VDA_V_MILESTONE_ACHVD_DET.STUDY_NUMBER IS 'The Study Number of the study the milestone is linked with'
/

COMMENT ON COLUMN VDA_V_MILESTONE_ACHVD_DET.fk_milestone IS 'The PK_milestone to uniquely identify a milestone'
/
