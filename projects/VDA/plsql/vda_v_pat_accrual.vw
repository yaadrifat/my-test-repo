/* Formatted on 3/11/2014 3:19:23 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_PAT_ACCRUAL  (View)
--
--  Dependencies:
--   ER_PATPROT (Table)
--   F_GET_CODELSTDESC (Function)
--   ER_STUDY (Table)
--   ER_USER (Table)
--   ER_SITE (Table)
--

CREATE OR REPLACE FORCE VIEW VDA_V_PAT_ACCRUAL
(
   PSTAT_PAT_STUD_ID,
   PSTAT_ASSIGNED_TO,
   PSTAT_PHYSICIAN,
   PSTAT_TREAT_LOCAT,
   PSTAT_EVAL_FLAG,
   PSTAT_EVAL_STAT,
   PSTAT_INEVAL_STAT,
   PSTAT_SURVIVAL_STAT,
   PSTAT_RANDOM_NUM,
   PSTAT_ENROLLED_BY,
   PSTAT_ENROLL_SITE,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATED_ON,
   FK_PER,
   FK_STUDY,
   PATPROT_ENROLDT,
   PATPROT_TREATINGORG,
   FK_SITE_ENROLLING,
   STUDY_NUMBER,
   FK_ACCOUNT,
   PK_PATPROT, DMGRPH_REPORTABLE,   PATPROT_DISEASE_CODE,
			PATPROT_MORE_DIS_CODE1,  PATPROT_MORE_DIS_CODE2, 
			PATPROT_OTHR_DIS_CODE
)
AS
   SELECT   PATPROT_PATSTDID PSTAT_PAT_STUD_ID,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = FK_USERASSTO)
               PSTAT_ASSIGNED_TO,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = PATPROT_PHYSICIAN)
               PSTAT_PHYSICIAN,
            ERES.F_GET_CODELSTDESC (FK_CODELSTLOC) PSTAT_TREAT_LOCAT,
            ERES.F_GET_CODELSTDESC (FK_CODELST_PTST_EVAL_FLAG)
               PSTAT_EVAL_FLAG,
            ERES.F_GET_CODELSTDESC (FK_CODELST_PTST_EVAL) PSTAT_EVAL_STAT,
            ERES.F_GET_CODELSTDESC (FK_CODELST_PTST_INEVAL) PSTAT_INEVAL_STAT,
            ERES.F_GET_CODELSTDESC (FK_CODELST_PTST_SURVIVAL) PSTAT_SURV_STAT,
            PATPROT_RANDOM PSTAT_RANDOM_NUM,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = pp.FK_USER)
               PSTAT_ENROLLED_BY,
            (SELECT   SITE_NAME
               FROM   eres.ER_SITE
              WHERE   PK_SITE = FK_SITE_ENROLLING)
               PSTAT_ENROLL_SITE,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = pp.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = pp.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            pp.LAST_MODIFIED_DATE,
            pp.CREATED_ON,
            fk_per,
            fk_study,
            patprot_enroldt,
            (SELECT   SITE_NAME
               FROM   eres.ER_SITE
              WHERE   PK_SITE = patprot_treatingorg)
               patprot_treatingorg,
            FK_SITE_ENROLLING,
            STUDY_NUMBER,
            FK_ACCOUNT,
            PK_PATPROT,
			decode(pp.DMGRPH_REPORTABLE, 1,'Yes','No')DMGRPH_REPORTABLE,   pp.PATPROT_DISEASE_CODE,
			pp.PATPROT_MORE_DIS_CODE1,  pp.PATPROT_MORE_DIS_CODE2, 
			pp.PATPROT_OTHR_DIS_CODE
     FROM   eres.ER_PATPROT pp, eres.er_study s
    WHERE       patprot_stat = 1
            AND patprot_enroldt IS NOT NULL
            AND s.pk_study = pp.fk_study
/
COMMENT ON TABLE VDA_V_PAT_ACCRUAL IS 'This view provides access to the patient accrual data for studies'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.FK_STUDY IS 'The Foreign Key to the study milestone is linked with'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PATPROT_ENROLDT IS 'The enrollment date'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PATPROT_TREATINGORG IS 'The treating organization'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.FK_SITE_ENROLLING IS 'The foreign key to the enrolling site'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.STUDY_NUMBER IS 'The Study Number of the study the enrollment record  is linked with'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.FK_ACCOUNT IS 'The account the patient is linked with'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PSTAT_PAT_STUD_ID IS 'The patient study id'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PSTAT_ASSIGNED_TO IS 'The user patient is assigned to'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PSTAT_PHYSICIAN IS 'The physician patient is assigned to'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PSTAT_TREAT_LOCAT IS 'The treatment location'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PSTAT_EVAL_FLAG IS 'The evaluation flag'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PSTAT_EVAL_STAT IS 'The evaluation status'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PSTAT_INEVAL_STAT IS 'The inevaluation status'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PSTAT_SURVIVAL_STAT IS 'The survival status'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PSTAT_RANDOM_NUM IS 'The randomization number allocated to the patient'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PSTAT_ENROLLED_BY IS 'Patient Enrolled By'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PSTAT_ENROLL_SITE IS 'Patient Enrolling Site'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.LAST_MODIFIED_DATE IS 'The date the record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.CREATED_ON IS 'The date the record was created on (Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.FK_PER IS 'The foreign Key to the patient record'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.DMGRPH_REPORTABLE IS 'Flag to indicate whether the patient demographics is CTRP reportable'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PATPROT_DISEASE_CODE IS 'Patient''s disease code value'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PATPROT_OTHR_DIS_CODE IS 'Patient''s Other disease code value'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PATPROT_MORE_DIS_CODE1 IS 'Patient''s More disease code 1 value'
/
 
COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PATPROT_MORE_DIS_CODE2 IS 'Patient''s More disease code 2 value'
/
