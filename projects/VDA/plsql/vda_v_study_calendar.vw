/* Formatted on 3/11/2014 3:19:41 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_STUDY_CALENDAR  (View)
--
--  Dependencies:
--   F_GET_DURATION (Function)
--   F_GET_DURUNIT (Function)
--   ER_CATLIB (Table)
--   ER_STUDY (Table)
--   EVENT_ASSOC (Table)
--   SCH_CODELST (Table)
--   ER_USER (Table)
--

CREATE OR REPLACE FORCE VIEW VDA_V_STUDY_CALENDAR
(
   CALENDAR_TYPE,
   CALENDAR_NAME,
   CALENDAR_DESC,
   CALENDAR_STAT,
   CALENDAR_DURATION,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATED_ON,
   CALENDAR_PK,
   STUDY_NUMBER,
   FK_ACCOUNT,
   EVENT_CALASSOCTO,
   CALENDAR_LINKING_TYPE
)
AS
   SELECT   (SELECT   CATLIB_NAME
               FROM   ERES.ER_CATLIB
              WHERE   PK_CATLIB = FK_CATLIB)
               LBCAL_TYPE,
            NAME LBCAL_NAME,
            DESCRIPTION LBCAL_DESC,
            (SELECT   CODELST_DESC
               FROM   esch.sch_codelst
              WHERE   PK_CODELST = FK_CODELST_CALSTAT)
               LBCAL_STAT,
               ERES.F_GET_DURATION (DURATION_UNIT, DURATION)
            || ' '
            || ERES.F_GET_DURUNIT (DURATION_UNIT)
               LBCAL_DURATION,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = E.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = E.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            E.LAST_MODIFIED_DATE,
            E.CREATED_ON,
            E.EVENT_ID CALENDAR_PK,
            study_number,
            FK_ACCOUNT,
            EVENT_CALASSOCTO,
            DECODE (EVENT_CALASSOCTO,
                    'P', 'Patient',
                    'S', 'Study',
                    EVENT_CALASSOCTO)
               Calendar_linking_type
     FROM   ESCH.EVENT_ASSOC E, eres.er_study
    WHERE   EVENT_TYPE = 'P' AND pk_study = chain_id
/
COMMENT ON TABLE VDA_V_STUDY_CALENDAR IS 'This view provides access to Study Calendars'' summary information'
/

COMMENT ON COLUMN VDA_V_STUDY_CALENDAR.CALENDAR_TYPE IS 'Calendar Type'
/

COMMENT ON COLUMN VDA_V_STUDY_CALENDAR.CALENDAR_NAME IS 'Calendar Name'
/

COMMENT ON COLUMN VDA_V_STUDY_CALENDAR.CALENDAR_DESC IS 'Calendar Description'
/

COMMENT ON COLUMN VDA_V_STUDY_CALENDAR.CALENDAR_STAT IS 'Calendar Status'
/

COMMENT ON COLUMN VDA_V_STUDY_CALENDAR.CALENDAR_DURATION IS 'Calendar Duration'
/

COMMENT ON COLUMN VDA_V_STUDY_CALENDAR.CREATOR IS 'The user who created the calendar record (Audit)'
/

COMMENT ON COLUMN VDA_V_STUDY_CALENDAR.LAST_MODIFIED_BY IS 'The user who last modified the calendar record(Audit)'
/

COMMENT ON COLUMN VDA_V_STUDY_CALENDAR.LAST_MODIFIED_DATE IS 'The date the calendar record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_STUDY_CALENDAR.CREATED_ON IS 'The date the calendar record was created on (Audit)'
/

COMMENT ON COLUMN VDA_V_STUDY_CALENDAR.CALENDAR_PK IS 'The Primary Key of the Calendar record'
/

COMMENT ON COLUMN VDA_V_STUDY_CALENDAR.STUDY_NUMBER IS 'The Study Number of the study the payment  is linked with'
/

COMMENT ON COLUMN VDA_V_STUDY_CALENDAR.FK_ACCOUNT IS 'The account the calendar is linked with'
/

COMMENT ON COLUMN VDA_V_STUDY_CALENDAR.EVENT_CALASSOCTO IS 'The calendar association: Patient Calendar or Admin Calendar'
/

COMMENT ON COLUMN VDA_V_STUDY_CALENDAR.CALENDAR_LINKING_TYPE IS 'The calendar association Description'
/


