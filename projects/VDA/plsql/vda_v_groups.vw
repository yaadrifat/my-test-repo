/* Formatted on 3/11/2014 3:19:04 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_GROUPS  (View)
--
--  Dependencies:
--   ER_CODELST (Table)
--   ER_GRPS (Table)
--   ER_USER (Table)
--

CREATE OR REPLACE FORCE VIEW VDA_V_GROUPS
(
   PK_GRP,
   GRP_NAME,
   GRP_DESC,
   RID,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATED_ON,
   FK_ACCOUNT,
   IP_ADD,
   IS_SUPERUSER_GRP,
   IS_BUD_SUPERUSER_GRP,
   IS_GRP_HIDDEN,
   GRP_STUDYTEAM_ROLE
)
AS
   SELECT   PK_GRP,
            GRP_NAME,
            GRP_DESC,
            RID,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   eres.ER_USER
              WHERE   pk_user = er_grps.CREATOR)
               creator,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   eres.ER_USER
              WHERE   pk_user = er_grps.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            LAST_MODIFIED_DATE,
            CREATED_ON,
            FK_ACCOUNT,
            IP_ADD,
            DECODE (GRP_SUPUSR_FLAG,
                    0,
                    'No',
                    1,
                    'Yes')
               IS_SUPERUSER_GRP,
            DECODE (GRP_SUPBUD_FLAG,
                    0,
                    'No',
                    1,
                    'Yes')
               IS_BUD_SUPERUSER_GRP,
            DECODE (GRP_HIDDEN,
                    0,
                    'No',
                    1,
                    'Yes')
               IS_GRP_HIDDEN,
            (SELECT   codelst_desc
               FROM   eres.er_codelst
              WHERE   pk_codelst = FK_CODELST_ST_ROLE)
               GRP_STUDYTEAM_ROLE
     FROM   eres.er_grps
/
COMMENT ON TABLE VDA_V_GROUPS IS 'This view provides access to all groups'
/

COMMENT ON COLUMN VDA_V_GROUPS.PK_GRP IS 'The Primary Key to uniquely identify the groups created in the database.'
/

COMMENT ON COLUMN VDA_V_GROUPS.GRP_NAME IS 'The group name'
/

COMMENT ON COLUMN VDA_V_GROUPS.GRP_DESC IS 'the group description'
/

COMMENT ON COLUMN VDA_V_GROUPS.RID IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database.'
/

COMMENT ON COLUMN VDA_V_GROUPS.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_GROUPS.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_GROUPS.LAST_MODIFIED_DATE IS 'The date the  record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_GROUPS.CREATED_ON IS 'The date the record was created on (Audit)'
/

COMMENT ON COLUMN VDA_V_GROUPS.FK_ACCOUNT IS 'The account the group is linked with'
/

COMMENT ON COLUMN VDA_V_GROUPS.IP_ADD IS 'The IP ADDRESS of the client machine (Audit)'
/

COMMENT ON COLUMN VDA_V_GROUPS.IS_SUPERUSER_GRP IS 'Indicates if the group is a super user group'
/

COMMENT ON COLUMN VDA_V_GROUPS.IS_BUD_SUPERUSER_GRP IS 'Indicates if the group is a super user group for budgets access'
/

COMMENT ON COLUMN VDA_V_GROUPS.IS_GRP_HIDDEN IS 'Indicates if this Group is hidden in Group and User selection Lookups'
/

COMMENT ON COLUMN VDA_V_GROUPS.GRP_STUDYTEAM_ROLE IS 'This is the Foreign Key to Study Team ROLE code in er_codelst'
/


