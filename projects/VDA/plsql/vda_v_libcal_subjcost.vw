/* Formatted on 3/11/2014 3:19:10 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_LIBCAL_SUBJCOST  (View)
--
--  Dependencies:
--   EVENT_DEF (Table)
--   SCH_CODELST (Table)
--   SCH_SUBCOST_ITEM (Table)
--   SCH_SUBCOST_ITEM_VISIT (Table)
--   SCH_PROTOCOL_VISIT (Table)
--

CREATE OR REPLACE FORCE VIEW VDA_V_LIBCAL_SUBJCOST
(
   PK_SUBCOST_ITEM,
   FK_CALENDAR,
   NAME,
   SUBCOST_ITEM_NAME,
   SUBCOST_ITEM_COST,
   SUBCOST_ITEM_UNIT,
   SUBCOST_CATEGORY,
   VISIT_NAME,
   FK_ACCOUNT
)
AS
   SELECT   PK_SUBCOST_ITEM,
            FK_CALENDAR,
            e.name,
            SUBCOST_ITEM_NAME,
            SUBCOST_ITEM_COST,
            SUBCOST_ITEM_UNIT,
            (SELECT   codelst_desc
               FROM   esch.sch_codelst
              WHERE   pk_codelst = FK_CODELST_CATEGORY)
               SUBCOST_CATEGORY,
            (SELECT   visit_name
               FROM   esch.sch_protocol_visit
              WHERE   pk_protocol_visit = FK_PROTOCOL_VISIT)
               visit_name,
            e.user_id
     FROM   esch.SCH_SUBCOST_ITEM s,
            esch.event_def e,
            esch.SCH_SUBCOST_ITEM_VISIT v
    WHERE   e.event_id = fk_calendar
            AND v.FK_SUBCOST_ITEM(+) = PK_SUBCOST_ITEM
/
COMMENT ON TABLE VDA_V_LIBCAL_SUBJCOST IS 'This view provides access to the Subject Cost Items linked with a Library Calendar'
/

COMMENT ON COLUMN VDA_V_LIBCAL_SUBJCOST.PK_SUBCOST_ITEM IS 'Primary Key of the Subject Cost record'
/

COMMENT ON COLUMN VDA_V_LIBCAL_SUBJCOST.FK_CALENDAR IS 'Foreign Key to the Calendar record'
/

COMMENT ON COLUMN VDA_V_LIBCAL_SUBJCOST.NAME IS 'Name of the Calendar the subject Cost Item is linked with'
/

COMMENT ON COLUMN VDA_V_LIBCAL_SUBJCOST.SUBCOST_ITEM_NAME IS 'Subject Cost Item Name'
/

COMMENT ON COLUMN VDA_V_LIBCAL_SUBJCOST.SUBCOST_ITEM_COST IS 'Subject Cost Item Cost value'
/

COMMENT ON COLUMN VDA_V_LIBCAL_SUBJCOST.SUBCOST_ITEM_UNIT IS 'Number of Units planned'
/

COMMENT ON COLUMN VDA_V_LIBCAL_SUBJCOST.SUBCOST_CATEGORY IS 'Subject Cost Item Category'
/

COMMENT ON COLUMN VDA_V_LIBCAL_SUBJCOST.VISIT_NAME IS 'Name of the visit the cost item is linked with'
/

COMMENT ON COLUMN VDA_V_LIBCAL_SUBJCOST.FK_ACCOUNT IS 'The account the subject cost item is linked with'
/


