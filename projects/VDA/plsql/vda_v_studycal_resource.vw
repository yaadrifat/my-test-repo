/* Formatted on 3/11/2014 3:19:44 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_STUDYCAL_RESOURCE  (View)
--
--  Dependencies:
--   ER_CODELST (Table)
--   EVENT_ASSOC (Table)
--   SCH_EVENTUSR (Table)
--   SCH_PROTOCOL_VISIT (Table)
--   ER_USER (Table)
--

CREATE OR REPLACE FORCE VIEW VDA_V_STUDYCAL_RESOURCE
(
   VISIT_NAME,
   EVREC_EVENT,
   EVREC_USER,
   EVREC_RESOURCE,
   EVREC_DURATION,
   EVREC_NOTES,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATED_ON,
   CALENDAR_PK,
   EVENT_PK,
   FK_ACCOUNT
)
AS
   SELECT   visit_name,
            d.NAME EVREC_EVENT,
            DECODE (EVENTUSR_TYPE,
                    'U', (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
                            FROM   eres.ER_USER
                           WHERE   PK_USER = EVENTUSR),
                    '')
               EVREC_USER,
            DECODE (EVENTUSR_TYPE,
                    'R', (SELECT   CODELST_DESC
                            FROM   eres.ER_CODELST
                           WHERE   PK_CODELST = EVENTUSR),
                    '')
               EVREC_RESOURCE,
            EVENTUSR_DURATION EVREC_DURATION,
            EVENTUSR_NOTES EVREC_NOTES,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = u.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = u.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            u.LAST_MODIFIED_DATE,
            u.CREATED_ON,
            d.chain_id,
            u.FK_EVENT,
            d.user_id
     FROM   esch.SCH_EVENTUSR u,
            esch.EVENT_ASSOC d,
            esch.sch_protocol_visit v
    WHERE       EVENT_ID = FK_EVENT
            AND EVENTUSR IS NOT NULL
            AND event_type = 'A'
            AND (EVENTUSR_TYPE = 'U' OR EVENTUSR_TYPE = 'R')
            AND fk_visit = pk_protocol_visit
/
COMMENT ON TABLE VDA_V_STUDYCAL_RESOURCE IS 'This view provides access to the Calendar resource information'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE.VISIT_NAME IS 'The name of the Calendar visit'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE.EVREC_EVENT IS 'The name of the Calendar Visit Event'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE.EVREC_USER IS 'The name of the user (named user in Velos)'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE.EVREC_RESOURCE IS 'The resource role (if named user is not used)'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE.EVREC_DURATION IS 'The duration the resource is needed for'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE.EVREC_NOTES IS 'Notes linked'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE.LAST_MODIFIED_DATE IS 'The date the record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE.CREATED_ON IS 'The date the record was created on (Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE.CALENDAR_PK IS 'The Foreign Key to the main Calendar record'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE.EVENT_PK IS 'The Primary Key of the Visit-Event record'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE.FK_ACCOUNT IS 'The account the record is linked with'
/


