/* Formatted on 3/11/2014 3:19:30 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_PAT_SITES  (View)
--
--  Dependencies:
--   ER_PATFACILITY (Table)
--   F_GET_YESNO (Function)
--   PERSON (Table)
--   ER_USER (Table)
--   CODE_LST_NAMES (Function)
--   ER_SITE (Table)
--

CREATE OR REPLACE FORCE VIEW VDA_V_PAT_SITES
(
   PATIENT_ID,
   SITE_NAME,
   PAT_FACILITY_ID,
   REG_DATE,
   PROVIDER,
   SPECIALTY_GROUP,
   FACILITY_ACCESS,
   FACILITY_DEFAULT,
   CREATED_ON,
   LAST_MODIFIED_DATE,
   CREATOR,
   LAST_MODIFIED_BY,
   FK_ACCOUNT,
   RID,
   PK_PATFACILITY,
   FK_PER
)
AS
   SELECT   PERSON_CODE,
            (SELECT   SITE_NAME
               FROM   eres.ER_SITE
              WHERE   PK_SITE = ER_PATFACILITY.FK_SITE)
               SITE_NAME,
            ER_PATFACILITY.PAT_FACILITYID,
            TRUNC (PATFACILITY_REGDATE) REG_DATE,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = PATFACILITY_PROVIDER)
               PROVIDER,
            eres.code_lst_names (PATFACILITY_SPLACCESS) SPECIALTY_GROUP,
            DECODE (PATFACILITY_ACCESSRIGHT, 0, 'Revoked', 'Granted')
               FACILITY_ACCESS,
            eres.F_GET_YESNO (PATFACILITY_DEFAULT) FACILITY_DEFAULT,
            ER_PATFACILITY.CREATED_ON,
            ER_PATFACILITY.LAST_MODIFIED_DATE,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = ER_PATFACILITY.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = ER_PATFACILITY.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            FK_ACCOUNT,
            ER_PATFACILITY.RID,
            PK_PATFACILITY,
            fk_per
     FROM   ERES.ER_PATFACILITY, EPAT.PERSON
    WHERE   ER_PATFACILITY.FK_PER = PERSON.PK_PERSON
/
COMMENT ON TABLE VDA_V_PAT_SITES IS 'This view provides access to the Patient Organization association'
/

COMMENT ON COLUMN VDA_V_PAT_SITES.PK_PATFACILITY IS 'The Primary Key of the patient facility or site record'
/

COMMENT ON COLUMN VDA_V_PAT_SITES.FK_PER IS 'The Primary Key of the patient record'
/

COMMENT ON COLUMN VDA_V_PAT_SITES.FACILITY_ACCESS IS 'The users of this site has access to the patient'
/

COMMENT ON COLUMN VDA_V_PAT_SITES.FACILITY_DEFAULT IS 'Flag to indicate if it is the default facility or site for the patient'
/

COMMENT ON COLUMN VDA_V_PAT_SITES.CREATED_ON IS 'The date the record was created on (Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_SITES.LAST_MODIFIED_DATE IS 'The date the record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_SITES.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_SITES.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_SITES.FK_ACCOUNT IS 'The account the patient is linked with'
/

COMMENT ON COLUMN VDA_V_PAT_SITES.RID IS 'The unique Row ID for Audit'
/

COMMENT ON COLUMN VDA_V_PAT_SITES.PATIENT_ID IS 'The Patient ID or Patient Code'
/

COMMENT ON COLUMN VDA_V_PAT_SITES.SITE_NAME IS 'The Site Name the patient is linked with'
/

COMMENT ON COLUMN VDA_V_PAT_SITES.PAT_FACILITY_ID IS 'The Patient Facility ID'
/

COMMENT ON COLUMN VDA_V_PAT_SITES.REG_DATE IS 'The Date the patient was associated with the facility'
/

COMMENT ON COLUMN VDA_V_PAT_SITES.PROVIDER IS 'The user linked with patient facility association'
/

COMMENT ON COLUMN VDA_V_PAT_SITES.SPECIALTY_GROUP IS 'The specialty the facility has access to'
/


