/* Formatted on 3/11/2014 3:20:05 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_USERSESSION  (View)
--
--  Dependencies:
--   ER_USER (Table)
--   ER_USERSESSION (Table)
--

CREATE OR REPLACE FORCE VIEW VDA_V_USERSESSION
(
   SESSION_ID,
   LOGIN_TIME,
   LOGOUT_TIME,
   IP_ADD,
   SUCCESS_FLAG,
   USER_NAME
)
AS
   SELECT   pk_usersession AS session_id,
            login_time,
            logout_time,
            ip_add,
            success_flag,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   eres.er_user
              WHERE   pk_user = fk_user)
               user_name
     FROM   eres.er_usersession
/
COMMENT ON TABLE VDA_V_USERSESSION IS 'This view provides access to the user session records'
/

COMMENT ON COLUMN VDA_V_USERSESSION.SESSION_ID IS 'The identifier of the session'
/

COMMENT ON COLUMN VDA_V_USERSESSION.LOGIN_TIME IS 'The recorded login time for this session'
/

COMMENT ON COLUMN VDA_V_USERSESSION.LOGOUT_TIME IS 'The recorded logout time for this session'
/

COMMENT ON COLUMN VDA_V_USERSESSION.IP_ADD IS 'The IP address of the client machine that initiated the session'
/

COMMENT ON COLUMN VDA_V_USERSESSION.SUCCESS_FLAG IS 'The success flag of the session'
/

COMMENT ON COLUMN VDA_V_USERSESSION.USER_NAME IS 'The name of the user who initiated the session'
/


