/* Formatted on 3/11/2014 3:18:33 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_BUDGET_CALENDAR  (View)
--
--  Dependencies:
--   F_GET_FSTAT (Function)
--   F_GET_YESNO (Function)
--   ER_STUDY (Table)
--   SCH_BGTCAL (Table)
--   SCH_BUDGET (Table)
--   SCH_CODELST (Table)
--   ER_USER (Table)
--   ER_SITE (Table)
--

CREATE OR REPLACE FORCE VIEW VDA_V_BUDGET_CALENDAR
(
   BGTDT_NAME,
   BGTDT_VERSION,
   BGTDT_TEMPLATE,
   BGTDT_STUDY_NUM,
   BGTDT_STUDY_TITLE,
   BGTDT_ORGANIZATION,
   BGTDT_STATUS,
   BGTDT_DESCRIPTION,
   BGTDT_CREATED_BY,
   BGTDT_CURRENCY,
   FK_ACCOUNT,
   PK_BUDGET,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATED_ON,
   FK_STUDY,
   BGTCAL_SPONSOROHEAD,
   SPONSOR_OHEAD_FLAG,
   BGTCAL_INDIRECTCOST,
   BGTCAL_FRGBENEFIT,
   FRINGEBFT_FLAG,
   BGTCAL_DISCOUNT,
   DISCNT_FLAG,
   EXCLUD_SOC,
   BUDGET_COMBFLAG
)
AS
   SELECT   DISTINCT
            BUDGET_NAME BGTDT_NAME,
            BUDGET_VERSION BGTDT_VERSION,
            NVL ( (SELECT   CODELST_DESC
                     FROM   esch.SCH_CODELST
                    WHERE   PK_CODELST = B.BUDGET_TEMPLATE),
                 (SELECT   BUDGET_NAME
                    FROM   esch.SCH_BUDGET
                   WHERE   PK_BUDGET = B.BUDGET_TEMPLATE))
               BGTDT_TEMPLATE,
            (SELECT   STUDY_NUMBER
               FROM   eres.ER_STUDY
              WHERE   PK_STUDY = FK_STUDY)
               BGTDT_STUDY_NUM,
            (SELECT   STUDY_TITLE
               FROM   eres.ER_STUDY
              WHERE   PK_STUDY = FK_STUDY)
               BGTDT_STUDY_TITLE,
            (SELECT   SITE_NAME
               FROM   eres.ER_SITE
              WHERE   PK_SITE = FK_SITE)
               BGTDT_ORGANIZATION,
            ERES.F_GEt_Fstat (BUDGET_STATUS) BGTDT_STATUS,
            BUDGET_DESC BGTDT_DESCRIPTION,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = BUDGET_CREATOR)
               BGTDT_CREATED_BY,
            (SELECT   CODELST_DESC
               FROM   esch.SCH_CODELST
              WHERE   PK_CODELST = BUDGET_CURRENCY)
               BGTDT_CURRENCY,
            B.FK_ACCOUNT,
            PK_BUDGET,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = B.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = B.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            B.LAST_MODIFIED_DATE,
            B.CREATED_ON,
            fk_study,
            BGTCAL_SPONSOROHEAD,
            ERES.F_GEt_Yesno (BGTCAL_SPONSORFLAG) SPONSOR_OHEAD_FLAG,
            BGTCAL_INDIRECTCOST,
            BGTCAL_FRGBENEFIT,
            ERES.F_GEt_Yesno (BGTCAL_FRGFLAG) FRINGEBFT_FLAG,
            BGTCAL_DISCOUNT,
            ERES.F_GEt_Yesno (BGTCAL_DISCOUNTFLAG) DISCNT_FLAG,
            ERES.F_GEt_Yesno (BGTCAL_EXCLDSOCFLAG) EXCLUD_SOC,
            BUDGET_COMBFLAG
     FROM   esch.SCH_BUDGET B, esch.SCH_BGTCAL C
    WHERE   C.FK_BUDGET = B.PK_BUDGET
            AND (BUDGET_DELFLAG IS NULL OR BUDGET_DELFLAG <> 'Y')
/
COMMENT ON TABLE VDA_V_BUDGET_CALENDAR IS 'This View provides access to the Budget Calendar information. There is a default calendar added for each budget. In addition, user can add one or more Library or Study Calendars directly to the budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.BGTDT_NAME IS 'The Budget name'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.BGTDT_VERSION IS 'The Budget version number'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.BGTDT_TEMPLATE IS 'The template used for creating the budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.BGTDT_STUDY_NUM IS 'The study number of the study linked with the budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.BGTDT_STUDY_TITLE IS 'The title of the study is linked with the budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.BGTDT_ORGANIZATION IS 'The organization linked with the budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.BGTDT_STATUS IS 'The status of the budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.BGTDT_DESCRIPTION IS 'The description of the budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.BGTDT_CREATED_BY IS 'The user who created the Budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.BGTDT_CURRENCY IS 'The currency the budget is created in'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.FK_ACCOUNT IS 'The Account linked with the Budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.PK_BUDGET IS 'The Primary Key of the main Budget record'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.CREATOR IS 'The user who created the Budget record (Audit)'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.LAST_MODIFIED_BY IS 'The user who last modified the Budget record (Audit)'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.LAST_MODIFIED_DATE IS 'The date the Budget record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.CREATED_ON IS 'The date the Budget record was created on (Audit)'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.FK_STUDY IS 'The Foreign Key to the study the Budget record is linked with'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.BGTCAL_SPONSOROHEAD IS 'The Sponsor Overhead number'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.SPONSOR_OHEAD_FLAG IS 'The Sponsor Overhead flag'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.BGTCAL_INDIRECTCOST IS 'The indirect cost number to be applied to selected lineitems'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.BGTCAL_FRGBENEFIT IS 'The Fringe Benefit number to be applied to selected lineitems'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.FRINGEBFT_FLAG IS 'The Fringe Benefit calculation flag'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.BGTCAL_DISCOUNT IS 'The discount or markup number'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.DISCNT_FLAG IS 'The discount or markup flag'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.EXCLUD_SOC IS 'The Exclude Standard of Care cost from totals flag'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.BUDGET_COMBFLAG IS 'The flag to indicate if the budget is a combined budget'
/


