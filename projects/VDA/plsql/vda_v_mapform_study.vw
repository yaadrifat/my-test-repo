/* Formatted on 3/11/2014 3:19:16 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_MAPFORM_STUDY  (View)
--
--  Dependencies:
--   ER_MAPFORM (Table)
--   ER_FORMLIB (Table)
--

CREATE OR REPLACE FORCE VIEW VDA_V_MAPFORM_STUDY
(
   PK_MP,
   FK_FORM,
   MP_FORMTYPE,
   MP_MAPCOLNAME,
   MP_SYSTEMID,
   MP_KEYWORD,
   MP_UID,
   MP_DISPNAME,
   MP_ORIGSYSID,
   MP_FLDDATATYPE,
   MP_BROWSER,
   MP_SEQUENCE,
   MP_PKSEC,
   MP_SECNAME,
   MP_PKFLD,
   MP_FLDCHARSNO,
   MP_FLDSEQNO,
   FORM_NAME,
   FORM_DESC,
   FK_ACCOUNT
)
AS
   SELECT   m."PK_MP",
            m."FK_FORM",
            m."MP_FORMTYPE",
            m."MP_MAPCOLNAME",
            m."MP_SYSTEMID",
            m."MP_KEYWORD",
            m."MP_UID",
            m."MP_DISPNAME",
            m."MP_ORIGSYSID",
            m."MP_FLDDATATYPE",
            m."MP_BROWSER",
            m."MP_SEQUENCE",
            m."MP_PKSEC",
            m."MP_SECNAME",
            m."MP_PKFLD",
            m."MP_FLDCHARSNO",
            m."MP_FLDSEQNO",
            f.form_name,
            f.form_desc,
            f.FK_ACCOUNT
     FROM   eres.er_mapform m, eres.er_formlib f
    WHERE   fk_form = pk_formlib AND mp_formtype = 'S'
/
COMMENT ON TABLE VDA_V_MAPFORM_STUDY IS 'This view provides access to the form design information that can be used to write reports on form responses. Please use information in this table to write a report on all study type form responses available through the view VDA.VDA_V_STUDYFORMRESPONSES'
/

COMMENT ON COLUMN VDA_V_MAPFORM_STUDY.PK_MP IS 'The Primary Key of the mapping record'
/

COMMENT ON COLUMN VDA_V_MAPFORM_STUDY.FK_FORM IS 'The Foreign Key to the form record'
/

COMMENT ON COLUMN VDA_V_MAPFORM_STUDY.MP_FORMTYPE IS 'The form type - account in this case'
/

COMMENT ON COLUMN VDA_V_MAPFORM_STUDY.MP_MAPCOLNAME IS 'The column name in VDA.VDA_V_STUDYFORMRESPONSES where data for this field is accessible from'
/

COMMENT ON COLUMN VDA_V_MAPFORM_STUDY.MP_SYSTEMID IS 'The auto generated system of the field that uniquely identifies the field in a form'
/

COMMENT ON COLUMN VDA_V_MAPFORM_STUDY.MP_KEYWORD IS 'The keyword given to the field'
/

COMMENT ON COLUMN VDA_V_MAPFORM_STUDY.MP_UID IS 'The field id given to the field'
/

COMMENT ON COLUMN VDA_V_MAPFORM_STUDY.MP_DISPNAME IS 'The display name of the field'
/

COMMENT ON COLUMN VDA_V_MAPFORM_STUDY.MP_ORIGSYSID IS 'The auto generated system of the field that uniquely identifies the field in a form. This field is relevant for fields that are part of ''repeating sections'' and ties them with the original field of such a section'
/

COMMENT ON COLUMN VDA_V_MAPFORM_STUDY.MP_FLDDATATYPE IS 'The data type of the field'
/

COMMENT ON COLUMN VDA_V_MAPFORM_STUDY.MP_BROWSER IS 'The flag whether the field should be displayed in the form response browser'
/

COMMENT ON COLUMN VDA_V_MAPFORM_STUDY.MP_SEQUENCE IS 'The sequence of the section within the form'
/

COMMENT ON COLUMN VDA_V_MAPFORM_STUDY.MP_PKSEC IS 'The Primary Key of the section'
/

COMMENT ON COLUMN VDA_V_MAPFORM_STUDY.MP_SECNAME IS 'The name of the section'
/

COMMENT ON COLUMN VDA_V_MAPFORM_STUDY.MP_PKFLD IS 'The Primary Key of the field'
/

COMMENT ON COLUMN VDA_V_MAPFORM_STUDY.MP_FLDCHARSNO IS 'The maximum number of characters that are valid for the field input. Applicable for edit box fields'
/

COMMENT ON COLUMN VDA_V_MAPFORM_STUDY.MP_FLDSEQNO IS 'The sequence of the field within the form section'
/

COMMENT ON COLUMN VDA_V_MAPFORM_STUDY.FORM_NAME IS 'The name of the form'
/

COMMENT ON COLUMN VDA_V_MAPFORM_STUDY.FORM_DESC IS 'The description of the form'
/

COMMENT ON COLUMN VDA_V_MAPFORM_STUDY.FK_ACCOUNT IS 'The account the form is linked with'
/


