/* Formatted on 3/11/2014 3:19:24 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_PAT_ACCRUAL_ALL  (View)
--
--  Dependencies:
--   PKG_UTIL (Package)
--   F_GET_CODELSTDESC (Function)
--   ER_STUDYSITES (Table)
--   CODE_LST_NAMES (Function)
--   VDA_V_PAT_ACCRUAL (View)
--   VDA_V_PAT_DEMO_NOPHI (View)
--

CREATE OR REPLACE FORCE VIEW VDA_V_PAT_ACCRUAL_ALL
(
   PSTAT_PAT_STUD_ID,
   PSTAT_ASSIGNED_TO,
   PSTAT_PHYSICIAN,
   PSTAT_TREAT_LOCAT,
   PSTAT_EVAL_FLAG,
   PSTAT_EVAL_STAT,
   PSTAT_INEVAL_STAT,
   PSTAT_SURVIVAL_STAT,
   PSTAT_RANDOM_NUM,
   PSTAT_ENROLLED_BY,
   PSTAT_ENROLL_SITE,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATED_ON,
   FK_PER,
   FK_STUDY,
   PATPROT_ENROLDT,
   PATPROT_TREATINGORG,
   PATPROT_ENROLL_YEAR,
   PTDEM_SURVIVSTAT,
   PTDEM_GENDER,
   PTDEM_MARSTAT,
   PTDEM_BLOODGRP,
   PTDEM_PRI_ETHNY,
   PTDEM_PRI_RACE,
   PTDEM_ADD_ETHNY,
   PTDEM_ADD_RACE,
   PTDEM_STATE,
   PTDEM_COUNTRY,
   PTDEM_EMPLOY,
   PTDEM_EDUCATION,
   PTDEM_REGDATE_YR,
   STUDYSITE_LSAMPLESIZE,DMGRPH_REPORTABLE,   PATPROT_DISEASE_CODE,
			PATPROT_MORE_DIS_CODE1,  PATPROT_MORE_DIS_CODE2, 
			PATPROT_OTHR_DIS_CODE
)
AS
   SELECT   PSTAT_PAT_STUD_ID,
            PSTAT_ASSIGNED_TO,
            PSTAT_PHYSICIAN,
            PSTAT_TREAT_LOCAT,
            PSTAT_EVAL_FLAG,
            PSTAT_EVAL_STAT,
            PSTAT_INEVAL_STAT,
            PSTAT_SURVIVAL_STAT,
            PSTAT_RANDOM_NUM,
            PSTAT_ENROLLED_BY,
            PSTAT_ENROLL_SITE,
            a.CREATOR,
            a.LAST_MODIFIED_BY,
            a.LAST_MODIFIED_DATE,
            a.CREATED_ON,
            a.FK_PER,
            a.FK_STUDY,
            patprot_enroldt,
            patprot_treatingorg,
            EXTRACT (YEAR FROM patprot_enroldt) patprot_enroll_year,
            d.PTDEM_SURVIVSTAT,
            d.PTDEM_GENDER,
            d.PTDEM_MARSTAT,
            d.PTDEM_BLOODGRP,
            d.PTDEM_PRI_ETHNY,
            d.PTDEM_PRI_RACE,
            d.PTDEM_ADD_ETHNY,
            d.PTDEM_ADD_RACE,
            d.PTDEM_STATE,
            d.PTDEM_COUNTRY,
            d.PTDEM_EMPLOY,
            d.PTDEM_EDUCATION,
            d.PTDEM_REGDATE_YR,
            studysite_lsamplesize, a.DMGRPH_REPORTABLE,   a.PATPROT_DISEASE_CODE,
			a.PATPROT_MORE_DIS_CODE1,  a.PATPROT_MORE_DIS_CODE2, 
			a.PATPROT_OTHR_DIS_CODE
     FROM   VDA.VDA_V_PAT_ACCRUAL a,
            VDA.VDA_V_PAT_DEMO_NOPHI d,
            eres.er_studySites s
    WHERE       a.fk_per = d.pk_person
            AND s.fk_study = a.fk_study
            AND s.fk_site = a.FK_SITE_ENROLLING
/
COMMENT ON TABLE VDA_V_PAT_ACCRUAL_ALL IS 'This view provides access to the patient accrual information with patient details'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PSTAT_PAT_STUD_ID IS 'The patiient study id'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PSTAT_ASSIGNED_TO IS 'The user patient is assigned to'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PSTAT_PHYSICIAN IS 'The physician patient is assigned to'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PSTAT_TREAT_LOCAT IS 'The treatment location'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PSTAT_EVAL_FLAG IS 'The Evaluable Flag'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PSTAT_EVAL_STAT IS 'The Evaluable Status'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PSTAT_INEVAL_STAT IS 'The Unevaluable Status'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PSTAT_SURVIVAL_STAT IS 'The Patient survival status'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PSTAT_RANDOM_NUM IS 'The randomization number'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PSTAT_ENROLLED_BY IS 'The user who enrolled the patient'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PSTAT_ENROLL_SITE IS 'The patient enrolling site'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.LAST_MODIFIED_DATE IS 'The date the  record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.CREATED_ON IS 'The date the record was created on (Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.FK_PER IS 'The Primary Key of the patient the schedule is linked with'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.FK_STUDY IS 'The Primary Key of the study schedule is linked with'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PATPROT_ENROLDT IS 'The enrollment date'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PATPROT_TREATINGORG IS 'The treating organization name'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PATPROT_ENROLL_YEAR IS 'The enrollment year'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PTDEM_SURVIVSTAT IS 'The survival status'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PTDEM_GENDER IS 'The patient gender'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PTDEM_MARSTAT IS 'The patient marital status'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PTDEM_BLOODGRP IS 'The patient blood group'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PTDEM_PRI_ETHNY IS 'The patient primary ethnicity'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PTDEM_PRI_RACE IS 'The patient primary race'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PTDEM_ADD_ETHNY IS 'The patient additional ethnicity'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PTDEM_ADD_RACE IS 'The patient additional race'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PTDEM_STATE IS 'The patient state'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PTDEM_COUNTRY IS 'The patient country'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PTDEM_EMPLOY IS 'The patient employment'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PTDEM_EDUCATION IS 'The patient education'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PTDEM_REGDATE_YR IS 'The patient registration year with the default site or organization'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.STUDYSITE_LSAMPLESIZE IS 'The enrolling site''s local sample size'
/


COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.DMGRPH_REPORTABLE IS 'Flag to indicate whether the patient demographics is CTRP reportable'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PATPROT_DISEASE_CODE IS 'Patient''s disease code value'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PATPROT_OTHR_DIS_CODE IS 'Patient''s Other disease code value'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PATPROT_MORE_DIS_CODE1 IS 'Patient''s More disease code 1 value'
/
 
COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PATPROT_MORE_DIS_CODE2 IS 'Patient''s More disease code 2 value'
/
