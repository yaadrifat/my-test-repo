/* Formatted on 6/11/2014 9:15:52 AM (QP5 v5.115.810.9015) */
--
-- VDA_V_PAYMENTS  (View)
--
--  Dependencies:
--   F_GET_CODELSTDESC (Function)
--   ER_MILEPAYMENT (Table)
--   ER_STUDY (Table)
--   ER_USER (Table)
--

CREATE OR REPLACE  VIEW VDA_V_PAYMENTS
(
   STUDY_NUMBER,
   STUDY_TITLE,
   MSPAY_DATE,
   MSPAY_TYPE,
   MSPAY_AMT_RECVD,
   MSPAY_DESC,
   MSPAY_COMMENTS,
   PK_MILEPAYMENT,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATED_ON,
   FK_STUDY,
   FK_ACCOUNT
)
AS
   SELECT   STUDY_NUMBER,
            STUDY_TITLE,
            MILEPAYMENT_DATE MSPAY_DATE,
            ERES.F_GET_CODELSTDESC (MILEPAYMENT_TYPE) MSPAY_TYPE,
            MILEPAYMENT_AMT MSPAY_AMT_RECVD,
            MILEPAYMENT_DESC MSPAY_DESC,
            MILEPAYMENT_COMMENTS MSPAY_COMMENTS,
            PK_MILEPAYMENT,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = x.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = x.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            x.LAST_MODIFIED_DATE,
            x.CREATED_ON,
            FK_STUDY,
            FK_ACCOUNT
     FROM   eres.ER_MILEPAYMENT x, eres.ER_STUDY s
    WHERE   PK_STUDY = FK_STUDY AND MILEPAYMENT_DELFLAG <> 'Y'
/
COMMENT ON TABLE VDA_V_PAYMENTS IS 'This view provides access to the payment information for studies'
/

COMMENT ON COLUMN VDA_V_PAYMENTS.FK_ACCOUNT IS 'The account the milestone is linked with'
/

COMMENT ON COLUMN VDA_V_PAYMENTS.STUDY_NUMBER IS 'The Study Number of the study the payment  is linked with'
/

COMMENT ON COLUMN VDA_V_PAYMENTS.STUDY_TITLE IS 'The Study Title of the study the payment is linked with'
/

COMMENT ON COLUMN VDA_V_PAYMENTS.MSPAY_DATE IS 'The payment date'
/

COMMENT ON COLUMN VDA_V_PAYMENTS.MSPAY_TYPE IS 'The payment type'
/

COMMENT ON COLUMN VDA_V_PAYMENTS.MSPAY_AMT_RECVD IS 'The payment amount received'
/

COMMENT ON COLUMN VDA_V_PAYMENTS.MSPAY_DESC IS 'The payment description'
/

COMMENT ON COLUMN VDA_V_PAYMENTS.MSPAY_COMMENTS IS 'The payment comments'
/

COMMENT ON COLUMN VDA_V_PAYMENTS.PK_MILEPAYMENT IS 'The Primary Key of the payment record'
/

COMMENT ON COLUMN VDA_V_PAYMENTS.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_PAYMENTS.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_PAYMENTS.LAST_MODIFIED_DATE IS 'The date the record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_PAYMENTS.CREATED_ON IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_PAYMENTS.FK_STUDY IS 'The Foreign Key to the study milestone is linked with'
/

