/* Formatted on 3/11/2014 3:18:25 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_ACCOUNTFORMRESPONSES  (View)
--
--  Dependencies:
--   ER_FORMSLINEAR (Table)
--   ER_FORMLIB (Table)
--   ER_USER (Table)
--

CREATE OR REPLACE FORCE VIEW VDA_V_ACCOUNTFORMRESPONSES
(
   FORM_NAME,
   FORM_DESC,
   FORM_VERSION,
   FK_FORM,
   FILLDATE,
   FORM_TYPE,
   FK_ACCOUNT,
   COL1,
   COL2,
   COL3,
   COL4,
   COL5,
   COL6,
   COL7,
   COL8,
   COL9,
   COL10,
   COL11,
   COL12,
   COL13,
   COL14,
   COL15,
   COL16,
   COL17,
   COL18,
   COL19,
   COL20,
   COL21,
   COL22,
   COL23,
   COL24,
   COL25,
   COL26,
   COL27,
   COL28,
   COL29,
   COL30,
   COL31,
   COL32,
   COL33,
   COL34,
   COL35,
   COL36,
   COL37,
   COL38,
   COL39,
   COL40,
   COL41,
   COL42,
   COL43,
   COL44,
   COL45,
   COL46,
   COL47,
   COL48,
   COL49,
   COL50,
   COL51,
   COL52,
   COL53,
   COL54,
   COL55,
   COL56,
   COL57,
   COL58,
   COL59,
   COL60,
   COL61,
   COL62,
   COL63,
   COL64,
   COL65,
   COL66,
   COL67,
   COL68,
   COL69,
   COL70,
   COL71,
   COL72,
   COL73,
   COL74,
   COL75,
   COL76,
   COL77,
   COL78,
   COL79,
   COL80,
   COL81,
   COL82,
   COL83,
   COL84,
   COL85,
   COL86,
   COL87,
   COL88,
   COL89,
   COL90,
   COL91,
   COL92,
   COL93,
   COL94,
   COL95,
   COL96,
   COL97,
   COL98,
   COL99,
   COL100,
   COL101,
   COL102,
   COL103,
   COL104,
   COL105,
   COL106,
   COL107,
   COL108,
   COL109,
   COL110,
   COL111,
   COL112,
   COL113,
   COL114,
   COL115,
   COL116,
   COL117,
   COL118,
   COL119,
   COL120,
   COL121,
   COL122,
   COL123,
   COL124,
   COL125,
   COL126,
   COL127,
   COL128,
   COL129,
   COL130,
   COL131,
   COL132,
   COL133,
   COL134,
   COL135,
   COL136,
   COL137,
   COL138,
   COL139,
   COL140,
   COL141,
   COL142,
   COL143,
   COL144,
   COL145,
   COL146,
   COL147,
   COL148,
   COL149,
   COL150,
   COL151,
   COL152,
   COL153,
   COL154,
   COL155,
   COL156,
   COL157,
   COL158,
   COL159,
   COL160,
   COL161,
   COL162,
   COL163,
   COL164,
   COL165,
   COL166,
   COL167,
   COL168,
   COL169,
   COL170,
   COL171,
   COL172,
   COL173,
   COL174,
   COL175,
   COL176,
   COL177,
   COL178,
   COL179,
   COL180,
   COL181,
   COL182,
   COL183,
   COL184,
   COL185,
   COL186,
   COL187,
   COL188,
   COL189,
   COL190,
   COL191,
   COL192,
   COL193,
   COL194,
   COL195,
   COL196,
   COL197,
   COL198,
   COL199,
   COL200,
   COL201,
   COL202,
   COL203,
   COL204,
   COL205,
   COL206,
   COL207,
   COL208,
   COL209,
   COL210,
   COL211,
   COL212,
   COL213,
   COL214,
   COL215,
   COL216,
   COL217,
   COL218,
   COL219,
   COL220,
   COL221,
   COL222,
   COL223,
   COL224,
   COL225,
   COL226,
   COL227,
   COL228,
   COL229,
   COL230,
   COL231,
   COL232,
   COL233,
   COL234,
   COL235,
   COL236,
   COL237,
   COL238,
   COL239,
   COL240,
   COL241,
   COL242,
   COL243,
   COL244,
   COL245,
   COL246,
   COL247,
   COL248,
   COL249,
   COL250,
   COL251,
   COL252,
   COL253,
   COL254,
   COL255,
   COL256,
   COL257,
   COL258,
   COL259,
   COL260,
   COL261,
   COL262,
   COL263,
   COL264,
   COL265,
   COL266,
   COL267,
   COL268,
   COL269,
   COL270,
   COL271,
   COL272,
   COL273,
   COL274,
   COL275,
   COL276,
   COL277,
   COL278,
   COL279,
   COL280,
   COL281,
   COL282,
   COL283,
   COL284,
   COL285,
   COL286,
   COL287,
   COL288,
   COL289,
   COL290,
   COL291,
   COL292,
   COL293,
   COL294,
   COL295,
   COL296,
   COL297,
   COL298,
   COL299,
   COL300,
   COL301,
   COL302,
   COL303,
   COL304,
   COL305,
   COL306,
   COL307,
   COL308,
   COL309,
   COL310,
   COL311,
   COL312,
   COL313,
   COL314,
   COL315,
   COL316,
   COL317,
   COL318,
   COL319,
   COL320,
   COL321,
   COL322,
   COL323,
   COL324,
   COL325,
   COL326,
   COL327,
   COL328,
   COL329,
   COL330,
   COL331,
   COL332,
   COL333,
   COL334,
   COL335,
   COL336,
   COL337,
   COL338,
   COL339,
   COL340,
   COL341,
   COL342,
   COL343,
   COL344,
   COL345,
   COL346,
   COL347,
   COL348,
   COL349,
   COL350,
   COL351,
   COL352,
   COL353,
   COL354,
   COL355,
   COL356,
   COL357,
   COL358,
   COL359,
   COL360,
   COL361,
   COL362,
   COL363,
   COL364,
   COL365,
   COL366,
   COL367,
   COL368,
   COL369,
   COL370,
   COL371,
   COL372,
   COL373,
   COL374,
   COL375,
   COL376,
   COL377,
   COL378,
   COL379,
   COL380,
   COL381,
   COL382,
   COL383,
   COL384,
   COL385,
   COL386,
   COL387,
   COL388,
   COL389,
   COL390,
   COL391,
   COL392,
   COL393,
   COL394,
   COL395,
   COL396,
   COL397,
   COL398,
   COL399,
   COL400,
   COL401,
   COL402,
   COL403,
   COL404,
   COL405,
   COL406,
   COL407,
   COL408,
   COL409,
   COL410,
   COL411,
   COL412,
   COL413,
   COL414,
   COL415,
   COL416,
   COL417,
   COL418,
   COL419,
   COL420,
   COL421,
   COL422,
   COL423,
   COL424,
   COL425,
   COL426,
   COL427,
   COL428,
   COL429,
   COL430,
   COL431,
   COL432,
   COL433,
   COL434,
   COL435,
   COL436,
   COL437,
   COL438,
   COL439,
   COL440,
   COL441,
   COL442,
   COL443,
   COL444,
   COL445,
   COL446,
   COL447,
   COL448,
   COL449,
   COL450,
   COL451,
   COL452,
   COL453,
   COL454,
   COL455,
   COL456,
   COL457,
   COL458,
   COL459,
   COL460,
   COL461,
   COL462,
   COL463,
   COL464,
   COL465,
   COL466,
   COL467,
   COL468,
   COL469,
   COL470,
   COL471,
   COL472,
   COL473,
   COL474,
   COL475,
   COL476,
   COL477,
   COL478,
   COL479,
   COL480,
   COL481,
   COL482,
   COL483,
   COL484,
   COL485,
   COL486,
   COL487,
   COL488,
   COL489,
   COL490,
   COL491,
   COL492,
   COL493,
   COL494,
   COL495,
   COL496,
   COL497,
   COL498,
   COL499,
   COL500,
   COL501,
   COL502,
   COL503,
   COL504,
   COL505,
   COL506,
   COL507,
   COL508,
   COL509,
   COL510,
   COL511,
   COL512,
   COL513,
   COL514,
   COL515,
   COL516,
   COL517,
   COL518,
   COL519,
   COL520,
   COL521,
   COL522,
   COL523,
   COL524,
   COL525,
   COL526,
   COL527,
   COL528,
   COL529,
   COL530,
   COL531,
   COL532,
   COL533,
   COL534,
   COL535,
   COL536,
   COL537,
   COL538,
   COL539,
   COL540,
   COL541,
   COL542,
   COL543,
   COL544,
   COL545,
   COL546,
   COL547,
   COL548,
   COL549,
   COL550,
   COL551,
   COL552,
   COL553,
   COL554,
   COL555,
   COL556,
   COL557,
   COL558,
   COL559,
   COL560,
   COL561,
   COL562,
   COL563,
   COL564,
   COL565,
   COL566,
   COL567,
   COL568,
   COL569,
   COL570,
   COL571,
   COL572,
   COL573,
   COL574,
   COL575,
   COL576,
   COL577,
   COL578,
   COL579,
   COL580,
   COL581,
   COL582,
   COL583,
   COL584,
   COL585,
   COL586,
   COL587,
   COL588,
   COL589,
   COL590,
   COL591,
   COL592,
   COL593,
   COL594,
   COL595,
   COL596,
   COL597,
   COL598,
   COL599,
   COL600,
   COL601,
   COL602,
   COL603,
   COL604,
   COL605,
   COL606,
   COL607,
   COL608,
   COL609,
   COL610,
   COL611,
   COL612,
   COL613,
   COL614,
   COL615,
   COL616,
   COL617,
   COL618,
   COL619,
   COL620,
   COL621,
   COL622,
   COL623,
   COL624,
   COL625,
   COL626,
   COL627,
   COL628,
   COL629,
   COL630,
   COL631,
   COL632,
   COL633,
   COL634,
   COL635,
   COL636,
   COL637,
   COL638,
   COL639,
   COL640,
   COL641,
   COL642,
   COL643,
   COL644,
   COL645,
   COL646,
   COL647,
   COL648,
   COL649,
   COL650,
   COL651,
   COL652,
   COL653,
   COL654,
   COL655,
   COL656,
   COL657,
   COL658,
   COL659,
   COL660,
   COL661,
   COL662,
   COL663,
   COL664,
   COL665,
   COL666,
   COL667,
   COL668,
   COL669,
   COL670,
   COL671,
   COL672,
   COL673,
   COL674,
   COL675,
   COL676,
   COL677,
   COL678,
   COL679,
   COL680,
   COL681,
   COL682,
   COL683,
   COL684,
   COL685,
   COL686,
   COL687,
   COL688,
   COL689,
   COL690,
   COL691,
   COL692,
   COL693,
   COL694,
   COL695,
   COL696,
   COL697,
   COL698,
   COL699,
   COL700,
   COL701,
   COL702,
   COL703,
   COL704,
   COL705,
   COL706,
   COL707,
   COL708,
   COL709,
   COL710,
   COL711,
   COL712,
   COL713,
   COL714,
   COL715,
   COL716,
   COL717,
   COL718,
   COL719,
   COL720,
   COL721,
   COL722,
   COL723,
   COL724,
   COL725,
   COL726,
   COL727,
   COL728,
   COL729,
   COL730,
   COL731,
   COL732,
   COL733,
   COL734,
   COL735,
   COL736,
   COL737,
   COL738,
   COL739,
   COL740,
   COL741,
   COL742,
   COL743,
   COL744,
   COL745,
   COL746,
   COL747,
   COL748,
   COL749,
   COL750,
   COL751,
   COL752,
   COL753,
   COL754,
   COL755,
   COL756,
   COL757,
   COL758,
   COL759,
   COL760,
   COL761,
   COL762,
   COL763,
   COL764,
   COL765,
   COL766,
   COL767,
   COL768,
   COL769,
   COL770,
   COL771,
   COL772,
   COL773,
   COL774,
   COL775,
   COL776,
   COL777,
   COL778,
   COL779,
   COL780,
   COL781,
   COL782,
   COL783,
   COL784,
   COL785,
   COL786,
   COL787,
   COL788,
   COL789,
   COL790,
   COL791,
   COL792,
   COL793,
   COL794,
   COL795,
   COL796,
   COL797,
   COL798,
   COL799,
   COL800,
   COL801,
   COL802,
   COL803,
   COL804,
   COL805,
   COL806,
   COL807,
   COL808,
   COL809,
   COL810,
   COL811,
   COL812,
   COL813,
   COL814,
   COL815,
   COL816,
   COL817,
   COL818,
   COL819,
   COL820,
   COL821,
   COL822,
   COL823,
   COL824,
   COL825,
   COL826,
   COL827,
   COL828,
   COL829,
   COL830,
   COL831,
   COL832,
   COL833,
   COL834,
   COL835,
   COL836,
   COL837,
   COL838,
   COL839,
   COL840,
   COL841,
   COL842,
   COL843,
   COL844,
   COL845,
   COL846,
   COL847,
   COL848,
   COL849,
   COL850,
   COL851,
   COL852,
   COL853,
   COL854,
   COL855,
   COL856,
   COL857,
   COL858,
   COL859,
   COL860,
   COL861,
   COL862,
   COL863,
   COL864,
   COL865,
   COL866,
   COL867,
   COL868,
   COL869,
   COL870,
   COL871,
   COL872,
   COL873,
   COL874,
   COL875,
   COL876,
   COL877,
   COL878,
   COL879,
   COL880,
   COL881,
   COL882,
   COL883,
   COL884,
   COL885,
   COL886,
   COL887,
   COL888,
   COL889,
   COL890,
   COL891,
   COL892,
   COL893,
   COL894,
   COL895,
   COL896,
   COL897,
   COL898,
   COL899,
   COL900,
   COL901,
   COL902,
   COL903,
   COL904,
   COL905,
   COL906,
   COL907,
   COL908,
   COL909,
   COL910,
   COL911,
   COL912,
   COL913,
   COL914,
   COL915,
   COL916,
   COL917,
   COL918,
   COL919,
   COL920,
   COL921,
   COL922,
   COL923,
   COL924,
   COL925,
   COL926,
   COL927,
   COL928,
   COL929,
   COL930,
   COL931,
   COL932,
   COL933,
   COL934,
   COL935,
   COL936,
   COL937,
   COL938,
   COL939,
   COL940,
   COL941,
   COL942,
   COL943,
   COL944,
   COL945,
   COL946,
   COL947,
   COL948,
   COL949,
   COL950,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATED_ON,
   FK_SPECIMEN
)
AS
   SELECT   form_name,
            form_desc,
            form_version,
            fk_form,
            filldate,
            form_type,
            id AS fk_Account,
            COL1,
            COL2,
            COL3,
            COL4,
            COL5,
            COL6,
            COL7,
            COL8,
            COL9,
            COL10,
            COL11,
            COL12,
            COL13,
            COL14,
            COL15,
            COL16,
            COL17,
            COL18,
            COL19,
            COL20,
            COL21,
            COL22,
            COL23,
            COL24,
            COL25,
            COL26,
            COL27,
            COL28,
            COL29,
            COL30,
            COL31,
            COL32,
            COL33,
            COL34,
            COL35,
            COL36,
            COL37,
            COL38,
            COL39,
            COL40,
            COL41,
            COL42,
            COL43,
            COL44,
            COL45,
            COL46,
            COL47,
            COL48,
            COL49,
            COL50,
            COL51,
            COL52,
            COL53,
            COL54,
            COL55,
            COL56,
            COL57,
            COL58,
            COL59,
            COL60,
            COL61,
            COL62,
            COL63,
            COL64,
            COL65,
            COL66,
            COL67,
            COL68,
            COL69,
            COL70,
            COL71,
            COL72,
            COL73,
            COL74,
            COL75,
            COL76,
            COL77,
            COL78,
            COL79,
            COL80,
            COL81,
            COL82,
            COL83,
            COL84,
            COL85,
            COL86,
            COL87,
            COL88,
            COL89,
            COL90,
            COL91,
            COL92,
            COL93,
            COL94,
            COL95,
            COL96,
            COL97,
            COL98,
            COL99,
            COL100,
            COL101,
            COL102,
            COL103,
            COL104,
            COL105,
            COL106,
            COL107,
            COL108,
            COL109,
            COL110,
            COL111,
            COL112,
            COL113,
            COL114,
            COL115,
            COL116,
            COL117,
            COL118,
            COL119,
            COL120,
            COL121,
            COL122,
            COL123,
            COL124,
            COL125,
            COL126,
            COL127,
            COL128,
            COL129,
            COL130,
            COL131,
            COL132,
            COL133,
            COL134,
            COL135,
            COL136,
            COL137,
            COL138,
            COL139,
            COL140,
            COL141,
            COL142,
            COL143,
            COL144,
            COL145,
            COL146,
            COL147,
            COL148,
            COL149,
            COL150,
            COL151,
            COL152,
            COL153,
            COL154,
            COL155,
            COL156,
            COL157,
            COL158,
            COL159,
            COL160,
            COL161,
            COL162,
            COL163,
            COL164,
            COL165,
            COL166,
            COL167,
            COL168,
            COL169,
            COL170,
            COL171,
            COL172,
            COL173,
            COL174,
            COL175,
            COL176,
            COL177,
            COL178,
            COL179,
            COL180,
            COL181,
            COL182,
            COL183,
            COL184,
            COL185,
            COL186,
            COL187,
            COL188,
            COL189,
            COL190,
            COL191,
            COL192,
            COL193,
            COL194,
            COL195,
            COL196,
            COL197,
            COL198,
            COL199,
            COL200,
            COL201,
            COL202,
            COL203,
            COL204,
            COL205,
            COL206,
            COL207,
            COL208,
            COL209,
            COL210,
            COL211,
            COL212,
            COL213,
            COL214,
            COL215,
            COL216,
            COL217,
            COL218,
            COL219,
            COL220,
            COL221,
            COL222,
            COL223,
            COL224,
            COL225,
            COL226,
            COL227,
            COL228,
            COL229,
            COL230,
            COL231,
            COL232,
            COL233,
            COL234,
            COL235,
            COL236,
            COL237,
            COL238,
            COL239,
            COL240,
            COL241,
            COL242,
            COL243,
            COL244,
            COL245,
            COL246,
            COL247,
            COL248,
            COL249,
            COL250,
            COL251,
            COL252,
            COL253,
            COL254,
            COL255,
            COL256,
            COL257,
            COL258,
            COL259,
            COL260,
            COL261,
            COL262,
            COL263,
            COL264,
            COL265,
            COL266,
            COL267,
            COL268,
            COL269,
            COL270,
            COL271,
            COL272,
            COL273,
            COL274,
            COL275,
            COL276,
            COL277,
            COL278,
            COL279,
            COL280,
            COL281,
            COL282,
            COL283,
            COL284,
            COL285,
            COL286,
            COL287,
            COL288,
            COL289,
            COL290,
            COL291,
            COL292,
            COL293,
            COL294,
            COL295,
            COL296,
            COL297,
            COL298,
            COL299,
            COL300,
            COL301,
            COL302,
            COL303,
            COL304,
            COL305,
            COL306,
            COL307,
            COL308,
            COL309,
            COL310,
            COL311,
            COL312,
            COL313,
            COL314,
            COL315,
            COL316,
            COL317,
            COL318,
            COL319,
            COL320,
            COL321,
            COL322,
            COL323,
            COL324,
            COL325,
            COL326,
            COL327,
            COL328,
            COL329,
            COL330,
            COL331,
            COL332,
            COL333,
            COL334,
            COL335,
            COL336,
            COL337,
            COL338,
            COL339,
            COL340,
            COL341,
            COL342,
            COL343,
            COL344,
            COL345,
            COL346,
            COL347,
            COL348,
            COL349,
            COL350,
            COL351,
            COL352,
            COL353,
            COL354,
            COL355,
            COL356,
            COL357,
            COL358,
            COL359,
            COL360,
            COL361,
            COL362,
            COL363,
            COL364,
            COL365,
            COL366,
            COL367,
            COL368,
            COL369,
            COL370,
            COL371,
            COL372,
            COL373,
            COL374,
            COL375,
            COL376,
            COL377,
            COL378,
            COL379,
            COL380,
            COL381,
            COL382,
            COL383,
            COL384,
            COL385,
            COL386,
            COL387,
            COL388,
            COL389,
            COL390,
            COL391,
            COL392,
            COL393,
            COL394,
            COL395,
            COL396,
            COL397,
            COL398,
            COL399,
            COL400,
            COL401,
            COL402,
            COL403,
            COL404,
            COL405,
            COL406,
            COL407,
            COL408,
            COL409,
            COL410,
            COL411,
            COL412,
            COL413,
            COL414,
            COL415,
            COL416,
            COL417,
            COL418,
            COL419,
            COL420,
            COL421,
            COL422,
            COL423,
            COL424,
            COL425,
            COL426,
            COL427,
            COL428,
            COL429,
            COL430,
            COL431,
            COL432,
            COL433,
            COL434,
            COL435,
            COL436,
            COL437,
            COL438,
            COL439,
            COL440,
            COL441,
            COL442,
            COL443,
            COL444,
            COL445,
            COL446,
            COL447,
            COL448,
            COL449,
            COL450,
            COL451,
            COL452,
            COL453,
            COL454,
            COL455,
            COL456,
            COL457,
            COL458,
            COL459,
            COL460,
            COL461,
            COL462,
            COL463,
            COL464,
            COL465,
            COL466,
            COL467,
            COL468,
            COL469,
            COL470,
            COL471,
            COL472,
            COL473,
            COL474,
            COL475,
            COL476,
            COL477,
            COL478,
            COL479,
            COL480,
            COL481,
            COL482,
            COL483,
            COL484,
            COL485,
            COL486,
            COL487,
            COL488,
            COL489,
            COL490,
            COL491,
            COL492,
            COL493,
            COL494,
            COL495,
            COL496,
            COL497,
            COL498,
            COL499,
            COL500,
            COL501,
            COL502,
            COL503,
            COL504,
            COL505,
            COL506,
            COL507,
            COL508,
            COL509,
            COL510,
            COL511,
            COL512,
            COL513,
            COL514,
            COL515,
            COL516,
            COL517,
            COL518,
            COL519,
            COL520,
            COL521,
            COL522,
            COL523,
            COL524,
            COL525,
            COL526,
            COL527,
            COL528,
            COL529,
            COL530,
            COL531,
            COL532,
            COL533,
            COL534,
            COL535,
            COL536,
            COL537,
            COL538,
            COL539,
            COL540,
            COL541,
            COL542,
            COL543,
            COL544,
            COL545,
            COL546,
            COL547,
            COL548,
            COL549,
            COL550,
            COL551,
            COL552,
            COL553,
            COL554,
            COL555,
            COL556,
            COL557,
            COL558,
            COL559,
            COL560,
            COL561,
            COL562,
            COL563,
            COL564,
            COL565,
            COL566,
            COL567,
            COL568,
            COL569,
            COL570,
            COL571,
            COL572,
            COL573,
            COL574,
            COL575,
            COL576,
            COL577,
            COL578,
            COL579,
            COL580,
            COL581,
            COL582,
            COL583,
            COL584,
            COL585,
            COL586,
            COL587,
            COL588,
            COL589,
            COL590,
            COL591,
            COL592,
            COL593,
            COL594,
            COL595,
            COL596,
            COL597,
            COL598,
            COL599,
            COL600,
            COL601,
            COL602,
            COL603,
            COL604,
            COL605,
            COL606,
            COL607,
            COL608,
            COL609,
            COL610,
            COL611,
            COL612,
            COL613,
            COL614,
            COL615,
            COL616,
            COL617,
            COL618,
            COL619,
            COL620,
            COL621,
            COL622,
            COL623,
            COL624,
            COL625,
            COL626,
            COL627,
            COL628,
            COL629,
            COL630,
            COL631,
            COL632,
            COL633,
            COL634,
            COL635,
            COL636,
            COL637,
            COL638,
            COL639,
            COL640,
            COL641,
            COL642,
            COL643,
            COL644,
            COL645,
            COL646,
            COL647,
            COL648,
            COL649,
            COL650,
            COL651,
            COL652,
            COL653,
            COL654,
            COL655,
            COL656,
            COL657,
            COL658,
            COL659,
            COL660,
            COL661,
            COL662,
            COL663,
            COL664,
            COL665,
            COL666,
            COL667,
            COL668,
            COL669,
            COL670,
            COL671,
            COL672,
            COL673,
            COL674,
            COL675,
            COL676,
            COL677,
            COL678,
            COL679,
            COL680,
            COL681,
            COL682,
            COL683,
            COL684,
            COL685,
            COL686,
            COL687,
            COL688,
            COL689,
            COL690,
            COL691,
            COL692,
            COL693,
            COL694,
            COL695,
            COL696,
            COL697,
            COL698,
            COL699,
            COL700,
            COL701,
            COL702,
            COL703,
            COL704,
            COL705,
            COL706,
            COL707,
            COL708,
            COL709,
            COL710,
            COL711,
            COL712,
            COL713,
            COL714,
            COL715,
            COL716,
            COL717,
            COL718,
            COL719,
            COL720,
            COL721,
            COL722,
            COL723,
            COL724,
            COL725,
            COL726,
            COL727,
            COL728,
            COL729,
            COL730,
            COL731,
            COL732,
            COL733,
            COL734,
            COL735,
            COL736,
            COL737,
            COL738,
            COL739,
            COL740,
            COL741,
            COL742,
            COL743,
            COL744,
            COL745,
            COL746,
            COL747,
            COL748,
            COL749,
            COL750,
            COL751,
            COL752,
            COL753,
            COL754,
            COL755,
            COL756,
            COL757,
            COL758,
            COL759,
            COL760,
            COL761,
            COL762,
            COL763,
            COL764,
            COL765,
            COL766,
            COL767,
            COL768,
            COL769,
            COL770,
            COL771,
            COL772,
            COL773,
            COL774,
            COL775,
            COL776,
            COL777,
            COL778,
            COL779,
            COL780,
            COL781,
            COL782,
            COL783,
            COL784,
            COL785,
            COL786,
            COL787,
            COL788,
            COL789,
            COL790,
            COL791,
            COL792,
            COL793,
            COL794,
            COL795,
            COL796,
            COL797,
            COL798,
            COL799,
            COL800,
            COL801,
            COL802,
            COL803,
            COL804,
            COL805,
            COL806,
            COL807,
            COL808,
            COL809,
            COL810,
            COL811,
            COL812,
            COL813,
            COL814,
            COL815,
            COL816,
            COL817,
            COL818,
            COL819,
            COL820,
            COL821,
            COL822,
            COL823,
            COL824,
            COL825,
            COL826,
            COL827,
            COL828,
            COL829,
            COL830,
            COL831,
            COL832,
            COL833,
            COL834,
            COL835,
            COL836,
            COL837,
            COL838,
            COL839,
            COL840,
            COL841,
            COL842,
            COL843,
            COL844,
            COL845,
            COL846,
            COL847,
            COL848,
            COL849,
            COL850,
            COL851,
            COL852,
            COL853,
            COL854,
            COL855,
            COL856,
            COL857,
            COL858,
            COL859,
            COL860,
            COL861,
            COL862,
            COL863,
            COL864,
            COL865,
            COL866,
            COL867,
            COL868,
            COL869,
            COL870,
            COL871,
            COL872,
            COL873,
            COL874,
            COL875,
            COL876,
            COL877,
            COL878,
            COL879,
            COL880,
            COL881,
            COL882,
            COL883,
            COL884,
            COL885,
            COL886,
            COL887,
            COL888,
            COL889,
            COL890,
            COL891,
            COL892,
            COL893,
            COL894,
            COL895,
            COL896,
            COL897,
            COL898,
            COL899,
            COL900,
            COL901,
            COL902,
            COL903,
            COL904,
            COL905,
            COL906,
            COL907,
            COL908,
            COL909,
            COL910,
            COL911,
            COL912,
            COL913,
            COL914,
            COL915,
            COL916,
            COL917,
            COL918,
            COL919,
            COL920,
            COL921,
            COL922,
            COL923,
            COL924,
            COL925,
            COL926,
            COL927,
            COL928,
            COL929,
            COL930,
            COL931,
            COL932,
            COL933,
            COL934,
            COL935,
            COL936,
            COL937,
            COL938,
            COL939,
            COL940,
            COL941,
            COL942,
            COL943,
            COL944,
            COL945,
            COL946,
            COL947,
            COL948,
            COL949,
            COL950,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = l.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = l.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            l.LAST_MODIFIED_DATE,
            l.CREATED_ON,
            FK_SPECIMEN
     FROM   eres.er_formslinear l, eres.er_formlib
    WHERE   form_type = 'A' AND fk_form = pk_formlib
/
COMMENT ON TABLE VDA_V_ACCOUNTFORMRESPONSES IS 'This view provides access to responses to all account level forms'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL877 IS 'Col877 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL878 IS 'Col878 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL879 IS 'Col879 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL880 IS 'Col880 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL881 IS 'Col881 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL882 IS 'Col882 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL883 IS 'Col883 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL884 IS 'Col884 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL885 IS 'Col885 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL886 IS 'Col886 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL887 IS 'Col887 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL888 IS 'Col888 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL889 IS 'Col889 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL890 IS 'Col890 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL891 IS 'Col891 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL892 IS 'Col892 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL893 IS 'Col893 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL894 IS 'Col894 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL895 IS 'Col895 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL896 IS 'Col896 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL897 IS 'Col897 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL898 IS 'Col898 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL899 IS 'Col899 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL900 IS 'Col900 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL901 IS 'Col901 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL902 IS 'Col902 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL903 IS 'Col903 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL904 IS 'Col904 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL905 IS 'Col905 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL906 IS 'Col906 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL907 IS 'Col907 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL908 IS 'Col908 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL909 IS 'Col909 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL910 IS 'Col910 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL911 IS 'Col911 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL912 IS 'Col912 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL913 IS 'Col913 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL914 IS 'Col914 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL915 IS 'Col915 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL916 IS 'Col916 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL917 IS 'Col917 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL918 IS 'Col918 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL919 IS 'Col919 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL920 IS 'Col920 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL921 IS 'Col921 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL922 IS 'Col922 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL923 IS 'Col923 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL924 IS 'Col924 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL925 IS 'Col925 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL926 IS 'Col926 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL927 IS 'Col927 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL928 IS 'Col928 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL929 IS 'Col929 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL930 IS 'Col930 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL931 IS 'Col931 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL932 IS 'Col932 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL933 IS 'Col933 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL934 IS 'Col934 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL935 IS 'Col935 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL936 IS 'Col936 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL937 IS 'Col937 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL938 IS 'Col938 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL939 IS 'Col939 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL940 IS 'Col940 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL941 IS 'Col941 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL942 IS 'Col942 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL943 IS 'Col943 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL944 IS 'Col944 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL945 IS 'Col945 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL946 IS 'Col946 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL947 IS 'Col947 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL948 IS 'Col948 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL949 IS 'Col949 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL950 IS 'Col950 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.CREATOR IS 'User who created record'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.LAST_MODIFIED_BY IS 'User who last modified the record'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.LAST_MODIFIED_DATE IS 'Last modified date'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.CREATED_ON IS 'Date record was created on'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.FK_SPECIMEN IS 'ID to identify the specimen a response is linked with'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL602 IS 'Col602 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL603 IS 'Col603 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL604 IS 'Col604 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL605 IS 'Col605 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL606 IS 'Col606 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL607 IS 'Col607 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL608 IS 'Col608 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL609 IS 'Col609 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL610 IS 'Col610 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL611 IS 'Col611 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL612 IS 'Col612 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL613 IS 'Col613 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL614 IS 'Col614 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL615 IS 'Col615 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL616 IS 'Col616 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL617 IS 'Col617 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL618 IS 'Col618 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL619 IS 'Col619 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL620 IS 'Col620 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL621 IS 'Col621 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL622 IS 'Col622 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL623 IS 'Col623 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL624 IS 'Col624 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL625 IS 'Col625 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL626 IS 'Col626 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL627 IS 'Col627 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL628 IS 'Col628 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL629 IS 'Col629 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL630 IS 'Col630 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL631 IS 'Col631 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL632 IS 'Col632 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL633 IS 'Col633 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL634 IS 'Col634 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL635 IS 'Col635 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL636 IS 'Col636 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL637 IS 'Col637 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL638 IS 'Col638 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL639 IS 'Col639 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL640 IS 'Col640 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL641 IS 'Col641 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL642 IS 'Col642 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL643 IS 'Col643 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL644 IS 'Col644 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL645 IS 'Col645 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL646 IS 'Col646 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL647 IS 'Col647 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL648 IS 'Col648 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL649 IS 'Col649 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL650 IS 'Col650 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL651 IS 'Col651 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL652 IS 'Col652 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL653 IS 'Col653 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL654 IS 'Col654 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL655 IS 'Col655 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL656 IS 'Col656 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL657 IS 'Col657 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL658 IS 'Col658 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL659 IS 'Col659 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL660 IS 'Col660 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL661 IS 'Col661 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL662 IS 'Col662 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL663 IS 'Col663 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL664 IS 'Col664 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL665 IS 'Col665 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL666 IS 'Col666 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL667 IS 'Col667 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL668 IS 'Col668 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL669 IS 'Col669 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL670 IS 'Col670 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL671 IS 'Col671 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL672 IS 'Col672 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL673 IS 'Col673 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL674 IS 'Col674 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL675 IS 'Col675 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL676 IS 'Col676 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL677 IS 'Col677 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL678 IS 'Col678 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL679 IS 'Col679 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL680 IS 'Col680 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL681 IS 'Col681 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL682 IS 'Col682 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL683 IS 'Col683 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL684 IS 'Col684 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL685 IS 'Col685 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL686 IS 'Col686 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL687 IS 'Col687 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL688 IS 'Col688 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL689 IS 'Col689 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL690 IS 'Col690 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL691 IS 'Col691 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL692 IS 'Col692 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL693 IS 'Col693 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL694 IS 'Col694 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL695 IS 'Col695 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL696 IS 'Col696 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL697 IS 'Col697 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL698 IS 'Col698 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL699 IS 'Col699 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL700 IS 'Col700 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL701 IS 'Col701 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL702 IS 'Col702 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL703 IS 'Col703 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL704 IS 'Col704 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL705 IS 'Col705 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL706 IS 'Col706 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL707 IS 'Col707 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL708 IS 'Col708 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL709 IS 'Col709 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL710 IS 'Col710 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL711 IS 'Col711 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL712 IS 'Col712 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL713 IS 'Col713 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL714 IS 'Col714 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL715 IS 'Col715 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL716 IS 'Col716 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL717 IS 'Col717 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL718 IS 'Col718 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL719 IS 'Col719 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL720 IS 'Col720 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL721 IS 'Col721 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL722 IS 'Col722 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL723 IS 'Col723 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL724 IS 'Col724 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL725 IS 'Col725 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL726 IS 'Col726 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL727 IS 'Col727 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL728 IS 'Col728 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL729 IS 'Col729 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL730 IS 'Col730 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL731 IS 'Col731 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL732 IS 'Col732 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL733 IS 'Col733 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL734 IS 'Col734 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL735 IS 'Col735 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL736 IS 'Col736 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL737 IS 'Col737 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL738 IS 'Col738 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL739 IS 'Col739 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL740 IS 'Col740 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL741 IS 'Col741 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL742 IS 'Col742 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL743 IS 'Col743 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL744 IS 'Col744 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL745 IS 'Col745 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL746 IS 'Col746 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL747 IS 'Col747 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL748 IS 'Col748 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL749 IS 'Col749 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL750 IS 'Col750 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL751 IS 'Col751 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL752 IS 'Col752 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL753 IS 'Col753 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL754 IS 'Col754 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL755 IS 'Col755 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL756 IS 'Col756 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL757 IS 'Col757 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL758 IS 'Col758 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL759 IS 'Col759 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL760 IS 'Col760 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL761 IS 'Col761 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL762 IS 'Col762 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL763 IS 'Col763 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL764 IS 'Col764 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL765 IS 'Col765 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL766 IS 'Col766 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL767 IS 'Col767 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL768 IS 'Col768 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL769 IS 'Col769 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL770 IS 'Col770 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL771 IS 'Col771 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL772 IS 'Col772 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL773 IS 'Col773 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL774 IS 'Col774 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL775 IS 'Col775 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL776 IS 'Col776 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL777 IS 'Col777 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL778 IS 'Col778 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL779 IS 'Col779 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL780 IS 'Col780 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL781 IS 'Col781 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL782 IS 'Col782 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL783 IS 'Col783 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL784 IS 'Col784 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL785 IS 'Col785 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL786 IS 'Col786 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL787 IS 'Col787 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL788 IS 'Col788 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL789 IS 'Col789 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL790 IS 'Col790 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL791 IS 'Col791 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL792 IS 'Col792 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL793 IS 'Col793 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL794 IS 'Col794 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL795 IS 'Col795 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL796 IS 'Col796 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL797 IS 'Col797 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL798 IS 'Col798 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL799 IS 'Col799 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL800 IS 'Col800 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL801 IS 'Col801 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL802 IS 'Col802 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL803 IS 'Col803 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL804 IS 'Col804 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL805 IS 'Col805 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL806 IS 'Col806 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL807 IS 'Col807 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL808 IS 'Col808 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL809 IS 'Col809 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL810 IS 'Col810 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL811 IS 'Col811 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL812 IS 'Col812 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL813 IS 'Col813 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL814 IS 'Col814 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL815 IS 'Col815 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL816 IS 'Col816 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL817 IS 'Col817 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL818 IS 'Col818 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL819 IS 'Col819 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL820 IS 'Col820 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL821 IS 'Col821 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL822 IS 'Col822 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL823 IS 'Col823 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL824 IS 'Col824 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL825 IS 'Col825 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL826 IS 'Col826 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL827 IS 'Col827 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL828 IS 'Col828 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL829 IS 'Col829 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL830 IS 'Col830 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL831 IS 'Col831 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL832 IS 'Col832 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL833 IS 'Col833 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL834 IS 'Col834 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL835 IS 'Col835 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL836 IS 'Col836 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL837 IS 'Col837 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL838 IS 'Col838 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL839 IS 'Col839 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL840 IS 'Col840 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL841 IS 'Col841 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL842 IS 'Col842 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL843 IS 'Col843 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL844 IS 'Col844 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL845 IS 'Col845 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL846 IS 'Col846 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL847 IS 'Col847 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL848 IS 'Col848 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL849 IS 'Col849 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL850 IS 'Col850 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL851 IS 'Col851 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL852 IS 'Col852 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL853 IS 'Col853 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL854 IS 'Col854 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL855 IS 'Col855 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL856 IS 'Col856 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL857 IS 'Col857 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL858 IS 'Col858 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL859 IS 'Col859 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL860 IS 'Col860 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL861 IS 'Col861 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL862 IS 'Col862 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL863 IS 'Col863 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.FORM_NAME IS 'Form name'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.FORM_DESC IS 'Form Description'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.FORM_VERSION IS 'Form version number'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.FK_FORM IS 'ID to identify a form record uniquely'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.FILLDATE IS 'Date the response was filled on'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.FORM_TYPE IS 'Type of the  form- always A for account level forms'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.FK_ACCOUNT IS 'The account the response is linked with. Applicable to Velos hosted or shared environments'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL1 IS 'Col1 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL2 IS 'Col2 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL3 IS 'Col3 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL4 IS 'Col4 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL5 IS 'Col5 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL6 IS 'Col6 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL7 IS 'Col7 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL8 IS 'Col8 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL9 IS 'Col9 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL10 IS 'Col10 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL11 IS 'Col11 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL12 IS 'Col12 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL13 IS 'Col13 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL14 IS 'Col14 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL15 IS 'Col15 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL16 IS 'Col16 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL17 IS 'Col17 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL18 IS 'Col18 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL19 IS 'Col19 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL20 IS 'Col20 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL21 IS 'Col21 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL22 IS 'Col22 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL23 IS 'Col23 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL24 IS 'Col24 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL25 IS 'Col25 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL26 IS 'Col26 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL27 IS 'Col27 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL28 IS 'Col28 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL29 IS 'Col29 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL30 IS 'Col30 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL31 IS 'Col31 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL32 IS 'Col32 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL33 IS 'Col33 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL34 IS 'Col34 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL35 IS 'Col35 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL36 IS 'Col36 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL37 IS 'Col37 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL38 IS 'Col38 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL39 IS 'Col39 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL40 IS 'Col40 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL41 IS 'Col41 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL42 IS 'Col42 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL43 IS 'Col43 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL44 IS 'Col44 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL45 IS 'Col45 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL46 IS 'Col46 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL47 IS 'Col47 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL48 IS 'Col48 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL49 IS 'Col49 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL50 IS 'Col50 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL51 IS 'Col51 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL52 IS 'Col52 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL53 IS 'Col53 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL54 IS 'Col54 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL55 IS 'Col55 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL56 IS 'Col56 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL57 IS 'Col57 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL58 IS 'Col58 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL59 IS 'Col59 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL60 IS 'Col60 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL61 IS 'Col61 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL62 IS 'Col62 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL63 IS 'Col63 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL64 IS 'Col64 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL65 IS 'Col65 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL66 IS 'Col66 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL67 IS 'Col67 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL68 IS 'Col68 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL69 IS 'Col69 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL70 IS 'Col70 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL71 IS 'Col71 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL72 IS 'Col72 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL73 IS 'Col73 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL74 IS 'Col74 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL75 IS 'Col75 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL76 IS 'Col76 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL77 IS 'Col77 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL78 IS 'Col78 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL79 IS 'Col79 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL80 IS 'Col80 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL81 IS 'Col81 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL82 IS 'Col82 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL83 IS 'Col83 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL84 IS 'Col84 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL85 IS 'Col85 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL86 IS 'Col86 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL87 IS 'Col87 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL88 IS 'Col88 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL89 IS 'Col89 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL90 IS 'Col90 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL91 IS 'Col91 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL92 IS 'Col92 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL93 IS 'Col93 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL94 IS 'Col94 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL95 IS 'Col95 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL96 IS 'Col96 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL97 IS 'Col97 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL98 IS 'Col98 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL99 IS 'Col99 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL100 IS 'Col100 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL101 IS 'Col101 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL102 IS 'Col102 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL103 IS 'Col103 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL104 IS 'Col104 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL105 IS 'Col105 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL106 IS 'Col106 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL107 IS 'Col107 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL108 IS 'Col108 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL109 IS 'Col109 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL110 IS 'Col110 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL111 IS 'Col111 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL112 IS 'Col112 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL113 IS 'Col113 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL114 IS 'Col114 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL115 IS 'Col115 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL116 IS 'Col116 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL117 IS 'Col117 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL118 IS 'Col118 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL119 IS 'Col119 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL120 IS 'Col120 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL121 IS 'Col121 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL122 IS 'Col122 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL123 IS 'Col123 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL124 IS 'Col124 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL125 IS 'Col125 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL126 IS 'Col126 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL127 IS 'Col127 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL128 IS 'Col128 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL129 IS 'Col129 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL130 IS 'Col130 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL131 IS 'Col131 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL132 IS 'Col132 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL133 IS 'Col133 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL134 IS 'Col134 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL135 IS 'Col135 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL136 IS 'Col136 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL137 IS 'Col137 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL138 IS 'Col138 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL139 IS 'Col139 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL140 IS 'Col140 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL141 IS 'Col141 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL142 IS 'Col142 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL143 IS 'Col143 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL144 IS 'Col144 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL145 IS 'Col145 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL146 IS 'Col146 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL147 IS 'Col147 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL148 IS 'Col148 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL149 IS 'Col149 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL150 IS 'Col150 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL151 IS 'Col151 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL152 IS 'Col152 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL153 IS 'Col153 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL154 IS 'Col154 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL155 IS 'Col155 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL156 IS 'Col156 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL157 IS 'Col157 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL158 IS 'Col158 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL159 IS 'Col159 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL160 IS 'Col160 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL161 IS 'Col161 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL162 IS 'Col162 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL163 IS 'Col163 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL164 IS 'Col164 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL165 IS 'Col165 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL166 IS 'Col166 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL167 IS 'Col167 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL168 IS 'Col168 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL169 IS 'Col169 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL170 IS 'Col170 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL171 IS 'Col171 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL172 IS 'Col172 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL173 IS 'Col173 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL174 IS 'Col174 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL175 IS 'Col175 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL176 IS 'Col176 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL177 IS 'Col177 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL178 IS 'Col178 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL179 IS 'Col179 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL180 IS 'Col180 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL181 IS 'Col181 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL182 IS 'Col182 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL183 IS 'Col183 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL184 IS 'Col184 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL185 IS 'Col185 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL186 IS 'Col186 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL187 IS 'Col187 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL188 IS 'Col188 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL189 IS 'Col189 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL190 IS 'Col190 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL191 IS 'Col191 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL192 IS 'Col192 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL193 IS 'Col193 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL194 IS 'Col194 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL195 IS 'Col195 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL196 IS 'Col196 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL197 IS 'Col197 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL198 IS 'Col198 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL199 IS 'Col199 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL200 IS 'Col200 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL201 IS 'Col201 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL202 IS 'Col202 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL203 IS 'Col203 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL204 IS 'Col204 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL205 IS 'Col205 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL206 IS 'Col206 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL207 IS 'Col207 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL208 IS 'Col208 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL209 IS 'Col209 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL210 IS 'Col210 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL211 IS 'Col211 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL212 IS 'Col212 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL213 IS 'Col213 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL214 IS 'Col214 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL215 IS 'Col215 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL216 IS 'Col216 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL217 IS 'Col217 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL218 IS 'Col218 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL219 IS 'Col219 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL220 IS 'Col220 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL221 IS 'Col221 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL222 IS 'Col222 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL223 IS 'Col223 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL224 IS 'Col224 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL225 IS 'Col225 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL226 IS 'Col226 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL227 IS 'Col227 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL228 IS 'Col228 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL229 IS 'Col229 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL230 IS 'Col230 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL231 IS 'Col231 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL232 IS 'Col232 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL233 IS 'Col233 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL234 IS 'Col234 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL235 IS 'Col235 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL236 IS 'Col236 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL237 IS 'Col237 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL238 IS 'Col238 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL239 IS 'Col239 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL240 IS 'Col240 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL241 IS 'Col241 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL242 IS 'Col242 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL243 IS 'Col243 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL244 IS 'Col244 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL245 IS 'Col245 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL246 IS 'Col246 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL247 IS 'Col247 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL248 IS 'Col248 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL249 IS 'Col249 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL250 IS 'Col250 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL251 IS 'Col251 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL252 IS 'Col252 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL253 IS 'Col253 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL254 IS 'Col254 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL255 IS 'Col255 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL256 IS 'Col256 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL257 IS 'Col257 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL258 IS 'Col258 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL259 IS 'Col259 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL864 IS 'Col864 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL865 IS 'Col865 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL866 IS 'Col866 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL867 IS 'Col867 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL868 IS 'Col868 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL869 IS 'Col869 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL870 IS 'Col870 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL871 IS 'Col871 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL872 IS 'Col872 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL873 IS 'Col873 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL874 IS 'Col874 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL875 IS 'Col875 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL876 IS 'Col876 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL260 IS 'Col260 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL261 IS 'Col261 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL262 IS 'Col262 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL263 IS 'Col263 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL264 IS 'Col264 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL265 IS 'Col265 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL266 IS 'Col266 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL267 IS 'Col267 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL268 IS 'Col268 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL269 IS 'Col269 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL270 IS 'Col270 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL271 IS 'Col271 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL272 IS 'Col272 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL273 IS 'Col273 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL274 IS 'Col274 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL275 IS 'Col275 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL276 IS 'Col276 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL277 IS 'Col277 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL278 IS 'Col278 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL279 IS 'Col279 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL280 IS 'Col280 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL281 IS 'Col281 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL282 IS 'Col282 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL283 IS 'Col283 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL284 IS 'Col284 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL285 IS 'Col285 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL286 IS 'Col286 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL287 IS 'Col287 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL288 IS 'Col288 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL289 IS 'Col289 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL290 IS 'Col290 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL291 IS 'Col291 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL292 IS 'Col292 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL293 IS 'Col293 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL294 IS 'Col294 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL295 IS 'Col295 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL296 IS 'Col296 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL297 IS 'Col297 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL298 IS 'Col298 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL299 IS 'Col299 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL300 IS 'Col300 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL301 IS 'Col301 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL302 IS 'Col302 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL303 IS 'Col303 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL304 IS 'Col304 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL305 IS 'Col305 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL306 IS 'Col306 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL307 IS 'Col307 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL308 IS 'Col308 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL309 IS 'Col309 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL310 IS 'Col310 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL311 IS 'Col311 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL312 IS 'Col312 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL313 IS 'Col313 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL314 IS 'Col314 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL315 IS 'Col315 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL316 IS 'Col316 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL317 IS 'Col317 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL318 IS 'Col318 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL319 IS 'Col319 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL320 IS 'Col320 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL321 IS 'Col321 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL322 IS 'Col322 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL323 IS 'Col323 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL324 IS 'Col324 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL325 IS 'Col325 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL326 IS 'Col326 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL327 IS 'Col327 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL328 IS 'Col328 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL329 IS 'Col329 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL330 IS 'Col330 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL331 IS 'Col331 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL332 IS 'Col332 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL333 IS 'Col333 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL334 IS 'Col334 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL335 IS 'Col335 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL336 IS 'Col336 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL337 IS 'Col337 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL338 IS 'Col338 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL339 IS 'Col339 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL340 IS 'Col340 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL341 IS 'Col341 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL342 IS 'Col342 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL343 IS 'Col343 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL344 IS 'Col344 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL345 IS 'Col345 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL346 IS 'Col346 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL347 IS 'Col347 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL348 IS 'Col348 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL349 IS 'Col349 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL350 IS 'Col350 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL351 IS 'Col351 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL352 IS 'Col352 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL353 IS 'Col353 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL354 IS 'Col354 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL355 IS 'Col355 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL356 IS 'Col356 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL357 IS 'Col357 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL358 IS 'Col358 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL359 IS 'Col359 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL360 IS 'Col360 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL361 IS 'Col361 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL362 IS 'Col362 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL363 IS 'Col363 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL364 IS 'Col364 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL365 IS 'Col365 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL366 IS 'Col366 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL367 IS 'Col367 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL368 IS 'Col368 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL369 IS 'Col369 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL370 IS 'Col370 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL371 IS 'Col371 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL372 IS 'Col372 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL373 IS 'Col373 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL374 IS 'Col374 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL375 IS 'Col375 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL376 IS 'Col376 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL377 IS 'Col377 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL378 IS 'Col378 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL379 IS 'Col379 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL380 IS 'Col380 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL381 IS 'Col381 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL382 IS 'Col382 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL383 IS 'Col383 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL384 IS 'Col384 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL385 IS 'Col385 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL386 IS 'Col386 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL387 IS 'Col387 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL388 IS 'Col388 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL389 IS 'Col389 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL390 IS 'Col390 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL391 IS 'Col391 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL392 IS 'Col392 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL393 IS 'Col393 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL394 IS 'Col394 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL395 IS 'Col395 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL396 IS 'Col396 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL397 IS 'Col397 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL398 IS 'Col398 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL399 IS 'Col399 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL400 IS 'Col400 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL401 IS 'Col401 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL402 IS 'Col402 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL403 IS 'Col403 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL404 IS 'Col404 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL405 IS 'Col405 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL406 IS 'Col406 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL407 IS 'Col407 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL408 IS 'Col408 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL409 IS 'Col409 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL410 IS 'Col410 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL411 IS 'Col411 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL412 IS 'Col412 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL413 IS 'Col413 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL414 IS 'Col414 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL415 IS 'Col415 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL416 IS 'Col416 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL417 IS 'Col417 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL418 IS 'Col418 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL419 IS 'Col419 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL420 IS 'Col420 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL421 IS 'Col421 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL422 IS 'Col422 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL423 IS 'Col423 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL424 IS 'Col424 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL425 IS 'Col425 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL426 IS 'Col426 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL427 IS 'Col427 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL428 IS 'Col428 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL429 IS 'Col429 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL430 IS 'Col430 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL431 IS 'Col431 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL432 IS 'Col432 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL433 IS 'Col433 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL434 IS 'Col434 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL435 IS 'Col435 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL436 IS 'Col436 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL437 IS 'Col437 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL438 IS 'Col438 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL439 IS 'Col439 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL440 IS 'Col440 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL441 IS 'Col441 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL442 IS 'Col442 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL443 IS 'Col443 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL444 IS 'Col444 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL445 IS 'Col445 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL446 IS 'Col446 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL447 IS 'Col447 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL448 IS 'Col448 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL449 IS 'Col449 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL450 IS 'Col450 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL451 IS 'Col451 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL452 IS 'Col452 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL453 IS 'Col453 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL454 IS 'Col454 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL455 IS 'Col455 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL456 IS 'Col456 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL457 IS 'Col457 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL458 IS 'Col458 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL459 IS 'Col459 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL460 IS 'Col460 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL461 IS 'Col461 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL462 IS 'Col462 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL463 IS 'Col463 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL464 IS 'Col464 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL465 IS 'Col465 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL466 IS 'Col466 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL467 IS 'Col467 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL468 IS 'Col468 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL469 IS 'Col469 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL470 IS 'Col470 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL471 IS 'Col471 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL472 IS 'Col472 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL473 IS 'Col473 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL474 IS 'Col474 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL475 IS 'Col475 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL476 IS 'Col476 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL477 IS 'Col477 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL478 IS 'Col478 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL479 IS 'Col479 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL480 IS 'Col480 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL481 IS 'Col481 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL482 IS 'Col482 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL483 IS 'Col483 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL484 IS 'Col484 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL485 IS 'Col485 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL486 IS 'Col486 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL487 IS 'Col487 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL488 IS 'Col488 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL489 IS 'Col489 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL490 IS 'Col490 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL491 IS 'Col491 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL492 IS 'Col492 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL493 IS 'Col493 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL494 IS 'Col494 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL495 IS 'Col495 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL496 IS 'Col496 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL497 IS 'Col497 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL498 IS 'Col498 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL499 IS 'Col499 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL500 IS 'Col500 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL501 IS 'Col501 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL502 IS 'Col502 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL503 IS 'Col503 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL504 IS 'Col504 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL505 IS 'Col505 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL506 IS 'Col506 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL507 IS 'Col507 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL508 IS 'Col508 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL509 IS 'Col509 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL510 IS 'Col510 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL511 IS 'Col511 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL512 IS 'Col512 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL513 IS 'Col513 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL514 IS 'Col514 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL515 IS 'Col515 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL516 IS 'Col516 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL517 IS 'Col517 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL518 IS 'Col518 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL519 IS 'Col519 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL520 IS 'Col520 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL521 IS 'Col521 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL522 IS 'Col522 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL523 IS 'Col523 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL524 IS 'Col524 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL525 IS 'Col525 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL526 IS 'Col526 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL527 IS 'Col527 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL528 IS 'Col528 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL529 IS 'Col529 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL530 IS 'Col530 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL531 IS 'Col531 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL532 IS 'Col532 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL533 IS 'Col533 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL534 IS 'Col534 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL535 IS 'Col535 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL536 IS 'Col536 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL537 IS 'Col537 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL538 IS 'Col538 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL539 IS 'Col539 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL540 IS 'Col540 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL541 IS 'Col541 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL542 IS 'Col542 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL543 IS 'Col543 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL544 IS 'Col544 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL545 IS 'Col545 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL546 IS 'Col546 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL547 IS 'Col547 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL548 IS 'Col548 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL549 IS 'Col549 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL550 IS 'Col550 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL551 IS 'Col551 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL552 IS 'Col552 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL553 IS 'Col553 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL554 IS 'Col554 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL555 IS 'Col555 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL556 IS 'Col556 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL557 IS 'Col557 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL558 IS 'Col558 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL559 IS 'Col559 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL560 IS 'Col560 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL561 IS 'Col561 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL562 IS 'Col562 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL563 IS 'Col563 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL564 IS 'Col564 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL565 IS 'Col565 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL566 IS 'Col566 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL567 IS 'Col567 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL568 IS 'Col568 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL569 IS 'Col569 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL570 IS 'Col570 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL571 IS 'Col571 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL572 IS 'Col572 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL573 IS 'Col573 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL574 IS 'Col574 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL575 IS 'Col575 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL576 IS 'Col576 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL577 IS 'Col577 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL578 IS 'Col578 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL579 IS 'Col579 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL580 IS 'Col580 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL581 IS 'Col581 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL582 IS 'Col582 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL583 IS 'Col583 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL584 IS 'Col584 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL585 IS 'Col585 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL586 IS 'Col586 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL587 IS 'Col587 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL588 IS 'Col588 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL589 IS 'Col589 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL590 IS 'Col590 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL591 IS 'Col591 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL592 IS 'Col592 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL593 IS 'Col593 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL594 IS 'Col594 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL595 IS 'Col595 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL596 IS 'Col596 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL597 IS 'Col597 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL598 IS 'Col598 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL599 IS 'Col599 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL600 IS 'Col600 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL601 IS 'Col601 data of the form response. The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column for the respective form'
/


