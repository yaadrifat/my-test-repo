/* Formatted on 3/11/2014 3:19:47 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_STUDYCAL_RESOURCE_BYPAT  (View)
--
--  Dependencies:
--   F_GET_DURATION (Function)
--   F_GET_DURUNIT (Function)
--   VDA_V_STUDY_CALENDAR (View)
--   VDA_V_PAT_SCHEDULE (View)
--   VDA_V_STUDYCAL_RESOURCE (View)
--

CREATE OR REPLACE FORCE VIEW VDA_V_STUDYCAL_RESOURCE_BYPAT
(
   VISIT_NAME,
   EVREC_EVENT,
   EVREC_USER,
   EVREC_RESOURCE,
   EVREC_DURATION,
   EVREC_NOTES,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATED_ON,
   CALENDAR_TYPE,
   CALENDAR_NAME,
   CALENDAR_DESC,
   CALENDAR_STAT,
   CALENDAR_DURATION,
   CALENDAR_PK,
   STUDY_NUMBER,
   DURATION_MINUTES,
   PAT_STUDY_ID,
   PTSCH_ACTSCH_DATE,
   PTSCH_EVENT_STAT,
   ENROLLING_SITE_NAME,
   EVENT_PK
)
AS
   SELECT   r.visit_name,
            r.EVREC_EVENT,
            r.EVREC_USER,
            r.EVREC_RESOURCE,
            r.EVREC_DURATION,
            r.EVREC_NOTES,
            r.CREATOR,
            r.LAST_MODIFIED_BY,
            r.LAST_MODIFIED_DATE,
            r.CREATED_ON,
            c.CALENDAR_TYPE,
            c.CALENDAR_NAME,
            c.CALENDAR_DESC,
            c.CALENDAR_STAT,
            c.CALENDAR_DURATION,
            c.CALENDAR_PK,
            c.study_number,
            DECODE (
               EVREC_DURATION,
               NULL,
               0,
               (  (TO_NUMBER (SUBSTR (EVREC_DURATION, 0, 2)) * 24 * 60)
                + (TO_NUMBER (SUBSTR (EVREC_DURATION, 4, 2)) * 60)
                + TO_NUMBER (SUBSTR (EVREC_DURATION, 7, 2))
                + ROUND ( (TO_NUMBER (SUBSTR (EVREC_DURATION, 10, 2)) / 60)))
            )
               duration_minutes,
            s.PAT_STUDY_ID,
            s.PTSCH_ACTSCH_DATE,
            s.PTSCH_EVENT_STAT,
            s.enrolling_site_name,
            r.EVENT_PK
     FROM   VDA.VDA_V_STUDYCAL_RESOURCE r,
            VDA.VDA_V_STUDY_CALENDAR c,
            VDA.VDA_V_PAT_SCHEDULE s
    WHERE   r.CALENDAR_PK = c.CALENDAR_PK AND s.PTSCH_EVENT_PK = r.EVENT_PK
/
COMMENT ON TABLE VDA_V_STUDYCAL_RESOURCE_BYPAT IS 'This view provides access to the study caledar resource information for each patient schedule event'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.VISIT_NAME IS 'The name of the visit'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.EVREC_EVENT IS 'The name of the event'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.EVREC_USER IS 'The user linked as a resource'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.EVREC_RESOURCE IS 'The role type linked as a resource'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.EVREC_DURATION IS 'The duration the resource is needed for '
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.EVREC_NOTES IS 'The notes linked with the event resource'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.LAST_MODIFIED_DATE IS 'The date the  record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.CREATED_ON IS 'The date the record was created on (Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.CALENDAR_TYPE IS 'The Calendar Type'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.CALENDAR_NAME IS 'The name of the Calendar'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.CALENDAR_DESC IS 'The Calendar description'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.CALENDAR_STAT IS 'The Calendar Status'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.CALENDAR_DURATION IS 'The Calendar duration'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.CALENDAR_PK IS 'The Primary Key of the Calendar'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.STUDY_NUMBER IS 'The study number of the study the calendar is linked with'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.DURATION_MINUTES IS 'The resource duration converted in minutes'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.PAT_STUDY_ID IS 'The patient study ID'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.PTSCH_ACTSCH_DATE IS 'The actual schedule date of the event'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.PTSCH_EVENT_STAT IS 'The patient schedule event status'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.ENROLLING_SITE_NAME IS 'The enrolling site name'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.EVENT_PK IS 'The Primary Key of the event'
/


