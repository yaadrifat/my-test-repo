/* Formatted on 3/11/2014 3:20:06 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_USERSESSIONLOG  (View)
--
--  Dependencies:
--   ER_USER (Table)
--   ER_USERSESSION (Table)
--   ER_USERSESSIONLOG (Table)
--

CREATE OR REPLACE FORCE VIEW VDA_V_USERSESSIONLOG
(
   SESSION_ID,
   LOGIN_TIME,
   LOGOUT_TIME,
   IP_ADD,
   SUCCESS_FLAG,
   USER_NAME,
   ACCESSURL,
   ACCESSTIME
)
AS
   SELECT   pk_usersession AS session_id,
            login_time,
            logout_time,
            ip_add,
            success_flag,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   eres.er_user
              WHERE   pk_user = fk_user)
               user_name,
            usl_url accessurl,
            usl_accesstime accesstime
     FROM   eres.er_usersession, eres.er_usersessionlog l
    WHERE   l.fk_usersession = pk_usersession
/
COMMENT ON TABLE VDA_V_USERSESSIONLOG IS 'This view provides access to the details of the user sessions'
/

COMMENT ON COLUMN VDA_V_USERSESSIONLOG.SESSION_ID IS 'The identifier of the session'
/

COMMENT ON COLUMN VDA_V_USERSESSIONLOG.LOGIN_TIME IS 'The recorded login time for this session'
/

COMMENT ON COLUMN VDA_V_USERSESSIONLOG.LOGOUT_TIME IS 'The recorded logout time for this session'
/

COMMENT ON COLUMN VDA_V_USERSESSIONLOG.IP_ADD IS 'The IP address of the client machine that initiated the session'
/

COMMENT ON COLUMN VDA_V_USERSESSIONLOG.SUCCESS_FLAG IS 'The success flag of the session'
/

COMMENT ON COLUMN VDA_V_USERSESSIONLOG.USER_NAME IS 'The name of the user who initiated the session'
/

COMMENT ON COLUMN VDA_V_USERSESSIONLOG.ACCESSURL IS 'The application URL or page visited'
/

COMMENT ON COLUMN VDA_V_USERSESSIONLOG.ACCESSTIME IS 'The time of the URL or page access'
/


