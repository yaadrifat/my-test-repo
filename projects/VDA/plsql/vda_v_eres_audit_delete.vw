/* Formatted on 3/11/2014 3:18:49 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_ERES_AUDIT_DELETE  (View)
--
--  Dependencies:
--   AUDIT_DELETE (Table)
--   AUDIT_ROW (Table)
--

CREATE OR REPLACE FORCE VIEW VDA_V_ERES_AUDIT_DELETE
(
   USER_NAME,
   TIMESTAMP,
   TABLE_NAME,
   ROW_DATA
)
AS
   SELECT   r.user_name,
            r.timestamp,
            r.table_name,
            c.row_data
     FROM   eres.audit_delete c, eres.audit_row r
    WHERE   r.action = 'D' AND c.raid = r.raid
/
COMMENT ON TABLE VDA_V_ERES_AUDIT_DELETE IS 'This view provides access to the detail audit trail on Delete actions in ERES schema'
/

COMMENT ON COLUMN VDA_V_ERES_AUDIT_DELETE.USER_NAME IS 'The user who triggered the action'
/

COMMENT ON COLUMN VDA_V_ERES_AUDIT_DELETE.TIMESTAMP IS 'The date and time of the action'
/

COMMENT ON COLUMN VDA_V_ERES_AUDIT_DELETE.TABLE_NAME IS 'The name of the table'
/

COMMENT ON COLUMN VDA_V_ERES_AUDIT_DELETE.ROW_DATA IS 'The pipe delimited data at the time of delete, The order of the column is same as the order of the columns in the table'
/


