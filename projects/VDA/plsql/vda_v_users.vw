/* Formatted on 3/11/2014 3:20:03 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_USERS  (View)
--
--  Dependencies:
--   F_GET_CODELSTDESC (Function)
--   F_GET_USRTYPE (Function)
--   ER_GRPS (Table)
--   SCH_TIMEZONES (Table)
--   ER_ADD (Table)
--   ER_USER (Table)
--   ER_SITE (Table)
--

CREATE OR REPLACE FORCE VIEW VDA_V_USERS
(
   USER_TYPE,
   USER_DEFAULT_GROUP_FK,
   SITE_NAME,
   USER_NAME,
   USER_JOBTYPE,
   USER_ADDRESS,
   USER_CITY,
   USER_STATE,
   USER_ZIP,
   USER_COUNTRY,
   USER_PHONE,
   USER_EMAIL,
   USER_TIMEZONE,
   USER_SPECIALTY,
   USER_WRKEXP,
   USER_PHASEINV,
   USER_DEFAULTGRP,
   USER_USRNAME,
   USER_STATUS,
   FK_ACCOUNT,
   CREATED_ON,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATOR,
   RID,
   PK_USER
)
AS
   SELECT   eres.F_Get_Usrtype (USR_TYPE) USER_TYPE,
            FK_GRP_DEFAULT USER_DEFAULT_GROUP_FK,
            (SELECT   SITE_NAME
               FROM   eres.ER_SITE
              WHERE   PK_SITE = FK_SITEID)
               SITE_NAME,
            o.USR_FIRSTNAME || ' ' || o.USR_LASTNAME AS USER_NAME,
            eres.F_Get_Codelstdesc (FK_CODELST_JOBTYPE) USER_JOBTYPE,
            a.ADDRESS AS USER_ADDRESS,
            a.ADD_CITY AS USER_CITY,
            a.ADD_STATE AS USER_STATE,
            a.ADD_ZIPCODE AS USER_ZIP,
            a.ADD_COUNTRY AS USER_COUNTRY,
            a.ADD_PHONE USER_PHONE,
            a.ADD_EMAIL AS USER_EMAIL,
            (SELECT   TZ_DESCRIPTION
               FROM   esch.SCH_TIMEZONES
              WHERE   PK_TZ = FK_TIMEZONE)
               USER_TIMEZONE,
            eres.F_Get_Codelstdesc (FK_CODELST_SPL) USER_SPECIALTY,
            USR_WRKEXP USER_WRKEXP,
            USR_PAHSEINV USER_PHASEINV,
            (SELECT   GRP_NAME
               FROM   eres.ER_GRPS
              WHERE   PK_GRP = FK_GRP_DEFAULT)
               USER_DEFAULT_GROUP,
            USR_LOGNAME USER_USRNAME,
            DECODE (USR_STAT,
                    'A', 'Active',
                    'B', 'Blocked',
                    'D', 'Deactivated',
                    'Other')
               USER_STATUS,
            FK_ACCOUNT,
            o.CREATED_ON CREATED_ON,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER i
              WHERE   i.PK_USER = o.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            o.LAST_MODIFIED_DATE LAST_MODIFIED_DATE,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER i
              WHERE   i.PK_USER = o.CREATOR)
               CREATOR,
            o.RID,
            PK_USER
     FROM   eres.ER_USER o, eres.ER_ADD a
    WHERE   o.USR_TYPE <> 'X' AND a.PK_ADD = o.FK_PERADD
/
COMMENT ON TABLE VDA_V_USERS IS 'This view provides access to the application users information'
/

COMMENT ON COLUMN VDA_V_USERS.USER_TIMEZONE IS 'The user address-timezone'
/

COMMENT ON COLUMN VDA_V_USERS.USER_SPECIALTY IS 'The user address specialty'
/

COMMENT ON COLUMN VDA_V_USERS.USER_WRKEXP IS 'The user work experience'
/

COMMENT ON COLUMN VDA_V_USERS.USER_PHASEINV IS 'The study phase user has been involved in'
/

COMMENT ON COLUMN VDA_V_USERS.USER_DEFAULTGRP IS 'The user address-city'
/

COMMENT ON COLUMN VDA_V_USERS.USER_USRNAME IS 'The default group of the user'
/

COMMENT ON COLUMN VDA_V_USERS.USER_STATUS IS 'The status of the user'
/

COMMENT ON COLUMN VDA_V_USERS.FK_ACCOUNT IS 'The account the site is linked with'
/

COMMENT ON COLUMN VDA_V_USERS.CREATED_ON IS 'The date the record was created on (Audit)'
/

COMMENT ON COLUMN VDA_V_USERS.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_USERS.LAST_MODIFIED_DATE IS 'The date the record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_USERS.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_USERS.RID IS 'The unique Row ID for Audit'
/

COMMENT ON COLUMN VDA_V_USERS.PK_USER IS 'The PK of the user record'
/

COMMENT ON COLUMN VDA_V_USERS.USER_EMAIL IS 'The user address-email'
/

COMMENT ON COLUMN VDA_V_USERS.USER_TYPE IS 'The Type of the users'
/

COMMENT ON COLUMN VDA_V_USERS.USER_DEFAULT_GROUP_FK IS 'The FK to default group of the user'
/

COMMENT ON COLUMN VDA_V_USERS.USER_PHONE IS 'The user address-phone'
/

COMMENT ON COLUMN VDA_V_USERS.SITE_NAME IS 'The default organization for the user'
/

COMMENT ON COLUMN VDA_V_USERS.USER_NAME IS 'The name of the user'
/

COMMENT ON COLUMN VDA_V_USERS.USER_JOBTYPE IS 'The user job type'
/

COMMENT ON COLUMN VDA_V_USERS.USER_ADDRESS IS 'The user address first line'
/

COMMENT ON COLUMN VDA_V_USERS.USER_CITY IS 'The user address-city'
/

COMMENT ON COLUMN VDA_V_USERS.USER_STATE IS 'The user address-state'
/

COMMENT ON COLUMN VDA_V_USERS.USER_ZIP IS 'The user address-zip'
/

COMMENT ON COLUMN VDA_V_USERS.USER_COUNTRY IS 'The user address-country'
/


