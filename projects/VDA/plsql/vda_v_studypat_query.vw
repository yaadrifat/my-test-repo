/* Formatted on 3/11/2014 3:19:56 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_STUDYPAT_QUERY  (View)
--
--  Dependencies:
--   ER_PATFORMS (Table)
--   ER_PATPROT (Table)
--   ER_PER (Table)
--   ER_CODELST (Table)
--   ER_FORMQUERY (Table)
--   ER_FORMQUERYSTATUS (Table)
--   ER_FORMSEC (Table)
--   ER_STUDY (Table)
--   ER_FLDLIB (Table)
--   ER_FORMFLD (Table)
--   ER_FORMLIB (Table)
--   ER_USER (Table)
--   ER_SITE (Table)
--

CREATE OR REPLACE FORCE VIEW VDA_V_STUDYPAT_QUERY
(
   PATIENT_ID,
   PATIENT_STUDY_ID,
   STUDY_NUMBER,
   FORM_NAME,
   FORMSEC_NAME,
   FIELD,
   QUERY_TYPE,
   QUERYDESC,
   QUERY_NOTES,
   QUERY_STATUS,
   CREATOR,
   QUERY_CREATED_ON,
   FK_FORMQUERY,
   FK_QUERYMODULE,
   PATFORMS_FILLDATE,
   FK_STUDY,
   FIELD_ID,
   FK_SITE_ENROLLING,
   ENROLLING_SITE_NAME,
   QUERY_STATUS_DATE,
   FK_ACCOUNT
)
AS
   SELECT   (SELECT   per_code
               FROM   eres.ER_PER
              WHERE   pk_per = g.fk_per)
               AS patient_id,
            patprot_patstdid AS patient_study_id,
            (SELECT   study_number
               FROM   eres.ER_STUDY
              WHERE   pk_study = fk_study)
               AS study_number,
            form_name,
            formsec_name,
            fld_name AS field,
            DECODE (entered_by, '0', 'System Query', 'Manual Query')
               AS query_type,
            (SELECT   codelst_desc
               FROM   eres.ER_CODELST
              WHERE   fk_codelst_querytype = pk_codelst)
               AS querydesc,
            query_notes,
            (SELECT   codelst_desc
               FROM   eres.ER_CODELST
              WHERE   fk_codelst_querystatus = pk_codelst)
               AS query_status,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   eres.ER_USER
              WHERE   pk_user = a.creator)
               AS creator,
            TRUNC (a.created_on) query_created_on,
            fk_formquery,
            fk_querymodule,
            patforms_filldate,
            g.fk_study,
            fld.fld_uniqueid AS field_id,
            fk_site_enrolling,
            (SELECT   site_name
               FROM   eres.er_site
              WHERE   pk_site = fk_site_enrolling)
               AS enrolling_site_name,
            TRUNC (b.entered_on) AS query_status_date,
            e.fk_account
     FROM   eres.ER_FORMQUERY a,
            eres.ER_FORMQUERYSTATUS b,
            eres.ER_FORMFLD c,
            eres.ER_FORMSEC d,
            eres.ER_FORMLIB e,
            eres.ER_PATFORMS f,
            eres.ER_PATPROT g,
            eres.ER_FLDLIB fld
    WHERE   a.QUERYMODULE_LINKEDTO = 4 AND b.fk_formquery = a.pk_formquery
            AND b.PK_FORMQUERYSTATUS =
                  (SELECT   MAX (b1.PK_FORMQUERYSTATUS)
                     FROM   eres.ER_FORMQUERYSTATUS b1
                    WHERE   a.pk_formquery = b1.fk_formquery
                            AND TRIM (b1.ENTERED_ON) =
                                  (SELECT   MAX (TRIM (b2.ENTERED_ON))
                                     FROM   eres.ER_FORMQUERYSTATUS b2
                                    WHERE   a.pk_formquery = b2.fk_formquery))
            AND a.fk_field = c.fk_field
            AND d.pk_formsec = c.fk_formsec
            AND e.pk_formlib = d.fk_formlib
            AND pk_formlib = f.fk_formlib
            AND f.RECORD_TYPE <> 'D'
            AND pk_patforms = fk_querymodule
            AND pk_patprot = fk_patprot
            AND pk_field = a.fk_field
/
COMMENT ON TABLE VDA_V_STUDYPAT_QUERY IS 'This view provides access to the study patient form queries'
/

COMMENT ON COLUMN VDA_V_STUDYPAT_QUERY.PATIENT_ID IS 'The Patient ID with which the query is linked'
/

COMMENT ON COLUMN VDA_V_STUDYPAT_QUERY.PATIENT_STUDY_ID IS 'The Patient Study ID with which the query is linked'
/

COMMENT ON COLUMN VDA_V_STUDYPAT_QUERY.STUDY_NUMBER IS 'The study number'
/

COMMENT ON COLUMN VDA_V_STUDYPAT_QUERY.FORM_NAME IS 'The name of the form'
/

COMMENT ON COLUMN VDA_V_STUDYPAT_QUERY.FORMSEC_NAME IS 'The form section name'
/

COMMENT ON COLUMN VDA_V_STUDYPAT_QUERY.FIELD IS 'The field with which the query is linked'
/

COMMENT ON COLUMN VDA_V_STUDYPAT_QUERY.QUERY_TYPE IS 'The Query Type'
/

COMMENT ON COLUMN VDA_V_STUDYPAT_QUERY.QUERYDESC IS 'The Query Description'
/

COMMENT ON COLUMN VDA_V_STUDYPAT_QUERY.QUERY_NOTES IS 'The notes linked with the query'
/

COMMENT ON COLUMN VDA_V_STUDYPAT_QUERY.QUERY_STATUS IS 'The most recent Query status'
/

COMMENT ON COLUMN VDA_V_STUDYPAT_QUERY.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYPAT_QUERY.QUERY_CREATED_ON IS 'The date the query record was created on'
/

COMMENT ON COLUMN VDA_V_STUDYPAT_QUERY.FK_FORMQUERY IS 'The Foreign Key to the main query record'
/

COMMENT ON COLUMN VDA_V_STUDYPAT_QUERY.FK_QUERYMODULE IS 'The FK of the query module(e.g.: in case of forms it stores the PK of the filled form'
/

COMMENT ON COLUMN VDA_V_STUDYPAT_QUERY.PATFORMS_FILLDATE IS 'The fill date of patient study form response'
/

COMMENT ON COLUMN VDA_V_STUDYPAT_QUERY.FK_STUDY IS 'The Foreign key of the study the query is linked with'
/

COMMENT ON COLUMN VDA_V_STUDYPAT_QUERY.FIELD_ID IS 'The Field ID as defined in form design'
/

COMMENT ON COLUMN VDA_V_STUDYPAT_QUERY.FK_SITE_ENROLLING IS 'The Foreign Key to the patient enrolling site record'
/

COMMENT ON COLUMN VDA_V_STUDYPAT_QUERY.ENROLLING_SITE_NAME IS 'The patient enrolling site name'
/

COMMENT ON COLUMN VDA_V_STUDYPAT_QUERY.QUERY_STATUS_DATE IS 'The Query status date'
/

COMMENT ON COLUMN VDA_V_STUDYPAT_QUERY.FK_ACCOUNT IS 'The account the query is linked with'
/


