/* Formatted on 3/11/2014 3:18:30 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_AE_ALL_BYSITE  (View)
--
--  Dependencies:
--   F_GETDIS_SITE (Function)
--   F_GET_CODELSTDESC (Function)
--   F_GET_YESNO (Function)
--   ER_SITE (Table)
--   VDA_V_AE_ALL (View)
--

CREATE OR REPLACE FORCE VIEW VDA_V_AE_ALL_BYSITE
(
   FK_SITE_ENROLLING,
   FK_STUDY,
   STUDY_NUMBER,
   STUDY_TITLE,
   STUDY_PI,
   STUDY_TAREA,
   STUDY_DISEASE_SITE,
   AE_TYPE,
   AE_DESC,
   AE_STDATE,
   AE_ENDDATE,
   AE_GRADE,
   AE_NAME,
   MEDDRA,
   DICTIONARY,
   SITE_NAME,
   STUDY_PK,
   PATIENT_PK
)
AS
   SELECT   fk_site_enrolling,
            fk_study,
            study_number,
            study_title,
            study_pi,
            study_tarea,
            study_disease_site,
            ae_type,
            ae_desc,
            ae_stdate,
            ae_enddate,
            ae_grade,
            ae_name,
            meddra,
            dictionary,
            site_name,
            fk_study study_pk,
            fk_per patient_pk
     FROM   VDA.VDA_V_ae_all, eres.er_site
    WHERE   fk_site_enrolling = pk_site
/
COMMENT ON TABLE VDA_V_AE_ALL_BYSITE IS 'This view provides access to the Adverse Event data by Patient enroling site'
/

COMMENT ON COLUMN VDA_V_AE_ALL_BYSITE.FK_SITE_ENROLLING IS 'The Primary Key of the Patient''s enrolling site'
/

COMMENT ON COLUMN VDA_V_AE_ALL_BYSITE.FK_STUDY IS 'The Primary Key of study the AE is linked with'
/

COMMENT ON COLUMN VDA_V_AE_ALL_BYSITE.STUDY_NUMBER IS 'The Study Numberof the study the AE is linked with'
/

COMMENT ON COLUMN VDA_V_AE_ALL_BYSITE.STUDY_TITLE IS 'The Study Title'
/

COMMENT ON COLUMN VDA_V_AE_ALL_BYSITE.STUDY_PI IS 'The Study PI'
/

COMMENT ON COLUMN VDA_V_AE_ALL_BYSITE.STUDY_TAREA IS 'The Study Therapeutic Area'
/

COMMENT ON COLUMN VDA_V_AE_ALL_BYSITE.STUDY_DISEASE_SITE IS 'The Study Disease Site'
/

COMMENT ON COLUMN VDA_V_AE_ALL_BYSITE.AE_TYPE IS 'The Adverse Event Type'
/

COMMENT ON COLUMN VDA_V_AE_ALL_BYSITE.AE_DESC IS 'The Adverse Event Description'
/

COMMENT ON COLUMN VDA_V_AE_ALL_BYSITE.AE_STDATE IS 'The Adverse Event Start Date'
/

COMMENT ON COLUMN VDA_V_AE_ALL_BYSITE.AE_ENDDATE IS 'The Adverse Event End Date'
/

COMMENT ON COLUMN VDA_V_AE_ALL_BYSITE.AE_GRADE IS 'The Adverse Event Grade'
/

COMMENT ON COLUMN VDA_V_AE_ALL_BYSITE.AE_NAME IS 'The Adverse Event Name'
/

COMMENT ON COLUMN VDA_V_AE_ALL_BYSITE.MEDDRA IS 'The Adverse Event MEDDRA code'
/

COMMENT ON COLUMN VDA_V_AE_ALL_BYSITE.DICTIONARY IS 'The Adverse Event Dictionaries'
/

COMMENT ON COLUMN VDA_V_AE_ALL_BYSITE.SITE_NAME IS 'The Patient''s enrolling site name'
/

COMMENT ON COLUMN VDA_V_AE_ALL_BYSITE.STUDY_PK IS 'The Primary Key of study the AE is linked with'
/

COMMENT ON COLUMN VDA_V_AE_ALL_BYSITE.PATIENT_PK IS 'The Primary Key of the patient the AE is linked with'
/


