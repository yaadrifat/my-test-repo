/* Formatted on 3/11/2014 3:18:53 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_ERES_AUDITROW  (View)
--
--  Dependencies:
--   AUDIT_ROW (Table)
--

CREATE OR REPLACE FORCE VIEW VDA_V_ERES_AUDITROW
(
   RAID,
   TABLE_NAME,
   RID,
   ACTION,
   TIMESTAMP,
   USER_NAME
)
AS
   SELECT   "RAID",
            "TABLE_NAME",
            "RID",
            "ACTION",
            "TIMESTAMP",
            "USER_NAME"
     FROM   eres.audit_row
/
COMMENT ON TABLE VDA_V_ERES_AUDITROW IS 'This view provides access to the high level audit trail on Insert, Update and Delete actions in ERES schema'
/

COMMENT ON COLUMN VDA_V_ERES_AUDITROW.RAID IS 'The Unique audit Primary key'
/

COMMENT ON COLUMN VDA_V_ERES_AUDITROW.TABLE_NAME IS 'The name of the table'
/

COMMENT ON COLUMN VDA_V_ERES_AUDITROW.RID IS 'The unique RID of the table row'
/

COMMENT ON COLUMN VDA_V_ERES_AUDITROW.ACTION IS 'The action, I:Insert,D:Delete and U:Update'
/

COMMENT ON COLUMN VDA_V_ERES_AUDITROW.TIMESTAMP IS 'The date and time of the action'
/

COMMENT ON COLUMN VDA_V_ERES_AUDITROW.USER_NAME IS 'The user who triggered the action'
/


