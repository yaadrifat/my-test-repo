/* Formatted on 3/11/2014 3:18:54 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_ERES_DELETELOG  (View)
--
--  Dependencies:
--   AUDIT_DELETELOG (Table)
--

CREATE OR REPLACE FORCE VIEW VDA_V_ERES_DELETELOG
(
   PK_APP_DL,
   TABLE_NAME,
   TABLE_PK,
   TABLE_RID,
   DELETED_BY,
   DELETED_ON,
   APP_MODULE,
   IP_ADDRESS,
   REASON_FOR_DELETION
)
AS
   SELECT   PK_APP_DL,
            TABLE_NAME,
            TABLE_PK,
            TABLE_RID,
            DELETED_BY,
            DELETED_ON,
            APP_MODULE,
            IP_ADDRESS,
            REASON_FOR_DELETION
     FROM   eres.audit_deletelog
/
COMMENT ON TABLE VDA_V_ERES_DELETELOG IS 'This view provides data on all delete transactions from Velos eResearch eres database schema'
/

COMMENT ON COLUMN VDA_V_ERES_DELETELOG.PK_APP_DL IS 'Primary key'
/

COMMENT ON COLUMN VDA_V_ERES_DELETELOG.TABLE_NAME IS 'Name of the database table'
/

COMMENT ON COLUMN VDA_V_ERES_DELETELOG.TABLE_PK IS 'The DELETED ROW''s Primary Key'
/

COMMENT ON COLUMN VDA_V_ERES_DELETELOG.TABLE_RID IS 'The DELETED ROW''s RID'
/

COMMENT ON COLUMN VDA_V_ERES_DELETELOG.DELETED_BY IS 'The PK of the er_user, user who deleted the row'
/

COMMENT ON COLUMN VDA_V_ERES_DELETELOG.DELETED_ON IS 'Stores the date of deletion'
/

COMMENT ON COLUMN VDA_V_ERES_DELETELOG.APP_MODULE IS 'Stores the Module name'
/

COMMENT ON COLUMN VDA_V_ERES_DELETELOG.IP_ADDRESS IS 'Stores the IP address of user'
/

COMMENT ON COLUMN VDA_V_ERES_DELETELOG.REASON_FOR_DELETION IS 'Reason why the row was deleted'
/


