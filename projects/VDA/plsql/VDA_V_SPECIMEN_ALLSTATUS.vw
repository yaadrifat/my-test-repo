CREATE OR REPLACE  VIEW VDA_V_SPECIMEN_ALLSTATUS
(
   PK_SPECIMEN_STATUS,
   FK_SPECIMEN,
   SS_DATE,
   FK_CODELST_STATUS,
   SS_QUANTITY,
   SS_QUANTITY_UNITS,
   SS_STATUS_BY,
   RID,
   CREATOR,
   CREATED_ON,
   IP_ADD,
   SS_PROC_TYPE,
   SS_PROC_DATE,
   STATUS_DESC,
   PROCESS_TYPE_DESC,
   SPECIMEN_ID,
   LAST_MODIFIED_BY,
   SS_HAND_OFF_DATE,
   SS_NOTES,
   SS_TRACKING_NUMBER,
   FK_USER_RECEPIENT,
   STUDY_NUMBER,
   SS_ACTION
)
AS
   SELECT   x.pk_specimen_status,
            x.fk_specimen,
            x.ss_date,
            x.fk_codelst_status,
            x.ss_quantity,
            eres.F_GET_CODELSTDESC (x.ss_quantity_units) ss_quantity_units,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = x.ss_status_by)
               ss_status_by,
            x.rid,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = x.creator)
               creator,
            x.created_on,
            x.ip_add,
            x.ss_proc_type,
            x.ss_proc_date,
            eres.F_GET_CODELSTDESC (fk_codelst_status) status_desc,
            eres.F_GET_CODELSTDESC (ss_proc_type) process_type_desc,
            spec_id as   specimen_id,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = x.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            x.SS_HAND_OFF_DATE,
            x.SS_NOTES,
            x.SS_TRACKING_NUMBER,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = x.FK_USER_RECEPIENT)
               FK_USER_RECEPIENT,
            (SELECT   study_number
               FROM   eres.er_study
              WHERE   pk_study = x.FK_STUDY)
               study_number,
            eres.F_GET_CODELSTDESC (SS_ACTION) SS_ACTION
     FROM   eres.er_specimen_status x, eres.er_specimen sp
	 where pk_specimen = x.fk_specimen 
/

COMMENT ON TABLE VDA_V_SPECIMEN_ALLSTATUS IS 'This view provides access to all specimen statuses'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.PK_SPECIMEN_STATUS IS 'The Primary key of the Specimen Status record'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.FK_SPECIMEN IS 'The FK to the main specimen record'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.SS_DATE IS 'The Status Date'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.FK_CODELST_STATUS IS 'The codelist value of the specimen status'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.SS_QUANTITY IS 'The quantity number linked with status record'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.SS_QUANTITY_UNITS IS 'The quantity unit linked with status record'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.SS_STATUS_BY IS 'The user who entered the status'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.RID IS 'The unique Row ID for Audit'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.CREATED_ON IS 'The date the record was created on (Audit)'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.IP_ADD IS 'The IP address of the client machine that initiated the session'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.SS_PROC_TYPE IS 'The processing type code'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.SS_PROC_DATE IS 'The prcessing date'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.STATUS_DESC IS 'The Status description'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.PROCESS_TYPE_DESC IS 'The Process Type description'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.SPECIMEN_ID IS 'The Specimen ID'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.SS_HAND_OFF_DATE IS 'The date the record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.SS_NOTES IS 'The notes associated with the status'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.SS_TRACKING_NUMBER IS 'The tracking number linked with the status'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.FK_USER_RECEPIENT IS 'The recepient linked with the status'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.STUDY_NUMBER IS 'The study number linked with the status'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.SS_ACTION IS 'Action performed: incremented/decremented '
/
