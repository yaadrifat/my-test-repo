/* Formatted on 3/11/2014 3:19:37 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_PATFORMS_AUDIT  (View)
--
--  Dependencies:
--   ER_PATFORMS (Table)
--   ER_PATPROT (Table)
--   ER_PER (Table)
--   ER_FORMAUDITCOL (Table)
--   ER_FORMLIB (Table)
--

CREATE OR REPLACE FORCE VIEW VDA_V_PATFORMS_AUDIT
(
   PK_FORMAUDITCOL,
   FK_FILLEDFORM,
   FK_FORM,
   FORM_NAME,
   FA_SYSTEMID,
   FA_DATETIME,
   FA_FLDNAME,
   FA_OLDVALUE,
   FA_NEWVALUE,
   FA_MODIFIEDBY_NAME,
   FA_REASON,
   FK_PER,
   FK_PATPROT,
   PATIENT_ID,
   PATIENT_STUDY_ID,
   FK_ACCOUNT
)
AS
   SELECT   PK_FORMAUDITCOL,
            FK_FILLEDFORM,
            FK_FORM,
            form_name,
            FA_SYSTEMID,
            FA_DATETIME,
            FA_FLDNAME,
            FA_OLDVALUE,
            FA_NEWVALUE,
            FA_MODIFIEDBY_NAME,
            FA_REASON,
            fk_per,
            fk_patprot,
            per_code AS Patient_id,
            (SELECT   patprot_patstdid
               FROM   eres.er_patprot
              WHERE   pk_patprot = fk_patprot)
               Patient_study_id,
            b.fk_account
     FROM   eres.er_formauditcol,
            eres.er_formlib b,
            eres.er_patforms pf,
            eres.er_per p
    WHERE       FA_FORMTYPE = 'P'
            AND b.pk_formlib = FK_FORM
            AND pf.pk_patforms = FK_FILLEDFORM
            AND pf.fk_formlib = FK_FORM
            AND p.pk_per = fk_per
/
COMMENT ON TABLE VDA_V_PATFORMS_AUDIT IS 'This view provides access to the column update level Audit information of account level forms'
/

COMMENT ON COLUMN VDA_V_PATFORMS_AUDIT.PK_FORMAUDITCOL IS 'The Primary Key of the Audit Transaction'
/

COMMENT ON COLUMN VDA_V_PATFORMS_AUDIT.FK_FILLEDFORM IS 'The FK to the form response (account form)'
/

COMMENT ON COLUMN VDA_V_PATFORMS_AUDIT.FK_FORM IS 'The FK to the form'
/

COMMENT ON COLUMN VDA_V_PATFORMS_AUDIT.FORM_NAME IS 'The form name'
/

COMMENT ON COLUMN VDA_V_PATFORMS_AUDIT.FA_SYSTEMID IS 'The systemid of the field'
/

COMMENT ON COLUMN VDA_V_PATFORMS_AUDIT.FA_DATETIME IS 'The date-time of the transaction'
/

COMMENT ON COLUMN VDA_V_PATFORMS_AUDIT.FA_FLDNAME IS 'The user defined field name'
/

COMMENT ON COLUMN VDA_V_PATFORMS_AUDIT.FA_OLDVALUE IS 'The old value of the field'
/

COMMENT ON COLUMN VDA_V_PATFORMS_AUDIT.FA_NEWVALUE IS 'The new value of the field'
/

COMMENT ON COLUMN VDA_V_PATFORMS_AUDIT.FA_MODIFIEDBY_NAME IS 'The user who modified the form response data'
/

COMMENT ON COLUMN VDA_V_PATFORMS_AUDIT.FA_REASON IS 'The reason for change'
/

COMMENT ON COLUMN VDA_V_PATFORMS_AUDIT.FK_PER IS 'The FK of the Patient the form response is linked with'
/

COMMENT ON COLUMN VDA_V_PATFORMS_AUDIT.FK_PATPROT IS 'The FK of the Patient-study record the form response is linked with'
/

COMMENT ON COLUMN VDA_V_PATFORMS_AUDIT.PATIENT_ID IS 'The Patient ID or Patient Code of the patient the form response is linked with'
/

COMMENT ON COLUMN VDA_V_PATFORMS_AUDIT.PATIENT_STUDY_ID IS 'The Patient Study ID of the patient the form response is linked with'
/

COMMENT ON COLUMN VDA_V_PATFORMS_AUDIT.FK_ACCOUNT IS 'The account the form response is linked with'
/


