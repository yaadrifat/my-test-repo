CREATE OR REPLACE VIEW VDA_V_MILESTONES
(
   STUDY_NUMBER,
   STUDY_TITLE,
   MSRUL_CATEGORY,
   MSRUL_AMOUNT,
   MSRUL_PT_COUNT,
   MSRUL_PT_STATUS,
   MSRUL_LIMIT,
   MSRUL_PAY_TYPE,
   MSRUL_PAY_FOR,
   MSRUL_STATUS,
   MSRUL_PROT_CAL,
   MSRUL_VISIT,
   MSRUL_MS_RULE,
   MSRUL_EVENT_STAT,
   MSRUL_STUDY_STATUS,
   FK_ACCOUNT,
   PK_MILESTONE,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATED_ON,
   FK_STUDY,
   EVENT_NAME,
   MILESTONE_ACHIEVEDCOUNT,
   MILESTONE_ACHIEVEDAMOUNT,
   LAST_CHECKED_ON,
   MILESTONE_DESCRIPTION,
   FK_BUDGET,
   FK_BGTCAL,
   FK_BGTSECTION,
   FK_LINEITEM,
   MILESTONE_DATE_FROM,
   MILESTONE_DATE_TO,
   MILESTONE_HOLDBACK,
   MILESTONE_DESC_CALCULATED, fk_visit,FK_EVENTASSOC
)
AS
   SELECT   STUDY_NUMBER,
            STUDY_TITLE,
            ERES.F_GEt_Mtype (MILESTONE_TYPE) MSRUL_CATEGORY,
            MILESTONE_AMOUNT MSRUL_AMOUNT,
            MILESTONE_COUNT MSRUL_PT_COUNT,
            (SELECT   CODELST_DESC
               FROM   eres.ER_CODELST
              WHERE   PK_CODELST = MILESTONE_STATUS
                      AND CODELST_TYPE = 'patStatus')
               MSRUL_PT_STATUS,
            MILESTONE_LIMIT MSRUL_LIMIT,
            ERES.F_GEt_Codelstdesc (MILESTONE_PAYTYPE) MSRUL_PAY_TYPE,
            ERES.F_GEt_Codelstdesc (MILESTONE_PAYFOR) MSRUL_PAY_FOR,
            ERES.F_GEt_Codelstdesc (FK_CODELST_MILESTONE_STAT) MSRUL_STATUS,
            (SELECT   NAME
               FROM   ESCH.EVENT_ASSOC
              WHERE   EVENT_ID = FK_CAL)
               MSRUL_PROT_CAL,
            (SELECT   visit_name
               FROM   esch.sch_protocol_visit
              WHERE   pk_protocol_visit = fk_visit)
               MSRUL_VISIT,
            ERES.F_GEt_Codelstdesc (FK_CODELST_RULE) MSRUL_MS_RULE,
            (SELECT   CODELST_DESC
               FROM   esch.SCH_CODELST
              WHERE   PK_CODELST = MILESTONE_EVENTSTATUS)
               MSRUL_EVENT_STAT,
            (SELECT   CODELST_DESC
               FROM   eres.ER_CODELST
              WHERE   PK_CODELST = MILESTONE_STATUS
                      AND CODELST_TYPE = 'studystat')
               MSRUL_STUDY_STATUS,
            s.fk_Account FK_ACCOUNT,
            PK_MILESTONE,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = m.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = m.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            m.LAST_MODIFIED_DATE,
            m.CREATED_ON,
            FK_STUDY,
            (SELECT   NAME
               FROM   ESCH.EVENT_ASSOC
              WHERE   EVENT_ID = FK_EVENTASSOC)
               EVENT_NAME,
            MILESTONE_ACHIEVEDCOUNT,
            NVL (MILESTONE_ACHIEVEDCOUNT * MILESTONE_AMOUNT, 0),
            LAST_CHECKED_ON,
            MILESTONE_DESCRIPTION,
            FK_BUDGET,
            FK_BGTCAL,
            FK_BGTSECTION,
            FK_LINEITEM,
            m.MILESTONE_DATE_FROM,
            m.MILESTONE_DATE_TO,
            m.MILESTONE_HOLDBACK,
			ERES.PKG_Milestone_New.f_getMilestoneDesc (PK_MILESTONE), fk_visit,FK_EVENTASSOC
     FROM   eres.ER_MILESTONE m, eres.er_study s
    WHERE   NVL (milestone_delflag, 'N') = 'N' AND m.fk_study = pk_study
/
COMMENT ON TABLE VDA_V_MILESTONES IS 'This view provides access to the Milestones defined for studies'
/

COMMENT ON COLUMN VDA_V_MILESTONES.STUDY_NUMBER IS 'The Study Number of the study the milestone is linked with'
/

COMMENT ON COLUMN VDA_V_MILESTONES.STUDY_TITLE IS 'The study title of the study the milestone is linked with'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_CATEGORY IS 'The milestone rule category'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_AMOUNT IS 'The amount linked with the milestone'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_PT_COUNT IS 'The patient count attribute of the milestone (if applicable)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_PT_STATUS IS 'The patient status attribute of the milestone (if applicable)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_LIMIT IS 'The milestone achievement count limit'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_PAY_TYPE IS 'The milestone payment type'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_PAY_FOR IS 'The milestone payment for'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_STATUS IS 'The milestone status'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_PROT_CAL IS 'The Calendar attribute of the milestone (if applicable)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_VISIT IS 'The Calendar visit attribute of the milestone (if applicable)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_MS_RULE IS 'The milestone rule description'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_EVENT_STAT IS 'The Calendar event status attribute of the milestone (if applicable)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_STUDY_STATUS IS 'The Study Status attribute of the milestone (if applicable)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.FK_ACCOUNT IS 'The account the milestone is linked with'
/

COMMENT ON COLUMN VDA_V_MILESTONES.PK_MILESTONE IS 'The primary key of the milestone record'
/

COMMENT ON COLUMN VDA_V_MILESTONES.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.LAST_MODIFIED_DATE IS 'The date the record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.CREATED_ON IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.FK_STUDY IS 'The Foreign Key to the study milestone is linked with'
/

COMMENT ON COLUMN VDA_V_MILESTONES.EVENT_NAME IS 'The event name attribute of the milestone (if applicable)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MILESTONE_ACHIEVEDCOUNT IS 'The current milestone achievement count'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MILESTONE_ACHIEVEDAMOUNT IS 'The current milestone achieved amount'
/

COMMENT ON COLUMN VDA_V_MILESTONES.LAST_CHECKED_ON IS 'The timestamp when the milestone achievement was last checked on'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MILESTONE_DESCRIPTION IS 'The milestone description'
/

COMMENT ON COLUMN VDA_V_MILESTONES.FK_BUDGET IS 'The foreign key to the Budget (if the milestone was created from a budget)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.FK_BGTCAL IS 'The foreign key to the Budget Calendar record (if the milestone was created from a budget)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.FK_BGTSECTION IS 'The foreign key to the Budget Section record Calendar record (if the milestone was created from a budget)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.FK_LINEITEM IS 'The foreign key to the Budget line item record (if the milestone was created from a budget)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MILESTONE_DATE_FROM IS 'The Start Date for the time-bounded milestones'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MILESTONE_DATE_TO IS 'The To or End Date for the time-bounded milestones'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MILESTONE_HOLDBACK IS 'The Holdback amount for the milestone'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MILESTONE_DESC_CALCULATED IS 'The Milestone description calculated from milestone attributes'
/

COMMENT ON COLUMN VDA_V_MILESTONES.fk_visit IS 'The PK of visit to uniquely identify a visit for visit milestones'
/

COMMENT ON COLUMN VDA_V_MILESTONES.FK_EVENTASSOC IS 'The PK of visit to uniquely identify an Event for event milestones'
/
