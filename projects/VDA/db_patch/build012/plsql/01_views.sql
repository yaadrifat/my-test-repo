-- VDA_V_LIB_EVENTS(View)

CREATE OR REPLACE FORCE VIEW "VDA"."VDA_V_LIB_EVENTS" ("LBEVE_CATEGORY", "LBEVE_EVENT", "LBEVE_DESCRIPTION", "LBEVE_CPTCODE", "LBEVE_DURATION", "LBEVE_VISIT_WIN_B4", "LBEVE_VISIT_WIN_AFT", "LBEVE_NOTES", "LBEVE_PTMESS_B4", "LBEVE_PTMESS_AFT", "LBEVE_PTDAYS_B4", "LBEVE_PTDAYS_AFT", "LBEVE_USRMESS_B4", "LBEVE_USRMESS_AFT", "LBEVE_USRDAYS_B4", "LBEVE_USRDAYS_AFT", "LBEVE_COST_CNT", "LBEVE_APPNX_CNT", "LBEVE_RESOURCE_CNT", "FK_ACCOUNT", "EVENT_ID", "CREATOR", "CREATED_ON", "LAST_MODIFIED_BY", "LAST_MODIFIED_DATE", "EVENT_LIBRARY_TYPE", "EVENT_LINE_CATEGORY", "SITE_OF_SERVICE_NAME", "FACILITY_NAME", "COVERAGE_TYPE")
AS
  SELECT
    (SELECT NAME
    FROM ESCH.EVENT_DEF B
    WHERE A.CHAIN_ID = B.EVENT_ID
    AND EVENT_TYPE   = 'L'
    ) LBEVE_CATEGORY,
    NAME LBEVE_EVENT,
    DESCRIPTION LBEVE_DESCRIPTION,
    EVENT_CPTCODE LBEVE_CPTCODE,
    DURATION
    || ' '
    || ERES.F_GET_DURUNIT (DURATION_UNIT) LBEVE_DURATION,
    FUZZY_PERIOD
    || ' '
    || ERES.F_GET_DURUNIT (EVENT_DURATIONBEFORE) LBEVE_VISIT_WIN_B4,
    EVENT_FUZZYAFTER
    || ' '
    || ERES.F_GET_DURUNIT (EVENT_DURATIONAFTER) LBEVE_VISIT_WIN_AFT,
    NOTES LBEVE_NOTES,
    PAT_MSGBEFORE LBEVE_PTMESS_B4,
    PAT_MSGAFTER LBEVE_PTMESS_AFT,
    PAT_DAYSBEFORE LBEVE_PTDAYS_B4,
    PAT_DAYSAFTER LBEVE_PTDAYS_AFT,
    USR_MSGBEFORE LBEVE_USRMESS_B4,
    USR_MSGAFTER LBEVE_USRMESS_AFT,
    USR_DAYSBEFORE LBEVE_USRDAYS_B4,
    USR_DAYSAFTER LBEVE_USRDAYS_AFT,
    (SELECT COUNT ( * ) FROM esch.SCH_EVENTCOST WHERE FK_EVENT = EVENT_ID
    ) LBEVE_COST_CNT,
    (SELECT COUNT ( * ) FROM esch.SCH_EVENTDOC WHERE FK_EVENT = EVENT_ID
    ) LBEVE_APPNX_CNT,
    (SELECT COUNT ( * )
    FROM esch.SCH_EVENTUSR
    WHERE FK_EVENT = EVENT_ID
    AND EVENTUSR_TYPE = 'R'
    ) LBEVE_RESOURCE_CNT,
    USER_ID FK_ACCOUNT,
    EVENT_ID,
    (SELECT USR_FIRSTNAME
      || ' '
      || USR_LASTNAME
    FROM eres.ER_USER
    WHERE PK_USER = A.CREATOR
    ) CREATOR,
    CREATED_ON,
    (SELECT USR_FIRSTNAME
      || ' '
      || USR_LASTNAME
    FROM eres.ER_USER
    WHERE PK_USER = A.LAST_MODIFIED_BY
    ) LAST_MODIFIED_BY,
    LAST_MODIFIED_DATE,
    (SELECT CODELST_DESC
    FROM esch.SCH_CODELST
    WHERE PK_CODELST = EVENT_LIBRARY_TYPE
    ) EVENT_LIBRARY_TYPE,
    (SELECT
      (SELECT CODELST_DESC
      FROM esch.SCH_CODELST
      WHERE PK_CODELST = b.EVENT_LINE_CATEGORY
      )
    FROM ESCH.EVENT_DEF B
    WHERE A.CHAIN_ID = B.EVENT_ID
    AND EVENT_TYPE   = 'L'
    ) EVENT_LINE_CATEGORY,
    (SELECT site_name FROM eres.er_site WHERE pk_site = SERVICE_SITE_ID
    ) SITE_OF_SERVICE_NAME,
    (SELECT site_name FROM eres.er_site WHERE pk_site = FACILITY_ID
    ) FACILITY_NAME,
    (SELECT CODELST_DESC
    FROM esch.SCH_CODELST
    WHERE PK_CODELST = FK_CODELST_COVERTYPE
    ) COVERAGE_TYPE
  FROM ESCH.EVENT_DEF A
  WHERE EVENT_TYPE = 'E';
  COMMENT ON COLUMN "VDA"."VDA_V_LIB_EVENTS"."LBEVE_CATEGORY"
IS
  'The Event category in the Event Library';
/
  	COMMENT ON COLUMN "VDA"."VDA_V_LIB_EVENTS"."LBEVE_EVENT"
IS
  'The event Name';
/  
	COMMENT ON COLUMN "VDA"."VDA_V_LIB_EVENTS"."LBEVE_DESCRIPTION"
IS
  'The Event Description';
/
	COMMENT ON COLUMN "VDA"."VDA_V_LIB_EVENTS"."LBEVE_CPTCODE"
IS
  'The CPT code linked with the event';
/
	COMMENT ON COLUMN "VDA"."VDA_V_LIB_EVENTS"."LBEVE_DURATION"
IS
  'The Event Duration';
/
	COMMENT ON COLUMN "VDA"."VDA_V_LIB_EVENTS"."LBEVE_VISIT_WIN_B4"
IS
  'Visit Window duration specified for before the event date';
/
	COMMENT ON COLUMN "VDA"."VDA_V_LIB_EVENTS"."LBEVE_VISIT_WIN_AFT"
IS
  'Visit Window duration specified for after the event date';
/
	COMMENT ON COLUMN "VDA"."VDA_V_LIB_EVENTS"."LBEVE_NOTES"
IS
  'Event Notes';
/
	COMMENT ON COLUMN "VDA"."VDA_V_LIB_EVENTS"."LBEVE_PTMESS_B4"
IS
  'Event Message for the patient-before the event';
/
	COMMENT ON COLUMN "VDA"."VDA_V_LIB_EVENTS"."LBEVE_PTMESS_AFT"
IS
  'Event Message for the patient-after the event';
/
	COMMENT ON COLUMN "VDA"."VDA_V_LIB_EVENTS"."LBEVE_PTDAYS_B4"
IS
  'Number of Days before the Event the patient Message to be sent out';
/
	COMMENT ON COLUMN "VDA"."VDA_V_LIB_EVENTS"."LBEVE_PTDAYS_AFT"
IS
  'Number of Days after the Event the patient Message to be sent out';
/
	COMMENT ON COLUMN "VDA"."VDA_V_LIB_EVENTS"."LBEVE_USRMESS_B4"
IS
  'Event Message for the user-before the event';
/
	COMMENT ON COLUMN "VDA"."VDA_V_LIB_EVENTS"."LBEVE_USRMESS_AFT"
IS
  'Event Message for the user-after the event';
/
	COMMENT ON COLUMN "VDA"."VDA_V_LIB_EVENTS"."LBEVE_USRDAYS_B4"
IS
  'Number of Days before the Event the user Message to be sent out';
/
	COMMENT ON COLUMN "VDA"."VDA_V_LIB_EVENTS"."LBEVE_USRDAYS_AFT"
IS
  'Number of Days after the Event the user Message to be sent out';
/
	COMMENT ON COLUMN "VDA"."VDA_V_LIB_EVENTS"."LBEVE_COST_CNT"
IS
  'Count of cost records linked with the event';
/
	COMMENT ON COLUMN "VDA"."VDA_V_LIB_EVENTS"."LBEVE_APPNX_CNT"
IS
  'Count of appendix records linked with the event';
/
	COMMENT ON COLUMN "VDA"."VDA_V_LIB_EVENTS"."LBEVE_RESOURCE_CNT"
IS
  'Count of resource records linked with the event';
/
	COMMENT ON COLUMN "VDA"."VDA_V_LIB_EVENTS"."FK_ACCOUNT"
IS
  'the account linked with event library';
/
	COMMENT ON COLUMN "VDA"."VDA_V_LIB_EVENTS"."EVENT_ID"
IS
  'the Primary Key of the event record';
/
	COMMENT ON COLUMN "VDA"."VDA_V_LIB_EVENTS"."CREATOR"
IS
  'The user who created the event  record (Audit)';
/
	COMMENT ON COLUMN "VDA"."VDA_V_LIB_EVENTS"."CREATED_ON"
IS
  'The date the event record was created on (Audit)';
/
	COMMENT ON COLUMN "VDA"."VDA_V_LIB_EVENTS"."LAST_MODIFIED_BY"
IS
  'The user who last modified the event  record(Audit)';
/
	COMMENT ON COLUMN "VDA"."VDA_V_LIB_EVENTS"."LAST_MODIFIED_DATE"
IS
  'The date the event record was last modified on (Audit)';
/
	COMMENT ON COLUMN "VDA"."VDA_V_LIB_EVENTS"."EVENT_LIBRARY_TYPE"
IS
  'The event library type the event is linked with';
/
	COMMENT ON COLUMN "VDA"."VDA_V_LIB_EVENTS"."EVENT_LINE_CATEGORY"
IS
  'The default budget category for the event';
/
	COMMENT ON COLUMN "VDA"."VDA_V_LIB_EVENTS"."SITE_OF_SERVICE_NAME"
IS
  'The Site of Service (Organization) linked with the event';
/
	COMMENT ON COLUMN "VDA"."VDA_V_LIB_EVENTS"."FACILITY_NAME"
IS
  'The Facility linked with the event';
/
	COMMENT ON COLUMN "VDA"."VDA_V_LIB_EVENTS"."COVERAGE_TYPE"
IS
  'The default coverage type linked with the event';
/
	COMMENT ON TABLE "VDA"."VDA_V_LIB_EVENTS"
IS
  'This view provides access to the Event Library data';
/


-- VDA_V_PAYMENTS View

CREATE OR REPLACE FORCE VIEW "VDA"."VDA_V_PAYMENTS" ("STUDY_NUMBER", "STUDY_TITLE", "MSPAY_DATE", "MSPAY_TYPE", "MSPAY_AMT_RECVD", "MSPAY_DESC", "MSPAY_COMMENTS", "PK_MILEPAYMENT", "CREATOR", "LAST_MODIFIED_BY", "LAST_MODIFIED_DATE", "CREATED_ON", "FK_STUDY", "FK_ACCOUNT")
AS
  SELECT STUDY_NUMBER,
    STUDY_TITLE,
    MILEPAYMENT_DATE MSPAY_DATE,
    ERES.F_GET_CODELSTDESC (MILEPAYMENT_TYPE) MSPAY_TYPE,
    MILEPAYMENT_AMT MSPAY_AMT_RECVD,
    MILEPAYMENT_DESC MSPAY_DESC,
    MILEPAYMENT_COMMENTS MSPAY_COMMENTS,
    PK_MILEPAYMENT,
    (SELECT USR_FIRSTNAME
      || ' '
      || USR_LASTNAME
    FROM eres.ER_USER
    WHERE PK_USER = s.CREATOR
    ) CREATOR,
    (SELECT USR_FIRSTNAME
      || ' '
      || USR_LASTNAME
    FROM eres.ER_USER
    WHERE PK_USER = s.LAST_MODIFIED_BY
    ) LAST_MODIFIED_BY,
    s.LAST_MODIFIED_DATE,
    s.CREATED_ON,
    FK_STUDY,
    FK_ACCOUNT
  FROM eres.ER_MILEPAYMENT,
    eres.ER_STUDY s
  WHERE PK_STUDY = FK_STUDY
  and MILEPAYMENT_DELFLAG <> 'Y';
  COMMENT ON COLUMN "VDA"."VDA_V_PAYMENTS"."STUDY_NUMBER"
IS
  'The Study Number of the study the payment  is linked with';
/
	COMMENT ON COLUMN "VDA"."VDA_V_PAYMENTS"."STUDY_TITLE"
IS
  'The Study Title of the study the payment is linked with';
/
	COMMENT ON COLUMN "VDA"."VDA_V_PAYMENTS"."MSPAY_DATE"
IS
  'The payment date';
/
	COMMENT ON COLUMN "VDA"."VDA_V_PAYMENTS"."MSPAY_TYPE"
IS
  'The payment type';
/
	COMMENT ON COLUMN "VDA"."VDA_V_PAYMENTS"."MSPAY_AMT_RECVD"
IS
  'The payment amount received';
/
	COMMENT ON COLUMN "VDA"."VDA_V_PAYMENTS"."MSPAY_DESC"
IS
  'The payment description';
/
	COMMENT ON COLUMN "VDA"."VDA_V_PAYMENTS"."MSPAY_COMMENTS"
IS
  'The payment comments';
/
	COMMENT ON COLUMN "VDA"."VDA_V_PAYMENTS"."PK_MILEPAYMENT"
IS
  'The Primary Key of the payment record';
/
	COMMENT ON COLUMN "VDA"."VDA_V_PAYMENTS"."CREATOR"
IS
  'The user who created the record (Audit)';
/
	COMMENT ON COLUMN "VDA"."VDA_V_PAYMENTS"."LAST_MODIFIED_BY"
IS
  'The user who last modified the record(Audit)';
/
	COMMENT ON COLUMN "VDA"."VDA_V_PAYMENTS"."LAST_MODIFIED_DATE"
IS
  'The date the record was last modified on (Audit)';
/
	COMMENT ON COLUMN "VDA"."VDA_V_PAYMENTS"."CREATED_ON"
IS
  'The user who created the record (Audit)';
/
	COMMENT ON COLUMN "VDA"."VDA_V_PAYMENTS"."FK_STUDY"
IS
  'The Foreign Key to the study milestone is linked with';
/
	COMMENT ON COLUMN "VDA"."VDA_V_PAYMENTS"."FK_ACCOUNT"
IS
  'The account the milestone is linked with';
/
	COMMENT ON TABLE "VDA"."VDA_V_PAYMENTS"
IS
  'This view provides access to the payment information for studies';
/


-- VDA_V_SPECIMEN_LATESTSTATUS View

CREATE OR REPLACE FORCE VIEW "VDA"."VDA_V_SPECIMEN_LATESTSTATUS" ("PK_SPECIMEN_STATUS", "FK_SPECIMEN", "SS_DATE", "FK_CODELST_STATUS", "SS_QUANTITY", "SS_QUANTITY_UNITS", "SS_STATUS_BY", "RID", "CREATOR", "CREATED_ON", "IP_ADD", "SS_PROC_TYPE", "SS_PROC_DATE", "STATUS_DESC", "PROCESS_TYPE_DESC", "SPECIMEN_ID", "LAST_MODIFIED_BY", "SS_HAND_OFF_DATE", "SS_NOTES", "SS_TRACKING_NUMBER", "FK_USER_RECEPIENT", "STUDY_NUMBER", "SS_ACTION", "LAST_MODIFIED_DATE")
AS
  SELECT x.pk_specimen_status,
    x.fk_specimen,
    x.ss_date,
    x.fk_codelst_status,
    x.ss_quantity,
    eres.F_GET_CODELSTDESC (x.ss_quantity_units) ss_quantity_units,
    (SELECT USR_FIRSTNAME
      || ' '
      || USR_LASTNAME
    FROM eres.ER_USER
    WHERE PK_USER = x.ss_status_by
    ) ss_status_by,
    x.rid,
    (SELECT USR_FIRSTNAME
      || ' '
      || USR_LASTNAME
    FROM eres.ER_USER
    WHERE PK_USER = x.creator
    ) creator,
    x.created_on,
    x.ip_add,
    x.ss_proc_type,
    x.ss_proc_date,
    eres.F_GET_CODELSTDESC (fk_codelst_status) status_desc,
    eres.F_GET_CODELSTDESC (ss_proc_type) process_type_desc,
    (SELECT spec_id FROM eres.er_specimen sp WHERE sp.pk_specimen = x.fk_specimen
    ) specimen_id,
    (SELECT USR_FIRSTNAME
      || ' '
      || USR_LASTNAME
    FROM eres.ER_USER
    WHERE PK_USER = x.LAST_MODIFIED_BY
    ) LAST_MODIFIED_BY,
    x.SS_HAND_OFF_DATE,
    x.SS_NOTES,
    x.SS_TRACKING_NUMBER,
    (SELECT USR_FIRSTNAME
      || ' '
      || USR_LASTNAME
    FROM eres.ER_USER
    WHERE PK_USER = x.FK_USER_RECEPIENT
    ) FK_USER_RECEPIENT,
    (SELECT study_number FROM eres.er_study WHERE pk_study = x.FK_STUDY
    ) study_number,
    eres.F_GET_CODELSTDESC (SS_ACTION) SS_ACTION,
    x.LAST_MODIFIED_DATE
  FROM eres.er_specimen_status x,
    (SELECT c.fk_specimen,
      MAX (c.PK_SPECIMEN_STATUS) pk_specimen_status
    FROM eres.ER_SPECIMEN_STATUS c
    WHERE c.ss_date =
      (SELECT MAX (d.ss_date)
      FROM eres.ER_SPECIMEN_STATUS d
      WHERE c.fk_SPECIMEN = d.fk_SPECIMEN
      )
    GROUP BY fk_specimen
    ) o
  WHERE x.pk_specimen_status = o.pk_specimen_status;
  COMMENT ON COLUMN "VDA"."VDA_V_SPECIMEN_LATESTSTATUS"."PK_SPECIMEN_STATUS"
IS
  'The Primary key of the Specimen Status record';
/
	COMMENT ON COLUMN "VDA"."VDA_V_SPECIMEN_LATESTSTATUS"."FK_SPECIMEN"
IS
  'The FK to the main specimen record';
/
	COMMENT ON COLUMN "VDA"."VDA_V_SPECIMEN_LATESTSTATUS"."SS_DATE"
IS
  'The Status Date';
/
	 COMMENT ON COLUMN "VDA"."VDA_V_SPECIMEN_LATESTSTATUS"."FK_CODELST_STATUS"
IS
  'The codelist value of the specimen status';
/
	COMMENT ON COLUMN "VDA"."VDA_V_SPECIMEN_LATESTSTATUS"."SS_QUANTITY"
IS
  'The quantity number linked with status record';
/
	COMMENT ON COLUMN "VDA"."VDA_V_SPECIMEN_LATESTSTATUS"."SS_QUANTITY_UNITS"
IS
  'The quantity unit linked with status record';
/
	COMMENT ON COLUMN "VDA"."VDA_V_SPECIMEN_LATESTSTATUS"."SS_STATUS_BY"
IS
  'The user who entered the status';
/
	COMMENT ON COLUMN "VDA"."VDA_V_SPECIMEN_LATESTSTATUS"."RID"
IS
  'The unique Row ID for Audit';
/
	COMMENT ON COLUMN "VDA"."VDA_V_SPECIMEN_LATESTSTATUS"."CREATOR"
IS
  'The user who created the record (Audit)';
/
	COMMENT ON COLUMN "VDA"."VDA_V_SPECIMEN_LATESTSTATUS"."CREATED_ON"
IS
  'The date the record was created on (Audit)';
/
	COMMENT ON COLUMN "VDA"."VDA_V_SPECIMEN_LATESTSTATUS"."IP_ADD"
IS
  'The IP address of the client machine that initiated the session';
/
	COMMENT ON COLUMN "VDA"."VDA_V_SPECIMEN_LATESTSTATUS"."SS_PROC_TYPE"
IS
  'The processing type code';
/
	COMMENT ON COLUMN "VDA"."VDA_V_SPECIMEN_LATESTSTATUS"."SS_PROC_DATE"
IS
  'The prcessing date';
/
	COMMENT ON COLUMN "VDA"."VDA_V_SPECIMEN_LATESTSTATUS"."STATUS_DESC"
IS
  'The Status description';
/
	COMMENT ON COLUMN "VDA"."VDA_V_SPECIMEN_LATESTSTATUS"."PROCESS_TYPE_DESC"
IS
  'The Process Type description';
/
	COMMENT ON COLUMN "VDA"."VDA_V_SPECIMEN_LATESTSTATUS"."SPECIMEN_ID"
IS
  'The Specimen ID';
/
	COMMENT ON COLUMN "VDA"."VDA_V_SPECIMEN_LATESTSTATUS"."LAST_MODIFIED_BY"
IS
  'The user who last modified the record(Audit)';
/
	COMMENT ON COLUMN "VDA"."VDA_V_SPECIMEN_LATESTSTATUS"."SS_HAND_OFF_DATE"
IS
  'The date the specimen was handed over for processing';
/
	COMMENT ON COLUMN "VDA"."VDA_V_SPECIMEN_LATESTSTATUS"."SS_NOTES"
IS
  'The notes associated with the status';
/
	COMMENT ON COLUMN "VDA"."VDA_V_SPECIMEN_LATESTSTATUS"."SS_TRACKING_NUMBER"
IS
  'The tracking number linked with the status';
/
	COMMENT ON COLUMN "VDA"."VDA_V_SPECIMEN_LATESTSTATUS"."FK_USER_RECEPIENT"
IS
  'The recepient linked with the status';
/
	COMMENT ON COLUMN "VDA"."VDA_V_SPECIMEN_LATESTSTATUS"."STUDY_NUMBER"
IS
  'The study number linked with the status';
/
	COMMENT ON COLUMN "VDA"."VDA_V_SPECIMEN_LATESTSTATUS"."SS_ACTION"
IS
  'Action performed: incremented/decremented ';
/
	COMMENT ON COLUMN "VDA"."VDA_V_SPECIMEN_LATESTSTATUS"."LAST_MODIFIED_DATE"
IS
  'The date the record was last modified on (Audit)';
/
	COMMENT ON TABLE "VDA"."VDA_V_SPECIMEN_LATESTSTATUS"
IS
  'This view provides latest specimen status. The latest status is calculated by most recent date and most recent entry for that date (in case of more than one status entered on that date).';
/
	
INSERT INTO VDA_TRACK_PATCHES
(
  PATCH_PK       ,
  DB_VER_MJR     ,
  DB_VER_MNR     ,
  DB_PATCH_NAME  ,
  FIRED_ON       ,
  APP_VERSION    
) VALUES(SEQ_VDA_TRACK_PATCHES.nextval,5,5,'01_views.sql',sysdate,'VDA 1.4 Build012')
/

commit
/