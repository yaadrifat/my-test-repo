  CREATE OR REPLACE  VIEW "VDA"."VDA_V_STUDY_SUMMARY" ("PK_STUDY", "STUDY_NUMBER", "STUDY_ENTERED_BY", "STUDY_PI",
  "STUDY_COORDINATOR", "STUDY_MAJ_AUTH", "STUDY_TITLE", "STUDY_OBJECTIVE", "STUDY_SUMMARY", "STUDY_AGENT_DEVICE", 
  "STUDY_DIVISION", "STUDY_TAREA", "STUDY_DISEASE_SITE", "STUDY_ICDCODE1", "STUDY_ICDCODE2", "STUDY_NATSAMPSIZE", 
  "STUDY_DURATION", "STUDY_DURATN_UNIT", "STUDY_EST_BEGIN_DATE", "STUDY_PHASE", "STUDY_RESEARCH_TYPE", 
  "STUDY_TYPE", "STUDY_LINKED_TO", "STUDY_BLINDING", "STUDY_RANDOM", "STUDY_SPONSOR", "SPONSOR_CONTACT",
  "STUDY_INFO", "STUDY_KEYWRDS", "FK_ACCOUNT", "CREATED_ON", "LAST_MODIFIED_BY", "LAST_MODIFIED_DATE", 
  "CREATOR", "SPONSOR_DD", "STUDY_SPONSORID", "STUDY_ACTUALDT", "STUDY_INVIND_FLAG", "CTRP_ACCRREP_LASTRUN_DATE",
  "CTRP_ACCRUAL_GEN_DATE", "CTRP_CUTOFF_DATE", "CTRP_LAST_DOWNLOAD_BY", "NCI_TRIAL_IDENTIFIER", "NCT_NUMBER", 
  "STUDY_ENTERED_BY_FK", "STUDY_PI_FK", "STUDY_COORDINATOR_FK", "STUDY_DIVISION_FK", "STUDY_TAREA_FK", 
  "STUDY_DISEASE_SITE_FK", "STUDY_PHASE_FK", "STUDY_RESEARCH_TYPE_FK", "STUDY_TYPE_FK", "STUDY_BLINDING_FK",
  "STUDY_RANDOM_FK", "CREATOR_FK", "LAST_MODIFIED_BY_FK", "SPONSOR_DD_FK", "CTRP_LAST_DOWNLOAD_BY_FK", "STUDY_SCOPE",
  "RECORD_TYPE","STUDY_CTRP_REPORTABLE","FDA_REGULATED_STUDY") AS 
  SELECT   pk_study,
            o.STUDY_NUMBER,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = o.FK_AUTHOR)
               STUDY_ENTERED_BY,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = o.STUDY_PRINV)
               STUDY_PI,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   ER_USER.PK_USER = o.STUDY_COORDINATOR)
               STUDY_COORDINATOR,
            ERES.F_GEt_Yesno (o.STUDY_MAJ_AUTH) AS STUDY_MAJ_AUTH,
            STUDY_TITLE,
            STUDY_OBJ_CLOB STUDY_OBJECTIVE,
            STUDY_SUM_CLOB STUDY_SUMMARY,
            STUDY_PRODNAME STUDY_AGENT_DEVICE,
            ERES.F_GEt_Codelstdesc (o.STUDY_DIVISION) STUDY_DIVISION,
            ERES.F_GEt_Codelstdesc (o.FK_CODELST_TAREA) STUDY_TAREA,
            ERES.F_GEtdis_Site (o.STUDY_DISEASE_SITE) STUDY_DISEASE_SITE,
            STUDY_ICDCODE1,
            STUDY_ICDCODE2,
            STUDY_NSAMPLSIZE STUDY_NATSAMPSIZE,
            STUDY_DUR STUDY_DURATION,
            STUDY_DURUNIT STUDY_DURATN_UNIT,
            STUDY_ESTBEGINDT STUDY_EST_BEGIN_DATE,
            ERES.F_GEt_Codelstdesc (o.FK_CODELST_PHASE) STUDY_PHASE,
            ERES.F_GEt_Codelstdesc (o.FK_CODELST_RESTYPE) STUDY_RESEARCH_TYPE,
            ERES.F_GEt_Codelstdesc (o.FK_CODELST_TYPE) STUDY_TYPE,
            DECODE (o.STUDY_ASSOC,
                    NULL, NULL,
                    (SELECT   xx.study_number
                       FROM   eres.er_study xx
                      WHERE   xx.pk_study = o.STUDY_ASSOC))
               STUDY_LINKED_TO,
            ERES.F_GEt_Codelstdesc (o.FK_CODELST_BLIND) STUDY_BLINDING,
            ERES.F_GEt_Codelstdesc (o.FK_CODELST_RANDOM) STUDY_RANDOM,
            STUDY_SPONSOR,
            STUDY_CONTACT SPONSOR_CONTACT,
            STUDY_INFO,
            STUDY_KEYWRDS,
            FK_ACCOUNT,
            CREATED_ON,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = o.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            o.LAST_MODIFIED_DATE,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = o.CREATOR)
               CREATIOR,
            ERES.F_GEt_Codelstdesc (FK_CODELST_SPONSOR) SPONSOR_DD,
            STUDY_SPONSORID,
            STUDY_ACTUALDT,
            ERES.F_GEt_Yesno (o.STUDY_INVIND_FLAG) STUDY_INVIND_FLAG,
            CTRP_ACCRREP_LASTRUN_DATE,
            CTRP_ACCRUAL_GEN_DATE,
            CTRP_CUTOFF_DATE,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = o.CTRP_LAST_DOWNLOAD_BY)
               CTRP_LAST_DOWNLOAD_BY,
            NCI_TRIAL_IDENTIFIER,
            NCT_NUMBER,
            o.FK_AUTHOR AS STUDY_ENTERED_BY_FK,
            o.STUDY_PRINV AS STUDY_PI_FK,
            o.STUDY_COORDINATOR AS STUDY_COORDINATOR_FK,
            o.STUDY_DIVISION AS STUDY_DIVISION_FK,
            o.FK_CODELST_TAREA AS STUDY_TAREA_FK,
            o.STUDY_DISEASE_SITE AS STUDY_DISEASE_SITE_FK,
            o.FK_CODELST_PHASE AS STUDY_PHASE_FK,
            o.FK_CODELST_RESTYPE AS STUDY_RESEARCH_TYPE_FK,
            o.FK_CODELST_TYPE AS STUDY_TYPE_FK,
            o.FK_CODELST_BLIND AS STUDY_BLINDING_FK,
            o.FK_CODELST_RANDOM AS STUDY_RANDOM_FK,
            o.CREATOR AS CREATOR_FK,
            o.LAST_MODIFIED_BY AS LAST_MODIFIED_BY_FK,
            o.FK_CODELST_SPONSOR AS SPONSOR_DD_FK,
            o.CTRP_LAST_DOWNLOAD_BY AS CTRP_LAST_DOWNLOAD_BY_FK,
            ERES.F_GEt_Codelstdesc (o.FK_CODELST_SCOPE)  STUDY_SCOPE ,
            decode(LAST_MODIFIED_BY,null,'I','U') record_type, decode(STUDY_CTRP_REPORTABLE,1,'Yes','No') STUDY_CTRP_REPORTABLE, 
            decode(FDA_REGULATED_STUDY,1,'Yes','No')  FDA_REGULATED_STUDY  
              FROM   eres.ER_STUDY o;

 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."PK_STUDY" IS 'The Primary Key of the study';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."STUDY_NUMBER" IS 'The Study Number of the study the 
payment  is linked with';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."STUDY_ENTERED_BY" IS 'The user who entered the study 
in the application';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."STUDY_PI" IS 'The Principal Investigator of the study 
The study coordinator (as documented in the study summary)';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."STUDY_COORDINATOR" IS 'The study coordinator (as 
documented in the study summary)';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."STUDY_MAJ_AUTH" IS ' Is the selected Principal 
investigator is the major author of the study';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."STUDY_TITLE" IS 'The Study title';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."STUDY_OBJECTIVE" IS 'The Study Objective';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."STUDY_SUMMARY" IS 'The Study Summary';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."STUDY_AGENT_DEVICE" IS 'Agent or Device';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."STUDY_DIVISION" IS 'Study division';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."STUDY_TAREA" IS 'Study Therapeutic Area';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."STUDY_DISEASE_SITE" IS 'Study Disease Site';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."STUDY_ICDCODE1" IS 'ICD code 1 linked with the study';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."STUDY_ICDCODE2" IS 'ICD code 2 linked with the study';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."STUDY_NATSAMPSIZE" IS 'Study National Sample Size';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."STUDY_DURATION" IS 'Estimated duration of the study';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."STUDY_DURATN_UNIT" IS 'Estimated duration unit of the 
study';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."STUDY_EST_BEGIN_DATE" IS 'Estimated begin date of the 
study';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."STUDY_PHASE" IS 'Study Phase';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."STUDY_RESEARCH_TYPE" IS 'Study Research Type';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."STUDY_TYPE" IS 'Study Type';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."STUDY_LINKED_TO" IS 'Study linked with another study';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."STUDY_BLINDING" IS 'Study Blinding';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."STUDY_RANDOM" IS 'Study Randomization';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."STUDY_SPONSOR" IS 'Study Sponsor Name';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."SPONSOR_CONTACT" IS 'Study Sponsor Contact';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."STUDY_INFO" IS 'Study sponsor information';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."STUDY_KEYWRDS" IS 'Study keywords';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."FK_ACCOUNT" IS 'The account study is linked with';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."CREATED_ON" IS 'The date the record was created on 
(Audit)';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."LAST_MODIFIED_BY" IS 'The user who last modified the 
record(Audit)';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."LAST_MODIFIED_DATE" IS 'The date the  record was last 
modified on (Audit)';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."CREATOR" IS 'The user who created the record (Audit)';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."SPONSOR_DD" IS 'Study Sponsor';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."STUDY_SPONSORID" IS 'The Sponsor Id for Sponsor Other 
Information';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."STUDY_ACTUALDT" IS 'Study Actual Begin date';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."STUDY_INVIND_FLAG" IS 'Study INV IND Flag';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."CTRP_ACCRREP_LASTRUN_DATE" IS 'Date when last CTRP 
accrual was run';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."CTRP_ACCRUAL_GEN_DATE" IS 'The Last Date of the CTRP 
Accrual download';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."CTRP_CUTOFF_DATE" IS 'The Cutoff Date of the CTRP 
Accrual download';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."CTRP_LAST_DOWNLOAD_BY" IS 'The user who last 
downloaded the CTRP Accrual report';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."NCI_TRIAL_IDENTIFIER" IS 'The NCI Trial Identifier 
for the Study';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."NCT_NUMBER" IS 'The NCT Number for the Study';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."STUDY_ENTERED_BY_FK" IS 'The Key to identify the 
Study Entered By User.';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."STUDY_PI_FK" IS 'The Key to identify the Study PI 
User.';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."STUDY_COORDINATOR_FK" IS 'The Key to identify the 
Study Coordinator User.';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."STUDY_DIVISION_FK" IS 'The Key to identify the Study 
Division Code';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."STUDY_TAREA_FK" IS 'The Key to identify the Study 
Therapeutic Area Code';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."STUDY_DISEASE_SITE_FK" IS 'The Key to identify the 
Study Disease Sites Code';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."STUDY_PHASE_FK" IS 'The Key to identify the Study 
Phase Code';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."STUDY_RESEARCH_TYPE_FK" IS 'The Key to identify the 
Study Research Type Code';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."STUDY_TYPE_FK" IS 'The Key to identify the Study Type 
Key';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."STUDY_BLINDING_FK" IS 'The Key to identify the Study 
Blinding Key';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."STUDY_RANDOM_FK" IS 'The Key to identify the Study 
Randomization Code';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."CREATOR_FK" IS 'The Key to identify the Creator of 
the record.';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."LAST_MODIFIED_BY_FK" IS 'The Key to identify the Last 
Modified By of the record.';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."SPONSOR_DD_FK" IS 'The Key to identify the Sponsor DD 
Code';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."CTRP_LAST_DOWNLOAD_BY_FK" IS 'The Key to identify the 
CTRP Last Downloaded By User.';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."STUDY_SCOPE" IS 'The Study Scope';
 
   COMMENT ON TABLE "VDA"."VDA_V_STUDY_SUMMARY"  IS 'This view provides access to the study summary 
record';

  COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."RECORD_TYPE" IS 'The Record type, I= Insert, U=Update';
 
  COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."STUDY_CTRP_REPORTABLE" IS 'Flag to indicate if study is CTRP reportable';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_SUMMARY"."FDA_REGULATED_STUDY" IS 'Flag to indicate if study is FDA Regulated';
 
 
create or replace view vda.VDA_V_STUDY_INDIDE
 as
 Select
	i.FK_STUDY, 
	decode(i.INDIDE_TYPE, 1,'IND','IDE') INDIDE_TYPE , 
	i.INDIDE_NUMBER , 
	i.FK_CODELST_INDIDE_GRANTOR, 
	i.FK_CODELST_INDIDE_HOLDER, 
	i.FK_CODELST_PROGRAM_CODE, 
  ERES.F_GEt_Codelstdesc (i.FK_CODELST_INDIDE_GRANTOR) CODELST_INDIDE_GRANTOR_DESC,
  ERES.F_GEt_Codelstdesc (i.FK_CODELST_INDIDE_HOLDER) CODELST_INDIDE_HOLDER_DESC,
  ERES.F_GEt_Codelstdesc (i.FK_CODELST_PROGRAM_CODE) CODELST_PROGRAM_CODE_DESC,
   ERES.F_GEt_Codelstdesc (i.FK_CODELST_ACCESS_TYPE) ACCESS_TYPE_DESC,
	decode(i.INDIDE_EXPAND_ACCESS,1,'Yes','No') INDIDE_EXPAND_ACCESS, 
	i.FK_CODELST_ACCESS_TYPE, 
	decode(i.INDIDE_EXEMPT,1,'Yes','No')  INDIDE_EXEMPT, 
	i.RID, 
	i.IP_ADD , 
	i.CREATOR CREATOR_FK , 
	i.CREATED_ON, 
	i.LAST_MODIFIED_BY LAST_MODIFIED_BY_FK , 
	i.LAST_MODIFIED_DATE,
  s.study_number,
     (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = i.LAST_MODIFIED_BY) 
               LAST_MODIFIED_BY,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = i.CREATOR) CREATOR 
  from eres.ER_STUDY_INDIDE i, eres.er_study s 
  where pk_study = fk_study;
  

   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_INDIDE"."CREATOR_FK" IS 'The Key to identify the Creator of 
the record.';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_INDIDE"."LAST_MODIFIED_BY_FK" IS 'The Key to identify the Last 
Modified By of the record.';
 
  COMMENT ON COLUMN "VDA"."VDA_V_STUDY_INDIDE"."CREATED_ON" IS 'The date the record was created on 
(Audit)';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_INDIDE"."LAST_MODIFIED_BY" IS 'The user who last modified the 
record(Audit)';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_INDIDE"."LAST_MODIFIED_DATE" IS 'The date the  record was last 
modified on (Audit)';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_INDIDE"."CREATOR" IS 'The user who created the record (Audit)';
 
COMMENT ON COLUMN "VDA"."VDA_V_STUDY_INDIDE"."FK_STUDY" IS 'The Primary Key of the study';
 
   COMMENT ON COLUMN "VDA"."VDA_V_STUDY_INDIDE"."STUDY_NUMBER" IS 'The Study Number of the study the 
payment  is linked with';
 
 COMMENT ON COLUMN "VDA"."VDA_V_STUDY_INDIDE"."INDIDE_TYPE" IS 'This column Identifies whether record is IND or IDE Type';
COMMENT ON COLUMN "VDA"."VDA_V_STUDY_INDIDE"."INDIDE_NUMBER" IS 'This IND or IDE number';
COMMENT ON COLUMN "VDA"."VDA_V_STUDY_INDIDE"."FK_CODELST_INDIDE_GRANTOR" IS 'FK to er_codelst. Identifies IND or IDE Grantor';
COMMENT ON COLUMN "VDA"."VDA_V_STUDY_INDIDE"."FK_CODELST_INDIDE_HOLDER" IS 'FK to er_codelst. Identifies IND or IDE Holder';
COMMENT ON COLUMN "VDA"."VDA_V_STUDY_INDIDE"."FK_CODELST_PROGRAM_CODE" IS 'FK to er_codelst. Identifies IND or IDE Program Code';

COMMENT ON COLUMN "VDA"."VDA_V_STUDY_INDIDE"."CODELST_INDIDE_GRANTOR_DESC" IS 'The IND or IDE Grantor';
COMMENT ON COLUMN "VDA"."VDA_V_STUDY_INDIDE"."CODELST_INDIDE_HOLDER_DESC" IS 'The IND or IDE Holder';
COMMENT ON COLUMN "VDA"."VDA_V_STUDY_INDIDE"."CODELST_PROGRAM_CODE_DESC" IS 'The IND or IDE Program Code';
COMMENT ON COLUMN "VDA"."VDA_V_STUDY_INDIDE"."ACCESS_TYPE_DESC" IS 'The Expanded access type';

COMMENT ON COLUMN "VDA"."VDA_V_STUDY_INDIDE"."FK_CODELST_ACCESS_TYPE" IS 'FK to er_codelst. Identifies the Expanded access type';

COMMENT ON COLUMN "VDA"."VDA_V_STUDY_INDIDE"."INDIDE_EXPAND_ACCESS" IS 'Indicates whether the IND or IDE has expanded access';
COMMENT ON COLUMN "VDA"."VDA_V_STUDY_INDIDE"."RID" IS 'This column is used for the audit trail. Uniquely identifies the row in the database'; 

COMMENT ON COLUMN "VDA"."VDA_V_STUDY_INDIDE"."INDIDE_EXEMPT" IS 'Identifies INDIDE Exempt'; 

COMMENT ON TABLE "VDA"."VDA_V_STUDY_INDIDE"  IS 'This view provides access to the study IND/IDE Information';


INSERT INTO VDA_TRACK_PATCHES
(
  PATCH_PK       ,
  DB_VER_MJR     ,
  DB_VER_MNR     ,
  DB_PATCH_NAME  ,
  FIRED_ON       ,
  APP_VERSION    
) VALUES(SEQ_VDA_TRACK_PATCHES.nextval,5,10,'01_alter.sql',sysdate,'VDA 1.4 Build017')
/

commit
/
