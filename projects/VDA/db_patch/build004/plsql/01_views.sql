/* Formatted on 10/30/2012 11:41:08 AM (QP5 v5.115.810.9015) */
--
-- VDA_V_BUDGET_LINEITEM  (View)
--
--  Dependencies:
--   ERV_BUDGET (View)
--   PKG_DATEUTIL (Synonym)
--

CREATE OR REPLACE  VIEW VDA_V_BUDGET_LINEITEM
(
   PK_BUDGET,
   BUDGET_NAME,
   LINEITEM_NAME,
   PROT_CALENDAR,
   BUDGET_VERSION,
   BUDGET_DESC,
   STUDY_NUMBER,
   STUDY_TITLE,
   SITE_NAME,
   CATEGORY,
   BGTSECTION_NAME,
   CPT_CODE,
   COST_PER_PATIENT,
   TOTAL_COST,
   LINE_ITEM_INDIRECTS_FLAG,
   COST_DISCOUNT_ON_LINE_ITEM,
   INDIRECTS,
   BUDGET_INDIRECT_FLAG,
   FRINGE_BENEFIT,
   FRINGE_FLAG,
   PER_PATIENT_LINE_FRINGE,
   TOTAL_LINE_FRINGE,
   BUDGET_DISCOUNT,
   BUDGET_DISCOUNT_FLAG,
   TOTAL_COST_AFTER,
   TOTAL_PAT_COST_AFTER,
   PER_PAT_LINE_ITEM_DISCOUNT,
   TOTAL_COST_DISCOUNT,
   PERPAT_INDIRECT,
   TOTAL_COST_INDIRECT,
   BUDGET_CURRENCY,
   UNIT_COST,
   NUMBER_OF_UNIT,
   BGTSECTION_PATNO,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   STANDARD_OF_CARE,
   CATEGORY_SUBTYP,
   SPONSOR_AMOUNT,
   L_VARIANCE,
   FK_CODELST_COST_TYPE,
   COST_TYPE_DESC,
   LINEITEM_DIRECT_PERPAT,
   TOTAL_COST_PER_PAT,
   TOTAL_COST_ALL_PAT,
   PK_BUDGETSEC,
   BUDGETSEC_FKVISIT,
   PK_LINEITEM,
   LINEITEM_INPERSEC,
   BGTCAL_EXCLDSOCFLAG,
   BUDGETSECTION_SEQUENCE,
   LINEITEM_SEQ,
   L_LINEITEM_NAME,
   BGTSECTION_TYPE,
   SUBCOST_ITEM_FLAG
)
AS
   SELECT   pk_budget,
            BUDGET_NAME,
            LINEITEM_NAME,
            PROT_CALENDAR,
            BUDGET_VERSION,
            BUDGET_DESC,
            STUDY_NUMBER,
            STUDY_TITLE,
            SITE_NAME,
            CATEGORY,
            BGTSECTION_NAME,
            CPT_CODE,
            NVL (COST_PER_PATIENT, 0) AS COST_PER_PATIENT,
            NVL (TOTAL_COST, 0) AS TOTAL_COST,
            DECODE (LINE_ITEM_INDIRECTS_FLAG, 1, 'Yes', '')
               AS LINE_ITEM_INDIRECTS_FLAG,
            DECODE (COST_DISCOUNT_ON_LINE_ITEM, 1, 'Yes', '')
               AS COST_DISCOUNT_ON_LINE_ITEM,
            INDIRECTS,
            BUDGET_INDIRECT_FLAG,
            FRINGE_BENEFIT,
            FRINGE_FLAG,
            PER_PATIENT_LINE_FRINGE,
            TOTAL_LINE_FRINGE,
            BUDGET_DISCOUNT,
            DECODE (BUDGET_DISCOUNT_FLAG, 1, 'Discount', 2, 'Markup', '')
               AS BUDGET_DISCOUNT_FLAG,
            TOTAL_COST_AFTER,
            TOTAL_PAT_COST_AFTER,
            PER_PAT_LINE_ITEM_DISCOUNT,
            TOTAL_COST_DISCOUNT,
            PERPAT_INDIRECT,
            TOTAL_COST_INDIRECT,
            BUDGET_CURRENCY,
            UNIT_COST,
            NUMBER_OF_UNIT,
            NVL (BGTSECTION_PATNO, 1) BGTSECTION_PATNO,
            LAST_MODIFIED_BY,
            LAST_MODIFIED_DATE,
            DECODE (COST_CUSTOMCOL, 'research', 'No', 'soc', 'Yes', 'Other')
               AS standard_of_care,
            category_subtyp,
            LINEITEM_SPONSORAMOUNT AS SPONSOR_AMOUNT,
            LINEITEM_VARIANCE AS L_VARIANCE,
            FK_CODELST_COST_TYPE,
            COST_TYPE_DESC,
            lineitem_direct_perpat,
            total_cost_per_pat,
            total_cost_all_pat,
            pk_budgetsec,
            NVL (budgetsec_fkvisit, 0) budgetsec_fkvisit,
            pk_lineitem,
            lineitem_inpersec,
            NVL (bgtcal_excldsocflag, 0) AS bgtcal_excldsocflag,
            budgetsection_sequence,
            lineitem_seq,
            LOWER (LINEITEM_NAME) AS l_LINEITEM_NAME,
            BGTSECTION_TYPE,
            SUBCOST_ITEM_FLAG
      FROM   esch.erv_budget
/
COMMENT ON TABLE VDA_V_BUDGET_LINEITEM IS 'This view provides access to the Budgets with lineitem details'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.PK_BUDGET IS 'The Primary key of the budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.BUDGET_NAME IS 'The Budget Name'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.LINEITEM_NAME IS 'The Lineitem Name'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.PROT_CALENDAR IS 'The Protocol Calendar the Budget is linked with'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.BUDGET_VERSION IS 'The Budget version'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.BUDGET_DESC IS 'The Budget Description'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.STUDY_NUMBER IS 'The Study Number the budget is linked with'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.SITE_NAME IS 'The Site Name'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.CATEGORY IS 'The Lineitem category'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.BGTSECTION_NAME IS 'The Budget Section Name'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.CPT_CODE IS 'The CPT code linked with the lineitem'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.COST_PER_PATIENT IS 'The total cost per patient'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.STUDY_TITLE IS 'The study title the budget is linked with'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.TOTAL_COST IS 'The total cost of the lineitem'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.LINE_ITEM_INDIRECTS_FLAG IS 'The flag indicating if indirects should be applied The flag indicating if discounts should be applied'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.COST_DISCOUNT_ON_LINE_ITEM IS 'The flag indicating if discounts should be applied on the lineitem'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.INDIRECTS IS 'The indirect amount to be applied on the lineitem'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.BUDGET_INDIRECT_FLAG IS 'The flag indicating if indirects should be applied on the budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.FRINGE_BENEFIT IS 'The fringe benefit  amount on the lineitem'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.FRINGE_FLAG IS 'The flag indicating if fringe benefit should be applied on the budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.PER_PATIENT_LINE_FRINGE IS 'Per Patient fringe for lineitem'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.TOTAL_LINE_FRINGE IS 'Total lineitem fringe'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.BUDGET_DISCOUNT IS 'The discount amount on the lineitem'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.BUDGET_DISCOUNT_FLAG IS 'The flag indicating if discount should be applied on the budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.TOTAL_COST_AFTER IS 'Total cost per lineitem'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.TOTAL_PAT_COST_AFTER IS 'Total cost per lineitem per patient'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.PER_PAT_LINE_ITEM_DISCOUNT IS 'Per patient lineitem discount'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.TOTAL_COST_DISCOUNT IS 'Total cost discount'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.PERPAT_INDIRECT IS 'Per Patient indirect  amount'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.TOTAL_COST_INDIRECT IS 'Total indirect cost'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.BUDGET_CURRENCY IS 'The Budget Currency'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.UNIT_COST IS 'The unit cost of the lineitem'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.NUMBER_OF_UNIT IS 'The number of units to be budgeted'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.BGTSECTION_PATNO IS 'The Number of Patients a per patient section is calculated for'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.LAST_MODIFIED_DATE IS 'The date the  record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.STANDARD_OF_CARE IS 'The standard of care flag for the lineitem'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.CATEGORY_SUBTYP IS 'The category subype (per codelist)'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.SPONSOR_AMOUNT IS 'The sponsor amount linked with the lineitem'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.L_VARIANCE IS 'Varience amount varience between sponsor amount and budgeted amount'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.FK_CODELST_COST_TYPE IS 'The cost type linked with the lineitem'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.COST_TYPE_DESC IS 'The cost type description'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.LINEITEM_DIRECT_PERPAT IS 'Direct lineitem cost per patient'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.TOTAL_COST_PER_PAT IS 'Total Cost for per patient'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.TOTAL_COST_ALL_PAT IS 'Total Cost for all patients'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.PK_BUDGETSEC IS 'The Primary key of the Budget section record'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.BUDGETSEC_FKVISIT IS 'The Primary key of the visit record if lineitem is linked with a calendar visit'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.PK_LINEITEM IS 'The Primary key of the lineitem'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.LINEITEM_INPERSEC IS 'This column indicates if the cost should be included in all Sections of the budget or only in one section titled ''Personnel Costs'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.BGTCAL_EXCLDSOCFLAG IS 'The exclude Standard or Care flag for the Budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.BUDGETSECTION_SEQUENCE IS 'The budget section sequence'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.LINEITEM_SEQ IS 'The Lineitem sequence in a budget section'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.L_LINEITEM_NAME IS 'The lineitem name'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.BGTSECTION_TYPE IS 'The Budget Section Type'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.SUBCOST_ITEM_FLAG IS 'The Budget Subject Cost Item Flag 0 indicating if the line item originated from the Subject Cost Items'
/


/* Formatted on 10/30/2012 11:49:14 AM (QP5 v5.115.810.9015) */
--
-- VDA_V_BUDGET_MILESTONES  (View)
--
--  Dependencies:
--   ERV_BUDGET (View)
--   F_GET_CODELSTDESC (Function)
--   F_GET_MTYPE (Function)
--   VDA_V_MILESTONES (View)
--   PKG_DATEUTIL (Synonym)
--

CREATE OR REPLACE VIEW VDA_V_BUDGET_MILESTONES
(
   STUDY_NUMBER,
   STUDY_TITLE,
   MSRUL_CATEGORY,
   MSRUL_AMOUNT,
   MSRUL_PT_COUNT,
   MSRUL_PT_STATUS,
   MSRUL_LIMIT,
   MSRUL_PAY_TYPE,
   MSRUL_PAY_FOR,
   MSRUL_STATUS,
   MSRUL_PROT_CAL,
   MSRUL_VISIT,
   MSRUL_MS_RULE,
   MSRUL_EVENT_STAT,
   FK_ACCOUNT,
   PK_MILESTONE,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATED_ON,
   FK_STUDY,
   EVENT_NAME,
   MILESTONE_ACHIEVEDCOUNT,
   MILESTONE_ACHIEVEDAMOOUNT,
   LAST_CHECKED_ON,
   MILESTONE_DESCRIPTION,
   BUDGET_NAME,
   LINEITEM_NAME,
   PROT_CALENDAR,
   BGTSECTION_NAME
)
AS
   SELECT   m.STUDY_NUMBER,
            m.STUDY_TITLE,
            MSRUL_CATEGORY,
            MSRUL_AMOUNT,
            MSRUL_PT_COUNT,
            MSRUL_PT_STATUS,
            MSRUL_LIMIT,
            MSRUL_PAY_TYPE,
            MSRUL_PAY_FOR,
            MSRUL_STATUS,
            MSRUL_PROT_CAL,
            MSRUL_VISIT,
            MSRUL_MS_RULE,
            MSRUL_EVENT_STAT,
            FK_ACCOUNT,
            PK_MILESTONE,
            m.CREATOR,
            m.LAST_MODIFIED_BY,
            m.LAST_MODIFIED_DATE,
            m.CREATED_ON,
            m.FK_STUDY,
            EVENT_NAME,
            MILESTONE_ACHIEVEDCOUNT,
            MILESTONE_ACHIEVEDAMOUNT,
            LAST_CHECKED_ON,
            MILESTONE_DESCRIPTION,
            BUDGET_NAME,
            LINEITEM_NAME,
            PROT_CALENDAR,
            BGTSECTION_NAME
     FROM   VDA.VDA_V_MILESTONES m, esch.erv_budget
    WHERE       fk_budget = pk_budget
            AND FK_BGTCAL = pk_bgtcal
            AND FK_BGTSECTION = pk_budgetsec
            AND FK_LINEITEM = pk_lineitem
/
COMMENT ON TABLE VDA_V_BUDGET_MILESTONES IS 'This view provides data on milestones that are created from a budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.STUDY_NUMBER IS 'The Study Number'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.STUDY_TITLE IS 'The Study Title'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.MSRUL_CATEGORY IS 'The Milestone Category'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.MSRUL_AMOUNT IS 'The Milestone amount'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.MSRUL_PT_COUNT IS 'The Milestone Patient Count for the achievement rule'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.MSRUL_PT_STATUS IS 'The Milestone Patient Status for the achievement rule'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.MSRUL_LIMIT IS 'The Milestone Patient Status for the achievement rule'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.MSRUL_PAY_TYPE IS 'The Milestone Payment Type'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.MSRUL_PAY_FOR IS 'The Milestone Payment For'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.MSRUL_STATUS IS 'The Milestone Status for the achievement rule'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.MSRUL_PROT_CAL IS 'The Milestone Calendar for the achievement rule'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.MSRUL_VISIT IS 'The Milestone Visit for the achievement rule'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.MSRUL_MS_RULE IS 'The Milestone achievement rule'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.MSRUL_EVENT_STAT IS 'The Milestone Event Status for achievement the rule'
/


COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.FK_ACCOUNT IS 'The account the milestone is linked with'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.PK_MILESTONE IS 'The Primary Key of the Milestone'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.CREATOR IS 'The date the record was created on (Audit)'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.LAST_MODIFIED_DATE IS 'The date the  record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.CREATED_ON IS 'The date the  record was created on (Audit)'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.FK_STUDY IS 'The Foreign Key of the Study the milestone is linked with'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.EVENT_NAME IS 'The Milestone Event Name for achievement the rule'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.MILESTONE_ACHIEVEDCOUNT IS 'The Milestones achieved so far'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.MILESTONE_ACHIEVEDAMOOUNT IS 'The Milestones amount achieved so far'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.LAST_CHECKED_ON IS 'The Last checked date for the milestone achievement'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.MILESTONE_DESCRIPTION IS 'The Milestone description'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.BUDGET_NAME IS 'he name of the budget the MS is linked with'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.LINEITEM_NAME IS 'The name of the line item the MS is linked with'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.PROT_CALENDAR IS 'The name of the budget calendar the MS is linked with'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.BGTSECTION_NAME IS 'The name of the budget section the MS is linked with'
/

INSERT INTO VDA_TRACK_PATCHES
(
  PATCH_PK       ,
  DB_VER_MJR     ,
  DB_VER_MNR     ,
  DB_PATCH_NAME  ,
  FIRED_ON       ,
  APP_VERSION    
) VALUES(SEQ_VDA_TRACK_PATCHES.nextval,2,2,'01_views.sql',sysdate,'VDA 1.1 Build 2')
/

commit
/ 
