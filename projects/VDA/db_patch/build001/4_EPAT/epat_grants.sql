declare

    v_sql Varchar2(4000);
    
begin
    

    
    for k in (select object_name from all_objects where object_type='TABLE' and object_name not like 'BIN$%' and owner in ('EPAT'))
    loop
    
            v_sql := 'grant select on EPAT.' || k.object_name || ' to VDA';
            
             EXECUTE IMMEDIATE v_sql;
             
    end loop;
    
    for l in (select object_name from all_objects where object_type='VIEW' and object_name not like 'BIN$%' and owner in ('EPAT'))
    loop
    
            v_sql := 'grant select on EPAT.' || l.object_name || ' to VDA';
            
             EXECUTE IMMEDIATE v_sql;
             
    end loop;
    
end;
/
    
