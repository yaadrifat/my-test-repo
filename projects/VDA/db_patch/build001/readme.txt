README
=============================================================
1. Open file 1_newuserVDA\1_sys_VDAuser.SQL and replace <Path> occurrences with the absolution path to the ORA files on the database server. This path will indicate where and with what name Tablespace will be created

2. Execute 1_newuserVDA\1_sys_VDAuser.SQL as SYS user.

3. Login as ERES user and execute the following SQL from '2_eres' folder:
	- eres_grants.sql
	-2_eres_grants.sql
	
3. Login as ESCH user and execute the following SQL from '2_esch' folder:
	- esch_grants.sql

3. Login as EPAT user and execute the following SQL from '2_epat' folder:
	- epat_grants.sql

4. Login as VDA user and execute the following SQL from '3_VDA' folder:
	- 01_views.sql