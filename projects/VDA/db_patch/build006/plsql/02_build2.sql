CREATE OR REPLACE FORCE VIEW VDA_V_EPAT_AUDIT_UPDATE
(
   USER_NAME,
   TIMESTAMP,
   TABLE_NAME,
   COLUMN_NAME,
   OLD_VALUE,
   NEW_VALUE,
   REMARKS,caid,rid
)
AS
   SELECT   r.user_name,
            r.timestamp,
            r.table_name,
            c.column_name,
            c.old_value,
            c.new_value,
            c.remarks,caid,r.rid
     FROM   epat.audit_column c, epat.audit_row r
    WHERE   r.action = 'U' AND c.raid = r.raid
/
COMMENT ON TABLE VDA_V_EPAT_AUDIT_UPDATE IS 'This view provides access to the detail audit trail on Update actions in EPAT schema'
/

COMMENT ON COLUMN VDA_V_EPAT_AUDIT_UPDATE.USER_NAME IS 'The user who triggered the action'
/

COMMENT ON COLUMN VDA_V_EPAT_AUDIT_UPDATE.TIMESTAMP IS 'The date and time of the action'
/

COMMENT ON COLUMN VDA_V_EPAT_AUDIT_UPDATE.TABLE_NAME IS 'The name of the table'
/

COMMENT ON COLUMN VDA_V_EPAT_AUDIT_UPDATE.COLUMN_NAME IS 'The name of the column updated by the action'
/

COMMENT ON COLUMN VDA_V_EPAT_AUDIT_UPDATE.OLD_VALUE IS 'The old value of the column'
/

COMMENT ON COLUMN VDA_V_EPAT_AUDIT_UPDATE.NEW_VALUE IS 'The new value of the column'
/

COMMENT ON COLUMN VDA_V_EPAT_AUDIT_UPDATE.REMARKS IS 'The reason for change comments linked with the action'
/

COMMENT ON COLUMN VDA_V_EPAT_AUDIT_UPDATE.caid IS 'The Primary Key of the audit column change'
/

COMMENT ON COLUMN VDA_V_EPAT_AUDIT_UPDATE.rid IS 'The Primary Key of the audit transaction'
/


CREATE OR REPLACE FORCE VIEW VDA_V_PAT_ACCRUAL
(
   PSTAT_PAT_STUD_ID,
   PSTAT_ASSIGNED_TO,
   PSTAT_PHYSICIAN,
   PSTAT_TREAT_LOCAT,
   PSTAT_EVAL_FLAG,
   PSTAT_EVAL_STAT,
   PSTAT_INEVAL_STAT,
   PSTAT_SURVIVAL_STAT,
   PSTAT_RANDOM_NUM,
   PSTAT_ENROLLED_BY,
   PSTAT_ENROLL_SITE,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATED_ON,
   FK_PER,
   FK_STUDY,
   PATPROT_ENROLDT,
   PATPROT_TREATINGORG,
   FK_SITE_ENROLLING,
   STUDY_NUMBER,
   FK_ACCOUNT,
   PK_PATPROT
)
AS
   SELECT   PATPROT_PATSTDID PSTAT_PAT_STUD_ID,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = FK_USERASSTO)
               PSTAT_ASSIGNED_TO,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = PATPROT_PHYSICIAN)
               PSTAT_PHYSICIAN,
            ERES.F_GET_CODELSTDESC (FK_CODELSTLOC) PSTAT_TREAT_LOCAT,
            ERES.F_GET_CODELSTDESC (FK_CODELST_PTST_EVAL_FLAG)
               PSTAT_EVAL_FLAG,
            ERES.F_GET_CODELSTDESC (FK_CODELST_PTST_EVAL) PSTAT_EVAL_STAT,
            ERES.F_GET_CODELSTDESC (FK_CODELST_PTST_INEVAL) PSTAT_INEVAL_STAT,
            ERES.F_GET_CODELSTDESC (FK_CODELST_PTST_SURVIVAL) PSTAT_SURV_STAT,
            PATPROT_RANDOM PSTAT_RANDOM_NUM,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = pp.FK_USER)
               PSTAT_ENROLLED_BY,
            (SELECT   SITE_NAME
               FROM   eres.ER_SITE
              WHERE   PK_SITE = FK_SITE_ENROLLING)
               PSTAT_ENROLL_SITE,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = pp.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = pp.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            pp.LAST_MODIFIED_DATE,
            pp.CREATED_ON,
            fk_per,
            fk_study,
            patprot_enroldt,
            (SELECT   SITE_NAME
               FROM   eres.ER_SITE
              WHERE   PK_SITE = patprot_treatingorg)
               patprot_treatingorg,
            FK_SITE_ENROLLING,
            STUDY_NUMBER,
            FK_ACCOUNT,PK_PATPROT
     FROM   eres.ER_PATPROT pp, eres.er_study s
    WHERE      patprot_stat = 1
            AND patprot_enroldt IS NOT NULL
            AND s.pk_study = pp.fk_study
/
COMMENT ON TABLE VDA_V_PAT_ACCRUAL IS 'This view provides access to the patient accrual data for studies'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.STUDY_NUMBER IS 'The Study Number of the study the enrollment record  is linked with'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.FK_ACCOUNT IS 'The account the patient is linked with'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PSTAT_PAT_STUD_ID IS 'The patient study id'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PSTAT_ASSIGNED_TO IS 'The user patient is assigned to'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PSTAT_PHYSICIAN IS 'The physician patient is assigned to'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PSTAT_TREAT_LOCAT IS 'The treatment location'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PSTAT_EVAL_FLAG IS 'The evaluation flag'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PSTAT_EVAL_STAT IS 'The evaluation status'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PSTAT_INEVAL_STAT IS 'The inevaluation status'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PSTAT_SURVIVAL_STAT IS 'The survival status'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PSTAT_RANDOM_NUM IS 'The randomization number allocated to the patient'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PSTAT_ENROLLED_BY IS 'Patient Enrolled By'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PSTAT_ENROLL_SITE IS 'Patient Enrolling Site'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.LAST_MODIFIED_DATE IS 'The date the record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.CREATED_ON IS 'The date the record was created on (Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.FK_PER IS 'The foreign Key to the patient record'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.FK_STUDY IS 'The Foreign Key to the study milestone is linked with'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PATPROT_ENROLDT IS 'The enrollment date'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PATPROT_TREATINGORG IS 'The treating organization'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.FK_SITE_ENROLLING IS 'The foreign key to the enrolling site'
/

CREATE OR REPLACE FORCE VIEW VDA_V_STUDYSTAT
(
   STUDY_NUMBER,
   STUDY_START_DATE,
   STUDY_END_DATE,
   SSTAT_SITE_NAME,
   SSTAT_STUDY_STATUS,
   SSTAT_VALID_FROM,
   SSTAT_VALID_UNTIL,
   SSTAT_NOTES,
   SSTAT_DOCUMNTD_BY,
   SSTAT_END_DATE_SYSTEM,
   CREATOR,
   CREATED_ON,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   PK_STUDYSTAT,
   FK_STUDY,
   SSTAT_FK_SITE,
   FK_ACCOUNT,
   SSTAT_CURRENT_STAT 
)
AS
   SELECT   STUDY_NUMBER,
            STUDY_ACTUALDT,
            STUDY_END_DATE,
            (SELECT   SITE_NAME
               FROM   eres.ER_SITE
              WHERE   PK_SITE = FK_SITE)
               SSTAT_ORGANIZTN,
            ERES.F_GET_CODELSTDESC (FK_CODELST_STUDYSTAT) SSTAT_STUDY_STATUS,
            STUDYSTAT_DATE,
            STUDYSTAT_VALIDT,
            STUDYSTAT_NOTE SSTAT_NOTES,
            (SELECT   ER_USER.USR_FIRSTNAME || ' ' || ER_USER.USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = FK_USER_DOCBY)
               SSTAT_DOCUMNTD_BY,
            STUDYSTAT_ENDT SSTAT_END_DATE,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = ER_STUDYSTAT.CREATOR)
               CREATOR,
            ER_STUDYSTAT.CREATED_ON,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = ER_STUDYSTAT.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            ER_STUDYSTAT.LAST_MODIFIED_DATE,
            PK_STUDYSTAT,
            FK_STUDY,
            FK_SITE,
            FK_ACCOUNT,current_stat
     FROM   eres.ER_STUDYSTAT, eres.ER_STUDY
    WHERE   ER_STUDYSTAT.FK_STUDY = ER_STUDY.PK_STUDY
/
COMMENT ON TABLE VDA_V_STUDYSTAT IS 'This view provides access to the study status history data'
/

COMMENT ON COLUMN VDA_V_STUDYSTAT.STUDY_NUMBER IS 'The Study Number of the study status is linked with'
/

COMMENT ON COLUMN VDA_V_STUDYSTAT.STUDY_START_DATE IS 'The Study Status Start Date'
/

COMMENT ON COLUMN VDA_V_STUDYSTAT.STUDY_END_DATE IS 'The Study Status End Date'
/

COMMENT ON COLUMN VDA_V_STUDYSTAT.SSTAT_SITE_NAME IS 'The site the status is linked with'
/

COMMENT ON COLUMN VDA_V_STUDYSTAT.SSTAT_STUDY_STATUS IS 'The Study Status description'
/

COMMENT ON COLUMN VDA_V_STUDYSTAT.SSTAT_VALID_FROM IS 'The study status valid from'
/

COMMENT ON COLUMN VDA_V_STUDYSTAT.SSTAT_VALID_UNTIL IS 'The study status valid until'
/

COMMENT ON COLUMN VDA_V_STUDYSTAT.SSTAT_NOTES IS 'The study status notes'
/

COMMENT ON COLUMN VDA_V_STUDYSTAT.SSTAT_DOCUMNTD_BY IS 'The study status documented by'
/

COMMENT ON COLUMN VDA_V_STUDYSTAT.SSTAT_END_DATE_SYSTEM IS 'The study end date generated by the system'
/

COMMENT ON COLUMN VDA_V_STUDYSTAT.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYSTAT.CREATED_ON IS 'The date the record was created on (Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYSTAT.LAST_MODIFIED_BY IS 'The date the record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYSTAT.LAST_MODIFIED_DATE IS 'The date the record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYSTAT.PK_STUDYSTAT IS 'The Primary Key of the status record'
/

COMMENT ON COLUMN VDA_V_STUDYSTAT.FK_STUDY IS 'The Foreign key of the study the status is linked with'
/

COMMENT ON COLUMN VDA_V_STUDYSTAT.SSTAT_FK_SITE IS 'The Foreign Key to the Study Site record linked with the status'
/

COMMENT ON COLUMN VDA_V_STUDYSTAT.FK_ACCOUNT IS 'The account the query is linked with'

/

COMMENT ON COLUMN VDA_V_STUDYSTAT.SSTAT_CURRENT_STAT IS 'The study status marked as current'
/

CREATE OR REPLACE VIEW VDA_V_ERES_AUDIT_UPDATE
(
   USER_NAME,
   TIMESTAMP,
   TABLE_NAME,
   COLUMN_NAME,
   OLD_VALUE,
   NEW_VALUE,
   REMARKS,caid,raid
)
AS
   SELECT   r.user_name,
            r.timestamp,
            r.table_name,
            c.column_name,
            c.old_value,
            c.new_value,
            c.remarks,c.caid,r.raid
     FROM   eres.audit_column c, eres.audit_row r
    WHERE   r.action = 'U' AND c.raid = r.raid
/

COMMENT ON TABLE VDA_V_ERES_AUDIT_UPDATE IS 'This view provides access to the detail audit trail on Update actions in ERES schema'
/

COMMENT ON COLUMN VDA_V_ERES_AUDIT_UPDATE.USER_NAME IS 'The user who triggered the action'
/

COMMENT ON COLUMN VDA_V_ERES_AUDIT_UPDATE.TIMESTAMP IS 'The date and time of the action'
/

COMMENT ON COLUMN VDA_V_ERES_AUDIT_UPDATE.TABLE_NAME IS 'The name of the table'
/

COMMENT ON COLUMN VDA_V_ERES_AUDIT_UPDATE.COLUMN_NAME IS 'The name of the column updated by the action'
/

COMMENT ON COLUMN VDA_V_ERES_AUDIT_UPDATE.OLD_VALUE IS 'The old value of the column'
/

COMMENT ON COLUMN VDA_V_ERES_AUDIT_UPDATE.NEW_VALUE IS 'The new value of the column'
/

COMMENT ON COLUMN VDA_V_ERES_AUDIT_UPDATE.REMARKS IS 'The reason for change comments linked with the action'
/

COMMENT ON COLUMN VDA_V_ERES_AUDIT_UPDATE.caid IS 'The Primary Key of the audit column change'
/

COMMENT ON COLUMN VDA_V_ERES_AUDIT_UPDATE.raid IS 'The Primary Key of the audit transaction'
/


CREATE OR REPLACE  VIEW VDA_V_ESCH_AUDIT_UPDATE
(
   USER_NAME,
   TIMESTAMP,
   TABLE_NAME,
   COLUMN_NAME,
   OLD_VALUE,
   NEW_VALUE,
   REMARKS,caid,raid
)
AS
   SELECT   r.user_name,
            r.timestamp,
            r.table_name,
            c.column_name,
            c.old_value,
            c.new_value,
            c.remarks,c.caid,r.raid
     FROM   esch.audit_column c, esch.audit_row r
    WHERE   r.action = 'U' AND c.raid = r.raid
/
COMMENT ON TABLE VDA_V_ESCH_AUDIT_UPDATE IS 'This view provides access to the detail audit trail on Update actions in ESCH schema'
/

COMMENT ON COLUMN VDA_V_ESCH_AUDIT_UPDATE.USER_NAME IS 'The user who triggered the action'
/

COMMENT ON COLUMN VDA_V_ESCH_AUDIT_UPDATE.TIMESTAMP IS 'The date and time of the action'
/

COMMENT ON COLUMN VDA_V_ESCH_AUDIT_UPDATE.TABLE_NAME IS 'The name of the table'
/

COMMENT ON COLUMN VDA_V_ESCH_AUDIT_UPDATE.COLUMN_NAME IS 'The name of the column updated by the action'
/

COMMENT ON COLUMN VDA_V_ESCH_AUDIT_UPDATE.OLD_VALUE IS 'The old value of the column'
/

COMMENT ON COLUMN VDA_V_ESCH_AUDIT_UPDATE.NEW_VALUE IS 'The new value of the column'
/

COMMENT ON COLUMN VDA_V_ESCH_AUDIT_UPDATE.REMARKS IS 'The reason for change comments linked with the action'
/

COMMENT ON COLUMN VDA_V_ESCH_AUDIT_UPDATE.caid IS 'The Primary Key of the audit column change'
/

COMMENT ON COLUMN VDA_V_ESCH_AUDIT_UPDATE.raid IS 'The Primary Key of the audit transaction'
/

/* Formatted on 3/20/2013 9:59:03 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_ALL_PATSTUDYSTAT  (View)
--
--  Dependencies:
--   ER_PATPROT (Table)
--   ER_PATSTUDYSTAT (Table)
--   F_GET_CODELSTDESC (Function)
--   ER_SITE (Table)
--   ER_STUDY (Table)
--   ER_USER (Table)
--

CREATE OR REPLACE FORCE VIEW VDA_V_ALL_PATSTUDYSTAT
(
   PSTAT_STATUS,
   PSTAT_REASON,
   PSTAT_STAT_DATE,
   PSTAT_NOTES,
   PSTAT_PAT_STUD_ID,
   PSTAT_ASSIGNED_TO,
   PSTAT_PHYSICIAN,
   PSTAT_TREAT_LOCAT,
   PSTAT_EVAL_FLAG,
   PSTAT_EVAL_STAT,
   PSTAT_INEVAL_STAT,
   PSTAT_SURVIVAL_STAT,
   PSTAT_RANDOM_NUM,
   PSTAT_ENROLLED_BY,
   PSTAT_ENROLL_SITE,
   PSTAT_NEXT_FU_DATE,
   PSTAT_IC_VERS_NUM,
   PSTAT_SCREEN_NUM,
   PSTAT_SCREEN_BY,
   PSTAT_SCREEN_OUTCOME,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATED_ON,
   FK_PER,
   FK_STUDY,
   PSTAT_CURRENT_STAT,
   PATSTUDYSTAT_ENDT,
   STUDY_NUMBER,
   FK_ACCOUNT,PK_PATSTUDYSTAT
)
AS
   SELECT   eres.F_GET_CODELSTDESC (FK_CODELST_STAT) PSTAT_STATUS,
            eres.F_GET_CODELSTDESC (PATSTUDYSTAT_REASON) PSTAT_REASON,
            PATSTUDYSTAT_DATE PSTAT_STAT_DATE,
            PATSTUDYSTAT_NOTE PSTAT_NOTES,
            PATPROT_PATSTDID PSTAT_PAT_STUD_ID,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = FK_USERASSTO)
               PSTAT_ASSIGNED_TO,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = PATPROT_PHYSICIAN)
               PSTAT_PHYSICIAN,
            ERES.F_GET_CODELSTDESC (FK_CODELSTLOC) PSTAT_TREAT_LOCAT,
            ERES.F_GET_CODELSTDESC (FK_CODELST_PTST_EVAL_FLAG)
               PSTAT_EVAL_FLAG,
            ERES.F_GET_CODELSTDESC (FK_CODELST_PTST_EVAL) PSTAT_EVAL_STAT,
            ERES.F_GET_CODELSTDESC (FK_CODELST_PTST_INEVAL) PSTAT_INEVAL_STAT,
            ERES.F_GET_CODELSTDESC (FK_CODELST_PTST_SURVIVAL) PSTAT_SURV_STAT,
            PATPROT_RANDOM PSTAT_RANDOM_NUM,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = ER_PATPROT.FK_USER)
               PSTAT_ENROLLED_BY,
            (SELECT   SITE_NAME
               FROM   eres.ER_SITE
              WHERE   PK_SITE = FK_SITE_ENROLLING)
               PSTAT_ENROLL_SITE,
            ER_PATSTUDYSTAT.NEXT_FOLLOWUP_ON PSTAT_NEXT_FU_DATE,
            ER_PATSTUDYSTAT.INFORM_CONSENT_VER PSTAT_IC_VERS_NUM,
            SCREEN_NUMBER PSTAT_SCREEN_NUM,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = SCREENED_BY)
               PSTAT_SCREEN_BY,
            ERES.F_GET_CODELSTDESC (SCREENING_OUTCOME) PSTAT_SCREEN_OUTCOME,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = ER_PATSTUDYSTAT.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = ER_PATSTUDYSTAT.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            ER_PATSTUDYSTAT.LAST_MODIFIED_DATE,
            ER_PATSTUDYSTAT.CREATED_ON,
            ER_PATSTUDYSTAT.fk_per,
            ER_PATSTUDYSTAT.fk_study,
            ER_PATSTUDYSTAT.CURRENT_STAT,
            ER_PATSTUDYSTAT.PATSTUDYSTAT_ENDT,
            STUDY_NUMBER,
            FK_ACCOUNT,PK_PATSTUDYSTAT
     FROM   eres.ER_PATSTUDYSTAT, eres.ER_PATPROT, eres.ER_STUDY
    WHERE       ER_PATSTUDYSTAT.FK_PER = ER_PATPROT.FK_PER
            AND ER_PATSTUDYSTAT.FK_STUDY = ER_PATPROT.FK_STUDY
            AND ER_STUDY.PK_STUDY = ER_PATPROT.FK_STUDY
            AND patprot_stat = 1
/
COMMENT ON TABLE VDA_V_ALL_PATSTUDYSTAT IS 'This view provides access to all patient study statuses records'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.CREATOR IS 'The user who created the patient study status record (Audit)'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.LAST_MODIFIED_BY IS 'The user who last modified the patient study status record (Audit)'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.LAST_MODIFIED_DATE IS 'The date the patient study status record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.CREATED_ON IS 'The date the patient study status record was created on (Audit)'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.FK_PER IS 'The Foreign Key to the patient the patient study status record is linked with'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.FK_STUDY IS 'The Foreign Key to the study the patient study status record is linked with'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_CURRENT_STAT IS 'The flag to indicate if this is the current status of the patient on the study'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PATSTUDYSTAT_ENDT IS 'The date this status ended on - auto calculated as a new status with a higher date is added'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.STUDY_NUMBER IS 'The Study Number the Patient Study Status is linked with'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.FK_ACCOUNT IS 'The Account the Patient Study status is linked with'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_STATUS IS 'The Patient Study Status description'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_REASON IS 'The Reason the previous Patient Study Status is changed to the new status'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_STAT_DATE IS 'The date this Patient Study Status started on'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_NOTES IS 'The notes linked with the Patient Study Status'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_PAT_STUD_ID IS 'The Patient Study ID (Identifier) assigned to the patient on the study'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_ASSIGNED_TO IS 'The user the patient is assigned to'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_PHYSICIAN IS 'The Physician the patient is assigned to'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_TREAT_LOCAT IS 'The location of the treatment'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_EVAL_FLAG IS 'The evaluation flag linked with the Patient Study Status'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_EVAL_STAT IS 'The evaluation Status linked with the Patient Study Status'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_INEVAL_STAT IS 'The in-evaluation flag linked with the Patient Study Status'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_SURVIVAL_STAT IS 'Patient Survival Status information'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_RANDOM_NUM IS 'Patient randomization number'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_ENROLLED_BY IS 'The user who enrolled the patient'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_ENROLL_SITE IS 'The Site or Organization Patient is Enrolled for'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_NEXT_FU_DATE IS 'The Next follow up date for the Patient'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_IC_VERS_NUM IS 'Patient Informed Consent Version Number'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_SCREEN_NUM IS 'Patient Screening Number'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_SCREEN_BY IS 'The user who screened the patient'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_SCREEN_OUTCOME IS 'The screening outcome'
/
COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PK_PATSTUDYSTAT IS 'The Primary Key of the patient study status record'
/


CREATE OR REPLACE FORCE VIEW VDA_V_PAT_STATUS_RECENT
(
   PSTAT_STATUS,
   PSTAT_REASON,
   PSTAT_STAT_DATE,
   PSTAT_NOTES,
   PSTAT_PAT_STUD_ID,
   PSTAT_ASSIGNED_TO,
   PSTAT_PHYSICIAN,
   PSTAT_TREAT_LOCAT,
   PSTAT_EVAL_FLAG,
   PSTAT_EVAL_STAT,
   PSTAT_INEVAL_STAT,
   PSTAT_SURVIVAL_STAT,
   PSTAT_RANDOM_NUM,
   PSTAT_ENROLLED_BY,
   PSTAT_ENROLL_SITE,
   PSTAT_NEXT_FU_DATE,
   PSTAT_IC_VERS_NUM,
   PSTAT_SCREEN_NUM,
   PSTAT_SCREEN_BY,
   PSTAT_SCREEN_OUTCOME,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATED_ON,
   FK_PER,
   FK_STUDY,
   PSTAT_CURRENT_STAT, PK_PATSTUDYSTAT
)
AS
   SELECT   PSTAT_STATUS,
            PSTAT_REASON,
            PSTAT_STAT_DATE,
            PSTAT_NOTES,
            PSTAT_PAT_STUD_ID,
            PSTAT_ASSIGNED_TO,
            PSTAT_PHYSICIAN,
            PSTAT_TREAT_LOCAT,
            PSTAT_EVAL_FLAG,
            PSTAT_EVAL_STAT,
            PSTAT_INEVAL_STAT,
            PSTAT_SURVIVAL_STAT,
            PSTAT_RANDOM_NUM,
            PSTAT_ENROLLED_BY,
            PSTAT_ENROLL_SITE,
            PSTAT_NEXT_FU_DATE,
            PSTAT_IC_VERS_NUM,
            PSTAT_SCREEN_NUM,
            PSTAT_SCREEN_BY,
            PSTAT_SCREEN_OUTCOME,
            CREATOR,
            LAST_MODIFIED_BY,
            LAST_MODIFIED_DATE,
            CREATED_ON,
            FK_PER,
            FK_STUDY,
            PSTAT_CURRENT_STAT, PK_PATSTUDYSTAT
     FROM   VDA.VDA_V_ALL_PATSTUDYSTAT
    WHERE   PATSTUDYSTAT_ENDT IS NULL
/
COMMENT ON TABLE VDA_V_PAT_STATUS_RECENT IS 'This view provides access to the patient recent status information'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_STATUS IS 'The Patient Status Description'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_REASON IS 'The reason for the current status of the patient'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_STAT_DATE IS 'The Patient Status Date'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_NOTES IS 'The notes linked with the patient'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_PAT_STUD_ID IS 'The patient study id'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_ASSIGNED_TO IS 'The user patient is assigned to'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_PHYSICIAN IS 'The physician patient is assigned to'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_TREAT_LOCAT IS 'The treatment location'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_EVAL_FLAG IS 'The Evaluable Flag'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_EVAL_STAT IS 'The Evaluable Status'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_INEVAL_STAT IS 'The Unevaluable Status'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_SURVIVAL_STAT IS 'The Patient survival status'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_RANDOM_NUM IS 'The randomization number'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_ENROLLED_BY IS 'The user who enrolled the patient'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_ENROLL_SITE IS 'The patient enrolling site'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_NEXT_FU_DATE IS 'The next follow up date'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_IC_VERS_NUM IS 'The informed consent version number'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_SCREEN_NUM IS 'The screening number'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_SCREEN_BY IS 'The The user who screended the patient'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_SCREEN_OUTCOME IS 'The screening outcome'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.LAST_MODIFIED_DATE IS 'The date the  record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.CREATED_ON IS 'The date the record was created on (Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.FK_PER IS 'The Primary Key of the patient the schedule is linked with'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.FK_STUDY IS 'The Primary Key of the study schedule is linked with'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_CURRENT_STAT IS 'The patient status current status flag'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PK_PATSTUDYSTAT IS 'The Primary Key of patient study status record'
/

/* Formatted on 3/28/2013 10:25:04 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_PATFORMRESPONSES  (View)
--
--  Dependencies:
--   ER_FORMLIB (Table)
--   ER_FORMSLINEAR (Table)
--   ER_USER (Table)
--

CREATE OR REPLACE  VIEW VDA_V_PATFORMRESPONSES
(
   PK_FORMSLINEAR,
   FORM_NAME,
   FORM_DESC,
   EVENT_NAME,
   VISIT_NAME,
   PROTOCOL_NAME,
   FORM_VERSION,
   FK_FORM,
   FILLDATE,
   FORM_TYPE,
   PATIENT_PK,
   FK_PATPROT,
   COL1,
   COL2,
   COL3,
   COL4,
   COL5,
   COL6,
   COL7,
   COL8,
   COL9,
   COL10,
   COL11,
   COL12,
   COL13,
   COL14,
   COL15,
   COL16,
   COL17,
   COL18,
   COL19,
   COL20,
   COL21,
   COL22,
   COL23,
   COL24,
   COL25,
   COL26,
   COL27,
   COL28,
   COL29,
   COL30,
   COL31,
   COL32,
   COL33,
   COL34,
   COL35,
   COL36,
   COL37,
   COL38,
   COL39,
   COL40,
   COL41,
   COL42,
   COL43,
   COL44,
   COL45,
   COL46,
   COL47,
   COL48,
   COL49,
   COL50,
   COL51,
   COL52,
   COL53,
   COL54,
   COL55,
   COL56,
   COL57,
   COL58,
   COL59,
   COL60,
   COL61,
   COL62,
   COL63,
   COL64,
   COL65,
   COL66,
   COL67,
   COL68,
   COL69,
   COL70,
   COL71,
   COL72,
   COL73,
   COL74,
   COL75,
   COL76,
   COL77,
   COL78,
   COL79,
   COL80,
   COL81,
   COL82,
   COL83,
   COL84,
   COL85,
   COL86,
   COL87,
   COL88,
   COL89,
   COL90,
   COL91,
   COL92,
   COL93,
   COL94,
   COL95,
   COL96,
   COL97,
   COL98,
   COL99,
   COL100,
   COL101,
   COL102,
   COL103,
   COL104,
   COL105,
   COL106,
   COL107,
   COL108,
   COL109,
   COL110,
   COL111,
   COL112,
   COL113,
   COL114,
   COL115,
   COL116,
   COL117,
   COL118,
   COL119,
   COL120,
   COL121,
   COL122,
   COL123,
   COL124,
   COL125,
   COL126,
   COL127,
   COL128,
   COL129,
   COL130,
   COL131,
   COL132,
   COL133,
   COL134,
   COL135,
   COL136,
   COL137,
   COL138,
   COL139,
   COL140,
   COL141,
   COL142,
   COL143,
   COL144,
   COL145,
   COL146,
   COL147,
   COL148,
   COL149,
   COL150,
   COL151,
   COL152,
   COL153,
   COL154,
   COL155,
   COL156,
   COL157,
   COL158,
   COL159,
   COL160,
   COL161,
   COL162,
   COL163,
   COL164,
   COL165,
   COL166,
   COL167,
   COL168,
   COL169,
   COL170,
   COL171,
   COL172,
   COL173,
   COL174,
   COL175,
   COL176,
   COL177,
   COL178,
   COL179,
   COL180,
   COL181,
   COL182,
   COL183,
   COL184,
   COL185,
   COL186,
   COL187,
   COL188,
   COL189,
   COL190,
   COL191,
   COL192,
   COL193,
   COL194,
   COL195,
   COL196,
   COL197,
   COL198,
   COL199,
   COL200,
   COL201,
   COL202,
   COL203,
   COL204,
   COL205,
   COL206,
   COL207,
   COL208,
   COL209,
   COL210,
   COL211,
   COL212,
   COL213,
   COL214,
   COL215,
   COL216,
   COL217,
   COL218,
   COL219,
   COL220,
   COL221,
   COL222,
   COL223,
   COL224,
   COL225,
   COL226,
   COL227,
   COL228,
   COL229,
   COL230,
   COL231,
   COL232,
   COL233,
   COL234,
   COL235,
   COL236,
   COL237,
   COL238,
   COL239,
   COL240,
   COL241,
   COL242,
   COL243,
   COL244,
   COL245,
   COL246,
   COL247,
   COL248,
   COL249,
   COL250,
   COL251,
   COL252,
   COL253,
   COL254,
   COL255,
   COL256,
   COL257,
   COL258,
   COL259,
   COL260,
   COL261,
   COL262,
   COL263,
   COL264,
   COL265,
   COL266,
   COL267,
   COL268,
   COL269,
   COL270,
   COL271,
   COL272,
   COL273,
   COL274,
   COL275,
   COL276,
   COL277,
   COL278,
   COL279,
   COL280,
   COL281,
   COL282,
   COL283,
   COL284,
   COL285,
   COL286,
   COL287,
   COL288,
   COL289,
   COL290,
   COL291,
   COL292,
   COL293,
   COL294,
   COL295,
   COL296,
   COL297,
   COL298,
   COL299,
   COL300,
   COL301,
   COL302,
   COL303,
   COL304,
   COL305,
   COL306,
   COL307,
   COL308,
   COL309,
   COL310,
   COL311,
   COL312,
   COL313,
   COL314,
   COL315,
   COL316,
   COL317,
   COL318,
   COL319,
   COL320,
   COL321,
   COL322,
   COL323,
   COL324,
   COL325,
   COL326,
   COL327,
   COL328,
   COL329,
   COL330,
   COL331,
   COL332,
   COL333,
   COL334,
   COL335,
   COL336,
   COL337,
   COL338,
   COL339,
   COL340,
   COL341,
   COL342,
   COL343,
   COL344,
   COL345,
   COL346,
   COL347,
   COL348,
   COL349,
   COL350,
   COL351,
   COL352,
   COL353,
   COL354,
   COL355,
   COL356,
   COL357,
   COL358,
   COL359,
   COL360,
   COL361,
   COL362,
   COL363,
   COL364,
   COL365,
   COL366,
   COL367,
   COL368,
   COL369,
   COL370,
   COL371,
   COL372,
   COL373,
   COL374,
   COL375,
   COL376,
   COL377,
   COL378,
   COL379,
   COL380,
   COL381,
   COL382,
   COL383,
   COL384,
   COL385,
   COL386,
   COL387,
   COL388,
   COL389,
   COL390,
   COL391,
   COL392,
   COL393,
   COL394,
   COL395,
   COL396,
   COL397,
   COL398,
   COL399,
   COL400,
   COL401,
   COL402,
   COL403,
   COL404,
   COL405,
   COL406,
   COL407,
   COL408,
   COL409,
   COL410,
   COL411,
   COL412,
   COL413,
   COL414,
   COL415,
   COL416,
   COL417,
   COL418,
   COL419,
   COL420,
   COL421,
   COL422,
   COL423,
   COL424,
   COL425,
   COL426,
   COL427,
   COL428,
   COL429,
   COL430,
   COL431,
   COL432,
   COL433,
   COL434,
   COL435,
   COL436,
   COL437,
   COL438,
   COL439,
   COL440,
   COL441,
   COL442,
   COL443,
   COL444,
   COL445,
   COL446,
   COL447,
   COL448,
   COL449,
   COL450,
   COL451,
   COL452,
   COL453,
   COL454,
   COL455,
   COL456,
   COL457,
   COL458,
   COL459,
   COL460,
   COL461,
   COL462,
   COL463,
   COL464,
   COL465,
   COL466,
   COL467,
   COL468,
   COL469,
   COL470,
   COL471,
   COL472,
   COL473,
   COL474,
   COL475,
   COL476,
   COL477,
   COL478,
   COL479,
   COL480,
   COL481,
   COL482,
   COL483,
   COL484,
   COL485,
   COL486,
   COL487,
   COL488,
   COL489,
   COL490,
   COL491,
   COL492,
   COL493,
   COL494,
   COL495,
   COL496,
   COL497,
   COL498,
   COL499,
   COL500,
   COL501,
   COL502,
   COL503,
   COL504,
   COL505,
   COL506,
   COL507,
   COL508,
   COL509,
   COL510,
   COL511,
   COL512,
   COL513,
   COL514,
   COL515,
   COL516,
   COL517,
   COL518,
   COL519,
   COL520,
   COL521,
   COL522,
   COL523,
   COL524,
   COL525,
   COL526,
   COL527,
   COL528,
   COL529,
   COL530,
   COL531,
   COL532,
   COL533,
   COL534,
   COL535,
   COL536,
   COL537,
   COL538,
   COL539,
   COL540,
   COL541,
   COL542,
   COL543,
   COL544,
   COL545,
   COL546,
   COL547,
   COL548,
   COL549,
   COL550,
   COL551,
   COL552,
   COL553,
   COL554,
   COL555,
   COL556,
   COL557,
   COL558,
   COL559,
   COL560,
   COL561,
   COL562,
   COL563,
   COL564,
   COL565,
   COL566,
   COL567,
   COL568,
   COL569,
   COL570,
   COL571,
   COL572,
   COL573,
   COL574,
   COL575,
   COL576,
   COL577,
   COL578,
   COL579,
   COL580,
   COL581,
   COL582,
   COL583,
   COL584,
   COL585,
   COL586,
   COL587,
   COL588,
   COL589,
   COL590,
   COL591,
   COL592,
   COL593,
   COL594,
   COL595,
   COL596,
   COL597,
   COL598,
   COL599,
   COL600,
   COL601,
   COL602,
   COL603,
   COL604,
   COL605,
   COL606,
   COL607,
   COL608,
   COL609,
   COL610,
   COL611,
   COL612,
   COL613,
   COL614,
   COL615,
   COL616,
   COL617,
   COL618,
   COL619,
   COL620,
   COL621,
   COL622,
   COL623,
   COL624,
   COL625,
   COL626,
   COL627,
   COL628,
   COL629,
   COL630,
   COL631,
   COL632,
   COL633,
   COL634,
   COL635,
   COL636,
   COL637,
   COL638,
   COL639,
   COL640,
   COL641,
   COL642,
   COL643,
   COL644,
   COL645,
   COL646,
   COL647,
   COL648,
   COL649,
   COL650,
   COL651,
   COL652,
   COL653,
   COL654,
   COL655,
   COL656,
   COL657,
   COL658,
   COL659,
   COL660,
   COL661,
   COL662,
   COL663,
   COL664,
   COL665,
   COL666,
   COL667,
   COL668,
   COL669,
   COL670,
   COL671,
   COL672,
   COL673,
   COL674,
   COL675,
   COL676,
   COL677,
   COL678,
   COL679,
   COL680,
   COL681,
   COL682,
   COL683,
   COL684,
   COL685,
   COL686,
   COL687,
   COL688,
   COL689,
   COL690,
   COL691,
   COL692,
   COL693,
   COL694,
   COL695,
   COL696,
   COL697,
   COL698,
   COL699,
   COL700,
   COL701,
   COL702,
   COL703,
   COL704,
   COL705,
   COL706,
   COL707,
   COL708,
   COL709,
   COL710,
   COL711,
   COL712,
   COL713,
   COL714,
   COL715,
   COL716,
   COL717,
   COL718,
   COL719,
   COL720,
   COL721,
   COL722,
   COL723,
   COL724,
   COL725,
   COL726,
   COL727,
   COL728,
   COL729,
   COL730,
   COL731,
   COL732,
   COL733,
   COL734,
   COL735,
   COL736,
   COL737,
   COL738,
   COL739,
   COL740,
   COL741,
   COL742,
   COL743,
   COL744,
   COL745,
   COL746,
   COL747,
   COL748,
   COL749,
   COL750,
   COL751,
   COL752,
   COL753,
   COL754,
   COL755,
   COL756,
   COL757,
   COL758,
   COL759,
   COL760,
   COL761,
   COL762,
   COL763,
   COL764,
   COL765,
   COL766,
   COL767,
   COL768,
   COL769,
   COL770,
   COL771,
   COL772,
   COL773,
   COL774,
   COL775,
   COL776,
   COL777,
   COL778,
   COL779,
   COL780,
   COL781,
   COL782,
   COL783,
   COL784,
   COL785,
   COL786,
   COL787,
   COL788,
   COL789,
   COL790,
   COL791,
   COL792,
   COL793,
   COL794,
   COL795,
   COL796,
   COL797,
   COL798,
   COL799,
   COL800,
   COL801,
   COL802,
   COL803,
   COL804,
   COL805,
   COL806,
   COL807,
   COL808,
   COL809,
   COL810,
   COL811,
   COL812,
   COL813,
   COL814,
   COL815,
   COL816,
   COL817,
   COL818,
   COL819,
   COL820,
   COL821,
   COL822,
   COL823,
   COL824,
   COL825,
   COL826,
   COL827,
   COL828,
   COL829,
   COL830,
   COL831,
   COL832,
   COL833,
   COL834,
   COL835,
   COL836,
   COL837,
   COL838,
   COL839,
   COL840,
   COL841,
   COL842,
   COL843,
   COL844,
   COL845,
   COL846,
   COL847,
   COL848,
   COL849,
   COL850,
   COL851,
   COL852,
   COL853,
   COL854,
   COL855,
   COL856,
   COL857,
   COL858,
   COL859,
   COL860,
   COL861,
   COL862,
   COL863,
   COL864,
   COL865,
   COL866,
   COL867,
   COL868,
   COL869,
   COL870,
   COL871,
   COL872,
   COL873,
   COL874,
   COL875,
   COL876,
   COL877,
   COL878,
   COL879,
   COL880,
   COL881,
   COL882,
   COL883,
   COL884,
   COL885,
   COL886,
   COL887,
   COL888,
   COL889,
   COL890,
   COL891,
   COL892,
   COL893,
   COL894,
   COL895,
   COL896,
   COL897,
   COL898,
   COL899,
   COL900,
   COL901,
   COL902,
   COL903,
   COL904,
   COL905,
   COL906,
   COL907,
   COL908,
   COL909,
   COL910,
   COL911,
   COL912,
   COL913,
   COL914,
   COL915,
   COL916,
   COL917,
   COL918,
   COL919,
   COL920,
   COL921,
   COL922,
   COL923,
   COL924,
   COL925,
   COL926,
   COL927,
   COL928,
   COL929,
   COL930,
   COL931,
   COL932,
   COL933,
   COL934,
   COL935,
   COL936,
   COL937,
   COL938,
   COL939,
   COL940,
   COL941,
   COL942,
   COL943,
   COL944,
   COL945,
   COL946,
   COL947,
   COL948,
   COL949,
   COL950,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATED_ON,
   FK_SCH_EVENTS1,
   FK_SPECIMEN,
   FK_ACCOUNT,
   STUDY_NUMBER
)
AS
   SELECT   PK_FORMSLINEAR, form_name,
            form_desc,
            event_name,
            visit_name,
            protocol_name,
            form_version,
            fk_form,
            filldate,
            form_type,
            id AS patient_pk,
            fk_patprot,
            COL1,
            COL2,
            COL3,
            COL4,
            COL5,
            COL6,
            COL7,
            COL8,
            COL9,
            COL10,
            COL11,
            COL12,
            COL13,
            COL14,
            COL15,
            COL16,
            COL17,
            COL18,
            COL19,
            COL20,
            COL21,
            COL22,
            COL23,
            COL24,
            COL25,
            COL26,
            COL27,
            COL28,
            COL29,
            COL30,
            COL31,
            COL32,
            COL33,
            COL34,
            COL35,
            COL36,
            COL37,
            COL38,
            COL39,
            COL40,
            COL41,
            COL42,
            COL43,
            COL44,
            COL45,
            COL46,
            COL47,
            COL48,
            COL49,
            COL50,
            COL51,
            COL52,
            COL53,
            COL54,
            COL55,
            COL56,
            COL57,
            COL58,
            COL59,
            COL60,
            COL61,
            COL62,
            COL63,
            COL64,
            COL65,
            COL66,
            COL67,
            COL68,
            COL69,
            COL70,
            COL71,
            COL72,
            COL73,
            COL74,
            COL75,
            COL76,
            COL77,
            COL78,
            COL79,
            COL80,
            COL81,
            COL82,
            COL83,
            COL84,
            COL85,
            COL86,
            COL87,
            COL88,
            COL89,
            COL90,
            COL91,
            COL92,
            COL93,
            COL94,
            COL95,
            COL96,
            COL97,
            COL98,
            COL99,
            COL100,
            COL101,
            COL102,
            COL103,
            COL104,
            COL105,
            COL106,
            COL107,
            COL108,
            COL109,
            COL110,
            COL111,
            COL112,
            COL113,
            COL114,
            COL115,
            COL116,
            COL117,
            COL118,
            COL119,
            COL120,
            COL121,
            COL122,
            COL123,
            COL124,
            COL125,
            COL126,
            COL127,
            COL128,
            COL129,
            COL130,
            COL131,
            COL132,
            COL133,
            COL134,
            COL135,
            COL136,
            COL137,
            COL138,
            COL139,
            COL140,
            COL141,
            COL142,
            COL143,
            COL144,
            COL145,
            COL146,
            COL147,
            COL148,
            COL149,
            COL150,
            COL151,
            COL152,
            COL153,
            COL154,
            COL155,
            COL156,
            COL157,
            COL158,
            COL159,
            COL160,
            COL161,
            COL162,
            COL163,
            COL164,
            COL165,
            COL166,
            COL167,
            COL168,
            COL169,
            COL170,
            COL171,
            COL172,
            COL173,
            COL174,
            COL175,
            COL176,
            COL177,
            COL178,
            COL179,
            COL180,
            COL181,
            COL182,
            COL183,
            COL184,
            COL185,
            COL186,
            COL187,
            COL188,
            COL189,
            COL190,
            COL191,
            COL192,
            COL193,
            COL194,
            COL195,
            COL196,
            COL197,
            COL198,
            COL199,
            COL200,
            COL201,
            COL202,
            COL203,
            COL204,
            COL205,
            COL206,
            COL207,
            COL208,
            COL209,
            COL210,
            COL211,
            COL212,
            COL213,
            COL214,
            COL215,
            COL216,
            COL217,
            COL218,
            COL219,
            COL220,
            COL221,
            COL222,
            COL223,
            COL224,
            COL225,
            COL226,
            COL227,
            COL228,
            COL229,
            COL230,
            COL231,
            COL232,
            COL233,
            COL234,
            COL235,
            COL236,
            COL237,
            COL238,
            COL239,
            COL240,
            COL241,
            COL242,
            COL243,
            COL244,
            COL245,
            COL246,
            COL247,
            COL248,
            COL249,
            COL250,
            COL251,
            COL252,
            COL253,
            COL254,
            COL255,
            COL256,
            COL257,
            COL258,
            COL259,
            COL260,
            COL261,
            COL262,
            COL263,
            COL264,
            COL265,
            COL266,
            COL267,
            COL268,
            COL269,
            COL270,
            COL271,
            COL272,
            COL273,
            COL274,
            COL275,
            COL276,
            COL277,
            COL278,
            COL279,
            COL280,
            COL281,
            COL282,
            COL283,
            COL284,
            COL285,
            COL286,
            COL287,
            COL288,
            COL289,
            COL290,
            COL291,
            COL292,
            COL293,
            COL294,
            COL295,
            COL296,
            COL297,
            COL298,
            COL299,
            COL300,
            COL301,
            COL302,
            COL303,
            COL304,
            COL305,
            COL306,
            COL307,
            COL308,
            COL309,
            COL310,
            COL311,
            COL312,
            COL313,
            COL314,
            COL315,
            COL316,
            COL317,
            COL318,
            COL319,
            COL320,
            COL321,
            COL322,
            COL323,
            COL324,
            COL325,
            COL326,
            COL327,
            COL328,
            COL329,
            COL330,
            COL331,
            COL332,
            COL333,
            COL334,
            COL335,
            COL336,
            COL337,
            COL338,
            COL339,
            COL340,
            COL341,
            COL342,
            COL343,
            COL344,
            COL345,
            COL346,
            COL347,
            COL348,
            COL349,
            COL350,
            COL351,
            COL352,
            COL353,
            COL354,
            COL355,
            COL356,
            COL357,
            COL358,
            COL359,
            COL360,
            COL361,
            COL362,
            COL363,
            COL364,
            COL365,
            COL366,
            COL367,
            COL368,
            COL369,
            COL370,
            COL371,
            COL372,
            COL373,
            COL374,
            COL375,
            COL376,
            COL377,
            COL378,
            COL379,
            COL380,
            COL381,
            COL382,
            COL383,
            COL384,
            COL385,
            COL386,
            COL387,
            COL388,
            COL389,
            COL390,
            COL391,
            COL392,
            COL393,
            COL394,
            COL395,
            COL396,
            COL397,
            COL398,
            COL399,
            COL400,
            COL401,
            COL402,
            COL403,
            COL404,
            COL405,
            COL406,
            COL407,
            COL408,
            COL409,
            COL410,
            COL411,
            COL412,
            COL413,
            COL414,
            COL415,
            COL416,
            COL417,
            COL418,
            COL419,
            COL420,
            COL421,
            COL422,
            COL423,
            COL424,
            COL425,
            COL426,
            COL427,
            COL428,
            COL429,
            COL430,
            COL431,
            COL432,
            COL433,
            COL434,
            COL435,
            COL436,
            COL437,
            COL438,
            COL439,
            COL440,
            COL441,
            COL442,
            COL443,
            COL444,
            COL445,
            COL446,
            COL447,
            COL448,
            COL449,
            COL450,
            COL451,
            COL452,
            COL453,
            COL454,
            COL455,
            COL456,
            COL457,
            COL458,
            COL459,
            COL460,
            COL461,
            COL462,
            COL463,
            COL464,
            COL465,
            COL466,
            COL467,
            COL468,
            COL469,
            COL470,
            COL471,
            COL472,
            COL473,
            COL474,
            COL475,
            COL476,
            COL477,
            COL478,
            COL479,
            COL480,
            COL481,
            COL482,
            COL483,
            COL484,
            COL485,
            COL486,
            COL487,
            COL488,
            COL489,
            COL490,
            COL491,
            COL492,
            COL493,
            COL494,
            COL495,
            COL496,
            COL497,
            COL498,
            COL499,
            COL500,
            COL501,
            COL502,
            COL503,
            COL504,
            COL505,
            COL506,
            COL507,
            COL508,
            COL509,
            COL510,
            COL511,
            COL512,
            COL513,
            COL514,
            COL515,
            COL516,
            COL517,
            COL518,
            COL519,
            COL520,
            COL521,
            COL522,
            COL523,
            COL524,
            COL525,
            COL526,
            COL527,
            COL528,
            COL529,
            COL530,
            COL531,
            COL532,
            COL533,
            COL534,
            COL535,
            COL536,
            COL537,
            COL538,
            COL539,
            COL540,
            COL541,
            COL542,
            COL543,
            COL544,
            COL545,
            COL546,
            COL547,
            COL548,
            COL549,
            COL550,
            COL551,
            COL552,
            COL553,
            COL554,
            COL555,
            COL556,
            COL557,
            COL558,
            COL559,
            COL560,
            COL561,
            COL562,
            COL563,
            COL564,
            COL565,
            COL566,
            COL567,
            COL568,
            COL569,
            COL570,
            COL571,
            COL572,
            COL573,
            COL574,
            COL575,
            COL576,
            COL577,
            COL578,
            COL579,
            COL580,
            COL581,
            COL582,
            COL583,
            COL584,
            COL585,
            COL586,
            COL587,
            COL588,
            COL589,
            COL590,
            COL591,
            COL592,
            COL593,
            COL594,
            COL595,
            COL596,
            COL597,
            COL598,
            COL599,
            COL600,
            COL601,
            COL602,
            COL603,
            COL604,
            COL605,
            COL606,
            COL607,
            COL608,
            COL609,
            COL610,
            COL611,
            COL612,
            COL613,
            COL614,
            COL615,
            COL616,
            COL617,
            COL618,
            COL619,
            COL620,
            COL621,
            COL622,
            COL623,
            COL624,
            COL625,
            COL626,
            COL627,
            COL628,
            COL629,
            COL630,
            COL631,
            COL632,
            COL633,
            COL634,
            COL635,
            COL636,
            COL637,
            COL638,
            COL639,
            COL640,
            COL641,
            COL642,
            COL643,
            COL644,
            COL645,
            COL646,
            COL647,
            COL648,
            COL649,
            COL650,
            COL651,
            COL652,
            COL653,
            COL654,
            COL655,
            COL656,
            COL657,
            COL658,
            COL659,
            COL660,
            COL661,
            COL662,
            COL663,
            COL664,
            COL665,
            COL666,
            COL667,
            COL668,
            COL669,
            COL670,
            COL671,
            COL672,
            COL673,
            COL674,
            COL675,
            COL676,
            COL677,
            COL678,
            COL679,
            COL680,
            COL681,
            COL682,
            COL683,
            COL684,
            COL685,
            COL686,
            COL687,
            COL688,
            COL689,
            COL690,
            COL691,
            COL692,
            COL693,
            COL694,
            COL695,
            COL696,
            COL697,
            COL698,
            COL699,
            COL700,
            COL701,
            COL702,
            COL703,
            COL704,
            COL705,
            COL706,
            COL707,
            COL708,
            COL709,
            COL710,
            COL711,
            COL712,
            COL713,
            COL714,
            COL715,
            COL716,
            COL717,
            COL718,
            COL719,
            COL720,
            COL721,
            COL722,
            COL723,
            COL724,
            COL725,
            COL726,
            COL727,
            COL728,
            COL729,
            COL730,
            COL731,
            COL732,
            COL733,
            COL734,
            COL735,
            COL736,
            COL737,
            COL738,
            COL739,
            COL740,
            COL741,
            COL742,
            COL743,
            COL744,
            COL745,
            COL746,
            COL747,
            COL748,
            COL749,
            COL750,
            COL751,
            COL752,
            COL753,
            COL754,
            COL755,
            COL756,
            COL757,
            COL758,
            COL759,
            COL760,
            COL761,
            COL762,
            COL763,
            COL764,
            COL765,
            COL766,
            COL767,
            COL768,
            COL769,
            COL770,
            COL771,
            COL772,
            COL773,
            COL774,
            COL775,
            COL776,
            COL777,
            COL778,
            COL779,
            COL780,
            COL781,
            COL782,
            COL783,
            COL784,
            COL785,
            COL786,
            COL787,
            COL788,
            COL789,
            COL790,
            COL791,
            COL792,
            COL793,
            COL794,
            COL795,
            COL796,
            COL797,
            COL798,
            COL799,
            COL800,
            COL801,
            COL802,
            COL803,
            COL804,
            COL805,
            COL806,
            COL807,
            COL808,
            COL809,
            COL810,
            COL811,
            COL812,
            COL813,
            COL814,
            COL815,
            COL816,
            COL817,
            COL818,
            COL819,
            COL820,
            COL821,
            COL822,
            COL823,
            COL824,
            COL825,
            COL826,
            COL827,
            COL828,
            COL829,
            COL830,
            COL831,
            COL832,
            COL833,
            COL834,
            COL835,
            COL836,
            COL837,
            COL838,
            COL839,
            COL840,
            COL841,
            COL842,
            COL843,
            COL844,
            COL845,
            COL846,
            COL847,
            COL848,
            COL849,
            COL850,
            COL851,
            COL852,
            COL853,
            COL854,
            COL855,
            COL856,
            COL857,
            COL858,
            COL859,
            COL860,
            COL861,
            COL862,
            COL863,
            COL864,
            COL865,
            COL866,
            COL867,
            COL868,
            COL869,
            COL870,
            COL871,
            COL872,
            COL873,
            COL874,
            COL875,
            COL876,
            COL877,
            COL878,
            COL879,
            COL880,
            COL881,
            COL882,
            COL883,
            COL884,
            COL885,
            COL886,
            COL887,
            COL888,
            COL889,
            COL890,
            COL891,
            COL892,
            COL893,
            COL894,
            COL895,
            COL896,
            COL897,
            COL898,
            COL899,
            COL900,
            COL901,
            COL902,
            COL903,
            COL904,
            COL905,
            COL906,
            COL907,
            COL908,
            COL909,
            COL910,
            COL911,
            COL912,
            COL913,
            COL914,
            COL915,
            COL916,
            COL917,
            COL918,
            COL919,
            COL920,
            COL921,
            COL922,
            COL923,
            COL924,
            COL925,
            COL926,
            COL927,
            COL928,
            COL929,
            COL930,
            COL931,
            COL932,
            COL933,
            COL934,
            COL935,
            COL936,
            COL937,
            COL938,
            COL939,
            COL940,
            COL941,
            COL942,
            COL943,
            COL944,
            COL945,
            COL946,
            COL947,
            COL948,
            COL949,
            COL950,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = l.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = l.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            l.LAST_MODIFIED_DATE,
            l.CREATED_ON,
            FK_SCH_EVENTS1,
            FK_SPECIMEN,
            eres.er_formlib.fk_account,
			(Select study_number from vda.VDA_V_PAT_ACCRUAL where pk_patprot = FK_PATPROT) STUDY_NUMBER
     FROM   eres.er_formslinear l, eres.er_formlib
    WHERE   form_type = 'P' AND fk_form = pk_formlib
/
COMMENT ON TABLE VDA_V_PATFORMRESPONSES IS 'This view provides access to responses to all study level forms'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL938 IS 'Col938 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL939 IS 'Col939 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL940 IS 'Col940 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL941 IS 'Col941 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL942 IS 'Col942 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL943 IS 'Col943 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL944 IS 'Col944 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL945 IS 'Col945 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL946 IS 'Col946 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL947 IS 'Col947 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL948 IS 'Col948 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL949 IS 'Col949 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL950 IS 'Col950 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.CREATOR IS 'User who created record'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.LAST_MODIFIED_BY IS 'User who last modified the record'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.LAST_MODIFIED_DATE IS 'Last modified date'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.CREATED_ON IS 'Date record was created on'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.FK_SCH_EVENTS1 IS 'ID to identify the patient scheduled event record the response is linked with'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.FK_SPECIMEN IS 'ID to identify the specimen a response is linked with'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.FK_ACCOUNT IS 'The account the response is linked with. Applicable to Velos hosted or shared environments'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.FORM_DESC IS 'Form Description'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.EVENT_NAME IS 'The name of the calendar event the response is linked with'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.VISIT_NAME IS 'The name of the calendar visit the response is linked with'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.PROTOCOL_NAME IS 'The name of the calendar the response is linked with'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.FORM_VERSION IS 'Form version number'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.FK_FORM IS 'ID to identify a form record uniquely'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.FILLDATE IS 'Date the response was filled on'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.FORM_NAME IS 'Form name'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.FORM_TYPE IS 'Type of the  form- always A for account level forms'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.PATIENT_PK IS 'ID to identify the patient the response is linked with'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.FK_PATPROT IS 'ID to identify the patient-study record the response is linked with'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL1 IS 'Col1 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL2 IS 'Col2 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL3 IS 'Col3 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL4 IS 'Col4 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL5 IS 'Col5 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL6 IS 'Col6 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL7 IS 'Col7 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL8 IS 'Col8 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL9 IS 'Col9 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL10 IS 'Col10 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL11 IS 'Col11 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL12 IS 'Col12 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL13 IS 'Col13 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL14 IS 'Col14 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL15 IS 'Col15 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL16 IS 'Col16 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL17 IS 'Col17 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL18 IS 'Col18 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL19 IS 'Col19 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL20 IS 'Col20 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL21 IS 'Col21 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL22 IS 'Col22 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL23 IS 'Col23 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL24 IS 'Col24 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL25 IS 'Col25 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL26 IS 'Col26 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL27 IS 'Col27 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL28 IS 'Col28 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL29 IS 'Col29 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL30 IS 'Col30 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL31 IS 'Col31 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL32 IS 'Col32 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL33 IS 'Col33 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL34 IS 'Col34 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL35 IS 'Col35 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL36 IS 'Col36 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL37 IS 'Col37 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL38 IS 'Col38 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL39 IS 'Col39 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL40 IS 'Col40 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL41 IS 'Col41 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL42 IS 'Col42 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL43 IS 'Col43 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL44 IS 'Col44 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL45 IS 'Col45 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL46 IS 'Col46 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL47 IS 'Col47 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL48 IS 'Col48 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL49 IS 'Col49 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL50 IS 'Col50 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL51 IS 'Col51 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL52 IS 'Col52 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL53 IS 'Col53 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL54 IS 'Col54 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL55 IS 'Col55 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL56 IS 'Col56 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL57 IS 'Col57 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL58 IS 'Col58 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL59 IS 'Col59 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL60 IS 'Col60 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL61 IS 'Col61 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL62 IS 'Col62 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL63 IS 'Col63 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL64 IS 'Col64 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL65 IS 'Col65 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL66 IS 'Col66 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL67 IS 'Col67 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL68 IS 'Col68 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL69 IS 'Col69 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL70 IS 'Col70 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL71 IS 'Col71 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL72 IS 'Col72 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL73 IS 'Col73 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL74 IS 'Col74 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL75 IS 'Col75 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL76 IS 'Col76 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL77 IS 'Col77 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL78 IS 'Col78 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL79 IS 'Col79 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL80 IS 'Col80 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL81 IS 'Col81 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL82 IS 'Col82 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL83 IS 'Col83 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL84 IS 'Col84 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL85 IS 'Col85 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL86 IS 'Col86 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL87 IS 'Col87 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL88 IS 'Col88 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL89 IS 'Col89 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL90 IS 'Col90 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL91 IS 'Col91 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL92 IS 'Col92 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL93 IS 'Col93 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL94 IS 'Col94 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL95 IS 'Col95 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL96 IS 'Col96 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL97 IS 'Col97 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL98 IS 'Col98 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL99 IS 'Col99 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL100 IS 'Col100 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL101 IS 'Col101 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL102 IS 'Col102 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL103 IS 'Col103 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL104 IS 'Col104 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL105 IS 'Col105 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL106 IS 'Col106 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL107 IS 'Col107 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL108 IS 'Col108 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL109 IS 'Col109 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL110 IS 'Col110 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL111 IS 'Col111 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL112 IS 'Col112 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL113 IS 'Col113 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL114 IS 'Col114 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL115 IS 'Col115 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL116 IS 'Col116 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL117 IS 'Col117 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL118 IS 'Col118 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL119 IS 'Col119 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL120 IS 'Col120 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL121 IS 'Col121 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL122 IS 'Col122 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL123 IS 'Col123 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL124 IS 'Col124 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL125 IS 'Col125 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL126 IS 'Col126 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL127 IS 'Col127 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL128 IS 'Col128 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL129 IS 'Col129 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL130 IS 'Col130 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL131 IS 'Col131 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL132 IS 'Col132 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL133 IS 'Col133 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL134 IS 'Col134 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL135 IS 'Col135 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL136 IS 'Col136 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL137 IS 'Col137 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL138 IS 'Col138 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL139 IS 'Col139 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL140 IS 'Col140 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL141 IS 'Col141 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL142 IS 'Col142 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL143 IS 'Col143 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL144 IS 'Col144 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL145 IS 'Col145 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL146 IS 'Col146 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL147 IS 'Col147 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL148 IS 'Col148 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL149 IS 'Col149 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL150 IS 'Col150 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL151 IS 'Col151 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL152 IS 'Col152 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL153 IS 'Col153 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL154 IS 'Col154 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL155 IS 'Col155 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL156 IS 'Col156 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL157 IS 'Col157 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL158 IS 'Col158 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL159 IS 'Col159 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL160 IS 'Col160 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL161 IS 'Col161 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL162 IS 'Col162 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL163 IS 'Col163 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL164 IS 'Col164 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL165 IS 'Col165 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL166 IS 'Col166 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL167 IS 'Col167 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL168 IS 'Col168 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL169 IS 'Col169 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL170 IS 'Col170 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL171 IS 'Col171 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL172 IS 'Col172 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL173 IS 'Col173 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL174 IS 'Col174 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL175 IS 'Col175 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL176 IS 'Col176 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL177 IS 'Col177 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL178 IS 'Col178 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL179 IS 'Col179 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL180 IS 'Col180 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL181 IS 'Col181 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL182 IS 'Col182 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL183 IS 'Col183 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL184 IS 'Col184 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL185 IS 'Col185 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL186 IS 'Col186 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL187 IS 'Col187 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL188 IS 'Col188 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL189 IS 'Col189 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL190 IS 'Col190 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL191 IS 'Col191 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL192 IS 'Col192 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL193 IS 'Col193 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL194 IS 'Col194 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL195 IS 'Col195 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL196 IS 'Col196 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL197 IS 'Col197 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL198 IS 'Col198 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL199 IS 'Col199 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL200 IS 'Col200 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL201 IS 'Col201 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL202 IS 'Col202 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL203 IS 'Col203 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL204 IS 'Col204 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL205 IS 'Col205 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL206 IS 'Col206 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL207 IS 'Col207 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL208 IS 'Col208 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL209 IS 'Col209 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL210 IS 'Col210 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL211 IS 'Col211 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL212 IS 'Col212 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL213 IS 'Col213 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL214 IS 'Col214 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL215 IS 'Col215 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL216 IS 'Col216 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL217 IS 'Col217 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL218 IS 'Col218 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL219 IS 'Col219 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL220 IS 'Col220 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL221 IS 'Col221 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL222 IS 'Col222 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL223 IS 'Col223 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL224 IS 'Col224 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL225 IS 'Col225 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL226 IS 'Col226 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL227 IS 'Col227 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL228 IS 'Col228 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL229 IS 'Col229 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL230 IS 'Col230 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL231 IS 'Col231 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL232 IS 'Col232 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL233 IS 'Col233 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL234 IS 'Col234 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL235 IS 'Col235 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL236 IS 'Col236 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL237 IS 'Col237 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL238 IS 'Col238 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL239 IS 'Col239 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL240 IS 'Col240 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL241 IS 'Col241 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL242 IS 'Col242 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL243 IS 'Col243 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL244 IS 'Col244 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL245 IS 'Col245 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL246 IS 'Col246 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL247 IS 'Col247 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL248 IS 'Col248 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL249 IS 'Col249 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL250 IS 'Col250 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL251 IS 'Col251 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL252 IS 'Col252 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL253 IS 'Col253 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL254 IS 'Col254 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL255 IS 'Col255 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL256 IS 'Col256 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL257 IS 'Col257 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL258 IS 'Col258 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL259 IS 'Col259 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL260 IS 'Col260 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL261 IS 'Col261 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL262 IS 'Col262 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL263 IS 'Col263 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL264 IS 'Col264 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL265 IS 'Col265 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL266 IS 'Col266 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL267 IS 'Col267 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL268 IS 'Col268 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL269 IS 'Col269 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL270 IS 'Col270 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL271 IS 'Col271 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL272 IS 'Col272 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL273 IS 'Col273 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL274 IS 'Col274 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL275 IS 'Col275 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL276 IS 'Col276 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL277 IS 'Col277 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL278 IS 'Col278 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL279 IS 'Col279 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL280 IS 'Col280 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL281 IS 'Col281 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL282 IS 'Col282 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL283 IS 'Col283 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL284 IS 'Col284 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL285 IS 'Col285 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL286 IS 'Col286 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL287 IS 'Col287 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL288 IS 'Col288 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL289 IS 'Col289 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL290 IS 'Col290 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL291 IS 'Col291 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL292 IS 'Col292 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL293 IS 'Col293 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL294 IS 'Col294 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL295 IS 'Col295 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL296 IS 'Col296 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL297 IS 'Col297 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL298 IS 'Col298 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL299 IS 'Col299 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL300 IS 'Col300 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL301 IS 'Col301 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL302 IS 'Col302 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL303 IS 'Col303 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL304 IS 'Col304 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL305 IS 'Col305 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL306 IS 'Col306 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL307 IS 'Col307 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL308 IS 'Col308 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL309 IS 'Col309 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL310 IS 'Col310 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL311 IS 'Col311 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL312 IS 'Col312 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL313 IS 'Col313 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL314 IS 'Col314 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL315 IS 'Col315 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL316 IS 'Col316 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL317 IS 'Col317 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL318 IS 'Col318 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL319 IS 'Col319 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL320 IS 'Col320 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL321 IS 'Col321 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL322 IS 'Col322 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL323 IS 'Col323 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL324 IS 'Col324 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL325 IS 'Col325 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL326 IS 'Col326 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL327 IS 'Col327 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL328 IS 'Col328 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL329 IS 'Col329 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL330 IS 'Col330 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL331 IS 'Col331 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL332 IS 'Col332 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL333 IS 'Col333 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL334 IS 'Col334 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL335 IS 'Col335 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL336 IS 'Col336 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL337 IS 'Col337 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL338 IS 'Col338 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL339 IS 'Col339 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL340 IS 'Col340 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL341 IS 'Col341 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL342 IS 'Col342 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL343 IS 'Col343 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL344 IS 'Col344 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL345 IS 'Col345 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL346 IS 'Col346 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL347 IS 'Col347 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL348 IS 'Col348 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL349 IS 'Col349 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL350 IS 'Col350 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL351 IS 'Col351 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL352 IS 'Col352 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL353 IS 'Col353 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL354 IS 'Col354 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL355 IS 'Col355 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL356 IS 'Col356 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL357 IS 'Col357 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL358 IS 'Col358 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL359 IS 'Col359 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL360 IS 'Col360 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL361 IS 'Col361 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL362 IS 'Col362 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL363 IS 'Col363 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL364 IS 'Col364 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL365 IS 'Col365 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL366 IS 'Col366 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL367 IS 'Col367 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL368 IS 'Col368 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL369 IS 'Col369 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL370 IS 'Col370 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL371 IS 'Col371 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL372 IS 'Col372 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL373 IS 'Col373 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL374 IS 'Col374 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL375 IS 'Col375 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL376 IS 'Col376 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL377 IS 'Col377 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL378 IS 'Col378 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL379 IS 'Col379 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL380 IS 'Col380 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL381 IS 'Col381 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL382 IS 'Col382 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL383 IS 'Col383 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL384 IS 'Col384 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL385 IS 'Col385 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL386 IS 'Col386 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL387 IS 'Col387 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL388 IS 'Col388 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL389 IS 'Col389 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL390 IS 'Col390 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL391 IS 'Col391 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL392 IS 'Col392 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL393 IS 'Col393 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL394 IS 'Col394 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL395 IS 'Col395 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL396 IS 'Col396 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL397 IS 'Col397 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL398 IS 'Col398 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL399 IS 'Col399 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL400 IS 'Col400 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL401 IS 'Col401 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL402 IS 'Col402 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL403 IS 'Col403 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL404 IS 'Col404 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL405 IS 'Col405 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL406 IS 'Col406 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL407 IS 'Col407 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL408 IS 'Col408 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL409 IS 'Col409 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL410 IS 'Col410 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL411 IS 'Col411 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL412 IS 'Col412 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL413 IS 'Col413 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL414 IS 'Col414 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL415 IS 'Col415 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL416 IS 'Col416 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL417 IS 'Col417 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL418 IS 'Col418 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL419 IS 'Col419 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL420 IS 'Col420 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL421 IS 'Col421 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL422 IS 'Col422 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL423 IS 'Col423 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL424 IS 'Col424 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL425 IS 'Col425 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL426 IS 'Col426 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL427 IS 'Col427 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL428 IS 'Col428 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL429 IS 'Col429 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL430 IS 'Col430 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL431 IS 'Col431 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL432 IS 'Col432 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL433 IS 'Col433 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL434 IS 'Col434 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL435 IS 'Col435 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL436 IS 'Col436 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL437 IS 'Col437 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL438 IS 'Col438 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL439 IS 'Col439 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL440 IS 'Col440 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL441 IS 'Col441 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL442 IS 'Col442 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL443 IS 'Col443 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL444 IS 'Col444 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL445 IS 'Col445 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL446 IS 'Col446 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL447 IS 'Col447 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL448 IS 'Col448 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL449 IS 'Col449 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL450 IS 'Col450 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL451 IS 'Col451 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL452 IS 'Col452 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL453 IS 'Col453 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL454 IS 'Col454 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL455 IS 'Col455 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL456 IS 'Col456 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL457 IS 'Col457 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL458 IS 'Col458 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL459 IS 'Col459 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL460 IS 'Col460 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL461 IS 'Col461 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL462 IS 'Col462 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL463 IS 'Col463 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL464 IS 'Col464 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL465 IS 'Col465 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL466 IS 'Col466 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL467 IS 'Col467 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL468 IS 'Col468 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL469 IS 'Col469 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL470 IS 'Col470 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL471 IS 'Col471 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL472 IS 'Col472 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL473 IS 'Col473 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL474 IS 'Col474 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL475 IS 'Col475 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL476 IS 'Col476 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL477 IS 'Col477 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL478 IS 'Col478 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL479 IS 'Col479 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL480 IS 'Col480 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL481 IS 'Col481 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL482 IS 'Col482 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL483 IS 'Col483 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL484 IS 'Col484 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL485 IS 'Col485 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL486 IS 'Col486 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL487 IS 'Col487 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL488 IS 'Col488 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL489 IS 'Col489 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL490 IS 'Col490 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL491 IS 'Col491 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL492 IS 'Col492 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL493 IS 'Col493 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL494 IS 'Col494 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL495 IS 'Col495 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL496 IS 'Col496 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL497 IS 'Col497 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL498 IS 'Col498 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL499 IS 'Col499 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL500 IS 'Col500 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL501 IS 'Col501 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL502 IS 'Col502 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL503 IS 'Col503 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL504 IS 'Col504 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL505 IS 'Col505 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL506 IS 'Col506 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL507 IS 'Col507 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL508 IS 'Col508 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL509 IS 'Col509 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL510 IS 'Col510 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL511 IS 'Col511 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL512 IS 'Col512 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL513 IS 'Col513 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL514 IS 'Col514 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL515 IS 'Col515 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL516 IS 'Col516 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL517 IS 'Col517 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL518 IS 'Col518 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL519 IS 'Col519 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL520 IS 'Col520 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL521 IS 'Col521 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL522 IS 'Col522 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL523 IS 'Col523 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL524 IS 'Col524 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL525 IS 'Col525 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL526 IS 'Col526 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL527 IS 'Col527 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL528 IS 'Col528 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL529 IS 'Col529 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL530 IS 'Col530 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL531 IS 'Col531 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL532 IS 'Col532 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL533 IS 'Col533 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL534 IS 'Col534 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL535 IS 'Col535 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL536 IS 'Col536 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL537 IS 'Col537 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL538 IS 'Col538 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL539 IS 'Col539 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL540 IS 'Col540 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL541 IS 'Col541 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL542 IS 'Col542 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL543 IS 'Col543 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL544 IS 'Col544 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL545 IS 'Col545 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL546 IS 'Col546 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL547 IS 'Col547 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL548 IS 'Col548 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL549 IS 'Col549 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL550 IS 'Col550 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL551 IS 'Col551 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL552 IS 'Col552 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL553 IS 'Col553 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL554 IS 'Col554 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL555 IS 'Col555 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL556 IS 'Col556 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL557 IS 'Col557 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL558 IS 'Col558 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL559 IS 'Col559 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL560 IS 'Col560 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL561 IS 'Col561 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL562 IS 'Col562 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL563 IS 'Col563 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL564 IS 'Col564 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL565 IS 'Col565 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL566 IS 'Col566 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL567 IS 'Col567 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL568 IS 'Col568 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL569 IS 'Col569 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL570 IS 'Col570 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL571 IS 'Col571 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL572 IS 'Col572 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL573 IS 'Col573 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL574 IS 'Col574 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL575 IS 'Col575 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL576 IS 'Col576 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL577 IS 'Col577 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL578 IS 'Col578 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL579 IS 'Col579 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL580 IS 'Col580 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL581 IS 'Col581 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL582 IS 'Col582 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL583 IS 'Col583 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL584 IS 'Col584 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL585 IS 'Col585 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL586 IS 'Col586 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL587 IS 'Col587 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL588 IS 'Col588 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL589 IS 'Col589 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL590 IS 'Col590 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL591 IS 'Col591 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL592 IS 'Col592 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL593 IS 'Col593 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL594 IS 'Col594 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL595 IS 'Col595 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL596 IS 'Col596 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL597 IS 'Col597 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL598 IS 'Col598 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL599 IS 'Col599 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL600 IS 'Col600 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL601 IS 'Col601 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL602 IS 'Col602 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL603 IS 'Col603 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL604 IS 'Col604 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL605 IS 'Col605 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL606 IS 'Col606 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL607 IS 'Col607 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL608 IS 'Col608 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL609 IS 'Col609 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL610 IS 'Col610 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL611 IS 'Col611 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL612 IS 'Col612 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL613 IS 'Col613 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL614 IS 'Col614 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL615 IS 'Col615 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL616 IS 'Col616 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL617 IS 'Col617 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL618 IS 'Col618 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL619 IS 'Col619 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL620 IS 'Col620 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL621 IS 'Col621 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL622 IS 'Col622 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL623 IS 'Col623 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL624 IS 'Col624 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL625 IS 'Col625 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL626 IS 'Col626 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL627 IS 'Col627 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL628 IS 'Col628 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL629 IS 'Col629 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL630 IS 'Col630 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL631 IS 'Col631 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL632 IS 'Col632 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL633 IS 'Col633 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL634 IS 'Col634 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL635 IS 'Col635 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL636 IS 'Col636 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL637 IS 'Col637 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL638 IS 'Col638 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL639 IS 'Col639 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL640 IS 'Col640 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL641 IS 'Col641 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL642 IS 'Col642 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL643 IS 'Col643 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL644 IS 'Col644 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL645 IS 'Col645 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL646 IS 'Col646 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL647 IS 'Col647 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL648 IS 'Col648 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL649 IS 'Col649 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL650 IS 'Col650 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL651 IS 'Col651 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL652 IS 'Col652 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL653 IS 'Col653 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL654 IS 'Col654 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL655 IS 'Col655 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL656 IS 'Col656 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL657 IS 'Col657 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL658 IS 'Col658 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL659 IS 'Col659 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL660 IS 'Col660 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL661 IS 'Col661 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL662 IS 'Col662 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL663 IS 'Col663 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL664 IS 'Col664 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL665 IS 'Col665 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL666 IS 'Col666 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL667 IS 'Col667 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL668 IS 'Col668 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL669 IS 'Col669 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL670 IS 'Col670 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL671 IS 'Col671 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL672 IS 'Col672 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL673 IS 'Col673 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL674 IS 'Col674 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL675 IS 'Col675 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL676 IS 'Col676 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL677 IS 'Col677 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL678 IS 'Col678 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL679 IS 'Col679 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL680 IS 'Col680 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL681 IS 'Col681 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL682 IS 'Col682 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL683 IS 'Col683 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL684 IS 'Col684 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL685 IS 'Col685 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL686 IS 'Col686 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL687 IS 'Col687 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL688 IS 'Col688 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL689 IS 'Col689 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL690 IS 'Col690 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL691 IS 'Col691 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL692 IS 'Col692 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL693 IS 'Col693 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL694 IS 'Col694 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL695 IS 'Col695 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL696 IS 'Col696 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL697 IS 'Col697 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL698 IS 'Col698 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL699 IS 'Col699 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL700 IS 'Col700 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL701 IS 'Col701 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL702 IS 'Col702 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL703 IS 'Col703 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL704 IS 'Col704 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL705 IS 'Col705 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL706 IS 'Col706 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL707 IS 'Col707 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL708 IS 'Col708 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL709 IS 'Col709 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL710 IS 'Col710 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL711 IS 'Col711 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL712 IS 'Col712 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL713 IS 'Col713 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL714 IS 'Col714 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL715 IS 'Col715 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL716 IS 'Col716 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL717 IS 'Col717 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL718 IS 'Col718 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL719 IS 'Col719 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL720 IS 'Col720 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL721 IS 'Col721 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL722 IS 'Col722 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL723 IS 'Col723 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL724 IS 'Col724 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL725 IS 'Col725 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL726 IS 'Col726 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL727 IS 'Col727 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL728 IS 'Col728 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL729 IS 'Col729 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL730 IS 'Col730 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL731 IS 'Col731 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL732 IS 'Col732 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL733 IS 'Col733 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL734 IS 'Col734 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL735 IS 'Col735 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL736 IS 'Col736 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL737 IS 'Col737 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL738 IS 'Col738 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL739 IS 'Col739 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL740 IS 'Col740 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL741 IS 'Col741 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL742 IS 'Col742 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL743 IS 'Col743 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL744 IS 'Col744 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL745 IS 'Col745 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL746 IS 'Col746 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL747 IS 'Col747 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL748 IS 'Col748 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL749 IS 'Col749 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL750 IS 'Col750 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL751 IS 'Col751 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL752 IS 'Col752 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL753 IS 'Col753 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL754 IS 'Col754 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL755 IS 'Col755 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL756 IS 'Col756 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL757 IS 'Col757 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL758 IS 'Col758 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL759 IS 'Col759 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL760 IS 'Col760 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL761 IS 'Col761 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL762 IS 'Col762 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL763 IS 'Col763 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL764 IS 'Col764 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL765 IS 'Col765 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL766 IS 'Col766 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL767 IS 'Col767 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL768 IS 'Col768 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL769 IS 'Col769 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL770 IS 'Col770 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL771 IS 'Col771 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL772 IS 'Col772 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL773 IS 'Col773 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL774 IS 'Col774 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL775 IS 'Col775 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL776 IS 'Col776 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL777 IS 'Col777 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL778 IS 'Col778 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL779 IS 'Col779 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL780 IS 'Col780 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL781 IS 'Col781 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL782 IS 'Col782 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL783 IS 'Col783 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL784 IS 'Col784 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL785 IS 'Col785 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL786 IS 'Col786 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL787 IS 'Col787 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL788 IS 'Col788 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL789 IS 'Col789 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL790 IS 'Col790 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL791 IS 'Col791 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL792 IS 'Col792 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL793 IS 'Col793 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL794 IS 'Col794 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL795 IS 'Col795 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL796 IS 'Col796 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL797 IS 'Col797 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL798 IS 'Col798 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL799 IS 'Col799 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL800 IS 'Col800 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL801 IS 'Col801 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL802 IS 'Col802 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL803 IS 'Col803 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL804 IS 'Col804 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL805 IS 'Col805 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL806 IS 'Col806 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL807 IS 'Col807 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL808 IS 'Col808 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL809 IS 'Col809 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL810 IS 'Col810 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL811 IS 'Col811 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL812 IS 'Col812 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL813 IS 'Col813 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL814 IS 'Col814 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL815 IS 'Col815 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL816 IS 'Col816 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL817 IS 'Col817 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL818 IS 'Col818 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL819 IS 'Col819 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL820 IS 'Col820 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL821 IS 'Col821 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL822 IS 'Col822 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL823 IS 'Col823 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL824 IS 'Col824 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL825 IS 'Col825 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL826 IS 'Col826 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL827 IS 'Col827 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL828 IS 'Col828 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL829 IS 'Col829 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL830 IS 'Col830 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL831 IS 'Col831 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL832 IS 'Col832 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL833 IS 'Col833 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL834 IS 'Col834 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL835 IS 'Col835 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL836 IS 'Col836 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL837 IS 'Col837 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL838 IS 'Col838 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL839 IS 'Col839 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL840 IS 'Col840 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL841 IS 'Col841 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL842 IS 'Col842 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL843 IS 'Col843 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL844 IS 'Col844 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL845 IS 'Col845 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL846 IS 'Col846 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL847 IS 'Col847 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL848 IS 'Col848 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL849 IS 'Col849 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL850 IS 'Col850 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL851 IS 'Col851 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL852 IS 'Col852 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL853 IS 'Col853 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL854 IS 'Col854 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL855 IS 'Col855 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL856 IS 'Col856 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL857 IS 'Col857 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL858 IS 'Col858 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL859 IS 'Col859 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL860 IS 'Col860 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL861 IS 'Col861 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL862 IS 'Col862 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL863 IS 'Col863 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL864 IS 'Col864 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL865 IS 'Col865 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL866 IS 'Col866 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL867 IS 'Col867 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL868 IS 'Col868 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL869 IS 'Col869 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL870 IS 'Col870 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL871 IS 'Col871 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL872 IS 'Col872 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL873 IS 'Col873 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL874 IS 'Col874 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL875 IS 'Col875 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL876 IS 'Col876 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL877 IS 'Col877 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL878 IS 'Col878 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL879 IS 'Col879 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL880 IS 'Col880 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL881 IS 'Col881 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL882 IS 'Col882 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL883 IS 'Col883 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL884 IS 'Col884 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL885 IS 'Col885 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL886 IS 'Col886 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL887 IS 'Col887 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL888 IS 'Col888 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL889 IS 'Col889 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL890 IS 'Col890 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL891 IS 'Col891 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL892 IS 'Col892 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL893 IS 'Col893 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL894 IS 'Col894 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL895 IS 'Col895 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL896 IS 'Col896 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL897 IS 'Col897 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL898 IS 'Col898 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL899 IS 'Col899 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL900 IS 'Col900 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL901 IS 'Col901 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL902 IS 'Col902 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL903 IS 'Col903 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL904 IS 'Col904 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL905 IS 'Col905 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL906 IS 'Col906 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL907 IS 'Col907 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL908 IS 'Col908 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL909 IS 'Col909 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL910 IS 'Col910 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL911 IS 'Col911 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL912 IS 'Col912 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL913 IS 'Col913 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL914 IS 'Col914 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL915 IS 'Col915 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL916 IS 'Col916 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL917 IS 'Col917 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL918 IS 'Col918 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL919 IS 'Col919 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL920 IS 'Col920 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL921 IS 'Col921 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL922 IS 'Col922 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL923 IS 'Col923 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL924 IS 'Col924 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL925 IS 'Col925 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL926 IS 'Col926 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL927 IS 'Col927 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL928 IS 'Col928 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL929 IS 'Col929 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL930 IS 'Col930 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL931 IS 'Col931 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL932 IS 'Col932 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL933 IS 'Col933 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL934 IS 'Col934 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL935 IS 'Col935 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL936 IS 'Col936 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL937 IS 'Col937 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/
COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.PK_FORMSLINEAR IS 'The Primary Key to identify the form response.'
/
COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.STUDY_NUMBER IS 'The Study the form response is linked with.'
/


COMMENT ON COLUMN VDA.VDA_V_GROUP_USERS.GROUP_NAME IS 'The name of the group'
/



COMMENT ON TABLE VDA.VDA_V_ACCTFORMS_AUDIT IS 'This view provides access to the column update level Audit information of account level forms'
/

COMMENT ON COLUMN VDA.VDA_V_ACCTFORMS_AUDIT.PK_FORMAUDITCOL IS 'The Primary Key of the Audit Transaction'
/

COMMENT ON COLUMN VDA.VDA_V_ACCTFORMS_AUDIT.FK_FILLEDFORM IS 'The FK to the form response (account form)'
/

COMMENT ON COLUMN VDA.VDA_V_ACCTFORMS_AUDIT.FK_FORM IS 'The FK to the form'
/

COMMENT ON COLUMN VDA.VDA_V_ACCTFORMS_AUDIT.FORM_NAME IS 'The form name'
/

COMMENT ON COLUMN VDA.VDA_V_ACCTFORMS_AUDIT.FA_SYSTEMID IS 'The systemid of the field'
/

COMMENT ON COLUMN VDA.VDA_V_ACCTFORMS_AUDIT.FA_DATETIME IS 'The date-time of the transaction'
/

COMMENT ON COLUMN VDA.VDA_V_ACCTFORMS_AUDIT.FA_FLDNAME IS 'The user defined field name'
/

COMMENT ON COLUMN VDA.VDA_V_ACCTFORMS_AUDIT.FA_OLDVALUE IS 'The old value of the field'
/

COMMENT ON COLUMN VDA.VDA_V_ACCTFORMS_AUDIT.FA_NEWVALUE IS 'The new value of the field'
/

COMMENT ON COLUMN VDA.VDA_V_ACCTFORMS_AUDIT.FA_MODIFIEDBY_NAME IS 'The user who modified the form response data'
/

COMMENT ON COLUMN VDA.VDA_V_ACCTFORMS_AUDIT.FA_REASON IS 'The reason for change'
/

COMMENT ON COLUMN VDA.VDA_V_ACCTFORMS_AUDIT.FK_ACCOUNT IS 'The account the form response is linked with'
/


COMMENT ON TABLE VDA.VDA_V_STUDYFORMS_AUDIT IS 'This view provides access to the column update level Audit information of account level forms'
/

COMMENT ON COLUMN VDA.VDA_V_STUDYFORMS_AUDIT.PK_FORMAUDITCOL IS 'The Primary Key of the Audit Transaction'
/

COMMENT ON COLUMN VDA.VDA_V_STUDYFORMS_AUDIT.FK_FILLEDFORM IS 'The FK to the form response (account form)'
/

COMMENT ON COLUMN VDA.VDA_V_STUDYFORMS_AUDIT.FK_FORM IS 'The FK to the form'
/

COMMENT ON COLUMN VDA.VDA_V_STUDYFORMS_AUDIT.FORM_NAME IS 'The form name'
/

COMMENT ON COLUMN VDA.VDA_V_STUDYFORMS_AUDIT.FA_SYSTEMID IS 'The systemid of the field'
/

COMMENT ON COLUMN VDA.VDA_V_STUDYFORMS_AUDIT.FA_DATETIME IS 'The date-time of the transaction'
/

COMMENT ON COLUMN VDA.VDA_V_STUDYFORMS_AUDIT.FA_FLDNAME IS 'The user defined field name'
/

COMMENT ON COLUMN VDA.VDA_V_STUDYFORMS_AUDIT.FA_OLDVALUE IS 'The old value of the field'
/

COMMENT ON COLUMN VDA.VDA_V_STUDYFORMS_AUDIT.FA_NEWVALUE IS 'The new value of the field'
/

COMMENT ON COLUMN VDA.VDA_V_STUDYFORMS_AUDIT.FA_MODIFIEDBY_NAME IS 'The user who modified the form response data'
/

COMMENT ON COLUMN VDA.VDA_V_STUDYFORMS_AUDIT.FA_REASON IS 'The reason for change'
/

COMMENT ON COLUMN VDA.VDA_V_STUDYFORMS_AUDIT.FK_ACCOUNT IS 'The account the form response is linked with'
/

COMMENT ON COLUMN VDA.VDA_V_STUDYFORMS_AUDIT.STUDY_NUMBER IS 'The study number of the study the form response is linked with'
/
COMMENT ON COLUMN VDA.VDA_V_STUDYFORMS_AUDIT.STUDY_TITLE IS 'The title of the study the form response is linked with'
/


COMMENT ON TABLE VDA.VDA_V_PATFORMS_AUDIT IS 'This view provides access to the column update level Audit information of account level forms'
/

COMMENT ON COLUMN VDA.VDA_V_PATFORMS_AUDIT.PK_FORMAUDITCOL IS 'The Primary Key of the Audit Transaction'
/

COMMENT ON COLUMN VDA.VDA_V_PATFORMS_AUDIT.FK_FILLEDFORM IS 'The FK to the form response (account form)'
/

COMMENT ON COLUMN VDA.VDA_V_PATFORMS_AUDIT.FK_FORM IS 'The FK to the form'
/

COMMENT ON COLUMN VDA.VDA_V_PATFORMS_AUDIT.FORM_NAME IS 'The form name'
/

COMMENT ON COLUMN VDA.VDA_V_PATFORMS_AUDIT.FA_SYSTEMID IS 'The systemid of the field'
/

COMMENT ON COLUMN VDA.VDA_V_PATFORMS_AUDIT.FA_DATETIME IS 'The date-time of the transaction'
/

COMMENT ON COLUMN VDA.VDA_V_PATFORMS_AUDIT.FA_FLDNAME IS 'The user defined field name'
/

COMMENT ON COLUMN VDA.VDA_V_PATFORMS_AUDIT.FA_OLDVALUE IS 'The old value of the field'
/

COMMENT ON COLUMN VDA.VDA_V_PATFORMS_AUDIT.FA_NEWVALUE IS 'The new value of the field'
/

COMMENT ON COLUMN VDA.VDA_V_PATFORMS_AUDIT.FA_MODIFIEDBY_NAME IS 'The user who modified the form response data'
/

COMMENT ON COLUMN VDA.VDA_V_PATFORMS_AUDIT.FA_REASON IS 'The reason for change'
/

COMMENT ON COLUMN VDA.VDA_V_PATFORMS_AUDIT.FK_ACCOUNT IS 'The account the form response is linked with'
/

COMMENT ON COLUMN VDA.VDA_V_PATFORMS_AUDIT.FK_PER IS 'The FK of the Patient the form response is linked with'
/
COMMENT ON COLUMN VDA.VDA_V_PATFORMS_AUDIT.FK_PATPROT IS 'The FK of the Patient-study record the form response is linked with'
/
COMMENT ON COLUMN VDA.VDA_V_PATFORMS_AUDIT.PATIENT_ID IS 'The Patient ID or Patient Code of the patient the form response is linked with'
/
COMMENT ON COLUMN VDA.VDA_V_PATFORMS_AUDIT.PATIENT_STUDY_ID IS 'The Patient Study ID of the patient the form response is linked with'
/

/* Formatted on 5/2/2013 3:06:11 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_USERSITES  (View)
--
--  Dependencies:
--   ER_CODELST (Table)
--   ER_SITE (Table)
--   ER_USER (Table)
--   ER_USERSITE (Table)
--

CREATE OR REPLACE FORCE VIEW VDA_V_USERSITES
(
   PK_SITE,
   FK_CODELST_TYPE,
   FK_ACCOUNT,
   SITE_NAME,
   PARENT_SITE,
   SITE_STAT,
   PARENT_SITE_NAME,
   SITETYPE,
   PK_USERSITE,
   FK_USER,
   USERSITE_RIGHT,
   SITE_ALTID,
   USER_NAME
)
AS
   SELECT   A.PK_SITE,
            A.FK_CODELST_TYPE,
            A.FK_ACCOUNT,
            A.SITE_NAME,
            A.SITE_PARENT AS PARENT_SITE,
            A.SITE_STAT,
            (SELECT   SITE_NAME
               FROM   eres.er_site B
              WHERE   B.PK_SITE = A.SITE_PARENT)
               AS PARENT_SITE_NAME,
            (SELECT   C.CODELST_DESC
               FROM   eres.ER_CODELST C
              WHERE   C.PK_CODELST = A.FK_CODELST_TYPE)
               SITETYPE,
            PK_USERSITE,
            FK_USER,
            decode(USERSITE_RIGHT,5,'NEW',6,'EDIT',7,'ALL','VIEW') USERSITE_RIGHT, 
            A.SITE_ALTID,
            usr.USR_FIRSTNAME || ' ' || usr.USR_LASTNAME AS user_name
     FROM   eres.ER_SITE A, eres.ER_USERSITE U, eres.ER_USER usr
    WHERE       U.FK_SITE = A.PK_SITE
            AND usr.pk_user = u.fk_user
            AND usersite_right > 0
/
COMMENT ON TABLE VDA_V_USERSITES IS 'This view provides access to the user-organization access'
/

COMMENT ON COLUMN VDA_V_USERSITES.PK_SITE IS 'The Primary Key of the site'
/

COMMENT ON COLUMN VDA_V_USERSITES.FK_CODELST_TYPE IS 'The FK to code list for site type'
/

COMMENT ON COLUMN VDA_V_USERSITES.FK_ACCOUNT IS 'The account the site is linked with'
/

COMMENT ON COLUMN VDA_V_USERSITES.SITE_NAME IS 'The Site Name'
/

COMMENT ON COLUMN VDA_V_USERSITES.PARENT_SITE IS 'The FK to Parent Site (immediate parent)'
/

COMMENT ON COLUMN VDA_V_USERSITES.SITE_STAT IS 'The Site Status'
/

COMMENT ON COLUMN VDA_V_USERSITES.PARENT_SITE_NAME IS 'The Parent Site Name (immediate parent)'
/

COMMENT ON COLUMN VDA_V_USERSITES.SITETYPE IS 'The Site Type Description'
/

COMMENT ON COLUMN VDA_V_USERSITES.PK_USERSITE IS 'The PK of usersite record'
/

COMMENT ON COLUMN VDA_V_USERSITES.FK_USER IS 'The FK to user'
/

COMMENT ON COLUMN VDA_V_USERSITES.USERSITE_RIGHT IS 'The right'
/

COMMENT ON COLUMN VDA_V_USERSITES.SITE_ALTID IS 'The site alternate id'
/

COMMENT ON COLUMN VDA_V_USERSITES.USER_NAME IS 'The user name'
/

COMMENT ON TABLE VDA_V_STUDYTEAM_MEMBERS IS 'This view provides access to study team member information for all the studies'
/

COMMENT ON COLUMN VDA_V_STUDYTEAM_MEMBERS.STUDY_NUMBER IS 'The Study Number'
/

COMMENT ON COLUMN VDA_V_STUDYTEAM_MEMBERS.STUDY_TITLE IS 'The Study Title'
/

COMMENT ON COLUMN VDA_V_STUDYTEAM_MEMBERS.USER_SITE_NAME IS 'The Site or Organization the team member is linked with'
/

COMMENT ON COLUMN VDA_V_STUDYTEAM_MEMBERS.USER_NAME IS 'The Study team member name'
/

COMMENT ON COLUMN VDA_V_STUDYTEAM_MEMBERS.USER_PHONE IS 'The Study Team member''s Phone number information'
/

COMMENT ON COLUMN VDA_V_STUDYTEAM_MEMBERS.USER_EMAIL IS 'The Study Team member''s email information'
/

COMMENT ON COLUMN VDA_V_STUDYTEAM_MEMBERS.USER_ADDRESS IS 'The Study Team member''s address information'
/

COMMENT ON COLUMN VDA_V_STUDYTEAM_MEMBERS.ROLE IS 'The Study Team member''s Role information'
/

COMMENT ON COLUMN VDA_V_STUDYTEAM_MEMBERS.FK_STUDY IS 'The FK to the study'
/

COMMENT ON COLUMN VDA_V_STUDYTEAM_MEMBERS.CREATED_ON IS 'The date the record was created on (Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYTEAM_MEMBERS.FK_ACCOUNT IS 'The account the study  is linked with'
/

COMMENT ON COLUMN VDA_V_STUDYTEAM_MEMBERS.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYTEAM_MEMBERS.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYTEAM_MEMBERS.LAST_MODIFIED_DATE IS 'The date the record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYTEAM_MEMBERS.RID IS 'The unique Row ID for Audit'
/

COMMENT ON COLUMN VDA_V_STUDYTEAM_MEMBERS.PK_STUDYTEAM IS 'The Primary Key of the Study Team record'
/

COMMENT ON COLUMN VDA_V_STUDYTEAM_MEMBERS.STUDYTEAM_STATUS IS 'The Study Team member''s status in the team'
/

COMMENT ON TABLE VDA_V_USERSITES IS 'This view provides access to the user-organization access'
/

COMMENT ON COLUMN VDA_V_USERSITES.PK_SITE IS 'The Primary Key of the site'
/

COMMENT ON COLUMN VDA_V_USERSITES.FK_CODELST_TYPE IS 'The FK to code list for site type'
/

COMMENT ON COLUMN VDA_V_USERSITES.FK_ACCOUNT IS 'The account the site is linked with'
/

COMMENT ON COLUMN VDA_V_USERSITES.SITE_NAME IS 'The Site Name'
/

COMMENT ON COLUMN VDA_V_USERSITES.PARENT_SITE IS 'The FK to Parent Site (immediate parent)'
/

COMMENT ON COLUMN VDA_V_USERSITES.SITE_STAT IS 'The Site Status'
/

COMMENT ON COLUMN VDA_V_USERSITES.PARENT_SITE_NAME IS 'The Parent Site Name (immediate parent)'
/

COMMENT ON COLUMN VDA_V_USERSITES.SITETYPE IS 'The Site Type Description'
/

COMMENT ON COLUMN VDA_V_USERSITES.PK_USERSITE IS 'The PK of usersite record'
/

COMMENT ON COLUMN VDA_V_USERSITES.FK_USER IS 'The FK to user'
/

COMMENT ON COLUMN VDA_V_USERSITES.USERSITE_RIGHT IS 'The right'
/

COMMENT ON COLUMN VDA_V_USERSITES.SITE_ALTID IS 'The site alternate id'
/

COMMENT ON COLUMN VDA_V_USERSITES.USER_NAME IS 'The user name'
/


COMMENT ON TABLE VDA.VDA_V_PAT_TXARM IS 'This view provides access to patient treatment arm information'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_TXARM.PATIENT_ID IS 'The Patient ID or Patient Code'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_TXARM.PATIENT_STUDY_ID IS 'The Patient Study ID'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_TXARM.TREATMENT_NAME IS 'The Treatment name'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_TXARM.DRUG_INFO IS 'Drug information linked with the treatment arm'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_TXARM.TREATMENT_STATUS_DATE IS 'The status date'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_TXARM.TREATMENT_END_DATE IS 'The Treatment arm end date'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_TXARM.TREATMENT_NOTES IS 'The Notes associated with the patient treatment arm'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_TXARM.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_TXARM.CREATED_ON IS 'The date the record was created on (Audit)'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_TXARM.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_TXARM.LAST_MODIFIED_DATE IS 'The date the record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_TXARM.FK_ACCOUNT IS 'The account the patient is linked with'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_TXARM.RID IS 'The unique Row ID for Audit'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_TXARM.PK_PATTXARM IS 'The PK of treatment arm record'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_TXARM.FK_STUDY IS 'The FK to the study record'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_TXARM.FK_PER IS 'The FK to the patient record'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_TXARM.STUDY_NUMBER IS 'The Study Number'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_TXARM.STUDY_TITLE IS 'The Study Title'
/


COMMENT ON TABLE VDA.VDA_V_MORE_PATIENT_DETAILS IS 'This view provides access to the more patient codes or details linked with a patient'
/

COMMENT ON COLUMN VDA.VDA_V_MORE_PATIENT_DETAILS.FIELD_NAME IS 'The Name of the Field'
/

COMMENT ON COLUMN VDA.VDA_V_MORE_PATIENT_DETAILS.FIELD_VALUE IS 'The value of the Field'
/

COMMENT ON COLUMN VDA.VDA_V_MORE_PATIENT_DETAILS.FK_PER IS 'The FK to the patient record'
/

COMMENT ON COLUMN VDA.VDA_V_MORE_PATIENT_DETAILS.CREATED_ON IS 'The date the record was created on (Audit)'
/

COMMENT ON COLUMN VDA.VDA_V_MORE_PATIENT_DETAILS.FK_ACCOUNT IS 'The account the patient is linked with'
/

COMMENT ON COLUMN VDA.VDA_V_MORE_PATIENT_DETAILS.PATIENT_ID IS 'The Patient ID or Patient Code'
/

COMMENT ON COLUMN VDA.VDA_V_MORE_PATIENT_DETAILS.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA.VDA_V_MORE_PATIENT_DETAILS.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA.VDA_V_MORE_PATIENT_DETAILS.LAST_MODIFIED_DATE IS 'The date the record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA.VDA_V_MORE_PATIENT_DETAILS.RID IS 'The Primary Key of the audit transaction'
/

COMMENT ON COLUMN VDA.VDA_V_MORE_PATIENT_DETAILS.PK_PERID IS 'The Primary Key of the patient record'
/

COMMENT ON COLUMN VDA.VDA_V_MORE_PATIENT_DETAILS.SITE_NAME IS 'The name of the default organization the patient is linked'
/


COMMENT ON TABLE VDA.VDA_V_PAT_SITES IS 'This view provides access to the Patient Organization association'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_SITES.PATIENT_ID IS 'The Patient ID or Patient Code'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_SITES.SITE_NAME IS 'The Site Name the patient is linked with'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_SITES.PAT_FACILITY_ID IS 'The Patient Facility ID'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_SITES.REG_DATE IS 'The Date the patient was associated with the facility'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_SITES.PROVIDER IS 'The user linked with patient facility association'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_SITES.SPECIALTY_GROUP IS 'The specialty the facility has access to'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_SITES.FACILITY_ACCESS IS 'The users of this site has access to the patient'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_SITES.FACILITY_DEFAULT IS 'Flag to indicate if it is the default facility or site for the patient'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_SITES.CREATED_ON IS 'The date the record was created on (Audit)'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_SITES.LAST_MODIFIED_DATE IS 'The date the record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_SITES.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_SITES.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_SITES.FK_ACCOUNT IS 'The account the patient is linked with'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_SITES.RID IS 'The unique Row ID for Audit'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_SITES.PK_PATFACILITY IS 'The Primary Key of the patient facility or site record'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_SITES.FK_PER IS 'The Primary Key of the patient record'
/


COMMENT ON TABLE VDA.VDA_V_USERS IS 'This view provides access to the application users information'
/

COMMENT ON COLUMN VDA.VDA_V_USERS.USER_TYPE IS 'The Type of the users'
/

COMMENT ON COLUMN VDA.VDA_V_USERS.USER_DEFAULT_GROUP_FK IS 'The FK to default group of the user'
/

COMMENT ON COLUMN VDA.VDA_V_USERS.SITE_NAME IS 'The default organization for the user'
/

COMMENT ON COLUMN VDA.VDA_V_USERS.USER_NAME IS 'The name of the user'
/

COMMENT ON COLUMN VDA.VDA_V_USERS.USER_JOBTYPE IS 'The user job type'
/

COMMENT ON COLUMN VDA.VDA_V_USERS.USER_ADDRESS IS 'The user address first line'
/

COMMENT ON COLUMN VDA.VDA_V_USERS.USER_CITY IS 'The user address-city'
/

COMMENT ON COLUMN VDA.VDA_V_USERS.USER_STATE IS 'The user address-state'
/

COMMENT ON COLUMN VDA.VDA_V_USERS.USER_ZIP IS 'The user address-zip'
/

COMMENT ON COLUMN VDA.VDA_V_USERS.USER_COUNTRY IS 'The user address-country'
/

COMMENT ON COLUMN VDA.VDA_V_USERS.USER_PHONE IS 'The user address-phone'
/

COMMENT ON COLUMN VDA.VDA_V_USERS.USER_EMAIL IS 'The user address-email'
/

COMMENT ON COLUMN VDA.VDA_V_USERS.USER_TIMEZONE IS 'The user address-timezone'
/

COMMENT ON COLUMN VDA.VDA_V_USERS.USER_SPECIALTY IS 'The user address specialty'
/

COMMENT ON COLUMN VDA.VDA_V_USERS.USER_WRKEXP IS 'The user work experience'
/

COMMENT ON COLUMN VDA.VDA_V_USERS.USER_PHASEINV IS 'The study phase user has been involved in'
/

COMMENT ON COLUMN VDA.VDA_V_USERS.USER_DEFAULTGRP IS 'The user address-city'
/

COMMENT ON COLUMN VDA.VDA_V_USERS.USER_USRNAME IS 'The default group of the user'
/

COMMENT ON COLUMN VDA.VDA_V_USERS.USER_STATUS IS 'The status of the user'
/

COMMENT ON COLUMN VDA.VDA_V_USERS.FK_ACCOUNT IS 'The account the site is linked with'
/

COMMENT ON COLUMN VDA.VDA_V_USERS.CREATED_ON IS 'The date the record was created on (Audit)'
/

COMMENT ON COLUMN VDA.VDA_V_USERS.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA.VDA_V_USERS.LAST_MODIFIED_DATE IS 'The date the record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA.VDA_V_USERS.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA.VDA_V_USERS.RID IS 'The unique Row ID for Audit'
/

COMMENT ON COLUMN VDA.VDA_V_USERS.PK_USER IS 'The PK of the user record'
/


COMMENT ON TABLE VDA.VDA_V_LIBRARY_FORMS IS 'This view provides access to the library forms information'
/

COMMENT ON COLUMN VDA.VDA_V_LIBRARY_FORMS.FORM_TYPE IS 'The Form Type'
/

COMMENT ON COLUMN VDA.VDA_V_LIBRARY_FORMS.FORM_NAME IS 'The name of the form'
/

COMMENT ON COLUMN VDA.VDA_V_LIBRARY_FORMS.FORM_DESC IS 'The form description'
/

COMMENT ON COLUMN VDA.VDA_V_LIBRARY_FORMS.FORM_STATUS IS 'The Form Status'
/

COMMENT ON COLUMN VDA.VDA_V_LIBRARY_FORMS.FORM_SHARED_WITH IS 'The Form Sharing option'
/

COMMENT ON COLUMN VDA.VDA_V_LIBRARY_FORMS.FK_ACCOUNT IS 'The account the site is linked with'
/

COMMENT ON COLUMN VDA.VDA_V_LIBRARY_FORMS.PK_FORMLIB IS 'The PK of the form record'
/

COMMENT ON COLUMN VDA.VDA_V_LIBRARY_FORMS.RID IS 'The unique Row ID for Audit'
/

COMMENT ON COLUMN VDA.VDA_V_LIBRARY_FORMS.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA.VDA_V_LIBRARY_FORMS.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA.VDA_V_LIBRARY_FORMS.LAST_MODIFIED_DATE IS 'The date the record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA.VDA_V_LIBRARY_FORMS.CREATED_ON IS 'The date the record was created on (Audit)'
/

COMMENT ON COLUMN VDA.VDA_V_LIBRARY_FORMS.FORM_ESIGN_REQUIRED IS 'Is eSign required for the form response'
/

INSERT INTO VDA_TRACK_PATCHES
(
  PATCH_PK       ,
  DB_VER_MJR     ,
  DB_VER_MNR     ,
  DB_PATCH_NAME  ,
  FIRED_ON       ,
  APP_VERSION    
) VALUES(SEQ_VDA_TRACK_PATCHES.nextval,3,1,'02_build2.sql',sysdate,'VDA 1.2 Build 2')
/

commit
/ 
