/* Formatted on 6/11/2014 9:15:52 AM (QP5 v5.115.810.9015) */
--
-- VDA_V_PAYMENTS  (View)
--
--  Dependencies:
--   F_GET_CODELSTDESC (Function)
--   ER_MILEPAYMENT (Table)
--   ER_STUDY (Table)
--   ER_USER (Table)
--

CREATE OR REPLACE  VIEW VDA_V_PAYMENTS
(
   STUDY_NUMBER,
   STUDY_TITLE,
   MSPAY_DATE,
   MSPAY_TYPE,
   MSPAY_AMT_RECVD,
   MSPAY_DESC,
   MSPAY_COMMENTS,
   PK_MILEPAYMENT,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATED_ON,
   FK_STUDY,
   FK_ACCOUNT
)
AS
   SELECT   STUDY_NUMBER,
            STUDY_TITLE,
            MILEPAYMENT_DATE MSPAY_DATE,
            ERES.F_GET_CODELSTDESC (MILEPAYMENT_TYPE) MSPAY_TYPE,
            MILEPAYMENT_AMT MSPAY_AMT_RECVD,
            MILEPAYMENT_DESC MSPAY_DESC,
            MILEPAYMENT_COMMENTS MSPAY_COMMENTS,
            PK_MILEPAYMENT,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = x.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = x.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            x.LAST_MODIFIED_DATE,
            x.CREATED_ON,
            FK_STUDY,
            FK_ACCOUNT
     FROM   eres.ER_MILEPAYMENT x, eres.ER_STUDY s
    WHERE   PK_STUDY = FK_STUDY AND MILEPAYMENT_DELFLAG <> 'Y'
/
COMMENT ON TABLE VDA_V_PAYMENTS IS 'This view provides access to the payment information for studies'
/

COMMENT ON COLUMN VDA_V_PAYMENTS.FK_ACCOUNT IS 'The account the milestone is linked with'
/

COMMENT ON COLUMN VDA_V_PAYMENTS.STUDY_NUMBER IS 'The Study Number of the study the payment  is linked with'
/

COMMENT ON COLUMN VDA_V_PAYMENTS.STUDY_TITLE IS 'The Study Title of the study the payment is linked with'
/

COMMENT ON COLUMN VDA_V_PAYMENTS.MSPAY_DATE IS 'The payment date'
/

COMMENT ON COLUMN VDA_V_PAYMENTS.MSPAY_TYPE IS 'The payment type'
/

COMMENT ON COLUMN VDA_V_PAYMENTS.MSPAY_AMT_RECVD IS 'The payment amount received'
/

COMMENT ON COLUMN VDA_V_PAYMENTS.MSPAY_DESC IS 'The payment description'
/

COMMENT ON COLUMN VDA_V_PAYMENTS.MSPAY_COMMENTS IS 'The payment comments'
/

COMMENT ON COLUMN VDA_V_PAYMENTS.PK_MILEPAYMENT IS 'The Primary Key of the payment record'
/

COMMENT ON COLUMN VDA_V_PAYMENTS.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_PAYMENTS.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_PAYMENTS.LAST_MODIFIED_DATE IS 'The date the record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_PAYMENTS.CREATED_ON IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_PAYMENTS.FK_STUDY IS 'The Foreign Key to the study milestone is linked with'
/


/* Formatted on 6/11/2014 6:25:44 AM (QP5 v5.115.810.9015) */
--
-- VDA_V_INVOICE_PAYMENTS_DETAILS  (View)
--
--  Dependencies:
--   F_GET_CODELSTDESC (Function)
--   ER_MILEPAYMENT_DETAILS (Table)
--   VDA_V_PAYMENTS (View)
--

CREATE OR REPLACE  VIEW VDA_V_INVOICE_PAYMENTS_DETAILS
(
   STUDY_NUMBER,
   STUDY_TITLE,
   MSPAY_DATE,
   MSPAY_TYPE,
   MSPAY_AMT_RECVD,
   MSPAY_DESC,
   MSPAY_COMMENTS,
   PK_MILEPAYMENT,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATED_ON,
   FK_STUDY,
   FK_ACCOUNT,
   FK_INVOICE,
   MP_AMOUNT
)
AS
   SELECT   "STUDY_NUMBER",
            "STUDY_TITLE",
            "MSPAY_DATE",
            "MSPAY_TYPE",
            "MSPAY_AMT_RECVD",
            "MSPAY_DESC",
            "MSPAY_COMMENTS",
            "PK_MILEPAYMENT",
            "CREATOR",
            "LAST_MODIFIED_BY",
            "LAST_MODIFIED_DATE",
            "CREATED_ON",
            "FK_STUDY",
            "FK_ACCOUNT",
            "FK_INVOICE",
            "MP_AMOUNT"
          FROM   VDA_V_PAYMENTS,
            (  SELECT   MP_LINKTO_ID fk_invoice,
                        fk_milepayment,
                        SUM (MP_AMOUNT) MP_AMOUNT
                 FROM   eres.ER_MILEPAYMENT_DETAILS
                WHERE   MP_LINKTO_TYPE = 'I'
             GROUP BY   MP_LINKTO_ID, fk_milepayment) x
    WHERE   PK_MILEPAYMENT = FK_MILEPAYMENT
/
COMMENT ON TABLE VDA_V_INVOICE_PAYMENTS_DETAILS IS 'This view provides access to the payment details information for payments linked with invoices'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.FK_ACCOUNT IS 'The account the milestone is linked with'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.FK_INVOICE IS 'The PK to uniquely identify invoice the payment is linked with'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.MP_AMOUNT IS 'The Payment amount linked with the invoice'
/


COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.STUDY_NUMBER IS 'The Study Number of the study the payment  is linked with'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.STUDY_TITLE IS 'The Study Title of the study the payment is linked with'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.MSPAY_DATE IS 'The payment date'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.MSPAY_TYPE IS 'The payment type'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.MSPAY_AMT_RECVD IS 'The payment amount received'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.MSPAY_DESC IS 'The payment description'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.MSPAY_COMMENTS IS 'The payment comments'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.PK_MILEPAYMENT IS 'The Primary Key of the payment record'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.LAST_MODIFIED_DATE IS 'The date the record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.CREATED_ON IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.FK_STUDY IS 'The Foreign Key to the study milestone is linked with'
/



 
INSERT INTO VDA_TRACK_PATCHES
(
  PATCH_PK       ,
  DB_VER_MJR     ,
  DB_VER_MNR     ,
  DB_PATCH_NAME  ,
  FIRED_ON       ,
  APP_VERSION    
) VALUES(SEQ_VDA_TRACK_PATCHES.nextval,5,8,'build15_1_views.sql',sysdate,'VDA 1.4 Build015')
/

commit
/