/* Formatted on 3/11/2014 3:19:06 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_INV_AGED_RECEIVABLES  (View)
--
--  Dependencies:
--   ER_INVOICE (Table)
--   ER_INVOICE_DETAIL (Table)
--   ER_MILEPAYMENT_DETAILS (Table)
--   ER_STUDY (Table)
--

CREATE OR REPLACE FORCE VIEW VDA_V_INV_AGED_RECEIVABLES
(
   PK_INVOICE,
   STUDY_NUMBER,
   STUDY_SPONSOR,
   INV_NUMBER,
   AMOUNT_OVERDUE,
   "0_30D_OVERDUE",
   "31_60D_OVERDUE",
   "61_90D_OVERDUE",
   OVER_90D_OVERDUE,
   INVOICE_AMOUNT,
   PAYMENT
)
AS
     SELECT   PK_INVOICE,
              STUDY_NUMBER,
              STUDY_SPONSOR,
              INV_NUMBER,
              (SUM (NVL (AMOUNT_INVOICED, 0))
               - NVL (
                    (SELECT   SUM (MP_AMOUNT)
                       FROM   eres.ER_MILEPAYMENT_DETAILS
                      WHERE   MP_LINKTO_ID = PK_INVOICE
                              AND MP_LINKTO_TYPE = 'I'),
                    0
                 ))
                 AS AMOUNT_OVERDUE,
              CASE
                 WHEN (TRUNC(SYSDATE
                             - (SELECT   (INV_DATE + eres.F_CALC_DAYS(INV_PAYUNIT,INV_PAYMENT_DUEBY))
                                            AS INVDUEDATE
                                  FROM   eres.ER_INVOICE
                                 WHERE   PK_INVOICE = INV.PK_INVOICE))) BETWEEN 0
                                                                            AND  30
                 THEN
                    (SUM (NVL (AMOUNT_INVOICED, 0))
                     - NVL (
                          (SELECT   SUM (MP_AMOUNT)
                             FROM   eres.ER_MILEPAYMENT_DETAILS
                            WHERE   MP_LINKTO_ID = PK_INVOICE
                                    AND MP_LINKTO_TYPE = 'I'),
                          0
                       ))
                 ELSE
                    0
              END
                 AS "0_30D_OVERDUE",
              CASE
                 WHEN (TRUNC(SYSDATE
                             - (SELECT   (INV_DATE + eres.F_CALC_DAYS(INV_PAYUNIT,INV_PAYMENT_DUEBY))
                                            AS INVDUEDATE
                                  FROM   eres.ER_INVOICE
                                 WHERE   PK_INVOICE = INV.PK_INVOICE))) BETWEEN 31
                                                                            AND  60
                 THEN
                    (SUM (NVL (AMOUNT_INVOICED, 0))
                     - NVL (
                          (SELECT   SUM (MP_AMOUNT)
                             FROM   eres.ER_MILEPAYMENT_DETAILS
                            WHERE   MP_LINKTO_ID = PK_INVOICE
                                    AND MP_LINKTO_TYPE = 'I'),
                          0
                       ))
                 ELSE
                    0
              END
                 AS "31_60D_OVERDUE",
              CASE
                 WHEN (TRUNC(SYSDATE
                             - (SELECT   (INV_DATE + eres.F_CALC_DAYS(INV_PAYUNIT,INV_PAYMENT_DUEBY))
                                            AS INVDUEDATE
                                  FROM   eres.ER_INVOICE
                                 WHERE   PK_INVOICE = INV.PK_INVOICE))) BETWEEN 61
                                                                            AND  90
                 THEN
                    ( (SUM (NVL (AMOUNT_INVOICED, 0)))
                     - NVL (
                          (SELECT   SUM (MP_AMOUNT)
                             FROM   eres.ER_MILEPAYMENT_DETAILS
                            WHERE   MP_LINKTO_ID = PK_INVOICE
                                    AND MP_LINKTO_TYPE = 'I'),
                          0
                       ))
                 ELSE
                    0
              END
                 AS "61_90D_OVERDUE",
              CASE
                 WHEN (TRUNC(SYSDATE
                             - (SELECT   (INV_DATE + eres.F_CALC_DAYS(INV_PAYUNIT,INV_PAYMENT_DUEBY))
                                            AS INVDUEDATE
                                  FROM   eres.ER_INVOICE
                                 WHERE   PK_INVOICE = INV.PK_INVOICE))) > 90
                 THEN
                    (SUM (NVL (AMOUNT_INVOICED, 0))
                     - NVL (
                          (SELECT   SUM (MP_AMOUNT)
                             FROM   eres.ER_MILEPAYMENT_DETAILS
                            WHERE   MP_LINKTO_ID = PK_INVOICE
                                    AND MP_LINKTO_TYPE = 'I'),
                          0
                       ))
                 ELSE
                    0
              END
                 AS "OVER_90D_OVERDUE",
              (SUM (NVL (AMOUNT_INVOICED, 0))) AS INVOICE_AMOUNT,
              NVL (
                 (SELECT   SUM (MP_AMOUNT)
                    FROM   eres.ER_MILEPAYMENT_DETAILS
                   WHERE   MP_LINKTO_ID = PK_INVOICE AND MP_LINKTO_TYPE = 'I'),
                 0
              )
                 AS PAYMENT
       FROM   eres.ER_STUDY STUDY,
              eres.ER_INVOICE INV,
              eres.ER_INVOICE_DETAIL INVDETAIL
      WHERE       STUDY.PK_STUDY = INV.FK_STUDY
              AND INV.PK_INVOICE = INVDETAIL.FK_INV
              AND INVDETAIL.DETAIL_TYPE = 'H'
   GROUP BY   PK_INVOICE,
              STUDY_NUMBER,
              STUDY_SPONSOR,
              INV_NUMBER
     HAVING   (SUM (NVL (AMOUNT_INVOICED, 0))
               - NVL (
                    (SELECT   SUM (MP_AMOUNT)
                       FROM   eres.ER_MILEPAYMENT_DETAILS
                      WHERE   MP_LINKTO_ID = PK_INVOICE
                              AND MP_LINKTO_TYPE = 'I'),
                    0
                 )) > 0
/
COMMENT ON TABLE VDA_V_INV_AGED_RECEIVABLES IS 'This view provides access to aged receivables information. The receivables are calculated by linking with 

payments that are reconciled with invoives and eliminating any invoice that is fully reconciled with a payment.'
/

COMMENT ON COLUMN VDA_V_INV_AGED_RECEIVABLES.PK_INVOICE IS 'Primary Key of Invoice'
/

COMMENT ON COLUMN VDA_V_INV_AGED_RECEIVABLES.STUDY_NUMBER IS 'The Study Number'
/

COMMENT ON COLUMN VDA_V_INV_AGED_RECEIVABLES.STUDY_SPONSOR IS 'The Study Sponsor (from summary)'
/

COMMENT ON COLUMN VDA_V_INV_AGED_RECEIVABLES.INV_NUMBER IS 'The Invoice Number'
/

COMMENT ON COLUMN VDA_V_INV_AGED_RECEIVABLES.AMOUNT_OVERDUE IS 'The amount overdue for the invoice'
/

COMMENT ON COLUMN VDA_V_INV_AGED_RECEIVABLES."0_30D_OVERDUE" IS 'The amount overdue for 0-30 days'
/

COMMENT ON COLUMN VDA_V_INV_AGED_RECEIVABLES."31_60D_OVERDUE" IS 'The amount overdue for 31-60  days'
/

COMMENT ON COLUMN VDA_V_INV_AGED_RECEIVABLES."61_90D_OVERDUE" IS 'The amount overdue for 61-90 days'
/

COMMENT ON COLUMN VDA_V_INV_AGED_RECEIVABLES.OVER_90D_OVERDUE IS 'The amount overdue forover 90 days'
/

COMMENT ON COLUMN VDA_V_INV_AGED_RECEIVABLES.INVOICE_AMOUNT IS 'The original invoice amount'
/

COMMENT ON COLUMN VDA_V_INV_AGED_RECEIVABLES.PAYMENT IS 'The payment amount (if any) reconciled with the invoice'
/

/* Formatted on 3/31/2014 2:41:31 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_SPECIMEN_LATESTSTATUS  (View)
--
--  Dependencies:
--   ER_USER (Table)
--   F_GET_CODELSTDESC (Function)
--   ER_SPECIMEN (Table)
--   ER_SPECIMEN_STATUS (Table)
--   ER_STUDY (Table)
--

CREATE OR REPLACE FORCE VIEW VDA_V_SPECIMEN_LATESTSTATUS
(
   PK_SPECIMEN_STATUS,
   FK_SPECIMEN,
   SS_DATE,
   FK_CODELST_STATUS,
   SS_QUANTITY,
   SS_QUANTITY_UNITS,
   SS_STATUS_BY,
   RID,
   CREATOR,
   CREATED_ON,
   IP_ADD,
   SS_PROC_TYPE,
   SS_PROC_DATE,
   STATUS_DESC,
   PROCESS_TYPE_DESC,
   SPECIMEN_ID,
   LAST_MODIFIED_BY,
   SS_HAND_OFF_DATE,
   SS_NOTES,
   SS_TRACKING_NUMBER,
   FK_USER_RECEPIENT,
   STUDY_NUMBER,
   SS_ACTION
)
AS
   SELECT   x.pk_specimen_status,
            x.fk_specimen,
            x.ss_date,
            x.fk_codelst_status,
            x.ss_quantity,
            eres.F_GET_CODELSTDESC (x.ss_quantity_units) ss_quantity_units,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = x.ss_status_by)
               ss_status_by,
            x.rid,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = x.creator)
               creator,
            x.created_on,
            x.ip_add,
            x.ss_proc_type,
            x.ss_proc_date,
            eres.F_GET_CODELSTDESC (fk_codelst_status) status_desc,
            eres.F_GET_CODELSTDESC (ss_proc_type) process_type_desc,
            (SELECT   spec_id
               FROM   eres.er_specimen sp
              WHERE   pk_specimen = fk_specimen)
               specimen_id,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = x.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            x.SS_HAND_OFF_DATE,
            x.SS_NOTES,
            x.SS_TRACKING_NUMBER,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = x.FK_USER_RECEPIENT)
               FK_USER_RECEPIENT,
            (SELECT   study_number
               FROM   eres.er_study
              WHERE   pk_study = x.FK_STUDY)
               study_number,
            eres.F_GET_CODELSTDESC (SS_ACTION) SS_ACTION 
     FROM   eres.er_specimen_status x,
            (  SELECT   c.fk_specimen,
                        MAX (c.PK_SPECIMEN_STATUS) pk_specimen_status
                 FROM   eres.ER_SPECIMEN_STATUS c
                WHERE   c.ss_date = (SELECT   MAX (d.ss_date)
                                       FROM   eres.ER_SPECIMEN_STATUS d
                                      WHERE   c.fk_SPECIMEN = d.fk_SPECIMEN)
             GROUP BY   fk_specimen) o
    WHERE   x.pk_specimen_status = o.pk_specimen_status
/
COMMENT ON TABLE VDA_V_SPECIMEN_LATESTSTATUS IS 'This view provides latest specimen status. The latest status is calculated by most recent date and most recent entry for that date (in case of more than one status entered on that date).'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.PK_SPECIMEN_STATUS IS 'The Primary key of the Specimen Status record'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.FK_SPECIMEN IS 'The FK to the main specimen record'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.SS_DATE IS 'The Status Date'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.FK_CODELST_STATUS IS 'The codelist value of the specimen status'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.SS_QUANTITY IS 'The quantity number linked with status record'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.SS_QUANTITY_UNITS IS 'The quantity unit linked with status record'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.SS_STATUS_BY IS 'The user who entered the status'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.RID IS 'The unique Row ID for Audit'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.CREATED_ON IS 'The date the record was created on (Audit)'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.IP_ADD IS 'The IP address of the client machine that initiated the session'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.SS_PROC_TYPE IS 'The processing type code'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.SS_PROC_DATE IS 'The prcessing date'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.STATUS_DESC IS 'The Status description'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.PROCESS_TYPE_DESC IS 'The Process Type description'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.SPECIMEN_ID IS 'The Specimen ID'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.SS_HAND_OFF_DATE IS 'The date the record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.SS_NOTES IS 'The notes associated with the status'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.SS_TRACKING_NUMBER IS 'The tracking number linked with the status'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.FK_USER_RECEPIENT IS 'The recepient linked with the status'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.STUDY_NUMBER IS 'The study number linked with the status'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.SS_ACTION IS 'Action performed: incremented/decremented '
/

CREATE OR REPLACE  VIEW VDA_V_SPECIMEN_ALLSTATUS
(
   PK_SPECIMEN_STATUS,
   FK_SPECIMEN,
   SS_DATE,
   FK_CODELST_STATUS,
   SS_QUANTITY,
   SS_QUANTITY_UNITS,
   SS_STATUS_BY,
   RID,
   CREATOR,
   CREATED_ON,
   IP_ADD,
   SS_PROC_TYPE,
   SS_PROC_DATE,
   STATUS_DESC,
   PROCESS_TYPE_DESC,
   SPECIMEN_ID,
   LAST_MODIFIED_BY,
   SS_HAND_OFF_DATE,
   SS_NOTES,
   SS_TRACKING_NUMBER,
   FK_USER_RECEPIENT,
   STUDY_NUMBER,
   SS_ACTION
)
AS
   SELECT   x.pk_specimen_status,
            x.fk_specimen,
            x.ss_date,
            x.fk_codelst_status,
            x.ss_quantity,
            eres.F_GET_CODELSTDESC (x.ss_quantity_units) ss_quantity_units,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = x.ss_status_by)
               ss_status_by,
            x.rid,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = x.creator)
               creator,
            x.created_on,
            x.ip_add,
            x.ss_proc_type,
            x.ss_proc_date,
            eres.F_GET_CODELSTDESC (fk_codelst_status) status_desc,
            eres.F_GET_CODELSTDESC (ss_proc_type) process_type_desc,
            spec_id as   specimen_id,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = x.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            x.SS_HAND_OFF_DATE,
            x.SS_NOTES,
            x.SS_TRACKING_NUMBER,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = x.FK_USER_RECEPIENT)
               FK_USER_RECEPIENT,
            (SELECT   study_number
               FROM   eres.er_study
              WHERE   pk_study = x.FK_STUDY)
               study_number,
            eres.F_GET_CODELSTDESC (SS_ACTION) SS_ACTION 
     FROM   eres.er_specimen_status x, eres.er_specimen sp
	 where pk_specimen = x.fk_specimen 
/

COMMENT ON TABLE VDA_V_SPECIMEN_ALLSTATUS IS 'This view provides access to all specimen statuses'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.PK_SPECIMEN_STATUS IS 'The Primary key of the Specimen Status record'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.FK_SPECIMEN IS 'The FK to the main specimen record'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.SS_DATE IS 'The Status Date'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.FK_CODELST_STATUS IS 'The codelist value of the specimen status'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.SS_QUANTITY IS 'The quantity number linked with status record'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.SS_QUANTITY_UNITS IS 'The quantity unit linked with status record'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.SS_STATUS_BY IS 'The user who entered the status'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.RID IS 'The unique Row ID for Audit'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.CREATED_ON IS 'The date the record was created on (Audit)'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.IP_ADD IS 'The IP address of the client machine that initiated the session'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.SS_PROC_TYPE IS 'The processing type code'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.SS_PROC_DATE IS 'The prcessing date'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.STATUS_DESC IS 'The Status description'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.PROCESS_TYPE_DESC IS 'The Process Type description'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.SPECIMEN_ID IS 'The Specimen ID'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.SS_HAND_OFF_DATE IS 'The date the record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.SS_NOTES IS 'The notes associated with the status'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.SS_TRACKING_NUMBER IS 'The tracking number linked with the status'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.FK_USER_RECEPIENT IS 'The recepient linked with the status'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.STUDY_NUMBER IS 'The study number linked with the status'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.SS_ACTION IS 'Action performed: incremented/decremented '
/

/* Formatted on 3/31/2014 4:34:53 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_SPECIMEN_DETAILS  (View)
--
--  Dependencies:
--   ER_USER (Table)
--   F_GET_CODELSTDESC (Function)
--   ER_CODELST (Table)
--   ER_SITE (Table)
--   ER_SPECIMEN (Table)
--   ER_STORAGE (Table)
--   ER_STUDY (Table)
--   SCH_PROTOCOL_VISIT (Table)
--   SCH_EVENTS1 (Table)
--   PERSON (Table)
--

CREATE OR REPLACE FORCE VIEW VDA_V_SPECIMEN_DETAILS
(
   PK_SPECIMEN,
   SPEC_ID,
   SPEC_ALTERNATE_ID,
   SPEC_DESCRIPTION,
   EXP_SPEC_QTY,
   EXP_QTY_UNITS,
   COLL_SPEC_QTY,
   COLL_QTY_UNITS,
   CURRENT_SPEC_QTY,
   CURRENT_SPEC_QTY_UNITS,
   SPEC_TYPE,
   SPEC_COLLECTION_DATE,
   OWNER,
   SPEC_STORAGE,
   SPEC_KIT_COMPONENT,
   FK_STUDY,
   SPEC_STUDY_NUMBER,
   FK_PER,
   SPEC_PATIENT_CODE,
   SPEC_PATIENT_NAME,
   SPEC_ORGANIZATION,
   VIST_NAME,
   EVENT_DESCRIPTION,
   SPEC_ANATOMIC_SITE,
   SPEC_TISSUE_SIDE,
   SPEC_PATHOLOGY_STAT,
   SPEC_STORAGE_KIT,
   SPEC_NOTES,
   PATHOLOGIST,
   SURGEON,
   COLLECTING_TECHNICIAN,
   PROCESSING_TECHNICIAN,
   SPEC_REMOVAL_TIME,
   SPEC_FREEZE_TIME,
   PARENT_SPEC_ID,
   CREATOR,
   CREATED_ON,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   FK_ACCOUNT,
   RID,
   IP_ADD,
   SPEC_ENVT_CONS,
   SPEC_DISPOSITION
)
AS
   SELECT   a.pk_specimen pk_specimen,
            SPEC_ID,
            SPEC_ALTERNATE_ID,
            SPEC_DESCRIPTION,
            SPEC_EXPECTED_QUANTITY exp_spec_qty,
            (SELECT   codelst_desc
               FROM   eres.ER_CODELST
              WHERE   pk_codelst = SPEC_EXPECTEDQ_UNITS)
               exp_qty_units,
            SPEC_BASE_ORIG_QUANTITY coll_spec_qty,
            (SELECT   codelst_desc
               FROM   eres.ER_CODELST
              WHERE   pk_codelst = SPEC_BASE_ORIGQ_UNITS)
               coll_qty_units,
            SPEC_ORIGINAL_QUANTITY current_spec_qty,
            (SELECT   codelst_desc
               FROM   eres.ER_CODELST
              WHERE   pk_codelst = SPEC_QUANTITY_UNITS)
               current_spec_qty_units,
            (SELECT   codelst_desc
               FROM   eres.ER_CODELST
              WHERE   pk_codelst = SPEC_TYPE)
               SPEC_TYPE,
            SPEC_COLLECTION_DATE,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = SPEC_OWNER_FK_USER)
               AS Owner,
            (SELECT   c.storage_name
               FROM   eres.ER_STORAGE c
              WHERE   c.pk_storage = a.fk_storage)
               spec_storage,
            (SELECT   d.storage_name
               FROM   eres.ER_STORAGE d
              WHERE   d.pk_storage = a.FK_STORAGE_KIT_COMPONENT)
               spec_kit_component,
            a.FK_STUDY fk_study,
            (SELECT   study_number
               FROM   eres.ER_STUDY
              WHERE   pk_study = a.FK_STUDY)
               spec_study_number,
            FK_PER,
            (SELECT   person_code
               FROM   epat.person
              WHERE   PK_person = fk_per)
               AS spec_patient_code,
            (SELECT   person_FNAME || ' ' || person_LNAME
               FROM   epat.person
              WHERE   PK_person = fk_per)
               AS spec_patient_name,
            (SELECT   site_name
               FROM   eres.ER_SITE
              WHERE   pk_site = FK_SITE)
               AS spec_organization,
            (SELECT   visit_name
               FROM   esch.sch_protocol_visit
              WHERE   pk_protocol_visit = fk_visit)
               VIST_NAME,
            (SELECT   description
               FROM   esch.sch_events1
              WHERE   event_id = FK_SCH_EVENTS1)
               Event_Description,
            (SELECT   codelst_desc
               FROM   eres.ER_CODELST
              WHERE   pk_codelst = SPEC_ANATOMIC_SITE)
               SPEC_ANATOMIC_SITE,
            (SELECT   codelst_desc
               FROM   eres.ER_CODELST
              WHERE   pk_codelst = SPEC_TISSUE_SIDE)
               SPEC_TISSUE_SIDE,
            (SELECT   codelst_desc
               FROM   eres.ER_CODELST
              WHERE   pk_codelst = SPEC_PATHOLOGY_STAT)
               SPEC_PATHOLOGY_STAT,
            (SELECT   STORAGE_NAME
               FROM   eres.ER_STORAGE
              WHERE   PK_STORAGE = FK_STORAGE_KIT)
               SPEC_STORAGE_KIT,
            SPEC_NOTES,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = FK_USER_PATH)
               AS Pathologist,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = FK_USER_SURG)
               AS Surgeon,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = FK_USER_COLLTECH)
               AS Collecting_Technician,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = FK_USER_PROCTECH)
               AS Processing_Technician,
            SPEC_REMOVAL_TIME,
            SPEC_FREEZE_TIME,
            (SELECT   aa.SPEC_ID
               FROM   eres.ER_SPECIMEN aa
              WHERE   aa.pk_specimen = a.FK_SPECIMEN)
               parent_spec_id,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = a.CREATOR)
               creator,
            a.CREATED_ON,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = a.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            a.LAST_MODIFIED_DATE,
            FK_ACCOUNT,
            a.RID,
            a.IP_ADD,
            SPEC_ENVT_CONS,
            eres.F_GET_CODELSTDESC (SPEC_DISPOSITION) SPEC_DISPOSITION
     FROM   eres.ER_SPECIMEN a
/

INSERT INTO VDA_TRACK_PATCHES
(
  PATCH_PK       ,
  DB_VER_MJR     ,
  DB_VER_MNR     ,
  DB_PATCH_NAME  ,
  FIRED_ON       ,
  APP_VERSION    
) VALUES(SEQ_VDA_TRACK_PATCHES.nextval,5,3,'01_views.sql',sysdate,'VDA 1.3 Build010')
/

commit
/ 
