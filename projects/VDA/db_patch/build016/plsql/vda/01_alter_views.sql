--
-- VDA_V_USERSITES  (View) 
--
--  Dependencies: 
--   ER_SITE (Table)
--   ER_CODELST (Table)
--   ER_USERSITE (Table)
--   ER_USER (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_USERSITES
(PK_SITE, FK_CODELST_TYPE, FK_ACCOUNT, SITE_NAME, PARENT_SITE, 
 SITE_STAT, PARENT_SITE_NAME, SITETYPE, PK_USERSITE, FK_USER, 
 USERSITE_RIGHT, SITE_ALTID, USER_NAME)
AS 
SELECT   A.PK_SITE,
            A.FK_CODELST_TYPE,
            A.FK_ACCOUNT,
            A.SITE_NAME,
            A.SITE_PARENT AS PARENT_SITE,
            A.SITE_STAT,
            (SELECT   SITE_NAME
               FROM   eres.er_site B
              WHERE   B.PK_SITE = A.SITE_PARENT)
               AS PARENT_SITE_NAME,
            (SELECT   C.CODELST_DESC
               FROM   eres.ER_CODELST C
              WHERE   C.PK_CODELST = A.FK_CODELST_TYPE)
               SITETYPE,
            PK_USERSITE,
            FK_USER,
            DECODE (USERSITE_RIGHT, 5, 'NEW', 6, 'EDIT', 7, 'ALL', 'VIEW')
               USERSITE_RIGHT,
            A.SITE_ALTID,
            usr.USR_FIRSTNAME || ' ' || usr.USR_LASTNAME AS user_name
     FROM   eres.ER_SITE A, eres.ER_USERSITE U, eres.ER_USER usr
    WHERE       U.FK_SITE = A.PK_SITE
            AND usr.pk_user = u.fk_user
            AND usersite_right > 0
/

COMMENT ON TABLE VDA_V_USERSITES IS 'This view provides access to the user-organization access'
/

COMMENT ON COLUMN VDA_V_USERSITES.PK_SITE IS 'The Primary Key of the site'
/

COMMENT ON COLUMN VDA_V_USERSITES.FK_CODELST_TYPE IS 'The FK to code list for site type'
/

COMMENT ON COLUMN VDA_V_USERSITES.FK_ACCOUNT IS 'The account the site is linked with'
/

COMMENT ON COLUMN VDA_V_USERSITES.SITE_NAME IS 'The Site Name'
/

COMMENT ON COLUMN VDA_V_USERSITES.PARENT_SITE IS 'The FK to Parent Site (immediate parent)'
/

COMMENT ON COLUMN VDA_V_USERSITES.SITE_STAT IS 'The Site Status'
/

COMMENT ON COLUMN VDA_V_USERSITES.PARENT_SITE_NAME IS 'The Parent Site Name (immediate parent)'
/

COMMENT ON COLUMN VDA_V_USERSITES.SITETYPE IS 'The Site Type Description'
/

COMMENT ON COLUMN VDA_V_USERSITES.PK_USERSITE IS 'The PK of usersite record'
/

COMMENT ON COLUMN VDA_V_USERSITES.FK_USER IS 'The FK to user'
/

COMMENT ON COLUMN VDA_V_USERSITES.USERSITE_RIGHT IS 'The right'
/

COMMENT ON COLUMN VDA_V_USERSITES.SITE_ALTID IS 'The site alternate id'
/

COMMENT ON COLUMN VDA_V_USERSITES.USER_NAME IS 'The user name'
/


--
-- VDA_V_USERSESSIONLOG  (View) 
--
--  Dependencies: 
--   ER_USERSESSIONLOG (Table)
--   ER_USERSESSION (Table)
--   ER_USER (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_USERSESSIONLOG
(SESSION_ID, LOGIN_TIME, LOGOUT_TIME, IP_ADD, SUCCESS_FLAG, 
 USER_NAME, ACCESSURL, ACCESSTIME)
AS 
SELECT   pk_usersession AS session_id,
            login_time,
            logout_time,
            ip_add,
            success_flag,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   eres.er_user
              WHERE   pk_user = fk_user)
               user_name,
            usl_url accessurl,
            usl_accesstime accesstime
     FROM   eres.er_usersession, eres.er_usersessionlog l
    WHERE   l.fk_usersession = pk_usersession
/

COMMENT ON TABLE VDA_V_USERSESSIONLOG IS 'This view provides access to the details of the user sessions'
/

COMMENT ON COLUMN VDA_V_USERSESSIONLOG.SESSION_ID IS 'The identifier of the session'
/

COMMENT ON COLUMN VDA_V_USERSESSIONLOG.LOGIN_TIME IS 'The recorded login time for this session'
/

COMMENT ON COLUMN VDA_V_USERSESSIONLOG.LOGOUT_TIME IS 'The recorded logout time for this session'
/

COMMENT ON COLUMN VDA_V_USERSESSIONLOG.IP_ADD IS 'The IP address of the client machine that initiated the session'
/

COMMENT ON COLUMN VDA_V_USERSESSIONLOG.SUCCESS_FLAG IS 'The success flag of the session'
/

COMMENT ON COLUMN VDA_V_USERSESSIONLOG.USER_NAME IS 'The name of the user who initiated the session'
/

COMMENT ON COLUMN VDA_V_USERSESSIONLOG.ACCESSURL IS 'The application URL or page visited'
/

COMMENT ON COLUMN VDA_V_USERSESSIONLOG.ACCESSTIME IS 'The time of the URL or page access'
/


--
-- VDA_V_USERSESSION  (View) 
--
--  Dependencies: 
--   ER_USERSESSION (Table)
--   ER_USER (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_USERSESSION
(SESSION_ID, LOGIN_TIME, LOGOUT_TIME, IP_ADD, SUCCESS_FLAG, 
 USER_NAME)
AS 
SELECT   pk_usersession AS session_id,
            login_time,
            logout_time,
            ip_add,
            success_flag,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   eres.er_user
              WHERE   pk_user = fk_user)
               user_name
     FROM   eres.er_usersession
/

COMMENT ON TABLE VDA_V_USERSESSION IS 'This view provides access to the user session records'
/

COMMENT ON COLUMN VDA_V_USERSESSION.SESSION_ID IS 'The identifier of the session'
/

COMMENT ON COLUMN VDA_V_USERSESSION.LOGIN_TIME IS 'The recorded login time for this session'
/

COMMENT ON COLUMN VDA_V_USERSESSION.LOGOUT_TIME IS 'The recorded logout time for this session'
/

COMMENT ON COLUMN VDA_V_USERSESSION.IP_ADD IS 'The IP address of the client machine that initiated the session'
/

COMMENT ON COLUMN VDA_V_USERSESSION.SUCCESS_FLAG IS 'The success flag of the session'
/

COMMENT ON COLUMN VDA_V_USERSESSION.USER_NAME IS 'The name of the user who initiated the session'
/


--
-- VDA_V_USERS  (View) 
--
--  Dependencies: 
--   ER_GRPS (Table)
--   ER_SITE (Table)
--   F_GET_USRTYPE (Function)
--   F_GET_CODELSTDESC (Function)
--   ER_ADD (Table)
--   ER_USER (Table)
--   SCH_TIMEZONES (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_USERS
(USER_TYPE, USER_DEFAULT_GROUP_FK, SITE_NAME, USER_NAME, USER_JOBTYPE, 
 USER_ADDRESS, USER_CITY, USER_STATE, USER_ZIP, USER_COUNTRY, 
 USER_PHONE, USER_EMAIL, USER_TIMEZONE, USER_SPECIALTY, USER_WRKEXP, 
 USER_PHASEINV, USER_DEFAULTGRP, USER_USRNAME, USER_STATUS, FK_ACCOUNT, 
 CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATOR, RID, 
 PK_USER, LAST_MODIFIED_BY_FK, CREATOR_FK)
AS 
SELECT   eres.F_Get_Usrtype (USR_TYPE) USER_TYPE,
            FK_GRP_DEFAULT USER_DEFAULT_GROUP_FK,
            (SELECT   SITE_NAME
               FROM   eres.ER_SITE
              WHERE   PK_SITE = FK_SITEID)
               SITE_NAME,
            o.USR_FIRSTNAME || ' ' || o.USR_LASTNAME AS USER_NAME,
            eres.F_Get_Codelstdesc (FK_CODELST_JOBTYPE) USER_JOBTYPE,
            a.ADDRESS AS USER_ADDRESS,
            a.ADD_CITY AS USER_CITY,
            a.ADD_STATE AS USER_STATE,
            a.ADD_ZIPCODE AS USER_ZIP,
            a.ADD_COUNTRY AS USER_COUNTRY,
            a.ADD_PHONE USER_PHONE,
            a.ADD_EMAIL AS USER_EMAIL,
            (SELECT   TZ_DESCRIPTION
               FROM   esch.SCH_TIMEZONES
              WHERE   PK_TZ = FK_TIMEZONE)
               USER_TIMEZONE,
            eres.F_Get_Codelstdesc (FK_CODELST_SPL) USER_SPECIALTY,
            USR_WRKEXP USER_WRKEXP,
            USR_PAHSEINV USER_PHASEINV,
            (SELECT   GRP_NAME
               FROM   eres.ER_GRPS
              WHERE   PK_GRP = FK_GRP_DEFAULT)
               USER_DEFAULT_GROUP,
            USR_LOGNAME USER_USRNAME,
            DECODE (USR_STAT,
                    'A', 'Active',
                    'B', 'Blocked',
                    'D', 'Deactivated',
                    'Other')
               USER_STATUS,
            FK_ACCOUNT,
            o.CREATED_ON CREATED_ON,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER i
              WHERE   i.PK_USER = o.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            o.LAST_MODIFIED_DATE LAST_MODIFIED_DATE,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER i
              WHERE   i.PK_USER = o.CREATOR)
               CREATOR,
            o.RID,
            PK_USER, o.LAST_MODIFIED_BY as LAST_MODIFIED_BY_FK, o.CREATOR as CREATOR_FK 
     FROM   eres.ER_USER o, eres.ER_ADD a
    WHERE   o.USR_TYPE <> 'X' AND a.PK_ADD = o.FK_PERADD
/

COMMENT ON TABLE VDA_V_USERS IS 'This view provides access to the application users 
information'
/

COMMENT ON COLUMN VDA_V_USERS.CREATOR_FK IS 'The Key to identify the Creator of the 
record.'
/

COMMENT ON COLUMN VDA_V_USERS.USER_TYPE IS 'The Type of the users'
/

COMMENT ON COLUMN VDA_V_USERS.USER_DEFAULT_GROUP_FK IS 'The FK to default group of the 
user'
/

COMMENT ON COLUMN VDA_V_USERS.SITE_NAME IS 'The default organization for the user'
/

COMMENT ON COLUMN VDA_V_USERS.USER_NAME IS 'The name of the user'
/

COMMENT ON COLUMN VDA_V_USERS.USER_JOBTYPE IS 'The user job type'
/

COMMENT ON COLUMN VDA_V_USERS.USER_ADDRESS IS 'The user address first line'
/

COMMENT ON COLUMN VDA_V_USERS.USER_CITY IS 'The user address-city'
/

COMMENT ON COLUMN VDA_V_USERS.USER_STATE IS 'The user address-state'
/

COMMENT ON COLUMN VDA_V_USERS.USER_ZIP IS 'The user address-zip'
/

COMMENT ON COLUMN VDA_V_USERS.USER_COUNTRY IS 'The user address-country'
/

COMMENT ON COLUMN VDA_V_USERS.USER_PHONE IS 'The user address-phone'
/

COMMENT ON COLUMN VDA_V_USERS.USER_EMAIL IS 'The user address-email'
/

COMMENT ON COLUMN VDA_V_USERS.USER_TIMEZONE IS 'The user address-timezone'
/

COMMENT ON COLUMN VDA_V_USERS.USER_SPECIALTY IS 'The user address specialty'
/

COMMENT ON COLUMN VDA_V_USERS.USER_WRKEXP IS 'The user work experience'
/

COMMENT ON COLUMN VDA_V_USERS.USER_PHASEINV IS 'The study phase user has been involved in'
/

COMMENT ON COLUMN VDA_V_USERS.USER_DEFAULTGRP IS 'The user address-city'
/

COMMENT ON COLUMN VDA_V_USERS.USER_USRNAME IS 'The default group of the user'
/

COMMENT ON COLUMN VDA_V_USERS.USER_STATUS IS 'The status of the user'
/

COMMENT ON COLUMN VDA_V_USERS.FK_ACCOUNT IS 'The account the site is linked with'
/

COMMENT ON COLUMN VDA_V_USERS.CREATED_ON IS 'The date the record was created on (Audit)'
/

COMMENT ON COLUMN VDA_V_USERS.LAST_MODIFIED_BY IS 'The user who last modified the record 
(Audit)'
/

COMMENT ON COLUMN VDA_V_USERS.LAST_MODIFIED_DATE IS 'The date the record was last modified 
on (Audit)'
/

COMMENT ON COLUMN VDA_V_USERS.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_USERS.RID IS 'The unique Row ID for Audit'
/

COMMENT ON COLUMN VDA_V_USERS.PK_USER IS 'The PK of the user record'
/

COMMENT ON COLUMN VDA_V_USERS.LAST_MODIFIED_BY_FK IS 'The Key to identify the Last 
Modified By of the record.'
/


--
-- VDA_V_STUDY_SUMMARY  (View) 
--
--  Dependencies: 
--   ER_STUDY (Table)
--   F_GET_YESNO (Function)
--   F_GET_CODELSTDESC (Function)
--   F_GETDIS_SITE (Function)
--   ER_USER (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_STUDY_SUMMARY
(PK_STUDY, STUDY_NUMBER, STUDY_ENTERED_BY, STUDY_PI, STUDY_COORDINATOR, 
 STUDY_MAJ_AUTH, STUDY_TITLE, STUDY_OBJECTIVE, STUDY_SUMMARY, STUDY_AGENT_DEVICE, 
 STUDY_DIVISION, STUDY_TAREA, STUDY_DISEASE_SITE, STUDY_ICDCODE1, STUDY_ICDCODE2, 
 STUDY_NATSAMPSIZE, STUDY_DURATION, STUDY_DURATN_UNIT, STUDY_EST_BEGIN_DATE, STUDY_PHASE, 
 STUDY_RESEARCH_TYPE, STUDY_TYPE, STUDY_LINKED_TO, STUDY_BLINDING, STUDY_RANDOM, 
 STUDY_SPONSOR, SPONSOR_CONTACT, STUDY_INFO, STUDY_KEYWRDS, FK_ACCOUNT, 
 CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATOR, SPONSOR_DD, 
 STUDY_SPONSORID, STUDY_ACTUALDT, STUDY_INVIND_FLAG, CTRP_ACCRREP_LASTRUN_DATE, CTRP_ACCRUAL_GEN_DATE, 
 CTRP_CUTOFF_DATE, CTRP_LAST_DOWNLOAD_BY, NCI_TRIAL_IDENTIFIER, NCT_NUMBER, STUDY_ENTERED_BY_FK, 
 STUDY_PI_FK, STUDY_COORDINATOR_FK, STUDY_DIVISION_FK, STUDY_TAREA_FK, STUDY_DISEASE_SITE_FK, 
 STUDY_PHASE_FK, STUDY_RESEARCH_TYPE_FK, STUDY_TYPE_FK, STUDY_BLINDING_FK, STUDY_RANDOM_FK, 
 CREATOR_FK, LAST_MODIFIED_BY_FK, SPONSOR_DD_FK, CTRP_LAST_DOWNLOAD_BY_FK, STUDY_SCOPE)
AS 
SELECT   pk_study,
            o.STUDY_NUMBER,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = o.FK_AUTHOR)
               STUDY_ENTERED_BY,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = o.STUDY_PRINV)
               STUDY_PI,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   ER_USER.PK_USER = o.STUDY_COORDINATOR)
               STUDY_COORDINATOR,
            ERES.F_GEt_Yesno (o.STUDY_MAJ_AUTH) AS STUDY_MAJ_AUTH,
            STUDY_TITLE,
            STUDY_OBJ_CLOB STUDY_OBJECTIVE,
            STUDY_SUM_CLOB STUDY_SUMMARY,
            STUDY_PRODNAME STUDY_AGENT_DEVICE,
            ERES.F_GEt_Codelstdesc (o.STUDY_DIVISION) STUDY_DIVISION,
            ERES.F_GEt_Codelstdesc (o.FK_CODELST_TAREA) STUDY_TAREA,
            ERES.F_GEtdis_Site (o.STUDY_DISEASE_SITE) STUDY_DISEASE_SITE,
            STUDY_ICDCODE1,
            STUDY_ICDCODE2,
            STUDY_NSAMPLSIZE STUDY_NATSAMPSIZE,
            STUDY_DUR STUDY_DURATION,
            STUDY_DURUNIT STUDY_DURATN_UNIT,
            STUDY_ESTBEGINDT STUDY_EST_BEGIN_DATE,
            ERES.F_GEt_Codelstdesc (o.FK_CODELST_PHASE) STUDY_PHASE,
            ERES.F_GEt_Codelstdesc (o.FK_CODELST_RESTYPE) STUDY_RESEARCH_TYPE,
            ERES.F_GEt_Codelstdesc (o.FK_CODELST_TYPE) STUDY_TYPE,
            DECODE (o.STUDY_ASSOC,
                    NULL, NULL,
                    (SELECT   xx.study_number
                       FROM   eres.er_study xx
                      WHERE   xx.pk_study = o.STUDY_ASSOC))
               STUDY_LINKED_TO,
            ERES.F_GEt_Codelstdesc (o.FK_CODELST_BLIND) STUDY_BLINDING,
            ERES.F_GEt_Codelstdesc (o.FK_CODELST_RANDOM) STUDY_RANDOM,
            STUDY_SPONSOR,
            STUDY_CONTACT SPONSOR_CONTACT,
            STUDY_INFO,
            STUDY_KEYWRDS,
            FK_ACCOUNT,
            CREATED_ON,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = o.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            o.LAST_MODIFIED_DATE,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = o.CREATOR)
               CREATIOR,
            ERES.F_GEt_Codelstdesc (FK_CODELST_SPONSOR) SPONSOR_DD,
            STUDY_SPONSORID,
            STUDY_ACTUALDT,
            ERES.F_GEt_Yesno (o.STUDY_INVIND_FLAG) STUDY_INVIND_FLAG,
            CTRP_ACCRREP_LASTRUN_DATE,
            CTRP_ACCRUAL_GEN_DATE,
            CTRP_CUTOFF_DATE,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = o.CTRP_LAST_DOWNLOAD_BY)
               CTRP_LAST_DOWNLOAD_BY,
            NCI_TRIAL_IDENTIFIER,
            NCT_NUMBER,
            o.FK_AUTHOR AS STUDY_ENTERED_BY_FK,
            o.STUDY_PRINV AS STUDY_PI_FK,
            o.STUDY_COORDINATOR AS STUDY_COORDINATOR_FK,
            o.STUDY_DIVISION AS STUDY_DIVISION_FK,
            o.FK_CODELST_TAREA AS STUDY_TAREA_FK,
            o.STUDY_DISEASE_SITE AS STUDY_DISEASE_SITE_FK,
            o.FK_CODELST_PHASE AS STUDY_PHASE_FK,
            o.FK_CODELST_RESTYPE AS STUDY_RESEARCH_TYPE_FK,
            o.FK_CODELST_TYPE AS STUDY_TYPE_FK,
            o.FK_CODELST_BLIND AS STUDY_BLINDING_FK,
            o.FK_CODELST_RANDOM AS STUDY_RANDOM_FK,
            o.CREATOR AS CREATOR_FK,
            o.LAST_MODIFIED_BY AS LAST_MODIFIED_BY_FK,
            o.FK_CODELST_SPONSOR AS SPONSOR_DD_FK,
            o.CTRP_LAST_DOWNLOAD_BY AS CTRP_LAST_DOWNLOAD_BY_FK,
            ERES.F_GEt_Codelstdesc (o.FK_CODELST_SCOPE)  STUDY_SCOPE 
     FROM   eres.ER_STUDY o
/

COMMENT ON TABLE VDA_V_STUDY_SUMMARY IS 'This view provides access to the study summary 
record'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_PHASE IS 'Study Phase'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_RESEARCH_TYPE IS 'Study Research Type'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_TYPE IS 'Study Type'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_LINKED_TO IS 'Study linked with another study'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_BLINDING IS 'Study Blinding'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_RANDOM IS 'Study Randomization'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_SPONSOR IS 'Study Sponsor Name'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.SPONSOR_CONTACT IS 'Study Sponsor Contact'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_INFO IS 'Study sponsor information'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_KEYWRDS IS 'Study keywords'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.FK_ACCOUNT IS 'The account study is linked with'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.CREATED_ON IS 'The date the record was created on 
(Audit)'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.LAST_MODIFIED_BY IS 'The user who last modified the 
record(Audit)'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.LAST_MODIFIED_DATE IS 'The date the  record was last 
modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.SPONSOR_DD IS 'Study Sponsor'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_SPONSORID IS 'The Sponsor Id for Sponsor Other 
Information'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_ACTUALDT IS 'Study Actual Begin date'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_INVIND_FLAG IS 'Study INV IND Flag'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.CTRP_ACCRREP_LASTRUN_DATE IS 'Date when last CTRP 
accrual was run'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.CTRP_ACCRUAL_GEN_DATE IS 'The Last Date of the CTRP 
Accrual download'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.CTRP_CUTOFF_DATE IS 'The Cutoff Date of the CTRP 
Accrual download'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.CTRP_LAST_DOWNLOAD_BY IS 'The user who last 
downloaded the CTRP Accrual report'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.NCI_TRIAL_IDENTIFIER IS 'The NCI Trial Identifier 
for the Study'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.NCT_NUMBER IS 'The NCT Number for the Study'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_ENTERED_BY_FK IS 'The Key to identify the 
Study Entered By User.'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_PI_FK IS 'The Key to identify the Study PI 
User.'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_COORDINATOR_FK IS 'The Key to identify the 
Study Coordinator User.'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_DIVISION_FK IS 'The Key to identify the Study 
Division Code'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_TAREA_FK IS 'The Key to identify the Study 
Therapeutic Area Code'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_DISEASE_SITE_FK IS 'The Key to identify the 
Study Disease Sites Code'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_PHASE_FK IS 'The Key to identify the Study 
Phase Code'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_RESEARCH_TYPE_FK IS 'The Key to identify the 
Study Research Type Code'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_TYPE_FK IS 'The Key to identify the Study Type 
Key'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_BLINDING_FK IS 'The Key to identify the Study 
Blinding Key'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_RANDOM_FK IS 'The Key to identify the Study 
Randomization Code'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.CREATOR_FK IS 'The Key to identify the Creator of 
the record.'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.LAST_MODIFIED_BY_FK IS 'The Key to identify the Last 
Modified By of the record.'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.SPONSOR_DD_FK IS 'The Key to identify the Sponsor DD 
Code'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.CTRP_LAST_DOWNLOAD_BY_FK IS 'The Key to identify the 
CTRP Last Downloaded By User.'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_SCOPE IS 'The Study Scope'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.PK_STUDY IS 'The Primary Key of the study'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_NUMBER IS 'The Study Number of the study the 
payment  is linked with'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_ENTERED_BY IS 'The user who entered the study 
in the application'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_PI IS 'The Principal Investigator of the study 
The study coordinator (as documented in the study summary)'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_COORDINATOR IS 'The study coordinator (as 
documented in the study summary)'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_MAJ_AUTH IS ' Is the selected Principal 
investigator is the major author of the study'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_TITLE IS 'The Study title'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_OBJECTIVE IS 'The Study Objective'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_SUMMARY IS 'The Study Summary'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_AGENT_DEVICE IS 'Agent or Device'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_DIVISION IS 'Study division'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_TAREA IS 'Study Therapeutic Area'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_DISEASE_SITE IS 'Study Disease Site'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_ICDCODE1 IS 'ICD code 1 linked with the study'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_ICDCODE2 IS 'ICD code 2 linked with the study'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_NATSAMPSIZE IS 'Study National Sample Size'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_DURATION IS 'Estimated duration of the study'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_DURATN_UNIT IS 'Estimated duration unit of the 
study'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_EST_BEGIN_DATE IS 'Estimated begin date of the 
study'
/


--
-- VDA_V_STUDY_CALENDAR  (View) 
--
--  Dependencies: 
--   ER_STUDY (Table)
--   F_GET_DURUNIT (Function)
--   F_GET_DURATION (Function)
--   ER_CATLIB (Table)
--   ER_USER (Table)
--   EVENT_ASSOC (Table)
--   SCH_CODELST (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_STUDY_CALENDAR
(CALENDAR_TYPE, CALENDAR_NAME, CALENDAR_DESC, CALENDAR_STAT, CALENDAR_DURATION, 
 CREATOR, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, CALENDAR_PK, 
 STUDY_NUMBER, FK_ACCOUNT, EVENT_CALASSOCTO, CALENDAR_LINKING_TYPE, LAST_MODIFIED_BY_FK, 
 CREATOR_FK, FK_STUDY)
AS 
SELECT   (SELECT   CATLIB_NAME
               FROM   ERES.ER_CATLIB
              WHERE   PK_CATLIB = FK_CATLIB)
               LBCAL_TYPE,
            NAME LBCAL_NAME,
            DESCRIPTION LBCAL_DESC,
            (SELECT   CODELST_DESC
               FROM   esch.sch_codelst
              WHERE   PK_CODELST = FK_CODELST_CALSTAT)
               LBCAL_STAT,
               ERES.F_GET_DURATION (DURATION_UNIT, DURATION)
            || ' '
            || ERES.F_GET_DURUNIT (DURATION_UNIT)
               LBCAL_DURATION,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = E.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = E.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            E.LAST_MODIFIED_DATE,
            E.CREATED_ON,
            E.EVENT_ID CALENDAR_PK,
            study_number,
            FK_ACCOUNT,
            EVENT_CALASSOCTO,
            DECODE (EVENT_CALASSOCTO,
                    'P', 'Patient',
                    'S', 'Study',
                    EVENT_CALASSOCTO)
               Calendar_linking_type, e.LAST_MODIFIED_BY as LAST_MODIFIED_BY_FK, e.CREATOR 
as CREATOR_FK ,pk_study as FK_STUDY
    FROM   ESCH.EVENT_ASSOC E, eres.er_study
    WHERE   EVENT_TYPE = 'P' AND pk_study = chain_id
/

COMMENT ON TABLE VDA_V_STUDY_CALENDAR IS 'This view provides access to Study Calendars'' 
summary information'
/

COMMENT ON COLUMN VDA_V_STUDY_CALENDAR.CALENDAR_DESC IS 'Calendar Description'
/

COMMENT ON COLUMN VDA_V_STUDY_CALENDAR.CALENDAR_STAT IS 'Calendar Status'
/

COMMENT ON COLUMN VDA_V_STUDY_CALENDAR.CALENDAR_DURATION IS 'Calendar Duration'
/

COMMENT ON COLUMN VDA_V_STUDY_CALENDAR.CREATOR IS 'The user who created the calendar 
record (Audit)'
/

COMMENT ON COLUMN VDA_V_STUDY_CALENDAR.LAST_MODIFIED_BY IS 'The user who last modified the 
calendar record(Audit)'
/

COMMENT ON COLUMN VDA_V_STUDY_CALENDAR.LAST_MODIFIED_DATE IS 'The date the calendar record 
was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_STUDY_CALENDAR.CREATED_ON IS 'The date the calendar record was 
created on (Audit)'
/

COMMENT ON COLUMN VDA_V_STUDY_CALENDAR.CALENDAR_PK IS 'The Primary Key of the Calendar 
record'
/

COMMENT ON COLUMN VDA_V_STUDY_CALENDAR.STUDY_NUMBER IS 'The Study Number of the study the 
payment  is linked with'
/

COMMENT ON COLUMN VDA_V_STUDY_CALENDAR.FK_ACCOUNT IS 'The account the calendar is linked 
with'
/

COMMENT ON COLUMN VDA_V_STUDY_CALENDAR.EVENT_CALASSOCTO IS 'The calendar association: 
Patient Calendar or Admin Calendar'
/

COMMENT ON COLUMN VDA_V_STUDY_CALENDAR.CALENDAR_LINKING_TYPE IS 'The calendar association 
Description'
/

COMMENT ON COLUMN VDA_V_STUDY_CALENDAR.LAST_MODIFIED_BY_FK IS 'The Key to identify the 
Last Modified By of the record.'
/

COMMENT ON COLUMN VDA_V_STUDY_CALENDAR.CREATOR_FK IS 'The Key to identify the Creator of 
the record.'
/

COMMENT ON COLUMN VDA_V_STUDY_CALENDAR.FK_STUDY IS 'The Key to identify the study linked 
with the calendar'
/

COMMENT ON COLUMN VDA_V_STUDY_CALENDAR.CALENDAR_TYPE IS 'Calendar Type'
/

COMMENT ON COLUMN VDA_V_STUDY_CALENDAR.CALENDAR_NAME IS 'Calendar Name'
/


--
-- VDA_V_STUDYTEAM_MEMBERS  (View) 
--
--  Dependencies: 
--   ER_STUDYTEAM (Table)
--   ER_STUDY (Table)
--   ER_SITE (Table)
--   F_GET_CODELSTDESC (Function)
--   ER_ADD (Table)
--   ER_USER (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_STUDYTEAM_MEMBERS
(STUDY_NUMBER, STUDY_TITLE, USER_SITE_NAME, USER_NAME, USER_PHONE, 
 USER_EMAIL, USER_ADDRESS, ROLE, FK_STUDY, CREATED_ON, 
 FK_ACCOUNT, CREATOR, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, RID, 
 PK_STUDYTEAM, STUDYTEAM_STATUS, LAST_MODIFIED_BY_FK, CREATOR_FK)
AS 
SELECT   study_number,
            study_title,
            (SELECT   site_name
               FROM   eres.ER_SITE
              WHERE   pk_site = fk_siteid)
               ORGANIZATION,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   eres.ER_USER
              WHERE   pk_user = fk_user)
               user_name,
            add_phone user_phone,
            add_email user_email,
               address
            || NVL2 (address, NVL2 (add_city, ', ', ''), '')
            || add_city
            || NVL2 (add_city, NVL2 (add_state, ', ', ''), '')
            || add_state
            || NVL2 (add_state, NVL2 (add_zipcode, ' - ', ''), '')
            || add_zipcode
               AS user_address,
            eres.F_GET_CODELSTDESC (fk_codelst_tmrole) ROLE,
            fk_study,
            ER_STUDYTEAM.CREATED_ON,
            ER_USER.fk_account,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = ER_STUDYTEAM.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = ER_STUDYTEAM.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            ER_STUDYTEAM.LAST_MODIFIED_DATE,
            ER_STUDYTEAM.RID,
            PK_STUDYTEAM STEAM_RESPONSE_ID,
            STUDYTEAM_STATUS, ER_STUDYTEAM.LAST_MODIFIED_BY as LAST_MODIFIED_BY_FK, 
ER_STUDYTEAM.CREATOR as CREATOR_FK 
     FROM   eres.ER_STUDY,
            eres.ER_STUDYTEAM,
            eres.ER_USER,
            eres.ER_ADD
    WHERE       ER_STUDY.pk_study = fk_study
            AND pk_user = fk_user
            AND pk_add = fk_peradd
            AND study_team_usr_type = 'D'
/

COMMENT ON TABLE VDA_V_STUDYTEAM_MEMBERS IS 'This view provides access to study team 
member information for all the studies'
/

COMMENT ON COLUMN VDA_V_STUDYTEAM_MEMBERS.CREATOR_FK IS 'The Key to identify the Creator 
of the record.'
/

COMMENT ON COLUMN VDA_V_STUDYTEAM_MEMBERS.STUDY_NUMBER IS 'The Study Number'
/

COMMENT ON COLUMN VDA_V_STUDYTEAM_MEMBERS.STUDY_TITLE IS 'The Study Title'
/

COMMENT ON COLUMN VDA_V_STUDYTEAM_MEMBERS.USER_SITE_NAME IS 'The Site or Organization the 
team member is linked with'
/

COMMENT ON COLUMN VDA_V_STUDYTEAM_MEMBERS.USER_NAME IS 'The Study team member name'
/

COMMENT ON COLUMN VDA_V_STUDYTEAM_MEMBERS.USER_PHONE IS 'The Study Team member''s Phone 
number information'
/

COMMENT ON COLUMN VDA_V_STUDYTEAM_MEMBERS.USER_EMAIL IS 'The Study Team member''s email 
information'
/

COMMENT ON COLUMN VDA_V_STUDYTEAM_MEMBERS.USER_ADDRESS IS 'The Study Team member''s 
address information'
/

COMMENT ON COLUMN VDA_V_STUDYTEAM_MEMBERS.ROLE IS 'The Study Team member''s Role 
information'
/

COMMENT ON COLUMN VDA_V_STUDYTEAM_MEMBERS.FK_STUDY IS 'The FK to the study'
/

COMMENT ON COLUMN VDA_V_STUDYTEAM_MEMBERS.CREATED_ON IS 'The date the record was created 
on (Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYTEAM_MEMBERS.FK_ACCOUNT IS 'The account the study  is linked 
with'
/

COMMENT ON COLUMN VDA_V_STUDYTEAM_MEMBERS.CREATOR IS 'The user who created the record 
(Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYTEAM_MEMBERS.LAST_MODIFIED_BY IS 'The user who last modified 
the record(Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYTEAM_MEMBERS.LAST_MODIFIED_DATE IS 'The date the record was 
last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYTEAM_MEMBERS.RID IS 'The unique Row ID for Audit'
/

COMMENT ON COLUMN VDA_V_STUDYTEAM_MEMBERS.PK_STUDYTEAM IS 'The Primary Key of the Study 
Team record'
/

COMMENT ON COLUMN VDA_V_STUDYTEAM_MEMBERS.STUDYTEAM_STATUS IS 'The Study Team member''s 
status in the team'
/

COMMENT ON COLUMN VDA_V_STUDYTEAM_MEMBERS.LAST_MODIFIED_BY_FK IS 'The Key to identify the 
Last Modified By of the record.'
/


--
-- VDA_V_STUDYSTATUS_TIMELINE  (View) 
--
--  Dependencies: 
--   ER_STUDYSTAT (Table)
--   ER_STUDY (Table)
--   ER_CODELST (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_STUDYSTATUS_TIMELINE
(STUDY_NUMBER, PK_STUDY, STUDY_TITLE, FIRST_NOT_ACTIVE, FIRST_IRB_APPROVED, 
 FIRST_ACTIVE, LAST_PERM_CLOSURE)
AS 
SELECT   study_number,
            pk_study,
            study_title,
            (SELECT   MIN (studystat_date)
               FROM   eres.er_studystat i
              WHERE   i.fk_study = o.pk_study
                      AND fk_codelst_studystat =
                            (SELECT   pk_codelst
                               FROM   eres.er_codelst
                              WHERE   codelst_type = 'studystat'
                                      AND codelst_subtyp = 'not_active'))
               first_not_active,
            (SELECT   MIN (studystat_date)
               FROM   eres.er_studystat i
              WHERE   i.fk_study = o.pk_study
                      AND fk_codelst_studystat =
                            (SELECT   pk_codelst
                               FROM   eres.er_codelst
                              WHERE   codelst_type = 'studystat'
                                      AND codelst_subtyp = 'app_CHR'))
               first_irb_approved,
            (SELECT   MIN (studystat_date)
               FROM   eres.er_studystat i
              WHERE   i.fk_study = o.pk_study
                      AND fk_codelst_studystat =
                            (SELECT   pk_codelst
                               FROM   eres.er_codelst
                              WHERE   codelst_type = 'studystat'
                                      AND codelst_subtyp = 'active'))
               first_active,
            (SELECT   MAX (studystat_date)
               FROM   eres.er_studystat i
              WHERE   i.fk_study = o.pk_study
                      AND fk_codelst_studystat =
                            (SELECT   pk_codelst
                               FROM   eres.er_codelst
                              WHERE   codelst_type = 'studystat'
                                      AND codelst_subtyp = 'prmnt_cls'))
               last_perm_closure
     FROM   eres.er_study o
/

COMMENT ON TABLE VDA_V_STUDYSTATUS_TIMELINE IS 'This view provides access to the study status timeline for predefined trial statuses'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_TIMELINE.STUDY_NUMBER IS 'The Study Number'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_TIMELINE.PK_STUDY IS 'The PK of the study'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_TIMELINE.STUDY_TITLE IS 'The Study Title'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_TIMELINE.FIRST_NOT_ACTIVE IS 'The date study was initiated-first ''Not Active'' or an equivalent status'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_TIMELINE.FIRST_IRB_APPROVED IS 'The date study was IRB Approved-first ''IRB Approved'' or an equivalent status'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_TIMELINE.FIRST_ACTIVE IS 'The date study was Active or Enrolling  -first ''Active/Enrolling'' or an equivalent status'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_TIMELINE.LAST_PERM_CLOSURE IS 'The date study was Closed -last ''Closed or Retired'' or an equivalent status'
/


--
-- VDA_V_STUDYSTATUS_METRICS  (View) 
--
--  Dependencies: 
--   VDA_V_STUDYSTATUS_TIMELINE (View)
--
CREATE OR REPLACE FORCE VIEW VDA_V_STUDYSTATUS_METRICS
(STUDY_NUMBER, PK_STUDY, STUDY_TITLE, FIRST_NOT_ACTIVE, FIRST_IRB_APPROVED, 
 FIRST_ACTIVE, LAST_PERM_CLOSURE, DAYS_IRB_APPROVAL, DAYS_NOTACTIVE_TO_ACTIVE, DAYS_NOTACTIVE_TO_CLOSURE)
AS 
SELECT   x."STUDY_NUMBER",
            x."PK_STUDY",
            x."STUDY_TITLE",
            x."FIRST_NOT_ACTIVE",
            x."FIRST_IRB_APPROVED",
            x."FIRST_ACTIVE",
            x."LAST_PERM_CLOSURE",
            (first_irb_approved - first_not_active) days_irb_approval,
            (first_active - first_not_active) days_notactive_to_active,
            (last_perm_closure - first_not_active) days_notactive_to_closure
     FROM   VDA_V_STUDYSTATUS_TIMELINE x
/

COMMENT ON TABLE VDA_V_STUDYSTATUS_METRICS IS 'This view provides access to the study status timeline metrics for predefined trial statuses'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_METRICS.STUDY_NUMBER IS 'The Study Number'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_METRICS.PK_STUDY IS 'The PK of the study'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_METRICS.STUDY_TITLE IS 'The Study Title'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_METRICS.FIRST_NOT_ACTIVE IS 'The date study was initiated-first ''Not Active'' or an equivalent status'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_METRICS.FIRST_IRB_APPROVED IS 'The date study was IRB Approved-first ''IRB Approved'' or an equivalent status'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_METRICS.FIRST_ACTIVE IS 'The date study was Active or Enrolling  -first ''Active/Enrolling'' or an equivalent status'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_METRICS.LAST_PERM_CLOSURE IS 'The date study was Closed -last ''Closed or Retired'' or an equivalent status'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_METRICS.DAYS_IRB_APPROVAL IS 'Number of days it took from study initiation to IRB Approval'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_METRICS.DAYS_NOTACTIVE_TO_ACTIVE IS 'Number of days it took from study initiation to Study Active/Enrolling'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_METRICS.DAYS_NOTACTIVE_TO_CLOSURE IS 'Number of days it took from study initiation to Study Closure/Completion'
/


--
-- VDA_V_STUDYSTAT  (View) 
--
--  Dependencies: 
--   ER_STUDYSTAT (Table)
--   ER_STUDY (Table)
--   ER_SITE (Table)
--   F_GET_CODELSTDESC (Function)
--   ER_USER (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_STUDYSTAT
(STUDY_NUMBER, STUDY_START_DATE, STUDY_END_DATE, SSTAT_SITE_NAME, SSTAT_STUDY_STATUS, 
 SSTAT_VALID_FROM, SSTAT_VALID_UNTIL, SSTAT_NOTES, SSTAT_DOCUMNTD_BY, SSTAT_END_DATE_SYSTEM, 
 CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, PK_STUDYSTAT, 
 FK_STUDY, SSTAT_FK_SITE, FK_ACCOUNT, SSTAT_CURRENT_STAT, SSTAT_STATUS_TYPE_FK, 
 SSTAT_STATUS_TYPE, LAST_MODIFIED_BY_FK, CREATOR_FK)
AS 
SELECT   STUDY_NUMBER,
            STUDY_ACTUALDT,
            STUDY_END_DATE,
            (SELECT   SITE_NAME
               FROM   eres.ER_SITE
              WHERE   PK_SITE = FK_SITE)
               SSTAT_ORGANIZTN,
            ERES.F_GET_CODELSTDESC (FK_CODELST_STUDYSTAT) SSTAT_STUDY_STATUS,
            STUDYSTAT_DATE,
            STUDYSTAT_VALIDT,
            STUDYSTAT_NOTE SSTAT_NOTES,
            (SELECT   ER_USER.USR_FIRSTNAME || ' ' || ER_USER.USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = FK_USER_DOCBY)
               SSTAT_DOCUMNTD_BY,
            STUDYSTAT_ENDT SSTAT_END_DATE,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = ER_STUDYSTAT.CREATOR)
               CREATOR,
            ER_STUDYSTAT.CREATED_ON,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = ER_STUDYSTAT.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            ER_STUDYSTAT.LAST_MODIFIED_DATE,
            PK_STUDYSTAT,
            FK_STUDY,
            FK_SITE,
            FK_ACCOUNT,
            DECODE (current_stat, 1, 'Yes', 'No') current_stat,
            STATUS_TYPE as SSTAT_STATUS_TYPE_FK, 
            ERES.F_GET_CODELSTDESC(STATUS_TYPE ) 
SSTAT_STATUS_TYPE,ER_STUDYSTAT.LAST_MODIFIED_BY as LAST_MODIFIED_BY_FK, 
ER_STUDYSTAT.CREATOR as CREATOR_FK 
     FROM   eres.ER_STUDYSTAT, eres.ER_STUDY
    WHERE   ER_STUDYSTAT.FK_STUDY = ER_STUDY.PK_STUDY
/

COMMENT ON TABLE VDA_V_STUDYSTAT IS 'This view provides access to the study status history 
data'
/

COMMENT ON COLUMN VDA_V_STUDYSTAT.SSTAT_CURRENT_STAT IS 'The study status marked as 
current'
/

COMMENT ON COLUMN VDA_V_STUDYSTAT.SSTAT_STATUS_TYPE_FK IS 'The Key to identify the study 
status type code'
/

COMMENT ON COLUMN VDA_V_STUDYSTAT.SSTAT_STATUS_TYPE IS 'The study status type code'
/

COMMENT ON COLUMN VDA_V_STUDYSTAT.LAST_MODIFIED_BY_FK IS 'The Key to identify the Last 
Modified By of the record.'
/

COMMENT ON COLUMN VDA_V_STUDYSTAT.CREATOR_FK IS 'The Key to identify the Creator of the 
record.'
/

COMMENT ON COLUMN VDA_V_STUDYSTAT.STUDY_NUMBER IS 'The Study Number of the study status is 
linked with'
/

COMMENT ON COLUMN VDA_V_STUDYSTAT.STUDY_START_DATE IS 'The Study Status Start Date'
/

COMMENT ON COLUMN VDA_V_STUDYSTAT.STUDY_END_DATE IS 'The Study Status End Date'
/

COMMENT ON COLUMN VDA_V_STUDYSTAT.SSTAT_SITE_NAME IS 'The site the status is linked with'
/

COMMENT ON COLUMN VDA_V_STUDYSTAT.SSTAT_STUDY_STATUS IS 'The Study Status description'
/

COMMENT ON COLUMN VDA_V_STUDYSTAT.SSTAT_VALID_FROM IS 'The study status valid from'
/

COMMENT ON COLUMN VDA_V_STUDYSTAT.SSTAT_VALID_UNTIL IS 'The study status valid until'
/

COMMENT ON COLUMN VDA_V_STUDYSTAT.SSTAT_NOTES IS 'The study status notes'
/

COMMENT ON COLUMN VDA_V_STUDYSTAT.SSTAT_DOCUMNTD_BY IS 'The study status documented by'
/

COMMENT ON COLUMN VDA_V_STUDYSTAT.SSTAT_END_DATE_SYSTEM IS 'The study end date generated 
by the system'
/

COMMENT ON COLUMN VDA_V_STUDYSTAT.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYSTAT.CREATED_ON IS 'The date the record was created on 
(Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYSTAT.LAST_MODIFIED_BY IS 'The date the record was last 
modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYSTAT.LAST_MODIFIED_DATE IS 'The date the record was last 
modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYSTAT.PK_STUDYSTAT IS 'The Primary Key of the status record'
/

COMMENT ON COLUMN VDA_V_STUDYSTAT.FK_STUDY IS 'The Foreign key of the study the status is 
linked with'
/

COMMENT ON COLUMN VDA_V_STUDYSTAT.SSTAT_FK_SITE IS 'The Foreign Key to the Study Site 
record linked with the status'
/

COMMENT ON COLUMN VDA_V_STUDYSTAT.FK_ACCOUNT IS 'The account the query is linked with'
/


--
-- VDA_V_STUDYSITES  (View) 
--
--  Dependencies: 
--   ER_STUDYSITES (Table)
--   ER_STUDY (Table)
--   ER_SITE (Table)
--   GETUSER (Function)
--   ER_CODELST (Table)
--   ER_USER (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_STUDYSITES
(PK_STUDYSITES, FK_STUDY, FK_SITE, STUDYSITE_TYPE, STUDYSITE_LSAMPLESIZE, 
 IS_STUDYSITE_PUBLIC, INCLUDE_IN_REPORTS, RID, CREATOR, LAST_MODIFIED_BY, 
 CREATED_ON, LAST_MODIFIED_ON, IP_ADD, STUDYSITE_ENRCOUNT, IS_REVIEWBASED_ENR, 
 SEND_ENROLLMENT_NOTIFICATION, SEND_ENROLLMENT_APPROVAL, STUDY_NUMBER, SITE_NAME, FK_ACCOUNT, 
 LAST_MODIFIED_BY_FK, CREATOR_FK)
AS 
SELECT   si.PK_STUDYSITES,
            si.FK_STUDY,
            si.FK_SITE,
            (SELECT   codelst_desc
               FROM   eres.er_codelst
              WHERE   pk_codelst = si.FK_CODELST_STUDYSITETYPE)
               STUDYSITE_TYPE,
            si.STUDYSITE_LSAMPLESIZE,
            DECODE (si.STUDYSITE_PUBLIC,
                    '0',
                    'No',
                    1,
                    'Yes')
               is_studysite_public,
            DECODE (si.STUDYSITE_REPINCLUDE,
                    '0',
                    'No',
                    1,
                    'Yes')
               include_in_reports,
            si.RID,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = si.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = si.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            si.CREATED_ON,
            si.LAST_MODIFIED_ON,
            si.IP_ADD,
            si.STUDYSITE_ENRCOUNT,
            DECODE (si.IS_REVIEWBASED_ENR,
                    '0',
                    'No',
                    1,
                    'Yes')
               IS_REVIEWBASED_ENR,
            eres.GETUSER (si.ENR_NOTIFYTO) send_enrollment_notification,
            eres.GETUSER (si.ENR_STAT_NOTIFYTO) send_enrollment_approval,
            st.study_number,
            (SELECT   site_name
               FROM   eres.er_site
              WHERE   pk_site = si.fk_site)
               site_name,
            st.fk_account, si.LAST_MODIFIED_BY as LAST_MODIFIED_BY_FK, si.CREATOR as 
CREATOR_FK 
     FROM   eres.er_studysites si, eres.er_study st
    WHERE   st.pk_study = si.fk_study
/

COMMENT ON TABLE VDA_V_STUDYSITES IS 'This view provides information on the sites or 
organizations associated with a study'
/

COMMENT ON COLUMN VDA_V_STUDYSITES.STUDYSITE_LSAMPLESIZE IS 'The local sample size for 
this study site'
/

COMMENT ON COLUMN VDA_V_STUDYSITES.IS_STUDYSITE_PUBLIC IS 'Flag for  information to be 
available to the public'
/

COMMENT ON COLUMN VDA_V_STUDYSITES.INCLUDE_IN_REPORTS IS 'Flag for study site to be 
included in reports'
/

COMMENT ON COLUMN VDA_V_STUDYSITES.RID IS 'This column is used for the audit trail. The 
RID uniquely identifies the row in the database.'
/

COMMENT ON COLUMN VDA_V_STUDYSITES.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYSITES.LAST_MODIFIED_BY IS 'The user who last modified the 
record(Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYSITES.CREATED_ON IS 'The date the record was created on 
(Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYSITES.LAST_MODIFIED_ON IS 'The date the  record was last 
modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYSITES.IP_ADD IS 'The IP ADDRESS of the client machine 
(Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYSITES.STUDYSITE_ENRCOUNT IS 'The number of current patients 
enrolled (with any status) for the study-site combination'
/

COMMENT ON COLUMN VDA_V_STUDYSITES.IS_REVIEWBASED_ENR IS 'Identifies if the site will have 
review based enrollment.'
/

COMMENT ON COLUMN VDA_V_STUDYSITES.SEND_ENROLLMENT_NOTIFICATION IS 'Identifies the users 
who will be notified when an ''enrolled'' status is added'
/

COMMENT ON COLUMN VDA_V_STUDYSITES.SEND_ENROLLMENT_APPROVAL IS 'Identifies the users who 
will be notified when a patient status with code_subtyp = "enroll_appr" or code_subtyp = 
"enroll_denied" status is added'
/

COMMENT ON COLUMN VDA_V_STUDYSITES.STUDY_NUMBER IS 'The Study number of the associated 
study'
/

COMMENT ON COLUMN VDA_V_STUDYSITES.SITE_NAME IS 'The name of the associated site'
/

COMMENT ON COLUMN VDA_V_STUDYSITES.FK_ACCOUNT IS 'The account the study site is linked 
with'
/

COMMENT ON COLUMN VDA_V_STUDYSITES.LAST_MODIFIED_BY_FK IS 'The Key to identify the Last 
Modified By of the record.'
/

COMMENT ON COLUMN VDA_V_STUDYSITES.CREATOR_FK IS 'The Key to identify the Creator of the 
record.'
/

COMMENT ON COLUMN VDA_V_STUDYSITES.PK_STUDYSITES IS 'Primary key'
/

COMMENT ON COLUMN VDA_V_STUDYSITES.FK_STUDY IS 'This column identifies the study to which 
this study site is associated'
/

COMMENT ON COLUMN VDA_V_STUDYSITES.FK_SITE IS 'This column identifies the site'
/

COMMENT ON COLUMN VDA_V_STUDYSITES.STUDYSITE_TYPE IS 'This column identifies the study 
site type'
/


--
-- VDA_V_STUDYPAT_QUERY  (View) 
--
--  Dependencies: 
--   ER_FORMSEC (Table)
--   ER_FORMQUERYSTATUS (Table)
--   ER_FORMQUERY (Table)
--   ER_FORMLIB (Table)
--   ER_FORMFLD (Table)
--   ER_FLDLIB (Table)
--   ER_STUDY (Table)
--   ER_SITE (Table)
--   ER_CODELST (Table)
--   ER_USER (Table)
--   ER_PER (Table)
--   ER_PATPROT (Table)
--   ER_PATFORMS (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_STUDYPAT_QUERY
(PATIENT_ID, PATIENT_STUDY_ID, STUDY_NUMBER, FORM_NAME, FORMSEC_NAME, 
 FIELD, QUERY_TYPE, QUERYDESC, QUERY_NOTES, QUERY_STATUS, 
 CREATOR, QUERY_CREATED_ON, FK_FORMQUERY, FK_QUERYMODULE, PATFORMS_FILLDATE, 
 FK_STUDY, FIELD_ID, FK_SITE_ENROLLING, ENROLLING_SITE_NAME, QUERY_STATUS_DATE, 
 FK_ACCOUNT, LAST_MODIFIED_BY_FK, CREATOR_FK)
AS 
SELECT   (SELECT   per_code
               FROM   eres.ER_PER
              WHERE   pk_per = g.fk_per)
               AS patient_id,
            patprot_patstdid AS patient_study_id,
            (SELECT   study_number
               FROM   eres.ER_STUDY
              WHERE   pk_study = fk_study)
               AS study_number,
            form_name,
            formsec_name,
            fld_name AS field,
            DECODE (entered_by, '0', 'System Query', 'Manual Query')
               AS query_type,
            (SELECT   codelst_desc
               FROM   eres.ER_CODELST
              WHERE   fk_codelst_querytype = pk_codelst)
               AS querydesc,
            query_notes,
            (SELECT   codelst_desc
               FROM   eres.ER_CODELST
              WHERE   fk_codelst_querystatus = pk_codelst)
               AS query_status,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   eres.ER_USER
              WHERE   pk_user = a.creator)
               AS creator,
            TRUNC (a.created_on) query_created_on,
            fk_formquery,
            fk_querymodule,
            patforms_filldate,
            g.fk_study,
            fld.fld_uniqueid AS field_id,
            fk_site_enrolling,
            (SELECT   site_name
               FROM   eres.er_site
              WHERE   pk_site = fk_site_enrolling)
               AS enrolling_site_name,
            TRUNC (b.entered_on) AS query_status_date,
            e.fk_account, a.LAST_MODIFIED_BY as LAST_MODIFIED_BY_FK, a.CREATOR as 
CREATOR_FK 
     FROM   eres.ER_FORMQUERY a,
            eres.ER_FORMQUERYSTATUS b,
            eres.ER_FORMFLD c,
            eres.ER_FORMSEC d,
            eres.ER_FORMLIB e,
            eres.ER_PATFORMS f,
            eres.ER_PATPROT g,
            eres.ER_FLDLIB fld
    WHERE   a.QUERYMODULE_LINKEDTO = 4 AND b.fk_formquery = a.pk_formquery
            AND b.PK_FORMQUERYSTATUS =
                  (SELECT   MAX (b1.PK_FORMQUERYSTATUS)
                     FROM   eres.ER_FORMQUERYSTATUS b1
                    WHERE   a.pk_formquery = b1.fk_formquery
                            AND TRIM (b1.ENTERED_ON) =
                                  (SELECT   MAX (TRIM (b2.ENTERED_ON))
                                     FROM   eres.ER_FORMQUERYSTATUS b2
                                    WHERE   a.pk_formquery = b2.fk_formquery))
            AND a.fk_field = c.fk_field
            AND d.pk_formsec = c.fk_formsec
            AND e.pk_formlib = d.fk_formlib
            AND pk_formlib = f.fk_formlib
            AND f.RECORD_TYPE <> 'D'
            AND pk_patforms = fk_querymodule
            AND pk_patprot = fk_patprot
            AND pk_field = a.fk_field
/

COMMENT ON TABLE VDA_V_STUDYPAT_QUERY IS 'This view provides access to the study patient 
form queries'
/

COMMENT ON COLUMN VDA_V_STUDYPAT_QUERY.PATIENT_ID IS 'The Patient ID with which the query 
is linked'
/

COMMENT ON COLUMN VDA_V_STUDYPAT_QUERY.PATIENT_STUDY_ID IS 'The Patient Study ID with 
which the query is linked'
/

COMMENT ON COLUMN VDA_V_STUDYPAT_QUERY.STUDY_NUMBER IS 'The study number'
/

COMMENT ON COLUMN VDA_V_STUDYPAT_QUERY.FORM_NAME IS 'The name of the form'
/

COMMENT ON COLUMN VDA_V_STUDYPAT_QUERY.FORMSEC_NAME IS 'The form section name'
/

COMMENT ON COLUMN VDA_V_STUDYPAT_QUERY.FIELD IS 'The field with which the query is linked'
/

COMMENT ON COLUMN VDA_V_STUDYPAT_QUERY.QUERY_TYPE IS 'The Query Type'
/

COMMENT ON COLUMN VDA_V_STUDYPAT_QUERY.QUERYDESC IS 'The Query Description'
/

COMMENT ON COLUMN VDA_V_STUDYPAT_QUERY.QUERY_NOTES IS 'The notes linked with the query'
/

COMMENT ON COLUMN VDA_V_STUDYPAT_QUERY.QUERY_STATUS IS 'The most recent Query status'
/

COMMENT ON COLUMN VDA_V_STUDYPAT_QUERY.CREATOR IS 'The user who created the record 
(Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYPAT_QUERY.QUERY_CREATED_ON IS 'The date the query record was 
created on'
/

COMMENT ON COLUMN VDA_V_STUDYPAT_QUERY.FK_FORMQUERY IS 'The Foreign Key to the main query 
record'
/

COMMENT ON COLUMN VDA_V_STUDYPAT_QUERY.FK_QUERYMODULE IS 'The FK of the query module(e.g.: 
in case of forms it stores the PK of the filled form'
/

COMMENT ON COLUMN VDA_V_STUDYPAT_QUERY.PATFORMS_FILLDATE IS 'The fill date of patient 
study form response'
/

COMMENT ON COLUMN VDA_V_STUDYPAT_QUERY.FK_STUDY IS 'The Foreign key of the study the query 
is linked with'
/

COMMENT ON COLUMN VDA_V_STUDYPAT_QUERY.FIELD_ID IS 'The Field ID as defined in form 
design'
/

COMMENT ON COLUMN VDA_V_STUDYPAT_QUERY.FK_SITE_ENROLLING IS 'The Foreign Key to the 
patient enrolling site record'
/

COMMENT ON COLUMN VDA_V_STUDYPAT_QUERY.ENROLLING_SITE_NAME IS 'The patient enrolling site 
name'
/

COMMENT ON COLUMN VDA_V_STUDYPAT_QUERY.QUERY_STATUS_DATE IS 'The Query status date'
/

COMMENT ON COLUMN VDA_V_STUDYPAT_QUERY.FK_ACCOUNT IS 'The account the query is linked 
with'
/

COMMENT ON COLUMN VDA_V_STUDYPAT_QUERY.LAST_MODIFIED_BY_FK IS 'The Key to identify the 
Last Modified By of the record.'
/

COMMENT ON COLUMN VDA_V_STUDYPAT_QUERY.CREATOR_FK IS 'The Key to identify the Creator of 
the record.'
/


--
-- VDA_V_STUDYFORMS_AUDIT  (View) 
--
--  Dependencies: 
--   ER_FORMLIB (Table)
--   ER_FORMAUDITCOL (Table)
--   ER_STUDYFORMS (Table)
--   ER_STUDY (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_STUDYFORMS_AUDIT
(PK_FORMAUDITCOL, FK_FILLEDFORM, FK_FORM, FORM_NAME, FA_SYSTEMID, 
 FA_DATETIME, FA_FLDNAME, FA_OLDVALUE, FA_NEWVALUE, FA_MODIFIEDBY_NAME, 
 FA_REASON, STUDY_NUMBER, STUDY_TITLE, FK_ACCOUNT)
AS 
SELECT   PK_FORMAUDITCOL,
            FK_FILLEDFORM,
            FK_FORM,
            form_name,
            FA_SYSTEMID,
            FA_DATETIME,
            FA_FLDNAME,
            FA_OLDVALUE,
            FA_NEWVALUE,
            FA_MODIFIEDBY_NAME,
            FA_REASON,
            study_number,
            study_title,
            b.fk_account
     FROM   eres.er_formauditcol,
            eres.er_formlib b,
            eres.er_studyforms sf,
            eres.er_study
    WHERE       FA_FORMTYPE = 'S'
            AND b.pk_formlib = FK_FORM
            AND sf.pk_studyforms = FK_FILLEDFORM
            AND sf.fk_formlib = FK_FORM
            AND pk_study = fk_study
/

COMMENT ON TABLE VDA_V_STUDYFORMS_AUDIT IS 'This view provides access to the column update level Audit information of account level forms'
/

COMMENT ON COLUMN VDA_V_STUDYFORMS_AUDIT.PK_FORMAUDITCOL IS 'The Primary Key of the Audit Transaction'
/

COMMENT ON COLUMN VDA_V_STUDYFORMS_AUDIT.FK_FILLEDFORM IS 'The FK to the form response (account form)'
/

COMMENT ON COLUMN VDA_V_STUDYFORMS_AUDIT.FK_FORM IS 'The FK to the form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMS_AUDIT.FORM_NAME IS 'The form name'
/

COMMENT ON COLUMN VDA_V_STUDYFORMS_AUDIT.FA_SYSTEMID IS 'The systemid of the field'
/

COMMENT ON COLUMN VDA_V_STUDYFORMS_AUDIT.FA_DATETIME IS 'The date-time of the transaction'
/

COMMENT ON COLUMN VDA_V_STUDYFORMS_AUDIT.FA_FLDNAME IS 'The user defined field name'
/

COMMENT ON COLUMN VDA_V_STUDYFORMS_AUDIT.FA_OLDVALUE IS 'The old value of the field'
/

COMMENT ON COLUMN VDA_V_STUDYFORMS_AUDIT.FA_NEWVALUE IS 'The new value of the field'
/

COMMENT ON COLUMN VDA_V_STUDYFORMS_AUDIT.FA_MODIFIEDBY_NAME IS 'The user who modified the form response data'
/

COMMENT ON COLUMN VDA_V_STUDYFORMS_AUDIT.FA_REASON IS 'The reason for change'
/

COMMENT ON COLUMN VDA_V_STUDYFORMS_AUDIT.STUDY_NUMBER IS 'The study number of the study the form response is linked with'
/

COMMENT ON COLUMN VDA_V_STUDYFORMS_AUDIT.STUDY_TITLE IS 'The title of the study the form response is linked with'
/

COMMENT ON COLUMN VDA_V_STUDYFORMS_AUDIT.FK_ACCOUNT IS 'The account the form response is linked with'
/


--
-- VDA_V_STUDYFORMRESPONSES  (View) 
--
--  Dependencies: 
--   ER_FORMSLINEAR (Table)
--   ER_FORMLIB (Table)
--   ER_STUDY (Table)
--   ER_USER (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_STUDYFORMRESPONSES
(FORM_NAME, FORM_DESC, FORM_VERSION, FK_FORM, FILLDATE, 
 FORM_TYPE, FK_STUDY, STUDY_NUMBER, COL1, COL2, 
 COL3, COL4, COL5, COL6, COL7, 
 COL8, COL9, COL10, COL11, COL12, 
 COL13, COL14, COL15, COL16, COL17, 
 COL18, COL19, COL20, COL21, COL22, 
 COL23, COL24, COL25, COL26, COL27, 
 COL28, COL29, COL30, COL31, COL32, 
 COL33, COL34, COL35, COL36, COL37, 
 COL38, COL39, COL40, COL41, COL42, 
 COL43, COL44, COL45, COL46, COL47, 
 COL48, COL49, COL50, COL51, COL52, 
 COL53, COL54, COL55, COL56, COL57, 
 COL58, COL59, COL60, COL61, COL62, 
 COL63, COL64, COL65, COL66, COL67, 
 COL68, COL69, COL70, COL71, COL72, 
 COL73, COL74, COL75, COL76, COL77, 
 COL78, COL79, COL80, COL81, COL82, 
 COL83, COL84, COL85, COL86, COL87, 
 COL88, COL89, COL90, COL91, COL92, 
 COL93, COL94, COL95, COL96, COL97, 
 COL98, COL99, COL100, COL101, COL102, 
 COL103, COL104, COL105, COL106, COL107, 
 COL108, COL109, COL110, COL111, COL112, 
 COL113, COL114, COL115, COL116, COL117, 
 COL118, COL119, COL120, COL121, COL122, 
 COL123, COL124, COL125, COL126, COL127, 
 COL128, COL129, COL130, COL131, COL132, 
 COL133, COL134, COL135, COL136, COL137, 
 COL138, COL139, COL140, COL141, COL142, 
 COL143, COL144, COL145, COL146, COL147, 
 COL148, COL149, COL150, COL151, COL152, 
 COL153, COL154, COL155, COL156, COL157, 
 COL158, COL159, COL160, COL161, COL162, 
 COL163, COL164, COL165, COL166, COL167, 
 COL168, COL169, COL170, COL171, COL172, 
 COL173, COL174, COL175, COL176, COL177, 
 COL178, COL179, COL180, COL181, COL182, 
 COL183, COL184, COL185, COL186, COL187, 
 COL188, COL189, COL190, COL191, COL192, 
 COL193, COL194, COL195, COL196, COL197, 
 COL198, COL199, COL200, COL201, COL202, 
 COL203, COL204, COL205, COL206, COL207, 
 COL208, COL209, COL210, COL211, COL212, 
 COL213, COL214, COL215, COL216, COL217, 
 COL218, COL219, COL220, COL221, COL222, 
 COL223, COL224, COL225, COL226, COL227, 
 COL228, COL229, COL230, COL231, COL232, 
 COL233, COL234, COL235, COL236, COL237, 
 COL238, COL239, COL240, COL241, COL242, 
 COL243, COL244, COL245, COL246, COL247, 
 COL248, COL249, COL250, COL251, COL252, 
 COL253, COL254, COL255, COL256, COL257, 
 COL258, COL259, COL260, COL261, COL262, 
 COL263, COL264, COL265, COL266, COL267, 
 COL268, COL269, COL270, COL271, COL272, 
 COL273, COL274, COL275, COL276, COL277, 
 COL278, COL279, COL280, COL281, COL282, 
 COL283, COL284, COL285, COL286, COL287, 
 COL288, COL289, COL290, COL291, COL292, 
 COL293, COL294, COL295, COL296, COL297, 
 COL298, COL299, COL300, COL301, COL302, 
 COL303, COL304, COL305, COL306, COL307, 
 COL308, COL309, COL310, COL311, COL312, 
 COL313, COL314, COL315, COL316, COL317, 
 COL318, COL319, COL320, COL321, COL322, 
 COL323, COL324, COL325, COL326, COL327, 
 COL328, COL329, COL330, COL331, COL332, 
 COL333, COL334, COL335, COL336, COL337, 
 COL338, COL339, COL340, COL341, COL342, 
 COL343, COL344, COL345, COL346, COL347, 
 COL348, COL349, COL350, COL351, COL352, 
 COL353, COL354, COL355, COL356, COL357, 
 COL358, COL359, COL360, COL361, COL362, 
 COL363, COL364, COL365, COL366, COL367, 
 COL368, COL369, COL370, COL371, COL372, 
 COL373, COL374, COL375, COL376, COL377, 
 COL378, COL379, COL380, COL381, COL382, 
 COL383, COL384, COL385, COL386, COL387, 
 COL388, COL389, COL390, COL391, COL392, 
 COL393, COL394, COL395, COL396, COL397, 
 COL398, COL399, COL400, COL401, COL402, 
 COL403, COL404, COL405, COL406, COL407, 
 COL408, COL409, COL410, COL411, COL412, 
 COL413, COL414, COL415, COL416, COL417, 
 COL418, COL419, COL420, COL421, COL422, 
 COL423, COL424, COL425, COL426, COL427, 
 COL428, COL429, COL430, COL431, COL432, 
 COL433, COL434, COL435, COL436, COL437, 
 COL438, COL439, COL440, COL441, COL442, 
 COL443, COL444, COL445, COL446, COL447, 
 COL448, COL449, COL450, COL451, COL452, 
 COL453, COL454, COL455, COL456, COL457, 
 COL458, COL459, COL460, COL461, COL462, 
 COL463, COL464, COL465, COL466, COL467, 
 COL468, COL469, COL470, COL471, COL472, 
 COL473, COL474, COL475, COL476, COL477, 
 COL478, COL479, COL480, COL481, COL482, 
 COL483, COL484, COL485, COL486, COL487, 
 COL488, COL489, COL490, COL491, COL492, 
 COL493, COL494, COL495, COL496, COL497, 
 COL498, COL499, COL500, COL501, COL502, 
 COL503, COL504, COL505, COL506, COL507, 
 COL508, COL509, COL510, COL511, COL512, 
 COL513, COL514, COL515, COL516, COL517, 
 COL518, COL519, COL520, COL521, COL522, 
 COL523, COL524, COL525, COL526, COL527, 
 COL528, COL529, COL530, COL531, COL532, 
 COL533, COL534, COL535, COL536, COL537, 
 COL538, COL539, COL540, COL541, COL542, 
 COL543, COL544, COL545, COL546, COL547, 
 COL548, COL549, COL550, COL551, COL552, 
 COL553, COL554, COL555, COL556, COL557, 
 COL558, COL559, COL560, COL561, COL562, 
 COL563, COL564, COL565, COL566, COL567, 
 COL568, COL569, COL570, COL571, COL572, 
 COL573, COL574, COL575, COL576, COL577, 
 COL578, COL579, COL580, COL581, COL582, 
 COL583, COL584, COL585, COL586, COL587, 
 COL588, COL589, COL590, COL591, COL592, 
 COL593, COL594, COL595, COL596, COL597, 
 COL598, COL599, COL600, COL601, COL602, 
 COL603, COL604, COL605, COL606, COL607, 
 COL608, COL609, COL610, COL611, COL612, 
 COL613, COL614, COL615, COL616, COL617, 
 COL618, COL619, COL620, COL621, COL622, 
 COL623, COL624, COL625, COL626, COL627, 
 COL628, COL629, COL630, COL631, COL632, 
 COL633, COL634, COL635, COL636, COL637, 
 COL638, COL639, COL640, COL641, COL642, 
 COL643, COL644, COL645, COL646, COL647, 
 COL648, COL649, COL650, COL651, COL652, 
 COL653, COL654, COL655, COL656, COL657, 
 COL658, COL659, COL660, COL661, COL662, 
 COL663, COL664, COL665, COL666, COL667, 
 COL668, COL669, COL670, COL671, COL672, 
 COL673, COL674, COL675, COL676, COL677, 
 COL678, COL679, COL680, COL681, COL682, 
 COL683, COL684, COL685, COL686, COL687, 
 COL688, COL689, COL690, COL691, COL692, 
 COL693, COL694, COL695, COL696, COL697, 
 COL698, COL699, COL700, COL701, COL702, 
 COL703, COL704, COL705, COL706, COL707, 
 COL708, COL709, COL710, COL711, COL712, 
 COL713, COL714, COL715, COL716, COL717, 
 COL718, COL719, COL720, COL721, COL722, 
 COL723, COL724, COL725, COL726, COL727, 
 COL728, COL729, COL730, COL731, COL732, 
 COL733, COL734, COL735, COL736, COL737, 
 COL738, COL739, COL740, COL741, COL742, 
 COL743, COL744, COL745, COL746, COL747, 
 COL748, COL749, COL750, COL751, COL752, 
 COL753, COL754, COL755, COL756, COL757, 
 COL758, COL759, COL760, COL761, COL762, 
 COL763, COL764, COL765, COL766, COL767, 
 COL768, COL769, COL770, COL771, COL772, 
 COL773, COL774, COL775, COL776, COL777, 
 COL778, COL779, COL780, COL781, COL782, 
 COL783, COL784, COL785, COL786, COL787, 
 COL788, COL789, COL790, COL791, COL792, 
 COL793, COL794, COL795, COL796, COL797, 
 COL798, COL799, COL800, COL801, COL802, 
 COL803, COL804, COL805, COL806, COL807, 
 COL808, COL809, COL810, COL811, COL812, 
 COL813, COL814, COL815, COL816, COL817, 
 COL818, COL819, COL820, COL821, COL822, 
 COL823, COL824, COL825, COL826, COL827, 
 COL828, COL829, COL830, COL831, COL832, 
 COL833, COL834, COL835, COL836, COL837, 
 COL838, COL839, COL840, COL841, COL842, 
 COL843, COL844, COL845, COL846, COL847, 
 COL848, COL849, COL850, COL851, COL852, 
 COL853, COL854, COL855, COL856, COL857, 
 COL858, COL859, COL860, COL861, COL862, 
 COL863, COL864, COL865, COL866, COL867, 
 COL868, COL869, COL870, COL871, COL872, 
 COL873, COL874, COL875, COL876, COL877, 
 COL878, COL879, COL880, COL881, COL882, 
 COL883, COL884, COL885, COL886, COL887, 
 COL888, COL889, COL890, COL891, COL892, 
 COL893, COL894, COL895, COL896, COL897, 
 COL898, COL899, COL900, COL901, COL902, 
 COL903, COL904, COL905, COL906, COL907, 
 COL908, COL909, COL910, COL911, COL912, 
 COL913, COL914, COL915, COL916, COL917, 
 COL918, COL919, COL920, COL921, COL922, 
 COL923, COL924, COL925, COL926, COL927, 
 COL928, COL929, COL930, COL931, COL932, 
 COL933, COL934, COL935, COL936, COL937, 
 COL938, COL939, COL940, COL941, COL942, 
 COL943, COL944, COL945, COL946, COL947, 
 COL948, COL949, COL950, CREATOR, LAST_MODIFIED_BY, 
 LAST_MODIFIED_DATE, CREATED_ON, FK_SPECIMEN, FK_ACCOUNT, LAST_MODIFIED_BY_FK, 
 CREATOR_FK)
AS 
SELECT   form_name,
            form_desc,
            form_version,
            fk_form,
            filldate,
            form_type,
            id AS fk_study,
            (SELECT   study_number
               FROM   eres.er_study
              WHERE   pk_study = l.id)
               study_number,
            COL1,
            COL2,
            COL3,
            COL4,
            COL5,
            COL6,
            COL7,
            COL8,
            COL9,
            COL10,
            COL11,
            COL12,
            COL13,
            COL14,
            COL15,
            COL16,
            COL17,
            COL18,
            COL19,
            COL20,
            COL21,
            COL22,
            COL23,
            COL24,
            COL25,
            COL26,
            COL27,
            COL28,
            COL29,
            COL30,
            COL31,
            COL32,
            COL33,
            COL34,
            COL35,
            COL36,
            COL37,
            COL38,
            COL39,
            COL40,
            COL41,
            COL42,
            COL43,
            COL44,
            COL45,
            COL46,
            COL47,
            COL48,
            COL49,
            COL50,
            COL51,
            COL52,
            COL53,
            COL54,
            COL55,
            COL56,
            COL57,
            COL58,
            COL59,
            COL60,
            COL61,
            COL62,
            COL63,
            COL64,
            COL65,
            COL66,
            COL67,
            COL68,
            COL69,
            COL70,
            COL71,
            COL72,
            COL73,
            COL74,
            COL75,
            COL76,
            COL77,
            COL78,
            COL79,
            COL80,
            COL81,
            COL82,
            COL83,
            COL84,
            COL85,
            COL86,
            COL87,
            COL88,
            COL89,
            COL90,
            COL91,
            COL92,
            COL93,
            COL94,
            COL95,
            COL96,
            COL97,
            COL98,
            COL99,
            COL100,
            COL101,
            COL102,
            COL103,
            COL104,
            COL105,
            COL106,
            COL107,
            COL108,
            COL109,
            COL110,
            COL111,
            COL112,
            COL113,
            COL114,
            COL115,
            COL116,
            COL117,
            COL118,
            COL119,
            COL120,
            COL121,
            COL122,
            COL123,
            COL124,
            COL125,
            COL126,
            COL127,
            COL128,
            COL129,
            COL130,
            COL131,
            COL132,
            COL133,
            COL134,
            COL135,
            COL136,
            COL137,
            COL138,
            COL139,
            COL140,
            COL141,
            COL142,
            COL143,
            COL144,
            COL145,
            COL146,
            COL147,
            COL148,
            COL149,
            COL150,
            COL151,
            COL152,
            COL153,
            COL154,
            COL155,
            COL156,
            COL157,
            COL158,
            COL159,
            COL160,
            COL161,
            COL162,
            COL163,
            COL164,
            COL165,
            COL166,
            COL167,
            COL168,
            COL169,
            COL170,
            COL171,
            COL172,
            COL173,
            COL174,
            COL175,
            COL176,
            COL177,
            COL178,
            COL179,
            COL180,
            COL181,
            COL182,
            COL183,
            COL184,
            COL185,
            COL186,
            COL187,
            COL188,
            COL189,
            COL190,
            COL191,
            COL192,
            COL193,
            COL194,
            COL195,
            COL196,
            COL197,
            COL198,
            COL199,
            COL200,
            COL201,
            COL202,
            COL203,
            COL204,
            COL205,
            COL206,
            COL207,
            COL208,
            COL209,
            COL210,
            COL211,
            COL212,
            COL213,
            COL214,
            COL215,
            COL216,
            COL217,
            COL218,
            COL219,
            COL220,
            COL221,
            COL222,
            COL223,
            COL224,
            COL225,
            COL226,
            COL227,
            COL228,
            COL229,
            COL230,
            COL231,
            COL232,
            COL233,
            COL234,
            COL235,
            COL236,
            COL237,
            COL238,
            COL239,
            COL240,
            COL241,
            COL242,
            COL243,
            COL244,
            COL245,
            COL246,
            COL247,
            COL248,
            COL249,
            COL250,
            COL251,
            COL252,
            COL253,
            COL254,
            COL255,
            COL256,
            COL257,
            COL258,
            COL259,
            COL260,
            COL261,
            COL262,
            COL263,
            COL264,
            COL265,
            COL266,
            COL267,
            COL268,
            COL269,
            COL270,
            COL271,
            COL272,
            COL273,
            COL274,
            COL275,
            COL276,
            COL277,
            COL278,
            COL279,
            COL280,
            COL281,
            COL282,
            COL283,
            COL284,
            COL285,
            COL286,
            COL287,
            COL288,
            COL289,
            COL290,
            COL291,
            COL292,
            COL293,
            COL294,
            COL295,
            COL296,
            COL297,
            COL298,
            COL299,
            COL300,
            COL301,
            COL302,
            COL303,
            COL304,
            COL305,
            COL306,
            COL307,
            COL308,
            COL309,
            COL310,
            COL311,
            COL312,
            COL313,
            COL314,
            COL315,
            COL316,
            COL317,
            COL318,
            COL319,
            COL320,
            COL321,
            COL322,
            COL323,
            COL324,
            COL325,
            COL326,
            COL327,
            COL328,
            COL329,
            COL330,
            COL331,
            COL332,
            COL333,
            COL334,
            COL335,
            COL336,
            COL337,
            COL338,
            COL339,
            COL340,
            COL341,
            COL342,
            COL343,
            COL344,
            COL345,
            COL346,
            COL347,
            COL348,
            COL349,
            COL350,
            COL351,
            COL352,
            COL353,
            COL354,
            COL355,
            COL356,
            COL357,
            COL358,
            COL359,
            COL360,
            COL361,
            COL362,
            COL363,
            COL364,
            COL365,
            COL366,
            COL367,
            COL368,
            COL369,
            COL370,
            COL371,
            COL372,
            COL373,
            COL374,
            COL375,
            COL376,
            COL377,
            COL378,
            COL379,
            COL380,
            COL381,
            COL382,
            COL383,
            COL384,
            COL385,
            COL386,
            COL387,
            COL388,
            COL389,
            COL390,
            COL391,
            COL392,
            COL393,
            COL394,
            COL395,
            COL396,
            COL397,
            COL398,
            COL399,
            COL400,
            COL401,
            COL402,
            COL403,
            COL404,
            COL405,
            COL406,
            COL407,
            COL408,
            COL409,
            COL410,
            COL411,
            COL412,
            COL413,
            COL414,
            COL415,
            COL416,
            COL417,
            COL418,
            COL419,
            COL420,
            COL421,
            COL422,
            COL423,
            COL424,
            COL425,
            COL426,
            COL427,
            COL428,
            COL429,
            COL430,
            COL431,
            COL432,
            COL433,
            COL434,
            COL435,
            COL436,
            COL437,
            COL438,
            COL439,
            COL440,
            COL441,
            COL442,
            COL443,
            COL444,
            COL445,
            COL446,
            COL447,
            COL448,
            COL449,
            COL450,
            COL451,
            COL452,
            COL453,
            COL454,
            COL455,
            COL456,
            COL457,
            COL458,
            COL459,
            COL460,
            COL461,
            COL462,
            COL463,
            COL464,
            COL465,
            COL466,
            COL467,
            COL468,
            COL469,
            COL470,
            COL471,
            COL472,
            COL473,
            COL474,
            COL475,
            COL476,
            COL477,
            COL478,
            COL479,
            COL480,
            COL481,
            COL482,
            COL483,
            COL484,
            COL485,
            COL486,
            COL487,
            COL488,
            COL489,
            COL490,
            COL491,
            COL492,
            COL493,
            COL494,
            COL495,
            COL496,
            COL497,
            COL498,
            COL499,
            COL500,
            COL501,
            COL502,
            COL503,
            COL504,
            COL505,
            COL506,
            COL507,
            COL508,
            COL509,
            COL510,
            COL511,
            COL512,
            COL513,
            COL514,
            COL515,
            COL516,
            COL517,
            COL518,
            COL519,
            COL520,
            COL521,
            COL522,
            COL523,
            COL524,
            COL525,
            COL526,
            COL527,
            COL528,
            COL529,
            COL530,
            COL531,
            COL532,
            COL533,
            COL534,
            COL535,
            COL536,
            COL537,
            COL538,
            COL539,
            COL540,
            COL541,
            COL542,
            COL543,
            COL544,
            COL545,
            COL546,
            COL547,
            COL548,
            COL549,
            COL550,
            COL551,
            COL552,
            COL553,
            COL554,
            COL555,
            COL556,
            COL557,
            COL558,
            COL559,
            COL560,
            COL561,
            COL562,
            COL563,
            COL564,
            COL565,
            COL566,
            COL567,
            COL568,
            COL569,
            COL570,
            COL571,
            COL572,
            COL573,
            COL574,
            COL575,
            COL576,
            COL577,
            COL578,
            COL579,
            COL580,
            COL581,
            COL582,
            COL583,
            COL584,
            COL585,
            COL586,
            COL587,
            COL588,
            COL589,
            COL590,
            COL591,
            COL592,
            COL593,
            COL594,
            COL595,
            COL596,
            COL597,
            COL598,
            COL599,
            COL600,
            COL601,
            COL602,
            COL603,
            COL604,
            COL605,
            COL606,
            COL607,
            COL608,
            COL609,
            COL610,
            COL611,
            COL612,
            COL613,
            COL614,
            COL615,
            COL616,
            COL617,
            COL618,
            COL619,
            COL620,
            COL621,
            COL622,
            COL623,
            COL624,
            COL625,
            COL626,
            COL627,
            COL628,
            COL629,
            COL630,
            COL631,
            COL632,
            COL633,
            COL634,
            COL635,
            COL636,
            COL637,
            COL638,
            COL639,
            COL640,
            COL641,
            COL642,
            COL643,
            COL644,
            COL645,
            COL646,
            COL647,
            COL648,
            COL649,
            COL650,
            COL651,
            COL652,
            COL653,
            COL654,
            COL655,
            COL656,
            COL657,
            COL658,
            COL659,
            COL660,
            COL661,
            COL662,
            COL663,
            COL664,
            COL665,
            COL666,
            COL667,
            COL668,
            COL669,
            COL670,
            COL671,
            COL672,
            COL673,
            COL674,
            COL675,
            COL676,
            COL677,
            COL678,
            COL679,
            COL680,
            COL681,
            COL682,
            COL683,
            COL684,
            COL685,
            COL686,
            COL687,
            COL688,
            COL689,
            COL690,
            COL691,
            COL692,
            COL693,
            COL694,
            COL695,
            COL696,
            COL697,
            COL698,
            COL699,
            COL700,
            COL701,
            COL702,
            COL703,
            COL704,
            COL705,
            COL706,
            COL707,
            COL708,
            COL709,
            COL710,
            COL711,
            COL712,
            COL713,
            COL714,
            COL715,
            COL716,
            COL717,
            COL718,
            COL719,
            COL720,
            COL721,
            COL722,
            COL723,
            COL724,
            COL725,
            COL726,
            COL727,
            COL728,
            COL729,
            COL730,
            COL731,
            COL732,
            COL733,
            COL734,
            COL735,
            COL736,
            COL737,
            COL738,
            COL739,
            COL740,
            COL741,
            COL742,
            COL743,
            COL744,
            COL745,
            COL746,
            COL747,
            COL748,
            COL749,
            COL750,
            COL751,
            COL752,
            COL753,
            COL754,
            COL755,
            COL756,
            COL757,
            COL758,
            COL759,
            COL760,
            COL761,
            COL762,
            COL763,
            COL764,
            COL765,
            COL766,
            COL767,
            COL768,
            COL769,
            COL770,
            COL771,
            COL772,
            COL773,
            COL774,
            COL775,
            COL776,
            COL777,
            COL778,
            COL779,
            COL780,
            COL781,
            COL782,
            COL783,
            COL784,
            COL785,
            COL786,
            COL787,
            COL788,
            COL789,
            COL790,
            COL791,
            COL792,
            COL793,
            COL794,
            COL795,
            COL796,
            COL797,
            COL798,
            COL799,
            COL800,
            COL801,
            COL802,
            COL803,
            COL804,
            COL805,
            COL806,
            COL807,
            COL808,
            COL809,
            COL810,
            COL811,
            COL812,
            COL813,
            COL814,
            COL815,
            COL816,
            COL817,
            COL818,
            COL819,
            COL820,
            COL821,
            COL822,
            COL823,
            COL824,
            COL825,
            COL826,
            COL827,
            COL828,
            COL829,
            COL830,
            COL831,
            COL832,
            COL833,
            COL834,
            COL835,
            COL836,
            COL837,
            COL838,
            COL839,
            COL840,
            COL841,
            COL842,
            COL843,
            COL844,
            COL845,
            COL846,
            COL847,
            COL848,
            COL849,
            COL850,
            COL851,
            COL852,
            COL853,
            COL854,
            COL855,
            COL856,
            COL857,
            COL858,
            COL859,
            COL860,
            COL861,
            COL862,
            COL863,
            COL864,
            COL865,
            COL866,
            COL867,
            COL868,
            COL869,
            COL870,
            COL871,
            COL872,
            COL873,
            COL874,
            COL875,
            COL876,
            COL877,
            COL878,
            COL879,
            COL880,
            COL881,
            COL882,
            COL883,
            COL884,
            COL885,
            COL886,
            COL887,
            COL888,
            COL889,
            COL890,
            COL891,
            COL892,
            COL893,
            COL894,
            COL895,
            COL896,
            COL897,
            COL898,
            COL899,
            COL900,
            COL901,
            COL902,
            COL903,
            COL904,
            COL905,
            COL906,
            COL907,
            COL908,
            COL909,
            COL910,
            COL911,
            COL912,
            COL913,
            COL914,
            COL915,
            COL916,
            COL917,
            COL918,
            COL919,
            COL920,
            COL921,
            COL922,
            COL923,
            COL924,
            COL925,
            COL926,
            COL927,
            COL928,
            COL929,
            COL930,
            COL931,
            COL932,
            COL933,
            COL934,
            COL935,
            COL936,
            COL937,
            COL938,
            COL939,
            COL940,
            COL941,
            COL942,
            COL943,
            COL944,
            COL945,
            COL946,
            COL947,
            COL948,
            COL949,
            COL950,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = l.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = l.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            l.LAST_MODIFIED_DATE,
            l.CREATED_ON,
            FK_SPECIMEN,
            er_formlib.FK_ACCOUNT,
            l.LAST_MODIFIED_BY as LAST_MODIFIED_BY_FK, l.CREATOR as CREATOR_FK 
     FROM   eres.er_formslinear l, eres.er_formlib
    WHERE   form_type = 'S' AND fk_form = pk_formlib
/

COMMENT ON TABLE VDA_V_STUDYFORMRESPONSES IS 'This view provides access to responses to 
all study level forms'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL894 IS 'Col894 data of the form response.The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this  column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL895 IS 'Col895 data of the form response.The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this  column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL896 IS 'Col896 data of the form response.The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this  column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL897 IS 'Col897 data of the form response.The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this  column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL898 IS 'Col898 data of the form response.The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this  column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL899 IS 'Col899 data of the form response.The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this  column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL900 IS 'Col900 data of the form response.The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this  column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL901 IS 'Col901 data of the form response.The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this  column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL902 IS 'Col902 data of the form response.The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this  column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL903 IS 'Col903 data of the form response.The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this  column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL904 IS 'Col904 data of the form response.The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this  column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL905 IS 'Col905 data of the form response.The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this  column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL906 IS 'Col906 data of the form response.The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this  column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL907 IS 'Col907 data of the form response.The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this  column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL908 IS 'Col908 data of the form response.The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this  column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL909 IS 'Col909 data of the form response.The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this  column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL910 IS 'Col910 data of the form response.The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this  column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL911 IS 'Col911 data of the form response.The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this  column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL912 IS 'Col912 data of the form response.The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this  column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL913 IS 'Col913 data of the form response.The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this  column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL914 IS 'Col914 data of the form response.The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this  column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL915 IS 'Col915 data of the form response.The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this  column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL916 IS 'Col916 data of the form response.The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this  column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL917 IS 'Col917 data of the form response.The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this  column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL918 IS 'Col918 data of the form response.The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this  column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL919 IS 'Col919 data of the form response.The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this  column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL920 IS 'Col920 data of the form response.The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this  column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL921 IS 'Col921 data of the form response.The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this  column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL922 IS 'Col922 data of the form response.The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this  column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL923 IS 'Col923 data of the form response.The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this  column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL924 IS 'Col924 data of the form response.The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this  column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL925 IS 'Col925 data of the form response.The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this  column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL926 IS 'Col926 data of the form response.The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this  column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL927 IS 'Col927 data of the form response.The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this  column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL928 IS 'Col928 data of the form response.The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this  column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL929 IS 'Col929 data of the form response.The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this  column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL930 IS 'Col930 data of the form response.The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this  column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL931 IS 'Col931 data of the form response.The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this  column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL932 IS 'Col932 data of the form response.The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this  column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL933 IS 'Col933 data of the form response.The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this  column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL934 IS 'Col934 data of the form response.The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this  column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL935 IS 'Col935 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL936 IS 'Col936 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL937 IS 'Col937 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL938 IS 'Col938 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL939 IS 'Col939 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL940 IS 'Col940 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL941 IS 'Col941 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL942 IS 'Col942 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL943 IS 'Col943 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL944 IS 'Col944 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL945 IS 'Col945 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL946 IS 'Col946 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL947 IS 'Col947 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL948 IS 'Col948 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL949 IS 'Col949 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL950 IS 'Col950 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.CREATOR IS 'User who created record'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.LAST_MODIFIED_BY IS 'User who last modified the 
record'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.LAST_MODIFIED_DATE IS 'Last modified date'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.CREATED_ON IS 'Date record was created on'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.FK_SPECIMEN IS 'ID to identify the specimen a 
response is linked with'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.FK_ACCOUNT IS 'The account the response is 
linked with. Applicable to Velos hosted or shared environments'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.LAST_MODIFIED_BY_FK IS 'The Key to identify the 
Last Modified By of the record.'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.CREATOR_FK IS 'The Key to identify the Creator 
of the record.'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.FORM_NAME IS 'Form name'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.FORM_DESC IS 'Form Description'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.FORM_VERSION IS 'Form version number'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.FK_FORM IS 'ID to identify a form record 
uniquely'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.FILLDATE IS 'Date the response was filled on'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.FORM_TYPE IS 'Type of the  form- always A for 
account level forms'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.FK_STUDY IS 'The id of the study the response 
is linked with'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.STUDY_NUMBER IS 'The study number of the study 
the response is linked with'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL1 IS 'Col1 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL2 IS 'Col2 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL3 IS 'Col3 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL4 IS 'Col4 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL5 IS 'Col5 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL6 IS 'Col6 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL7 IS 'Col7 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL8 IS 'Col8 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL9 IS 'Col9 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL10 IS 'Col10 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL11 IS 'Col11 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL12 IS 'Col12 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL13 IS 'Col13 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL14 IS 'Col14 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL15 IS 'Col15 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL16 IS 'Col16 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL17 IS 'Col17 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL18 IS 'Col18 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL19 IS 'Col19 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL20 IS 'Col20 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL21 IS 'Col21 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL22 IS 'Col22 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL23 IS 'Col23 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL24 IS 'Col24 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL25 IS 'Col25 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL26 IS 'Col26 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL27 IS 'Col27 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL28 IS 'Col28 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL29 IS 'Col29 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL30 IS 'Col30 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL31 IS 'Col31 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL32 IS 'Col32 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL33 IS 'Col33 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL34 IS 'Col34 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL35 IS 'Col35 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL36 IS 'Col36 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL37 IS 'Col37 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL38 IS 'Col38 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL39 IS 'Col39 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL40 IS 'Col40 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL41 IS 'Col41 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL42 IS 'Col42 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL43 IS 'Col43 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL44 IS 'Col44 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL45 IS 'Col45 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL46 IS 'Col46 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL47 IS 'Col47 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL48 IS 'Col48 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL49 IS 'Col49 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL50 IS 'Col50 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL51 IS 'Col51 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL52 IS 'Col52 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL53 IS 'Col53 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL54 IS 'Col54 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL55 IS 'Col55 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL56 IS 'Col56 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL57 IS 'Col57 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL58 IS 'Col58 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL59 IS 'Col59 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL60 IS 'Col60 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL61 IS 'Col61 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL62 IS 'Col62 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL63 IS 'Col63 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL64 IS 'Col64 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL65 IS 'Col65 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL66 IS 'Col66 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL67 IS 'Col67 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL68 IS 'Col68 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL69 IS 'Col69 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL70 IS 'Col70 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL71 IS 'Col71 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL72 IS 'Col72 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL73 IS 'Col73 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL74 IS 'Col74 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL75 IS 'Col75 data of the form response. The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL76 IS 'Col76 data of the form response. The 
view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL77 IS 'Col77 data of the form response. The 
view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL78 IS 'Col78 data of the form response. The 
view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL79 IS 'Col79 data of the form response. The 
view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL80 IS 'Col80 data of the form response. The 
view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL81 IS 'Col81 data of the form response. The 
view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL82 IS 'Col82 data of the form response. The 
view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL83 IS 'Col83 data of the form response. The 
view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL84 IS 'Col84 data of the form response. The 
view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL85 IS 'Col85 data of the form response. The 
view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL86 IS 'Col86 data of the form response. The 
view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL87 IS 'Col87 data of the form response. The 
view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL88 IS 'Col88 data of the form response. The 
view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL89 IS 'Col89 data of the form response. The 
view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL90 IS 'Col90 data of the form response. The 
view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL91 IS 'Col91 data of the form response. The 
view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL92 IS 'Col92 data of the form response. The 
view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL93 IS 'Col93 data of the form response. The 
view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL94 IS 'Col94 data of the form response. The 
view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL95 IS 'Col95 data of the form response. The 
view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL96 IS 'Col96 data of the form response. The 
view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL97 IS 'Col97 data of the form response. The 
view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL98 IS 'Col98 data of the form response. The 
view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL99 IS 'Col99 data of the form response. The 
view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL100 IS 'Col100 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL101 IS 'Col101 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL102 IS 'Col102 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL103 IS 'Col103 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL104 IS 'Col104 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL105 IS 'Col105 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL106 IS 'Col106 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL107 IS 'Col107 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL108 IS 'Col108 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL109 IS 'Col109 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL110 IS 'Col110 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL111 IS 'Col111 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL112 IS 'Col112 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL113 IS 'Col113 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL114 IS 'Col114 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL115 IS 'Col115 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL116 IS 'Col116 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL117 IS 'Col117 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL118 IS 'Col118 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL119 IS 'Col119 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL120 IS 'Col120 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL121 IS 'Col121 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL122 IS 'Col122 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL123 IS 'Col123 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL124 IS 'Col124 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL125 IS 'Col125 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL126 IS 'Col126 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL127 IS 'Col127 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL128 IS 'Col128 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL129 IS 'Col129 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL130 IS 'Col130 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL131 IS 'Col131 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL132 IS 'Col132 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL133 IS 'Col133 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL134 IS 'Col134 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL135 IS 'Col135 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL136 IS 'Col136 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL137 IS 'Col137 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL138 IS 'Col138 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL139 IS 'Col139 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL140 IS 'Col140 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL141 IS 'Col141 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL142 IS 'Col142 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL143 IS 'Col143 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL144 IS 'Col144 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL145 IS 'Col145 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL146 IS 'Col146 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL147 IS 'Col147 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL148 IS 'Col148 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL149 IS 'Col149 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL150 IS 'Col150 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL151 IS 'Col151 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL152 IS 'Col152 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL153 IS 'Col153 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL154 IS 'Col154 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL155 IS 'Col155 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL156 IS 'Col156 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL157 IS 'Col157 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL158 IS 'Col158 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL159 IS 'Col159 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL160 IS 'Col160 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL161 IS 'Col161 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL162 IS 'Col162 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL163 IS 'Col163 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL164 IS 'Col164 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL165 IS 'Col165 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL166 IS 'Col166 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL167 IS 'Col167 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL168 IS 'Col168 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL169 IS 'Col169 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL170 IS 'Col170 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL171 IS 'Col171 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL172 IS 'Col172 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL173 IS 'Col173 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL174 IS 'Col174 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL175 IS 'Col175 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL176 IS 'Col176 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL177 IS 'Col177 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL178 IS 'Col178 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL179 IS 'Col179 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL180 IS 'Col180 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL181 IS 'Col181 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL182 IS 'Col182 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL183 IS 'Col183 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL184 IS 'Col184 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL185 IS 'Col185 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL186 IS 'Col186 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL187 IS 'Col187 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL188 IS 'Col188 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL189 IS 'Col189 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL190 IS 'Col190 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL191 IS 'Col191 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL192 IS 'Col192 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL193 IS 'Col193 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL194 IS 'Col194 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL195 IS 'Col195 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL196 IS 'Col196 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL197 IS 'Col197 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL198 IS 'Col198 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL199 IS 'Col199 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL200 IS 'Col200 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL201 IS 'Col201 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL202 IS 'Col202 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL203 IS 'Col203 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL204 IS 'Col204 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL205 IS 'Col205 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL206 IS 'Col206 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL207 IS 'Col207 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL208 IS 'Col208 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL209 IS 'Col209 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL210 IS 'Col210 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL211 IS 'Col211 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL212 IS 'Col212 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL213 IS 'Col213 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL214 IS 'Col214 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL215 IS 'Col215 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL216 IS 'Col216 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL217 IS 'Col217 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL218 IS 'Col218 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL219 IS 'Col219 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL220 IS 'Col220 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL221 IS 'Col221 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL222 IS 'Col222 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL223 IS 'Col223 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL224 IS 'Col224 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL225 IS 'Col225 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL226 IS 'Col226 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL227 IS 'Col227 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL228 IS 'Col228 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL229 IS 'Col229 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL230 IS 'Col230 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL231 IS 'Col231 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL232 IS 'Col232 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL233 IS 'Col233 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL234 IS 'Col234 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL235 IS 'Col235 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL236 IS 'Col236 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL237 IS 'Col237 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL238 IS 'Col238 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL239 IS 'Col239 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL240 IS 'Col240 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL241 IS 'Col241 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL242 IS 'Col242 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL243 IS 'Col243 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL244 IS 'Col244 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL245 IS 'Col245 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL246 IS 'Col246 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL247 IS 'Col247 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL248 IS 'Col248 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL249 IS 'Col249 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL250 IS 'Col250 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL251 IS 'Col251 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL252 IS 'Col252 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL253 IS 'Col253 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL254 IS 'Col254 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL255 IS 'Col255 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL256 IS 'Col256 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL257 IS 'Col257 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL258 IS 'Col258 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL259 IS 'Col259 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL260 IS 'Col260 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL261 IS 'Col261 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL262 IS 'Col262 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL263 IS 'Col263 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL264 IS 'Col264 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL265 IS 'Col265 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL266 IS 'Col266 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL267 IS 'Col267 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL268 IS 'Col268 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL269 IS 'Col269 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL270 IS 'Col270 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL271 IS 'Col271 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL272 IS 'Col272 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL273 IS 'Col273 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL274 IS 'Col274 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL275 IS 'Col275 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL276 IS 'Col276 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL277 IS 'Col277 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL278 IS 'Col278 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL279 IS 'Col279 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL280 IS 'Col280 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL281 IS 'Col281 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL282 IS 'Col282 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL283 IS 'Col283 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL284 IS 'Col284 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL285 IS 'Col285 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL286 IS 'Col286 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL287 IS 'Col287 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL288 IS 'Col288 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL289 IS 'Col289 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL290 IS 'Col290 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL291 IS 'Col291 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL292 IS 'Col292 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL293 IS 'Col293 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL294 IS 'Col294 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL295 IS 'Col295 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL296 IS 'Col296 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL297 IS 'Col297 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL298 IS 'Col298 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL299 IS 'Col299 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL300 IS 'Col300 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL301 IS 'Col301 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL302 IS 'Col302 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL303 IS 'Col303 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL304 IS 'Col304 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL305 IS 'Col305 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL306 IS 'Col306 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL307 IS 'Col307 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL308 IS 'Col308 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL309 IS 'Col309 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL310 IS 'Col310 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL311 IS 'Col311 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL312 IS 'Col312 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL313 IS 'Col313 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL314 IS 'Col314 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL315 IS 'Col315 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL316 IS 'Col316 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL317 IS 'Col317 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL318 IS 'Col318 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL319 IS 'Col319 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL320 IS 'Col320 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL321 IS 'Col321 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL322 IS 'Col322 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL323 IS 'Col323 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL324 IS 'Col324 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL325 IS 'Col325 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL326 IS 'Col326 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL327 IS 'Col327 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL328 IS 'Col328 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL329 IS 'Col329 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL330 IS 'Col330 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL331 IS 'Col331 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL332 IS 'Col332 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL333 IS 'Col333 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL334 IS 'Col334 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL335 IS 'Col335 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL336 IS 'Col336 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL337 IS 'Col337 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL338 IS 'Col338 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL339 IS 'Col339 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL340 IS 'Col340 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL341 IS 'Col341 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL342 IS 'Col342 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL343 IS 'Col343 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL344 IS 'Col344 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL345 IS 'Col345 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL346 IS 'Col346 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL347 IS 'Col347 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL348 IS 'Col348 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL349 IS 'Col349 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL350 IS 'Col350 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL351 IS 'Col351 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL352 IS 'Col352 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL353 IS 'Col353 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL354 IS 'Col354 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL355 IS 'Col355 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL356 IS 'Col356 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL357 IS 'Col357 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL358 IS 'Col358 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL359 IS 'Col359 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL360 IS 'Col360 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL361 IS 'Col361 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL362 IS 'Col362 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL363 IS 'Col363 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL364 IS 'Col364 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL365 IS 'Col365 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL366 IS 'Col366 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL367 IS 'Col367 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL368 IS 'Col368 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL369 IS 'Col369 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL370 IS 'Col370 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL371 IS 'Col371 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL372 IS 'Col372 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL373 IS 'Col373 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL374 IS 'Col374 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL375 IS 'Col375 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL376 IS 'Col376 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL377 IS 'Col377 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL378 IS 'Col378 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL379 IS 'Col379 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL380 IS 'Col380 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL381 IS 'Col381 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL382 IS 'Col382 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL383 IS 'Col383 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL384 IS 'Col384 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL385 IS 'Col385 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL386 IS 'Col386 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL387 IS 'Col387 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL388 IS 'Col388 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL389 IS 'Col389 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL390 IS 'Col390 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL391 IS 'Col391 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL392 IS 'Col392 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL393 IS 'Col393 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL394 IS 'Col394 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL395 IS 'Col395 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL396 IS 'Col396 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL397 IS 'Col397 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL398 IS 'Col398 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL399 IS 'Col399 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL400 IS 'Col400 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL401 IS 'Col401 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL402 IS 'Col402 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL403 IS 'Col403 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL404 IS 'Col404 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL405 IS 'Col405 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL406 IS 'Col406 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL407 IS 'Col407 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL408 IS 'Col408 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL409 IS 'Col409 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL410 IS 'Col410 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL411 IS 'Col411 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL412 IS 'Col412 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL413 IS 'Col413 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL414 IS 'Col414 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL415 IS 'Col415 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL416 IS 'Col416 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL417 IS 'Col417 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL418 IS 'Col418 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL419 IS 'Col419 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL420 IS 'Col420 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL421 IS 'Col421 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL422 IS 'Col422 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL423 IS 'Col423 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL424 IS 'Col424 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL425 IS 'Col425 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL426 IS 'Col426 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL427 IS 'Col427 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL428 IS 'Col428 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL429 IS 'Col429 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL430 IS 'Col430 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL431 IS 'Col431 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL432 IS 'Col432 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL433 IS 'Col433 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL434 IS 'Col434 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL435 IS 'Col435 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL436 IS 'Col436 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL437 IS 'Col437 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL438 IS 'Col438 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL439 IS 'Col439 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL440 IS 'Col440 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL441 IS 'Col441 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL442 IS 'Col442 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL443 IS 'Col443 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL444 IS 'Col444 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL445 IS 'Col445 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL446 IS 'Col446 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL447 IS 'Col447 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL448 IS 'Col448 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL449 IS 'Col449 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL450 IS 'Col450 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL451 IS 'Col451 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL452 IS 'Col452 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL453 IS 'Col453 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL454 IS 'Col454 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL455 IS 'Col455 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL456 IS 'Col456 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL457 IS 'Col457 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL458 IS 'Col458 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL459 IS 'Col459 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL460 IS 'Col460 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL461 IS 'Col461 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL462 IS 'Col462 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL463 IS 'Col463 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL464 IS 'Col464 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL465 IS 'Col465 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL466 IS 'Col466 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL467 IS 'Col467 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL468 IS 'Col468 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL469 IS 'Col469 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL470 IS 'Col470 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL471 IS 'Col471 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL472 IS 'Col472 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL473 IS 'Col473 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL474 IS 'Col474 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL475 IS 'Col475 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL476 IS 'Col476 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL477 IS 'Col477 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL478 IS 'Col478 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL479 IS 'Col479 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL480 IS 'Col480 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL481 IS 'Col481 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL482 IS 'Col482 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL483 IS 'Col483 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL484 IS 'Col484 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL485 IS 'Col485 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL486 IS 'Col486 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL487 IS 'Col487 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL488 IS 'Col488 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL489 IS 'Col489 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL490 IS 'Col490 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL491 IS 'Col491 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL492 IS 'Col492 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL493 IS 'Col493 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL494 IS 'Col494 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL495 IS 'Col495 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL496 IS 'Col496 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL497 IS 'Col497 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL498 IS 'Col498 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL499 IS 'Col499 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL500 IS 'Col500 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL501 IS 'Col501 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL502 IS 'Col502 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL503 IS 'Col503 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL504 IS 'Col504 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL505 IS 'Col505 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL506 IS 'Col506 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL507 IS 'Col507 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL508 IS 'Col508 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL509 IS 'Col509 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL510 IS 'Col510 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL511 IS 'Col511 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL512 IS 'Col512 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL513 IS 'Col513 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL514 IS 'Col514 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL515 IS 'Col515 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL516 IS 'Col516 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL517 IS 'Col517 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL518 IS 'Col518 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL519 IS 'Col519 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL520 IS 'Col520 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL521 IS 'Col521 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL522 IS 'Col522 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL523 IS 'Col523 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL524 IS 'Col524 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL525 IS 'Col525 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL526 IS 'Col526 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL527 IS 'Col527 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL528 IS 'Col528 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL529 IS 'Col529 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL530 IS 'Col530 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL531 IS 'Col531 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL532 IS 'Col532 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL533 IS 'Col533 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL534 IS 'Col534 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL535 IS 'Col535 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL536 IS 'Col536 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL537 IS 'Col537 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL538 IS 'Col538 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL539 IS 'Col539 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL540 IS 'Col540 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL541 IS 'Col541 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL542 IS 'Col542 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL543 IS 'Col543 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL544 IS 'Col544 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL545 IS 'Col545 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL546 IS 'Col546 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL547 IS 'Col547 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL548 IS 'Col548 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL549 IS 'Col549 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL550 IS 'Col550 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL551 IS 'Col551 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL552 IS 'Col552 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL553 IS 'Col553 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL554 IS 'Col554 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL555 IS 'Col555 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL556 IS 'Col556 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL557 IS 'Col557 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL558 IS 'Col558 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL559 IS 'Col559 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL560 IS 'Col560 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL561 IS 'Col561 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL562 IS 'Col562 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL563 IS 'Col563 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL564 IS 'Col564 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL565 IS 'Col565 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL566 IS 'Col566 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL567 IS 'Col567 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL568 IS 'Col568 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL569 IS 'Col569 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL570 IS 'Col570 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL571 IS 'Col571 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL572 IS 'Col572 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL573 IS 'Col573 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL574 IS 'Col574 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL575 IS 'Col575 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL576 IS 'Col576 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL577 IS 'Col577 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL578 IS 'Col578 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL579 IS 'Col579 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL580 IS 'Col580 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL581 IS 'Col581 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL582 IS 'Col582 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL583 IS 'Col583 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL584 IS 'Col584 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL585 IS 'Col585 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL586 IS 'Col586 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL587 IS 'Col587 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL588 IS 'Col588 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL589 IS 'Col589 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL590 IS 'Col590 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL591 IS 'Col591 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL592 IS 'Col592 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL593 IS 'Col593 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL594 IS 'Col594 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL595 IS 'Col595 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL596 IS 'Col596 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL597 IS 'Col597 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL598 IS 'Col598 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL599 IS 'Col599 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL600 IS 'Col600 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL601 IS 'Col601 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL602 IS 'Col602 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL603 IS 'Col603 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL604 IS 'Col604 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL605 IS 'Col605 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL606 IS 'Col606 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL607 IS 'Col607 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL608 IS 'Col608 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL609 IS 'Col609 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL610 IS 'Col610 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL611 IS 'Col611 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL612 IS 'Col612 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL613 IS 'Col613 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL614 IS 'Col614 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL615 IS 'Col615 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL616 IS 'Col616 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL617 IS 'Col617 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL618 IS 'Col618 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL619 IS 'Col619 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL620 IS 'Col620 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL621 IS 'Col621 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL622 IS 'Col622 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL623 IS 'Col623 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL624 IS 'Col624 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL625 IS 'Col625 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL626 IS 'Col626 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL627 IS 'Col627 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL628 IS 'Col628 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL629 IS 'Col629 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL630 IS 'Col630 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL631 IS 'Col631 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL632 IS 'Col632 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL633 IS 'Col633 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL634 IS 'Col634 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL635 IS 'Col635 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL636 IS 'Col636 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL637 IS 'Col637 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL638 IS 'Col638 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL639 IS 'Col639 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL640 IS 'Col640 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL641 IS 'Col641 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL642 IS 'Col642 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL643 IS 'Col643 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL644 IS 'Col644 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL645 IS 'Col645 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL646 IS 'Col646 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL647 IS 'Col647 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL648 IS 'Col648 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL649 IS 'Col649 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL650 IS 'Col650 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL651 IS 'Col651 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL652 IS 'Col652 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL653 IS 'Col653 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL654 IS 'Col654 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL655 IS 'Col655 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL656 IS 'Col656 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL657 IS 'Col657 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL658 IS 'Col658 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL659 IS 'Col659 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL660 IS 'Col660 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL661 IS 'Col661 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL662 IS 'Col662 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL663 IS 'Col663 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL664 IS 'Col664 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL665 IS 'Col665 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL666 IS 'Col666 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL667 IS 'Col667 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL668 IS 'Col668 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL669 IS 'Col669 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL670 IS 'Col670 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL671 IS 'Col671 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL672 IS 'Col672 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL673 IS 'Col673 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL674 IS 'Col674 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL675 IS 'Col675 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL676 IS 'Col676 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL677 IS 'Col677 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL678 IS 'Col678 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL679 IS 'Col679 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL680 IS 'Col680 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL681 IS 'Col681 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL682 IS 'Col682 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL683 IS 'Col683 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL684 IS 'Col684 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL685 IS 'Col685 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL686 IS 'Col686 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL687 IS 'Col687 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL688 IS 'Col688 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL689 IS 'Col689 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL690 IS 'Col690 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL691 IS 'Col691 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL692 IS 'Col692 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL693 IS 'Col693 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL694 IS 'Col694 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL695 IS 'Col695 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL696 IS 'Col696 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL697 IS 'Col697 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL698 IS 'Col698 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL699 IS 'Col699 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL700 IS 'Col700 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL701 IS 'Col701 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL702 IS 'Col702 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL703 IS 'Col703 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL704 IS 'Col704 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL705 IS 'Col705 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL706 IS 'Col706 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL707 IS 'Col707 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL708 IS 'Col708 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL709 IS 'Col709 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL710 IS 'Col710 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL711 IS 'Col711 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL712 IS 'Col712 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL713 IS 'Col713 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL714 IS 'Col714 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL715 IS 'Col715 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL716 IS 'Col716 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL717 IS 'Col717 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL718 IS 'Col718 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL719 IS 'Col719 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL720 IS 'Col720 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL721 IS 'Col721 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL722 IS 'Col722 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL723 IS 'Col723 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL724 IS 'Col724 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL725 IS 'Col725 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL726 IS 'Col726 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL727 IS 'Col727 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL728 IS 'Col728 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL729 IS 'Col729 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL730 IS 'Col730 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL731 IS 'Col731 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL732 IS 'Col732 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL733 IS 'Col733 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL734 IS 'Col734 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL735 IS 'Col735 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL736 IS 'Col736 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL737 IS 'Col737 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL738 IS 'Col738 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL739 IS 'Col739 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL740 IS 'Col740 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL741 IS 'Col741 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL742 IS 'Col742 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL743 IS 'Col743 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL744 IS 'Col744 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL745 IS 'Col745 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL746 IS 'Col746 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL747 IS 'Col747 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL748 IS 'Col748 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL749 IS 'Col749 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL750 IS 'Col750 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL751 IS 'Col751 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL752 IS 'Col752 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL753 IS 'Col753 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL754 IS 'Col754 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL755 IS 'Col755 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL756 IS 'Col756 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL757 IS 'Col757 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL758 IS 'Col758 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL759 IS 'Col759 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL760 IS 'Col760 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL761 IS 'Col761 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL762 IS 'Col762 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL763 IS 'Col763 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL764 IS 'Col764 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL765 IS 'Col765 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL766 IS 'Col766 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL767 IS 'Col767 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL768 IS 'Col768 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL769 IS 'Col769 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL770 IS 'Col770 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL771 IS 'Col771 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL772 IS 'Col772 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL773 IS 'Col773 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL774 IS 'Col774 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL775 IS 'Col775 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL776 IS 'Col776 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL777 IS 'Col777 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL778 IS 'Col778 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL779 IS 'Col779 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL780 IS 'Col780 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL781 IS 'Col781 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL782 IS 'Col782 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL783 IS 'Col783 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL784 IS 'Col784 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL785 IS 'Col785 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL786 IS 'Col786 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL787 IS 'Col787 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL788 IS 'Col788 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL789 IS 'Col789 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL790 IS 'Col790 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL791 IS 'Col791 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL792 IS 'Col792 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL793 IS 'Col793 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL794 IS 'Col794 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL795 IS 'Col795 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL796 IS 'Col796 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL797 IS 'Col797 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL798 IS 'Col798 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL799 IS 'Col799 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL800 IS 'Col800 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL801 IS 'Col801 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL802 IS 'Col802 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL803 IS 'Col803 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL804 IS 'Col804 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL805 IS 'Col805 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL806 IS 'Col806 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL807 IS 'Col807 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL808 IS 'Col808 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL809 IS 'Col809 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL810 IS 'Col810 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL811 IS 'Col811 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL812 IS 'Col812 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL813 IS 'Col813 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL814 IS 'Col814 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL815 IS 'Col815 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL816 IS 'Col816 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL817 IS 'Col817 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL818 IS 'Col818 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL819 IS 'Col819 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL820 IS 'Col820 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL821 IS 'Col821 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL822 IS 'Col822 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL823 IS 'Col823 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL824 IS 'Col824 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL825 IS 'Col825 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL826 IS 'Col826 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL827 IS 'Col827 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL828 IS 'Col828 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL829 IS 'Col829 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL830 IS 'Col830 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL831 IS 'Col831 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL832 IS 'Col832 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL833 IS 'Col833 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL834 IS 'Col834 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL835 IS 'Col835 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL836 IS 'Col836 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL837 IS 'Col837 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL838 IS 'Col838 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL839 IS 'Col839 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL840 IS 'Col840 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL841 IS 'Col841 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL842 IS 'Col842 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL843 IS 'Col843 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL844 IS 'Col844 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL845 IS 'Col845 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL846 IS 'Col846 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL847 IS 'Col847 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL848 IS 'Col848 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL849 IS 'Col849 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL850 IS 'Col850 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL851 IS 'Col851 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL852 IS 'Col852 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL853 IS 'Col853 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL854 IS 'Col854 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL855 IS 'Col855 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL856 IS 'Col856 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL857 IS 'Col857 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL858 IS 'Col858 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL859 IS 'Col859 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL860 IS 'Col860 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL861 IS 'Col861 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL862 IS 'Col862 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL863 IS 'Col863 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL864 IS 'Col864 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL865 IS 'Col865 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL866 IS 'Col866 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL867 IS 'Col867 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL868 IS 'Col868 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL869 IS 'Col869 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL870 IS 'Col870 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL871 IS 'Col871 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL872 IS 'Col872 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL873 IS 'Col873 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL874 IS 'Col874 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL875 IS 'Col875 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL876 IS 'Col876 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL877 IS 'Col877 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL878 IS 'Col878 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL879 IS 'Col879 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL880 IS 'Col880 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL881 IS 'Col881 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL882 IS 'Col882 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL883 IS 'Col883 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL884 IS 'Col884 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL885 IS 'Col885 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL886 IS 'Col886 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL887 IS 'Col887 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL888 IS 'Col888 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL889 IS 'Col889 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL890 IS 'Col890 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL891 IS 'Col891 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL892 IS 'Col892 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_STUDYFORMRESPONSES.COL893 IS 'Col893 data of the form response.
The view VDA.VDA_V_MAPFORM_STUDY provides information of the field related with this 
 column for the respective form'
/


--
-- VDA_V_STUDYCAL_SUBJCOST  (View) 
--
--  Dependencies: 
--   SCH_SUBCOST_ITEM_VISIT (Table)
--   SCH_SUBCOST_ITEM (Table)
--   SCH_PROTOCOL_VISIT (Table)
--   EVENT_ASSOC (Table)
--   SCH_CODELST (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_STUDYCAL_SUBJCOST
(PK_SUBCOST_ITEM, FK_CALENDAR, NAME, SUBCOST_ITEM_NAME, SUBCOST_ITEM_COST, 
 SUBCOST_ITEM_UNIT, SUBCOST_ITEM_SEQ, SUBCOST_CATEGORY, SUBCOST_COST_TYPE, VISIT_NAME, 
 FK_STUDY, FK_ACCOUNT)
AS 
SELECT   PK_SUBCOST_ITEM,
            FK_CALENDAR,
            e.name,
            SUBCOST_ITEM_NAME,
            SUBCOST_ITEM_COST,
            SUBCOST_ITEM_UNIT,
            SUBCOST_ITEM_SEQ,
            (SELECT   codelst_desc
               FROM   esch.sch_codelst
              WHERE   pk_codelst = FK_CODELST_CATEGORY)
               SUBCOST_CATEGORY,
            (SELECT   codelst_desc
               FROM   esch.sch_codelst
              WHERE   pk_codelst = FK_CODELST_COST_TYPE)
               SUBCOST_COST_TYPE,
            (SELECT   visit_name
               FROM   esch.sch_protocol_visit
              WHERE   pk_protocol_visit = FK_PROTOCOL_VISIT)
               visit_name,
            e.chain_id AS fk_study,
            e.user_id
     FROM   esch.SCH_SUBCOST_ITEM s,
            esch.event_assoc e,
            esch.SCH_SUBCOST_ITEM_VISIT v
    WHERE   e.event_id = fk_calendar
            AND v.FK_SUBCOST_ITEM(+) = PK_SUBCOST_ITEM
/

COMMENT ON TABLE VDA_V_STUDYCAL_SUBJCOST IS 'This view provides access to the Subject Cost Items linked with a Study Calendar'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_SUBJCOST.SUBCOST_CATEGORY IS 'Subject Cost Item Category'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_SUBJCOST.SUBCOST_COST_TYPE IS 'Subject Cost Type'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_SUBJCOST.VISIT_NAME IS 'Name of the visit the cost item is linked with'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_SUBJCOST.FK_STUDY IS 'The Foreign Key of the study'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_SUBJCOST.FK_ACCOUNT IS 'The account the subject cost item is linked with'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_SUBJCOST.PK_SUBCOST_ITEM IS 'Primary Key of the Subject Cost record'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_SUBJCOST.FK_CALENDAR IS 'Foreign Key to the Calendar record'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_SUBJCOST.NAME IS 'Name of the Calendar the subject Cost Item is linked with'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_SUBJCOST.SUBCOST_ITEM_NAME IS 'Subject Cost Item Name'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_SUBJCOST.SUBCOST_ITEM_COST IS 'Subject Cost Item Cost value'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_SUBJCOST.SUBCOST_ITEM_UNIT IS 'Number of Units planned'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_SUBJCOST.SUBCOST_ITEM_SEQ IS 'Sequence of the Cost Item'
/


--
-- VDA_V_STUDYCAL_RESOURCE  (View) 
--
--  Dependencies: 
--   ER_CODELST (Table)
--   ER_USER (Table)
--   SCH_PROTOCOL_VISIT (Table)
--   SCH_EVENTUSR (Table)
--   EVENT_ASSOC (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_STUDYCAL_RESOURCE
(VISIT_NAME, EVREC_EVENT, EVREC_USER, EVREC_RESOURCE, EVREC_DURATION, 
 EVREC_NOTES, CREATOR, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, 
 CALENDAR_PK, EVENT_PK, FK_ACCOUNT, LAST_MODIFIED_BY_FK, CREATOR_FK, 
 FK_STUDY)
AS 
SELECT   visit_name,
            d.NAME EVREC_EVENT,
            DECODE (EVENTUSR_TYPE,
                    'U', (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
                            FROM   eres.ER_USER
                           WHERE   PK_USER = EVENTUSR),
                    '')
               EVREC_USER,
            DECODE (EVENTUSR_TYPE,
                    'R', (SELECT   CODELST_DESC
                            FROM   eres.ER_CODELST
                           WHERE   PK_CODELST = EVENTUSR),
                    '')
               EVREC_RESOURCE,
            EVENTUSR_DURATION EVREC_DURATION,
            EVENTUSR_NOTES EVREC_NOTES,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = u.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = u.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            u.LAST_MODIFIED_DATE,
            u.CREATED_ON,
            d.chain_id,
            u.FK_EVENT,
            d.user_id, u.LAST_MODIFIED_BY as LAST_MODIFIED_BY_FK, u.CREATOR as CREATOR_FK 
,(select y.chain_id from esch.EVENT_ASSOC y where y.event_id = d.chain_id) FK_STUDY 
     FROM   esch.SCH_EVENTUSR u,
            esch.EVENT_ASSOC d,
            esch.sch_protocol_visit v
    WHERE       EVENT_ID = FK_EVENT
            AND EVENTUSR IS NOT NULL
            AND event_type = 'A'
            AND (EVENTUSR_TYPE = 'U' OR EVENTUSR_TYPE = 'R')
            AND fk_visit = pk_protocol_visit
/

COMMENT ON TABLE VDA_V_STUDYCAL_RESOURCE IS 'This view provides access to the Calendar 
resource information'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE.VISIT_NAME IS 'The name of the Calendar visit'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE.EVREC_EVENT IS 'The name of the Calendar Visit 
Event'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE.EVREC_USER IS 'The name of the user (named user 
in Velos)'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE.EVREC_RESOURCE IS 'The resource role (if named 
user is not used)'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE.EVREC_DURATION IS 'The duration the resource is 
needed for'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE.EVREC_NOTES IS 'Notes linked'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE.CREATOR IS 'The user who created the record 
(Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE.LAST_MODIFIED_BY IS 'The user who last modified 
the record(Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE.LAST_MODIFIED_DATE IS 'The date the record was 
last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE.CREATED_ON IS 'The date the record was created 
on (Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE.CALENDAR_PK IS 'The Foreign Key to the main 
Calendar record'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE.EVENT_PK IS 'The Primary Key of the Visit-Event 
record'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE.FK_ACCOUNT IS 'The account the record is linked 
with'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE.LAST_MODIFIED_BY_FK IS 'The Key to identify the 
Last Modified By of the record.'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE.CREATOR_FK IS 'The Key to identify the Creator 
of the record.'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE.FK_STUDY IS 'The Key to identify the study 
linked with the form response'
/


--
-- VDA_V_STORAGE_SPECIMEN  (View) 
--
--  Dependencies: 
--   ER_STUDY (Table)
--   ER_STORAGE_STATUS (Table)
--   ER_STORAGE (Table)
--   ER_SPECIMEN (Table)
--   ER_CODELST (Table)
--   ER_USER (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_STORAGE_SPECIMEN
(FK_ACCOUNT, PK_STORAGE, STORAGE_ID, STORAGE_NAME, STORAGE_ALTERNATEID, 
 STORAGE_TYPE, CHILD_STORAGE_TYPE, PARENT_STORAGE_ID, PARENT_STORAGE, STORAGE_FK_STUDY, 
 STORAGE_STUDY_NUMBER, STORAGE_STATUS, STORAGE_STATUS_DATE, STORAGE_TRACKING_USER, STORAGE_TRACKING_NO, 
 RID, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, 
 PK_SPECIMEN, SPEC_ID, STORAGE_MULTI_SPECIMEN, IP_ADD, LAST_MODIFIED_BY_FK, 
 CREATOR_FK)
AS 
SELECT   a.fk_account,
            pk_storage,
            a.storage_id,
            storage_name,
            STORAGE_ALTERNALEID AS STORAGE_ALTERNATEID,
            (SELECT   codelst_desc
               FROM   eres.ER_CODELST
              WHERE   pk_codelst = fk_codelst_storage_type)
               storage_type,
            (SELECT   codelst_desc
               FROM   eres.ER_CODELST
              WHERE   pk_codelst = fk_codelst_child_stype)
               child_storage_type,
            (SELECT   aa.storage_id
               FROM   eres.ER_STORAGE aa
              WHERE   aa.pk_storage = a.fk_storage)
               parent_storage_id,
            (SELECT   aa.storage_name
               FROM   eres.ER_STORAGE aa
              WHERE   aa.pk_storage = a.fk_storage)
               parent_storage,
            b.fk_study storage_fk_study,
            (SELECT   study_number
               FROM   eres.ER_STUDY
              WHERE   pk_study = b.fk_study)
               storage_study_number,
            (SELECT   codelst_desc
               FROM   eres.ER_CODELST
              WHERE   pk_codelst = fk_codelst_storage_status)
               storage_status,
            ss_start_date storage_status_date,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   eres.ER_USER
              WHERE   pk_user = b.fk_user)
               AS Storage_tracking_user,
            b.ss_tracking_number Storage_tracking_no,
            a.rid,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   eres.ER_USER
              WHERE   pk_user = a.creator)
               creator,
            a.created_on,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   eres.ER_USER
              WHERE   pk_user = a.last_modified_by)
               last_modified_by,
            a.last_modified_date,
            (SELECT   pk_specimen
               FROM   eres.er_specimen x
              WHERE   x.fk_storage = a.pk_storage)
               pk_specimen,
            (SELECT   SPEC_ID
               FROM   eres.er_specimen x
              WHERE   x.fk_storage = a.pk_storage)
               SPEC_ID,
            storage_multi_specimen,
            a.IP_ADD, a.LAST_MODIFIED_BY as LAST_MODIFIED_BY_FK, a.CREATOR as CREATOR_FK 
     FROM   eres.ER_STORAGE a, eres.ER_STORAGE_STATUS b
    WHERE   NVL (storage_istemplate, 0) <> 1 AND a.pk_storage = b.fk_storage
            AND b.pk_storage_status =
                  (SELECT   MAX (c.pk_storage_status)
                     FROM   eres.ER_STORAGE_STATUS c
                    WHERE   a.pk_storage = c.fk_storage
                            AND c.ss_start_date =
                                  (SELECT   MAX (d.ss_start_date)
                                     FROM   eres.ER_STORAGE_STATUS d
                                    WHERE   a.pk_storage = d.fk_storage))
/

COMMENT ON TABLE VDA_V_STORAGE_SPECIMEN IS 'This view provides access to storage units 
designed in Velos eSample. The view does not include the ones marked as a template.'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.PARENT_STORAGE IS 'The Parent Storage Name'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.STORAGE_FK_STUDY IS 'The FK to the study storage 
is linked with (through storage status)'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.STORAGE_STUDY_NUMBER IS 'The study number of the 
study the storage is linked with (through storage status)'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.STORAGE_STATUS IS 'The recent storage status'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.STORAGE_STATUS_DATE IS 'The Storage Status Date'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.STORAGE_TRACKING_USER IS 'The user linked with 
storage status'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.STORAGE_TRACKING_NO IS 'The tracking number 
linked with the storage status'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.RID IS 'The unique Row ID for Audit'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.CREATOR IS 'The user who created the record 
(Audit)'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.CREATED_ON IS 'The date the record was created on 
(Audit)'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.LAST_MODIFIED_BY IS 'The user who last modified 
the record (Audit)'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.LAST_MODIFIED_DATE IS 'The date the record was 
last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.PK_SPECIMEN IS 'The Primary Key of the specimen - 
if storage has a specimen stored in it'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.SPEC_ID IS 'The Specimen ID of the specimen - if 
storage has a specimen stored in it'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.STORAGE_MULTI_SPECIMEN IS 'Flag to indicate if 
storage can contain multiple specimens'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.IP_ADD IS 'The IP address of the client machine 
that initiated the session'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.LAST_MODIFIED_BY_FK IS 'The Key to identify the 
Last Modified By of the record.'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.CREATOR_FK IS 'The Key to identify the Creator of 
the record.'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.FK_ACCOUNT IS 'The account storage is linked 
with'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.PK_STORAGE IS 'The Primary Key of the storage 
record'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.STORAGE_ID IS 'The unique Storage ID'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.STORAGE_NAME IS 'The Storage Name'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.STORAGE_ALTERNATEID IS 'The Storage Alternate ID'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.STORAGE_TYPE IS 'The Storage Type'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.CHILD_STORAGE_TYPE IS 'The Child Storage type - 
immediate child'
/

COMMENT ON COLUMN VDA_V_STORAGE_SPECIMEN.PARENT_STORAGE_ID IS 'The Parent Storage ID'
/


--
-- VDA_V_SPECIMEN_LATESTSTATUS  (View) 
--
--  Dependencies: 
--   ER_STUDY (Table)
--   ER_SPECIMEN_STATUS (Table)
--   ER_SPECIMEN (Table)
--   F_GET_CODELSTDESC (Function)
--   ER_USER (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_SPECIMEN_LATESTSTATUS
(PK_SPECIMEN_STATUS, FK_SPECIMEN, SS_DATE, FK_CODELST_STATUS, SS_QUANTITY, 
 SS_QUANTITY_UNITS, SS_STATUS_BY, RID, CREATOR, CREATED_ON, 
 IP_ADD, SS_PROC_TYPE, SS_PROC_DATE, STATUS_DESC, PROCESS_TYPE_DESC, 
 SPECIMEN_ID, LAST_MODIFIED_BY, SS_HAND_OFF_DATE, SS_NOTES, SS_TRACKING_NUMBER, 
 FK_USER_RECEPIENT, STUDY_NUMBER, SS_ACTION, LAST_MODIFIED_DATE, LAST_MODIFIED_BY_FK, 
 CREATOR_FK, FK_STUDY)
AS 
SELECT   x.pk_specimen_status,
            x.fk_specimen,
            x.ss_date,
            x.fk_codelst_status,
            x.ss_quantity,
            eres.F_GET_CODELSTDESC (x.ss_quantity_units) ss_quantity_units,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = x.ss_status_by)
               ss_status_by,
            x.rid,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = x.creator)
               creator,
            x.created_on,
            x.ip_add,
            x.ss_proc_type,
            x.ss_proc_date,
            eres.F_GET_CODELSTDESC (fk_codelst_status) status_desc,
            eres.F_GET_CODELSTDESC (ss_proc_type) process_type_desc,
            (SELECT   spec_id
               FROM   eres.er_specimen sp
              WHERE   sp.pk_specimen = x.fk_specimen)
               specimen_id,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = x.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            x.SS_HAND_OFF_DATE,
            x.SS_NOTES,
            x.SS_TRACKING_NUMBER,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = x.FK_USER_RECEPIENT)
               FK_USER_RECEPIENT,
            (SELECT   study_number
               FROM   eres.er_study
              WHERE   pk_study = x.FK_STUDY)
               study_number,
            eres.F_GET_CODELSTDESC (SS_ACTION) SS_ACTION,
            x.LAST_MODIFIED_DATE,
            x.LAST_MODIFIED_BY as LAST_MODIFIED_BY_FK, x.CREATOR as CREATOR_FK 
,x.FK_STUDY
     FROM   eres.er_specimen_status x,
            (  SELECT   c.fk_specimen,
                        MAX (c.PK_SPECIMEN_STATUS) pk_specimen_status
                 FROM   eres.ER_SPECIMEN_STATUS c
                WHERE   c.ss_date = (SELECT   MAX (d.ss_date)
                                       FROM   eres.ER_SPECIMEN_STATUS d
                                      WHERE   c.fk_SPECIMEN = d.fk_SPECIMEN)
             GROUP BY   fk_specimen) o
    WHERE   x.pk_specimen_status = o.pk_specimen_status
/

COMMENT ON TABLE VDA_V_SPECIMEN_LATESTSTATUS IS 'This view provides latest specimen 
status. The latest status is calculated by most recent date and most recent entry for that 
date (in case of more than one status entered on that date).'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.SS_QUANTITY IS 'The quantity number linked 
with status record'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.SS_QUANTITY_UNITS IS 'The quantity unit 
linked with status record'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.SS_STATUS_BY IS 'The user who entered the 
status'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.RID IS 'The unique Row ID for Audit'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.CREATOR IS 'The user who created the record 
(Audit)'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.CREATED_ON IS 'The date the record was 
created on (Audit)'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.IP_ADD IS 'The IP address of the client 
machine that initiated the session'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.SS_PROC_TYPE IS 'The processing type code'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.SS_PROC_DATE IS 'The prcessing date'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.STATUS_DESC IS 'The Status description'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.PROCESS_TYPE_DESC IS 'The Process Type 
description'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.SPECIMEN_ID IS 'The Specimen ID'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.LAST_MODIFIED_BY IS 'The user who last 
modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.SS_HAND_OFF_DATE IS 'The date the specimen 
was handed over for processing'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.SS_NOTES IS 'The notes associated with the 
status'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.SS_TRACKING_NUMBER IS 'The tracking number 
linked with the status'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.FK_USER_RECEPIENT IS 'The recepient linked 
with the status'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.STUDY_NUMBER IS 'The study number linked 
with the status'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.SS_ACTION IS 'Action performed: 
incremented/decremented '
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.LAST_MODIFIED_DATE IS 'The date the record 
was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.LAST_MODIFIED_BY_FK IS 'The Key to identify 
the Last Modified By of the record.'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.CREATOR_FK IS 'The Key to identify the 
Creator of the record.'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.FK_STUDY IS 'The Key to identify the study 
linked with the form response'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.PK_SPECIMEN_STATUS IS 'The Primary key of 
the Specimen Status record'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.FK_SPECIMEN IS 'The FK to the main specimen 
record'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.SS_DATE IS 'The Status Date'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_LATESTSTATUS.FK_CODELST_STATUS IS 'The codelist value of 
the specimen status'
/


--
-- VDA_V_SPECIMEN_DETAILS  (View) 
--
--  Dependencies: 
--   ER_STUDY (Table)
--   ER_STORAGE (Table)
--   ER_SPECIMEN (Table)
--   ER_SITE (Table)
--   F_GET_CODELSTDESC (Function)
--   ER_CODELST (Table)
--   ER_USER (Table)
--   SCH_PROTOCOL_VISIT (Table)
--   SCH_EVENTS1 (Table)
--   PERSON (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_SPECIMEN_DETAILS
(PK_SPECIMEN, SPEC_ID, SPEC_ALTERNATE_ID, SPEC_DESCRIPTION, EXP_SPEC_QTY, 
 EXP_QTY_UNITS, COLL_SPEC_QTY, COLL_QTY_UNITS, CURRENT_SPEC_QTY, CURRENT_SPEC_QTY_UNITS, 
 SPEC_TYPE, SPEC_COLLECTION_DATE, OWNER, SPEC_STORAGE, SPEC_KIT_COMPONENT, 
 FK_STUDY, SPEC_STUDY_NUMBER, FK_PER, SPEC_PATIENT_CODE, SPEC_PATIENT_NAME, 
 SPEC_ORGANIZATION, VIST_NAME, EVENT_DESCRIPTION, SPEC_ANATOMIC_SITE, SPEC_TISSUE_SIDE, 
 SPEC_PATHOLOGY_STAT, SPEC_STORAGE_KIT, SPEC_NOTES, PATHOLOGIST, SURGEON, 
 COLLECTING_TECHNICIAN, PROCESSING_TECHNICIAN, SPEC_REMOVAL_TIME, SPEC_FREEZE_TIME, PARENT_SPEC_ID, 
 CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, FK_ACCOUNT, 
 RID, IP_ADD, SPEC_ENVT_CONS, SPEC_DISPOSITION, FK_SITE, 
 LAST_MODIFIED_BY_FK, CREATOR_FK)
AS 
SELECT   a.pk_specimen pk_specimen,
            SPEC_ID,
            SPEC_ALTERNATE_ID,
            SPEC_DESCRIPTION,
            SPEC_EXPECTED_QUANTITY exp_spec_qty,
            (SELECT   codelst_desc
               FROM   eres.ER_CODELST
              WHERE   pk_codelst = SPEC_EXPECTEDQ_UNITS)
               exp_qty_units,
            SPEC_BASE_ORIG_QUANTITY coll_spec_qty,
            (SELECT   codelst_desc
               FROM   eres.ER_CODELST
              WHERE   pk_codelst = SPEC_BASE_ORIGQ_UNITS)
               coll_qty_units,
            SPEC_ORIGINAL_QUANTITY current_spec_qty,
            (SELECT   codelst_desc
               FROM   eres.ER_CODELST
              WHERE   pk_codelst = SPEC_QUANTITY_UNITS)
               current_spec_qty_units,
            (SELECT   codelst_desc
               FROM   eres.ER_CODELST
              WHERE   pk_codelst = SPEC_TYPE)
               SPEC_TYPE,
            SPEC_COLLECTION_DATE,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = SPEC_OWNER_FK_USER)
               AS Owner,
            (SELECT   c.storage_name
               FROM   eres.ER_STORAGE c
              WHERE   c.pk_storage = a.fk_storage)
               spec_storage,
            (SELECT   d.storage_name
               FROM   eres.ER_STORAGE d
              WHERE   d.pk_storage = a.FK_STORAGE_KIT_COMPONENT)
               spec_kit_component,
            a.FK_STUDY fk_study,
            (SELECT   study_number
               FROM   eres.ER_STUDY
              WHERE   pk_study = a.FK_STUDY)
               spec_study_number,
            FK_PER,
            (SELECT   person_code
               FROM   epat.person
              WHERE   PK_person = fk_per)
               AS spec_patient_code,
            (SELECT   person_FNAME || ' ' || person_LNAME
               FROM   epat.person
              WHERE   PK_person = fk_per)
               AS spec_patient_name,
            (SELECT   site_name
               FROM   eres.ER_SITE
              WHERE   pk_site = FK_SITE)
               AS spec_organization,
            (SELECT   visit_name
               FROM   esch.sch_protocol_visit
              WHERE   pk_protocol_visit = fk_visit)
               VIST_NAME,
            (SELECT   description
               FROM   esch.sch_events1
              WHERE   event_id = FK_SCH_EVENTS1)
               Event_Description,
            (SELECT   codelst_desc
               FROM   eres.ER_CODELST
              WHERE   pk_codelst = SPEC_ANATOMIC_SITE)
               SPEC_ANATOMIC_SITE,
            (SELECT   codelst_desc
               FROM   eres.ER_CODELST
              WHERE   pk_codelst = SPEC_TISSUE_SIDE)
               SPEC_TISSUE_SIDE,
            (SELECT   codelst_desc
               FROM   eres.ER_CODELST
              WHERE   pk_codelst = SPEC_PATHOLOGY_STAT)
               SPEC_PATHOLOGY_STAT,
            (SELECT   STORAGE_NAME
               FROM   eres.ER_STORAGE
              WHERE   PK_STORAGE = FK_STORAGE_KIT)
               SPEC_STORAGE_KIT,
            SPEC_NOTES,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = FK_USER_PATH)
               AS Pathologist,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = FK_USER_SURG)
               AS Surgeon,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = FK_USER_COLLTECH)
               AS Collecting_Technician,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = FK_USER_PROCTECH)
               AS Processing_Technician,
            SPEC_REMOVAL_TIME,
            SPEC_FREEZE_TIME,
            (SELECT   aa.SPEC_ID
               FROM   eres.ER_SPECIMEN aa
              WHERE   aa.pk_specimen = a.FK_SPECIMEN)
               parent_spec_id,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = a.CREATOR)
               creator,
            a.CREATED_ON,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = a.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            a.LAST_MODIFIED_DATE,
            FK_ACCOUNT,
            a.RID,
            a.IP_ADD,
            SPEC_ENVT_CONS,
            eres.F_GET_CODELSTDESC (SPEC_DISPOSITION) SPEC_DISPOSITION,
            FK_SITE, a.LAST_MODIFIED_BY as LAST_MODIFIED_BY_FK, a.CREATOR as 
CREATOR_FK
     FROM   eres.ER_SPECIMEN a
/

COMMENT ON TABLE VDA_V_SPECIMEN_DETAILS IS 'This view provides access to specimen 
attributes.'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.SPEC_PATHOLOGY_STAT IS 'The pathological status '
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.SPEC_STORAGE_KIT IS 'The storage kit the specimen 
is linked with'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.SPEC_NOTES IS 'The specimen notes'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.PATHOLOGIST IS 'The pathologist linked with 
specimen record'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.SURGEON IS 'The surgeon linked with specimen 
record'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.COLLECTING_TECHNICIAN IS 'The collecting 
technician linked with specimen record'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.PROCESSING_TECHNICIAN IS 'The processing 
technician linked with specimen record'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.PARENT_SPEC_ID IS 'The parent specimen ID'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.CREATOR IS 'The user who created the record 
(Audit)'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.CREATED_ON IS 'The date the record was created on 
(Audit)'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.LAST_MODIFIED_BY IS 'The user who last modified 
the record (Audit)'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.LAST_MODIFIED_DATE IS 'The date the record was 
last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.FK_ACCOUNT IS 'The account storage is linked 
with'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.RID IS 'The unique Row ID for Audit'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.IP_ADD IS 'The IP address of the client machine 
that initiated the session'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.SPEC_ENVT_CONS IS 'The environmental constraints'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.SPEC_DISPOSITION IS 'The specimen disposition'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.FK_SITE IS 'The Key to identify the 
Study/Organization linked with the specimen'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.LAST_MODIFIED_BY_FK IS 'The Key to identify the 
Last Modified By of the record.'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.CREATOR_FK IS 'The Key to identify the Creator of 
the record.'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.PK_SPECIMEN IS 'The Primary Key of the specimen 
record'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.SPEC_ID IS 'The specimen id'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.SPEC_ALTERNATE_ID IS 'The specimen Alternate ID'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.SPEC_DESCRIPTION IS 'The specimen description'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.EXP_SPEC_QTY IS 'The expected specimen quantity'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.EXP_QTY_UNITS IS 'The expected specimen quantity 
units'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.COLL_SPEC_QTY IS 'The collected specimen 
quantity'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.COLL_QTY_UNITS IS 'The collected specimen 
quantity units'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.CURRENT_SPEC_QTY IS 'The current specimen 
quantity'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.CURRENT_SPEC_QTY_UNITS IS 'The current specimen 
quantity units'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.SPEC_TYPE IS 'The specimen type'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.SPEC_COLLECTION_DATE IS 'The specimen collection 
date'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.OWNER IS 'The specimen owner'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.SPEC_STORAGE IS 'The specimen storage'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.SPEC_KIT_COMPONENT IS 'The specimen kit 
component'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.FK_STUDY IS 'The FK of study linked with specimen 
(if linked with a study)'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.SPEC_STUDY_NUMBER IS 'The study number of the 
study the specimen is linked with'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.FK_PER IS 'The FK of patient the specimen is 
linked with (if linked with a patient)'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.SPEC_PATIENT_CODE IS 'The patient code of patient 
the specimen is linked with (if linked with a patient)'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.SPEC_PATIENT_NAME IS 'The patient name of patient 
the specimen is linked with (if linked with a patient)'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.SPEC_ORGANIZATION IS 'The organization or site 
the specimen is linked with'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.VIST_NAME IS 'The name of the patient visit the 
specimen was collected with'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.EVENT_DESCRIPTION IS 'The name of the patient 
visit event the specimen was collected with'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.SPEC_ANATOMIC_SITE IS 'The specimen anatomic 
site'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_DETAILS.SPEC_TISSUE_SIDE IS 'The specimen tissue side'
/


--
-- VDA_V_SPECIMEN_ALLSTATUS  (View) 
--
--  Dependencies: 
--   ER_STUDY (Table)
--   ER_SPECIMEN_STATUS (Table)
--   ER_SPECIMEN (Table)
--   F_GET_CODELSTDESC (Function)
--   ER_USER (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_SPECIMEN_ALLSTATUS
(PK_SPECIMEN_STATUS, FK_SPECIMEN, SS_DATE, FK_CODELST_STATUS, SS_QUANTITY, 
 SS_QUANTITY_UNITS, SS_STATUS_BY, RID, CREATOR, CREATED_ON, 
 IP_ADD, SS_PROC_TYPE, SS_PROC_DATE, STATUS_DESC, PROCESS_TYPE_DESC, 
 SPECIMEN_ID, LAST_MODIFIED_BY, SS_HAND_OFF_DATE, SS_NOTES, SS_TRACKING_NUMBER, 
 FK_USER_RECEPIENT, STUDY_NUMBER, SS_ACTION, LAST_MODIFIED_BY_FK, CREATOR_FK, 
 FK_STUDY)
AS 
SELECT   x.pk_specimen_status,
            x.fk_specimen,
            x.ss_date,
            x.fk_codelst_status,
            x.ss_quantity,
            eres.F_GET_CODELSTDESC (x.ss_quantity_units) ss_quantity_units,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = x.ss_status_by)
               ss_status_by,
            x.rid,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = x.creator)
               creator,
            x.created_on,
            x.ip_add,
            x.ss_proc_type,
            x.ss_proc_date,
            eres.F_GET_CODELSTDESC (fk_codelst_status) status_desc,
            eres.F_GET_CODELSTDESC (ss_proc_type) process_type_desc,
            spec_id AS specimen_id,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = x.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            x.SS_HAND_OFF_DATE,
            x.SS_NOTES,
            x.SS_TRACKING_NUMBER,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = x.FK_USER_RECEPIENT)
               FK_USER_RECEPIENT,
            (SELECT   study_number
               FROM   eres.er_study
              WHERE   pk_study = x.FK_STUDY)
               study_number,
            eres.F_GET_CODELSTDESC (SS_ACTION) SS_ACTION,
            x.LAST_MODIFIED_BY as LAST_MODIFIED_BY_FK, x.CREATOR as CREATOR_FK 
,x.FK_STUDY
     FROM   eres.er_specimen_status x, eres.er_specimen sp
    WHERE   pk_specimen = x.fk_specimen
/

COMMENT ON TABLE VDA_V_SPECIMEN_ALLSTATUS IS 'This view provides access to all specimen 
statuses'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.LAST_MODIFIED_BY_FK IS 'The Key to identify the 
Last Modified By of the record.'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.CREATOR_FK IS 'The Key to identify the Creator 
of the record.'
/

COMMENT ON COLUMN VDA_V_SPECIMEN_ALLSTATUS.FK_STUDY IS 'The Key to identify the study 
linked with the status record'
/


--
-- VDA_V_PAYMENTS  (View) 
--
--  Dependencies: 
--   ER_STUDY (Table)
--   F_GET_CODELSTDESC (Function)
--   ER_USER (Table)
--   ER_MILEPAYMENT (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_PAYMENTS
(STUDY_NUMBER, STUDY_TITLE, MSPAY_DATE, MSPAY_TYPE, MSPAY_AMT_RECVD, 
 MSPAY_DESC, MSPAY_COMMENTS, PK_MILEPAYMENT, CREATOR, LAST_MODIFIED_BY, 
 LAST_MODIFIED_DATE, CREATED_ON, FK_STUDY, FK_ACCOUNT, CREATOR_FK, 
 LAST_MODIFIED_BY_FK)
AS 
SELECT   STUDY_NUMBER,
            STUDY_TITLE,
            MILEPAYMENT_DATE MSPAY_DATE,
            ERES.F_GET_CODELSTDESC (MILEPAYMENT_TYPE) MSPAY_TYPE,
            MILEPAYMENT_AMT MSPAY_AMT_RECVD,
            MILEPAYMENT_DESC MSPAY_DESC,
            MILEPAYMENT_COMMENTS MSPAY_COMMENTS,
            PK_MILEPAYMENT,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = x.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = x.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            x.LAST_MODIFIED_DATE,
            x.CREATED_ON,
            FK_STUDY,
            FK_ACCOUNT, x.CREATOR as CREATOR_FK,  x.LAST_MODIFIED_BY as  
LAST_MODIFIED_BY_FK
     FROM   eres.ER_MILEPAYMENT x, eres.ER_STUDY s
    WHERE   PK_STUDY = FK_STUDY AND MILEPAYMENT_DELFLAG <> 'Y'
/

COMMENT ON TABLE VDA_V_PAYMENTS IS 'This view provides access to the payment information 
for studies'
/

COMMENT ON COLUMN VDA_V_PAYMENTS.STUDY_NUMBER IS 'The Study Number of the study the 
payment  is linked with'
/

COMMENT ON COLUMN VDA_V_PAYMENTS.STUDY_TITLE IS 'The Study Title of the study the payment 
is linked with'
/

COMMENT ON COLUMN VDA_V_PAYMENTS.MSPAY_DATE IS 'The payment date'
/

COMMENT ON COLUMN VDA_V_PAYMENTS.MSPAY_TYPE IS 'The payment type'
/

COMMENT ON COLUMN VDA_V_PAYMENTS.MSPAY_AMT_RECVD IS 'The payment amount received'
/

COMMENT ON COLUMN VDA_V_PAYMENTS.MSPAY_DESC IS 'The payment description'
/

COMMENT ON COLUMN VDA_V_PAYMENTS.MSPAY_COMMENTS IS 'The payment comments'
/

COMMENT ON COLUMN VDA_V_PAYMENTS.PK_MILEPAYMENT IS 'The Primary Key of the payment record'
/

COMMENT ON COLUMN VDA_V_PAYMENTS.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_PAYMENTS.LAST_MODIFIED_BY IS 'The user who last modified the 
record(Audit)'
/

COMMENT ON COLUMN VDA_V_PAYMENTS.LAST_MODIFIED_DATE IS 'The date the record was last 
modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_PAYMENTS.CREATED_ON IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_PAYMENTS.FK_STUDY IS 'The Foreign Key to the study milestone is 
linked with'
/

COMMENT ON COLUMN VDA_V_PAYMENTS.FK_ACCOUNT IS 'The account the milestone is linked with'
/

COMMENT ON COLUMN VDA_V_PAYMENTS.CREATOR_FK IS 'The Key to identify the Creator of the 
record.'
/

COMMENT ON COLUMN VDA_V_PAYMENTS.LAST_MODIFIED_BY_FK IS 'The Key to identify the Last 
Modified By of the record.'
/


--
-- VDA_V_PAT_TXARM  (View) 
--
--  Dependencies: 
--   ER_STUDYTXARM (Table)
--   ER_STUDY (Table)
--   ER_USER (Table)
--   ER_PER (Table)
--   ER_PATTXARM (Table)
--   ER_PATPROT (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_PAT_TXARM
(PATIENT_ID, PATIENT_STUDY_ID, TREATMENT_NAME, DRUG_INFO, TREATMENT_STATUS_DATE, 
 TREATMENT_END_DATE, TREATMENT_NOTES, CREATOR, CREATED_ON, LAST_MODIFIED_BY, 
 LAST_MODIFIED_DATE, FK_ACCOUNT, RID, PK_PATTXARM, FK_STUDY, 
 FK_PER, STUDY_NUMBER, STUDY_TITLE, LAST_MODIFIED_BY_FK, CREATOR_FK)
AS 
SELECT   PER_CODE,
            PATPROT_PATSTDID,
            (SELECT   TX_NAME
               FROM   eres.ER_STUDYTXARM
              WHERE   PK_STUDYTXARM = FK_STUDYTXARM)
               PTARM_TREATMT_NAME,
            TX_DRUG_INFO,
            TX_START_DATE,
            TX_END_DATE,
            NOTES PTARM_NOTES,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = pt.CREATOR)
               CREATOR,
            pt.CREATED_ON,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = pt.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            pt.LAST_MODIFIED_DATE,
            p.FK_ACCOUNT,
            pt.RID,
            PK_PATTXARM,
            fk_study,
            fk_per,
            study_number,
            study_title,  pt.LAST_MODIFIED_BY LAST_MODIFIED_BY_FK, pt.CREATOR CREATOR_FK 
     FROM   eres.ER_PATTXARM pt,
            eres.ER_PATPROT pp,
            eres.ER_PER p,
            eres.er_study s
    WHERE       PK_PATPROT = FK_PATPROT
            AND pk_per = fk_per
            AND s.pk_study = fk_study
/

COMMENT ON TABLE VDA_V_PAT_TXARM IS 'This view provides access to patient treatment arm 
information'
/

COMMENT ON COLUMN VDA_V_PAT_TXARM.PATIENT_ID IS 'The Patient ID or Patient Code'
/

COMMENT ON COLUMN VDA_V_PAT_TXARM.PATIENT_STUDY_ID IS 'The Patient Study ID'
/

COMMENT ON COLUMN VDA_V_PAT_TXARM.TREATMENT_NAME IS 'The Treatment name'
/

COMMENT ON COLUMN VDA_V_PAT_TXARM.DRUG_INFO IS 'Drug information linked with the treatment 
arm'
/

COMMENT ON COLUMN VDA_V_PAT_TXARM.TREATMENT_STATUS_DATE IS 'The status date'
/

COMMENT ON COLUMN VDA_V_PAT_TXARM.TREATMENT_END_DATE IS 'The Treatment arm end date'
/

COMMENT ON COLUMN VDA_V_PAT_TXARM.TREATMENT_NOTES IS 'The Notes associated with the 
patient treatment arm'
/

COMMENT ON COLUMN VDA_V_PAT_TXARM.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_TXARM.CREATED_ON IS 'The date the record was created on 
(Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_TXARM.LAST_MODIFIED_BY IS 'The user who last modified the 
record(Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_TXARM.LAST_MODIFIED_DATE IS 'The date the record was last 
modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_TXARM.FK_ACCOUNT IS 'The account the patient is linked with'
/

COMMENT ON COLUMN VDA_V_PAT_TXARM.RID IS 'The unique Row ID for Audit'
/

COMMENT ON COLUMN VDA_V_PAT_TXARM.PK_PATTXARM IS 'The PK of treatment arm record'
/

COMMENT ON COLUMN VDA_V_PAT_TXARM.FK_STUDY IS 'The FK to the study record'
/

COMMENT ON COLUMN VDA_V_PAT_TXARM.FK_PER IS 'The FK to the patient record'
/

COMMENT ON COLUMN VDA_V_PAT_TXARM.STUDY_NUMBER IS 'The Study Number'
/

COMMENT ON COLUMN VDA_V_PAT_TXARM.STUDY_TITLE IS 'The Study Title'
/

COMMENT ON COLUMN VDA_V_PAT_TXARM.LAST_MODIFIED_BY_FK IS 'The Key to identify the Last 
Modified By of the record.'
/

COMMENT ON COLUMN VDA_V_PAT_TXARM.CREATOR_FK IS 'The Key to identify the Creator of the 
record.'
/


--
-- VDA_V_PAT_SITES  (View) 
--
--  Dependencies: 
--   ER_SITE (Table)
--   F_GET_YESNO (Function)
--   CODE_LST_NAMES (Function)
--   ER_USER (Table)
--   ER_PATFACILITY (Table)
--   PERSON (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_PAT_SITES
(PATIENT_ID, SITE_NAME, PAT_FACILITY_ID, REG_DATE, PROVIDER, 
 SPECIALTY_GROUP, FACILITY_ACCESS, FACILITY_DEFAULT, CREATED_ON, LAST_MODIFIED_DATE, 
 CREATOR, LAST_MODIFIED_BY, FK_ACCOUNT, RID, PK_PATFACILITY, 
 FK_PER, FK_SITE, LAST_MODIFIED_BY_FK, CREATOR_FK)
AS 
SELECT   PERSON_CODE,
            (SELECT   SITE_NAME
               FROM   eres.ER_SITE
              WHERE   PK_SITE = ER_PATFACILITY.FK_SITE)
               SITE_NAME,
            ER_PATFACILITY.PAT_FACILITYID,
            TRUNC (PATFACILITY_REGDATE) REG_DATE,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = PATFACILITY_PROVIDER)
               PROVIDER,
            eres.code_lst_names (PATFACILITY_SPLACCESS) SPECIALTY_GROUP,
            DECODE (PATFACILITY_ACCESSRIGHT, 0, 'Revoked', 'Granted')
               FACILITY_ACCESS,
            eres.F_GET_YESNO (PATFACILITY_DEFAULT) FACILITY_DEFAULT,
            ER_PATFACILITY.CREATED_ON,
            ER_PATFACILITY.LAST_MODIFIED_DATE,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = ER_PATFACILITY.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = ER_PATFACILITY.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            FK_ACCOUNT,
            ER_PATFACILITY.RID,
            PK_PATFACILITY,
            fk_per, ER_PATFACILITY.FK_SITE , ER_PATFACILITY.LAST_MODIFIED_BY as 
LAST_MODIFIED_BY_FK, ER_PATFACILITY.CREATOR as CREATOR_FK 
     FROM   ERES.ER_PATFACILITY, EPAT.PERSON
    WHERE   ER_PATFACILITY.FK_PER = PERSON.PK_PERSON
/

COMMENT ON TABLE VDA_V_PAT_SITES IS 'This view provides access to the Patient Organization 
association'
/

COMMENT ON COLUMN VDA_V_PAT_SITES.PATIENT_ID IS 'The Patient ID or Patient Code'
/

COMMENT ON COLUMN VDA_V_PAT_SITES.SITE_NAME IS 'The Site Name the patient is linked with'
/

COMMENT ON COLUMN VDA_V_PAT_SITES.PAT_FACILITY_ID IS 'The Patient Facility ID'
/

COMMENT ON COLUMN VDA_V_PAT_SITES.REG_DATE IS 'The Date the patient was associated with 
the facility'
/

COMMENT ON COLUMN VDA_V_PAT_SITES.PROVIDER IS 'The user linked with patient facility 
association'
/

COMMENT ON COLUMN VDA_V_PAT_SITES.SPECIALTY_GROUP IS 'The specialty the facility has 
access to'
/

COMMENT ON COLUMN VDA_V_PAT_SITES.FACILITY_ACCESS IS 'The users of this site has access to 
the patient'
/

COMMENT ON COLUMN VDA_V_PAT_SITES.FACILITY_DEFAULT IS 'Flag to indicate if it is the 
default facility or site for the patient'
/

COMMENT ON COLUMN VDA_V_PAT_SITES.CREATED_ON IS 'The date the record was created on 
(Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_SITES.LAST_MODIFIED_DATE IS 'The date the record was last 
modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_SITES.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_SITES.LAST_MODIFIED_BY IS 'The user who last modified the 
record(Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_SITES.FK_ACCOUNT IS 'The account the patient is linked with'
/

COMMENT ON COLUMN VDA_V_PAT_SITES.RID IS 'The unique Row ID for Audit'
/

COMMENT ON COLUMN VDA_V_PAT_SITES.PK_PATFACILITY IS 'The Primary Key of the patient 
facility or site record'
/

COMMENT ON COLUMN VDA_V_PAT_SITES.FK_PER IS 'The Primary Key of the patient record'
/

COMMENT ON COLUMN VDA_V_PAT_SITES.FK_SITE IS 'The Key to identify Patient Site or 
Organization'
/

COMMENT ON COLUMN VDA_V_PAT_SITES.LAST_MODIFIED_BY_FK IS 'The Key to identify the Last 
Modified By of the record.'
/

COMMENT ON COLUMN VDA_V_PAT_SITES.CREATOR_FK IS 'The Key to identify the Creator'
/


--
-- VDA_V_PAT_SCHEDULE  (View) 
--
--  Dependencies: 
--   ER_STUDY (Table)
--   ER_SITE (Table)
--   ER_USER (Table)
--   ER_PATPROT (Table)
--   SCH_PROTOCOL_VISIT (Table)
--   SCH_EVENTS1 (Table)
--   EVENT_ASSOC (Table)
--   SCH_CODELST (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_PAT_SCHEDULE
(PAT_STUDY_ID, FK_STUDY, CALENDAR_PK, PTSCH_VISIT, PTSCH_SUGSCH_DATE, 
 PTSCH_ACTSCH_DATE, PTSCH_EVENT_PK, PTSCH_EVENT, PTSCH_EVENT_STAT, EVENT_ID, 
 CREATOR, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, FK_PER, 
 EVENTSTAT_DATE, EVENTSTAT_NOTES, PATPROT_STAT, FK_SITE_ENROLLING, EVENT_SEQUENCE, 
 SERVICE_SITE_ID, FACILITY_ID, COVERAGE_TYPE, ENROLLING_SITE_NAME, STUDY_NUMBER, 
 FK_ACCOUNT, VISIT_NAME, SCHEDULE_STATUS, LAST_MODIFIED_BY_FK, CREATOR_FK, 
 EVENT_CPTCODE)
AS 
SELECT   PATPROT_PATSTDID PAT_STUDY_ID,
            c.FK_STUDY,
            a.SESSION_ID,
            FK_VISIT,
            START_DATE_TIME PTSCH_SUGSCH_DATE,
            ACTUAL_SCHDATE PTSCH_ACTSCH_DATE,
            a.fk_assoc,
            DESCRIPTION PTSCH_EVENT,
            (SELECT   CODELST_DESC
               FROM   esch.SCH_CODELST
              WHERE   ISCONFIRMED = PK_CODELST)
               PTSCH_EVENT_STAT,
            EVENT_ID,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = a.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = a.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            a.LAST_MODIFIED_DATE,
            a.CREATED_ON,
            c.FK_PER,
            EVENT_EXEON EVENTSTAT_DATE,
            notes EVENTSTAT_NOTES,
            PATPROT_STAT,
            fk_site_enrolling,
            event_sequence,
            DECODE (service_site_id,
                    NULL, NULL,
                    0, NULL,
                    (SELECT   site_name
                       FROM   eres.er_site
                      WHERE   pk_site = service_site_id))
               service_site_id,
            DECODE (facility_id,
                    NULL, NULL,
                    0, NULL,
                    (SELECT   site_name
                       FROM   eres.er_site
                      WHERE   pk_site = facility_id))
               facility_id,
            (SELECT   CODELST_DESC
               FROM   esch.SCH_CODELST
              WHERE   fk_codelst_covertype = PK_CODELST)
               coverage_type,
            (SELECT   site_name
               FROM   eres.er_site st
              WHERE   st.pk_site = fk_site_enrolling)
               enrolling_site_name,
            STUDY_NUMBER,
            FK_ACCOUNT,
            (SELECT   visit_name
               FROM   esch.sch_protocol_visit
              WHERE   pk_protocol_visit = FK_VISIT)
               visit_name,
            DECODE (status,
                    0,
                    'Current',
                    5,
                    'Discontinued or old')
               schedule_status, a.LAST_MODIFIED_BY as LAST_MODIFIED_BY_FK, a.CREATOR as 
CREATOR_FK ,
               (select x.event_cptcode from esch.event_assoc x where x.event_id = 
a.fk_assoc) EVENT_CPTCODE 
     FROM   esch.SCH_EVENTS1 a, eres.ER_PATPROT c, eres.er_study s
    WHERE       fk_patprot <> 0
            AND pk_patprot = fk_patprot
            AND s.pk_study = c.fk_study
/

COMMENT ON TABLE VDA_V_PAT_SCHEDULE IS 'This view provides access to the Patient Schedules 
on studies'
/

COMMENT ON COLUMN VDA_V_PAT_SCHEDULE.EVENT_CPTCODE IS 'The CPT Code linked with the 
scheduled event'
/

COMMENT ON COLUMN VDA_V_PAT_SCHEDULE.PAT_STUDY_ID IS 'he patient study id'
/

COMMENT ON COLUMN VDA_V_PAT_SCHEDULE.FK_STUDY IS 'The Foreign Key to the study milestone 
is linked with'
/

COMMENT ON COLUMN VDA_V_PAT_SCHEDULE.CALENDAR_PK IS 'The foreign key to the calendar 
record'
/

COMMENT ON COLUMN VDA_V_PAT_SCHEDULE.PTSCH_VISIT IS 'The visit the scheduled event is 
linked with '
/

COMMENT ON COLUMN VDA_V_PAT_SCHEDULE.PTSCH_SUGSCH_DATE IS 'The suggested or initial 
scheduled date of the event per calendar design'
/

COMMENT ON COLUMN VDA_V_PAT_SCHEDULE.PTSCH_ACTSCH_DATE IS 'The actual scheduled date of 
the event'
/

COMMENT ON COLUMN VDA_V_PAT_SCHEDULE.PTSCH_EVENT_PK IS 'The foreign key to the event 
record in the source calendar'
/

COMMENT ON COLUMN VDA_V_PAT_SCHEDULE.PTSCH_EVENT IS 'The name of the event'
/

COMMENT ON COLUMN VDA_V_PAT_SCHEDULE.PTSCH_EVENT_STAT IS 'The current event status'
/

COMMENT ON COLUMN VDA_V_PAT_SCHEDULE.EVENT_ID IS 'The Primary Key of the schedule record'
/

COMMENT ON COLUMN VDA_V_PAT_SCHEDULE.CREATOR IS 'The user who created the calendar record 
(Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_SCHEDULE.LAST_MODIFIED_BY IS 'The user who last modified the 
calendar record(Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_SCHEDULE.LAST_MODIFIED_DATE IS 'The date the calendar record 
was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_SCHEDULE.CREATED_ON IS 'The date the calendar record was 
created on (Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_SCHEDULE.FK_PER IS 'The foreign Key to the patient record'
/

COMMENT ON COLUMN VDA_V_PAT_SCHEDULE.EVENTSTAT_DATE IS 'The date event status was entered 
on '
/

COMMENT ON COLUMN VDA_V_PAT_SCHEDULE.EVENTSTAT_NOTES IS 'The notes linked with the event 
status'
/

COMMENT ON COLUMN VDA_V_PAT_SCHEDULE.PATPROT_STAT IS 'The enrollment record status. A 
value of 1 indicates that the record is for most recent patient calendar schedule'
/

COMMENT ON COLUMN VDA_V_PAT_SCHEDULE.FK_SITE_ENROLLING IS 'The foreign key to the 
enrolling site'
/

COMMENT ON COLUMN VDA_V_PAT_SCHEDULE.EVENT_SEQUENCE IS 'The sequence of the event in the 
visit'
/

COMMENT ON COLUMN VDA_V_PAT_SCHEDULE.SERVICE_SITE_ID IS 'The site the event/service is or 
will be performed at'
/

COMMENT ON COLUMN VDA_V_PAT_SCHEDULE.FACILITY_ID IS 'The facility the event/service is 
linked with'
/

COMMENT ON COLUMN VDA_V_PAT_SCHEDULE.COVERAGE_TYPE IS 'The patient level coverage type 
identifier for the calendar-visit-even'
/

COMMENT ON COLUMN VDA_V_PAT_SCHEDULE.ENROLLING_SITE_NAME IS 'Patient Enrolling Site name'
/

COMMENT ON COLUMN VDA_V_PAT_SCHEDULE.STUDY_NUMBER IS 'The Study Number of the study the 
payment  is linked with'
/

COMMENT ON COLUMN VDA_V_PAT_SCHEDULE.FK_ACCOUNT IS 'The account the calendar is linked 
with'
/

COMMENT ON COLUMN VDA_V_PAT_SCHEDULE.SCHEDULE_STATUS IS 'Flag for indicating if this the 
current schedule for the patient'
/

COMMENT ON COLUMN VDA_V_PAT_SCHEDULE.LAST_MODIFIED_BY_FK IS 'The Key to identify the Last 
Modified By of the record.'
/

COMMENT ON COLUMN VDA_V_PAT_SCHEDULE.CREATOR_FK IS 'The Key to identify the Creator'
/


--
-- VDA_V_PAT_DEMO_NOPHI  (View) 
--
--  Dependencies: 
--   ER_SITE (Table)
--   F_GET_CODELSTDESC (Function)
--   PKG_UTIL (Package)
--   CODE_LST_NAMES (Function)
--   ER_USER (Table)
--   PERSON (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_PAT_DEMO_NOPHI
(PTDEM_PAT_ID, PTDEM_SURVIVSTAT, PTDEM_GENDER, PTDEM_MARSTAT, PTDEM_BLOODGRP, 
 PTDEM_PRI_ETHNY, PTDEM_PRI_RACE, PTDEM_ADD_ETHNY, PTDEM_ADD_RACE, PTDEM_STATE, 
 PTDEM_COUNTRY, PTDEM_PRI_SITE, FK_SITE, PTDEM_EMPLOY, PTDEM_EDUCATION, 
 PTDEM_NOTES, CREATOR, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, 
 PK_PERSON, PTDEM_PHYOTHER, PTDEM_SPLACCESS, PTDEM_REGBY, PTDEM_REGDATE_YR, 
 FK_ACCOUNT, PERSON_REGBY_FK, LAST_MODIFIED_BY_FK, CREATOR_FK)
AS 
SELECT   PERSON_CODE PTDEM_PAT_ID,
            ERES.F_GET_CODELSTDESC (FK_CODELST_PSTAT) PTDEM_SURVIVSTAT,
            ERES.F_GET_CODELSTDESC (FK_CODELST_GENDER) PTDEM_GENDER,
            ERES.F_GET_CODELSTDESC (FK_CODELST_MARITAL) PTDEM_MARSTAT,
            ERES.F_GET_CODELSTDESC (FK_CODELST_BLOODGRP) PTDEM_BLOODGRP,
            ERES.F_GET_CODELSTDESC (FK_CODELST_ETHNICITY) PTDEM_PRI_ETHNY,
            ERES.F_GET_CODELSTDESC (FK_CODELST_RACE) PTDEM_PRI_RACE,
            ERES.CODE_LST_NAMES (PERSON_ADD_ETHNICITY) PTDEM_ADD_ETHNY,
            ERES.CODE_LST_NAMES (PERSON_ADD_RACE) PTDEM_ADD_RACE,
            PERSON_STATE PTDEM_STATE,
            PERSON_COUNTRY PTDEM_COUNTRY,
            SITE_NAME AS PTDEM_PRI_SITE,
            fk_site,
            ERES.F_GET_CODELSTDESC (FK_CODELST_EMPLOYMENT) PTDEM_EMPLOY,
            ERES.F_GET_CODELSTDESC (FK_CODELST_EDU) PTDEM_EDUCATION,
            PERSON_NOTES_CLOB PTDEM_NOTES,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = p.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = p.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            p.LAST_MODIFIED_DATE,
            p.CREATED_ON,
            PK_PERSON,
            PERSON_PHYOTHER PTDEM_PHYOTHER,
            ERES.PKG_util.f_getCodeValues (PERSON_SPLACCESS, ',', ';')
               AS PTDEM_SPLACCESS,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   pk_user = PERSON_REGBY)
               PTDEM_REGBY,
            EXTRACT (YEAR FROM PERSON_REGDATE) PTDEM_REGDATE_YR,
            p.FK_ACCOUNT, PERSON_REGBY PERSON_REGBY_FK, p.LAST_MODIFIED_BY 
LAST_MODIFIED_BY_FK, p.CREATOR as CREATOR_FK 
     FROM   EPAT.PERSON p, eres.er_site
    WHERE   PK_SITE = FK_SITE
/

COMMENT ON TABLE VDA_V_PAT_DEMO_NOPHI IS 'This view provides access to the patient 
demographics data. This view does not provide any PHI'
/

COMMENT ON COLUMN VDA_V_PAT_DEMO_NOPHI.LAST_MODIFIED_BY_FK IS 'The Key to identify the 
Last Modified By of the record.'
/

COMMENT ON COLUMN VDA_V_PAT_DEMO_NOPHI.CREATOR_FK IS 'The Key to identify the Creator'
/

COMMENT ON COLUMN VDA_V_PAT_DEMO_NOPHI.PTDEM_PAT_ID IS 'The Patient ID or Patient Code'
/

COMMENT ON COLUMN VDA_V_PAT_DEMO_NOPHI.PTDEM_SURVIVSTAT IS 'Patient Survival Status'
/

COMMENT ON COLUMN VDA_V_PAT_DEMO_NOPHI.PTDEM_GENDER IS 'Patient Gender'
/

COMMENT ON COLUMN VDA_V_PAT_DEMO_NOPHI.PTDEM_MARSTAT IS 'Patient Marital Status'
/

COMMENT ON COLUMN VDA_V_PAT_DEMO_NOPHI.PTDEM_BLOODGRP IS 'Patient Blood Group'
/

COMMENT ON COLUMN VDA_V_PAT_DEMO_NOPHI.PTDEM_PRI_ETHNY IS 'Patient Primary Ethnicity'
/

COMMENT ON COLUMN VDA_V_PAT_DEMO_NOPHI.PTDEM_PRI_RACE IS 'Patient Primary Race'
/

COMMENT ON COLUMN VDA_V_PAT_DEMO_NOPHI.PTDEM_ADD_ETHNY IS 'Patient Additional Ethnicity'
/

COMMENT ON COLUMN VDA_V_PAT_DEMO_NOPHI.PTDEM_ADD_RACE IS 'Patient Additional Race'
/

COMMENT ON COLUMN VDA_V_PAT_DEMO_NOPHI.PTDEM_STATE IS 'Patient address-State'
/

COMMENT ON COLUMN VDA_V_PAT_DEMO_NOPHI.PTDEM_COUNTRY IS 'Patient address-Country'
/

COMMENT ON COLUMN VDA_V_PAT_DEMO_NOPHI.PTDEM_PRI_SITE IS 'Patient primary site or 
organization'
/

COMMENT ON COLUMN VDA_V_PAT_DEMO_NOPHI.FK_SITE IS 'The foreign key to primary site or 
organization record'
/

COMMENT ON COLUMN VDA_V_PAT_DEMO_NOPHI.PTDEM_EMPLOY IS 'Patient Employment status'
/

COMMENT ON COLUMN VDA_V_PAT_DEMO_NOPHI.PTDEM_EDUCATION IS 'Patient Education information'
/

COMMENT ON COLUMN VDA_V_PAT_DEMO_NOPHI.PTDEM_NOTES IS 'Notes for the patient'
/

COMMENT ON COLUMN VDA_V_PAT_DEMO_NOPHI.CREATOR IS 'The user who created the record 
(Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_DEMO_NOPHI.LAST_MODIFIED_BY IS 'The user who last modified the 
record(Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_DEMO_NOPHI.LAST_MODIFIED_DATE IS 'The date the record was last 
modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_DEMO_NOPHI.CREATED_ON IS 'The date the record was created on 
(Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_DEMO_NOPHI.PK_PERSON IS 'The primary key of the patient 
record'
/

COMMENT ON COLUMN VDA_V_PAT_DEMO_NOPHI.PTDEM_PHYOTHER IS 'Patient Physician-if other'
/

COMMENT ON COLUMN VDA_V_PAT_DEMO_NOPHI.PTDEM_SPLACCESS IS 'The groups/departments with 
access to edit this patient''s demographics. If left blank, all groups will have this 
right'
/

COMMENT ON COLUMN VDA_V_PAT_DEMO_NOPHI.PTDEM_REGBY IS 'Patient Registered By'
/

COMMENT ON COLUMN VDA_V_PAT_DEMO_NOPHI.PTDEM_REGDATE_YR IS 'Year of patient registration'
/

COMMENT ON COLUMN VDA_V_PAT_DEMO_NOPHI.FK_ACCOUNT IS 'The account the calendar is linked 
with'
/

COMMENT ON COLUMN VDA_V_PAT_DEMO_NOPHI.PERSON_REGBY_FK IS 'The key to Patient Registered 
By User'
/


--
-- VDA_V_PAT_ACCRUAL  (View) 
--
--  Dependencies: 
--   ER_STUDY (Table)
--   ER_SITE (Table)
--   F_GET_CODELSTDESC (Function)
--   ER_USER (Table)
--   ER_PATPROT (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_PAT_ACCRUAL
(PSTAT_PAT_STUD_ID, PSTAT_ASSIGNED_TO, PSTAT_PHYSICIAN, PSTAT_TREAT_LOCAT, PSTAT_EVAL_FLAG, 
 PSTAT_EVAL_STAT, PSTAT_INEVAL_STAT, PSTAT_SURVIVAL_STAT, PSTAT_RANDOM_NUM, PSTAT_ENROLLED_BY, 
 PSTAT_ENROLL_SITE, CREATOR, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, 
 FK_PER, FK_STUDY, PATPROT_ENROLDT, PATPROT_TREATINGORG, FK_SITE_ENROLLING, 
 STUDY_NUMBER, FK_ACCOUNT, PK_PATPROT, DMGRPH_REPORTABLE, PATPROT_DISEASE_CODE, 
 PATPROT_MORE_DIS_CODE1, PATPROT_MORE_DIS_CODE2, PATPROT_OTHR_DIS_CODE, CREATOR_FK, LAST_MODIFIED_BY_FK, 
 PSTAT_ENROLLED_BY_FK)
AS 
SELECT   PATPROT_PATSTDID PSTAT_PAT_STUD_ID,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = FK_USERASSTO)
               PSTAT_ASSIGNED_TO,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = PATPROT_PHYSICIAN)
               PSTAT_PHYSICIAN,
            ERES.F_GET_CODELSTDESC (FK_CODELSTLOC) PSTAT_TREAT_LOCAT,
            ERES.F_GET_CODELSTDESC (FK_CODELST_PTST_EVAL_FLAG)
               PSTAT_EVAL_FLAG,
            ERES.F_GET_CODELSTDESC (FK_CODELST_PTST_EVAL) PSTAT_EVAL_STAT,
            ERES.F_GET_CODELSTDESC (FK_CODELST_PTST_INEVAL) PSTAT_INEVAL_STAT,
            ERES.F_GET_CODELSTDESC (FK_CODELST_PTST_SURVIVAL) PSTAT_SURV_STAT,
            PATPROT_RANDOM PSTAT_RANDOM_NUM,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = pp.FK_USER)
               PSTAT_ENROLLED_BY,
            (SELECT   SITE_NAME
               FROM   eres.ER_SITE
              WHERE   PK_SITE = FK_SITE_ENROLLING)
               PSTAT_ENROLL_SITE,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = pp.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = pp.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            pp.LAST_MODIFIED_DATE,
            pp.CREATED_ON,
            fk_per,
            fk_study,
            patprot_enroldt,
            (SELECT   SITE_NAME
               FROM   eres.ER_SITE
              WHERE   PK_SITE = patprot_treatingorg)
               patprot_treatingorg,
            FK_SITE_ENROLLING,
            STUDY_NUMBER,
            FK_ACCOUNT,
            PK_PATPROT,
            DECODE (pp.DMGRPH_REPORTABLE, 1, 'Yes', 'No') DMGRPH_REPORTABLE,
            pp.PATPROT_DISEASE_CODE,
            pp.PATPROT_MORE_DIS_CODE1,
            pp.PATPROT_MORE_DIS_CODE2,
            pp.PATPROT_OTHR_DIS_CODE, pp.CREATOR as CREATOR_FK , pp.LAST_MODIFIED_BY as 
LAST_MODIFIED_BY_FK, pp.FK_USER as PSTAT_ENROLLED_BY_FK
     FROM   eres.ER_PATPROT pp, eres.er_study s
    WHERE       patprot_stat = 1
            AND patprot_enroldt IS NOT NULL
            AND s.pk_study = pp.fk_study
/

COMMENT ON TABLE VDA_V_PAT_ACCRUAL IS 'This view provides access to the patient accrual 
data for studies'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PSTAT_PAT_STUD_ID IS 'The patient study id'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PSTAT_ASSIGNED_TO IS 'The user patient is assigned to'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PSTAT_PHYSICIAN IS 'The physician patient is assigned 
to'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PSTAT_TREAT_LOCAT IS 'The treatment location'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PSTAT_EVAL_FLAG IS 'The evaluation flag'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PSTAT_EVAL_STAT IS 'The evaluation status'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PSTAT_INEVAL_STAT IS 'The inevaluation status'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PSTAT_SURVIVAL_STAT IS 'The survival status'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PSTAT_RANDOM_NUM IS 'The randomization number 
allocated to the patient'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PSTAT_ENROLLED_BY IS 'Patient Enrolled By'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PSTAT_ENROLL_SITE IS 'Patient Enrolling Site'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.LAST_MODIFIED_BY IS 'The user who last modified the 
record(Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.LAST_MODIFIED_DATE IS 'The date the record was last 
modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.CREATED_ON IS 'The date the record was created on 
(Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.FK_PER IS 'The foreign Key to the patient record'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.FK_STUDY IS 'The Foreign Key to the study milestone is 
linked with'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PATPROT_ENROLDT IS 'The enrollment date'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PATPROT_TREATINGORG IS 'The treating organization'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.FK_SITE_ENROLLING IS 'The foreign key to the enrolling 
site'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.STUDY_NUMBER IS 'The Study Number of the study the 
enrollment record  is linked with'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.FK_ACCOUNT IS 'The account the patient is linked with'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.DMGRPH_REPORTABLE IS 'Flag to indicate whether the 
patient demographics is CTRP reportable'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PATPROT_DISEASE_CODE IS 'Patient''s disease code 
value'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PATPROT_MORE_DIS_CODE1 IS 'Patient''s More disease 
code 1 value'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PATPROT_MORE_DIS_CODE2 IS 'Patient''s More disease 
code 2 value'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PATPROT_OTHR_DIS_CODE IS 'Patient''s Other disease 
code value'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.CREATOR_FK IS 'The Key to identify the Creator of the 
record.'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.LAST_MODIFIED_BY_FK IS 'The Key to identify the Last 
Modified By of the record.'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PSTAT_ENROLLED_BY_FK IS 'The Key to identify the 
Enrolled By User'
/


--
-- VDA_V_PATSCHEDULE_ALL  (View) 
--
--  Dependencies: 
--   ER_SITE (Table)
--   F_GET_DURUNIT (Function)
--   SCH_PROTOCOL_VISIT (Table)
--   EVENT_ASSOC (Table)
--   VDA_V_PAT_SCHEDULE (View)
--
CREATE OR REPLACE FORCE VIEW VDA_V_PATSCHEDULE_ALL
(PAT_STUDY_ID, FK_STUDY, CALENDAR_PK, PTSCH_VISIT, PTSCH_SUGSCH_DATE, 
 PTSCH_ACTSCH_DATE, PTSCH_EVENT_PK, PTSCH_EVENT, PTSCH_EVENT_STAT, SCH_EVENTS1_PK, 
 CREATOR, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, FK_PER, 
 EVENTSTAT_DATE, EVENTSTAT_NOTES, PATPROT_STAT, FK_SITE_ENROLLING, EVENT_SEQUENCE, 
 SERVICE_SITE_ID, FACILITY_ID, COVERAGE_TYPE, CALENDAR_NAME, PTSCH_VISIT_WIN, 
 EVENT_WINDOW_START, EVENT_WINDOW_END, VISIT_NAME, SITE_NAME, LAST_MODIFIED_BY_FK, 
 CREATOR_FK, EVENT_CPTCODE)
AS 
SELECT   PAT_STUDY_ID,
            FK_STUDY,
            CALENDAR_PK,
            PTSCH_VISIT,
            PTSCH_SUGSCH_DATE,
            PTSCH_ACTSCH_DATE,
            PTSCH_EVENT_PK,
            PTSCH_EVENT,
            PTSCH_EVENT_STAT,
            a.EVENT_ID sch_events1_pk,
            a.CREATOR,
            a.LAST_MODIFIED_BY,
            a.LAST_MODIFIED_DATE,
            a.CREATED_ON,
            FK_PER,
            EVENTSTAT_DATE,
            EVENTSTAT_NOTES,
            PATPROT_STAT,
            fk_site_enrolling,
            a.event_sequence,
            a.service_site_id,
            a.facility_id,
            coverage_type,
            cal.NAME AS calendar_name,
            (   NVL (ev.FUZZY_PERIOD, 0)
             || ' '
             || NVL (eres.F_Get_Durunit (ev.EVENT_DURATIONBEFORE), 'days')
             || ' Before / '
             || NVL (ev.EVENT_FUZZYAFTER, 0)
             || ' '
             || NVL (eres.F_Get_Durunit (ev.EVENT_DURATIONAFTER), 'days')
             || ' After')
               PTSCH_VISIT_WIN,
            NVL (PTSCH_ACTSCH_DATE
                 - DECODE (ev.event_durationbefore,
                           'D',
                           (ev.fuzzy_period * 1),
                           'H',
                           1,
                           'W',
                           (ev.fuzzy_period * 7),
                           'M',
                           (ev.fuzzy_period * 30)), PTSCH_ACTSCH_DATE)
               AS event_window_start,
            NVL (PTSCH_ACTSCH_DATE
                 + DECODE (ev.event_durationafter,
                           'D',
                           (ev.event_fuzzyafter * 1),
                           'H',
                           1,
                           'W',
                           (ev.event_fuzzyafter * 7),
                           'M',
                           (ev.event_fuzzyafter * 30)), PTSCH_ACTSCH_DATE)
               AS event_window_end,
            sch_protocol_visit.visit_name,
            site_name,  LAST_MODIFIED_BY_FK, CREATOR_FK ,
               a.EVENT_CPTCODE 
     FROM   VDA.VDA_V_PAT_SCHEDULE a,
            esch.event_assoc cal,
            esch.event_assoc ev,
            esch.sch_protocol_visit,
            eres.er_site s
    WHERE       cal.EVENT_ID = a.CALENDAR_PK
            AND ev.event_id = a.PTSCH_EVENT_PK
            AND PK_PROTOCOL_VISIT = a.PTSCH_VISIT
            AND s.pk_site = a.FK_SITE_ENROLLING
/

COMMENT ON TABLE VDA_V_PATSCHEDULE_ALL IS 'This view provides access to the patient 
schedules'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.PATPROT_STAT IS 'The flag to indicate if the 
schedule is current. 1 indicates current schedule, 0 indicates old or discontinued 
schedule'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.FK_SITE_ENROLLING IS 'The Primary Key of the 
enrolling site'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.EVENT_SEQUENCE IS 'The sequenceof the event in the 
visit'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.SERVICE_SITE_ID IS 'The site of service'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.FACILITY_ID IS 'The facility linked with the 
event'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.COVERAGE_TYPE IS 'The coverage type of the event'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.CALENDAR_NAME IS 'The Calendar name'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.PTSCH_VISIT_WIN IS 'The scheduled visit window per 
Calendar'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.EVENT_WINDOW_START IS 'The scheduled event 
window-start date'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.EVENT_WINDOW_END IS 'The scheduled event window-
end  date'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.VISIT_NAME IS 'The scheduled visit'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.SITE_NAME IS 'The patient enrolling site name'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.LAST_MODIFIED_BY_FK IS 'The Key to identify the 
Last Modified By of the record.'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.CREATOR_FK IS 'The Key to identify the Creator'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.EVENT_CPTCODE IS 'The CPT Code linked with the 
scheduled event'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.PAT_STUDY_ID IS 'The Patient Study ID'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.FK_STUDY IS 'The Primary Key of the study schedule 
is linked with'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.CALENDAR_PK IS 'The Primary Key of the calendar 
schedule is linked with'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.PTSCH_VISIT IS 'The Scheduled Visit Primary Key'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.PTSCH_SUGSCH_DATE IS 'The suggested date for the 
scheduled event'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.PTSCH_ACTSCH_DATE IS 'The actual date for the 
scheduled event'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.PTSCH_EVENT_PK IS 'The PK of the event'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.PTSCH_EVENT IS 'The name of the event'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.PTSCH_EVENT_STAT IS 'The event status'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.SCH_EVENTS1_PK IS 'The Primary Key of the 
scheduled event'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.CREATOR IS 'The user who created the record 
(Audit)'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.LAST_MODIFIED_BY IS 'The user who last modified 
the record(Audit)'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.LAST_MODIFIED_DATE IS 'The date the  record was 
last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.CREATED_ON IS 'The date the record was created on 
(Audit)'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.FK_PER IS 'The Primary Key of the patient the 
schedule is linked with'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.EVENTSTAT_DATE IS 'The date of the current event 
status'
/

COMMENT ON COLUMN VDA_V_PATSCHEDULE_ALL.EVENTSTAT_NOTES IS 'The notes linked with the 
current event status'
/


--
-- VDA_V_PATFORMS_AUDIT  (View) 
--
--  Dependencies: 
--   ER_FORMLIB (Table)
--   ER_FORMAUDITCOL (Table)
--   ER_PER (Table)
--   ER_PATPROT (Table)
--   ER_PATFORMS (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_PATFORMS_AUDIT
(PK_FORMAUDITCOL, FK_FILLEDFORM, FK_FORM, FORM_NAME, FA_SYSTEMID, 
 FA_DATETIME, FA_FLDNAME, FA_OLDVALUE, FA_NEWVALUE, FA_MODIFIEDBY_NAME, 
 FA_REASON, FK_PER, FK_PATPROT, PATIENT_ID, PATIENT_STUDY_ID, 
 FK_ACCOUNT)
AS 
SELECT   PK_FORMAUDITCOL,
            FK_FILLEDFORM,
            FK_FORM,
            form_name,
            FA_SYSTEMID,
            FA_DATETIME,
            FA_FLDNAME,
            FA_OLDVALUE,
            FA_NEWVALUE,
            FA_MODIFIEDBY_NAME,
            FA_REASON,
            fk_per,
            fk_patprot,
            per_code AS Patient_id,
            (SELECT   patprot_patstdid
               FROM   eres.er_patprot
              WHERE   pk_patprot = fk_patprot)
               Patient_study_id,
            b.fk_account
     FROM   eres.er_formauditcol,
            eres.er_formlib b,
            eres.er_patforms pf,
            eres.er_per p
    WHERE       FA_FORMTYPE = 'P'
            AND b.pk_formlib = FK_FORM
            AND pf.pk_patforms = FK_FILLEDFORM
            AND pf.fk_formlib = FK_FORM
            AND p.pk_per = fk_per
/

COMMENT ON TABLE VDA_V_PATFORMS_AUDIT IS 'This view provides access to the column update level Audit information of account level forms'
/

COMMENT ON COLUMN VDA_V_PATFORMS_AUDIT.PK_FORMAUDITCOL IS 'The Primary Key of the Audit Transaction'
/

COMMENT ON COLUMN VDA_V_PATFORMS_AUDIT.FK_FILLEDFORM IS 'The FK to the form response (account form)'
/

COMMENT ON COLUMN VDA_V_PATFORMS_AUDIT.FK_FORM IS 'The FK to the form'
/

COMMENT ON COLUMN VDA_V_PATFORMS_AUDIT.FORM_NAME IS 'The form name'
/

COMMENT ON COLUMN VDA_V_PATFORMS_AUDIT.FA_SYSTEMID IS 'The systemid of the field'
/

COMMENT ON COLUMN VDA_V_PATFORMS_AUDIT.FA_DATETIME IS 'The date-time of the transaction'
/

COMMENT ON COLUMN VDA_V_PATFORMS_AUDIT.FA_FLDNAME IS 'The user defined field name'
/

COMMENT ON COLUMN VDA_V_PATFORMS_AUDIT.FA_OLDVALUE IS 'The old value of the field'
/

COMMENT ON COLUMN VDA_V_PATFORMS_AUDIT.FA_NEWVALUE IS 'The new value of the field'
/

COMMENT ON COLUMN VDA_V_PATFORMS_AUDIT.FA_MODIFIEDBY_NAME IS 'The user who modified the form response data'
/

COMMENT ON COLUMN VDA_V_PATFORMS_AUDIT.FA_REASON IS 'The reason for change'
/

COMMENT ON COLUMN VDA_V_PATFORMS_AUDIT.FK_PER IS 'The FK of the Patient the form response is linked with'
/

COMMENT ON COLUMN VDA_V_PATFORMS_AUDIT.FK_PATPROT IS 'The FK of the Patient-study record the form response is linked with'
/

COMMENT ON COLUMN VDA_V_PATFORMS_AUDIT.PATIENT_ID IS 'The Patient ID or Patient Code of the patient the form response is linked with'
/

COMMENT ON COLUMN VDA_V_PATFORMS_AUDIT.PATIENT_STUDY_ID IS 'The Patient Study ID of the patient the form response is linked with'
/

COMMENT ON COLUMN VDA_V_PATFORMS_AUDIT.FK_ACCOUNT IS 'The account the form response is linked with'
/


--
-- VDA_V_PATFORMRESPONSES  (View) 
--
--  Dependencies: 
--   ER_FORMSLINEAR (Table)
--   ER_FORMLIB (Table)
--   F_GET_CODELSTDESC (Function)
--   ER_USER (Table)
--   VDA_V_PAT_ACCRUAL (View)
--
CREATE OR REPLACE FORCE VIEW VDA_V_PATFORMRESPONSES
(PK_FORMSLINEAR, FORM_NAME, FORM_DESC, EVENT_NAME, VISIT_NAME, 
 PROTOCOL_NAME, FORM_VERSION, FK_FORM, FILLDATE, FORM_TYPE, 
 PATIENT_PK, FK_PATPROT, COL1, COL2, COL3, 
 COL4, COL5, COL6, COL7, COL8, 
 COL9, COL10, COL11, COL12, COL13, 
 COL14, COL15, COL16, COL17, COL18, 
 COL19, COL20, COL21, COL22, COL23, 
 COL24, COL25, COL26, COL27, COL28, 
 COL29, COL30, COL31, COL32, COL33, 
 COL34, COL35, COL36, COL37, COL38, 
 COL39, COL40, COL41, COL42, COL43, 
 COL44, COL45, COL46, COL47, COL48, 
 COL49, COL50, COL51, COL52, COL53, 
 COL54, COL55, COL56, COL57, COL58, 
 COL59, COL60, COL61, COL62, COL63, 
 COL64, COL65, COL66, COL67, COL68, 
 COL69, COL70, COL71, COL72, COL73, 
 COL74, COL75, COL76, COL77, COL78, 
 COL79, COL80, COL81, COL82, COL83, 
 COL84, COL85, COL86, COL87, COL88, 
 COL89, COL90, COL91, COL92, COL93, 
 COL94, COL95, COL96, COL97, COL98, 
 COL99, COL100, COL101, COL102, COL103, 
 COL104, COL105, COL106, COL107, COL108, 
 COL109, COL110, COL111, COL112, COL113, 
 COL114, COL115, COL116, COL117, COL118, 
 COL119, COL120, COL121, COL122, COL123, 
 COL124, COL125, COL126, COL127, COL128, 
 COL129, COL130, COL131, COL132, COL133, 
 COL134, COL135, COL136, COL137, COL138, 
 COL139, COL140, COL141, COL142, COL143, 
 COL144, COL145, COL146, COL147, COL148, 
 COL149, COL150, COL151, COL152, COL153, 
 COL154, COL155, COL156, COL157, COL158, 
 COL159, COL160, COL161, COL162, COL163, 
 COL164, COL165, COL166, COL167, COL168, 
 COL169, COL170, COL171, COL172, COL173, 
 COL174, COL175, COL176, COL177, COL178, 
 COL179, COL180, COL181, COL182, COL183, 
 COL184, COL185, COL186, COL187, COL188, 
 COL189, COL190, COL191, COL192, COL193, 
 COL194, COL195, COL196, COL197, COL198, 
 COL199, COL200, COL201, COL202, COL203, 
 COL204, COL205, COL206, COL207, COL208, 
 COL209, COL210, COL211, COL212, COL213, 
 COL214, COL215, COL216, COL217, COL218, 
 COL219, COL220, COL221, COL222, COL223, 
 COL224, COL225, COL226, COL227, COL228, 
 COL229, COL230, COL231, COL232, COL233, 
 COL234, COL235, COL236, COL237, COL238, 
 COL239, COL240, COL241, COL242, COL243, 
 COL244, COL245, COL246, COL247, COL248, 
 COL249, COL250, COL251, COL252, COL253, 
 COL254, COL255, COL256, COL257, COL258, 
 COL259, COL260, COL261, COL262, COL263, 
 COL264, COL265, COL266, COL267, COL268, 
 COL269, COL270, COL271, COL272, COL273, 
 COL274, COL275, COL276, COL277, COL278, 
 COL279, COL280, COL281, COL282, COL283, 
 COL284, COL285, COL286, COL287, COL288, 
 COL289, COL290, COL291, COL292, COL293, 
 COL294, COL295, COL296, COL297, COL298, 
 COL299, COL300, COL301, COL302, COL303, 
 COL304, COL305, COL306, COL307, COL308, 
 COL309, COL310, COL311, COL312, COL313, 
 COL314, COL315, COL316, COL317, COL318, 
 COL319, COL320, COL321, COL322, COL323, 
 COL324, COL325, COL326, COL327, COL328, 
 COL329, COL330, COL331, COL332, COL333, 
 COL334, COL335, COL336, COL337, COL338, 
 COL339, COL340, COL341, COL342, COL343, 
 COL344, COL345, COL346, COL347, COL348, 
 COL349, COL350, COL351, COL352, COL353, 
 COL354, COL355, COL356, COL357, COL358, 
 COL359, COL360, COL361, COL362, COL363, 
 COL364, COL365, COL366, COL367, COL368, 
 COL369, COL370, COL371, COL372, COL373, 
 COL374, COL375, COL376, COL377, COL378, 
 COL379, COL380, COL381, COL382, COL383, 
 COL384, COL385, COL386, COL387, COL388, 
 COL389, COL390, COL391, COL392, COL393, 
 COL394, COL395, COL396, COL397, COL398, 
 COL399, COL400, COL401, COL402, COL403, 
 COL404, COL405, COL406, COL407, COL408, 
 COL409, COL410, COL411, COL412, COL413, 
 COL414, COL415, COL416, COL417, COL418, 
 COL419, COL420, COL421, COL422, COL423, 
 COL424, COL425, COL426, COL427, COL428, 
 COL429, COL430, COL431, COL432, COL433, 
 COL434, COL435, COL436, COL437, COL438, 
 COL439, COL440, COL441, COL442, COL443, 
 COL444, COL445, COL446, COL447, COL448, 
 COL449, COL450, COL451, COL452, COL453, 
 COL454, COL455, COL456, COL457, COL458, 
 COL459, COL460, COL461, COL462, COL463, 
 COL464, COL465, COL466, COL467, COL468, 
 COL469, COL470, COL471, COL472, COL473, 
 COL474, COL475, COL476, COL477, COL478, 
 COL479, COL480, COL481, COL482, COL483, 
 COL484, COL485, COL486, COL487, COL488, 
 COL489, COL490, COL491, COL492, COL493, 
 COL494, COL495, COL496, COL497, COL498, 
 COL499, COL500, COL501, COL502, COL503, 
 COL504, COL505, COL506, COL507, COL508, 
 COL509, COL510, COL511, COL512, COL513, 
 COL514, COL515, COL516, COL517, COL518, 
 COL519, COL520, COL521, COL522, COL523, 
 COL524, COL525, COL526, COL527, COL528, 
 COL529, COL530, COL531, COL532, COL533, 
 COL534, COL535, COL536, COL537, COL538, 
 COL539, COL540, COL541, COL542, COL543, 
 COL544, COL545, COL546, COL547, COL548, 
 COL549, COL550, COL551, COL552, COL553, 
 COL554, COL555, COL556, COL557, COL558, 
 COL559, COL560, COL561, COL562, COL563, 
 COL564, COL565, COL566, COL567, COL568, 
 COL569, COL570, COL571, COL572, COL573, 
 COL574, COL575, COL576, COL577, COL578, 
 COL579, COL580, COL581, COL582, COL583, 
 COL584, COL585, COL586, COL587, COL588, 
 COL589, COL590, COL591, COL592, COL593, 
 COL594, COL595, COL596, COL597, COL598, 
 COL599, COL600, COL601, COL602, COL603, 
 COL604, COL605, COL606, COL607, COL608, 
 COL609, COL610, COL611, COL612, COL613, 
 COL614, COL615, COL616, COL617, COL618, 
 COL619, COL620, COL621, COL622, COL623, 
 COL624, COL625, COL626, COL627, COL628, 
 COL629, COL630, COL631, COL632, COL633, 
 COL634, COL635, COL636, COL637, COL638, 
 COL639, COL640, COL641, COL642, COL643, 
 COL644, COL645, COL646, COL647, COL648, 
 COL649, COL650, COL651, COL652, COL653, 
 COL654, COL655, COL656, COL657, COL658, 
 COL659, COL660, COL661, COL662, COL663, 
 COL664, COL665, COL666, COL667, COL668, 
 COL669, COL670, COL671, COL672, COL673, 
 COL674, COL675, COL676, COL677, COL678, 
 COL679, COL680, COL681, COL682, COL683, 
 COL684, COL685, COL686, COL687, COL688, 
 COL689, COL690, COL691, COL692, COL693, 
 COL694, COL695, COL696, COL697, COL698, 
 COL699, COL700, COL701, COL702, COL703, 
 COL704, COL705, COL706, COL707, COL708, 
 COL709, COL710, COL711, COL712, COL713, 
 COL714, COL715, COL716, COL717, COL718, 
 COL719, COL720, COL721, COL722, COL723, 
 COL724, COL725, COL726, COL727, COL728, 
 COL729, COL730, COL731, COL732, COL733, 
 COL734, COL735, COL736, COL737, COL738, 
 COL739, COL740, COL741, COL742, COL743, 
 COL744, COL745, COL746, COL747, COL748, 
 COL749, COL750, COL751, COL752, COL753, 
 COL754, COL755, COL756, COL757, COL758, 
 COL759, COL760, COL761, COL762, COL763, 
 COL764, COL765, COL766, COL767, COL768, 
 COL769, COL770, COL771, COL772, COL773, 
 COL774, COL775, COL776, COL777, COL778, 
 COL779, COL780, COL781, COL782, COL783, 
 COL784, COL785, COL786, COL787, COL788, 
 COL789, COL790, COL791, COL792, COL793, 
 COL794, COL795, COL796, COL797, COL798, 
 COL799, COL800, COL801, COL802, COL803, 
 COL804, COL805, COL806, COL807, COL808, 
 COL809, COL810, COL811, COL812, COL813, 
 COL814, COL815, COL816, COL817, COL818, 
 COL819, COL820, COL821, COL822, COL823, 
 COL824, COL825, COL826, COL827, COL828, 
 COL829, COL830, COL831, COL832, COL833, 
 COL834, COL835, COL836, COL837, COL838, 
 COL839, COL840, COL841, COL842, COL843, 
 COL844, COL845, COL846, COL847, COL848, 
 COL849, COL850, COL851, COL852, COL853, 
 COL854, COL855, COL856, COL857, COL858, 
 COL859, COL860, COL861, COL862, COL863, 
 COL864, COL865, COL866, COL867, COL868, 
 COL869, COL870, COL871, COL872, COL873, 
 COL874, COL875, COL876, COL877, COL878, 
 COL879, COL880, COL881, COL882, COL883, 
 COL884, COL885, COL886, COL887, COL888, 
 COL889, COL890, COL891, COL892, COL893, 
 COL894, COL895, COL896, COL897, COL898, 
 COL899, COL900, COL901, COL902, COL903, 
 COL904, COL905, COL906, COL907, COL908, 
 COL909, COL910, COL911, COL912, COL913, 
 COL914, COL915, COL916, COL917, COL918, 
 COL919, COL920, COL921, COL922, COL923, 
 COL924, COL925, COL926, COL927, COL928, 
 COL929, COL930, COL931, COL932, COL933, 
 COL934, COL935, COL936, COL937, COL938, 
 COL939, COL940, COL941, COL942, COL943, 
 COL944, COL945, COL946, COL947, COL948, 
 COL949, COL950, CREATOR, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, 
 CREATED_ON, FK_SCH_EVENTS1, FK_SPECIMEN, FK_ACCOUNT, STUDY_NUMBER, 
 LAST_MODIFIED_BY_FK, CREATOR_FK, FK_STUDY)
AS 
SELECT   PK_FORMSLINEAR,
            form_name,
            form_desc,
            event_name,
            visit_name,
            protocol_name,
            form_version,
            fk_form,
            filldate,
            form_type,
            id AS patient_pk,
            fk_patprot,
            COL1,
            COL2,
            COL3,
            COL4,
            COL5,
            COL6,
            COL7,
            COL8,
            COL9,
            COL10,
            COL11,
            COL12,
            COL13,
            COL14,
            COL15,
            COL16,
            COL17,
            COL18,
            COL19,
            COL20,
            COL21,
            COL22,
            COL23,
            COL24,
            COL25,
            COL26,
            COL27,
            COL28,
            COL29,
            COL30,
            COL31,
            COL32,
            COL33,
            COL34,
            COL35,
            COL36,
            COL37,
            COL38,
            COL39,
            COL40,
            COL41,
            COL42,
            COL43,
            COL44,
            COL45,
            COL46,
            COL47,
            COL48,
            COL49,
            COL50,
            COL51,
            COL52,
            COL53,
            COL54,
            COL55,
            COL56,
            COL57,
            COL58,
            COL59,
            COL60,
            COL61,
            COL62,
            COL63,
            COL64,
            COL65,
            COL66,
            COL67,
            COL68,
            COL69,
            COL70,
            COL71,
            COL72,
            COL73,
            COL74,
            COL75,
            COL76,
            COL77,
            COL78,
            COL79,
            COL80,
            COL81,
            COL82,
            COL83,
            COL84,
            COL85,
            COL86,
            COL87,
            COL88,
            COL89,
            COL90,
            COL91,
            COL92,
            COL93,
            COL94,
            COL95,
            COL96,
            COL97,
            COL98,
            COL99,
            COL100,
            COL101,
            COL102,
            COL103,
            COL104,
            COL105,
            COL106,
            COL107,
            COL108,
            COL109,
            COL110,
            COL111,
            COL112,
            COL113,
            COL114,
            COL115,
            COL116,
            COL117,
            COL118,
            COL119,
            COL120,
            COL121,
            COL122,
            COL123,
            COL124,
            COL125,
            COL126,
            COL127,
            COL128,
            COL129,
            COL130,
            COL131,
            COL132,
            COL133,
            COL134,
            COL135,
            COL136,
            COL137,
            COL138,
            COL139,
            COL140,
            COL141,
            COL142,
            COL143,
            COL144,
            COL145,
            COL146,
            COL147,
            COL148,
            COL149,
            COL150,
            COL151,
            COL152,
            COL153,
            COL154,
            COL155,
            COL156,
            COL157,
            COL158,
            COL159,
            COL160,
            COL161,
            COL162,
            COL163,
            COL164,
            COL165,
            COL166,
            COL167,
            COL168,
            COL169,
            COL170,
            COL171,
            COL172,
            COL173,
            COL174,
            COL175,
            COL176,
            COL177,
            COL178,
            COL179,
            COL180,
            COL181,
            COL182,
            COL183,
            COL184,
            COL185,
            COL186,
            COL187,
            COL188,
            COL189,
            COL190,
            COL191,
            COL192,
            COL193,
            COL194,
            COL195,
            COL196,
            COL197,
            COL198,
            COL199,
            COL200,
            COL201,
            COL202,
            COL203,
            COL204,
            COL205,
            COL206,
            COL207,
            COL208,
            COL209,
            COL210,
            COL211,
            COL212,
            COL213,
            COL214,
            COL215,
            COL216,
            COL217,
            COL218,
            COL219,
            COL220,
            COL221,
            COL222,
            COL223,
            COL224,
            COL225,
            COL226,
            COL227,
            COL228,
            COL229,
            COL230,
            COL231,
            COL232,
            COL233,
            COL234,
            COL235,
            COL236,
            COL237,
            COL238,
            COL239,
            COL240,
            COL241,
            COL242,
            COL243,
            COL244,
            COL245,
            COL246,
            COL247,
            COL248,
            COL249,
            COL250,
            COL251,
            COL252,
            COL253,
            COL254,
            COL255,
            COL256,
            COL257,
            COL258,
            COL259,
            COL260,
            COL261,
            COL262,
            COL263,
            COL264,
            COL265,
            COL266,
            COL267,
            COL268,
            COL269,
            COL270,
            COL271,
            COL272,
            COL273,
            COL274,
            COL275,
            COL276,
            COL277,
            COL278,
            COL279,
            COL280,
            COL281,
            COL282,
            COL283,
            COL284,
            COL285,
            COL286,
            COL287,
            COL288,
            COL289,
            COL290,
            COL291,
            COL292,
            COL293,
            COL294,
            COL295,
            COL296,
            COL297,
            COL298,
            COL299,
            COL300,
            COL301,
            COL302,
            COL303,
            COL304,
            COL305,
            COL306,
            COL307,
            COL308,
            COL309,
            COL310,
            COL311,
            COL312,
            COL313,
            COL314,
            COL315,
            COL316,
            COL317,
            COL318,
            COL319,
            COL320,
            COL321,
            COL322,
            COL323,
            COL324,
            COL325,
            COL326,
            COL327,
            COL328,
            COL329,
            COL330,
            COL331,
            COL332,
            COL333,
            COL334,
            COL335,
            COL336,
            COL337,
            COL338,
            COL339,
            COL340,
            COL341,
            COL342,
            COL343,
            COL344,
            COL345,
            COL346,
            COL347,
            COL348,
            COL349,
            COL350,
            COL351,
            COL352,
            COL353,
            COL354,
            COL355,
            COL356,
            COL357,
            COL358,
            COL359,
            COL360,
            COL361,
            COL362,
            COL363,
            COL364,
            COL365,
            COL366,
            COL367,
            COL368,
            COL369,
            COL370,
            COL371,
            COL372,
            COL373,
            COL374,
            COL375,
            COL376,
            COL377,
            COL378,
            COL379,
            COL380,
            COL381,
            COL382,
            COL383,
            COL384,
            COL385,
            COL386,
            COL387,
            COL388,
            COL389,
            COL390,
            COL391,
            COL392,
            COL393,
            COL394,
            COL395,
            COL396,
            COL397,
            COL398,
            COL399,
            COL400,
            COL401,
            COL402,
            COL403,
            COL404,
            COL405,
            COL406,
            COL407,
            COL408,
            COL409,
            COL410,
            COL411,
            COL412,
            COL413,
            COL414,
            COL415,
            COL416,
            COL417,
            COL418,
            COL419,
            COL420,
            COL421,
            COL422,
            COL423,
            COL424,
            COL425,
            COL426,
            COL427,
            COL428,
            COL429,
            COL430,
            COL431,
            COL432,
            COL433,
            COL434,
            COL435,
            COL436,
            COL437,
            COL438,
            COL439,
            COL440,
            COL441,
            COL442,
            COL443,
            COL444,
            COL445,
            COL446,
            COL447,
            COL448,
            COL449,
            COL450,
            COL451,
            COL452,
            COL453,
            COL454,
            COL455,
            COL456,
            COL457,
            COL458,
            COL459,
            COL460,
            COL461,
            COL462,
            COL463,
            COL464,
            COL465,
            COL466,
            COL467,
            COL468,
            COL469,
            COL470,
            COL471,
            COL472,
            COL473,
            COL474,
            COL475,
            COL476,
            COL477,
            COL478,
            COL479,
            COL480,
            COL481,
            COL482,
            COL483,
            COL484,
            COL485,
            COL486,
            COL487,
            COL488,
            COL489,
            COL490,
            COL491,
            COL492,
            COL493,
            COL494,
            COL495,
            COL496,
            COL497,
            COL498,
            COL499,
            COL500,
            COL501,
            COL502,
            COL503,
            COL504,
            COL505,
            COL506,
            COL507,
            COL508,
            COL509,
            COL510,
            COL511,
            COL512,
            COL513,
            COL514,
            COL515,
            COL516,
            COL517,
            COL518,
            COL519,
            COL520,
            COL521,
            COL522,
            COL523,
            COL524,
            COL525,
            COL526,
            COL527,
            COL528,
            COL529,
            COL530,
            COL531,
            COL532,
            COL533,
            COL534,
            COL535,
            COL536,
            COL537,
            COL538,
            COL539,
            COL540,
            COL541,
            COL542,
            COL543,
            COL544,
            COL545,
            COL546,
            COL547,
            COL548,
            COL549,
            COL550,
            COL551,
            COL552,
            COL553,
            COL554,
            COL555,
            COL556,
            COL557,
            COL558,
            COL559,
            COL560,
            COL561,
            COL562,
            COL563,
            COL564,
            COL565,
            COL566,
            COL567,
            COL568,
            COL569,
            COL570,
            COL571,
            COL572,
            COL573,
            COL574,
            COL575,
            COL576,
            COL577,
            COL578,
            COL579,
            COL580,
            COL581,
            COL582,
            COL583,
            COL584,
            COL585,
            COL586,
            COL587,
            COL588,
            COL589,
            COL590,
            COL591,
            COL592,
            COL593,
            COL594,
            COL595,
            COL596,
            COL597,
            COL598,
            COL599,
            COL600,
            COL601,
            COL602,
            COL603,
            COL604,
            COL605,
            COL606,
            COL607,
            COL608,
            COL609,
            COL610,
            COL611,
            COL612,
            COL613,
            COL614,
            COL615,
            COL616,
            COL617,
            COL618,
            COL619,
            COL620,
            COL621,
            COL622,
            COL623,
            COL624,
            COL625,
            COL626,
            COL627,
            COL628,
            COL629,
            COL630,
            COL631,
            COL632,
            COL633,
            COL634,
            COL635,
            COL636,
            COL637,
            COL638,
            COL639,
            COL640,
            COL641,
            COL642,
            COL643,
            COL644,
            COL645,
            COL646,
            COL647,
            COL648,
            COL649,
            COL650,
            COL651,
            COL652,
            COL653,
            COL654,
            COL655,
            COL656,
            COL657,
            COL658,
            COL659,
            COL660,
            COL661,
            COL662,
            COL663,
            COL664,
            COL665,
            COL666,
            COL667,
            COL668,
            COL669,
            COL670,
            COL671,
            COL672,
            COL673,
            COL674,
            COL675,
            COL676,
            COL677,
            COL678,
            COL679,
            COL680,
            COL681,
            COL682,
            COL683,
            COL684,
            COL685,
            COL686,
            COL687,
            COL688,
            COL689,
            COL690,
            COL691,
            COL692,
            COL693,
            COL694,
            COL695,
            COL696,
            COL697,
            COL698,
            COL699,
            COL700,
            COL701,
            COL702,
            COL703,
            COL704,
            COL705,
            COL706,
            COL707,
            COL708,
            COL709,
            COL710,
            COL711,
            COL712,
            COL713,
            COL714,
            COL715,
            COL716,
            COL717,
            COL718,
            COL719,
            COL720,
            COL721,
            COL722,
            COL723,
            COL724,
            COL725,
            COL726,
            COL727,
            COL728,
            COL729,
            COL730,
            COL731,
            COL732,
            COL733,
            COL734,
            COL735,
            COL736,
            COL737,
            COL738,
            COL739,
            COL740,
            COL741,
            COL742,
            COL743,
            COL744,
            COL745,
            COL746,
            COL747,
            COL748,
            COL749,
            COL750,
            COL751,
            COL752,
            COL753,
            COL754,
            COL755,
            COL756,
            COL757,
            COL758,
            COL759,
            COL760,
            COL761,
            COL762,
            COL763,
            COL764,
            COL765,
            COL766,
            COL767,
            COL768,
            COL769,
            COL770,
            COL771,
            COL772,
            COL773,
            COL774,
            COL775,
            COL776,
            COL777,
            COL778,
            COL779,
            COL780,
            COL781,
            COL782,
            COL783,
            COL784,
            COL785,
            COL786,
            COL787,
            COL788,
            COL789,
            COL790,
            COL791,
            COL792,
            COL793,
            COL794,
            COL795,
            COL796,
            COL797,
            COL798,
            COL799,
            COL800,
            COL801,
            COL802,
            COL803,
            COL804,
            COL805,
            COL806,
            COL807,
            COL808,
            COL809,
            COL810,
            COL811,
            COL812,
            COL813,
            COL814,
            COL815,
            COL816,
            COL817,
            COL818,
            COL819,
            COL820,
            COL821,
            COL822,
            COL823,
            COL824,
            COL825,
            COL826,
            COL827,
            COL828,
            COL829,
            COL830,
            COL831,
            COL832,
            COL833,
            COL834,
            COL835,
            COL836,
            COL837,
            COL838,
            COL839,
            COL840,
            COL841,
            COL842,
            COL843,
            COL844,
            COL845,
            COL846,
            COL847,
            COL848,
            COL849,
            COL850,
            COL851,
            COL852,
            COL853,
            COL854,
            COL855,
            COL856,
            COL857,
            COL858,
            COL859,
            COL860,
            COL861,
            COL862,
            COL863,
            COL864,
            COL865,
            COL866,
            COL867,
            COL868,
            COL869,
            COL870,
            COL871,
            COL872,
            COL873,
            COL874,
            COL875,
            COL876,
            COL877,
            COL878,
            COL879,
            COL880,
            COL881,
            COL882,
            COL883,
            COL884,
            COL885,
            COL886,
            COL887,
            COL888,
            COL889,
            COL890,
            COL891,
            COL892,
            COL893,
            COL894,
            COL895,
            COL896,
            COL897,
            COL898,
            COL899,
            COL900,
            COL901,
            COL902,
            COL903,
            COL904,
            COL905,
            COL906,
            COL907,
            COL908,
            COL909,
            COL910,
            COL911,
            COL912,
            COL913,
            COL914,
            COL915,
            COL916,
            COL917,
            COL918,
            COL919,
            COL920,
            COL921,
            COL922,
            COL923,
            COL924,
            COL925,
            COL926,
            COL927,
            COL928,
            COL929,
            COL930,
            COL931,
            COL932,
            COL933,
            COL934,
            COL935,
            COL936,
            COL937,
            COL938,
            COL939,
            COL940,
            COL941,
            COL942,
            COL943,
            COL944,
            COL945,
            COL946,
            COL947,
            COL948,
            COL949,
            COL950,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = l.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = l.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            l.LAST_MODIFIED_DATE,
            l.CREATED_ON,
            FK_SCH_EVENTS1,
            FK_SPECIMEN,
            eres.er_formlib.fk_account,
            (SELECT   study_number
               FROM   vda.VDA_V_PAT_ACCRUAL
              WHERE   pk_patprot = FK_PATPROT)
               STUDY_NUMBER, l.LAST_MODIFIED_BY as LAST_MODIFIED_BY_FK,
   l.CREATOR as CREATOR_FK, (SELECT   FK_STUDY
               FROM   vda.VDA_V_PAT_ACCRUAL
              WHERE   pk_patprot = FK_PATPROT) FK_STUDY
     FROM   eres.er_formslinear l, eres.er_formlib
    WHERE   form_type = 'P' AND fk_form = pk_formlib
/

COMMENT ON TABLE VDA_V_PATFORMRESPONSES IS 'This view provides access to responses to all 
study level forms'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL850 IS 'Col850 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL851 IS 'Col851 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL852 IS 'Col852 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL853 IS 'Col853 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL854 IS 'Col854 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL855 IS 'Col855 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL856 IS 'Col856 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL857 IS 'Col857 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL858 IS 'Col858 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL859 IS 'Col859 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL860 IS 'Col860 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL861 IS 'Col861 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL862 IS 'Col862 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL863 IS 'Col863 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL864 IS 'Col864 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL865 IS 'Col865 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL866 IS 'Col866 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL867 IS 'Col867 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL868 IS 'Col868 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL869 IS 'Col869 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL870 IS 'Col870 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL871 IS 'Col871 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL872 IS 'Col872 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL873 IS 'Col873 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL874 IS 'Col874 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL875 IS 'Col875 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL876 IS 'Col876 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL877 IS 'Col877 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL878 IS 'Col878 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL879 IS 'Col879 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL880 IS 'Col880 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL881 IS 'Col881 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL882 IS 'Col882 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL883 IS 'Col883 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL884 IS 'Col884 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL885 IS 'Col885 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL886 IS 'Col886 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL887 IS 'Col887 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL888 IS 'Col888 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL889 IS 'Col889 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL890 IS 'Col890 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL891 IS 'Col891 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL892 IS 'Col892 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL893 IS 'Col893 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL894 IS 'Col894 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL895 IS 'Col895 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL896 IS 'Col896 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL897 IS 'Col897 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL898 IS 'Col898 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL899 IS 'Col899 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL900 IS 'Col900 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL901 IS 'Col901 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL902 IS 'Col902 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL903 IS 'Col903 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL904 IS 'Col904 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL905 IS 'Col905 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL906 IS 'Col906 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL907 IS 'Col907 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL908 IS 'Col908 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL909 IS 'Col909 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL910 IS 'Col910 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL911 IS 'Col911 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL912 IS 'Col912 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL913 IS 'Col913 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL914 IS 'Col914 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL915 IS 'Col915 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL916 IS 'Col916 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL917 IS 'Col917 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL918 IS 'Col918 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL919 IS 'Col919 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL920 IS 'Col920 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL921 IS 'Col921 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL922 IS 'Col922 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL923 IS 'Col923 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL924 IS 'Col924 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL925 IS 'Col925 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL926 IS 'Col926 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL927 IS 'Col927 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL928 IS 'Col928 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL929 IS 'Col929 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL930 IS 'Col930 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL931 IS 'Col931 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL932 IS 'Col932 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL933 IS 'Col933 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL934 IS 'Col934 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL935 IS 'Col935 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL936 IS 'Col936 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL937 IS 'Col937 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL938 IS 'Col938 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL939 IS 'Col939 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL940 IS 'Col940 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL941 IS 'Col941 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL942 IS 'Col942 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL943 IS 'Col943 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL944 IS 'Col944 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL945 IS 'Col945 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL946 IS 'Col946 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL947 IS 'Col947 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL948 IS 'Col948 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL949 IS 'Col949 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL950 IS 'Col950 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.CREATOR IS 'User who created record'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.LAST_MODIFIED_BY IS 'User who last modified the 
record'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.LAST_MODIFIED_DATE IS 'Last modified date'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.CREATED_ON IS 'Date record was created on'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.FK_SCH_EVENTS1 IS 'ID to identify the patient 
scheduled event record the response is linked with'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.FK_SPECIMEN IS 'ID to identify the specimen a 
response is linked with'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.FK_ACCOUNT IS 'The account the response is linked 
with. Applicable to Velos hosted or shared environments'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.STUDY_NUMBER IS 'The Study the form response is 
linked with.'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.LAST_MODIFIED_BY_FK IS 'The Key to identify the 
Last Modified By of the record.'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.CREATOR_FK IS 'The Key to identify the Creator of 
the record.'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.FK_STUDY IS 'The Key to identify the study linked 
with the form response'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.PK_FORMSLINEAR IS 'The Primary Key to identify 
the form response.'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.FORM_NAME IS 'Form name'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.FORM_DESC IS 'Form Description'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.EVENT_NAME IS 'The name of the calendar event the 
response is linked with'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.VISIT_NAME IS 'The name of the calendar visit the 
response is linked with'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.PROTOCOL_NAME IS 'The name of the calendar the 
response is linked with'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.FORM_VERSION IS 'Form version number'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.FK_FORM IS 'ID to identify a form record 
uniquely'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.FILLDATE IS 'Date the response was filled on'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.FORM_TYPE IS 'Type of the  form- always A for 
account level forms'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.PATIENT_PK IS 'ID to identify the patient the 
response is linked with'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.FK_PATPROT IS 'ID to identify the patient-study 
record the response is linked with'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL1 IS 'Col1 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL2 IS 'Col2 data of the form response. The view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL3 IS 'Col3 data of the form response. The view 
VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL4 IS 'Col4 data of the form response. The view 
VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL5 IS 'Col5 data of the form response. The view 
VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for 
the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL6 IS 'Col6 data of the form response. The view 
VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for 
the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL7 IS 'Col7 data of the form response. The view 
VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for 
the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL8 IS 'Col8 data of the form response. The view 
VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for 
the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL9 IS 'Col9 data of the form response. The view 
VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column for 
the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL10 IS 'Col10 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL11 IS 'Col11 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL12 IS 'Col12 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL13 IS 'Col13 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL14 IS 'Col14 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL15 IS 'Col15 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL16 IS 'Col16 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL17 IS 'Col17 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL18 IS 'Col18 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL19 IS 'Col19 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL20 IS 'Col20 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL21 IS 'Col21 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL22 IS 'Col22 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL23 IS 'Col23 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL24 IS 'Col24 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL25 IS 'Col25 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL26 IS 'Col26 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL27 IS 'Col27 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL28 IS 'Col28 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL29 IS 'Col29 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL30 IS 'Col30 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL31 IS 'Col31 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL32 IS 'Col32 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL33 IS 'Col33 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL34 IS 'Col34 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL35 IS 'Col35 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL36 IS 'Col36 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL37 IS 'Col37 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL38 IS 'Col38 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL39 IS 'Col39 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL40 IS 'Col40 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL41 IS 'Col41 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL42 IS 'Col42 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL43 IS 'Col43 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL44 IS 'Col44 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL45 IS 'Col45 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL46 IS 'Col46 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL47 IS 'Col47 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL48 IS 'Col48 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL49 IS 'Col49 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL50 IS 'Col50 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL51 IS 'Col51 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL52 IS 'Col52 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL53 IS 'Col53 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL54 IS 'Col54 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL55 IS 'Col55 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL56 IS 'Col56 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL57 IS 'Col57 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL58 IS 'Col58 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL59 IS 'Col59 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL60 IS 'Col60 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL61 IS 'Col61 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL62 IS 'Col62 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL63 IS 'Col63 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL64 IS 'Col64 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL65 IS 'Col65 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL66 IS 'Col66 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL67 IS 'Col67 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL68 IS 'Col68 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL69 IS 'Col69 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL70 IS 'Col70 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL71 IS 'Col71 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL72 IS 'Col72 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL73 IS 'Col73 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL74 IS 'Col74 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL75 IS 'Col75 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL76 IS 'Col76 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL77 IS 'Col77 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL78 IS 'Col78 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL79 IS 'Col79 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL80 IS 'Col80 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL81 IS 'Col81 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL82 IS 'Col82 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL83 IS 'Col83 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL84 IS 'Col84 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL85 IS 'Col85 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL86 IS 'Col86 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL87 IS 'Col87 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL88 IS 'Col88 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL89 IS 'Col89 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL90 IS 'Col90 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL91 IS 'Col91 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL92 IS 'Col92 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL93 IS 'Col93 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL94 IS 'Col94 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL95 IS 'Col95 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL96 IS 'Col96 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL97 IS 'Col97 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL98 IS 'Col98 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL99 IS 'Col99 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL100 IS 'Col100 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL101 IS 'Col101 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL102 IS 'Col102 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL103 IS 'Col103 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL104 IS 'Col104 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL105 IS 'Col105 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL106 IS 'Col106 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL107 IS 'Col107 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL108 IS 'Col108 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL109 IS 'Col109 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL110 IS 'Col110 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL111 IS 'Col111 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL112 IS 'Col112 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL113 IS 'Col113 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL114 IS 'Col114 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL115 IS 'Col115 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL116 IS 'Col116 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL117 IS 'Col117 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL118 IS 'Col118 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL119 IS 'Col119 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL120 IS 'Col120 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL121 IS 'Col121 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL122 IS 'Col122 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL123 IS 'Col123 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL124 IS 'Col124 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL125 IS 'Col125 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL126 IS 'Col126 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL127 IS 'Col127 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL128 IS 'Col128 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL129 IS 'Col129 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL130 IS 'Col130 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL131 IS 'Col131 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL132 IS 'Col132 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL133 IS 'Col133 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL134 IS 'Col134 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL135 IS 'Col135 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL136 IS 'Col136 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL137 IS 'Col137 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL138 IS 'Col138 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL139 IS 'Col139 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL140 IS 'Col140 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL141 IS 'Col141 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL142 IS 'Col142 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL143 IS 'Col143 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL144 IS 'Col144 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL145 IS 'Col145 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL146 IS 'Col146 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL147 IS 'Col147 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL148 IS 'Col148 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL149 IS 'Col149 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL150 IS 'Col150 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL151 IS 'Col151 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL152 IS 'Col152 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL153 IS 'Col153 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL154 IS 'Col154 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL155 IS 'Col155 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL156 IS 'Col156 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL157 IS 'Col157 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL158 IS 'Col158 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL159 IS 'Col159 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL160 IS 'Col160 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL161 IS 'Col161 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL162 IS 'Col162 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL163 IS 'Col163 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL164 IS 'Col164 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL165 IS 'Col165 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL166 IS 'Col166 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL167 IS 'Col167 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL168 IS 'Col168 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL169 IS 'Col169 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL170 IS 'Col170 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL171 IS 'Col171 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL172 IS 'Col172 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL173 IS 'Col173 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL174 IS 'Col174 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL175 IS 'Col175 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL176 IS 'Col176 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL177 IS 'Col177 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL178 IS 'Col178 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL179 IS 'Col179 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL180 IS 'Col180 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL181 IS 'Col181 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL182 IS 'Col182 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL183 IS 'Col183 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL184 IS 'Col184 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL185 IS 'Col185 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL186 IS 'Col186 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL187 IS 'Col187 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL188 IS 'Col188 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL189 IS 'Col189 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL190 IS 'Col190 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL191 IS 'Col191 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL192 IS 'Col192 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL193 IS 'Col193 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL194 IS 'Col194 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL195 IS 'Col195 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL196 IS 'Col196 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL197 IS 'Col197 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL198 IS 'Col198 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL199 IS 'Col199 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL200 IS 'Col200 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL201 IS 'Col201 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL202 IS 'Col202 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL203 IS 'Col203 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL204 IS 'Col204 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL205 IS 'Col205 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL206 IS 'Col206 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL207 IS 'Col207 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL208 IS 'Col208 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL209 IS 'Col209 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL210 IS 'Col210 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL211 IS 'Col211 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL212 IS 'Col212 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL213 IS 'Col213 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL214 IS 'Col214 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL215 IS 'Col215 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL216 IS 'Col216 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL217 IS 'Col217 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL218 IS 'Col218 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL219 IS 'Col219 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL220 IS 'Col220 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL221 IS 'Col221 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL222 IS 'Col222 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL223 IS 'Col223 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL224 IS 'Col224 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL225 IS 'Col225 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL226 IS 'Col226 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL227 IS 'Col227 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL228 IS 'Col228 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL229 IS 'Col229 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL230 IS 'Col230 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL231 IS 'Col231 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL232 IS 'Col232 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL233 IS 'Col233 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL234 IS 'Col234 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL235 IS 'Col235 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL236 IS 'Col236 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL237 IS 'Col237 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL238 IS 'Col238 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL239 IS 'Col239 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL240 IS 'Col240 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL241 IS 'Col241 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL242 IS 'Col242 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL243 IS 'Col243 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL244 IS 'Col244 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL245 IS 'Col245 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL246 IS 'Col246 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL247 IS 'Col247 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL248 IS 'Col248 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL249 IS 'Col249 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL250 IS 'Col250 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL251 IS 'Col251 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL252 IS 'Col252 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL253 IS 'Col253 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL254 IS 'Col254 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL255 IS 'Col255 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL256 IS 'Col256 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL257 IS 'Col257 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL258 IS 'Col258 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL259 IS 'Col259 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL260 IS 'Col260 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL261 IS 'Col261 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL262 IS 'Col262 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL263 IS 'Col263 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL264 IS 'Col264 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL265 IS 'Col265 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL266 IS 'Col266 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL267 IS 'Col267 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL268 IS 'Col268 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL269 IS 'Col269 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL270 IS 'Col270 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL271 IS 'Col271 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL272 IS 'Col272 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL273 IS 'Col273 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL274 IS 'Col274 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL275 IS 'Col275 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL276 IS 'Col276 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL277 IS 'Col277 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL278 IS 'Col278 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL279 IS 'Col279 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL280 IS 'Col280 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL281 IS 'Col281 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL282 IS 'Col282 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL283 IS 'Col283 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL284 IS 'Col284 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL285 IS 'Col285 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL286 IS 'Col286 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL287 IS 'Col287 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL288 IS 'Col288 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL289 IS 'Col289 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL290 IS 'Col290 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL291 IS 'Col291 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL292 IS 'Col292 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL293 IS 'Col293 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL294 IS 'Col294 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL295 IS 'Col295 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL296 IS 'Col296 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL297 IS 'Col297 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL298 IS 'Col298 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL299 IS 'Col299 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL300 IS 'Col300 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL301 IS 'Col301 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL302 IS 'Col302 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL303 IS 'Col303 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL304 IS 'Col304 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL305 IS 'Col305 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL306 IS 'Col306 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL307 IS 'Col307 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL308 IS 'Col308 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL309 IS 'Col309 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL310 IS 'Col310 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL311 IS 'Col311 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL312 IS 'Col312 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL313 IS 'Col313 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL314 IS 'Col314 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL315 IS 'Col315 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL316 IS 'Col316 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL317 IS 'Col317 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL318 IS 'Col318 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL319 IS 'Col319 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL320 IS 'Col320 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL321 IS 'Col321 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL322 IS 'Col322 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL323 IS 'Col323 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL324 IS 'Col324 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL325 IS 'Col325 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL326 IS 'Col326 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL327 IS 'Col327 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL328 IS 'Col328 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL329 IS 'Col329 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL330 IS 'Col330 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL331 IS 'Col331 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL332 IS 'Col332 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL333 IS 'Col333 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL334 IS 'Col334 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL335 IS 'Col335 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL336 IS 'Col336 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL337 IS 'Col337 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL338 IS 'Col338 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL339 IS 'Col339 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL340 IS 'Col340 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL341 IS 'Col341 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL342 IS 'Col342 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL343 IS 'Col343 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL344 IS 'Col344 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL345 IS 'Col345 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL346 IS 'Col346 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL347 IS 'Col347 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL348 IS 'Col348 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL349 IS 'Col349 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL350 IS 'Col350 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL351 IS 'Col351 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL352 IS 'Col352 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL353 IS 'Col353 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL354 IS 'Col354 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL355 IS 'Col355 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL356 IS 'Col356 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL357 IS 'Col357 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL358 IS 'Col358 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL359 IS 'Col359 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL360 IS 'Col360 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL361 IS 'Col361 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL362 IS 'Col362 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL363 IS 'Col363 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL364 IS 'Col364 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL365 IS 'Col365 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL366 IS 'Col366 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL367 IS 'Col367 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL368 IS 'Col368 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL369 IS 'Col369 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL370 IS 'Col370 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL371 IS 'Col371 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL372 IS 'Col372 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL373 IS 'Col373 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL374 IS 'Col374 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL375 IS 'Col375 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL376 IS 'Col376 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL377 IS 'Col377 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL378 IS 'Col378 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL379 IS 'Col379 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL380 IS 'Col380 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL381 IS 'Col381 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL382 IS 'Col382 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL383 IS 'Col383 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL384 IS 'Col384 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL385 IS 'Col385 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL386 IS 'Col386 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL387 IS 'Col387 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL388 IS 'Col388 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL389 IS 'Col389 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL390 IS 'Col390 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL391 IS 'Col391 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL392 IS 'Col392 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL393 IS 'Col393 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL394 IS 'Col394 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL395 IS 'Col395 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL396 IS 'Col396 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL397 IS 'Col397 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL398 IS 'Col398 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL399 IS 'Col399 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL400 IS 'Col400 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL401 IS 'Col401 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL402 IS 'Col402 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL403 IS 'Col403 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL404 IS 'Col404 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL405 IS 'Col405 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL406 IS 'Col406 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL407 IS 'Col407 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL408 IS 'Col408 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL409 IS 'Col409 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL410 IS 'Col410 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL411 IS 'Col411 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL412 IS 'Col412 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL413 IS 'Col413 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL414 IS 'Col414 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL415 IS 'Col415 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL416 IS 'Col416 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL417 IS 'Col417 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL418 IS 'Col418 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL419 IS 'Col419 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL420 IS 'Col420 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL421 IS 'Col421 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL422 IS 'Col422 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL423 IS 'Col423 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL424 IS 'Col424 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL425 IS 'Col425 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL426 IS 'Col426 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL427 IS 'Col427 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL428 IS 'Col428 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL429 IS 'Col429 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL430 IS 'Col430 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL431 IS 'Col431 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL432 IS 'Col432 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL433 IS 'Col433 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL434 IS 'Col434 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL435 IS 'Col435 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL436 IS 'Col436 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL437 IS 'Col437 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL438 IS 'Col438 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL439 IS 'Col439 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL440 IS 'Col440 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL441 IS 'Col441 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL442 IS 'Col442 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL443 IS 'Col443 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL444 IS 'Col444 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL445 IS 'Col445 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL446 IS 'Col446 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL447 IS 'Col447 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL448 IS 'Col448 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL449 IS 'Col449 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL450 IS 'Col450 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL451 IS 'Col451 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL452 IS 'Col452 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL453 IS 'Col453 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL454 IS 'Col454 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL455 IS 'Col455 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL456 IS 'Col456 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL457 IS 'Col457 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL458 IS 'Col458 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL459 IS 'Col459 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL460 IS 'Col460 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL461 IS 'Col461 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL462 IS 'Col462 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL463 IS 'Col463 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL464 IS 'Col464 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL465 IS 'Col465 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL466 IS 'Col466 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL467 IS 'Col467 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL468 IS 'Col468 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL469 IS 'Col469 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL470 IS 'Col470 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL471 IS 'Col471 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL472 IS 'Col472 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL473 IS 'Col473 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL474 IS 'Col474 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL475 IS 'Col475 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL476 IS 'Col476 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL477 IS 'Col477 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL478 IS 'Col478 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL479 IS 'Col479 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL480 IS 'Col480 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL481 IS 'Col481 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL482 IS 'Col482 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL483 IS 'Col483 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL484 IS 'Col484 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL485 IS 'Col485 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL486 IS 'Col486 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL487 IS 'Col487 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL488 IS 'Col488 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL489 IS 'Col489 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL490 IS 'Col490 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL491 IS 'Col491 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL492 IS 'Col492 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL493 IS 'Col493 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL494 IS 'Col494 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL495 IS 'Col495 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL496 IS 'Col496 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL497 IS 'Col497 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL498 IS 'Col498 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL499 IS 'Col499 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL500 IS 'Col500 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL501 IS 'Col501 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL502 IS 'Col502 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL503 IS 'Col503 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL504 IS 'Col504 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL505 IS 'Col505 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL506 IS 'Col506 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL507 IS 'Col507 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL508 IS 'Col508 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL509 IS 'Col509 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL510 IS 'Col510 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL511 IS 'Col511 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL512 IS 'Col512 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL513 IS 'Col513 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL514 IS 'Col514 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL515 IS 'Col515 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL516 IS 'Col516 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL517 IS 'Col517 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL518 IS 'Col518 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL519 IS 'Col519 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL520 IS 'Col520 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL521 IS 'Col521 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL522 IS 'Col522 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL523 IS 'Col523 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL524 IS 'Col524 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL525 IS 'Col525 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL526 IS 'Col526 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL527 IS 'Col527 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL528 IS 'Col528 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL529 IS 'Col529 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL530 IS 'Col530 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL531 IS 'Col531 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL532 IS 'Col532 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL533 IS 'Col533 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL534 IS 'Col534 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL535 IS 'Col535 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL536 IS 'Col536 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL537 IS 'Col537 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL538 IS 'Col538 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL539 IS 'Col539 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL540 IS 'Col540 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL541 IS 'Col541 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL542 IS 'Col542 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL543 IS 'Col543 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL544 IS 'Col544 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL545 IS 'Col545 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL546 IS 'Col546 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL547 IS 'Col547 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL548 IS 'Col548 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL549 IS 'Col549 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL550 IS 'Col550 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL551 IS 'Col551 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL552 IS 'Col552 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL553 IS 'Col553 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL554 IS 'Col554 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL555 IS 'Col555 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL556 IS 'Col556 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL557 IS 'Col557 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL558 IS 'Col558 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL559 IS 'Col559 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL560 IS 'Col560 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL561 IS 'Col561 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL562 IS 'Col562 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL563 IS 'Col563 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL564 IS 'Col564 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL565 IS 'Col565 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL566 IS 'Col566 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL567 IS 'Col567 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL568 IS 'Col568 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL569 IS 'Col569 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL570 IS 'Col570 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL571 IS 'Col571 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL572 IS 'Col572 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL573 IS 'Col573 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL574 IS 'Col574 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL575 IS 'Col575 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL576 IS 'Col576 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL577 IS 'Col577 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL578 IS 'Col578 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL579 IS 'Col579 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL580 IS 'Col580 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL581 IS 'Col581 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL582 IS 'Col582 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL583 IS 'Col583 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL584 IS 'Col584 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL585 IS 'Col585 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL586 IS 'Col586 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL587 IS 'Col587 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL588 IS 'Col588 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL589 IS 'Col589 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL590 IS 'Col590 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL591 IS 'Col591 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL592 IS 'Col592 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL593 IS 'Col593 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL594 IS 'Col594 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL595 IS 'Col595 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL596 IS 'Col596 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL597 IS 'Col597 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL598 IS 'Col598 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL599 IS 'Col599 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL600 IS 'Col600 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL601 IS 'Col601 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL602 IS 'Col602 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL603 IS 'Col603 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL604 IS 'Col604 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL605 IS 'Col605 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL606 IS 'Col606 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL607 IS 'Col607 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL608 IS 'Col608 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL609 IS 'Col609 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL610 IS 'Col610 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL611 IS 'Col611 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL612 IS 'Col612 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL613 IS 'Col613 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL614 IS 'Col614 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL615 IS 'Col615 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL616 IS 'Col616 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL617 IS 'Col617 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL618 IS 'Col618 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL619 IS 'Col619 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL620 IS 'Col620 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL621 IS 'Col621 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL622 IS 'Col622 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL623 IS 'Col623 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL624 IS 'Col624 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL625 IS 'Col625 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL626 IS 'Col626 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL627 IS 'Col627 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL628 IS 'Col628 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL629 IS 'Col629 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL630 IS 'Col630 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL631 IS 'Col631 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL632 IS 'Col632 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL633 IS 'Col633 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL634 IS 'Col634 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL635 IS 'Col635 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL636 IS 'Col636 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL637 IS 'Col637 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL638 IS 'Col638 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL639 IS 'Col639 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL640 IS 'Col640 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL641 IS 'Col641 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL642 IS 'Col642 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL643 IS 'Col643 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL644 IS 'Col644 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL645 IS 'Col645 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL646 IS 'Col646 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL647 IS 'Col647 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL648 IS 'Col648 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL649 IS 'Col649 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL650 IS 'Col650 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL651 IS 'Col651 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL652 IS 'Col652 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL653 IS 'Col653 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL654 IS 'Col654 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL655 IS 'Col655 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL656 IS 'Col656 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL657 IS 'Col657 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL658 IS 'Col658 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL659 IS 'Col659 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL660 IS 'Col660 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL661 IS 'Col661 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL662 IS 'Col662 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL663 IS 'Col663 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL664 IS 'Col664 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL665 IS 'Col665 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL666 IS 'Col666 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL667 IS 'Col667 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL668 IS 'Col668 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL669 IS 'Col669 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL670 IS 'Col670 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL671 IS 'Col671 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL672 IS 'Col672 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL673 IS 'Col673 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL674 IS 'Col674 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL675 IS 'Col675 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL676 IS 'Col676 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL677 IS 'Col677 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL678 IS 'Col678 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL679 IS 'Col679 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL680 IS 'Col680 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL681 IS 'Col681 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL682 IS 'Col682 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL683 IS 'Col683 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL684 IS 'Col684 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL685 IS 'Col685 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL686 IS 'Col686 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL687 IS 'Col687 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL688 IS 'Col688 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL689 IS 'Col689 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL690 IS 'Col690 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL691 IS 'Col691 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL692 IS 'Col692 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL693 IS 'Col693 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL694 IS 'Col694 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL695 IS 'Col695 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL696 IS 'Col696 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL697 IS 'Col697 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL698 IS 'Col698 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL699 IS 'Col699 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL700 IS 'Col700 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL701 IS 'Col701 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL702 IS 'Col702 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL703 IS 'Col703 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL704 IS 'Col704 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL705 IS 'Col705 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL706 IS 'Col706 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL707 IS 'Col707 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL708 IS 'Col708 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL709 IS 'Col709 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL710 IS 'Col710 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL711 IS 'Col711 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL712 IS 'Col712 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL713 IS 'Col713 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL714 IS 'Col714 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL715 IS 'Col715 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL716 IS 'Col716 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL717 IS 'Col717 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL718 IS 'Col718 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL719 IS 'Col719 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL720 IS 'Col720 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL721 IS 'Col721 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL722 IS 'Col722 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL723 IS 'Col723 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL724 IS 'Col724 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL725 IS 'Col725 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL726 IS 'Col726 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL727 IS 'Col727 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL728 IS 'Col728 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL729 IS 'Col729 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL730 IS 'Col730 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL731 IS 'Col731 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL732 IS 'Col732 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL733 IS 'Col733 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL734 IS 'Col734 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL735 IS 'Col735 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL736 IS 'Col736 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL737 IS 'Col737 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL738 IS 'Col738 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL739 IS 'Col739 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL740 IS 'Col740 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL741 IS 'Col741 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL742 IS 'Col742 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL743 IS 'Col743 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL744 IS 'Col744 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL745 IS 'Col745 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL746 IS 'Col746 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL747 IS 'Col747 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL748 IS 'Col748 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL749 IS 'Col749 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL750 IS 'Col750 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL751 IS 'Col751 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL752 IS 'Col752 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL753 IS 'Col753 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL754 IS 'Col754 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL755 IS 'Col755 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL756 IS 'Col756 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL757 IS 'Col757 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL758 IS 'Col758 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL759 IS 'Col759 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL760 IS 'Col760 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL761 IS 'Col761 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL762 IS 'Col762 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL763 IS 'Col763 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL764 IS 'Col764 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL765 IS 'Col765 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL766 IS 'Col766 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL767 IS 'Col767 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL768 IS 'Col768 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL769 IS 'Col769 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL770 IS 'Col770 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL771 IS 'Col771 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL772 IS 'Col772 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL773 IS 'Col773 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL774 IS 'Col774 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL775 IS 'Col775 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL776 IS 'Col776 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL777 IS 'Col777 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL778 IS 'Col778 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL779 IS 'Col779 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL780 IS 'Col780 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL781 IS 'Col781 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL782 IS 'Col782 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL783 IS 'Col783 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL784 IS 'Col784 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL785 IS 'Col785 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL786 IS 'Col786 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL787 IS 'Col787 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL788 IS 'Col788 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL789 IS 'Col789 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL790 IS 'Col790 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL791 IS 'Col791 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL792 IS 'Col792 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL793 IS 'Col793 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL794 IS 'Col794 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL795 IS 'Col795 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL796 IS 'Col796 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL797 IS 'Col797 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL798 IS 'Col798 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL799 IS 'Col799 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL800 IS 'Col800 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL801 IS 'Col801 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL802 IS 'Col802 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL803 IS 'Col803 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL804 IS 'Col804 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL805 IS 'Col805 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL806 IS 'Col806 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL807 IS 'Col807 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL808 IS 'Col808 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL809 IS 'Col809 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL810 IS 'Col810 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL811 IS 'Col811 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL812 IS 'Col812 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL813 IS 'Col813 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL814 IS 'Col814 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL815 IS 'Col815 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL816 IS 'Col816 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL817 IS 'Col817 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL818 IS 'Col818 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL819 IS 'Col819 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL820 IS 'Col820 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL821 IS 'Col821 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL822 IS 'Col822 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL823 IS 'Col823 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL824 IS 'Col824 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL825 IS 'Col825 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL826 IS 'Col826 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL827 IS 'Col827 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL828 IS 'Col828 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL829 IS 'Col829 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL830 IS 'Col830 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL831 IS 'Col831 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL832 IS 'Col832 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL833 IS 'Col833 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL834 IS 'Col834 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL835 IS 'Col835 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL836 IS 'Col836 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL837 IS 'Col837 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL838 IS 'Col838 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL839 IS 'Col839 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL840 IS 'Col840 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL841 IS 'Col841 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL842 IS 'Col842 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL843 IS 'Col843 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL844 IS 'Col844 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL845 IS 'Col845 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL846 IS 'Col846 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL847 IS 'Col847 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL848 IS 'Col848 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_PATFORMRESPONSES.COL849 IS 'Col849 data of the form response. The 
view VDA.VDA_V_MAPFORM_PATIENT provides information of the field related with this column 
for the respective form'
/


--
-- VDA_V_MORE_STUDY_DETAILS  (View) 
--
--  Dependencies: 
--   ER_STUDYID (Table)
--   ER_STUDY (Table)
--   ER_CODELST (Table)
--   ER_USER (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_MORE_STUDY_DETAILS
(FIELD_NAME, FIELD_VALUE, FK_STUDY, CREATED_ON, FK_ACCOUNT, 
 STUDY_NUMBER, CREATOR, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, RID, 
 PK_STUDYID, LAST_MODIFIED_BY_FK, CREATOR_FK)
AS 
SELECT   codelst_desc AS field_name,
            studyid_id AS field_value,
            fk_STUDY,
            a.CREATED_ON,
            p.fk_account,
            STUDY_NUMBER AS STUDY_NUMBER,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   eres.er_user
              WHERE   pk_user = A.CREATOR)
               creator,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   eres.er_user
              WHERE   pk_user = A.LAST_MODIFIED_BY)
               last_modified_by,
            a.LAST_MODIFIED_DATE,
            A.RID,
            PK_STUDYID, A.LAST_MODIFIED_BY as LAST_MODIFIED_BY_FK,A.CREATOR as CREATOR_FK
     FROM   eres.er_studyid a, eres.er_study p, eres.er_codelst c
    WHERE   pk_study = FK_STUDY AND c.pk_codelst = fk_codelst_idtype
/

COMMENT ON TABLE VDA_V_MORE_STUDY_DETAILS IS 'This view provides access to the more study 
details or IDs linked with a study'
/

COMMENT ON COLUMN VDA_V_MORE_STUDY_DETAILS.FIELD_NAME IS 'The Name of the Field'
/

COMMENT ON COLUMN VDA_V_MORE_STUDY_DETAILS.FIELD_VALUE IS 'The value of the Field'
/

COMMENT ON COLUMN VDA_V_MORE_STUDY_DETAILS.FK_STUDY IS 'The FK to the Study record'
/

COMMENT ON COLUMN VDA_V_MORE_STUDY_DETAILS.CREATED_ON IS 'The date the record was created 
on (Audit)'
/

COMMENT ON COLUMN VDA_V_MORE_STUDY_DETAILS.FK_ACCOUNT IS 'The account the study is linked 
with'
/

COMMENT ON COLUMN VDA_V_MORE_STUDY_DETAILS.STUDY_NUMBER IS 'The Study Number'
/

COMMENT ON COLUMN VDA_V_MORE_STUDY_DETAILS.CREATOR IS 'The user who created the record 
(Audit)'
/

COMMENT ON COLUMN VDA_V_MORE_STUDY_DETAILS.LAST_MODIFIED_BY IS 'The user who last modified 
the record(Audit)'
/

COMMENT ON COLUMN VDA_V_MORE_STUDY_DETAILS.LAST_MODIFIED_DATE IS 'The date the record was 
last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_MORE_STUDY_DETAILS.RID IS 'The Primary Key of the audit 
transaction'
/

COMMENT ON COLUMN VDA_V_MORE_STUDY_DETAILS.PK_STUDYID IS 'The Primary Key of the more 
study details record'
/

COMMENT ON COLUMN VDA_V_MORE_STUDY_DETAILS.LAST_MODIFIED_BY_FK IS 'The Key to identify the 
Last Modified By of the record.'
/

COMMENT ON COLUMN VDA_V_MORE_STUDY_DETAILS.CREATOR_FK IS 'The Key to identify the Creator 
of the record.'
/


--
-- VDA_V_MORE_PATIENT_DETAILS  (View) 
--
--  Dependencies: 
--   ER_SITE (Table)
--   ER_CODELST (Table)
--   ER_USER (Table)
--   ER_PER (Table)
--   PAT_PERID (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_MORE_PATIENT_DETAILS
(FIELD_NAME, FIELD_VALUE, FK_PER, CREATED_ON, FK_ACCOUNT, 
 PATIENT_ID, CREATOR, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, RID, 
 PK_PERID, SITE_NAME, FK_SITE, LAST_MODIFIED_BY_FK, CREATOR_FK)
AS 
SELECT   codelst_desc AS field_name,
            perid_id AS field_value,
            fk_per,
            a.CREATED_ON,
            p.fk_account,
            PER_CODE PATIENT_ID,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   eres.er_user
              WHERE   pk_user = A.CREATOR)
               creator,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   eres.er_user
              WHERE   pk_user = A.LAST_MODIFIED_BY)
               last_modified_by,
            a.LAST_MODIFIED_DATE,
            A.RID,
            PK_PERID,
            (SELECT   site_name
               FROM   eres.ER_SITE
              WHERE   pk_site = p.fk_site)
               site_name, p.fk_site as FK_SITE, A.LAST_MODIFIED_BY as 
LAST_MODIFIED_BY_FK,A.CREATOR as CREATOR_FK
     FROM   epat.pat_perid a, eres.ER_PER p, eres.er_codelst c
    WHERE   pk_per = fk_per AND c.pk_codelst = fk_codelst_idtype
/

COMMENT ON TABLE VDA_V_MORE_PATIENT_DETAILS IS 'This view provides access to the more 
patient codes or details linked with a patient'
/

COMMENT ON COLUMN VDA_V_MORE_PATIENT_DETAILS.FIELD_NAME IS 'The Name of the Field'
/

COMMENT ON COLUMN VDA_V_MORE_PATIENT_DETAILS.FIELD_VALUE IS 'The value of the Field'
/

COMMENT ON COLUMN VDA_V_MORE_PATIENT_DETAILS.FK_PER IS 'The FK to the patient record'
/

COMMENT ON COLUMN VDA_V_MORE_PATIENT_DETAILS.CREATED_ON IS 'The date the record was 
created on (Audit)'
/

COMMENT ON COLUMN VDA_V_MORE_PATIENT_DETAILS.FK_ACCOUNT IS 'The account the patient is 
linked with'
/

COMMENT ON COLUMN VDA_V_MORE_PATIENT_DETAILS.PATIENT_ID IS 'The Patient ID or Patient 
Code'
/

COMMENT ON COLUMN VDA_V_MORE_PATIENT_DETAILS.CREATOR IS 'The user who created the record 
(Audit)'
/

COMMENT ON COLUMN VDA_V_MORE_PATIENT_DETAILS.LAST_MODIFIED_BY IS 'The user who last 
modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_MORE_PATIENT_DETAILS.LAST_MODIFIED_DATE IS 'The date the record 
was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_MORE_PATIENT_DETAILS.RID IS 'The Primary Key of the audit 
transaction'
/

COMMENT ON COLUMN VDA_V_MORE_PATIENT_DETAILS.PK_PERID IS 'The Primary Key of the more 
patient details record'
/

COMMENT ON COLUMN VDA_V_MORE_PATIENT_DETAILS.SITE_NAME IS 'The name of the default 
organization the patient is linked'
/

COMMENT ON COLUMN VDA_V_MORE_PATIENT_DETAILS.FK_SITE IS 'The Key to the default 
organization the patient is linked'
/

COMMENT ON COLUMN VDA_V_MORE_PATIENT_DETAILS.LAST_MODIFIED_BY_FK IS 'The Key to identify 
the Last Modified By of the record.'
/

COMMENT ON COLUMN VDA_V_MORE_PATIENT_DETAILS.CREATOR_FK IS 'The Key to identify the 
Creator of the record.'
/


--
-- VDA_V_MILESTONE_ACHVD_DET  (View) 
--
--  Dependencies: 
--   ER_STUDY (Table)
--   PKG_MILESTONE_NEW (Package)
--   ER_USER (Table)
--   ER_PER (Table)
--   ER_PATPROT (Table)
--   ER_MILEACHIEVED (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_MILESTONE_ACHVD_DET
(PK_MILEACHIEVED, MSACH_MILESTONE_DESC, MSACH_PATIENT_ID, MSACH_PTSTUDY_ID, MSACH_ACH_DATE, 
 CREATOR, CREATED_ON, LAST_MODIFIED_DATE, LAST_MODIFIED_BY, FK_STUDY, 
 FK_ACCOUNT, STUDY_NUMBER, FK_MILESTONE, LAST_MODIFIED_BY_FK, CREATOR_FK)
AS 
SELECT   PK_MILEACHIEVED,
            ERES.PKG_Milestone_New.f_getMilestoneDesc (FK_MILESTONE)
               MSACH_MILESTONE_DESC,
            (SELECT   PER_CODE
               FROM   eres.ER_PER
              WHERE   PK_PER = FK_PER)
               MSACH_PATIENT_ID,
            (SELECT   PATPROT_PATSTDID
               FROM   eres.ER_PATPROT pp
              WHERE       pp.fk_per = a.fk_per
                      AND pp.fk_study = a.fk_study
                      AND pp.patprot_stat = 1
                      AND ROWNUM < 2)
               MSACH_PTSTUDY_ID,
            ACH_DATE MSACH_ACH_DATE,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   eres.ER_USER
              WHERE   pk_user = a.CREATOR)
               creator,
            a.CREATED_ON,
            a.LAST_MODIFIED_DATE,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   eres.ER_USER
              WHERE   pk_user = a.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            fk_study,
            b.fk_account,
            b.STUDY_NUMBER,
            fk_milestone,a.LAST_MODIFIED_BY as LAST_MODIFIED_BY_FK,  a.CREATOR CREATOR_FK
     FROM   eres.ER_MILEACHIEVED a, eres.ER_STUDY b
    WHERE   IS_COMPLETE = 1 AND b.pk_study = a.fk_study
/

COMMENT ON TABLE VDA_V_MILESTONE_ACHVD_DET IS 'This view provides access to the milestone 
achievement data'
/

COMMENT ON COLUMN VDA_V_MILESTONE_ACHVD_DET.PK_MILEACHIEVED IS 'The Primary of the 
milestones achievement records'
/

COMMENT ON COLUMN VDA_V_MILESTONE_ACHVD_DET.MSACH_MILESTONE_DESC IS 'Milestone 
description'
/

COMMENT ON COLUMN VDA_V_MILESTONE_ACHVD_DET.MSACH_PATIENT_ID IS 'Patient ID for Patient 
related milestones'
/

COMMENT ON COLUMN VDA_V_MILESTONE_ACHVD_DET.MSACH_PTSTUDY_ID IS 'Patient Study ID for 
Patient related milestones'
/

COMMENT ON COLUMN VDA_V_MILESTONE_ACHVD_DET.MSACH_ACH_DATE IS 'milestone achievement date'
/

COMMENT ON COLUMN VDA_V_MILESTONE_ACHVD_DET.CREATOR IS 'The user who created the record 
(Audit)'
/

COMMENT ON COLUMN VDA_V_MILESTONE_ACHVD_DET.CREATED_ON IS 'The date the record was created 
on (Audit)'
/

COMMENT ON COLUMN VDA_V_MILESTONE_ACHVD_DET.LAST_MODIFIED_DATE IS 'The date the record was 
last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_MILESTONE_ACHVD_DET.LAST_MODIFIED_BY IS 'The user who last 
modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_MILESTONE_ACHVD_DET.FK_STUDY IS 'The Foreign Key to the study 
milestone is linked with'
/

COMMENT ON COLUMN VDA_V_MILESTONE_ACHVD_DET.FK_ACCOUNT IS 'The account the milestone is 
linked with'
/

COMMENT ON COLUMN VDA_V_MILESTONE_ACHVD_DET.STUDY_NUMBER IS 'The Study Number of the study 
the milestone is linked with'
/

COMMENT ON COLUMN VDA_V_MILESTONE_ACHVD_DET.FK_MILESTONE IS 'The PK_milestone to uniquely 
identify a milestone'
/

COMMENT ON COLUMN VDA_V_MILESTONE_ACHVD_DET.LAST_MODIFIED_BY_FK IS 'The Key to identify 
the Last Modified By of the record.'
/

COMMENT ON COLUMN VDA_V_MILESTONE_ACHVD_DET.CREATOR_FK IS 'The Key to identify the Creator 
of the record.'
/


--
-- VDA_V_MILESTONES  (View) 
--
--  Dependencies: 
--   ER_STUDY (Table)
--   F_GET_MTYPE (Function)
--   F_GET_CODELSTDESC (Function)
--   ER_CODELST (Table)
--   PKG_MILESTONE_NEW (Package)
--   ER_USER (Table)
--   ER_MILESTONE (Table)
--   SCH_PROTOCOL_VISIT (Table)
--   EVENT_ASSOC (Table)
--   SCH_CODELST (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_MILESTONES
(STUDY_NUMBER, STUDY_TITLE, MSRUL_CATEGORY, MSRUL_AMOUNT, MSRUL_PT_COUNT, 
 MSRUL_PT_STATUS, MSRUL_LIMIT, MSRUL_PAY_TYPE, MSRUL_PAY_FOR, MSRUL_STATUS, 
 MSRUL_PROT_CAL, MSRUL_VISIT, MSRUL_MS_RULE, MSRUL_EVENT_STAT, MSRUL_STUDY_STATUS, 
 FK_ACCOUNT, PK_MILESTONE, CREATOR, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, 
 CREATED_ON, FK_STUDY, EVENT_NAME, MILESTONE_ACHIEVEDCOUNT, MILESTONE_ACHIEVEDAMOUNT, 
 LAST_CHECKED_ON, MILESTONE_DESCRIPTION, FK_BUDGET, FK_BGTCAL, FK_BGTSECTION, 
 FK_LINEITEM, MILESTONE_DATE_FROM, MILESTONE_DATE_TO, MILESTONE_HOLDBACK, MILESTONE_DESC_CALCULATED, 
 FK_VISIT, FK_EVENTASSOC, LAST_MODIFIED_BY_FK, CREATOR_FK)
AS 
SELECT   STUDY_NUMBER,
            STUDY_TITLE,
            ERES.F_GEt_Mtype (MILESTONE_TYPE) MSRUL_CATEGORY,
            MILESTONE_AMOUNT MSRUL_AMOUNT,
            MILESTONE_COUNT MSRUL_PT_COUNT,
            (SELECT   CODELST_DESC
               FROM   eres.ER_CODELST
              WHERE   PK_CODELST = MILESTONE_STATUS
                      AND CODELST_TYPE = 'patStatus')
               MSRUL_PT_STATUS,
            MILESTONE_LIMIT MSRUL_LIMIT,
            ERES.F_GEt_Codelstdesc (MILESTONE_PAYTYPE) MSRUL_PAY_TYPE,
            ERES.F_GEt_Codelstdesc (MILESTONE_PAYFOR) MSRUL_PAY_FOR,
            ERES.F_GEt_Codelstdesc (FK_CODELST_MILESTONE_STAT) MSRUL_STATUS,
            (SELECT   NAME
               FROM   ESCH.EVENT_ASSOC
              WHERE   EVENT_ID = FK_CAL)
               MSRUL_PROT_CAL,
            (SELECT   visit_name
               FROM   esch.sch_protocol_visit
              WHERE   pk_protocol_visit = fk_visit)
               MSRUL_VISIT,
            ERES.F_GEt_Codelstdesc (FK_CODELST_RULE) MSRUL_MS_RULE,
            (SELECT   CODELST_DESC
               FROM   esch.SCH_CODELST
              WHERE   PK_CODELST = MILESTONE_EVENTSTATUS)
               MSRUL_EVENT_STAT,
            (SELECT   CODELST_DESC
               FROM   eres.ER_CODELST
              WHERE   PK_CODELST = MILESTONE_STATUS
                      AND CODELST_TYPE = 'studystat')
               MSRUL_STUDY_STATUS,
            s.fk_Account FK_ACCOUNT,
            PK_MILESTONE,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = m.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = m.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            m.LAST_MODIFIED_DATE,
            m.CREATED_ON,
            FK_STUDY,
            (SELECT   NAME
               FROM   ESCH.EVENT_ASSOC
              WHERE   EVENT_ID = FK_EVENTASSOC)
               EVENT_NAME,
            MILESTONE_ACHIEVEDCOUNT,
            NVL (MILESTONE_ACHIEVEDCOUNT * MILESTONE_AMOUNT, 0),
            LAST_CHECKED_ON,
            MILESTONE_DESCRIPTION,
            FK_BUDGET,
            FK_BGTCAL,
            FK_BGTSECTION,
            FK_LINEITEM,
            m.MILESTONE_DATE_FROM,
            m.MILESTONE_DATE_TO,
            m.MILESTONE_HOLDBACK,
            ERES.PKG_Milestone_New.f_getMilestoneDesc (PK_MILESTONE),
            fk_visit,
            FK_EVENTASSOC, m.LAST_MODIFIED_BY LAST_MODIFIED_BY_FK, m.CREATOR CREATOR_FK
     FROM   eres.ER_MILESTONE m, eres.er_study s
    WHERE   NVL (milestone_delflag, 'N') = 'N' AND m.fk_study = pk_study
/

COMMENT ON TABLE VDA_V_MILESTONES IS 'This view provides access to the Milestones defined 
for studies'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_AMOUNT IS 'The amount linked with the milestone'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_PT_COUNT IS 'The patient count attribute of the 
milestone (if applicable)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_PT_STATUS IS 'The patient status attribute of the 
milestone (if applicable)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_LIMIT IS 'The milestone achievement count limit'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_PAY_TYPE IS 'The milestone payment type'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_PAY_FOR IS 'The milestone payment for'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_STATUS IS 'The milestone status'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_PROT_CAL IS 'The Calendar attribute of the 
milestone (if applicable)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_VISIT IS 'The Calendar visit attribute of the 
milestone (if applicable)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_MS_RULE IS 'The milestone rule description'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_EVENT_STAT IS 'The Calendar event status 
attribute of the milestone (if applicable)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_STUDY_STATUS IS 'The Study Status attribute of 
the milestone (if applicable)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.FK_ACCOUNT IS 'The account the milestone is linked 
with'
/

COMMENT ON COLUMN VDA_V_MILESTONES.PK_MILESTONE IS 'The primary key of the milestone 
record'
/

COMMENT ON COLUMN VDA_V_MILESTONES.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.LAST_MODIFIED_BY IS 'The user who last modified the 
record(Audit)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.LAST_MODIFIED_DATE IS 'The date the record was last 
modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.CREATED_ON IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.FK_STUDY IS 'The Foreign Key to the study milestone is 
linked with'
/

COMMENT ON COLUMN VDA_V_MILESTONES.EVENT_NAME IS 'The event name attribute of the 
milestone (if applicable)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MILESTONE_ACHIEVEDCOUNT IS 'The current milestone 
achievement count'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MILESTONE_ACHIEVEDAMOUNT IS 'The current milestone 
achieved amount'
/

COMMENT ON COLUMN VDA_V_MILESTONES.LAST_CHECKED_ON IS 'The timestamp when the milestone 
achievement was last checked on'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MILESTONE_DESCRIPTION IS 'The milestone description'
/

COMMENT ON COLUMN VDA_V_MILESTONES.FK_BUDGET IS 'The foreign key to the Budget (if the 
milestone was created from a budget)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.FK_BGTCAL IS 'The foreign key to the Budget Calendar 
record (if the milestone was created from a budget)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.FK_BGTSECTION IS 'The foreign key to the Budget Section 
record Calendar record (if the milestone was created from a budget)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.FK_LINEITEM IS 'The foreign key to the Budget line item 
record (if the milestone was created from a budget)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MILESTONE_DATE_FROM IS 'The Start Date for the time-
bounded milestones'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MILESTONE_DATE_TO IS 'The To or End Date for the time-
bounded milestones'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MILESTONE_HOLDBACK IS 'The Holdback amount for the 
milestone'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MILESTONE_DESC_CALCULATED IS 'The Milestone description 
calculated from milestone attributes'
/

COMMENT ON COLUMN VDA_V_MILESTONES.FK_VISIT IS 'The PK of visit to uniquely identify a 
visit for visit milestones'
/

COMMENT ON COLUMN VDA_V_MILESTONES.FK_EVENTASSOC IS 'The PK of visit to uniquely identify 
an Event for event milestones'
/

COMMENT ON COLUMN VDA_V_MILESTONES.LAST_MODIFIED_BY_FK IS 'The Key to identify the Last 
Modified By of the record.'
/

COMMENT ON COLUMN VDA_V_MILESTONES.CREATOR_FK IS 'The Key to identify the Creator of the 
record.'
/

COMMENT ON COLUMN VDA_V_MILESTONES.STUDY_NUMBER IS 'The Study Number of the study the 
milestone is linked with'
/

COMMENT ON COLUMN VDA_V_MILESTONES.STUDY_TITLE IS 'The study title of the study the 
milestone is linked with'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_CATEGORY IS 'The milestone rule category'
/


--
-- VDA_V_MAPFORM_STUDY  (View) 
--
--  Dependencies: 
--   ER_FORMLIB (Table)
--   ER_MAPFORM (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_MAPFORM_STUDY
(PK_MP, FK_FORM, MP_FORMTYPE, MP_MAPCOLNAME, MP_SYSTEMID, 
 MP_KEYWORD, MP_UID, MP_DISPNAME, MP_ORIGSYSID, MP_FLDDATATYPE, 
 MP_BROWSER, MP_SEQUENCE, MP_PKSEC, MP_SECNAME, MP_PKFLD, 
 MP_FLDCHARSNO, MP_FLDSEQNO, FORM_NAME, FORM_DESC, FK_ACCOUNT)
AS 
SELECT   m."PK_MP",
            m."FK_FORM",
            m."MP_FORMTYPE",
            m."MP_MAPCOLNAME",
            m."MP_SYSTEMID",
            m."MP_KEYWORD",
            m."MP_UID",
            m."MP_DISPNAME",
            m."MP_ORIGSYSID",
            m."MP_FLDDATATYPE",
            m."MP_BROWSER",
            m."MP_SEQUENCE",
            m."MP_PKSEC",
            m."MP_SECNAME",
            m."MP_PKFLD",
            m."MP_FLDCHARSNO",
            m."MP_FLDSEQNO",
            f.form_name,
            f.form_desc,
            f.FK_ACCOUNT
     FROM   eres.er_mapform m, eres.er_formlib f
    WHERE   fk_form = pk_formlib AND mp_formtype = 'S'
/

COMMENT ON TABLE VDA_V_MAPFORM_STUDY IS 'This view provides access to the form design information that can be used to write reports on form responses. Please use information in this table to write a report on all study type form responses available through the view VDA.VDA_V_STUDYFORMRESPONSES'
/

COMMENT ON COLUMN VDA_V_MAPFORM_STUDY.PK_MP IS 'The Primary Key of the mapping record'
/

COMMENT ON COLUMN VDA_V_MAPFORM_STUDY.FK_FORM IS 'The Foreign Key to the form record'
/

COMMENT ON COLUMN VDA_V_MAPFORM_STUDY.MP_FORMTYPE IS 'The form type - account in this case'
/

COMMENT ON COLUMN VDA_V_MAPFORM_STUDY.MP_MAPCOLNAME IS 'The column name in VDA.VDA_V_STUDYFORMRESPONSES where data for this field is accessible from'
/

COMMENT ON COLUMN VDA_V_MAPFORM_STUDY.MP_SYSTEMID IS 'The auto generated system of the field that uniquely identifies the field in a form'
/

COMMENT ON COLUMN VDA_V_MAPFORM_STUDY.MP_KEYWORD IS 'The keyword given to the field'
/

COMMENT ON COLUMN VDA_V_MAPFORM_STUDY.MP_UID IS 'The field id given to the field'
/

COMMENT ON COLUMN VDA_V_MAPFORM_STUDY.MP_DISPNAME IS 'The display name of the field'
/

COMMENT ON COLUMN VDA_V_MAPFORM_STUDY.MP_ORIGSYSID IS 'The auto generated system of the field that uniquely identifies the field in a form. This field is relevant for fields that are part of ''repeating sections'' and ties them with the original field of such a section'
/

COMMENT ON COLUMN VDA_V_MAPFORM_STUDY.MP_FLDDATATYPE IS 'The data type of the field'
/

COMMENT ON COLUMN VDA_V_MAPFORM_STUDY.MP_BROWSER IS 'The flag whether the field should be displayed in the form response browser'
/

COMMENT ON COLUMN VDA_V_MAPFORM_STUDY.MP_SEQUENCE IS 'The sequence of the section within the form'
/

COMMENT ON COLUMN VDA_V_MAPFORM_STUDY.MP_PKSEC IS 'The Primary Key of the section'
/

COMMENT ON COLUMN VDA_V_MAPFORM_STUDY.MP_SECNAME IS 'The name of the section'
/

COMMENT ON COLUMN VDA_V_MAPFORM_STUDY.MP_PKFLD IS 'The Primary Key of the field'
/

COMMENT ON COLUMN VDA_V_MAPFORM_STUDY.MP_FLDCHARSNO IS 'The maximum number of characters that are valid for the field input. Applicable for edit box fields'
/

COMMENT ON COLUMN VDA_V_MAPFORM_STUDY.MP_FLDSEQNO IS 'The sequence of the field within the form section'
/

COMMENT ON COLUMN VDA_V_MAPFORM_STUDY.FORM_NAME IS 'The name of the form'
/

COMMENT ON COLUMN VDA_V_MAPFORM_STUDY.FORM_DESC IS 'The description of the form'
/

COMMENT ON COLUMN VDA_V_MAPFORM_STUDY.FK_ACCOUNT IS 'The account the form is linked with'
/


--
-- VDA_V_MAPFORM_PATIENT  (View) 
--
--  Dependencies: 
--   ER_FORMLIB (Table)
--   ER_MAPFORM (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_MAPFORM_PATIENT
(PK_MP, FK_FORM, MP_FORMTYPE, MP_MAPCOLNAME, MP_SYSTEMID, 
 MP_KEYWORD, MP_UID, MP_DISPNAME, MP_ORIGSYSID, MP_FLDDATATYPE, 
 MP_BROWSER, MP_SEQUENCE, MP_PKSEC, MP_SECNAME, MP_PKFLD, 
 MP_FLDCHARSNO, MP_FLDSEQNO, FORM_NAME, FORM_DESC, FK_ACCOUNT)
AS 
SELECT   m."PK_MP",
            m."FK_FORM",
            m."MP_FORMTYPE",
            m."MP_MAPCOLNAME",
            m."MP_SYSTEMID",
            m."MP_KEYWORD",
            m."MP_UID",
            m."MP_DISPNAME",
            m."MP_ORIGSYSID",
            m."MP_FLDDATATYPE",
            m."MP_BROWSER",
            m."MP_SEQUENCE",
            m."MP_PKSEC",
            m."MP_SECNAME",
            m."MP_PKFLD",
            m."MP_FLDCHARSNO",
            m."MP_FLDSEQNO",
            f.form_name,
            f.form_desc,
            f.fk_account
     FROM   eres.er_mapform m, eres.er_formlib f
    WHERE   fk_form = pk_formlib AND mp_formtype = 'P'
/

COMMENT ON TABLE VDA_V_MAPFORM_PATIENT IS 'This view provides access to the form design information that can be used to write reports on form responses. Please use information in this table to write a report on all patient type form responses available through the view VDA.VDA_V_PATFORMRESPONSES'
/

COMMENT ON COLUMN VDA_V_MAPFORM_PATIENT.MP_SYSTEMID IS 'The auto generated system of the field that uniquely identifies the field in a form'
/

COMMENT ON COLUMN VDA_V_MAPFORM_PATIENT.MP_KEYWORD IS 'The keyword given to the field'
/

COMMENT ON COLUMN VDA_V_MAPFORM_PATIENT.MP_UID IS 'The field id given to the field'
/

COMMENT ON COLUMN VDA_V_MAPFORM_PATIENT.MP_DISPNAME IS 'The display name of the field'
/

COMMENT ON COLUMN VDA_V_MAPFORM_PATIENT.MP_ORIGSYSID IS 'The auto generated system of the field that uniquely identifies the field in a form. This field is relevant for fields that are part of ''repeating sections'' and ties them with the original field of such a section'
/

COMMENT ON COLUMN VDA_V_MAPFORM_PATIENT.MP_FLDDATATYPE IS 'The data type of the field'
/

COMMENT ON COLUMN VDA_V_MAPFORM_PATIENT.MP_BROWSER IS 'The flag whether the field should be displayed in the form response browser'
/

COMMENT ON COLUMN VDA_V_MAPFORM_PATIENT.MP_SEQUENCE IS 'The sequence of the section within the form'
/

COMMENT ON COLUMN VDA_V_MAPFORM_PATIENT.MP_PKSEC IS 'The Primary Key of the section'
/

COMMENT ON COLUMN VDA_V_MAPFORM_PATIENT.MP_SECNAME IS 'The name of the section'
/

COMMENT ON COLUMN VDA_V_MAPFORM_PATIENT.MP_PKFLD IS 'The Primary Key of the field'
/

COMMENT ON COLUMN VDA_V_MAPFORM_PATIENT.MP_FLDCHARSNO IS 'The maximum number of characters that are valid for the field input. Applicable for edit box fields'
/

COMMENT ON COLUMN VDA_V_MAPFORM_PATIENT.MP_FLDSEQNO IS 'The sequence of the field within the form section'
/

COMMENT ON COLUMN VDA_V_MAPFORM_PATIENT.FORM_NAME IS 'The name of the form'
/

COMMENT ON COLUMN VDA_V_MAPFORM_PATIENT.FORM_DESC IS 'The description of the form'
/

COMMENT ON COLUMN VDA_V_MAPFORM_PATIENT.FK_ACCOUNT IS 'The account the form is linked with'
/

COMMENT ON COLUMN VDA_V_MAPFORM_PATIENT.PK_MP IS 'The Primary Key of the mapping record'
/

COMMENT ON COLUMN VDA_V_MAPFORM_PATIENT.FK_FORM IS 'The Foreign Key to the form record'
/

COMMENT ON COLUMN VDA_V_MAPFORM_PATIENT.MP_FORMTYPE IS 'The form type - account in this case'
/

COMMENT ON COLUMN VDA_V_MAPFORM_PATIENT.MP_MAPCOLNAME IS 'The column name in VDA.VDA_V_PATFORMRESPONSES where data for this field is accessible from'
/


--
-- VDA_V_MAPFORM_ACCOUNT  (View) 
--
--  Dependencies: 
--   ER_FORMLIB (Table)
--   ER_MAPFORM (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_MAPFORM_ACCOUNT
(PK_MP, FK_FORM, MP_FORMTYPE, MP_MAPCOLNAME, MP_SYSTEMID, 
 MP_KEYWORD, MP_UID, MP_DISPNAME, MP_ORIGSYSID, MP_FLDDATATYPE, 
 MP_BROWSER, MP_SEQUENCE, MP_PKSEC, MP_SECNAME, MP_PKFLD, 
 MP_FLDCHARSNO, MP_FLDSEQNO, FORM_NAME, FORM_DESC, FK_ACCOUNT)
AS 
SELECT   m."PK_MP",
            m."FK_FORM",
            m."MP_FORMTYPE",
            m."MP_MAPCOLNAME",
            m."MP_SYSTEMID",
            m."MP_KEYWORD",
            m."MP_UID",
            m."MP_DISPNAME",
            m."MP_ORIGSYSID",
            m."MP_FLDDATATYPE",
            m."MP_BROWSER",
            m."MP_SEQUENCE",
            m."MP_PKSEC",
            m."MP_SECNAME",
            m."MP_PKFLD",
            m."MP_FLDCHARSNO",
            m."MP_FLDSEQNO",
            f.form_name,
            f.form_desc,
            f.fk_account
     FROM   eres.er_mapform m, eres.er_formlib f
    WHERE   fk_form = pk_formlib AND mp_formtype = 'A'
/

COMMENT ON TABLE VDA_V_MAPFORM_ACCOUNT IS 'This view provides access to the form design information that can be used to write reports on form responses. Please use information in this table to write a report on all account level form responses available through the view VDA.VDA_V_ACCOUNTFORMRESPONSES'
/

COMMENT ON COLUMN VDA_V_MAPFORM_ACCOUNT.FORM_DESC IS 'The description of the form'
/

COMMENT ON COLUMN VDA_V_MAPFORM_ACCOUNT.FK_ACCOUNT IS 'The account the form is linked with'
/

COMMENT ON COLUMN VDA_V_MAPFORM_ACCOUNT.PK_MP IS 'The Primary Key of the mapping record'
/

COMMENT ON COLUMN VDA_V_MAPFORM_ACCOUNT.FK_FORM IS 'The Foreign Key to the form record'
/

COMMENT ON COLUMN VDA_V_MAPFORM_ACCOUNT.MP_FORMTYPE IS 'The form type - account in this case'
/

COMMENT ON COLUMN VDA_V_MAPFORM_ACCOUNT.MP_MAPCOLNAME IS 'The column name in VDA.VDA_V_ACCOUNTFORMRESPONSES where data for this field is accessible from'
/

COMMENT ON COLUMN VDA_V_MAPFORM_ACCOUNT.MP_SYSTEMID IS 'The auto generated system of the field that uniquely identifies the field in a form'
/

COMMENT ON COLUMN VDA_V_MAPFORM_ACCOUNT.MP_KEYWORD IS 'The keyword given to the field'
/

COMMENT ON COLUMN VDA_V_MAPFORM_ACCOUNT.MP_UID IS 'The field id given to the field'
/

COMMENT ON COLUMN VDA_V_MAPFORM_ACCOUNT.MP_DISPNAME IS 'The display name of the field'
/

COMMENT ON COLUMN VDA_V_MAPFORM_ACCOUNT.MP_ORIGSYSID IS 'The auto generated system of the field that uniquely identifies the field in a form. This field is relevant for fields that are part of ''repeating sections'' and ties them with the original field of such a section'
/

COMMENT ON COLUMN VDA_V_MAPFORM_ACCOUNT.MP_FLDDATATYPE IS 'The data type of the field'
/

COMMENT ON COLUMN VDA_V_MAPFORM_ACCOUNT.MP_BROWSER IS 'The flag whether the field should be displayed in the form response browser'
/

COMMENT ON COLUMN VDA_V_MAPFORM_ACCOUNT.MP_SEQUENCE IS 'The sequence of the section within the form'
/

COMMENT ON COLUMN VDA_V_MAPFORM_ACCOUNT.MP_PKSEC IS 'The Primary Key of the section'
/

COMMENT ON COLUMN VDA_V_MAPFORM_ACCOUNT.MP_SECNAME IS 'The name of the section'
/

COMMENT ON COLUMN VDA_V_MAPFORM_ACCOUNT.MP_PKFLD IS 'The Primary Key of the field'
/

COMMENT ON COLUMN VDA_V_MAPFORM_ACCOUNT.MP_FLDCHARSNO IS 'The maximum number of characters that are valid for the field input. Applicable for edit box fields'
/

COMMENT ON COLUMN VDA_V_MAPFORM_ACCOUNT.MP_FLDSEQNO IS 'The sequence of the field within the form section'
/

COMMENT ON COLUMN VDA_V_MAPFORM_ACCOUNT.FORM_NAME IS 'The name of the form'
/


--
-- VDA_V_LIB_EVENTS  (View) 
--
--  Dependencies: 
--   ER_SITE (Table)
--   F_GET_DURUNIT (Function)
--   ER_USER (Table)
--   SCH_EVENTUSR (Table)
--   SCH_EVENTDOC (Table)
--   SCH_EVENTCOST (Table)
--   EVENT_DEF (Table)
--   SCH_CODELST (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_LIB_EVENTS
(LBEVE_CATEGORY, LBEVE_EVENT, LBEVE_DESCRIPTION, LBEVE_CPTCODE, LBEVE_DURATION, 
 LBEVE_VISIT_WIN_B4, LBEVE_VISIT_WIN_AFT, LBEVE_NOTES, LBEVE_PTMESS_B4, LBEVE_PTMESS_AFT, 
 LBEVE_PTDAYS_B4, LBEVE_PTDAYS_AFT, LBEVE_USRMESS_B4, LBEVE_USRMESS_AFT, LBEVE_USRDAYS_B4, 
 LBEVE_USRDAYS_AFT, LBEVE_COST_CNT, LBEVE_APPNX_CNT, LBEVE_RESOURCE_CNT, FK_ACCOUNT, 
 EVENT_ID, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, 
 EVENT_LIBRARY_TYPE, EVENT_LINE_CATEGORY, SITE_OF_SERVICE_NAME, FACILITY_NAME, COVERAGE_TYPE, 
 LAST_MODIFIED_BY_FK, CREATOR_FK)
AS 
SELECT   (SELECT   NAME
               FROM   ESCH.EVENT_DEF B
              WHERE   A.CHAIN_ID = B.EVENT_ID AND EVENT_TYPE = 'L')
               LBEVE_CATEGORY,
            NAME LBEVE_EVENT,
            DESCRIPTION LBEVE_DESCRIPTION,
            EVENT_CPTCODE LBEVE_CPTCODE,
            DURATION || ' ' || ERES.F_GET_DURUNIT (DURATION_UNIT)
               LBEVE_DURATION,
            FUZZY_PERIOD || ' ' || ERES.F_GET_DURUNIT (EVENT_DURATIONBEFORE)
               LBEVE_VISIT_WIN_B4,
               EVENT_FUZZYAFTER
            || ' '
            || ERES.F_GET_DURUNIT (EVENT_DURATIONAFTER)
               LBEVE_VISIT_WIN_AFT,
            NOTES LBEVE_NOTES,
            PAT_MSGBEFORE LBEVE_PTMESS_B4,
            PAT_MSGAFTER LBEVE_PTMESS_AFT,
            PAT_DAYSBEFORE LBEVE_PTDAYS_B4,
            PAT_DAYSAFTER LBEVE_PTDAYS_AFT,
            USR_MSGBEFORE LBEVE_USRMESS_B4,
            USR_MSGAFTER LBEVE_USRMESS_AFT,
            USR_DAYSBEFORE LBEVE_USRDAYS_B4,
            USR_DAYSAFTER LBEVE_USRDAYS_AFT,
            (SELECT   COUNT ( * )
               FROM   esch.SCH_EVENTCOST
              WHERE   FK_EVENT = EVENT_ID)
               LBEVE_COST_CNT,
            (SELECT   COUNT ( * )
               FROM   esch.SCH_EVENTDOC
              WHERE   FK_EVENT = EVENT_ID)
               LBEVE_APPNX_CNT,
            (SELECT   COUNT ( * )
               FROM   esch.SCH_EVENTUSR
              WHERE   FK_EVENT = EVENT_ID AND EVENTUSR_TYPE = 'R')
               LBEVE_RESOURCE_CNT,
            USER_ID FK_ACCOUNT,
            EVENT_ID,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = A.CREATOR)
               CREATOR,
            CREATED_ON,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = A.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            LAST_MODIFIED_DATE,
            (SELECT   CODELST_DESC
               FROM   esch.SCH_CODELST
              WHERE   PK_CODELST = EVENT_LIBRARY_TYPE)
               EVENT_LIBRARY_TYPE,
            (SELECT   (SELECT   CODELST_DESC
                         FROM   esch.SCH_CODELST
                        WHERE   PK_CODELST = b.EVENT_LINE_CATEGORY)
               FROM   ESCH.EVENT_DEF B
              WHERE   A.CHAIN_ID = B.EVENT_ID AND EVENT_TYPE = 'L')
               EVENT_LINE_CATEGORY,
            (SELECT   site_name
               FROM   eres.er_site
              WHERE   pk_site = SERVICE_SITE_ID)
               SITE_OF_SERVICE_NAME,
            (SELECT   site_name
               FROM   eres.er_site
              WHERE   pk_site = FACILITY_ID)
               FACILITY_NAME,
            (SELECT   CODELST_DESC
               FROM   esch.SCH_CODELST
              WHERE   PK_CODELST = FK_CODELST_COVERTYPE)
               COVERAGE_TYPE, A.LAST_MODIFIED_BY as LAST_MODIFIED_BY_FK, A.CREATOR as 
CREATOR_FK
     FROM   ESCH.EVENT_DEF A
    WHERE   EVENT_TYPE = 'E'
/

COMMENT ON TABLE VDA_V_LIB_EVENTS IS 'This view provides access to the Event Library data'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.LBEVE_USRDAYS_AFT IS 'Number of Days after the Event 
the user Message to be sent out'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.LBEVE_COST_CNT IS 'Count of cost records linked with 
the event'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.LBEVE_APPNX_CNT IS 'Count of appendix records linked 
with the event'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.LBEVE_RESOURCE_CNT IS 'Count of resource records linked 
with the event'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.FK_ACCOUNT IS 'the account linked with event library'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.EVENT_ID IS 'the Primary Key of the event record'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.CREATOR IS 'The user who created the event  record 
(Audit)'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.CREATED_ON IS 'The date the event record was created on 
(Audit)'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.LAST_MODIFIED_BY IS 'The user who last modified the 
event  record(Audit)'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.LAST_MODIFIED_DATE IS 'The date the event record was 
last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.EVENT_LIBRARY_TYPE IS 'The event library type the event 
is linked with'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.EVENT_LINE_CATEGORY IS 'The default budget category for 
the event'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.SITE_OF_SERVICE_NAME IS 'The Site of Service 
(Organization) linked with the event'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.FACILITY_NAME IS 'The Facility linked with the event'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.COVERAGE_TYPE IS 'The default coverage type linked with 
the event'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.LAST_MODIFIED_BY_FK IS 'The Key to identify the Last 
Modified By of the record.'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.CREATOR_FK IS 'The Key to identify the Creator of the 
record.'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.LBEVE_CATEGORY IS 'The Event category in the Event 
Library'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.LBEVE_EVENT IS 'The event Name'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.LBEVE_DESCRIPTION IS 'The Event Description'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.LBEVE_CPTCODE IS 'The CPT code linked with the event'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.LBEVE_DURATION IS 'The Event Duration'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.LBEVE_VISIT_WIN_B4 IS 'Visit Window duration specified 
for before the event date'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.LBEVE_VISIT_WIN_AFT IS 'Visit Window duration specified 
for after the event date'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.LBEVE_NOTES IS 'Event Notes'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.LBEVE_PTMESS_B4 IS 'Event Message for the patient-
before the event'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.LBEVE_PTMESS_AFT IS 'Event Message for the patient-
after the event'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.LBEVE_PTDAYS_B4 IS 'Number of Days before the Event the 
patient Message to be sent out'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.LBEVE_PTDAYS_AFT IS 'Number of Days after the Event the 
patient Message to be sent out'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.LBEVE_USRMESS_B4 IS 'Event Message for the user-before 
the event'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.LBEVE_USRMESS_AFT IS 'Event Message for the user-after 
the event'
/

COMMENT ON COLUMN VDA_V_LIB_EVENTS.LBEVE_USRDAYS_B4 IS 'Number of Days before the Event 
the user Message to be sent out'
/


--
-- VDA_V_LIB_CALENDAR  (View) 
--
--  Dependencies: 
--   F_GET_SHAREDWITH (Function)
--   F_GET_DURUNIT (Function)
--   F_GET_DURATION (Function)
--   ER_CATLIB (Table)
--   ER_USER (Table)
--   EVENT_DEF (Table)
--   SCH_CODELST (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_LIB_CALENDAR
(LBCAL_TYPE, LBCAL_NAME, LBCAL_DESC, LBCAL_STAT, LBCAL_SHARED_WITH, 
 LBCAL_DURATION, CREATOR, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, 
 CALENDAR_PK, FK_ACCOUNT, LAST_MODIFIED_BY_FK, CREATOR_FK)
AS 
SELECT   (SELECT   CATLIB_NAME
               FROM   ERES.ER_CATLIB
              WHERE   PK_CATLIB = FK_CATLIB)
               LBCAL_TYPE,
            NAME LBCAL_NAME,
            DESCRIPTION LBCAL_DESC,
            --JM: 08Feb2011, D-FIN9
            --ERES.F_GET_FSTAT (STATUS) LBCAL_STAT,
            (SELECT   CODELST_DESC
               FROM   esch.sch_codelst
              WHERE   PK_CODELST = FK_CODELST_CALSTAT)
               LBCAL_STAT,
            ERES.F_GET_SHAREDWITH (CALENDAR_SHAREDWITH) LBCAL_SHARED_WITH,
               ERES.F_GET_DURATION (DURATION_UNIT, DURATION)
            || ' '
            || ERES.F_GET_DURUNIT (DURATION_UNIT)
               LBCAL_DURATION,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = E.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = E.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            LAST_MODIFIED_DATE,
            CREATED_ON,
            EVENT_ID CALENDAR_PK,
            USER_ID FK_ACCOUNT, E.LAST_MODIFIED_BY as LAST_MODIFIED_BY_FK, E.CREATOR as 
CREATOR_FK
     FROM   ESCH.EVENT_DEF E
    WHERE   EVENT_TYPE = 'P'
/

COMMENT ON TABLE VDA_V_LIB_CALENDAR IS 'This view provides access to Library Calendars'' 
summary information'
/

COMMENT ON COLUMN VDA_V_LIB_CALENDAR.LBCAL_TYPE IS 'Calendar Type'
/

COMMENT ON COLUMN VDA_V_LIB_CALENDAR.LBCAL_NAME IS 'Calendar Name'
/

COMMENT ON COLUMN VDA_V_LIB_CALENDAR.LBCAL_DESC IS 'Calendar Description'
/

COMMENT ON COLUMN VDA_V_LIB_CALENDAR.LBCAL_STAT IS 'Calendar Status'
/

COMMENT ON COLUMN VDA_V_LIB_CALENDAR.LBCAL_SHARED_WITH IS 'Shared With-access information'
/

COMMENT ON COLUMN VDA_V_LIB_CALENDAR.LBCAL_DURATION IS 'Calendar Duration'
/

COMMENT ON COLUMN VDA_V_LIB_CALENDAR.CREATOR IS 'The user who created the calendar record 
(Audit)'
/

COMMENT ON COLUMN VDA_V_LIB_CALENDAR.LAST_MODIFIED_BY IS 'The user who last modified the 
calendar record(Audit)'
/

COMMENT ON COLUMN VDA_V_LIB_CALENDAR.LAST_MODIFIED_DATE IS 'The date the calendar record 
was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_LIB_CALENDAR.CREATED_ON IS 'The date the calendar record was 
created on (Audit)'
/

COMMENT ON COLUMN VDA_V_LIB_CALENDAR.CALENDAR_PK IS 'The Primary Key of the Calendar 
record'
/

COMMENT ON COLUMN VDA_V_LIB_CALENDAR.FK_ACCOUNT IS 'The account the calendar is linked 
with'
/

COMMENT ON COLUMN VDA_V_LIB_CALENDAR.LAST_MODIFIED_BY_FK IS 'The Key to identify the Last 
Modified By of the record.'
/

COMMENT ON COLUMN VDA_V_LIB_CALENDAR.CREATOR_FK IS 'The Key to identify the Creator of the 
record.'
/


--
-- VDA_V_LIBRARY_FORMS  (View) 
--
--  Dependencies: 
--   ER_FORMLIB (Table)
--   F_GET_SHAREDWITH (Function)
--   ER_CATLIB (Table)
--   F_CODELST_DESC (Function)
--   ER_USER (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_LIBRARY_FORMS
(FORM_TYPE, FORM_NAME, FORM_DESC, FORM_STATUS, FORM_SHARED_WITH, 
 FK_ACCOUNT, PK_FORMLIB, RID, CREATOR, LAST_MODIFIED_BY, 
 LAST_MODIFIED_DATE, CREATED_ON, FORM_ESIGN_REQUIRED)
AS 
SELECT   (SELECT   CATLIB_NAME
               FROM   eres.ER_CATLIB
              WHERE   PK_CATLIB = FK_CATLIB)
               FORM_TYPE,
            FORM_NAME FORM_NAME,
            FORM_DESC FORM_DESC,
            eres.F_CODELST_DESC (FORM_STATUS) FORM_STATUS,
            eres.F_GET_SHAREDWITH (FORM_SHAREDWITH) FORM_SHARED_WITH,
            FK_ACCOUNT,
            PK_FORMLIB,
            RID,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   eres.ER_USER
              WHERE   pk_user = ER_FORMLIB.CREATOR)
               creator,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   eres.ER_USER
              WHERE   pk_user = ER_FORMLIB.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            LAST_MODIFIED_DATE,
            CREATED_ON,
            DECODE (NVL (FORM_ESIGNREQ, 0),
                    0,
                    'No',
                    1,
                    'Yes')
               FORM_ESIGN_REQUIRED
     FROM   eres.ER_FORMLIB
    WHERE   form_linkto = 'L' AND NVL (record_type, 'N') <> 'D'
/

COMMENT ON TABLE VDA_V_LIBRARY_FORMS IS 'This view provides access to the library forms information'
/

COMMENT ON COLUMN VDA_V_LIBRARY_FORMS.FORM_ESIGN_REQUIRED IS 'Is eSign required for the form response'
/

COMMENT ON COLUMN VDA_V_LIBRARY_FORMS.FORM_TYPE IS 'The Form Type'
/

COMMENT ON COLUMN VDA_V_LIBRARY_FORMS.FORM_NAME IS 'The name of the form'
/

COMMENT ON COLUMN VDA_V_LIBRARY_FORMS.FORM_DESC IS 'The form description'
/

COMMENT ON COLUMN VDA_V_LIBRARY_FORMS.FORM_STATUS IS 'The Form Status'
/

COMMENT ON COLUMN VDA_V_LIBRARY_FORMS.FORM_SHARED_WITH IS 'The Form Sharing option'
/

COMMENT ON COLUMN VDA_V_LIBRARY_FORMS.FK_ACCOUNT IS 'The account the site is linked with'
/

COMMENT ON COLUMN VDA_V_LIBRARY_FORMS.PK_FORMLIB IS 'The PK of the form record'
/

COMMENT ON COLUMN VDA_V_LIBRARY_FORMS.RID IS 'The unique Row ID for Audit'
/

COMMENT ON COLUMN VDA_V_LIBRARY_FORMS.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_LIBRARY_FORMS.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_LIBRARY_FORMS.LAST_MODIFIED_DATE IS 'The date the record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_LIBRARY_FORMS.CREATED_ON IS 'The date the record was created on (Audit)'
/


--
-- VDA_V_LIBCAL_SUBJCOST  (View) 
--
--  Dependencies: 
--   SCH_SUBCOST_ITEM_VISIT (Table)
--   SCH_SUBCOST_ITEM (Table)
--   SCH_PROTOCOL_VISIT (Table)
--   EVENT_DEF (Table)
--   SCH_CODELST (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_LIBCAL_SUBJCOST
(PK_SUBCOST_ITEM, FK_CALENDAR, NAME, SUBCOST_ITEM_NAME, SUBCOST_ITEM_COST, 
 SUBCOST_ITEM_UNIT, SUBCOST_CATEGORY, VISIT_NAME, FK_ACCOUNT)
AS 
SELECT   PK_SUBCOST_ITEM,
            FK_CALENDAR,
            e.name,
            SUBCOST_ITEM_NAME,
            SUBCOST_ITEM_COST,
            SUBCOST_ITEM_UNIT,
            (SELECT   codelst_desc
               FROM   esch.sch_codelst
              WHERE   pk_codelst = FK_CODELST_CATEGORY)
               SUBCOST_CATEGORY,
            (SELECT   visit_name
               FROM   esch.sch_protocol_visit
              WHERE   pk_protocol_visit = FK_PROTOCOL_VISIT)
               visit_name,
            e.user_id
     FROM   esch.SCH_SUBCOST_ITEM s,
            esch.event_def e,
            esch.SCH_SUBCOST_ITEM_VISIT v
    WHERE   e.event_id = fk_calendar
            AND v.FK_SUBCOST_ITEM(+) = PK_SUBCOST_ITEM
/

COMMENT ON TABLE VDA_V_LIBCAL_SUBJCOST IS 'This view provides access to the Subject Cost Items linked with a Library Calendar'
/

COMMENT ON COLUMN VDA_V_LIBCAL_SUBJCOST.PK_SUBCOST_ITEM IS 'Primary Key of the Subject Cost record'
/

COMMENT ON COLUMN VDA_V_LIBCAL_SUBJCOST.FK_CALENDAR IS 'Foreign Key to the Calendar record'
/

COMMENT ON COLUMN VDA_V_LIBCAL_SUBJCOST.NAME IS 'Name of the Calendar the subject Cost Item is linked with'
/

COMMENT ON COLUMN VDA_V_LIBCAL_SUBJCOST.SUBCOST_ITEM_NAME IS 'Subject Cost Item Name'
/

COMMENT ON COLUMN VDA_V_LIBCAL_SUBJCOST.SUBCOST_ITEM_COST IS 'Subject Cost Item Cost value'
/

COMMENT ON COLUMN VDA_V_LIBCAL_SUBJCOST.SUBCOST_ITEM_UNIT IS 'Number of Units planned'
/

COMMENT ON COLUMN VDA_V_LIBCAL_SUBJCOST.SUBCOST_CATEGORY IS 'Subject Cost Item Category'
/

COMMENT ON COLUMN VDA_V_LIBCAL_SUBJCOST.VISIT_NAME IS 'Name of the visit the cost item is linked with'
/

COMMENT ON COLUMN VDA_V_LIBCAL_SUBJCOST.FK_ACCOUNT IS 'The account the subject cost item is linked with'
/


--
-- VDA_V_INV_AGED_RECEIVABLES  (View) 
--
--  Dependencies: 
--   ER_INVOICE_DETAIL (Table)
--   ER_INVOICE (Table)
--   ER_STUDY (Table)
--   F_GET_CODELSTDESC (Function)
--   F_CALC_DAYS (Function)
--   ER_MILEPAYMENT_DETAILS (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_INV_AGED_RECEIVABLES
(PK_INVOICE, STUDY_NUMBER, INVOICE_DATE, INVOICE_DUEDATE, STUDY_SPONSOR, 
 INV_NUMBER, AMOUNT_OVERDUE, "0_30D_OVERDUE", "31_60D_OVERDUE", "61_90D_OVERDUE", 
 OVER_90D_OVERDUE, INVOICE_AMOUNT, PAYMENT, PK_STUDY)
AS 
SELECT   PK_INVOICE,
              STUDY_NUMBER,
              INV_DATE INVOICE_DATE,
              (INV_DATE + eres.F_CALC_DAYS (INV_PAYUNIT, INV_PAYMENT_DUEBY))
                 AS INVOICE_DUEDATE,
              DECODE (FK_CODELST_SPONSOR,
                      NULL, STUDY_SPONSOR,
                      eres.F_GET_CODELSTDESC (FK_CODELST_SPONSOR))
                 STUDY_SPONSOR,
              INV_NUMBER,
              (SUM (NVL (AMOUNT_INVOICED, 0))
               - NVL (
                    (SELECT   SUM (MP_AMOUNT)
                       FROM   eres.ER_MILEPAYMENT_DETAILS
                      WHERE   MP_LINKTO_ID = PK_INVOICE
                              AND MP_LINKTO_TYPE = 'I'),
                    0
                 ))
                 AS AMOUNT_OVERDUE,
              CASE
                 WHEN (TRUNC(SYSDATE
                             - (SELECT   (INV_DATE
                                          + eres.F_CALC_DAYS (
                                               INV_PAYUNIT,
                                               INV_PAYMENT_DUEBY
                                            ))
                                            AS INVDUEDATE
                                  FROM   eres.ER_INVOICE
                                 WHERE   PK_INVOICE = INV.PK_INVOICE))) BETWEEN 0
                                                                            AND  30
                 THEN
                    (SUM (NVL (AMOUNT_INVOICED, 0))
                     - NVL (
                          (SELECT   SUM (MP_AMOUNT)
                             FROM   eres.ER_MILEPAYMENT_DETAILS
                            WHERE   MP_LINKTO_ID = PK_INVOICE
                                    AND MP_LINKTO_TYPE = 'I'),
                          0
                       ))
                 ELSE
                    0
              END
                 AS "0_30D_OVERDUE",
              CASE
                 WHEN (TRUNC(SYSDATE
                             - (SELECT   (INV_DATE
                                          + eres.F_CALC_DAYS (
                                               INV_PAYUNIT,
                                               INV_PAYMENT_DUEBY
                                            ))
                                            AS INVDUEDATE
                                  FROM   eres.ER_INVOICE
                                 WHERE   PK_INVOICE = INV.PK_INVOICE))) BETWEEN 31
                                                                            AND  60
                 THEN
                    (SUM (NVL (AMOUNT_INVOICED, 0))
                     - NVL (
                          (SELECT   SUM (MP_AMOUNT)
                             FROM   eres.ER_MILEPAYMENT_DETAILS
                            WHERE   MP_LINKTO_ID = PK_INVOICE
                                    AND MP_LINKTO_TYPE = 'I'),
                          0
                       ))
                 ELSE
                    0
              END
                 AS "31_60D_OVERDUE",
              CASE
                 WHEN (TRUNC(SYSDATE
                             - (SELECT   (INV_DATE
                                          + eres.F_CALC_DAYS (
                                               INV_PAYUNIT,
                                               INV_PAYMENT_DUEBY
                                            ))
                                            AS INVDUEDATE
                                  FROM   eres.ER_INVOICE
                                 WHERE   PK_INVOICE = INV.PK_INVOICE))) BETWEEN 61
                                                                            AND  90
                 THEN
                    ( (SUM (NVL (AMOUNT_INVOICED, 0)))
                     - NVL (
                          (SELECT   SUM (MP_AMOUNT)
                             FROM   eres.ER_MILEPAYMENT_DETAILS
                            WHERE   MP_LINKTO_ID = PK_INVOICE
                                    AND MP_LINKTO_TYPE = 'I'),
                          0
                       ))
                 ELSE
                    0
              END
                 AS "61_90D_OVERDUE",
              CASE
                 WHEN (TRUNC(SYSDATE
                             - (SELECT   (INV_DATE
                                          + eres.F_CALC_DAYS (
                                               INV_PAYUNIT,
                                               INV_PAYMENT_DUEBY
                                            ))
                                            AS INVDUEDATE
                                  FROM   eres.ER_INVOICE
                                 WHERE   PK_INVOICE = INV.PK_INVOICE))) > 90
                 THEN
                    (SUM (NVL (AMOUNT_INVOICED, 0))
                     - NVL (
                          (SELECT   SUM (MP_AMOUNT)
                             FROM   eres.ER_MILEPAYMENT_DETAILS
                            WHERE   MP_LINKTO_ID = PK_INVOICE
                                    AND MP_LINKTO_TYPE = 'I'),
                          0
                       ))
                 ELSE
                    0
              END
                 AS "OVER_90D_OVERDUE",
              (SUM (NVL (AMOUNT_INVOICED, 0))) AS INVOICE_AMOUNT,
              NVL (
                 (SELECT   SUM (MP_AMOUNT)
                    FROM   eres.ER_MILEPAYMENT_DETAILS
                   WHERE   MP_LINKTO_ID = PK_INVOICE AND MP_LINKTO_TYPE = 'I'),
                 0
              )
                 AS PAYMENT, PK_STUDY 
       FROM   eres.ER_STUDY STUDY,
              eres.ER_INVOICE INV,
              eres.ER_INVOICE_DETAIL INVDETAIL
      WHERE       STUDY.PK_STUDY = INV.FK_STUDY
              AND INV.PK_INVOICE = INVDETAIL.FK_INV
              AND INVDETAIL.DETAIL_TYPE = 'H'
   GROUP BY   PK_INVOICE,
              STUDY_NUMBER,
              INV_DATE,
              INV_DATE + eres.F_CALC_DAYS (INV_PAYUNIT, INV_PAYMENT_DUEBY),
              DECODE (FK_CODELST_SPONSOR,
                      NULL, STUDY_SPONSOR,
                      eres.F_GET_CODELSTDESC (FK_CODELST_SPONSOR)),
              INV_NUMBER, PK_STUDY
     HAVING   (SUM (NVL (AMOUNT_INVOICED, 0))
               - NVL (
                    (SELECT   SUM (MP_AMOUNT)
                       FROM   eres.ER_MILEPAYMENT_DETAILS
                      WHERE   MP_LINKTO_ID = PK_INVOICE
                              AND MP_LINKTO_TYPE = 'I'),
                    0
                 )) > 0
/

COMMENT ON TABLE VDA_V_INV_AGED_RECEIVABLES IS 'This view provides access to aged 
receivables information. The receivables are calculated by linking with payments that are 
reconciled with invoives and eliminating any invoice that is fully reconciled with a 
payment.'
/

COMMENT ON COLUMN VDA_V_INV_AGED_RECEIVABLES.PK_STUDY IS 'Primary Key of the Study'
/

COMMENT ON COLUMN VDA_V_INV_AGED_RECEIVABLES.PK_INVOICE IS 'Primary Key of Invoice'
/

COMMENT ON COLUMN VDA_V_INV_AGED_RECEIVABLES.STUDY_NUMBER IS 'The Study Number'
/

COMMENT ON COLUMN VDA_V_INV_AGED_RECEIVABLES.INVOICE_DATE IS 'The Invoice Due Date'
/

COMMENT ON COLUMN VDA_V_INV_AGED_RECEIVABLES.STUDY_SPONSOR IS 'The Study Sponsor (from 
summary), if study sponsor field is empty, it gets ''if other'''
/

COMMENT ON COLUMN VDA_V_INV_AGED_RECEIVABLES.INV_NUMBER IS 'The Invoice Number'
/

COMMENT ON COLUMN VDA_V_INV_AGED_RECEIVABLES.AMOUNT_OVERDUE IS 'The amount overdue for the 
invoice'
/

COMMENT ON COLUMN VDA_V_INV_AGED_RECEIVABLES."0_30D_OVERDUE" IS 'The amount overdue for 0
-30 days'
/

COMMENT ON COLUMN VDA_V_INV_AGED_RECEIVABLES."31_60D_OVERDUE" IS 'The amount overdue for 
31-60  days'
/

COMMENT ON COLUMN VDA_V_INV_AGED_RECEIVABLES."61_90D_OVERDUE" IS 'The amount overdue for 
61-90 days'
/

COMMENT ON COLUMN VDA_V_INV_AGED_RECEIVABLES.OVER_90D_OVERDUE IS 'The amount overdue 
forover 90 days'
/

COMMENT ON COLUMN VDA_V_INV_AGED_RECEIVABLES.INVOICE_AMOUNT IS 'The original invoice 
amount'
/

COMMENT ON COLUMN VDA_V_INV_AGED_RECEIVABLES.PAYMENT IS 'The payment amount (if any) 
reconciled with the invoice'
/


--
-- VDA_V_INVOICE_PAYMENTS_DETAILS  (View) 
--
--  Dependencies: 
--   F_GET_CODELSTDESC (Function)
--   ER_MILEPAYMENT_DETAILS (Table)
--   VDA_V_PAYMENTS (View)
--
CREATE OR REPLACE FORCE VIEW VDA_V_INVOICE_PAYMENTS_DETAILS
(STUDY_NUMBER, STUDY_TITLE, MSPAY_DATE, MSPAY_TYPE, MSPAY_AMT_RECVD, 
 MSPAY_DESC, MSPAY_COMMENTS, PK_MILEPAYMENT, CREATOR, LAST_MODIFIED_BY, 
 LAST_MODIFIED_DATE, CREATED_ON, FK_STUDY, FK_ACCOUNT, FK_INVOICE, 
 MP_AMOUNT, CREATOR_FK, LAST_MODIFIED_BY_FK)
AS 
SELECT   "STUDY_NUMBER",
            "STUDY_TITLE",
            "MSPAY_DATE",
            "MSPAY_TYPE",
            "MSPAY_AMT_RECVD",
            "MSPAY_DESC",
            "MSPAY_COMMENTS",
            "PK_MILEPAYMENT",
            "CREATOR",
            "LAST_MODIFIED_BY",
            "LAST_MODIFIED_DATE",
            "CREATED_ON",
            "FK_STUDY",
            "FK_ACCOUNT",
            "FK_INVOICE",
            "MP_AMOUNT", VDA_V_PAYMENTS.CREATOR_FK,  VDA_V_PAYMENTS.LAST_MODIFIED_BY_FK
     FROM   VDA_V_PAYMENTS,
            (  SELECT   MP_LINKTO_ID fk_invoice,
                        fk_milepayment,
                        SUM (MP_AMOUNT) MP_AMOUNT
                 FROM   eres.ER_MILEPAYMENT_DETAILS
                WHERE   MP_LINKTO_TYPE = 'I'
             GROUP BY   MP_LINKTO_ID, fk_milepayment) x
    WHERE   PK_MILEPAYMENT = FK_MILEPAYMENT
/

COMMENT ON TABLE VDA_V_INVOICE_PAYMENTS_DETAILS IS 'This view provides access to the 
payment details information for payments linked with invoices'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.STUDY_NUMBER IS 'The Study Number of the 
study the payment  is linked with'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.STUDY_TITLE IS 'The Study Title of the 
study the payment is linked with'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.MSPAY_DATE IS 'The payment date'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.MSPAY_TYPE IS 'The payment type'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.MSPAY_AMT_RECVD IS 'The payment amount 
received'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.MSPAY_DESC IS 'The payment description'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.MSPAY_COMMENTS IS 'The payment comments'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.PK_MILEPAYMENT IS 'The Primary Key of the 
payment record'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.CREATOR IS 'The user who created the 
record (Audit)'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.LAST_MODIFIED_BY IS 'The user who last 
modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.LAST_MODIFIED_DATE IS 'The date the 
record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.CREATED_ON IS 'The user who created the 
record (Audit)'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.FK_STUDY IS 'The Foreign Key to the study 
milestone is linked with'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.FK_ACCOUNT IS 'The account the milestone 
is linked with'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.FK_INVOICE IS 'The PK to uniquely 
identify invoice the payment is linked with'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.MP_AMOUNT IS 'The Payment amount linked 
with the invoice'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.CREATOR_FK IS 'The Key to identify the 
Creator of the record.'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.LAST_MODIFIED_BY_FK IS 'The Key to 
identify the Last Modified By of the record.'
/


--
-- VDA_V_INVOICE_PAYMENTS  (View) 
--
--  Dependencies: 
--   ER_INVOICE_DETAIL (Table)
--   ER_INVOICE (Table)
--   ER_STUDY (Table)
--   F_GET_CODELSTDESC (Function)
--   ER_MILEPAYMENT_DETAILS (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_INVOICE_PAYMENTS
(PK_INVOICE, STUDY_NUMBER, STUDY_SPONSOR, INV_NUMBER, INV_DATE, 
 INVOICE_AMOUNT, INVOICE_AMOUNT_BALANCE, PAYMENTS)
AS 
SELECT   PK_INVOICE,
              STUDY_NUMBER,
              decode(FK_CODELST_SPONSOR,null,STUDY_SPONSOR,eres.F_GET_CODELSTDESC (FK_CODELST_SPONSOR)) STUDY_SPONSOR, 
              INV_NUMBER,
              INV_DATE,
              (SUM (NVL (AMOUNT_INVOICED, 0))) AS INVOICE_AMOUNT,
              (SUM (NVL (AMOUNT_INVOICED, 0))
               - NVL (
                    (SELECT   SUM (MP_AMOUNT)
                       FROM   eres.ER_MILEPAYMENT_DETAILS
                      WHERE   MP_LINKTO_ID = PK_INVOICE
                              AND MP_LINKTO_TYPE = 'I'),
                    0
                 ))
                 AS INVOICE_AMOUNT_BALANCE,
              NVL (
                 (SELECT   SUM (MP_AMOUNT)
                    FROM   eres.ER_MILEPAYMENT_DETAILS
                   WHERE   MP_LINKTO_ID = PK_INVOICE AND MP_LINKTO_TYPE = 'I'),
                 0
              )
                 AS PAYMENTS
       FROM   eres.ER_STUDY STUDY,
              eres.ER_INVOICE INV,
              eres.ER_INVOICE_DETAIL INVDETAIL
      WHERE       STUDY.PK_STUDY = INV.FK_STUDY
              AND INV.PK_INVOICE = INVDETAIL.FK_INV
              AND INVDETAIL.DETAIL_TYPE = 'H'
   GROUP BY   PK_INVOICE,
              STUDY_NUMBER,
              decode(FK_CODELST_SPONSOR,null,STUDY_SPONSOR,eres.F_GET_CODELSTDESC (FK_CODELST_SPONSOR)),
              INV_NUMBER,
              INV_DATE
/

COMMENT ON TABLE VDA_V_INVOICE_PAYMENTS IS 'This view provides access to invoice and payment summary'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS.PK_INVOICE IS 'Primary Key of Invoice'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS.STUDY_NUMBER IS 'The Study Number'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS.STUDY_SPONSOR IS 'The Study Sponsor (from summary), if study sponsor field is empty, it gets ''if other'''
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS.INV_NUMBER IS 'The Invoice Number'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS.INV_DATE IS 'The invoive Date'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS.INVOICE_AMOUNT IS 'The original invoice amount'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS.INVOICE_AMOUNT_BALANCE IS 'The amount balance for the invoice'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS.PAYMENTS IS 'The payment amount (if any) reconciled with the invoice'
/


--
-- VDA_V_GROUP_USERS  (View) 
--
--  Dependencies: 
--   ER_GRPS (Table)
--   ER_USRGRP (Table)
--   ER_USER (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_GROUP_USERS
(PK_USRGRP, FK_USER, FK_GRP, RID, CREATOR, 
 LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, USR_LASTNAME, 
 USR_FIRSTNAME, GROUP_NAME, FK_ACCOUNT)
AS 
SELECT   PK_USRGRP,
            FK_USER,
            FK_GRP,
            g.RID,
            g.CREATOR,
            g.LAST_MODIFIED_BY,
            g.LAST_MODIFIED_DATE,
            g.CREATED_ON,
            g.IP_ADD,
            usr_lastname,
            usr_firstname,
            (SELECT   grp_name
               FROM   eres.er_grps
              WHERE   pk_grp = FK_GRP)
               group_name,
            u.fk_account
     FROM   eres.ER_USRGRP g, eres.er_user u
    WHERE   pk_user = FK_USER
/

COMMENT ON TABLE VDA_V_GROUP_USERS IS 'This provides access the group-user relationship: all users in a group'
/

COMMENT ON COLUMN VDA_V_GROUP_USERS.PK_USRGRP IS 'The Primary key'
/

COMMENT ON COLUMN VDA_V_GROUP_USERS.FK_USER IS 'The PK of er_user'
/

COMMENT ON COLUMN VDA_V_GROUP_USERS.FK_GRP IS 'The PK of er_grps'
/

COMMENT ON COLUMN VDA_V_GROUP_USERS.RID IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database.'
/

COMMENT ON COLUMN VDA_V_GROUP_USERS.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_GROUP_USERS.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_GROUP_USERS.LAST_MODIFIED_DATE IS 'The date the  record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_GROUP_USERS.CREATED_ON IS 'The date the record was created on (Audit)'
/

COMMENT ON COLUMN VDA_V_GROUP_USERS.IP_ADD IS 'The IP ADDRESS of the client machine (Audit)'
/

COMMENT ON COLUMN VDA_V_GROUP_USERS.USR_LASTNAME IS 'The last name of the user'
/

COMMENT ON COLUMN VDA_V_GROUP_USERS.USR_FIRSTNAME IS 'The first name of the user'
/

COMMENT ON COLUMN VDA_V_GROUP_USERS.GROUP_NAME IS 'The name of the group'
/

COMMENT ON COLUMN VDA_V_GROUP_USERS.FK_ACCOUNT IS 'The account the study site is linked with'
/


--
-- VDA_V_GROUPS  (View) 
--
--  Dependencies: 
--   ER_GRPS (Table)
--   ER_CODELST (Table)
--   ER_USER (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_GROUPS
(PK_GRP, GRP_NAME, GRP_DESC, RID, CREATOR, 
 LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, FK_ACCOUNT, IP_ADD, 
 IS_SUPERUSER_GRP, IS_BUD_SUPERUSER_GRP, IS_GRP_HIDDEN, GRP_STUDYTEAM_ROLE)
AS 
SELECT   PK_GRP,
            GRP_NAME,
            GRP_DESC,
            RID,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   eres.ER_USER
              WHERE   pk_user = er_grps.CREATOR)
               creator,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   eres.ER_USER
              WHERE   pk_user = er_grps.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            LAST_MODIFIED_DATE,
            CREATED_ON,
            FK_ACCOUNT,
            IP_ADD,
            DECODE (GRP_SUPUSR_FLAG,
                    0,
                    'No',
                    1,
                    'Yes')
               IS_SUPERUSER_GRP,
            DECODE (GRP_SUPBUD_FLAG,
                    0,
                    'No',
                    1,
                    'Yes')
               IS_BUD_SUPERUSER_GRP,
            DECODE (GRP_HIDDEN,
                    0,
                    'No',
                    1,
                    'Yes')
               IS_GRP_HIDDEN,
            (SELECT   codelst_desc
               FROM   eres.er_codelst
              WHERE   pk_codelst = FK_CODELST_ST_ROLE)
               GRP_STUDYTEAM_ROLE
     FROM   eres.er_grps
/

COMMENT ON TABLE VDA_V_GROUPS IS 'This view provides access to all groups'
/

COMMENT ON COLUMN VDA_V_GROUPS.PK_GRP IS 'The Primary Key to uniquely identify the groups created in the database.'
/

COMMENT ON COLUMN VDA_V_GROUPS.GRP_NAME IS 'The group name'
/

COMMENT ON COLUMN VDA_V_GROUPS.GRP_DESC IS 'the group description'
/

COMMENT ON COLUMN VDA_V_GROUPS.RID IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database.'
/

COMMENT ON COLUMN VDA_V_GROUPS.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_GROUPS.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_GROUPS.LAST_MODIFIED_DATE IS 'The date the  record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_GROUPS.CREATED_ON IS 'The date the record was created on (Audit)'
/

COMMENT ON COLUMN VDA_V_GROUPS.FK_ACCOUNT IS 'The account the group is linked with'
/

COMMENT ON COLUMN VDA_V_GROUPS.IP_ADD IS 'The IP ADDRESS of the client machine (Audit)'
/

COMMENT ON COLUMN VDA_V_GROUPS.IS_SUPERUSER_GRP IS 'Indicates if the group is a super user group'
/

COMMENT ON COLUMN VDA_V_GROUPS.IS_BUD_SUPERUSER_GRP IS 'Indicates if the group is a super user group for budgets access'
/

COMMENT ON COLUMN VDA_V_GROUPS.IS_GRP_HIDDEN IS 'Indicates if this Group is hidden in Group and User selection Lookups'
/

COMMENT ON COLUMN VDA_V_GROUPS.GRP_STUDYTEAM_ROLE IS 'This is the Foreign Key to Study Team ROLE code in er_codelst'
/


--
-- VDA_V_ESCH_DELETELOG  (View) 
--
--  Dependencies: 
--   AUDIT_DELETELOG (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_ESCH_DELETELOG
(PK_APP_DL, TABLE_NAME, TABLE_PK, TABLE_RID, DELETED_BY, 
 DELETED_ON, APP_MODULE, IP_ADDRESS, REASON_FOR_DELETION)
AS 
SELECT   PK_APP_DL,
            TABLE_NAME,
            TABLE_PK,
            TABLE_RID,
            DELETED_BY,
            DELETED_ON,
            APP_MODULE,
            IP_ADDRESS,
            REASON_FOR_DELETION
     FROM   esch.audit_deletelog
/

COMMENT ON TABLE VDA_V_ESCH_DELETELOG IS 'This view provides data on all delete transactions from Velos eResearch esch database schema'
/

COMMENT ON COLUMN VDA_V_ESCH_DELETELOG.PK_APP_DL IS 'Primary key'
/

COMMENT ON COLUMN VDA_V_ESCH_DELETELOG.TABLE_NAME IS 'Name of the database table'
/

COMMENT ON COLUMN VDA_V_ESCH_DELETELOG.TABLE_PK IS 'The DELETED ROW''s Primary Key'
/

COMMENT ON COLUMN VDA_V_ESCH_DELETELOG.TABLE_RID IS 'The DELETED ROW''s RID'
/

COMMENT ON COLUMN VDA_V_ESCH_DELETELOG.DELETED_BY IS 'The PK of the er_user, user who deleted the row'
/

COMMENT ON COLUMN VDA_V_ESCH_DELETELOG.DELETED_ON IS 'Stores the date of deletion'
/

COMMENT ON COLUMN VDA_V_ESCH_DELETELOG.APP_MODULE IS 'Stores the Module name'
/

COMMENT ON COLUMN VDA_V_ESCH_DELETELOG.IP_ADDRESS IS 'Stores the IP address of user'
/

COMMENT ON COLUMN VDA_V_ESCH_DELETELOG.REASON_FOR_DELETION IS 'Reason why the row was deleted'
/


--
-- VDA_V_ESCH_AUDIT_UPDATE  (View) 
--
--  Dependencies: 
--   AUDIT_ROW (Table)
--   AUDIT_COLUMN (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_ESCH_AUDIT_UPDATE
(USER_NAME, TIMESTAMP, TABLE_NAME, COLUMN_NAME, OLD_VALUE, 
 NEW_VALUE, REMARKS, CAID, RAID, RID, 
 ACTION)
AS 
SELECT   r.user_name,
            r.timestamp,
            r.table_name,
            c.column_name,
            c.old_value,
            c.new_value,
            c.remarks,
            c.caid,
            r.raid, r.RID, r.ACTION
     FROM   esch.audit_column c, esch.audit_row r
    WHERE   r.action = 'U' AND c.raid = r.raid
/

COMMENT ON TABLE VDA_V_ESCH_AUDIT_UPDATE IS 'This view provides access to the detail audit 
trail on Update actions in ESCH schema'
/

COMMENT ON COLUMN VDA_V_ESCH_AUDIT_UPDATE.USER_NAME IS 'The user who triggered the action'
/

COMMENT ON COLUMN VDA_V_ESCH_AUDIT_UPDATE.TIMESTAMP IS 'The date and time of the action'
/

COMMENT ON COLUMN VDA_V_ESCH_AUDIT_UPDATE.TABLE_NAME IS 'The name of the table'
/

COMMENT ON COLUMN VDA_V_ESCH_AUDIT_UPDATE.COLUMN_NAME IS 'The name of the column updated 
by the action'
/

COMMENT ON COLUMN VDA_V_ESCH_AUDIT_UPDATE.OLD_VALUE IS 'The old value of the column'
/

COMMENT ON COLUMN VDA_V_ESCH_AUDIT_UPDATE.NEW_VALUE IS 'The new value of the column'
/

COMMENT ON COLUMN VDA_V_ESCH_AUDIT_UPDATE.REMARKS IS 'The reason for change comments 
linked with the action'
/

COMMENT ON COLUMN VDA_V_ESCH_AUDIT_UPDATE.CAID IS 'The Primary Key of the audit column 
change'
/

COMMENT ON COLUMN VDA_V_ESCH_AUDIT_UPDATE.RAID IS 'The Primary Key of the audit 
transaction'
/

COMMENT ON COLUMN VDA_V_ESCH_AUDIT_UPDATE.RID IS 'The Unique Row ID for the table row'
/

COMMENT ON COLUMN VDA_V_ESCH_AUDIT_UPDATE.ACTION IS 'The Type of audit transaction. From 
eResearch version 9.2, any new audit data created will be stored in audit_column for all 
transactions. I=Insert,D=Delete and U=Update'
/


--
-- VDA_V_ESCH_AUDIT_INSERT  (View) 
--
--  Dependencies: 
--   AUDIT_ROW (Table)
--   AUDIT_INSERT (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_ESCH_AUDIT_INSERT
(USER_NAME, TIMESTAMP, TABLE_NAME, ROW_DATA)
AS 
SELECT   r.user_name,
            r.timestamp,
            r.table_name,
            c.row_data
     FROM   esch.audit_insert c, esch.audit_row r
    WHERE   r.action = 'I' AND c.raid = r.raid
/

COMMENT ON TABLE VDA_V_ESCH_AUDIT_INSERT IS 'This view provides access to the detail audit trail on Insert actions in ESCH schema'
/

COMMENT ON COLUMN VDA_V_ESCH_AUDIT_INSERT.USER_NAME IS 'The user who triggered the action'
/

COMMENT ON COLUMN VDA_V_ESCH_AUDIT_INSERT.TIMESTAMP IS 'The date and time of the action'
/

COMMENT ON COLUMN VDA_V_ESCH_AUDIT_INSERT.TABLE_NAME IS 'The name of the table'
/

COMMENT ON COLUMN VDA_V_ESCH_AUDIT_INSERT.ROW_DATA IS 'The pipe delimited data at the time of insert, The order of the column is same as the order of the columns in the table'
/


--
-- VDA_V_ESCH_AUDIT_DELETE  (View) 
--
--  Dependencies: 
--   AUDIT_ROW (Table)
--   AUDIT_DELETE (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_ESCH_AUDIT_DELETE
(USER_NAME, TIMESTAMP, TABLE_NAME, ROW_DATA)
AS 
SELECT   r.user_name,
            r.timestamp,
            r.table_name,
            c.row_data
     FROM   esch.audit_delete c, esch.audit_row r
    WHERE   r.action = 'D' AND c.raid = r.raid
/

COMMENT ON TABLE VDA_V_ESCH_AUDIT_DELETE IS 'This view provides access to the detail audit trail on Delete actions in ESCH schema'
/

COMMENT ON COLUMN VDA_V_ESCH_AUDIT_DELETE.USER_NAME IS 'The user who triggered the action'
/

COMMENT ON COLUMN VDA_V_ESCH_AUDIT_DELETE.TIMESTAMP IS 'The date and time of the action'
/

COMMENT ON COLUMN VDA_V_ESCH_AUDIT_DELETE.TABLE_NAME IS 'The name of the table'
/

COMMENT ON COLUMN VDA_V_ESCH_AUDIT_DELETE.ROW_DATA IS 'The pipe delimited data at the time of delete, The order of the column is same as the order of the columns in the table'
/


--
-- VDA_V_ESCH_AUDITROW  (View) 
--
--  Dependencies: 
--   AUDIT_ROW (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_ESCH_AUDITROW
(RAID, TABLE_NAME, RID, ACTION, TIMESTAMP, 
 USER_NAME)
AS 
SELECT   "RAID",
            "TABLE_NAME",
            "RID",
            "ACTION",
            "TIMESTAMP",
            "USER_NAME"
     FROM   esch.audit_row
/

COMMENT ON TABLE VDA_V_ESCH_AUDITROW IS 'This view provides access to the high level audit trail on Insert, Update and Delete actions in ESCH schema'
/

COMMENT ON COLUMN VDA_V_ESCH_AUDITROW.RAID IS 'The Unique audit Primary key'
/

COMMENT ON COLUMN VDA_V_ESCH_AUDITROW.TABLE_NAME IS 'The name of the table'
/

COMMENT ON COLUMN VDA_V_ESCH_AUDITROW.RID IS 'The unique RID of the table row'
/

COMMENT ON COLUMN VDA_V_ESCH_AUDITROW.ACTION IS 'The action, I:Insert,D:Delete and U:Update'
/

COMMENT ON COLUMN VDA_V_ESCH_AUDITROW.TIMESTAMP IS 'The date and time of the action'
/

COMMENT ON COLUMN VDA_V_ESCH_AUDITROW.USER_NAME IS 'The user who triggered the action'
/


--
-- VDA_V_ER_CODELST  (View) 
--
--  Dependencies: 
--   ER_CODELST (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_ER_CODELST
(PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, 
 CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
AS 
SELECT   PK_CODELST,
            CODELST_TYPE,
            CODELST_SUBTYP,
            CODELST_DESC,
            CODELST_HIDE,
            CODELST_SEQ,
            CODELST_CUSTOM_COL,
            CODELST_CUSTOM_COL1,
            CODELST_STUDY_ROLE
     FROM   eres.er_codelst
/

COMMENT ON TABLE VDA_V_ER_CODELST IS 'This view provides access to all the codes (eres schema) used in the application. Mostly, these codes are available in the form of ''Dropdown'' data.'
/

COMMENT ON COLUMN VDA_V_ER_CODELST.PK_CODELST IS 'This uniquely identifies the code. Primary key of the table'
/

COMMENT ON COLUMN VDA_V_ER_CODELST.CODELST_TYPE IS 'The code type to categorize the codes'
/

COMMENT ON COLUMN VDA_V_ER_CODELST.CODELST_SUBTYP IS 'The second level categorization of the code. This is in addition to the code type column and is sometimes used in the application for specific purposes'
/

COMMENT ON COLUMN VDA_V_ER_CODELST.CODELST_DESC IS 'The Description of the code. This value is shown in the application instead of the code'
/

COMMENT ON COLUMN VDA_V_ER_CODELST.CODELST_HIDE IS 'A flag (Y/N) to indicate if the value has to be displayed or not. Default N'
/

COMMENT ON COLUMN VDA_V_ER_CODELST.CODELST_SEQ IS 'This number indicates the sequence in which the values for a particular code type/sub-type will be displayed in the application'
/

COMMENT ON COLUMN VDA_V_ER_CODELST.CODELST_CUSTOM_COL IS 'This column can be used to establish relationships between codes or some specific attributes.'
/

COMMENT ON COLUMN VDA_V_ER_CODELST.CODELST_CUSTOM_COL1 IS 'This column can be used to establish relationships between codes or some specific attributes. In code_type=''studyidtype'', this stores the dropdown html structure'
/

COMMENT ON COLUMN VDA_V_ER_CODELST.CODELST_STUDY_ROLE IS 'This is used to store the Study Team Roles that may have access to the code'
/


--
-- VDA_V_ERES_DELETELOG  (View) 
--
--  Dependencies: 
--   AUDIT_DELETELOG (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_ERES_DELETELOG
(PK_APP_DL, TABLE_NAME, TABLE_PK, TABLE_RID, DELETED_BY, 
 DELETED_ON, APP_MODULE, IP_ADDRESS, REASON_FOR_DELETION)
AS 
SELECT   PK_APP_DL,
            TABLE_NAME,
            TABLE_PK,
            TABLE_RID,
            DELETED_BY,
            DELETED_ON,
            APP_MODULE,
            IP_ADDRESS,
            REASON_FOR_DELETION
     FROM   eres.audit_deletelog
/

COMMENT ON TABLE VDA_V_ERES_DELETELOG IS 'This view provides data on all delete transactions from Velos eResearch eres database schema'
/

COMMENT ON COLUMN VDA_V_ERES_DELETELOG.PK_APP_DL IS 'Primary key'
/

COMMENT ON COLUMN VDA_V_ERES_DELETELOG.TABLE_NAME IS 'Name of the database table'
/

COMMENT ON COLUMN VDA_V_ERES_DELETELOG.TABLE_PK IS 'The DELETED ROW''s Primary Key'
/

COMMENT ON COLUMN VDA_V_ERES_DELETELOG.TABLE_RID IS 'The DELETED ROW''s RID'
/

COMMENT ON COLUMN VDA_V_ERES_DELETELOG.DELETED_BY IS 'The PK of the er_user, user who deleted the row'
/

COMMENT ON COLUMN VDA_V_ERES_DELETELOG.DELETED_ON IS 'Stores the date of deletion'
/

COMMENT ON COLUMN VDA_V_ERES_DELETELOG.APP_MODULE IS 'Stores the Module name'
/

COMMENT ON COLUMN VDA_V_ERES_DELETELOG.IP_ADDRESS IS 'Stores the IP address of user'
/

COMMENT ON COLUMN VDA_V_ERES_DELETELOG.REASON_FOR_DELETION IS 'Reason why the row was deleted'
/


--
-- VDA_V_ERES_AUDIT_UPDATE  (View) 
--
--  Dependencies: 
--   AUDIT_ROW (Table)
--   AUDIT_COLUMN (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_ERES_AUDIT_UPDATE
(USER_NAME, TIMESTAMP, TABLE_NAME, COLUMN_NAME, OLD_VALUE, 
 NEW_VALUE, REMARKS, CAID, RAID, RID, 
 ACTION)
AS 
SELECT   r.user_name,
            r.timestamp,
            r.table_name,
            c.column_name,
            c.old_value,
            c.new_value,
            c.remarks,
            c.caid,
            r.raid, r.RID, r.ACTION
     FROM   eres.audit_column c, eres.audit_row r
    WHERE   r.action = 'U' AND c.raid = r.raid
/

COMMENT ON TABLE VDA_V_ERES_AUDIT_UPDATE IS 'This view provides access to the detail audit 
trail on Update actions in ERES schema'
/

COMMENT ON COLUMN VDA_V_ERES_AUDIT_UPDATE.USER_NAME IS 'The user who triggered the action'
/

COMMENT ON COLUMN VDA_V_ERES_AUDIT_UPDATE.TIMESTAMP IS 'The date and time of the action'
/

COMMENT ON COLUMN VDA_V_ERES_AUDIT_UPDATE.TABLE_NAME IS 'The name of the table'
/

COMMENT ON COLUMN VDA_V_ERES_AUDIT_UPDATE.COLUMN_NAME IS 'The name of the column updated 
by the action'
/

COMMENT ON COLUMN VDA_V_ERES_AUDIT_UPDATE.OLD_VALUE IS 'The old value of the column'
/

COMMENT ON COLUMN VDA_V_ERES_AUDIT_UPDATE.NEW_VALUE IS 'The new value of the column'
/

COMMENT ON COLUMN VDA_V_ERES_AUDIT_UPDATE.REMARKS IS 'The reason for change comments 
linked with the action'
/

COMMENT ON COLUMN VDA_V_ERES_AUDIT_UPDATE.CAID IS 'The Primary Key of the audit column 
change'
/

COMMENT ON COLUMN VDA_V_ERES_AUDIT_UPDATE.RAID IS 'The Primary Key of the audit 
transaction'
/

COMMENT ON COLUMN VDA_V_ERES_AUDIT_UPDATE.RID IS 'The Unique Row ID for the table row'
/

COMMENT ON COLUMN VDA_V_ERES_AUDIT_UPDATE.ACTION IS 'The Type of audit transaction. From 
eResearch version 9.2, any new audit data created will be stored in audit_column for all 
transactions. I=Insert,D=Delete and U=Update'
/


--
-- VDA_V_ERES_AUDIT_INSERT  (View) 
--
--  Dependencies: 
--   AUDIT_ROW (Table)
--   AUDIT_INSERT (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_ERES_AUDIT_INSERT
(USER_NAME, TIMESTAMP, TABLE_NAME, ROW_DATA)
AS 
SELECT   r.user_name,
            r.timestamp,
            r.table_name,
            c.row_data
     FROM   eres.audit_insert c, eres.audit_row r
    WHERE   r.action = 'I' AND c.raid = r.raid
/

COMMENT ON TABLE VDA_V_ERES_AUDIT_INSERT IS 'This view provides access to the detail audit trail on Insert actions in ERES schema'
/

COMMENT ON COLUMN VDA_V_ERES_AUDIT_INSERT.USER_NAME IS 'The user who triggered the action'
/

COMMENT ON COLUMN VDA_V_ERES_AUDIT_INSERT.TIMESTAMP IS 'The date and time of the action'
/

COMMENT ON COLUMN VDA_V_ERES_AUDIT_INSERT.TABLE_NAME IS 'The name of the table'
/

COMMENT ON COLUMN VDA_V_ERES_AUDIT_INSERT.ROW_DATA IS 'The pipe delimited data at the time of insert, The order of the column is same as the order of the columns in the table'
/


--
-- VDA_V_ERES_AUDIT_DELETE  (View) 
--
--  Dependencies: 
--   AUDIT_ROW (Table)
--   AUDIT_DELETE (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_ERES_AUDIT_DELETE
(USER_NAME, TIMESTAMP, TABLE_NAME, ROW_DATA)
AS 
SELECT   r.user_name,
            r.timestamp,
            r.table_name,
            c.row_data
     FROM   eres.audit_delete c, eres.audit_row r
    WHERE   r.action = 'D' AND c.raid = r.raid
/

COMMENT ON TABLE VDA_V_ERES_AUDIT_DELETE IS 'This view provides access to the detail audit trail on Delete actions in ERES schema'
/

COMMENT ON COLUMN VDA_V_ERES_AUDIT_DELETE.USER_NAME IS 'The user who triggered the action'
/

COMMENT ON COLUMN VDA_V_ERES_AUDIT_DELETE.TIMESTAMP IS 'The date and time of the action'
/

COMMENT ON COLUMN VDA_V_ERES_AUDIT_DELETE.TABLE_NAME IS 'The name of the table'
/

COMMENT ON COLUMN VDA_V_ERES_AUDIT_DELETE.ROW_DATA IS 'The pipe delimited data at the time of delete, The order of the column is same as the order of the columns in the table'
/


--
-- VDA_V_ERES_AUDITROW  (View) 
--
--  Dependencies: 
--   AUDIT_ROW (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_ERES_AUDITROW
(RAID, TABLE_NAME, RID, ACTION, TIMESTAMP, 
 USER_NAME)
AS 
SELECT   "RAID",
            "TABLE_NAME",
            "RID",
            "ACTION",
            "TIMESTAMP",
            "USER_NAME"
     FROM   eres.audit_row
/

COMMENT ON TABLE VDA_V_ERES_AUDITROW IS 'This view provides access to the high level audit trail on Insert, Update and Delete actions in ERES schema'
/

COMMENT ON COLUMN VDA_V_ERES_AUDITROW.RAID IS 'The Unique audit Primary key'
/

COMMENT ON COLUMN VDA_V_ERES_AUDITROW.TABLE_NAME IS 'The name of the table'
/

COMMENT ON COLUMN VDA_V_ERES_AUDITROW.RID IS 'The unique RID of the table row'
/

COMMENT ON COLUMN VDA_V_ERES_AUDITROW.ACTION IS 'The action, I:Insert,D:Delete and U:Update'
/

COMMENT ON COLUMN VDA_V_ERES_AUDITROW.TIMESTAMP IS 'The date and time of the action'
/

COMMENT ON COLUMN VDA_V_ERES_AUDITROW.USER_NAME IS 'The user who triggered the action'
/


--
-- VDA_V_EPAT_DELETELOG  (View) 
--
--  Dependencies: 
--   AUDIT_DELETELOG (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_EPAT_DELETELOG
(PK_APP_DL, TABLE_NAME, TABLE_PK, TABLE_RID, DELETED_BY, 
 DELETED_ON, APP_MODULE, IP_ADDRESS, REASON_FOR_DELETION)
AS 
SELECT   PK_APP_DL,
            TABLE_NAME,
            TABLE_PK,
            TABLE_RID,
            DELETED_BY,
            DELETED_ON,
            APP_MODULE,
            IP_ADDRESS,
            REASON_FOR_DELETION
     FROM   epat.audit_deletelog
/

COMMENT ON TABLE VDA_V_EPAT_DELETELOG IS 'This view provides data on all delete transactions from Velos eResearch epat database schema'
/

COMMENT ON COLUMN VDA_V_EPAT_DELETELOG.PK_APP_DL IS 'Primary key'
/

COMMENT ON COLUMN VDA_V_EPAT_DELETELOG.TABLE_NAME IS 'Name of the database table'
/

COMMENT ON COLUMN VDA_V_EPAT_DELETELOG.TABLE_PK IS 'The DELETED ROW''s Primary Key'
/

COMMENT ON COLUMN VDA_V_EPAT_DELETELOG.TABLE_RID IS 'The DELETED ROW''s RID'
/

COMMENT ON COLUMN VDA_V_EPAT_DELETELOG.DELETED_BY IS 'The PK of the er_user, user who deleted the row'
/

COMMENT ON COLUMN VDA_V_EPAT_DELETELOG.DELETED_ON IS 'Stores the date of deletion'
/

COMMENT ON COLUMN VDA_V_EPAT_DELETELOG.APP_MODULE IS 'Stores the Module name'
/

COMMENT ON COLUMN VDA_V_EPAT_DELETELOG.IP_ADDRESS IS 'Stores the IP address of user'
/

COMMENT ON COLUMN VDA_V_EPAT_DELETELOG.REASON_FOR_DELETION IS 'Reason why the row was deleted'
/


--
-- VDA_V_EPAT_AUDIT_UPDATE  (View) 
--
--  Dependencies: 
--   AUDIT_ROW (Table)
--   AUDIT_COLUMN (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_EPAT_AUDIT_UPDATE
(USER_NAME, TIMESTAMP, TABLE_NAME, COLUMN_NAME, OLD_VALUE, 
 NEW_VALUE, REMARKS, CAID, RID, RAID, 
 ACTION)
AS 
SELECT   r.user_name,
            r.timestamp,
            r.table_name,
            c.column_name,
            c.old_value,
            c.new_value,
            c.remarks,
            caid,
            r.rid, r.RAID, r.ACTION
     FROM   epat.audit_column c, epat.audit_row r
    WHERE   r.action = 'U' AND c.raid = r.raid
/

COMMENT ON TABLE VDA_V_EPAT_AUDIT_UPDATE IS 'This view provides access to the detail audit 
trail on Update actions in EPAT schema'
/

COMMENT ON COLUMN VDA_V_EPAT_AUDIT_UPDATE.USER_NAME IS 'The user who triggered the action'
/

COMMENT ON COLUMN VDA_V_EPAT_AUDIT_UPDATE.TIMESTAMP IS 'The date and time of the action'
/

COMMENT ON COLUMN VDA_V_EPAT_AUDIT_UPDATE.TABLE_NAME IS 'The name of the table'
/

COMMENT ON COLUMN VDA_V_EPAT_AUDIT_UPDATE.COLUMN_NAME IS 'The name of the column updated 
by the action'
/

COMMENT ON COLUMN VDA_V_EPAT_AUDIT_UPDATE.OLD_VALUE IS 'The old value of the column'
/

COMMENT ON COLUMN VDA_V_EPAT_AUDIT_UPDATE.NEW_VALUE IS 'The new value of the column'
/

COMMENT ON COLUMN VDA_V_EPAT_AUDIT_UPDATE.REMARKS IS 'The reason for change comments 
linked with the action'
/

COMMENT ON COLUMN VDA_V_EPAT_AUDIT_UPDATE.CAID IS 'The Primary Key of the audit column 
change'
/

COMMENT ON COLUMN VDA_V_EPAT_AUDIT_UPDATE.RID IS 'The Unique Row ID for the table row'
/

COMMENT ON COLUMN VDA_V_EPAT_AUDIT_UPDATE.RAID IS 'The Primary Key of the audit 
transaction'
/

COMMENT ON COLUMN VDA_V_EPAT_AUDIT_UPDATE.ACTION IS 'The Type of audit transaction. From 
eResearch version 9.2, any new audit data created will be stored in audit_column for all 
transactions. I=Insert,D=Delete and U=Update'
/


--
-- VDA_V_EPAT_AUDIT_INSERT  (View) 
--
--  Dependencies: 
--   AUDIT_ROW (Table)
--   AUDIT_INSERT (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_EPAT_AUDIT_INSERT
(USER_NAME, TIMESTAMP, TABLE_NAME, ROW_DATA)
AS 
SELECT   r.user_name,
            r.timestamp,
            r.table_name,
            c.row_data
     FROM   epat.audit_insert c, epat.audit_row r
    WHERE   r.action = 'I' AND c.raid = r.raid
/

COMMENT ON TABLE VDA_V_EPAT_AUDIT_INSERT IS 'This view provides access to the detail audit trail on Insert actions in EPAT schema'
/

COMMENT ON COLUMN VDA_V_EPAT_AUDIT_INSERT.USER_NAME IS 'The user who triggered the action'
/

COMMENT ON COLUMN VDA_V_EPAT_AUDIT_INSERT.TIMESTAMP IS 'The date and time of the action'
/

COMMENT ON COLUMN VDA_V_EPAT_AUDIT_INSERT.TABLE_NAME IS 'The name of the table'
/

COMMENT ON COLUMN VDA_V_EPAT_AUDIT_INSERT.ROW_DATA IS 'The pipe delimited data at the time of insert, The order of the column is same as the order of the columns in the table'
/


--
-- VDA_V_EPAT_AUDIT_DELETE  (View) 
--
--  Dependencies: 
--   AUDIT_ROW (Table)
--   AUDIT_DELETE (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_EPAT_AUDIT_DELETE
(USER_NAME, TIMESTAMP, TABLE_NAME, ROW_DATA)
AS 
SELECT   r.user_name,
            r.timestamp,
            r.table_name,
            c.row_data
     FROM   epat.audit_delete c, epat.audit_row r
    WHERE   r.action = 'D' AND c.raid = r.raid
/

COMMENT ON TABLE VDA_V_EPAT_AUDIT_DELETE IS 'This view provides access to the detail audit trail on Delete actions in EPAT schema'
/

COMMENT ON COLUMN VDA_V_EPAT_AUDIT_DELETE.USER_NAME IS 'The user who triggered the action'
/

COMMENT ON COLUMN VDA_V_EPAT_AUDIT_DELETE.TIMESTAMP IS 'The date and time of the action'
/

COMMENT ON COLUMN VDA_V_EPAT_AUDIT_DELETE.TABLE_NAME IS 'The name of the table'
/

COMMENT ON COLUMN VDA_V_EPAT_AUDIT_DELETE.ROW_DATA IS 'The pipe delimited data at the time of delete, The order of the column is same as the order of the columns in the table'
/


--
-- VDA_V_EPAT_AUDITROW  (View) 
--
--  Dependencies: 
--   AUDIT_ROW (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_EPAT_AUDITROW
(RAID, TABLE_NAME, RID, ACTION, TIMESTAMP, 
 USER_NAME)
AS 
SELECT   "RAID",
            "TABLE_NAME",
            "RID",
            "ACTION",
            "TIMESTAMP",
            "USER_NAME"
     FROM   epat.audit_row
/

COMMENT ON TABLE VDA_V_EPAT_AUDITROW IS 'This view provides access to the high level audit trail on Insert, Update and Delete actions in EPAT schema'
/

COMMENT ON COLUMN VDA_V_EPAT_AUDITROW.RAID IS 'The Unique audit Primary key'
/

COMMENT ON COLUMN VDA_V_EPAT_AUDITROW.TABLE_NAME IS 'The name of the table'
/

COMMENT ON COLUMN VDA_V_EPAT_AUDITROW.RID IS 'The unique RID of the table row'
/

COMMENT ON COLUMN VDA_V_EPAT_AUDITROW.ACTION IS 'The action, I:Insert,D:Delete and U:Update'
/

COMMENT ON COLUMN VDA_V_EPAT_AUDITROW.TIMESTAMP IS 'The date and time of the action'
/

COMMENT ON COLUMN VDA_V_EPAT_AUDITROW.USER_NAME IS 'The user who triggered the action'
/


--
-- VDA_V_BUDGET_SECTION  (View) 
--
--  Dependencies: 
--   ER_STUDY (Table)
--   ER_SITE (Table)
--   F_GET_YESNO (Function)
--   F_GET_FSTAT (Function)
--   ER_USER (Table)
--   SCH_CODELST (Table)
--   SCH_BUDGET (Table)
--   SCH_BGTSECTION (Table)
--   SCH_BGTCAL (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_BUDGET_SECTION
(BGTDT_NAME, BGTDT_VERSION, BGTDT_TEMPLATE, BGTDT_STUDY_NUM, BGTDT_STUDY_TITLE, 
 BGTDT_ORGANIZATION, BGTDT_STATUS, BGTDT_DESCRIPTION, BGTDT_CREATED_BY, BGTDT_CURRENCY, 
 FK_ACCOUNT, PK_BUDGET, CREATOR, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, 
 CREATED_ON, FK_STUDY, BGTCAL_SPONSOROHEAD, SPONSOR_OHEAD_FLAG, BGTCAL_INDIRECTCOST, 
 BGTCAL_FRGBENEFIT, FRINGEBFT_FLAG, BGTCAL_DISCOUNT, DISCNT_FLAG, EXCLUD_SOC, 
 BUDGET_COMBFLAG, PK_BUDGETSEC, BGTSECTION_NAME, BGTSECTION_SEQUENCE, BGTSECTION_TYPE, 
 BGTSECTION_PATNO, BGTSECTION_NOTES, BGTSECTION_PERSONLFLAG, BUDGET_VISIT_PK, CREATOR_FK, 
 LAST_MODIFIED_BY_FK)
AS 
SELECT   BUDGET_NAME BGTDT_NAME,
            BUDGET_VERSION BGTDT_VERSION,
            NVL ( (SELECT   CODELST_DESC
                     FROM   esch.SCH_CODELST
                    WHERE   PK_CODELST = B.BUDGET_TEMPLATE),
                 (SELECT   BUDGET_NAME
                    FROM   esch.SCH_BUDGET
                   WHERE   PK_BUDGET = B.BUDGET_TEMPLATE))
               BGTDT_TEMPLATE,
            (SELECT   STUDY_NUMBER
               FROM   eres.ER_STUDY
              WHERE   PK_STUDY = FK_STUDY)
               BGTDT_STUDY_NUM,
            (SELECT   STUDY_TITLE
               FROM   eres.ER_STUDY
              WHERE   PK_STUDY = FK_STUDY)
               BGTDT_STUDY_TITLE,
            (SELECT   SITE_NAME
               FROM   eres.ER_SITE
              WHERE   PK_SITE = FK_SITE)
               BGTDT_ORGANIZATION,
            ERES.F_GEt_Fstat (BUDGET_STATUS) BGTDT_STATUS,
            BUDGET_DESC BGTDT_DESCRIPTION,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = BUDGET_CREATOR)
               BGTDT_CREATED_BY,
            (SELECT   CODELST_DESC
               FROM   esch.SCH_CODELST
              WHERE   PK_CODELST = BUDGET_CURRENCY)
               BGTDT_CURRENCY,
            B.FK_ACCOUNT,
            PK_BUDGET,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = B.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = B.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            B.LAST_MODIFIED_DATE,
            B.CREATED_ON,
            fk_study,
            BGTCAL_SPONSOROHEAD,
            ERES.F_GEt_Yesno (BGTCAL_SPONSORFLAG) SPONSOR_OHEAD_FLAG,
            BGTCAL_INDIRECTCOST,
            BGTCAL_FRGBENEFIT,
            ERES.F_GEt_Yesno (BGTCAL_FRGFLAG) FRINGEBFT_FLAG,
            BGTCAL_DISCOUNT,
            ERES.F_GEt_Yesno (BGTCAL_DISCOUNTFLAG) DISCNT_FLAG,
            ERES.F_GEt_Yesno (BGTCAL_EXCLDSOCFLAG) EXCLUD_SOC,
            BUDGET_COMBFLAG,
            pk_budgetsec,
            bgtsection_name,
            bgtsection_sequence,
            bgtsection_type,
            bgtsection_patno,
            bgtsection_notes,
            bgtsection_personlflag,
            fk_visit, B.CREATOR CREATOR_FK, B.LAST_MODIFIED_BY LAST_MODIFIED_BY_FK
     FROM   ESCH.SCH_BUDGET B, ESCH.SCH_BGTCAL C, ESCH.SCH_BGTSECTION S
    WHERE       C.FK_BUDGET = B.PK_BUDGET
            AND (BUDGET_DELFLAG IS NULL OR BUDGET_DELFLAG <> 'Y')
            AND S.fk_bgtcal = C.pk_bgtcal
            AND NVL (BGTSECTION_DELFLAG, 'N') <> 'Y'
            AND NVL (BGTCAL_DELFLAG, 'N') <> 'Y'
/

COMMENT ON TABLE VDA_V_BUDGET_SECTION IS 'This view provides access to the Budget Sections 
design'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.LAST_MODIFIED_BY_FK IS 'The Key to identify the 
Last Modified By of the record.'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.BGTDT_NAME IS 'The name of the Budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.BGTDT_VERSION IS 'The version of the Budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.BGTDT_TEMPLATE IS 'The template used for creating 
the budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.BGTDT_STUDY_NUM IS 'The study number of the study 
linked with the budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.BGTDT_STUDY_TITLE IS 'The title of the study linked 
with the budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.BGTDT_ORGANIZATION IS 'The organization linked with 
the budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.BGTDT_STATUS IS 'The status of the budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.BGTDT_DESCRIPTION IS 'The description of the 
budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.BGTDT_CREATED_BY IS 'The user who created the 
Budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.BGTDT_CURRENCY IS 'The currency the budget is 
created in'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.FK_ACCOUNT IS 'The Account linked with the 
Budget'''
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.PK_BUDGET IS 'The Primary Key of the main Budget 
record'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.CREATOR IS 'The user who created the Budget record 
(Audit)'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.LAST_MODIFIED_BY IS 'The user who last modified the 
Budget Section record(Audit)'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.LAST_MODIFIED_DATE IS 'The date the Budget Section 
record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.CREATED_ON IS 'The date the Budget Section record 
was created on (Audit)'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.FK_STUDY IS 'The Foreign Key to the study the 
patient study status record is linked with'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.BGTCAL_SPONSOROHEAD IS 'The Sponsor Overhead 
number'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.SPONSOR_OHEAD_FLAG IS 'The Sponsor Overhead flag'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.BGTCAL_INDIRECTCOST IS 'The indirect cost number to 
be applied to selected line items'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.BGTCAL_FRGBENEFIT IS 'The Fringe Benefit number to 
be applied to selected line items'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.FRINGEBFT_FLAG IS 'The Fringe Benefit calculation 
flag'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.BGTCAL_DISCOUNT IS 'The discount or markup number'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.DISCNT_FLAG IS 'The discount or markup flag '
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.EXCLUD_SOC IS 'The Exclude Standard of Care cost 
from totals flag'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.BUDGET_COMBFLAG IS 'The flag to indicate if the 
budget is a combined budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.PK_BUDGETSEC IS 'The Primary Key of the Budget 
Section Record'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.BGTSECTION_NAME IS 'The name of the Budget Section'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.BGTSECTION_SEQUENCE IS 'The Sequence of the Section 
in the Budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.BGTSECTION_TYPE IS 'The Budget Section type'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.BGTSECTION_PATNO IS 'The number of patients the 
section is applicable for (used in calculating cost)'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.BGTSECTION_NOTES IS 'The notes linked with the 
section'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.BGTSECTION_PERSONLFLAG IS 'The Flag to indicate if 
it is a Personnel type section'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.BUDGET_VISIT_PK IS 'The Foreign Key to the visit 
the Budget line item is linked with'
/

COMMENT ON COLUMN VDA_V_BUDGET_SECTION.CREATOR_FK IS 'The Key to identify the Creator of 
the record.'
/


--
-- VDA_V_BUDGET_MILESTONES  (View) 
--
--  Dependencies: 
--   F_GET_MTYPE (Function)
--   F_GET_CODELSTDESC (Function)
--   PKG_MILESTONE_NEW (Package)
--   PKG_DATEUTIL (Synonym)
--   ERV_BUDGET (View)
--   VDA_V_MILESTONES (View)
--
CREATE OR REPLACE FORCE VIEW VDA_V_BUDGET_MILESTONES
(STUDY_NUMBER, STUDY_TITLE, MSRUL_CATEGORY, MSRUL_AMOUNT, MSRUL_PT_COUNT, 
 MSRUL_PT_STATUS, MSRUL_LIMIT, MSRUL_PAY_TYPE, MSRUL_PAY_FOR, MSRUL_STATUS, 
 MSRUL_PROT_CAL, MSRUL_VISIT, MSRUL_MS_RULE, MSRUL_EVENT_STAT, FK_ACCOUNT, 
 PK_MILESTONE, CREATOR, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, 
 FK_STUDY, EVENT_NAME, MILESTONE_ACHIEVEDCOUNT, MILESTONE_ACHIEVEDAMOOUNT, LAST_CHECKED_ON, 
 MILESTONE_DESCRIPTION, BUDGET_NAME, LINEITEM_NAME, PROT_CALENDAR, BGTSECTION_NAME, 
 CREATOR_FK, LAST_MODIFIED_BY_FK)
AS 
SELECT   m.STUDY_NUMBER,
            m.STUDY_TITLE,
            MSRUL_CATEGORY,
            MSRUL_AMOUNT,
            MSRUL_PT_COUNT,
            MSRUL_PT_STATUS,
            MSRUL_LIMIT,
            MSRUL_PAY_TYPE,
            MSRUL_PAY_FOR,
            MSRUL_STATUS,
            MSRUL_PROT_CAL,
            MSRUL_VISIT,
            MSRUL_MS_RULE,
            MSRUL_EVENT_STAT,
            FK_ACCOUNT,
            PK_MILESTONE,
            m.CREATOR,
            m.LAST_MODIFIED_BY,
            m.LAST_MODIFIED_DATE,
            m.CREATED_ON,
            m.FK_STUDY,
            EVENT_NAME,
            MILESTONE_ACHIEVEDCOUNT,
            MILESTONE_ACHIEVEDAMOUNT,
            LAST_CHECKED_ON,
            MILESTONE_DESCRIPTION,
            BUDGET_NAME,
            LINEITEM_NAME,
            PROT_CALENDAR,
            BGTSECTION_NAME, M.CREATOR CREATOR_FK, m.LAST_MODIFIED_BY LAST_MODIFIED_BY_FK
     FROM   VDA.VDA_V_MILESTONES m, esch.erv_budget
    WHERE       fk_budget = pk_budget
            AND FK_BGTCAL = pk_bgtcal
            AND FK_BGTSECTION = pk_budgetsec
            AND FK_LINEITEM = pk_lineitem
/

COMMENT ON TABLE VDA_V_BUDGET_MILESTONES IS 'This view provides data on milestones that 
are created from a budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.LAST_MODIFIED_BY_FK IS 'The Key to identify the 
Last Modified By of the record.'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.STUDY_NUMBER IS 'The Study Number'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.STUDY_TITLE IS 'The Study Title'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.MSRUL_CATEGORY IS 'The Milestone Category'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.MSRUL_AMOUNT IS 'The Milestone amount'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.MSRUL_PT_COUNT IS 'The Milestone Patient Count 
for the achievement rule'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.MSRUL_PT_STATUS IS 'The Milestone Patient Status 
for the achievement rule'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.MSRUL_LIMIT IS 'The Milestone Patient Status for 
the achievement rule'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.MSRUL_PAY_TYPE IS 'The Milestone Payment Type'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.MSRUL_PAY_FOR IS 'The Milestone Payment For'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.MSRUL_STATUS IS 'The Milestone Status for the 
achievement rule'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.MSRUL_PROT_CAL IS 'The Milestone Calendar for 
the achievement rule'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.MSRUL_VISIT IS 'The Milestone Visit for the 
achievement rule'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.MSRUL_MS_RULE IS 'The Milestone achievement 
rule'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.MSRUL_EVENT_STAT IS 'The Milestone Event Status 
for achievement the rule'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.FK_ACCOUNT IS 'The account the milestone is 
linked with'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.PK_MILESTONE IS 'The Primary Key of the 
Milestone'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.CREATOR IS 'The date the record was created on 
(Audit)'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.LAST_MODIFIED_BY IS 'The user who last modified 
the record(Audit)'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.LAST_MODIFIED_DATE IS 'The date the  record was 
last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.CREATED_ON IS 'The date the  record was created 
on (Audit)'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.FK_STUDY IS 'The Foreign Key of the Study the 
milestone is linked with'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.EVENT_NAME IS 'The Milestone Event Name for 
achievement the rule'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.MILESTONE_ACHIEVEDCOUNT IS 'The Milestones 
achieved so far'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.MILESTONE_ACHIEVEDAMOOUNT IS 'The Milestones 
amount achieved so far'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.LAST_CHECKED_ON IS 'The Last checked date for 
the milestone achievement'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.MILESTONE_DESCRIPTION IS 'The Milestone 
description'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.BUDGET_NAME IS 'he name of the budget the MS is 
linked with'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.LINEITEM_NAME IS 'The name of the line item the 
MS is linked with'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.PROT_CALENDAR IS 'The name of the budget 
calendar the MS is linked with'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.BGTSECTION_NAME IS 'The name of the budget 
section the MS is linked with'
/

COMMENT ON COLUMN VDA_V_BUDGET_MILESTONES.CREATOR_FK IS 'The Key to identify the Creator 

of the record.'
/


--
-- VDA_V_BUDGET_LINEITEM  (View) 
--
--  Dependencies: 
--   PKG_DATEUTIL (Synonym)
--   ERV_BUDGET (View)
--
CREATE OR REPLACE FORCE VIEW VDA_V_BUDGET_LINEITEM
(PK_BUDGET, BUDGET_NAME, LINEITEM_NAME, PROT_CALENDAR, BUDGET_VERSION, 
 BUDGET_DESC, STUDY_NUMBER, STUDY_TITLE, SITE_NAME, CATEGORY, 
 BGTSECTION_NAME, CPT_CODE, COST_PER_PATIENT, TOTAL_COST, LINE_ITEM_INDIRECTS_FLAG, 
 COST_DISCOUNT_ON_LINE_ITEM, INDIRECTS, BUDGET_INDIRECT_FLAG, FRINGE_BENEFIT, FRINGE_FLAG, 
 PER_PATIENT_LINE_FRINGE, TOTAL_LINE_FRINGE, BUDGET_DISCOUNT, BUDGET_DISCOUNT_FLAG, TOTAL_COST_AFTER, 
 TOTAL_PAT_COST_AFTER, PER_PAT_LINE_ITEM_DISCOUNT, TOTAL_COST_DISCOUNT, PERPAT_INDIRECT, TOTAL_COST_INDIRECT, 
 BUDGET_CURRENCY, UNIT_COST, NUMBER_OF_UNIT, BGTSECTION_PATNO, LAST_MODIFIED_BY, 
 LAST_MODIFIED_DATE, STANDARD_OF_CARE, CATEGORY_SUBTYP, SPONSOR_AMOUNT, L_VARIANCE, 
 FK_CODELST_COST_TYPE, COST_TYPE_DESC, LINEITEM_DIRECT_PERPAT, TOTAL_COST_PER_PAT, TOTAL_COST_ALL_PAT, 
 PK_BUDGETSEC, BUDGETSEC_FKVISIT, PK_LINEITEM, LINEITEM_INPERSEC, BGTCAL_EXCLDSOCFLAG, 
 BUDGETSECTION_SEQUENCE, LINEITEM_SEQ, L_LINEITEM_NAME, BGTSECTION_TYPE, SUBCOST_ITEM_FLAG)
AS 
SELECT   pk_budget,
            BUDGET_NAME,
            LINEITEM_NAME,
            PROT_CALENDAR,
            BUDGET_VERSION,
            BUDGET_DESC,
            STUDY_NUMBER,
            STUDY_TITLE,
            SITE_NAME,
            CATEGORY,
            BGTSECTION_NAME,
            CPT_CODE,
            NVL (COST_PER_PATIENT, 0) AS COST_PER_PATIENT,
            NVL (TOTAL_COST, 0) AS TOTAL_COST,
            DECODE (LINE_ITEM_INDIRECTS_FLAG, 1, 'Yes', '')
               AS LINE_ITEM_INDIRECTS_FLAG,
            DECODE (COST_DISCOUNT_ON_LINE_ITEM, 1, 'Yes', '')
               AS COST_DISCOUNT_ON_LINE_ITEM,
            INDIRECTS,
            BUDGET_INDIRECT_FLAG,
            FRINGE_BENEFIT,
            FRINGE_FLAG,
            PER_PATIENT_LINE_FRINGE,
            TOTAL_LINE_FRINGE,
            BUDGET_DISCOUNT,
            DECODE (BUDGET_DISCOUNT_FLAG, 1, 'Discount', 2, 'Markup', '')
               AS BUDGET_DISCOUNT_FLAG,
            TOTAL_COST_AFTER,
            TOTAL_PAT_COST_AFTER,
            PER_PAT_LINE_ITEM_DISCOUNT,
            TOTAL_COST_DISCOUNT,
            PERPAT_INDIRECT,
            TOTAL_COST_INDIRECT,
            BUDGET_CURRENCY,
            UNIT_COST,
            NUMBER_OF_UNIT,
            NVL (BGTSECTION_PATNO, 1) BGTSECTION_PATNO,
            LAST_MODIFIED_BY,
            LAST_MODIFIED_DATE,
            DECODE (COST_CUSTOMCOL, 'research', 'No', 'soc', 'Yes', 'Other')
               AS standard_of_care,
            category_subtyp,
            LINEITEM_SPONSORAMOUNT AS SPONSOR_AMOUNT,
            LINEITEM_VARIANCE AS L_VARIANCE,
            FK_CODELST_COST_TYPE,
            COST_TYPE_DESC,
            lineitem_direct_perpat,
            total_cost_per_pat,
            total_cost_all_pat,
            pk_budgetsec,
            NVL (budgetsec_fkvisit, 0) budgetsec_fkvisit,
            pk_lineitem,
            lineitem_inpersec,
            NVL (bgtcal_excldsocflag, 0) AS bgtcal_excldsocflag,
            budgetsection_sequence,
            lineitem_seq,
            LOWER (LINEITEM_NAME) AS l_LINEITEM_NAME,
            BGTSECTION_TYPE,
            SUBCOST_ITEM_FLAG
     FROM   esch.erv_budget
/

COMMENT ON TABLE VDA_V_BUDGET_LINEITEM IS 'This view provides access to the Budgets with lineitem details'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.SITE_NAME IS 'The Site Name'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.CATEGORY IS 'The Lineitem category'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.BGTSECTION_NAME IS 'The Budget Section Name'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.CPT_CODE IS 'The CPT code linked with the lineitem'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.COST_PER_PATIENT IS 'The total cost per patient'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.TOTAL_COST IS 'The total cost of the lineitem'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.LINE_ITEM_INDIRECTS_FLAG IS 'The flag indicating if indirects should be applied The flag indicating if discounts should be applied'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.COST_DISCOUNT_ON_LINE_ITEM IS 'The flag indicating if discounts should be applied on the lineitem'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.INDIRECTS IS 'The indirect amount to be applied on the lineitem'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.BUDGET_INDIRECT_FLAG IS 'The flag indicating if indirects should be applied on the budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.FRINGE_BENEFIT IS 'The fringe benefit  amount on the lineitem'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.FRINGE_FLAG IS 'The flag indicating if fringe benefit should be applied on the budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.PER_PATIENT_LINE_FRINGE IS 'Per Patient fringe for lineitem'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.TOTAL_LINE_FRINGE IS 'Total lineitem fringe'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.BUDGET_DISCOUNT IS 'The discount amount on the lineitem'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.BUDGET_DISCOUNT_FLAG IS 'The flag indicating if discount should be applied on the budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.TOTAL_COST_AFTER IS 'Total cost per lineitem'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.TOTAL_PAT_COST_AFTER IS 'Total cost per lineitem per patient'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.PER_PAT_LINE_ITEM_DISCOUNT IS 'Per patient lineitem discount'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.TOTAL_COST_DISCOUNT IS 'Total cost discount'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.PERPAT_INDIRECT IS 'Per Patient indirect  amount'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.TOTAL_COST_INDIRECT IS 'Total indirect cost'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.BUDGET_CURRENCY IS 'The Budget Currency'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.UNIT_COST IS 'The unit cost of the lineitem'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.NUMBER_OF_UNIT IS 'The number of units to be budgeted'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.BGTSECTION_PATNO IS 'The Number of Patients a per patient section is calculated for'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.LAST_MODIFIED_DATE IS 'The date the  record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.STANDARD_OF_CARE IS 'The standard of care flag for the lineitem'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.CATEGORY_SUBTYP IS 'The category subype (per codelist)'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.SPONSOR_AMOUNT IS 'The sponsor amount linked with the lineitem'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.L_VARIANCE IS 'Varience amount varience between sponsor amount and budgeted amount'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.FK_CODELST_COST_TYPE IS 'The cost type linked with the lineitem'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.COST_TYPE_DESC IS 'The cost type description'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.LINEITEM_DIRECT_PERPAT IS 'Direct lineitem cost per patient'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.TOTAL_COST_PER_PAT IS 'Total Cost for per patient'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.TOTAL_COST_ALL_PAT IS 'Total Cost for all patients'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.PK_BUDGETSEC IS 'The Primary key of the Budget section record'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.BUDGETSEC_FKVISIT IS 'The Primary key of the visit record if lineitem is linked with a calendar visit'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.PK_LINEITEM IS 'The Primary key of the lineitem'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.LINEITEM_INPERSEC IS 'This column indicates if the cost should be included in all Sections of the budget or only in one section titled ''Personnel Costs'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.BGTCAL_EXCLDSOCFLAG IS 'The exclude Standard or Care flag for the Budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.BUDGETSECTION_SEQUENCE IS 'The budget section sequence'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.LINEITEM_SEQ IS 'The Lineitem sequence in a budget section'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.L_LINEITEM_NAME IS 'The lineitem name'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.BGTSECTION_TYPE IS 'The Budget Section Type'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.SUBCOST_ITEM_FLAG IS 'The Budget Subject Cost Item Flag 0 indicating if the line item originated from the Subject Cost Items'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.PK_BUDGET IS 'The Primary key of the budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.BUDGET_NAME IS 'The Budget Name'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.LINEITEM_NAME IS 'The Lineitem Name'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.PROT_CALENDAR IS 'The Protocol Calendar the Budget is linked with'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.BUDGET_VERSION IS 'The Budget version'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.BUDGET_DESC IS 'The Budget Description'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.STUDY_NUMBER IS 'The Study Number the budget is linked with'
/

COMMENT ON COLUMN VDA_V_BUDGET_LINEITEM.STUDY_TITLE IS 'The study title the budget is linked with'
/


--
-- VDA_V_BUDGET_CALENDAR  (View) 
--
--  Dependencies: 
--   ER_STUDY (Table)
--   ER_SITE (Table)
--   F_GET_YESNO (Function)
--   F_GET_FSTAT (Function)
--   ER_USER (Table)
--   SCH_CODELST (Table)
--   SCH_BUDGET (Table)
--   SCH_BGTCAL (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_BUDGET_CALENDAR
(BGTDT_NAME, BGTDT_VERSION, BGTDT_TEMPLATE, BGTDT_STUDY_NUM, BGTDT_STUDY_TITLE, 
 BGTDT_ORGANIZATION, BGTDT_STATUS, BGTDT_DESCRIPTION, BGTDT_CREATED_BY, BGTDT_CURRENCY, 
 FK_ACCOUNT, PK_BUDGET, CREATOR, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, 
 CREATED_ON, FK_STUDY, BGTCAL_SPONSOROHEAD, SPONSOR_OHEAD_FLAG, BGTCAL_INDIRECTCOST, 
 BGTCAL_FRGBENEFIT, FRINGEBFT_FLAG, BGTCAL_DISCOUNT, DISCNT_FLAG, EXCLUD_SOC, 
 BUDGET_COMBFLAG, CREATOR_FK, LAST_MODIFIED_BY_FK)
AS 
SELECT   DISTINCT
            BUDGET_NAME BGTDT_NAME,
            BUDGET_VERSION BGTDT_VERSION,
            NVL ( (SELECT   CODELST_DESC
                     FROM   esch.SCH_CODELST
                    WHERE   PK_CODELST = B.BUDGET_TEMPLATE),
                 (SELECT   BUDGET_NAME
                    FROM   esch.SCH_BUDGET
                   WHERE   PK_BUDGET = B.BUDGET_TEMPLATE))
               BGTDT_TEMPLATE,
            (SELECT   STUDY_NUMBER
               FROM   eres.ER_STUDY
              WHERE   PK_STUDY = FK_STUDY)
               BGTDT_STUDY_NUM,
            (SELECT   STUDY_TITLE
               FROM   eres.ER_STUDY
              WHERE   PK_STUDY = FK_STUDY)
               BGTDT_STUDY_TITLE,
            (SELECT   SITE_NAME
               FROM   eres.ER_SITE
              WHERE   PK_SITE = FK_SITE)
               BGTDT_ORGANIZATION,
            ERES.F_GEt_Fstat (BUDGET_STATUS) BGTDT_STATUS,
            BUDGET_DESC BGTDT_DESCRIPTION,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = BUDGET_CREATOR)
               BGTDT_CREATED_BY,
            (SELECT   CODELST_DESC
               FROM   esch.SCH_CODELST
              WHERE   PK_CODELST = BUDGET_CURRENCY)
               BGTDT_CURRENCY,
            B.FK_ACCOUNT,
            PK_BUDGET,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = B.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = B.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            B.LAST_MODIFIED_DATE,
            B.CREATED_ON,
            fk_study,
            BGTCAL_SPONSOROHEAD,
            ERES.F_GEt_Yesno (BGTCAL_SPONSORFLAG) SPONSOR_OHEAD_FLAG,
            BGTCAL_INDIRECTCOST,
            BGTCAL_FRGBENEFIT,
            ERES.F_GEt_Yesno (BGTCAL_FRGFLAG) FRINGEBFT_FLAG,
            BGTCAL_DISCOUNT,
            ERES.F_GEt_Yesno (BGTCAL_DISCOUNTFLAG) DISCNT_FLAG,
            ERES.F_GEt_Yesno (BGTCAL_EXCLDSOCFLAG) EXCLUD_SOC,
            BUDGET_COMBFLAG,
            B.CREATOR CREATOR_FK, B.LAST_MODIFIED_BY LAST_MODIFIED_BY_FK
     FROM   esch.SCH_BUDGET B, esch.SCH_BGTCAL C
    WHERE   C.FK_BUDGET = B.PK_BUDGET
            AND (BUDGET_DELFLAG IS NULL OR BUDGET_DELFLAG <> 'Y')
/

COMMENT ON TABLE VDA_V_BUDGET_CALENDAR IS 'This View provides access to the Budget 
Calendar information. There is a default calendar added for each budget. In addition, user 
can add one or more Library or Study Calendars directly to the budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.DISCNT_FLAG IS 'The discount or markup flag'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.EXCLUD_SOC IS 'The Exclude Standard of Care cost 
from totals flag'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.BUDGET_COMBFLAG IS 'The flag to indicate if the 
budget is a combined budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.CREATOR_FK IS 'The Key to identify the Creator of 
the record.'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.LAST_MODIFIED_BY_FK IS 'The Key to identify the 
Last Modified By of the record.'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.BGTDT_NAME IS 'The Budget name'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.BGTDT_VERSION IS 'The Budget version number'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.BGTDT_TEMPLATE IS 'The template used for creating 
the budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.BGTDT_STUDY_NUM IS 'The study number of the study 
linked with the budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.BGTDT_STUDY_TITLE IS 'The title of the study is 
linked with the budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.BGTDT_ORGANIZATION IS 'The organization linked 
with the budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.BGTDT_STATUS IS 'The status of the budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.BGTDT_DESCRIPTION IS 'The description of the 
budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.BGTDT_CREATED_BY IS 'The user who created the 
Budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.BGTDT_CURRENCY IS 'The currency the budget is 
created in'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.FK_ACCOUNT IS 'The Account linked with the Budget'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.PK_BUDGET IS 'The Primary Key of the main Budget 
record'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.CREATOR IS 'The user who created the Budget record 
(Audit)'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.LAST_MODIFIED_BY IS 'The user who last modified 
the Budget record (Audit)'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.LAST_MODIFIED_DATE IS 'The date the Budget record 
was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.CREATED_ON IS 'The date the Budget record was 
created on (Audit)'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.FK_STUDY IS 'The Foreign Key to the study the 
Budget record is linked with'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.BGTCAL_SPONSOROHEAD IS 'The Sponsor Overhead 
number'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.SPONSOR_OHEAD_FLAG IS 'The Sponsor Overhead flag'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.BGTCAL_INDIRECTCOST IS 'The indirect cost number 
to be applied to selected lineitems'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.BGTCAL_FRGBENEFIT IS 'The Fringe Benefit number to 
be applied to selected lineitems'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.FRINGEBFT_FLAG IS 'The Fringe Benefit calculation 
flag'
/

COMMENT ON COLUMN VDA_V_BUDGET_CALENDAR.BGTCAL_DISCOUNT IS 'The discount or markup number'
/


--
-- VDA_V_AUDIT_REPDOWNLOAD_LOG  (View) 
--
--  Dependencies: 
--   ER_USERREPORTLOG (Table)
--   ER_USER (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_AUDIT_REPDOWNLOAD_LOG
(USER_NAME, USR_RPT_LOG_REP_NAME, USR_RPT_LOG_TABLE_NAME, IP_ADD, USR_RPT_LOG_URL, 
 USR_RPT_LOG_URLPARAM, USR_RPT_LOG_ACCESSTIME, USR_RPT_LOG_MODNAME, USR_RPT_LOG_FILENAME, USR_RPT_LOG_FORMAT, 
 USR_RPT_LOG_DOWNLOAD_FLAG, USER_FK)
AS 
SELECT   (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = FK_USER)
               USER_NAME,
            USR_RPT_LOG_REP_NAME,
            USR_RPT_LOG_TABLE_NAME,
            IP_ADD,
            USR_RPT_LOG_URL,
            USR_RPT_LOG_URLPARAM,
            USR_RPT_LOG_ACCESSTIME,
            USR_RPT_LOG_MODNAME,
            USR_RPT_LOG_FILENAME,
            DECODE (USR_RPT_LOG_FORMAT,
                    NULL, 'Screen Output',
                    'null', 'Screen Output',
                    USR_RPT_LOG_FORMAT)
               USR_RPT_LOG_FORMAT,
            DECODE (USR_RPT_LOG_DOWNLOAD_FLAG, 0, 'No', 'Yes')
               USR_RPT_LOG_DOWNLOAD_FLAG,
               FK_USER as USER_FK
     FROM   eres.ER_USERREPORTLOG
/

COMMENT ON TABLE VDA_V_AUDIT_REPDOWNLOAD_LOG IS 'This view provides access to the audit 
log generated based on user activity on executing reports and/or file downloads.'
/

COMMENT ON COLUMN VDA_V_AUDIT_REPDOWNLOAD_LOG.USER_NAME IS 'The name of the user who 
accessed a report or downloaded a file'
/

COMMENT ON COLUMN VDA_V_AUDIT_REPDOWNLOAD_LOG.USR_RPT_LOG_REP_NAME IS 'The name of the 
report or file'
/

COMMENT ON COLUMN VDA_V_AUDIT_REPDOWNLOAD_LOG.USR_RPT_LOG_TABLE_NAME IS 'The source table 
name'
/

COMMENT ON COLUMN VDA_V_AUDIT_REPDOWNLOAD_LOG.IP_ADD IS 'The IP ADDRESS of the client 
machine'
/

COMMENT ON COLUMN VDA_V_AUDIT_REPDOWNLOAD_LOG.USR_RPT_LOG_URL IS 'The URL accessed in the 
Velos eResearch application'
/

COMMENT ON COLUMN VDA_V_AUDIT_REPDOWNLOAD_LOG.USR_RPT_LOG_URLPARAM IS 'The parameters 
passed to the URL accessed in the Velos eResearch application'
/

COMMENT ON COLUMN VDA_V_AUDIT_REPDOWNLOAD_LOG.USR_RPT_LOG_ACCESSTIME IS 'The time of 
report access or file download'
/

COMMENT ON COLUMN VDA_V_AUDIT_REPDOWNLOAD_LOG.USR_RPT_LOG_MODNAME IS 'The module name'
/

COMMENT ON COLUMN VDA_V_AUDIT_REPDOWNLOAD_LOG.USR_RPT_LOG_FILENAME IS 'The downloaded 
file''s name or temporary report file generated'
/

COMMENT ON COLUMN VDA_V_AUDIT_REPDOWNLOAD_LOG.USR_RPT_LOG_FORMAT IS 'The file or report 
output format'
/

COMMENT ON COLUMN VDA_V_AUDIT_REPDOWNLOAD_LOG.USR_RPT_LOG_DOWNLOAD_FLAG IS 'Flag 
indicating file download'
/

COMMENT ON COLUMN VDA_V_AUDIT_REPDOWNLOAD_LOG.USER_FK IS 'The Key to identify the User who 
accessed a report or downloaded a file'
/


--
-- VDA_V_ALL_PATSTUDYSTAT  (View) 
--
--  Dependencies: 
--   ER_STUDY (Table)
--   ER_SITE (Table)
--   F_GET_CODELSTDESC (Function)
--   ER_USER (Table)
--   ER_PATSTUDYSTAT (Table)
--   ER_PATPROT (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_ALL_PATSTUDYSTAT
(PSTAT_STATUS, PSTAT_REASON, PSTAT_STAT_DATE, PSTAT_NOTES, PSTAT_PAT_STUD_ID, 
 PSTAT_ASSIGNED_TO, PSTAT_PHYSICIAN, PSTAT_TREAT_LOCAT, PSTAT_EVAL_FLAG, PSTAT_EVAL_STAT, 
 PSTAT_INEVAL_STAT, PSTAT_SURVIVAL_STAT, PSTAT_RANDOM_NUM, PSTAT_ENROLLED_BY, PSTAT_ENROLL_SITE, 
 PSTAT_NEXT_FU_DATE, PSTAT_IC_VERS_NUM, PSTAT_SCREEN_NUM, PSTAT_SCREEN_BY, PSTAT_SCREEN_OUTCOME, 
 CREATOR, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, FK_PER, 
 FK_STUDY, PSTAT_CURRENT_STAT, PATSTUDYSTAT_ENDT, STUDY_NUMBER, FK_ACCOUNT, 
 PK_PATSTUDYSTAT, PSTAT_STATUS_FK, CREATOR_FK, LAST_MODIFIED_BY_FK, PSTAT_SCREEN_BY_FK, 
 ENROLLED_BY_FK, PSTAT_PHYSICIAN_FK, PSTAT_ASSIGNED_TO_FK, PSTAT_REASON_FK, FK_SITE_ENROLLING)
AS 
SELECT   eres.F_GET_CODELSTDESC (FK_CODELST_STAT) PSTAT_STATUS,
            eres.F_GET_CODELSTDESC (PATSTUDYSTAT_REASON) PSTAT_REASON,
            PATSTUDYSTAT_DATE PSTAT_STAT_DATE,
            PATSTUDYSTAT_NOTE PSTAT_NOTES,
            PATPROT_PATSTDID PSTAT_PAT_STUD_ID,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = FK_USERASSTO)
               PSTAT_ASSIGNED_TO,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = PATPROT_PHYSICIAN)
               PSTAT_PHYSICIAN,
            ERES.F_GET_CODELSTDESC (FK_CODELSTLOC) PSTAT_TREAT_LOCAT,
            ERES.F_GET_CODELSTDESC (FK_CODELST_PTST_EVAL_FLAG)
               PSTAT_EVAL_FLAG,
            ERES.F_GET_CODELSTDESC (FK_CODELST_PTST_EVAL) PSTAT_EVAL_STAT,
            ERES.F_GET_CODELSTDESC (FK_CODELST_PTST_INEVAL) PSTAT_INEVAL_STAT,
            ERES.F_GET_CODELSTDESC (FK_CODELST_PTST_SURVIVAL) PSTAT_SURV_STAT,
            PATPROT_RANDOM PSTAT_RANDOM_NUM,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = ER_PATPROT.FK_USER)
               PSTAT_ENROLLED_BY,
            (SELECT   SITE_NAME
               FROM   eres.ER_SITE
              WHERE   PK_SITE = FK_SITE_ENROLLING)
               PSTAT_ENROLL_SITE,
            ER_PATSTUDYSTAT.NEXT_FOLLOWUP_ON PSTAT_NEXT_FU_DATE,
            ER_PATSTUDYSTAT.INFORM_CONSENT_VER PSTAT_IC_VERS_NUM,
            SCREEN_NUMBER PSTAT_SCREEN_NUM,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = SCREENED_BY)
               PSTAT_SCREEN_BY,
            ERES.F_GET_CODELSTDESC (SCREENING_OUTCOME) PSTAT_SCREEN_OUTCOME,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = ER_PATSTUDYSTAT.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = ER_PATSTUDYSTAT.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            ER_PATSTUDYSTAT.LAST_MODIFIED_DATE,
            ER_PATSTUDYSTAT.CREATED_ON,
            ER_PATSTUDYSTAT.fk_per,
            ER_PATSTUDYSTAT.fk_study,
            ER_PATSTUDYSTAT.CURRENT_STAT,
            ER_PATSTUDYSTAT.PATSTUDYSTAT_ENDT,
            STUDY_NUMBER,
            FK_ACCOUNT,
            PK_PATSTUDYSTAT, FK_CODELST_STAT as PSTAT_STATUS_FK, ER_PATSTUDYSTAT.CREATOR 
as CREATOR_FK, ER_PATSTUDYSTAT.LAST_MODIFIED_BY as LAST_MODIFIED_BY_FK ,
            SCREENED_BY as PSTAT_SCREEN_BY_FK , ER_PATPROT.FK_USER as 

ENROLLED_BY_FK , PATPROT_PHYSICIAN as PSTAT_PHYSICIAN_FK,
FK_USERASSTO as PSTAT_ASSIGNED_TO_FK, PATSTUDYSTAT_REASON as  PSTAT_REASON_FK, 
FK_SITE_ENROLLING 
     FROM   eres.ER_PATSTUDYSTAT, eres.ER_PATPROT, eres.ER_STUDY
    WHERE       ER_PATSTUDYSTAT.FK_PER = ER_PATPROT.FK_PER
            AND ER_PATSTUDYSTAT.FK_STUDY = ER_PATPROT.FK_STUDY
            AND ER_STUDY.PK_STUDY = ER_PATPROT.FK_STUDY
            AND patprot_stat = 1
/

COMMENT ON TABLE VDA_V_ALL_PATSTUDYSTAT IS 'This view provides access to all patient study 
statuses records'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_SCREEN_BY IS 'The user who screened the 
patient'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_SCREEN_OUTCOME IS 'The screening outcome'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.CREATOR IS 'The user who created the patient 
study status record (Audit)'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.LAST_MODIFIED_BY IS 'The user who last modified 
the patient study status record (Audit)'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.LAST_MODIFIED_DATE IS 'The date the patient study 
status record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.CREATED_ON IS 'The date the patient study status 
record was created on (Audit)'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.FK_PER IS 'The Foreign Key to the patient the 
patient study status record is linked with'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.FK_STUDY IS 'The Foreign Key to the study the 
patient study status record is linked with'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_CURRENT_STAT IS 'The flag to indicate if 
this is the current status of the patient on the study'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PATSTUDYSTAT_ENDT IS 'The date this status ended 
on - auto calculated as a new status with a higher date is added'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.STUDY_NUMBER IS 'The Study Number the Patient 
Study Status is linked with'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.FK_ACCOUNT IS 'The Account the Patient Study 
status is linked with'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PK_PATSTUDYSTAT IS 'The Primary Key of the 
patient study status record'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_STATUS_FK IS 'The Key to identify the 
Patient Study Status Code.'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.CREATOR_FK IS 'The Key to identify the Creator of 
the record.'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.LAST_MODIFIED_BY_FK IS 'The Key to identify the 
Last Modified By of the record.'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_SCREEN_BY_FK IS 'The Key to identify the 
Patient Screened By User.'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.ENROLLED_BY_FK IS 'The Key to identify the 
Patient Enrolled By User.'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_PHYSICIAN_FK IS 'The Key to identify the 
Patient Physician User.'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_ASSIGNED_TO_FK IS 'The Key to identify the 
Patient Assigned To User.'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_REASON_FK IS 'The Key to identify the 
Patient Study Status Reason Change Key.'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.FK_SITE_ENROLLING IS 'The Key to identify the 
Patient Enrolling Organization Key'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_STATUS IS 'The Patient Study Status 
description'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_REASON IS 'The Reason the previous Patient 
Study Status is changed to the new status'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_STAT_DATE IS 'The date this Patient Study 
Status started on'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_NOTES IS 'The notes linked with the Patient 
Study Status'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_PAT_STUD_ID IS 'The Patient Study ID 
(Identifier) assigned to the patient on the study'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_ASSIGNED_TO IS 'The user the patient is 
assigned to'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_PHYSICIAN IS 'The Physician the patient is 
assigned to'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_TREAT_LOCAT IS 'The location of the 
treatment'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_EVAL_STAT IS 'The evaluation Status linked 
with the Patient Study Status'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_INEVAL_STAT IS 'The in-evaluation flag 
linked with the Patient Study Status'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_SURVIVAL_STAT IS 'Patient Survival Status 
information'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_RANDOM_NUM IS 'Patient randomization 
number'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_ENROLLED_BY IS 'The user who enrolled the 
patient'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_ENROLL_SITE IS 'The Site or Organization 
Patient is Enrolled for'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_NEXT_FU_DATE IS 'The Next follow up date 
for the Patient'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_IC_VERS_NUM IS 'Patient Informed Consent 
Version Number'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_SCREEN_NUM IS 'Patient Screening Number'
/

COMMENT ON COLUMN VDA_V_ALL_PATSTUDYSTAT.PSTAT_EVAL_FLAG IS 'The evaluation flag linked 
with the Patient Study Status'
/


--
-- VDA_V_AE_ALL  (View) 
--
--  Dependencies: 
--   F_GET_YESNO (Function)
--   F_GET_CODELSTDESC (Function)
--   F_GETDIS_SITE (Function)
--   ER_PATPROT (Table)
--   SCH_CODELST (Table)
--   SCH_ADVERSEVE (Table)
--   VDA_V_STUDY_SUMMARY (View)
--
CREATE OR REPLACE FORCE VIEW VDA_V_AE_ALL
(FK_STUDY, FK_SITE_ENROLLING, STUDY_NUMBER, STUDY_TITLE, STUDY_PI, 
 STUDY_TAREA, STUDY_DISEASE_SITE, AE_TYPE, AE_DESC, AE_STDATE, 
 AE_ENDDATE, AE_GRADE, AE_NAME, MEDDRA, DICTIONARY, 
 FK_PER, FK_ACCOUNT, STUDY_TAREA_FK, STUDY_DISEASE_SITE_FK, STUDY_PI_FK)
AS 
SELECT   e.fk_study,
            p.fk_site_enrolling,
            s.study_number,
            s.study_title,
            s.study_pi,
            s.study_tarea,
            s.study_disease_site,
            (SELECT   codelst_desc
               FROM   esch.sch_codelst
              WHERE   pk_codelst = e.fk_codlst_aetype)
               ae_type,
            e.ae_desc,
            e.ae_stdate,
            e.ae_enddate,
            e.ae_grade,
            e.ae_name,
            e.meddra,
            e.dictionary,
            e.fk_per,
            s.fk_account, s.STUDY_TAREA_FK,s.STUDY_DISEASE_SITE_FK,s.STUDY_PI_FK
     FROM   esch.sch_adverseve e,
            eres.er_patprot p,
            VDA.VDA_V_STUDY_SUMMARY s
    WHERE       e.fk_per = p.fk_per
            AND e.fk_study = p.fk_study
            AND p.patprot_stat = 1
            AND e.fk_study = pk_study
/

COMMENT ON TABLE VDA_V_AE_ALL IS 'This view provides access to all adverse events on a 
study'
/

COMMENT ON COLUMN VDA_V_AE_ALL.AE_ENDDATE IS 'The End Date of the Adverse Event'
/

COMMENT ON COLUMN VDA_V_AE_ALL.AE_GRADE IS 'The grade of the Adverse Event'
/

COMMENT ON COLUMN VDA_V_AE_ALL.AE_NAME IS 'The name of the Adverse Event'
/

COMMENT ON COLUMN VDA_V_AE_ALL.MEDDRA IS 'The Meddra code linked with the Adverse Event'
/

COMMENT ON COLUMN VDA_V_AE_ALL.DICTIONARY IS 'The Dictionary linked with the Adverse 
Event'
/

COMMENT ON COLUMN VDA_V_AE_ALL.FK_PER IS 'The Patient for which the Adverse Event is 
reported'
/

COMMENT ON COLUMN VDA_V_AE_ALL.FK_ACCOUNT IS 'The account the Adverse Event is linked 
with'
/

COMMENT ON COLUMN VDA_V_AE_ALL.STUDY_TAREA_FK IS 'The Key to identify the Study 
Therapeutic Area Code'
/

COMMENT ON COLUMN VDA_V_AE_ALL.STUDY_DISEASE_SITE_FK IS 'The Key to identify the Study 
Disease Sites Code'
/

COMMENT ON COLUMN VDA_V_AE_ALL.STUDY_PI_FK IS 'The Key to identify the Study PI User.'
/

COMMENT ON COLUMN VDA_V_AE_ALL.FK_STUDY IS 'The Foreign Key of the Study record for which 
the Adverse Event is reported'
/

COMMENT ON COLUMN VDA_V_AE_ALL.FK_SITE_ENROLLING IS 'The Site or Organization Patient is 
Enrolled for'
/

COMMENT ON COLUMN VDA_V_AE_ALL.STUDY_NUMBER IS 'The Study Number the Adverse Event is 
reported'
/

COMMENT ON COLUMN VDA_V_AE_ALL.STUDY_TITLE IS 'Title of the Study the Adverse Event is 
reported on'
/

COMMENT ON COLUMN VDA_V_AE_ALL.STUDY_PI IS 'Principal Investigator of the study'
/

COMMENT ON COLUMN VDA_V_AE_ALL.STUDY_TAREA IS 'Therapeutic Area of the study'
/

COMMENT ON COLUMN VDA_V_AE_ALL.STUDY_DISEASE_SITE IS 'The Disease site associated with the 
study'
/

COMMENT ON COLUMN VDA_V_AE_ALL.AE_TYPE IS 'Description of the Adverse Event Type'
/

COMMENT ON COLUMN VDA_V_AE_ALL.AE_DESC IS 'Adverse Event''s description'
/

COMMENT ON COLUMN VDA_V_AE_ALL.AE_STDATE IS 'The Start Date of the Adverse Event'
/


--
-- VDA_V_ACCTFORMS_AUDIT  (View) 
--
--  Dependencies: 
--   ER_FORMLIB (Table)
--   ER_FORMAUDITCOL (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_ACCTFORMS_AUDIT
(PK_FORMAUDITCOL, FK_FILLEDFORM, FK_FORM, FORM_NAME, FA_SYSTEMID, 
 FA_DATETIME, FA_FLDNAME, FA_OLDVALUE, FA_NEWVALUE, FA_MODIFIEDBY_NAME, 
 FA_REASON, FK_ACCOUNT)
AS 
SELECT   PK_FORMAUDITCOL,
            FK_FILLEDFORM,
            FK_FORM,
            form_name,
            FA_SYSTEMID,
            FA_DATETIME,
            FA_FLDNAME,
            FA_OLDVALUE,
            FA_NEWVALUE,
            FA_MODIFIEDBY_NAME,
            FA_REASON,
            b.fk_Account
     FROM   eres.er_formauditcol, eres.er_formlib b
    WHERE   FA_FORMTYPE = 'A' AND b.pk_formlib = FK_FORM
/

COMMENT ON TABLE VDA_V_ACCTFORMS_AUDIT IS 'This view provides access to the column update level Audit information of account level forms'
/

COMMENT ON COLUMN VDA_V_ACCTFORMS_AUDIT.PK_FORMAUDITCOL IS 'The Primary Key of the Audit Transaction'
/

COMMENT ON COLUMN VDA_V_ACCTFORMS_AUDIT.FK_FILLEDFORM IS 'The FK to the form response (account form)'
/

COMMENT ON COLUMN VDA_V_ACCTFORMS_AUDIT.FK_FORM IS 'The FK to the form'
/

COMMENT ON COLUMN VDA_V_ACCTFORMS_AUDIT.FORM_NAME IS 'The form name'
/

COMMENT ON COLUMN VDA_V_ACCTFORMS_AUDIT.FA_SYSTEMID IS 'The systemid of the field'
/

COMMENT ON COLUMN VDA_V_ACCTFORMS_AUDIT.FA_DATETIME IS 'The date-time of the transaction'
/

COMMENT ON COLUMN VDA_V_ACCTFORMS_AUDIT.FA_FLDNAME IS 'The user defined field name'
/

COMMENT ON COLUMN VDA_V_ACCTFORMS_AUDIT.FA_OLDVALUE IS 'The old value of the field'
/

COMMENT ON COLUMN VDA_V_ACCTFORMS_AUDIT.FA_NEWVALUE IS 'The new value of the field'
/

COMMENT ON COLUMN VDA_V_ACCTFORMS_AUDIT.FA_MODIFIEDBY_NAME IS 'The user who modified the form response data'
/

COMMENT ON COLUMN VDA_V_ACCTFORMS_AUDIT.FA_REASON IS 'The reason for change'
/

COMMENT ON COLUMN VDA_V_ACCTFORMS_AUDIT.FK_ACCOUNT IS 'The account the form response is linked with'
/


--
-- VDA_V_ACCOUNTFORMRESPONSES  (View) 
--
--  Dependencies: 
--   ER_FORMSLINEAR (Table)
--   ER_FORMLIB (Table)
--   ER_USER (Table)
--
CREATE OR REPLACE FORCE VIEW VDA_V_ACCOUNTFORMRESPONSES
(FORM_NAME, FORM_DESC, FORM_VERSION, FK_FORM, FILLDATE, 
 FORM_TYPE, FK_ACCOUNT, COL1, COL2, COL3, 
 COL4, COL5, COL6, COL7, COL8, 
 COL9, COL10, COL11, COL12, COL13, 
 COL14, COL15, COL16, COL17, COL18, 
 COL19, COL20, COL21, COL22, COL23, 
 COL24, COL25, COL26, COL27, COL28, 
 COL29, COL30, COL31, COL32, COL33, 
 COL34, COL35, COL36, COL37, COL38, 
 COL39, COL40, COL41, COL42, COL43, 
 COL44, COL45, COL46, COL47, COL48, 
 COL49, COL50, COL51, COL52, COL53, 
 COL54, COL55, COL56, COL57, COL58, 
 COL59, COL60, COL61, COL62, COL63, 
 COL64, COL65, COL66, COL67, COL68, 
 COL69, COL70, COL71, COL72, COL73, 
 COL74, COL75, COL76, COL77, COL78, 
 COL79, COL80, COL81, COL82, COL83, 
 COL84, COL85, COL86, COL87, COL88, 
 COL89, COL90, COL91, COL92, COL93, 
 COL94, COL95, COL96, COL97, COL98, 
 COL99, COL100, COL101, COL102, COL103, 
 COL104, COL105, COL106, COL107, COL108, 
 COL109, COL110, COL111, COL112, COL113, 
 COL114, COL115, COL116, COL117, COL118, 
 COL119, COL120, COL121, COL122, COL123, 
 COL124, COL125, COL126, COL127, COL128, 
 COL129, COL130, COL131, COL132, COL133, 
 COL134, COL135, COL136, COL137, COL138, 
 COL139, COL140, COL141, COL142, COL143, 
 COL144, COL145, COL146, COL147, COL148, 
 COL149, COL150, COL151, COL152, COL153, 
 COL154, COL155, COL156, COL157, COL158, 
 COL159, COL160, COL161, COL162, COL163, 
 COL164, COL165, COL166, COL167, COL168, 
 COL169, COL170, COL171, COL172, COL173, 
 COL174, COL175, COL176, COL177, COL178, 
 COL179, COL180, COL181, COL182, COL183, 
 COL184, COL185, COL186, COL187, COL188, 
 COL189, COL190, COL191, COL192, COL193, 
 COL194, COL195, COL196, COL197, COL198, 
 COL199, COL200, COL201, COL202, COL203, 
 COL204, COL205, COL206, COL207, COL208, 
 COL209, COL210, COL211, COL212, COL213, 
 COL214, COL215, COL216, COL217, COL218, 
 COL219, COL220, COL221, COL222, COL223, 
 COL224, COL225, COL226, COL227, COL228, 
 COL229, COL230, COL231, COL232, COL233, 
 COL234, COL235, COL236, COL237, COL238, 
 COL239, COL240, COL241, COL242, COL243, 
 COL244, COL245, COL246, COL247, COL248, 
 COL249, COL250, COL251, COL252, COL253, 
 COL254, COL255, COL256, COL257, COL258, 
 COL259, COL260, COL261, COL262, COL263, 
 COL264, COL265, COL266, COL267, COL268, 
 COL269, COL270, COL271, COL272, COL273, 
 COL274, COL275, COL276, COL277, COL278, 
 COL279, COL280, COL281, COL282, COL283, 
 COL284, COL285, COL286, COL287, COL288, 
 COL289, COL290, COL291, COL292, COL293, 
 COL294, COL295, COL296, COL297, COL298, 
 COL299, COL300, COL301, COL302, COL303, 
 COL304, COL305, COL306, COL307, COL308, 
 COL309, COL310, COL311, COL312, COL313, 
 COL314, COL315, COL316, COL317, COL318, 
 COL319, COL320, COL321, COL322, COL323, 
 COL324, COL325, COL326, COL327, COL328, 
 COL329, COL330, COL331, COL332, COL333, 
 COL334, COL335, COL336, COL337, COL338, 
 COL339, COL340, COL341, COL342, COL343, 
 COL344, COL345, COL346, COL347, COL348, 
 COL349, COL350, COL351, COL352, COL353, 
 COL354, COL355, COL356, COL357, COL358, 
 COL359, COL360, COL361, COL362, COL363, 
 COL364, COL365, COL366, COL367, COL368, 
 COL369, COL370, COL371, COL372, COL373, 
 COL374, COL375, COL376, COL377, COL378, 
 COL379, COL380, COL381, COL382, COL383, 
 COL384, COL385, COL386, COL387, COL388, 
 COL389, COL390, COL391, COL392, COL393, 
 COL394, COL395, COL396, COL397, COL398, 
 COL399, COL400, COL401, COL402, COL403, 
 COL404, COL405, COL406, COL407, COL408, 
 COL409, COL410, COL411, COL412, COL413, 
 COL414, COL415, COL416, COL417, COL418, 
 COL419, COL420, COL421, COL422, COL423, 
 COL424, COL425, COL426, COL427, COL428, 
 COL429, COL430, COL431, COL432, COL433, 
 COL434, COL435, COL436, COL437, COL438, 
 COL439, COL440, COL441, COL442, COL443, 
 COL444, COL445, COL446, COL447, COL448, 
 COL449, COL450, COL451, COL452, COL453, 
 COL454, COL455, COL456, COL457, COL458, 
 COL459, COL460, COL461, COL462, COL463, 
 COL464, COL465, COL466, COL467, COL468, 
 COL469, COL470, COL471, COL472, COL473, 
 COL474, COL475, COL476, COL477, COL478, 
 COL479, COL480, COL481, COL482, COL483, 
 COL484, COL485, COL486, COL487, COL488, 
 COL489, COL490, COL491, COL492, COL493, 
 COL494, COL495, COL496, COL497, COL498, 
 COL499, COL500, COL501, COL502, COL503, 
 COL504, COL505, COL506, COL507, COL508, 
 COL509, COL510, COL511, COL512, COL513, 
 COL514, COL515, COL516, COL517, COL518, 
 COL519, COL520, COL521, COL522, COL523, 
 COL524, COL525, COL526, COL527, COL528, 
 COL529, COL530, COL531, COL532, COL533, 
 COL534, COL535, COL536, COL537, COL538, 
 COL539, COL540, COL541, COL542, COL543, 
 COL544, COL545, COL546, COL547, COL548, 
 COL549, COL550, COL551, COL552, COL553, 
 COL554, COL555, COL556, COL557, COL558, 
 COL559, COL560, COL561, COL562, COL563, 
 COL564, COL565, COL566, COL567, COL568, 
 COL569, COL570, COL571, COL572, COL573, 
 COL574, COL575, COL576, COL577, COL578, 
 COL579, COL580, COL581, COL582, COL583, 
 COL584, COL585, COL586, COL587, COL588, 
 COL589, COL590, COL591, COL592, COL593, 
 COL594, COL595, COL596, COL597, COL598, 
 COL599, COL600, COL601, COL602, COL603, 
 COL604, COL605, COL606, COL607, COL608, 
 COL609, COL610, COL611, COL612, COL613, 
 COL614, COL615, COL616, COL617, COL618, 
 COL619, COL620, COL621, COL622, COL623, 
 COL624, COL625, COL626, COL627, COL628, 
 COL629, COL630, COL631, COL632, COL633, 
 COL634, COL635, COL636, COL637, COL638, 
 COL639, COL640, COL641, COL642, COL643, 
 COL644, COL645, COL646, COL647, COL648, 
 COL649, COL650, COL651, COL652, COL653, 
 COL654, COL655, COL656, COL657, COL658, 
 COL659, COL660, COL661, COL662, COL663, 
 COL664, COL665, COL666, COL667, COL668, 
 COL669, COL670, COL671, COL672, COL673, 
 COL674, COL675, COL676, COL677, COL678, 
 COL679, COL680, COL681, COL682, COL683, 
 COL684, COL685, COL686, COL687, COL688, 
 COL689, COL690, COL691, COL692, COL693, 
 COL694, COL695, COL696, COL697, COL698, 
 COL699, COL700, COL701, COL702, COL703, 
 COL704, COL705, COL706, COL707, COL708, 
 COL709, COL710, COL711, COL712, COL713, 
 COL714, COL715, COL716, COL717, COL718, 
 COL719, COL720, COL721, COL722, COL723, 
 COL724, COL725, COL726, COL727, COL728, 
 COL729, COL730, COL731, COL732, COL733, 
 COL734, COL735, COL736, COL737, COL738, 
 COL739, COL740, COL741, COL742, COL743, 
 COL744, COL745, COL746, COL747, COL748, 
 COL749, COL750, COL751, COL752, COL753, 
 COL754, COL755, COL756, COL757, COL758, 
 COL759, COL760, COL761, COL762, COL763, 
 COL764, COL765, COL766, COL767, COL768, 
 COL769, COL770, COL771, COL772, COL773, 
 COL774, COL775, COL776, COL777, COL778, 
 COL779, COL780, COL781, COL782, COL783, 
 COL784, COL785, COL786, COL787, COL788, 
 COL789, COL790, COL791, COL792, COL793, 
 COL794, COL795, COL796, COL797, COL798, 
 COL799, COL800, COL801, COL802, COL803, 
 COL804, COL805, COL806, COL807, COL808, 
 COL809, COL810, COL811, COL812, COL813, 
 COL814, COL815, COL816, COL817, COL818, 
 COL819, COL820, COL821, COL822, COL823, 
 COL824, COL825, COL826, COL827, COL828, 
 COL829, COL830, COL831, COL832, COL833, 
 COL834, COL835, COL836, COL837, COL838, 
 COL839, COL840, COL841, COL842, COL843, 
 COL844, COL845, COL846, COL847, COL848, 
 COL849, COL850, COL851, COL852, COL853, 
 COL854, COL855, COL856, COL857, COL858, 
 COL859, COL860, COL861, COL862, COL863, 
 COL864, COL865, COL866, COL867, COL868, 
 COL869, COL870, COL871, COL872, COL873, 
 COL874, COL875, COL876, COL877, COL878, 
 COL879, COL880, COL881, COL882, COL883, 
 COL884, COL885, COL886, COL887, COL888, 
 COL889, COL890, COL891, COL892, COL893, 
 COL894, COL895, COL896, COL897, COL898, 
 COL899, COL900, COL901, COL902, COL903, 
 COL904, COL905, COL906, COL907, COL908, 
 COL909, COL910, COL911, COL912, COL913, 
 COL914, COL915, COL916, COL917, COL918, 
 COL919, COL920, COL921, COL922, COL923, 
 COL924, COL925, COL926, COL927, COL928, 
 COL929, COL930, COL931, COL932, COL933, 
 COL934, COL935, COL936, COL937, COL938, 
 COL939, COL940, COL941, COL942, COL943, 
 COL944, COL945, COL946, COL947, COL948, 
 COL949, COL950, CREATOR, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, 
 CREATED_ON, FK_SPECIMEN, CREATOR_FK, LAST_MODIFIED_BY_FK)
AS 
SELECT   form_name,
            form_desc,
            form_version,
            fk_form,
            filldate,
            form_type,
            id AS fk_Account,
            COL1,
            COL2,
            COL3,
            COL4,
            COL5,
            COL6,
            COL7,
            COL8,
            COL9,
            COL10,
            COL11,
            COL12,
            COL13,
            COL14,
            COL15,
            COL16,
            COL17,
            COL18,
            COL19,
            COL20,
            COL21,
            COL22,
            COL23,
            COL24,
            COL25,
            COL26,
            COL27,
            COL28,
            COL29,
            COL30,
            COL31,
            COL32,
            COL33,
            COL34,
            COL35,
            COL36,
            COL37,
            COL38,
            COL39,
            COL40,
            COL41,
            COL42,
            COL43,
            COL44,
            COL45,
            COL46,
            COL47,
            COL48,
            COL49,
            COL50,
            COL51,
            COL52,
            COL53,
            COL54,
            COL55,
            COL56,
            COL57,
            COL58,
            COL59,
            COL60,
            COL61,
            COL62,
            COL63,
            COL64,
            COL65,
            COL66,
            COL67,
            COL68,
            COL69,
            COL70,
            COL71,
            COL72,
            COL73,
            COL74,
            COL75,
            COL76,
            COL77,
            COL78,
            COL79,
            COL80,
            COL81,
            COL82,
            COL83,
            COL84,
            COL85,
            COL86,
            COL87,
            COL88,
            COL89,
            COL90,
            COL91,
            COL92,
            COL93,
            COL94,
            COL95,
            COL96,
            COL97,
            COL98,
            COL99,
            COL100,
            COL101,
            COL102,
            COL103,
            COL104,
            COL105,
            COL106,
            COL107,
            COL108,
            COL109,
            COL110,
            COL111,
            COL112,
            COL113,
            COL114,
            COL115,
            COL116,
            COL117,
            COL118,
            COL119,
            COL120,
            COL121,
            COL122,
            COL123,
            COL124,
            COL125,
            COL126,
            COL127,
            COL128,
            COL129,
            COL130,
            COL131,
            COL132,
            COL133,
            COL134,
            COL135,
            COL136,
            COL137,
            COL138,
            COL139,
            COL140,
            COL141,
            COL142,
            COL143,
            COL144,
            COL145,
            COL146,
            COL147,
            COL148,
            COL149,
            COL150,
            COL151,
            COL152,
            COL153,
            COL154,
            COL155,
            COL156,
            COL157,
            COL158,
            COL159,
            COL160,
            COL161,
            COL162,
            COL163,
            COL164,
            COL165,
            COL166,
            COL167,
            COL168,
            COL169,
            COL170,
            COL171,
            COL172,
            COL173,
            COL174,
            COL175,
            COL176,
            COL177,
            COL178,
            COL179,
            COL180,
            COL181,
            COL182,
            COL183,
            COL184,
            COL185,
            COL186,
            COL187,
            COL188,
            COL189,
            COL190,
            COL191,
            COL192,
            COL193,
            COL194,
            COL195,
            COL196,
            COL197,
            COL198,
            COL199,
            COL200,
            COL201,
            COL202,
            COL203,
            COL204,
            COL205,
            COL206,
            COL207,
            COL208,
            COL209,
            COL210,
            COL211,
            COL212,
            COL213,
            COL214,
            COL215,
            COL216,
            COL217,
            COL218,
            COL219,
            COL220,
            COL221,
            COL222,
            COL223,
            COL224,
            COL225,
            COL226,
            COL227,
            COL228,
            COL229,
            COL230,
            COL231,
            COL232,
            COL233,
            COL234,
            COL235,
            COL236,
            COL237,
            COL238,
            COL239,
            COL240,
            COL241,
            COL242,
            COL243,
            COL244,
            COL245,
            COL246,
            COL247,
            COL248,
            COL249,
            COL250,
            COL251,
            COL252,
            COL253,
            COL254,
            COL255,
            COL256,
            COL257,
            COL258,
            COL259,
            COL260,
            COL261,
            COL262,
            COL263,
            COL264,
            COL265,
            COL266,
            COL267,
            COL268,
            COL269,
            COL270,
            COL271,
            COL272,
            COL273,
            COL274,
            COL275,
            COL276,
            COL277,
            COL278,
            COL279,
            COL280,
            COL281,
            COL282,
            COL283,
            COL284,
            COL285,
            COL286,
            COL287,
            COL288,
            COL289,
            COL290,
            COL291,
            COL292,
            COL293,
            COL294,
            COL295,
            COL296,
            COL297,
            COL298,
            COL299,
            COL300,
            COL301,
            COL302,
            COL303,
            COL304,
            COL305,
            COL306,
            COL307,
            COL308,
            COL309,
            COL310,
            COL311,
            COL312,
            COL313,
            COL314,
            COL315,
            COL316,
            COL317,
            COL318,
            COL319,
            COL320,
            COL321,
            COL322,
            COL323,
            COL324,
            COL325,
            COL326,
            COL327,
            COL328,
            COL329,
            COL330,
            COL331,
            COL332,
            COL333,
            COL334,
            COL335,
            COL336,
            COL337,
            COL338,
            COL339,
            COL340,
            COL341,
            COL342,
            COL343,
            COL344,
            COL345,
            COL346,
            COL347,
            COL348,
            COL349,
            COL350,
            COL351,
            COL352,
            COL353,
            COL354,
            COL355,
            COL356,
            COL357,
            COL358,
            COL359,
            COL360,
            COL361,
            COL362,
            COL363,
            COL364,
            COL365,
            COL366,
            COL367,
            COL368,
            COL369,
            COL370,
            COL371,
            COL372,
            COL373,
            COL374,
            COL375,
            COL376,
            COL377,
            COL378,
            COL379,
            COL380,
            COL381,
            COL382,
            COL383,
            COL384,
            COL385,
            COL386,
            COL387,
            COL388,
            COL389,
            COL390,
            COL391,
            COL392,
            COL393,
            COL394,
            COL395,
            COL396,
            COL397,
            COL398,
            COL399,
            COL400,
            COL401,
            COL402,
            COL403,
            COL404,
            COL405,
            COL406,
            COL407,
            COL408,
            COL409,
            COL410,
            COL411,
            COL412,
            COL413,
            COL414,
            COL415,
            COL416,
            COL417,
            COL418,
            COL419,
            COL420,
            COL421,
            COL422,
            COL423,
            COL424,
            COL425,
            COL426,
            COL427,
            COL428,
            COL429,
            COL430,
            COL431,
            COL432,
            COL433,
            COL434,
            COL435,
            COL436,
            COL437,
            COL438,
            COL439,
            COL440,
            COL441,
            COL442,
            COL443,
            COL444,
            COL445,
            COL446,
            COL447,
            COL448,
            COL449,
            COL450,
            COL451,
            COL452,
            COL453,
            COL454,
            COL455,
            COL456,
            COL457,
            COL458,
            COL459,
            COL460,
            COL461,
            COL462,
            COL463,
            COL464,
            COL465,
            COL466,
            COL467,
            COL468,
            COL469,
            COL470,
            COL471,
            COL472,
            COL473,
            COL474,
            COL475,
            COL476,
            COL477,
            COL478,
            COL479,
            COL480,
            COL481,
            COL482,
            COL483,
            COL484,
            COL485,
            COL486,
            COL487,
            COL488,
            COL489,
            COL490,
            COL491,
            COL492,
            COL493,
            COL494,
            COL495,
            COL496,
            COL497,
            COL498,
            COL499,
            COL500,
            COL501,
            COL502,
            COL503,
            COL504,
            COL505,
            COL506,
            COL507,
            COL508,
            COL509,
            COL510,
            COL511,
            COL512,
            COL513,
            COL514,
            COL515,
            COL516,
            COL517,
            COL518,
            COL519,
            COL520,
            COL521,
            COL522,
            COL523,
            COL524,
            COL525,
            COL526,
            COL527,
            COL528,
            COL529,
            COL530,
            COL531,
            COL532,
            COL533,
            COL534,
            COL535,
            COL536,
            COL537,
            COL538,
            COL539,
            COL540,
            COL541,
            COL542,
            COL543,
            COL544,
            COL545,
            COL546,
            COL547,
            COL548,
            COL549,
            COL550,
            COL551,
            COL552,
            COL553,
            COL554,
            COL555,
            COL556,
            COL557,
            COL558,
            COL559,
            COL560,
            COL561,
            COL562,
            COL563,
            COL564,
            COL565,
            COL566,
            COL567,
            COL568,
            COL569,
            COL570,
            COL571,
            COL572,
            COL573,
            COL574,
            COL575,
            COL576,
            COL577,
            COL578,
            COL579,
            COL580,
            COL581,
            COL582,
            COL583,
            COL584,
            COL585,
            COL586,
            COL587,
            COL588,
            COL589,
            COL590,
            COL591,
            COL592,
            COL593,
            COL594,
            COL595,
            COL596,
            COL597,
            COL598,
            COL599,
            COL600,
            COL601,
            COL602,
            COL603,
            COL604,
            COL605,
            COL606,
            COL607,
            COL608,
            COL609,
            COL610,
            COL611,
            COL612,
            COL613,
            COL614,
            COL615,
            COL616,
            COL617,
            COL618,
            COL619,
            COL620,
            COL621,
            COL622,
            COL623,
            COL624,
            COL625,
            COL626,
            COL627,
            COL628,
            COL629,
            COL630,
            COL631,
            COL632,
            COL633,
            COL634,
            COL635,
            COL636,
            COL637,
            COL638,
            COL639,
            COL640,
            COL641,
            COL642,
            COL643,
            COL644,
            COL645,
            COL646,
            COL647,
            COL648,
            COL649,
            COL650,
            COL651,
            COL652,
            COL653,
            COL654,
            COL655,
            COL656,
            COL657,
            COL658,
            COL659,
            COL660,
            COL661,
            COL662,
            COL663,
            COL664,
            COL665,
            COL666,
            COL667,
            COL668,
            COL669,
            COL670,
            COL671,
            COL672,
            COL673,
            COL674,
            COL675,
            COL676,
            COL677,
            COL678,
            COL679,
            COL680,
            COL681,
            COL682,
            COL683,
            COL684,
            COL685,
            COL686,
            COL687,
            COL688,
            COL689,
            COL690,
            COL691,
            COL692,
            COL693,
            COL694,
            COL695,
            COL696,
            COL697,
            COL698,
            COL699,
            COL700,
            COL701,
            COL702,
            COL703,
            COL704,
            COL705,
            COL706,
            COL707,
            COL708,
            COL709,
            COL710,
            COL711,
            COL712,
            COL713,
            COL714,
            COL715,
            COL716,
            COL717,
            COL718,
            COL719,
            COL720,
            COL721,
            COL722,
            COL723,
            COL724,
            COL725,
            COL726,
            COL727,
            COL728,
            COL729,
            COL730,
            COL731,
            COL732,
            COL733,
            COL734,
            COL735,
            COL736,
            COL737,
            COL738,
            COL739,
            COL740,
            COL741,
            COL742,
            COL743,
            COL744,
            COL745,
            COL746,
            COL747,
            COL748,
            COL749,
            COL750,
            COL751,
            COL752,
            COL753,
            COL754,
            COL755,
            COL756,
            COL757,
            COL758,
            COL759,
            COL760,
            COL761,
            COL762,
            COL763,
            COL764,
            COL765,
            COL766,
            COL767,
            COL768,
            COL769,
            COL770,
            COL771,
            COL772,
            COL773,
            COL774,
            COL775,
            COL776,
            COL777,
            COL778,
            COL779,
            COL780,
            COL781,
            COL782,
            COL783,
            COL784,
            COL785,
            COL786,
            COL787,
            COL788,
            COL789,
            COL790,
            COL791,
            COL792,
            COL793,
            COL794,
            COL795,
            COL796,
            COL797,
            COL798,
            COL799,
            COL800,
            COL801,
            COL802,
            COL803,
            COL804,
            COL805,
            COL806,
            COL807,
            COL808,
            COL809,
            COL810,
            COL811,
            COL812,
            COL813,
            COL814,
            COL815,
            COL816,
            COL817,
            COL818,
            COL819,
            COL820,
            COL821,
            COL822,
            COL823,
            COL824,
            COL825,
            COL826,
            COL827,
            COL828,
            COL829,
            COL830,
            COL831,
            COL832,
            COL833,
            COL834,
            COL835,
            COL836,
            COL837,
            COL838,
            COL839,
            COL840,
            COL841,
            COL842,
            COL843,
            COL844,
            COL845,
            COL846,
            COL847,
            COL848,
            COL849,
            COL850,
            COL851,
            COL852,
            COL853,
            COL854,
            COL855,
            COL856,
            COL857,
            COL858,
            COL859,
            COL860,
            COL861,
            COL862,
            COL863,
            COL864,
            COL865,
            COL866,
            COL867,
            COL868,
            COL869,
            COL870,
            COL871,
            COL872,
            COL873,
            COL874,
            COL875,
            COL876,
            COL877,
            COL878,
            COL879,
            COL880,
            COL881,
            COL882,
            COL883,
            COL884,
            COL885,
            COL886,
            COL887,
            COL888,
            COL889,
            COL890,
            COL891,
            COL892,
            COL893,
            COL894,
            COL895,
            COL896,
            COL897,
            COL898,
            COL899,
            COL900,
            COL901,
            COL902,
            COL903,
            COL904,
            COL905,
            COL906,
            COL907,
            COL908,
            COL909,
            COL910,
            COL911,
            COL912,
            COL913,
            COL914,
            COL915,
            COL916,
            COL917,
            COL918,
            COL919,
            COL920,
            COL921,
            COL922,
            COL923,
            COL924,
            COL925,
            COL926,
            COL927,
            COL928,
            COL929,
            COL930,
            COL931,
            COL932,
            COL933,
            COL934,
            COL935,
            COL936,
            COL937,
            COL938,
            COL939,
            COL940,
            COL941,
            COL942,
            COL943,
            COL944,
            COL945,
            COL946,
            COL947,
            COL948,
            COL949,
            COL950,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = l.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = l.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            l.LAST_MODIFIED_DATE,
            l.CREATED_ON,
            FK_SPECIMEN,
            l.CREATOR as CREATOR_FK,
            l.LAST_MODIFIED_BY as LAST_MODIFIED_BY_FK    
     FROM   eres.er_formslinear l, eres.er_formlib
    WHERE   form_type = 'A' AND fk_form = pk_formlib
/

COMMENT ON TABLE VDA_V_ACCOUNTFORMRESPONSES IS 'This view provides access to responses to 

all account level forms'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL938 IS 'Col938 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL939 IS 'Col939 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL940 IS 'Col940 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL941 IS 'Col941 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL942 IS 'Col942 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL943 IS 'Col943 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL944 IS 'Col944 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL945 IS 'Col945 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL946 IS 'Col946 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL947 IS 'Col947 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL948 IS 'Col948 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL949 IS 'Col949 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL950 IS 'Col950 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.CREATOR IS 'User who created record'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.LAST_MODIFIED_BY IS 'User who last modified 
the record'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.LAST_MODIFIED_DATE IS 'Last modified date'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.CREATED_ON IS 'Date record was created on'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.FK_SPECIMEN IS 'ID to identify the specimen a 
response is linked with'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.CREATOR_FK IS 'The Key to identify the 
Creator of the record.'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.LAST_MODIFIED_BY_FK IS 'The Key to identify 
the Last Modified By of the record.'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.FORM_NAME IS 'Form name'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.FORM_DESC IS 'Form Description'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.FORM_VERSION IS 'Form version number'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.FK_FORM IS 'ID to identify a form record 
uniquely'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.FILLDATE IS 'Date the response was filled on'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.FORM_TYPE IS 'Type of the  form- always A for 
account level forms'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.FK_ACCOUNT IS 'The account the response is 
linked with. Applicable to Velos hosted or shared environments'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL1 IS 'Col1 data of the form response. The 
view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL2 IS 'Col2 data of the form response. The 
view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL3 IS 'Col3 data of the form response. The 
view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL4 IS 'Col4 data of the form response. The 
view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL5 IS 'Col5 data of the form response. The 
view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL6 IS 'Col6 data of the form response. The 
view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL7 IS 'Col7 data of the form response. The 
view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL8 IS 'Col8 data of the form response. The 
view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL9 IS 'Col9 data of the form response. The 
view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this column 
for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL10 IS 'Col10 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL11 IS 'Col11 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL12 IS 'Col12 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL13 IS 'Col13 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL14 IS 'Col14 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL15 IS 'Col15 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL16 IS 'Col16 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL17 IS 'Col17 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL18 IS 'Col18 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL19 IS 'Col19 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL20 IS 'Col20 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL21 IS 'Col21 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL22 IS 'Col22 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL23 IS 'Col23 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL24 IS 'Col24 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL25 IS 'Col25 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL26 IS 'Col26 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL27 IS 'Col27 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL28 IS 'Col28 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL29 IS 'Col29 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL30 IS 'Col30 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL31 IS 'Col31 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL32 IS 'Col32 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL33 IS 'Col33 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL34 IS 'Col34 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL35 IS 'Col35 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL36 IS 'Col36 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL37 IS 'Col37 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL38 IS 'Col38 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL39 IS 'Col39 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL40 IS 'Col40 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL41 IS 'Col41 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL42 IS 'Col42 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL43 IS 'Col43 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL44 IS 'Col44 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL45 IS 'Col45 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL46 IS 'Col46 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL47 IS 'Col47 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL48 IS 'Col48 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL49 IS 'Col49 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL50 IS 'Col50 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL51 IS 'Col51 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL52 IS 'Col52 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL53 IS 'Col53 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL54 IS 'Col54 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL55 IS 'Col55 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL56 IS 'Col56 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL57 IS 'Col57 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL58 IS 'Col58 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL59 IS 'Col59 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL60 IS 'Col60 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL61 IS 'Col61 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL62 IS 'Col62 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL63 IS 'Col63 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL64 IS 'Col64 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL65 IS 'Col65 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL66 IS 'Col66 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL67 IS 'Col67 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL68 IS 'Col68 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL69 IS 'Col69 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL70 IS 'Col70 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL71 IS 'Col71 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL72 IS 'Col72 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL73 IS 'Col73 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL74 IS 'Col74 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL75 IS 'Col75 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL76 IS 'Col76 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL77 IS 'Col77 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL78 IS 'Col78 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL79 IS 'Col79 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL80 IS 'Col80 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL81 IS 'Col81 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL82 IS 'Col82 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL83 IS 'Col83 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL84 IS 'Col84 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL85 IS 'Col85 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL86 IS 'Col86 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL87 IS 'Col87 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL88 IS 'Col88 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL89 IS 'Col89 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL90 IS 'Col90 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL91 IS 'Col91 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL92 IS 'Col92 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL93 IS 'Col93 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL94 IS 'Col94 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL95 IS 'Col95 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL96 IS 'Col96 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL97 IS 'Col97 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL98 IS 'Col98 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL99 IS 'Col99 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL100 IS 'Col100 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL101 IS 'Col101 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL102 IS 'Col102 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL103 IS 'Col103 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL104 IS 'Col104 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL105 IS 'Col105 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL106 IS 'Col106 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL107 IS 'Col107 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL108 IS 'Col108 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL109 IS 'Col109 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL110 IS 'Col110 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL111 IS 'Col111 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL112 IS 'Col112 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL113 IS 'Col113 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL114 IS 'Col114 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL115 IS 'Col115 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL116 IS 'Col116 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL117 IS 'Col117 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL118 IS 'Col118 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL119 IS 'Col119 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL120 IS 'Col120 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL121 IS 'Col121 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL122 IS 'Col122 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL123 IS 'Col123 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL124 IS 'Col124 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL125 IS 'Col125 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL126 IS 'Col126 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL127 IS 'Col127 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL128 IS 'Col128 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL129 IS 'Col129 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL130 IS 'Col130 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL131 IS 'Col131 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL132 IS 'Col132 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL133 IS 'Col133 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL134 IS 'Col134 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL135 IS 'Col135 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL136 IS 'Col136 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL137 IS 'Col137 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL138 IS 'Col138 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL139 IS 'Col139 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL140 IS 'Col140 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL141 IS 'Col141 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL142 IS 'Col142 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL143 IS 'Col143 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL144 IS 'Col144 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL145 IS 'Col145 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL146 IS 'Col146 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL147 IS 'Col147 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL148 IS 'Col148 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL149 IS 'Col149 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL150 IS 'Col150 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL151 IS 'Col151 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL152 IS 'Col152 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL153 IS 'Col153 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL154 IS 'Col154 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL155 IS 'Col155 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL156 IS 'Col156 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL157 IS 'Col157 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL158 IS 'Col158 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL159 IS 'Col159 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL160 IS 'Col160 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL161 IS 'Col161 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL162 IS 'Col162 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL163 IS 'Col163 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL164 IS 'Col164 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL165 IS 'Col165 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL166 IS 'Col166 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL167 IS 'Col167 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL168 IS 'Col168 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL169 IS 'Col169 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL170 IS 'Col170 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL171 IS 'Col171 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL172 IS 'Col172 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL173 IS 'Col173 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL174 IS 'Col174 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL175 IS 'Col175 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL176 IS 'Col176 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL177 IS 'Col177 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL178 IS 'Col178 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL179 IS 'Col179 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL180 IS 'Col180 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL181 IS 'Col181 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL182 IS 'Col182 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL183 IS 'Col183 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL184 IS 'Col184 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL185 IS 'Col185 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL186 IS 'Col186 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL187 IS 'Col187 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL188 IS 'Col188 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL189 IS 'Col189 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL190 IS 'Col190 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL191 IS 'Col191 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL192 IS 'Col192 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL193 IS 'Col193 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL194 IS 'Col194 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL195 IS 'Col195 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL196 IS 'Col196 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL197 IS 'Col197 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL198 IS 'Col198 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL199 IS 'Col199 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL200 IS 'Col200 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL201 IS 'Col201 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL202 IS 'Col202 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL203 IS 'Col203 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL204 IS 'Col204 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL205 IS 'Col205 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL206 IS 'Col206 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL207 IS 'Col207 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL208 IS 'Col208 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL209 IS 'Col209 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL210 IS 'Col210 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL211 IS 'Col211 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL212 IS 'Col212 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL213 IS 'Col213 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL214 IS 'Col214 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL215 IS 'Col215 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL216 IS 'Col216 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL217 IS 'Col217 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL218 IS 'Col218 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL219 IS 'Col219 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL220 IS 'Col220 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL221 IS 'Col221 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL222 IS 'Col222 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL223 IS 'Col223 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL224 IS 'Col224 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL225 IS 'Col225 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL226 IS 'Col226 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL227 IS 'Col227 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL228 IS 'Col228 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL229 IS 'Col229 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL230 IS 'Col230 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL231 IS 'Col231 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL232 IS 'Col232 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL233 IS 'Col233 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL234 IS 'Col234 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL235 IS 'Col235 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL236 IS 'Col236 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL237 IS 'Col237 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL238 IS 'Col238 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL239 IS 'Col239 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL240 IS 'Col240 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL241 IS 'Col241 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL242 IS 'Col242 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL243 IS 'Col243 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL244 IS 'Col244 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL245 IS 'Col245 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL246 IS 'Col246 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL247 IS 'Col247 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL248 IS 'Col248 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL249 IS 'Col249 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL250 IS 'Col250 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL251 IS 'Col251 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL252 IS 'Col252 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL253 IS 'Col253 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL254 IS 'Col254 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL255 IS 'Col255 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL256 IS 'Col256 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL257 IS 'Col257 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL258 IS 'Col258 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL259 IS 'Col259 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL260 IS 'Col260 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL261 IS 'Col261 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL262 IS 'Col262 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL263 IS 'Col263 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL264 IS 'Col264 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL265 IS 'Col265 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL266 IS 'Col266 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL267 IS 'Col267 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL268 IS 'Col268 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL269 IS 'Col269 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL270 IS 'Col270 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL271 IS 'Col271 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL272 IS 'Col272 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL273 IS 'Col273 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL274 IS 'Col274 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL275 IS 'Col275 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL276 IS 'Col276 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL277 IS 'Col277 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL278 IS 'Col278 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL279 IS 'Col279 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL280 IS 'Col280 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL281 IS 'Col281 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL282 IS 'Col282 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL283 IS 'Col283 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL284 IS 'Col284 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL285 IS 'Col285 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL286 IS 'Col286 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL287 IS 'Col287 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL288 IS 'Col288 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL289 IS 'Col289 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL290 IS 'Col290 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL291 IS 'Col291 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL292 IS 'Col292 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL293 IS 'Col293 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL294 IS 'Col294 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL295 IS 'Col295 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL296 IS 'Col296 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL297 IS 'Col297 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL298 IS 'Col298 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL299 IS 'Col299 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL300 IS 'Col300 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL301 IS 'Col301 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL302 IS 'Col302 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL303 IS 'Col303 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL304 IS 'Col304 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL305 IS 'Col305 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL306 IS 'Col306 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL307 IS 'Col307 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL308 IS 'Col308 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL309 IS 'Col309 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL310 IS 'Col310 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL311 IS 'Col311 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL312 IS 'Col312 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL313 IS 'Col313 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL314 IS 'Col314 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL315 IS 'Col315 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL316 IS 'Col316 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL317 IS 'Col317 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL318 IS 'Col318 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL319 IS 'Col319 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL320 IS 'Col320 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL321 IS 'Col321 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL322 IS 'Col322 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL323 IS 'Col323 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL324 IS 'Col324 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL325 IS 'Col325 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL326 IS 'Col326 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL327 IS 'Col327 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL328 IS 'Col328 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL329 IS 'Col329 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL330 IS 'Col330 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL331 IS 'Col331 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL332 IS 'Col332 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL333 IS 'Col333 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL334 IS 'Col334 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL335 IS 'Col335 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL336 IS 'Col336 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL337 IS 'Col337 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL338 IS 'Col338 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL339 IS 'Col339 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL340 IS 'Col340 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL341 IS 'Col341 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL342 IS 'Col342 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL343 IS 'Col343 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL344 IS 'Col344 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL345 IS 'Col345 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL346 IS 'Col346 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL347 IS 'Col347 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL348 IS 'Col348 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL349 IS 'Col349 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL350 IS 'Col350 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL351 IS 'Col351 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL352 IS 'Col352 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL353 IS 'Col353 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL354 IS 'Col354 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL355 IS 'Col355 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL356 IS 'Col356 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL357 IS 'Col357 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL358 IS 'Col358 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL359 IS 'Col359 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL360 IS 'Col360 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL361 IS 'Col361 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL362 IS 'Col362 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL363 IS 'Col363 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL364 IS 'Col364 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL365 IS 'Col365 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL366 IS 'Col366 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL367 IS 'Col367 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL368 IS 'Col368 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL369 IS 'Col369 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL370 IS 'Col370 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL371 IS 'Col371 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL372 IS 'Col372 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL373 IS 'Col373 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL374 IS 'Col374 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL375 IS 'Col375 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL376 IS 'Col376 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL377 IS 'Col377 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL378 IS 'Col378 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL379 IS 'Col379 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL380 IS 'Col380 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL381 IS 'Col381 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL382 IS 'Col382 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL383 IS 'Col383 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL384 IS 'Col384 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL385 IS 'Col385 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL386 IS 'Col386 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL387 IS 'Col387 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL388 IS 'Col388 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL389 IS 'Col389 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL390 IS 'Col390 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL391 IS 'Col391 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL392 IS 'Col392 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL393 IS 'Col393 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL394 IS 'Col394 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL395 IS 'Col395 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL396 IS 'Col396 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL397 IS 'Col397 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL398 IS 'Col398 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL399 IS 'Col399 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL400 IS 'Col400 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL401 IS 'Col401 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL402 IS 'Col402 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL403 IS 'Col403 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL404 IS 'Col404 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL405 IS 'Col405 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL406 IS 'Col406 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL407 IS 'Col407 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL408 IS 'Col408 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL409 IS 'Col409 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL410 IS 'Col410 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL411 IS 'Col411 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL412 IS 'Col412 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL413 IS 'Col413 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL414 IS 'Col414 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL415 IS 'Col415 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL416 IS 'Col416 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL417 IS 'Col417 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL418 IS 'Col418 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL419 IS 'Col419 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL420 IS 'Col420 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL421 IS 'Col421 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL422 IS 'Col422 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL423 IS 'Col423 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL424 IS 'Col424 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL425 IS 'Col425 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL426 IS 'Col426 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL427 IS 'Col427 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL428 IS 'Col428 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL429 IS 'Col429 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL430 IS 'Col430 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL431 IS 'Col431 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL432 IS 'Col432 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL433 IS 'Col433 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL434 IS 'Col434 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL435 IS 'Col435 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL436 IS 'Col436 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL437 IS 'Col437 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL438 IS 'Col438 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL439 IS 'Col439 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL440 IS 'Col440 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL441 IS 'Col441 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL442 IS 'Col442 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL443 IS 'Col443 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL444 IS 'Col444 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL445 IS 'Col445 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL446 IS 'Col446 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL447 IS 'Col447 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL448 IS 'Col448 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL449 IS 'Col449 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL450 IS 'Col450 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL451 IS 'Col451 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL452 IS 'Col452 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL453 IS 'Col453 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL454 IS 'Col454 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL455 IS 'Col455 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL456 IS 'Col456 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL457 IS 'Col457 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL458 IS 'Col458 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL459 IS 'Col459 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL460 IS 'Col460 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL461 IS 'Col461 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL462 IS 'Col462 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL463 IS 'Col463 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL464 IS 'Col464 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL465 IS 'Col465 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL466 IS 'Col466 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL467 IS 'Col467 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL468 IS 'Col468 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL469 IS 'Col469 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL470 IS 'Col470 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL471 IS 'Col471 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL472 IS 'Col472 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL473 IS 'Col473 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL474 IS 'Col474 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL475 IS 'Col475 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL476 IS 'Col476 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL477 IS 'Col477 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL478 IS 'Col478 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL479 IS 'Col479 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL480 IS 'Col480 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL481 IS 'Col481 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL482 IS 'Col482 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL483 IS 'Col483 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL484 IS 'Col484 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL485 IS 'Col485 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL486 IS 'Col486 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL487 IS 'Col487 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL488 IS 'Col488 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL489 IS 'Col489 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL490 IS 'Col490 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL491 IS 'Col491 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL492 IS 'Col492 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL493 IS 'Col493 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL494 IS 'Col494 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL495 IS 'Col495 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL496 IS 'Col496 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL497 IS 'Col497 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL498 IS 'Col498 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL499 IS 'Col499 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL500 IS 'Col500 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL501 IS 'Col501 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL502 IS 'Col502 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL503 IS 'Col503 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL504 IS 'Col504 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL505 IS 'Col505 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL506 IS 'Col506 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL507 IS 'Col507 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL508 IS 'Col508 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL509 IS 'Col509 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL510 IS 'Col510 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL511 IS 'Col511 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL512 IS 'Col512 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL513 IS 'Col513 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL514 IS 'Col514 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL515 IS 'Col515 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL516 IS 'Col516 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL517 IS 'Col517 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL518 IS 'Col518 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL519 IS 'Col519 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL520 IS 'Col520 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL521 IS 'Col521 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL522 IS 'Col522 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL523 IS 'Col523 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL524 IS 'Col524 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL525 IS 'Col525 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL526 IS 'Col526 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL527 IS 'Col527 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL528 IS 'Col528 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL529 IS 'Col529 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL530 IS 'Col530 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL531 IS 'Col531 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL532 IS 'Col532 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL533 IS 'Col533 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL534 IS 'Col534 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL535 IS 'Col535 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL536 IS 'Col536 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL537 IS 'Col537 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL538 IS 'Col538 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL539 IS 'Col539 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL540 IS 'Col540 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL541 IS 'Col541 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL542 IS 'Col542 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL543 IS 'Col543 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL544 IS 'Col544 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL545 IS 'Col545 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL546 IS 'Col546 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL547 IS 'Col547 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL548 IS 'Col548 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL549 IS 'Col549 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL550 IS 'Col550 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL551 IS 'Col551 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL552 IS 'Col552 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL553 IS 'Col553 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL554 IS 'Col554 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL555 IS 'Col555 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL556 IS 'Col556 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL557 IS 'Col557 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL558 IS 'Col558 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL559 IS 'Col559 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL560 IS 'Col560 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL561 IS 'Col561 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL562 IS 'Col562 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL563 IS 'Col563 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL564 IS 'Col564 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL565 IS 'Col565 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL566 IS 'Col566 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL567 IS 'Col567 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL568 IS 'Col568 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL569 IS 'Col569 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL570 IS 'Col570 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL571 IS 'Col571 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL572 IS 'Col572 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL573 IS 'Col573 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL574 IS 'Col574 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL575 IS 'Col575 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL576 IS 'Col576 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL577 IS 'Col577 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL578 IS 'Col578 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL579 IS 'Col579 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL580 IS 'Col580 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL581 IS 'Col581 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL582 IS 'Col582 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL583 IS 'Col583 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL584 IS 'Col584 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL585 IS 'Col585 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL586 IS 'Col586 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL587 IS 'Col587 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL588 IS 'Col588 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL589 IS 'Col589 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL590 IS 'Col590 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL591 IS 'Col591 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL592 IS 'Col592 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL593 IS 'Col593 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL594 IS 'Col594 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL595 IS 'Col595 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL596 IS 'Col596 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL597 IS 'Col597 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL598 IS 'Col598 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL599 IS 'Col599 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL600 IS 'Col600 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL601 IS 'Col601 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL602 IS 'Col602 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL603 IS 'Col603 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL604 IS 'Col604 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL605 IS 'Col605 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL606 IS 'Col606 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL607 IS 'Col607 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL608 IS 'Col608 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL609 IS 'Col609 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL610 IS 'Col610 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL611 IS 'Col611 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL612 IS 'Col612 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL613 IS 'Col613 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL614 IS 'Col614 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL615 IS 'Col615 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL616 IS 'Col616 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL617 IS 'Col617 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL618 IS 'Col618 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL619 IS 'Col619 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL620 IS 'Col620 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL621 IS 'Col621 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL622 IS 'Col622 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL623 IS 'Col623 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL624 IS 'Col624 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL625 IS 'Col625 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL626 IS 'Col626 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL627 IS 'Col627 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL628 IS 'Col628 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL629 IS 'Col629 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL630 IS 'Col630 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL631 IS 'Col631 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL632 IS 'Col632 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL633 IS 'Col633 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL634 IS 'Col634 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL635 IS 'Col635 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL636 IS 'Col636 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL637 IS 'Col637 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL638 IS 'Col638 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL639 IS 'Col639 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL640 IS 'Col640 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL641 IS 'Col641 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL642 IS 'Col642 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL643 IS 'Col643 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL644 IS 'Col644 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL645 IS 'Col645 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL646 IS 'Col646 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL647 IS 'Col647 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL648 IS 'Col648 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL649 IS 'Col649 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL650 IS 'Col650 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL651 IS 'Col651 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL652 IS 'Col652 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL653 IS 'Col653 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL654 IS 'Col654 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL655 IS 'Col655 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL656 IS 'Col656 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL657 IS 'Col657 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL658 IS 'Col658 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL659 IS 'Col659 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL660 IS 'Col660 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL661 IS 'Col661 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL662 IS 'Col662 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL663 IS 'Col663 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL664 IS 'Col664 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL665 IS 'Col665 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL666 IS 'Col666 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL667 IS 'Col667 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL668 IS 'Col668 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL669 IS 'Col669 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL670 IS 'Col670 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL671 IS 'Col671 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL672 IS 'Col672 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL673 IS 'Col673 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL674 IS 'Col674 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL675 IS 'Col675 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL676 IS 'Col676 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL677 IS 'Col677 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL678 IS 'Col678 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL679 IS 'Col679 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL680 IS 'Col680 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL681 IS 'Col681 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL682 IS 'Col682 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL683 IS 'Col683 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL684 IS 'Col684 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL685 IS 'Col685 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL686 IS 'Col686 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL687 IS 'Col687 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL688 IS 'Col688 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL689 IS 'Col689 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL690 IS 'Col690 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL691 IS 'Col691 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL692 IS 'Col692 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL693 IS 'Col693 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL694 IS 'Col694 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL695 IS 'Col695 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL696 IS 'Col696 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL697 IS 'Col697 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL698 IS 'Col698 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL699 IS 'Col699 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL700 IS 'Col700 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL701 IS 'Col701 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL702 IS 'Col702 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL703 IS 'Col703 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL704 IS 'Col704 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL705 IS 'Col705 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL706 IS 'Col706 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL707 IS 'Col707 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL708 IS 'Col708 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL709 IS 'Col709 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL710 IS 'Col710 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL711 IS 'Col711 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL712 IS 'Col712 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL713 IS 'Col713 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL714 IS 'Col714 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL715 IS 'Col715 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL716 IS 'Col716 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL717 IS 'Col717 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL718 IS 'Col718 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL719 IS 'Col719 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL720 IS 'Col720 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL721 IS 'Col721 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL722 IS 'Col722 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL723 IS 'Col723 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL724 IS 'Col724 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL725 IS 'Col725 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL726 IS 'Col726 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL727 IS 'Col727 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL728 IS 'Col728 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL729 IS 'Col729 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL730 IS 'Col730 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL731 IS 'Col731 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL732 IS 'Col732 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL733 IS 'Col733 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL734 IS 'Col734 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL735 IS 'Col735 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL736 IS 'Col736 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL737 IS 'Col737 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL738 IS 'Col738 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL739 IS 'Col739 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL740 IS 'Col740 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL741 IS 'Col741 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL742 IS 'Col742 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL743 IS 'Col743 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL744 IS 'Col744 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL745 IS 'Col745 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL746 IS 'Col746 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL747 IS 'Col747 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL748 IS 'Col748 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL749 IS 'Col749 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL750 IS 'Col750 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL751 IS 'Col751 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL752 IS 'Col752 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL753 IS 'Col753 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL754 IS 'Col754 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL755 IS 'Col755 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL756 IS 'Col756 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL757 IS 'Col757 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL758 IS 'Col758 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL759 IS 'Col759 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL760 IS 'Col760 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL761 IS 'Col761 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL762 IS 'Col762 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL763 IS 'Col763 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL764 IS 'Col764 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL765 IS 'Col765 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL766 IS 'Col766 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL767 IS 'Col767 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL768 IS 'Col768 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL769 IS 'Col769 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL770 IS 'Col770 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL771 IS 'Col771 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL772 IS 'Col772 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL773 IS 'Col773 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL774 IS 'Col774 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL775 IS 'Col775 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL776 IS 'Col776 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL777 IS 'Col777 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL778 IS 'Col778 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL779 IS 'Col779 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL780 IS 'Col780 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL781 IS 'Col781 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL782 IS 'Col782 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL783 IS 'Col783 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL784 IS 'Col784 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL785 IS 'Col785 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL786 IS 'Col786 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL787 IS 'Col787 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL788 IS 'Col788 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL789 IS 'Col789 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL790 IS 'Col790 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL791 IS 'Col791 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL792 IS 'Col792 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL793 IS 'Col793 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL794 IS 'Col794 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL795 IS 'Col795 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL796 IS 'Col796 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL797 IS 'Col797 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL798 IS 'Col798 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL799 IS 'Col799 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL800 IS 'Col800 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL801 IS 'Col801 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL802 IS 'Col802 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL803 IS 'Col803 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL804 IS 'Col804 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL805 IS 'Col805 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL806 IS 'Col806 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL807 IS 'Col807 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL808 IS 'Col808 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL809 IS 'Col809 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL810 IS 'Col810 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL811 IS 'Col811 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL812 IS 'Col812 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL813 IS 'Col813 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL814 IS 'Col814 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL815 IS 'Col815 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL816 IS 'Col816 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL817 IS 'Col817 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL818 IS 'Col818 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL819 IS 'Col819 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL820 IS 'Col820 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL821 IS 'Col821 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL822 IS 'Col822 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL823 IS 'Col823 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL824 IS 'Col824 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL825 IS 'Col825 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL826 IS 'Col826 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL827 IS 'Col827 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL828 IS 'Col828 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL829 IS 'Col829 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL830 IS 'Col830 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL831 IS 'Col831 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL832 IS 'Col832 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL833 IS 'Col833 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL834 IS 'Col834 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL835 IS 'Col835 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL836 IS 'Col836 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL837 IS 'Col837 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL838 IS 'Col838 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL839 IS 'Col839 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL840 IS 'Col840 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL841 IS 'Col841 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL842 IS 'Col842 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL843 IS 'Col843 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL844 IS 'Col844 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL845 IS 'Col845 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL846 IS 'Col846 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL847 IS 'Col847 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL848 IS 'Col848 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL849 IS 'Col849 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL850 IS 'Col850 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL851 IS 'Col851 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL852 IS 'Col852 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL853 IS 'Col853 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL854 IS 'Col854 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL855 IS 'Col855 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL856 IS 'Col856 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL857 IS 'Col857 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL858 IS 'Col858 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL859 IS 'Col859 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL860 IS 'Col860 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL861 IS 'Col861 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL862 IS 'Col862 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL863 IS 'Col863 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL864 IS 'Col864 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL865 IS 'Col865 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL866 IS 'Col866 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL867 IS 'Col867 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL868 IS 'Col868 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL869 IS 'Col869 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL870 IS 'Col870 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL871 IS 'Col871 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL872 IS 'Col872 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL873 IS 'Col873 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL874 IS 'Col874 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL875 IS 'Col875 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL876 IS 'Col876 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL877 IS 'Col877 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL878 IS 'Col878 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL879 IS 'Col879 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL880 IS 'Col880 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL881 IS 'Col881 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL882 IS 'Col882 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL883 IS 'Col883 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL884 IS 'Col884 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL885 IS 'Col885 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL886 IS 'Col886 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL887 IS 'Col887 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL888 IS 'Col888 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL889 IS 'Col889 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL890 IS 'Col890 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL891 IS 'Col891 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL892 IS 'Col892 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL893 IS 'Col893 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL894 IS 'Col894 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL895 IS 'Col895 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL896 IS 'Col896 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL897 IS 'Col897 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL898 IS 'Col898 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL899 IS 'Col899 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL900 IS 'Col900 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL901 IS 'Col901 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL902 IS 'Col902 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL903 IS 'Col903 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL904 IS 'Col904 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL905 IS 'Col905 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL906 IS 'Col906 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL907 IS 'Col907 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL908 IS 'Col908 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL909 IS 'Col909 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL910 IS 'Col910 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL911 IS 'Col911 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL912 IS 'Col912 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL913 IS 'Col913 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL914 IS 'Col914 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL915 IS 'Col915 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL916 IS 'Col916 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL917 IS 'Col917 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL918 IS 'Col918 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL919 IS 'Col919 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL920 IS 'Col920 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL921 IS 'Col921 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL922 IS 'Col922 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL923 IS 'Col923 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL924 IS 'Col924 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL925 IS 'Col925 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL926 IS 'Col926 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL927 IS 'Col927 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL928 IS 'Col928 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL929 IS 'Col929 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL930 IS 'Col930 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL931 IS 'Col931 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL932 IS 'Col932 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL933 IS 'Col933 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL934 IS 'Col934 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL935 IS 'Col935 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL936 IS 'Col936 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/

COMMENT ON COLUMN VDA_V_ACCOUNTFORMRESPONSES.COL937 IS 'Col937 data of the form response.
The view VDA.VDA_V_MAPFORM_ACCOUNT provides information of the field related with this 
 column for the respective form'
/


--
-- VDA_V_STUDYCAL_RESOURCE_DETAIL  (View) 
--
--  Dependencies: 
--   F_GET_DURUNIT (Function)
--   F_GET_DURATION (Function)
--   VDA_V_STUDY_CALENDAR (View)
--   VDA_V_STUDYCAL_RESOURCE (View)
--
CREATE OR REPLACE FORCE VIEW VDA_V_STUDYCAL_RESOURCE_DETAIL
(VISIT_NAME, EVREC_EVENT, EVREC_USER, EVREC_RESOURCE, EVREC_DURATION, 
 EVREC_NOTES, CREATOR, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, 
 CALENDAR_TYPE, CALENDAR_NAME, CALENDAR_DESC, CALENDAR_STAT, CALENDAR_DURATION, 
 CALENDAR_PK, STUDY_NUMBER, DURATION_MINUTES, EVENT_PK, LAST_MODIFIED_BY_FK, 
 CREATOR_FK, FK_STUDY)
AS 
SELECT   r.visit_name,
            r.EVREC_EVENT,
            r.EVREC_USER,
            r.EVREC_RESOURCE,
            r.EVREC_DURATION,
            r.EVREC_NOTES,
            r.CREATOR,
            r.LAST_MODIFIED_BY,
            r.LAST_MODIFIED_DATE,
            r.CREATED_ON,
            c.CALENDAR_TYPE,
            c.CALENDAR_NAME,
            c.CALENDAR_DESC,
            c.CALENDAR_STAT,
            c.CALENDAR_DURATION,
            c.CALENDAR_PK,
            c.study_number,
            DECODE (
               EVREC_DURATION,
               NULL,
               0,
               (  (TO_NUMBER (SUBSTR (EVREC_DURATION, 0, 2)) * 24 * 60)
                + (TO_NUMBER (SUBSTR (EVREC_DURATION, 4, 2)) * 60)
                + TO_NUMBER (SUBSTR (EVREC_DURATION, 7, 2))
                + ROUND ( (TO_NUMBER (SUBSTR (EVREC_DURATION, 10, 2)) / 60)))
            )
               duration_minutes,
            r.EVENT_PK, r.LAST_MODIFIED_BY_FK,  r.CREATOR_FK, r.fk_study 
     FROM   VDA.VDA_V_STUDYCAL_RESOURCE r, VDA.VDA_V_STUDY_CALENDAR c
    WHERE   r.CALENDAR_PK = c.CALENDAR_PK
/

COMMENT ON TABLE VDA_V_STUDYCAL_RESOURCE_DETAIL IS 'This view provides access to the study 
caledar resource information'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_DETAIL.VISIT_NAME IS 'The name of the visit'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_DETAIL.EVREC_EVENT IS 'The name of the event'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_DETAIL.EVREC_USER IS 'The user linked as a 
resource'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_DETAIL.EVREC_RESOURCE IS 'The role type linked 
as a resource'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_DETAIL.EVREC_DURATION IS 'The duration the 
resource is needed for '
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_DETAIL.EVREC_NOTES IS 'The notes linked with the 
event resource'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_DETAIL.CREATOR IS 'The user who created the 
record (Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_DETAIL.LAST_MODIFIED_BY IS 'The user who last 
modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_DETAIL.LAST_MODIFIED_DATE IS 'The date the  
record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_DETAIL.CREATED_ON IS 'The date the record was 
created on (Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_DETAIL.CALENDAR_TYPE IS 'The Calendar Type'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_DETAIL.CALENDAR_NAME IS 'The name of the 
Calendar'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_DETAIL.CALENDAR_DESC IS 'The Calendar 
description'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_DETAIL.CALENDAR_STAT IS 'The Calendar Status'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_DETAIL.CALENDAR_DURATION IS 'The Calendar 
duration'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_DETAIL.CALENDAR_PK IS 'The Primary Key of the 
Calendar'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_DETAIL.STUDY_NUMBER IS 'The study number of the 
study the calendar is linked with'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_DETAIL.DURATION_MINUTES IS 'The resource 
duration converted in minutes'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_DETAIL.EVENT_PK IS 'The Primary Key of the 
event'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_DETAIL.LAST_MODIFIED_BY_FK IS 'The Key to 
identify the Last Modified By of the record.'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_DETAIL.CREATOR_FK IS 'The Key to identify the 
Creator of the record.'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_DETAIL.FK_STUDY IS 'The Key to identify the 
study linked with the Calendar Resource'
/


--
-- VDA_V_STUDYCAL_RESOURCE_BYPAT  (View) 
--
--  Dependencies: 
--   F_GET_DURUNIT (Function)
--   F_GET_DURATION (Function)
--   VDA_V_STUDY_CALENDAR (View)
--   VDA_V_STUDYCAL_RESOURCE (View)
--   VDA_V_PAT_SCHEDULE (View)
--
CREATE OR REPLACE FORCE VIEW VDA_V_STUDYCAL_RESOURCE_BYPAT
(VISIT_NAME, EVREC_EVENT, EVREC_USER, EVREC_RESOURCE, EVREC_DURATION, 
 EVREC_NOTES, CREATOR, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, 
 CALENDAR_TYPE, CALENDAR_NAME, CALENDAR_DESC, CALENDAR_STAT, CALENDAR_DURATION, 
 CALENDAR_PK, STUDY_NUMBER, DURATION_MINUTES, PAT_STUDY_ID, PTSCH_ACTSCH_DATE, 
 PTSCH_EVENT_STAT, ENROLLING_SITE_NAME, EVENT_PK, LAST_MODIFIED_BY_FK, CREATOR_FK, 
 FK_STUDY)
AS 
SELECT   r.visit_name,
            r.EVREC_EVENT,
            r.EVREC_USER,
            r.EVREC_RESOURCE,
            r.EVREC_DURATION,
            r.EVREC_NOTES,
            r.CREATOR,
            r.LAST_MODIFIED_BY,
            r.LAST_MODIFIED_DATE,
            r.CREATED_ON,
            c.CALENDAR_TYPE,
            c.CALENDAR_NAME,
            c.CALENDAR_DESC,
            c.CALENDAR_STAT,
            c.CALENDAR_DURATION,
            c.CALENDAR_PK,
            c.study_number,
            DECODE (
               EVREC_DURATION,
               NULL,
               0,
               (  (TO_NUMBER (SUBSTR (EVREC_DURATION, 0, 2)) * 24 * 60)
                + (TO_NUMBER (SUBSTR (EVREC_DURATION, 4, 2)) * 60)
                + TO_NUMBER (SUBSTR (EVREC_DURATION, 7, 2))
                + ROUND ( (TO_NUMBER (SUBSTR (EVREC_DURATION, 10, 2)) / 60)))
            )
               duration_minutes,
            s.PAT_STUDY_ID,
            s.PTSCH_ACTSCH_DATE,
            s.PTSCH_EVENT_STAT,
            s.enrolling_site_name,
            r.EVENT_PK, r.LAST_MODIFIED_BY_FK,  r.CREATOR_FK, r.fk_study 
     FROM   VDA.VDA_V_STUDYCAL_RESOURCE r,
            VDA.VDA_V_STUDY_CALENDAR c,
            VDA.VDA_V_PAT_SCHEDULE s
    WHERE   r.CALENDAR_PK = c.CALENDAR_PK AND s.PTSCH_EVENT_PK = r.EVENT_PK
/

COMMENT ON TABLE VDA_V_STUDYCAL_RESOURCE_BYPAT IS 'This view provides access to the study 
caledar resource information for each patient schedule event'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.VISIT_NAME IS 'The name of the visit'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.EVREC_EVENT IS 'The name of the event'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.EVREC_USER IS 'The user linked as a 
resource'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.EVREC_RESOURCE IS 'The role type linked as 
a resource'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.EVREC_DURATION IS 'The duration the 
resource is needed for '
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.EVREC_NOTES IS 'The notes linked with the 
event resource'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.CREATOR IS 'The user who created the 
record (Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.LAST_MODIFIED_BY IS 'The user who last 
modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.LAST_MODIFIED_DATE IS 'The date the  
record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.CREATED_ON IS 'The date the record was 
created on (Audit)'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.CALENDAR_TYPE IS 'The Calendar Type'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.CALENDAR_NAME IS 'The name of the 
Calendar'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.CALENDAR_DESC IS 'The Calendar 
description'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.CALENDAR_STAT IS 'The Calendar Status'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.CALENDAR_DURATION IS 'The Calendar 
duration'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.CALENDAR_PK IS 'The Primary Key of the 
Calendar'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.STUDY_NUMBER IS 'The study number of the 
study the calendar is linked with'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.DURATION_MINUTES IS 'The resource duration 
converted in minutes'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.PAT_STUDY_ID IS 'The patient study ID'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.PTSCH_ACTSCH_DATE IS 'The actual schedule 
date of the event'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.PTSCH_EVENT_STAT IS 'The patient schedule 
event status'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.ENROLLING_SITE_NAME IS 'The enrolling site 
name'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.EVENT_PK IS 'The Primary Key of the event'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.LAST_MODIFIED_BY_FK IS 'The Key to 
identify the Last Modified By of the record.'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.CREATOR_FK IS 'The Key to identify the 
Creator of the record.'
/

COMMENT ON COLUMN VDA_V_STUDYCAL_RESOURCE_BYPAT.FK_STUDY IS 'The Key to identify the study 
linked with the Calendar Resource'
/


--
-- VDA_V_PAT_STATUS_RECENT  (View) 
--
--  Dependencies: 
--   F_GET_CODELSTDESC (Function)
--   VDA_V_ALL_PATSTUDYSTAT (View)
--
CREATE OR REPLACE FORCE VIEW VDA_V_PAT_STATUS_RECENT
(PSTAT_STATUS, PSTAT_REASON, PSTAT_STAT_DATE, PSTAT_NOTES, PSTAT_PAT_STUD_ID, 
 PSTAT_ASSIGNED_TO, PSTAT_PHYSICIAN, PSTAT_TREAT_LOCAT, PSTAT_EVAL_FLAG, PSTAT_EVAL_STAT, 
 PSTAT_INEVAL_STAT, PSTAT_SURVIVAL_STAT, PSTAT_RANDOM_NUM, PSTAT_ENROLLED_BY, PSTAT_ENROLL_SITE, 
 PSTAT_NEXT_FU_DATE, PSTAT_IC_VERS_NUM, PSTAT_SCREEN_NUM, PSTAT_SCREEN_BY, PSTAT_SCREEN_OUTCOME, 
 CREATOR, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, FK_PER, 
 FK_STUDY, PSTAT_CURRENT_STAT, PK_PATSTUDYSTAT, PSTAT_STATUS_FK, CREATOR_FK, 
 LAST_MODIFIED_BY_FK, PSTAT_SCREEN_BY_FK, ENROLLED_BY_FK, PSTAT_PHYSICIAN_FK, PSTAT_ASSIGNED_TO_FK, 
 PSTAT_REASON_FK, FK_SITE_ENROLLING)
AS 
SELECT   PSTAT_STATUS,
            PSTAT_REASON,
            PSTAT_STAT_DATE,
            PSTAT_NOTES,
            PSTAT_PAT_STUD_ID,
            PSTAT_ASSIGNED_TO,
            PSTAT_PHYSICIAN,
            PSTAT_TREAT_LOCAT,
            PSTAT_EVAL_FLAG,
            PSTAT_EVAL_STAT,
            PSTAT_INEVAL_STAT,
            PSTAT_SURVIVAL_STAT,
            PSTAT_RANDOM_NUM,
            PSTAT_ENROLLED_BY,
            PSTAT_ENROLL_SITE,
            PSTAT_NEXT_FU_DATE,
            PSTAT_IC_VERS_NUM,
            PSTAT_SCREEN_NUM,
            PSTAT_SCREEN_BY,
            PSTAT_SCREEN_OUTCOME,
            CREATOR,
            LAST_MODIFIED_BY,
            LAST_MODIFIED_DATE,
            CREATED_ON,
            FK_PER,
            FK_STUDY,
            PSTAT_CURRENT_STAT,
            PK_PATSTUDYSTAT, PSTAT_STATUS_FK,  CREATOR_FK,  LAST_MODIFIED_BY_FK ,
             PSTAT_SCREEN_BY_FK ,  ENROLLED_BY_FK ,  PSTAT_PHYSICIAN_FK,
 PSTAT_ASSIGNED_TO_FK,   PSTAT_REASON_FK, FK_SITE_ENROLLING 
     FROM   VDA.VDA_V_ALL_PATSTUDYSTAT
    WHERE   PATSTUDYSTAT_ENDT IS NULL
/

COMMENT ON TABLE VDA_V_PAT_STATUS_RECENT IS 'This view provides access to the patient 
recent status information'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_ASSIGNED_TO_FK IS 'The Key to identify the 
Patient Assigned To User.'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_REASON_FK IS 'The Key to identify the 
Patient Study Status Reason Change Key.'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.FK_SITE_ENROLLING IS 'The Key to identify the 
Patient Enrolling Organization Key'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_STATUS IS 'The Patient Status Description'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_REASON IS 'The reason for the current 
status of the patient'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_STAT_DATE IS 'The Patient Status Date'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_NOTES IS 'The notes linked with the 
patient'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_PAT_STUD_ID IS 'The patient study id'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_ASSIGNED_TO IS 'The user patient is 
assigned to'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_PHYSICIAN IS 'The physician patient is 
assigned to'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_TREAT_LOCAT IS 'The treatment location'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_EVAL_FLAG IS 'The Evaluable Flag'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_EVAL_STAT IS 'The Evaluable Status'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_INEVAL_STAT IS 'The Unevaluable Status'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_SURVIVAL_STAT IS 'The Patient survival 
status'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_RANDOM_NUM IS 'The randomization number'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_ENROLLED_BY IS 'The user who enrolled the 
patient'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_ENROLL_SITE IS 'The patient enrolling 
site'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_NEXT_FU_DATE IS 'The next follow up date'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_IC_VERS_NUM IS 'The informed consent 
version number'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_SCREEN_NUM IS 'The screening number'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_SCREEN_BY IS 'The The user who screended 
the patient'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_SCREEN_OUTCOME IS 'The screening outcome'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.CREATOR IS 'The user who created the record 
(Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.LAST_MODIFIED_BY IS 'The user who last modified 
the record(Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.LAST_MODIFIED_DATE IS 'The date the  record was 
last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.CREATED_ON IS 'The date the record was created 
on (Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.FK_PER IS 'The Primary Key of the patient the 
schedule is linked with'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.FK_STUDY IS 'The Primary Key of the study 
schedule is linked with'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_CURRENT_STAT IS 'The patient status 
current status flag'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PK_PATSTUDYSTAT IS 'The Primary Key of patient 
study status record'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_STATUS_FK IS 'The Key to identify the 
Patient Study Status Code.'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.CREATOR_FK IS 'The Key to identify the Creator 
of the record.'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.LAST_MODIFIED_BY_FK IS 'The Key to identify the 
Last Modified By of the record.'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_SCREEN_BY_FK IS 'The Key to identify the 
Patient Screened By User.'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.ENROLLED_BY_FK IS 'The Key to identify the 
Patient Enrolled By User.'
/

COMMENT ON COLUMN VDA_V_PAT_STATUS_RECENT.PSTAT_PHYSICIAN_FK IS 'The Key to identify the 
Patient Physician User.'
/


--
-- VDA_V_PAT_ACCRUAL_YEAR  (View) 
--
--  Dependencies: 
--   F_GET_CODELSTDESC (Function)
--   VDA_V_PAT_ACCRUAL (View)
--
CREATE OR REPLACE FORCE VIEW VDA_V_PAT_ACCRUAL_YEAR
(FK_STUDY, FK_SITE_ENROLLING, PSTAT_ENROLL_SITE, PATPROT_ENROLL_YEAR, ACCRUAL_COUNT)
AS 
SELECT   fk_study,
              FK_SITE_ENROLLING,
              PSTAT_ENROLL_SITE,
              EXTRACT (YEAR FROM patprot_enroldt) patprot_enroll_year,
              COUNT ( * ) accrual_count
       FROM   VDA.VDA_V_PAT_ACCRUAL
   GROUP BY   fk_study,
              FK_SITE_ENROLLING,
              PSTAT_ENROLL_SITE,
              EXTRACT (YEAR FROM patprot_enroldt)
/

COMMENT ON TABLE VDA_V_PAT_ACCRUAL_YEAR IS 'This view provides access to the yearly accrual or enrollment counts. Accrual is considered as the enrollment status and enrollment date entered'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_YEAR.FK_STUDY IS 'The Primary Key of the study the accrual is linked with'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_YEAR.FK_SITE_ENROLLING IS 'The Primary Key of the enrolling site'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_YEAR.PSTAT_ENROLL_SITE IS 'The Patient Enrolling Site'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_YEAR.PATPROT_ENROLL_YEAR IS 'The enrollment year'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_YEAR.ACCRUAL_COUNT IS 'The enrollment or accrual count'
/


--
-- VDA_V_PAT_ACCRUAL_ALL  (View) 
--
--  Dependencies: 
--   ER_STUDYSITES (Table)
--   F_GET_CODELSTDESC (Function)
--   PKG_UTIL (Package)
--   CODE_LST_NAMES (Function)
--   VDA_V_PAT_DEMO_NOPHI (View)
--   VDA_V_PAT_ACCRUAL (View)
--
CREATE OR REPLACE FORCE VIEW VDA_V_PAT_ACCRUAL_ALL
(PSTAT_PAT_STUD_ID, PSTAT_ASSIGNED_TO, PSTAT_PHYSICIAN, PSTAT_TREAT_LOCAT, PSTAT_EVAL_FLAG, 
 PSTAT_EVAL_STAT, PSTAT_INEVAL_STAT, PSTAT_SURVIVAL_STAT, PSTAT_RANDOM_NUM, PSTAT_ENROLLED_BY, 
 PSTAT_ENROLL_SITE, CREATOR, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, 
 FK_PER, FK_STUDY, PATPROT_ENROLDT, PATPROT_TREATINGORG, PATPROT_ENROLL_YEAR, 
 PTDEM_SURVIVSTAT, PTDEM_GENDER, PTDEM_MARSTAT, PTDEM_BLOODGRP, PTDEM_PRI_ETHNY, 
 PTDEM_PRI_RACE, PTDEM_ADD_ETHNY, PTDEM_ADD_RACE, PTDEM_STATE, PTDEM_COUNTRY, 
 PTDEM_EMPLOY, PTDEM_EDUCATION, PTDEM_REGDATE_YR, STUDYSITE_LSAMPLESIZE, DMGRPH_REPORTABLE, 
 PATPROT_DISEASE_CODE, PATPROT_MORE_DIS_CODE1, PATPROT_MORE_DIS_CODE2, PATPROT_OTHR_DIS_CODE, CREATOR_FK, 
 LAST_MODIFIED_BY_FK, PSTAT_ENROLLED_BY_FK, FK_SITE_ENROLLING)
AS 
SELECT   PSTAT_PAT_STUD_ID,
            PSTAT_ASSIGNED_TO,
            PSTAT_PHYSICIAN,
            PSTAT_TREAT_LOCAT,
            PSTAT_EVAL_FLAG,
            PSTAT_EVAL_STAT,
            PSTAT_INEVAL_STAT,
            PSTAT_SURVIVAL_STAT,
            PSTAT_RANDOM_NUM,
            PSTAT_ENROLLED_BY,
            PSTAT_ENROLL_SITE,
            a.CREATOR,
            a.LAST_MODIFIED_BY,
            a.LAST_MODIFIED_DATE,
            a.CREATED_ON,
            a.FK_PER,
            a.FK_STUDY,
            patprot_enroldt,
            patprot_treatingorg,
            EXTRACT (YEAR FROM patprot_enroldt) patprot_enroll_year,
            d.PTDEM_SURVIVSTAT,
            d.PTDEM_GENDER,
            d.PTDEM_MARSTAT,
            d.PTDEM_BLOODGRP,
            d.PTDEM_PRI_ETHNY,
            d.PTDEM_PRI_RACE,
            d.PTDEM_ADD_ETHNY,
            d.PTDEM_ADD_RACE,
            d.PTDEM_STATE,
            d.PTDEM_COUNTRY,
            d.PTDEM_EMPLOY,
            d.PTDEM_EDUCATION,
            d.PTDEM_REGDATE_YR,
            studysite_lsamplesize,
            a.DMGRPH_REPORTABLE,
            a.PATPROT_DISEASE_CODE,
            a.PATPROT_MORE_DIS_CODE1,
            a.PATPROT_MORE_DIS_CODE2,
            a.PATPROT_OTHR_DIS_CODE, a.CREATOR_FK ,  
a.LAST_MODIFIED_BY_FK,a.PSTAT_ENROLLED_BY_FK, a.FK_SITE_ENROLLING
     FROM   VDA.VDA_V_PAT_ACCRUAL a,
            VDA.VDA_V_PAT_DEMO_NOPHI d,
            eres.er_studySites s
    WHERE       a.fk_per = d.pk_person
            AND s.fk_study = a.fk_study
            AND s.fk_site = a.FK_SITE_ENROLLING
/

COMMENT ON TABLE VDA_V_PAT_ACCRUAL_ALL IS 'This view provides access to the patient 
accrual information with patient details'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.FK_SITE_ENROLLING IS 'The foreign key to the 
enrolling site'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PTDEM_STATE IS 'The patient state'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PTDEM_COUNTRY IS 'The patient country'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PTDEM_EMPLOY IS 'The patient employment'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PTDEM_EDUCATION IS 'The patient education'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PTDEM_REGDATE_YR IS 'The patient registration year 
with the default site or organization'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.STUDYSITE_LSAMPLESIZE IS 'The enrolling site''s 
local sample size'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.CREATOR_FK IS 'The Key to identify the Creator of 
the record.'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.LAST_MODIFIED_BY_FK IS 'The Key to identify the 
Last Modified By of the record.'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PSTAT_ENROLLED_BY_FK IS 'The Key to identify the 
Enrolled By User'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PSTAT_PAT_STUD_ID IS 'The patiient study id'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PSTAT_ASSIGNED_TO IS 'The user patient is assigned 
to'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PSTAT_PHYSICIAN IS 'The physician patient is 
assigned to'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PSTAT_TREAT_LOCAT IS 'The treatment location'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PSTAT_EVAL_FLAG IS 'The Evaluable Flag'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PSTAT_EVAL_STAT IS 'The Evaluable Status'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PSTAT_INEVAL_STAT IS 'The Unevaluable Status'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PSTAT_SURVIVAL_STAT IS 'The Patient survival 
status'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PSTAT_RANDOM_NUM IS 'The randomization number'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PSTAT_ENROLLED_BY IS 'The user who enrolled the 
patient'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PSTAT_ENROLL_SITE IS 'The patient enrolling site'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.CREATOR IS 'The user who created the record 
(Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.LAST_MODIFIED_BY IS 'The user who last modified 
the record(Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.LAST_MODIFIED_DATE IS 'The date the  record was 
last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.CREATED_ON IS 'The date the record was created on 
(Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.FK_PER IS 'The Primary Key of the patient the 
schedule is linked with'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.FK_STUDY IS 'The Primary Key of the study schedule 
is linked with'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PATPROT_ENROLDT IS 'The enrollment date'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PATPROT_TREATINGORG IS 'The treating organization 
name'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PATPROT_ENROLL_YEAR IS 'The enrollment year'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PTDEM_SURVIVSTAT IS 'The survival status'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PTDEM_GENDER IS 'The patient gender'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PTDEM_MARSTAT IS 'The patient marital status'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PTDEM_BLOODGRP IS 'The patient blood group'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PTDEM_PRI_ETHNY IS 'The patient primary ethnicity'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PTDEM_PRI_RACE IS 'The patient primary race'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PTDEM_ADD_ETHNY IS 'The patient additional 
ethnicity'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PTDEM_ADD_RACE IS 'The patient additional race'
/


--
-- VDA_V_AE_ALL_BYSITE  (View) 
--
--  Dependencies: 
--   ER_SITE (Table)
--   F_GET_YESNO (Function)
--   F_GET_CODELSTDESC (Function)
--   F_GETDIS_SITE (Function)
--   VDA_V_AE_ALL (View)
--
CREATE OR REPLACE FORCE VIEW VDA_V_AE_ALL_BYSITE
(FK_SITE_ENROLLING, FK_STUDY, STUDY_NUMBER, STUDY_TITLE, STUDY_PI, 
 STUDY_TAREA, STUDY_DISEASE_SITE, AE_TYPE, AE_DESC, AE_STDATE, 
 AE_ENDDATE, AE_GRADE, AE_NAME, MEDDRA, DICTIONARY, 
 SITE_NAME, STUDY_PK, PATIENT_PK, STUDY_TAREA_FK, STUDY_DISEASE_SITE_FK, 
 STUDY_PI_FK)
AS 
SELECT   fk_site_enrolling,
            fk_study,
            study_number,
            study_title,
            study_pi,
            study_tarea,
            study_disease_site,
            ae_type,
            ae_desc,
            ae_stdate,
            ae_enddate,
            ae_grade,
            ae_name,
            meddra,
            dictionary,
            site_name,
            fk_study study_pk,
            fk_per patient_pk, STUDY_TAREA_FK,STUDY_DISEASE_SITE_FK,STUDY_PI_FK
     FROM   VDA.VDA_V_ae_all, eres.er_site
    WHERE   fk_site_enrolling = pk_site
/

COMMENT ON TABLE VDA_V_AE_ALL_BYSITE IS 'This view provides access to the Adverse Event 
data by Patient enroling site'
/

COMMENT ON COLUMN VDA_V_AE_ALL_BYSITE.STUDY_PI_FK IS 'The Key to identify the Study PI 
User.'
/

COMMENT ON COLUMN VDA_V_AE_ALL_BYSITE.SITE_NAME IS 'The Patient''s enrolling site name'
/

COMMENT ON COLUMN VDA_V_AE_ALL_BYSITE.STUDY_PK IS 'The Primary Key of study the AE is 
linked with'
/

COMMENT ON COLUMN VDA_V_AE_ALL_BYSITE.PATIENT_PK IS 'The Primary Key of the patient the AE 
is linked with'
/

COMMENT ON COLUMN VDA_V_AE_ALL_BYSITE.STUDY_TAREA_FK IS 'The Key to identify the Study 
Therapeutic Area Code'
/

COMMENT ON COLUMN VDA_V_AE_ALL_BYSITE.STUDY_DISEASE_SITE_FK IS 'The Key to identify the 
Study Disease Sites Code'
/

COMMENT ON COLUMN VDA_V_AE_ALL_BYSITE.FK_SITE_ENROLLING IS 'The Primary Key of the 
Patient''s enrolling site'
/

COMMENT ON COLUMN VDA_V_AE_ALL_BYSITE.FK_STUDY IS 'The Primary Key of study the AE is 
linked with'
/

COMMENT ON COLUMN VDA_V_AE_ALL_BYSITE.STUDY_NUMBER IS 'The Study Numberof the study the AE 
is linked with'
/

COMMENT ON COLUMN VDA_V_AE_ALL_BYSITE.STUDY_TITLE IS 'The Study Title'
/

COMMENT ON COLUMN VDA_V_AE_ALL_BYSITE.STUDY_PI IS 'The Study PI'
/

COMMENT ON COLUMN VDA_V_AE_ALL_BYSITE.STUDY_TAREA IS 'The Study Therapeutic Area'
/

COMMENT ON COLUMN VDA_V_AE_ALL_BYSITE.STUDY_DISEASE_SITE IS 'The Study Disease Site'
/

COMMENT ON COLUMN VDA_V_AE_ALL_BYSITE.AE_TYPE IS 'The Adverse Event Type'
/

COMMENT ON COLUMN VDA_V_AE_ALL_BYSITE.AE_DESC IS 'The Adverse Event Description'
/

COMMENT ON COLUMN VDA_V_AE_ALL_BYSITE.AE_STDATE IS 'The Adverse Event Start Date'
/

COMMENT ON COLUMN VDA_V_AE_ALL_BYSITE.AE_ENDDATE IS 'The Adverse Event End Date'
/

COMMENT ON COLUMN VDA_V_AE_ALL_BYSITE.AE_GRADE IS 'The Adverse Event Grade'
/

COMMENT ON COLUMN VDA_V_AE_ALL_BYSITE.AE_NAME IS 'The Adverse Event Name'
/

COMMENT ON COLUMN VDA_V_AE_ALL_BYSITE.MEDDRA IS 'The Adverse Event MEDDRA code'
/

COMMENT ON COLUMN VDA_V_AE_ALL_BYSITE.DICTIONARY IS 'The Adverse Event Dictionaries'
/


 
INSERT INTO VDA_TRACK_PATCHES
(
  PATCH_PK       ,
  DB_VER_MJR     ,
  DB_VER_MNR     ,
  DB_PATCH_NAME  ,
  FIRED_ON       ,
  APP_VERSION    
) VALUES(SEQ_VDA_TRACK_PATCHES.nextval,5,9,'01_alter_views.sq;',sysdate,'VDA 1.4 Build016')
/

commit
/

