grant select on ER_USERREPORTLOG to VDA
/
 
INSERT INTO TRACK_PATCHES
(
  PATCH_PK       ,
  DB_VER_MJR     ,
  DB_VER_MNR     ,
  DB_PATCH_NAME  ,
  FIRED_ON       ,
  APP_VERSION    
) VALUES(SEQ_TRACK_PATCHES.nextval,5,9,'01_alter.sql',sysdate,'VDA 1.4 Build016')
/

commit
/