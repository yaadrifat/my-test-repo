/* Formatted on 8/16/2012 11:08:19 AM (QP5 v5.115.810.9015) */
--
-- VDA.VDA_V_STUDY_SUMMARY  (View)
--
--  Dependencies:
--   F_GETDIS_SITE (Function)
--   F_GET_CODELSTDESC (Function)
--   F_GET_YESNO (Function)
--   ER_STUDY (Table)
--   ER_USER (Table)
--

CREATE OR REPLACE  VIEW VDA.VDA_V_STUDY_SUMMARY
(
   PK_STUDY,
   STUDY_NUMBER,
   STUDY_ENTERED_BY,
   STUDY_PI,
   STUDY_COORDINATOR,
   STUDY_MAJ_AUTH,
   STUDY_TITLE,
   STUDY_OBJECTIVE,
   STUDY_SUMMARY,
   STUDY_AGENT_DEVICE,
   STUDY_DIVISION,
   STUDY_TAREA,
   STUDY_DISEASE_SITE,
   STUDY_ICDCODE1,
   STUDY_ICDCODE2,
   STUDY_NATSAMPSIZE,
   STUDY_DURATION,
   STUDY_DURATN_UNIT,
   STUDY_EST_BEGIN_DATE,
   STUDY_PHASE,
   STUDY_RESEARCH_TYPE,
   STUDY_TYPE,
   STUDY_LINKED_TO,
   STUDY_BLINDING,
   STUDY_RANDOM,
   STUDY_SPONSOR,
   SPONSOR_CONTACT,
   STUDY_INFO,
   STUDY_KEYWRDS,
   FK_ACCOUNT,
   CREATED_ON,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATOR,
   SPONSOR_DD,
   STUDY_SPONSORID,
   STUDY_ACTUALDT, STUDY_INVIND_FLAG
)
AS
   SELECT   pk_study,
            ER_STUDY.STUDY_NUMBER,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = ER_STUDY.FK_AUTHOR)
               STUDY_ENTERED_BY,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = ER_STUDY.STUDY_PRINV)
               STUDY_PI,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   ER_USER.PK_USER = ER_STUDY.STUDY_COORDINATOR)
               STUDY_COORDINATOR,
            ERES.F_GEt_Yesno (ER_STUDY.STUDY_MAJ_AUTH) AS STUDY_MAJ_AUTH,
             STUDY_TITLE,
            STUDY_OBJ_CLOB STUDY_OBJECTIVE,
            STUDY_SUM_CLOB STUDY_SUMMARY,
            STUDY_PRODNAME STUDY_AGENT_DEVICE,
            ERES.F_GEt_Codelstdesc (ER_STUDY.STUDY_DIVISION) STUDY_DIVISION,
            ERES.F_GEt_Codelstdesc (ER_STUDY.FK_CODELST_TAREA) STUDY_TAREA,
            ERES.F_GEtdis_Site (ER_STUDY.STUDY_DISEASE_SITE)
               STUDY_DISEASE_SITE,
            STUDY_ICDCODE1,
            STUDY_ICDCODE2,
            STUDY_NSAMPLSIZE STUDY_NATSAMPSIZE,
            STUDY_DUR STUDY_DURATION,
            STUDY_DURUNIT STUDY_DURATN_UNIT,
            STUDY_ESTBEGINDT STUDY_EST_BEGIN_DATE,
            ERES.F_GEt_Codelstdesc (ER_STUDY.FK_CODELST_PHASE) STUDY_PHASE,
            ERES.F_GEt_Codelstdesc (ER_STUDY.FK_CODELST_RESTYPE)
               STUDY_RESEARCH_TYPE,
            ERES.F_GEt_Codelstdesc (ER_STUDY.FK_CODELST_TYPE) STUDY_TYPE,
            STUDY_ASSOC STUDY_LINKED_TO,
            ERES.F_GEt_Codelstdesc (ER_STUDY.FK_CODELST_BLIND) STUDY_BLINDING,
            ERES.F_GEt_Codelstdesc (ER_STUDY.FK_CODELST_RANDOM) STUDY_RANDOM,
            STUDY_SPONSOR,
            STUDY_CONTACT SPONSOR_CONTACT,
            STUDY_INFO,
            STUDY_KEYWRDS,
            FK_ACCOUNT,
            CREATED_ON,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = ER_STUDY.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            ER_STUDY.LAST_MODIFIED_DATE,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = ER_STUDY.CREATOR)
               CREATIOR,
            ERES.F_GEt_Codelstdesc (FK_CODELST_SPONSOR) SPONSOR_DD,
            STUDY_SPONSORID,
            STUDY_ACTUALDT,
			ERES.F_GEt_Yesno (ER_STUDY.STUDY_INVIND_FLAG) STUDY_INVIND_FLAG
     FROM   eres.ER_STUDY
/
COMMENT ON TABLE VDA.VDA_V_STUDY_SUMMARY IS 'This view provides access to the study summary record'
/

COMMENT ON COLUMN VDA.VDA_V_STUDY_SUMMARY.STUDY_SPONSORID IS 'The Sponsor Id for Sponsor Other Information'
/

COMMENT ON COLUMN VDA.VDA_V_STUDY_SUMMARY.STUDY_ACTUALDT IS 'Study Actual Begin date'
/

COMMENT ON COLUMN VDA.VDA_V_STUDY_SUMMARY.PK_STUDY IS 'The Primary Key of the study'
/

COMMENT ON COLUMN VDA.VDA_V_STUDY_SUMMARY.STUDY_NUMBER IS 'The Study Number of the study the payment  is linked with'
/

COMMENT ON COLUMN VDA.VDA_V_STUDY_SUMMARY.STUDY_ENTERED_BY IS 'The user who entered the study in the application'
/

COMMENT ON COLUMN VDA.VDA_V_STUDY_SUMMARY.STUDY_PI IS 'The Principal Investigator of the study The study coordinator (as documented in the study summary)'
/

COMMENT ON COLUMN VDA.VDA_V_STUDY_SUMMARY.STUDY_COORDINATOR IS 'The study coordinator (as documented in the study summary)'
/

COMMENT ON COLUMN VDA.VDA_V_STUDY_SUMMARY.STUDY_MAJ_AUTH IS ' Is the selected Principal investigator is the major author of the study'
/

COMMENT ON COLUMN VDA.VDA_V_STUDY_SUMMARY.STUDY_OBJECTIVE IS 'The Study Objective'
/

COMMENT ON COLUMN VDA.VDA_V_STUDY_SUMMARY.STUDY_TITLE IS 'The Study title'
/

COMMENT ON COLUMN VDA.VDA_V_STUDY_SUMMARY.STUDY_SUMMARY IS 'The Study Summary'
/

COMMENT ON COLUMN VDA.VDA_V_STUDY_SUMMARY.STUDY_AGENT_DEVICE IS 'Agent or Device'
/

COMMENT ON COLUMN VDA.VDA_V_STUDY_SUMMARY.STUDY_DIVISION IS 'Study division'
/

COMMENT ON COLUMN VDA.VDA_V_STUDY_SUMMARY.STUDY_TAREA IS 'Study Therapeutic Area'
/

COMMENT ON COLUMN VDA.VDA_V_STUDY_SUMMARY.STUDY_DISEASE_SITE IS 'Study Disease Site'
/

COMMENT ON COLUMN VDA.VDA_V_STUDY_SUMMARY.STUDY_ICDCODE1 IS 'ICD code 1 linked with the study'
/

COMMENT ON COLUMN VDA.VDA_V_STUDY_SUMMARY.STUDY_ICDCODE2 IS 'ICD code 2 linked with the study'
/

COMMENT ON COLUMN VDA.VDA_V_STUDY_SUMMARY.STUDY_NATSAMPSIZE IS 'Study National Sample Size'
/

COMMENT ON COLUMN VDA.VDA_V_STUDY_SUMMARY.STUDY_DURATION IS 'Estimated duration of the study'
/

COMMENT ON COLUMN VDA.VDA_V_STUDY_SUMMARY.STUDY_DURATN_UNIT IS 'Estimated duration unit of the study'
/

COMMENT ON COLUMN VDA.VDA_V_STUDY_SUMMARY.STUDY_EST_BEGIN_DATE IS 'Estimated begin date of the study'
/

COMMENT ON COLUMN VDA.VDA_V_STUDY_SUMMARY.STUDY_PHASE IS 'Study Phase'
/

COMMENT ON COLUMN VDA.VDA_V_STUDY_SUMMARY.STUDY_RESEARCH_TYPE IS 'Study Research Type'
/

COMMENT ON COLUMN VDA.VDA_V_STUDY_SUMMARY.STUDY_TYPE IS 'Study Type'
/

COMMENT ON COLUMN VDA.VDA_V_STUDY_SUMMARY.STUDY_LINKED_TO IS 'Study linked with another study'
/

COMMENT ON COLUMN VDA.VDA_V_STUDY_SUMMARY.STUDY_BLINDING IS 'Study Blinding'
/

COMMENT ON COLUMN VDA.VDA_V_STUDY_SUMMARY.STUDY_RANDOM IS 'Study Randomization'
/

COMMENT ON COLUMN VDA.VDA_V_STUDY_SUMMARY.STUDY_SPONSOR IS 'Study Sponsor Name'
/

COMMENT ON COLUMN VDA.VDA_V_STUDY_SUMMARY.SPONSOR_CONTACT IS 'Study Sponsor Contact'
/

COMMENT ON COLUMN VDA.VDA_V_STUDY_SUMMARY.STUDY_INFO IS 'Study sponsor information'
/

COMMENT ON COLUMN VDA.VDA_V_STUDY_SUMMARY.STUDY_KEYWRDS IS 'Study keywords'
/

COMMENT ON COLUMN VDA.VDA_V_STUDY_SUMMARY.FK_ACCOUNT IS 'The account study is linked with'
/

COMMENT ON COLUMN VDA.VDA_V_STUDY_SUMMARY.CREATED_ON IS 'The date the record was created on (Audit)'
/

COMMENT ON COLUMN VDA.VDA_V_STUDY_SUMMARY.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA.VDA_V_STUDY_SUMMARY.LAST_MODIFIED_DATE IS 'The date the  record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA.VDA_V_STUDY_SUMMARY.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA.VDA_V_STUDY_SUMMARY.SPONSOR_DD IS 'Study Sponsor'
/

COMMENT ON COLUMN VDA.VDA_V_STUDY_SUMMARY.STUDY_INVIND_FLAG IS 'Study INV IND Flag'
/

alter view VDA.VDA_V_AE_ALL compile
/

alter view VDA.VDA_V_AE_ALL_BYSITE compile
/

/* Formatted on 8/22/2012 10:47:40 PM (QP5 v5.115.810.9015) */
--
-- VDA.VDA_V_BUDGET_SECTION  (View)
--
--  Dependencies:
--   F_GET_FSTAT (Function)
--   F_GET_YESNO (Function)
--   ER_SITE (Table)
--   ER_STUDY (Table)
--   ER_USER (Table)
--   SCH_BGTCAL (Table)
--   SCH_BGTSECTION (Table)
--   SCH_BUDGET (Table)
--   SCH_CODELST (Table)
--

CREATE OR REPLACE FORCE VIEW VDA.VDA_V_BUDGET_SECTION
(
   BGTDT_NAME,
   BGTDT_VERSION,
   BGTDT_TEMPLATE,
   BGTDT_STUDY_NUM,
   BGTDT_STUDY_TITLE,
   BGTDT_ORGANIZATION,
   BGTDT_STATUS,
   BGTDT_DESCRIPTION,
   BGTDT_CREATED_BY,
   BGTDT_CURRENCY,
   FK_ACCOUNT,
   PK_BUDGET,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATED_ON,
   FK_STUDY,
   BGTCAL_SPONSOROHEAD,
   SPONSOR_OHEAD_FLAG,
   BGTCAL_INDIRECTCOST,
   BGTCAL_FRGBENEFIT,
   FRINGEBFT_FLAG,
   BGTCAL_DISCOUNT,
   DISCNT_FLAG,
   EXCLUD_SOC,
   BUDGET_COMBFLAG,
   PK_BUDGETSEC,
   BGTSECTION_NAME,
   BGTSECTION_SEQUENCE,
   BGTSECTION_TYPE,
   BGTSECTION_PATNO,
   BGTSECTION_NOTES,
   BGTSECTION_PERSONLFLAG,
   BUDGET_VISIT_PK
)
AS
   SELECT   
            BUDGET_NAME BGTDT_NAME,
            BUDGET_VERSION BGTDT_VERSION,
            NVL ( (SELECT   CODELST_DESC
                     FROM   esch.SCH_CODELST
                    WHERE   PK_CODELST = B.BUDGET_TEMPLATE),
                 (SELECT   BUDGET_NAME
                    FROM   esch.SCH_BUDGET
                   WHERE   PK_BUDGET = B.BUDGET_TEMPLATE))
               BGTDT_TEMPLATE,
            (SELECT   STUDY_NUMBER
               FROM   eres.ER_STUDY
              WHERE   PK_STUDY = FK_STUDY)
               BGTDT_STUDY_NUM,
            (SELECT   STUDY_TITLE
               FROM   eres.ER_STUDY
              WHERE   PK_STUDY = FK_STUDY)
               BGTDT_STUDY_TITLE,
            (SELECT   SITE_NAME
               FROM   eres.ER_SITE
              WHERE   PK_SITE = FK_SITE)
               BGTDT_ORGANIZATION,
            ERES.F_GEt_Fstat (BUDGET_STATUS) BGTDT_STATUS,
            BUDGET_DESC BGTDT_DESCRIPTION,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = BUDGET_CREATOR)
               BGTDT_CREATED_BY,
            (SELECT   CODELST_DESC
               FROM   esch.SCH_CODELST
              WHERE   PK_CODELST = BUDGET_CURRENCY)
               BGTDT_CURRENCY,
            B.FK_ACCOUNT,
            PK_BUDGET,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = B.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = B.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            B.LAST_MODIFIED_DATE,
            B.CREATED_ON,
            fk_study,
            BGTCAL_SPONSOROHEAD,
            ERES.F_GEt_Yesno (BGTCAL_SPONSORFLAG) SPONSOR_OHEAD_FLAG,
            BGTCAL_INDIRECTCOST,
            BGTCAL_FRGBENEFIT,
            ERES.F_GEt_Yesno (BGTCAL_FRGFLAG) FRINGEBFT_FLAG,
            BGTCAL_DISCOUNT,
            ERES.F_GEt_Yesno (BGTCAL_DISCOUNTFLAG) DISCNT_FLAG,
            ERES.F_GEt_Yesno (BGTCAL_EXCLDSOCFLAG) EXCLUD_SOC,
            BUDGET_COMBFLAG,
            pk_budgetsec,
            bgtsection_name,
            bgtsection_sequence,
            bgtsection_type,
            bgtsection_patno,
            bgtsection_notes,
            bgtsection_personlflag,
            fk_visit
     FROM   ESCH.SCH_BUDGET B, ESCH.SCH_BGTCAL C, ESCH.SCH_BGTSECTION S
    WHERE       C.FK_BUDGET = B.PK_BUDGET
            AND (BUDGET_DELFLAG IS NULL OR BUDGET_DELFLAG <> 'Y')
            AND S.fk_bgtcal = C.pk_bgtcal
            AND NVL (BGTSECTION_DELFLAG, 'N') <> 'Y'
            AND NVL (BGTCAL_DELFLAG, 'N') <> 'Y'
/
COMMENT ON TABLE VDA.VDA_V_BUDGET_SECTION IS 'This view provides access to the Budget Sections design'
/

COMMENT ON COLUMN VDA.VDA_V_BUDGET_SECTION.BGTCAL_SPONSOROHEAD IS 'The Sponsor Overhead number'
/

COMMENT ON COLUMN VDA.VDA_V_BUDGET_SECTION.SPONSOR_OHEAD_FLAG IS 'The Sponsor Overhead flag'
/

COMMENT ON COLUMN VDA.VDA_V_BUDGET_SECTION.BGTCAL_INDIRECTCOST IS 'The indirect cost number to be applied to selected line items'
/

COMMENT ON COLUMN VDA.VDA_V_BUDGET_SECTION.BGTCAL_FRGBENEFIT IS 'The Fringe Benefit number to be applied to selected line items'
/

COMMENT ON COLUMN VDA.VDA_V_BUDGET_SECTION.FRINGEBFT_FLAG IS 'The Fringe Benefit calculation flag'
/

COMMENT ON COLUMN VDA.VDA_V_BUDGET_SECTION.BGTCAL_DISCOUNT IS 'The discount or markup number'
/

COMMENT ON COLUMN VDA.VDA_V_BUDGET_SECTION.DISCNT_FLAG IS 'The discount or markup flag '
/

COMMENT ON COLUMN VDA.VDA_V_BUDGET_SECTION.EXCLUD_SOC IS 'The Exclude Standard of Care cost from totals flag'
/

COMMENT ON COLUMN VDA.VDA_V_BUDGET_SECTION.BUDGET_COMBFLAG IS 'The flag to indicate if the budget is a combined budget'
/

COMMENT ON COLUMN VDA.VDA_V_BUDGET_SECTION.PK_BUDGETSEC IS 'The Primary Key of the Budget Section Record'
/

COMMENT ON COLUMN VDA.VDA_V_BUDGET_SECTION.BGTSECTION_NAME IS 'The name of the Budget Section'
/

COMMENT ON COLUMN VDA.VDA_V_BUDGET_SECTION.BGTSECTION_SEQUENCE IS 'The Sequence of the Section in the Budget'
/

COMMENT ON COLUMN VDA.VDA_V_BUDGET_SECTION.BGTSECTION_TYPE IS 'The Budget Section type'
/

COMMENT ON COLUMN VDA.VDA_V_BUDGET_SECTION.BGTSECTION_PATNO IS 'The number of patients the section is applicable for (used in calculating cost)'
/

COMMENT ON COLUMN VDA.VDA_V_BUDGET_SECTION.BGTSECTION_NOTES IS 'The notes linked with the section'
/

COMMENT ON COLUMN VDA.VDA_V_BUDGET_SECTION.BGTSECTION_PERSONLFLAG IS 'The Flag to indicate if it is a Personnel type section'
/

COMMENT ON COLUMN VDA.VDA_V_BUDGET_SECTION.BUDGET_VISIT_PK IS 'The Foreign Key to the visit the Budget line item is linked with'
/

COMMENT ON COLUMN VDA.VDA_V_BUDGET_SECTION.BGTDT_NAME IS 'The name of the Budget'
/

COMMENT ON COLUMN VDA.VDA_V_BUDGET_SECTION.BGTDT_VERSION IS 'The version of the Budget'
/

COMMENT ON COLUMN VDA.VDA_V_BUDGET_SECTION.BGTDT_TEMPLATE IS 'The template used for creating the budget'
/

COMMENT ON COLUMN VDA.VDA_V_BUDGET_SECTION.BGTDT_STUDY_NUM IS 'The study number of the study linked with the budget'
/

COMMENT ON COLUMN VDA.VDA_V_BUDGET_SECTION.BGTDT_STUDY_TITLE IS 'The title of the study linked with the budget'
/

COMMENT ON COLUMN VDA.VDA_V_BUDGET_SECTION.BGTDT_ORGANIZATION IS 'The organization linked with the budget'
/

COMMENT ON COLUMN VDA.VDA_V_BUDGET_SECTION.BGTDT_STATUS IS 'The status of the budget'
/

COMMENT ON COLUMN VDA.VDA_V_BUDGET_SECTION.BGTDT_DESCRIPTION IS 'The description of the budget'
/

COMMENT ON COLUMN VDA.VDA_V_BUDGET_SECTION.BGTDT_CREATED_BY IS 'The user who created the Budget'
/

COMMENT ON COLUMN VDA.VDA_V_BUDGET_SECTION.BGTDT_CURRENCY IS 'The currency the budget is created in'
/

COMMENT ON COLUMN VDA.VDA_V_BUDGET_SECTION.FK_ACCOUNT IS 'The Account linked with the Budget'''
/

COMMENT ON COLUMN VDA.VDA_V_BUDGET_SECTION.PK_BUDGET IS 'The Primary Key of the main Budget record'
/

COMMENT ON COLUMN VDA.VDA_V_BUDGET_SECTION.CREATOR IS 'The user who created the Budget record (Audit)'
/

COMMENT ON COLUMN VDA.VDA_V_BUDGET_SECTION.LAST_MODIFIED_BY IS 'The user who last modified the Budget Section record(Audit)'
/

COMMENT ON COLUMN VDA.VDA_V_BUDGET_SECTION.LAST_MODIFIED_DATE IS 'The date the Budget Section record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA.VDA_V_BUDGET_SECTION.CREATED_ON IS 'The date the Budget Section record was created on (Audit)'
/

COMMENT ON COLUMN VDA.VDA_V_BUDGET_SECTION.FK_STUDY IS 'The Foreign Key to the study the patient study status record is linked with'
/

/* Formatted on 8/22/2012 11:02:37 PM (QP5 v5.115.810.9015) */
--
-- VDA.VDA_V_PAT_DEMO_NOPHI  (View)
--
--  Dependencies:
--   F_GET_CODELSTDESC (Function)
--   PKG_UTIL (Package)
--   ER_SITE (Table)
--   ER_USER (Table)
--   PERSON (Table)
--

CREATE OR REPLACE FORCE VIEW VDA.VDA_V_PAT_DEMO_NOPHI
(
   PTDEM_PAT_ID,
   PTDEM_SURVIVSTAT,
   PTDEM_GENDER,
   PTDEM_MARSTAT,
   PTDEM_BLOODGRP,
   PTDEM_PRI_ETHNY,
   PTDEM_PRI_RACE,
   PTDEM_ADD_ETHNY,
   PTDEM_ADD_RACE,
   PTDEM_STATE,
   PTDEM_COUNTRY,
   PTDEM_PRI_SITE,
   FK_SITE,
   PTDEM_EMPLOY,
   PTDEM_EDUCATION,
   PTDEM_NOTES,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATED_ON,
   PK_PERSON,
   PTDEM_PHYOTHER,
   PTDEM_SPLACCESS,
   PTDEM_REGBY,
   PTDEM_REGDATE_YR,
   FK_ACCOUNT
)
AS
   SELECT   PERSON_CODE PTDEM_PAT_ID,
            ERES.F_GET_CODELSTDESC (FK_CODELST_PSTAT) PTDEM_SURVIVSTAT,
            ERES.F_GET_CODELSTDESC (FK_CODELST_GENDER) PTDEM_GENDER,
            ERES.F_GET_CODELSTDESC (FK_CODELST_MARITAL) PTDEM_MARSTAT,
            ERES.F_GET_CODELSTDESC (FK_CODELST_BLOODGRP) PTDEM_BLOODGRP,
            ERES.F_GET_CODELSTDESC (FK_CODELST_ETHNICITY) PTDEM_PRI_ETHNY,
            ERES.F_GET_CODELSTDESC (FK_CODELST_RACE) PTDEM_PRI_RACE,
            PERSON_ADD_ETHNICITY PTDEM_ADD_ETHNY,
            PERSON_ADD_RACE PTDEM_ADD_RACE,
            PERSON_STATE PTDEM_STATE,
            PERSON_COUNTRY PTDEM_COUNTRY,
            SITE_NAME AS PTDEM_PRI_SITE,
            fk_site,
            ERES.F_GET_CODELSTDESC (FK_CODELST_EMPLOYMENT) PTDEM_EMPLOY,
            ERES.F_GET_CODELSTDESC (FK_CODELST_EDU) PTDEM_EDUCATION,
            PERSON_NOTES_CLOB PTDEM_NOTES,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = p.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = p.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            p.LAST_MODIFIED_DATE,
            p.CREATED_ON,
            PK_PERSON,
            PERSON_PHYOTHER PTDEM_PHYOTHER,
            ERES.PKG_util.f_getCodeValues (PERSON_SPLACCESS, ',', ';')
               AS PTDEM_SPLACCESS,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   pk_user = PERSON_REGBY)
               PTDEM_REGBY,
            EXTRACT (YEAR FROM PERSON_REGDATE) PTDEM_REGDATE_YR,
            p.FK_ACCOUNT
     FROM   EPAT.PERSON p, eres.er_site
    WHERE   PK_SITE = FK_SITE
/
COMMENT ON TABLE VDA.VDA_V_PAT_DEMO_NOPHI IS 'This view provides access to the patient demographics data. This view does not provide any PHI'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_DEMO_NOPHI.PTDEM_PAT_ID IS 'The Patient ID or Patient Code'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_DEMO_NOPHI.PTDEM_SURVIVSTAT IS 'Patient Survival Status'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_DEMO_NOPHI.PTDEM_GENDER IS 'Patient Gender'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_DEMO_NOPHI.PTDEM_MARSTAT IS 'Patient Marital Status'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_DEMO_NOPHI.PTDEM_BLOODGRP IS 'Patient Blood Group'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_DEMO_NOPHI.PTDEM_PRI_ETHNY IS 'Patient Primary Ethnicity'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_DEMO_NOPHI.PTDEM_PRI_RACE IS 'Patient Primary Race'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_DEMO_NOPHI.PTDEM_ADD_ETHNY IS 'Patient Additional Ethnicity'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_DEMO_NOPHI.PTDEM_ADD_RACE IS 'Patient Additional Race'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_DEMO_NOPHI.PTDEM_STATE IS 'Patient address-State'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_DEMO_NOPHI.PTDEM_COUNTRY IS 'Patient address-Country'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_DEMO_NOPHI.PTDEM_PRI_SITE IS 'Patient primary site or organization'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_DEMO_NOPHI.FK_SITE IS 'The foreign key to primary site or organization record'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_DEMO_NOPHI.PTDEM_EMPLOY IS 'Patient Employment status'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_DEMO_NOPHI.PTDEM_EDUCATION IS 'Patient Education information'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_DEMO_NOPHI.PTDEM_NOTES IS 'Notes for the patient'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_DEMO_NOPHI.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_DEMO_NOPHI.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_DEMO_NOPHI.LAST_MODIFIED_DATE IS 'The date the record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_DEMO_NOPHI.CREATED_ON IS 'The date the record was created on (Audit)'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_DEMO_NOPHI.PK_PERSON IS 'The primary key of the patient record'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_DEMO_NOPHI.PTDEM_PHYOTHER IS 'Patient Physician-if other'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_DEMO_NOPHI.PTDEM_SPLACCESS IS 'The groups/departments with access to edit this patient''s demographics. If left blank, all groups will have this right'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_DEMO_NOPHI.PTDEM_REGBY IS 'Patient Registered By'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_DEMO_NOPHI.PTDEM_REGDATE_YR IS 'Year of patient registration'
/

COMMENT ON COLUMN VDA.VDA_V_PAT_DEMO_NOPHI.FK_ACCOUNT IS 'The account the calendar is linked with'
/

/* Formatted on 8/23/2012 9:48:33 AM (QP5 v5.115.810.9015) */
--
-- VDA.VDA_V_LIBCAL_SUBJCOST  (View)
--
--  Dependencies:
--   EVENT_DEF (Table)
--   SCH_CODELST (Table)
--   SCH_PROTOCOL_VISIT (Table)
--   SCH_SUBCOST_ITEM (Table)
--   SCH_SUBCOST_ITEM_VISIT (Table)
--

CREATE OR REPLACE FORCE VIEW VDA.VDA_V_LIBCAL_SUBJCOST
(
   PK_SUBCOST_ITEM,
   FK_CALENDAR,
   NAME,
   SUBCOST_ITEM_NAME,
   SUBCOST_ITEM_COST,
   SUBCOST_ITEM_UNIT,
   SUBCOST_CATEGORY,
   VISIT_NAME,
   FK_ACCOUNT
)
AS
   SELECT   PK_SUBCOST_ITEM,
            FK_CALENDAR,
            e.name,
            SUBCOST_ITEM_NAME,
            SUBCOST_ITEM_COST,
            SUBCOST_ITEM_UNIT,
              (SELECT   codelst_desc
               FROM   esch.sch_codelst
              WHERE   pk_codelst = FK_CODELST_CATEGORY)
               SUBCOST_CATEGORY,
                    (SELECT   visit_name
               FROM   esch.sch_protocol_visit
              WHERE   pk_protocol_visit = FK_PROTOCOL_VISIT)
               visit_name,
            e.user_id
     FROM   esch.SCH_SUBCOST_ITEM s,
            esch.event_def e,
            esch.SCH_SUBCOST_ITEM_VISIT v
    WHERE   e.event_id = fk_calendar
            AND v.FK_SUBCOST_ITEM(+) = PK_SUBCOST_ITEM
/
COMMENT ON TABLE VDA.VDA_V_LIBCAL_SUBJCOST IS 'This view provides access to the Subject Cost Items linked with a Library Calendar'
/

COMMENT ON COLUMN VDA.VDA_V_LIBCAL_SUBJCOST.PK_SUBCOST_ITEM IS 'Primary Key of the Subject Cost record'
/

COMMENT ON COLUMN VDA.VDA_V_LIBCAL_SUBJCOST.FK_CALENDAR IS 'Foreign Key to the Calendar record'
/

COMMENT ON COLUMN VDA.VDA_V_LIBCAL_SUBJCOST.NAME IS 'Name of the Calendar the subject Cost Item is linked with'
/

COMMENT ON COLUMN VDA.VDA_V_LIBCAL_SUBJCOST.SUBCOST_ITEM_NAME IS 'Subject Cost Item Name'
/

COMMENT ON COLUMN VDA.VDA_V_LIBCAL_SUBJCOST.SUBCOST_ITEM_COST IS 'Subject Cost Item Cost value'
/

COMMENT ON COLUMN VDA.VDA_V_LIBCAL_SUBJCOST.SUBCOST_ITEM_UNIT IS 'Number of Units planned'
/

COMMENT ON COLUMN VDA.VDA_V_LIBCAL_SUBJCOST.SUBCOST_CATEGORY IS 'Subject Cost Item Category'
/


COMMENT ON COLUMN VDA.VDA_V_LIBCAL_SUBJCOST.VISIT_NAME IS 'Name of the visit the cost item is linked with'
/

COMMENT ON COLUMN VDA.VDA_V_LIBCAL_SUBJCOST.FK_ACCOUNT IS 'The account the subject cost item is linked with'
/

CREATE OR REPLACE FORCE VIEW VDA.VDA_V_STUDYCAL_SUBJCOST
(
   PK_SUBCOST_ITEM,
   FK_CALENDAR,
   NAME,
   SUBCOST_ITEM_NAME,
   SUBCOST_ITEM_COST,
   SUBCOST_ITEM_UNIT,
   SUBCOST_CATEGORY,
   VISIT_NAME,
   FK_STUDY,
   FK_ACCOUNT
)
AS
   SELECT   PK_SUBCOST_ITEM,
            FK_CALENDAR,
            e.name,
            SUBCOST_ITEM_NAME,
            SUBCOST_ITEM_COST,
            SUBCOST_ITEM_UNIT,
            (SELECT   codelst_desc
               FROM   esch.sch_codelst
              WHERE   pk_codelst = FK_CODELST_CATEGORY)
               SUBCOST_CATEGORY,
            (SELECT   visit_name
               FROM   esch.sch_protocol_visit
              WHERE   pk_protocol_visit = FK_PROTOCOL_VISIT)
               visit_name,
            e.chain_id AS fk_study,
            e.user_id
     FROM   esch.SCH_SUBCOST_ITEM s,
            esch.event_assoc e,
            esch.SCH_SUBCOST_ITEM_VISIT v
    WHERE   e.event_id = fk_calendar
            AND v.FK_SUBCOST_ITEM(+) = PK_SUBCOST_ITEM
/
COMMENT ON TABLE VDA.VDA_V_STUDYCAL_SUBJCOST IS 'This view provides access to the Subject Cost Items linked with a Study Calendar'
/

COMMENT ON COLUMN VDA.VDA_V_STUDYCAL_SUBJCOST.PK_SUBCOST_ITEM IS 'Primary Key of the Subject Cost record'
/

COMMENT ON COLUMN VDA.VDA_V_STUDYCAL_SUBJCOST.FK_CALENDAR IS 'Foreign Key to the Calendar record'
/

COMMENT ON COLUMN VDA.VDA_V_STUDYCAL_SUBJCOST.NAME IS 'Name of the Calendar the subject Cost Item is linked with'
/

COMMENT ON COLUMN VDA.VDA_V_STUDYCAL_SUBJCOST.SUBCOST_ITEM_NAME IS 'Subject Cost Item Name'
/

COMMENT ON COLUMN VDA.VDA_V_STUDYCAL_SUBJCOST.SUBCOST_ITEM_COST IS 'Subject Cost Item Cost value'
/

COMMENT ON COLUMN VDA.VDA_V_STUDYCAL_SUBJCOST.SUBCOST_ITEM_UNIT IS 'Number of Units planned'
/

COMMENT ON COLUMN VDA.VDA_V_STUDYCAL_SUBJCOST.SUBCOST_CATEGORY IS 'Subject Cost Item Category'
/

COMMENT ON COLUMN VDA.VDA_V_STUDYCAL_SUBJCOST.VISIT_NAME IS 'Name of the visit the cost item is linked with'
/

COMMENT ON COLUMN VDA.VDA_V_STUDYCAL_SUBJCOST.FK_STUDY IS 'The Foreign Key of the study'
/

COMMENT ON COLUMN VDA.VDA_V_STUDYCAL_SUBJCOST.FK_ACCOUNT IS 'The account the subject cost item is linked with'
/

INSERT INTO VDA.VDA_TRACK_PATCHES
(
  PATCH_PK       ,
  DB_VER_MJR     ,
  DB_VER_MNR     ,
  DB_PATCH_NAME  ,
  FIRED_ON       ,
  APP_VERSION    
) VALUES(VDA.SEQ_VDA_TRACK_PATCHES.nextval,3,1,'01_fixes.sql',sysdate,'VDA 1.0 Build 1-a')
/

commit
/ 