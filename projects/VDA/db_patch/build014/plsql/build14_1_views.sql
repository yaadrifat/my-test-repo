/* Formatted on 6/9/2014 9:04:37 AM (QP5 v5.115.810.9015) */
--
-- VDA_V_MILESTONES  (View)
--
--  Dependencies:
--   F_GET_CODELSTDESC (Function)
--   F_GET_MTYPE (Function)
--   ER_CODELST (Table)
--   ER_MILESTONE (Table)
--   ER_STUDY (Table)
--   EVENT_ASSOC (Table)
--   SCH_CODELST (Table)
--   SCH_PROTOCOL_VISIT (Table)
--   ER_USER (Table)
--

CREATE OR REPLACE VIEW VDA_V_MILESTONES
(
   STUDY_NUMBER,
   STUDY_TITLE,
   MSRUL_CATEGORY,
   MSRUL_AMOUNT,
   MSRUL_PT_COUNT,
   MSRUL_PT_STATUS,
   MSRUL_LIMIT,
   MSRUL_PAY_TYPE,
   MSRUL_PAY_FOR,
   MSRUL_STATUS,
   MSRUL_PROT_CAL,
   MSRUL_VISIT,
   MSRUL_MS_RULE,
   MSRUL_EVENT_STAT,
   MSRUL_STUDY_STATUS,
   FK_ACCOUNT,
   PK_MILESTONE,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATED_ON,
   FK_STUDY,
   EVENT_NAME,
   MILESTONE_ACHIEVEDCOUNT,
   MILESTONE_ACHIEVEDAMOUNT,
   LAST_CHECKED_ON,
   MILESTONE_DESCRIPTION,
   FK_BUDGET,
   FK_BGTCAL,
   FK_BGTSECTION,
   FK_LINEITEM,
   MILESTONE_DATE_FROM,
   MILESTONE_DATE_TO,
   MILESTONE_HOLDBACK,
   MILESTONE_DESC_CALCULATED, fk_visit,FK_EVENTASSOC
)
AS
   SELECT   STUDY_NUMBER,
            STUDY_TITLE,
            ERES.F_GEt_Mtype (MILESTONE_TYPE) MSRUL_CATEGORY,
            MILESTONE_AMOUNT MSRUL_AMOUNT,
            MILESTONE_COUNT MSRUL_PT_COUNT,
            (SELECT   CODELST_DESC
               FROM   eres.ER_CODELST
              WHERE   PK_CODELST = MILESTONE_STATUS
                      AND CODELST_TYPE = 'patStatus')
               MSRUL_PT_STATUS,
            MILESTONE_LIMIT MSRUL_LIMIT,
            ERES.F_GEt_Codelstdesc (MILESTONE_PAYTYPE) MSRUL_PAY_TYPE,
            ERES.F_GEt_Codelstdesc (MILESTONE_PAYFOR) MSRUL_PAY_FOR,
            ERES.F_GEt_Codelstdesc (FK_CODELST_MILESTONE_STAT) MSRUL_STATUS,
            (SELECT   NAME
               FROM   ESCH.EVENT_ASSOC
              WHERE   EVENT_ID = FK_CAL)
               MSRUL_PROT_CAL,
            (SELECT   visit_name
               FROM   esch.sch_protocol_visit
              WHERE   pk_protocol_visit = fk_visit)
               MSRUL_VISIT,
            ERES.F_GEt_Codelstdesc (FK_CODELST_RULE) MSRUL_MS_RULE,
            (SELECT   CODELST_DESC
               FROM   esch.SCH_CODELST
              WHERE   PK_CODELST = MILESTONE_EVENTSTATUS)
               MSRUL_EVENT_STAT,
            (SELECT   CODELST_DESC
               FROM   eres.ER_CODELST
              WHERE   PK_CODELST = MILESTONE_STATUS
                      AND CODELST_TYPE = 'studystat')
               MSRUL_STUDY_STATUS,
            s.fk_Account FK_ACCOUNT,
            PK_MILESTONE,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = m.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = m.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            m.LAST_MODIFIED_DATE,
            m.CREATED_ON,
            FK_STUDY,
            (SELECT   NAME
               FROM   ESCH.EVENT_ASSOC
              WHERE   EVENT_ID = FK_EVENTASSOC)
               EVENT_NAME,
            MILESTONE_ACHIEVEDCOUNT,
            NVL (MILESTONE_ACHIEVEDCOUNT * MILESTONE_AMOUNT, 0),
            LAST_CHECKED_ON,
            MILESTONE_DESCRIPTION,
            FK_BUDGET,
            FK_BGTCAL,
            FK_BGTSECTION,
            FK_LINEITEM,
            m.MILESTONE_DATE_FROM,
            m.MILESTONE_DATE_TO,
            m.MILESTONE_HOLDBACK,
			ERES.PKG_Milestone_New.f_getMilestoneDesc (PK_MILESTONE), fk_visit,FK_EVENTASSOC
     FROM   eres.ER_MILESTONE m, eres.er_study s
    WHERE   NVL (milestone_delflag, 'N') = 'N' AND m.fk_study = pk_study
/
COMMENT ON TABLE VDA_V_MILESTONES IS 'This view provides access to the Milestones defined for studies'
/

COMMENT ON COLUMN VDA_V_MILESTONES.STUDY_NUMBER IS 'The Study Number of the study the milestone is linked with'
/

COMMENT ON COLUMN VDA_V_MILESTONES.STUDY_TITLE IS 'The study title of the study the milestone is linked with'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_CATEGORY IS 'The milestone rule category'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_AMOUNT IS 'The amount linked with the milestone'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_PT_COUNT IS 'The patient count attribute of the milestone (if applicable)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_PT_STATUS IS 'The patient status attribute of the milestone (if applicable)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_LIMIT IS 'The milestone achievement count limit'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_PAY_TYPE IS 'The milestone payment type'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_PAY_FOR IS 'The milestone payment for'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_STATUS IS 'The milestone status'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_PROT_CAL IS 'The Calendar attribute of the milestone (if applicable)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_VISIT IS 'The Calendar visit attribute of the milestone (if applicable)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_MS_RULE IS 'The milestone rule description'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_EVENT_STAT IS 'The Calendar event status attribute of the milestone (if applicable)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_STUDY_STATUS IS 'The Study Status attribute of the milestone (if applicable)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.FK_ACCOUNT IS 'The account the milestone is linked with'
/

COMMENT ON COLUMN VDA_V_MILESTONES.PK_MILESTONE IS 'The primary key of the milestone record'
/

COMMENT ON COLUMN VDA_V_MILESTONES.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.LAST_MODIFIED_DATE IS 'The date the record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.CREATED_ON IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.FK_STUDY IS 'The Foreign Key to the study milestone is linked with'
/

COMMENT ON COLUMN VDA_V_MILESTONES.EVENT_NAME IS 'The event name attribute of the milestone (if applicable)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MILESTONE_ACHIEVEDCOUNT IS 'The current milestone achievement count'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MILESTONE_ACHIEVEDAMOUNT IS 'The current milestone achieved amount'
/

COMMENT ON COLUMN VDA_V_MILESTONES.LAST_CHECKED_ON IS 'The timestamp when the milestone achievement was last checked on'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MILESTONE_DESCRIPTION IS 'The milestone description'
/

COMMENT ON COLUMN VDA_V_MILESTONES.FK_BUDGET IS 'The foreign key to the Budget (if the milestone was created from a budget)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.FK_BGTCAL IS 'The foreign key to the Budget Calendar record (if the milestone was created from a budget)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.FK_BGTSECTION IS 'The foreign key to the Budget Section record Calendar record (if the milestone was created from a budget)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.FK_LINEITEM IS 'The foreign key to the Budget line item record (if the milestone was created from a budget)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MILESTONE_DATE_FROM IS 'The Start Date for the time-bounded milestones'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MILESTONE_DATE_TO IS 'The To or End Date for the time-bounded milestones'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MILESTONE_HOLDBACK IS 'The Holdback amount for the milestone'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MILESTONE_DESC_CALCULATED IS 'The Milestone description calculated from milestone attributes'
/

COMMENT ON COLUMN VDA_V_MILESTONES.fk_visit IS 'The PK of visit to uniquely identify a visit for visit milestones'
/

COMMENT ON COLUMN VDA_V_MILESTONES.FK_EVENTASSOC IS 'The PK of visit to uniquely identify an Event for event milestones'
/
   
   
/* Formatted on 6/9/2014 10:21:36 AM (QP5 v5.115.810.9015) */
--
-- VDA_V_MILESTONE_ACHVD_DET  (View)
--
--  Dependencies:
--   ER_PATPROT (Table)
--   ER_PER (Table)
--   PKG_MILESTONE_NEW (Package)
--   ER_MILEACHIEVED (Table)
--   ER_STUDY (Table)
--   ER_USER (Table)
--

CREATE OR REPLACE FORCE VIEW VDA_V_MILESTONE_ACHVD_DET
(
   PK_MILEACHIEVED,
   MSACH_MILESTONE_DESC,
   MSACH_PATIENT_ID,
   MSACH_PTSTUDY_ID,
   MSACH_ACH_DATE,
   CREATOR,
   CREATED_ON,
   LAST_MODIFIED_DATE,
   LAST_MODIFIED_BY,
   FK_STUDY,
   FK_ACCOUNT,
   STUDY_NUMBER, fk_milestone
)
AS
   SELECT   PK_MILEACHIEVED,
            ERES.PKG_Milestone_New.f_getMilestoneDesc (FK_MILESTONE)
               MSACH_MILESTONE_DESC,
            (SELECT   PER_CODE
               FROM   eres.ER_PER
              WHERE   PK_PER = FK_PER)
               MSACH_PATIENT_ID,
            (SELECT   PATPROT_PATSTDID
               FROM   eres.ER_PATPROT pp
              WHERE       pp.fk_per = a.fk_per
                      AND pp.fk_study = a.fk_study
                      AND pp.patprot_stat = 1
                      AND ROWNUM < 2)
               MSACH_PTSTUDY_ID,
            ACH_DATE MSACH_ACH_DATE,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   eres.ER_USER
              WHERE   pk_user = a.CREATOR)
               creator,
            a.CREATED_ON,
            a.LAST_MODIFIED_DATE,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   eres.ER_USER
              WHERE   pk_user = a.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            fk_study,
            b.fk_account,
            b.STUDY_NUMBER, fk_milestone
     FROM   eres.ER_MILEACHIEVED a, eres.ER_STUDY b
    WHERE   IS_COMPLETE = 1 AND b.pk_study = a.fk_study
/
COMMENT ON TABLE VDA_V_MILESTONE_ACHVD_DET IS 'This view provides access to the milestone achievement data'
/

COMMENT ON COLUMN VDA_V_MILESTONE_ACHVD_DET.PK_MILEACHIEVED IS 'The Primary of the milestones achievement records'
/

COMMENT ON COLUMN VDA_V_MILESTONE_ACHVD_DET.MSACH_MILESTONE_DESC IS 'Milestone description'
/

COMMENT ON COLUMN VDA_V_MILESTONE_ACHVD_DET.MSACH_PATIENT_ID IS 'Patient ID for Patient related milestones'
/

COMMENT ON COLUMN VDA_V_MILESTONE_ACHVD_DET.MSACH_PTSTUDY_ID IS 'Patient Study ID for Patient related milestones'
/

COMMENT ON COLUMN VDA_V_MILESTONE_ACHVD_DET.MSACH_ACH_DATE IS 'milestone achievement date'
/

COMMENT ON COLUMN VDA_V_MILESTONE_ACHVD_DET.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_MILESTONE_ACHVD_DET.CREATED_ON IS 'The date the record was created on (Audit)'
/

COMMENT ON COLUMN VDA_V_MILESTONE_ACHVD_DET.LAST_MODIFIED_DATE IS 'The date the record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_MILESTONE_ACHVD_DET.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_MILESTONE_ACHVD_DET.FK_STUDY IS 'The Foreign Key to the study milestone is linked with'
/

COMMENT ON COLUMN VDA_V_MILESTONE_ACHVD_DET.FK_ACCOUNT IS 'The account the milestone is linked with'
/

COMMENT ON COLUMN VDA_V_MILESTONE_ACHVD_DET.STUDY_NUMBER IS 'The Study Number of the study the milestone is linked with'
/

COMMENT ON COLUMN VDA_V_MILESTONE_ACHVD_DET.fk_milestone IS 'The PK_milestone to uniquely identify a milestone'
/

/* Formatted on 6/9/2014 12:33:11 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_INVOICE_PAYMENTS_DETAILS  (View)
--
--  Dependencies:
--   F_GET_CODELSTDESC (Function)
--   ER_MILEPAYMENT_DETAILS (Table)
--   VDA_V_PAYMENTS (View)
--

CREATE OR REPLACE FORCE VIEW VDA_V_INVOICE_PAYMENTS_DETAILS
(
   STUDY_NUMBER,
   STUDY_TITLE,
   MSPAY_DATE,
   MSPAY_TYPE,
   MSPAY_AMT_RECVD,
   MSPAY_DESC,
   MSPAY_COMMENTS,
   PK_MILEPAYMENT,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATED_ON,
   FK_STUDY,
   FK_ACCOUNT,
   FK_INVOICE,
   MP_AMOUNT,
   MP_HOLDBACK_AMOUNT
)
AS
   SELECT   "STUDY_NUMBER",
            "STUDY_TITLE",
            "MSPAY_DATE",
            "MSPAY_TYPE",
            "MSPAY_AMT_RECVD",
            "MSPAY_DESC",
            "MSPAY_COMMENTS",
            "PK_MILEPAYMENT",
            "CREATOR",
            "LAST_MODIFIED_BY",
            "LAST_MODIFIED_DATE",
            "CREATED_ON",
            "FK_STUDY",
            "FK_ACCOUNT",
            "FK_INVOICE",
            "MP_AMOUNT",
            "MP_HOLDBACK_AMOUNT"
     FROM   VDA_V_PAYMENTS,
            (  SELECT   MP_LINKTO_ID fk_invoice,
                        fk_milepayment,
                        SUM (MP_AMOUNT) MP_AMOUNT,
                        SUM (mp_holdback_amount) mp_holdback_amount
                 FROM   eres.ER_MILEPAYMENT_DETAILS
                WHERE   MP_LINKTO_TYPE = 'I'
             GROUP BY   MP_LINKTO_ID, fk_milepayment) x
    WHERE   PK_MILEPAYMENT = FK_MILEPAYMENT
/

COMMENT ON TABLE VDA_V_INVOICE_PAYMENTS_DETAILS IS 'This view provides access to the payment details information for payments linked with invoices'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.FK_ACCOUNT IS 'The account the milestone is linked with'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.FK_INVOICE IS 'The PK to uniquely identify invoice the payment is linked with'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.MP_AMOUNT IS 'The Payment amount linked with the invoice'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.MP_HOLDBACK_AMOUNT IS 'The Payment amount linked with the invoice and was holdback'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.STUDY_NUMBER IS 'The Study Number of the study the payment  is linked with'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.STUDY_TITLE IS 'The Study Title of the study the payment is linked with'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.MSPAY_DATE IS 'The payment date'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.MSPAY_TYPE IS 'The payment type'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.MSPAY_AMT_RECVD IS 'The payment amount received'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.MSPAY_DESC IS 'The payment description'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.MSPAY_COMMENTS IS 'The payment comments'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.PK_MILEPAYMENT IS 'The Primary Key of the payment record'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.LAST_MODIFIED_DATE IS 'The date the record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.CREATED_ON IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_INVOICE_PAYMENTS_DETAILS.FK_STUDY IS 'The Foreign Key to the study milestone is linked with'
/

 
INSERT INTO VDA_TRACK_PATCHES
(
  PATCH_PK       ,
  DB_VER_MJR     ,
  DB_VER_MNR     ,
  DB_PATCH_NAME  ,
  FIRED_ON       ,
  APP_VERSION    
) VALUES(SEQ_VDA_TRACK_PATCHES.nextval,5,7,'build14_1_views.sql',sysdate,'VDA 1.4 Build014')
/

commit
/