/* Formatted on 3/11/2014 3:19:23 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_PAT_ACCRUAL  (View)
--
--  Dependencies:
--   ER_PATPROT (Table)
--   F_GET_CODELSTDESC (Function)
--   ER_STUDY (Table)
--   ER_USER (Table)
--   ER_SITE (Table)
--

CREATE OR REPLACE FORCE VIEW VDA_V_PAT_ACCRUAL
(
   PSTAT_PAT_STUD_ID,
   PSTAT_ASSIGNED_TO,
   PSTAT_PHYSICIAN,
   PSTAT_TREAT_LOCAT,
   PSTAT_EVAL_FLAG,
   PSTAT_EVAL_STAT,
   PSTAT_INEVAL_STAT,
   PSTAT_SURVIVAL_STAT,
   PSTAT_RANDOM_NUM,
   PSTAT_ENROLLED_BY,
   PSTAT_ENROLL_SITE,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATED_ON,
   FK_PER,
   FK_STUDY,
   PATPROT_ENROLDT,
   PATPROT_TREATINGORG,
   FK_SITE_ENROLLING,
   STUDY_NUMBER,
   FK_ACCOUNT,
   PK_PATPROT, DMGRPH_REPORTABLE,   PATPROT_DISEASE_CODE,
			PATPROT_MORE_DIS_CODE1,  PATPROT_MORE_DIS_CODE2, 
			PATPROT_OTHR_DIS_CODE
)
AS
   SELECT   PATPROT_PATSTDID PSTAT_PAT_STUD_ID,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = FK_USERASSTO)
               PSTAT_ASSIGNED_TO,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = PATPROT_PHYSICIAN)
               PSTAT_PHYSICIAN,
            ERES.F_GET_CODELSTDESC (FK_CODELSTLOC) PSTAT_TREAT_LOCAT,
            ERES.F_GET_CODELSTDESC (FK_CODELST_PTST_EVAL_FLAG)
               PSTAT_EVAL_FLAG,
            ERES.F_GET_CODELSTDESC (FK_CODELST_PTST_EVAL) PSTAT_EVAL_STAT,
            ERES.F_GET_CODELSTDESC (FK_CODELST_PTST_INEVAL) PSTAT_INEVAL_STAT,
            ERES.F_GET_CODELSTDESC (FK_CODELST_PTST_SURVIVAL) PSTAT_SURV_STAT,
            PATPROT_RANDOM PSTAT_RANDOM_NUM,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = pp.FK_USER)
               PSTAT_ENROLLED_BY,
            (SELECT   SITE_NAME
               FROM   eres.ER_SITE
              WHERE   PK_SITE = FK_SITE_ENROLLING)
               PSTAT_ENROLL_SITE,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = pp.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = pp.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            pp.LAST_MODIFIED_DATE,
            pp.CREATED_ON,
            fk_per,
            fk_study,
            patprot_enroldt,
            (SELECT   SITE_NAME
               FROM   eres.ER_SITE
              WHERE   PK_SITE = patprot_treatingorg)
               patprot_treatingorg,
            FK_SITE_ENROLLING,
            STUDY_NUMBER,
            FK_ACCOUNT,
            PK_PATPROT,
			decode(pp.DMGRPH_REPORTABLE, 1,'Yes','No')DMGRPH_REPORTABLE,   pp.PATPROT_DISEASE_CODE,
			pp.PATPROT_MORE_DIS_CODE1,  pp.PATPROT_MORE_DIS_CODE2, 
			pp.PATPROT_OTHR_DIS_CODE
     FROM   eres.ER_PATPROT pp, eres.er_study s
    WHERE       patprot_stat = 1
            AND patprot_enroldt IS NOT NULL
            AND s.pk_study = pp.fk_study
/
COMMENT ON TABLE VDA_V_PAT_ACCRUAL IS 'This view provides access to the patient accrual data for studies'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.FK_STUDY IS 'The Foreign Key to the study milestone is linked with'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PATPROT_ENROLDT IS 'The enrollment date'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PATPROT_TREATINGORG IS 'The treating organization'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.FK_SITE_ENROLLING IS 'The foreign key to the enrolling site'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.STUDY_NUMBER IS 'The Study Number of the study the enrollment record  is linked with'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.FK_ACCOUNT IS 'The account the patient is linked with'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PSTAT_PAT_STUD_ID IS 'The patient study id'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PSTAT_ASSIGNED_TO IS 'The user patient is assigned to'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PSTAT_PHYSICIAN IS 'The physician patient is assigned to'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PSTAT_TREAT_LOCAT IS 'The treatment location'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PSTAT_EVAL_FLAG IS 'The evaluation flag'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PSTAT_EVAL_STAT IS 'The evaluation status'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PSTAT_INEVAL_STAT IS 'The inevaluation status'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PSTAT_SURVIVAL_STAT IS 'The survival status'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PSTAT_RANDOM_NUM IS 'The randomization number allocated to the patient'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PSTAT_ENROLLED_BY IS 'Patient Enrolled By'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PSTAT_ENROLL_SITE IS 'Patient Enrolling Site'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.LAST_MODIFIED_DATE IS 'The date the record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.CREATED_ON IS 'The date the record was created on (Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.FK_PER IS 'The foreign Key to the patient record'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.DMGRPH_REPORTABLE IS 'Flag to indicate whether the patient demographics is CTRP reportable'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PATPROT_DISEASE_CODE IS 'Patient''s disease code value'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PATPROT_OTHR_DIS_CODE IS 'Patient''s Other disease code value'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PATPROT_MORE_DIS_CODE1 IS 'Patient''s More disease code 1 value'
/
 
COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PATPROT_MORE_DIS_CODE2 IS 'Patient''s More disease code 2 value'
/

/* Formatted on 3/11/2014 3:19:24 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_PAT_ACCRUAL_ALL  (View)
--
--  Dependencies:
--   PKG_UTIL (Package)
--   F_GET_CODELSTDESC (Function)
--   ER_STUDYSITES (Table)
--   CODE_LST_NAMES (Function)
--   VDA_V_PAT_ACCRUAL (View)
--   VDA_V_PAT_DEMO_NOPHI (View)
--

CREATE OR REPLACE FORCE VIEW VDA_V_PAT_ACCRUAL_ALL
(
   PSTAT_PAT_STUD_ID,
   PSTAT_ASSIGNED_TO,
   PSTAT_PHYSICIAN,
   PSTAT_TREAT_LOCAT,
   PSTAT_EVAL_FLAG,
   PSTAT_EVAL_STAT,
   PSTAT_INEVAL_STAT,
   PSTAT_SURVIVAL_STAT,
   PSTAT_RANDOM_NUM,
   PSTAT_ENROLLED_BY,
   PSTAT_ENROLL_SITE,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATED_ON,
   FK_PER,
   FK_STUDY,
   PATPROT_ENROLDT,
   PATPROT_TREATINGORG,
   PATPROT_ENROLL_YEAR,
   PTDEM_SURVIVSTAT,
   PTDEM_GENDER,
   PTDEM_MARSTAT,
   PTDEM_BLOODGRP,
   PTDEM_PRI_ETHNY,
   PTDEM_PRI_RACE,
   PTDEM_ADD_ETHNY,
   PTDEM_ADD_RACE,
   PTDEM_STATE,
   PTDEM_COUNTRY,
   PTDEM_EMPLOY,
   PTDEM_EDUCATION,
   PTDEM_REGDATE_YR,
   STUDYSITE_LSAMPLESIZE,DMGRPH_REPORTABLE,   PATPROT_DISEASE_CODE,
			PATPROT_MORE_DIS_CODE1,  PATPROT_MORE_DIS_CODE2, 
			PATPROT_OTHR_DIS_CODE
)
AS
   SELECT   PSTAT_PAT_STUD_ID,
            PSTAT_ASSIGNED_TO,
            PSTAT_PHYSICIAN,
            PSTAT_TREAT_LOCAT,
            PSTAT_EVAL_FLAG,
            PSTAT_EVAL_STAT,
            PSTAT_INEVAL_STAT,
            PSTAT_SURVIVAL_STAT,
            PSTAT_RANDOM_NUM,
            PSTAT_ENROLLED_BY,
            PSTAT_ENROLL_SITE,
            a.CREATOR,
            a.LAST_MODIFIED_BY,
            a.LAST_MODIFIED_DATE,
            a.CREATED_ON,
            a.FK_PER,
            a.FK_STUDY,
            patprot_enroldt,
            patprot_treatingorg,
            EXTRACT (YEAR FROM patprot_enroldt) patprot_enroll_year,
            d.PTDEM_SURVIVSTAT,
            d.PTDEM_GENDER,
            d.PTDEM_MARSTAT,
            d.PTDEM_BLOODGRP,
            d.PTDEM_PRI_ETHNY,
            d.PTDEM_PRI_RACE,
            d.PTDEM_ADD_ETHNY,
            d.PTDEM_ADD_RACE,
            d.PTDEM_STATE,
            d.PTDEM_COUNTRY,
            d.PTDEM_EMPLOY,
            d.PTDEM_EDUCATION,
            d.PTDEM_REGDATE_YR,
            studysite_lsamplesize, a.DMGRPH_REPORTABLE,   a.PATPROT_DISEASE_CODE,
			a.PATPROT_MORE_DIS_CODE1,  a.PATPROT_MORE_DIS_CODE2, 
			a.PATPROT_OTHR_DIS_CODE
     FROM   VDA.VDA_V_PAT_ACCRUAL a,
            VDA.VDA_V_PAT_DEMO_NOPHI d,
            eres.er_studySites s
    WHERE       a.fk_per = d.pk_person
            AND s.fk_study = a.fk_study
            AND s.fk_site = a.FK_SITE_ENROLLING
/
COMMENT ON TABLE VDA_V_PAT_ACCRUAL_ALL IS 'This view provides access to the patient accrual information with patient details'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PSTAT_PAT_STUD_ID IS 'The patiient study id'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PSTAT_ASSIGNED_TO IS 'The user patient is assigned to'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PSTAT_PHYSICIAN IS 'The physician patient is assigned to'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PSTAT_TREAT_LOCAT IS 'The treatment location'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PSTAT_EVAL_FLAG IS 'The Evaluable Flag'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PSTAT_EVAL_STAT IS 'The Evaluable Status'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PSTAT_INEVAL_STAT IS 'The Unevaluable Status'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PSTAT_SURVIVAL_STAT IS 'The Patient survival status'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PSTAT_RANDOM_NUM IS 'The randomization number'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PSTAT_ENROLLED_BY IS 'The user who enrolled the patient'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PSTAT_ENROLL_SITE IS 'The patient enrolling site'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.LAST_MODIFIED_DATE IS 'The date the  record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.CREATED_ON IS 'The date the record was created on (Audit)'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.FK_PER IS 'The Primary Key of the patient the schedule is linked with'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.FK_STUDY IS 'The Primary Key of the study schedule is linked with'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PATPROT_ENROLDT IS 'The enrollment date'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PATPROT_TREATINGORG IS 'The treating organization name'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PATPROT_ENROLL_YEAR IS 'The enrollment year'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PTDEM_SURVIVSTAT IS 'The survival status'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PTDEM_GENDER IS 'The patient gender'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PTDEM_MARSTAT IS 'The patient marital status'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PTDEM_BLOODGRP IS 'The patient blood group'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PTDEM_PRI_ETHNY IS 'The patient primary ethnicity'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PTDEM_PRI_RACE IS 'The patient primary race'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PTDEM_ADD_ETHNY IS 'The patient additional ethnicity'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PTDEM_ADD_RACE IS 'The patient additional race'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PTDEM_STATE IS 'The patient state'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PTDEM_COUNTRY IS 'The patient country'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PTDEM_EMPLOY IS 'The patient employment'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PTDEM_EDUCATION IS 'The patient education'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.PTDEM_REGDATE_YR IS 'The patient registration year with the default site or organization'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL_ALL.STUDYSITE_LSAMPLESIZE IS 'The enrolling site''s local sample size'
/


COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.DMGRPH_REPORTABLE IS 'Flag to indicate whether the patient demographics is CTRP reportable'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PATPROT_DISEASE_CODE IS 'Patient''s disease code value'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PATPROT_OTHR_DIS_CODE IS 'Patient''s Other disease code value'
/

COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PATPROT_MORE_DIS_CODE1 IS 'Patient''s More disease code 1 value'
/
 
COMMENT ON COLUMN VDA_V_PAT_ACCRUAL.PATPROT_MORE_DIS_CODE2 IS 'Patient''s More disease code 2 value'
/

/* Formatted on 3/11/2014 3:19:19 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_MILESTONES  (View)
--
--  Dependencies:
--   F_GET_CODELSTDESC (Function)
--   F_GET_MTYPE (Function)
--   ER_CODELST (Table)
--   ER_MILESTONE (Table)
--   ER_STUDY (Table)
--   EVENT_ASSOC (Table)
--   SCH_CODELST (Table)
--   SCH_PROTOCOL_VISIT (Table)
--   ER_USER (Table)
--

CREATE OR REPLACE FORCE VIEW VDA_V_MILESTONES
(
   STUDY_NUMBER,
   STUDY_TITLE,
   MSRUL_CATEGORY,
   MSRUL_AMOUNT,
   MSRUL_PT_COUNT,
   MSRUL_PT_STATUS,
   MSRUL_LIMIT,
   MSRUL_PAY_TYPE,
   MSRUL_PAY_FOR,
   MSRUL_STATUS,
   MSRUL_PROT_CAL,
   MSRUL_VISIT,
   MSRUL_MS_RULE,
   MSRUL_EVENT_STAT,
   MSRUL_STUDY_STATUS,
   FK_ACCOUNT,
   PK_MILESTONE,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATED_ON,
   FK_STUDY,
   EVENT_NAME,
   MILESTONE_ACHIEVEDCOUNT,
   MILESTONE_ACHIEVEDAMOUNT,
   LAST_CHECKED_ON,
   MILESTONE_DESCRIPTION,
   FK_BUDGET,
   FK_BGTCAL,
   FK_BGTSECTION,
   FK_LINEITEM,
   MILESTONE_DATE_FROM,
   MILESTONE_DATE_TO,
   MILESTONE_HOLDBACK 
)
AS
   SELECT   STUDY_NUMBER,
            STUDY_TITLE,
            ERES.F_GEt_Mtype (MILESTONE_TYPE) MSRUL_CATEGORY,
            MILESTONE_AMOUNT MSRUL_AMOUNT,
            MILESTONE_COUNT MSRUL_PT_COUNT,
            (SELECT   CODELST_DESC
               FROM   eres.ER_CODELST
              WHERE   PK_CODELST = MILESTONE_STATUS
                      AND CODELST_TYPE = 'patStatus')
               MSRUL_PT_STATUS,
            MILESTONE_LIMIT MSRUL_LIMIT,
            ERES.F_GEt_Codelstdesc (MILESTONE_PAYTYPE) MSRUL_PAY_TYPE,
            ERES.F_GEt_Codelstdesc (MILESTONE_PAYFOR) MSRUL_PAY_FOR,
            ERES.F_GEt_Codelstdesc (FK_CODELST_MILESTONE_STAT) MSRUL_STATUS,
            (SELECT   NAME
               FROM   ESCH.EVENT_ASSOC
              WHERE   EVENT_ID = FK_CAL)
               MSRUL_PROT_CAL,
            (SELECT   visit_name
               FROM   esch.sch_protocol_visit
              WHERE   pk_protocol_visit = fk_visit)
               MSRUL_VISIT,
            ERES.F_GEt_Codelstdesc (FK_CODELST_RULE) MSRUL_MS_RULE,
            (SELECT   CODELST_DESC
               FROM   esch.SCH_CODELST
              WHERE   PK_CODELST = MILESTONE_EVENTSTATUS)
               MSRUL_EVENT_STAT,
            (SELECT   CODELST_DESC
               FROM   eres.ER_CODELST
              WHERE   PK_CODELST = MILESTONE_STATUS
                      AND CODELST_TYPE = 'studystat')
               MSRUL_STUDY_STATUS,
            s.fk_Account FK_ACCOUNT,
            PK_MILESTONE,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = m.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = m.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            m.LAST_MODIFIED_DATE,
            m.CREATED_ON,
            FK_STUDY,
            (SELECT   NAME
               FROM   ESCH.EVENT_ASSOC
              WHERE   EVENT_ID = FK_EVENTASSOC)
               EVENT_NAME,
            MILESTONE_ACHIEVEDCOUNT,
            NVL (MILESTONE_ACHIEVEDCOUNT * MILESTONE_AMOUNT, 0),
            LAST_CHECKED_ON,
            MILESTONE_DESCRIPTION,
            FK_BUDGET,
            FK_BGTCAL,
            FK_BGTSECTION,
            FK_LINEITEM, m.MILESTONE_DATE_FROM, m.MILESTONE_DATE_TO, m.MILESTONE_HOLDBACK
     FROM   eres.ER_MILESTONE m, eres.er_study s
    WHERE   NVL (milestone_delflag, 'N') = 'N' AND m.fk_study = pk_study
/
COMMENT ON TABLE VDA_V_MILESTONES IS 'This view provides access to the Milestones defined for studies'
/

COMMENT ON COLUMN VDA_V_MILESTONES.FK_BGTCAL IS 'The foreign key to the Budget Calendar record (if the milestone was created from a budget)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.FK_BGTSECTION IS 'The foreign key to the Budget Section record Calendar record (if the milestone was created from a budget)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.FK_LINEITEM IS 'The foreign key to the Budget line item record (if the milestone was created from a budget)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.STUDY_NUMBER IS 'The Study Number of the study the milestone is linked with'
/

COMMENT ON COLUMN VDA_V_MILESTONES.STUDY_TITLE IS 'The study title of the study the milestone is linked with'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_CATEGORY IS 'The milestone rule category'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_AMOUNT IS 'The amount linked with the milestone'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_PT_COUNT IS 'The patient count attribute of the milestone (if applicable)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_PT_STATUS IS 'The patient status attribute of the milestone (if applicable)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_LIMIT IS 'The milestone achievement count limit'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_PAY_TYPE IS 'The milestone payment type'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_PAY_FOR IS 'The milestone payment for'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_STATUS IS 'The milestone status'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_PROT_CAL IS 'The Calendar attribute of the milestone (if applicable)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_VISIT IS 'The Calendar visit attribute of the milestone (if applicable)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_MS_RULE IS 'The milestone rule description'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_EVENT_STAT IS 'The Calendar event status attribute of the milestone (if applicable)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MSRUL_STUDY_STATUS IS 'The Study Status attribute of the milestone (if applicable)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.FK_ACCOUNT IS 'The account the milestone is linked with'
/

COMMENT ON COLUMN VDA_V_MILESTONES.PK_MILESTONE IS 'The primary key of the milestone record'
/

COMMENT ON COLUMN VDA_V_MILESTONES.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.LAST_MODIFIED_DATE IS 'The date the record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.CREATED_ON IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.FK_STUDY IS 'The Foreign Key to the study milestone is linked with'
/

COMMENT ON COLUMN VDA_V_MILESTONES.EVENT_NAME IS 'The event name attribute of the milestone (if applicable)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MILESTONE_ACHIEVEDCOUNT IS 'The current milestone achievement count'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MILESTONE_ACHIEVEDAMOUNT IS 'The current milestone achieved amount'
/

COMMENT ON COLUMN VDA_V_MILESTONES.LAST_CHECKED_ON IS 'The timestamp when the milestone achievement was last checked on'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MILESTONE_DESCRIPTION IS 'The milestone description'
/

COMMENT ON COLUMN VDA_V_MILESTONES.FK_BUDGET IS 'The foreign key to the Budget (if the milestone was created from a budget)'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MILESTONE_DATE_FROM IS 'The Start Date for the time-bounded milestones'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MILESTONE_DATE_TO IS 'The To or End Date for the time-bounded milestones'
/

COMMENT ON COLUMN VDA_V_MILESTONES.MILESTONE_HOLDBACK IS 'The Holdback amount for the milestone'
/

/* Formatted on 3/18/2014 2:39:10 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_AUDIT_REPDOWNLOAD_LOG  (View)
--
--  Dependencies:
--   ER_USER (Table)
--   ER_USERREPORTLOG (Table)
--

CREATE OR REPLACE FORCE VIEW VDA_V_AUDIT_REPDOWNLOAD_LOG
(
   USER_NAME,
   USR_RPT_LOG_REP_NAME,
   USR_RPT_LOG_TABLE_NAME,
   IP_ADD,
   USR_RPT_LOG_URL,
   USR_RPT_LOG_URLPARAM,
   USR_RPT_LOG_ACCESSTIME,
   USR_RPT_LOG_MODNAME,
   USR_RPT_LOG_FILENAME,
   USR_RPT_LOG_FORMAT,
   USR_RPT_LOG_DOWNLOAD_FLAG
)
AS
   SELECT   (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = FK_USER)
               USER_NAME,
            USR_RPT_LOG_REP_NAME,
            USR_RPT_LOG_TABLE_NAME,
            IP_ADD,
            USR_RPT_LOG_URL,
            USR_RPT_LOG_URLPARAM,
            USR_RPT_LOG_ACCESSTIME,
            USR_RPT_LOG_MODNAME,
            USR_RPT_LOG_FILENAME,
            DECODE (USR_RPT_LOG_FORMAT,
                    NULL, 'Screen Output',
                    'null', 'Screen Output',
                    USR_RPT_LOG_FORMAT)
               USR_RPT_LOG_FORMAT,
            decode(USR_RPT_LOG_DOWNLOAD_FLAG,0,'No','Yes') USR_RPT_LOG_DOWNLOAD_FLAG
     FROM   eres.ER_USERREPORTLOG
/
COMMENT ON TABLE VDA_V_AUDIT_REPDOWNLOAD_LOG IS 'This view provides access to the audit log generated based on user activity on executing reports and/or file downloads.'
/

COMMENT ON COLUMN VDA_V_AUDIT_REPDOWNLOAD_LOG.USER_NAME IS 'The name of the user who accessed a report or downloaded a file'
/

COMMENT ON COLUMN VDA_V_AUDIT_REPDOWNLOAD_LOG.USR_RPT_LOG_REP_NAME IS 'The name of the report or file'
/

COMMENT ON COLUMN VDA_V_AUDIT_REPDOWNLOAD_LOG.USR_RPT_LOG_TABLE_NAME IS 'The source table name'
/

COMMENT ON COLUMN VDA_V_AUDIT_REPDOWNLOAD_LOG.IP_ADD IS 'The IP ADDRESS of the client machine'
/

COMMENT ON COLUMN VDA_V_AUDIT_REPDOWNLOAD_LOG.USR_RPT_LOG_URL IS 'The URL accessed in the Velos eResearch application'
/

COMMENT ON COLUMN VDA_V_AUDIT_REPDOWNLOAD_LOG.USR_RPT_LOG_URLPARAM IS 'The parameters passed to the URL accessed in the Velos eResearch application'
/

COMMENT ON COLUMN VDA_V_AUDIT_REPDOWNLOAD_LOG.USR_RPT_LOG_ACCESSTIME IS 'The time of report access or file download'
/

COMMENT ON COLUMN VDA_V_AUDIT_REPDOWNLOAD_LOG.USR_RPT_LOG_MODNAME IS 'The module name'
/

COMMENT ON COLUMN VDA_V_AUDIT_REPDOWNLOAD_LOG.USR_RPT_LOG_FILENAME IS 'The downloaded file''s name or temporary report file generated'
/

COMMENT ON COLUMN VDA_V_AUDIT_REPDOWNLOAD_LOG.USR_RPT_LOG_FORMAT IS 'The file or report output format'
/

COMMENT ON COLUMN VDA_V_AUDIT_REPDOWNLOAD_LOG.USR_RPT_LOG_DOWNLOAD_FLAG IS 'Flad indicating file download'
/

/* Formatted on 3/11/2014 3:19:43 PM (QP5 v5.115.810.9015) */
--
-- VDA_V_STUDY_SUMMARY  (View)
--
--  Dependencies:
--   F_GETDIS_SITE (Function)
--   F_GET_CODELSTDESC (Function)
--   F_GET_YESNO (Function)
--   ER_STUDY (Table)
--   ER_USER (Table)
--

CREATE OR REPLACE FORCE VIEW VDA_V_STUDY_SUMMARY
(
   PK_STUDY,
   STUDY_NUMBER,
   STUDY_ENTERED_BY,
   STUDY_PI,
   STUDY_COORDINATOR,
   STUDY_MAJ_AUTH,
   STUDY_TITLE,
   STUDY_OBJECTIVE,
   STUDY_SUMMARY,
   STUDY_AGENT_DEVICE,
   STUDY_DIVISION,
   STUDY_TAREA,
   STUDY_DISEASE_SITE,
   STUDY_ICDCODE1,
   STUDY_ICDCODE2,
   STUDY_NATSAMPSIZE,
   STUDY_DURATION,
   STUDY_DURATN_UNIT,
   STUDY_EST_BEGIN_DATE,
   STUDY_PHASE,
   STUDY_RESEARCH_TYPE,
   STUDY_TYPE,
   STUDY_LINKED_TO,
   STUDY_BLINDING,
   STUDY_RANDOM,
   STUDY_SPONSOR,
   SPONSOR_CONTACT,
   STUDY_INFO,
   STUDY_KEYWRDS,
   FK_ACCOUNT,
   CREATED_ON,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATOR,
   SPONSOR_DD,
   STUDY_SPONSORID,
   STUDY_ACTUALDT,
   STUDY_INVIND_FLAG,
   CTRP_ACCRREP_LASTRUN_DATE,  
   CTRP_ACCRUAL_GEN_DATE, 
   CTRP_CUTOFF_DATE, 
   CTRP_LAST_DOWNLOAD_BY, 
   NCI_TRIAL_IDENTIFIER,  
   NCT_NUMBER
)
AS
   SELECT   pk_study,
            o.STUDY_NUMBER,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = o.FK_AUTHOR)
               STUDY_ENTERED_BY,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = o.STUDY_PRINV)
               STUDY_PI,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   ER_USER.PK_USER = o.STUDY_COORDINATOR)
               STUDY_COORDINATOR,
            ERES.F_GEt_Yesno (o.STUDY_MAJ_AUTH) AS STUDY_MAJ_AUTH,
            STUDY_TITLE,
            STUDY_OBJ_CLOB STUDY_OBJECTIVE,
            STUDY_SUM_CLOB STUDY_SUMMARY,
            STUDY_PRODNAME STUDY_AGENT_DEVICE,
            ERES.F_GEt_Codelstdesc (o.STUDY_DIVISION) STUDY_DIVISION,
            ERES.F_GEt_Codelstdesc (o.FK_CODELST_TAREA) STUDY_TAREA,
            ERES.F_GEtdis_Site (o.STUDY_DISEASE_SITE) STUDY_DISEASE_SITE,
            STUDY_ICDCODE1,
            STUDY_ICDCODE2,
            STUDY_NSAMPLSIZE STUDY_NATSAMPSIZE,
            STUDY_DUR STUDY_DURATION,
            STUDY_DURUNIT STUDY_DURATN_UNIT,
            STUDY_ESTBEGINDT STUDY_EST_BEGIN_DATE,
            ERES.F_GEt_Codelstdesc (o.FK_CODELST_PHASE) STUDY_PHASE,
            ERES.F_GEt_Codelstdesc (o.FK_CODELST_RESTYPE) STUDY_RESEARCH_TYPE,
            ERES.F_GEt_Codelstdesc (o.FK_CODELST_TYPE) STUDY_TYPE,
            DECODE (o.STUDY_ASSOC,
                    NULL, NULL,
                    (SELECT   xx.study_number
                       FROM   eres.er_study xx
                      WHERE   xx.pk_study = o.STUDY_ASSOC))
               STUDY_LINKED_TO,
            ERES.F_GEt_Codelstdesc (o.FK_CODELST_BLIND) STUDY_BLINDING,
            ERES.F_GEt_Codelstdesc (o.FK_CODELST_RANDOM) STUDY_RANDOM,
            STUDY_SPONSOR,
            STUDY_CONTACT SPONSOR_CONTACT,
            STUDY_INFO,
            STUDY_KEYWRDS,
            FK_ACCOUNT,
            CREATED_ON,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = o.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            o.LAST_MODIFIED_DATE,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = o.CREATOR)
               CREATIOR,
            ERES.F_GEt_Codelstdesc (FK_CODELST_SPONSOR) SPONSOR_DD,
            STUDY_SPONSORID,
            STUDY_ACTUALDT,
            ERES.F_GEt_Yesno (o.STUDY_INVIND_FLAG) STUDY_INVIND_FLAG,
		   CTRP_ACCRREP_LASTRUN_DATE,  
		   CTRP_ACCRUAL_GEN_DATE, 
		   CTRP_CUTOFF_DATE, 
		    (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = o.CTRP_LAST_DOWNLOAD_BY) CTRP_LAST_DOWNLOAD_BY, 
		   NCI_TRIAL_IDENTIFIER,  
		   NCT_NUMBER
     FROM   eres.ER_STUDY o
/
COMMENT ON TABLE VDA_V_STUDY_SUMMARY IS 'This view provides access to the study summary record'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.PK_STUDY IS 'The Primary Key of the study'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_NUMBER IS 'The Study Number of the study the payment  is linked with'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_ENTERED_BY IS 'The user who entered the study in the application'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_PI IS 'The Principal Investigator of the study The study coordinator (as documented in the study summary)'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_COORDINATOR IS 'The study coordinator (as documented in the study summary)'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_MAJ_AUTH IS ' Is the selected Principal investigator is the major author of the study'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_TITLE IS 'The Study title'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_OBJECTIVE IS 'The Study Objective'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_SUMMARY IS 'The Study Summary'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_AGENT_DEVICE IS 'Agent or Device'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_DIVISION IS 'Study division'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_TAREA IS 'Study Therapeutic Area'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_DISEASE_SITE IS 'Study Disease Site'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_ICDCODE1 IS 'ICD code 1 linked with the study'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_ICDCODE2 IS 'ICD code 2 linked with the study'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_NATSAMPSIZE IS 'Study National Sample Size'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_DURATION IS 'Estimated duration of the study'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_DURATN_UNIT IS 'Estimated duration unit of the study'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_EST_BEGIN_DATE IS 'Estimated begin date of the study'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_PHASE IS 'Study Phase'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_RESEARCH_TYPE IS 'Study Research Type'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_TYPE IS 'Study Type'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_LINKED_TO IS 'Study linked with another study'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_BLINDING IS 'Study Blinding'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_RANDOM IS 'Study Randomization'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_SPONSOR IS 'Study Sponsor Name'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.SPONSOR_CONTACT IS 'Study Sponsor Contact'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_INFO IS 'Study sponsor information'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_KEYWRDS IS 'Study keywords'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.FK_ACCOUNT IS 'The account study is linked with'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.CREATED_ON IS 'The date the record was created on (Audit)'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.LAST_MODIFIED_DATE IS 'The date the  record was last modified on (Audit)'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.SPONSOR_DD IS 'Study Sponsor'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_SPONSORID IS 'The Sponsor Id for Sponsor Other Information'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_ACTUALDT IS 'Study Actual Begin date'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.STUDY_INVIND_FLAG IS 'Study INV IND Flag'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.CTRP_ACCRREP_LASTRUN_DATE IS 'Date when last CTRP accrual was run'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.CTRP_ACCRUAL_GEN_DATE IS 'The Last Date of the CTRP Accrual download'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.CTRP_CUTOFF_DATE IS 'The Cutoff Date of the CTRP Accrual download'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.CTRP_LAST_DOWNLOAD_BY IS 'The user who last downloaded the CTRP Accrual report'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.NCI_TRIAL_IDENTIFIER IS 'The NCI Trial Identifier for the Study'
/

COMMENT ON COLUMN VDA_V_STUDY_SUMMARY.NCT_NUMBER IS 'The NCT Number for the Study'
/

CREATE OR REPLACE FORCE VIEW VDA_V_STUDYSTATUS_TIMELINE
(
   STUDY_NUMBER,
   PK_STUDY,
   STUDY_TITLE,
   FIRST_NOT_ACTIVE,
   FIRST_IRB_APPROVED,
   FIRST_ACTIVE,
   LAST_PERM_CLOSURE
)
AS
   SELECT   study_number,
            pk_study,
            study_title,
            (SELECT   MIN (studystat_date)
               FROM   eres.er_studystat i
              WHERE   i.fk_study = o.pk_study
                      AND fk_codelst_studystat =
                            (SELECT   pk_codelst
                               FROM   eres.er_codelst
                              WHERE   codelst_type = 'studystat'
                                      AND codelst_subtyp = 'not_active'))
               first_not_active,
            (SELECT   MIN (studystat_date)
               FROM   eres.er_studystat i
              WHERE   i.fk_study = o.pk_study
                      AND fk_codelst_studystat =
                            (SELECT   pk_codelst
                               FROM   eres.er_codelst
                              WHERE   codelst_type = 'studystat'
                                      AND codelst_subtyp = 'app_CHR'))
               first_irb_approved,
            (SELECT   MIN (studystat_date)
               FROM   eres.er_studystat i
              WHERE   i.fk_study = o.pk_study
                      AND fk_codelst_studystat =
                            (SELECT   pk_codelst
                               FROM   eres.er_codelst
                              WHERE   codelst_type = 'studystat'
                                      AND codelst_subtyp = 'active'))
               first_active,
            (SELECT   MAX (studystat_date)
               FROM   eres.er_studystat i
              WHERE   i.fk_study = o.pk_study
                      AND fk_codelst_studystat =
                            (SELECT   pk_codelst
                               FROM   eres.er_codelst
                              WHERE   codelst_type = 'studystat'
                                      AND codelst_subtyp = 'prmnt_cls'))
               last_perm_closure
     FROM   eres.er_study o
/
COMMENT ON TABLE VDA_V_STUDYSTATUS_TIMELINE IS 'This view provides access to the study status timeline for predefined trial statuses'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_TIMELINE.STUDY_NUMBER IS 'The Study Number'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_TIMELINE.PK_STUDY IS 'The PK of the study'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_TIMELINE.STUDY_TITLE IS 'The Study Title'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_TIMELINE.FIRST_NOT_ACTIVE IS 'The date study was initiated-first ''Not Active'' or an equivalent status'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_TIMELINE.FIRST_IRB_APPROVED IS 'The date study was IRB Approved-first ''IRB Approved'' or an equivalent status'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_TIMELINE.FIRST_ACTIVE IS 'The date study was Active or Enrolling  -first ''Active/Enrolling'' or an equivalent status'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_TIMELINE.LAST_PERM_CLOSURE IS 'The date study was Closed -last ''Closed or Retired'' or an equivalent status'
/

CREATE OR REPLACE FORCE VIEW VDA_V_STUDYSTATUS_METRICS
(
   STUDY_NUMBER,
   PK_STUDY,
   STUDY_TITLE,
   FIRST_NOT_ACTIVE,
   FIRST_IRB_APPROVED,
   FIRST_ACTIVE,
   LAST_PERM_CLOSURE,
   DAYS_IRB_APPROVAL,
   DAYS_NOTACTIVE_TO_ACTIVE,
   DAYS_NOTACTIVE_TO_CLOSURE
)
AS
   SELECT   x."STUDY_NUMBER",
            x."PK_STUDY",
            x."STUDY_TITLE",
            x."FIRST_NOT_ACTIVE",
            x."FIRST_IRB_APPROVED",
            x."FIRST_ACTIVE",
            x."LAST_PERM_CLOSURE",
            (first_irb_approved - first_not_active) days_irb_approval,
            (first_active - first_not_active) days_notactive_to_active,
            (last_perm_closure - first_not_active) days_notactive_to_closure
     FROM   VDA_V_STUDYSTATUS_TIMELINE x
/

COMMENT ON TABLE VDA_V_STUDYSTATUS_METRICS IS 'This view provides access to the study status timeline metrics for predefined trial statuses'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_METRICS.STUDY_NUMBER IS 'The Study Number'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_METRICS.PK_STUDY IS 'The PK of the study'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_METRICS.STUDY_TITLE IS 'The Study Title'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_METRICS.FIRST_NOT_ACTIVE IS 'The date study was initiated-first ''Not Active'' or an equivalent status'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_METRICS.FIRST_IRB_APPROVED IS 'The date study was IRB Approved-first ''IRB Approved'' or an equivalent status'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_METRICS.FIRST_ACTIVE IS 'The date study was Active or Enrolling  -first ''Active/Enrolling'' or an equivalent status'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_METRICS.LAST_PERM_CLOSURE IS 'The date study was Closed -last ''Closed or Retired'' or an equivalent status'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_METRICS.DAYS_IRB_APPROVAL IS 'Number of days it took from study initiation to IRB Approval'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_METRICS.DAYS_NOTACTIVE_TO_ACTIVE IS 'Number of days it took from study initiation to Study Active/Enrolling'
/

COMMENT ON COLUMN VDA_V_STUDYSTATUS_METRICS.DAYS_NOTACTIVE_TO_CLOSURE IS 'Number of days it took from study initiation to Study Closure/Completion'
/



INSERT INTO VDA_TRACK_PATCHES
(
  PATCH_PK       ,
  DB_VER_MJR     ,
  DB_VER_MNR     ,
  DB_PATCH_NAME  ,
  FIRED_ON       ,
  APP_VERSION    
) VALUES(SEQ_VDA_TRACK_PATCHES.nextval,5,1,'01_views.sql',sysdate,'VDA 1.4 Build008')
/

commit
/ 
