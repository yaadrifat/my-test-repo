package com.velos.integration.adt.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;

import com.velos.integration.adt.bean.PatientBean;
import com.velos.integration.adt.bean.PatientSearchBean;
import com.velos.integration.adt.exception.AdtException;
import com.velos.integration.adt.mapping.PatientSearchMappingHelper;
import com.velos.integration.adt.util.Utility;

public class PatientDaoImpl {
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplate;

	private static Logger logger = Logger.getLogger(PatientDaoImpl.class);

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	public boolean createPatientRecord(PatientBean patient) throws Exception {
		try {

			String sql = "INSERT INTO PATIENT_EMR "
					+ "(PK_PEMR, ADDL_ETHNICITY, ADDL_RACE, ADDRESS1,"
					+ " ADDRESS2, WORK_PHONE, CITY, COUNTRY,"
					+ " COUNTY, DOB, DEATH_DATE, ETHNICITY,"
					+ " FIRSTNAME, GENDER, HOME_PHONE, LASTNAME,"
					+ " MARITAL_STATUS, MIDDLENAME, PERSON_CODE, SURVIVAL_STATUS,"
					+ " RACE, SSN, STATE, ZIPCODE, EVENT_TYPE)"
					+ " VALUES (SEQ_PATIENT_EMR.nextval, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

			jdbcTemplate = new JdbcTemplate(dataSource);

			jdbcTemplate.update(
					sql,
					new Object[] {
							patient.getAdditionalEthnicity(),
							patient.getAdditionalRace(),
							patient.getAddress1(),
							patient.getAddress2(),
							patient.getwPhoneNumber(),
							patient.getCity(),
							patient.getCountry(),
							patient.getCounty(),
							new java.sql.Date(patient.getDateOfBirth()
									.getTime()),
							patient.getDeathDate() == null ? null
									: new java.sql.Date(patient.getDeathDate()
											.getTime()),
							patient.getEthnicity(), patient.getFirstName(),
							patient.getGender(), patient.gethPhoneNumber(),
							patient.getLastName(), patient.getMaritalStatus(),
							patient.getMiddleName(), patient.getPatientID(),
							patient.getSurvivalStatus(), patient.getRace(),
							patient.getSsn(), patient.getState(),
							patient.getZipcode(), patient.getEventType() });
			return true;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new AdtException("Exception while creating Patient Demographic Record.");
		}
		
	}

	/*public boolean patientRecordExist(String patientId) {
		String sql = "SELECT count(*) FROM PATIENT_EMR WHERE PERSON_CODE = "
				+ patientId;
		jdbcTemplate = new JdbcTemplate(dataSource);

		int rowCount = jdbcTemplate.queryForInt(sql);
		if (rowCount == 0)
			return false;
		else
			return true;
	}

	public boolean updatePatientRecord(PatientBean patient) throws Exception {
		try {

			String sql = "UPDATE PATIENT_EMR SET "
					+ " ADDL_ETHNICITY = ?, ADDL_RACE = ?, ADDRESS1 = ?,"
					+ " ADDRESS2 = ?, WORK_PHONE = ?, CITY = ?, COUNTRY = ?,"
					+ " COUNTY = ?, DOB = ?, DEATH_DATE = ?, ETHNICITY = ?,"
					+ " FIRSTNAME = ?, GENDER = ?, HOME_PHONE = ?, LASTNAME = ?,"
					+ " MARITAL_STATUS = ?, MIDDLENAME = ?, SURVIVAL_STATUS = ?,"
					+ " RACE = ?, SSN = ?, STATE = ?, ZIPCODE = ?"
					+ " WHERE PERSON_CODE = ?";

			jdbcTemplate = new JdbcTemplate(dataSource);

			jdbcTemplate.update(
					sql,
					new Object[] {
							patient.getAdditionalEthnicity(),
							patient.getAdditionalRace(),
							patient.getAddress1(),
							patient.getAddress2(),
							patient.getwPhoneNumber(),
							patient.getCity(),
							patient.getCountry(),
							patient.getCounty(),
							new java.sql.Date(patient.getDateOfBirth()
									.getTime()),
							patient.getDeathDate() == null ? null
									: new java.sql.Date(patient.getDeathDate()
											.getTime()),
							patient.getEthnicity(), patient.getFirstName(),
							patient.getGender(), patient.gethPhoneNumber(),
							patient.getLastName(), patient.getMaritalStatus(),
							patient.getMiddleName(),
							patient.getSurvivalStatus(), patient.getRace(),
							patient.getSsn(), patient.getState(),
							patient.getZipcode(), patient.getPatientID() });
			return true;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new AdtException("Exception while updating Patient Demographic Record.");
		}
	}
	
	public List<PatientBean> getPatientRecords(
			PatientSearchBean patientSearchBean) {
		List<PatientBean> patientRecords = new ArrayList<PatientBean>();
		try {
			StringBuilder whereClause = new StringBuilder();
			PatientSearchMappingHelper patientSearchMapper = new PatientSearchMappingHelper();

			String ageFilter = patientSearchMapper
					.getAgePeriodFilter(patientSearchBean.getAgePeriod());
			if (!Utility.isEmpty(ageFilter)) {
				whereClause.append(ageFilter);
			}

			String nameFilter = patientSearchMapper
					.getNameFilter(patientSearchBean.getPatientName());
			if (!Utility.isEmpty(nameFilter)) {
				whereClause.append(nameFilter);
			}

			String survivalStatFilter = patientSearchMapper
					.getSurvivalStatusFilter(patientSearchBean
							.getSurvivalStatus());
			if (!Utility.isEmpty(survivalStatFilter)) {
				whereClause.append(survivalStatFilter);
			}

			String genderFilter = patientSearchMapper
					.getGenderFilter(patientSearchBean.getGender());
			if (!Utility.isEmpty(genderFilter)) {
				whereClause.append(genderFilter);
			}

			String patientCodeFilter = patientSearchMapper
					.getPatientCodeFilter(patientSearchBean.getPatientID());
			if (!Utility.isEmpty(patientCodeFilter)) {
				whereClause.append(patientCodeFilter);
			}

			String whereCondition = "";
			// Trim the last AND
			if (!Utility.isEmpty(whereClause.toString())) {
				whereClause.insert(0, "WHERE");
				whereCondition = whereClause.substring(0,
						whereClause.lastIndexOf(" "));
			}

			String sql = "SELECT ADDL_ETHNICITY, ADDL_RACE, ADDRESS1,"
					+ " ADDRESS2, WORK_PHONE, CITY, COUNTRY,"
					+ " COUNTY, DOB, DEATH_DATE, ETHNICITY,"
					+ " FIRSTNAME, GENDER, HOME_PHONE, LASTNAME,"
					+ " MARITAL_STATUS, MIDDLENAME, PERSON_CODE, SURVIVAL_STATUS,"
					+ " RACE, SSN, STATE " + " FROM PATIENT_EMR  "
					+ whereCondition;

			System.out.println("Sql : " + sql);

			jdbcTemplate = new JdbcTemplate(dataSource);
			List<Map> rows = jdbcTemplate.queryForList(sql);
			for (Map row : rows) {
				PatientBean patient = new PatientBean();
				patient.setAdditionalEthnicity((String) row
						.get("ADDL_ETHNICITY"));
				patient.setAdditionalRace((String) row.get("ADDL_RACE"));
				patient.setAddress1((String) row.get("ADDRESS1"));
				patient.setAddress2((String) row.get("ADDRESS2"));
				patient.setwPhoneNumber((String) row.get("WORK_PHONE"));
				patient.setCity((String) row.get("CITY"));
				patient.setCountry((String) row.get("COUNTRY"));
				patient.setCounty((String) row.get("COUNTY"));
				patient.setDateOfBirth((java.util.Date) row.get("DOB"));
				patient.setDeathDate((java.util.Date) row.get("DEATH_DATE"));
				patient.setEthnicity((String) row.get("ETHNICITY"));
				patient.setFirstName((String) row.get("FIRSTNAME"));
				patient.setGender((String) row.get("GENDER"));
				patient.sethPhoneNumber((String) row.get("HOME_PHONE"));
				patient.setLastName((String) row.get("LASTNAME"));
				patient.setMaritalStatus((String) row.get("MARITAL_STATUS"));
				patient.setMiddleName((String) row.get("MIDDLENAME"));
				patient.setSurvivalStatus((String) row.get("SURVIVAL_STATUS"));
				patient.setRace((String) row.get("RACE"));
				patient.setSsn((String) row.get("SSN"));
				patient.setState((String) row.get("STATE"));
				patient.setZipcode((String) row.get("ZIPCODE"));
				patient.setPatientID((String) row.get("PERSON_CODE"));
				patientRecords.add(patient);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return patientRecords;
	}*/
}
