package com.velos.integration.core.processor;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import com.velos.services.ObjectInfo;
import com.velos.services.OperationException_Exception;
import com.velos.services.SimpleIdentifier;

@WebService(targetNamespace = "http://velos.com/services/", name = "FormSEI")
public interface VelosEspPatientStudyFormDesignEndpoint {
	@WebResult(name = "FormDesign", targetNamespace = "")
@RequestWrapper(localName = "getStudyPatientFormDesign", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetStudyPatientFormDesign")
@WebMethod
@ResponseWrapper(localName = "getStudyPatientFormDesignResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetStudyPatientFormDesignResponse")
public com.velos.services.StudyPatientFormDesign getStudyPatientFormDesign(
    @WebParam(name = "FormIdentifier", targetNamespace = "")
    com.velos.services.FormIdentifier formIdentifier,
    @WebParam(name = "StudyIdentifier", targetNamespace = "")
    com.velos.services.StudyIdentifier studyIdentifier,
    @WebParam(name = "FormName", targetNamespace = "")
    java.lang.String formName,
    @WebParam(name = "IncludeFormatting", targetNamespace = "")
    boolean includeFormatting
) throws OperationException_Exception;
}
