package com.velos.integration.charges.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.support.lob.LobHandler;

import com.velos.integration.charges.bean.ChargesBean;
import com.velos.integration.charges.bean.SaveChargeFileBean;

public class ChargesDaoImpl {
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplate;
	static int num = 10000;
	private static Logger logger = Logger.getLogger(ChargesDaoImpl.class);

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public String listFilesForFolder(final File folder) {
		for (final File fileEntry : folder.listFiles()) {
			if (fileEntry.isDirectory()) {
				listFilesForFolder(fileEntry);
			} else {
				return fileEntry.getName();
			}
		}
		return "";
	}

	public static java.util.Date stringToDate(String dtString) {
		String format = "yyyyMMddHHmm";

		java.util.Date dt = null;
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		try {
			dt = sdf.parse(dtString);
		} catch (Exception e) {
		}
		return dt;
	}

	public int createSequenceForChargeFiles() {
		int sequence = 0;
		try {
			String sqlSqnc = "SELECT SEQ_SAVE_CHARGE_FILES.nextval from dual";
			PreparedStatement pstmtSqnc = null;
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			sequence = jdbcTemplate.queryForInt(sqlSqnc);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return sequence;
	}

	public boolean saveChargeFile(final SaveChargeFileBean saveChargeFileBean) {
		boolean isSuccess = false;
		try {
			PreparedStatement pstmt = null;
			final File file = saveChargeFileBean.getFile();
			final Date date = new Date();
			final byte[] bFile = new byte[(int) file.length()];

			FileInputStream fileInputStream;
			fileInputStream = new FileInputStream(file);
			fileInputStream.read(bFile);
			fileInputStream.close();

			String fileNameWithoutExtension = file.getName().replaceFirst(
					"[.][^.]+$", "");

			final InputStream clobIs = new FileInputStream(file);
			final InputStreamReader clobReader = new InputStreamReader(clobIs);
			try {

				final LobHandler lobHandler = saveChargeFileBean
						.getLobHandler();
				final String fileIdFinal = file.getName() + "_"
						+ saveChargeFileBean.getBatchDateString() + "_"
						+ saveChargeFileBean.getSequence();

				saveChargeFileBean.setSystemFileID(fileIdFinal);
				saveChargeFileBean.setProcessDate(date);
				saveChargeFileBean.setFileSize(file.length() + " bytes");
				String sql = "INSERT INTO SAVE_CHARGE_FILES "
						+ "(PK_IPF,FILENAME, BATCHDATE, PROCESS_START_DATE, TOTALCHARGES, FILEID,"
						+ " FILE_CONTENT,  FILE_SRC, FILE_TYPE, FILE_HEADER, FILE_FOOTER)"
						+ " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
				jdbcTemplate = new JdbcTemplate(dataSource);
				jdbcTemplate.update(sql, new PreparedStatementSetter() {
					public void setValues(PreparedStatement ps)
							throws SQLException {
						ps.setInt(1, saveChargeFileBean.getSequence());
						ps.setString(2, file.getName());
						ps.setDate(3, new java.sql.Date(saveChargeFileBean
								.getBatchDate().getTime()));
						ps.setTimestamp(4, new Timestamp(date.getTime()));
						ps.setInt(5, saveChargeFileBean.getTotalCharges());
						ps.setString(6, fileIdFinal);
						lobHandler.getLobCreator().setClobAsCharacterStream(ps,
								7, clobReader, (int) file.length());
						lobHandler.getLobCreator().setBlobAsBytes(ps, 8, bFile);
						ps.setString(9, saveChargeFileBean.getFileType());
						ps.setString(10, saveChargeFileBean.getFileHeader());
						ps.setString(11, saveChargeFileBean.getFileFooter());
					}
				});
				isSuccess = true;
			} catch (Exception e) {
				e.printStackTrace();
				isSuccess = false;
			} finally {
				try {
					if (pstmt != null)
						pstmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			isSuccess = false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			isSuccess = false;
		}

		return isSuccess;
	}

	public boolean createCharge(ChargesBean chargeBean) {
		PreparedStatement pstmtSqnc = null;
		try {
			String sqlSqnc = "SELECT SEQ_MDA_DI_PATH_CHARGES.nextval from dual";

			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			final int sequence = jdbcTemplate.queryForInt(sqlSqnc);

			String sql = "INSERT INTO MDA_DI_PATH_CHARGES "
					+ "(RECORD_ID, PATIENT_FIRST_NAME, PATIENT_LAST_NAME,"
					+ " PATIENT_MRN_CHARGES, SERVICE_DESCRIPTION, SERVICE_CODE, CPT_CODE,"
					+ " RESEARCH_MODIFIER_1, RESEARCH_MODIFIER_2, RESEARCH_MODIFIER_3, SERVICE_DATE,"
					+ " BATCH_DATE, ANCILLARY_DEPARTMENT, INPUT_FILE_NAME, INPUT_FILE_ID, UNIQUE_CHARGE_ID, INPUT_FILE_CHARGE_ROW)"
					+ " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?)";

			jdbcTemplate = new JdbcTemplate(dataSource);

			jdbcTemplate.update(
					sql,
					new Object[] { sequence, chargeBean.getLastName(),
							chargeBean.getFirstName(), chargeBean.getMrn(),
							chargeBean.getServiceDescription(),
							chargeBean.getServiceCode(),
							chargeBean.getCptCode(),
							chargeBean.getResearchModifier1(),
							chargeBean.getResearchModifier2(),
							chargeBean.getResearchModifier3(),
							chargeBean.getServiceDate(),
							chargeBean.getBatchDate(),
							chargeBean.getDepartment(),
							chargeBean.getFileName(), chargeBean.getFileId(),
							sequence, chargeBean.getChargeRow() });
			return true;
		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			if (pstmtSqnc != null)
				try {
					pstmtSqnc.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		return false;

	}

	public List<String> getOutPutFiles(String fileType) {
		int erCode = 0;
		List<Map> rows = null;
		List<String> outputFiles = new ArrayList<String>();
		String sql = "select FILENAME from SAVE_CHARGE_FILES where FILE_TYPE = ? and PROCESS_COMPLETE_DATE is null";
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		try {
			rows = jdbcTemplate.queryForList(sql, new Object[] { fileType });
			for(Map row : rows)
			{
				outputFiles.add((String)row.get("FILENAME"));
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return outputFiles;
	}

	public void updateOutPutFiles(SaveChargeFileBean saveChargeFileBean) {
		List<Map> rows = null;

		try {
			String sql = "UPDATE SAVE_CHARGE_FILES SET "
					+ " FILE_CONTENT = ?,  FILE_SRC = ?, PROCESS_COMPLETE_DATE = SYSDATE WHERE FILENAME = ?";

			final File file = saveChargeFileBean.getFile();
			final Date date = new Date();
			final byte[] bFile = new byte[(int) file.length()];

			FileInputStream fileInputStream;
			fileInputStream = new FileInputStream(file);
			fileInputStream.read(bFile);
			fileInputStream.close();

			final InputStream clobIs = new FileInputStream(file);
			final InputStreamReader clobReader = new InputStreamReader(clobIs);
			try {

				final LobHandler lobHandler = saveChargeFileBean
						.getLobHandler();
				final String fileIdFinal = file.getName() + "_"
						+ saveChargeFileBean.getBatchDateString() + "_"
						+ saveChargeFileBean.getSequence();

				jdbcTemplate = new JdbcTemplate(dataSource);
				jdbcTemplate.update(sql, new PreparedStatementSetter() {
					public void setValues(PreparedStatement ps)
							throws SQLException {
						lobHandler.getLobCreator().setClobAsCharacterStream(ps,
								1, clobReader, (int) file.length());
						lobHandler.getLobCreator().setBlobAsBytes(ps, 2, bFile);
						ps.setString(3, file.getName());
					}
				});

			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}
}
