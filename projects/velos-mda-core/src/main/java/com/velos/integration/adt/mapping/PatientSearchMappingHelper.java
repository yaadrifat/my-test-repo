package com.velos.integration.adt.mapping;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.velos.integration.adt.util.Utility;

public class PatientSearchMappingHelper {

	public String getAgePeriodFilter(String agePeriod) {
		String minDate = "";
		String maxDate = "";
		String ageFilter = "";
		Format formatter = new SimpleDateFormat("yyyy/MM/dd");
		Date date1 = new java.util.Date();
		Date date2 = new java.util.Date();

		if (!Utility.isEmpty(agePeriod)) {
			if (agePeriod.equals("0,15")) {

				date1.setYear(date1.getYear() - 0);
				date2.setYear(date2.getYear() - 16);
				maxDate = formatter.format(date1);
				minDate = formatter.format(date2);
				ageFilter = "  (((to_char(DOB,'YYYY/MM/DD')) > '"
						+ minDate
						+ "'  and (to_char(DOB,'YYYY/MM/DD')) <='"
						+ maxDate
						+ "'  AND DEATH_DATE IS NULL) or  trunc(months_between(DEATH_DATE,DOB))/12 between 0 and 15.99) AND";

			} else if (agePeriod.equals("16,30")) {
				date1.setYear(date1.getYear() - 16);
				date2.setYear(date2.getYear() - 31);
				maxDate = formatter.format(date1);
				minDate = formatter.format(date2);

				ageFilter = "  (((to_char(DOB,'YYYY/MM/DD')) > '"
						+ minDate
						+ "'  and (to_char(DOB,'YYYY/MM/DD')) <='"
						+ maxDate
						+ "'  AND DEATH_DATE IS NULL)  or trunc(months_between(DEATH_DATE,DOB))/12 between 16 and 30.99) AND";
			} else if (agePeriod.equals("31,45")) {

				date1.setYear(date1.getYear() - 31);
				date2.setYear(date2.getYear() - 46);
				maxDate = formatter.format(date1);
				minDate = formatter.format(date2);

				ageFilter = "  (((to_char(DOB,'YYYY/MM/DD')) > '"
						+ minDate
						+ "'  and (to_char(DOB,'YYYY/MM/DD')) <='"
						+ maxDate
						+ "'  AND DEATH_DATE IS NULL) or   trunc(months_between(DEATH_DATE,DOB))/12 between 31 and 45.99) AND";
			} else if (agePeriod.equals("46,60")) {

				date1.setYear(date1.getYear() - 46);
				date2.setYear(date2.getYear() - 61);
				maxDate = formatter.format(date1);
				minDate = formatter.format(date2);

				ageFilter = "  (((to_char(DOB,'YYYY/MM/DD')) > '"
						+ minDate
						+ "'  and (to_char(DOB,'YYYY/MM/DD')) <='"
						+ maxDate
						+ "'  AND DEATH_DATE IS NULL) or  trunc(months_between(DEATH_DATE,DOB))/12 between 46 and 60.99) AND";

			} else if (agePeriod.equals("61,75")) {
				date1.setYear(date1.getYear() - 61);
				date2.setYear(date2.getYear() - 76);
				maxDate = formatter.format(date1);
				minDate = formatter.format(date2);

				ageFilter = "  (((to_char(DOB,'YYYY/MM/DD')) > '"
						+ minDate
						+ "'  and (to_char(DOB,'YYYY/MM/DD')) <='"
						+ maxDate
						+ "'  AND DEATH_DATE IS NULL) or trunc(months_between(DEATH_DATE,DOB))/12 between 61 and 75.99) AND";
			} else if (agePeriod.equals("76,Over")) {

				date1.setYear(date1.getYear() - 76);
				maxDate = formatter.format(date1);

				ageFilter = " and (((to_char(DOB,'YYYY/MM/DD')) <='"
						+ maxDate
						+ "'  AND DEATH_DATE IS NULL) or   trunc(months_between(DEATH_DATE,DOB))/12 >= 76) AND";

			}
		}
		return ageFilter;
	}

	public String getNameFilter(String patName) {
		StringBuffer nameFilter = new StringBuffer();
		if (!Utility.isEmpty(patName)) {
			String[] names1 = patName.trim().split(" ");
			nameFilter.append("  (  lower( FIRSTNAME) like lower('%"
					+ patName.trim() + "%') or ");
			if (names1 != null && names1.length > 0) {
				nameFilter.append(" lower(FIRSTNAME) like lower('%"
						+ names1[0] + "%')  or ");
			}
			if (names1 != null && names1.length > 0) {
				nameFilter.append(" lower(LASTNAME) like lower('%"
						+ names1[names1.length - 1] + "%')  or ");
			}
			if (names1 != null && names1.length > 1) {
				nameFilter.append(" lower(MIDDLENAME) like lower('%"
						+ names1[1] + "%')  or ");
			}
			nameFilter.append(" lower(LASTNAME) like lower('%"
					+ patName.trim() + "%')  or ");
			nameFilter.append(" lower(MIDDLENAME) like lower('%"
					+ patName.trim() + "%')     ");
			nameFilter.append(" ) AND");
		}
		return nameFilter.toString();
	}
	
	public String getSurvivalStatusFilter(String survivalStat)
	{
		String survivalStatFilter = "";
		if(!Utility.isEmpty(survivalStat))
		{
			survivalStatFilter = " SURVIVAL_STATUS = " + survivalStat + " AND";
		}
		return survivalStatFilter;
	}
	
	public String getGenderFilter(String gender)
	{
		String genderFilter = "";
		if(!Utility.isEmpty(gender))
		{
			genderFilter = " GENDER = " + gender + " AND";
		}
		return genderFilter;
	}
	
	public String getPatientCodeFilter(String patientCode)
	{
		String patientCodeFilter = "";
		if(!Utility.isEmpty(patientCode))
		{ 
			patientCodeFilter = " lower(PERSON_CODE) like lower('%"+patientCode+"%')" + " AND";
		}
		return patientCodeFilter;
	}
}
