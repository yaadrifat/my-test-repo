package com.velos.integration.charges.bean;

import java.io.File;
import java.util.Date;

import org.springframework.jdbc.support.lob.LobHandler;

public class SaveChargeFileBean {
	private int sequence;
	private int totalCharges;
	private String batchDateString;
	private Date batchDate;
	private String batchDateFormat;
	private Date processDate;
	private String systemFileID;
	private String fileSize;
	private File file;
	private String fileName;
	private String fileType;
	private LobHandler lobHandler;
	private String inputMetafilePath;
	private String fileHeader;
	private String fileFooter;
	
	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public int getTotalCharges() {
		return totalCharges;
	}

	public void setTotalCharges(int totalCharges) {
		this.totalCharges = totalCharges;
	}

	public Date getBatchDate() {
		return batchDate;
	}

	public void setBatchDate(Date batchDate) {
		this.batchDate = batchDate;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public LobHandler getLobHandler() {
		return lobHandler;
	}

	public void setLobHandler(LobHandler lobHandler) {
		this.lobHandler = lobHandler;
	}

	public String getBatchDateString() {
		return batchDateString;
	}

	public void setBatchDateString(String batchDateString) {
		this.batchDateString = batchDateString;
	}

	public Date getProcessDate() {
		return processDate;
	}

	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}

	public String getSystemFileID() {
		return systemFileID;
	}

	public void setSystemFileID(String systemFileID) {
		this.systemFileID = systemFileID;
	}

	public String getFileSize() {
		return fileSize;
	}

	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}

	public String getInputMetafilePath() {
		return inputMetafilePath;
	}

	public void setInputMetafilePath(String inputMetafilePath) {
		this.inputMetafilePath = inputMetafilePath;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileHeader() {
		return fileHeader;
	}

	public void setFileHeader(String fileHeader) {
		this.fileHeader = fileHeader;
	}

	public String getFileFooter() {
		return fileFooter;
	}

	public void setFileFooter(String fileFooter) {
		this.fileFooter = fileFooter;
	}

	public String getBatchDateFormat() {
		return batchDateFormat;
	}

	public void setBatchDateFormat(String batchDateFormat) {
		this.batchDateFormat = batchDateFormat;
	}
		
}
