package com.velos.integration.adt.bean;

import com.velos.integration.adt.mapping.AckCodeType;
import com.velos.integration.adt.mapping.AckMessageType;

public class AckMessageBean {
	private AckCodeType ackCode;
	private AckMessageType ackMessage;
	
	public AckCodeType getAckCode() {
		return ackCode;
	}
	public void setAckCode(AckCodeType ackCode) {
		this.ackCode = ackCode;
	}
	public AckMessageType getAckMessage() {
		return ackMessage;
	}
	public void setAckMessage(AckMessageType messagetyperequired) {
		this.ackMessage = messagetyperequired;
	}
}
