package com.velos.integration.adt.mapping;

public enum AckMessageType {
	MessageTypeRequired("Message Type is a mandatory field."),
	EventTypeRequired("Event Type is a mandatory field."),
	MessageIDRequired("Message ID is a mandatory field."),
	PatientIDRequired("MRN(Patient ID) is a mandatory field."),
	FirstNameRequired("First Name is a mandatory field."),
	LastNameRequired("Last Name is a mandatory field."),
	DOBRequired("Date Of Birth is a mandatory field."),
	AckError("Error while processing the message"),
	AckAccept("Acknowledgement Accepted")
	;
	
	private String key;
	
	AckMessageType(String key) {
		this.key = key;
	}
	
	@Override
	public String toString() {
		return key;
	}}
