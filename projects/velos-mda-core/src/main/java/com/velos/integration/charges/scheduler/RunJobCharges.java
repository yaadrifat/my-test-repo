package com.velos.integration.charges.scheduler;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/*
 * Quartz scheduler is used to schedule cron job to update coverage.
 */
public class RunJobCharges extends QuartzJobBean {
	
	private static Logger logger = Logger.getLogger(RunJobCharges.class.getName());
	private RunTaskCharges runTaskCharges;

	public void setRunTaskCharges(RunTaskCharges runTaskCharges) {
		this.runTaskCharges = runTaskCharges;
	}

	@Override
	protected void executeInternal(JobExecutionContext arg0)
			throws JobExecutionException {
		try {
			runTaskCharges.runTask();
		} catch (Exception e) {
			logger.info(" Exception caught in executeInternal method of RunTaskCharges " + e.getMessage());
		}		
	}

}
