package com.velos.integration.core.processor;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import com.velos.services.ObjectInfo;
import com.velos.services.OperationException_Exception;
import com.velos.services.SimpleIdentifier;

@WebService(targetNamespace = "http://velos.com/services/", name = "FormResponseSEI")
public interface VelosEspPatientStudyFormResponseDetailEndpoint {
	@WebResult(name = "StudyPatientFormResponse", targetNamespace = "")
    @RequestWrapper(localName = "getStudyPatientFormResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetStudyPatientFormResponse")
    @WebMethod
    @ResponseWrapper(localName = "getStudyPatientFormResponseResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetStudyPatientFormResponseResponse")
    public com.velos.services.StudyPatientFormResponse getStudyPatientFormResponse(
        @WebParam(name = "FormResponseIdentifier", targetNamespace = "")
        com.velos.services.StudyPatientFormResponseIdentifier formResponseIdentifier
    ) throws OperationException_Exception;
}
