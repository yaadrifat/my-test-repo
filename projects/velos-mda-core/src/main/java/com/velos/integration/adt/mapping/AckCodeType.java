package com.velos.integration.adt.mapping;

public enum AckCodeType {
	AcknowledgementAccept("AA"),
	AcknowledgementReject("AR"),
	AcknowledgementError("AE"),
	AcknowledgementInProcess("AP")
	;
	
	private String key;
	
	AckCodeType(String key) {
		this.key = key;
	}
	
	@Override
	public String toString() {
		return key;
	}}
