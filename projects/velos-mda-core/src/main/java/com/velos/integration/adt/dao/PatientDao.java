package com.velos.integration.adt.dao;

import java.util.ArrayList;
import java.util.List;

import com.velos.integration.adt.bean.PatientBean;

public class PatientDao {
	
	public boolean createPatientRecord(PatientBean patient)
	{
		return true;
	}

	public boolean patientRecordExist(String patientId)
	{
		return true;
	}
	
	public boolean updatePatientRecord(PatientBean patient)
	{
		return true;
	}
	
	public List<PatientBean> getPatientRecords(PatientBean patient)
	{
		return new ArrayList<PatientBean>();
	}
}
