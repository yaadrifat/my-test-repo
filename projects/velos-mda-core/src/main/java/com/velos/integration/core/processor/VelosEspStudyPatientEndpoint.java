package com.velos.integration.core.processor;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import com.velos.services.ObjectInfo;
import com.velos.services.OperationException_Exception;
import com.velos.services.SimpleIdentifier;

@WebService(targetNamespace = "http://velos.com/services/", name = "StudyPatientSEI")
public interface VelosEspStudyPatientEndpoint {
	@WebResult(name = "PatientEnrollmentDetails", targetNamespace = "")
	@RequestWrapper(localName = "getStudyPatientStatus", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetStudyPatientStatus")
	@WebMethod
	@ResponseWrapper(localName = "getStudyPatientStatusResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetStudyPatientStatusResponse")
	public com.velos.services.PatientEnrollmentDetails getStudyPatientStatus(
			@WebParam(name = "PatientStudyStatusIdentifier", targetNamespace = "") com.velos.services.PatientStudyStatusIdentifier patientStudyStatusIdentifier)
			throws OperationException_Exception;
}
