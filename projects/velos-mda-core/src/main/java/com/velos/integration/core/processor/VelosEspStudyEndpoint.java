package com.velos.integration.core.processor;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import com.velos.services.ObjectInfo;
import com.velos.services.OperationException_Exception;
import com.velos.services.SimpleIdentifier;

@WebService(targetNamespace = "http://velos.com/services/", name = "StudySEI")
public interface VelosEspStudyEndpoint {
	@WebResult(name = "StudySummary", targetNamespace = "")
	@RequestWrapper(localName = "getStudySummary", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetStudySummary")
	@WebMethod
	@ResponseWrapper(localName = "getStudySummaryResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetStudySummaryResponse")
	public com.velos.services.StudySummary getStudySummary(
			@WebParam(name = "StudyIdentifier", targetNamespace = "") com.velos.services.StudyIdentifier studyIdentifier)
			throws OperationException_Exception;
}
