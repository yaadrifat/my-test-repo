package com.velos.integration.core.messaging;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.springframework.mail.MailParseException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.apache.log4j.Logger;

public class SendEmail {
	private static Logger logger = Logger.getLogger(SendEmail.class.getName());

	private JavaMailSender mailSender;
	private SimpleMailMessage simpleMailMessage;

	public void setSimpleMailMessage(SimpleMailMessage simpleMailMessage) {
		this.simpleMailMessage = simpleMailMessage;
	}

	public void setMailSender(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}

	public void sendMail(String to, String content, String studyNumber, String subjectMrn) throws IOException {

		MimeMessage message = mailSender.createMimeMessage();
		InputStream input = null;

		try {
			Properties prop = new Properties();

			input = this.getClass().getClassLoader()
					.getResourceAsStream("email.properties");
			prop.load(input);

			String enviornment = prop.getProperty("enviornment");
			MimeMessageHelper helper = new MimeMessageHelper(message, true);

			if (simpleMailMessage.getFrom() != null
					&& !"".equals(simpleMailMessage.getFrom())) {

				helper.setFrom(simpleMailMessage.getFrom());
				
				if (simpleMailMessage.getCc() != null
						&& !"".equals(simpleMailMessage.getCc())) {
					String[] ccList = simpleMailMessage.getCc()[0].trim()
							.split("\\s*,\\s*");
					helper.setCc(ccList);
				}
				
				if (to != null && !"".equals(to))
					helper.setTo(to);
				
				if (simpleMailMessage.getSubject() != null
						&& !"".equals(simpleMailMessage.getSubject()))
					helper.setSubject(enviornment + " : "
							+ simpleMailMessage.getSubject() + " For Study Number : " + studyNumber + ", Subject MRN : " + subjectMrn);
				
				if (simpleMailMessage.getText() != null
						&& !"".equals(simpleMailMessage.getText()))
					helper.setText(String.format(simpleMailMessage.getText(),
							content));
				
				mailSender.send(message);
				logger.info("Email Message : " + message);
			} else {
				logger.info("No From email ID specified.");
			}

		} catch (MessagingException e) {
			logger.info("e " + e.getMessage() );
			throw new MailParseException(e);
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}
}
