package com.velos.integration.core.processor;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.camel.CamelContext;
import org.apache.camel.ExchangePattern;
import org.apache.camel.ProducerTemplate;
import org.apache.cxf.message.MessageContentsList;
import org.apache.log4j.Logger;
import org.springframework.integration.Message;
import org.springframework.integration.MessageChannel;
import org.springframework.integration.core.MessagingTemplate;
import org.springframework.integration.support.MessageBuilder;

import com.velos.integration.core.dao.CoreMappingDao;
import com.velos.integration.core.mapping.CoreCodeListMapper;
import com.velos.integration.core.mapping.ProtocolKeys;
import com.velos.services.Code;
import com.velos.services.FormFieldResponse;
import com.velos.services.FormIdentifier;
import com.velos.services.FormInfo;
import com.velos.services.FormList;
import com.velos.services.GroupIdentifier;
import com.velos.services.NvPair;
import com.velos.services.ObjectInfo;
import com.velos.services.OperationException_Exception;
import com.velos.services.Organizations;
import com.velos.services.Patient;
import com.velos.services.PatientDemographics;
import com.velos.services.PatientEnrollmentDetails;
import com.velos.services.PatientIdentifier;
import com.velos.services.PatientStudyStatusIdentifier;
import com.velos.services.SimpleIdentifier;
import com.velos.services.StudyIdentifier;
import com.velos.services.StudyPatientFormResponse;
import com.velos.services.StudyPatientFormResponseIdentifier;
import com.velos.services.StudyPatientFormResponses;
import com.velos.services.StudyPatientStatus;
import com.velos.services.StudyPatientStatuses;
import com.velos.services.StudySummary;
import com.velos.services.UserIdentifier;

public class PostEnrollment implements
		VelosEspPatientStudyFormResponseDetailEndpoint,
		VelosEspPatientDemographicsEndpoint, VelosEspStudyPatientEndpoint,
		VelosEspPatientStudyFormsEndpoint, VelosEspStudyEndpoint,
		VelosEspUserEndpoint, VelosEspStudyPatientHistoryEndpoint,
		VelosEspPatientDemographicsDetailsEndpoint,
		VelosEspPatientStudyFormResponseEndpoint,
		VelosEspSystemAdministrationEndpoint {

	private static Logger logger = Logger.getLogger(PostEnrollment.class);

	private Map<ProtocolKeys, String> studyPatientMap = new HashMap<ProtocolKeys, String>();
	private CamelContext context;
	private String subjectMrn;

	public String getSubjectMrn() {
		return subjectMrn;
	}

	public void setContext(CamelContext context) {
		this.context = context;
	}

	public ArrayList<String> updatePostEnrollment(
			List<StudyPatientStatus> studyPatientStatusList,
			StudyPatientStatus studyPatientStatus,
			PatientIdentifier patientIdentifier,
			StudyIdentifier studyIdentifier,
			PatientStudyStatusIdentifier studyPatStatId,
			CoreMappingDao coreMappingDao, CamelContext context,
			String sessionToken, String parentOrganization)
			throws OperationException_Exception {
		ArrayList<String> argList = new ArrayList<String>();
		try {
			this.setContext(context);

			Code status = studyPatientStatus.getStudyPatStatus();
			Code reason = studyPatientStatus.getStatusReason();
			String note = studyPatientStatus.getStatusNote();

			XMLGregorianCalendar date = studyPatientStatus.getStatusDate();

			if (date != null) {

				if ("followup".equals(status.getCode())) {
					setData(ProtocolKeys.FollowUpStartDate, getDate(date));
				} else
					setData(ProtocolKeys.FollowUpStartDate, "");

				if ("offstudy".equals(status.getCode())) {
					setData(ProtocolKeys.OffStudyDate, getDate(date));
				} else
					setData(ProtocolKeys.OffStudyDate, "");

				setData(ProtocolKeys.ResponseDate, "");
				setData(ProtocolKeys.ProgressionDate, "");
			} else {
				setData(ProtocolKeys.ResponseDate, "");
				setData(ProtocolKeys.ProgressionDate, "");
				setData(ProtocolKeys.FollowUpStartDate, "");
				setData(ProtocolKeys.OffStudyDate, "");
			}

			boolean isRegistrationDate = false;

			for (StudyPatientStatus studyPatientStatusRow : studyPatientStatusList) {
				if ("screening".equals(studyPatientStatusRow
						.getStudyPatStatus().getCode())) {
					setData(ProtocolKeys.RegistrationDate,
							coreMappingDao
									.getCreationDate(studyPatientStatusRow
											.getStudyPatStatId().getPK()));
					isRegistrationDate = true;
					break;
				}
			}

			if (!isRegistrationDate)
				setData(ProtocolKeys.RegistrationDate, "");

			CoreCodeListMapper coreCodeListMapperProtocolResponse = new CoreCodeListMapper();
			coreCodeListMapperProtocolResponse
					.setCodeListType(ProtocolKeys.PostEnrollmentProtocolResponse
							.toString());
			coreCodeListMapperProtocolResponse.setCodeListSubType("");
			coreMappingDao.getDescription(coreCodeListMapperProtocolResponse);

			if (coreCodeListMapperProtocolResponse.getCodeListSubDesc() != null
					&& !"".equals(coreCodeListMapperProtocolResponse
							.getCodeListSubDesc())) {
				studyPatientMap
						.put(ProtocolKeys.ProtocolResponse,
								coreCodeListMapperProtocolResponse
										.getCodeListSubDesc());
			} else {
				studyPatientMap.put(ProtocolKeys.ProtocolResponse, "");
			}

			PatientEnrollmentDetails patientEnrollmentDetails = getStudyPatientStatus(studyPatStatId);

			setData(ProtocolKeys.AccessionNumber,
					patientEnrollmentDetails.getPatientStudyId());

			if ("offstudy".equals(status.getCode())) {
				setData(ProtocolKeys.OffStudyNote,
						patientEnrollmentDetails.getNotes());
				if (reason != null) {
					CoreCodeListMapper coreCodeListMapperReason = new CoreCodeListMapper();
					coreCodeListMapperReason
							.setCodeListType(ProtocolKeys.PostEnrollmentOffStudyReason
									.toString());
					coreCodeListMapperReason.setCodeListSubType(reason
							.getCode());
					coreMappingDao.getDescription(coreCodeListMapperReason);
					if (coreCodeListMapperReason.getCodeListSubDesc() != null)
						setData(ProtocolKeys.ReasonOffStudy,
								coreCodeListMapperReason.getCodeListSubDesc());
					else
						setData(ProtocolKeys.ReasonOffStudy, "");

				} else
					setData(ProtocolKeys.ReasonOffStudy, "");
			} else {
				setData(ProtocolKeys.OffStudyNote, "");
				setData(ProtocolKeys.ReasonOffStudy, "");
			}

			if (patientEnrollmentDetails.getEvaluationStatus() != null) {
				CoreCodeListMapper coreCodeListMapperEvaluability = new CoreCodeListMapper();
				coreCodeListMapperEvaluability
						.setCodeListType(ProtocolKeys.PostEnrollmentEvaluability
								.toString());
				coreCodeListMapperEvaluability
						.setCodeListSubType(patientEnrollmentDetails
								.getEvaluationStatus().getCode());
				coreMappingDao.getDescription(coreCodeListMapperEvaluability);
				if (coreCodeListMapperEvaluability.getCodeListSubDesc() != null) {
					setData(ProtocolKeys.Evaluability,
							coreCodeListMapperEvaluability.getCodeListSubDesc());
				} else {
					setData(ProtocolKeys.Evaluability, "");
				}
			} else {
				setData(ProtocolKeys.Evaluability, "");
			}

			if (patientEnrollmentDetails.getEvaluationFlag() != null) {
				CoreCodeListMapper coreCodeListMapperEvaluableForResponse = new CoreCodeListMapper();
				coreCodeListMapperEvaluableForResponse
						.setCodeListType(ProtocolKeys.PostEnrollmentEvaluableForResponse
								.toString());
				coreCodeListMapperEvaluableForResponse
						.setCodeListSubType(patientEnrollmentDetails
								.getEvaluationFlag().getCode());
				coreMappingDao
						.getDescription(coreCodeListMapperEvaluableForResponse);
				if (coreCodeListMapperEvaluableForResponse.getCodeListSubDesc() != null) {
					setData(ProtocolKeys.EvaluableForResponse,
							coreCodeListMapperEvaluableForResponse
									.getCodeListSubDesc());
				} else {
					setData(ProtocolKeys.EvaluableForResponse, "");
				}
			} else {
				setData(ProtocolKeys.EvaluableForResponse, "");
			}

			if (patientEnrollmentDetails.getInevaluationStatus() != null) {
				CoreCodeListMapper coreCodeListMapperInevaluableReason = new CoreCodeListMapper();
				coreCodeListMapperInevaluableReason
						.setCodeListType(ProtocolKeys.PostEnrollmentInevaluableReason
								.toString());
				coreCodeListMapperInevaluableReason
						.setCodeListSubType(patientEnrollmentDetails
								.getInevaluationStatus().getCode());
				coreMappingDao
						.getDescription(coreCodeListMapperInevaluableReason);
				if (coreCodeListMapperInevaluableReason.getCodeListSubDesc() != null) {
					setData(ProtocolKeys.InevaluableReason,
							coreCodeListMapperInevaluableReason
									.getCodeListSubDesc());
				} else {
					setData(ProtocolKeys.InevaluableReason, "");
				}
			} else {
				setData(ProtocolKeys.InevaluableReason, "");
			}

			PatientDemographics patientDemographics = getPatientDemographics(patientIdentifier);

			ObjectInfo objectInfoOrganization;
			SimpleIdentifier simpleIdentifier = new SimpleIdentifier();
			simpleIdentifier.setOID(patientEnrollmentDetails.getEnrollingSite()
					.getOID());

			objectInfoOrganization = getObjectInfoFromOID(simpleIdentifier);

			setData(ProtocolKeys.SubjetMRN, coreMappingDao.getPatFacilityID(
					patientDemographics.getPatientIdentifier().getPK(),
					objectInfoOrganization.getTablePk()));

			subjectMrn = studyPatientMap.get(ProtocolKeys.SubjetMRN);

			setData(ProtocolKeys.MDACCPatient, coreMappingDao.checkPatientSite(
					objectInfoOrganization.getTablePk(), parentOrganization));

			setData(ProtocolKeys.PatFirstName,
					patientDemographics.getFirstName());
			setData(ProtocolKeys.PatLastName, patientDemographics.getLastName());
			Code race = patientDemographics.getRace();
			if (race != null) {
				CoreCodeListMapper coreCodeListMapperRace = new CoreCodeListMapper();
				coreCodeListMapperRace.setCodeListType(race.getType());
				coreCodeListMapperRace.setCodeListSubType(race.getCode());
				coreMappingDao.getDescription(coreCodeListMapperRace);
				if (coreCodeListMapperRace.getCodeListSubDesc() != null)
					setData(ProtocolKeys.PatRace,
							coreCodeListMapperRace.getCodeListSubDesc());
				else
					setData(ProtocolKeys.PatRace, "");
			} else
				setData(ProtocolKeys.PatRace, "");

			XMLGregorianCalendar dob = patientDemographics.getDateOfBirth();
			if (dob != null)
				setData(ProtocolKeys.PatDOB, getDate(dob));
			else
				setData(ProtocolKeys.PatDOB, "");
			Code gender = patientDemographics.getGender();
			if (gender != null) {
				CoreCodeListMapper coreCodeListMapperGender = new CoreCodeListMapper();
				coreCodeListMapperGender.setCodeListType(gender.getType());
				coreCodeListMapperGender.setCodeListSubType(gender.getCode());
				coreMappingDao.getDescription(coreCodeListMapperGender);
				if (coreCodeListMapperGender.getCodeListSubDesc() != null)
					setData(ProtocolKeys.PatGender,
							coreCodeListMapperGender.getCodeListSubDesc());
				else
					setData(ProtocolKeys.PatGender, "");
			} else
				setData(ProtocolKeys.PatGender, "");

			Code ethnicity = patientDemographics.getEthnicity();
			if (ethnicity != null) {
				CoreCodeListMapper coreCodeListMapperEthnicity = new CoreCodeListMapper();
				coreCodeListMapperEthnicity
						.setCodeListType(ethnicity.getType());
				coreCodeListMapperEthnicity.setCodeListSubType(ethnicity
						.getCode());
				coreMappingDao.getDescription(coreCodeListMapperEthnicity);
				if (coreCodeListMapperEthnicity.getCodeListSubDesc() != null)
					setData(ProtocolKeys.PatEthnicity,
							coreCodeListMapperEthnicity.getCodeListSubDesc());
				else
					setData(ProtocolKeys.PatEthnicity, "");

			} else
				setData(ProtocolKeys.PatEthnicity, "");

			List<NvPair> morePatientDetails = patientDemographics
					.getMorePatientDetails();

			setData(ProtocolKeys.StudyNumber, studyIdentifier.getStudyNumber());

			Map<String, String> loginUser = coreMappingDao
					.getLoginUser(studyPatStatId.getPK());

			setData(ProtocolKeys.LoginUserUID, loginUser.get("LoginUserId"));

			setData(ProtocolKeys.LoginUserFirstname,
					loginUser.get("LoginUserFirstname"));

			setData(ProtocolKeys.LoginUserLastname,
					loginUser.get("LoginUserLastname"));

			argList.add(sessionToken);
			argList.add(coreMappingDao
					.getUniqueIdentifierSequence(ProtocolKeys.PostEnrollment
							.toString()));
			argList.add(studyPatientMap.get(ProtocolKeys.PatLastName));
			argList.add(studyPatientMap.get(ProtocolKeys.PatFirstName));
			argList.add(studyPatientMap.get(ProtocolKeys.PatDOB));
			argList.add(studyPatientMap.get(ProtocolKeys.PatGender));
			argList.add(studyPatientMap.get(ProtocolKeys.PatRace));
			argList.add(studyPatientMap.get(ProtocolKeys.PatEthnicity));
			argList.add(studyPatientMap.get(ProtocolKeys.SubjetMRN));
			argList.add(studyPatientMap.get(ProtocolKeys.MDACCPatient));
			argList.add(studyPatientMap.get(ProtocolKeys.StudyNumber));
			argList.add(studyPatientMap.get(ProtocolKeys.AccessionNumber));
			argList.add(studyPatientMap.get(ProtocolKeys.RegistrationDate));
			argList.add(studyPatientMap.get(ProtocolKeys.ProtocolResponse));
			argList.add(studyPatientMap.get(ProtocolKeys.ResponseDate));
			argList.add(studyPatientMap.get(ProtocolKeys.ProgressionDate));
			argList.add(studyPatientMap.get(ProtocolKeys.Evaluability));
			argList.add(studyPatientMap.get(ProtocolKeys.EvaluableForResponse));
			argList.add(studyPatientMap.get(ProtocolKeys.InevaluableReason));
			argList.add(studyPatientMap.get(ProtocolKeys.FollowUpStartDate));
			argList.add(studyPatientMap.get(ProtocolKeys.OffStudyDate));
			argList.add(studyPatientMap.get(ProtocolKeys.ReasonOffStudy));
			argList.add(studyPatientMap.get(ProtocolKeys.OffStudyNote));
			argList.add(studyPatientMap.get(ProtocolKeys.LoginUserFirstname));
			argList.add(studyPatientMap.get(ProtocolKeys.LoginUserLastname));
			argList.add(studyPatientMap.get(ProtocolKeys.LoginUserUID));

			return argList;
		} catch (Exception e) {
			argList.add(e.getMessage());
			return argList;
		}
	}

	@Override
	public StudyPatientStatuses getStudyPatientStatusHistory(
			StudyIdentifier studyIdentifier, PatientIdentifier patientIdentifier)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList inpList = new MessageContentsList();
		inpList.add(studyIdentifier);
		inpList.add(patientIdentifier);
		MessageContentsList list = (MessageContentsList) producer.sendBody(
				"cxf:bean:espPatStudyStatusInfoEndpoint",
				ExchangePattern.InOut, inpList);
		return (StudyPatientStatuses) list.get(0);
	}

	@Override
	public PatientDemographics getPatientDemographics(
			PatientIdentifier patientIdentifier)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList list = (MessageContentsList) producer.sendBody(
				"cxf:bean:espPatientDemographics", ExchangePattern.InOut,
				patientIdentifier);
		return (PatientDemographics) list.get(0);
	}

	@Override
	public PatientEnrollmentDetails getStudyPatientStatus(
			PatientStudyStatusIdentifier patientStudyStatusIdentifier)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList list = (MessageContentsList) producer.sendBody(
				"cxf:bean:espStudyPatientStatus", ExchangePattern.InOut,
				patientStudyStatusIdentifier);
		return (PatientEnrollmentDetails) list.get(0);
	}

	@Override
	public Patient getPatientDetails(PatientIdentifier patientIdentifier)
			throws OperationException_Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public StudySummary getStudySummary(StudyIdentifier studyIdentifier)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList list = (MessageContentsList) producer.sendBody(
				"cxf:bean:espStudyPatientStatus", ExchangePattern.InOut,
				studyIdentifier);
		return (StudySummary) list.get(0);
	}

	@Override
	public Organizations getAllOrganizations()
			throws OperationException_Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public void setData(ProtocolKeys key, String value) {
		if (value == null || "".equals(value))
			studyPatientMap.put(key, "");
		else
			studyPatientMap.put(key, value);
	}

	@Override
	public FormList getListOfStudyPatientForms(
			PatientIdentifier patientIdentifier,
			StudyIdentifier studyIdentifier, int maxNumberOfResults,
			boolean formHasResponses) throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList inpList = new MessageContentsList();

		inpList.add(patientIdentifier);
		inpList.add(studyIdentifier);
		inpList.add(maxNumberOfResults);
		inpList.add(formHasResponses);
		MessageContentsList list = (MessageContentsList) producer.sendBody(
				"cxf:bean:espPatStudyStatusFormsInfoEndpoint",
				ExchangePattern.InOut, inpList);
		return (FormList) list.get(0);
	}

	@Override
	public StudyPatientFormResponses getListOfStudyPatientFormResponses(
			FormIdentifier formIdentifier, PatientIdentifier patientIdentifier,
			StudyIdentifier studyIdentifier, int pageNumber, int pageSize)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList inpList = new MessageContentsList();
		inpList.add(formIdentifier);
		inpList.add(patientIdentifier);
		inpList.add(studyIdentifier);
		inpList.add(pageNumber);
		inpList.add(pageSize);
		MessageContentsList list = (MessageContentsList) producer.sendBody(
				"cxf:bean:espPatStudyStatusFormResponseInfoEndpoint",
				ExchangePattern.InOut, inpList);
		return (StudyPatientFormResponses) list.get(0);
	}

	@Override
	public StudyPatientFormResponse getStudyPatientFormResponse(
			StudyPatientFormResponseIdentifier formResponseIdentifier)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList list = (MessageContentsList) producer.sendBody(
				"cxf:bean:espPatStudyStatusFormResponseDetailInfoEndpoint",
				ExchangePattern.InOut, formResponseIdentifier);
		return (StudyPatientFormResponse) list.get(0);
	}

	@Override
	public ObjectInfo getObjectInfoFromOID(SimpleIdentifier arg0)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList list = (MessageContentsList) producer.sendBody(
				"cxf:bean:espPatStudyEndpoint", ExchangePattern.InOut, arg0);
		return (ObjectInfo) list.get(0);
	}

	public static String getDate(XMLGregorianCalendar date) {
		StringBuilder stringDate = new StringBuilder();
		if (date.getMonth() > 9)
			stringDate.append(date.getMonth());
		else
			stringDate.append("0" + date.getMonth());

		stringDate.append("/");

		if (date.getDay() > 9)
			stringDate.append(date.getDay());
		else
			stringDate.append("0" + date.getDay());

		stringDate.append("/");

		stringDate.append(date.getYear());
		return stringDate.toString();
	}
}
