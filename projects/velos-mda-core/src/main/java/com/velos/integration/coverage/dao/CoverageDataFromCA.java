package com.velos.integration.coverage.dao;

import java.math.BigDecimal;

public class CoverageDataFromCA {
	
	private String eventName; /* FULL_SERVICE_NAME */
	private String calendarName;
	private String visitName;
	
	private String cptCode;
	
	private String profChargeCode; /*PROFESSIONAL_CHARGE_CODE */
	private BigDecimal profFeeDiscount; /* % Discount */
	private BigDecimal adjustedProfFee;
	
	private String techChargeCode; /*TECHNICAL_CHARGE_CODE */
	private BigDecimal techFeeDiscount; /* % Discount */
	private BigDecimal adjustedTechFee;

	private String modifier; /* coverageType */
	private String sponsor;
	private String sponsorMdaccId;
	private BigDecimal caXlsRow;
	private String addtionalComments;	
	
	

	public String getAddtionalComments() {
		return addtionalComments;
	}
	public void setAddtionalComments(String addtionalComments) {
		this.addtionalComments = addtionalComments;
	}
	private String studyNumber; /* PROTOCOL_NUMBER */	
	public String getStudyNumber() {
		return studyNumber;
	}
	public void setStudyNumber(String studyNumber) {
		this.studyNumber = studyNumber;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getCalendarName() {
		return calendarName;
	}
	public void setCalendarName(String calendarName) {
		this.calendarName = calendarName;
	}
	public String getVisitName() {
		return visitName;
	}
	public void setVisitName(String visitName) {
		this.visitName = visitName;
	}
	public String getCptCode() {
		return cptCode;
	}
	public void setCptCode(String cptCode) {
		this.cptCode = cptCode;
	}
	public String getProfChargeCode() {
		return profChargeCode;
	}
	public void setProfChargeCode(String profChargeCode) {
		this.profChargeCode = profChargeCode;
	}
	public BigDecimal getProfFeeDiscount() {
		return profFeeDiscount;
	}
	public void setProfFeeDiscount(BigDecimal profFeeDiscount) {
		this.profFeeDiscount = profFeeDiscount;
	}
	public BigDecimal getAdjustedProfFee() {
		return adjustedProfFee;
	}
	public void setAdjustedProfFee(BigDecimal adjustedProfFee) {
		this.adjustedProfFee = adjustedProfFee;
	}
	public String getTechChargeCode() {
		return techChargeCode;
	}
	public void setTechChargeCode(String techChargeCode) {
		this.techChargeCode = techChargeCode;
	}
	public BigDecimal getTechFeeDiscount() {
		return techFeeDiscount;
	}
	public void setTechFeeDiscount(BigDecimal techFeeDiscount) {
		this.techFeeDiscount = techFeeDiscount;
	}
	public BigDecimal getAdjustedTechFee() {
		return adjustedTechFee;
	}
	public void setAdjustedTechFee(BigDecimal adjustedTechFee) {
		this.adjustedTechFee = adjustedTechFee;
	}
	public String getModifier() {
		return modifier;
	}
	public void setModifier(String modifier) {
		this.modifier = modifier;
	}
	public String getSponsor() {
		return sponsor;
	}
	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}
	public String getSponsorMdaccId() {
		return sponsorMdaccId;
	}
	public void setSponsorMdaccId(String sponsorMdaccId) {
		this.sponsorMdaccId = sponsorMdaccId;
	}
	public BigDecimal getCaXlsRow() {
		return caXlsRow;
	}
	public void setCaXlsRow(BigDecimal caXlsRow) {
		this.caXlsRow = caXlsRow;
	}
	
}
