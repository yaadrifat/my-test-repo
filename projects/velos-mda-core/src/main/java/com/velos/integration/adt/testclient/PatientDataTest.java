package com.velos.integration.adt.testclient;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.velos.integration.incomingmessage.scheduler.RunIncomingMsgTask;

public class PatientDataTest {

	public static void main(String[] args) {
		ApplicationContext  springContext = new ClassPathXmlApplicationContext("/WEB-INF/protocol-ws-servlet.xml");
		RunIncomingMsgTask runIncomingMsgTask = (RunIncomingMsgTask) springContext.getBean("runIncomingMsgTask");
		runIncomingMsgTask.runTask();
		
	}

}
