package com.velos.integration.ctmsvgdb.dao;

import java.math.BigDecimal;
import java.sql.Timestamp;

/*
 * This class represents sql table data in VGDB that will be populated by 
 * patient schedule data pulled from CTMS db.
 */
public class PatientScheduleInfo {

	private String studyNumber; /* Protocol */
	private String patientId;
	private String studyCalendarId;

	private BigDecimal eventId; /* Service Code */
	private String eventName; /* Service Name */
	private String eventDescription; /* Service Description */

	private String cptCode;
	private String coverageType; /* Modifier */

	private String visitName;
	private Timestamp eventDate;

	private BigDecimal studyCoordinatorId;	
	private String studyCoordinatorName;
	private String principalInvestigatorName;	
	private String prsCode;
	
	private BigDecimal PatprotId;	
	private BigDecimal PatprotStatus;
	private Timestamp PatprotStartDate;	
	
	public BigDecimal getPatprotStatus() {
		return PatprotStatus;
	}

	public void setPatprotStatus(BigDecimal patprotStatus) {
		PatprotStatus = patprotStatus;
	}
	
	public BigDecimal getPatprotId() {
		return PatprotId;
	}

	public void setPatprotId(BigDecimal patprotId) {
		PatprotId = patprotId;
	}

	public Timestamp getPatprotStartDate() {
		return PatprotStartDate;
	}

	public void setPatprotStartDate(Timestamp patprotStartDate) {
		PatprotStartDate = patprotStartDate;
	}
	
	public BigDecimal getEventId() {
		return eventId;
	}

	public void setEventId(BigDecimal eventId) {
		this.eventId = eventId;
	}

	public BigDecimal getStudyCoordinatorId() {
		return studyCoordinatorId;
	}

	public void setStudyCoordinatorId(BigDecimal studyCoordinatorId) {
		this.studyCoordinatorId = studyCoordinatorId;
	}

	public String getPrsCode() {
		return prsCode;
	}

	public void setPrsCode(String prsCode) {
		this.prsCode = prsCode;
	}

	public String getStudyNumber() {
		return studyNumber;
	}

	public void setStudyNumber(String studyNumber) {
		this.studyNumber = studyNumber;
	}

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public String getStudyCalendarId() {
		return studyCalendarId;
	}

	public void setStudyCalendarId(String studyCalendarId) {
		this.studyCalendarId = studyCalendarId;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getEventDescription() {
		return eventDescription;
	}

	public void setEventDescription(String eventDescription) {
		this.eventDescription = eventDescription;
	}

	public String getCptCode() {
		return cptCode;
	}

	public void setCptCode(String cptCode) {
		this.cptCode = cptCode;
	}

	public String getCoverageType() {
		return coverageType;
	}

	public void setCoverageType(String coverageType) {
		this.coverageType = coverageType;
	}

	public String getVisitName() {
		return visitName;
	}

	public void setVisitName(String visitName) {
		this.visitName = visitName;
	}

	public Timestamp getEventDate() {
		return eventDate;
	}

	public void setEventDate(Timestamp eventDate) {
		this.eventDate = eventDate;
	}

	public String getStudyCoordinatorName() {
		return studyCoordinatorName;
	}

	public void setStudyCoordinatorName(String studyCoordinatorName) {
		this.studyCoordinatorName = studyCoordinatorName;
	}

	public String getPrincipalInvestigatorName() {
		return principalInvestigatorName;
	}

	public void setPrincipalInvestigatorName(String principalInvestigatorName) {
		this.principalInvestigatorName = principalInvestigatorName;
	}

}
