package com.velos.integration.core.mapping;

public class CoreCodeListMapper {
	String codeListType;
	String codeListSubType;
	String codeListSubDesc;
	public String getCodeListType() {
		return codeListType;
	}
	public void setCodeListType(String codeListType) {
		this.codeListType = codeListType;
	}
	public String getCodeListSubType() {
		return codeListSubType;
	}
	public void setCodeListSubType(String codeListSubType) {
		this.codeListSubType = codeListSubType;
	}
	public String getCodeListSubDesc() {
		return codeListSubDesc;
	}
	public void setCodeListSubDesc(String codeListSubDesc) {
		this.codeListSubDesc = codeListSubDesc;
	}
	
	
}
