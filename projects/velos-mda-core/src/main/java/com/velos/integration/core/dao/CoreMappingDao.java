package com.velos.integration.core.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.support.lob.LobHandler;

import com.ibm.icu.math.BigDecimal;
import com.velos.integration.charges.bean.SaveChargeFileBean;
import com.velos.integration.core.mapping.CoreCodeListMapper;

public class CoreMappingDao {
	private DataSource vgdbDataSource;
	private DataSource eresDataSource;
	private JdbcTemplate jdbcTemplate;
	private LobHandler lobHandler;

	private static Logger logger = Logger.getLogger(CoreMappingDao.class);

	public void setVgdbDataSource(DataSource vgdbDataSource) {
		this.vgdbDataSource = vgdbDataSource;
	}

	public void setEresDataSource(DataSource eresDataSource) {
		this.eresDataSource = eresDataSource;
	}

	public void setLobHandler(LobHandler lobHandler) {
		this.lobHandler = lobHandler;
	}

	public String getUniqueIdentifierSequence(String type) {
		String uniqueSequence = null;
		int sequence = 0;
		try {
			String sqlSqnc = "SELECT SEQ_CORE_UNIQUE_IDENTIFIER.nextval from dual";
			JdbcTemplate jdbcTemplate = new JdbcTemplate(vgdbDataSource);
			sequence = jdbcTemplate.queryForInt(sqlSqnc);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		uniqueSequence = type + "_" + sequence;
		return uniqueSequence;
	}

	public void getDescription(CoreCodeListMapper coreCodeListMapper) {
		try {
			String sql = "SELECT CODELST_SUBTYP_DESC FROM CORE_CODELST  WHERE CODELST_TYPE = ? AND (CODELST_SUBTYP= ? OR CODELST_SUBTYP IS NULL)";
			jdbcTemplate = new JdbcTemplate(vgdbDataSource);

			String description = (String) jdbcTemplate.queryForObject(sql,
					new Object[] { coreCodeListMapper.getCodeListType(),
							coreCodeListMapper.getCodeListSubType() },
					String.class);

			coreCodeListMapper.setCodeListSubDesc(description);

		} catch (EmptyResultDataAccessException em) {
			coreCodeListMapper.setCodeListSubDesc("");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

	}

	public String getUserGroup(int userGroup) {
		String userGroupName = "";
		try {
			String sql = "select grp_name from er_grps where pk_grp = ?";
			jdbcTemplate = new JdbcTemplate(vgdbDataSource);

			userGroupName = (String) jdbcTemplate.queryForObject(sql,
					new Object[] { userGroup }, String.class);

		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return userGroupName;

	}

	public String getUserOrganization(int userOrganization) {
		String userOrganizationName = "";
		try {
			String sql = "select site_name from er_site where pk_site = ?";
			jdbcTemplate = new JdbcTemplate(vgdbDataSource);

			userOrganizationName = (String) jdbcTemplate.queryForObject(sql,
					new Object[] { userOrganization }, String.class);

		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return userOrganizationName;

	}

	public void getTypeAndDescription(CoreCodeListMapper coreCodeListMapper) {
		List<Map<String, String>> rows = null;
		try {
			String sql = "SELECT CODELST_TYPE, CODELST_SUBTYP_DESC FROM CORE_CODELST  WHERE CODELST_SUBTYP= ? AND CODELST_TYPE IN ('studyEnrollment','pIOverride', 'preEnrollment', 'postEnrollment', 'preAndStudyEnrll')";
			jdbcTemplate = new JdbcTemplate(vgdbDataSource);

			rows = jdbcTemplate.queryForList(sql,
					new Object[] { coreCodeListMapper.getCodeListSubType() });
			boolean isInfConsent = false;
			for (Map<String, String> row : rows) {
				if ("preAndStudyEnrll".equals(row.get("CODELST_TYPE"))) {
					coreCodeListMapper.setCodeListType(row.get("CODELST_TYPE"));
					coreCodeListMapper.setCodeListSubDesc(row
							.get("CODELST_SUBTYP_DESC"));
					isInfConsent = true;
					break;
				}
			}
			if (!isInfConsent) {
				for (Map<String, String> row : rows) {
					coreCodeListMapper.setCodeListType(row.get("CODELST_TYPE"));
					coreCodeListMapper.setCodeListSubDesc(row
							.get("CODELST_SUBTYP_DESC"));
					break;
				}

			}

		} catch (EmptyResultDataAccessException em) {
			coreCodeListMapper.setCodeListType("");
			coreCodeListMapper.setCodeListSubDesc("");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

	}

	public String getCreaterEmail(int patientStudyStat) {
		String emailId = "";
		try {
			String sql = "select a.add_email from er_patstudystat ps, er_user u, er_add a where ps.pk_patstudystat = ? and ps.creator = u.pk_user and u.fk_peradd = a.pk_add" ;

			jdbcTemplate = new JdbcTemplate(eresDataSource);

			emailId = (String) jdbcTemplate.queryForObject(sql, new Object[] { patientStudyStat }, String.class);

		} catch (EmptyResultDataAccessException em) {
			emailId = "";
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return emailId;

	}
	
	public Map<String, String> getLoginUser(int patientStudyStat) {
		Map<String, String> loginUser = new HashMap<String, String>();
		try {
			String sql = "select u.usr_logname, u.usr_firstname, u.usr_lastname from er_patstudystat ps, er_user u where ps.pk_patstudystat = ? and ps.creator = u.pk_user ";
			jdbcTemplate = new JdbcTemplate(eresDataSource);

			loginUser = (Map) jdbcTemplate.queryForMap(sql,
					new Object[] { patientStudyStat });

			loginUser.put("LoginUserId", loginUser.get("USR_LOGNAME"));
			loginUser.put("LoginUserLastName", loginUser.get("USR_LASTNAME"));
			loginUser.put("LoginUserFirstName", loginUser.get("USR_FIRSTNAME"));

		} catch (EmptyResultDataAccessException em) {
			loginUser.put("LoginUserId", "");
			loginUser.put("LoginUserLastName", "");
			loginUser.put("LoginUserFirstName", "");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return loginUser;

	}
	
	public String getCreationDate(int patientStudyStat) {
		String creationDate ="";
		try {
			String sql = "select to_char(created_on,'mm/dd/yyyy hh:mi:ss am') from er_patstudystat ps where ps.pk_patstudystat = ?";
			jdbcTemplate = new JdbcTemplate(eresDataSource);

			creationDate = (String) jdbcTemplate.queryForObject(sql,
					new Object[] { patientStudyStat }, String.class);

		} catch (EmptyResultDataAccessException em) {
			creationDate = "";
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return creationDate;

	}

	public String getPatFacilityID(int patientKey, int patientSite) {
		String patFacility = "";
		try {
			String sql = "select ep.pat_facilityid from er_patfacility ep where ep.fk_per = ? and ep.FK_SITE = ?";
			jdbcTemplate = new JdbcTemplate(eresDataSource);
			patFacility = (String) jdbcTemplate.queryForObject(sql,
					new Object[] { patientKey, patientSite }, String.class);
		} catch (EmptyResultDataAccessException em) {
			patFacility = "";
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return patFacility;
	}

	public String checkPatientSite(int patientSite, String parentOrganization) {
		String yesNo = "N";
		int count = 0;
		try {
			String sql = "select count(*) from er_site where regexp_replace(lower(site_name), '[[:space:]]*','') = regexp_replace(lower(?), '[[:space:]]*','') start with pk_site = ? connect by prior site_parent = pk_site";
			jdbcTemplate = new JdbcTemplate(eresDataSource);
			count = (Integer) jdbcTemplate.queryForObject(sql, new Object[] {
					parentOrganization, patientSite }, Integer.class);
			if (count > 0)
				yesNo = "Y";
			else
				yesNo = "N";

		} catch (EmptyResultDataAccessException em) {
			yesNo = "N";
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return yesNo;
	}

	public Map<String, String> getSplitedName(String logName) {
		Map<String, String> name = new HashMap<String, String>();
		try {
			String sql = "select u.usr_firstname, u.usr_lastname from  er_user u where u.usr_logname = ? and rownum = 1";
			jdbcTemplate = new JdbcTemplate(eresDataSource);

			name = (Map) jdbcTemplate
					.queryForMap(sql, new Object[] { logName });

			name.put("firstName", name.get("USR_FIRSTNAME"));
			name.put("lastName", name.get("USR_LASTNAME"));

		} catch (EmptyResultDataAccessException em) {
			name.put("firstName", "");
			name.put("lastName", "");

		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return name;

	}

	public boolean saveMessageDetails(String enrollmentType, String requestXML,
			String responseXML, String response, String tableName,
			int tablePK) {
		boolean isSuccess = false;
		try {
			String sql = "INSERT INTO core_audit "
					+ "(pk_audit,table_name, table_pk, enrollment_type, request_xml, response_xml,"
					+ " response_message, created_on)"
					+ " VALUES (SEQ_CORE_AUDIT.nextval, ?, ?, ?, ?, ?, ?, sysdate)";
			jdbcTemplate = new JdbcTemplate(eresDataSource);

			jdbcTemplate.update(sql, new Object[] { tableName, tablePK,
					enrollmentType, requestXML, responseXML, response });

			isSuccess = true;
		} catch (Exception e) {
			e.printStackTrace();
			isSuccess = false;
		}

		return isSuccess;
	}
}
