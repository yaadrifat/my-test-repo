package com.velos.integration.core.processor;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import com.velos.services.ObjectInfo;
import com.velos.services.OperationException_Exception;
import com.velos.services.SimpleIdentifier;

@WebService(targetNamespace = "http://velos.com/services/", name = "PatientDemographicsSEI")
public interface VelosEspPatientDemographicsEndpoint {
	@WebResult(name = "PatientDemographics", targetNamespace = "")
	@RequestWrapper(localName = "getPatientDemographics", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetPatientDemographics")
	@WebMethod
	@ResponseWrapper(localName = "getPatientDemographicsResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetPatientDemographicsResponse")
	public com.velos.services.PatientDemographics getPatientDemographics(
			@WebParam(name = "PatientIdentifier", targetNamespace = "") com.velos.services.PatientIdentifier patientIdentifier)
			throws OperationException_Exception;
}
