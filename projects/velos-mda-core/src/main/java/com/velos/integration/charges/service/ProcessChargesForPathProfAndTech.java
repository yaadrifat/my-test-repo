package com.velos.integration.charges.service;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.text.SimpleDateFormat;
import java.util.StringTokenizer;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v23.segment.MSH;
import ca.uhn.hl7v2.model.v23.segment.PID;
import ca.uhn.hl7v2.parser.PipeParser;
import ca.uhn.hl7v2.util.Hl7InputStreamMessageIterator;

import com.velos.integration.charges.bean.ChargesBean;
import com.velos.integration.charges.bean.SaveChargeFileBean;
import com.velos.integration.charges.dao.ChargesDaoImpl;

public class ProcessChargesForPathProfAndTech {

	public void createCharges(SaveChargeFileBean saveChargeFileBean,
			ChargesDaoImpl chargesDao) {

		FileReader reader = null;
		try {
			reader = new FileReader(saveChargeFileBean.getFile());
			LineNumberReader ln = new LineNumberReader(reader);
			String currentLine = "";
			int count = 0;
			StringBuilder messageBuilder = new StringBuilder();
			StringBuilder messageBuilderDB = new StringBuilder();
			while ((currentLine = ln.readLine()) != null) {
				if (currentLine.contains("MSH|")
						|| currentLine.contains("FTS|")) {
					if (count == 0) {
						count++;
					} else {

						ChargesBean chargeBean = new ChargesBean();
						String serviceCodeResearchModifier = "";
						PipeParser pipeParser = new PipeParser();
						pipeParser.setValidationContext(new MyValidationContext());
						Message next = pipeParser.parse(messageBuilder
								.toString().trim());

						chargeBean
								.setFileName(saveChargeFileBean.getFileName());
						chargeBean.setFileId(saveChargeFileBean
								.getSystemFileID());

						chargeBean.setChargeRow(messageBuilderDB.toString()
								.trim());

						// Message Header
						MSH msh = (MSH) next.get("MSH");
						/*
						 * chargeBean.setDepartment(msh.getMsh4_SendingFacility()
						 * .getComponent(0).encode());
						 */
						chargeBean.setDepartment(saveChargeFileBean
								.getFileType());
						chargeBean.setBatchDate(this.stringToDate(msh
								.getMsh7_DateTimeOfMessage().getComponent(0)
								.encode(),
								saveChargeFileBean.getBatchDateFormat()));

						// Patient Details
						PID pid = (PID) next.get("PID");
						chargeBean.setMrn(pid.getPatientIDInternalID(0)
								.getComponent(0).encode());
						chargeBean.setLastName(pid.getPid5_PatientName(0)
								.getComponent(0).encode());
						chargeBean.setFirstName(pid.getPid5_PatientName(0)
								.getComponent(1).encode());

						StringTokenizer segments = new StringTokenizer(
								next.toString(),
								System.getProperty("line.separator"));
						while (segments.hasMoreElements()) {
							StringBuilder segment = new StringBuilder(
									(String) segments.nextElement());
							String segmentName = segment.substring(0, 3);
							if ("FT1".equals(segmentName)) {
								int counter = 0;
								String components[] = segment.toString().split(
										"\\|");
								for (String component : components) {
									switch (counter) {
									case 4:
										chargeBean.setServiceDate(chargeBean
												.returnDate(component));
										break;
									case 7:
										String completeSegment[] = component
												.split("\\^");
										serviceCodeResearchModifier = completeSegment[0];
										String serviceCodeResearchModifiers[] = serviceCodeResearchModifier
												.split("-");
										if (serviceCodeResearchModifiers.length > 0)
											chargeBean
													.setServiceCode(serviceCodeResearchModifiers[0]);
										if (serviceCodeResearchModifiers.length > 1)
											chargeBean
													.setResearchModifier1(serviceCodeResearchModifiers[1]);
										if (serviceCodeResearchModifiers.length > 2)
											chargeBean
													.setResearchModifier2(serviceCodeResearchModifiers[2]);
										if (serviceCodeResearchModifiers.length > 3)
											chargeBean
													.setResearchModifier3(serviceCodeResearchModifiers[3]);
										break;
									case 8:
										chargeBean.setServiceName(component);
										chargeBean
												.setServiceDescription(component);
										break;
									default:
										break;
									}
									counter++;
								}

							}
						}
						chargesDao.createCharge(chargeBean);
					}
					messageBuilder = new StringBuilder();
					messageBuilder.append(currentLine);
					messageBuilder.append(System.getProperty("line.separator"));
					messageBuilderDB = new StringBuilder();
					messageBuilderDB.append(currentLine);
					messageBuilderDB.append(System
							.getProperty("line.separator"));
				} else if (!currentLine.contains("FTS|")) {
					if (currentLine.contains("PID|")
							|| currentLine.contains("FT1|")) {
						messageBuilder.append(System
								.getProperty("line.separator"));
						messageBuilder.append(currentLine);
					}
					messageBuilderDB.append(System
							.getProperty("line.separator"));
					messageBuilderDB.append(currentLine);
				}
			}
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (HL7Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static java.util.Date stringToDate(String dtString, String format) {
		java.util.Date dt = null;
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		try {
			dt = sdf.parse(dtString);
		} catch (Exception e) {
		}
		return dt;
	}

	public String truncateResearchModifier(String researchModifier) {
		if (researchModifier != null && !"".equals(researchModifier)) {
			if (researchModifier.length() > 2)
				return researchModifier.substring(0, 1);
		}
		return researchModifier;
	}
}
