package com.velos.integration.charges.bean;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ChargesBean {
	private String department;
	private Date batchDate;
	private String mrn;
	private String lastName;
	private String firstName;
	private Date serviceDate;
	private String serviceName;
	private String serviceDescription;
	private String cptCode;
	private String serviceCode;
	private String researchModifier1;
	private String researchModifier2;
	private String researchModifier3;
	private String fileName;
	private String fileId;
	private String chargeRow;
	
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public Date getBatchDate() {
		return batchDate;
	}
	public void setBatchDate(Date batchDate) {
		this.batchDate = batchDate;
	}
	public String getMrn() {
		return mrn;
	}
	public void setMrn(String mrn) {
		this.mrn = mrn;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public Date getServiceDate() {
		return serviceDate;
	}
	public void setServiceDate(Date serviceDate) {
		this.serviceDate = serviceDate;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String getServiceDescription() {
		return serviceDescription;
	}
	public void setServiceDescription(String serviceDescription) {
		this.serviceDescription = serviceDescription;
	}
	public String getCptCode() {
		return cptCode;
	}
	public void setCptCode(String cptCode) {
		this.cptCode = cptCode;
	}
	public String getResearchModifier1() {
		return researchModifier1;
	}
	public void setResearchModifier1(String researchModifier1) {
		this.researchModifier1 = researchModifier1;
	}
	public String getResearchModifier2() {
		return researchModifier2;
	}
	public void setResearchModifier2(String researchModifier2) {
		this.researchModifier2 = researchModifier2;
	}
	public String getResearchModifier3() {
		return researchModifier3;
	}
	public void setResearchModifier3(String researchModifier3) {
		this.researchModifier3 = researchModifier3;
	}
	
	public String getServiceCode() {
		return serviceCode;
	}
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFileId() {
		return fileId;
	}
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}
	public String getChargeRow() {
		return chargeRow;
	}
	public void setChargeRow(String chargeRow) {
		this.chargeRow = chargeRow;
	}
	public Date returnDate(String dateString) {
		Date parsed = null;
		if (dateString == null || "".equals(dateString)) {
			return null;
		} else {
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmm");

			try {
				parsed = format.parse(dateString);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return parsed;
	}
}
