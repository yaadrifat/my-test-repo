package com.velos.integration.core.processor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.camel.CamelContext;
import org.apache.camel.ExchangePattern;
import org.apache.camel.ProducerTemplate;
import org.apache.cxf.message.MessageContentsList;
import org.apache.log4j.Logger;

import com.velos.integration.core.dao.CoreMappingDao;
import com.velos.integration.core.mapping.CoreCodeListMapper;
import com.velos.integration.core.mapping.ProtocolKeys;
import com.velos.services.Code;
import com.velos.services.FormField;
import com.velos.services.FormFieldResponse;
import com.velos.services.FormIdentifier;
import com.velos.services.FormInfo;
import com.velos.services.FormList;
import com.velos.services.FormSection;
import com.velos.services.GroupIdentifier;
import com.velos.services.NvPair;
import com.velos.services.ObjectInfo;
import com.velos.services.OperationException_Exception;
import com.velos.services.OrganizationIdentifier;
import com.velos.services.Organizations;
import com.velos.services.Patient;
import com.velos.services.PatientDemographics;
import com.velos.services.PatientEnrollmentDetails;
import com.velos.services.PatientIdentifier;
import com.velos.services.PatientStudyStatusIdentifier;
import com.velos.services.SimpleIdentifier;
import com.velos.services.StudyIdentifier;
import com.velos.services.StudyPatientFormDesign;
import com.velos.services.StudyPatientFormResponse;
import com.velos.services.StudyPatientFormResponseIdentifier;
import com.velos.services.StudyPatientFormResponses;
import com.velos.services.StudyPatientStatus;
import com.velos.services.StudyPatientStatuses;
import com.velos.services.StudySummary;
import com.velos.services.UserIdentifier;

public class PreEnrollment implements
		VelosEspPatientStudyFormResponseDetailEndpoint,
		VelosEspPatientDemographicsEndpoint, VelosEspStudyPatientEndpoint,
		VelosEspPatientStudyFormsEndpoint, VelosEspStudyEndpoint,
		VelosEspUserEndpoint, VelosEspStudyPatientHistoryEndpoint,
		VelosEspPatientDemographicsDetailsEndpoint,
		VelosEspPatientStudyFormDesignEndpoint,
		VelosEspPatientStudyFormResponseEndpoint,
		VelosEspSystemAdministrationEndpoint {

	private static Logger logger = Logger.getLogger(PreEnrollment.class);

	private Map<ProtocolKeys, String> studyPatientMap = new HashMap<ProtocolKeys, String>();
	private CamelContext context;
	private String subjectMrn;

	public String getSubjectMrn() {
		return subjectMrn;
	}

	public void setContext(CamelContext context) {
		this.context = context;
	}

	public ArrayList<String> updatePreEnrollment(
			List<StudyPatientStatus> studyPatientStatusList,
			StudyPatientStatus studyPatientStatus,
			PatientIdentifier patientIdentifier,
			StudyIdentifier studyIdentifier,
			PatientStudyStatusIdentifier studyPatStatId,
			CoreMappingDao coreMappingDao, CamelContext context,
			String sessionToken, String parentOrganization)
			throws OperationException_Exception {
		ArrayList<String> argList = new ArrayList<String>();
		try {
			this.setContext(context);

			Code status = studyPatientStatus.getStudyPatStatus();
			Code reason = studyPatientStatus.getStatusReason();
			String note = studyPatientStatus.getStatusNote();

			CoreCodeListMapper coreCodeListMapperEligibilityForm = new CoreCodeListMapper();
			coreCodeListMapperEligibilityForm
					.setCodeListType(ProtocolKeys.FormName.toString());
			coreCodeListMapperEligibilityForm
					.setCodeListSubType(ProtocolKeys.EligibilityForm.toString());
			coreMappingDao.getDescription(coreCodeListMapperEligibilityForm);

			if (coreCodeListMapperEligibilityForm.getCodeListSubDesc() != null) {
				FormList formList = getListOfStudyPatientForms(
						patientIdentifier, studyIdentifier, 50, true);
				for (FormInfo formInfo : formList.getFormInfo()) {

					/*
					 * if
					 * (coreCodeListMapperEligibilityForm.getCodeListSubDesc()
					 * .equals(formInfo.getFormName())) {
					 */
					if (formInfo
							.getFormName()
							.toLowerCase()
							.replaceAll("\\s", "")
							.contains(
									coreCodeListMapperEligibilityForm
											.getCodeListSubDesc().toLowerCase()
											.replaceAll("\\s", ""))) {
						Map<Integer, Integer> map = new HashMap<Integer, Integer>();

						StudyPatientFormDesign studyPatientFormDesign = getStudyPatientFormDesign(
								formInfo.getFormIdentifier(), studyIdentifier,
								formInfo.getFormName(), true);

						for (FormSection formSection : studyPatientFormDesign
								.getSections().getSection()) {
							int questionNumber = 0;
							for (FormField formField : formSection.getFields()
									.getField()) {
								if (formField.getDataType() != null) {
									if ("MULTIPLE_CHOICE_RADIO_BUTTON"
											.equals(formField.getDataType()
													.toString())) {
										try {
											questionNumber = Integer
													.parseInt(formField
															.getUniqueID().substring(formField
																	.getUniqueID().lastIndexOf("_")+1));											
											map.put(questionNumber, formField
													.getFieldIdentifier()
													.getPK());
										} catch (NumberFormatException nfe) {
											argList.add("Field Id for Question with Field Id : "
													+ formField.getUniqueID()
													+ " in the form - "
													+ formInfo.getFormName()
													+ " should be in a number format");
											return argList;
										}

									}
								}
							}
							break;
						}

						StudyPatientFormResponses studyPatientFormResponses = getListOfStudyPatientFormResponses(
								formInfo.getFormIdentifier(),
								patientIdentifier, studyIdentifier, 1, 1000);

						for (StudyPatientFormResponse studyPatientFormResponse : studyPatientFormResponses
								.getStudyPatientFormResponses()) {
							StudyPatientFormResponseIdentifier studyPatientFormResponseIdentifier = studyPatientFormResponse
									.getSystemID();

							StudyPatientFormResponse studyPatientFormResponseDetails = getStudyPatientFormResponse(studyPatientFormResponseIdentifier);

							StringBuilder eligibilityAnswer = new StringBuilder();

							SortedSet<Integer> keys = new TreeSet<Integer>(
									map.keySet());

							for (Integer key : keys) {
								Integer value = map.get(key);
								for (FormFieldResponse formFieldResponse : studyPatientFormResponseDetails
										.getFormFieldResponses().getField()) {
									if (value.equals((formFieldResponse
											.getFieldIdentifier().getPK()))
											|| value == formFieldResponse
													.getFieldIdentifier()
													.getPK()) {
										if ("yes".equals(formFieldResponse
												.getValue())
												|| "Yes".equals(formFieldResponse
														.getValue())) {
											eligibilityAnswer.append("Y");
										} else if ("no"
												.equals(formFieldResponse
														.getValue())
												|| "No".equals(formFieldResponse
														.getValue())) {
											eligibilityAnswer.append("N");
										} else if ("N/A"
												.equals(formFieldResponse
														.getValue())
												|| "NA".equals(formFieldResponse
														.getValue())
												|| "n/a".equals(formFieldResponse
														.getValue())
												|| "na".equals(formFieldResponse
														.getValue())) {
											eligibilityAnswer.append("I");
										} else
											eligibilityAnswer.append("*");
									}

								}
							}
							setData(ProtocolKeys.EligibilityQuestionAnswers,
									eligibilityAnswer.toString());
							break;
							// do something
						}

						break;
					} else {
						setData(ProtocolKeys.EligibilityQuestionAnswers, "");
					}

				}
				if (studyPatientMap
						.get(ProtocolKeys.EligibilityQuestionAnswers) == null)
					setData(ProtocolKeys.EligibilityQuestionAnswers, "");
			} else {
				setData(ProtocolKeys.EligibilityQuestionAnswers, "");
			}

			CoreCodeListMapper coreCodeListMapperStatus = new CoreCodeListMapper();
			coreCodeListMapperStatus.setCodeListType(ProtocolKeys.PreEnrollment
					.toString());
			coreCodeListMapperStatus.setCodeListSubType(status.getCode());
			coreMappingDao.getDescription(coreCodeListMapperStatus);
			if (coreCodeListMapperStatus.getCodeListSubDesc() != null) {
				setData(ProtocolKeys.Eligible,
						coreCodeListMapperStatus.getCodeListSubDesc());
			} else
				setData(ProtocolKeys.Eligible, "");

			CoreCodeListMapper coreCodeListMapperReasonRequired = new CoreCodeListMapper();
			coreCodeListMapperReasonRequired
					.setCodeListType(ProtocolKeys.PreEnrollmentReasonRequired
							.toString());
			coreCodeListMapperReasonRequired.setCodeListSubType(status
					.getCode());
			coreMappingDao.getDescription(coreCodeListMapperReasonRequired);

			if ("Y".equals(coreCodeListMapperReasonRequired
					.getCodeListSubDesc())) {
				if (reason != null && reason.getCode() != null) {
					CoreCodeListMapper coreCodeListMapperReason = new CoreCodeListMapper();
					coreCodeListMapperReason
							.setCodeListType(ProtocolKeys.PreEnrollmentReason
									.toString());
					coreCodeListMapperReason.setCodeListSubType(reason
							.getCode());
					coreMappingDao.getDescription(coreCodeListMapperReason);
					if (coreCodeListMapperReason.getCodeListSubDesc() != null)
						setData(ProtocolKeys.Reason,
								coreCodeListMapperReason.getCodeListSubDesc());
					else
						setData(ProtocolKeys.Reason, "");
				} else
					setData(ProtocolKeys.Reason, "");
			} else {
				setData(ProtocolKeys.Reason, "");
			}

			if (!"infConsent".equals(studyPatientStatus
					.getStudyPatStatus().getCode())) {
				setData(ProtocolKeys.PreEnrollmentDate,
						coreMappingDao.getCreationDate(studyPatientStatus.getStudyPatStatId().getPK()));	
			}
			else
			{
				int prescreenPassPk = -1;
				int prescreenFailPk = -1;
				
				for (StudyPatientStatus studyPatientStatusRow : studyPatientStatusList) {
					if ("patStatus_12".equals(studyPatientStatusRow
							.getStudyPatStatus().getCode())) {
						prescreenPassPk = studyPatientStatusRow.getStudyPatStatId().getPK();	
						
						break;
					}
					if ("patStatus_13".equals(studyPatientStatusRow
							.getStudyPatStatus().getCode())) {
						prescreenFailPk = studyPatientStatusRow.getStudyPatStatId().getPK();					
					}
				}
				if(prescreenPassPk != -1)
				setData(ProtocolKeys.PreEnrollmentDate,
						coreMappingDao.getCreationDate(prescreenPassPk));
				else if(prescreenFailPk != -1)
					setData(ProtocolKeys.PreEnrollmentDate,
							coreMappingDao.getCreationDate(prescreenFailPk));
				else
					setData(ProtocolKeys.PreEnrollmentDate,
							"");
			}
			
			boolean isSignedConsentDate = false;
			
			for (StudyPatientStatus studyPatientStatusRow : studyPatientStatusList) {
				if ("infConsent".equals(studyPatientStatusRow
						.getStudyPatStatus().getCode())) {
					XMLGregorianCalendar signedConsentDate = studyPatientStatusRow
							.getStatusDate();
					setData(ProtocolKeys.SignedConsentFormDate,
							getDate(signedConsentDate));
					isSignedConsentDate = true;
					break;
				}
			}

			if (!isSignedConsentDate)
				setData(ProtocolKeys.SignedConsentFormDate, "");
			
			PatientEnrollmentDetails patientEnrollmentDetails = getStudyPatientStatus(studyPatStatId);

			setData(ProtocolKeys.PreEnrollmentNumber,
					patientEnrollmentDetails.getPatientStudyId());

			if (reason != null) {
				CoreCodeListMapper coreCodeListMapperOtherTypeRequired = new CoreCodeListMapper();
				coreCodeListMapperOtherTypeRequired
						.setCodeListType(ProtocolKeys.PreEnrollmentOtherTypeRequired
								.toString());
				coreCodeListMapperOtherTypeRequired.setCodeListSubType(reason
						.getCode());
				coreMappingDao
						.getDescription(coreCodeListMapperOtherTypeRequired);
				if ("Y".equals(coreCodeListMapperOtherTypeRequired
						.getCodeListSubDesc()))
					setData(ProtocolKeys.OtherType,
							patientEnrollmentDetails.getNotes());
				else
					setData(ProtocolKeys.OtherType, "");
			} else
				setData(ProtocolKeys.OtherType, "");

			setData(ProtocolKeys.PreEnrollmentComment,
					patientEnrollmentDetails.getNotes());
			UserIdentifier userIdentifierAssignedTo = patientEnrollmentDetails
					.getAssignedTo();
			UserIdentifier userIdentifierPhysician = patientEnrollmentDetails
					.getPhysician();
			if (userIdentifierPhysician != null) {

				setData(ProtocolKeys.PreEnrollmentInvestFirstName,
						userIdentifierPhysician.getFirstName());

				setData(ProtocolKeys.PreEnrollmentInvestLastName,
						userIdentifierPhysician.getLastName());

				setData(ProtocolKeys.PreEnrollmentInvestUserId,
						userIdentifierPhysician.getUserLoginName());

				OrganizationIdentifier organizationIdentifier = patientEnrollmentDetails
						.getTreatingOrganization();

				if (organizationIdentifier != null)
					setData(ProtocolKeys.PreEnrollmentInvestInstitution,
							organizationIdentifier.getSiteName());
				else
					setData(ProtocolKeys.PreEnrollmentInvestInstitution, "");

				setData(ProtocolKeys.PreEnrollmentInvestOncologyGrp,
						patientEnrollmentDetails.getTreatmentLocation()
								.getDescription());
			} else {
				setData(ProtocolKeys.PreEnrollmentInvestFirstName, "");
				setData(ProtocolKeys.PreEnrollmentInvestLastName, "");
				setData(ProtocolKeys.PreEnrollmentInvestUserId, "");
				setData(ProtocolKeys.PreEnrollmentInvestInstitution, "");
				setData(ProtocolKeys.PreEnrollmentInvestOncologyGrp, "");
			}

			Map<String, String> enroller = coreMappingDao
					.getLoginUser(studyPatStatId.getPK());

			setData(ProtocolKeys.PreEnrollerUserid, enroller.get("LoginUserId"));

			setData(ProtocolKeys.PreEnrollerFirstName,
					enroller.get("LoginUserFirstName"));

			setData(ProtocolKeys.PreEnrollerLastName,
					enroller.get("LoginUserLastName"));

			Code evaluationStatus = patientEnrollmentDetails
					.getEvaluationStatus();
			Code inEvaluationStatus = patientEnrollmentDetails
					.getInevaluationStatus();

			PatientDemographics patientDemographics = getPatientDemographics(patientIdentifier);

			ObjectInfo objectInfoOrganization;
			SimpleIdentifier simpleIdentifier = new SimpleIdentifier();
			simpleIdentifier.setOID(patientEnrollmentDetails.getEnrollingSite()
					.getOID());

			objectInfoOrganization = getObjectInfoFromOID(simpleIdentifier);

			setData(ProtocolKeys.SubjetMRN, coreMappingDao.getPatFacilityID(
					patientDemographics.getPatientIdentifier().getPK(),
					objectInfoOrganization.getTablePk()));

			subjectMrn = studyPatientMap.get(ProtocolKeys.SubjetMRN);

			setData(ProtocolKeys.MDACCPatient, coreMappingDao.checkPatientSite(
					objectInfoOrganization.getTablePk(), parentOrganization));

			setData(ProtocolKeys.PatFirstName,
					patientDemographics.getFirstName());
			setData(ProtocolKeys.PatLastName, patientDemographics.getLastName());
			Code race = patientDemographics.getRace();
			if (race != null) {
				CoreCodeListMapper coreCodeListMapperRace = new CoreCodeListMapper();
				coreCodeListMapperRace.setCodeListType(race.getType());
				coreCodeListMapperRace.setCodeListSubType(race.getCode());
				coreMappingDao.getDescription(coreCodeListMapperRace);
				if (coreCodeListMapperRace.getCodeListSubDesc() != null)
					setData(ProtocolKeys.PatRace,
							coreCodeListMapperRace.getCodeListSubDesc());
				else
					setData(ProtocolKeys.PatRace, "");
			} else
				setData(ProtocolKeys.PatRace, "");

			XMLGregorianCalendar dob = patientDemographics.getDateOfBirth();
			if (dob != null)
				setData(ProtocolKeys.PatDOB, getDate(dob));
			else
				setData(ProtocolKeys.PatDOB, "");
			Code gender = patientDemographics.getGender();
			if (gender != null) {
				CoreCodeListMapper coreCodeListMapperGender = new CoreCodeListMapper();
				coreCodeListMapperGender.setCodeListType(gender.getType());
				coreCodeListMapperGender.setCodeListSubType(gender.getCode());
				coreMappingDao.getDescription(coreCodeListMapperGender);
				if (coreCodeListMapperGender.getCodeListSubDesc() != null)
					setData(ProtocolKeys.PatGender,
							coreCodeListMapperGender.getCodeListSubDesc());
				else
					setData(ProtocolKeys.PatGender, "");
			} else
				setData(ProtocolKeys.PatGender, "");

			Code ethnicity = patientDemographics.getEthnicity();
			if (ethnicity != null) {
				CoreCodeListMapper coreCodeListMapperEthnicity = new CoreCodeListMapper();
				coreCodeListMapperEthnicity
						.setCodeListType(ethnicity.getType());
				coreCodeListMapperEthnicity.setCodeListSubType(ethnicity
						.getCode());
				coreMappingDao.getDescription(coreCodeListMapperEthnicity);
				if (coreCodeListMapperEthnicity.getCodeListSubDesc() != null)
					setData(ProtocolKeys.PatEthnicity,
							coreCodeListMapperEthnicity.getCodeListSubDesc());
				else
					setData(ProtocolKeys.PatEthnicity, "");

			} else
				setData(ProtocolKeys.PatEthnicity, "");

			List<NvPair> morePatientDetails = patientDemographics
					.getMorePatientDetails();

			setData(ProtocolKeys.StudyNumber, studyIdentifier.getStudyNumber());

			/*
			 * StudySummary studySummary = new StudySummary(); studySummary =
			 * getStudySummary(studyIdentifier); List<NvPair>
			 * studySummaryDetails = studySummary .getMoreStudyDetails();
			 */

			argList.add(sessionToken);
			argList.add(coreMappingDao
					.getUniqueIdentifierSequence(ProtocolKeys.PreEnrollment
							.toString()));
			argList.add(studyPatientMap.get(ProtocolKeys.PatLastName));
			argList.add(studyPatientMap.get(ProtocolKeys.PatFirstName));
			argList.add(studyPatientMap.get(ProtocolKeys.PatDOB));
			argList.add(studyPatientMap.get(ProtocolKeys.PatGender));
			argList.add(studyPatientMap.get(ProtocolKeys.PatRace));
			argList.add(studyPatientMap.get(ProtocolKeys.PatEthnicity));
			argList.add(studyPatientMap.get(ProtocolKeys.SubjetMRN));
			argList.add(studyPatientMap.get(ProtocolKeys.MDACCPatient));
			argList.add(studyPatientMap.get(ProtocolKeys.StudyNumber));
			argList.add(studyPatientMap.get(ProtocolKeys.PreEnrollmentDate));
			argList.add(studyPatientMap.get(ProtocolKeys.PreEnrollmentNumber));
			argList.add(studyPatientMap
					.get(ProtocolKeys.PreEnrollmentInvestFirstName));
			argList.add(studyPatientMap
					.get(ProtocolKeys.PreEnrollmentInvestLastName));
			argList.add(studyPatientMap
					.get(ProtocolKeys.PreEnrollmentInvestUserId));
			argList.add(studyPatientMap
					.get(ProtocolKeys.PreEnrollmentInvestInstitution));
			argList.add(studyPatientMap
					.get(ProtocolKeys.PreEnrollmentInvestOncologyGrp));
			argList.add(studyPatientMap.get(ProtocolKeys.SignedConsentFormDate));
			argList.add(studyPatientMap.get(ProtocolKeys.Reason));
			argList.add(studyPatientMap.get(ProtocolKeys.OtherType));
			argList.add(studyPatientMap
					.get(ProtocolKeys.EligibilityQuestionAnswers));
			argList.add(studyPatientMap.get(ProtocolKeys.Eligible));
			argList.add(studyPatientMap.get(ProtocolKeys.PreEnrollmentComment));
			argList.add(studyPatientMap.get(ProtocolKeys.PreEnrollerFirstName));
			argList.add(studyPatientMap.get(ProtocolKeys.PreEnrollerLastName));
			argList.add(studyPatientMap.get(ProtocolKeys.PreEnrollerUserid));

			return argList;
		} catch (Exception e) {
			argList.add(e.getMessage());
			return argList;
		}

	}

	@Override
	public StudyPatientStatuses getStudyPatientStatusHistory(
			StudyIdentifier studyIdentifier, PatientIdentifier patientIdentifier)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList inpList = new MessageContentsList();
		inpList.add(studyIdentifier);
		inpList.add(patientIdentifier);
		MessageContentsList list = (MessageContentsList) producer.sendBody(
				"cxf:bean:espPatStudyStatusInfoEndpoint",
				ExchangePattern.InOut, inpList);
		return (StudyPatientStatuses) list.get(0);
	}

	@Override
	public PatientDemographics getPatientDemographics(
			PatientIdentifier patientIdentifier)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList list = (MessageContentsList) producer.sendBody(
				"cxf:bean:espPatientDemographics", ExchangePattern.InOut,
				patientIdentifier);
		return (PatientDemographics) list.get(0);
	}

	@Override
	public PatientEnrollmentDetails getStudyPatientStatus(
			PatientStudyStatusIdentifier patientStudyStatusIdentifier)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList list = (MessageContentsList) producer.sendBody(
				"cxf:bean:espStudyPatientStatus", ExchangePattern.InOut,
				patientStudyStatusIdentifier);
		return (PatientEnrollmentDetails) list.get(0);
	}

	@Override
	public Patient getPatientDetails(PatientIdentifier patientIdentifier)
			throws OperationException_Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public StudySummary getStudySummary(StudyIdentifier studyIdentifier)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList list = (MessageContentsList) producer.sendBody(
				"cxf:bean:espStudyPatientStatus", ExchangePattern.InOut,
				studyIdentifier);
		return (StudySummary) list.get(0);
	}

	@Override
	public Organizations getAllOrganizations()
			throws OperationException_Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public void setData(ProtocolKeys key, String value) {
		if (value == null || "".equals(value))
			studyPatientMap.put(key, "");
		else
			studyPatientMap.put(key, value);
	}

	@Override
	public FormList getListOfStudyPatientForms(
			PatientIdentifier patientIdentifier,
			StudyIdentifier studyIdentifier, int maxNumberOfResults,
			boolean formHasResponses) throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList inpList = new MessageContentsList();

		inpList.add(patientIdentifier);
		inpList.add(studyIdentifier);
		inpList.add(maxNumberOfResults);
		inpList.add(formHasResponses);
		MessageContentsList list = (MessageContentsList) producer.sendBody(
				"cxf:bean:espPatStudyStatusFormsInfoEndpoint",
				ExchangePattern.InOut, inpList);
		return (FormList) list.get(0);
	}

	@Override
	public StudyPatientFormResponses getListOfStudyPatientFormResponses(
			FormIdentifier formIdentifier, PatientIdentifier patientIdentifier,
			StudyIdentifier studyIdentifier, int pageNumber, int pageSize)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList inpList = new MessageContentsList();
		inpList.add(formIdentifier);
		inpList.add(patientIdentifier);
		inpList.add(studyIdentifier);
		inpList.add(pageNumber);
		inpList.add(pageSize);
		MessageContentsList list = (MessageContentsList) producer.sendBody(
				"cxf:bean:espPatStudyStatusFormResponseInfoEndpoint",
				ExchangePattern.InOut, inpList);
		return (StudyPatientFormResponses) list.get(0);
	}

	@Override
	public StudyPatientFormResponse getStudyPatientFormResponse(
			StudyPatientFormResponseIdentifier formResponseIdentifier)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList list = (MessageContentsList) producer.sendBody(
				"cxf:bean:espPatStudyStatusFormResponseDetailInfoEndpoint",
				ExchangePattern.InOut, formResponseIdentifier);
		return (StudyPatientFormResponse) list.get(0);
	}

	@Override
	public ObjectInfo getObjectInfoFromOID(SimpleIdentifier arg0)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList list = (MessageContentsList) producer.sendBody(
				"cxf:bean:espPatStudyEndpoint", ExchangePattern.InOut, arg0);
		return (ObjectInfo) list.get(0);
	}

	@Override
	public StudyPatientFormDesign getStudyPatientFormDesign(
			FormIdentifier formIdentifier, StudyIdentifier studyIdentifier,
			String formName, boolean includeFormatting)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList inpList = new MessageContentsList();
		inpList.add(formIdentifier);
		inpList.add(studyIdentifier);
		inpList.add(formName);
		inpList.add(includeFormatting);
		MessageContentsList list = (MessageContentsList) producer.sendBody(
				"cxf:bean:espPatStudyStatusFormDesignInfoEndpoint",
				ExchangePattern.InOut, inpList);
		return (StudyPatientFormDesign) list.get(0);
	}

	public static String getDate(XMLGregorianCalendar date) {
		StringBuilder stringDate = new StringBuilder();
		if (date.getMonth() > 9)
			stringDate.append(date.getMonth());
		else
			stringDate.append("0" + date.getMonth());

		stringDate.append("/");

		if (date.getDay() > 9)
			stringDate.append(date.getDay());
		else
			stringDate.append("0" + date.getDay());

		stringDate.append("/");

		stringDate.append(date.getYear());
		return stringDate.toString();
	}

}
