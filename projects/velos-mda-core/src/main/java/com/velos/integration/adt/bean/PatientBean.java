package com.velos.integration.adt.bean;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class PatientBean {

	private String eventType;
	private String patientID;
	private String lastName;
	private String firstName;
	private String middleName;
	private Date dateOfBirth;
	private String gender;
	private String race;
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String zipcode;
	private String country;
	private String county;
	private String hPhoneNumber;
	private String wPhoneNumber;
	private String ethnicity;
	private Date deathDate;
	private String survivalStatus;
	private String maritalStatus;
	private String ssn;
	private String additionalRace;
	private String additionalEthnicity;

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public String getPatientID() {
		return patientID;
	}

	public void setPatientID(String patientID) {
		this.patientID = patientID;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getRace() {
		return race;
	}

	public void setRace(String race) {
		this.race = race;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String gethPhoneNumber() {
		return hPhoneNumber;
	}

	public void sethPhoneNumber(String hPhoneNumber) {
		this.hPhoneNumber = hPhoneNumber;
	}

	public String getEthnicity() {
		return ethnicity;
	}

	public void setEthnicity(String ethnicity) {
		this.ethnicity = ethnicity;
	}

	public Date getDeathDate() {
		return deathDate;
	}

	public void setDeathDate(Date deathDate) {
		this.deathDate = deathDate;
	}

	public String getSurvivalStatus() {
		return survivalStatus;
	}

	public void setSurvivalStatus(String survivalStatus) {
		this.survivalStatus = survivalStatus;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public String getAdditionalRace() {
		return additionalRace;
	}

	public void setAdditionalRace(String additionalRace) {
		this.additionalRace = additionalRace;
	}

	public String getAdditionalEthnicity() {
		return additionalEthnicity;
	}

	public void setAdditionalEthnicity(String additionalEthnicity) {
		this.additionalEthnicity = additionalEthnicity;
	}

	public String getwPhoneNumber() {
		return wPhoneNumber;
	}

	public void setwPhoneNumber(String wPhoneNumber) {
		this.wPhoneNumber = wPhoneNumber;
	}

	public Date returnDate(String dateString) {
		Date parsed = null;
		if (dateString == null || "".equals(dateString)) {
			return null;
		} else {
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");

			try {
				parsed = format.parse(dateString);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return parsed;
	}
}
