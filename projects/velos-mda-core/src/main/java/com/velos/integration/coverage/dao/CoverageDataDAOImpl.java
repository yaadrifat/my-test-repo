package com.velos.integration.coverage.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

public class CoverageDataDAOImpl {

	private static Logger logger = Logger.getLogger(CoverageDataDAOImpl.class
			.getName());

	private JdbcTemplate jdbcTemplate;
	private JdbcTemplate mdaJdbcTemplate;
	List<CoverageDataFromCA> list = new ArrayList<CoverageDataFromCA>();
	
	private PlatformTransactionManager platformTransactionManager;

	public void setPlatformTransactionManager(
			PlatformTransactionManager platformTransactionManager) {
		this.platformTransactionManager = platformTransactionManager;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
		
	public void setMdaDataSource(DataSource dataSource) {
		this.mdaJdbcTemplate = new JdbcTemplate(dataSource);
		logger.info("mdaDatasource and jdbcTemplate created successfully");
	}

	
	public List<CoverageDataFromCA> getAllCoverageDataFromCA() {

		String sql = "SELECT * FROM CA_CRMS_INTERFACE ";

		if (this.mdaJdbcTemplate != null) {
			@SuppressWarnings("unchecked")
			List<Map<String, Object>> rows = this.mdaJdbcTemplate.queryForList(sql);
			for (Map<String, Object> resultSet : rows) {

				CoverageDataFromCA covCa = new CoverageDataFromCA();

				covCa.setStudyNumber((String) resultSet.get("PROTOCOL_NUMBER"));
				covCa.setCalendarName((String) resultSet.get("CALENDAR_NAME"));
				covCa.setCptCode((String) resultSet.get("CPT_CODE"));
				covCa.setEventName((String) resultSet.get("FULL_SERVICE_NAME"));
				covCa.setVisitName((String) resultSet.get("VISIT_NAME"));

				covCa.setProfChargeCode((String) resultSet
						.get("PROFESSIONAL_CHARGE_CODE"));
				covCa.setProfFeeDiscount((BigDecimal) resultSet
						.get("PROFESSIONAL_FEE_DISCOUNT"));
				covCa.setAdjustedProfFee((BigDecimal) resultSet
						.get("ADJUSTED_PROFESSIONAL_FEE"));

				covCa.setTechChargeCode((String) resultSet
						.get("TECHNICAL_CHARGE_CODE"));
				covCa.setTechFeeDiscount((BigDecimal) resultSet
						.get("TECHNICAL_FEE_DISCOUNT"));
				covCa.setAdjustedTechFee((BigDecimal) resultSet
						.get("ADJUSTED_TECHNICAL_FEE"));				

				covCa.setModifier((String) resultSet.get("MODIFIER"));
				covCa.setCaXlsRow((BigDecimal) resultSet.get("CA_XLS_ROW"));
				covCa.setSponsor((String) resultSet.get("SPONSOR"));
				covCa.setSponsorMdaccId((String) resultSet
						.get("SPONSOR_MDACC_ID"));
				
				covCa.setAddtionalComments((String) resultSet.get("COMMENTS"));

				list.add(covCa);
			}
		}
		return list;
	}
	
	public void updateCoverageDataInCTMS(List<CoverageDataFromCA> caList) {
		logger.debug("In updateCoverageDataInCTMS method and size of list is: " + caList.size());				
		List<CoverageDataFromCA> studyList = new ArrayList<CoverageDataFromCA>();		
		String sql = "select distinct protocol_number from CA_CRMS_INTERFACE";		
		
		@SuppressWarnings("rawtypes")
		List studyIdList = mdaJdbcTemplate.queryForList(sql);		
		logger.info("Total number of Studies coming from CA_CRMS_INTERFACE Table : " + studyIdList.size());
		String studyNum="";
		
		if(studyIdList.size() < 1) {
			return;
		}		
		for(int i=0; i<studyIdList.size(); i++) {
			for(int k=0; k<caList.size(); k++) {					
				studyNum = (caList.get(k)).getStudyNumber();
				studyNum="{PROTOCOL_NUMBER="+ studyNum + "}";					
				if( (studyNum).equals((studyIdList.get(i)).toString())) {
					studyList.add(caList.get(k));						
				}					
			}
			logger.info("Study : " + (studyIdList.get(i)).toString() + " size: " + studyList.size());
			try {
				updateCoverageDataForStudyInCTMS(studyList);					
				studyList.clear();									
			} catch( Exception e) {
				studyList.clear();
				logger.info("Exception caught in updateCoverageDataInCTMS method" + e.getMessage());
			}				
		}	

	}	
	
	public void updateCoverageDataForStudyInCTMS(List<CoverageDataFromCA> objList)  	 {

		logger.debug("In updateCoverageDataForStudyInCTMS method and size of list is: "	+ objList.size());	
		
		DefaultTransactionDefinition paramTransactionDefinition = new    DefaultTransactionDefinition();
		TransactionStatus status=platformTransactionManager.getTransaction(paramTransactionDefinition );
		int row=0;
		try {				
			for (int j = 0; j < objList.size(); j++) {
				row++;
				String sql = "select es.study_number, es.pk_study, ea.chain_id, ea.name as name, sc.codelst_subtyp as codelst_subtyp"
						+ " from er_study es,  event_assoc ea, sch_codelst sc"
						+ " where es.study_number ="
						+ "'"
						+ ((objList.get(j)).getStudyNumber()).trim()
						+ "'"
						+ " and es.pk_study = ea.chain_id "
						+ " and ea.name = "
						+ "'"
						+ ((objList.get(j)).getCalendarName()).trim()
						+ "'"
						+ " and ea.event_calassocto='P'"
						+ "  and ea.fk_codelst_calstat = sc.pk_codelst";
				
				logger.debug("sql is : " + sql);
				@SuppressWarnings("unchecked")
				List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
				logger.info("Row : " + row);
	
				logger.debug("rows.size() is: " + rows.size());
				if (rows.size() == 0) {
					logger.error("Study : "
							+ ((objList.get(j)).getStudyNumber()).trim() + " or "
							+ "Calendar : "
							+ ((objList.get(j)).getCalendarName()).trim()
							+ " is not present in CRMS database");				
					platformTransactionManager.rollback(status);
					logger.error("Transaction rolled back :");
					logger.info("**************************************************************************************");
					return;
				} else {
					logger.info("Service : " +  (objList.get(j)).getEventName() );
					logger.info("Visit : " + (objList.get(j)).getVisitName());
					for (Map<String, Object> rs : rows) {
						logger.info("Calendar Name : " + "  "
								+ (String)rs.get("NAME") + "  "
								+ " And Calendar Status : " + (String)rs.get("CODELST_SUBTYP"));
	
						String code = ((String)rs.get("CODELST_SUBTYP")).trim();
						logger.debug("code is : " + code);
	
						if (code.equals("A") || code.equals("D")
								|| code.equals("R")) {
							logger.error("Can not update modifier because calendar status is Active or Deactived or Reactivated");
							platformTransactionManager.rollback(status);
							logger.error("Transaction rolled back :");
							logger.info("**************************************************************************************");
							return;
	
						} else {
						
							int eventId=0;
							
							String eveNameStr = ((objList.get(j)).getEventName()).trim();
							String eventName="";
							if(eveNameStr.startsWith("'")) {
								eventName = eveNameStr.substring(1,eveNameStr.length());
							} else {
								eventName=((objList.get(j)).getEventName()).trim();
							}
							@SuppressWarnings("rawtypes")
							List eventIdList = getEventId(((objList.get(j)).getStudyNumber()).trim(),((objList.get(j)).getCalendarName()).trim(), ((objList.get(j)).getVisitName()).trim(), eventName );
							if(eventIdList.size() == 0 ) {
								@SuppressWarnings("rawtypes")
								List eventIdList1 = getEventIdWhenNoEventNameMatch(((objList.get(j)).getStudyNumber()).trim(),((objList.get(j)).getCalendarName()).trim(), ((objList.get(j)).getVisitName()).trim(), eventName );
								if(eventIdList1.size() == 0 ) {
									logger.error("EventId for given combination of protocol_number, calender_name and visit_name and event_name is not present in CRMS.");
									platformTransactionManager.rollback(status);
									logger.error("Transaction rolled back :");
									logger.info("**************************************************************************************");
									return;
								} else if(eventIdList1.size() > 1) {
									logger.error("List(more than 1) of eventId's are present for given combination of protocol_number, calender_name and visit_name and event_name in CRMS.");
									platformTransactionManager.rollback(status);
									logger.error("Transaction rolled back :");
									logger.info("**************************************************************************************");
									return;
								} else if (eventIdList1.size() == 1) {								
									String eventIdStr=(eventIdList1.get(0)).toString();
									StringTokenizer str = new StringTokenizer(eventIdStr,"=");
									String token="";
									while(str.hasMoreTokens()) {
										token=str.nextToken();
									}								
									eventId= Integer.valueOf(token.substring(0, token.length()-1));									
								}
							} else if(eventIdList.size() > 1) {
								logger.error("List(more than 1) of eventId's are present for given combination of protocol_number, calender_name and visit_name and event_name in CRMS.");
								platformTransactionManager.rollback(status);
								logger.error("Transaction rolled back :");
								logger.info("**************************************************************************************");
								return;
							} else if (eventIdList.size() == 1) {								
								String eventIdStr=(eventIdList.get(0)).toString();
								StringTokenizer str = new StringTokenizer(eventIdStr,"=");
								String token="";
								while(str.hasMoreTokens()) {
									token=str.nextToken();
								}								
								eventId= Integer.valueOf(token.substring(0, token.length()-1));									
							}
							
							logger.debug("eventId : " + eventId);							
							if (eventId != 0) {									
															
								if( ((objList.get(j)).getCptCode() != null)) {
									logger.info("CPT Code : " + (objList.get(j)).getCptCode());
									String cptCode = getCptCode(eventId);	
									logger.debug("CPT Code from CRMS : " + cptCode);									
									String cptCodeStr = "{EVENT_CPTCODE=" + (objList.get(j)).getCptCode() + "}";
									logger.debug("CPT Code from CA Table : " + cptCodeStr);								
									if( !(cptCodeStr.equals(cptCode))) {
										logger.info("CPT Code from CRMS : " + cptCode + " And CPT Code from CA_CRMS_INTERFACE Table : " + cptCodeStr);
										platformTransactionManager.rollback(status);
										logger.error("Transaction rolled back : CPT Code does not match");
										logger.info("**************************************************************************************");
										return;
									}									
								}
								
								if( ((objList.get(j)).getProfChargeCode() != null)) {
									logger.info("PRS Code : " + (objList.get(j)).getProfChargeCode());
									String profStrCd = (objList.get(j)).getProfChargeCode();
									if(profStrCd.startsWith("'")) {
										profStrCd = profStrCd.substring(1,profStrCd.length());
									}
									String sCode = getServiceCode(eventId, "PRS_CODE");	
									logger.debug("Professional Code from CRMS : " + sCode);									
									String sCodeStr = "{MD_MODELEMENTDATA=" + profStrCd + "}"; 
									logger.debug("Professional Code from CA Table : " + sCodeStr);
									if( !(sCodeStr.equals(sCode))) {
										logger.info("Professional Code from CRMS : " + sCode + " And Professional Code from CA_CRMS_INTERFACE Table : " + sCodeStr);
										platformTransactionManager.rollback(status);
										logger.error("Transaction rolled back : PRS Code does not match");
										logger.info("**************************************************************************************");
										return;
									}									
								}
								
								if( ((objList.get(j)).getTechChargeCode() != null)) {
									logger.info("Technical Code : " + (objList.get(j)).getTechChargeCode());
									String techStrCd = (objList.get(j)).getTechChargeCode();
									if(techStrCd.startsWith("'")) {
										techStrCd = techStrCd.substring(1,techStrCd.length());
									}
									String sCode = getServiceCode(eventId, "SVC_CD");	
									logger.debug("Technical Code from CRMS : " + sCode);
									logger.debug("techStrCd : " + techStrCd);									
									String sCodeStr = "{MD_MODELEMENTDATA=" + techStrCd + "}";																		
									if( !(sCodeStr.equals(sCode))) {
										logger.info("Technical Code from CRMS : " + sCode + " And Technical Code from CA_CRMS_INTERFACE Table : " + sCodeStr);
										platformTransactionManager.rollback(status);
										logger.error("Transaction rolled back : Technical Code does not match");
										logger.info("**************************************************************************************");
										return;
									}									
								}								
								
								int modifier = getPkForModifer((objList.get(j)).getModifier());
								updateModifier(modifier, eventId, (objList.get(j)).getModifier());									
								
								String notes=(objList.get(j)).getAddtionalComments();
								if( notes == null ) {
									notes="";
								}
								updateNotes(notes, eventId);
								
								if ((objList.get(j)).getProfChargeCode() == null) {
									if (((objList.get(j)).getAdjustedProfFee() != null)) {
										logger.warn("Adjusted Professional Fee is present without PRS code : " + (objList.get(j)).getAdjustedProfFee());											
									}									
								}
								
								int prs_pk = getPkOfSchCodelst("PRS PRICE");
								logger.debug("PRS PRICE pk : " + prs_pk);										
								updateAdjustedFees(eventId, prs_pk, (objList.get(j)).getAdjustedProfFee(), "Professional Fee" );
								
								if((objList.get(j)).getTechChargeCode() == null) {
									if((objList.get(j)).getAdjustedTechFee() != null) {
										logger.warn("Adjusted Technical Fee is present without Technical Code : " + (objList.get(j)).getAdjustedTechFee());
									}									
								}
								
								int svc_pk = getPkOfSchCodelst("SVC PRICE");
								logger.debug("SVC PRICE pk : " + svc_pk);										
								updateAdjustedFees(eventId, svc_pk, (objList.get(j)).getAdjustedTechFee(), "Technical Fee");
								
								if ((objList.get(j)).getProfChargeCode() == null) {
									if (((objList.get(j)).getProfFeeDiscount() != null)) {
										logger.warn("Professional Fee Discount is present without PRS code : " + (objList.get(j)).getProfFeeDiscount());											
									}									
								}
								int pk_prof_discount = getPkOfErCodelstTable("prs_discount");
								logger.debug("pk_prof_discount : " + pk_prof_discount);	
								String profDis="";
								if((objList.get(j)).getProfFeeDiscount() != null) {
									profDis=(objList.get(j)).getProfFeeDiscount().toString();
								} else {
								    profDis=null;
								}
								updateEventMoreDetails(eventId, pk_prof_discount , profDis, "Professional Fee Discount");	
								
								if((objList.get(j)).getTechChargeCode() == null) {
									if((objList.get(j)).getTechFeeDiscount() != null) {
										logger.warn("Technical Fee Discount is present without Technical Code : " + (objList.get(j)).getTechFeeDiscount());
									}									
								}
								int pk_tech_discount = getPkOfErCodelstTable("hcc_discount");
								logger.debug("pk_tech_discount : " + pk_tech_discount);		
								String techDis= "";
								if( (objList.get(j)).getTechFeeDiscount() != null )  {
									techDis= (objList.get(j)).getTechFeeDiscount().toString();
								} else {
									techDis = null;
								}
								 
								updateEventMoreDetails(eventId, pk_tech_discount , techDis, "Technical Fee Discount");								
								
								int pk_sponsor_id = getPkOfErCodelstTable("sponsor_id");
								logger.debug("pk_sponsor_id : " + pk_sponsor_id);	
								String sponsorId = getSponsorData(eventId, pk_sponsor_id);
								String sId="";
								if( sponsorId != null ) {
									logger.info( "SponsorId from CRMS : " + sponsorId.substring(19, (sponsorId.length()-1)));
								} else {
									logger.info( "SponsorId from CRMS :  null");
								}
								if( (objList.get(j)).getSponsorMdaccId() != null ) {
									sId = (objList.get(j)).getSponsorMdaccId();
									logger.info("SponsorId from CA_CRMS_INTERFACE table: " + sId );
								} else {
									logger.info("SponsorId from CA_CRMS_INTERFACE table: null ");
								}
								updateEventMoreDetails(eventId, pk_sponsor_id , sId, "Sponsor Id");								
								
								int pk_sponsor = getPkOfErCodelstTable("sponsor");
								logger.debug("pk_sponsor : " + pk_sponsor);	
								String sponsorName = getSponsorData(eventId, pk_sponsor );
								String sponsor="";
								if( sponsorName != null ) {
									logger.info("SponsorName From CRMS : " + sponsorName.substring(19, (sponsorName.length()-1)));
								} else {
									logger.info("SponsorName From CRMS : null ");
								}
								if((objList.get(j)).getSponsor() != null ) {
									sponsor =  (objList.get(j)).getSponsor();
									logger.info("SponsorName from CA_CRMS_INTERFACE table : " + sponsor);
								} else {
									logger.info("SponsorName from CA_CRMS_INTERFACE table : null");
								}
								updateEventMoreDetails(eventId, pk_sponsor , sponsor, "Sponsor Name");							
								
							} 
						}
					}
				}						
			}			
			
			platformTransactionManager.commit(status);		
			logger.info("Transaction is commited : Updated study successfully in CRMS ");
			logger.info("**************************************************************************************");			
		} catch(Exception e2) {
			logger.error("Before rolling back from catch block of updateCoverageDataForStudyInCTMS method : " + e2.getMessage() );
			platformTransactionManager.rollback(status);
			logger.error("Transaction is rolled back: ");
			logger.info("**************************************************************************************");
		}
	}
	
	@SuppressWarnings("rawtypes")
	List getEventId(String study_number, String calendar_name, String visit_name, String event_name) {	
		String selectSql = " select ea.event_id "
				+ " from event_assoc ea, sch_protocol_visit spv "
				+ " where chain_id = ( "
				+ " select ea.event_id "
				+ " from er_study es,  ESCH.event_assoc ea "
				+ " where es.study_number = " + "'"
				+ study_number + "'"
				+ " and es.pk_study = ea.chain_id "
				+ " and ea.name = " + "'"
				+ calendar_name + "'"
				+ " and ea.event_calassocto='P') "
				+ " and fk_visit = spv.pk_protocol_visit "
				+ " and spv.visit_name= " + "'"
				+ visit_name + "'"
			    +  " and ea.name= " + "'"
			    + event_name + "'";
		
		logger.debug("Sql for selecting study_number, calender_name, event_name and visit name combination : \n " + selectSql);
				
		List evtIdList =jdbcTemplate.queryForList(selectSql);
		return evtIdList;		
	}
	
	
	@SuppressWarnings("rawtypes")
	List getEventIdWhenNoEventNameMatch(String study_number, String calendar_name, String visit_name, String event_name) {	
		String selectSql = " select ea.event_id "
				+ " from event_assoc ea, sch_protocol_visit spv "
				+ " where chain_id = ( "
				+ " select ea.event_id "
				+ " from er_study es,  ESCH.event_assoc ea "
				+ " where es.study_number = " + "'"
				+ study_number + "'"
				+ " and es.pk_study = ea.chain_id "
				+ " and ea.name = " + "'"
				+ calendar_name + "'"
				+ " and ea.event_calassocto='P') "
				+ " and fk_visit = spv.pk_protocol_visit "
				+ " and spv.visit_name= " + "'"
				+ visit_name + "'"
				+ " and regexp_replace(ea.name, '[[:space:]]*','') = regexp_replace(' " + event_name + " ', '[[:space:]]*','')";  				
				
		logger.debug("Sql for selecting study_number, calender_name, event_name and visit name combination : \n " + selectSql);
				
		List evtIdList =jdbcTemplate.queryForList(selectSql);
		return evtIdList;		
	}
	
	String getCptCode(int eventId){
		String cptCodeSql = " select event_cptcode from event_assoc "
				+ " where event_id = " + eventId;		
		@SuppressWarnings("rawtypes")
		List cptList = jdbcTemplate.queryForList(cptCodeSql);	
		String cptCode="";
		if( cptList.size() > 0 ) {
			logger.debug("CPT Code : " + cptList.get(0));	
			cptCode=(cptList.get(0)).toString();
		}
		return cptCode;
		
	}
	
	String getServiceCode(int eventId, String subtype) {		
		String sql = " select md_modelementdata from er_moredetails where fk_modpk= "
				+ eventId + " and  md_modelementpk = (select pk_codelst from er_codelst where codelst_subtyp='" + subtype + "')";
		
		@SuppressWarnings("rawtypes")
		List codeList = jdbcTemplate.queryForList(sql);	
		String sCode="";
		if( codeList.size() > 0 ) {
			logger.debug("CPT Code : " + codeList.get(0));	
			sCode=(codeList.get(0)).toString();
		}
		return sCode;		
	}
	
	void updateModifier(int modifier, int eventId, String sModifier) {		
		String updateSql = " update event_assoc"
				+ " set fk_codelst_covertype= " + modifier
				+ " where event_id = " + eventId;		
		int result1 = jdbcTemplate.update(updateSql);
		if( result1 > 0 ) {
			logger.info("Updated Modifier : New value : " + sModifier);	
		}		
	}	
	
	void updateNotes(String addtionalComments, int eventId) {		
		String updateNotesSql = " update event_assoc"
				+ " set notes= ' " + addtionalComments + "'"
				+ " where event_id = " + eventId;		
		int result1 = jdbcTemplate.update(updateNotesSql);
		if( result1 > 0 ) {
			logger.info("Updated Notes : New value : " + addtionalComments);	
		}		
	}
	
	void updateAdjustedFees(int eventId, int pk, BigDecimal fee, String subtype){
		int size = checkRowExists(eventId, pk);								
		if( size == 0 ) {
			if( pk != 0 ) {
				insertIntoSchEventcost(eventId, pk, fee, subtype);
			}
		} else {									
			if( pk != 0 ) {
				updateSchEventcost(eventId, pk, fee, subtype);										
			}
		}		
	}	
	
	void updateEventMoreDetails(int eventId, int pk_er_codelst , String data, String subtype) {		
		int size = getPkOfMoreDetails(eventId, pk_er_codelst);		
		if( size == 0) {									
			if (pk_er_codelst != 0) {
				insertIntoMoreDetails(eventId, pk_er_codelst, data, subtype);										
			}									
		} else {								
			if (pk_er_codelst != 0) {										
				updateMoreDetails(eventId, pk_er_codelst, data, subtype);												
			}
		}		
	}
	
	int checkRowExists(int eventId, int pk) {		
		String selSql=" select pk_eventcost from sch_eventcost where fk_event = " + eventId + " and fk_cost_desc = " + pk ;		
		@SuppressWarnings("rawtypes")
		List pkList = jdbcTemplate.queryForList(selSql);
		logger.debug("sql is: " + selSql);
		logger.debug("Eventcost pkList size is :" + pkList.size());
		if(pkList.size() != 0 ) {
			String element = (pkList.get(0)).toString();			
			logger.debug("element is : " + element);
		}
		return pkList.size();		
	}
	
	int getPkOfSchCodelst(String codelst_subtyp) {
		String sql = "select pk_codelst from sch_codelst where codelst_type ='cost_desc' and codelst_subtyp = '" + codelst_subtyp + "'";
		int pk = jdbcTemplate.queryForInt(sql);
		return pk;
	}
	
	void insertIntoSchEventcost(int eventId, int pk, BigDecimal fee , String subtype) {		
		String insertSchEventCost="INSERT INTO SCH_EVENTCOST (PK_EVENTCOST, EVENTCOST_VALUE, FK_EVENT, FK_COST_DESC, FK_CURRENCY) VALUES (SCH_EVENTCOST_SEQ.NEXTVAL," + fee +"," + eventId + "," + pk + "," + "1)";
		int result = jdbcTemplate.update(insertSchEventCost);
		if( result > 0 ) {
			logger.info("Updated Adjusted " + subtype + " : New Value : " +  fee );
		}		
	}
	
	void updateSchEventcost(int eventId, int pk, BigDecimal fee, String subtype) {
		String updateSchEventCost = " update sch_eventcost set eventcost_value = " +  fee + 
				" where fk_event = " + eventId + " and fk_cost_desc = " + pk ;		
		int result = jdbcTemplate.update(updateSchEventCost);	
		if( result > 0 ) {
			logger.info("Updated Adjusted " + subtype + " : New Value : " +  fee );
		}
	}
	
	int getPkForModifer(String caModifier) {
		logger.debug("Modifier "	+ caModifier);
		String getPKForModiferSql = " select pk_codelst from sch_codelst where codelst_subtyp = '"+ caModifier + "'";
		logger.debug("getPKForModiferSql is : " + getPKForModiferSql);
		int modifierpk = jdbcTemplate.queryForInt(getPKForModiferSql);
		logger.debug("modifierpk : " + modifierpk);
		return modifierpk;
	}

	int getPkOfErCodelstTable(String codelst_subtyp) {
		String sql = "select pk_codelst from er_codelst where codelst_type='evtaddlcode' and codelst_subtyp = '" + codelst_subtyp + "'";
		int pk = jdbcTemplate.queryForInt(sql);
		return pk;
	}
	
	void insertIntoMoreDetails(int eventId, int pk_codelst , String modElementData , String subtype) {		
		String insertMoreDetailsRow = "INSERT INTO ER_MOREDETAILS(PK_MOREDETAILS, FK_MODPK, MD_MODNAME, MD_MODELEMENTPK, MD_MODELEMENTDATA ) VALUES(SEQ_ER_MOREDETAILS.NEXTVAL," + eventId + "," + "'evtaddlcode'," + pk_codelst + "," + "'"+ modElementData + "')"; 
		logger.debug("insertMoreDetailsRow: " + insertMoreDetailsRow);
		int result = jdbcTemplate.update(insertMoreDetailsRow);
		if( result > 0) {
			logger.info("Updated " + subtype + " :  New Value : " + modElementData );
		}
	}
	
	void updateMoreDetails(int eventId, int pk_codelst , String modElementData, String subtype) {		
		String updateRowSql = " update er_moredetails set md_modelementdata = '" + modElementData + "' where fk_modpk = " + eventId + "and md_modelementpk = " + pk_codelst ; 
		int result = jdbcTemplate.update(updateRowSql);
		if( result > 0) {
			logger.info("Updated " + subtype + " :  New Value : " + modElementData);		
		}
	}
	
	int getPkOfMoreDetails(int eventId, int pk_er_codelst) {
		String selSql=" select pk_moredetails from er_moredetails where fk_modpk = " + eventId + "and md_modelementpk = " + pk_er_codelst  ;		
		@SuppressWarnings("rawtypes")
		List pkList = jdbcTemplate.queryForList(selSql);		
		logger.debug("MoreDetails pkList size is :" + pkList.size() );
		return pkList.size();
	}
	
	String getSponsorData(int eventId, int pk_sponsor){
		String sql = "select md_modelementdata from er_moredetails where fk_modpk= " + eventId + " and md_modelementpk= " + pk_sponsor;
		@SuppressWarnings("rawtypes")
		List sponsorDetails = jdbcTemplate.queryForList(sql);
		String sData=null;
		if(sponsorDetails.size() > 0) {
			sData = (sponsorDetails.get(0)).toString();			
		}
		return sData;
	}	
}
	
	
	