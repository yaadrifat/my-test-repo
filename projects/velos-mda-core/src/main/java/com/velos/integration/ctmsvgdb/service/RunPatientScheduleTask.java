package com.velos.integration.ctmsvgdb.service;

import java.sql.SQLException;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import com.velos.integration.ctmsvgdb.dao.PatientScheduleDataDAOImpl;
import com.velos.integration.ctmsvgdb.dao.PatientScheduleInfo;

public class RunPatientScheduleTask implements ApplicationContextAware {

	private static Logger logger = Logger
			.getLogger(RunPatientScheduleTask.class.getName());

	private static List<PatientScheduleInfo> pSchList;
	private PatientScheduleDataDAOImpl pSchBean = null;
	ApplicationContext context=null;

	public PatientScheduleDataDAOImpl getpSchBean() {
		return pSchBean;
	}

	public void setpSchBean(PatientScheduleDataDAOImpl pSchBean) {
		this.pSchBean =  pSchBean;
		logger.info("Bean pSchBean is created successfully.");
	}

	public void runTask() throws SQLException {

		try {
			if (pSchBean != null) {

				pSchList = pSchBean.getPatientSchdulingInfoFromCTMS();
				
				logger.info("Method getPatientSchdulingInfoFromCTMS : patientList size : "
						+ pSchList.size());
				
				if (pSchList != null || pSchList.size() != 0) {
					pSchBean.insertPatientSchdulingInfoInVGDB(pSchList);
					logger.info("Patient Schedule Data is inserted into VGDB successfully. ");
				}

				pSchList.clear();				
			}

		} catch (Exception e) {
			logger.info("Exception caught in runTask() method of RunPatientScheduleTask : "
					+ e.getMessage());
		} finally
		{
			DriverManagerDataSource ds = (DriverManagerDataSource)(this.context).getBean("vgdbDataSource");
			DriverManagerDataSource ds1 = (DriverManagerDataSource)(this.context).getBean("eresDataSource");
			(ds.getConnection()).close();
			logger.info("closed : ds.getConnection()");
			(ds1.getConnection()).close();
			logger.info("closed : ds1.getConnection()");			
		}

	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.context=applicationContext;
		
	}

}
