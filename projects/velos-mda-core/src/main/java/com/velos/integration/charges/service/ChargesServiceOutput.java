package com.velos.integration.charges.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.jdbc.support.lob.LobHandler;

import com.velos.integration.charges.bean.FolderMapping;
import com.velos.integration.charges.bean.SaveChargeFileBean;
import com.velos.integration.charges.dao.ChargesDaoImpl;

public class ChargesServiceOutput {
	LobHandler lobHandler;
	ChargesDaoImpl chargesDao;

	public ChargesDaoImpl getChargesDao() {
		return chargesDao;
	}

	public void setChargesDao(ChargesDaoImpl chargesDao) {
		this.chargesDao = chargesDao;
	}

	public LobHandler getLobHandler() {
		return lobHandler;
	}

	public void setLobHandler(LobHandler lobHandler) {
		this.lobHandler = lobHandler;
	}

	private static Logger logger = Logger.getLogger(ChargesDaoImpl.class);

	public String listFilesForFolder(final File folder) {
		for (final File fileEntry : folder.listFiles()) {
			if (fileEntry.isDirectory()) {
				listFilesForFolder(fileEntry);
			} else {
				return fileEntry.getName();
			}
		}
		return "";
	}

	public static java.util.Date stringToDate(String dtString, String format) {
		java.util.Date dt = null;
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		try {
			dt = sdf.parse(dtString);
		} catch (Exception e) {
		}
		return dt;
	}

	public void processOutputFile(String chargeType) {
		FolderMapping mapper = this.loadProperties(chargeType);
		
		List<String> files = chargesDao.getOutPutFiles(mapper.getChargeTypeDesc());

		
		String folderName = mapper.getClientOutFiles();

		String fileName = "";
		String fileNameWithFolder = "";

		for (String file : files) {
			File filefolder = new File(folderName + "/" + file);
			SaveChargeFileBean saveChargeFileBean = new SaveChargeFileBean();
			saveChargeFileBean.setFile(filefolder);

			saveChargeFileBean.setFileType(mapper.getChargeTypeDesc());

			saveChargeFileBean.setLobHandler(this.getLobHandler());

			chargesDao.updateOutPutFiles(saveChargeFileBean);

			}
	}

	FolderMapping loadProperties(String folder) {
		FolderMapping folderMapping = new FolderMapping();
		Properties prop = new Properties();
		try {
			prop.load(this.getClass().getClassLoader()
					.getResourceAsStream("poc-config.properties"));
			folderMapping.setChargeTypeDesc(prop.getProperty(folder + "_desc"));
			folderMapping.setClientOutFiles(prop.getProperty(folder
					+ "ClientProcessedPassed"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return folderMapping;
	}

}
