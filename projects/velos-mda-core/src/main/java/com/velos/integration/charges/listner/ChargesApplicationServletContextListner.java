package com.velos.integration.charges.listner;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;
import org.quartz.Scheduler;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.velos.integration.ctmsvgdb.dao.PatientScheduleDataDAOImpl;
import com.velos.integration.ctmsvgdb.dao.PatientScheduleInfo;
import com.velos.integration.init.AppServletContextListener;

public class ChargesApplicationServletContextListner implements
		ServletContextListener {

	private static Logger logger = Logger
			.getLogger(ChargesApplicationServletContextListner.class.getName());

	private ApplicationContext context;

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		logger.info("ServletContextListener destroyed");
		try {
			// Get a reference to the Scheduler and shut it down
			logger.info("Context is : " + context);
			Scheduler scheduler = (Scheduler) ((AbstractApplicationContext) context)
					.getBean("quartzSchedulerFactoryCharges");
			logger.info("Scheduler is : " + scheduler);
			scheduler.shutdown(true);

			logger.info("Started scheduler shutdown: ");
			// Sleep for a bit so that we don't get any errors
			Thread.sleep(10000);

		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.info("Scheduler Shutdown complete");

	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		logger.info("ServletContextListener started");
		try {
			context = new ClassPathXmlApplicationContext("camel-config-charges.xml");
			System.out.println("ChargesApplicationServletContextListner started....................");
		} catch (Exception e) {
			System.out.println("ChargesApplicationServletContextListner exception....................");
			logger.info("Exception caught in contextInitialized : "
					+ e.getMessage());
		}

	}

}
