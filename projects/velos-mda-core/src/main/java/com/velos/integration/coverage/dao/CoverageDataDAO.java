package com.velos.integration.coverage.dao;

import java.util.List;

public interface CoverageDataDAO {
	
	public List<CoverageDataFromCA> getAllCoverageDataFromCA();
	public void updateCoverageTypeInCTMS(List<CoverageDataFromCA> objList);

}
