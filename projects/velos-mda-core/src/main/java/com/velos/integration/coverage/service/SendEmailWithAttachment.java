package com.velos.integration.coverage.service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.springframework.mail.MailParseException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.apache.log4j.Logger;

public class SendEmailWithAttachment {
	private static Logger logger = Logger
			.getLogger(SendEmailWithAttachment.class.getName());
	
	private JavaMailSender mailSender;
	private SimpleMailMessage simpleMailMessage;
 
	public void setSimpleMailMessage(SimpleMailMessage simpleMailMessage) {
		this.simpleMailMessage = simpleMailMessage;
	}
 
	public void setMailSender(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}
 
	public void sendMail(String dear, String content) throws IOException {
 
	   MimeMessage message = mailSender.createMimeMessage();
	   InputStream input=null;
 
	   try{
		   	Properties prop = new Properties();
			
		   	input = this.getClass().getClassLoader()
					.getResourceAsStream("email.properties");			
			prop.load(input);
			
			String enviornment = prop.getProperty("enviornment");   
			MimeMessageHelper helper = new MimeMessageHelper(message, true);
	 
			helper.setFrom(simpleMailMessage.getFrom());
			helper.setTo(simpleMailMessage.getTo());
			helper.setSubject(enviornment + " : " + simpleMailMessage.getSubject());
			helper.setText(String.format(
				simpleMailMessage.getText(), "" , content));		
			
			String fileLoc = new File(System.getProperty("catalina.base"))+ "/logs/coverage.log";
			logger.info("Log File location : " + fileLoc);
			File file = new File(fileLoc);			
			helper.addAttachment("coverage.log", file);
	 
	     }catch (MessagingException e) {
	    	 throw new MailParseException(e);
	     }finally {
				if (input != null) {
					try {
						input.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
	     	mailSender.send(message);
         }
}
