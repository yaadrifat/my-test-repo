package com.velos.integration.core.processor;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.camel.CamelContext;
import org.apache.camel.ExchangePattern;
import org.apache.camel.ProducerTemplate;
import org.apache.cxf.message.MessageContentsList;
import org.apache.cxf.ws.security.wss4j.WSS4JOutInterceptor;
import org.apache.log4j.Logger;
import org.apache.ws.security.WSConstants;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.integration.MessageChannel;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.velos.integration.core.dao.CoreMappingDao;
import com.velos.integration.core.mapping.CoreCodeListMapper;
import com.velos.integration.core.mapping.ProtocolKeys;
import com.velos.integration.core.messaging.Change;
import com.velos.integration.core.messaging.CustomHandler;
import com.velos.integration.core.messaging.IdentifierConstants;
import com.velos.integration.core.messaging.SendEmail;
import com.velos.services.Code;
import com.velos.services.ObjectInfo;
import com.velos.services.OperationException_Exception;
import com.velos.services.PatientIdentifier;
import com.velos.services.PatientProtocolIdentifier;
import com.velos.services.PatientStudyStatusIdentifier;
import com.velos.services.SimpleIdentifier;
import com.velos.services.StudyIdentifier;
import com.velos.services.StudyPatientStatus;
import com.velos.services.StudyPatientStatuses;

@Component
public class PatientStudyStatusMessageProcessor implements
		VelosEspSystemAdministrationEndpoint, VelosEspPatientStudyEndpoint,
		VelosEspStudyPatientHistoryEndpoint {

	private static Logger logger = Logger
			.getLogger(PatientStudyStatusMessageProcessor.class);

	private CoreMappingDao coreMappingDao;
	private SendEmail email;

	public CoreMappingDao getCoreMappingDao() {
		return coreMappingDao;
	}

	public void setCoreMappingDao(CoreMappingDao coreMappingDao) {
		this.coreMappingDao = coreMappingDao;
	}

	public void setEmail(SendEmail email) {
		this.email = email;
	}

	private static String protocolLoginRequestTemplate;
	private static String preEnrollmentTemplate;
	private static String studyEnrollmentTemplate;
	private static String postEnrollmentTemplate;
	private static String pIOverrideTemplateStart;
	private static String pIOverrideTemplateConfig;
	private static String pIOverrideTemplateConfigNull;
	private static String pIOverrideTemplateEnd;
	private static String parentOrganization;
	private static String invalidToken;
	private String flag;
	private String sessionToken;
	int i[] = new int[]{1233};
	int j[] = {1,2};
	String s[] = new String[12];

	private String enrollmentType;
	private String requestXML;
	private String responseXML;
	private String response;
	private String tableName;
	private int tablePK;
	private String studyNumber;
	private String subjectMrn;

	public String getSessionToken() {
		return sessionToken;
	}

	public void setSessionToken(String sessionToken) {
		this.sessionToken = sessionToken;
	}

	private static Map<ProtocolKeys, String> studyPatientMap = new HashMap<ProtocolKeys, String>();

	private ClassPathXmlApplicationContext clientContext;

	private Map<String, String> map = new HashMap<String, String>();
	private Map<ProtocolKeys, String> requestMap = new HashMap<ProtocolKeys, String>();

	private CamelContext context;

	public void setContext(CamelContext context) {
		this.context = context;
	}

	public void setClientContext(ClassPathXmlApplicationContext clientContext) {
		this.clientContext = clientContext;
	}

	private void configureCamel() {
		if (this.context != null) {
			return;
		}
		@SuppressWarnings("resource")
		ClassPathXmlApplicationContext springContext = new ClassPathXmlApplicationContext(
				"camel-config.xml");
		this.setContext((CamelContext) springContext.getBean("camel"));

		@SuppressWarnings("resource")
		ClassPathXmlApplicationContext clientContext = new ClassPathXmlApplicationContext(
				"velos-mdaclient-req.xml");
		this.setClientContext(clientContext);
	}

	public void processPatientStudyStatMessage(CustomHandler handler) {
		SimpleIdentifier simpleIdentifier = (SimpleIdentifier) handler
				.getParentOID().get(IdentifierConstants.SIMPLE_IDENTIFIER);
		List<Change> childHandlers = handler.getChanges();

		for (Change childHandler : childHandlers) {
			requestMap.put(ProtocolKeys.childOID, childHandler.getIdentifier()
					.getOID());
		}

		requestMap.put(ProtocolKeys.OID, simpleIdentifier.getOID());
		this.configureCamel();

		Properties prop = new Properties();

		try {
			prop.load(this.getClass().getClassLoader()
					.getResourceAsStream("velos-mdaclient-req.properties"));
			protocolLoginRequestTemplate = prop
					.getProperty("core.protocolLoginRequestTemplate");
			preEnrollmentTemplate = prop
					.getProperty("core.preEnrollmentTemplate");
			studyEnrollmentTemplate = prop
					.getProperty("core.studyEnrollmentTemplate");
			postEnrollmentTemplate = prop
					.getProperty("core.postEnrollmentTemplate");
			pIOverrideTemplateStart = prop
					.getProperty("core.pIOverrideTemplateStart");
			pIOverrideTemplateConfig = prop
					.getProperty("core.pIOverrideTemplateConfig");
			pIOverrideTemplateConfigNull = prop
					.getProperty("core.pIOverrideTemplateConfigNull");
			pIOverrideTemplateEnd = prop
					.getProperty("core.pIOverrideTemplateEnd");
			parentOrganization = prop.getProperty("core.parentOrganization");
			invalidToken = prop.getProperty("core.invalidToken");
			map.put("StageName", prop.getProperty("core.stageName"));
			map.put("UserId", prop.getProperty("core.userId"));
			map.put("Password", prop.getProperty("core.password"));

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		loginRequest(map);
		patStudyRequest(requestMap);
	}

	public void loginRequest(Map<String, String> map) {

		String body = null;

		ArrayList<String> argList = new ArrayList<String>();
		argList.add(map.get("StageName"));
		argList.add(map.get("UserId"));
		argList.add(map.get("Password"));

		body = String.format(protocolLoginRequestTemplate, argList.toArray());
		WebServiceTemplate webServiceTemplate = (WebServiceTemplate) this.clientContext
				.getBean("protocolLoginRequest");
		StreamSource source = new StreamSource(new StringReader(body));
		StringWriter writer = new StringWriter();
		StreamResult result = new StreamResult(writer);
		webServiceTemplate.sendSourceAndReceiveToResult(source, result);

		logger.info("Login Response Body : " + writer.toString());

		XPath xpath = XPathFactory.newInstance().newXPath();
		XPathExpression expr;
		try {
			expr = xpath.compile("//*[local-name()='return']");
			DOMSource domSource;
			DocumentBuilderFactory factory = DocumentBuilderFactory
					.newInstance();
			factory.setNamespaceAware(true);
			DocumentBuilder builder = factory.newDocumentBuilder();
			org.w3c.dom.Document document = builder.parse(new InputSource(
					new ByteArrayInputStream(((String) writer.toString())
							.getBytes("utf-8"))));
			domSource = new DOMSource(document);

			setSessionToken(expr.evaluate(domSource.getNode()));

			logger.info("Session Token : " + getSessionToken());
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Bean
	public WSS4JOutInterceptor wssInterceptor() {
		Properties prop = new Properties();
		try {
			prop.load(this.getClass().getClassLoader()
					.getResourceAsStream("velos-espclient-req.properties"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Map<String, Object> props = new HashMap<String, Object>();
		props.put(WSHandlerConstants.ACTION, WSHandlerConstants.USERNAME_TOKEN);
		props.put(WSHandlerConstants.PASSWORD_TYPE, WSConstants.PW_TEXT);
		props.put(WSHandlerConstants.USER, prop.getProperty("velos.userID"));
		props.put(WSHandlerConstants.PW_CALLBACK_CLASS,
				SimpleAuthCallbackHandler.class.getName());
		WSS4JOutInterceptor wssBean = new WSS4JOutInterceptor(props);
		return wssBean;
	}

	public void patStudyRequest(Map<ProtocolKeys, String> requestMap) {

		// simpleIdentifier.setPK(Integer.parseInt(requestMap.get(ProtocolKeys.PK)));
		ObjectInfo objectInfoParent;
		ObjectInfo objectInfoChild;

		MessageChannel channel = null;
		String body = null;
		try {
			SimpleIdentifier simpleIdentifier = new SimpleIdentifier();

			simpleIdentifier.setOID(requestMap.get(ProtocolKeys.childOID));

			objectInfoChild = getObjectInfoFromOID(simpleIdentifier);

			logger.info("Child Table : " + objectInfoChild.getTableName());
			logger.info("Child Pk : " + objectInfoChild.getTablePk());
			this.tableName = objectInfoChild.getTableName();
			this.tablePK = objectInfoChild.getTablePk();
			simpleIdentifier = new SimpleIdentifier();
			simpleIdentifier.setOID(requestMap.get(ProtocolKeys.OID));
			objectInfoParent = getObjectInfoFromOID(simpleIdentifier);

			logger.info("Parent Table : " + objectInfoParent.getTableName());
			logger.info("Parent Pk : " + objectInfoParent.getTablePk());

			PatientProtocolIdentifier patientProtocolIdentifier = getPatientStudyInfo(objectInfoParent
					.getTablePk());

			PatientIdentifier patientIdentifier = patientProtocolIdentifier
					.getPatientIdentifier();

			StudyIdentifier studyIdentifier = patientProtocolIdentifier
					.getStudyIdentifier();
			studyNumber = studyIdentifier.getStudyNumber();
			StudyPatientStatuses studyPatientStatuses = getStudyPatientStatusHistory(
					studyIdentifier, patientIdentifier);

			List<StudyPatientStatus> studyPatientStatusList = studyPatientStatuses
					.getStudyPatientStatus();

			ObjectInfo objectInfoEachChild;
			for (StudyPatientStatus studyPatientStatus : studyPatientStatusList) {

				SimpleIdentifier simpleIdentifierEachChild = studyPatientStatus
						.getStudyPatStatId();
				objectInfoEachChild = getObjectInfoFromOID(simpleIdentifierEachChild);
				if (objectInfoEachChild.getTablePk() == objectInfoChild
						.getTablePk()) {
					PatientStudyStatusIdentifier studyPatStatId = studyPatientStatus
							.getStudyPatStatId();
					Code status = studyPatientStatus.getStudyPatStatus();
					Code reason = studyPatientStatus.getStatusReason();
					String note = studyPatientStatus.getStatusNote();

					logger.info("Patient Study Status from CTMS : "
							+ status.getCode());

					CoreCodeListMapper coreCodeListMapper = new CoreCodeListMapper();
					coreCodeListMapper.setCodeListSubType(status.getCode());

					this.getCoreMappingDao().getTypeAndDescription(
							coreCodeListMapper);

					logger.info("Patient Study Status : "
							+ coreCodeListMapper.getCodeListType());
					this.enrollmentType = coreCodeListMapper.getCodeListType();
					if (ProtocolKeys.PreEnrollment.toString().equals(
							coreCodeListMapper.getCodeListType())) {
						PreEnrollment preEnrollment = new PreEnrollment();
						ArrayList<String> preEnrollList = preEnrollment
								.updatePreEnrollment(studyPatientStatusList,
										studyPatientStatus, patientIdentifier,
										studyIdentifier, studyPatStatId,
										coreMappingDao, context,
										getSessionToken(), parentOrganization);
						if (preEnrollList.size() == 1) {
							sendError(preEnrollList.get(0));
						} else {
							subjectMrn = preEnrollment.getSubjectMrn();
							body = String.format(preEnrollmentTemplate,
									preEnrollList.toArray());
							this.requestXML = body;
							logger.info("PreEnrollment Request Body : : "
									+ body);
							WebServiceTemplate webServiceTemplate = (WebServiceTemplate) this.clientContext
									.getBean("enrollmentRequest");
							StreamSource source = new StreamSource(
									new StringReader(body));
							StringWriter writer = new StringWriter();
							StreamResult result = new StreamResult(writer);
							webServiceTemplate.sendSourceAndReceiveToResult(
									source, result);

							logger.info("PreEnrollment Response Body : : "
									+ writer.toString());
							checkResponse(writer.toString());
						}

					}

					else if (ProtocolKeys.PostEnrollment.toString().equals(
							coreCodeListMapper.getCodeListType())) {
						PostEnrollment postEnrollment = new PostEnrollment();
						ArrayList<String> postEnrollList = postEnrollment
								.updatePostEnrollment(studyPatientStatusList,
										studyPatientStatus, patientIdentifier,
										studyIdentifier, studyPatStatId,
										coreMappingDao, context,
										getSessionToken(), parentOrganization);

						if (postEnrollList.size() == 1) {
							sendError(postEnrollList.get(0));
						} else {
							subjectMrn = postEnrollment.getSubjectMrn();

							body = String.format(postEnrollmentTemplate,
									postEnrollList.toArray());
							this.requestXML = body;
							logger.info("PostEnrollment Request Body : : "
									+ body);
							WebServiceTemplate webServiceTemplate = (WebServiceTemplate) this.clientContext
									.getBean("enrollmentRequest");
							StreamSource source = new StreamSource(
									new StringReader(body));
							StringWriter writer = new StringWriter();
							StreamResult result = new StreamResult(writer);
							webServiceTemplate.sendSourceAndReceiveToResult(
									source, result);

							logger.info("PostEnrollment Response Body : : "
									+ writer.toString());
							checkResponse(writer.toString());
						}
					} else if (ProtocolKeys.StudyEnrollment.toString().equals(
							coreCodeListMapper.getCodeListType())) {
						StudyEnrollment studyEnrollment = new StudyEnrollment();
						ArrayList<String> studyEnrollList = studyEnrollment
								.updateStudyEnrollment(studyPatientStatusList,
										studyPatientStatus, patientIdentifier,
										studyIdentifier, studyPatStatId,
										coreMappingDao, context,
										getSessionToken(), parentOrganization);

						if (studyEnrollList.size() == 1) {
							sendError(studyEnrollList.get(0));
						} else {
							subjectMrn = studyEnrollment.getSubjectMrn();

							body = String.format(studyEnrollmentTemplate,
									studyEnrollList.toArray());
							this.requestXML = body;
							logger.info("StudyEnrollment Request Body : : "
									+ body);
							WebServiceTemplate webServiceTemplate = (WebServiceTemplate) this.clientContext
									.getBean("enrollmentRequest");
							StreamSource source = new StreamSource(
									new StringReader(body));
							StringWriter writer = new StringWriter();
							StreamResult result = new StreamResult(writer);
							webServiceTemplate.sendSourceAndReceiveToResult(
									source, result);

							logger.info("StudyEnrollment Response Body : : "
									+ writer.toString());
							checkResponse(writer.toString());
						}
					}

					else if (ProtocolKeys.PIOverride.toString().equals(
							coreCodeListMapper.getCodeListType())) {
						PIOverride pIOverride = new PIOverride();
						ArrayList<String> pIOverrideList = pIOverride
								.updatepIOverride(studyPatientStatusList,
										studyPatientStatus, patientIdentifier,
										studyIdentifier, studyPatStatId,
										coreMappingDao, context,
										getSessionToken(), parentOrganization);

						if (pIOverrideList.size() == 1) {
							sendError(pIOverrideList.get(0));
						} else {

							subjectMrn = pIOverride.getSubjectMrn();

							int failedQuestionCount = Integer
									.parseInt(pIOverrideList
											.remove(pIOverrideList.size() - 1));

							StringBuffer pIOverrideTemplateFailedQuestion = new StringBuffer();
							if (failedQuestionCount == 0)
								pIOverrideTemplateFailedQuestion
										.append(pIOverrideTemplateConfigNull);
							else {
								for (int i = 0; i < failedQuestionCount; i++) {
									pIOverrideTemplateFailedQuestion
											.append(pIOverrideTemplateConfig);
								}
							}

							String pIOverrideTemplate = pIOverrideTemplateStart
									+ pIOverrideTemplateFailedQuestion
											.toString() + pIOverrideTemplateEnd;

							body = String.format(pIOverrideTemplate,
									pIOverrideList.toArray());
							this.requestXML = body;
							logger.info("PIOverride Request Body : : " + body);
							WebServiceTemplate webServiceTemplate = (WebServiceTemplate) this.clientContext
									.getBean("enrollmentRequest");
							StreamSource source = new StreamSource(
									new StringReader(body));
							StringWriter writer = new StringWriter();
							StreamResult result = new StreamResult(writer);
							webServiceTemplate.sendSourceAndReceiveToResult(
									source, result);

							logger.info("PIOverride Response Body : : "
									+ writer.toString());
							checkResponse(writer.toString());
						}
					}

					else if (ProtocolKeys.PreAndStudyEnrollment.toString()
							.equals(coreCodeListMapper.getCodeListType())) {
						boolean alreadyPreScreened = false;
						
						for (StudyPatientStatus studyPatientStatusRow : studyPatientStatusList) {
							if ("patStatus_12".equals(studyPatientStatusRow
									.getStudyPatStatus().getCode())) {
								
								alreadyPreScreened = true;
								break;
							}
						}

						if(alreadyPreScreened)
						{
							PreEnrollment preEnrollment = new PreEnrollment();
							ArrayList<String> preEnrollList = preEnrollment
									.updatePreEnrollment(studyPatientStatusList,
											studyPatientStatus, patientIdentifier,
											studyIdentifier, studyPatStatId,
											coreMappingDao, context,
											getSessionToken(), parentOrganization);
							if (preEnrollList.size() == 1) {
								sendError(preEnrollList.get(0));
							} else {
								subjectMrn = preEnrollment.getSubjectMrn();

								body = String.format(preEnrollmentTemplate,
										preEnrollList.toArray());
								this.requestXML = body;
								logger.info("PreEnrollment Request Body : : "
										+ body);
								WebServiceTemplate webServiceTemplate = (WebServiceTemplate) this.clientContext
										.getBean("enrollmentRequest");
								StreamSource source = new StreamSource(
										new StringReader(body));
								StringWriter writer = new StringWriter();
								StreamResult result = new StreamResult(writer);
								webServiceTemplate.sendSourceAndReceiveToResult(
										source, result);

								logger.info("PreEnrollment Response Body : : "
										+ writer.toString());
								checkResponse(writer.toString());
							}
						}
						
						boolean alreadyEnrolled = false;

						for (StudyPatientStatus studyPatientStatusRow : studyPatientStatusList) {
							if ("screening".equals(studyPatientStatusRow
									.getStudyPatStatus().getCode())) {
								alreadyEnrolled = true;
								break;
							}
						}

						if (alreadyEnrolled) {
							StudyEnrollment studyEnrollment = new StudyEnrollment();
							ArrayList<String> studyEnrollList = studyEnrollment
									.updateStudyEnrollment(
											studyPatientStatusList,
											studyPatientStatus,
											patientIdentifier, studyIdentifier,
											studyPatStatId, coreMappingDao,
											context, getSessionToken(),
											parentOrganization);
							if (studyEnrollList.size() == 1) {
								sendError(studyEnrollList.get(0));
							} else {
								subjectMrn = studyEnrollment.getSubjectMrn();

								body = String.format(studyEnrollmentTemplate,
										studyEnrollList.toArray());
								this.requestXML = body;
								logger.info("StudyEnrollment Request Body : : "
										+ body);
								WebServiceTemplate webServiceTemplate = (WebServiceTemplate) this.clientContext
										.getBean("enrollmentRequest");
								StreamSource source = new StreamSource(
										new StringReader(body));
								StringWriter writer = new StringWriter();
								StreamResult result = new StreamResult(writer);
								webServiceTemplate
										.sendSourceAndReceiveToResult(source,
												result);

								logger.info("StudyEnrollment Response Body : : "
										+ writer.toString());
								checkResponse(writer.toString());
							}
						}
					}
				}
			}
			System.out.println("Finish");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// VelosStudyMapper mapper = new
		// VelosStudyMapper(requestMap.get(EndpointKeys.Endpoint));
		// return mapper.mapStudySummary(studySummary);

	}

	public void sendError(String errorMessage) {
		String emailId = coreMappingDao.getCreaterEmail(tablePK);
		try {
			email.sendMail(emailId, errorMessage,
					studyNumber, subjectMrn);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void checkResponse(String responseMessage) {
		this.responseXML = responseMessage;
		XPath xpath = XPathFactory.newInstance().newXPath();
		XPathExpression expr;
		try {
			expr = xpath.compile("//*[local-name()='return']");
			DOMSource domSource;
			DocumentBuilderFactory factory = DocumentBuilderFactory
					.newInstance();
			factory.setNamespaceAware(true);
			DocumentBuilder builder = factory.newDocumentBuilder();
			org.w3c.dom.Document document = builder
					.parse(new InputSource(new ByteArrayInputStream(
							responseMessage.getBytes("utf-8"))));
			domSource = new DOMSource(document);

			String response = expr.evaluate(domSource.getNode());
			coreMappingDao.saveMessageDetails(this.enrollmentType,
					this.requestXML, responseMessage, response, this.tableName,
					this.tablePK);
			if (!response.startsWith("Succeeded")) {
				if (response.contains(invalidToken)) {
					loginRequest(map);
					patStudyRequest(requestMap);
				} else {
					String emailId = coreMappingDao.getCreaterEmail(tablePK);
					StringBuilder contentBuilder = new StringBuilder();
					contentBuilder.append(System.getProperty("line.separator"));
					contentBuilder
							.append("Message summary from CORe interface : "
									+ response);
					contentBuilder.append(System.getProperty("line.separator"));
					contentBuilder.append(System.getProperty("line.separator"));
					contentBuilder.append("Message Details - ");
					contentBuilder.append(System.getProperty("line.separator"));
					contentBuilder.append(System.getProperty("line.separator"));
					contentBuilder.append("Request XML : " + requestXML);
					contentBuilder.append(System.getProperty("line.separator"));
					contentBuilder.append(System.getProperty("line.separator"));
					contentBuilder.append("Response XML : " + responseXML);
					logger.info("Email Message : : "
							+ contentBuilder.toString());
					logger.info("studyNumber: "
							+ studyNumber);
					logger.info("subjectMrn: "
							+ subjectMrn);
					email.sendMail(emailId, contentBuilder.toString(),
							studyNumber, subjectMrn);

				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public ObjectInfo getObjectInfoFromOID(SimpleIdentifier arg0)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList list = (MessageContentsList) producer.sendBody(
				"cxf:bean:espPatStudyEndpoint", ExchangePattern.InOut, arg0);
		return (ObjectInfo) list.get(0);
	}

	@Override
	public PatientProtocolIdentifier getPatientStudyInfo(Integer patProtId)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList list = (MessageContentsList) producer.sendBody(
				"cxf:bean:espPatStudyInfoEndpoint", ExchangePattern.InOut,
				patProtId);
		return (PatientProtocolIdentifier) list.get(0);
	}

	@Override
	public StudyPatientStatuses getStudyPatientStatusHistory(
			StudyIdentifier studyIdentifier, PatientIdentifier patientIdentifier)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList inpList = new MessageContentsList();
		studyIdentifier.setPK(100);
		patientIdentifier.setPK(100);
		inpList.add(studyIdentifier);
		inpList.add(patientIdentifier);
		MessageContentsList list = (MessageContentsList) producer.sendBody(
				"cxf:bean:espPatStudyStatusInfoEndpoint",
				ExchangePattern.InOut, inpList);
		return (StudyPatientStatuses) list.get(0);
	}

}
