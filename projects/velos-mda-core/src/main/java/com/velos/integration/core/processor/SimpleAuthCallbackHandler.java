package com.velos.integration.core.processor;

import java.io.IOException;
import java.util.Properties;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;

import org.apache.ws.security.WSPasswordCallback;

public class SimpleAuthCallbackHandler implements CallbackHandler {

	public void handle(Callback[] callbacks) {
		Properties prop = new Properties();
		try {
			prop.load(this.getClass().getClassLoader()
					.getResourceAsStream("velos-espclient-req.properties"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		WSPasswordCallback pc = (WSPasswordCallback) callbacks[0];
		if (pc.getIdentifier().equals(prop
				.getProperty("velos.userID"))) {
			pc.setPassword(prop
					.getProperty("velos.password"));
		}
	}
}
