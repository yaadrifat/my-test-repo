package com.velos.integration.adt.exception;

public class AdtException extends Exception {
	private String message;

	public AdtException(String message) {
		setMessage(message);
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
