package com.velos.integration.core.processor;

public interface VelosKeys {
	// Intentionally left blank to be implemented by XxxKeys enum classes
	// and used in HashMaps.
}
