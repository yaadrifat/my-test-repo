package com.velos.integration.incomingmessage;

public enum TableMapper {
	patientEMR("PATIENT_EMR")
	;
	
	private String key;
	
	TableMapper(String key) {
		this.key = key;
	}
	
	@Override
	public String toString() {
		return key;
	}}
