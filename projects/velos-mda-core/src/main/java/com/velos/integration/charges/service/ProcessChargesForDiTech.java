package com.velos.integration.charges.service;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.velos.integration.charges.bean.ChargesBean;
import com.velos.integration.charges.bean.SaveChargeFileBean;
import com.velos.integration.charges.dao.ChargesDaoImpl;

public class ProcessChargesForDiTech {

	public void createCharges(SaveChargeFileBean saveChargeFileBean,
			ChargesDaoImpl chargesDao) {

		FileReader reader = null;
		try {
			reader = new FileReader(saveChargeFileBean.getFile());
			LineNumberReader ln = new LineNumberReader(reader);
			String currentLine = ln.readLine();
			String nextLine = "";
			while ((nextLine = ln.readLine()) != null) {
				try {
					ChargesBean chargeBean = new ChargesBean();
					chargeBean.setFileName(saveChargeFileBean.getFileName());
					chargeBean.setFileId(saveChargeFileBean.getSystemFileID());
					chargeBean.setChargeRow(currentLine);
					chargeBean.setDepartment(saveChargeFileBean.getFileType());
					chargeBean.setBatchDate(stringToDate(currentLine.substring(4, 10), saveChargeFileBean.getBatchDateFormat()));
					chargeBean.setServiceDate(stringToDate(currentLine.substring(10,  16), saveChargeFileBean.getBatchDateFormat()));
					chargeBean.setMrn(currentLine.substring(16,  23));
					chargeBean.setLastName(currentLine.substring(85,  101));
					chargeBean.setServiceDescription(currentLine.substring(41,73));
					chargeBean.setServiceCode(currentLine.substring(24,  31));
					chargeBean.setCptCode(currentLine.substring(119,  124));
					chargeBean.setResearchModifier1(currentLine.substring(125,  127));
					chargeBean.setResearchModifier2(currentLine.substring(127,  129));
					chargeBean.setResearchModifier3(currentLine.substring(129,  131));
					chargesDao.createCharge(chargeBean);
				} catch (Exception e) {

					e.printStackTrace();
				}
				currentLine = nextLine;
			}
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static java.util.Date stringToDate(String dtString, String format) {
		java.util.Date dt = null;
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		try {
			dt = sdf.parse(dtString);
		} catch (Exception e) {
		}
		return dt;
	}

	public String truncateResearchModifier(String researchModifier) {
		if (researchModifier != null && !"".equals(researchModifier)) {
			if (researchModifier.length() > 2)
				return researchModifier.substring(0, 1);
		}
		return researchModifier;
	}
}
