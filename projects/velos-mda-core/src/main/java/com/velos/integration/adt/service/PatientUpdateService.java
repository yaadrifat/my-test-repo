package com.velos.integration.adt.service;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import com.velos.integration.adt.bean.PatientBean;
import com.velos.integration.adt.dao.PatientUpdateDao;
import com.velos.integration.incomingmessage.bean.IncomingMessageBean;

public class PatientUpdateService {

	private PatientUpdateDao patientUpdateDao;

	public PatientUpdateDao getPatientUpdateDao() {
		return patientUpdateDao;
	}

	public void setPatientUpdateDao(PatientUpdateDao patientUpdateDao) {
		this.patientUpdateDao = patientUpdateDao;
	}

	private static String parentOrganization;

	public void updatePatient(IncomingMessageBean incomingMessage) {
		// Get the Patient Details from VGDB
		PatientBean patient = this.getPatientUpdateDao().getPatientData(
				incomingMessage);

		// Check if message exists in CTMS
		Properties prop = new Properties();

		try {
			prop.load(this.getClass().getClassLoader()
					.getResourceAsStream("config.properties"));
			parentOrganization = prop.getProperty("adt.parentOrganization");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		List<Integer> pkList = this.getPatientUpdateDao()
				.checkRecordExistsInCTMS(patient, parentOrganization);
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		int count = 0;

		for (int pk : pkList) {
			count++;
			break;
		}

		if (count > 0) {
			this.getPatientUpdateDao().updateRecordInCTMS(patient, pkList);

		}

	}

}
