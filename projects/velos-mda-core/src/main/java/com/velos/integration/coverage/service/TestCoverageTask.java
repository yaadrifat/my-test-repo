package com.velos.integration.coverage.service;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/*
 * Test class for testing coverage task scheduler.
 */
public class TestCoverageTask {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		new ClassPathXmlApplicationContext("ca-config.xml");
	}

}
