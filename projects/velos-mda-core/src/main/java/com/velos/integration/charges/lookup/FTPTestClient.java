package com.velos.integration.charges.lookup;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.velos.integration.charges.dao.ChargesDaoImpl;

public class FTPTestClient {
	private CamelContext context;

	public CamelContext getContext() {
		return context;
	}

	public void setContext(CamelContext context) {
		this.context = context;
	}

	private void configureCamel() throws Exception {
		Properties prop = new Properties();

		InputStream input = null;
		String root = "";
		String ftp = "";
		String diProf = "";
		String toBeProcessed = "";
		String username = "";
		String password = "";
		try {

			input = new FileInputStream("config.properties");

			// load a properties file
			prop.load(input);

			// get the property value and print it out
			root = prop.getProperty("root");
			ftp = prop.getProperty("ftp");
			diProf = prop.getProperty("diProf");
			toBeProcessed = prop.getProperty("toBeProcessed");
			username = prop.getProperty("username");
			password = prop.getProperty("password");

			final String from = "ftp://" + ftp + "/" + diProf + "/"
					+ toBeProcessed + "/?" + "username=" + username
					+ "&password=" + password;
			final String to = "file:" + root + "/" + diProf + "/" + toBeProcessed
					+ "?noop=true";
			this.getContext().addRoutes(new RouteBuilder() {
				@Override
				public void configure() {
					from(from).to(to);
				}
			});
			this.getContext().start();
			Thread.sleep(10000);
			this.getContext().stop();
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void createInputMetaData(ClassPathXmlApplicationContext springContext)
			throws Exception {
		this.configureCamel();
		ChargesDaoImpl dao = (ChargesDaoImpl) springContext
				.getBean("chargesDao");
		//dao.createInputMetaData();

	}

	public static void main(String[] args) throws Exception {
		FTPTestClient client = new FTPTestClient();
		ClassPathXmlApplicationContext springContext = new ClassPathXmlApplicationContext(
				"camel-config.xml");
		client.setContext((CamelContext) springContext.getBean("camel"));
		client.createInputMetaData(springContext);
		System.out.println("Finish");
	}

}
