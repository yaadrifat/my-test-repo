package com.velos.integration.core.processor;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import com.velos.services.ObjectInfo;
import com.velos.services.OperationException_Exception;
import com.velos.services.SimpleIdentifier;

@WebService(targetNamespace = "http://velos.com/services/", name = "PatientDemographicsSEI")
public interface VelosEspPatientDemographicsDetailsEndpoint {
	@WebResult(name = "PatientDetails", targetNamespace = "")
	@RequestWrapper(localName = "getPatientDetails", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetPatientDetails")
	@WebMethod
	@ResponseWrapper(localName = "getPatientDetailsResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetPatientDetailsResponse")
	public com.velos.services.Patient getPatientDetails(
			@WebParam(name = "PatientIdentifier", targetNamespace = "") com.velos.services.PatientIdentifier patientIdentifier)
			throws OperationException_Exception;

}
