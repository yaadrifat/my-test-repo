package com.velos.integration.incomingmessage.scheduler;

import java.util.List;

import org.apache.log4j.Logger;

import com.velos.integration.adt.ApplicationContextProvider;
import com.velos.integration.adt.service.PatientUpdateService;
import com.velos.integration.incomingmessage.TableMapper;
import com.velos.integration.incomingmessage.bean.IncomingMessageBean;
import com.velos.integration.incomingmessage.dao.IncomingMessageDao;

public class RunIncomingMsgTask {

	private IncomingMessageDao incomingMessageDao;

	public IncomingMessageDao getIncomingMessageDao() {
		return incomingMessageDao;
	}

	public void setIncomingMessageDao(IncomingMessageDao incomingMessageDao) {
		this.incomingMessageDao = incomingMessageDao;
	}

	public static Logger getLogger() {
		return logger;
	}

	public static void setLogger(Logger logger) {
		RunIncomingMsgTask.logger = logger;
	}

	private static Logger logger = Logger.getLogger(RunIncomingMsgTask.class
			.getName());

	public void runTask() {
		try {
			// Get all the incoming messages that may need to trigger a change
			// to CTMS.
			List<IncomingMessageBean> incomingMessages = this
					.getIncomingMessageDao().getIncomingMessages();

			// For each message get the VGDB message details.
			for (IncomingMessageBean incomingMessage : incomingMessages) {

				if (TableMapper.patientEMR.toString().equals(incomingMessage
						.getTableName())) {
					PatientUpdateService patientUpdateService = ApplicationContextProvider
							.getApplicationContext().getBean(
									"patientUpdateService",
									PatientUpdateService.class);
					patientUpdateService.updatePatient(incomingMessage);
				}

				this.getIncomingMessageDao().deleteMessage(incomingMessage);

			}
			// Update the message to CTMS if required.

		} catch (Exception e) {
			logger.info("Exception caught in runTask() method of RunIncomingMsgTask : "
					+ e.getMessage());
		}

	}

}
