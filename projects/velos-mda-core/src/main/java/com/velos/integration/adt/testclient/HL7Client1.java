package com.velos.integration.adt.testclient;

import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.Segment;
import ca.uhn.hl7v2.model.v24.segment.MSH;
import ca.uhn.hl7v2.parser.PipeParser;
import ca.uhn.hl7v2.util.Terser;

public class HL7Client1 {
	private void configureCamel() {
		if (this.context != null) {
			return;
		}
		@SuppressWarnings("resource")
		ClassPathXmlApplicationContext springContext = new ClassPathXmlApplicationContext(
				"camel-config.xml");
		this.setContext((CamelContext) springContext.getBean("camel"));
	}

	private CamelContext context;

	public void setContext(CamelContext context) {
		this.context = context;
	}

	public void temp() {

		String line1 = "MSH|^~%&|ADT|PLW||A01-I|201112131606||ADT^A04|PLW21219057037581622|P|2.4||AL|NE|";
		String line2 = "EVN||201112131606||ADM|RICHALA^test^test^a";
		String line3 = "PID||1001836|1001039||PRECARE^BERNADETTEEE||197510621|F||W~I~A|1515 HOLCOMBE BLVD^^HOUSTON^TX^77030^US|101|(733)499-9999^^^^^733^4999999~(723)499-9999^^^^^723^4999999|(713)299-9999^^^^^713^2999999||S|NSP|9093000638~9093000639||||N~H";

		StringBuilder in = new StringBuilder();
		in.append(line1);
		in.append(System.getProperty("line.separator"));
		in.append(line2);
		in.append(System.getProperty("line.separator"));
		in.append(line3);

		String ackMessageString = "MSH|^~\\&|foo|foo||foo|200108151718||ACK^A01^ACK|1|D|2.4|\rMSA|AA\r";

		// instantiate a PipeParser, which handles the "traditional encoding"
		PipeParser pipeParser = new PipeParser();

		try {
			// parse the message string into a Message object
			//Message input = pipeParser.parse(in.toString());
			ProducerTemplate template = context.createProducerTemplate();
			Message out = (Message) template.requestBody(
					"mina2:tcp://localhost:8888", in.toString());
			System.out.println(out.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String a[]) {

		HL7Client1 client = new HL7Client1();
		ClassPathXmlApplicationContext springContext = new ClassPathXmlApplicationContext(
				"camel-config.xml");
		client.setContext((CamelContext) springContext.getBean("camel"));
		client.temp();

	}
}
