package com.velos.integration.adt.mapping;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class CodeSubTypeMapper {

	private static final Map<String, String> genderCodeMap;
	private static final Map<String, String> raceCodeMap;
	private static final Map<String, String> survivalStatCodeMap;
	private static final Map<String, String> ethnicityCodeMap;
	private static final Map<String, String> maritalStatusCodeMap;
	
    static {
        Map<String, String> genderMap = new HashMap<String, String>();
        genderMap.put("F", "female");
        genderMap.put("M", "male");
        genderMap.put("U", "unknown");
        genderMap.put("N", "other");
        genderMap.put("ELSE", "");
        genderCodeMap = Collections.unmodifiableMap(genderMap);
        
        Map<String, String> raceMap = new HashMap<String, String>();
        raceMap.put("R", "race_indala");
        raceMap.put("W", "race_white");
        raceMap.put("N", "race_hwnisl");
        raceMap.put("B", "race_blkafr");
        raceMap.put("A", "race_asian");
        raceMap.put("H", "race_hisp");
        raceMap.put("O", "race_other");
        raceMap.put("D", "race_notrep");
        raceMap.put("U", "race_unknown");
        raceMap.put("ELSE", "");
        raceCodeMap = Collections.unmodifiableMap(raceMap);
        
        Map<String, String> survivalStatMap = new HashMap<String, String>();
        survivalStatMap.put("A", "A");
        survivalStatMap.put("D", "dead");
        survivalStatMap.put("ELSE", "A");
        survivalStatCodeMap = Collections.unmodifiableMap(survivalStatMap);
        
        Map<String, String> ethnicityMap = new HashMap<String, String>();
        ethnicityMap.put("N", "nonhispanic");
        ethnicityMap.put("H", "hispanic");
        ethnicityMap.put("U", "unknown");
        ethnicityMap.put("D", "notreported");
        ethnicityMap.put("ELSE", "");
        ethnicityCodeMap = Collections.unmodifiableMap(ethnicityMap);
        
        Map<String, String> maritalStatusMap = new HashMap<String, String>();
        maritalStatusMap.put("M", "married");
        maritalStatusMap.put("S", "single");
        maritalStatusMap.put("D", "divorced");
        maritalStatusMap.put("P", "marital_st_6");
        maritalStatusMap.put("N", "marital_st_7");
        maritalStatusMap.put("T", "marital_st_8");
        maritalStatusMap.put("X", "separated");
        maritalStatusMap.put("W", "widowed");
        maritalStatusMap.put("U", "marital_st_9");
        maritalStatusMap.put("ELSE", "");
        maritalStatusCodeMap = Collections.unmodifiableMap(maritalStatusMap);
    }

	public static Map<String, String> getGendercodemap() {
		return genderCodeMap;
	}

	public static Map<String, String> getRacecodemap() {
		return raceCodeMap;
	}

	public static Map<String, String> getSurvivalstatcodemap() {
		return survivalStatCodeMap;
	}

	public static Map<String, String> getEthnicitycodemap() {
		return ethnicityCodeMap;
	}

	public static Map<String, String> getMaritalstatuscodemap() {
		return maritalStatusCodeMap;
	}
    
    
    
}
