package com.velos.integration.adt.bean;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class PatientSearchBean {

	private String patientID;
	private String gender;
	private String survivalStatus;
	private String agePeriod;
	private String patientName;

	public String getPatientID() {
		return patientID;
	}

	public void setPatientID(String patientID) {
		this.patientID = patientID;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getSurvivalStatus() {
		return survivalStatus;
	}

	public void setSurvivalStatus(String survivalStatus) {
		this.survivalStatus = survivalStatus;
	}

	public String getAgePeriod() {
		return agePeriod;
	}

	public void setAgePeriod(String agePeriod) {
		this.agePeriod = agePeriod;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

}
