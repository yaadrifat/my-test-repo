package com.velos.integration.ctmsvgdb.service;

import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class RunPatientScheduleJob extends QuartzJobBean {

	private static Logger logger = Logger.getLogger(RunPatientScheduleJob.class
			.getName());
	private RunPatientScheduleTask runPatientScheduleTask;

	public void setRunPatientScheduleTask(
			RunPatientScheduleTask runPatientScheduleTask) {
		this.runPatientScheduleTask = runPatientScheduleTask;
	}

	@Override
	protected void executeInternal(JobExecutionContext arg0)
			throws JobExecutionException {
		try {
			runPatientScheduleTask.runTask();
		} catch (SQLException se) {
			logger.info(" SQL Exception caught in executeInternal method of RunPatientScheduleJob "
					+ se.getMessage());
		}catch (Exception e) {
			logger.info(" Exception caught in executeInternal method of RunPatientScheduleJob "
					+ e.getMessage());
		}
	}

}
