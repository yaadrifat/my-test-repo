package com.velos.integration.init;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;
import org.quartz.Scheduler;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.velos.integration.coverage.dao.CoverageDataDAOImpl;
import com.velos.integration.coverage.dao.CoverageDataFromCA;
import com.velos.integration.coverage.service.SendEmailWithAttachment;

public class CaContextListener implements ServletContextListener {
	
private static Logger logger = Logger.getLogger(CaContextListener.class.getName());
	
	private ApplicationContext context1;

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		logger.info("ServletContextListener destroyed");
		try
        {
            // Get a reference to the Scheduler and shut it down			
            logger.info("Context is : " + context1);
            Scheduler scheduler1 = (Scheduler) ((AbstractApplicationContext) context1).getBean("caquartzSchedulerFactory");                      
            logger.info("Scheduler is : " + scheduler1);
            scheduler1.shutdown(true);

            logger.info("Started scheduler1 shutdown: ");
            // Sleep for a bit so that we don't get any errors
            Thread.sleep(10000); 
          
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
		logger.info("Scheduler Shutdown complete");
		
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		logger.debug("ServletContextListener started");

		context1 = new ClassPathXmlApplicationContext("ca-config.xml");
		CoverageDataDAOImpl bean1 = (CoverageDataDAOImpl) context1
				.getBean("coverageDataDAO");

		List<CoverageDataFromCA> calist = new ArrayList<CoverageDataFromCA>();
		try {
			if (bean1 != null) {

				calist = bean1.getAllCoverageDataFromCA();

				if (calist != null) {

					if (calist.size() != 0) {

						logger.info("CA_CRMS_INTERFACE Table : List size : "
								+ calist.size());
						bean1.updateCoverageDataInCTMS(calist);						
						calist.clear();
						logger.info("Coverage Data update is completed. ");
					}
				}
				
				SendEmailWithAttachment sEmail = (SendEmailWithAttachment)context1.getBean("mailMail");				
				sEmail.sendMail("", "Please find attached coverage log file.");
		        logger.info("Email Sent.");
				logger.info("------------------------------------------------------------------------------");
			}

		} catch (Exception e) {
			logger.info("Exception caught in contextInitialized : "	+ e.getMessage());
			
		}finally
		{
			DriverManagerDataSource mdads = (DriverManagerDataSource)(context1).getBean("mdaDataSource");
			DriverManagerDataSource ctmsds = (DriverManagerDataSource)(context1).getBean("caEresDataSource");
			try { 
				(mdads.getConnection()).close();
				logger.debug("Closed mdaDataSource connection.");
				(ctmsds.getConnection()).close();
				logger.debug("Closed caEresDataSource connection.");	
			} catch(SQLException se){
				logger.info("Exception thrown in finally clause : "  + se.getMessage());
			}
		}
		
	}

}
