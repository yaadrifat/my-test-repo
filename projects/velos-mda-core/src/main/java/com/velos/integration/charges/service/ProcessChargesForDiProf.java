package com.velos.integration.charges.service;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v24.segment.MSH;
import ca.uhn.hl7v2.model.v24.segment.PID;
import ca.uhn.hl7v2.parser.PipeParser;
import ca.uhn.hl7v2.util.Hl7InputStreamMessageIterator;

import com.velos.integration.charges.bean.ChargesBean;
import com.velos.integration.charges.bean.SaveChargeFileBean;
import com.velos.integration.charges.dao.ChargesDaoImpl;

public class ProcessChargesForDiProf {

	public void createCharges(SaveChargeFileBean saveChargeFileBean,
			ChargesDaoImpl chargesDao) {

		FileReader reader = null;
		try {
			reader = new FileReader(saveChargeFileBean.getFile());
			LineNumberReader ln = new LineNumberReader(reader);
			String currentLine = "";
			int count = 0;
			StringBuilder messageBuilder = new StringBuilder();
			StringBuilder messageBuilderDB = new StringBuilder();
			while ((currentLine = ln.readLine()) != null) {
				if (currentLine.contains("MSH|")
						|| currentLine.contains("BTS|")) {
					if (count == 0) {
						count++;
					} else {
						ChargesBean chargeBean = new ChargesBean();

						PipeParser pipeParser = new PipeParser();
						pipeParser.setValidationContext(new MyValidationContext());
						Message next = pipeParser.parse(messageBuilder
								.toString());

						chargeBean
								.setFileName(saveChargeFileBean.getFileName());
						chargeBean.setFileId(saveChargeFileBean
								.getSystemFileID());

						chargeBean.setChargeRow(messageBuilderDB.toString()
								.trim());
						String researchModifier = "";

						// Message Header
						MSH msh = (MSH) next.get("MSH");

						chargeBean.setDepartment(saveChargeFileBean
								.getFileType());
						chargeBean.setBatchDate(this.stringToDate(msh
								.getMsh7_DateTimeOfMessage().getComponent(0)
								.encode(),
								saveChargeFileBean.getBatchDateFormat()));

						// Patient Details
						PID pid = (PID) next.get("PID");
						chargeBean.setMrn(pid.getPid3_PatientIdentifierList(0)
								.getComponent(0).encode());
						chargeBean.setLastName(pid.getPid5_PatientName(0)
								.getComponent(0).encode());
						chargeBean.setFirstName(pid.getPid5_PatientName(0)
								.getComponent(1).encode());

						StringTokenizer segments = new StringTokenizer(
								next.toString(),
								System.getProperty("line.separator"));
						while (segments.hasMoreElements()) {
							StringBuilder segment = new StringBuilder(
									(String) segments.nextElement());
							String segmentName = segment.substring(0, 3);
							if ("FT1".equals(segmentName)) {
								int counter = 0;
								String components[] = segment.toString().split(
										"\\|");
								for (String component : components) {
									switch (counter) {
									case 4:
										chargeBean.setServiceDate(chargeBean
												.returnDate(component));
										break;
									case 7:
										chargeBean.setCptCode(component);
										break;
									case 8:
										chargeBean.setServiceName(component);
										chargeBean
												.setServiceDescription(component);
										break;
									case 26:
										String completeSegment[] = component
												.split("\\^");
										researchModifier = completeSegment[0];
										String researchModifiers[] = researchModifier
												.split("-");
										if (researchModifiers.length > 0)
											chargeBean
													.setResearchModifier1(truncateResearchModifier(researchModifiers[0]));
										if (researchModifiers.length > 1)
											chargeBean
													.setResearchModifier2(truncateResearchModifier(researchModifiers[1]));
										if (researchModifiers.length > 2)
											chargeBean
													.setResearchModifier3(truncateResearchModifier(researchModifiers[2]));
										break;
									default:
										break;
									}
									counter++;
								}

							}
						}
						chargesDao.createCharge(chargeBean);
					}
					messageBuilder = new StringBuilder();

					messageBuilder.append(currentLine);
					messageBuilder.append(System.getProperty("line.separator"));
					messageBuilderDB = new StringBuilder();
					messageBuilderDB.append(currentLine);
					messageBuilderDB.append(System
							.getProperty("line.separator"));
				} else if (!currentLine.contains("BHS|")) {
					if (currentLine.contains("PID|")
							|| currentLine.contains("FT1|")) {
						messageBuilder.append(System
								.getProperty("line.separator"));
						messageBuilder.append(currentLine);
					}
					messageBuilderDB.append(System
							.getProperty("line.separator"));
					messageBuilderDB.append(currentLine);

				}
			}
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (HL7Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static java.util.Date stringToDate(String dtString, String format) {
		java.util.Date dt = null;
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		try {
			dt = sdf.parse(dtString);
		} catch (Exception e) {
		}
		return dt;
	}

	public String truncateResearchModifier(String researchModifier) {
		if (researchModifier != null && !"".equals(researchModifier)) {
			if (researchModifier.length() > 2)
				return researchModifier.substring(0, 1);
		}
		return researchModifier;
	}
}
