package com.velos.integration.core.processor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.camel.CamelContext;
import org.apache.camel.ExchangePattern;
import org.apache.camel.ProducerTemplate;
import org.apache.cxf.message.MessageContentsList;
import org.apache.log4j.Logger;

import com.velos.integration.core.dao.CoreMappingDao;
import com.velos.integration.core.mapping.CoreCodeListMapper;
import com.velos.integration.core.mapping.ProtocolKeys;
import com.velos.services.Code;
import com.velos.services.FormField;
import com.velos.services.FormFieldResponse;
import com.velos.services.FormIdentifier;
import com.velos.services.FormInfo;
import com.velos.services.FormList;
import com.velos.services.FormSection;
import com.velos.services.NvPair;
import com.velos.services.ObjectInfo;
import com.velos.services.OperationException_Exception;
import com.velos.services.OrganizationIdentifier;
import com.velos.services.Organizations;
import com.velos.services.Patient;
import com.velos.services.PatientDemographics;
import com.velos.services.PatientEnrollmentDetails;
import com.velos.services.PatientIdentifier;
import com.velos.services.PatientStudyStatusIdentifier;
import com.velos.services.SimpleIdentifier;
import com.velos.services.StudyIdentifier;
import com.velos.services.StudyPatientFormDesign;
import com.velos.services.StudyPatientFormResponse;
import com.velos.services.StudyPatientFormResponseIdentifier;
import com.velos.services.StudyPatientFormResponses;
import com.velos.services.StudyPatientStatus;
import com.velos.services.StudyPatientStatuses;
import com.velos.services.StudySummary;
import com.velos.services.UserIdentifier;

public class PIOverride implements
		VelosEspPatientStudyFormResponseDetailEndpoint,
		VelosEspPatientDemographicsEndpoint, VelosEspStudyPatientEndpoint,
		VelosEspPatientStudyFormsEndpoint, VelosEspStudyEndpoint,
		VelosEspUserEndpoint, VelosEspStudyPatientHistoryEndpoint,
		VelosEspPatientDemographicsDetailsEndpoint,
		VelosEspPatientStudyFormDesignEndpoint,
		VelosEspPatientStudyFormResponseEndpoint,
		VelosEspSystemAdministrationEndpoint {

	private static Logger logger = Logger.getLogger(PIOverride.class);

	private Map<ProtocolKeys, String> studyPatientMap = new HashMap<ProtocolKeys, String>();
	private CamelContext context;
	private String subjectMrn;

	public String getSubjectMrn() {
		return subjectMrn;
	}

	public void setContext(CamelContext context) {
		this.context = context;
	}

	public ArrayList<String> updatepIOverride(
			List<StudyPatientStatus> studyPatientStatusList,
			StudyPatientStatus studyPatientStatus,
			PatientIdentifier patientIdentifier,
			StudyIdentifier studyIdentifier,
			PatientStudyStatusIdentifier studyPatStatId,
			CoreMappingDao coreMappingDao, CamelContext context,
			String sessionToken, String parentOrganization)
			throws OperationException_Exception {
		ArrayList<String> argList = new ArrayList<String>();
		try {
			this.setContext(context);

			Map<Integer, String> failedQuestionsDetails = new HashMap<Integer, String>();

			Code status = studyPatientStatus.getStudyPatStatus();
			Code reason = studyPatientStatus.getStatusReason();
			String note = studyPatientStatus.getStatusNote();

			CoreCodeListMapper coreCodeListMapperEligibilityForm = new CoreCodeListMapper();
			coreCodeListMapperEligibilityForm
					.setCodeListType(ProtocolKeys.FormName.toString());
			coreCodeListMapperEligibilityForm
					.setCodeListSubType(ProtocolKeys.EligibilityForm.toString());
			coreMappingDao.getDescription(coreCodeListMapperEligibilityForm);

			setData(ProtocolKeys.AnticipatedTreatmentStartDate, "");
			setData(ProtocolKeys.OverallComment, "");
			setData(ProtocolKeys.IRBChairViceChairFirstName1stCycle, "");
			setData(ProtocolKeys.IRBChairViceChairLastName1stCycle, "");
			setData(ProtocolKeys.IRBChairViceChairUserId1stCycle, "");
			setData(ProtocolKeys.IRBChairViceChairFirstName2ndCycle, "");
			setData(ProtocolKeys.IRBChairViceChairLastName2ndCycle, "");
			setData(ProtocolKeys.IRBChairViceChairUserId2ndCycle, "");
			setData(ProtocolKeys.IRBChairViceChairSigned1stCycle, "");
			setData(ProtocolKeys.IRBChairViceChairSigned2ndCycle, "");
			setData(ProtocolKeys.IRBChairViceChairComment1stCycle, "");
			setData(ProtocolKeys.IRBChairViceChairComment2ndCycle, "");
			setData(ProtocolKeys.PIOverrideSponsorApproval, "");

			if (coreCodeListMapperEligibilityForm.getCodeListSubDesc() != null) {
				FormList formList = getListOfStudyPatientForms(
						patientIdentifier, studyIdentifier, 50, true);
				for (FormInfo formInfo : formList.getFormInfo()) {
					/*
					 * if
					 * (coreCodeListMapperEligibilityForm.getCodeListSubDesc()
					 * .equals(formInfo.getFormName())) {
					 */
					if (formInfo
							.getFormName()
							.toLowerCase()
							.replaceAll("\\s", "")
							.contains(
									coreCodeListMapperEligibilityForm
											.getCodeListSubDesc().toLowerCase()
											.replaceAll("\\s", ""))) {
						Map<Integer, String> formFields = new HashMap<Integer, String>();
						Map<Integer, Integer> formQuestionAnswer = new HashMap<Integer, Integer>();

						Map<Integer, Integer> map = new HashMap<Integer, Integer>();

						StudyPatientFormDesign studyPatientFormDesign = getStudyPatientFormDesign(
								formInfo.getFormIdentifier(), studyIdentifier,
								formInfo.getFormName(), true);

						boolean count = false;
						// Skiping the first section as its Elig Question
						// Answers
						for (FormSection formSection : studyPatientFormDesign
								.getSections().getSection()) {
							if (count) {
								for (FormField formField : formSection
										.getFields().getField()) {

									formFields.put(formField
											.getFieldIdentifier().getPK(),
											formField.getUniqueID());
								}
								break;
							} else {
								int questionNumber = 0;
								for (FormField formField : formSection
										.getFields().getField()) {
									if (formField.getDataType() != null) {
										if ("EDIT_BOX_TEXT".equals(formField
												.getDataType().toString())) {
											try {
												questionNumber = Integer
														.parseInt(formField
																.getUniqueID().substring(formField
																		.getUniqueID().lastIndexOf("_")+1));
												map.put(questionNumber,
														formField
																.getFieldIdentifier()
																.getPK());
											} catch (NumberFormatException nfe) {
												argList.add("Field Id for Justification Comment with Field Id : "
														+ formField
																.getUniqueID()
														+ " in the form - "
														+ formInfo
																.getFormName()
														+ " should be in a number format");
												return argList;
											}
										}
									}
								}
								count = true;
							}
						}

						int questionNo = 0;

						SortedSet<Integer> keys = new TreeSet<Integer>(
								map.keySet());

						for (Integer key : keys) {
							questionNo++;
							formQuestionAnswer.put(map.get(key), questionNo);
						}

						StudyPatientFormResponses studyPatientFormResponses = getListOfStudyPatientFormResponses(
								formInfo.getFormIdentifier(),
								patientIdentifier, studyIdentifier, 1, 1000);

						for (StudyPatientFormResponse studyPatientFormResponse : studyPatientFormResponses
								.getStudyPatientFormResponses()) {
							StudyPatientFormResponseIdentifier studyPatientFormResponseIdentifier = studyPatientFormResponse
									.getSystemID();

							StudyPatientFormResponse studyPatientFormResponseDetails = getStudyPatientFormResponse(studyPatientFormResponseIdentifier);
							int questionNoSequence = 0;
							for (FormFieldResponse formFieldResponse : studyPatientFormResponseDetails
									.getFormFieldResponses().getField()) {
								if (formQuestionAnswer
										.containsKey(formFieldResponse
												.getFieldIdentifier().getPK())) {
									if (!"".equals(formFieldResponse.getValue())
											&& formFieldResponse.getValue() != null) {
										failedQuestionsDetails
												.put(formQuestionAnswer
														.get(formFieldResponse
																.getFieldIdentifier()
																.getPK()),
														formFieldResponse
																.getValue());
									}

								}

								if (formFields.containsKey(formFieldResponse
										.getFieldIdentifier().getPK())) {

									String fieldId = formFields
											.get(formFieldResponse
													.getFieldIdentifier()
													.getPK());
									CoreCodeListMapper coreCodeListFormFields = new CoreCodeListMapper();
									coreCodeListFormFields
											.setCodeListType(ProtocolKeys.PIOverrideFormField
													.toString());
									coreCodeListFormFields
											.setCodeListSubType(ProtocolKeys.AnticipatedTreatmentStartDate
													.toString());
									coreMappingDao
											.getDescription(coreCodeListFormFields);

									if (fieldId.equals(coreCodeListFormFields
											.getCodeListSubDesc())) {
										setData(ProtocolKeys.AnticipatedTreatmentStartDate,
												formFieldResponse.getValue());

									}

									coreCodeListFormFields
											.setCodeListSubType(ProtocolKeys.OverallComment
													.toString());
									coreMappingDao
											.getDescription(coreCodeListFormFields);

									if (fieldId.equals(coreCodeListFormFields
											.getCodeListSubDesc())) {
										setData(ProtocolKeys.OverallComment,
												formFieldResponse.getValue());

									}

									coreCodeListFormFields
											.setCodeListSubType(ProtocolKeys.IRBChairViceChairFullName1stCycle
													.toString());
									coreMappingDao
											.getDescription(coreCodeListFormFields);

									if (fieldId.equals(coreCodeListFormFields
											.getCodeListSubDesc())) {
										setData(ProtocolKeys.IRBChairViceChairFullName1stCycle,
												formFieldResponse.getValue());

									}

									coreCodeListFormFields
											.setCodeListSubType(ProtocolKeys.IRBChairViceChairUserId1stCycle
													.toString());
									coreMappingDao
											.getDescription(coreCodeListFormFields);

									if (fieldId.equals(coreCodeListFormFields
											.getCodeListSubDesc())) {
										setData(ProtocolKeys.IRBChairViceChairUserId1stCycle,
												formFieldResponse.getValue());

										if (!"".equals(formFieldResponse
												.getValue())
												&& formFieldResponse.getValue() == null) {
											Map<String, String> name = coreMappingDao
													.getSplitedName(formFieldResponse
															.getValue());
											setData(ProtocolKeys.IRBChairViceChairFirstName1stCycle,
													name.get("firstName"));
											setData(ProtocolKeys.IRBChairViceChairLastName1stCycle,
													name.get("lastName"));
										} else {
											Map<String, String> name = splitName(studyPatientMap
													.get(ProtocolKeys.IRBChairViceChairFullName1stCycle));
											setData(ProtocolKeys.IRBChairViceChairFirstName1stCycle,
													name.get("firstName"));
											setData(ProtocolKeys.IRBChairViceChairLastName1stCycle,
													name.get("lastName"));
										}
									}

									coreCodeListFormFields
											.setCodeListSubType(ProtocolKeys.IRBChairViceChairFullName2ndCycle
													.toString());
									coreMappingDao
											.getDescription(coreCodeListFormFields);

									if (fieldId.equals(coreCodeListFormFields
											.getCodeListSubDesc())) {
										setData(ProtocolKeys.IRBChairViceChairFullName2ndCycle,
												formFieldResponse.getValue());

									}

									coreCodeListFormFields
											.setCodeListSubType(ProtocolKeys.IRBChairViceChairUserId2ndCycle
													.toString());
									coreMappingDao
											.getDescription(coreCodeListFormFields);

									if (fieldId.equals(coreCodeListFormFields
											.getCodeListSubDesc())) {
										setData(ProtocolKeys.IRBChairViceChairUserId2ndCycle,
												formFieldResponse.getValue());

										if (!"".equals(formFieldResponse
												.getValue())
												&& formFieldResponse.getValue() == null) {
											Map<String, String> name = coreMappingDao
													.getSplitedName(formFieldResponse
															.getValue());
											setData(ProtocolKeys.IRBChairViceChairFirstName2ndCycle,
													name.get("firstName"));
											setData(ProtocolKeys.IRBChairViceChairLastName2ndCycle,
													name.get("lastName"));
										} else {
											Map<String, String> name = splitName(studyPatientMap
													.get(ProtocolKeys.IRBChairViceChairFullName2ndCycle));
											setData(ProtocolKeys.IRBChairViceChairFirstName2ndCycle,
													name.get("firstName"));
											setData(ProtocolKeys.IRBChairViceChairLastName2ndCycle,
													name.get("lastName"));
										}

									}

									coreCodeListFormFields
											.setCodeListSubType(ProtocolKeys.IRBChairViceChairSigned1stCycle
													.toString());
									coreMappingDao
											.getDescription(coreCodeListFormFields);

									if (fieldId.equals(coreCodeListFormFields
											.getCodeListSubDesc())) {
										setData(ProtocolKeys.IRBChairViceChairSigned1stCycle,
												formFieldResponse.getValue());

									}

									coreCodeListFormFields
											.setCodeListSubType(ProtocolKeys.IRBChairViceChairSigned2ndCycle
													.toString());
									coreMappingDao
											.getDescription(coreCodeListFormFields);

									if (fieldId.equals(coreCodeListFormFields
											.getCodeListSubDesc())) {
										setData(ProtocolKeys.IRBChairViceChairSigned2ndCycle,
												formFieldResponse.getValue());

									}

									coreCodeListFormFields
											.setCodeListSubType(ProtocolKeys.IRBChairViceChairComment1stCycle
													.toString());
									coreMappingDao
											.getDescription(coreCodeListFormFields);

									if (fieldId.equals(coreCodeListFormFields
											.getCodeListSubDesc())) {
										setData(ProtocolKeys.IRBChairViceChairComment1stCycle,
												formFieldResponse.getValue());

									}

									coreCodeListFormFields
											.setCodeListSubType(ProtocolKeys.IRBChairViceChairComment2ndCycle
													.toString());
									coreMappingDao
											.getDescription(coreCodeListFormFields);

									if (fieldId.equals(coreCodeListFormFields
											.getCodeListSubDesc())) {
										setData(ProtocolKeys.IRBChairViceChairComment2ndCycle,
												formFieldResponse.getValue());

									}

									coreCodeListFormFields
											.setCodeListSubType(ProtocolKeys.PIOverrideSponsorApproval
													.toString());
									coreMappingDao
											.getDescription(coreCodeListFormFields);

									if (fieldId.equals(coreCodeListFormFields
											.getCodeListSubDesc())) {
										if ("yes".equals(formFieldResponse
												.getValue())
												|| "Yes".equals(formFieldResponse
														.getValue())
												|| "Y".equals(formFieldResponse
														.getValue())) {
											setData(ProtocolKeys.PIOverrideSponsorApproval,
													"Y");
										} else if ("no"
												.equals(formFieldResponse
														.getValue())
												|| "No".equals(formFieldResponse
														.getValue())
												|| "N".equals(formFieldResponse
														.getValue())) {
											setData(ProtocolKeys.PIOverrideSponsorApproval,
													"N");
										} else {
											setData(ProtocolKeys.PIOverrideSponsorApproval,
													"");
										}
									}
								}
							}
						}
						break;
					}
				}
			}

			CoreCodeListMapper coreCodeListMapperStatus = new CoreCodeListMapper();
			coreCodeListMapperStatus.setCodeListType(status.getCode());
			if (reason == null || reason.getCode() == null)
				coreCodeListMapperStatus.setCodeListSubType(null);
			else
				coreCodeListMapperStatus.setCodeListSubType(reason.getCode());

			coreMappingDao.getDescription(coreCodeListMapperStatus);

			if (coreCodeListMapperStatus.getCodeListSubDesc() != null) {
				setData(ProtocolKeys.PIOverrideStatus,
						coreCodeListMapperStatus.getCodeListSubDesc());
			} else {
				setData(ProtocolKeys.PIOverrideStatus, "");
			}

			XMLGregorianCalendar date = studyPatientStatus.getStatusDate();

			if (date != null) {
				setData(ProtocolKeys.Submitter1SignedPIOverrideDate,
						getDate(date));
				setData(ProtocolKeys.Submitter2SignedPIOverrideDate,
						getDate(date));
				setData(ProtocolKeys.SponsorApprovalOPRUploadDate,
						getDate(date));
			} else {
				setData(ProtocolKeys.Submitter1SignedPIOverrideDate, "");
				setData(ProtocolKeys.Submitter2SignedPIOverrideDate, "");
				setData(ProtocolKeys.SponsorApprovalOPRUploadDate, "");
			}

			PatientEnrollmentDetails patientEnrollmentDetails = getStudyPatientStatus(studyPatStatId);

			setData(ProtocolKeys.OPRComment,
					patientEnrollmentDetails.getNotes());

			UserIdentifier userIdentifierPhysician = patientEnrollmentDetails
					.getPhysician();
			if (userIdentifierPhysician != null) {

				setData(ProtocolKeys.Submitter1FirstName,
						userIdentifierPhysician.getFirstName());

				setData(ProtocolKeys.Submitter1LastName,
						userIdentifierPhysician.getLastName());

				setData(ProtocolKeys.Submitter1UserId,
						userIdentifierPhysician.getUserLoginName());

			} else {
				setData(ProtocolKeys.Submitter1FirstName, "");
				setData(ProtocolKeys.Submitter1LastName, "");
				setData(ProtocolKeys.Submitter1UserId, "");

			}

			UserIdentifier userIdentifierAssignedTo = patientEnrollmentDetails
					.getAssignedTo();
			if (userIdentifierAssignedTo != null) {

				setData(ProtocolKeys.Submitter2FirstName,
						userIdentifierAssignedTo.getFirstName());

				setData(ProtocolKeys.Submitter2LastName,
						userIdentifierAssignedTo.getLastName());

				setData(ProtocolKeys.Submitter2UserId,
						userIdentifierAssignedTo.getUserLoginName());

			} else {
				setData(ProtocolKeys.Submitter2FirstName, "");
				setData(ProtocolKeys.Submitter2LastName, "");
				setData(ProtocolKeys.Submitter2UserId, "");

			}

			Map<String, String> oPRStaffMember = coreMappingDao
					.getLoginUser(studyPatStatId.getPK());

			setData(ProtocolKeys.OPRStaffMemberUserId,
					oPRStaffMember.get("LoginUserId"));

			setData(ProtocolKeys.OPRStaffMemberFirstName,
					oPRStaffMember.get("LoginUserLastName"));

			setData(ProtocolKeys.OPRStaffMemberLastName,
					oPRStaffMember.get("LoginUserFirstName"));

			Code evaluationStatus = patientEnrollmentDetails
					.getEvaluationStatus();
			Code inEvaluationStatus = patientEnrollmentDetails
					.getInevaluationStatus();

			PatientDemographics patientDemographics = getPatientDemographics(patientIdentifier);

			setData(ProtocolKeys.AccessionNumber,
					patientEnrollmentDetails.getPatientStudyId());

			boolean isRegistrationDate = false;
			for (StudyPatientStatus studyPatientStatusRow : studyPatientStatusList) {
				if ("screening".equals(studyPatientStatusRow
						.getStudyPatStatus().getCode())) {

					setData(ProtocolKeys.RegistrationDate,
							coreMappingDao
									.getCreationDate(studyPatientStatusRow
											.getStudyPatStatId().getPK()));
					isRegistrationDate = true;
					break;
				}
			}
			if (!isRegistrationDate) {
				setData(ProtocolKeys.RegistrationDate, "");
			}

			ObjectInfo objectInfoOrganization;
			SimpleIdentifier simpleIdentifier = new SimpleIdentifier();
			simpleIdentifier.setOID(patientEnrollmentDetails.getEnrollingSite()
					.getOID());

			objectInfoOrganization = getObjectInfoFromOID(simpleIdentifier);

			setData(ProtocolKeys.SubjetMRN, coreMappingDao.getPatFacilityID(
					patientDemographics.getPatientIdentifier().getPK(),
					objectInfoOrganization.getTablePk()));

			subjectMrn = studyPatientMap.get(ProtocolKeys.SubjetMRN);

			setData(ProtocolKeys.MDACCPatient, coreMappingDao.checkPatientSite(
					objectInfoOrganization.getTablePk(), parentOrganization));

			setData(ProtocolKeys.PatFirstName,
					patientDemographics.getFirstName());
			setData(ProtocolKeys.PatLastName, patientDemographics.getLastName());
			Code race = patientDemographics.getRace();
			if (race != null) {
				CoreCodeListMapper coreCodeListMapperRace = new CoreCodeListMapper();
				coreCodeListMapperRace.setCodeListType(race.getType());
				coreCodeListMapperRace.setCodeListSubType(race.getCode());
				coreMappingDao.getDescription(coreCodeListMapperRace);
				if (coreCodeListMapperRace.getCodeListSubDesc() != null)
					setData(ProtocolKeys.PatRace,
							coreCodeListMapperRace.getCodeListSubDesc());
				else
					setData(ProtocolKeys.PatRace, "");
			} else
				setData(ProtocolKeys.PatRace, "");

			XMLGregorianCalendar dob = patientDemographics.getDateOfBirth();
			if (dob != null)
				setData(ProtocolKeys.PatDOB, getDate(dob));
			else
				setData(ProtocolKeys.PatDOB, "");
			Code gender = patientDemographics.getGender();
			if (gender != null) {
				CoreCodeListMapper coreCodeListMapperGender = new CoreCodeListMapper();
				coreCodeListMapperGender.setCodeListType(gender.getType());
				coreCodeListMapperGender.setCodeListSubType(gender.getCode());
				coreMappingDao.getDescription(coreCodeListMapperGender);
				if (coreCodeListMapperGender.getCodeListSubDesc() != null)
					setData(ProtocolKeys.PatGender,
							coreCodeListMapperGender.getCodeListSubDesc());
				else
					setData(ProtocolKeys.PatGender, "");
			} else
				setData(ProtocolKeys.PatGender, "");

			Code ethnicity = patientDemographics.getEthnicity();
			if (ethnicity != null) {
				CoreCodeListMapper coreCodeListMapperEthnicity = new CoreCodeListMapper();
				coreCodeListMapperEthnicity
						.setCodeListType(ethnicity.getType());
				coreCodeListMapperEthnicity.setCodeListSubType(ethnicity
						.getCode());
				coreMappingDao.getDescription(coreCodeListMapperEthnicity);
				if (coreCodeListMapperEthnicity.getCodeListSubDesc() != null)
					setData(ProtocolKeys.PatEthnicity,
							coreCodeListMapperEthnicity.getCodeListSubDesc());
				else
					setData(ProtocolKeys.PatEthnicity, "");

			} else
				setData(ProtocolKeys.PatEthnicity, "");

			List<NvPair> morePatientDetails = patientDemographics
					.getMorePatientDetails();

			setData(ProtocolKeys.ProtocolNumber,
					studyIdentifier.getStudyNumber());

			/*
			 * StudySummary studySummary = new StudySummary(); studySummary =
			 * getStudySummary(studyIdentifier); List<NvPair>
			 * studySummaryDetails = studySummary .getMoreStudyDetails();
			 */

			argList.add(sessionToken);
			argList.add(coreMappingDao
					.getUniqueIdentifierSequence(ProtocolKeys.PIOverride
							.toString()));
			argList.add(studyPatientMap.get(ProtocolKeys.PatLastName));
			argList.add(studyPatientMap.get(ProtocolKeys.PatFirstName));
			argList.add(studyPatientMap.get(ProtocolKeys.PatDOB));
			argList.add(studyPatientMap.get(ProtocolKeys.PatGender));
			argList.add(studyPatientMap.get(ProtocolKeys.PatRace));
			argList.add(studyPatientMap.get(ProtocolKeys.PatEthnicity));
			argList.add(studyPatientMap.get(ProtocolKeys.SubjetMRN));
			argList.add(studyPatientMap.get(ProtocolKeys.MDACCPatient));
			argList.add(studyPatientMap.get(ProtocolKeys.RegistrationDate));
			argList.add(studyPatientMap.get(ProtocolKeys.AccessionNumber));
			argList.add(studyPatientMap.get(ProtocolKeys.ProtocolNumber));
			argList.add(studyPatientMap
					.get(ProtocolKeys.AnticipatedTreatmentStartDate));
			argList.add(studyPatientMap.get(ProtocolKeys.PIOverrideStatus));
			argList.add(studyPatientMap.get(ProtocolKeys.OverallComment));
			argList.add(studyPatientMap.get(ProtocolKeys.Submitter1FirstName));
			argList.add(studyPatientMap.get(ProtocolKeys.Submitter1LastName));
			argList.add(studyPatientMap.get(ProtocolKeys.Submitter1UserId));
			argList.add(studyPatientMap.get(ProtocolKeys.Submitter2FirstName));
			argList.add(studyPatientMap.get(ProtocolKeys.Submitter2LastName));
			argList.add(studyPatientMap.get(ProtocolKeys.Submitter2UserId));
			argList.add(studyPatientMap
					.get(ProtocolKeys.Submitter1SignedPIOverrideDate));
			argList.add(studyPatientMap
					.get(ProtocolKeys.Submitter2SignedPIOverrideDate));
			argList.add(studyPatientMap
					.get(ProtocolKeys.IRBChairViceChairFirstName1stCycle));
			argList.add(studyPatientMap
					.get(ProtocolKeys.IRBChairViceChairLastName1stCycle));
			argList.add(studyPatientMap
					.get(ProtocolKeys.IRBChairViceChairUserId1stCycle));
			argList.add(studyPatientMap
					.get(ProtocolKeys.IRBChairViceChairFirstName2ndCycle));
			argList.add(studyPatientMap
					.get(ProtocolKeys.IRBChairViceChairLastName2ndCycle));
			argList.add(studyPatientMap
					.get(ProtocolKeys.IRBChairViceChairUserId2ndCycle));
			argList.add(studyPatientMap
					.get(ProtocolKeys.IRBChairViceChairSigned1stCycle));
			argList.add(studyPatientMap
					.get(ProtocolKeys.IRBChairViceChairSigned2ndCycle));
			argList.add(studyPatientMap
					.get(ProtocolKeys.IRBChairViceChairComment1stCycle));
			argList.add(studyPatientMap
					.get(ProtocolKeys.IRBChairViceChairComment2ndCycle));
			argList.add(studyPatientMap
					.get(ProtocolKeys.PIOverrideSponsorApproval));
			argList.add(studyPatientMap
					.get(ProtocolKeys.OPRStaffMemberFirstName));
			argList.add(studyPatientMap
					.get(ProtocolKeys.OPRStaffMemberLastName));
			argList.add(studyPatientMap.get(ProtocolKeys.OPRStaffMemberUserId));
			argList.add(studyPatientMap
					.get(ProtocolKeys.SponsorApprovalOPRUploadDate));
			argList.add(studyPatientMap.get(ProtocolKeys.OPRComment));

			Iterator it = failedQuestionsDetails.entrySet().iterator();
			Integer failedQuestionCount = 0;
			while (it.hasNext()) {
				Map.Entry<Integer, String> pairs = (Map.Entry) it.next();
				argList.add(pairs.getKey().toString());
				argList.add(pairs.getValue());
				failedQuestionCount++;
			}
			argList.add(failedQuestionCount.toString());

			return argList;
		} catch (Exception e) {
			argList.add(e.getMessage());
			return argList;
		}
	}

	@Override
	public StudyPatientStatuses getStudyPatientStatusHistory(
			StudyIdentifier studyIdentifier, PatientIdentifier patientIdentifier)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList inpList = new MessageContentsList();
		inpList.add(studyIdentifier);
		inpList.add(patientIdentifier);
		MessageContentsList list = (MessageContentsList) producer.sendBody(
				"cxf:bean:espPatStudyStatusInfoEndpoint",
				ExchangePattern.InOut, inpList);
		return (StudyPatientStatuses) list.get(0);
	}

	@Override
	public PatientDemographics getPatientDemographics(
			PatientIdentifier patientIdentifier)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList list = (MessageContentsList) producer.sendBody(
				"cxf:bean:espPatientDemographics", ExchangePattern.InOut,
				patientIdentifier);
		return (PatientDemographics) list.get(0);
	}

	@Override
	public PatientEnrollmentDetails getStudyPatientStatus(
			PatientStudyStatusIdentifier patientStudyStatusIdentifier)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList list = (MessageContentsList) producer.sendBody(
				"cxf:bean:espStudyPatientStatus", ExchangePattern.InOut,
				patientStudyStatusIdentifier);
		return (PatientEnrollmentDetails) list.get(0);
	}

	@Override
	public Patient getPatientDetails(PatientIdentifier patientIdentifier)
			throws OperationException_Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public StudySummary getStudySummary(StudyIdentifier studyIdentifier)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList list = (MessageContentsList) producer.sendBody(
				"cxf:bean:espStudyPatientStatus", ExchangePattern.InOut,
				studyIdentifier);
		return (StudySummary) list.get(0);
	}

	@Override
	public Organizations getAllOrganizations()
			throws OperationException_Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public void setData(ProtocolKeys key, String value) {
		if (value == null || "".equals(value))
			studyPatientMap.put(key, "");
		else
			studyPatientMap.put(key, value);
	}

	@Override
	public FormList getListOfStudyPatientForms(
			PatientIdentifier patientIdentifier,
			StudyIdentifier studyIdentifier, int maxNumberOfResults,
			boolean formHasResponses) throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList inpList = new MessageContentsList();

		inpList.add(patientIdentifier);
		inpList.add(studyIdentifier);
		inpList.add(maxNumberOfResults);
		inpList.add(formHasResponses);
		MessageContentsList list = (MessageContentsList) producer.sendBody(
				"cxf:bean:espPatStudyStatusFormsInfoEndpoint",
				ExchangePattern.InOut, inpList);
		return (FormList) list.get(0);
	}

	@Override
	public StudyPatientFormResponses getListOfStudyPatientFormResponses(
			FormIdentifier formIdentifier, PatientIdentifier patientIdentifier,
			StudyIdentifier studyIdentifier, int pageNumber, int pageSize)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList inpList = new MessageContentsList();
		inpList.add(formIdentifier);
		inpList.add(patientIdentifier);
		inpList.add(studyIdentifier);
		inpList.add(pageNumber);
		inpList.add(pageSize);
		MessageContentsList list = (MessageContentsList) producer.sendBody(
				"cxf:bean:espPatStudyStatusFormResponseInfoEndpoint",
				ExchangePattern.InOut, inpList);
		return (StudyPatientFormResponses) list.get(0);
	}

	@Override
	public StudyPatientFormResponse getStudyPatientFormResponse(
			StudyPatientFormResponseIdentifier formResponseIdentifier)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList list = (MessageContentsList) producer.sendBody(
				"cxf:bean:espPatStudyStatusFormResponseDetailInfoEndpoint",
				ExchangePattern.InOut, formResponseIdentifier);
		return (StudyPatientFormResponse) list.get(0);
	}

	@Override
	public ObjectInfo getObjectInfoFromOID(SimpleIdentifier arg0)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList list = (MessageContentsList) producer.sendBody(
				"cxf:bean:espPatStudyEndpoint", ExchangePattern.InOut, arg0);
		return (ObjectInfo) list.get(0);
	}

	@Override
	public StudyPatientFormDesign getStudyPatientFormDesign(
			FormIdentifier formIdentifier, StudyIdentifier studyIdentifier,
			String formName, boolean includeFormatting)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList inpList = new MessageContentsList();
		inpList.add(formIdentifier);
		inpList.add(studyIdentifier);
		inpList.add(formName);
		inpList.add(includeFormatting);
		MessageContentsList list = (MessageContentsList) producer.sendBody(
				"cxf:bean:espPatStudyStatusFormDesignInfoEndpoint",
				ExchangePattern.InOut, inpList);
		return (StudyPatientFormDesign) list.get(0);
	}

	public static String getDate(XMLGregorianCalendar date) {
		StringBuilder stringDate = new StringBuilder();
		if (date.getMonth() > 9)
			stringDate.append(date.getMonth());
		else
			stringDate.append("0" + date.getMonth());

		stringDate.append("/");

		if (date.getDay() > 9)
			stringDate.append(date.getDay());
		else
			stringDate.append("0" + date.getDay());

		stringDate.append("/");

		stringDate.append(date.getYear());
		return stringDate.toString();
	}

	public Map<String, String> splitName(String fullName) {
		String firstName = "";
		String lastName = "";
		int start = fullName.indexOf(' ');
		int end = fullName.lastIndexOf(' ');

		if (start == -1) {
			firstName = fullName;
		}
		if (start >= 0) {
			firstName = fullName.substring(0, start);

			lastName = fullName.substring(end + 1, fullName.length());
		}
		Map<String, String> name = new HashMap<String, String>();

		name.put("firstName", firstName);
		name.put("lastName", lastName);
		return name;
	}

}
