package com.velos.integration.coverage.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.velos.integration.coverage.dao.CoverageDataDAOImpl;
import com.velos.integration.coverage.dao.CoverageDataFromCA;


public class TestCoverageDataDAO {
	
	private static Logger logger = Logger.getLogger(TestCoverageDataDAO.class.getName());

	private static ApplicationContext caContext;
	private static List<CoverageDataFromCA> calist;

	public static void main(String[] args) throws SQLException {
	
		caContext = new ClassPathXmlApplicationContext("ca-config.xml");
		CoverageDataDAOImpl caBean = (CoverageDataDAOImpl) caContext.getBean("coverageDataDAO");

		calist = new ArrayList<CoverageDataFromCA>();
		try {
			if (caBean != null) {

				calist = caBean.getAllCoverageDataFromCA();

				if (calist != null || calist.size() != 0) {
					caBean.updateCoverageDataInCTMS(calist);					
				}

				logger.debug("CA_CRMS_INTERFACE Table :  " + calist.size());
				calist.clear();				
				
				SendEmailWithAttachment sEmail = (SendEmailWithAttachment)caContext.getBean("mailMail");				
				sEmail.sendMail("", "Please find attached coverage log file.");
		        logger.info("Email Sent.");
		        logger.info("--------------------------------------------------------------------------");
			}

		} catch (Exception e) {
			logger.info("Exception caught in TestCoverageDataDAO : " + e.getMessage());
		} finally
		{
			DriverManagerDataSource mdads = (DriverManagerDataSource)(caContext).getBean("mdaDataSource");
			DriverManagerDataSource ctmsds = (DriverManagerDataSource)(caContext).getBean("caEresDataSource");
			(mdads.getConnection()).close();
			logger.info("Closed mdaDataSource connection.");
			(ctmsds.getConnection()).close();
			logger.info("Closed caEresDataSource connection.");			
		}		

	}

}
