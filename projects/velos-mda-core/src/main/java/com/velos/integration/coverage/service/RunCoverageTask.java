package com.velos.integration.coverage.service;

import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.velos.integration.coverage.dao.CoverageDataDAOImpl;
import com.velos.integration.coverage.dao.CoverageDataFromCA;

public class RunCoverageTask  implements ApplicationContextAware {

	private static Logger logger = Logger.getLogger(RunCoverageTask.class.getName());

	private static List<CoverageDataFromCA> list;
	private CoverageDataDAOImpl caBean = null;
	ApplicationContext cacontext=null;

	public CoverageDataDAOImpl getBean() {
		return caBean;
	}

	public void setCaBean(CoverageDataDAOImpl bean) {
		this.caBean = bean;			
	}
	
	public void runTask() throws SQLException  {
		try {
			if (this.caBean != null) {

				list = this.caBean.getAllCoverageDataFromCA();

				if (list != null || list.size() != 0) {
					this.caBean.updateCoverageDataInCTMS(list);
				}

				logger.info("CA_CRMS_INTERFACE Table :  " + list.size());
				
				list.clear();
				
				logger.debug("After removing items in the list.. List size :  "	+ list.size());
				
				logger.info("Coverage Data update is completed. ");
				
				SendEmailWithAttachment sEmail = (SendEmailWithAttachment)cacontext.getBean("mailMail");				
				sEmail.sendMail("", "Please find attached coverage log file.");
		        logger.info("Email Sent.");
				logger.info("------------------------------------------------------------------------------");
			}

		} catch (Exception e) {
			logger.info("Exception caught in runTask() method of RunCoverageTask : "
					+ e.getMessage());
		} finally
		{
			DriverManagerDataSource mdads = (DriverManagerDataSource)(this.cacontext).getBean("mdaDataSource");
			DriverManagerDataSource ctmsds = (DriverManagerDataSource)(this.cacontext).getBean("caEresDataSource");
			(mdads.getConnection()).close();
			logger.info("Closed mdaDataSource connection.");
			(ctmsds.getConnection()).close();
			logger.info("Closed caEresDataSource connection.");			
		}		
	}
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.cacontext=applicationContext;
		
	}
	
}
