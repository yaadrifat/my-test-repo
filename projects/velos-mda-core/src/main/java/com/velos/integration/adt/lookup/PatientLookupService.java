package com.velos.integration.adt.lookup;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v24.segment.MSH;
import ca.uhn.hl7v2.model.v24.segment.PID;
import ca.uhn.hl7v2.parser.PipeParser;

import com.velos.integration.adt.bean.AckMessageBean;
import com.velos.integration.adt.bean.PatientBean;
import com.velos.integration.adt.dao.PatientDaoImpl;
import com.velos.integration.adt.mapping.AckCodeType;
import com.velos.integration.adt.mapping.AckMessageType;
import com.velos.integration.adt.mapping.AckResponseType;
import com.velos.integration.adt.service.PatientService;

public class PatientLookupService {
	private PatientDaoImpl patientDao;
	
	public PatientDaoImpl getPatientDao() {
		return patientDao;
	}

	public void setPatientDao(PatientDaoImpl patientDao) {
		this.patientDao = patientDao;
	}

	private static Logger logger = Logger.getLogger(PatientLookupService.class);

	public Message lookupPatient(Message input) {

		AckMessageBean ackMessage = null;
		try {
			logger.info("Incoming message : " + input.toString());
			// Message Header
			MSH msh = (MSH) input.get("MSH");
			// Patient Details
			PID pid = (PID) input.get("PID");

			PatientService patientService = new PatientService();

			// Validating all the mandatory fields
			ackMessage = patientService.validatePatientDetails(msh, pid);

			if (AckCodeType.AcknowledgementInProcess.equals(ackMessage
					.getAckCode())) {
				// Mapping HL7 message to PatientBean
				PatientBean patient = patientService.setPatientDetails(msh, pid);

				// Retrieving PatientDao

				String event = msh.getMsh9_MessageType().getComponent(1)
						.encode();
				
				boolean sqlSuccess = false;
				sqlSuccess = getPatientDao().createPatientRecord(patient);

				ackMessage.setAckCode(AckCodeType.AcknowledgementAccept);
				ackMessage.setAckMessage(AckMessageType.AckAccept);
			}

		} catch (Exception e) {
			return this.getErrorResponse(input.toString(), e.getMessage());
		} 

		HL7MessageHandler messageHandler = new HL7MessageHandler();
		try {
			return messageHandler.makeACK(input, ackMessage);
		} catch (Exception e) {
			return this.getErrorResponse(input.toString(), e.getMessage());
		}
	}

	public Message getErrorResponse(String message, String errorMsg) {
		logger.error("Error in coming HL7 message : " + errorMsg);
		String[] segments1 = message
				.split(System.getProperty("line.separator"));
		String[] segments2 = segments1[0].split("\\|");

		String messageId = segments2[9];

		StringBuilder mshString = new StringBuilder("");
		mshString.append(AckResponseType.MSH);
		mshString.append(AckResponseType.Pipe);
		mshString.append(AckResponseType.EncodingCharacters);
		mshString.append(AckResponseType.Pipe);
		mshString.append(AckResponseType.SendingApplication);
		mshString.append(AckResponseType.Pipe);
		mshString.append(AckResponseType.SendingFacility);
		mshString.append(AckResponseType.Pipe);
		mshString.append(AckResponseType.ReceivingApplication);
		mshString.append(AckResponseType.Pipe);
		mshString.append(AckResponseType.ReceivingFacility);
		mshString.append(AckResponseType.Pipe);
		mshString.append(AckResponseType.Pipe);
		mshString.append(AckResponseType.Pipe);
		mshString.append(AckResponseType.MessageType);
		mshString.append(AckResponseType.Pipe);
		mshString.append(AckResponseType.ResponseMessageType + messageId);
		mshString.append(AckResponseType.Pipe);
		mshString.append(AckResponseType.Pipe);
		mshString.append(AckResponseType.Version);

		mshString.append(System.getProperty("line.separator"));

		mshString.append(AckResponseType.MSA);
		mshString.append(AckResponseType.Pipe);

		mshString.append(AckCodeType.AcknowledgementError);
		mshString.append(AckResponseType.Pipe);
		mshString.append(messageId);
		mshString.append(AckResponseType.Pipe);

		mshString.append(errorMsg);

		logger.info("Response : " + mshString.toString());
		PipeParser pipeParser = new PipeParser();
		pipeParser = new PipeParser();
		Message output = null;
		try {
			output = pipeParser.parse(mshString.toString());
		} catch (HL7Exception e) {
			logger.error("Error while parsing the Acknowledgement : "
					+ e.getMessage());
		}
		return output;
	}
}
