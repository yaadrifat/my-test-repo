package com.velos.integration.ctmsvgdb.dao;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;

public class PatientScheduleDataDAOImpl implements PatientScheduleDataDAO {

	private static Logger logger = Logger
			.getLogger(PatientScheduleDataDAOImpl.class.getName());

	private JdbcTemplate jdbcTemplate;
	private JdbcTemplate jdbcTemplate1;

	List<PatientScheduleInfo> patientList = new ArrayList<PatientScheduleInfo>();
	PatientScheduleInfo pSchInfo = null;

	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		logger.info("Datasource and jdbcTemplate created successfully");
	}
	
	public void setVgdbDataSource(DataSource dataSource) {
		this.jdbcTemplate1 = new JdbcTemplate(dataSource);
		logger.info("VGDB Datasource and jdbcTemplate created successfully");
	}

	@Override
	public List<PatientScheduleInfo> getPatientSchdulingInfoFromCTMS() {

		String sql = "select es.study_number as Protocol#,  epf.pat_facilityid  as PatientId , (select ea3.name from event_assoc ea3 where ea3.event_id = (select chain_id from event_assoc where event_id = ea.event_id) ) as Study_Calendar_Id,"
				+ " se.fk_patprot as PatprotId, ep.patprot_stat as Patprot_Status, ep.patprot_start as Patprot_Start_Date,"
				+ " ea.event_id as Service_Code , ea.name as Service_Name,  ea.description as Service_Description  , ea.event_cptcode as CPT_Code,"
				+ "  ( select codelst_subtyp from sch_codelst where pk_codelst=se.fk_codelst_covertype) as Modifier, spv.visit_name as Visit_Name, se.actual_schdate as Event_Date , es.study_coordinator as Study_Coordinator_Id, "
				+ " (SELECT   usr_firstname || ' ' || usr_lastname FROM  er_user WHERE   pk_user = es.STUDY_COORDINATOR) AS Study_Coordinator_Name,"
				+ " (SELECT   usr_firstname || ' ' || usr_lastname FROM   er_user WHERE   pk_user = es.STUDY_PRINV) AS Principal_Investigator_Name, ea.last_modified_date, " 
				+ " (select em.md_modelementdata from er_moredetails em, event_assoc eas , er_codelst ec where eas.event_id = em.fk_modpk  and ec.pk_codelst = em.md_modelementpk "
				+ " and eas.event_id = ea.event_id and ec.codelst_subtyp = 'PRS Code') as PRS_Code "
				+ " from sch_events1 se, er_study es , er_patprot ep,  er_per per, event_assoc ea, sch_protocol_visit spv , er_patfacility epf "
				+ " where ep.fk_study=es.pk_study "
				+ " and per.pk_per = ep.fk_per "
				+ " and se.fk_assoc = ea.event_id "
				+ " and ea.fk_visit = spv.pk_protocol_visit "
				+ " and ep.pk_patprot = se.fk_patprot "
				+ " and epf.fk_per = per.pk_per "
				+ " and epf.fk_site= per.fk_site ";
				

		if (this.jdbcTemplate != null) {

			@SuppressWarnings("unchecked")
			List<Map<String, Object>> rows = this.jdbcTemplate
			.queryForList(sql);
			for (Map<String, Object> resultSet : rows) {

				PatientScheduleInfo pScheduleData = new PatientScheduleInfo();
				pScheduleData.setStudyNumber((String) resultSet
						.get("Protocol#"));
				pScheduleData.setPatientId((String) resultSet.get("PatientId"));
				pScheduleData.setStudyCalendarId((String) resultSet
						.get("Study_Calendar_Id"));
				
				pScheduleData.setPatprotId((BigDecimal) resultSet.get("PatprotId"));
				pScheduleData.setPatprotStatus((BigDecimal) resultSet.get("Patprot_Status"));
				pScheduleData.setPatprotStartDate((Timestamp) resultSet
						.get("Patprot_Start_Date"));
				
				pScheduleData.setEventId((BigDecimal) resultSet.get("Service_Code"));
				pScheduleData.setEventName((String) resultSet
						.get("Service_Name"));
				pScheduleData.setEventDescription((String) resultSet
						.get("Service_Description"));

				pScheduleData.setCptCode((String) resultSet.get("CPT_Code"));
				pScheduleData.setCoverageType((String) resultSet
						.get("Modifier"));

				pScheduleData
				.setVisitName((String) resultSet.get("Visit_Name"));
				pScheduleData.setEventDate((Timestamp) resultSet
						.get("Event_Date"));

				pScheduleData.setStudyCoordinatorId((BigDecimal) resultSet
						.get("Study_Coordinator_Id"));
				pScheduleData.setStudyCoordinatorName((String) resultSet
						.get("Study_Coordinator_Name"));
				pScheduleData.setPrincipalInvestigatorName((String) resultSet
						.get("Principal_Investigator_Name"));
				pScheduleData.setPrsCode((String)resultSet.get("PRS_Code"));

				patientList.add(pScheduleData);
			}
		}

		return patientList;
	}

	@Override
	public void insertPatientSchdulingInfoInVGDB(
			final List<PatientScheduleInfo> patientList) {

		String sql = "INSERT INTO PATIENT_SCHEDULE_DATA "
				+ " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

		jdbcTemplate1.batchUpdate(sql, new BatchPreparedStatementSetter() {

			@Override
			public void setValues(PreparedStatement ps, int i)
					throws SQLException {

				pSchInfo = patientList.get(i);
				ps.setString(1, pSchInfo.getStudyNumber());
				ps.setString(2, pSchInfo.getPatientId());
				ps.setString(3, pSchInfo.getStudyCalendarId());
				
				ps.setBigDecimal(4, pSchInfo.getPatprotId());
				ps.setBigDecimal(5, pSchInfo.getPatprotStatus());
				ps.setTimestamp(6, pSchInfo.getPatprotStartDate());

				ps.setBigDecimal(7, pSchInfo.getEventId());
				ps.setString(8, pSchInfo.getEventName());
				ps.setString(9, pSchInfo.getEventDescription());

				ps.setString(10, pSchInfo.getCptCode());
				ps.setString(11, pSchInfo.getCoverageType());

				ps.setString(12, pSchInfo.getVisitName());
				ps.setTimestamp(13, pSchInfo.getEventDate());

				ps.setBigDecimal(14, pSchInfo.getStudyCoordinatorId());
				ps.setString(15, pSchInfo.getStudyCoordinatorName());
				ps.setString(16, pSchInfo.getPrincipalInvestigatorName());
				ps.setString(17, pSchInfo.getPrsCode());

				Timestamp ts = new Timestamp(System.currentTimeMillis());
				ps.setTimestamp(18, ts);

			}

			@Override
			public int getBatchSize() {
				return patientList.size();
			}

		});

	}

}
