package com.velos.integration.ctmsvgdb.service;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.velos.integration.ctmsvgdb.dao.PatientScheduleDataDAOImpl;
import com.velos.integration.ctmsvgdb.dao.PatientScheduleInfo;

public class TestPatientScheduleDataDAO {

	private static Logger logger = Logger
			.getLogger(TestPatientScheduleDataDAO.class.getName());

	private static ApplicationContext context;
	private static List<PatientScheduleInfo> patientList;

	public static void main(String[] args) {

		context = new ClassPathXmlApplicationContext("ctms-config.xml");
		PatientScheduleDataDAOImpl bean = (PatientScheduleDataDAOImpl) context.getBean("patientScheduleDataDAO");

		patientList = new ArrayList<PatientScheduleInfo>();
		try {
			if (bean != null) {

				patientList = bean.getPatientSchdulingInfoFromCTMS();

				if (patientList != null || patientList.size() != 0) {

					logger.info("Method getPatientSchdulingInfoFromCTMS's returned patientList size : "
							+ patientList.size());
					bean.insertPatientSchdulingInfoInVGDB(patientList);
					logger.info("Patient Schedule Data is inserted into VGDB successfully. ");
				}

			}

		} catch (Exception e) {
			logger.info("Exception caught in TestPatientScheduleDataDAO : "
					+ e.getMessage());
		}

	}
}
