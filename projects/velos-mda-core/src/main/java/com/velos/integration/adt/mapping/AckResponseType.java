package com.velos.integration.adt.mapping;

public enum AckResponseType {
	MSH("MSH"),
	MSA("MSA"),
	Pipe("|"),
	EncodingCharacters("^~\\&"),
	SendingApplication("CTMS"),
	SendingFacility("Velos"),
	ReceivingApplication("ADT"),
	ReceivingFacility("MD Anderson"),
	MessageType("ACK"),
	ResponseMessageType("ACK_"),
	Version("2.4")
	;
	
	private String key;
	
	AckResponseType(String key) {
		this.key = key;
	}
	
	@Override
	public String toString() {
		return key;
	}}
