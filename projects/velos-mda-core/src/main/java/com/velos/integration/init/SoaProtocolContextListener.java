package com.velos.integration.init;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SoaProtocolContextListener implements ServletContextListener {
	
	ClassPathXmlApplicationContext context = null;
	
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		((AbstractApplicationContext) this.context).close();
		System.out.println("SoaProtocolContext destroyed");
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		this.context = new ClassPathXmlApplicationContext("soa-protocol-client.xml");
		System.out.println("SoaProtocolContext initialized");
	}

}
