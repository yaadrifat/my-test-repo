package com.velos.integration.coverage.service;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/*
 * Quartz scheduler is used to schedule cron job to update coverage.
 */
public class RunCoverageJob extends QuartzJobBean {
	
	private static Logger logger = Logger.getLogger(RunCoverageJob.class.getName());
	private RunCoverageTask runCoverageTask;

	public void setRunCoverageTask(RunCoverageTask runCoverageTask) {
		this.runCoverageTask = runCoverageTask;
	}

	@Override
	protected void executeInternal(JobExecutionContext arg0)
			throws JobExecutionException {
		try {
			runCoverageTask.runTask();
		} catch (Exception e) {
			logger.info(" Exception caught in executeInternal method of RunCoverageJob " + e.getMessage());
		}		
	}

}
