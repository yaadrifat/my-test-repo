package com.velos.integration.incomingmessage.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;

import com.velos.integration.adt.dao.PatientDaoImpl;
import com.velos.integration.incomingmessage.bean.IncomingMessageBean;

public class IncomingMessageDao {
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplate;

	private static Logger logger = Logger.getLogger(PatientDaoImpl.class);

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	public List<IncomingMessageBean> getIncomingMessages() {
		List<IncomingMessageBean> incomingMessages = new ArrayList<IncomingMessageBean>();
		try {

			String sql = "SELECT PK_MAPPER, TABLENAME, TABLE_PK"
					+ " FROM VGDB_INCOMING_MSG";

			System.out.println("Sql : " + sql);

			jdbcTemplate = new JdbcTemplate(dataSource);
			List<Map> rows = jdbcTemplate.queryForList(sql);
			for (Map row : rows) {
				IncomingMessageBean incomingMessage = new IncomingMessageBean();
				incomingMessage.setPrimaryKey(((BigDecimal) row
						.get("PK_MAPPER")).intValue());
				incomingMessage.setTableName((String) row.get("TABLENAME"));
				incomingMessage.setTableKey(((BigDecimal) row.get("TABLE_PK"))
						.intValue());
				incomingMessages.add(incomingMessage);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return incomingMessages;
	}

	public void deleteMessage(IncomingMessageBean incomingMessage) {

		try {

			String sql = "DELETE FROM VGDB_INCOMING_MSG WHERE PK_MAPPER = "
					+ incomingMessage.getPrimaryKey();

			System.out.println("Sql : " + sql);

			jdbcTemplate = new JdbcTemplate(dataSource);
			int rows = jdbcTemplate.update(sql);

		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}
}
