package com.velos.integration.adt.lookup;

import java.io.IOException;

import org.apache.log4j.Logger;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.Segment;
import ca.uhn.hl7v2.model.v24.segment.MSH;
import ca.uhn.hl7v2.parser.DefaultModelClassFactory;
import ca.uhn.hl7v2.parser.PipeParser;
import ca.uhn.hl7v2.util.Terser;

import com.velos.integration.adt.bean.AckMessageBean;
import com.velos.integration.adt.mapping.AckCodeType;
import com.velos.integration.adt.mapping.AckMessageType;
import com.velos.integration.adt.mapping.AckResponseType;

public class HL7MessageHandler {

	private static Logger logger = Logger.getLogger(HL7MessageHandler.class);

	public Message makeACK(Message inboundMessage, AckMessageBean ackMessageBean)
			throws HL7Exception, IOException {
		Terser t = new Terser(inboundMessage);
		Segment inboundHeader = null;
		MSH msh = null;
		try {
			msh = (MSH) inboundMessage.get("MSH");
			inboundHeader = t.getSegment("MSH");
		} catch (HL7Exception ex) {
			throw new HL7Exception(
					"Need an MSH segment to create a response ACK");
		}
		return this.makeACK(inboundHeader, ackMessageBean, msh);
	}

	public Message makeACK(Segment inboundHeader,
			AckMessageBean ackMessageBean, MSH msh) throws HL7Exception,
			IOException {
		if (!inboundHeader.getName().equals("MSH"))
			throw new HL7Exception(
					"Need an MSH segment to create a response ACK (got "
							+ inboundHeader.getName() + ")");

		// Find the HL7 version of the inbound message:
		//
		String version = null;
		try {
			version = Terser.get(inboundHeader, 12, 0, 1, 1);
		} catch (HL7Exception e) {
			throw new HL7Exception(
					"Failed to get valid HL7 version from inbound MSH-12-1");
		}

		// Make an ACK of the same version as the inbound message:
		//
		String ackClassName = DefaultModelClassFactory
				.getVersionPackageName(version) + "message.ACK";
		Message out = null;
		try {
			Class ackClass = Class.forName(ackClassName);
			out = (Message) ackClass.newInstance();
		} catch (Exception e) {
			throw new HL7Exception("Can't instantiate ACK of class "
					+ ackClassName + ": " + e.getClass().getName());
		}

		StringBuilder mshString = new StringBuilder("");
		mshString.append(AckResponseType.MSH);
		mshString.append(AckResponseType.Pipe);
		mshString.append(AckResponseType.EncodingCharacters);
		mshString.append(AckResponseType.Pipe);
		mshString.append(AckResponseType.SendingApplication);
		mshString.append(AckResponseType.Pipe);
		mshString.append(AckResponseType.SendingFacility);
		mshString.append(AckResponseType.Pipe);
		mshString.append(AckResponseType.ReceivingApplication);
		mshString.append(AckResponseType.Pipe);
		mshString.append(AckResponseType.ReceivingFacility);
		mshString.append(AckResponseType.Pipe);
		mshString.append(AckResponseType.Pipe);
		mshString.append(AckResponseType.Pipe);
		mshString.append(AckResponseType.MessageType);
		mshString.append(AckResponseType.Pipe);
		mshString.append(AckResponseType.ResponseMessageType
				+ msh.getMsh10_MessageControlID().encode().toString());
		mshString.append(AckResponseType.Pipe);
		mshString.append(AckResponseType.Pipe);
		mshString.append(version);

		mshString.append(System.getProperty("line.separator"));

		mshString.append(AckResponseType.MSA);
		mshString.append(AckResponseType.Pipe);
		mshString
				.append(ackMessageBean == null ? AckCodeType.AcknowledgementError
						.toString() : ackMessageBean.getAckCode().toString());
		mshString.append(AckResponseType.Pipe);
		mshString.append(msh.getMsh10_MessageControlID().encode().toString());
		mshString.append(AckResponseType.Pipe);
		mshString.append(ackMessageBean == null ? AckMessageType.AckError
				.toString() : ackMessageBean.getAckMessage().toString());

		logger.info("Response : " + mshString.toString());
		PipeParser pipeParser = new PipeParser();
		out = pipeParser.parse(mshString.toString());

		return out;
	}
}