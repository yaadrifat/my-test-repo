package com.velos.integration.core.processor;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import com.velos.services.ObjectInfo;
import com.velos.services.OperationException_Exception;
import com.velos.services.SimpleIdentifier;

@WebService(targetNamespace = "http://velos.com/services/", name = "UserSEI")
public interface VelosEspUserEndpoint {
	@WebResult(name = "UserOrganizations", targetNamespace = "")
    @RequestWrapper(localName = "getAllOrganizations", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetAllOrganizations")
    @WebMethod
    @ResponseWrapper(localName = "getAllOrganizationsResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetAllOrganizationsResponse")
    public com.velos.services.Organizations getAllOrganizations() throws OperationException_Exception;

}
