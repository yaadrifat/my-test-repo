package com.velos.integration.charges.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.LineNumberReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.support.lob.LobHandler;

import com.velos.integration.charges.bean.ChargesBean;
import com.velos.integration.charges.bean.FolderMapping;
import com.velos.integration.charges.bean.SaveChargeFileBean;
import com.velos.integration.charges.dao.ChargesDaoImpl;

public class ChargesService {
	LobHandler lobHandler;
	ChargesDaoImpl chargesDao;

	public ChargesDaoImpl getChargesDao() {
		return chargesDao;
	}

	public void setChargesDao(ChargesDaoImpl chargesDao) {
		this.chargesDao = chargesDao;
	}

	public LobHandler getLobHandler() {
		return lobHandler;
	}

	public void setLobHandler(LobHandler lobHandler) {
		this.lobHandler = lobHandler;
	}

	private static Logger logger = Logger.getLogger(ChargesDaoImpl.class);

	public String listFilesForFolder(final File folder) {
		for (final File fileEntry : folder.listFiles()) {
			if (fileEntry.isDirectory()) {
				listFilesForFolder(fileEntry);
			} else {
				return fileEntry.getName();
			}
		}
		return "";
	}

	public static java.util.Date stringToDate(String dtString, String format) {
		java.util.Date dt = null;
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		try {
			dt = sdf.parse(dtString);
		} catch (Exception e) {
		}
		return dt;
	}

	public void processInputFile(String chargeType) {
		FolderMapping mapper = this.loadProperties(chargeType);
		String folderName = mapper.getServerInpFiles();

		File filefolder = new File(folderName);
		String fileName = "";
		String fileNameWithFolder = "";

		for (final File fileEntry : filefolder.listFiles()) {
			fileName = fileEntry.getName();

			fileNameWithFolder = folderName + "/" + fileName;
			File file = new File(fileNameWithFolder);
			String fileId = "";
			String batchDateString = "";
			int recordsCount = 0;
			Date batchDate = null;
			int sequence = 0;
			try {
				SaveChargeFileBean saveChargeFileBean = new SaveChargeFileBean();
				final Date date = new Date();
				final byte[] bFile = new byte[(int) file.length()];

				FileInputStream fileInputStream = new FileInputStream(file);
				fileInputStream.read(bFile);
				fileInputStream.close();
				FileReader fr = new FileReader(file);
				LineNumberReader ln = new LineNumberReader(fr);
				String firstLine = "";
				String lastLine = "";
				String currentLine = "";

				while (ln.getLineNumber() == 0)
					firstLine = ln.readLine();
				

				while ((currentLine = ln.readLine()) != null)
					lastLine = currentLine;
				saveChargeFileBean.setFileFooter(lastLine);

				if ("diTech".equals(chargeType)) {
					batchDateString = firstLine.substring(4, 10);
					saveChargeFileBean.setBatchDateString(batchDateString);

					saveChargeFileBean.setBatchDate(stringToDate(
							batchDateString, mapper.getDateFormat()));

					saveChargeFileBean.setBatchDateFormat(mapper
							.getDateFormat());
					
					saveChargeFileBean.setTotalCharges(Integer
							.parseInt(lastLine.substring(lastLine.indexOf('0')).trim()));
					
				} else {
					saveChargeFileBean.setFileHeader(firstLine);
					String firstLineComponents[] = firstLine.split("\\|");
					batchDateString = firstLineComponents[mapper.getBatchDate()];
					saveChargeFileBean.setBatchDateString(batchDateString);

					saveChargeFileBean.setBatchDate(stringToDate(
							batchDateString, mapper.getDateFormat()));

					saveChargeFileBean.setBatchDateFormat(mapper
							.getDateFormat());
					String lastLineComponents[] = lastLine.split("\\|");
					if ("diProf".equals(chargeType)) {
						String segment = lastLineComponents[mapper
								.getRecordsCount()];
						String[] subSegment = segment.split("\\~");
						saveChargeFileBean.setTotalCharges(Integer
								.parseInt(subSegment[1]));
					} else {
						saveChargeFileBean.setTotalCharges(Integer
								.parseInt(lastLineComponents[mapper
										.getRecordsCount()]));
					}
				}
				saveChargeFileBean.setFile(fileEntry);

				saveChargeFileBean.setSequence(chargesDao
						.createSequenceForChargeFiles());

				saveChargeFileBean.setFileType(mapper.getChargeTypeDesc());

				saveChargeFileBean.setLobHandler(this.getLobHandler());

				saveChargeFileBean.setInputMetafilePath(mapper
						.getServerOutFiles());
				chargesDao.saveChargeFile(saveChargeFileBean);

				saveChargeFileBean.setFileName(fileName);
				this.createInputMetaData(saveChargeFileBean);

				this.createCharges(saveChargeFileBean, chargeType);

			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} finally {

			}

		}
	}

	public void createCharges(SaveChargeFileBean saveChargeFileBean,
			String chargeType) {
		ChargesBean chargesBean = null;
		if ("diProf".equals(chargeType)) {
			ProcessChargesForDiProf processChargesForDiProf = new ProcessChargesForDiProf();
			processChargesForDiProf.createCharges(saveChargeFileBean,
					this.getChargesDao());

		}else if ("diTech".equals(chargeType)) {
			ProcessChargesForDiTech processChargesForDiTech = new ProcessChargesForDiTech();
			processChargesForDiTech.createCharges(saveChargeFileBean,
					this.getChargesDao());

		} 
		else {
			ProcessChargesForPathProfAndTech processChargesForPathProfAndTech = new ProcessChargesForPathProfAndTech();
			processChargesForPathProfAndTech.createCharges(saveChargeFileBean,
					this.getChargesDao());
		}
	}

	public boolean createInputMetaData(SaveChargeFileBean saveChargeFileBean) {
		try {
			String fileName = saveChargeFileBean.getFileName();
			String fileExtension = fileName
					.substring(fileName.lastIndexOf('.') + 1);
			String fileNameWithoutExtension = fileName.replaceFirst(
					"[.][^.]+$", "");
			String inputMetaDataFileName = fileNameWithoutExtension
					+ "_INPUTFILE_DETAILS." + fileExtension;
			inputMetaDataFileName = saveChargeFileBean.getInputMetafilePath()
					+ "/" + inputMetaDataFileName;
			File inputMetaDataFile = new File(inputMetaDataFileName);

			// if file doesnt exists, then create it
			if (!inputMetaDataFile.exists()) {
				inputMetaDataFile.createNewFile();
			}

			StringBuilder content = new StringBuilder();
			content.append("Input File Name : " + fileName);
			content.append(System.getProperty("line.separator"));
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");
			 sdf = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss");
			 
			content.append("Date/Time when processing of input file started : "
					+ sdf.format(saveChargeFileBean.getProcessDate()));
			content.append(System.getProperty("line.separator"));
			content.append("System generated File ID : "
					+ saveChargeFileBean.getSystemFileID());
			content.append(System.getProperty("line.separator"));
			content.append("File size : " + saveChargeFileBean.getFileSize());
			content.append(System.getProperty("line.separator"));
			content.append("# of charges : "
					+ saveChargeFileBean.getTotalCharges());
			FileWriter fw = new FileWriter(inputMetaDataFile.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(content.toString());
			bw.close();
			fw.close();
		} catch (Exception e) {
			logger.error(e.getMessage());
			return false;
		}
		return true;
	}

	FolderMapping loadProperties(String folder) {
		FolderMapping folderMapping = new FolderMapping();
		Properties prop = new Properties();
		try {
			prop.load(this.getClass().getClassLoader()
					.getResourceAsStream("poc-config.properties"));
			folderMapping.setChargeTypeDesc(prop.getProperty(folder + "_desc"));
			folderMapping.setBatchDate(Integer.parseInt(prop.getProperty(folder
					+ "_batchDate")));
			folderMapping.setRecordsCount(Integer.parseInt(prop
					.getProperty(folder + "_recordsCount")));
			folderMapping.setServerInpFiles(prop.getProperty(folder
					+ "ServerTEMPInpPath"));
			folderMapping.setServerOutFiles(prop.getProperty(folder
					+ "ServerTEMPOutPath"));
			folderMapping.setDateFormat(prop
					.getProperty(folder + "_dateFormat"));

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return folderMapping;
	}

}
