package com.velos.integration.adt.lookup;

import java.util.StringTokenizer;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.PipeParser;

import com.velos.integration.adt.mapping.AckCodeType;
import com.velos.integration.adt.mapping.AckResponseType;

public class PatientLookupProcessor implements Processor {
	private PatientLookupService patientLookupService;
		
	public PatientLookupService getPatientLookupService() {
		return patientLookupService;
	}

	public void setPatientLookupService(PatientLookupService patientLookupService) {
		this.patientLookupService = patientLookupService;
	}

	private static Logger logger = Logger
			.getLogger(PatientLookupProcessor.class);

	@Override
	public void process(Exchange exchange) throws Exception {
		String message = exchange.getIn().getBody(String.class);

		PipeParser pipeParser = new PipeParser();
		Message output = null;
		try {
			Message input = pipeParser.parse(message);
			output = getPatientLookupService().lookupPatient(input);
			exchange.getOut().setBody(output);
		} catch (Exception e) {
			logger.info("Error while parsing the incoming message : " + message.toString());
			String errorMsg = e.getCause().getMessage();
			if(errorMsg ==null || "".equals(errorMsg))
			 errorMsg = e.getMessage();
			exchange.getOut().setBody(
					patientLookupService.getErrorResponse(message, errorMsg));
		}
	}
	 
}
