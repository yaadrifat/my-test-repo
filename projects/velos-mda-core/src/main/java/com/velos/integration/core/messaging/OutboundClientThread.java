package com.velos.integration.core.messaging;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Properties;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.jms.TopicSubscriber;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.velos.integration.core.processor.PatientStudyStatusMessageProcessor;

public class OutboundClientThread implements Runnable {

	private static Logger logger = Logger.getLogger(OutboundClientThread.class);

	@Override
	public void run() {
		logger.info("Inside run method : ");
		ApplicationContext context = new ClassPathXmlApplicationContext(
				"ctms-config.xml");
		OutboundClient client = (OutboundClient) context
				.getBean("outboundClient");
		Properties prop = new Properties();
		logger.info("OutboundClient");
		try {
			client.run();
		} catch (Exception e) {
			logger.info("error : " + e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
