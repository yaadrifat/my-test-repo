package com.velos.integration.charges.scheduler;

import java.util.List;

import org.apache.log4j.Logger;

import com.velos.integration.charges.lookup.ChargeLookup;
import com.velos.integration.coverage.dao.CoverageDataDAOImpl;
import com.velos.integration.coverage.dao.CoverageDataFromCA;

public class RunTaskCharges {

	private ChargeLookup chargeLookup;
	static int counter;
	public ChargeLookup getChargeLookup() {
		return chargeLookup;
	}

	public void setChargeLookup(ChargeLookup chargeLookup) {
		this.chargeLookup = chargeLookup;
	}

	private static Logger logger = Logger.getLogger(RunTaskCharges.class
			.getName());

	public void runTask() {
		try {
			System.out.println("Counter RunTaskCharges : " + counter ++);
			getChargeLookup().processCharges();
		} catch (Exception e) {
			logger.info("Exception caught in runTask() method of RunTaskCharges : "
					+ e.getMessage());
		}

	}

}
