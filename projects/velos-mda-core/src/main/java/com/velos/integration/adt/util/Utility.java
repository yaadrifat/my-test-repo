package com.velos.integration.adt.util;

public class Utility {
	
	public static boolean isEmpty(String param) {

		if ((!(param == null)) && (!(param.trim()).equals(""))
				&& ((param.trim()).length() > 0))
			return false;
		else
			return true;
	}
}
