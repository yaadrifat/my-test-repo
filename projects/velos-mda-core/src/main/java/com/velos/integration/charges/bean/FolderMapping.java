package com.velos.integration.charges.bean;

public class FolderMapping {

	private String chargeTypeDesc;
	private int batchDate;
	private int recordsCount;
	private String dateFormat;
	private String serverInpFiles;
	private String serverOutFiles;
	private String clientInpFiles;
	private String clientOutFiles;

	public String getChargeTypeDesc() {
		return chargeTypeDesc;
	}

	public void setChargeTypeDesc(String chargeTypeDesc) {
		this.chargeTypeDesc = chargeTypeDesc;
	}

	public int getBatchDate() {
		return batchDate;
	}

	public void setBatchDate(int batchDate) {
		this.batchDate = batchDate;
	}

	public int getRecordsCount() {
		return recordsCount;
	}

	public void setRecordsCount(int recordsCount) {
		this.recordsCount = recordsCount;
	}

	public String getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	public String getServerInpFiles() {
		return serverInpFiles;
	}

	public void setServerInpFiles(String serverInpFiles) {
		this.serverInpFiles = serverInpFiles;
	}

	public String getServerOutFiles() {
		return serverOutFiles;
	}

	public void setServerOutFiles(String serverOutFiles) {
		this.serverOutFiles = serverOutFiles;
	}

	public String getClientInpFiles() {
		return clientInpFiles;
	}

	public void setClientInpFiles(String clientInpFiles) {
		this.clientInpFiles = clientInpFiles;
	}

	public String getClientOutFiles() {
		return clientOutFiles;
	}

	public void setClientOutFiles(String clientOutFiles) {
		this.clientOutFiles = clientOutFiles;
	}
	
	

}
