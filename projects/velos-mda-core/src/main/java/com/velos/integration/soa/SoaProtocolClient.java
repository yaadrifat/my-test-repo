package com.velos.integration.soa;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.support.AbstractApplicationContext;
//import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.integration.Message;
import org.springframework.integration.MessageChannel;
import org.springframework.integration.MessageHandlingException;
import org.springframework.integration.core.MessagingTemplate;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.stereotype.Component;
import org.springframework.ws.soap.security.wss4j.Wss4jSecurityInterceptor;


/**
 * Client to call SOA Protocol triggering service. This should have been called SoaProtocolClient.
 * 
 * @author ihuang
 */
@Component
public class SoaProtocolClient implements ApplicationContextAware {
	
	public static void main(String[] args) {
		SoaProtocolClient mdaClient = new SoaProtocolClient();
		Map<String, String> requestMap = new HashMap<String, String>();
		requestMap.put(StudyNumberKey, "IH-A22");
		requestMap.put(ActionKey, "add");
		requestMap.put(SubmissionTypeKey, "Initial Study");
		requestMap.put(InterfaceVersionKey, "1");
		requestMap.put(SubmissionDateKey, "2014-08-19");
		if (context == null) {
			context = new ClassPathXmlApplicationContext("soa-protocol-client.xml");
		}
		Map<String, Object> resultMap = null;
		try {
			resultMap = mdaClient.handleRequest(requestMap);
			System.out.println("Received: "+resultMap.get(GetStudyResponseKey));
		} catch (Exception e) {
			e.printStackTrace();
		}
		((AbstractApplicationContext) context).close();
	}
	
	private static boolean isDebugMode = false;
	private static ApplicationContext context = null; // This should be passed in from SoaProtocolContextListener
	
	public static final String StudyNumberKey = "studyNumber";
	public static final String ActionKey = "action";
	public static final String SubmissionTypeKey = "submissionType";
	public static final String InterfaceVersionKey = "InterfaceVersion";
	public static final String SubmissionDateKey = "SubmissionDate";
	public static final String GetStudyResponseKey = "getStudyResponse";
	
	private static final String studyRequestTemplate =
			"<v1:ProcessProtocol xmlns:v1=\"http://MDAnderson.org/SOAServices/V1_0\">" +
					"<v1:studyNumber>%s</v1:studyNumber>" +
					"<v1:action>%s</v1:action>" +
					"<v1:submissionType>%s</v1:submissionType>" +
					"<v1:InterfaceVersion>%s</v1:InterfaceVersion>"+
					"<v1:SubmissionDate>%s</v1:SubmissionDate>"+
					"</v1:ProcessProtocol>";
	private static final String studyRequestTemplate1 =
			"<ser:getStudy xmlns:ser=\"http://velos.com/services/\">" +
			"<StudyIdentifier><studyNumber>%s</studyNumber></StudyIdentifier></ser:getStudy>";
	
	public Map<String, Object> handleRequest(Map<String, String> requestMap) throws Exception {
    	if (requestMap == null) { return null; }
		MessageChannel channel = null;
		String body = null;
        Map<String, Object> resultMap = new HashMap<String, Object>();
        channel = context.getBean("protocolRequests", MessageChannel.class);
        body = String.format(studyRequestTemplate, 
        		requestMap.get(StudyNumberKey),
        		requestMap.get(ActionKey),
        		requestMap.get(SubmissionTypeKey),
        		requestMap.get(InterfaceVersionKey),
        		requestMap.get(SubmissionDateKey));
        if (isDebugMode) {
        	System.out.println("Warning: SOA interface is running on DEBUG mode!!");
        	body = String.format(studyRequestTemplate1, requestMap.get(StudyNumberKey));
        }
		System.out.println("Requested:"+body);
        MessagingTemplate messagingTemplate = new MessagingTemplate();
        Message<?> message = null;
        try {
        	message = messagingTemplate.sendAndReceive(
                channel, MessageBuilder.withPayload(body).build());
            resultMap.put(GetStudyResponseKey, message.getPayload());
        } catch(MessageHandlingException e) {
        	e.printStackTrace();
        	throw e;
        }
		return resultMap;
	}
	
	//@Bean  // This is commented out; wssBean is injected in mda-client-req.xml
    public Wss4jSecurityInterceptor wssBean() {
    	Wss4jSecurityInterceptor wssBean = new Wss4jSecurityInterceptor();
    	wssBean.setSecurementActions("UsernameToken");
    	wssBean.setSecurementUsername("myuser");
    	wssBean.setSecurementPassword("mypass");
    	wssBean.setSecurementPasswordType("PasswordText");
    	return wssBean;
    }

	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		context = applicationContext;
	}
	
}
