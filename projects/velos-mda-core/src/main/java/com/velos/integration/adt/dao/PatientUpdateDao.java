package com.velos.integration.adt.dao;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;

import com.velos.integration.adt.bean.PatientBean;
import com.velos.integration.adt.util.Utility;
import com.velos.integration.incomingmessage.bean.IncomingMessageBean;

public class PatientUpdateDao {
	private DataSource dataSource;
	private DataSource dataSourceEres;
	private DataSource dataSourceEpat;

	private JdbcTemplate jdbcTemplate;

	private static Logger logger = Logger.getLogger(PatientUpdateDao.class);

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;

	}

	public void setDataSourceEres(DataSource dataSourceEres) {
		this.dataSourceEres = dataSourceEres;
	}

	public void setDataSourceEpat(DataSource dataSourceEpat) {
		this.dataSourceEpat = dataSourceEpat;
	}

	public PatientBean getPatientData(IncomingMessageBean incomingMessage) {
		PatientBean patient = new PatientBean();
		try {
			String sql = "SELECT PERSON_CODE, ADDL_ETHNICITY, ADDL_RACE, ADDRESS1,"
					+ " ADDRESS2, WORK_PHONE, CITY, COUNTRY,"
					+ " COUNTY, DOB, DEATH_DATE, ETHNICITY,"
					+ " FIRSTNAME, GENDER, HOME_PHONE, LASTNAME,"
					+ " MARITAL_STATUS, MIDDLENAME, ZIPCODE, SURVIVAL_STATUS,"
					+ " RACE, SSN, STATE  FROM PATIENT_EMR  WHERE PK_PEMR = ?";
			jdbcTemplate = new JdbcTemplate(dataSource);

			Map row = jdbcTemplate.queryForMap(sql,
					new Object[] { incomingMessage.getTableKey() });

			patient.setAdditionalEthnicity((String) row.get("ADDL_ETHNICITY"));
			patient.setAdditionalRace((String) row.get("ADDL_RACE"));
			patient.setAddress1((String) row.get("ADDRESS1"));
			patient.setAddress2((String) row.get("ADDRESS2"));
			patient.setwPhoneNumber((String) row.get("WORK_PHONE"));
			patient.setCity((String) row.get("CITY"));
			patient.setCountry((String) row.get("COUNTRY"));
			patient.setCounty((String) row.get("COUNTY"));
			patient.setDateOfBirth((java.util.Date) row.get("DOB"));
			patient.setDeathDate((java.util.Date) row.get("DEATH_DATE"));
			patient.setEthnicity((String) row.get("ETHNICITY"));
			patient.setFirstName((String) row.get("FIRSTNAME"));
			patient.setGender((String) row.get("GENDER"));
			patient.sethPhoneNumber((String) row.get("HOME_PHONE"));
			patient.setLastName((String) row.get("LASTNAME"));
			patient.setMaritalStatus((String) row.get("MARITAL_STATUS"));
			patient.setMiddleName((String) row.get("MIDDLENAME"));
			patient.setSurvivalStatus((String) row.get("SURVIVAL_STATUS"));
			patient.setRace((String) row.get("RACE"));
			patient.setSsn((String) row.get("SSN"));
			patient.setState((String) row.get("STATE"));
			patient.setZipcode((String) row.get("ZIPCODE"));
			patient.setPatientID((String) row.get("PERSON_CODE"));

		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return patient;
	}

	public List checkRecordExistsInCTMS(PatientBean patient, String parentOrganization) {
		List<Map> rows = null;
		List<Integer> pkList = new ArrayList<Integer>();

		String sql = "select ep.fk_per from er_patfacility ep where ep.pat_facilityid = ? and (select count(*) from er_site"
				+ " where regexp_replace(lower(site_name), '[[:space:]]*','') = regexp_replace(lower(?), '[[:space:]]*','') start with pk_site = ep.fk_site connect by prior site_parent = pk_site ) > 0";

		jdbcTemplate = new JdbcTemplate(dataSourceEres);
		try {
			rows = jdbcTemplate.queryForList(sql,
					new Object[] { patient.getPatientID(), parentOrganization });
			for (Map row : rows) {
				pkList.add(((BigDecimal) row.get("FK_PER")).intValue());
			}
		} catch (EmptyResultDataAccessException em) {
			pkList = null;
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return pkList;
	}

	public void updateRecordInCTMS(final PatientBean patient,
			final List<Integer> pkList) {
		List<Map> rows = null;
		PreparedStatement preparedStatement = null;
		try {

			String sql = "UPDATE PERSON SET "
					+ " PERSON_ADD_ETHNICITY = ?, PERSON_ADD_RACE = ?, PERSON_ADDRESS1 = ?,"
					+ " PERSON_ADDRESS2 = ?, PERSON_BPHONE = ?, PERSON_CITY = ?, PERSON_COUNTRY = ?,"
					+ " PERSON_COUNTY = ?, PERSON_DOB = ?, PERSON_DEATHDT = ?, FK_CODELST_ETHNICITY = ?,"
					+ " PERSON_FNAME = ?, FK_CODELST_GENDER = ?, PERSON_HPHONE = ?, PERSON_LNAME = ?,"
					+ " FK_CODELST_MARITAL = ?, PERSON_MNAME = ?, FK_CODELST_PSTAT = ?,"
					+ " FK_CODELST_RACE = ?, PERSON_SSN = ?, PERSON_STATE = ?, PERSON_ZIP = ?"
					+ " WHERE PK_PERSON IN ";

			StringBuilder queryBuilder = new StringBuilder(sql);
			queryBuilder.append("(");
			for (int i = 0; i < pkList.size(); i++) {
				queryBuilder.append(" ?");
				if (i != pkList.size() - 1)
					queryBuilder.append(",");
			}
			queryBuilder.append(")");

			jdbcTemplate.update(queryBuilder.toString(),
					new PreparedStatementSetter() {
						public void setValues(
								PreparedStatement preparedStatement)
								throws SQLException {
							if (patient.getAdditionalEthnicity() == null)
								preparedStatement.setNull(1,
										java.sql.Types.VARCHAR);
							else
								preparedStatement.setString(
										1,
										(Utility.isEmpty(patient
												.getAdditionalEthnicity()) ? null
												: getAddltnlCodes(
														"ethnicity",
														patient.getAdditionalEthnicity())));

							if (patient.getAdditionalRace() == null)
								preparedStatement.setNull(2,
										java.sql.Types.VARCHAR);
							else
								preparedStatement.setString(
										2,
										(Utility.isEmpty(patient
												.getAdditionalRace()) ? null
												: getAddltnlCodes(
														"race",
														patient.getAdditionalRace())));

							if (patient.getAddress1() == null)
								preparedStatement.setNull(3,
										java.sql.Types.VARCHAR);
							else
								preparedStatement.setString(3,
										patient.getAddress1());

							if (patient.getAddress2() == null)
								preparedStatement.setNull(4,
										java.sql.Types.VARCHAR);
							else
								preparedStatement.setString(4,
										patient.getAddress2());

							if (patient.getwPhoneNumber() == null)
								preparedStatement.setNull(5,
										java.sql.Types.VARCHAR);
							else
								preparedStatement.setString(5,
										patient.getwPhoneNumber());

							if (patient.getCity() == null)
								preparedStatement.setNull(6,
										java.sql.Types.VARCHAR);
							else
								preparedStatement.setString(6,
										patient.getCity());

							if (patient.getCountry() == null)
								preparedStatement.setNull(7,
										java.sql.Types.VARCHAR);
							else
								preparedStatement.setString(7,
										patient.getCountry());

							if (patient.getCounty() == null)
								preparedStatement.setNull(8,
										java.sql.Types.VARCHAR);
							else
								preparedStatement.setString(8,
										patient.getCounty());

							if (patient.getDateOfBirth() == null)
								preparedStatement.setNull(9,
										java.sql.Types.DATE);
							else
								preparedStatement.setDate(9, new java.sql.Date(
										patient.getDateOfBirth().getTime()));

							if (patient.getDeathDate() == null)
								preparedStatement.setNull(10,
										java.sql.Types.DATE);
							else
								preparedStatement.setDate(10, patient
										.getDeathDate() == null ? null
										: new java.sql.Date(patient
												.getDeathDate().getTime()));

							if (patient.getEthnicity() == null)
								preparedStatement.setNull(11,
										java.sql.Types.INTEGER);
							else
								preparedStatement.setInt(
										11,
										(Utility.isEmpty(patient.getEthnicity()) ? null
												: getERCode("ethnicity",
														patient.getEthnicity())));

							if (patient.getFirstName() == null)
								preparedStatement.setNull(12,
										java.sql.Types.VARCHAR);
							else
								preparedStatement.setString(12,
										patient.getFirstName());

							if (patient.getGender() == null)
								preparedStatement.setNull(13,
										java.sql.Types.INTEGER);
							else
								preparedStatement.setInt(
										13,
										(Utility.isEmpty(patient.getGender()) ? null
												: getERCode("gender",
														patient.getGender())));

							if (patient.gethPhoneNumber() == null)
								preparedStatement.setNull(14,
										java.sql.Types.VARCHAR);
							else
								preparedStatement.setString(14,
										patient.gethPhoneNumber());

							if (patient.getLastName() == null)
								preparedStatement.setNull(15,
										java.sql.Types.VARCHAR);
							else
								preparedStatement.setString(15,
										patient.getLastName());

							if (patient.getMaritalStatus() == null)
								preparedStatement.setNull(16,
										java.sql.Types.INTEGER);
							else
								preparedStatement.setInt(
										16,
										(Utility.isEmpty(patient
												.getMaritalStatus()) ? null
												: getERCode(
														"marital_st",
														patient.getMaritalStatus())));

							if (patient.getMiddleName() == null)
								preparedStatement.setNull(17,
										java.sql.Types.VARCHAR);
							else
								preparedStatement.setString(17,
										patient.getMiddleName());

							if (patient.getSurvivalStatus() == null)
								preparedStatement.setNull(18,
										java.sql.Types.INTEGER);
							else
								preparedStatement.setInt(
										18,
										(Utility.isEmpty(patient
												.getSurvivalStatus()) ? null
												: getERCode(
														"patient_status",
														patient.getSurvivalStatus())));

							if (patient.getRace() == null)
								preparedStatement.setNull(19,
										java.sql.Types.INTEGER);
							else
								preparedStatement.setInt(19, (Utility
										.isEmpty(patient.getRace()) ? null
										: getERCode("race", patient.getRace())));

							if (patient.getSsn() == null)
								preparedStatement.setNull(20,
										java.sql.Types.VARCHAR);
							else
								preparedStatement.setString(20,
										patient.getSsn());

							if (patient.getState() == null)
								preparedStatement.setNull(21,
										java.sql.Types.VARCHAR);
							else
								preparedStatement.setString(21,
										patient.getState());

							if (patient.getZipcode() == null)
								preparedStatement.setNull(22,
										java.sql.Types.VARCHAR);
							else
								preparedStatement.setString(22,
										patient.getZipcode());

							int count = 22;

							for (int pk : pkList) {
								preparedStatement.setInt(++count, pk);
							}
						}
					});

		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {

			if (preparedStatement != null)
				try {
					preparedStatement.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}

	}

	public int getERCode(String codeType, String codeSubType) {
		int erCode = 0;
		List<Map> rows = null;
		String sql = "select PK_CODELST from er_codelst where CODELST_TYPE=? and CODELST_SUBTYP=?";
		jdbcTemplate = new JdbcTemplate(dataSourceEres);
		try {
			rows = jdbcTemplate.queryForList(sql, new Object[] { codeType,
					codeSubType });
			for (Map row : rows) {
				erCode = ((BigDecimal) row.get("PK_CODELST")).intValue();
				break;
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return erCode;
	}

	public String getAddltnlCodes(String codeType, String codeSubTypes) {
		StringBuilder erCodes = new StringBuilder();
		StringTokenizer st = new StringTokenizer(codeSubTypes, ",");
		String token = "";
		int codeId = 0;
		String delim = "";
		while (st.hasMoreElements()) {
			token = st.nextToken().trim();
			codeId = getERCode(codeType, token);
			erCodes.append(delim);
			erCodes.append(codeId);
			delim = ",";
		}
		return erCodes.toString();
	}
}
