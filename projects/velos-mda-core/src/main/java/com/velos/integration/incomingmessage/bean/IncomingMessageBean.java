package com.velos.integration.incomingmessage.bean;

public class IncomingMessageBean {
	private int primaryKey;
	private String tableName;
	private int tableKey;
	private String tableKeyColName;

	public int getPrimaryKey() {
		return primaryKey;
	}

	public void setPrimaryKey(int primaryKey) {
		this.primaryKey = primaryKey;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public int getTableKey() {
		return tableKey;
	}

	public void setTableKey(int tableKey) {
		this.tableKey = tableKey;
	}

	public String getTableKeyColName() {
		return tableKeyColName;
	}

	public void setTableKeyColName(String tableKeyColName) {
		this.tableKeyColName = tableKeyColName;
	}

}
