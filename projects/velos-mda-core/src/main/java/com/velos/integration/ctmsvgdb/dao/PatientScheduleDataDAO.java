package com.velos.integration.ctmsvgdb.dao;

import java.util.List;

public interface PatientScheduleDataDAO {

	public List<PatientScheduleInfo> getPatientSchdulingInfoFromCTMS();

	public void insertPatientSchdulingInfoInVGDB(List<PatientScheduleInfo> patientList);
}
