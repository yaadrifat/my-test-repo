package com.velos.integration.core.messaging;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.velos.integration.core.processor.PatientStudyStatusMessageProcessor;

public class OutboundSAXProcessor {
	private PatientStudyStatusMessageProcessor processor;

	public PatientStudyStatusMessageProcessor getProcessor() {
		return processor;
	}

	public void setProcessor(PatientStudyStatusMessageProcessor processor) {
		this.processor = processor;
	}

	public void processBySAXParser(String xml) {
		try {
			byte[] byteString = xml.getBytes();
			ByteArrayInputStream in = new ByteArrayInputStream(byteString);
			InputSource inputSource = new InputSource(in);
			SAXParserFactory factory = SAXParserFactory.newInstance();

			SAXParser parser;

			parser = factory.newSAXParser();

			CustomHandler handler = new CustomHandler();
			parser.parse(inputSource, handler);

			if (handler.getParentOID().size() == 1
					&& handler.getParentOID().containsKey(
							IdentifierConstants.SIMPLE_IDENTIFIER)) {
				this.getProcessor().processPatientStudyStatMessage(handler);
			}

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
