package com.velos.integration.adt.service;

import java.lang.reflect.Field;

import org.apache.log4j.Logger;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.DataTypeException;
import ca.uhn.hl7v2.model.v24.datatype.CE;
import ca.uhn.hl7v2.model.v24.datatype.MSG;
import ca.uhn.hl7v2.model.v24.datatype.XPN;
import ca.uhn.hl7v2.model.v24.datatype.XTN;
import ca.uhn.hl7v2.model.v24.segment.MSH;
import ca.uhn.hl7v2.model.v24.segment.PID;

import com.velos.integration.adt.bean.AckMessageBean;
import com.velos.integration.adt.bean.PatientBean;
import com.velos.integration.adt.mapping.AckCodeType;
import com.velos.integration.adt.mapping.CodeSubTypeMapper;
import com.velos.integration.adt.mapping.AckMessageType;
import com.velos.integration.adt.util.Utility;

public class PatientService {

	private static Logger logger = Logger.getLogger(PatientService.class);

	public AckMessageBean validatePatientDetails(MSH msh, PID pid)
			throws DataTypeException, HL7Exception {
		AckMessageBean ackMessage = new AckMessageBean();

		MSG msg = msh.getMsh9_MessageType();
		System.out.println(msh.getMsh12_VersionID().encode());
		String messageType = msg.getComponent(0).encode();
		if (messageType == null || "".equals(messageType)) {
			logger.error(AckMessageType.MessageTypeRequired);
			ackMessage.setAckCode(AckCodeType.AcknowledgementReject);
			ackMessage.setAckMessage(AckMessageType.MessageTypeRequired);
			return ackMessage;
		}

		String eventType = msg.getComponent(1).encode();
		if (eventType == null || "".equals(eventType)) {
			logger.error(AckMessageType.EventTypeRequired);
			ackMessage.setAckCode(AckCodeType.AcknowledgementReject);
			ackMessage.setAckMessage(AckMessageType.EventTypeRequired);
			return ackMessage;
		}

		String MessageID = msh.getMsh10_MessageControlID().encode();
		if (MessageID == null || "".equals(MessageID)) {
			logger.error(AckMessageType.MessageIDRequired);
			ackMessage.setAckCode(AckCodeType.AcknowledgementReject);
			ackMessage.setAckMessage(AckMessageType.MessageIDRequired);
			return ackMessage;
		}

		String patientID = pid.getPid3_PatientIdentifierList(0).getComponent(0)
				.encode();
		if (patientID == null || "".equals(patientID)) {
			logger.error(AckMessageType.PatientIDRequired);
			ackMessage.setAckCode(AckCodeType.AcknowledgementReject);
			ackMessage.setAckMessage(AckMessageType.PatientIDRequired);
			return ackMessage;
		}

		XPN xpn = pid.getPid5_PatientName(0);
		String lastName = xpn.getComponent(0).encode();
		if (lastName == null || "".equals(lastName)) {
			logger.error(AckMessageType.LastNameRequired);
			ackMessage.setAckCode(AckCodeType.AcknowledgementReject);
			ackMessage.setAckMessage(AckMessageType.LastNameRequired);
			return ackMessage;
		}

		String firstName = xpn.getComponent(1).encode();
		if (firstName == null || "".equals(firstName)) {
			logger.error(AckMessageType.FirstNameRequired);
			ackMessage.setAckCode(AckCodeType.AcknowledgementReject);
			ackMessage.setAckMessage(AckMessageType.FirstNameRequired);
			return ackMessage;
		}

		String dateOfBirth = pid.getPid7_DateTimeOfBirth().getComponent(0)
				.encode();
		if (dateOfBirth == null || "".equals(dateOfBirth)) {
			logger.error(AckMessageType.DOBRequired);
			ackMessage.setAckCode(AckCodeType.AcknowledgementReject);
			ackMessage.setAckMessage(AckMessageType.DOBRequired);
			return ackMessage;
		}
		ackMessage.setAckCode(AckCodeType.AcknowledgementInProcess);
		return ackMessage;
	}

	public PatientBean setPatientDetails(MSH msh, PID pid) throws DataTypeException,
			HL7Exception {
		PatientBean patient = new PatientBean();
		String delim = "";
		
		//Event Type
		patient.setEventType(msh.getMsh9_MessageType().getComponent(1).encode());
				
		// Patient Code
		patient.setPatientID(pid.getPid3_PatientIdentifierList(0)
				.getComponent(0).encode());

		// First Name, Last Name and Middle Name
		XPN xpn = pid.getPid5_PatientName(0);
		patient.setLastName(xpn.getComponent(0).encode());
		patient.setFirstName(xpn.getComponent(1).encode());
		patient.setMiddleName(xpn.getComponent(2).encode());

		// DOB
		String dateOfBirth = pid.getPid7_DateTimeOfBirth().getComponent(0)
				.encode();
		patient.setDateOfBirth(patient.returnDate(dateOfBirth));

		// Gender
		String gender = pid.getPid8_AdministrativeSex().encode();
		if (Utility.isEmpty(CodeSubTypeMapper.getGendercodemap().get(gender)))
			gender = CodeSubTypeMapper.getGendercodemap().get("ELSE");
		else
			gender = CodeSubTypeMapper.getGendercodemap().get(gender);
		patient.setGender(gender);

		// Primary and Additional Race
		CE[] primaryRaceTypeList = pid.getPid10_Race();
		if (primaryRaceTypeList.length > 0) {
			String primaryRace = primaryRaceTypeList[0].getComponent(0)
					.encode();
			if (Utility.isEmpty(CodeSubTypeMapper.getRacecodemap().get(
					primaryRace)))
				primaryRace = CodeSubTypeMapper.getRacecodemap().get("ELSE");
			else
				primaryRace = CodeSubTypeMapper.getRacecodemap().get(
						primaryRace);
			patient.setRace(primaryRace);
		} else {
			patient.setRace("");
		}

		StringBuilder addlRaceBuilder = new StringBuilder();
		for (int i = 1; i < primaryRaceTypeList.length; i++) {
			addlRaceBuilder.append(delim);
			delim = ", ";
			String addlRace = primaryRaceTypeList[i].getComponent(0).encode();
			if (Utility.isEmpty(CodeSubTypeMapper.getRacecodemap()
					.get(addlRace)))
				addlRace = CodeSubTypeMapper.getRacecodemap().get("ELSE");
			else
				addlRace = CodeSubTypeMapper.getRacecodemap().get(addlRace);
			addlRaceBuilder.append(addlRace);
		}
		String addlRace = addlRaceBuilder.toString();
		patient.setAdditionalRace(addlRace);

		// Address1, Address2, City, State, Zipcode, Country
		patient.setAddress1(pid.getPid11_PatientAddress(0).getComponent(0)
				.encode());
		patient.setAddress2(pid.getPid11_PatientAddress(0).getComponent(1)
				.encode());
		patient.setCity(pid.getPid11_PatientAddress(0).getComponent(2).encode());
		patient.setState(pid.getPid11_PatientAddress(0).getComponent(3)
				.encode());
		patient.setZipcode(pid.getPid11_PatientAddress(0).getComponent(4)
				.encode());
		patient.setCountry(pid.getPid11_PatientAddress(0).getComponent(5)
				.encode());

		// County
		patient.setCounty(pid.getPid12_CountyCode().encode());

		// Home Phone Number
		XTN[] homePhoneTypeList = pid.getPid13_PhoneNumberHome();
		StringBuilder homePhoneBuilder = new StringBuilder();
		delim = "";
		for (XTN homePhoneType : homePhoneTypeList) {
			homePhoneBuilder.append(delim);
			delim = ", ";
			homePhoneBuilder.append(("".equals(homePhoneType.getComponent(4)
					.encode())) ? "" : homePhoneType.getComponent(4).encode());
			homePhoneBuilder.append(("".equals(homePhoneType.getComponent(5)
					.encode()) ? "" : ("".equals(homePhoneType.getComponent(4)
					.encode()) ? homePhoneType.getComponent(5).encode() : "-"
					+ homePhoneType.getComponent(5).encode())));
			homePhoneBuilder.append(("".equals(homePhoneType.getComponent(6)
					.encode()) ? "" : ("".equals(homePhoneType.getComponent(5)
					.encode()) ? homePhoneType.getComponent(6).encode() : "-"
					+ homePhoneType.getComponent(6).encode())));
			homePhoneBuilder.append(("".equals(homePhoneType.getComponent(7)
					.encode()) ? "" : ("".equals(homePhoneType.getComponent(6)
					.encode()) ? homePhoneType.getComponent(7).encode() : "-"
					+ homePhoneType.getComponent(7).encode())));
		}
		String homePhone = homePhoneBuilder.toString();
		patient.sethPhoneNumber(homePhone);

		// Work Phone Number
		XTN[] workPhoneTypeList = pid.getPid14_PhoneNumberBusiness();
		StringBuilder workPhnonBuilder = new StringBuilder();
		delim = "";
		for (XTN workPhoneType : workPhoneTypeList) {
			workPhnonBuilder.append(delim);
			delim = ", ";
			workPhnonBuilder.append(("".equals(workPhoneType.getComponent(4)
					.encode())) ? "" : workPhoneType.getComponent(4).encode());
			workPhnonBuilder.append(("".equals(workPhoneType.getComponent(5)
					.encode()) ? "" : ("".equals(workPhoneType.getComponent(4)
					.encode()) ? workPhoneType.getComponent(5).encode() : "-"
					+ workPhoneType.getComponent(5).encode())));
			workPhnonBuilder.append(("".equals(workPhoneType.getComponent(6)
					.encode()) ? "" : ("".equals(workPhoneType.getComponent(5)
					.encode()) ? workPhoneType.getComponent(6).encode() : "-"
					+ workPhoneType.getComponent(6).encode())));
			workPhnonBuilder.append(("".equals(workPhoneType.getComponent(7)
					.encode()) ? "" : ("".equals(workPhoneType.getComponent(6)
					.encode()) ? workPhoneType.getComponent(7).encode() : "-"
					+ workPhoneType.getComponent(7).encode())));

		}
		String workPhone = workPhnonBuilder.toString();
		patient.setwPhoneNumber(workPhone);

		// Ethnicity and Additional ethnicity
		CE[] ethnicityTypeList = pid.getPid22_EthnicGroup();
		if (ethnicityTypeList.length > 0) {
			String ethnicity = ethnicityTypeList[0].getComponent(0).encode();
			if (Utility.isEmpty(CodeSubTypeMapper.getEthnicitycodemap().get(
					ethnicity)))
				ethnicity = CodeSubTypeMapper.getEthnicitycodemap().get("ELSE");
			else
				ethnicity = CodeSubTypeMapper.getEthnicitycodemap().get(
						ethnicity);
			patient.setEthnicity(ethnicity);

		} else {
			patient.setEthnicity("");
		}

		StringBuilder ethnicityBuilder = new StringBuilder();
		delim = "";
		for (int i = 1; i < ethnicityTypeList.length; i++) {
			ethnicityBuilder.append(delim);
			delim = ", ";
			String addlEthnicity = ethnicityTypeList[i].getComponent(0)
					.encode();
			if (Utility.isEmpty(CodeSubTypeMapper.getEthnicitycodemap().get(
					addlEthnicity)))
				addlEthnicity = CodeSubTypeMapper.getEthnicitycodemap().get(
						"ELSE");
			else
				addlEthnicity = CodeSubTypeMapper.getEthnicitycodemap().get(
						addlEthnicity);
			ethnicityBuilder.append(addlEthnicity);
		}
		String addlEthnicity = ethnicityBuilder.toString();
		patient.setAdditionalEthnicity(addlEthnicity);

		// Death Date
		String deathDate = pid.getPid29_PatientDeathDateAndTime()
				.getComponent(0).encode();
		patient.setDeathDate(patient.returnDate(deathDate));

		// Survival Status
		String survivalStatus = pid.getPid30_PatientDeathIndicator().encode();
		if (Utility.isEmpty(CodeSubTypeMapper.getSurvivalstatcodemap().get(
				survivalStatus)))
			survivalStatus = CodeSubTypeMapper.getSurvivalstatcodemap().get(
					"ELSE");
		else
			survivalStatus = CodeSubTypeMapper.getSurvivalstatcodemap().get(
					survivalStatus);
		patient.setSurvivalStatus(survivalStatus);

		// Marital Status
		String maritalStatus = pid.getPid16_MaritalStatus().getComponent(0)
				.encode();
		if (Utility.isEmpty(CodeSubTypeMapper.getMaritalstatuscodemap().get(
				maritalStatus)))
			maritalStatus = CodeSubTypeMapper.getMaritalstatuscodemap().get(
					"ELSE");
		else
			maritalStatus = CodeSubTypeMapper.getMaritalstatuscodemap().get(
					maritalStatus);
		patient.setMaritalStatus(maritalStatus);

		// SSN
		patient.setSsn(pid.getPid19_SSNNumberPatient().encode());

		// Logging the Patient Bean Object
		for (Field field : patient.getClass().getDeclaredFields()) {
			try {
				field.setAccessible(true);
				String name = field.getName();
				Object value;

				value = field.get(patient);

				logger.info(name + " : " + value);
			} catch (IllegalArgumentException e) {
				logger.error(e.getMessage());
			} catch (IllegalAccessException e) {
				logger.error(e.getMessage());
			}

		}

		return patient;
	}
}
