package com.velos.integration.charges.lookup;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.velos.integration.charges.bean.FolderMapping;
import com.velos.integration.charges.service.ChargesService;

public class ChargeLookup {
	private ChargesService service;
	private CamelContext context;
	
	public CamelContext getContext() {
		return context;
	}

	public void setContext(CamelContext context) {
		this.context = context;
	}

	public ChargesService getService() {
		return service;
	}

	public void setService(ChargesService service) {
		this.service = service;
	}

	private void moveToClientTempInputFiles(String chargeType) throws Exception {
		try {
			if (chargeType == "diProf") {
				this.getContext().startRoute("diProfClientTempInp");
				Thread.sleep(10000);
				this.getContext().stopRoute("diProfClientTempInp");
			} else if (chargeType == "diTech") {
				this.getContext().startRoute("diTechClientTempInp");
				Thread.sleep(10000);
				this.getContext().stopRoute("diTechClientTempInp");
			}else if (chargeType == "pathTech") {
				this.getContext().startRoute("pathTechClientTempInp");
				Thread.sleep(10000);
				this.getContext().stopRoute("pathTechClientTempInp");
			} else if (chargeType == "pathProf") {
				this.getContext().startRoute("pathProfClientTempInp");
				Thread.sleep(10000);
				this.getContext().stopRoute("pathProfClientTempInp");
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void moveToServer(String chargeType) throws Exception {
		try {
			if (chargeType == "diProf") {
				this.getContext().startRoute("diProfServerTempInp");
				Thread.sleep(10000);
				this.getContext().stopRoute("diProfServerTempInp");
			}else if (chargeType == "diTech") {
				this.getContext().startRoute("diTechServerTempInp");
				Thread.sleep(10000);
				this.getContext().stopRoute("diTechServerTempInp");
			}else if (chargeType == "pathTech") {
				this.getContext().startRoute("pathTechServerTempInp");
				Thread.sleep(10000);
				this.getContext().stopRoute("pathTechServerTempInp");
			} else if (chargeType == "pathProf") {
				this.getContext().startRoute("pathProfServerTempInp");
				Thread.sleep(10000);
				this.getContext().stopRoute("pathProfServerTempInp");
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void moveToClientTempOutputFile(String chargeType) throws Exception {
		try {
			if (chargeType == "diProf") {
				this.getContext().startRoute("diProfClientTempOut");
				Thread.sleep(10000);
				this.getContext().stopRoute("diProfClientTempOut");
			} else if (chargeType == "diTech") {
				this.getContext().startRoute("diTechClientTempOut");
				Thread.sleep(10000);
				this.getContext().stopRoute("diTechClientTempOut");
			}else if (chargeType == "pathTech") {
				this.getContext().startRoute("pathTechClientTempOut");
				Thread.sleep(10000);
				this.getContext().stopRoute("pathTechClientTempOut");
			} else if (chargeType == "pathProf") {
				this.getContext().startRoute("pathProfClientTempOut");
				Thread.sleep(10000);
				this.getContext().stopRoute("pathProfClientTempOut");
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*private void moveToServerBackTemp(String chargeType) throws Exception {
		try {
			if (chargeType == "diProf") {
				this.getContext().startRoute("diProfServerBackTempInp");
				Thread.sleep(10000);
				this.getContext().stopRoute("diProfServerBackTempInp");
				this.getContext().startRoute("diProfServerBackTempOut");
				Thread.sleep(10000);
				this.getContext().stopRoute("diProfServerBackTempOut");
			} else if (chargeType == "pathTech") {
				this.getContext().startRoute("pathTechServerBackTempInp");
				Thread.sleep(10000);
				this.getContext().stopRoute("pathTechServerBackTempInp");
				this.getContext().startRoute("pathTechServerBackTempOut");
				Thread.sleep(10000);
				this.getContext().stopRoute("pathTechServerBackTempOut");
			} else if (chargeType == "pathProf") {
				this.getContext().startRoute("pathProfServerBackTempInp");
				Thread.sleep(10000);
				this.getContext().stopRoute("pathProfServerBackTempInp");
				this.getContext().startRoute("pathProfServerBackTempOut");
				Thread.sleep(10000);
				this.getContext().stopRoute("pathProfServerBackTempOut");
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/

	public void processCharges() throws Exception {

		for (int i = 0; i < 4; i++) {
			String chargeType = "";
			String folder = "";
			switch (i) {
			case 0:
				chargeType = "diProf";
				break;
			case 1:
				chargeType = "diTech";
				break;
			case 2:
				chargeType = "pathTech";
				break;
			case 3:
				chargeType = "pathProf";
				break;
			}
			
			this.moveToClientTempInputFiles(chargeType);
			this.moveToServer(chargeType);

			this.getService().processInputFile(chargeType);
			
			 this.moveToClientTempOutputFile(chargeType);
			// this.moveToServerBackTemp(chargeType);
			System.out.println("Finished Processing " + chargeType);
		}

	}


}
