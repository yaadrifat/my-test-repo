
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for userIdentifier complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="userIdentifier">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}simpleIdentifier">
 *       &lt;sequence>
 *         &lt;element name="firstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="organizationIdentifier" type="{http://velos.com/services/}organizationIdentifier" minOccurs="0"/>
 *         &lt;element name="userLoginName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "userIdentifier", propOrder = {
    "firstName",
    "lastName",
    "organizationIdentifier",
    "groupIdentifier",
    "userLoginName"
})
public class UserIdentifier
    extends SimpleIdentifier
{

    protected String firstName;
    protected String lastName;
    protected OrganizationIdentifier organizationIdentifier;
    private GroupIdentifier groupIdentifier = null;
    protected String userLoginName;

    /**
     * Gets the value of the firstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the value of the firstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstName(String value) {
        this.firstName = value;
    }

    /**
     * Gets the value of the lastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the value of the lastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastName(String value) {
        this.lastName = value;
    }

    /**
     * Gets the value of the organizationIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link OrganizationIdentifier }
     *     
     */
    public OrganizationIdentifier getOrganizationIdentifier() {
        return organizationIdentifier;
    }

    /**
     * Sets the value of the organizationIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganizationIdentifier }
     *     
     */
    public void setOrganizationIdentifier(OrganizationIdentifier value) {
        this.organizationIdentifier = value;
    }

    
    public GroupIdentifier getGroupIdentifier() {
		return groupIdentifier;
	}

	public void setGroupIdentifier(GroupIdentifier groupIdentifier) {
		this.groupIdentifier = groupIdentifier;
	}
	
	
    public String getUserLoginName() {
        return userLoginName;
    }

    /**
     * Sets the value of the userLoginName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserLoginName(String value) {
        this.userLoginName = value;
    }

}
