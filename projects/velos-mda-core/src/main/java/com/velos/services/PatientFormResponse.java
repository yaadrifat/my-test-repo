
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for patientFormResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="patientFormResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}formResponse">
 *       &lt;sequence>
 *         &lt;element name="formFilledFormId" type="{http://velos.com/services/}patientFormResponseIdentifier" minOccurs="0"/>
 *         &lt;element name="patientID" type="{http://velos.com/services/}patientIdentifier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "patientFormResponse", propOrder = {
    "formFilledFormId",
    "patientID"
})
public class PatientFormResponse
    extends FormResponse
{

    protected PatientFormResponseIdentifier formFilledFormId;
    protected PatientIdentifier patientID;

    /**
     * Gets the value of the formFilledFormId property.
     * 
     * @return
     *     possible object is
     *     {@link PatientFormResponseIdentifier }
     *     
     */
    public PatientFormResponseIdentifier getFormFilledFormId() {
        return formFilledFormId;
    }

    /**
     * Sets the value of the formFilledFormId property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientFormResponseIdentifier }
     *     
     */
    public void setFormFilledFormId(PatientFormResponseIdentifier value) {
        this.formFilledFormId = value;
    }

    /**
     * Gets the value of the patientID property.
     * 
     * @return
     *     possible object is
     *     {@link PatientIdentifier }
     *     
     */
    public PatientIdentifier getPatientID() {
        return patientID;
    }

    /**
     * Sets the value of the patientID property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientIdentifier }
     *     
     */
    public void setPatientID(PatientIdentifier value) {
        this.patientID = value;
    }

}
