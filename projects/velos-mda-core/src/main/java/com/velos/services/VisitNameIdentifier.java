
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for visitNameIdentifier complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="visitNameIdentifier">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}visitIdentifier">
 *       &lt;sequence>
 *         &lt;element name="visitName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "visitNameIdentifier", propOrder = {
    "visitName"
})
public class VisitNameIdentifier
    extends VisitIdentifier
{

    protected String visitName;

    /**
     * Gets the value of the visitName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVisitName() {
        return visitName;
    }

    /**
     * Sets the value of the visitName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVisitName(String value) {
        this.visitName = value;
    }

}
