CREATE OR REPLACE 
PACKAGE "PKG_PATIENT_SCHEDULE" AS 
PROCEDURE SP_PURGE_PATIENTSCHEDULEDATA;
END PKG_PATIENT_SCHEDULE;

CREATE OR REPLACE
PACKAGE BODY PKG_PATIENT_SCHEDULE AS

  PROCEDURE SP_PURGE_PATIENTSCHEDULEDATA AS
  BEGIN
    delete from patient_schedule_data;
    -- This below line should be used on QA/Staging and Preprod/Prod env. Above line should be used on dev env.
    -- delete from patient_schedule_data where created_on < sysdate-30;
    commit;
  END SP_PURGE_PATIENTSCHEDULEDATA;

END PKG_PATIENT_SCHEDULE;

commit;