--DROP TABLE patient_schedule_data;
CREATE TABLE patient_schedule_data (
     	 protocol#      				VARCHAR2(4000 BYTE),
		 patient_id      				VARCHAR2(4000 BYTE),
		 study_calendar_id    			VARCHAR2(4000 BYTE), 
		 schedule_id					NUMBER,
		 schedule_status				NUMBER,
		 schedule_start_date			DATE,
		 service_code					VARCHAR2(4000 BYTE),
		 service_name					VARCHAR2(4000 BYTE),
		 service_description			VARCHAR2(4000 BYTE),		 
		 cpt_code      					VARCHAR2(50 BYTE),
		 modifier						VARCHAR2(15),
		 visit_name						VARCHAR2(4000 BYTE),
		 event_date						DATE,
		 study_coordinator_id  			VARCHAR2(50 BYTE),
		 study_coordinator_name			VARCHAR2(4000 BYTE),
		 principal_investigator_name	VARCHAR2(4000 BYTE),
		 prs_code      					VARCHAR2(50 BYTE),		
		 created_on						DATE
); 
commit;




