-- inetrval = for Qa/stage , preprod and prod env: First day of the month at midnight
-- interval = for dev and test env : 'SYSDATE+15/1440'
DECLARE
  X NUMBER;
BEGIN
  SYS.DBMS_JOB.SUBMIT
  ( job       => X
   ,what      => 'PKG_PATIENT_SCHEDULE.SP_PURGE_PATIENTSCHEDULEDATA;'
   ,next_date => to_date(SYSDATE,'dd/mm/yyyy hh24:mi:ss')
   ,interval  => 'TRUNC(LAST_DAY(SYSDATE ) + 1)'
   ,no_parse  => FALSE
  );
  SYS.DBMS_OUTPUT.PUT_LINE('Job Number is: ' || to_char(x));
COMMIT;
END;
/