
  CREATE TABLE "PATIENT_EMR" 
   (	"PERSON_CODE" VARCHAR2(20 BYTE), 
	"LASTNAME" VARCHAR2(30 BYTE), 
	"FIRSTNAME" VARCHAR2(30 BYTE), 
	"MIDDLENAME" VARCHAR2(20 BYTE), 
	"DOB" DATE, 
	"GENDER" VARCHAR2(15 BYTE), 
	"RACE" VARCHAR2(15 BYTE), 
	"ADDRESS1" VARCHAR2(100 BYTE), 
	"ADDRESS2" VARCHAR2(100 BYTE), 
	"CITY" VARCHAR2(100 BYTE), 
	"STATE" VARCHAR2(100 BYTE), 
	"ZIPCODE" VARCHAR2(20 BYTE), 
	"COUNTRY" VARCHAR2(100 BYTE), 
	"COUNTY" VARCHAR2(100 BYTE), 
	"HOME_PHONE" VARCHAR2(100 BYTE), 
	"WORK_PHONE" VARCHAR2(100 BYTE), 
	"ETHNICITY" VARCHAR2(15 BYTE), 
	"DEATH_DATE" DATE, 
	"SURVIVAL_STATUS" VARCHAR2(15 BYTE), 
	"MARITAL_STATUS" VARCHAR2(15 BYTE), 
	"SSN" VARCHAR2(16 BYTE), 
	"ADDL_RACE" VARCHAR2(100 BYTE), 	
	"ADDL_ETHNICITY" VARCHAR2(100 BYTE), 
	"CREATED_DATE" DATE DEFAULT sysdate, 
	"LAST_MODIFIED_DATE" DATE DEFAULT NULL
   );
   /
   
   
  CREATE OR REPLACE TRIGGER "PATIENT_EMR_BU_LM" 
BEFORE UPDATE
ON PATIENT_EMR
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
BEGIN	:NEW.LAST_MODIFIED_DATE := SYSDATE;
 END;
/

create or replace FUNCTION f_datediffemr(p_date DATE, p_base_date DATE := SYSDATE,p_inclunit char:='Y') RETURN varchar
IS
      v_diff NUMBER(10) := 0;
      v_unit varchar2(10) :='Days';
BEGIN
      if ((p_date is null) or length(p_date)=0) then
      return '';
      end if;
      v_diff:=TRUNC(MONTHS_BETWEEN(p_base_date,p_date));
      if (v_diff=0) then
      v_diff:=trunc(p_base_date) - trunc (p_date) + 1;
      if (v_diff=0) then
      v_diff:=1;
      end if;
      v_unit:='Days';
      elsif (v_diff>=12)  then
       v_unit:='Years';
       v_diff:=trunc(v_diff/12);
      elsif ( (v_diff>0) or (v_diff<21)) then
      v_unit:='Months';
      end if;
      if (p_inclunit='Y') then
     RETURN to_char(v_diff) || ' ' || v_unit ;
     else
     RETURN to_char(v_diff);
     end if;
END;
/


   CREATE OR REPLACE FORCE VIEW "PATIENT_EMR_VIEW" ("AGE_DTL", "AGE", "FULLNAME", "PERSON_CODE", "LASTNAME", "FIRSTNAME", "MIDDLENAME", "DOB", "GENDER", "RACE", "ADDRESS1", "ADDRESS2", "CITY", "STATE", "ZIPCODE", "COUNTRY", "COUNTY", "HOME_PHONE", "WORK_PHONE", "ETHNICITY", "DEATH_DATE", "SURVIVAL_STATUS", "MARITAL_STATUS", "SSN", "ADDL_RACE", "ADDL_ETHNICITY", "LAST_MODIFIED_DATE", "CREATED_DATE") AS 
  (
SELECT decode(DEATH_DATE,null,f_datediffemr(DOB),f_datediffemr(DOB,DEATH_DATE)) AGE_DTL,
TRUNC(months_between(sysdate,dob)/12) AGE,
  (decode(FIRSTNAME,'','',FIRSTNAME ||' ') ||
       decode(MIDDLENAME,'','',MIDDLENAME ||' ') || LASTNAME) FULLNAME ,
       
  PERSON_CODE,
  LASTNAME,
  FIRSTNAME,
  MIDDLENAME,
  DOB,
  GENDER,
  RACE,
  ADDRESS1,
  ADDRESS2,
  CITY,
  STATE,
  ZIPCODE,
  COUNTRY,
  COUNTY,
  HOME_PHONE,
  WORK_PHONE,
  ETHNICITY,
  DEATH_DATE,
  SURVIVAL_STATUS,
  MARITAL_STATUS,
  SSN,
  ADDL_RACE,
  ADDL_ETHNICITY,
  LAST_MODIFIED_DATE,
  CREATED_DATE
  
FROM PATIENT_EMR);
/
