--insert into er_codelst for event additional more details

INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL)
VALUES (SEQ_ER_CODELST.NEXTVAL, 'evtaddlcode', 'prs_discount', 'Professional fee percent discount','N',1, 'moredetails.jsp');

INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL)
VALUES (SEQ_ER_CODELST.NEXTVAL, 'evtaddlcode', 'hcc_discount', 'Technical fee percent discount','N',1, 'moredetails.jsp');

INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL)
VALUES (SEQ_ER_CODELST.NEXTVAL, 'evtaddlcode', 'sponsor', 'Sponsor','N',1, 'moredetails.jsp');

INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL)
VALUES (SEQ_ER_CODELST.NEXTVAL, 'evtaddlcode', 'sponsor_id', 'Sponsor Id','N',1, 'moredetails.jsp');

commit;

