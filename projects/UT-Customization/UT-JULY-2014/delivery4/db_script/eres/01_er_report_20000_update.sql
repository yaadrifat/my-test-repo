SET define OFF;
-- INSERTING into ER_REPORT
--- Velos Invoice
DECLARE
  v_record_exists NUMBER := 0;
BEGIN
  SELECT COUNT(*) INTO v_record_exists FROM er_report WHERE pk_report = 20000;
  IF (v_record_exists != 0) THEN

update er_report set REP_SQL = 'SELECT LINEITEM_NAME,
BUDGET_NAME,
BGTCAL_PROTTYPE,
PROT_CALENDAR,
BUDGET_VERSION,
BUDGET_DESC,
STUDY_NUMBER,
IRB_NUMBER,
STUDY_TITLE,
STUDY_PRINV,
STUDY_SPONSOR,
SPONSOR_NUMBER,
SITE_NAME,
CATEGORY,
BGTSECTION_NAME,
case when (FK_EVENT is not null AND ~1 = 0) then '' '' else CPT_CODE end as CPT_CODE,
esch.lst_additionalcodes_financial(FK_EVENT,''evtaddlcode'',~1) as ADDITIONALCODE,
NVL(COST_PER_PATIENT,0) AS COST_PER_PATIENT,
DECODE(LINE_ITEM_INDIRECTS_FLAG,1,''Yes'','' '') AS LINE_ITEM_INDIRECTS_FLAG,
DECODE(COST_DISCOUNT_ON_LINE_ITEM,1,''Yes'','' '') AS COST_DISCOUNT_ON_LINE_ITEM,
INDIRECTS,
BUDGET_INDIRECT_FLAG,
FRINGE_BENEFIT,
FRINGE_FLAG,
PER_PATIENT_LINE_FRINGE,
TOTAL_LINE_FRINGE,
BUDGET_DISCOUNT,
DECODE(BUDGET_DISCOUNT_FLAG,1,''Discount'',2,''Markup'','' '') AS BUDGET_DISCOUNT_FLAG,
TOTAL_PAT_COST_AFTER,
PER_PAT_LINE_ITEM_DISCOUNT,
BUDGET_CURRENCY,
UNIT_COST_NEW,
UNIT_COST_FLAG,
UNIT_COST,
NUMBER_OF_UNIT,
case when BGTSECTION_PATNO is null then ''1'' else BGTSECTION_PATNO end AS BGTSECTION_PATNO,
LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,
DECODE(COST_CUSTOMCOL,''research'',''No'',''soc'',''Yes'',''Other'') AS standard_of_care,
category_subtyp,
NVL(LINEITEM_SPONSORAMOUNT, 0) AS SPONSOR_AMOUNT,
VARIANCE_PER_PAT,
VARIANCE_ALL_PAT,
FK_CODELST_COST_TYPE,COST_TYPE_DESC,lineitem_direct_perpat_new,total_cost_per_pat_new,total_cost_all_pat_new,
lineitem_direct_perpat,total_cost_per_pat, total_cost_all_pat,pk_budgetsec,nvl(budgetsec_fkvisit,0)budgetsec_fkvisit,pk_lineitem,lineitem_inpersec,
nvl(bgtcal_excldsocflag,0) as bgtcal_excldsocflag,budgetsection_sequence,lineitem_seq ,lower(LINEITEM_NAME) as l_LINEITEM_NAME,BGTSECTION_TYPE,
SUBCOST_ITEM_FLAG,
NVL(FK_EVENT, 0) as FK_EVENT
FROM erv_budget_comp
WHERE pk_budget = ~2 AND
pk_bgtcal = ~3
UNION ALL
SELECT '' '' as LINEITEM_NAME, NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,BGTSECTION_NAME,NULL ,
NULL, NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,
case when BGTSECTION_PATNO is null then ''1'' else BGTSECTION_PATNO end AS BGTSECTION_PATNO,NULL ,NULL ,NULL ,NULL ,NULL , NULL ,NULL ,NULL ,NULL ,
0,0,0,0,0 , 0 ,pk_budgetsec,0,NULL ,0 ,NULL,bgtsection_sequence as budgetsection_sequence ,NULL as lineitem_seq ,null as l_LINEITEM_NAME, BGTSECTION_TYPE,0,
0
from sch_bgtsection s where s.fk_bgtcal = ~3 AND NVL (bgtsection_delflag, ''N'') = ''N'' AND not exists (select * from erv_budget_comp v where pk_bgtcal = ~3 and v.pk_budgetsec = s.pk_budgetsec )
ORDER BY budgetsection_sequence, budgetsec_fkvisit, lineitem_seq,l_LINEITEM_NAME '
where pk_report = 20000;

update er_report set REP_SQL_CLOB = 'SELECT LINEITEM_NAME,
BUDGET_NAME,
BGTCAL_PROTTYPE,
PROT_CALENDAR,
BUDGET_VERSION,
BUDGET_DESC,
STUDY_NUMBER,
IRB_NUMBER,
STUDY_TITLE,
STUDY_PRINV,
STUDY_SPONSOR,
SPONSOR_NUMBER,
SITE_NAME,
CATEGORY,
BGTSECTION_NAME,
case when (FK_EVENT is not null AND ~1 = 0) then '' '' else CPT_CODE end as CPT_CODE,
esch.lst_additionalcodes_financial(FK_EVENT,''evtaddlcode'',~1) as ADDITIONALCODE,
NVL(COST_PER_PATIENT,0) AS COST_PER_PATIENT,
DECODE(LINE_ITEM_INDIRECTS_FLAG,1,''Yes'','' '') AS LINE_ITEM_INDIRECTS_FLAG,
DECODE(COST_DISCOUNT_ON_LINE_ITEM,1,''Yes'','' '') AS COST_DISCOUNT_ON_LINE_ITEM,
INDIRECTS,
BUDGET_INDIRECT_FLAG,
FRINGE_BENEFIT,
FRINGE_FLAG,
PER_PATIENT_LINE_FRINGE,
TOTAL_LINE_FRINGE,
BUDGET_DISCOUNT,
DECODE(BUDGET_DISCOUNT_FLAG,1,''Discount'',2,''Markup'','' '') AS BUDGET_DISCOUNT_FLAG,
TOTAL_PAT_COST_AFTER,
PER_PAT_LINE_ITEM_DISCOUNT,
BUDGET_CURRENCY,
UNIT_COST_NEW,
UNIT_COST_FLAG,
UNIT_COST,
NUMBER_OF_UNIT,
case when BGTSECTION_PATNO is null then ''1'' else BGTSECTION_PATNO end AS BGTSECTION_PATNO,
LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,
DECODE(COST_CUSTOMCOL,''research'',''No'',''soc'',''Yes'',''Other'') AS standard_of_care,
category_subtyp,
NVL(LINEITEM_SPONSORAMOUNT, 0) AS SPONSOR_AMOUNT,
VARIANCE_PER_PAT,
VARIANCE_ALL_PAT,
FK_CODELST_COST_TYPE,COST_TYPE_DESC,lineitem_direct_perpat_new,total_cost_per_pat_new,total_cost_all_pat_new,
lineitem_direct_perpat,total_cost_per_pat, total_cost_all_pat,pk_budgetsec,nvl(budgetsec_fkvisit,0)budgetsec_fkvisit,pk_lineitem,lineitem_inpersec,
nvl(bgtcal_excldsocflag,0) as bgtcal_excldsocflag,budgetsection_sequence,lineitem_seq ,lower(LINEITEM_NAME) as l_LINEITEM_NAME,BGTSECTION_TYPE,
SUBCOST_ITEM_FLAG,
NVL(FK_EVENT, 0) as FK_EVENT
FROM erv_budget_comp
WHERE pk_budget = ~2 AND
pk_bgtcal = ~3
UNION ALL
SELECT '' '' as LINEITEM_NAME, NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,BGTSECTION_NAME,NULL ,
NULL, NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,
case when BGTSECTION_PATNO is null then ''1'' else BGTSECTION_PATNO end AS BGTSECTION_PATNO,NULL ,NULL ,NULL ,NULL ,NULL , NULL ,NULL ,NULL ,NULL ,
0,0,0,0,0 , 0 ,pk_budgetsec,0,NULL ,0 ,NULL,bgtsection_sequence as budgetsection_sequence ,NULL as lineitem_seq ,null as l_LINEITEM_NAME, BGTSECTION_TYPE,0,
0
from sch_bgtsection s where s.fk_bgtcal = ~3 AND NVL (bgtsection_delflag, ''N'') = ''N'' AND not exists (select * from erv_budget_comp v where pk_bgtcal = ~3 and v.pk_budgetsec = s.pk_budgetsec )
ORDER BY budgetsection_sequence, budgetsec_fkvisit, lineitem_seq,l_LINEITEM_NAME '
where pk_report = 20000;
COMMIT;
  END IF;
END;
/