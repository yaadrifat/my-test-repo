CREATE OR REPLACE FORCE VIEW "ESCH"."ERV_BUDGET_COMP" ("PK_BUDGET", "BGTCAL_PROTID", "PK_BGTCAL", "BGTCAL_PROTTYPE", "PROT_CALENDAR", "BUDGET_NAME", "BUDGET_VERSION", "BUDGET_DESC", "STUDY_NUMBER", "IRB_NUMBER", "FK_STUDY", "STUDY_TITLE", "STUDY_PRINV", "STUDY_SPONSOR", "SPONSOR_NUMBER", "SITE_NAME", "BGTSECTION_NAME", "BGTSECTION_PATNO", "LINEITEM_NAME", "TOTAL_PAT_COST_AFTER", "TOTAL_LINE_FRINGE", "PER_PATIENT_LINE_FRINGE", "CATEGORY", "CATEGORY_SEQ", "CATEGORY_SUBTYP", "UNIT_COST_NEW", "UNIT_COST_FLAG", "UNIT_COST", "NUMBER_OF_UNIT", "STANDARD_OF_CARE", "COST_PER_PATIENT", "COST_DISCOUNT_ON_LINE_ITEM", "PER_PAT_LINE_ITEM_DISCOUNT", "CPT_CODE", "TMID", "CDM", "LINE_ITEM_INDIRECTS_FLAG", "INDIRECTS", "BUDGET_INDIRECT_FLAG", "FRINGE_BENEFIT", "FRINGE_FLAG", "BUDGET_DISCOUNT", "BUDGET_DISCOUNT_FLAG", "BUDGET_CURRENCY", "LAST_MODIFIED_BY", "LAST_MODIFIED_DATE", "BUDGETSECTION_SEQUENCE", "BGTCAL_INDIRECTCOST",
  "LINEITEM_DESC", "LINEITEM_SPONSORAMOUNT", "LINEITEM_APPLYPATIENTCOUNT", "VARIANCE_PER_PAT", "VARIANCE_ALL_PAT", "BGTSECTION_TYPE", "LINEITEM_OTHERCOST", "FK_CODELST_COST_TYPE", "COST_TYPE_DESC", "LINEITEM_SEQ", "LINITEM_SPONSOR_PERPAT", "LINEITEM_DIRECT_PERPAT_NEW", "TOTAL_COST_PER_PAT_NEW", "TOTAL_COST_ALL_PAT_NEW", "LINEITEM_DIRECT_PERPAT", "TOTAL_COST_PER_PAT", "TOTAL_COST_ALL_PAT", "COST_CUSTOMCOL", "PK_BUDGETSEC", "BUDGETSEC_FKVISIT", "PK_LINEITEM", "LINEITEM_INPERSEC", "LINEITEM_FKEVENT", "BGTCAL_EXCLDSOCFLAG", "BUDGET_COMBFLAG", "COVERAGE_TYPE", "SUBCOST_ITEM_FLAG", "FK_EVENT")
AS
  SELECT DISTINCT pk_budget,
    bgtcal_protid,
    pk_bgtcal,
    bgtcal_prottype,
    prot_calendar,
    budget_name,
    budget_version,
    budget_desc,
    study_number,
    irb_number,
    fk_study,
    study_title,
    study_prinv,
    study_sponsor,
    sponsor_number,
    site_name,
    bgtsection_name,
    bgtsection_patno,
    lineitem_name,
    ROUND(NVL (total_pat_cost_after, 0), 2) total_pat_cost_after,
    total_line_fringe,
    per_patient_line_fringe,
    CATEGORY,
    category_seq,
    category_subtyp,
    unit_cost_new,
    unit_cost_flag,
    unit_cost,
    number_of_unit,
    standard_of_care,
    ROUND(cost_per_patient, 2) AS cost_per_patient,
    cost_discount_on_line_item,
    per_pat_line_item_discount,
    cpt_code,
    tmid,
    cdm,
    line_item_indirects_flag,
    indirects,
    budget_indirect_flag,
    fringe_benefit,
    fringe_flag,
    budget_discount,
    budget_discount_flag,
    budget_currency,
    last_modified_by,
    last_modified_date,
    bgtsection_sequence,
    NVL (bgtcal_indirectcost, 0) AS bgtcal_indirectcost,
    lineitem_desc,
    ROUND((lineitem_sponsoramount ), 2) lineitem_sponsoramount,
    LINEITEM_APPLYPATIENTCOUNT,
    (ROUND(NVL ( (DECODE ( budget_indirect_flag, 'Y', total_pat_cost_after * line_item_indirects_flag * indirects/100 , 0 ) + lineitem_direct_perpat), 0 ) , 2) - ROUND(NVL ( (DECODE ( budget_indirect_flag, 'Y', total_pat_cost_after_new * line_item_indirects_flag * indirects/100 , 0 ) + lineitem_direct_perpat_new), 0 ) , 2)) AS variance_per_pat,
    (ROUND(NVL ( (DECODE ( budget_indirect_flag, 'Y', total_pat_cost_after * line_item_indirects_flag * indirects/100 , 0 ) + lineitem_direct_perpat) * bgtsection_patno, 0 ) , 2) - ROUND(NVL ( (DECODE ( budget_indirect_flag, 'Y', total_pat_cost_after_new * line_item_indirects_flag * indirects/100 , 0 ) + lineitem_direct_perpat_new) * bgtsection_patno, 0 ) , 2)) AS variance_all_pat,
    bgtsection_type,
    LINEITEM_OTHERCOST,
    FK_CODELST_COST_TYPE,
    COST_TYPE_DESC,
    lineitem_seq,
    linitem_sponsor_perpat,
    ROUND(DECODE (bgtsection_type, 'P',NVL (lineitem_direct_perpat_new, 0),'O',0 ) ,2) lineitem_direct_perpat_new,
    ROUND(DECODE (bgtsection_type, 'P',NVL ( DECODE ( budget_indirect_flag, 'Y' , total_pat_cost_after_new * line_item_indirects_flag * indirects/100 , 0 ) + lineitem_direct_perpat_new, 0 ) ,'O',0), 2) as total_cost_per_pat_new,
    ROUND(NVL ( (DECODE ( budget_indirect_flag, 'Y', total_pat_cost_after_new * line_item_indirects_flag * indirects/100 , 0 ) + lineitem_direct_perpat_new) * bgtsection_patno, 0 ) , 2) as total_cost_All_pat_new,
    ROUND(DECODE (bgtsection_type, 'P',NVL (lineitem_direct_perpat, 0),'O',0 ) ,2) lineitem_direct_perpat,
    ROUND(DECODE (bgtsection_type, 'P',NVL ( DECODE ( budget_indirect_flag, 'Y' , total_pat_cost_after * line_item_indirects_flag * indirects/100 , 0 ) + lineitem_direct_perpat, 0 ) ,'O',0), 2)              AS total_cost_per_pat,
    ROUND(NVL ( (DECODE ( budget_indirect_flag, 'Y', total_pat_cost_after                              * line_item_indirects_flag * indirects/100 , 0 ) + lineitem_direct_perpat) * bgtsection_patno, 0 ) , 2) AS total_cost_all_pat,
    (SELECT codelst_custom_col1
    FROM sch_codelst
    WHERE pk_codelst = FK_CODELST_COST_TYPE
    ) COST_CUSTOMCOL,
    pk_budgetsec,
    budgetsec_fkvisit,
    pk_lineitem,
    lineitem_inpersec,
    lineitem_fkevent,
    bgtcal_excldsocflag,
    BUDGET_COMBFLAG,
    coverage_type,
    SUBCOST_ITEM_FLAG,
    FK_EVENT
  FROM
    (SELECT pk_budget,
      bgtcal_protid,
      pk_bgtcal,
      bgtcal_prottype,
      DECODE(bgtcal_prottype, 'L',
      ( SELECT NAME FROM event_def WHERE event_id = bgtcal_protid
      ) , 'S',
      ( SELECT NAME FROM event_assoc WHERE event_id = bgtcal_protid
      ) , NULL, 'No Calendar') AS prot_calendar,
      budget_name,
      budget_version,
      budget_desc,
      study_number,
      irb_number,
      fk_study,
      study_title,
      study_prinv,
      study_sponsor,
      sponsor_number,
      site_name,
      bgtsection_name,
      bgtsection_patno,
      lineitem_name,
      ROUND ( (DECODE(unit_cost_flag, 'No' , 0 , per_patient_line_fringe) + (unit_cost_new * lineitem_clinicnofunit)) - DECODE ( bgtcal_discountflag, 0, 0, 2, 0, 1, (DECODE(unit_cost_flag, 'No' , 0 , per_patient_line_fringe) + (unit_cost_new * lineitem_clinicnofunit)) * bgtcal_discount * lineitem_incostdisc / 100 ) + DECODE ( bgtcal_discountflag, 0, 0, 1, 0, 2, (DECODE(unit_cost_flag, 'No' , 0 , per_patient_line_fringe) + (unit_cost_new * lineitem_clinicnofunit)) * bgtcal_discount * lineitem_incostdisc / 100 ), 2 ) AS total_pat_cost_after_new,
      ROUND ( (DECODE(unit_cost_flag, 'Yes' , 0 , per_patient_line_fringe) + (lineitem_othercost)) - DECODE ( bgtcal_discountflag, 0, 0, 2, 0, 1, (DECODE(unit_cost_flag, 'Yes' , 0 , per_patient_line_fringe) + (lineitem_othercost)) * bgtcal_discount * lineitem_incostdisc / 100 ) + DECODE ( bgtcal_discountflag, 0, 0, 1, 0, 2, (DECODE(unit_cost_flag, 'Yes' , 0 , per_patient_line_fringe) + (lineitem_othercost)) * bgtcal_discount * lineitem_incostdisc / 100 ), 2 ) AS total_pat_cost_after,
      total_line_fringe,
      per_patient_line_fringe,
      CATEGORY,
      category_seq,
      category_subtyp,
      unit_cost_new,
      unit_cost_flag,
      lineitem_sponsorunit                                                                                                                                                                                                                  AS unit_cost,
      lineitem_clinicnofunit                                                                                                                                                                                                                AS number_of_unit,
      DECODE (lineitem_stdcarecost, 0, 0, lineitem_stdcarecost)                                                                                                                                                                             AS standard_of_care,
      lineitem_othercost                                                                                                                                                                                                                    AS cost_per_patient,
      lineitem_incostdisc                                                                                                                                                                                                                   AS cost_discount_on_line_item,
      DECODE ( bgtcal_discountflag, 0, 0, (lineitem_incostdisc * ( lineitem_othercost * DECODE ( category_subtyp, 'ctgry_per', NVL2 (bgtcal_frgbenefit, bgtcal_frgbenefit / 100 , 0), 0 ) + lineitem_othercost) * bgtcal_discount / 100 ) ) AS per_pat_line_item_discount,
      lineitem_cptcode                                                                                                                                                                                                                      AS cpt_code,
      lineitem_tmid                                                                                                                                                                                                                         AS tmid,
      lineitem_cdm                                                                                                                                                                                                                          AS cdm,
      lineitem_applyindirects                                                                                                                                                                                                               AS line_item_indirects_flag,
      NVL (bgtcal_sponsorohead, 0)                                                                                                                                                                                                          AS indirects,
      bgtcal_sponsorflag                                                                                                                                                                                                                    AS budget_indirect_flag,
      NVL (bgtcal_frgbenefit, 0)                                                                                                                                                                                                            AS fringe_benefit,
      bgtcal_frgflag                                                                                                                                                                                                                        AS fringe_flag,
      NVL (bgtcal_discount, 0)                                                                                                                                                                                                              AS budget_discount,
      bgtcal_discountflag                                                                                                                                                                                                                   AS budget_discount_flag,
      budget_currency,
      last_modified_by,
      last_modified_date,
      bgtsection_sequence,
      bgtcal_indirectcost,
      lineitem_desc,
      lineitem_sponsoramount,
      LINEITEM_APPLYPATIENTCOUNT,
      bgtsection_type,
      LINEITEM_OTHERCOST,
      FK_CODELST_COST_TYPE,
      COST_TYPE_DESC,
      lineitem_seq,
      linitem_sponsor_perpat,
      ROUND ( (NVL (DECODE(unit_cost_flag, 'No' , 0 , per_patient_line_fringe), 0) + DECODE(unit_cost_flag,'Other',lineitem_othercost,(unit_cost_new * LINEITEM_CLINICNOFUNIT))) - DECODE( bgtcal_discountflag, 0,0,2,0,1, (DECODE(unit_cost_flag, 'No' , 0 , per_patient_line_fringe) + DECODE(unit_cost_flag,'Other',lineitem_othercost,(unit_cost_new * LINEITEM_CLINICNOFUNIT))) * bgtcal_discount * lineitem_incostdisc / 100 ) + DECODE( bgtcal_discountflag, 0,0,1,0,2,(DECODE(unit_cost_flag, 'No' , 0 , per_patient_line_fringe) + DECODE(unit_cost_flag,'Other',lineitem_othercost,(unit_cost_new * LINEITEM_CLINICNOFUNIT))) * bgtcal_discount * lineitem_incostdisc / 100 ), 2 ) AS lineitem_direct_perpat_new,
      ROUND ( (NVL (DECODE(unit_cost_flag, 'Yes' , 0 , per_patient_line_fringe), 0) + (lineitem_othercost)) - DECODE( bgtcal_discountflag, 0,0,2,0,1, (DECODE(unit_cost_flag, 'Yes' , 0 , per_patient_line_fringe) + ( lineitem_othercost)) * bgtcal_discount * lineitem_incostdisc / 100 ) + DECODE( bgtcal_discountflag, 0,0,1,0,2,(DECODE(unit_cost_flag, 'Yes' , 0 , per_patient_line_fringe) + ( lineitem_othercost)) * bgtcal_discount * lineitem_incostdisc / 100 ), 2 ) AS lineitem_direct_perpat,
      pk_budgetsec,
      budgetsec_fkvisit,
      pk_lineitem,
      lineitem_inpersec,
      lineitem_fkevent,
      bgtcal_excldsocflag,
      BUDGET_COMBFLAG,
      coverage_type,
      SUBCOST_ITEM_FLAG,
      FK_EVENT,
      EVENT_TYPE
    FROM
      (SELECT pk_budget,
        bgtcal_prottype,
        bgtcal_protid,
        pk_bgtcal,
        budget_name,
        budget_version,
        budget_desc,
        bgtsection_name,
        bgtsection_type,
        NVL (bgtsection_patno, 1) bgtsection_patno,
        lineitem_name,
        DECODE ( TRIM(
        (SELECT codelst_subtyp
        FROM sch_codelst
        WHERE pk_codelst = fk_codelst_category
        ) ), 'ctgry_per', DECODE ( bgtsection_type, 'P', NVL ( bgtsection_patno, 1) * lineitem_othercost , 'O', lineitem_sponsorunit * lineitem_clinicnofunit ) * bgtcal_frgbenefit * NVL (bgtcal_frgflag, 0)/100, 0 ) AS total_line_fringe,
        DECODE ( TRIM(
        (SELECT codelst_subtyp
        FROM sch_codelst
        WHERE pk_codelst = fk_codelst_category
        ) ), 'ctgry_per', lineitem_othercost * bgtcal_frgbenefit * NVL (bgtcal_frgflag, 0)/100 , 0 ) AS per_patient_line_fringe,
        fk_codelst_category,
        DECODE ( (DECODE(bgtcal_prottype,'S',(select count(*) from SCH_EVENTCOST where fk_event in (select event_id from EVENT_ASSOC 
        Where chain_id = (select FK_PROTOCOL from sch_protocol_visit where PK_PROTOCOL_VISIT = NVL(sch_bgtsection.fk_visit,0))
        AND EVENT_ASSOC.fk_visit is null and NAME = sch_lineitem.LINEITEM_NAME)AND FK_COST_DESC in (SELECT pk_codelst  FROM sch_codelst
        WHERE CODELST_TYPE='cost_desc' and CODELST_SUBTYP = 'baseline')),0)), 0, 'Other','No') as unit_cost_flag,
        DECODE ( (DECODE(bgtcal_prottype,'S',(select count(*) from SCH_EVENTCOST where fk_event in (select event_id from EVENT_ASSOC 
        Where chain_id = (select FK_PROTOCOL from sch_protocol_visit where PK_PROTOCOL_VISIT = NVL(sch_bgtsection.fk_visit,0))
        AND EVENT_ASSOC.fk_visit is null and NAME = sch_lineitem.LINEITEM_NAME)AND FK_COST_DESC in (SELECT pk_codelst  FROM sch_codelst
        WHERE CODELST_TYPE='cost_desc' and CODELST_SUBTYP = 'baseline')),0)), 
        0 , to_number(to_char(trim(replace(lineitem_sponsorunit,',','')),'999999999999999.99')),
        0  ) as unit_cost_new,
        lineitem_sponsorunit AS lineitem_sponsorunit,
        lineitem_clinicnofunit,
        lineitem_stdcarecost AS lineitem_stdcarecost,
        lineitem_rescost     AS lineitem_rescost,
        lineitem_incostdisc  AS lineitem_incostdisc,
        lineitem_cptcode,
        (SELECT codelst_desc FROM sch_codelst WHERE pk_codelst = fk_codelst_category
        ) AS CATEGORY,
        ( SELECT codelst_seq FROM sch_codelst WHERE pk_codelst = fk_codelst_category
        ) AS category_seq,
        (SELECT TRIM (codelst_subtyp)
        FROM sch_codelst
        WHERE pk_codelst = fk_codelst_category
        ) AS category_subtyp,
        lineitem_tmid,
        lineitem_cdm,
        NVL (lineitem_applyindirects, 0) lineitem_applyindirects,
        NVL (bgtcal_sponsorohead, 0) bgtcal_sponsorohead,
        NVL (bgtcal_sponsorflag, 0) bgtcal_sponsorflag,
        NVL (bgtcal_frgbenefit, 0) bgtcal_frgbenefit,
        NVL (bgtcal_frgflag, 0) bgtcal_frgflag,
        NVL (bgtcal_discount, 0) bgtcal_discount,
        NVL (bgtcal_discountflag, 0) bgtcal_discountflag,
        ( SELECT study_number FROM er_study WHERE pk_study = fk_study
        ) AS study_number,
        (SELECT STUDYID_ID
        FROM ER_STUDYID
        WHERE FK_STUDY        = sch_budget.fk_study
        AND FK_CODELST_IDTYPE =
          (SELECT PK_CODELST
          FROM ER_CODELST
          WHERE CODELST_TYPE = 'studyidtype'
          AND CODELST_SUBTYP = 'irb_number'
          )
        ) AS irb_number,
        ( SELECT pk_study FROM er_study WHERE pk_study = fk_study
        ) AS fk_study,
        ( SELECT study_title FROM er_study WHERE pk_study = fk_study
        ) AS study_title,
        (SELECT usr_firstname
          || ' '
          || usr_lastname
        FROM er_user
        WHERE pk_user =
          (SELECT STUDY_PRINV FROM er_study WHERE pk_study = fk_study
          )
        ) AS STUDY_PRINV,
        (SELECT DECODE(fk_codelst_sponsor,NULL,''
          ||study_sponsor,
          (SELECT CODELST_DESC FROM ER_CODELST WHERE PK_CODELST = FK_CODELST_SPONSOR
          )
          ||DECODE(study_sponsor,NULL,'',','
          ||study_sponsor))
        FROM er_study
        WHERE pk_study = fk_study
        ) AS study_sponsor,
        (SELECT STUDYID_ID
        FROM ER_STUDYID
        WHERE FK_STUDY        = sch_budget.fk_study
        AND FK_CODELST_IDTYPE =
          (SELECT PK_CODELST
          FROM ER_CODELST
          WHERE CODELST_TYPE = 'studyidtype'
          AND CODELST_SUBTYP = 'id_sponsor'
          )
        ) AS sponsor_number,
        ( SELECT site_name FROM er_site WHERE pk_site = fk_site
        ) AS site_name,
        (SELECT NVL (codelst_subtyp, codelst_desc)
        FROM sch_codelst
        WHERE pk_codelst = budget_currency
        ) AS budget_currency,
        (SELECT usr_firstname
          || ' '
          || usr_lastname
        FROM er_user
        WHERE pk_user = NVL ( sch_budget.last_modified_by, sch_budget.creator )
        ) AS last_modified_by,
        TO_CHAR ( NVL (sch_budget.last_modified_date, sch_budget.created_on ), PKG_DATEUTIL.F_GET_DATEFORMAT
        || ' hh:mi:ss' ) AS last_modified_date,
        bgtsection_sequence,
        bgtcal_indirectcost AS bgtcal_indirectcost,
        lineitem_desc,
        DECODE(NVL(LINEITEM_APPLYPATIENTCOUNT,'0'),'0' , lineitem_sponsoramount , ( lineitem_sponsoramount * NVL( bgtsection_patno,'1'))) AS lineitem_sponsoramount,
        LINEITEM_APPLYPATIENTCOUNT,
        LINEITEM_OTHERCOST AS LINEITEM_OTHERCOST,
        FK_CODELST_COST_TYPE,
        (SELECT codelst_desc
        FROM sch_codelst
        WHERE pk_codelst = FK_CODELST_COST_TYPE
        ) COST_TYPE_DESC,
        lineitem_seq,
        NULL AS linitem_sponsor_perpat,
        pk_budgetsec,
        fk_visit budgetsec_fkvisit,
        pk_lineitem,
        NVL (lineitem_inpersec, 0) lineitem_inpersec,
        sch_lineitem.fk_event lineitem_fkevent,
        bgtcal_excldsocflag,
        BUDGET_COMBFLAG,
        (SELECT EVENT_TYPE
        FROM EVENT_ASSOC
        WHERE event_id= sch_lineitem.fk_event
        ) AS event_type,
        DECODE( bgtcal_prottype, 'L',
        (SELECT codelst_desc
        FROM sch_codelst
        WHERE pk_codelst =
          (SELECT FK_CODELST_COVERTYPE
          FROM EVENT_DEF
          WHERE event_id= sch_lineitem.fk_event
          )
        ) , 'S',
        (SELECT codelst_desc
        FROM sch_codelst
        WHERE pk_codelst =
          (SELECT FK_CODELST_COVERTYPE
          FROM EVENT_ASSOC
          WHERE event_id= sch_lineitem.fk_event
          )
        ) , NULL, 'No Coverage Type' ) AS coverage_type,
        NVL(SUBCOST_ITEM_FLAG,0)       AS SUBCOST_ITEM_FLAG,
        FK_EVENT
      FROM sch_bgtcal,
        sch_budget,
        sch_bgtsection,
        sch_lineitem
      WHERE pk_budget                   = fk_budget
      AND pk_bgtcal                     = fk_bgtcal
      AND NVL (bgtsection_delflag, 'N') = 'N'
      AND pk_budgetsec                  = fk_bgtsection(+)
      AND NVL (lineitem_delflag, 'N')   = 'N'
      UNION ALL
      select pk_budget, NULL, NULL, pk_bgtcal,  budget_name, budget_version,
        budget_desc , BGTSECTION_NAME , bgtsection_type, bgtsection_patno,
      LINEITEM_NAME, NULL ,
      DECODE ( TRIM(
        (SELECT codelst_subtyp
        FROM sch_codelst
        WHERE pk_codelst = fk_codelst_category
        ) ), 'ctgry_per', (unit_cost_new * LINEITEM_CLINICNOFUNIT) * bgtcal_frgbenefit * NVL (bgtcal_frgflag, 0)/100 , 0 ) AS per_patient_line_fringe,
        fk_codelst_category, unit_cost_flag, unit_cost_new,
      NULL, LINEITEM_CLINICNOFUNIT ,NULL ,NULL ,lineitem_incostdisc ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,
      lineitem_applyindirects, bgtcal_sponsorohead, bgtcal_sponsorflag, bgtcal_frgbenefit, bgtcal_frgflag, bgtcal_discount,
      bgtcal_discountflag, NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL,
      NULL, NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,EVENT_TYPE ,NULL ,NULL ,NULL
      from (select pk_budget, pk_bgtcal,  budget_name, budget_version,
        budget_desc , BGTSECTION_NAME , bgtsection_type, bgtsection_patno, LINEITEM_NAME, LINEITEM_CLINICNOFUNIT,
      DECODE ( (select count(*) from SCH_EVENTCOST where fk_event = event_assoc.event_id 
        AND FK_COST_DESC in (SELECT pk_codelst  FROM sch_codelst
        WHERE CODELST_TYPE='cost_desc' and CODELST_SUBTYP = 'baseline')), 0, 'No','Yes') as unit_cost_flag,
      DECODE ( (select count(*) from SCH_EVENTCOST where fk_event = event_assoc.event_id
        AND FK_COST_DESC in (SELECT pk_codelst  FROM sch_codelst
        WHERE CODELST_TYPE='cost_desc' and CODELST_SUBTYP = 'baseline')), 
        0 , 0, NVL((select EVENTCOST_VALUE from sch_eventcost where PK_EVENTCOST =
      (select min(PK_EVENTCOST) from sch_eventcost where FK_EVENT = event_assoc.event_id 
      and FK_COST_DESC in (SELECT pk_codelst  FROM sch_codelst
      WHERE CODELST_TYPE='cost_desc' and CODELST_SUBTYP = 'baseline'))), 0)  ) as unit_cost_new,
      lineitem_applyindirects, bgtcal_sponsorohead, bgtcal_sponsorflag, bgtcal_frgbenefit, bgtcal_frgflag, bgtcal_discount,
      bgtcal_discountflag, lineitem_incostdisc, FK_CODELST_CATEGORY,sch.EVENT_TYPE,
     NVL(event_id, 0) as event_id,
     NVL(fk_event,0) as fk_event,
     fk_visit
      from event_assoc, (SELECT distinct LINEITEM_NAME, pk_budget, pk_bgtcal,  budget_name, budget_version,
        budget_desc ,
      BGTSECTION_NAME,
     LINEITEM_CLINICNOFUNIT,
    NVL (BGTSECTION_PATNO, 1) AS BGTSECTION_PATNO,
    bgtsection_type,
    NVL (lineitem_applyindirects, 0) lineitem_applyindirects,
    NVL (bgtcal_sponsorohead, 0) bgtcal_sponsorohead,
    NVL (bgtcal_sponsorflag, 0) bgtcal_sponsorflag,
    NVL (bgtcal_frgbenefit, 0) bgtcal_frgbenefit,
    NVL (bgtcal_frgflag, 0) bgtcal_frgflag,
    NVL (bgtcal_discount, 0) bgtcal_discount,
    NVL (bgtcal_discountflag, 0) bgtcal_discountflag, 
    lineitem_incostdisc  AS lineitem_incostdisc,
    FK_CODELST_CATEGORY,
   NVL(FK_EVENT, 0) as FK_EVENT,(SELECT EVENT_TYPE
        FROM EVENT_ASSOC
        WHERE event_id= sch_lineitem.fk_event
        ) AS event_type,
   FK_PROTOCOL
   FROM sch_bgtsection s, sch_lineitem, sch_bgtcal, sch_budget, sch_protocol_visit
   WHERE pk_budget                   = fk_budget
   AND s.FK_BGTCAL = PK_BGTCAL
   AND PK_PROTOCOL_VISIT = fk_visit
   AND FK_BGTSECTION = PK_BUDGETSEC
   AND NVL (bgtsection_delflag, 'N') = 'N'
   AND NVL (lineitem_delflag, 'N')   = 'N'
   and fk_event is not null) sch 
   where chain_id = sch.FK_PROTOCOL
   and sch.fk_event <> event_id
   and NAME = sch.LINEITEM_NAME
   and fk_visit is null)
   WHERE unit_cost_flag = 'Yes'  )
    WHERE NVL(EVENT_TYPE,'A') <> 'U'
    );
/

CREATE OR REPLACE SYNONYM ERES.ERV_BUDGET_COMP FOR ERV_BUDGET_COMP;

CREATE OR REPLACE SYNONYM EPAT.ERV_BUDGET_COMP FOR ERV_BUDGET_COMP;