*****************************************************
This Patch is for Comparative Budget Variance Report
*****************************************************
There is a critical issue bug#20031 reported in "Comparative Budget Variance Report"

We have fixed this issue and  given below are the details of the patch.

1. Bug  20031:  356 Comparative Variance Budget Report total cost per patient calculation error

This patch consists of following files:


1. DB Script files:
    eres:
        a) er_report_20000_update.sql
        b) er_repxsl_20000_delete.sql
    esch:
        c) ERV_BUDGET_COMP.sql  
     xsl:
        d) 20000.xsl
        e) GENREPXSL.ctl
        f) loadxsl.bat  


Steps to deploy the Patch:
- Stop the eResearch application.
- Execute the DB patch in the 'db_patch' folder on the database.
- - Replace the Java files on the application server with the files in this 'server' folder.
- Clear the tmp and work folders under JBoss as usual and start the eResearch application.