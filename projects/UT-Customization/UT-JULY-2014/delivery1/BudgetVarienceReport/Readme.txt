***************************************
This Patch is for Comparative Budget Variance Report
***************************************

There is a custom report requirement  to generate "Comparative Budget Variance Report". Please  refer "CR Comparative Budget Variance Report.doc"  for details.

We have implemented this report and  given below are the details of the patch.


This patch consists of following files:

1. Java Files: 
     server/eresearch/deploy/velos.ear/velos-common.jar/com/velos/eres/service/util/LC.class
2. ereshome
     1. labelBundle.properties      

3. DB Script files:
    eres:
        a)er_report_20000.sql
    esch:
        b) ERV_BUDGET_COMP.sql       
     xsl:
        c)20000.xsl
        d)GENREPXSL.ctl
        e)loadxsl.bat   


Steps to deploy the Patch:
- Stop the eResearch application.
- Execute the DB patch in the 'db_patch' folder on the database.
- - Replace the Java files on the application server with the files in this 'server' folder.
- Clear the tmp and work folders under JBoss as usual and start the eResearch application.
