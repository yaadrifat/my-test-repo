create or replace
TRIGGER ERES.ER_STUDYSTAT_AI0_UT
BEFORE INSERT
ON ERES.ER_STUDYSTAT REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
declare 
      v_checkenrollment number(10) ;
      v_msgtemplate VARCHAR2(4000);
      v_mail VARCHAR2(4000);
      v_stdnum VARCHAR2(40);
      v_stdtitle VARCHAR2(1000);
      v_irb VARCHAR2(100);
      v_author VARCHAR2(40);
      v_pi VARCHAR2(40) := null;
      v_pi_name VARCHAR2(40) := null;
      v_fparam VARCHAR2(4000);
      v_msgsubject VARCHAR2(100);
        PRAGMA AUTONOMOUS_TRANSACTION;
       pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'STUDYSTAT', pLEVEL  => Plog.LDEBUG);

BEGIN

select count(*) into v_checkenrollment
    from er_codelst
    where :new.fk_codelst_studystat = pk_codelst
    and codelst_type='studystat'
    and codelst_subtyp='active';


if (v_checkenrollment = 1) then
       v_msgtemplate   :=    PKG_COMMON_UT.SCH_GETMAILMSG('stdstat');
       v_msgsubject := PKG_COMMON_UT.SCH_GETMSGSUBJECT('stdstat');
      begin
      	select NVL(STUDYID_ID, '-') into v_irb from ER_STUDYID where FK_STUDY = (:new.fk_study) and FK_CODELST_IDTYPE = (select pk_codelst from er_codelst where codelst_type = 'studyidtype' and CODELST_SUBTYP = 'irb_number');
      EXCEPTION WHEN NO_DATA_FOUND THEN
        v_irb := '-';
      end; 
       begin
     select study_number, nvl(study_title, '-'), study_prinv, fk_author into v_stdnum, v_stdtitle, v_pi, v_author from er_study where pk_study = :new.fk_study;
       EXCEPTION WHEN NO_DATA_FOUND THEN
        if (v_pi is null) then
			v_pi := '';
		end if;
		
        if (v_author is null) then
			v_author := '';
		end if;
     end; 
     if(v_pi is not null) then
      SELECT  usr_lastname ||' '|| usr_firstname
        INTO v_pi_name FROM er_user
      WHERE pk_user = v_pi ;
     end if;
     v_fparam :=   v_pi_name||'~'|| v_irb ||'~'|| v_stdnum ||'~'|| v_stdtitle ;
     v_mail :=    PKG_COMMON_UT.SCH_GETMAIL(v_msgtemplate,v_fparam);

	if(v_author is not null and v_pi is not null and v_author = v_pi) then
	    INSERT INTO SCH_DISPATCHMSG (PK_MSG ,MSG_SENDON ,MSG_STATUS  ,
                MSG_TYPE    ,
                MSG_TEXT,FK_PAT,MSG_SUBJECT) values(SCH_DISPATCHMSG_SEQ1.NEXTVAL,SYSDATE ,0,'U',
                v_mail,NVL(v_pi,''),NVL(v_msgsubject,''));
        commit;
	else
		begin
			if (v_pi is not null) then
		    	INSERT INTO SCH_DISPATCHMSG (PK_MSG ,MSG_SENDON ,MSG_STATUS  ,
		                        MSG_TYPE    ,
		                        MSG_TEXT,FK_PAT,MSG_SUBJECT) values(SCH_DISPATCHMSG_SEQ1.NEXTVAL,SYSDATE ,0,'U',
		                        v_mail,NVL(v_pi,''),NVL(v_msgsubject,''));
			end if;
			if (v_author is not null) then
		    	INSERT INTO SCH_DISPATCHMSG (PK_MSG ,MSG_SENDON ,MSG_STATUS  ,
		                        MSG_TYPE    ,
		                        MSG_TEXT,FK_PAT,MSG_SUBJECT) values(SCH_DISPATCHMSG_SEQ1.NEXTVAL,SYSDATE ,0,'U',
		                        v_mail,NVL(v_author,''),NVL(v_msgsubject,''));
			end if;
			
			commit;
		end;
	end if;
end if;
END;
/


