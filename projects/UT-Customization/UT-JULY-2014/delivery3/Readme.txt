*************************************************
This is a bug fix patch for Change Request - 316	
*************************************************
This patch contains fix for bugs #19952, and #19953.

This patch consists of following files:

1. DB Script files:
    eres:
        1)01_ER_STUDYSTAT_AI0_UT.sql


Steps to deploy the Patch:
1] Stop the eResearch application.
2] Execute the DB scripts in the 'db_patch' folder on the database in the following order.
- Clear the tmp and work folders under JBoss as usual and start the eResearch application.
