*************************************************
This Patch is for Velos Reports - Change Request - 315 ,316 & 319	
*************************************************
IMPORTANT POINTS:
1. This is a consolidated patch for the UT change request 315,316&319. O n 29th July 2014, we had released  an initial  patch  for UT. Since this is not deployed at QA environment yet, we are sending a consolidated patch for the change request 315,316&319.Please treat this as latest.
2. The track patch entries are included in each script with an assumption that UT  is on v9.2build#693.06
3. Before execution of patch no: 09_ER_CODELST-INSERT-319.sql, Please make sure to validate this patch against the requirement document(HSC SA Study Statuses - 319.pdf),since it involves code list maintenance entries.
4. Please also note that there are some dependent code list entries in 09_ER_CODELST-INSERT-319.sql  for correct functioning of another requirement:CR for Study Active Notification - 316.


 We have received three UT specific requirements   on eResearch v.9.2.
1. To generate custom reports  in eResearch application. Please  refer "Velos Reports - Change Request - 315.pdf"  for details.
2. To include Status Types and Study Statuses in eResearch application. Please refer "HSC SA Study Statuses - 319.pdf" for details.
3.  CR for Study Active Notification - 316  Please refer "CR for Study Active Notification - 316.doc"

We have implemented these changes  and  given below are the details of the patch.

This patch consists of following files:

1. Java Files: 
     server/eresearch/deploy/velos.ear/velos-common.jar/com/velos/eres/service/util/LC.class

2. ereshome
     1. labelBundle.properties      

3. DB Script files:
	
   esch :                1) 01_SCH_MSGTXT_INSERT.sql
		2) 02_pkg_common_spec_ut.sql
		3)03_pkg_common_body_ut.sql
    eres:
        		1)01_er_report_20001.sql     
		2)02_er_report_20002.sql
		3)03_er_report_20003.sql
		4)04_er_report_20004.sql
		5)05_er_report_20005.sql
		6)06_PKG_REPORT_SPEC_UT.sql
		7)07_PKG_REPORT_BODY_UT.sql
		8)08_ER_STUDYSTAT_AI0_UT.sql
		9)09_ER_CODELST-INSERT-319.sql
     	
     xsl:
        		1)20001.xsl
		2)20002.xsl
		3)20003.xsl
		4)20004.xsl
		5)20005.xsl
		6GENREPXSL.ctl
		7)loadxsl.bat   


Steps to deploy the Patch:

- Stop the eResearch application.

-- Execute the DB scripts in the 'db_patch' folder on the database in the following order.
 1. esch  2. eres 3.xsl

- Replace the Java files on the application server with the files in this 'server' folder.
- Clear the tmp and work folders under JBoss as usual and start the eResearch application.


Assumptions for Change Request - 315.pdf:
1. For All Account Users report, we have used codelst_type = 'user' and CODELST_SUBTYP = 'cellno'.
2. For All Protocols report, We have introduced a new column "Enrollment Status". It is having same data as Study Status column, which is already there. Thereby having duplicate columns with same data and different column name.
3. For Adverse Events by Patient, we have used codelst_type = 'advtype' and CODELST_SUBTYP = 'modifications'.
4. These reports will be shown under Report Central with names suffixed by " - UT". e.g, All Account Users - UT

Assumptions for Change Request - 316.pdf:
5 For Study Status "Study Enrollment-Open To Enrollment", we have used codelst_type = 'studystat' and codelst_subtyp = 'open_to_enrollment'.
6. For display of Principal Investigatorr Name, we followed the "Last Name First Name" standard.
7. We were facing some technical issue in the Mail Body while using this " - "(with Spaces on both side) character. So, we have used ":" in place of "-" right after "CTRC CTO and VPR CTO".
8. The Subject of the Mail is "Study Activation Notification".
***********************************************************************************************************************************************************************