SET define OFF;
-- INSERTING into ER_REPORT
--- All Protocols
DECLARE
  v_record_exists NUMBER := 0;
BEGIN
  SELECT COUNT(*) INTO v_record_exists FROM er_report WHERE pk_report = 20002;
  IF (v_record_exists = 0) THEN
    INSERT
    INTO ER_REPORT
      (
        PK_REPORT,
        REP_NAME,
        REP_DESC,
        REP_SQL,
        FK_ACCOUNT,
        FK_CODELST_TYPE,
        IP_ADD,
        REP_HIDE,
        REP_COLUMNS,
        REP_FILTERBY,
        GENERATE_XML,
        REP_TYPE,
        REP_SQL_CLOB,
        REP_FILTERKEYWORD,
        REP_FILTERAPPLICABLE
      )
      VALUES
      (
        20002,
        'All Protocols - UT',
        'All Protocols - UT',
        '',
        0,
        NULL,
        NULL,
        'N',
        NULL,
        NULL,
        1,
        'rep_gar',
        '',
        'userId:orgId',
        'orgId:date:tAreaId:studyId:studyDivId:userId'
      );
    COMMIT;

update er_report set REP_SQL = 'select  distinct c.USR_LASTNAME,
  c.USR_FIRSTNAME, ER_STUDY.pk_study,
  ER_STUDY.STUDY_TITLE, TO_CHAR(study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT) study_actualdt ,
  ER_STUDY.STUDY_NUMBER,   a.CODELST_DESC tarea,
  b.CODELST_DESC team_role, f.codelst_desc study_stat, (SELECT codelst_desc 
  FROM ER_CODELST
  WHERE pk_codelst = (SELECT FK_CODELST_STUDYSTAT
  FROM er_studystat where PK_STUDYSTAT = (select max(PK_STUDYSTAT) from er_studystat 
  where FK_STUDY = ER_STUDY.pk_study
  and  STATUS_TYPE  =(select pk_codelst from er_codelst where CODELST_TYPE = ''studystat_type'' 
  and CODELST_SUBTYP = ''studystat_ty_11''))
  )) as Enrollment_Status,
 d.site_name, (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = study_division) AS division,
 (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = ER_STUDY.fk_codelst_phase) AS study_Phase,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = ER_STUDY.fk_codelst_restype) AS Research_Type,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = ER_STUDY.fk_codelst_type) AS Study_Type,
(select STUDYID_ID from ER_STUDYID where PK_STUDY = FK_STUDY and FK_CODELST_IDTYPE = (select pk_codelst from er_codelst where codelst_type = ''studyidtype'' and CODELST_SUBTYP = ''irb_number'')) AS IRB_Number
 FROM  ER_STUDYTEAM,   ER_USER c,
  ER_STUDY,   ER_CODELST a,
  ER_CODELST b, ER_CODELST f,
  ER_SITE d ,ER_STUDYSTAT e
 WHERE  c.fk_siteid IN (:orgId)
 AND  TRUNC(ER_STUDY.created_on) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
 AND  er_studyteam.fk_user in (:userId) and ER_STUDYTEAM.FK_USER = c.pk_user
 AND c.fk_siteid = d.pk_site
 AND  ER_STUDY.pk_study = ER_STUDYTEAM.FK_STUDY
 AND  ER_STUDY.FK_CODELST_TAREA = a.pk_codelst
 AND  ER_STUDYTEAM.FK_CODELST_TMROLE = b.pk_codelst
AND e.fk_study = ER_STUDY.pk_study
AND e.FK_CODELST_STUDYSTAT = f.pk_codelst
AND f.codelst_type = ''studystat'' AND e.STUDYSTAT_ENDT IS NULL
 AND study_verparent IS NULL
 AND fk_codelst_tarea IN (:tAreaId)
 AND pk_study IN (:studyId)
 AND (study_division IN (:studyDivId) OR study_division IS NULL)
     AND EXISTS  (SELECT fk_study FROM ER_STUDYSITES WHERE fk_study=pk_study AND fk_site IN (:orgId) )
ORDER BY tarea, pk_study'
where pk_report = 20002;

update er_report set REP_SQL_CLOB = 'select  distinct c.USR_LASTNAME,
  c.USR_FIRSTNAME, ER_STUDY.pk_study,
  ER_STUDY.STUDY_TITLE, TO_CHAR(study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT) study_actualdt ,
  ER_STUDY.STUDY_NUMBER,   a.CODELST_DESC tarea,
  b.CODELST_DESC team_role, f.codelst_desc study_stat, (SELECT codelst_desc 
  FROM ER_CODELST
  WHERE pk_codelst = (SELECT FK_CODELST_STUDYSTAT
  FROM er_studystat where PK_STUDYSTAT = (select max(PK_STUDYSTAT) from er_studystat 
  where FK_STUDY = ER_STUDY.pk_study
  and  STATUS_TYPE  =(select pk_codelst from er_codelst where CODELST_TYPE = ''studystat_type'' 
  and CODELST_SUBTYP = ''studystat_ty_11''))
  )) as Enrollment_Status,
 d.site_name, (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = study_division) AS division,
 (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = ER_STUDY.fk_codelst_phase) AS study_Phase,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = ER_STUDY.fk_codelst_restype) AS Research_Type,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = ER_STUDY.fk_codelst_type) AS Study_Type,
(select STUDYID_ID from ER_STUDYID where PK_STUDY = FK_STUDY and FK_CODELST_IDTYPE = (select pk_codelst from er_codelst where codelst_type = ''studyidtype'' and CODELST_SUBTYP = ''irb_number'')) AS IRB_Number
 FROM  ER_STUDYTEAM,   ER_USER c,
  ER_STUDY,   ER_CODELST a,
  ER_CODELST b, ER_CODELST f,
  ER_SITE d ,ER_STUDYSTAT e
 WHERE  c.fk_siteid IN (:orgId)
 AND  TRUNC(ER_STUDY.created_on) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
 AND  er_studyteam.fk_user in (:userId) and ER_STUDYTEAM.FK_USER = c.pk_user
 AND c.fk_siteid = d.pk_site
 AND  ER_STUDY.pk_study = ER_STUDYTEAM.FK_STUDY
 AND  ER_STUDY.FK_CODELST_TAREA = a.pk_codelst
 AND  ER_STUDYTEAM.FK_CODELST_TMROLE = b.pk_codelst
AND e.fk_study = ER_STUDY.pk_study
AND e.FK_CODELST_STUDYSTAT = f.pk_codelst
AND f.codelst_type = ''studystat'' AND e.STUDYSTAT_ENDT IS NULL
 AND study_verparent IS NULL
 AND fk_codelst_tarea IN (:tAreaId)
 AND pk_study IN (:studyId)
 AND (study_division IN (:studyDivId) OR study_division IS NULL)
     AND EXISTS  (SELECT fk_study FROM ER_STUDYSITES WHERE fk_study=pk_study AND fk_site IN (:orgId) )
ORDER BY tarea, pk_study'
where pk_report = 20002;
COMMIT;
  END IF;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,292,921315,'02_er_report_20002.sql',sysdate,'v9.2.0 #693.06');

commit;

