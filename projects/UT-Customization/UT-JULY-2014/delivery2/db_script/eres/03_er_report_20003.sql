SET define OFF;
-- INSERTING into ER_REPORT
--- Study Patient List
DECLARE
  v_record_exists NUMBER := 0;
BEGIN
  SELECT COUNT(*) INTO v_record_exists FROM er_report WHERE pk_report = 20003;
  IF (v_record_exists = 0) THEN
    INSERT
    INTO ER_REPORT
      (
        PK_REPORT,
        REP_NAME,
        REP_DESC,
        REP_SQL,
        FK_ACCOUNT,
        FK_CODELST_TYPE,
        IP_ADD,
        REP_HIDE,
        REP_COLUMNS,
        REP_FILTERBY,
        GENERATE_XML,
        REP_TYPE,
        REP_SQL_CLOB,
        REP_FILTERKEYWORD,
        REP_FILTERAPPLICABLE
      )
      VALUES
      (
        20003,
        'Study Patient List - UT',
        'Study Patient List - UT',
        '',
        0,
        NULL,
        NULL,
        'N',
        NULL,
        NULL,
        1,
        'rep_pat_onestd',
        '',
        'date:studyId:patStatusId:orgId',
        'studyId:orgId:date:patStatusId'
      );
    COMMIT;

update er_report set REP_SQL = 'select er_study.study_number,
er_site.site_name,
to_char(er_study.study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT)
study_actualdt,
er_study.study_title,
PER_CODE patient_id,
patprot_patstdid,
to_char(er_patprot.PATPROT_ENROLDT,PKG_DATEUTIL.F_GET_DATEFORMAT) pe,
(select name         from ESCH.event_assoc        where
EVENT_ID = er_patprot.fk_protocol) protocol,
to_char(er_patprot.patprot_start,PKG_DATEUTIL.F_GET_DATEFORMAT) ps,
(select STUDYID_ID from ER_STUDYID where PK_STUDY = FK_STUDY and FK_CODELST_IDTYPE =
(select pk_codelst from er_codelst where codelst_type = ''studyidtype'' and CODELST_SUBTYP = ''irb_number'')) AS IRB_Number,
(SELECT site_name FROM er_site WHERE pk_site = er_patprot.fk_site_enrolling) AS Enrolling_site,
(SELECT site_name FROM er_site WHERE pk_site = er_patprot.patprot_treatingorg) AS Treating_Site,
( select codelst_desc
from er_codelst
where pk_codelst = fk_codelst_stat ) patient_status
from er_patprot, er_study,  er_per  , er_site,ER_PATSTUDYSTAT stat
where   er_patprot.PATPROT_STAT = 1
and     er_patprot.fk_study in (:studyId)
and     er_per.PK_PER = er_patprot.fk_per
and     er_per.fk_site in (:orgId)
and     er_per.fk_site = er_site.pk_site
and     er_study.pk_study = er_patprot.fk_study
AND ER_PATPROT.fk_per=stat.fk_per AND ER_PATPROT.fk_study=stat.fk_study
AND  PATSTUDYSTAT_ENDT IS NULL
and     er_patprot.PATPROT_ENROLDT >= TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
and     er_patprot.PATPROT_ENROLDT <= TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
and fk_codelst_stat in (:patStatusId)'
where pk_report = 20003;

update er_report set REP_SQL_CLOB = 'select er_study.study_number,
er_site.site_name,
to_char(er_study.study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT)
study_actualdt,
er_study.study_title,
PER_CODE patient_id,
patprot_patstdid,
to_char(er_patprot.PATPROT_ENROLDT,PKG_DATEUTIL.F_GET_DATEFORMAT) pe,
(select name         from ESCH.event_assoc        where
EVENT_ID = er_patprot.fk_protocol) protocol,
to_char(er_patprot.patprot_start,PKG_DATEUTIL.F_GET_DATEFORMAT) ps,
(select STUDYID_ID from ER_STUDYID where PK_STUDY = FK_STUDY and FK_CODELST_IDTYPE =
(select pk_codelst from er_codelst where codelst_type = ''studyidtype'' and CODELST_SUBTYP = ''irb_number'')) AS IRB_Number,
(SELECT site_name FROM er_site WHERE pk_site = er_patprot.fk_site_enrolling) AS Enrolling_site,
(SELECT site_name FROM er_site WHERE pk_site = er_patprot.patprot_treatingorg) AS Treating_Site,
( select codelst_desc
from er_codelst
where pk_codelst = fk_codelst_stat ) patient_status
from er_patprot, er_study,  er_per  , er_site,ER_PATSTUDYSTAT stat
where   er_patprot.PATPROT_STAT = 1
and     er_patprot.fk_study in (:studyId)
and     er_per.PK_PER = er_patprot.fk_per
and     er_per.fk_site in (:orgId)
and     er_per.fk_site = er_site.pk_site
and     er_study.pk_study = er_patprot.fk_study
AND ER_PATPROT.fk_per=stat.fk_per AND ER_PATPROT.fk_study=stat.fk_study
AND  PATSTUDYSTAT_ENDT IS NULL
and     er_patprot.PATPROT_ENROLDT >= TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
and     er_patprot.PATPROT_ENROLDT <= TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
and fk_codelst_stat in (:patStatusId)'
where pk_report = 20003;
COMMIT;
  END IF;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,292,921315,'03_er_report_20003.sql',sysdate,'v9.2.0 #693.06');

commit;

