SET define OFF;
-- INSERTING into ER_REPORT
--- All Account Users
DECLARE
  v_record_exists NUMBER := 0;
BEGIN
  SELECT COUNT(*) INTO v_record_exists FROM er_report WHERE pk_report = 20001;
  IF (v_record_exists = 0) THEN
    INSERT
    INTO ER_REPORT
      (
        PK_REPORT,
        REP_NAME,
        REP_DESC,
        REP_SQL,
        FK_ACCOUNT,
        FK_CODELST_TYPE,
        IP_ADD,
        REP_HIDE,
        REP_COLUMNS,
        REP_FILTERBY,
        GENERATE_XML,
        REP_TYPE,
        REP_SQL_CLOB,
        REP_FILTERKEYWORD,
        REP_FILTERAPPLICABLE
      )
      VALUES
      (
        20001,
        'All Account Users - UT',
        'All Account Users - UT',
        '',
        0,
        NULL,
        NULL,
        'N',
        NULL,
        NULL,
        1,
        'rep_gar',
        '',
        NULL,
        'date'
      );
    COMMIT;

update er_report set REP_SQL = 'select  USR_LASTNAME, USR_FIRSTNAME,(select MD_MODELEMENTDATA from ER_MOREDETAILS where PK_USER = FK_MODPK and MD_MODELEMENTPK = (select pk_codelst from er_codelst where codelst_type = ''user'' and CODELST_SUBTYP = ''user_1'')) add_cellPhone, add_phone, add_email,
f_get_usersites(pk_user) AS ORGANIZATION,
 ( SELECT codelst_desc  FROM  ER_CODELST WHERE pk_codelst = FK_CODELST_JOBTYPE) AS jobType,
 ( SELECT codelst_desc  FROM  ER_CODELST WHERE pk_codelst = fk_codelst_spl) AS primary_spl,
 DECODE(usr_stat, ''A'',''Active'', ''D'',''Deactivated'', ''B'', ''Blocked'') AS usr_stat,
f_get_usergrp(pk_user) AS user_groups
 FROM  ER_USER, ER_ADD
 WHERE  usr_stat IN (''A'', ''D'', ''B'')
  AND fk_account =:sessAccId
 AND  fk_peradd = pk_add AND usr_type = ''S'' ORDER
 BY USR_FIRSTNAME,USR_LASTNAME'
where pk_report = 20001;

update er_report set REP_SQL_CLOB = 'select  USR_LASTNAME, USR_FIRSTNAME,(select MD_MODELEMENTDATA from ER_MOREDETAILS where PK_USER = FK_MODPK and MD_MODELEMENTPK = (select pk_codelst from er_codelst where codelst_type = ''user'' and CODELST_SUBTYP = ''user_1'')) add_cellPhone, add_phone, add_email,
f_get_usersites(pk_user) AS ORGANIZATION,
 ( SELECT codelst_desc  FROM  ER_CODELST WHERE pk_codelst = FK_CODELST_JOBTYPE) AS jobType,
 ( SELECT codelst_desc  FROM  ER_CODELST WHERE pk_codelst = fk_codelst_spl) AS primary_spl,
 DECODE(usr_stat, ''A'',''Active'', ''D'',''Deactivated'', ''B'', ''Blocked'') AS usr_stat,
f_get_usergrp(pk_user) AS user_groups
 FROM  ER_USER, ER_ADD
 WHERE  usr_stat IN (''A'', ''D'', ''B'')
  AND fk_account =:sessAccId
 AND  fk_peradd = pk_add AND usr_type = ''S'' ORDER
 BY USR_FIRSTNAME,USR_LASTNAME '
where pk_report = 20001;
COMMIT;
  END IF;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,292,921315,'01_er_report_20001.sql',sysdate,'v9.2.0 #693.06');

commit;

