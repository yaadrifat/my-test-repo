ALTER TABLE ERES.ER_CODELST MODIFY(CODELST_SUBTYP VARCHAR2(40 BYTE));
/
--STARTS INSERTING RECORD INTO ER_CODELST TABLE(FOR STATUSTYPE)--
set define off;
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat_type' and CODELST_SUBTYP = 'irb';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat_type', 'irb', 'IRB', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat_type'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, NULL, NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat_type' and CODELST_SUBTYP = 'study_enrollment';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat_type', 'study_enrollment', 'Study Enrollment', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat_type'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, NULL, NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat_type' and CODELST_SUBTYP = 'ctrp';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat_type', 'ctrp', 'CTRP', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat_type'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, NULL, NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat_type' and CODELST_SUBTYP = 'funding';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat_type', 'funding', 'Funding', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat_type'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, NULL, NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat_type' and CODELST_SUBTYP = 'operations';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat_type', 'operations', 'Operations', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat_type'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, NULL, NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat_type' and CODELST_SUBTYP = 'ctrc_prc';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat_type', 'ctrc_prc', 'CTRC-PRC', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat_type'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, NULL, NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat_type' and CODELST_SUBTYP = 'hsc_institutional';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat_type', 'hsc_institutional', 'HSC-Institutional', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat_type'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, NULL, NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
--STARTS INSERTING RECORD INTO ER_CODELST TABLE(FOR STUDY STATUS)--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'irb_new';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'irb_new', 'IRB NEW', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'irb', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'irb_withdrawn';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'irb_withdrawn', 'IRB Withdrawn', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'irb', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'irb_rejected';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'irb_rejected', 'IRB Rejected', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'irb', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'irb_active';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'irb_active', 'IRB Active', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'irb', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'irb_lapsed';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'irb_lapsed', 'IRB Lapsed', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'irb', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'irb_suspended';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'irb_suspended', 'IRB Suspended', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'irb', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'irb_inactivated';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'irb_inactivated', 'IRB Inactivated', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'irb', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'irb_terminated';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'irb_terminated', 'IRB Terminated', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'irb', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'not_recruiting';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'not_recruiting', 'Study Enrollment Not Recruiting', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'study_enrollment', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;

/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'open_to_enrollment';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'open_to_enrollment', 'Study Enrollment Open to Enrollment', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'study_enrollment', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'enrollment_temp_closed';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'enrollment_temp_closed', 'Study Enrollment Enrollment Temp Closed', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'study_enrollment', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'enrollment_closed';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'enrollment_closed', 'Study Enrollment Enrollment Closed', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'study_enrollment', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'ctrp_inreview';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'ctrp_inreview', 'CTRP-In Review', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'ctrp', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'ctrp_withdrawn';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'ctrp_withdrawn', 'CTRP-Withdrawn', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'ctrp', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'ctrp_approved';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'ctrp_approved', 'CTRP-Approved','N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'ctrp', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;  
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'ctrp_active';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'ctrp_active', 'CTRP-Active', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'ctrp', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'ctrp_enrolling_by_invitation';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'ctrp_enrolling_by_invitation', 'CTRP-Enrolling by Invitation', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'ctrp', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'ctrp_temp_cls_accru';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'ctrp_temp_cls_accru', 'CTRP-Temporarily Closed to Accrual', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'ctrp', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'ctrp_temp_cls_accru_int';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'ctrp_temp_cls_accru_int', 'CTRP-Temporarily Closed to Accrual & Intervention', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'ctrp', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'ctrp_closed_accru';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'ctrp_closed_accru', 'CTRP-Closed to Accrual', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'ctrp', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'ctrp_closed_accru_int';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'ctrp_closed_accru_int', 'CTRP-Closed to Accrual & Intervention', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'ctrp', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'ctrp_complete';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'ctrp_complete', 'CTRP-Completed', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'ctrp', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'ctrp_admn_corp';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'ctrp_admn_corp', 'CTRP-Administratively Complete', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'ctrp', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'fund_inreview';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'fund_inreview', 'Fund-In Review', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'funding', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'fund_withdrawn';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'fund_withdrawn', 'Fund Withdrawn', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'funding', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'fund_rejected';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'fund_rejected', 'Fund Rejected', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'funding', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'fund_exec/rewarded';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'fund_exec/rewarded', 'Fund Executed/Awarded', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'funding', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'fund_modification';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'fund_modification', 'Fund Modification', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'funding', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'fund_inrecon';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'fund_inrecon', 'Fund-In Reconciliation', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'funding', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'fund_suspended';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'fund_suspended', 'Fund Suspended', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'funding', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'fund_terminated';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'fund_terminated', 'Fund Terminated', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'funding', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'fund_closed';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'fund_closed', 'Fund Closed', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'funding', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'ops_inreview';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'ops_inreview', 'Ops-In-Review', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'operations', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'ops_appealed';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'ops_appealed', 'Ops Appealed', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'operations', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'ops_withdrwan_by_pi';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'ops_withdrwan_by_pi', 'Ops Withdrawn by PI', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'operations', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'ops_withdrwan_by_sponsor';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'ops_withdrwan_by_sponsor', 'Ops Withdrawn by Sponsor', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'operations', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'ops_rejected';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'ops_rejected', 'Ops Rejected', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'operations', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'ops_approved';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'ops_approved', 'Ops Approved', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'operations', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'ops_approved_inrevision';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'ops_approved_inrevision', 'Ops Approved/In Revision', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'operations', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'ops_suspended';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'ops_suspended', 'Ops Suspended', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'operations', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'ops_terminated';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'ops_terminated', 'Ops Terminated', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'operations', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'ops_in_recon';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'ops_in_recon', 'Ops In Reconciliation', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'operations', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'ops_closed';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'ops_closed', 'Ops Closed', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'operations', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;  
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'prc_review_new';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'prc_review_new', 'PRC Review New', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'ctrc_prc', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'prc_conditionally_approved';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'prc_conditionally_approved', 'PRC Conditionally Approved', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'ctrc_prc', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'prc_withdrawn';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'prc_withdrawn', 'PRC Withdrawn', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'ctrc_prc', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'prc_tabled';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'prc_tabled', 'PRC Tabled', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'ctrc_prc', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'prc_disapproved';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'prc_disapproved', 'PRC Disapproved-A protocol that is disapproved may be resubmitted at a later date for review', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'ctrc_prc', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'prc_approved/inrevision';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'prc_approved/inrevision', 'PRC Approved/In Revision', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'ctrc_prc', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'prc_onhold';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'prc_onhold', 'PRC ON HOLD', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'ctrc_prc', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'prc_closed';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'prc_closed', 'PRC Closed', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'ctrc_prc', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'hsc_new';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'hsc_new', 'HSC New', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'hsc_institutional', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'hsc_withdrawn';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'hsc_withdrawn', 'HSC Withdrawn', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'hsc_institutional', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'hsc_active';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'hsc_active', 'HSC Active', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'hsc_institutional', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'hsc_suspended';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'hsc_suspended', 'HSC Suspended', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'hsc_institutional', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'hsc_inactivated';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'hsc_inactivated', 'HSC Inactivated', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, 'hsc_institutional', NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,292,921319,'09_ER_CODELST-INSERT-319.sql',sysdate,'v9.2.0 #693.06');

commit;

