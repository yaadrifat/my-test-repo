SET define OFF;
-- INSERTING into ER_REPORT
--- All Account Users
DECLARE
  v_record_exists NUMBER := 0;
BEGIN
  SELECT COUNT(*) INTO v_record_exists FROM er_report WHERE pk_report = 20005;
  IF (v_record_exists = 0) THEN
    INSERT
    INTO ER_REPORT
      (
        PK_REPORT,
        REP_NAME,
        REP_DESC,
        REP_SQL,
        FK_ACCOUNT,
        FK_CODELST_TYPE,
        IP_ADD,
        REP_HIDE,
        REP_COLUMNS,
        REP_FILTERBY,
        GENERATE_XML,
        REP_TYPE,
        REP_SQL_CLOB,
        REP_FILTERKEYWORD,
        REP_FILTERAPPLICABLE
      )
      VALUES
      (
        20005,
        'User Profile and Access - UT',
        'User Profile and Access - UT',
        '',
        0,
        NULL,
        NULL,
        'N',
        NULL,
        NULL,
        0,
        'rep_usr',
        '',
        'userId',
        'userId'
      );
    COMMIT;

update er_report set REP_SQL = 'select PKG_REPORT_UT.F_Gen_UsrProfile_UT('':userId'') from dual'
where pk_report = 20005;

update er_report set REP_SQL_CLOB = 'select PKG_REPORT_UT.F_Gen_UsrProfile_UT('':userId'') from dual'
where pk_report = 20005;
COMMIT;
  END IF;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,292,921315,'05_er_report_20005.sql',sysdate,'v9.2.0 #693.06');

commit;

