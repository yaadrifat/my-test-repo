SET define OFF;
-- INSERTING into ER_REPORT
--- Adverse Events by Patient
DECLARE
  v_record_exists NUMBER := 0;
  SQL_CLOB CLOB;
BEGIN
  SELECT COUNT(*) INTO v_record_exists FROM er_report WHERE pk_report = 20004;
  IF (v_record_exists = 0) THEN
    INSERT
    INTO ER_REPORT
      (
        PK_REPORT,
        REP_NAME,
        REP_DESC,
        REP_SQL,
        FK_ACCOUNT,
        FK_CODELST_TYPE,
        IP_ADD,
        REP_HIDE,
        REP_COLUMNS,
        REP_FILTERBY,
        GENERATE_XML,
        REP_TYPE,
        REP_SQL_CLOB,
        REP_FILTERKEYWORD,
        REP_FILTERAPPLICABLE
      )
      VALUES
      (
        20004,
        'Adverse Events by Patient - UT',
        'Adverse Events by Patient - UT',
        '',
        0,
        NULL,
        NULL,
        'N',
        NULL,
        NULL,
        1,
        'rep_ae',
        '',
        'studyId:patientId',
        'studyId:patientId:date'
      );
    COMMIT;

  SQL_CLOB := 'select  b.PAT_STUDYID , b.FK_STUDY, b.STUDY_NUMBER,(SELECT field_value FROM erv_study_more_details WHERE b.fk_study = fk_study
  )AS irb_number, b.SITE_NAME, b.STUDY_TITLE, to_char(b.STUDY_ACTUALDT,PKG_DATEUTIL.F_GET_DATEFORMAT) STUDY_ACTUALDT,   b.PER_CODE,
ad.PK_ADVEVE, ad.AE_TYPE_DESC,to_char(ad.AE_STDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) AE_STDATE,
to_char(ad.AE_ENDDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) AE_ENDDATE,  ad.AE_SEVERITY,  ad.AE_DESC,   ad.AE_RELATIONSHIP_DESC, (SELECT codelst_desc FROM SCH_CODELST,SCH_ADVERSEVE WHERE pk_codelst = fk_codelst_outaction and sch_adverseve.pk_adveve = ad.pk_adveve) AS AE_Action,
ad.AE_BDSYSTEM_AFF,  ad.AE_RECOVERY_DESC, ''O'' REC_TYPE,    ev.info_desc OUTCOME, null ADVNOTIFY_DATE,  ad.AE_OUTNOTES,
(Select Decode(ADVINFO_VALUE,0,''No'',1,''Yes'') from ESCH.EV_ADV_CHKINFO
Where fk_adverse = ad.pk_adveve and trim(ADVINFO_TYPE) = ''A'' and trim(codelst_subtyp) = ''adv_unexp'')  unexpected ,
(Select  Decode(ADVINFO_VALUE,0,''No'',1,''Yes'') from ESCH.EV_ADV_CHKINFO
Where fk_adverse = ad.pk_adveve and trim(ADVINFO_TYPE) = ''A'' and trim(codelst_subtyp) = ''adv_dopped'') adv_dropped,
(Select  Decode(ADVINFO_VALUE,0,''No'',1,''Yes'') from ESCH.EV_ADV_CHKINFO
Where fk_adverse = ad.pk_adveve and trim(ADVINFO_TYPE) = ''A'' and trim(codelst_subtyp) = ''adv_violation'') adv_violation ,
(select count(*) from ESCH.sch_Adverseve e
where e.fk_study in (:studyId)  and e.fk_per in (:patientId) and trunc(ad.AE_STDATE) between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)) adv_cnt,
(ad.AE_ADV_GRADE) adv_grade
from  ESCH.ev_adv_type ad, erv_patstudy b, ESCH.EV_ADV_CHKINFO_CHECKED ev
where  ad.fk_study = b.fk_study and   ad.fk_per =  b.fk_per and  ad.PK_ADVEVE = ev.fk_adverse(+) and
ad.fk_study in (:studyId) and
b.fk_per in (:patientId) and  trunc(ad.AE_STDATE) between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
UNION
select b.PAT_STUDYID, b.FK_STUDY, b.STUDY_NUMBER, (SELECT field_value FROM erv_study_more_details WHERE b.fk_study = fk_study
  )AS irb_number, b.SITE_NAME, b.STUDY_TITLE, to_char(b.STUDY_ACTUALDT,PKG_DATEUTIL.F_GET_DATEFORMAT) STUDY_ACTUALDT,
b.PER_CODE,   ad.PK_ADVEVE, ad.AE_TYPE_DESC,  to_char(ad.AE_STDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) AE_STDATE,
to_char(ad.AE_ENDDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) AE_ENDDATE, ad.AE_SEVERITY,  ad.AE_DESC,    ad.AE_RELATIONSHIP_DESC, (SELECT codelst_desc FROM SCH_CODELST,SCH_ADVERSEVE WHERE pk_codelst = fk_codelst_outaction and sch_adverseve.pk_adveve = ad.pk_adveve) AS AE_Action,
ad.AE_BDSYSTEM_AFF,    ad.AE_RECOVERY_DESC,  ''N'' REC_TYPE,  adnot.ADV_NOT_SENT_TO, to_char(adnot.ADVNOTIFY_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) ADVNOTIFY_DATE, ad.AE_OUTNOTES,
(Select Decode(ADVINFO_VALUE,0,''No'',1,''Yes'') from ESCH.EV_ADV_CHKINFO
Where fk_adverse = ad.pk_adveve and trim(ADVINFO_TYPE) = ''A'' and trim(codelst_subtyp) = ''adv_unexp'') unexpected ,
(Select  Decode(ADVINFO_VALUE,0,''No'',1,''Yes'') from ESCH.EV_ADV_CHKINFO
Where fk_adverse = ad.pk_adveve and trim(ADVINFO_TYPE) = ''A'' and trim(codelst_subtyp) = ''adv_dopped'') adv_dropped,
(Select  Decode(ADVINFO_VALUE,0,''No'',1,''Yes'') from ESCH.EV_ADV_CHKINFO
Where fk_adverse = ad.pk_adveve and trim(ADVINFO_TYPE) = ''A'' and trim(codelst_subtyp) = ''adv_violation'') adv_violation ,
(select count(*) from ESCH.sch_Adverseve e
where e.fk_study in (:studyId)  and e.fk_per in (:patientId)      and trunc(e.AE_STDATE) between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)) adv_cnt,
(ad.AE_ADV_GRADE) adv_grade
from ESCH. ev_adv_type ad, erv_patstudy b, ERV_SCH_ADVNOTIY_CHECKED adnot
where ad.fk_study = b.fk_study and ad.fk_per =  b.fk_per and
ad.pk_adveve = adnot.pk_adveve(+) and
ad.fk_study in (:studyId) and  b.fk_per in (:patientId) and
trunc(ad.AE_STDATE) between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) ';

update er_report set REP_SQL_CLOB = SQL_CLOB
where pk_report = 20004;
COMMIT;
  END IF;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,292,921315,'04_er_report_20004.sql',sysdate,'v9.2.0 #693.06');

commit;

