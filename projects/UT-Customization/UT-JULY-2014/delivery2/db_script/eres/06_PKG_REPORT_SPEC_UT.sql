create or replace
PACKAGE        "PKG_REPORT_UT" AS
FUNCTION F_Gen_UsrProfile_UT(p_user VARCHAR2) RETURN CLOB;
pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'PKG_REPORT_UT', pLEVEL  => Plog.LFATAL);
END Pkg_Report_UT;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,292,921315,'06_PKG_REPORT_SPEC_UT.sql',sysdate,'v9.2.0 #693.06');

commit;

