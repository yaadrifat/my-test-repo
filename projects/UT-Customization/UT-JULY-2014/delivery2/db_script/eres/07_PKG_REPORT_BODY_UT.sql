create or replace
PACKAGE BODY      "ERES"."PKG_REPORT_UT"
AS
FUNCTION F_Gen_UsrProfile_UT(p_user VARCHAR2)
RETURN  CLOB
AS
v_userclob CLOB ;
v_group CLOB ;
v_site CLOB ;
v_username VARCHAR2(200);
v_experience VARCHAR2(4000);
v_phases VARCHAR2(200);
v_jobtype VARCHAR2(4000);
v_special VARCHAR2(4000);
v_address VARCHAR2(4000);
v_city VARCHAR2(200);
v_state VARCHAR2(200);
v_zip VARCHAR2(50);
v_country VARCHAR2(200);
v_phone VARCHAR2(200);
v_email VARCHAR2(4000);
v_groupname VARCHAR2(200);
v_groupdft VARCHAR2(50);
v_sitename VARCHAR2(200);
v_primsite VARCHAR2(50);
v_study CLOB;
v_studypk NUMBER;
v_studynum VARCHAR2(200);
v_irbnum VARCHAR2(200);
v_studytitle VARCHAR2(4000);
v_teamrole VARCHAR2(200);
v_orgname CLOB;
v_org CLOB;
v_notify CLOB;
v_ntype CLOB;
v_nstudy CLOB;
v_npatient CLOB;
v_nform CLOB;
v_ncalendar CLOB;
v_nvisit CLOB;
v_ndesc CLOB;

v_primary_site NUMBER;
v_user VARCHAR2(200);
--v_finalclob CLOB := EMPTY_CLOB();
v_index_comma NUMBER;
v_account Number;
BEGIN
--temporary fix so that if multiple users are passed, the report does not crash
v_user  := p_user;
v_index_comma  := INSTR(v_user,',');
IF  v_index_comma  > 0 THEN
	v_user  := SUBSTR(v_user,1, (v_index_comma -1)  );
END IF  ;


--collect user data
SELECT usr_firstname || ' ' || usr_lastname,usr_wrkexp,usr_pahseinv,F_Get_Codelstdesc(fk_codelst_jobtype),F_Get_Codelstdesc(fk_codelst_spl),address,add_city,
       add_state,add_zipcode,add_country,add_phone,add_email,fk_siteid,ER_USER.fk_account
INTO v_username,v_experience,v_phases,v_jobtype,v_special,v_address,v_city,v_state,v_zip,v_country,v_phone,v_email,v_primary_site,v_account
FROM ER_USER,ER_ADD
WHERE pk_user = v_user AND pk_add = fk_peradd ;

	  --escape spl chars
	    v_username := Pkg_Util.f_escapespecialcharsforxml (v_username);
	    v_experience := Pkg_Util.f_escapespecialcharsforxml (v_experience);
	    v_address := Pkg_Util.f_escapespecialcharsforxml (v_address);
	    v_email := Pkg_Util.f_escapespecialcharsforxml (v_email);

--open <user> and add user data as attributes
v_userclob := v_userclob || '<user name="' || v_username || '" experience="' || v_experience || '" phases="' || v_phases || '" jobtype="' || v_jobtype || '" special="' || v_special || '" Address="' || v_address || '" city="' || v_city || '" state="' || v_state || '" zip="' || v_zip || '" country="' || v_country || '" phone="' || v_phone || '" email="' || v_email || '"> ' ;

--open <groups>
v_group := v_group || '<groups>' ;

--collect group data
FOR i IN (SELECT grp_name,CASE WHEN (SELECT fk_grp_default FROM ER_USER WHERE fk_user = pk_user)= fk_grp THEN 'Yes' ELSE 'No' END AS grpdef
          FROM ER_USRGRP,ER_GRPS WHERE fk_user = v_user AND fk_grp = pk_grp ORDER BY  1 )
   LOOP
    v_groupname := i.grp_name;

    v_groupdft := i.grpdef;

   v_groupname := Pkg_Util.f_escapespecialcharsforxml (v_groupname);

      --open <group> and add group data as attributes
      v_group := v_group || '<group name="' || v_groupname || '" default="' || v_groupdft || '"></group>'; --close <group>
   END LOOP;

--close <groups>
v_group := v_group || '</groups>';

--open <sites>
v_site := v_site || '<sites>' ;

--collect site data
-- Bug fix for #3015-1 By SM 06/18/08
FOR i IN (SELECT distinct site_name,CASE WHEN (v_primary_site = pk_site) THEN 'Yes' ELSE 'No' END AS sitedef
      FROM ER_STUDY_SITE_RIGHTS,ER_SITE WHERE fk_user = v_user AND fk_site = pk_site AND user_study_site_rights > 0)
    LOOP

   v_sitename := i.site_name;

    v_sitename := Pkg_Util.f_escapespecialcharsforxml (v_sitename);

   v_primsite := i.sitedef;

      --open <site> and add site data as attributes
      v_site:= v_site || '<site name="' || v_sitename || '" primary="' ||  v_primsite || '"/>'; --close <site>
   END LOOP;

--close <sites>
v_site := v_site || '</sites>';

--open <studies>
v_study := v_study || '<studies>';

--collect study data
FOR i IN (SELECT pk_study,study_number,(select STUDYID_ID from ER_STUDYID where er_study.PK_STUDY = FK_STUDY and FK_CODELST_IDTYPE =
(select pk_codelst from er_codelst where codelst_type = 'studyidtype' and CODELST_SUBTYP = 'irb_number')) AS irb_number,study_title,F_Get_Codelstdesc(fk_codelst_tmrole) strole
          FROM ER_STUDYTEAM,ER_STUDY WHERE fk_account = v_account and pk_study = fk_study AND fk_user = v_user and study_team_usr_type <> 'X'
          union
          SELECT pk_study,study_number,(select STUDYID_ID from ER_STUDYID where er_study.PK_STUDY = FK_STUDY and FK_CODELST_IDTYPE =
          (select pk_codelst from er_codelst where codelst_type = 'studyidtype' and CODELST_SUBTYP = 'irb_number')) AS irb_number,study_title,'Super User'
          from ER_STUDY WHERE fk_account = v_account  and  Pkg_Superuser.F_Is_Superuser(v_user, pk_study) = 1
          ORDER BY  2)
   LOOP

   v_studypk := i.pk_study;
   v_studynum := i.study_number;
   v_irbnum := i.irb_number;
   v_studytitle := i.study_title;
   v_teamrole := i.strole;
   v_studytitle := Pkg_Util.f_escapespecialcharsforxml (v_studytitle);
   v_studynum := Pkg_Util.f_escapespecialcharsforxml ( v_studynum);
   v_teamrole := Pkg_Util.f_escapespecialcharsforxml ( v_teamrole);

      --open <study> and add study data as attributes
      v_study:= v_study || '<study number="' || v_studynum || '" irb_number="' || v_irbnum || '" title="' || v_studytitle || '" role="' || v_teamrole || '" org="';

      v_orgname := '';

      -- Bug fix for #3015-2 By SM 05/29/08
      if v_teamrole = 'Super User' then
        v_orgname := 'All the Organizations user has access as per User Details page';
      else
        -- Bug fix for #3015-4 By SM 06/18/08
        --collect Site Org Name
        FOR j IN (SELECT distinct site_name,fk_study FROM ER_STUDY_SITE_RIGHTS,ER_SITE WHERE fk_user = v_user AND fk_study = v_studypk AND pk_site = fk_site AND user_study_site_rights > 0 ORDER BY  1)
          LOOP

          v_orgname := v_orgname ||  Pkg_Util.f_escapespecialcharsforxml (j.site_name) || ', ';

          END LOOP;
      end if;

      v_org := trim(TRAILING ',' FROM (trim(TRAILING ' ' FROM v_orgname)));

      --open <org> and add org names
       v_study:= v_study || v_org || '"></study>'; --close <org> --close <study>

      END LOOP;

--close <studies>
v_study:= v_study || '</studies>';

--open <notifications>
v_notify := v_notify || '<notifications>' ;

--collect notification information
FOR i IN (
				SELECT TYPE,study_number   study,
	       NULL AS  patient,(SELECT form_name FROM ER_FORMLIB WHERE pk_formlib = fk_crf) FORM,
	       (SELECT NAME FROM event_assoc WHERE event_id = fk_cal) calendar,(SELECT visit_name FROM sch_protocol_visit WHERE pk_protocol_visit = fk_visit) visit,description
	FROM (
	  SELECT alnot_users users,'Alert' TYPE,fk_study,fk_patprot,fk_protocol fk_cal,NULL fk_crf,NULL fk_visit,
	         (SELECT codelst_desc FROM sch_codelst WHERE pk_codelst = fk_codelst_an) description FROM sch_alertnotify WHERE fk_patprot IS NULL AND alnot_users IS NOT NULL AND   ',' || alnot_users || ',' LIKE '%,' || v_user || ',%'
	  UNION
		    SELECT fn_users,'Form',fk_study,NULL,NULL,fn.fk_formlib,NULL,
	         DECODE(fn_sendtype,'F' ,'First time form is filled' , 'Every time form is filled')
	  FROM ER_FORMNOTIFY fn,ER_LINKEDFORMS sf WHERE fn.fk_formlib = sf.fk_formlib AND  fn_msgtype = 'N'
	  AND fn_users IS NOT NULL AND   ',' || fn_users || ',' LIKE '%,' || v_user || ',%'
	  UNION
	  SELECT milestone_usersto,'Milestone',fk_study,NULL,fk_cal,NULL,NULL,Pkg_Milestone_New.f_getmilestonedesc(pk_milestone) FROM ER_MILESTONE
	  WHERE milestone_usersto IS NOT NULL AND ',' || milestone_usersto || ',' LIKE '%,' || v_user || ',%'
	  UNION
	  SELECT TO_CHAR(eventusr),'Calendar',CASE WHEN event_type = 'P' THEN chain_id ELSE (SELECT ea.chain_id FROM event_assoc ea WHERE e.chain_id = ea.event_id) END,NULL,
	         CASE WHEN event_type = 'P' THEN event_id ELSE (SELECT ea.event_id FROM event_assoc ea WHERE e.chain_id = ea.event_id) END,NULL,fk_visit,
	         CASE WHEN usr_daysbefore IS NULL THEN '' WHEN usr_daysbefore = 0 THEN '' ELSE 'Message sent to user ' || usr_daysbefore || ' days before event: ' || NAME END ||
	         '   ' || CASE WHEN usr_daysafter IS NULL THEN '' WHEN usr_daysafter = 0 THEN '' ELSE 'Message sent to user ' || usr_daysafter || ' days after event: ' || NAME END descript
	  FROM event_assoc e RIGHT JOIN sch_eventusr ON fk_event = event_id WHERE eventusr_type = 'S'
	  AND ',' || eventusr || ',' LIKE '%,' || v_user || ',%'
	  AND (usr_daysbefore IS NOT NULL OR usr_daysafter IS NOT NULL)) a
	LEFT JOIN ER_STUDY ON pk_study = fk_study
ORDER BY TYPE
)
  LOOP
    v_ntype := i.TYPE;
    v_nstudy := CASE WHEN i.study IS NULL THEN NULL ELSE 'Study=' || i.study END;
    v_npatient := CASE WHEN i.patient IS NULL THEN NULL ELSE 'Patient=' || i.patient END;
    v_nform := CASE WHEN i.FORM IS NULL THEN NULL ELSE 'Form=' || i.FORM END;
    v_ncalendar := CASE WHEN i.calendar IS NULL THEN NULL ELSE 'Calendar=' || i.calendar END;
    v_nvisit := CASE WHEN i.visit IS NULL THEN NULL ELSE 'Visit=' || i.visit END;
    v_ndesc := i.description;

	    v_nstudy  := Pkg_Util.f_escapespecialcharsforxml (v_nstudy ) ;
	    v_npatient   := Pkg_Util.f_escapespecialcharsforxml (v_npatient  ) ;
       v_nform    := Pkg_Util.f_escapespecialcharsforxml (   v_nform  ) ;
       v_ncalendar    := Pkg_Util.f_escapespecialcharsforxml (   v_ncalendar ) ;
	    v_nvisit    := Pkg_Util.f_escapespecialcharsforxml (    v_nvisit  ) ;
		 v_ndesc    := Pkg_Util.f_escapespecialcharsforxml (   v_ndesc ) ;

    --open <notify> and add attributes
    v_notify := v_notify || '<notify type="' || v_ntype || '" link="' || v_nstudy || ' ' || v_npatient || ' ' || v_nform || ' ' || v_ncalendar || ' ' || v_nvisit;
    v_notify := v_notify || '" Description="' || v_ndesc || '"/>';

  END LOOP;

  --close <notifications>
  v_notify := v_notify || '</notifications>';

--add <groups>, <sites> and <studies> and close <user>

--v_user := v_user ||     '</user>';

v_userclob := v_userclob || v_group || v_site || v_study || v_notify || '</user>';


  RETURN v_userclob;

END;

END Pkg_Report_UT;
/

CREATE OR REPLACE SYNONYM ESCH.Pkg_Report_UT FOR Pkg_Report_UT;

CREATE OR REPLACE SYNONYM EPAT.Pkg_Report_UT FOR Pkg_Report_UT;

GRANT EXECUTE, DEBUG ON Pkg_Report_UT TO EPAT;

GRANT EXECUTE, DEBUG ON Pkg_Report_UT TO ESCH;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,292,921315,'07_PKG_REPORT_BODY_UT.sql',sysdate,'v9.2.0 #693.06');

commit;




