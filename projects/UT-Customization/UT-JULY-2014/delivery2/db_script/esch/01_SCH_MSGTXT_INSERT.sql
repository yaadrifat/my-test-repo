SET Define OFF;
DECLARE
  rowExists NUMBER := 0;
BEGIN
  SELECT COUNT(*) INTO rowExists FROM SCH_MSGTXT WHERE msgtxt_type = 'stdstat';
  IF (rowExists = 0) THEN
    INSERT
    INTO "ESCH"."SCH_MSGTXT"
      (
        PK_MSGTXT,
        MSGTXT_TYPE,
        MSGTXT_LONG,
        FK_ACCOUNT,
        CREATED_ON,
        MSGTXT_SUBJECT
      )
      VALUES
      (
        (SEQ_ER_STUDYSTAT.nextval)+1 ,
        'stdstat',
        'PI Name: ~1~
IRB Number: ~2~
Study Number: ~3~
Title: ~4~ 

This notice is to inform you that the study referenced above has completed all institutional activation requirements.  This includes, but is not limited to:  IRB approval, Institutional approval, and execution of applicable financial agreements.

This study is now open for enrollment in Velos eResearch.

**Please note that if this study has a Study Sponsor, that all Sponsor requirements must also be completed prior to enrolling participants. 

If you have any questions or need further assistance, please contact the following:

		For cancer-related studies:  CTRC CTO : cto@uthscsa.edu
		For non-cancer studies:      VPR CTO : vprcto@uthscsa.edu',
        '0',
        SYSDATE,
        'Study Activation Notification'
      );
  END IF;
COMMIT;
END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,292,921316,'01_SCH_MSGTXT_INSERT.sql',sysdate,'v9.2.0 #693.06');

commit;