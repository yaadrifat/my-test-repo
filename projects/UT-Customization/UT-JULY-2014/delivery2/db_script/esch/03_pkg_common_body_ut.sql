create or replace
PACKAGE BODY        "PKG_COMMON_UT" AS

FUNCTION SCH_GETMAIL (p_message VARCHAR2, p_params VARCHAR2)
    RETURN  VARCHAR2
IS
       v_msg  VARCHAR2 (4000);
       V_POS      NUMBER         := 0;
       V_CNT      NUMBER         := 0;
       V_PARAMS   VARCHAR2 (2000)DEFAULT p_params || '~';
       V_STR      VARCHAR2 (2000)DEFAULT P_PARAMS || '~';
        BEGIN
         v_msg :=  p_message;
     /*replace the placeholders in message string in order of parameters*/
            LOOP
               EXIT WHEN V_PARAMS IS NULL;
               V_CNT := V_CNT + 1;
               V_POS := INSTR (V_STR, '~');
               V_PARAMS := SUBSTR (V_STR, 1, V_POS - 1);
               if (V_PARAMS = 'B_Value') then
                v_msg := REPLACE (v_msg, '~' || V_CNT || '~', '');
               else
                v_msg := REPLACE (v_msg, '~' || V_CNT || '~', V_PARAMS);
               end if;
               V_STR := SUBSTR (V_STR, V_POS + 1);
         END LOOP;

            /*Test*/
      --p(v_msg);
              RETURN v_msg;
END SCH_GETMAIL;

FUNCTION SCH_GETMAILMSG (p_mesgtype VARCHAR2)
    RETURN  VARCHAR2
IS
       v_msg  VARCHAR2 (4000);
        BEGIN
             SELECT dbms_lob.SUBSTR(msgtxt_long,4000,1)
             INTO v_msg
             FROM sch_msgtxt
             WHERE UPPER(msgtxt_type) = UPPER(p_mesgtype) ;
          --  p(v_msg);
             RETURN v_msg;
END SCH_GETMAILMSG ;

FUNCTION SCH_GETMSGSUBJECT (p_mesgtype VARCHAR2)
    RETURN  VARCHAR2
IS
       v_msg  VARCHAR2 (4000);
        BEGIN
             SELECT msgtxt_subject
             INTO v_msg
             FROM sch_msgtxt
             WHERE UPPER(msgtxt_type) = UPPER(p_mesgtype) ;
          --  p(v_msg);
             RETURN v_msg;
END SCH_GETMSGSUBJECT;

END Pkg_Common_UT ;
/

CREATE OR REPLACE SYNONYM ERES.Pkg_Common_UT FOR Pkg_Common_UT;


CREATE OR REPLACE SYNONYM EPAT.Pkg_Common_UT FOR Pkg_Common_UT;


GRANT EXECUTE, DEBUG ON Pkg_Common_UT TO EPAT;

GRANT EXECUTE, DEBUG ON Pkg_Common_UT TO ERES;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,292,921316,'03_pkg_common_body_ut.sql',sysdate,'v9.2.0 #693.06');

commit;