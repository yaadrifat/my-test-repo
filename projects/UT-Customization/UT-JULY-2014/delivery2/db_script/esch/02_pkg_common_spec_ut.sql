set Define Off;
create or replace
PACKAGE  "PKG_COMMON_UT" as

FUNCTION SCH_GETMAIL (p_message varchar2, p_params varchar2)
    RETURN  varchar2 ;

FUNCTION SCH_GETMAILMSG (p_mesgtype varchar2)
    RETURN  varchar2 ;

FUNCTION SCH_GETMSGSUBJECT (p_mesgtype varchar2)
    RETURN  varchar2 ;
end pkg_common_ut ;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,292,921316,'02_pkg_common_spec_ut.sql',sysdate,'v9.2.0 #693.06');

commit;