select * from (SELECT LINEITEM_NAME,
BUDGET_NAME,
BGTCAL_PROTTYPE,
PROT_CALENDAR,
BUDGET_VERSION,
BUDGET_DESC,
STUDY_NUMBER,
IRB_NUMBER,
STUDY_TITLE,
STUDY_PRINV,
STUDY_SPONSOR,
SPONSOR_NUMBER,
SITE_NAME,
BGTSECTION_NAME,
DECODE(LINE_ITEM_INDIRECTS_FLAG,1,'Yes',' ') AS LINE_ITEM_INDIRECTS_FLAG,
DECODE(COST_DISCOUNT_ON_LINE_ITEM,1,'Yes',' ') AS COST_DISCOUNT_ON_LINE_ITEM,
INDIRECTS,
BUDGET_INDIRECT_FLAG,
BUDGET_CURRENCY,
UNIT_COST,
LINEITEM_RECEIVABLE,
NUMBER_OF_UNIT,
LINEITEM_RECEIVABLE_DIRECT,
TOTAL_LITEM_RECEIVABLE_PER_PAT,
TOTAL_LITEM_RECEIVABLE_ALL_PAT,
case when BGTSECTION_PATNO is null then '1' else BGTSECTION_PATNO end AS BGTSECTION_PATNO,
LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,
DECODE(COST_CUSTOMCOL,'research','No','soc','Yes','Other') AS standard_of_care,
NVL(LINEITEM_SPONSORAMOUNT, 0) AS SPONSOR_AMOUNT,
FK_CODELST_COST_TYPE,COST_TYPE_DESC,
lineitem_direct_perpat,total_cost_per_pat, total_cost_all_pat,pk_budgetsec,nvl(budgetsec_fkvisit,0)budgetsec_fkvisit,pk_lineitem,
nvl(bgtcal_excldsocflag,0) as bgtcal_excldsocflag,bgtsection_sequence,lineitem_seq ,lower(LINEITEM_NAME) as l_LINEITEM_NAME,BGTSECTION_TYPE,
NVL(FK_EVENT, 0) as FK_EVENT
FROM esch.erv_budget_comp
WHERE pk_budget = ~2 AND
pk_bgtcal = ~3
) where pk_budgetsec is not null
ORDER BY pk_budgetsec, BGTSECTION_NAME, LINEITEM_NAME