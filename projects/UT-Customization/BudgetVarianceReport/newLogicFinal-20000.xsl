<?xml version="1.0"?> 
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" version="4.0" encoding="iso-8859-1" indent="yes"/>
[VELNUMFORMATDEF]
[VELFLOATFORMATDEF]
<xsl:key name="RecordsByCategory" match="ROW" use="BGTSECTION_NAME" />
<xsl:key name="RecordsByLineitemName" match="ROW" use="concat(BGTSECTION_NAME, ' ', PK_LINEITEM)" />
<xsl:key name="RecordsByUnitCostFlag" match="ROW" use="concat(BGTSECTION_NAME, ' ', PK_LINEITEM, ' ', UNIT_COST_FLAG)" />
<xsl:param name="hdrFileName" />
<xsl:param name="ftrFileName" />
<xsl:param name="repTitle"/>
<xsl:param name="repName"/>
<xsl:param name="repBy"/>
<xsl:param name="repDate"/>
<xsl:param name="argsStr"/>
<xsl:param name="wd"/>
<xsl:param name="xd"/>
<xsl:param name="hd"/>
<xsl:param name="cond"/>
<xsl:param name="hdrflag"/>
<xsl:param name="ftrflag"/>
<xsl:param name="mode"/>
<xsl:param name="budgetTemplate"/>
<xsl:param name="pkBgtCal"/>
<xsl:param name="budgetStatus"/>
<xsl:param name="pkBudget"/>
<xsl:param name="pageRight"/>
<xsl:param name="includedIn"/>
<xsl:param name="from"/>
<xsl:param name="ddList"/>
<xsl:param name="numFormat"/>
<xsl:param name="floatFormat"/>
<xsl:template match="/">
<HTML>
<HEAD><TITLE> <xsl:value-of select="$repName" /> </TITLE>
<link rel="stylesheet" href="./styles/common.css" type="text/css"/>
</HEAD>
<BODY class="repBody">
<input>
	<xsl:attribute name="type">hidden</xsl:attribute>
	<xsl:attribute name="id">fromWhere</xsl:attribute>
	<xsl:attribute name="name">fromWhere</xsl:attribute>
	<xsl:attribute name="value"><xsl:value-of select="$from"/></xsl:attribute>
</input>
        
<xsl:if test="$cond='T'">
    <xsl:if test="$mode='V'">
        <table width="100%" >
        <tr class="reportGreyRow">
        <td class="reportPanel"> 
        VELMESSGE[M_Download_ReportIn]<!-- Download the report in -->: 
        <A href='{$wd}' >
        <img border="0" title="VELLABEL[L_Word_Format]" alt="VELLABEL[L_Word_Format]" src="./images/word.GIF" ></img>
        </A> 
        <xsl:text>&#xa0;</xsl:text>
        <xsl:text>&#xa0;</xsl:text>
        <xsl:text>&#xa0;</xsl:text>
        <xsl:text>&#xa0;</xsl:text>
        <xsl:text>&#xa0;</xsl:text>
        <A href='{$xd}' >
        <img border="0" title="VELLABEL[L_Excel_Format]" alt="VELLABEL[L_Excel_Format]" src="./images/excel.GIF" ></img>
        </A>
        <xsl:text>&#xa0;</xsl:text>
        <xsl:text>&#xa0;</xsl:text>
        <xsl:text>&#xa0;</xsl:text>
        <xsl:text>&#xa0;</xsl:text>
        <xsl:text>&#xa0;</xsl:text>
        <A href='{$hd}' >
        <img src="./images/printer.gif" border="0" title="VELLABEL[L_Printer_FriendlyFormat]" alt="VELLABEL[L_Printer_FriendlyFormat]"></img>
        </A> 
        </td>
        </tr>
        </table>
    </xsl:if>
</xsl:if>
<xsl:apply-templates select="ROWSET"/>
</BODY>
</HTML>
</xsl:template> 

<xsl:variable name="excludeSOC_flag">
<xsl:value-of select="//BGTCAL_EXCLDSOCFLAG"/>
</xsl:variable>

<xsl:template match="ROWSET">

<xsl:if test="$mode='V'">
    <TABLE WIDTH="100%" >
    <xsl:if test="$hdrflag='1'">
    <TR>
    <TD WIDTH="100%" ALIGN="CENTER">
    <img src="{$hdrFileName}"/>
    </TD>
    </TR>
    </xsl:if>
    <TR>
    <TD class="reportName" WIDTH="100%" ALIGN="CENTER">
    <xsl:value-of select="$repName" />
    </TD>
    </TR>
    </TABLE>
    <hr class="thickLine" />

    <TABLE WIDTH="100%" >
	    <TR>
		    <TD class="reportGrouping" ALIGN="Left" width="22%">
		    VELLABEL[L_Study_Number] (CTMS#)<!-- Study Number (CTMS#)-->: </TD><TD class="reportData"><b><xsl:value-of select="//STUDY_NUMBER" /></b>
		    </TD>
	    </TR>
	    <TR>
		    <TD class="reportGrouping" ALIGN="Left" width="22%">
		    	VELLABEL[L_Irb_Number]<!-- IRB Number -->: </TD><TD class="reportData" align="left"><b><xsl:value-of select="//IRB_NUMBER" /></b>
		    </TD>
	    </TR>
	    <TR>
		    <TD class="reportGrouping" ALIGN="Left" width="22%">
		    	VELLABEL[L_Principal_Investigator]<!-- Principal Investigator -->: </TD><TD class="reportData"><b><xsl:value-of select="//STUDY_PRINV" /></b>
		    </TD>
	    </TR>
	    <TR>
		    <TD class="reportGrouping" ALIGN="left" width="22%">
		    	VELLABEL[L_Sponsor_Name]<!-- Sponsor Name -->: </TD><TD class="reportData"><b><xsl:value-of select="//STUDY_SPONSOR" /></b>
		    </TD>
	    </TR>
	    <TR>
		    <TD class="reportGrouping" ALIGN="LEFT" width="22%">
		    	VELLABEL[L_Spnprot_Number]<!-- Sponsor Protocol Number -->: </TD><TD class="reportData"><b><xsl:value-of select="//SPONSOR_NUMBER" /></b>
		    </TD>
	    </TR>
    </TABLE>
    <hr class="thinLine" />
</xsl:if> 
<xsl:variable name="colsp3">5</xsl:variable>
<xsl:variable name="colsp6">
	<xsl:choose>
		<xsl:when test="$budgetTemplate!='C' and $mode='V'">13</xsl:when>
		<xsl:when test="$budgetTemplate!='C' and $mode='M'">11</xsl:when>
		<xsl:when test="$budgetTemplate='C' and $mode='V'">15</xsl:when>
		<xsl:when test="$budgetTemplate='C' and $mode='M'">12</xsl:when>
		<xsl:otherwise>11</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<div id="TableContainer">
    <TABLE style="border: 1px solid;">
	<thead>
        <TR>
   			<TH class="reportHeading" ALIGN="CENTER">VELLABEL[L_Visit]<!-- Visit --></TH>
			<TH class="reportHeading" ALIGN="CENTER">VELLABEL[L_Event]<!-- Event --></TH>
			<TH class="reportHeading" ALIGN="CENTER">VELLABEL[L_Unit_Cost]<!-- Unit Cost --></TH>
			<TH class="reportHeading" ALIGN="CENTER" title="VELLABEL[L_Number_OfUnits]">VELLABEL[L_Units]</TH><!-- Units -->
			<TH class="reportHeading" ALIGN="CENTER" title="VELMESSGE[M_Apply_DcntOrMarkup]">VELLABEL[L_DOrM]</TH><!-- D/M -->
			<TH class="reportHeading" ALIGN="CENTER" title="VELLABEL[L_Apply_Indirects]">VELLABEL[L_IOrD_Applied]<!-- I/D Applied --></TH>
			<TH class="reportHeading" ALIGN="CENTER">VELLABEL[L_Direct_CostOrPat]<!-- Direct Cost/Patient --></TH>
			<TH class="reportHeading" ALIGN="CENTER">VELLABEL[L_Total_CostOrPat]<!-- Total Cost/Patient --></TH>
			<TH class="reportHeading" ALIGN="CENTER">VELLABEL[L_Total_CostOrAllPatient]<!-- Total Cost/All Patients --></TH>
			<TH class="reportHeading" ALIGN="CENTER">VELLABEL[L_Receivable_Amount]<!-- Receivable Amount --></TH>
			<TH class="reportHeading" ALIGN="CENTER">VELLABEL[L_DirReceivable_Patient]<!-- Direct Receivable/Patient --></TH>
			<TH class="reportHeading" ALIGN="CENTER">VELLABEL[L_TotReceivable_Patient]<!-- Total Receivable/Patient --></TH>
			<TH class="reportHeading" ALIGN="CENTER">VELLABEL[L_TotReceivable_AllPatient]<!-- Total Receivable/All Patient --></TH>
			<TH class="reportHeading" ALIGN="CENTER">VELLABEL[L_Variance_Patient]<!-- Variance/Patient --></TH>
			<TH class="reportHeading" ALIGN="CENTER">VELLABEL[L_Variance_AllPatient]<!-- Variance/All Patient --></TH>
		</TR>
	</thead>
	
    <tbody name="scrollTableBody">
    	<xsl:if test="$mode!='V'">
    		<xsl:attribute name="class">scrollContent bodyFormat</xsl:attribute>
    		<xsl:attribute name="style">height:460px;</xsl:attribute>
    	</xsl:if>
<xsl:for-each select="ROW[count(. | key('RecordsByCategory', BGTSECTION_NAME)[1])=1]">
	<xsl:variable name="bgtSectionName" select="BGTSECTION_NAME" />
	<xsl:variable name="categoryStr" select="key('RecordsByCategory', BGTSECTION_NAME)" />
	
	<xsl:for-each select="key('RecordsByCategory', BGTSECTION_NAME)">
		<xsl:variable name="bgtsection"><xsl:value-of select="BGTSECTION_NAME"/></xsl:variable>
		<xsl:variable name="str" select="key('RecordsByLineitemName', concat($bgtsection, ' ', PK_LINEITEM))" />

		<xsl:variable name="vcost_custom">
			<xsl:value-of select="STANDARD_OF_CARE"/>
		</xsl:variable>
		<xsl:variable name="subCostItem_flag">
			<xsl:value-of select="SUBCOST_ITEM_FLAG"/>
		</xsl:variable>
		<xsl:variable name="class">
			<xsl:if test="$excludeSOC_flag='1' and $vcost_custom='Yes'">
				stdOfCare
			</xsl:if>
			<xsl:if test="( $excludeSOC_flag='1' and $vcost_custom != 'Yes') or $excludeSOC_flag='0'">
				<xsl:choose>
				<xsl:when test="number(position() mod 2)=0" >reportEvenRow</xsl:when> 
				<xsl:otherwise>reportOddRow</xsl:otherwise>
				</xsl:choose> 
			</xsl:if>      
		</xsl:variable>

		<xsl:variable name="visitLine">
		<xsl:value-of select="BUDGETSEC_FKVISIT"/>
		</xsl:variable>

		<xsl:variable name="allowSubmitLine">
		<xsl:choose>
		<xsl:when test="(($includedIn='P' and $visitLine = 0) or $includedIn='')">1</xsl:when> 
		<xsl:otherwise>0</xsl:otherwise>
		</xsl:choose> 
		</xsl:variable>
		<xsl:if test="LINEITEM_NAME != ''">
		<TR style="border: 1px solid;"> 
		<xsl:attribute name="class">
			<xsl:choose>
				<xsl:when test="($subCostItem_flag='1')">subCostRow</xsl:when>
				<xsl:when test="($subCostItem_flag='1' and $class='')">subCostRow</xsl:when>
				<xsl:when test="($class='')">reportOddRow</xsl:when> 
				<xsl:otherwise><xsl:value-of select="$class"/></xsl:otherwise>
			</xsl:choose> 
		</xsl:attribute>
		<xsl:variable name="lineitem"><xsl:value-of select="LINEITEM_NAME"/></xsl:variable>
			<!-- Visit -->
			<TD class="reportData"><xsl:value-of select="BGTSECTION_NAME" /></TD>
			<!-- Event -->
			<TD class="reportData"><xsl:value-of select="LINEITEM_NAME" /></TD>

			<!-- Unit Cost -->
			<TD class="reportData" ALIGN="right"><xsl:value-of select="format-number($str/UNIT_COST,$floatFormat,'floatFormatDef')" /></TD>
			<!-- Units -->
			<TD class="reportData" ALIGN="right"><xsl:value-of select="format-number(NUMBER_OF_UNIT,$floatFormat,'floatFormatDef')" /></TD>
			<!-- D/M -->
			<TD class="reportData" ALIGN="center"><xsl:value-of select="$str/COST_DISCOUNT_ON_LINE_ITEM" /></TD>
			<!-- I/D Applied -->
			<TD class="reportData" ALIGN="center"><xsl:value-of select="LINE_ITEM_INDIRECTS_FLAG"/></TD>
			
			<!-- Direct Cost/Patient -->
			<TD class="reportData" ALIGN="right"><xsl:value-of select="format-number($str/LINEITEM_DIRECT_PERPAT ,$floatFormat,'floatFormatDef')" /></TD>	
			<!-- Total Cost/Patient -->
			<TD class="reportData" ALIGN="right"><xsl:value-of select="format-number($str/TOTAL_COST_PER_PAT,$floatFormat,'floatFormatDef')" /></TD>
			<!-- Total Cost/All Patients -->
			<TD class="reportData" ALIGN="right"><xsl:value-of select="format-number($str/TOTAL_COST_ALL_PAT,$floatFormat,'floatFormatDef')" /></TD>
		
			<!-- Receivable Amount -->
			<TD class="reportData" ALIGN="right"><xsl:value-of select="format-number($str/LINEITEM_RECEIVABLE,$floatFormat,'floatFormatDef')" /></TD>
			<!-- Direct Receivable/Patient -->
			<TD class="reportData" ALIGN="right"><xsl:value-of select="format-number($str/LINEITEM_RECEIVABLE_DIRECT ,$floatFormat,'floatFormatDef')" /></TD>
			<!-- Total Receivable/Patient -->
			<TD class="reportData" ALIGN="right"><xsl:value-of select="format-number($str/TOTAL_LITEM_RECEIVABLE_PER_PAT ,$floatFormat,'floatFormatDef')" /></TD>
			<!-- Total Receivable/All Patient -->
			<TD class="reportData" ALIGN="right"><xsl:value-of select="format-number($str/TOTAL_LITEM_RECEIVABLE_ALL_PAT,$floatFormat,'floatFormatDef')" /></TD>

			<!-- Variance/Patient -->
			<td class="reportData" align="right"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(($str/TOTAL_LITEM_RECEIVABLE_PER_PAT - $str/TOTAL_COST_PER_PAT),$floatFormat,'floatFormatDef')"/></td>
			<!-- Variance/All Patient -->
			<td class="reportData" align="right"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(($str/TOTAL_LITEM_RECEIVABLE_ALL_PAT - $str/TOTAL_COST_ALL_PAT),$floatFormat,'floatFormatDef')"/></td>
		</TR>
		</xsl:if>
	</xsl:for-each>
		<tr>
			<td colspan="15" bgColor="black"></td>
		</tr>
		<tr>
			<td class="reportGrouping">
				<xsl:attribute name="colspan"><xsl:value-of select="$colsp3"/></xsl:attribute>
				<font color="blue">
					<xsl:value-of select="BGTSECTION_NAME" />&#xa0;
				</font>
			</td>
			<td colspan="2" align="left" class="reportGrouping"><font color="blue">Visit Total</font></td>
			<!-- Total Cost/Patient -->
			<td align="right" class="reportGrouping"><font color="blue"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(/ROWSET/ROW[BGTSECTION_NAME=$bgtSectionName]//TOTAL_COST_PER_PAT),$floatFormat,'floatFormatDef')"/></font></td>
			<!-- Total Cost/All Patients -->
			<td align="right" class="reportGrouping"><font color="blue"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(/ROWSET/ROW[BGTSECTION_NAME=$bgtSectionName]//TOTAL_COST_ALL_PAT) ,$floatFormat,'floatFormatDef')"/></font></td>
			<!-- Receivable Amount -->
			<td align="right" class="reportGrouping"><font color="blue"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(/ROWSET/ROW[BGTSECTION_NAME=$bgtSectionName]//LINEITEM_RECEIVABLE) ,$floatFormat,'floatFormatDef')"/></font></td>
			<!-- Direct Receivable/Patient -->
			<td align="right" class="reportGrouping"><font color="blue"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(/ROWSET/ROW[BGTSECTION_NAME=$bgtSectionName]//LINEITEM_RECEIVABLE_DIRECT),$floatFormat,'floatFormatDef')"/></font></td>
			<!-- Total Receivable/Patient -->
			<td align="right" class="reportGrouping"><font color="blue"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(/ROWSET/ROW[BGTSECTION_NAME=$bgtSectionName]//TOTAL_LITEM_RECEIVABLE_PER_PAT) ,$floatFormat,'floatFormatDef')"/></font></td>
			<!-- Total Receivable/All Patient -->
			<td align="right" class="reportGrouping"><font color="blue"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(/ROWSET/ROW[BGTSECTION_NAME=$bgtSectionName]//TOTAL_LITEM_RECEIVABLE_ALL_PAT) ,$floatFormat,'floatFormatDef')"/></font></td>
			
			<!-- Variance/Patient -->
			<td align="right" class="reportGrouping"><font color="blue"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(/ROWSET/ROW[BGTSECTION_NAME=$bgtSectionName]//TOTAL_LITEM_RECEIVABLE_PER_PAT )- sum(/ROWSET/ROW[BGTSECTION_NAME=$bgtSectionName]//TOTAL_COST_PER_PAT),$floatFormat,'floatFormatDef')"/></font></td>
			<!-- Variance/All Patient -->
			<td align="right" class="reportGrouping"><font color="blue"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(/ROWSET/ROW[BGTSECTION_NAME=$bgtSectionName]//TOTAL_LITEM_RECEIVABLE_ALL_PAT) - sum(/ROWSET/ROW[BGTSECTION_NAME=$bgtSectionName]//TOTAL_COST_ALL_PAT),$floatFormat,'floatFormatDef')"/></font></td>
		</tr>
		<tr>
			<td colspan="15" bgColor="black"></td>
		</tr>
</xsl:for-each>
<TR>
	<td>
		<xsl:attribute name="colspan"><xsl:value-of select="$colsp3"/></xsl:attribute>
		&#xa0;
	</td>
	<td colspan="2" align="left" class="reportGrouping"><font color="red">VELLABEL[L_Grand_Total]<!-- Grand Total --> </font></td>
	<!-- Total Cost/Patient -->
	<td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(//TOTAL_COST_PER_PAT),$floatFormat,'floatFormatDef')"/></font></td>
	<!-- Total Cost/All Patients -->
	<td  align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(//TOTAL_COST_ALL_PAT),$floatFormat,'floatFormatDef')"/></font></td>
	<!-- Receivable Amount -->
	<td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(//LINEITEM_RECEIVABLE),$floatFormat,'floatFormatDef')"/></font></td>
	<!-- Direct Receivable/Patient -->
	<td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(//LINEITEM_RECEIVABLE_DIRECT) ,$floatFormat,'floatFormatDef')"/></font></td>
	<!-- Total Receivable/Patient -->
	<td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(//TOTAL_LITEM_RECEIVABLE_PER_PAT) ,$floatFormat,'floatFormatDef')"/></font></td>
	<!-- Total Receivable/All Patient -->
	<td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(//TOTAL_LITEM_RECEIVABLE_ALL_PAT) ,$floatFormat,'floatFormatDef')"/></font></td>
	<!-- Variance/Patient -->
	<td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(//TOTAL_LITEM_RECEIVABLE_PER_PAT) - sum(//TOTAL_COST_PER_PAT) ,$floatFormat,'floatFormatDef')"/></font></td>
	<!-- Variance/All Patient -->
	<td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(//TOTAL_LITEM_RECEIVABLE_ALL_PAT) - sum(//TOTAL_COST_ALL_PAT), $floatFormat,'floatFormatDef')"/></font></td>
</TR>
<tr>
	<td colspan="15" bgColor="black"></td>
</tr>
</tbody>
</TABLE>
</div>
<hr class="thickLine" />
<xsl:if test="$mode='V'">
    <TABLE WIDTH="100%" >
        <TR>
        <TD class="reportFooter" WIDTH="50%" ALIGN="LEFT">VELLABEL[L_Last_ModifiedBy]<!-- Last Modified by -->: <xsl:value-of select="//LAST_MODIFIED_BY"/></TD>
        <TD class="reportFooter" WIDTH="50%" ALIGN="RIGHT">VELLABEL[L_Date_LastModified]<!-- Date Last Modified -->: <xsl:value-of select="//LAST_MODIFIED_DATE"/></TD>
        </TR>
        <TR>
        <TD class="reportFooter" WIDTH="50%" ALIGN="LEFT">VELLABEL[L_Report_By]<!-- Report By -->:<xsl:value-of select="$repBy" /></TD>
        <TD class="reportFooter" WIDTH="50%" ALIGN="RIGHT">VELLABEL[L_Date]<!-- Date -->:<xsl:value-of select="$repDate" /></TD>
        </TR>
    </TABLE>
    <xsl:if test="$ftrflag='1'">
    <TABLE>
        <TR>
        <TD WIDTH="100%" ALIGN="CENTER">
        <img src="{$ftrFileName}"/>
        </TD>
        </TR>
    </TABLE>
    </xsl:if>
</xsl:if>
</xsl:template> 
</xsl:stylesheet>