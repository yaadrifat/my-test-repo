
  CREATE OR REPLACE FORCE VIEW "ESCH"."ERV_BUDGET_COMP"
  AS 
  SELECT DISTINCT pk_budget,
    bgtcal_protid,
    pk_bgtcal,
    bgtcal_prottype,
    prot_calendar,
    budget_name,
    budget_version,
    budget_desc,
    study_number,
	irb_number,
    fk_study,
    study_title,
    STUDY_PRINV,
	study_sponsor,
	sponsor_number,
    site_name,
    bgtsection_name,
    bgtsection_patno,
    lineitem_name,
    ROUND(unit_cost, 2) AS unit_cost,
    lineitem_receivable,
    number_of_unit,
	lineitem_othercost,
	ROUND(DECODE (bgtsection_type, 'P',NVL ( DECODE ( budget_indirect_flag, 'Y' , total_pat_cost_after * line_item_indirects_flag * indirects/100 , 0 ) + lineitem_direct_perpat, 0 ) ,'O',0), 2)              AS total_cost_per_pat,
    ROUND(NVL ( (DECODE ( budget_indirect_flag, 'Y', total_pat_cost_after                              * line_item_indirects_flag * indirects/100 , 0 ) + lineitem_direct_perpat) * bgtsection_patno, 0 ) , 2) AS total_cost_all_pat,
    lineitem_receivable_direct,
    ROUND(DECODE (bgtsection_type, 'P',NVL ( DECODE ( budget_indirect_flag, 'Y' , lineitem_receivable * line_item_indirects_flag * indirects/100 , 0 ) + lineitem_receivable_direct, 0 ) ,'O',0), 2)           AS total_litem_receivable_per_pat,
    ROUND(NVL ( (DECODE ( budget_indirect_flag, 'Y', lineitem_receivable * line_item_indirects_flag * indirects/100 , 0 ) + lineitem_receivable_direct) * bgtsection_patno, 0 ) , 2) AS total_litem_receivable_all_pat,
    cost_discount_on_line_item,
    line_item_indirects_flag,
    indirects,
    budget_indirect_flag,
    budget_currency,
    last_modified_by,
    last_modified_date,
    bgtsection_sequence,
    lineitem_desc,
    ROUND((lineitem_sponsoramount ), 2) lineitem_sponsoramount,
    LINEITEM_APPLYPATIENTCOUNT,
    bgtsection_type,
    FK_CODELST_COST_TYPE,
    COST_TYPE_DESC,
    lineitem_seq,
    ROUND(DECODE (bgtsection_type, 'P',NVL (lineitem_direct_perpat, 0),'O',0 ) ,2) lineitem_direct_perpat,
	(SELECT codelst_custom_col1
    FROM sch_codelst
    WHERE pk_codelst = FK_CODELST_COST_TYPE
    ) COST_CUSTOMCOL,
    pk_budgetsec,
    budgetsec_fkvisit,
    pk_lineitem,
    lineitem_fkevent,
    bgtcal_excldsocflag,
    BUDGET_COMBFLAG,
    FK_EVENT
  FROM
    (SELECT pk_budget,
      bgtcal_protid,
      pk_bgtcal,
      bgtcal_prottype,
      DECODE(bgtcal_prottype, 'L',
      ( SELECT NAME FROM event_def WHERE event_id = bgtcal_protid
      ) , 'S',
      ( SELECT NAME FROM event_assoc WHERE event_id = bgtcal_protid
      ) , NULL, 'No Calendar') AS prot_calendar,
      budget_name,
      budget_version,
      budget_desc,
      study_number,
	  irb_number,
      fk_study,
      study_title,
      STUDY_PRINV,
	  study_sponsor,
	  sponsor_number,
      site_name,
      bgtsection_name,
      bgtsection_patno,
      lineitem_name,
      ROUND ( (per_patient_line_fringe + (lineitem_othercost)) - DECODE ( bgtcal_discountflag, 0, 0, 2, 0, 1, (per_patient_line_fringe + ( lineitem_othercost)) * bgtcal_discount * lineitem_incostdisc / 100 ) + DECODE ( bgtcal_discountflag, 0, 0, 1, 0, 2, (per_patient_line_fringe + (lineitem_othercost)) * bgtcal_discount * lineitem_incostdisc / 100 ), 2 ) AS total_pat_cost_after,
      per_patient_line_fringe,
      lineitem_sponsorunit                                                                                                                                                                                                                  AS unit_cost,
      lineitem_receivable,
      lineitem_clinicnofunit                                                                                                                                                                                                                AS number_of_unit,
	  ROUND ( (NVL (per_patient_line_fringe, 0) + (lineitem_receivable)) - DECODE( bgtcal_discountflag, 0,0,2,0,1, (per_patient_line_fringe + ( lineitem_receivable)) * bgtcal_discount * lineitem_incostdisc / 100 ) + DECODE( bgtcal_discountflag, 0,0,1,0,2,(per_patient_line_fringe + ( lineitem_receivable)) * bgtcal_discount * lineitem_incostdisc / 100 ), 2 ) AS lineitem_receivable_direct,
	  lineitem_othercost                                                                                                                                                                                                                    AS cost_per_patient,
      lineitem_incostdisc                                                                                                                                                                                                                   AS cost_discount_on_line_item,
      lineitem_applyindirects                                                                                                                                                                                                               AS line_item_indirects_flag,
      NVL (bgtcal_sponsorohead, 0)                                                                                                                                                                                                          AS indirects,
      bgtcal_sponsorflag                                                                                                                                                                                                                    AS budget_indirect_flag,
      budget_currency,
	  bgtsection_sequence,
      last_modified_by,
      last_modified_date,
      lineitem_desc,
      lineitem_sponsoramount,
      LINEITEM_APPLYPATIENTCOUNT,
      bgtsection_type,
      LINEITEM_OTHERCOST,
      FK_CODELST_COST_TYPE,
      COST_TYPE_DESC,
      lineitem_seq,
      ROUND ( (NVL (per_patient_line_fringe, 0) + (lineitem_othercost)) - DECODE( bgtcal_discountflag, 0,0,2,0,1, (per_patient_line_fringe + ( lineitem_othercost)) * bgtcal_discount * lineitem_incostdisc / 100 ) + DECODE( bgtcal_discountflag, 0,0,1,0,2,(per_patient_line_fringe + ( lineitem_othercost)) * bgtcal_discount * lineitem_incostdisc / 100 ), 2 ) AS lineitem_direct_perpat,
      pk_budgetsec,
      budgetsec_fkvisit,
      pk_lineitem,
      lineitem_fkevent,
      bgtcal_excldsocflag,
      BUDGET_COMBFLAG,
      FK_EVENT,
      EVENT_TYPE
    FROM
      (SELECT pk_budget,
        bgtcal_prottype,
        bgtcal_protid,
        pk_bgtcal,
        budget_name,
        budget_version,
        budget_desc,
        bgtsection_name,
        bgtsection_type,
        bgtsection_patno,
        lineitem_name,
        per_patient_line_fringe,
        fk_codelst_category,
        lineitem_sponsorunit AS lineitem_sponsorunit,
        lineitem_receivable,
        lineitem_clinicnofunit,
        lineitem_stdcarecost AS lineitem_stdcarecost,
        lineitem_incostdisc  AS lineitem_incostdisc,
        lineitem_applyindirects,
        bgtcal_sponsorohead,
        bgtcal_sponsorflag,
        bgtcal_frgbenefit,
        bgtcal_frgflag,
        bgtcal_discount,
        bgtcal_discountflag,
        study_number,
		irb_number,
        fk_study,
        study_title,
        STUDY_PRINV,
		study_sponsor,
		sponsor_number,
        site_name,
        budget_currency,
        last_modified_by,
        last_modified_date,
        bgtsection_sequence,
        lineitem_desc,
        lineitem_sponsoramount,
        LINEITEM_APPLYPATIENTCOUNT,
        LINEITEM_OTHERCOST,
        FK_CODELST_COST_TYPE,
        COST_TYPE_DESC,
        lineitem_seq,
        pk_budgetsec,
        budgetsec_fkvisit,
        pk_lineitem,
        lineitem_fkevent,
        bgtcal_excldsocflag,
        BUDGET_COMBFLAG,
        event_type,
        FK_EVENT
	FROM(SELECT pk_budget,
        bgtcal_prottype,
        bgtcal_protid,
        pk_bgtcal,
        budget_name,
        budget_version,
        budget_desc,
        bgtsection_name,
        bgtsection_type,
        NVL (bgtsection_patno, 1) bgtsection_patno,
        lineitem_name,
        DECODE ( TRIM(
        (SELECT codelst_subtyp
        FROM sch_codelst
        WHERE pk_codelst = fk_codelst_category
        ) ), 'ctgry_per', lineitem_othercost * bgtcal_frgbenefit * NVL (bgtcal_frgflag, 0)/100 , 0 ) AS per_patient_line_fringe,
        fk_codelst_category,
		CASE WHEN (select count(*) from SCH_EVENTCOST where SCH_EVENTCOST.fk_event in (select event_id from EVENT_ASSOC 
			Where chain_id = (select FK_PROTOCOL from sch_protocol_visit where PK_PROTOCOL_VISIT = NVL(sch_bgtsection.fk_visit,0))
			AND EVENT_ASSOC.fk_visit is null and NAME = sch_lineitem.LINEITEM_NAME)AND FK_COST_DESC in (SELECT pk_codelst  FROM sch_codelst
			WHERE CODELST_TYPE='cost_desc' and CODELST_SUBTYP = 'baseline')) > 0 
			THEN
			(select SUM(EVENTCOST_VALUE) from SCH_EVENTCOST where SCH_EVENTCOST.fk_event in (select event_id from EVENT_ASSOC 
			Where chain_id = (select FK_PROTOCOL from sch_protocol_visit where PK_PROTOCOL_VISIT = NVL(sch_bgtsection.fk_visit,0))
			AND EVENT_ASSOC.fk_visit is null and NAME = sch_lineitem.LINEITEM_NAME)AND FK_COST_DESC in (SELECT pk_codelst  FROM sch_codelst
			WHERE CODELST_TYPE='cost_desc' and CODELST_SUBTYP = 'baseline') group by fk_event)
		ELSE
			to_number(to_char(trim(replace(lineitem_sponsorunit,',','')),'999999999999999.99')) 
		END AS lineitem_sponsorunit,
		to_number(to_char(trim(replace(lineitem_sponsorunit,',','')),'999999999999999.99')) as lineitem_receivable,
        lineitem_clinicnofunit,
        lineitem_stdcarecost AS lineitem_stdcarecost,
        lineitem_incostdisc  AS lineitem_incostdisc,
        NVL (lineitem_applyindirects, 0) lineitem_applyindirects,
        NVL (bgtcal_sponsorohead, 0) bgtcal_sponsorohead,
        NVL (bgtcal_sponsorflag, 0) bgtcal_sponsorflag,
        NVL (bgtcal_frgbenefit, 0) bgtcal_frgbenefit,
        NVL (bgtcal_frgflag, 0) bgtcal_frgflag,
        NVL (bgtcal_discount, 0) bgtcal_discount,
        NVL (bgtcal_discountflag, 0) bgtcal_discountflag,
        ( SELECT study_number FROM er_study WHERE pk_study = fk_study
        ) AS study_number,
		(SELECT STUDYID_ID
        FROM ER_STUDYID
        WHERE FK_STUDY        = sch_budget.fk_study
        AND FK_CODELST_IDTYPE =
          (SELECT PK_CODELST
          FROM ER_CODELST
          WHERE CODELST_TYPE = 'studyidtype'
          AND CODELST_SUBTYP = 'irb_number'
          )
        ) AS irb_number,
        ( SELECT pk_study FROM er_study WHERE pk_study = fk_study
        ) AS fk_study,
        ( SELECT study_title FROM er_study WHERE pk_study = fk_study
        ) AS study_title,
		(SELECT usr_firstname
          || ' '
          || usr_lastname
        FROM er_user
        WHERE pk_user =
          (SELECT STUDY_PRINV FROM er_study WHERE pk_study = fk_study
          )
        ) AS STUDY_PRINV,
        (SELECT DECODE(fk_codelst_sponsor,NULL,''
          ||study_sponsor,
          (SELECT CODELST_DESC FROM ER_CODELST WHERE PK_CODELST = FK_CODELST_SPONSOR
          )
          ||DECODE(study_sponsor,NULL,'',','
          ||study_sponsor))
        FROM er_study
        WHERE pk_study = fk_study
        ) AS study_sponsor,
        (SELECT STUDYID_ID
        FROM ER_STUDYID
        WHERE FK_STUDY        = sch_budget.fk_study
        AND FK_CODELST_IDTYPE =
          (SELECT PK_CODELST
          FROM ER_CODELST
          WHERE CODELST_TYPE = 'studyidtype'
          AND CODELST_SUBTYP = 'id_sponsor'
          )
        ) AS sponsor_number,
        ( SELECT site_name FROM er_site WHERE pk_site = fk_site
        ) AS site_name,
        (SELECT NVL (codelst_subtyp, codelst_desc)
        FROM sch_codelst
        WHERE pk_codelst = budget_currency
        ) AS budget_currency,
        (SELECT usr_firstname
          || ' '
          || usr_lastname
        FROM er_user
        WHERE pk_user = NVL ( sch_budget.last_modified_by, sch_budget.creator )
        ) AS last_modified_by,
        TO_CHAR ( NVL (sch_budget.last_modified_date, sch_budget.created_on ), PKG_DATEUTIL.F_GET_DATEFORMAT
        || ' hh:mi:ss' ) AS last_modified_date,
        bgtsection_sequence,
        lineitem_desc,
        DECODE(NVL(LINEITEM_APPLYPATIENTCOUNT,'0'),'0' , lineitem_sponsoramount , ( lineitem_sponsoramount * NVL( bgtsection_patno,'1'))) AS lineitem_sponsoramount,
        LINEITEM_APPLYPATIENTCOUNT,
		to_number(to_char(trim(replace((select EVENTCOST_VALUE from SCH_EVENTCOST where SCH_EVENTCOST.fk_event in (select event_id from EVENT_ASSOC 
		Where chain_id = (select FK_PROTOCOL from sch_protocol_visit where PK_PROTOCOL_VISIT = NVL(sch_bgtsection.fk_visit,0))
		AND EVENT_ASSOC.fk_visit is null and NAME = sch_lineitem.LINEITEM_NAME)AND FK_COST_DESC = FK_CODELST_COST_TYPE),',','')),'999999999999999.99'))  AS LINEITEM_OTHERCOST,
        FK_CODELST_COST_TYPE,
        (SELECT codelst_desc
        FROM sch_codelst
        WHERE pk_codelst = FK_CODELST_COST_TYPE
        ) COST_TYPE_DESC,
        lineitem_seq,
        pk_budgetsec,
        fk_visit budgetsec_fkvisit,
        pk_lineitem,
        sch_lineitem.fk_event lineitem_fkevent,
        bgtcal_excldsocflag,
        BUDGET_COMBFLAG,
        (SELECT EVENT_TYPE
          FROM EVENT_ASSOC
          WHERE event_id= sch_lineitem.fk_event) AS event_type,
        FK_EVENT
      FROM sch_bgtcal,
        sch_budget,
        sch_bgtsection,
        sch_lineitem
      WHERE pk_budget                   = fk_budget
      AND pk_bgtcal                     = fk_bgtcal
      AND NVL (bgtsection_delflag, 'N') = 'N'
      AND pk_budgetsec                  = fk_bgtsection(+)
      AND NVL (lineitem_delflag, 'N')   = 'N'
      ))
      WHERE NVL(EVENT_TYPE,'A')         <> 'U'
    );
