<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<title><%=LC.L_Evt_Lib%><%-- Event Library*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*" %>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>
<script>
// commented for sorting on 04-07-2005
//function setOrder(formObj,orderBy,pgRight) //orderBy column number

//KM-01Sep09
function callAjaxGetCatDD(formobj){
	   selval = formobj.cmbLibType.value;
 	   new VELOS.ajaxObject("getCodeDropDown.jsp", {
   		urlData:"selectedVal="+selval+"&ddName=catId&codeType=L&ddType=category&from=All" ,
		   reqType:"POST",
		   outputElement: "span_catId" }

   ).startRequest();

}

//KM-Removed Mandatory check - #4254
/*function validate(formobj)
{
	if (!(validate_col('Event Library Type',formobj.cmbLibType))) return false
}*/

function setOrder(formObj,orderBy)
{
	var lorderType;
	if (formObj.orderType.value=="asc") {
		formObj.orderType.value= "desc";
		lorderType="desc";
	} else 	if (formObj.orderType.value=="desc") {
		formObj.orderType.value= "asc";
		lorderType="asc";
	}

	lsrc = formObj.srcmenu.value;
	lcurPage = formObj.page.value;

	formObj.action="eventlibrary.jsp?srcmenu="+lsrc+"&page="+lcurPage+"&orderBy="+orderBy+"&orderType="+lorderType;
	formObj.submit();
}
</Script>

<SCRIPT language="javascript">

function confirmBox(fileName,type,pgRight) {
	var msg="";
	if (f_check_perm(pgRight,'E') == true) {
		switch(type){
		case "L":
			var paramArray = [fileName];
			msg=getLocalizedMessageString("M_DelEvtCat_DelEvtCat",paramArray);/*msg="Delete '" + fileName + "' Event Category from Library?. This will delete all the Events in this Event Category.";*****/
		case "E":
			var paramArray = [fileName];
			msg=getLocalizedMessageString("L_Del_FrmLib",paramArray);/*msg="Delete '" + fileName + "' from Library?"*****/
	 	}

       	if (confirm(msg)) {
    	   return true;
	   	}else {
		   return false;
		}
	} else {
	   return false
	}
}

</SCRIPT>

<% String src;

src= request.getParameter("srcmenu");

%>

<jsp:include page="panel.jsp" flush="true">

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>

<body style="overflow:hidden;">

<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>



<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>

<jsp:useBean id="ctrldao" scope="request" class="com.velos.esch.business.common.EventdefDao"/>

<jsp:useBean id="ctrldao1" scope="request" class="com.velos.esch.business.common.EventdefDao"/>

<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>

<jsp:useBean id="assocdao" scope="request" class="com.velos.esch.business.common.EventAssocDao"/>

<jsp:useBean id="siteB" scope="request" class="com.velos.eres.web.site.SiteJB"/>


<%@ page language = "java" import = "com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.BrowserRows,com.velos.eres.service.util.Configuration,java.sql.*,com.velos.eres.service.util.StringUtil,com.velos.esch.business.common.*"%>



<jsp:include page="ui-include.jsp" flush="true"></jsp:include>

<% int pageRight = 0;

    HttpSession tSession = request.getSession(true);

   if (sessionmaint.isValidSession(tSession))
	{

	String sAcc = (String) tSession.getValue("accountId");
    //int accId = EJBUtil.stringToNum((String) (tSession.getValue("accountId")));
    int accId = EJBUtil.stringToNum(sAcc);
	String eventDefSql="";
	String countSql="";
	String pagenum = "";
	int curPage = 0;
	long startPage = 1;
	String stPage;
	long cntr = 0;

	pagenum = request.getParameter("page");

    if (pagenum == null)
    {
    	pagenum = "1";
    }

    curPage = EJBUtil.stringToNum(pagenum);

    String orderBy = "";
    orderBy = request.getParameter("orderBy");
    String orderType = "";
	String sortOrderType="";
    orderType = request.getParameter("orderType");

    if (orderType == null)
    {
    	orderType = "asc";
    }

//Added by Ganapathy on 04-07-2005 for sorting
	if (EJBUtil.isEmpty(orderBy) || (orderBy==null)){
				orderBy="CATEGORY";
			    sortOrderType="lower(CATEGORY) asc ,lower(EVENT_NAME) asc " ;
	} else {
	 		if (orderBy.equals("CATEGORY")){
			sortOrderType = "lower(CATEGORY) "+orderType+", lower(EVENT_NAME) asc " ;

		}
		else {
			sortOrderType = "lower(CATEGORY) asc , lower("+orderBy+") "+orderType ;

		}
	}

    String catId="";
    //catId = request.getParameter("catId");
    //if (catId==null) {catId="";}

%>



	<%
		String description="";
		String catDesc="";

		String protocolId="";

		String eventId="";

		String eventType="";

		String eventName="";
		String eventNameTrim="";

		String eventNameHide="";

		String calledFrom="";

		String calStatus="";

		String catName="";

		String categId = "";

		String libTypePullDn = "";

		String catDD = "";

		String cptCode = "";

		String notes ="";

		String eventcost ="";

		String additionalcode ="";

		String facilityname ="";//KV:SW-FIN2c

		int counter = 0;

		Integer id;

		String uName = (String) tSession.getValue("userName");

		String duration= request.getParameter("duration");

		protocolId= request.getParameter("protocolId");

		calledFrom= request.getParameter("calledFrom");

		String mode= request.getParameter("mode");

		calStatus= request.getParameter("calStatus");

		String categoryId= request.getParameter("catId");

		String tab= request.getParameter("selectedTab");

		String srcMenu           = request.getParameter("srcmenu");
		String eventNameandDesc  = request.getParameter("eventName");


	    categoryId = (categoryId == null)?"":categoryId;


		eventNameandDesc=(eventNameandDesc ==null) ? "" :eventNameandDesc;

		String cptCodeSearch = request.getParameter("cptCodeSearch");
		cptCodeSearch=(cptCodeSearch ==null) ? "" :cptCodeSearch;

		String addlCode = request.getParameter("addlCode");
		addlCode = (addlCode ==null) ? "" :addlCode;


		int libTypeId = 0;

		String libTypeSearch = request.getParameter("cmbLibType");

		//Rohit - SW-FIN2A
		int eventFacId = 0;
		String strEventFacilityId = "";
		eventFacId = EJBUtil.stringToNum(request.getParameter("ddFacid"));
		if (eventFacId == 0) {
			strEventFacilityId = "";
		}
		else {
			strEventFacilityId = "" + eventFacId;
		}

		int CostType = 0;
		String strCostType = "";
		CostType = EJBUtil.stringToNum(request.getParameter("costDesc"));
		if (CostType == 0) {
			strCostType = "";
		}
		else {
			strCostType = "" + CostType;
		}

		//KM-01Sep09

		SchCodeDao schDao = new SchCodeDao();
		schDao.getCodeValues("lib_type");


		//KM-#4297
		if(libTypeSearch == null) {
			libTypeId=0;
		}

		else
			libTypeId = EJBUtil.stringToNum(libTypeSearch);


		libTypePullDn = schDao.toPullDown("cmbLibType",libTypeId," onChange=callAjaxGetCatDD(document.eventlibrary);");


		ArrayList eventIds= null;

		ArrayList names= null;

		ArrayList descriptions= null;

		ArrayList event_types= null;

		if (calledFrom.equals("S")) {

   	   	   StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");

	  	   if ((stdRights.getFtrRights().size()) == 0){

			 	pageRight= 0;

		   }else{

				pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));

	   	   }


		} else if (calledFrom.equals("P")) {

				GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");

				pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));
		}

		else if(calledFrom.equals("L")){

				GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");

				pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("EVLIB"));

		}

	    String searchClause = "";
		String fromClause = " ";

        if (!StringUtil.isEmpty(eventNameandDesc)){
              searchClause = " AND ( LOWER(DESCRIPTION) LIKE LOWER('%"+eventNameandDesc.trim()+"%') OR LOWER(NAME) LIKE LOWER('%"+eventNameandDesc.trim()+"%') OR LOWER(NOTES) LIKE LOWER('%"+eventNameandDesc.trim()+"%') )";
        }

		if (!StringUtil.isEmpty(cptCodeSearch)) {
			searchClause = searchClause + " AND (LOWER(EVENT_CPTCODE) LIKE LOWER('%"+cptCodeSearch.trim()+"%')) ";
		}
		//Rohit - SW-FIN2A
		if (!StringUtil.isEmpty(strEventFacilityId)) {
			searchClause = searchClause + " AND (LOWER(FACILITY_ID) LIKE LOWER('%"+strEventFacilityId+"%')) ";
		}

		if (!StringUtil.isEmpty(strCostType)) {
			searchClause = searchClause + " AND (LOWER(fk_cost_desc) LIKE LOWER('%"+strCostType+"%')) and fk_event = event_id ";
			fromClause = fromClause + " , sch_eventcost ";

		}

		if (!StringUtil.isEmpty(addlCode)) {

			searchClause = searchClause + " AND (LOWER(md_modelementdata) LIKE LOWER('%"+addlCode.trim()+"%')) and fk_modpk = event_id and md_modname = 'evtaddlcode' ";
			fromClause = fromClause + " , er_moredetails ";
		}

		searchClause = searchClause + " AND EVENT_LIBRARY_TYPE = "+libTypeId +" ";

		//KV:SW-FIN2c
	//when "all" category is selected
      if (categoryId.equals("") || categoryId.equals(" ")){
         eventDefSql="SELECT distinct EVENT_ID, case when length(DESCRIPTION) > 100 then substr(DESCRIPTION,1,100) || '...' else DESCRIPTION end as DESCRIPTION, chain_id category_id,EVENT_TYPE, NAME event_name ,(select NAME from EVENT_DEF a where a.event_id = b.chain_id and TRIM(UPPER(EVENT_TYPE)) = 'L' ) category, (select DESCRIPTION from EVENT_DEF a where a.event_id = b.chain_id and TRIM(UPPER(EVENT_TYPE)) = 'L' ) category_desc,event_cptcode, case when length(notes) > 100 then substr(notes,1,100) || '...' else notes end as notes, lst_cost(event_id) as eventcost, lst_additionalcode(event_id,'evtaddlcode') as additionalcode,(select site_name from er_site s  where s.pk_site = b.FACILITY_ID) as Facilityname FROM EVENT_DEF b ";
   		// if (!StringUtil.isEmpty(addlCode)) {
				eventDefSql= eventDefSql + fromClause;
		// }


		 eventDefSql= eventDefSql + " WHERE TRIM(USER_ID)= "+accId+" AND TRIM(UPPER(EVENT_TYPE)) IN ('E','L')" ;
      // commented for sorting on 04-07-2005
      //ORDER BY CHAIN_ID";
         countSql="select count(*) from (SELECT distinct event_id FROM EVENT_DEF ";
		 //if (!StringUtil.isEmpty(addlCode)) {
			 countSql=countSql + fromClause;
		// }
		 countSql= countSql + " WHERE  TRIM(USER_ID)= "+accId+" AND TRIM(UPPER(EVENT_TYPE)) IN('L','E')";
   }
    //KV:SW-FIN2c
   //when category is selected
   else{
       eventDefSql="SELECT distinct EVENT_ID, case when length(DESCRIPTION) > 100 then substr(DESCRIPTION,1,100) || '...' else DESCRIPTION end as DESCRIPTION, chain_id category_id,EVENT_TYPE, NAME event_name ,(select NAME from EVENT_DEF a where a.event_id = b.chain_id and TRIM(UPPER(EVENT_TYPE)) = 'L' ) category, (select DESCRIPTION from EVENT_DEF a where a.event_id = b.chain_id and TRIM(UPPER(EVENT_TYPE)) = 'L' ) category_desc,event_cptcode, case when length(notes) > 100 then substr(notes,1,100) || '...' else notes end as notes , lst_cost(event_id) as eventcost,  lst_additionalcode(event_id,'evtaddlcode') as additionalcode,(select site_name from er_site s  where s.pk_site = b.FACILITY_ID) as Facilityname FROM EVENT_DEF b ";
	   // if (!StringUtil.isEmpty(addlCode)) {

			eventDefSql= eventDefSql + fromClause;
		// }
	   eventDefSql= eventDefSql + " WHERE CHAIN_ID= "+categoryId+" AND TRIM(UPPER(EVENT_TYPE)) IN ('E','L')" ;
      // commented for sorting on 04-07-2005
      //ORDER BY CHAIN_ID";
       countSql="select count(*) from (SELECT distinct event_id FROM EVENT_DEF ";
	   //if (!StringUtil.isEmpty(addlCode)) {
			 countSql=countSql + fromClause;
		// }
	   countSql= countSql + " WHERE  CHAIN_ID= "+categoryId+" and TRIM(UPPER(EVENT_TYPE)) IN('L','E') ";

   }

    eventDefSql = eventDefSql + searchClause ;
    countSql = countSql + searchClause +" )";
   	   long rowsPerPage=0;
   	   long totalPages=0;
	   long rowsReturned = 0;
	   long showPages = 0;
	   long firstRec = 0;
	   long lastRec = 0;
	   long totalRows = 0;
	   boolean hasMore = false;
	   boolean hasPrevious = false;




   	   rowsPerPage = Configuration.MOREBROWSERROWS;
	   totalPages = Configuration.PAGEPERBROWSER ;
	   BrowserRows br = new BrowserRows();

//	   br.getPageRows(curPage,rowsPerPage,eventDefSql,totalPages,countSql,orderBy,"null");
          //Modified by Ganapathy for sorting on 04-07-2005

       if ( libTypeId!=0 ){
		br.getPageRows(curPage,rowsPerPage,eventDefSql,totalPages,countSql,sortOrderType,"");
   	   	rowsReturned = br.getRowReturned();
	   	showPages = br.getShowPages();
	   	startPage = br.getStartPage();
	   	hasMore = br.getHasMore();
	   	hasPrevious = br.getHasPrevious();
	   	totalRows = br.getTotalRows();
	   	firstRec = br.getFirstRec();
	   	lastRec = br.getLastRec();
       }

if (pageRight > 0 )
   		{

	%>
<DIV class="tabDefTopN" id="div1">
			<jsp:include page="librarytabs.jsp" flush="true">
			<jsp:param name="calledFrom" value="<%=calledFrom%>"/>
			</jsp:include>
</div>

<DIV class="tabDefBotN" id="div2">
<!--KM-#4254-->
		<Form name="eventlibrary" action="eventlibrary.jsp?page=1" method="post" >

			    <%

				//KM-01Sep09-- changes for category dropdown
				ctrldao1= eventdefB.getHeaderList(accId,"L",libTypeId);
				ArrayList catIds = ctrldao1.getEvent_ids();
				ArrayList catNames = ctrldao1.getNames();

				int length = catIds.size();
				//Rohit-SW-FIN2A
				String ddcost;
				SchCodeDao cdesc = new SchCodeDao();
				cdesc.getCodeValues("cost_desc", EJBUtil.stringToNum(sAcc));
				//ddcost = cdesc.toPullDown("costDesc",CostType,"");
				//ddcost = StringUtil.replace(ddcost,"Select an Option","ALL");

				//KM-#4976
				StringBuffer sbCost = new StringBuffer();
				ArrayList costIds = cdesc.getCId();
				ArrayList costDescs = cdesc.getCDesc();
				int inCostType = 0;
				sbCost.append("<SELECT NAME='costDesc' >") ;
				sbCost.append("<option selected value='' >"+LC.L_All+"</option>") ;/*sbCost.append("<option selected value='' >All</option>") ;*****/
				if (costIds.size() > 0)
				{
					for (int count = 0; count < costIds.size(); count++)
					{
						inCostType = ((Integer)costIds.get(count)).intValue();
						if(inCostType == CostType){
							sbCost.append("<OPTION value = "+ inCostType+" selected>" + costDescs.get(count)+ "</OPTION>");
						}else{
						sbCost.append("<OPTION value = "+ inCostType+">" + costDescs.get(count)+ "</OPTION>");
						}

					}
				}
				sbCost.append("</SELECT>");
				ddcost  = sbCost.toString();

				//Rohit-SW-FIN2A
				CodeDao codeDao = new CodeDao();
				codeDao.getCodeValues("site_type","service");
				ArrayList siteType = new ArrayList();
				siteType = codeDao.getCId();
				String strSiteType = EJBUtil.ArrayListToString(siteType);
				SiteDao siteDao = siteB.getBySiteTypeCodeId(accId,strSiteType);
				String ddFacility ="";
				//ddFacility=siteDao.toPullDown("ddFacid",eventFacId,"");
       			//ddFacility = StringUtil.replace(ddFacility,"Select an Option","ALL");

				//KM-#4976
				StringBuffer sbFacility = new StringBuffer();
				ArrayList facIds = siteDao.getSiteIds();
				ArrayList facDescs = siteDao.getSiteNames();//JM: 14Jul2010: #5135
				int inFacType = 0;
				sbFacility.append("<SELECT NAME='ddFacid' >") ;
				sbFacility.append("<option selected value='' >"+LC.L_All+"</option>") ;/*sbFacility.append("<option selected value='' >All</option>") ;*****/
				if (facIds.size() > 0)
				{
					for (int count = 0; count < facIds.size(); count++)
					{
						inFacType = ((Integer)facIds.get(count)).intValue();
						if(inFacType == eventFacId){
							sbFacility.append("<OPTION value = "+ inFacType+" selected>" + facDescs.get(count)+ "</OPTION>");
						}else{
						sbFacility.append("<OPTION value = "+ inFacType+">" + facDescs.get(count)+ "</OPTION>");
						}

					}
				}
				sbFacility.append("</SELECT>");
				ddFacility  = sbFacility.toString();
				%>
<div class="tmpHeight"></div>				
<table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl midAlign" >
      <tr >
      <!--Virendra: Fixed Bug No. 4979, Added width in <td> and space(&nbsp) in labels  -->
		<td width="19%"><b><%=LC.L_Search_By%><%-- Search By*****--%></b></td>
		<td width="9%"></td>
		<td width="17%"></td>
		<td width="9%"></td>
		<td width="17%"></td>
		<td width="9%"></td>
		<td width="10%"></td>
	  </tr>
	  <tr >
	  	<td align="right"><%=LC.L_Evt_Lib%><%-- Event Library Type*****--%>:&nbsp</td> <td> <%=libTypePullDn%> </td>
		<td align="right"><%=LC.L_Cost_Type%><%-- Cost Type*****--%>:&nbsp</td> <td><%=ddcost%></td>
		<td  align="right"><%=LC.L_Evt_Cat%><%-- Event Category*****--%>:&nbsp</td>


		<td > <span id = "span_catId">	<select  name="catId">
						<option value=" " Selected><%=LC.L_All%><%-- All*****--%> </option>

						<%

						for(int i = 0; i< length;i++)

						{

							if (categoryId.equals(catIds.get(i).toString())){

							%>

								<option value="<%=catIds.get(i)%>" Selected> <%=catNames.get(i)%> </option>

							<% }else{%>

							   <option value="<%=catIds.get(i)%>"> <%=catNames.get(i)%> </option>

							<% }

						}

						%>

					</select>  </span>



		<%-- <td width="%">	<span id = "span_catId"> <%=catId%> </span> </td> --%>


					</td>
						<td></td>

	</tr>
	<tr >
	<!--Virendra: Fixed Bug No. 4979, Removed spaces for proper alignment of labels  -->
	 <td  align="right"> <%=LC.L_Cpt_Code%><%-- CPT Code*****--%>:&nbsp</td> <td> <input type="text" name="cptCodeSearch" value="<%=cptCodeSearch%>" size=17 MAXLENGTH = 50> </td>
	 <td  align="right"> <%=LC.L_Addl_Codes%><%-- Additional Codes*****--%>:&nbsp </td> <td> <input type="text" name="addlCode" value="<%=addlCode%>" size=17 MAXLENGTH = 50> </td>

 	 <td  align="right"><%=MC.M_EvtName_DescOrNotes%><%-- Event Name, Description or Notes*****--%>:&nbsp</td>
	 <td ><input type="text" name="eventName" value="<%=eventNameandDesc%>" size=17 MAXLENGTH = 50></td>
	 <td></td>
	</tr>
	<tr >
		<td align="right"><%=LC.L_Facility%><%-- Facility*****--%>:&nbsp</td>

		<td><%=ddFacility%></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	<td ><button type="submit"><%=LC.L_Search%></button></td>
					</tr>
					</table>

				<P></P>

			  	<table width="98%" cellspacing="1" cellpadding="0" border="0" >

				    <tr >

					    <td width = "70%">

				    	    <P class = "sectionHeadingsFrm"><%=LC.L_Lib_Events%><%-- Library Events*****--%></P>

					    </td>

					    <td width = "30%" align="right">

					        <p> <A href="category.jsp?mode=<%=mode%>&catMode=N&srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>" onClick="return f_check_perm(<%=pageRight%>,'N')"><img src="../images/jpg/AddNewCategory.gif" title="<%=MC.M_New_EventCategory%><%--New Event Category*****--%>" border="0"/></A> </p>

					    </td>

				    </tr>

				</table>
			    <input type="hidden" name="srcmenu" value="<%=src%>">

			    <Input name="duration" type="hidden" value=<%=duration%>>

			    <Input name="protocolId" type="hidden" value=<%=protocolId%>>

		    	<Input name="calledFrom" type="hidden" value=<%=calledFrom%>>

			    <Input name="mode" type="hidden" value=<%=mode%>>

			    <Input name="calStatus" type="hidden" value=<%=calStatus%>>

			    <Input name="selectedTab" type="hidden" value=<%=tab%>>



				<Input type="hidden" name="page" value="<%=curPage%>">

				<Input type="hidden" name="orderBy" value="<%=orderBy%>">

				<Input type="hidden" name="orderType" value="<%=orderType%>">
		<% if(libTypeId==0){%>
				<p><font class="recNumber"><%=MC.M_EntEvSearchCrt %></font></p>
		<%}else{ %>
			    <table class="outline midAlign" width="99%" cellpadding="0" cellspacing="0" >

				    <tr>
<!-- Modified by Ganapathy for Column sorting 04-07-2005 -->
				<!-- KV:BugNo:4980-->
				        <th width="10%" onclick="setOrder(document.eventlibrary,'CATEGORY')"> <%=LC.L_Category%><%-- Category*****--%> &loz;</th>

						<th width="10%" onclick="setOrder(document.eventlibrary,'event_cptcode')"> <%=LC.L_Cpt_Code%><%-- CPT code*****--%> &loz;</th>

				        <th width="15%" onclick="setOrder(document.eventlibrary,'EVENT_NAME')"> <%=LC.L_Event%><%-- Event*****--%> &loz;</th>

				        <th width="15%" onclick="setOrder(document.eventlibrary,'DESCRIPTION')"> <%=LC.L_Description%><%-- Description*****--%> &loz;</th>
						<th width="15%" onClick="setOrder(document.eventlibrary,'lower(NOTES)')"> <%=LC.L_EventNotes%><%-- Notes*****--%> &loz;</th>
						<th width="15%" onClick="setOrder(document.eventlibrary,'lower(eventcost)')"> <%=LC.L_Cost%><%-- Cost*****--%> &loz;</th>
						<th width="15%" onClick="setOrder(document.eventlibrary,'lower(additionalcode)')"> <%=LC.L_Addl_Codes%><%-- Additional codes*****--%> &loz; </th>
						<!-- KV:SW-FIN2c -->
						<th width="15%" onClick="setOrder(document.eventlibrary,'lower(Facilityname)')"> <%=LC.L_Facility%><%-- Facility*****--%> &loz;</th>
						<th width="5%"> <%=LC.L_Delete%><%--Delete*****--%></th>

					</tr>

					<%

					String previousEventName="";
					String preEventName="";
					String tempCatId ="";
						int eventCounter = 0 ;

			for(counter =1;counter<=rowsReturned;counter++)

					{
						description =  br.getBValues(counter,"DESCRIPTION");
						description = (  description==null )?"-":(  description ) ;
						eventType = br.getBValues(counter,"EVENT_TYPE");

						eventId = br.getBValues(counter,"EVENT_ID");

						catName = br.getBValues(counter,"category");
						catDesc = br.getBValues(counter,"category_desc");
						catDesc = (  catDesc==null )?"-":(  catDesc ) ;
						categId = br.getBValues(counter,"category_id");

						eventName = br.getBValues(counter,"EVENT_NAME");
						//Changed by PK: Bug#4277
						eventName = (  eventName==null )?"-":(  eventName ) ;
						eventNameTrim= eventName;
						if(eventNameTrim.length()>50){
							eventNameTrim = eventNameTrim.substring(0,50);
							eventNameTrim +="...";
						}

						eventNameHide="event_name"+eventId;

						cptCode = br.getBValues(counter,"event_cptcode");
						cptCode = (  cptCode==null )?"-":(  cptCode ) ;
						notes = br.getBValues(counter,"notes");
						notes = (  notes==null )?"-":(  notes ) ;
						eventcost = br.getBValues(counter,"eventcost");
						eventcost = (  eventcost==null )?"-":(  eventcost ) ;
						additionalcode = br.getBValues(counter,"additionalcode");
						additionalcode = (  additionalcode==null )?"-":(  additionalcode ) ;
						//KV: SW_FIN2c
						facilityname = br.getBValues(counter,"Facilityname");
						facilityname = (  facilityname==null )?"-":(  facilityname ) ;


						if(counter!=1)
						{
							tempCatId = br.getBValues(counter-1,"category_id");
						}


						if(!tempCatId.equals(categId))
							{%>
							<tr height="15">
								<td><A href = "category.jsp?mode=M&srcmenu=<%=src%>&catMode=M&categoryId=<%=categId%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>" onClick="return f_check_perm(<%=pageRight%>,'E')"><%=catName%></A> </td>
							<td></td>
							<td align=right>
							<A href = "eventdetails.jsp?srcmenu=<%=src%>&eventmode=N&categoryId=<%=categId%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&fromPage=eventlibrary&selectedTab=1" onClick="return f_check_perm(<%=pageRight%>,'N')">
							<img src="./images/AddNewEvent.gif" border="0" title="<%=LC.L_NewEvent%>"></A>&nbsp;&nbsp;&nbsp;&nbsp;</td>

							<td><%=catDesc%></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td align="center"><A  href="eventdelete.jsp?eventId=<%=categId%>&eventType=L&srcmenu=<%=src%>&selectedTab=2&duration=2&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>" onClick="return confirmBox('<%=catName%>','L',<%=pageRight%>);" ><img src="./images/delete.gif" title="<%=LC.L_Delete%>" alt="<%=LC.L_Delete%>" border="0"/></A></td>
						</tr>
							<%}

  		         if (eventType.equals("E")) {
				 	    eventCounter = eventCounter + 1 ;
						if ((eventCounter%2)==0 ) {

						%>
					      	<tr class="browserEvenRow">
						<%
						}else {

						%>
				      		<tr class="browserOddRow">
						<%}%>

							<td>&nbsp;</td>


						<%

							String param = "srcmenu=" + src + "&eventmode=M&eventId=" + eventId + "&duration=" + duration + "&protocolId=" + protocolId + "&calledFrom=" + calledFrom + "&mode=" + mode + "&calStatus=" + calStatus + "&fromPage=eventlibrary&selectedTab=1";

							%>

  		   		   <td><%=cptCode%></td>
  		   		   <!-- Changed by PK: Bug#4277 -->
  		   		    <%--Modified By Yogendra Pratap Singh : Bug#6436 --%>
					<td><%--Modified By Tarun Kumar : Bug#10759 --%><A href ="eventdetails.jsp?srcmenu=<%=src%>&eventmode=M&eventId=<%=eventId%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&fromPage=eventlibrary&selectedTab=1" onmouseover="return overlib(htmlEncode('<%=eventName%>'),CAPTION,'<%=LC.L_Event_Name%><%--<%=LC.Event_Name%> Name*****--%>');" onmouseout="return nd();" ><%=eventNameTrim%></A> </td>

							<INPUT name=<%=eventNameHide%> type=hidden value="<%=eventName%>">
							<td><%=description%></td>
							<td><%=notes%></td>
							<td><%=eventcost%></td>
							<td><%=additionalcode%></td>
							<!-- KV:SW-FIN2c -->
							<td><%=facilityname%></td>
						<td align="center"><A  href="eventdelete.jsp?eventId=<%=eventId%>&eventType=<%=eventType%>&srcmenu=<%=src%>&selectedTab=2&duration=2&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&eventName=<%=eventNameandDesc%>" onClick="return confirmBox('<%=eventName%>','<%=eventType%>',<%=pageRight%>);" ><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0"/></A></td>

			    	 </tr>

					<%

					}

					} //eventType = "E"

					%>

				</table>
	<!-- Bug#9751 16-May-2012 Ankit -->			
	<table width="98%" cellspacing="0" cellpadding="0" class="midalign">
		<tr valign="top"><td>
		<% if (totalRows > 0)
		{ Object[] arguments = {firstRec,lastRec,totalRows};
		%>
			<font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%></font><%-- <font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s)</font>*****--%>
		<% }else{%>
		<font class="recNumber"><%=MC.M_NoRecordsFound%><%-- No Records Found*****--%></font>
		<%} %>
	   </td></tr>
	</table>
	<%} %>		
	<div align="center" class="midalign">
	<%
		if (curPage==1) startPage=1;


	    for ( counter = 1; counter <= showPages;counter++)
		{


   			cntr = (startPage - 1) + counter;

	 		if ((counter == 1) && (hasPrevious))
			{
			  %>


			  	<A href="eventlibrary.jsp?selectedTab=2&mode=<%=mode%>&catId=<%=categoryId%>&eventId=<%=eventId%>&duration=<%=duration%>&srcmenu=<%=src%>&protocolId=<%=protocolId%>&fromPage=eventlibrary&calledFrom=<%=calledFrom%>&page=<%=cntr-1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&eventName=<%=eventNameandDesc%>&cmbLibType=<%=libTypeSearch%>&cptCodeSearch=<%=cptCodeSearch%>&addlCode=<%=addlCode%>&ddFacid=<%=strEventFacilityId%>&costDesc=<%=strCostType%>">< <%=LC.L_Previous%><%-- Previous*****--%> <%=totalPages%>> </A> <!--KM-05Aug09 -->
				&nbsp;&nbsp;&nbsp;&nbsp;

				<%
  			}	%>

		<%
 		 if (curPage  == cntr)
		 {
	    	 %>
				<FONT class = "pageNumber"><%=cntr%></Font>
       		<%
       	}else
        {	if(categoryId=="")
			{
			categoryId=" ";
			}%>
		<A href="eventlibrary.jsp?selectedTab=2&mode=<%=mode%>&catId=<%=categoryId%>&eventId=<%=eventId%>&duration=<%=duration%>&srcmenu=<%=src%>&protocolId=<%=protocolId%>&fromPage=eventlibrary&calledFrom=<%=calledFrom%>&page=<%=cntr%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&eventName=<%=eventNameandDesc%>&cmbLibType=<%=libTypeSearch%>&cptCodeSearch=<%=cptCodeSearch%>&addlCode=<%=addlCode%>&ddFacid=<%=strEventFacilityId%>&costDesc=<%=strCostType%>"><%=cntr%></A>

       <%}%>

		<%	}
		if (hasMore)
		{   %>
	   &nbsp;&nbsp;&nbsp;&nbsp;
	   <A href="eventlibrary.jsp?selectedTab=2&mode=<%=mode%>&catId=<%=categoryId%>&eventId=<%=eventId%>&duration=<%=duration%>&srcmenu=<%=src%>&protocolId=<%=protocolId%>&fromPage=eventlibrary&calledFrom=<%=calledFrom%>&page=<%=cntr+1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&eventName=<%=eventNameandDesc%>&cmbLibType=<%=libTypeSearch%>&cptCodeSearch=<%=cptCodeSearch%>&addlCode=<%=addlCode%>&ddFacid=<%=strEventFacilityId%>&costDesc=<%=strCostType%>">< <%=LC.L_Next%><%-- Next*****--%> <%=totalPages%>></A>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<%}%>
	</div>
				<br>



			</Form>

<jsp:include page="paramHide.jsp" flush="true"/>

		<%



		} //end of if body for page right

		else

		{

		%>

			<jsp:include page="accessdenied.jsp" flush="true"/>

		<%

		} //end of else body for page right

	}//end of if body for session

	else

	{

	%>

		<jsp:include page="timeout.html" flush="true"/>

	<%

	}

	%>

	<div>

    	<jsp:include page="bottompanel.jsp" flush="true"/>

  	</div>

</div>

<div class ="mainMenu" id = "emenu">

  <jsp:include page="getmenu.jsp" flush="true"/>

</div>

</body>

</html>


