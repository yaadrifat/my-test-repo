<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<html>

<head>

<title><%=LC.L_Evt_DetsPage%><%-- Event Details Page*****--%></title>



<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.aithent.file.uploadDownload.*,com.velos.eres.service.util.StringUtil" %>
<%@ page import="com.velos.eres.service.util.VelosResourceBundle" %>
<%@ page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC" %>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
	<%-- Nicholas : Start --%>
	<jsp:include page="skinChoser.jsp" flush="true"/>
	<%-- Nicholas : End --%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.esch.business.common.EventInfoDao,com.velos.esch.business.common.*"%>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="codelstB" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id="crflibB" scope="request" class="com.velos.esch.web.crflib.CrflibJB"/>
<jsp:useBean id="grpRights" scope="request" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="eventuser" scope="request" class="com.velos.esch.web.eventuser.EventuserJB"/>
<jsp:useBean id="eventcrf" scope="request" class="com.velos.esch.web.eventCrf.EventCrfJB"/>
<jsp:useBean id="siteB" scope="request" class="com.velos.eres.web.site.SiteJB"/>



<% String src;
src= request.getParameter("srcmenu");
%>
<body style="overflow:scroll;">
<%--DIV class="formDefault" id="div1"--%>
<%
//	String protocolId=request.getParameter("protocolId");
//	String calledFrom="S";
	String calledFrom=request.getParameter("calledFrom");
	String categoryId = "";
	String eventId = "";
	String eventName = "";
	String eventDesc = "";
	// Added by gopu to fix the Bugzilla Issue #2347 on 03/07/06
	String eventCpt = "";
	String eventDuration = "";
	String eventDurDays = "";
	String eventFuzzyPerDays = "";
	String eventFuzzyPerDaysA = "";
	String eventNotes = "";

	int eventSOSId = 0;
	int eventFacilityId = 0;
	int eventCoverTypeId =0;

	String eventSOS ="";
	String eventFacility ="";
	String eventCoverageType ="";

	String eventMsgFlag = "0";
	String eventMsg = "";
	String eventMsgTo = "";
	String eventMsgDays = "";
	int count = 0;
///for message data
	String eventMsgDaysBpat = "";
  	String eventMsgBpat = "";
	String eventMsgDaysApat = "";
  	String eventMsgApat = "";
	String eventMsgDaysBuser = "";
	String eventMsgBuser = "";
	String eventMsgDaysAuser = "";
	String eventMsgAuser = "";


	String eventDurationUnit="";

//	String protName = request.getParameter("protName");
String protName="dinesh";

	HttpSession tSession = request.getSession(true);

	if (sessionmaint.isValidSession(tSession))
	{
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
		String modRight = (String) tSession.getValue("modRight");
		int modlen = modRight.length();
		ctrl.getControlValues("module");
		ArrayList feature =  ctrl.getCValue();
		ArrayList ftrDesc = ctrl.getCDesc();
		ArrayList ftrSeq = ctrl.getCSeq();
		ArrayList ftrRight = new ArrayList();
		String strR;
		for (int counter = 0; counter <= (modlen - 1);counter ++) {
			strR = String.valueOf(modRight.charAt(counter));
			ftrRight.add(strR);
		}

		grpRights.setGrSeq(ftrSeq);
		grpRights.setFtrRights(ftrRight);
		grpRights.setGrValue(feature);
		grpRights.setGrDesc(ftrDesc);
		int eptRight = 0;
		eptRight  = Integer.parseInt(grpRights.getFtrRightsByValue("MODEPT"));
		String uName = (String) tSession.getValue("userName");

		String dnld;
		Configuration.readSettings("sch");
		Configuration.readUploadDownloadParam(Configuration.FILE_UPLOAD_DOWNLOAD + "fileUploadDownload.xml", "eventapndx");
		dnld=Configuration.DOWNLOADSERVLET ;
		String modDnld = "";

		   eventId = request.getParameter("eventId");

		//Added by Gopu to fix the Bugzilla issue #2347
		String eventDurationUnitBefore = "";
		String eventDurationUnitAfter	="";

		   if (calledFrom.equals("P")){

  		  	eventdefB.setEvent_id(EJBUtil.stringToNum(eventId));
			eventdefB.getEventdefDetails();
		    eventName = eventdefB.getName();
   		   	eventDesc = eventdefB.getDescription();
			// Added by gopu to fix the Bugzilla Issue #2347 on 03/07/06
			eventCpt = eventdefB.getCptCode();
			eventNotes = eventdefB.getNotes();

			eventSOSId = EJBUtil.stringToNum(eventdefB.getEventSOSId());
			eventFacilityId = EJBUtil.stringToNum(eventdefB.getEventFacilityId());
			eventCoverTypeId = EJBUtil.stringToNum(eventdefB.getEventCoverageType());

			eventDuration = eventdefB.getDuration();

			// Added by gopu to fix the Bugzilla Issue #2347 on 03/07/06
			eventDurationUnit = eventdefB.getDurationUnit();
			eventDurationUnitBefore = eventdefB.getDurationUnitBefore();
			eventDurationUnitAfter = eventdefB.getDurationUnitAfter();

	   	    eventFuzzyPerDays = eventdefB.getFuzzy_period();

			//KM-#3922
			if (eventFuzzyPerDays==null) eventFuzzyPerDays="0";
			    eventFuzzyPerDaysA=eventdefB.getFuzzyAfter();
			if (eventFuzzyPerDaysA==null) eventFuzzyPerDaysA="0";

			eventMsgTo = eventdefB.getMsg_to();
		    eventMsg = eventdefB.getEvent_msg();
		    eventMsgDays = eventdefB.getLinked_uri();

			//data for message
			eventMsgDaysBpat = eventdefB.getPatDaysBefore();
		    eventMsgDaysApat = eventdefB.getPatDaysAfter();
  		    eventMsgDaysBuser = eventdefB.getUsrDaysBefore();
		    eventMsgDaysAuser = eventdefB.getUsrDaysAfter();
			eventMsgBpat = eventdefB.getPatMsgBefore();
  	   	    eventMsgApat = eventdefB.getPatMsgAfter();
  	   	    eventMsgBuser = eventdefB.getUsrMsgBefore();
  	   	    eventMsgAuser = eventdefB.getUsrMsgAfter();

		   }else{

				eventassocB.setEvent_id(EJBUtil.stringToNum(eventId));
				eventassocB.getEventAssocDetails();
				eventName = eventassocB.getName();
				eventDesc = eventassocB.getDescription();
				// Added by gopu to fix the Bugzilla Issue #2347 on 03/07/06
				eventCpt = eventassocB.getCptCode();
				eventNotes = eventassocB.getNotes();

				eventSOSId = EJBUtil.stringToNum(eventassocB.getEventSOSId());
				eventFacilityId = EJBUtil.stringToNum(eventassocB.getEventFacilityId());
				eventCoverTypeId = EJBUtil.stringToNum(eventassocB.getEventCoverageType());

			   eventDuration = eventassocB.getDuration();
			   	// Added by gopu to fix the Bugzilla Issue #2347 on 03/07/06
			   eventDurationUnit = eventassocB.getDurationUnit();
			   eventDurationUnitBefore = eventassocB.getDurationUnitBefore(); //
			   eventDurationUnitAfter = eventassocB.getDurationUnitAfter(); //

	   		   eventFuzzyPerDays = eventassocB.getFuzzy_period();
  		      	// Added by gopu to fix the Bugzilla Issue #2347 on 03/07/06
				if (eventFuzzyPerDays==null) eventFuzzyPerDays="0";//
			   eventFuzzyPerDaysA=eventassocB.getFuzzyAfter();
				if (eventFuzzyPerDaysA==null) eventFuzzyPerDaysA="0";//

			   eventMsgTo = eventassocB.getMsg_to();
			   eventMsg = eventassocB.getEvent_msg();
			   eventMsgDays = eventassocB.getLinked_uri();
				//message
			   eventMsgDaysBpat = eventassocB.getPatDaysBefore();
	           eventMsgDaysApat = eventassocB.getPatDaysAfter();
  		       eventMsgDaysBuser = eventassocB.getUsrDaysBefore();
		       eventMsgDaysAuser = eventassocB.getUsrDaysAfter();
			   eventMsgBpat = eventassocB.getPatMsgBefore();
	       	   eventMsgApat = eventassocB.getPatMsgAfter();
 	   	       eventMsgBuser = eventassocB.getUsrMsgBefore();
  	   	       eventMsgAuser = eventassocB.getUsrMsgAfter();
		}

		if (eventSOSId !=0){
			siteB.setSiteId(eventSOSId);
			siteB.getSiteDetails();
			eventSOS = siteB.getSiteName();
		} else {
			eventSOS = "--";
		}

		if (eventFacilityId !=0){
			siteB.setSiteId(eventFacilityId);
			siteB.getSiteDetails();
			eventFacility = siteB.getSiteName();
		} else {
			eventFacility ="--";
		}

		SchCodeDao schdao = new SchCodeDao();
		eventCoverageType = schdao.getCodeDescription(eventCoverTypeId);
		eventCoverageType= eventCoverageType == null?"--":eventCoverageType;

		if(eventMsgDaysBpat == null || eventMsgDaysBpat .equals("")) {
		eventMsgDaysBpat="--"; }
		if(eventMsgDaysApat == null || eventMsgDaysApat .equals("")) {
		eventMsgDaysApat ="--"; }
		if(eventMsgDaysBuser == null || eventMsgDaysBuser .equals("")) {
		eventMsgDaysBuser ="--"; }
		if(eventMsgDaysAuser == null || eventMsgDaysAuser .equals("")) {
		eventMsgDaysAuser ="--"; }

////////////

		if(eventMsgBpat == null || eventMsgBpat .equals("")) {
		eventMsgBpat ="--"; }
		if(eventMsgApat == null || eventMsgApat .equals("")) {
		eventMsgApat ="--"; }
		if(eventMsgBuser == null || eventMsgBuser .equals("")) {
		eventMsgBuser ="--"; }
		if(eventMsgAuser == null || eventMsgAuser .equals("")) {
		eventMsgAuser ="--"; }
		if(eventNotes == null || eventNotes.equals("")) {	eventNotes ="--"; }
		if(eventDesc == null || eventDesc.equals("")) {	eventDesc ="--"; }
		// Added by gopu to fix the Bugzilla Issue #2347 on 03/07/06
		if(eventCpt == null || eventCpt.equals("")) {	eventCpt ="--"; }


		// get CRF Forms

		CrfDao efCRfDaoForms = new CrfDao();
		CrfDao efCRfDaoOther = new CrfDao();

		efCRfDaoForms = eventcrf.getEventCrfForms(EJBUtil.stringToNum(eventId),"PS_SP_PR");
		efCRfDaoOther = eventcrf.getEventCrfForms(EJBUtil.stringToNum(eventId),"link");

		ArrayList crfFormNames = new ArrayList();
		ArrayList crfFormTypes = new ArrayList();

		ArrayList otherCRFLinks = new ArrayList();

		crfFormNames = efCRfDaoForms.getFormName();
		crfFormTypes = efCRfDaoForms.getCrfFormType();

		otherCRFLinks = efCRfDaoOther.getOtherLinks();

		String crfFormName = "";
		String crfOtherLink = "";
		String crfFormTypeName = "";
		String crfFormType ="";

	   EventInfoDao eventinfoDao = crflibB.getEventCrfs(EJBUtil.stringToNum(eventId));
	   ArrayList crflibIds= eventinfoDao.getCrflibIds() ;
	   ArrayList crflibNumbers= eventinfoDao.getCrflibNumbers() ;
	   ArrayList crflibNames = eventinfoDao.getCrflibNames();
	   String crflibId= "";
	   String crflibNumber= "";
	   String crflibName = "";

			if(eventMsg == null || eventMsg.equals("") || eventMsg.length() == 0) {
			eventMsgFlag = "0";
		   } else {
			eventMsgFlag = "1";
		   }

	 eventinfoDao = eventdefB.getEventDefInfo(EJBUtil.stringToNum(eventId));
	   ArrayList costIds = eventinfoDao.getCostIds() ;
	   ArrayList costDescs = eventinfoDao.getCostDescriptions() ;
	   ArrayList costValues = eventinfoDao.getCostValues();

	   /*ArrayList eventUserIds = eventinfoDao.getEventUserIds();
	   ArrayList userTypes = eventinfoDao.getUserTypes();
	   ArrayList userIds = eventinfoDao.getUserIds();
	   CodeDao codeDao = codelstB.getDescForEventUsers(userIds, userTypes);
	   ArrayList eventUserDesc = codeDao.getCDesc();
	   */

	   ArrayList costCurs = eventinfoDao.getCurrencyIds();
	   ArrayList docIds= eventinfoDao.getDocIds();
	   ArrayList docNames= eventinfoDao.getDocNames();
	   ArrayList docTypes= eventinfoDao.getDocTypes();
	   ArrayList docDescs= eventinfoDao.getDocDescs();

	   //get event resource information from Eventuser bean

   	   EventuserDao evd = eventuser.getEventRoles(EJBUtil.stringToNum(eventId));
	   EventuserDao evUsers = eventuser.getEventUsers(EJBUtil.stringToNum(eventId));


   	   ArrayList eventUserIds = evd.getEventUserIds();
	   ArrayList userTypes = evd.getUserTypes();
	   ArrayList userIds = evd.getUserIds();
   	   ArrayList eventUserDesc = evd.getResourceName();
	   ArrayList eventUserDuration = evd.getDuration();
	   ArrayList eventUserNotes = evd.getNotes();

	   String evName = null;
	   String eventUserId = null;

	   ArrayList evUserIds = evUsers.getEventUserIds();
	   ArrayList evuser = evUsers.getUserIds();
   	   ArrayList evUserDesc = evUsers.getResourceName();
	   ArrayList evDuration = evUsers.getDuration();
	   ArrayList evNotes = evUsers.getNotes();

	   String evUserName = null;
	   String evUserId = null;



	   String costId = "";
	   String costDesc = "";
	   String costValue = "";
	   String docId="";
	   String docName="";
   	   String docType="";
   	   String docDesc="";
	   String costCur = "";


%>

<form METHOD=POST class=formdefault>



<% if(calledFrom.equals("P")) {%>
  <table width="100%" cellspacing="0" cellpadding="0">
	<tr>
      <td align=center>
	<!--<input type="image" src="../images/jpg/Back.gif" border="0" align="absmiddle" onclick="window.history.go(-1); return false;"> -->
	  <button onclick="window.history.go(-1); return false;"><%=LC.L_Back%></button>
      </td>
      </tr>
<%} else {%>
  <table width="100%" cellspacing="0" cellpadding="0">
	<tr>
      <td align=center>
		<button onclick= "javascript:self.close()"><%=LC.L_Close%></button>
      </td>
      </tr>
<%}%>
  </table>
<TABLE width="100%" cellspacing="0" cellpadding="0" >
<tr id="headSection" class="headSection">
<td>
	<%=LC.L_Evt_Info%><%--Event Information*****--%>
</td>
</tr>
</TABLE>
<TABLE width="100%">
<tr><td>&nbsp;</td></tr>
   <tr>
	<td width="40%"><P > <%=MC.M_NameOf_TheEvt%><%-- Name of the event*****--%> </P></td>
	<td width="60%"> <P >
   <%=eventName%></P>
	</td>
   </tr>
   <tr>
	<td width="40%"> <P > <%=LC.L_Description%><%-- Description*****--%> </P> </td>
	<td width="60%"> <P > <%=eventDesc%></P></td>
   </tr>
   <!--Added by gopu to fix the Bugzilla Issue #2347 on 03/07/06 -->
    <tr>
	<td width="40%"> <P > <%=LC.L_Cpt_Code%><%-- CPT Code*****--%> </P> </td>
	<td width="60%"> <P > <%=eventCpt%></P></td>
   </tr>
<%

	if (eventDurationUnit.equals("W")) {%>
   <tr>
	<td width="40%"> <P > <%=MC.M_Duration_OfEvt%><%-- Duration of the event*****--%> </P></td>
	<td width="60%"> <P ><%Object[] arguments4 = {eventDuration}; %>
	    <%=VelosResourceBundle.getLabelString("L_Wks",arguments4)%><%--<%=eventDuration%> Weeks*****--%>
	</P></td>
  </tr>
  <%} else {%>
     <tr>
	<td width="40%"> <P > <%=MC.M_Duration_OfEvt%><%-- Duration of the event*****--%> </P></td>
	<td width="60%"> <P ><%Object[] arguments4 = {eventDuration}; %>
	    <%=VelosResourceBundle.getLabelString("L_Dys",arguments4)%><%--<%=eventDuration%> days*****--%>
	  
	</P></td>
  </tr>
<%}%>
  <tr>
	<td width="40%"> <P > <%=LC.L_Event_Window%><%-- Event Window*****--%> </P></td>
	<td width="60%"> <P >
<%

	//Modified by Gopu to fix the Bugzilla issue #2347

	if (eventDurationUnitAfter==null)			eventDurationUnitAfter = LC.L_Day_S;/*eventDurationUnitAfter = "Day(s)";*****/
	if (eventDurationUnitBefore==null)			eventDurationUnitBefore = LC.L_Day_S;/*"Day(s)";*****/

	if( eventDurationUnitAfter.equals("H") )	eventDurationUnitAfter = LC.L_Hour_S;/*"Hour(s)";*****/
	if( eventDurationUnitAfter.equals("D") )	eventDurationUnitAfter = LC.L_Day_S;/*"Day(s)";*****/
	if (eventDurationUnitAfter.equals("W")) 	eventDurationUnitAfter = LC.L_Week_S;/*"Week(s)";*****/
	if (eventDurationUnitAfter.equals("M"))		eventDurationUnitAfter = LC.L_Month_S;/*"Month(s)";*****/

	if( eventDurationUnitBefore.equals("H") )	eventDurationUnitBefore = LC.L_Hour_S;/*"Hour(s)";*****/
	if (eventDurationUnitBefore.equals("D") )	eventDurationUnitBefore = LC.L_Day_S;/*"Day(s)";*****/
	if (eventDurationUnitBefore.equals("W"))	eventDurationUnitBefore = LC.L_Week_S;/*"Week(s)";*****/
	if (eventDurationUnitBefore.equals("M"))	eventDurationUnitBefore = LC.L_Month_S;/*"Month(s)";*****/

	if(eventFuzzyPerDays.trim().equals("0") && eventFuzzyPerDaysA.trim().equals("0")) {

%>
	    <%=LC.L_None%><%-- None*****--%>
<%


	} else {

%>
	<%

	if(eventFuzzyPerDays.substring(0,1).equals("-"))
	{
		if (eventFuzzyPerDays.substring(1,eventFuzzyPerDays.length()).trim().equals("0")){
		%>
			<%=LC.L_None%><%-- None*****--%>
		<%
		} else{
			//out.println(eventFuzzyPerDays.substring(1,eventFuzzyPerDays.length()).trim()+" Days Before");
					//Query modified by Gopu to fix the Bugzilla issue #2347
			Object[] arguments1 = {eventFuzzyPerDays.substring(1,eventFuzzyPerDays.length()).trim(),eventDurationUnitBefore};
			out.println(VelosResourceBundle.getLabelString("L_Bfore",arguments1));/*out.println(eventFuzzyPerDays.substring(1,eventFuzzyPerDays.length()).trim()+" " + eventDurationUnitBefore +" Before");*****/

		}

	}
	else if(eventFuzzyPerDays.substring(0,1).equals("+"))
	{
		if (eventFuzzyPerDays.substring(1,eventFuzzyPerDays.length()).trim().equals("0")){
		%>
			<%=LC.L_None%><%--None*****--%>
		<%
		} else{
			//out.println(eventFuzzyPerDays.substring(1,eventFuzzyPerDays.length()).trim()+" Days After");
			//Modified by Gopu to fix the Bugzilla issue #2347
			Object[] arguments2 = {eventFuzzyPerDays.substring(1,eventFuzzyPerDays.length()).trim(),eventDurationUnitAfter};
			out.println(VelosResourceBundle.getLabelString("L_Aftr",arguments2));/*out.println(eventFuzzyPerDays.substring(1,eventFuzzyPerDays.length()).trim()+" "+ eventDurationUnitAfter + " After");*****/
		}

	}else{
		//Modified by Gopu to fix the Bugzilla issue #2347
		Object[] arguments3 = {eventFuzzyPerDays.substring(0,eventFuzzyPerDays.length()).trim(),eventDurationUnitBefore,eventFuzzyPerDaysA,eventDurationUnitAfter};
		out.println(VelosResourceBundle.getLabelString("L_Before_After",arguments3));/*out.println(eventFuzzyPerDays.substring(0,eventFuzzyPerDays.length()).trim()+ " "+ eventDurationUnitBefore+" Before, " + eventFuzzyPerDaysA + " "+ eventDurationUnitAfter + " After" );*****/
 	}

 	}

	%>

	</P></td>

   	</tr>
	<tr>
		<td width="40%"> <P ><%=LC.L_Site_OfService%><%-- Site of Service*****--%> </P></td>
		<td width="60%"> <P ><%=eventSOS%></P></td>
  	</tr>
	<tr>
		<td width="40%"> <P ><%=LC.L_Facility%><%-- Facility*****--%> </P></td>
		<td width="60%"> <P ><%=eventFacility%></P></td>
  	</tr>
	<tr>
		<td width="40%"> <P ><%=LC.L_Coverage_Type%><%--<%=VelosResourceBundle.getLabelString("Evt_CoverageType")%>*****--%> </P></td>
		<td width="60%"> <P ><%=eventCoverageType%></P></td>
  	</tr>
	<tr>
	<td width="40%"> <P ><%=LC.L_Evt_Notes%><%-- Event Notes*****--%> </P></td>
	<td width="60%"> <P ><%=eventNotes%></P></td>
  </tr>
 </table>

<BR>

<TABLE width="100%">

	<tr id="headSection" class="headSection">

	<td width=100% colspan=2> <%=LC.L_Links%><%-- Links*****--%> </td>

   </tr>

</table>



<TABLE width="100%">

  <tr >

	<td > <P >  <%=MC.M_Cur_LinkedUrl%><%-- Currently linked url*****--%></P></td>

   </tr>

<%
   for(int i=0;i<docIds.size(); i++) {
	docId=   (String)docIds.get(i);
	docDesc= (String)docDescs.get(i);
	docName = (String)docNames.get(i);
	docType = (String)docTypes.get(i);
	if(docType.equals("F"))
	continue;

		if ((i%2)==0) {

  %>
      <tr id="browserEvenRow">
        <%

		}
		else{
  %>
      <tr id="browserEvenRow">
        <%
		}
  %>
	<td width="70%" ><A href=<%=docName%> target="_new"><%=docName%></A></td>
	<td width="30%"> <%=docDesc%> </td>
   </tr>
<%
   }
%>
</TABLE>

<TABLE width="100%">
  <tr >
	<td colspan=4> <P >  <%=MC.M_Cur_LinkedFiles%><%-- Currently linked files*****--%></P></td>
   </tr>
<%
   for(int i=0;i<docIds.size(); i++) {
	docId=   (String)docIds.get(i);
	docDesc= (String)docDescs.get(i);
	docName = (String)docNames.get(i);
	docType = (String)docTypes.get(i);
	if(docType.equals("U"))
	continue;

		if ((i%2)==0) {

  %>
      <tr id="browserEvenRow">
        <%

		}

		else{

  %>
      <tr id="browserEvenRow">
        <%

		}
	modDnld = dnld + "?file=" + StringUtil.encodeString(docName);
  %>

	<td width="70%"> <A href="<%=modDnld%>&pkValue=<%=docId%>&tableName=SCH_DOCS&columnName=DOC&pkColumnName=PK_DOCS&module=eventapndx&db=sch" target="_new" ><%=docName%></A> </td>
	<td width="30%"> <%=docDesc%> </td>

   </tr>
<%
   }
%>
</TABLE>

<br>

<TABLE width="100%">

	<tr id="headSection" class="headSection">
	<td width=100% colspan=2> <%=LC.L_Associated_Costs%><%-- Associated Costs*****--%></td>
      </tr>
</TABLE>
<br>
<TABLE width="100%">
<%
   for(int i=0;i<costIds.size(); i++) {
	costValue= (String)costValues.get(i);
	costDesc= (String)costDescs.get(i);
	costId = (String)costIds.get(i);
	costCur = (String)costCurs.get(i);

		if ((i%2)==0) {

  %>
      <tr id="browserEvenRow">
        <%

		}

		else{


  %>

      <tr id="browserEvenRow">

        <%



		}



  %>

	<td width="70%"> <%=costDesc %></td>
	<td width="30%"> <%=costCur%> <%=costValue%> </td>
   </tr>
<%
   }
%>
</TABLE>
<br>
<TABLE width="100%">
	<tr id="headSection" class="headSection">
	<td>  <%=LC.L_Associated_Resources%><%-- Associated Resources*****--%></td>
   </tr>
</TABLE>
<br>
<TABLE width="100%">

   <tr>
	<td width = "30%" align="center"><P ><%=LC.L_Role_Types%><%-- Role Types*****--%></p></td>
	<td width = "20%" align="center"><P ><%=LC.L_Duration%><%-- Duration*****--%></p></td>
	<td width = "50%" align="center"><P ><%=LC.L_EventNotes%><%-- Notes*****--%></p></td>
   </tr>

 <%
  for(int i=0;i<eventUserIds.size(); i++)
	{

		if ((count%2)==0) {	%>

      	<tr id="browserEvenRow">

        <%
		}
		else
		{ %>

      <tr id="browserOddRow">
        <%
		}

		count++;
  %>

	<td width = "30%">
	<%=eventUserDesc.get(i)%>
	</td>

	<td width = "20%">
	<%=eventUserDuration.get(i)%>
	</td>

	<td width = "50%">
	<%=eventUserNotes.get(i) == null ? "-" : eventUserNotes.get(i)%>
	</td>

   </tr>

<%
	}
%>

</table>
<br>
<TABLE width="100%">

   <tr>
	<td width = "80%" align="center"><P ><%=LC.L_Users%><%-- Users*****--%></p></td>
   </tr>

<%

	count = 0;

	for(int i=0;i<evUserIds.size(); i++)
	{

		if ((count%2)==0) {	%>

      	<tr id="browserEvenRow">

        <%
		}
		else
		{ %>

      <tr id="browserOddRow">
        <%
		}

		count++;
  %>

	<td width = "80%">
	<%=evUserDesc.get(i)%>
	</td>

   </tr>

<%
	}

%>

</table>
<br>


<%

if(eptRight==1){

%>

<br>

<TABLE width="100%">

	<tr id="headSection" class="headSection">

	<td>  <%=LC.L_Associated_Crfs %><%-- Associated CRFs*****--%></td>

   </tr>

</TABLE>

<br>

<TABLE width="100%">



<tr>

<td width="50%" align="center" > <P > <%=LC.L_Crf_Name%><%-- CRF Name*****--%> </P></td>

<td width="50%" align="center" > <P > <%=LC.L_Crf_Number%><%-- CRF Number*****--%> </P></td>

</tr>



<%

	for(int i=0;i<crflibIds.size(); i++) {

		crflibNumber = (String) crflibNumbers.get(i);

	   	crflibName = (String) crflibNames.get(i);

		if(crflibNumber == null || crflibNumber.equals("")){

			crflibNumber ="--";

		}

		if(crflibName == null || crflibName.equals("")){

			crflibName ="--";

		}
%>
	<tr id="browserEvenRow">
		<td align="center"> <%=crflibName%></td>
		<td align="center"> <%=crflibNumber%></td>
	</tr>
<%

	}

%>
<tr>

<td width="50%" align="center" > <P > <BR><%=LC.L_Crf_FormName%><%-- CRF Form Name*****--%> </P></td>
<td width="50%" align="center" > <P > <BR><%=LC.L_Crf_FormType %><%-- CRF Form Type*****--%></P></td>

</tr>
<%


	for(int k=0;k<crfFormNames.size(); k++) {

		crfFormName = (String) crfFormNames.get(k);
	   	crfFormType = (String) crfFormTypes.get(k);

		if (StringUtil.isEmpty(crfFormName))
		{
			crfFormName = "-";
			crfFormTypeName = "-";
		}
		else
		{
			if (crfFormType.equals("SP"))
			{
				crfFormTypeName = LC.L_Pat_SpecStd;/*crfFormTypeName = LC.Pat_Patient+" (Specific "+LC.Std_Study+")";*****/
			}
			else if (crfFormType.equals("PS"))
			{
				crfFormTypeName = LC.L_Pat_AllStd;/*crfFormTypeName = LC.Pat_Patient+" (All "+LC.Std_Studies+")";*****/
			}
			else if (crfFormType.equals("PR"))
			{
				crfFormTypeName = LC.L_Pat_AllStdRest;/*crfFormTypeName = LC.Pat_Patient+" (All "+LC.Std_Studies+" - Restricted)";*****/
			}
		}


%>
	<tr id="browserEvenRow">
		<td align="center"> <%=crfFormName%></td>
		<td align="center"> <%=crfFormTypeName%></td>
	</tr>
<%

	}

//other links
%>

<tr>

<td width="50%" align="center" > <P > <BR><%=LC.L_Other_Links%><%-- Other Links*****--%></P></td>

</tr>
<%

	for(int k=0;k<otherCRFLinks.size(); k++) {

		crfOtherLink = (String) otherCRFLinks.get(k);


		if (StringUtil.isEmpty(crfOtherLink))
		{
			crfOtherLink = "-";

		}

%>
	<tr id="browserEvenRow">
		<td align="center"> <%=crfOtherLink%></td>

	</tr>
<%

	}

%>




</TABLE>

<%

} //end of if for eptRight

%>

<br>





<TABLE width="100%">

	<tr id="headSection" class="headSection">

	<td>  <%=LC.L_Associated_Messages%><%-- Associated Messages*****--%></td>



   </tr>

</TABLE>



<TABLE width="100%">
<tr>
<td width="10%" align="center" > <P > <%=LC.L_Type%><%-- Type*****--%> </P></td>
<td width="20%" align="center" > <P ><%=LC.L_Days_Before %><%-- Days before*****--%> </P> </td>
<td width="20%" align="center" > <P > <%=LC.L_Days_After%><%-- Days after*****--%> </P></td>
<td width="50%" align="center" > <P > <%=LC.L_Message%><%-- Message*****--%> </P></td>
</tr>

<tr id="browserEvenRow">

<td align="center" ><%=LC.L_User_Lower %><%-- user*****--%> </td>
<td align="center" ><%= eventMsgDaysBuser%></td>
<td align="center" >--</td>
<td align="center" > <%= eventMsgBuser%> </td>
</tr>

<tr id="browserEvenRow">

<td align="center" ><%=LC.L_User_Lower %><%-- user*****--%> </td>
<td align="center">--</td>
<td align="center"><%= eventMsgDaysAuser%></td>
<td align="center"> <%= eventMsgAuser%> </td>
</tr>



<tr id="browserEvenRow">
<td align="center"><%=LC.L_Patient_Lower%><%-- <%=LC.Pat_Patient_Lower%>*****--%>	 </td>
<td align="center"><%= eventMsgDaysBpat %></td>
<td align="center">--</td>
<td align="center"><%= eventMsgBpat %></td>
</tr>

<tr id="browserEvenRow">
<td align="center"><%=LC.L_Patient_Lower%><%-- <%=LC.Pat_Patient_Lower%>*****--%>	 </td>
<td align="center">--</td>
<td align="center"><%= eventMsgDaysApat %></td>
<td align="center"><%= eventMsgApat %></td>
</tr>

</TABLE>
<TABLE width="100%">
<tr id="headSection" class="headSection">
	<td><%=MC.M_UsrSelRcv_MsgAre%><%-- Users selected to receive these messages are*****--%></td>

   </tr>
  <!-- Added by Gopu to fix the issues #2490 list of selected user -->
	<%
	   userTypes = eventinfoDao.getUserTypes();
	   userIds = eventinfoDao.getUserIds();
	   CodeDao codeDao = codelstB.getDescForEventUsers(userIds, userTypes);
	   ArrayList eventUserDesc1 = codeDao.getCDesc();
	   String users ="";
	   String userIdList ="";
for(int i=0;i<userIds.size(); i++) {

	   if(userTypes.get(i).equals("S")) {

		users = " "+ eventUserDesc1.get(i) ;
%>
		<tr id="browserEvenRow"><td><%=users%></td></tr>

<%

	   }

	}

	%>

</tr>

</TABLE>


    <br>
  <% if(calledFrom.equals("P")) {%>
  <table width="100%" cellspacing="0" cellpadding="0">
	<tr>
      <td align=center>
<!--	<input type="image" src="../images/jpg/Back.gif" border="0" align="absmiddle" onclick="window.history.go(-1); return 	false;"> -->
	 <button onclick="window.history.go(-1); return false;"><%=LC.L_Back%></button>
      </td>
      </tr>
<%} else {%>
  <table width="100%" cellspacing="0" cellpadding="0">
	<tr>
      <td align=center>
      	<button onclick= "javascript:self.close()"><%=LC.L_Close%></button>
      </td>
      </tr>
<%}%>
  </table>
</form>
<%
} else {  //else of if body for session
%>
  <jsp:include page="timeout_childwindow.jsp" flush="true"/>
  <%
}
%>
</body>
</html>





