<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Select_Evts%><%--Select Events*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss_skin.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">

    function initEvents() {
        //bug 4766
        if (document.eventlibrary.eSign != null){
        	document.eventlibrary.eSign.onkeypress = setESign;
        }
    }
    function setESign(evt) {
    	evt = (evt) || window.event;
        if (evt) {
            var code = evt.charCode || evt.keyCode;
            if (code == 13 || code == 10) {
                // Modified by Parminder Singh Bug 10452#
                document.eventlibrary.submitMode.value = "";
                return validate(document.eventlibrary);
            }
        }
    }
	function validate(formobj) {
		var calStatus = document.getElementById("calStatus").value;
		if (calStatus == "A"){
			var fdaVal = document.getElementById("fdaStudy").value;
			if (fdaVal == "1"){
				if (  !(validate_col('<%=LC.L_ReasonForChangeFDA%>',formobj.remarks))) return false;
			}
		}
		if (!(validate_col('Esign',formobj.eSign))) return false

 		 if(isNaN(formobj.eSign.value) == true) {
 			alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
			formobj.eSign.focus();
			return false;
		}

		count = formobj.noOfCheckboxes.value;
		for (i=0;i<count;i++) {
			if (count > 1) {
				if (formobj.eventcheckbox[i].checked) {
					formobj.submit();
					document.getElementById('submit_btn').disabled = true;
					setTimeout('document.seleventlib.eSign.disabled=true',10);
					return false;
				}
			} else {
				if (formobj.eventcheckbox.checked) {
					formobj.submit();
					document.getElementById('submit_btn').disabled = true;
					setTimeout('document.seleventlib.eSign.disabled=true',10);
					return false;
				}
			}
		}
		alert("<%=MC.M_Selc_AnEvt%>");/*alert("Please select an Event.");*****/
		return false;
	}
	function validate_select(formobj) {
		//KM-#3207
		var pageRight = formobj.pageRight.value;
		if (f_check_perm(pageRight,'N') == false) {
			 return false;
		}
		count = formobj.noOfCheckboxes_list.value;
   //     formobj.submitMode.value="select" ;
		for (i=0;i<count;i++) {
			if (count > 1) {
				if (formobj.eventcheckbox_list[i].checked) {
					// to fix the Bug #2366
			        formobj.submitMode.value="select" ;
			        formobj.page.value=formobj.page.value;
					formobj.submit();
					return false;
				}
			} else {
	 			if (formobj.eventcheckbox_list.checked) {
					// to fix the Bug #2366
			        formobj.submitMode.value="select" ;
			        formobj.page.value=formobj.page.value;
					formobj.submit();
					return false;
				}
			}
		}
		alert("<%=MC.M_Selc_AnEvt%>");/*alert("Please select an Event.");*****/
		return false;
	}
	function validate_search(formobj) {
		//KM-Mandatory removed -- #4254
		//if (!(validate_col('Event Library Type',formobj.cmbLibType))) return false

	    formobj.submitMode.value="search" ;
	    formobj.page.value="1";
	    formobj.submit();
    	return false;
	}
	function validate_refresh(formobj) {
	    formobj.submitMode.value="refresh" ;
	    formobj.submit();
    	return false;
	}


	 function fclose() {
		self.close();
	}

//KM-04Sep09
function callAjaxGetCatDD(formobj){
	   selval = formobj.cmbLibType.value;
 	   new VELOS.ajaxObject("getCodeDropDown.jsp", {
   		urlData:"selectedVal="+selval+"&ddName=catId&codeType=L&ddType=category&from=valAll" ,
		   reqType:"POST",
		   outputElement: "span_catId" }

   ).startRequest();

}




</SCRIPT>

</head>



<% response.setHeader("Cache-Control", "max-age=1, must-revalidate");%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="ctrldao" scope="request" class="com.velos.esch.business.common.EventdefDao"/>
<jsp:useBean id="ctrldao_prot" scope="request" class="com.velos.esch.business.common.EventdefDao"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="siteB" scope="request" class="com.velos.eres.web.site.SiteJB"/>

<%
int ienet = 2;

String agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1)
     ienet = 0; //IE
    else
	ienet = 1;
	if(ienet == 0) {
%>
<body onload="initEvents();" style="overflow:scroll;">
<%
	} else {
%>
<body onload="initEvents();" style="overflow:visible;">
<%
	}
%>
<%@ page language = "java" import = "com.velos.eres.business.group.*,com.velos.eres.business.common.*,com.velos.esch.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.* , com.velos.eres.web.studyRights.StudyRightsJB"%>
<jsp:include page="popupPanel.jsp" flush="true"/>
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<% int pageRight = 0;
   HttpSession tSession = request.getSession(true);
   Configuration conf = new Configuration();
   if (sessionmaint.isValidSession(tSession))

 {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
	<jsp:include page="include.jsp" flush="true"/>
<%

	String studyId = (String) tSession.getValue("studyId");
	studyB.setId(EJBUtil.stringToNum(studyId));
	studyB.getStudyDetails();
	int fdaStudy = ("1".equals(studyB.getFdaRegulatedStudy()))? 1:0;

	String evtLibType = request.getParameter("evtLibType");

   //JM: 05May2008, added for, Enh. #PS16
    String calledFromPg = request.getParameter("calledFromPage");
    calledFromPg = (calledFromPg==null)?"":calledFromPg;

    String visitNumber = request.getParameter("visitId");
    visitNumber = (visitNumber==null)?"":visitNumber;

	//JM: #3531
    String patientId = request.getParameter("patId");
    patientId = (patientId==null)?"":patientId;

//JM: 28May2008 for #3531
    String patProtKey = request.getParameter("patProtId");
    patProtKey = (patProtKey==null)?"":patProtKey;


    String dsplcmnt = request.getParameter("displacement");
    dsplcmnt = (dsplcmnt==null)?"0":dsplcmnt;

/////////
	String cateId="";
	String addlCode = request.getParameter("addlCode");
	
	//modified by sonia - 09/14/10 - this variable was set a 'space' for no reason. after null check, it should just be set to "" and NOT " "
	addlCode = (addlCode ==null) ? "" :addlCode;

	if (! StringUtil.isEmpty(addlCode))
	{
		addlCode = addlCode.trim();
	}
///////////
   String sAcc = (String) tSession.getValue("accountId");
   //int accId = EJBUtil.stringToNum((String) (tSession.getValue("accountId")));
   int accId = EJBUtil.stringToNum(sAcc);
   String description="";
   String protocolId="";
   String categoryId="";
   String eventId="";
   String eventType="";
   String eventName="";
   String eventNameTrim="";
   String catName="";
   String src="" ;
   String duration="";
   String calledFrom ="";
   String mode="";
   String calStatus="";
   String cost="";
   String notes="";
   int evpageRight = 0;
   String cpt="";
   String eventcost ="";
   String additionalcode ="";
   String facilityname="";//KV:SW-FIN2c

   ArrayList eventIdArray = null;
   ArrayList eventparam = null;


   if ((request.getParameter("fromPage"))!=null)
   {
     if ((request.getParameter("fromPage")).equals("NewEvent"))
     {

   	       eventparam=(ArrayList) tSession.getAttribute("EventParam");
		   eventparam=(ArrayList) tSession.getAttribute("EventParam");
   	       src= (String)eventparam.get(8);
   	       duration= (String)eventparam.get(3);
   	       protocolId= (String)eventparam.get(6);
		   calledFrom= (String)eventparam.get(5);
		   mode= (String)eventparam.get(1);
		   calStatus= (String)eventparam.get(4);
		  cost= (String)eventparam.get(2);
		  categoryId= (String)eventparam.get(0);
		  catName= (String)eventparam.get(7);
   }//endif for ((request.getParameter("frompage")).equals("NewEvent"))
  }//endif for ((request.getParameter("frompage"))!=null)
  else {
	   categoryId= request.getParameter("catId");


	   catName= request.getParameter("catName");
	   src= request.getParameter("srcmenu");
	   duration= request.getParameter("duration");
	   protocolId= request.getParameter("protocolId");
	  calledFrom= request.getParameter("calledFrom");
	  mode= request.getParameter("mode");
	  calStatus= request.getParameter("calStatus");
	  cost= request.getParameter("cost");

	}

	if (StringUtil.isEmpty(calStatus) || "null".equals(calStatus)){
		calStatus = "";
	}

  	String libTypePullDn = "";
	int libTypeId = 0;
	String libTypeSearch = request.getParameter("cmbLibType");


	//Rohit SW-FIN2A
	int eventFacId = 0;
	String strEventFacilityId = "";
	eventFacId = EJBUtil.stringToNum(request.getParameter("ddFacid"));
	if (eventFacId == 0) {
		strEventFacilityId = "";
	}
	else {
		strEventFacilityId = "" + eventFacId;
	}

	int CostType = 0;
	String strCostType = "";
	CostType = EJBUtil.stringToNum(request.getParameter("costDesc"));
	if (CostType == 0) {
		strCostType = "";
	}
	else {
		strCostType = "" + CostType;
	}




	SchCodeDao schDao = new SchCodeDao();
	schDao.getCodeValues("lib_type");

	if(libTypeSearch == null && evtLibType != null) {
		libTypeId = StringUtil.stringToNum(evtLibType);
		libTypeSearch = libTypeId +"";
	}else if(libTypeSearch!=null && !libTypeSearch.isEmpty()){
		libTypeId = StringUtil.stringToNum(libTypeSearch);
	}else{
		libTypeId=0;
	}
	
	libTypePullDn = schDao.toPullDown("cmbLibType",libTypeId," onChange=callAjaxGetCatDD(document.eventlibrary);");


//	SchCodeDao schDao1 = new SchCodeDao();
//	schDao1.getValuesForCategory("L",String.valueOf(libTypeId),(String) (tSession.getValue("accountId")));//KM
//	cateId = schDao1.toPullDown("catId",EJBUtil.stringToNum(categoryId),"");



  String mainmode ;
  //this varibale mainmode is passed to New event screen for session variable setting flag.
  mainmode= mode ;
  String calltime=request.getParameter("calltime");
  if (calltime!=null) {
   if (calltime.equals("New"))
    {

	  	  if ((tSession.getAttribute("EventArray"))!= null)
   	  {
   	  	  tSession.removeValue("EventArray");

   	  }
   	 calltime="save";
   	}
  }
   if (mode.equals("N")){

   	     if (eventIdArray!= null){tSession.putValue("EventArray",eventIdArray);}
   }

   if (tSession.getAttribute("EventArray")!=null)
   {
   	   eventIdArray=((ArrayList)(tSession.getAttribute("EventArray")));


    }

   int counter = 0;
   int counter1=0;
   int  id;
   String uName = (String) tSession.getValue("userName");


  //Get all the categories for Dropdown menu
	   ArrayList eventIds_prot= null;
	   ArrayList names_prot= null;
	   ArrayList descriptions_prot= null;
	   ArrayList event_types_prot= null;
	   ArrayList eventCosts_prot= null;

	   ctrldao_prot= eventdefB.getProtSelectedEvents(EJBUtil.stringToNum(protocolId),"","");//KM
	   eventIds_prot=ctrldao.getEvent_ids();
	   names_prot= ctrldao.getNames();
	   descriptions_prot= ctrldao.getDescriptions();
	   event_types_prot= ctrldao.getEvent_types();
	   eventCosts_prot= ctrldao.getCosts();


		if(evtLibType == null)
			evtLibType = libTypeSearch;

	   ctrldao= eventdefB.getHeaderList(accId,"L",EJBUtil.stringToNum(evtLibType));
       ArrayList catIds = ctrldao.getEvent_ids();
	   ArrayList catNames = ctrldao.getNames();
	   int length = catIds.size();
	 //End category menu

	//  by salil
	String evtName = request.getParameter("srhevent");
    //km
	String cptName=request.getParameter("srhcpt");
	if ( evtName == null)
	{  evtName="";}
	cptName=(cptName==null)?"":cptName;
	 /////////for pagination by salil
		String pagenum = "";
		int curPage = 0;
		long startPage = 1;
		String stPage;
		long cntr = 0;


		String searchClause = "";
		String fromClause = " ";


		pagenum = request.getParameter("page");

		if (pagenum == null)
		{
			pagenum = "1";
		}
		curPage = EJBUtil.stringToNum(pagenum);

		String orderBy = "";
		orderBy = request.getParameter("orderBy");
		String orderType = "";
		orderType = request.getParameter("orderType");
		String countSql = "";
		String formSql = "";
		if (orderBy == null)
		{
			orderBy = "";
		}
		if (orderType == null)
		{
			orderType = "";
		}


		 //Added by Manimaran for November enhancement #3.
		String str="";
		if(categoryId.equals("All")){

			if(length > 0) { //KM
				for(int cnt = 0; cnt< length-1;cnt++)
				{
					str= str+(catIds.get(cnt))+",";
				}
				str=str+catIds.get(length-1);
			}
		}


		String str1,str2,str3,str4;

		//KV:SW-FIN2c
		 if (categoryId.equals("All")){

		 //KM-#DFin9
		 str1 ="select distinct EVENT_ID,ORG_ID,CHAIN_ID,EVENT_TYPE,(select name from event_def where chain_id=e.chain_id and event_type='L' ) CATNAME,NAME,NOTES,COST,COST_DESCRIPTION,DURATION,USER_ID,LINKED_URI,FUZZY_PERIOD,MSG_TO,DESCRIPTION,DISPLACEMENT,EVENT_CPTCODE,		 lst_cost(event_id) as eventcost, lst_additionalcode(event_id,'evtaddlcode') as additionalcode, (select site_name from er_site s  where s.pk_site = e.FACILITY_ID) as facilityname FROM EVENT_DEF e ";

		 if (!StringUtil.isEmpty(addlCode)) {
			str1 = str1+ ", er_moredetails ";
		 }

		 if (!StringUtil.isEmpty(strCostType)) {
			 str1 = str1+ " , sch_eventcost ";

			}
		 str1 = str1+ " WHERE  CHAIN_ID in "
		+"("+str+")"
		+" and event_type = 'E' AND EVENT_LIBRARY_TYPE = "+libTypeId + " ";

//modified by Sonia-09/14/10 - check for md_modname was missing
		if (!StringUtil.isEmpty(addlCode)) {
			str1 = str1+ " AND (LOWER(md_modelementdata) LIKE LOWER('%"+addlCode+"%')) and fk_modpk = event_id and md_modname = 'evtaddlcode' ";
		 }

		}
		//KV:SW-FIN2c
		else
		{
		//KM-#DFin9
		str1 ="select distinct EVENT_ID,ORG_ID,CHAIN_ID,EVENT_TYPE,NAME,NOTES," 	+	"COST,COST_DESCRIPTION,DURATION,USER_ID,LINKED_URI,FUZZY_PERIOD,MSG_TO,DESCRIPTION,"
		+"DISPLACEMENT,EVENT_CPTCODE, lst_cost(event_id) as eventcost, lst_additionalcode(event_id,'evtaddlcode') as additionalcode,(select site_name from er_site s  where s.pk_site = e.FACILITY_ID) as facilityname FROM EVENT_DEF e";

		if (!StringUtil.isEmpty(addlCode)) {
			str1 = str1+ ", er_moredetails ";
		 }
		if (!StringUtil.isEmpty(strCostType)) {
			 str1 = str1+ " , sch_eventcost ";

			}
		str1= str1+ " WHERE  CHAIN_ID = "
		+ EJBUtil.stringToNum(categoryId)
		+" and event_type = 'E' AND EVENT_LIBRARY_TYPE = "+libTypeId + " ";

//modified by Sonia-09/14/10 - check for md_modname was missing

		if (!StringUtil.isEmpty(addlCode)) {
			str1 = str1+ " AND (LOWER(md_modelementdata) LIKE LOWER('%"+addlCode.trim()+"%')) and fk_modpk = event_id and md_modname = 'evtaddlcode'";
		}

		}

		 str2="  and (lower(NAME) like lower('%"+evtName.trim()+"%') OR LOWER(NOTES) LIKE LOWER('%"+evtName.trim()+"%') OR LOWER(DESCRIPTION) LIKE LOWER('%"+evtName.trim()+"%'))  ";
		 str3="and lower(EVENT_CPTCODE) like lower('%"+cptName.trim()+"%')";
		 str4="  ORDER BY CHAIN_ID,COST,EVENT_ID,NAME,DISPLACEMENT ";


		if(evtName.equals("") && (cptName.equals(""))){
		//formSql= str1+str4;
			formSql= str1;
		}
		else
		{
		    if (evtName.equals("") && !(cptName.equals("")))
		        formSql=str1+str3;
		    if(!(evtName.equals("")) && (cptName.equals("")))
		    	formSql=str1+str2;

	        //JM: 16Jan2008, #3224: when both the search strings are not null
	        if((!evtName.equals("")) && (!cptName.equals("")))
		    	formSql=str1+str2+str3;

	        }

		//out.println("formSql::"+formSql);

		//modified by Sonia-09/14/10 - check for md_modname was missing
		 if (!StringUtil.isEmpty(addlCode)) {
					fromClause = fromClause + " , er_moredetails ";
					searchClause = " AND (LOWER(md_modelementdata) LIKE LOWER('%"+addlCode.trim()+"%')) and fk_modpk = event_id and md_modname = 'evtaddlcode' ";
		  }

		 if (!StringUtil.isEmpty(strCostType)) {
			 fromClause = fromClause + " , sch_eventcost ";
			}

		if((evtName.equals("")) && (cptName.equals(""))) {

		  if (categoryId.equals("All")){

		    countSql="select count(*) from (select 	distinct event_id	FROM  EVENT_DEF "+ fromClause +"	WHERE  CHAIN_ID in "
		    +"("+str+")"
		    +" and event_type = 'E' AND EVENT_LIBRARY_TYPE = "+libTypeId + " " + searchClause ;
		  }
		  else
		  {
		      countSql ="select 	count(*) FROM (select distinct event_id from EVENT_DEF "+ fromClause +"	WHERE  CHAIN_ID= "+EJBUtil.stringToNum(categoryId)+" and event_type = 'E'  AND EVENT_LIBRARY_TYPE = "+libTypeId + " " + searchClause ;
		}
		}
		else
		{
		    if (evtName.equals("") && !(cptName.equals(""))) {


		    	if (categoryId.equals("All")){
		         countSql ="select 	count(*)	FROM (select distinct event_id from EVENT_DEF "+ fromClause +"	WHERE  CHAIN_ID in"
			 +"("+str+")"
			  +" and event_type = 'E' and lower(EVENT_CPTCODE) like lower('%"+cptName.trim()+"%') AND EVENT_LIBRARY_TYPE = "+libTypeId + " "+ searchClause ;
		      }
			else
			{
			  countSql ="select 	count(*)	FROM (select distinct event_id from  EVENT_DEF "+ fromClause +"	WHERE  CHAIN_ID= "+EJBUtil.stringToNum(categoryId)+" and event_type = 'E' and lower(EVENT_CPTCODE) like lower('%"+cptName.trim()+"%') AND EVENT_LIBRARY_TYPE = "+libTypeId + " "+ searchClause ;

			}

		     }
		     if(!(evtName.equals("")) && (cptName.equals(""))){
		          if(categoryId.equals("All")){
			  countSql ="select 	count(*)	FROM (select distinct event_id from EVENT_DEF "+ fromClause +"	WHERE  CHAIN_ID in "
			  +"("+str+")"
			  +" and event_type = 'E' and (lower(NAME) like lower('%"+evtName.trim()+"%') OR LOWER(NOTES) LIKE LOWER('%"+evtName.trim()+"%') OR LOWER(DESCRIPTION) LIKE LOWER('%"+evtName.trim()+"%')) AND EVENT_LIBRARY_TYPE = "+libTypeId + " "+ searchClause ;
			  }
			  else
			  {

			  countSql ="select 	count(*)	FROM (select distinct event_id from  EVENT_DEF "+ fromClause +"	WHERE  CHAIN_ID= "+EJBUtil.stringToNum(categoryId)+" and event_type = 'E' and (lower(NAME) like lower('%"+evtName.trim()+"%') OR LOWER(NOTES) LIKE LOWER('%"+evtName.trim()+"%') OR LOWER(DESCRIPTION) LIKE LOWER('%"+evtName.trim()+"%')) AND EVENT_LIBRARY_TYPE = "+libTypeId + " "+ searchClause ;
			 }
			}

			//JM: 16Jan2008, #3224: when both the search strings are not null
			if ( !evtName.equals("") && !cptName.equals("")) {

		    	if (categoryId.equals("All")){
		         countSql ="select 	count(*)	FROM (select distinct event_id from  EVENT_DEF "+ fromClause +"	WHERE  CHAIN_ID in"
			 +"("+str+")"
			  +" and event_type = 'E' and (lower(NAME) like lower('%"+evtName.trim()+"%') OR LOWER(NOTES) LIKE LOWER('%"+evtName.trim()+"%') OR LOWER(DESCRIPTION) LIKE LOWER('%"+evtName.trim()+"%')) and lower(EVENT_CPTCODE) like lower('%"+cptName.trim()+"%') AND EVENT_LIBRARY_TYPE = "+libTypeId + " "+ searchClause  ;
		      }
			else
			{
			  countSql ="select 	count(*)	FROM (select distinct event_id from  EVENT_DEF "+ fromClause +"	WHERE  CHAIN_ID= "+EJBUtil.stringToNum(categoryId)+" and event_type = 'E' and (lower(NAME) like lower('%"+evtName.trim()+"%') OR LOWER(NOTES) LIKE LOWER('%"+evtName.trim()+"%') OR LOWER(DESCRIPTION) LIKE LOWER('%"+evtName.trim()+"%')) and lower(EVENT_CPTCODE) like lower('%"+cptName.trim()+"%') AND EVENT_LIBRARY_TYPE = "+libTypeId + " "+ searchClause  ;

			}

		     }
			//JM: 16Jan2008, #3224
		 }

		//*********************
		String searchsql = "" ;
		if (!StringUtil.isEmpty(strEventFacilityId)) {
			searchsql = searchsql + " AND (LOWER(FACILITY_ID) LIKE LOWER('%"+strEventFacilityId+"%')) ";
			}

		if (!StringUtil.isEmpty(strCostType)) {
			searchsql = searchsql + " AND (LOWER(fk_cost_desc) LIKE LOWER('%"+strCostType+"%')) and fk_event = event_id ";
			//fromClause = fromClause + " , sch_eventcost ";

		}

		formSql = formSql + searchsql + str4 ;
		countSql = countSql + searchsql + " ) " ;


        //***************************

		long rowsPerPage=0;
		long totalPages=0;
		long rowsReturned = 0;
		long showPages = 0;

	   boolean hasMore = false;
	   boolean hasPrevious = false;
	   long firstRec = 0;
	   long lastRec = 0;
	   long totalRows = 0;


	   rowsPerPage=conf.MOREBROWSERROWS;
	   totalPages = conf.getTotalPagesPerBrowser(conf.ERES_HOME +"eresearch.xml");


	   BrowserRows br = new BrowserRows();
	   if(libTypeId!=0){
       br.getPageRows(curPage,rowsPerPage,formSql,totalPages,countSql,orderBy,"null");
   	   rowsReturned = br.getRowReturned();
	   showPages = br.getShowPages();
	   startPage = br.getStartPage();
	   hasMore = br.getHasMore();
	   hasPrevious = br.getHasPrevious();
	   totalRows = br.getTotalRows();
   	   firstRec = br.getFirstRec();
	   lastRec = br.getLastRec();
	   }
	   ///////////end for pagination

	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
	if (calledFrom.equals("S")) {

   	   	   StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");

	  	   if ((stdRights.getFtrRights().size()) == 0){

			 	pageRight= 0;

		   }else{

				pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));

	   	   }

		} else if ((calledFrom.equals("P")) || (calledFrom.equals("L"))) {

			pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));

		}

		evpageRight = Integer.parseInt(grpRights.getFtrRightsByValue("EVLIB"));

  // end  by salil
    int i=1;
	int noOfCheckboxes=0;
	int noOfCheckboxes_list=0;

	//Rohit SW-FIN2A changes for Facility dropdown
	CodeDao codeDao = new CodeDao();
	codeDao.getCodeValues("site_type","service");
	ArrayList siteType = new ArrayList();
	siteType = codeDao.getCId();
	String strSiteType = EJBUtil.ArrayListToString(siteType);
	SiteDao siteDao = siteB.getBySiteTypeCodeId(accId,strSiteType);
	String ddFacility ="";
	//ddFacility=siteDao.toPullDown("ddFacid",eventFacId, " ");
	//ddFacility = StringUtil.replace(ddFacility,"Select an Option","ALL");

	//KM-#4976
	StringBuffer sbFacility = new StringBuffer();
	ArrayList facIds = siteDao.getSiteIds();
	ArrayList facDescs = siteDao.getSiteNames();//JM: 14Jul2010: #5135
	int inFacType = 0;
	sbFacility.append("<SELECT NAME='ddFacid' >") ;
	sbFacility.append("<option selected value='' >"+LC.L_All/*All*****/+"</option>") ;
	if (facIds.size() > 0)
	{
		for (int count = 0; count < facIds.size(); count++)
		{
			inFacType = ((Integer)facIds.get(count)).intValue();
			if(inFacType == eventFacId){
				sbFacility.append("<OPTION value = "+ inFacType+" selected>" + facDescs.get(count)+ "</OPTION>");
			}else{
			sbFacility.append("<OPTION value = "+ inFacType+">" + facDescs.get(count)+ "</OPTION>");
			}

		}
	}
	sbFacility.append("</SELECT>");
	ddFacility  = sbFacility.toString();

	//Rohit SW-FIN2A changes for Cost Type dropdown
	String sd1 ;
	SchCodeDao cdesc = new SchCodeDao();
	cdesc.getCodeValues("cost_desc", EJBUtil.stringToNum(sAcc));
	//sd1 = cdesc.toPullDown("costDesc",CostType,"");
	//sd1 = StringUtil.replace(sd1,"Select an Option","ALL");

	//KM-#4976
	StringBuffer sbCost = new StringBuffer();
	ArrayList costIds = cdesc.getCId();
	ArrayList costDescs = cdesc.getCDesc();
	int inCostType = 0;
	sbCost.append("<SELECT NAME='costDesc' >") ;
	sbCost.append("<option selected value='' >"+LC.L_All/*All*****/+"</option>") ;
	if (costIds.size() > 0)
	{
		for (int count = 0; count < costIds.size(); count++)
		{
			inCostType = ((Integer)costIds.get(count)).intValue();
			if(inCostType == CostType){
				sbCost.append("<OPTION value = "+ inCostType+" selected>" + costDescs.get(count)+ "</OPTION>");
			}else{
			sbCost.append("<OPTION value = "+ inCostType+">" + costDescs.get(count)+ "</OPTION>");
			}

		}
	}
	sbCost.append("</SELECT>");
	sd1  = sbCost.toString();


 if (pageRight > 0 )
	{
 %>

<P class="sectionHeadings"> <%=LC.L_Select_Evts%><%--Select Events*****--%> </P>
  <Form name="eventlibrary" id="seleventlib" method="post" action="protocoleventsave.jsp?mode=<%=mode%>&srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFromPage=<%=calledFromPg%>&calledFrom=<%=calledFrom%>&from=selecteventus" onSubmit="if (validate(document.eventlibrary)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

  	<Input name="calledFromPage" type=hidden value=<%=calledFromPg%>><!--//JM: 05May2008, added for, Enh. #PS16 -->
  	<Input name="visitId" type=hidden value=<%=visitNumber%>>
  	<Input name="displacement" type=hidden value=<%=dsplcmnt%>>
	<input name ="pageRight" type= hidden value =<%=pageRight%>>
	<Input name="patId" type=hidden value=<%=patientId%>>
	<Input name="patProtId" type=hidden value=<%=patProtKey%>><!--JM: 28May2008 for #3531-->
	<Input name="page" type=hidden value=<%=curPage%>><!--JM: 18Aug2010 for #5241-->
	<Input id="fdaStudy" name="fdaStudy" type=hidden value="<%=fdaStudy%>"/>
	<Input id="calStatus" name="calStatus" type=hidden value="<%=calStatus%>"/>


  	<DIV id="div1">
  	<table width="100%" cellspacing="2" cellpadding="0" border="0" class="basetbl outline">
	<tr >
		<!--KM- #4254 -->
		<td align="right" width="25%"><%=LC.L_Evt_Lib%><%--Event Library Type*****--%> &nbsp;&nbsp;&nbsp;</td> <td> <%=libTypePullDn%> </td>
		<INPUT TYPE="HIDDEN" name="submitMode" value="">
		<Input name="calltime" type=hidden value=<%=calltime%>>
		<td align="right" width="20%" >
			<%=LC.L_Evt_Cat%><%--Event Category*****--%> &nbsp;&nbsp;
		</td>


	<%--	<td> <span id = "span_catId"> <%=cateId%> </span> </td>  --%>


			<td > <span id = "span_catId">
				<select  name="catId">
						<option value="All"><%=LC.L_All%><%--All*****--%></option>
						<%

						for(int count = 0; count< length;count++)
						{
							if(categoryId.equals(catIds.get(count).toString())){
								catName=((String) catNames.get(count)) ;
							%>
							    <option value="<%=catIds.get(count)%>" SELECTED> <%=catNames.get(count)%> </option>

							<% }else{%>
							<option value="<%=catIds.get(count)%>"> <%=catNames.get(count)%> </option>
							<% }
						}
							%>
				</select>
					</span>
			</td>


			<td width="10%">

			<!--KM- #3207 -->
			<!--Rohit- #5198 -->
			<A href= "eventdetails.jsp?mainmode=<%=mode%>&cost=<%=cost%>&catName=&srcmenu=<%=src%>&eventmode=N&categoryId=<%=categoryId%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=N&calStatus=<%=calStatus%>&calledFromPage=<%=calledFromPg%>&displacement=<%=dsplcmnt%>&visitId=<%=visitNumber%>&patId=<%=patientId%>&patProtId=<%=patProtKey%>&fromPage=selectEvent&selectedTab=1" onclick ="return f_check_perm(<%=evpageRight%>,'N')"  ><img src="./images/AddNewEvent.gif" title="<%=LC.L_Add_NewEvt_Upper%><%--ADD NEW EVENT*****--%>" alt="<%=LC.L_Add_NewEvt_Upper%><%--ADD NEW EVENT*****--%>" border="0"></img></A>

			</td>

		<td align="left" valign="middle">
			<button type="submit" onclick="return validate_search(document.eventlibrary)"><%=LC.L_Search%></button>
		</td>

    </tr>
	<tr>
		<td width="18%" align="right">
		<%=MC.M_EvtName_DescOrNotes%><%--Event Name, Description or Notes*****--%> &nbsp;&nbsp;&nbsp;
		</td>
		<td width="15%"> <!--  KV:Fixed Bug No. 4397 -->
			<input type=text name="srhevent" size="20" maxlength="50" value= "<%=evtName%>" >
		</td>
		<!--  KV:Fixed Bug No. 4849 -->
		<td align="right"> <%=LC.L_Addl_Codes%><%--Additional Codes*****--%> &nbsp;&nbsp;</td> <td> <input type="text" name="addlCode"   size=20 MAXLENGTH = 50 value= "<%=addlCode%>" > </td>

		<td align="right"><%=LC.L_Facility%><%--Facility*****--%> &nbsp;&nbsp;&nbsp;</td>
		<td> <%=ddFacility%> </td>
  		<!--<P>
  			<td width="25%" colspan="4" align="right">
	  		 <A href="javascript:window.opener.location.reload(); window.close()"><img src="../images/jpg/Close.gif" border="0"></img></A>
  			</td>
  		</P>-->
     </tr>
     <!--   Added by Manimaran for November enhancement #3. -->
	 <tr>
  		<td align="right">
  			 <%=LC.L_Cpt%><%--CPT*****--%> &nbsp;&nbsp;&nbsp;&nbsp;
  		</td>
		<td width="15%"  valign="middle">
		<!--  KV:Fixed Bug No. 4849 -->
			<input type=text name="srhcpt" size="20" maxlength="50" value= "<%=cptName%>" >
		</td>
		<td align="right"><%=LC.L_Cost_Type%><%--Cost Type*****--%> &nbsp;&nbsp;&nbsp;</td>
		<td> <%=sd1%> </td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>


	</table>

   	<br>

<table width="100%" border="0" cellspacing="0" cellpadding="0" >
				<tr>
				<td  width="70%" >
<%if(libTypeId==0){ %>
<p><font class="recNumber"><%=MC.M_EntEvSearchCrt %></font></p>
<%}else{ %>
<div style="overflow:auto;overflow-x:hidden; height:350;">
  <table width="100%" cellspacing="0" cellpadding="0" border="0" class="outline"><tr><td>

	 <table width="100%">
      <tr class = "popupHeader">
         <th width="5%"><%=LC.L_Select%><%--Select*****--%></th>
         <th width="15%"><%=LC.L_Category%><%--Category*****--%></th>
        <th width="10%"> <%=LC.L_Cpt%><%--CPT*****--%> </th>
        <th width="15%"> <%=LC.L_Event%><%--Event*****--%> </th>
        <th width="20%"> <%=LC.L_Description%><%--Description*****--%> </th>
        <th width="15%"> <%=LC.L_EventNotes%><%--Notes*****--%> </th>
		 <th width="10%"> <%=LC.L_Cost%><%--Cost*****--%> </th>
		 <th width="10%"> <%=LC.L_Addl_Codes%><%--Additional Codes*****--%> </th>
		 <th width="15%"> <%=LC.L_Facility%><%--Facility*****--%> </th>

      </tr>
       <%
   if (categoryId==null) { //end of len
 %>

        <P class = "defComments"><%=MC.M_Selc_Cat%><%--Please select a Category*****--%></P>


 <%	}      %>

<%// salil cut if of len

 // Prabhat : Bug # 4277
					%>

					<script language=javascript>
	                 var varViewTitle =  new Array (<%=rowsReturned%>);
	                </script>

            <%

    for(counter = 1;counter<=rowsReturned;counter++)
	{


   	eventId =(br.getBValues(counter,"EVENT_ID"));
	id = EJBUtil.stringToNum(eventId);

	eventType=((br.getBValues(counter,"EVENT_TYPE")) == null)?"-":(br.getBValues(counter,"EVENT_TYPE")).toString();

	if (categoryId.equals("All")){
	  catName=((br.getBValues(counter,"CATNAME"))==null)?"-":(br.getBValues(counter,"CATNAME")).toString();
	 }
	eventName=((br.getBValues(counter,"NAME")) == null)?"-":(br.getBValues(counter,"NAME")).toString();
//Prabhat: Bug #4277 Fixed
	eventNameTrim = eventName;
	if(eventNameTrim.length()>50){
		eventNameTrim = eventNameTrim.substring(0,50)+"...";
	}

	description=((br.getBValues(counter,"DESCRIPTION")) == null)?"-":(br.getBValues(counter,"DESCRIPTION")).toString();
	cpt=((br.getBValues(counter,"EVENT_CPTCODE"))==null)?"-":(br.getBValues(counter,"EVENT_CPTCODE")).toString();
	notes=((br.getBValues(counter,"NOTES")) == null)?"-":(br.getBValues(counter,"NOTES")).toString();
	eventcost = ((br.getBValues(counter,"eventcost")) == null)?"-":(br.getBValues(counter,"eventcost")).toString();
	additionalcode = ((br.getBValues(counter,"additionalcode")) == null)?"-":(br.getBValues(counter,"additionalcode")).toString();
	//KV:SW-FIN2c
	facilityname =((br.getBValues(counter,"facilityname")) == null)?"-":(br.getBValues(counter,"facilityname")).toString();

	if ((counter%2)==0) {
	%>
      <tr class="browserEvenRow">
  <%
		}
	else {
  %>
      <tr class="browserOddRow">
   <%
		}
	%>


	<td align=center><INPUT name=eventcheckbox_list type=checkbox value='<%=id%>+"||"+<%=eventName%>+"||"+<%=description%>+"||"+<%=cpt%>+"||"+<%=notes%>+"||"+<%=catName%>'></td>

	<td><%=catName%> </td>
	<td><%=cpt%></td>

	               <script language=javascript>
		             varViewTitle[<%=counter%>] = htmlEncode('<%=eventName%>');

		           </script>
	<td><A href="viewevent.jsp?eventId=<%=id%>&calledFrom=P" onmouseover="return overlib('<%=eventName%>',CAPTION,'<%=LC.L_Event_Name%><%--Event Name*****--%>');" onmouseout="return nd();"> <%=eventNameTrim%></A></td><!-- Prabhat: Bug #4277 Fixed-->
	<td><%=description%></td>
	<td><%=notes%></td>
	<td><%=eventcost%></td>
   	<td><%=additionalcode%></td>
   	<td><%=facilityname%></td>  <!-- KV:SW-FIN2c -->
	</tr>
 <%
 	noOfCheckboxes_list++;
		}

%>

<input type=hidden name=noOfCheckboxes_list value=<%=noOfCheckboxes_list%>>
 <tr>
  <%
     //Table for page nos by salil
  if (totalRows > 0)
		{ Object[] arguments = {firstRec,lastRec,totalRows}; 
		%>
		    <font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%></font><%--<font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s)</font>*****--%>
		<%} else {%>
			<font class="recNumber"><%=MC.M_NoRecordsFound%><%--No Records Found*****--%></font>
		<%}%>

	<%
		if (curPage==1) startPage=1;
	    for ( int count = 1; count <= showPages;count++)
		{


   			cntr = (startPage - 1) + count;
	 		if ((count == 1) && (hasPrevious))
			{
			  %>

			  	<A href="selecteventus.jsp?mode=<%=mode%>&srcmenu=tdmenubaritem4&duration=<%=duration%>&protocolId=<%=protocolId%>&catId=<%=categoryId%>&catName=<%=catName%>&page=<%=cntr-1%>&calledFrom=<%=calledFrom%>&cost=-1&calStatus=<%=calStatus%>&calltime=<%=calltime%>&srhevent=<%=evtName%>&srhcpt=<%=cptName%>&calledFromPage=<%=calledFromPg%>&visitId=<%=visitNumber%>&patId=<%=patientId%>&patProtId=<%=patProtKey%>&displacement=<%=dsplcmnt%>&cmbLibType=<%=libTypeSearch%>&addlCode=<%=addlCode%>&ddFacid=<%=strEventFacilityId%>&costDesc=<%=strCostType%>" >< <%=LC.L_Previous%><%--Previous*****--%> <%=totalPages%>> </A>
				&nbsp;&nbsp;&nbsp;&nbsp;

				<%
  			}	%>

		<%
 		 if (curPage  == cntr)
		 {
	    	 %>
				<FONT class = "pageNumber"><%= cntr %></Font>
       		<%
       	}else
        {	%>
		<A href="selecteventus.jsp?mode=<%=mode%>&srcmenu=tdmenubaritem4&duration=<%=duration%>&protocolId=<%=protocolId%>&catId=<%=categoryId%>&catName=<%=catName%>&page=<%=cntr%>&calledFrom=<%=calledFrom%>&cost=-1&calStatus=<%=calStatus%>&calltime=<%=calltime%>&srhevent=<%=evtName%>&srhcpt=<%=cptName%>&calledFromPage=<%=calledFromPg%>&visitId=<%=visitNumber%>&patId=<%=patientId%>&patProtId=<%=patProtKey%>&displacement=<%=dsplcmnt%>&cmbLibType=<%=libTypeSearch%>&addlCode=<%=addlCode%>&ddFacid=<%=strEventFacilityId%>&costDesc=<%=strCostType%>"   ><%= cntr%></A>
       <%}%>

		<%	}
		if (hasMore)
		{   %>
	   &nbsp;&nbsp;&nbsp;&nbsp;
	   <A href="selecteventus.jsp?mode=<%=mode%>&srcmenu=tdmenubaritem4&duration=<%=duration%>&protocolId=<%=protocolId%>&catId=<%=categoryId%>&catName=<%=catName%>&page=<%=cntr+1%>&calledFrom=<%=calledFrom%>&cost=-1&calStatus=<%=calStatus%>&calltime=<%=calltime%>&srhevent=<%=evtName%>&srhcpt=<%=cptName%>&calledFromPage=<%=calledFromPg%>&visitId=<%=visitNumber%>&patId=<%=patientId%>&patProtId=<%=patProtKey%>&displacement=<%=dsplcmnt%>&cmbLibType=<%=libTypeSearch%>&addlCode=<%=addlCode%>&ddFacid=<%=strEventFacilityId%>&costDesc=<%=strCostType%>"  >< <%=LC.L_Next%><%--Next*****--%> <%=totalPages%>></A>
		</td>
		<%	}	%>
	   </tr>
	  </table>

  </td></tr></table>
  <%} %>
  </div>
  </td>


  <td width="3%" align="center">

  <A  name="thebutton"  value= "select" onclick="return validate_select(document.eventlibrary)"  href="#"> <img src="../images/jpg/formright.gif" align="absmiddle" border="0"></A>
  </td>


<!--		<table><tr><td>&nbsp;</td>
		<td align=center>

		<A  name="thebutton"  value= "select" onclick="return validate_select(document.eventlibrary)"  href="#"> <img src="../images/jpg/selectevents.gif" align="absmiddle" border="0"></A>
      </td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
      <td>
	  <A name="thebutton" value="refresh" onclick="return validate_refresh(document.eventlibrary)" href="#"> <img src="../images/jpg/refresh.gif" align="absmiddle" border="0"></A>
    </td>			</tr>
      </table>-->


<!--// salil cut else -->

 <td width="30%">

			<div style="overflow:auto;overflow-x:hidden; height:350;" >

		<table width="100%" cellspacing="0" cellpadding="0" border="0"  class="outline">
		 <tr><td width="100%" align="left" colspan="2"><font class="comments"><div id="totalSelected"><%=LC.L_Selected_Evt_S%><%--Selected Event(s)*****--%>: </div><a onclick="return validate_refresh(document.eventlibrary)" href="#"><%=LC.L_Clear_Unselected%><%--Clear Unselected*****--%></a></font></td></tr>
		<tr class = "popupHeader">
	<th width="5%"><%=LC.L_Select%><%--Select*****--%></th>
        <!-- <th width="15%">Category</th>-->
        <th width="15%"> <%=LC.L_Event%><%--Event*****--%> </th>
        <!--<th width="10%"> CPT </th>
        <th width="20%"> Description </th>
        <th width="15%"> Notes </th>-->


      </tr>

 <%
 	if (eventIdArray!=null) {
 	 i=1;


	 String Id = "";
	 String catname =null;
	 int tempIndex= 0;
     int tempIndexNext =0;
	for(counter1 = 0;counter1<eventIdArray.size();counter1++)
		 {
		             	 tempIndex=0;
		             	 tempIndexNext=0;
		 	        String detail_str= (String)eventIdArray.get(counter1);
                    Id= detail_str.substring(0,(detail_str.indexOf("||") - 2));
		 	        tempIndex= (detail_str.indexOf("||") + 4) ;
		 	        tempIndexNext =(detail_str.indexOf("||",tempIndex) - 2);
		 	        eventName=detail_str.substring(tempIndex,tempIndexNext);
		 	   // Prabhat:  # Bug4277 Fixed
		 	         eventName = (  eventName==null )?"-":(  eventName ) ;
					 eventNameTrim = eventName;
					if(eventNameTrim.length()>50){
						eventNameTrim = eventNameTrim.substring(0,50)+"...";

					}
		 	         tempIndex= detail_str.indexOf("||",tempIndex )+4;
		 	         tempIndexNext =(detail_str.indexOf("||",tempIndex )) - 2;
		 	         description=detail_str.substring(tempIndex,tempIndexNext);
				 tempIndex= detail_str.indexOf("||",tempIndex )+4;
				 //Added by Manimaran for November enhancement #3.
		 	         tempIndexNext =(detail_str.indexOf("||",tempIndex )) - 2;
				 cpt=detail_str.substring(tempIndex,tempIndexNext);
				 tempIndex= detail_str.indexOf("||",tempIndex )+4;
		 	         tempIndexNext =(detail_str.indexOf("||",tempIndex )) - 2;
				 notes=detail_str.substring(tempIndex,tempIndexNext);
				 catname=detail_str.substring((detail_str.lastIndexOf("||") + 4));




  	i++;
	if ((i%2)==0) {
	%>
      <tr class="browserEvenRow">
  <%
		}
	else {
  %>
      <tr class="browserOddRow">
   <%
		}
	%>
	<td align=center><INPUT name=eventcheckbox type=checkbox value=<%=Id%>  checked></td>
	<!--KM-3396-->
<!--	<td><%=catname%> </td>-->
<input type="hidden" value="<%=catname%>" name="catNames">	<!-- KM -->
                <%//Prabhat :  #4277 Fixed%>

                   <script language=javascript>
		             varViewTitle[<%=counter1%>] = htmlEncode('<%=eventName%>');

		           </script>
	<td><A href="viewevent.jsp?eventId=<%=Id%>&calledFrom=P" onmouseover="return overlib(varViewTitle[<%=counter1%>],CAPTION,'<%=LC.L_Event_Name%><%--Event Name*****--%>');" onmouseout="return nd();"> <%=eventNameTrim%> </A></td>
		<input type ="hidden" name="eventnames" value = "<%=eventName%>" > <!--KM-->
<!--	<td><%=cpt%></td>
	<td><%=description%></td>
	<td><%=notes%></td>-->


    </tr>
 <% noOfCheckboxes++;


		}

		%>
    </table>
    </div>
    <script>
   		document.getElementById('totalSelected').innerHTML = "<%=LC.L_Selected_Evt_S%><%--Selected Event(s)*****--%>: <%=noOfCheckboxes%>";
   	</script>
    <%
    		mode="N" ;
		}

				%>
	<input type="hidden" name=noOfCheckboxes value=<%=noOfCheckboxes%>>
<% if (eventIdArray == null) { %>
<tr>
<td colspan="5"></td>
</tr>
</table>
</div>
<% }  %>
</td>
</tr>
</table>
<table align="center">
<tr>
	<%if ("A".equals(calStatus)){ %>
    <td><%=LC.L_ReasonForChangeFDA%>
     	<%if (fdaStudy == 1){%><FONT class="Mandatory">* </FONT><%} %>
	</td>
	<td><textarea id="remarks" name="remarks" rows="2" cols="20" MAXLENGTH="4000"></textarea></td>
	<%} %>
	<td>
<%  if(mode.equals("M")) {
		if (pageRight >= 5) { %>
			<jsp:include page="submitBar.jsp" flush="true">
					<jsp:param name="displayESign" value="Y"/>
					<jsp:param name="formID" value="seleventlib"/>
					<jsp:param name="showDiscard" value="N"/>
			</jsp:include>
	<%  }
	} else {  // "New" mode
		if (pageRight>=5) { %>
			<jsp:include page="submitBar.jsp" flush="true">
					<jsp:param name="displayESign" value="Y"/>
					<jsp:param name="formID" value="seleventlib"/>
					<jsp:param name="showDiscard" value="N"/>
			</jsp:include>
	<%  }
	}
	%>
	</td>
</tr>
</table>
<!-- Modified by Parminder Singh Bug10452#-->
 <SCRIPT LANGUAGE = "JavaScript">
{ 
	 document.getElementById('submit_btn').disabled=false; 
 }
 </SCRIPT>
</div>
</Form>

<%
	} //end of if body for page right
else
{
%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
 <%
 } //end of else body for page right
}//end of if body for session
else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
<%
}
%>
<jsp:include page="ui-include.jsp" flush="true"></jsp:include>
<div class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</body>
</html>
