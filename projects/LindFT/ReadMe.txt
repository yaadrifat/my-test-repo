This is a custom project of Velos eRerearch for Lind FT project.

The base eResearch is v9.3.

In Eclipse, link this project with your velos project as follows.
a) Right-click on this project -> Properties
b) Project References -> put a check on your velos project -> OK
c) Also in the Project Properties, go to Java Build Path -> Add your velos project -> OK -> OK



2014-JUL-18
Removed the following files because they were moved to velos head:
ConfigUtilAction.java
UIFlxPageDao.java
FlxPageCache.java
FlxPageFields.java
FlxPageSections.java
struts-eresearch.xml
tiles.xml
configStudyScreen.jsp
flexPageCookie.jsp
flexStudyPageBySection.jsp
flexStudyPageSectStudyDocs.jsp
flexStudyPageSectStudyTeam.jsp
menusie.jsp
studySubmissionScreen.jsp
build.xml

2014-JUL-30
Moved the following to velos head:
StudyAction.java

