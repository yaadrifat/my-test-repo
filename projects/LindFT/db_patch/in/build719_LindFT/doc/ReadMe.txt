1) Label customization
Please add the following lines in eResearch_jboss510\conf\labelBundle_custom.properties:

L_Therapeutic_Area=Department
L_Sponsor_Id=Sponsor Protocol Number
L_Sample_Size=Total number enrolled at MDACC

2) Config customization
Please add the following lines in eResearch_jboss510\conf\configBundle_custom.properties:

EIRB_MODE=LIND
Workflows_Enabled=Y
Workflow_FlexStudy_WfTypes=STUDY_INITIATION,STUDY_ACTIVATION
subm_status.pi_respond.colorcode=RosyBrown
subm_status.withdrawn.colorcode=Wheat
subm_status.reviewer.colorcode=YellowGreen
subm_status.admin_review.colorcode=PaleGoldenRod
subm_status.admin_rev_comp.colorcode=DarkGoldenRod
subm_status.asgn_pr.colorcode=Yellow
subm_status.pi_resp_req.colorcode=Thistle
subm_status.submitted.colorcode=PowderBlue
subm_status.approved.colorcode=SandyBrown
subm_status.resubmitted.colorcode=Gold
subm_status.rejected.colorcode=Lavender
subm_status.unlock_crc.colorcode=LimeGreen
subm_status.unlock_irb.colorcode=HoneyDew
subm_status.savedItems.colorcode=Khaki
study.savedItems=Not Submitted

