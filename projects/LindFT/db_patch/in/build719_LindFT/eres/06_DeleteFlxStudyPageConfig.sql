set define off;

delete from UI_FLX_PAGE where PAGE_ID ='protocol';

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,318,930,'06_DeleteFlxStudyPageConfig.sql',sysdate,'v9.3.0 #719');

commit;