set define off;
set serveroutput on;

declare
v_CodeExists number := 0;
begin
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'onlyOther';	
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_SEQ,
		CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyscope', 'onlyOther', 'Only at Other Sites', 8, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcReturn';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studystat', 'crcReturn', 'CRC modifications required', 
		    208, NULL, 'departmental', 'default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'sys_err_soa';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studystat', 'sys_err_soa', 'System Error - SOA', 
		    'N', 500, NULL, 'irb', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'sys_err_click';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studystat', 'sys_err_click', 'System Error - Click', 
		    'N', 501, NULL, 'irb', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'juvinileEligYes';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, 
		    CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'juvinileEligYes', 'Additional Comment', 
		    'N', 432, NULL, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'recrutPopIncarc';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'recrutPopIncarc', 'Will the recruitment population at M. D. Anderson include persons who are incarcerated at time of enrollment (e.g., prisoners) or likely to become incarcerated during the study?', 
		    'N', 433, 'dropdown', '<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'grantingAgency';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'grantingAgency', 'Does the Study have a Sponsor, Supporter or Granting Agency?', 
		    'N', 434, 'dropdown', '<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'grntnAgncyData';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'grntnAgncyData', 'Will the Sponsor/Supporter/Granting Agency receive data?', 
		    'N', 435, 'dropdown', '<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'radioComp';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'radioComp', 'Does this protocol include the administration of a radioactive compound (or drug) to a patient intended to obtain basic information regarding metabolism (including kinetics, distribution, and localization) of the drug or regarding human physiology, pathophysiology, biochemistry, but not intended for immediate therapeutic, diagnostic, or similar purposes or to determine the safety and effectiveness of the drug in humans for such purposes (i.e. to carry out a clinical study)?', 
		    'N', 436, 'dropdown', '<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'radioCompAvail';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'radioCompAvail', 'Is the radioactive compound (or drug) FDA approved and/or commercially available?', 
		    'N', 437, 'dropdown', '<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'clicResrCtgry';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'clicResrCtgry', 'Clinical Research Category', 
		    'N', 438, 'dropdown', '<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "INT">Interventional (INT)</OPTION>
		<OPTION value = "OBS">Observational (OBS)</OPTION>
		<OPTION value = "ANCCOR">Ancillary or Correlative (ANC/COR)</OPTION>
		</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'preClincData';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'preClincData', '<A href="#"><span onmouseover="return overlib(''Summarize the available non-clinical data (published or available unpublished data) that could have clinical significance..'',
		CAPTION,''Pre-clinical Data'',LEFT, ABOVE);"
		onmouseout="return nd();"
		alt="">
		Pre-clinical Data
		</span></A>', 
		    'N', 441, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		    
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'preCliDrugDev';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'preCliDrugDev', 'Was preclinical work to develop the therapy or methodology being tested in this protocol performed at UTMDACC or by UTMDACC faculty?', 
		    'N', 439, 'dropdown', '<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'stdyDisease';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'stdyDisease', '<A href="#">
		<img src="../images/jpg/help.jpg" border="0" 
		onmouseover="return overlib(''This section should contain a background discussion of the target disease state to which the investigational product(s) hold promise, and any pathophysiology relevant to potential study treatment action.'',
		CAPTION,''Study Disease'',LEFT, ABOVE);"
		onmouseout="return nd();"
		alt="">
		</img>
		</A>
		Study Disease (Description of Disease)', 
		    'N', 440, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		    
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'clincDataToDate';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'clincDataToDate', 'Clinical Data to Date
		<br>
		<i><small>Summarize the available clinical study data (published or available unpublished data) with relevance to the protocol under construction -- if none is available, include a statement that there is no available clinical research data to date on the investigational product.</small></i>', 
		    'N', 442, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		    
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'lngTrmFollowUp';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'lngTrmFollowUp', 'Long Term Follow-up', 
		    'N', 455, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		    
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'patAssessmnt';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'patAssessmnt', 'Patient Assessments', 
		    'N', 449, 'splfld', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		    
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'womenPartici';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'womenPartici', 'Women', 
		    'N', 446, 'dropdown', '<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'explrtObjctv';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'explrtObjctv', 'Exploratory Objective', 
		    'N', 444, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'corrStdBkgrnd';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'corrStdBkgrnd', 'Correlative Studies Background', 
		    'N', 443, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'elgbltyCriteria';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'elgbltyCriteria', '<h4>Eligibility Criteria</h4>', 
		    'N', 445, 'splfld', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'subRecrScreen';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'subRecrScreen', '<h4>Subject Recruitment and Screening</h4>', 
		    'N', 447, 'splfld', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'stdyCalndr';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'stdyCalndr', 'Study Calendar/Schedule of Events', 
		    'N', 448, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'endTheraEvals';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'endTheraEvals', '<li>End of Therapy Evaluations', 
		    'N', 453, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'medMontrng';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'medMontrng', 'Medical Monitoring', 
		    'N', 451, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'baselineEvals';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'baselineEvals', '<li>Screening/Baseline', 
		    'N', 450, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'onstdyEvals';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'onstdyEvals', '<li>On-study/Treatment Evaluations', 
		    'N', 452, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'followUp';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'followUp', '<li>Follow-up', 
		    'N', 454, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		    
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'womenParticiYes';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'womenParticiYes', 'Additional Comment', 
		    'N', 456, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'recrStrategy';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'recrStrategy', '<i>Recruitment Strategy</i>', 
		    'N', 468, 'splfld', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'diseaseGrp';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'diseaseGrp', '<i>Disease Group</i>', 
		    'N', 466, 'splfld', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'priInvConsor';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'priInvConsor', 'Principal Investigator for the Consortium', 
		    'N', 461, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'pregEligbltyYes';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'pregEligbltyYes', 'Additional Comment', 
		    'N', 459, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'minorPartici';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'minorPartici', 'Minorities', 
		    'N', 457, 'dropdown', '<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'minorParticiYes';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'minorParticiYes', 'Additional Comment', 
		    'N', 458, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'consortiName';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'consortiName', 'Consortium Name', 
		    'N', 460, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'doseRationale';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'doseRationale', 'Dose Rationale', 
		    'N', 464, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'descDrug';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'descDrug', 'Description of Drug', 
		    'N', 462, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'descDevice';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'descDevice', 'Description of Device', 
		    'N', 463, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'trtmntArms';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'trtmntArms', 'Treatment Arms', 
		    'N', 465, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'subRecrLoc';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'subRecrLoc', '<i>Location of Subject Recruitment</i>', 
		    'N', 467, 'splfld', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'constProced';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'constProced', '<i>Consenting Procedures</i>', 
		    'N', 469, 'splfld', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'spnsrdProt';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'spnsrdProt', 'Protocol Template', 
		    'N', 472, 'dropdown', '<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "coopGrp">Cooperative Group</OPTION>
		<OPTION value = "consrtm">Consortium</OPTION>
		<OPTION value = "nciEtctn">NCI ETCTN</OPTION>
		<OPTION value = "indstrStdy">Industry Study</OPTION>
		<OPTION value = "MDACCInvIni">MDACC Investigator Initiated</OPTION>
		</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'tarPopPat';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'tarPopPat', 'Targeted Population of Patients', 
		    'N', 470, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'stdyIncludes';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'stdyIncludes', 'Study includes', 
		    'N', 471, 'checkbox', '{chkArray:[
		{data: "option1", display:"Drug"},
		{data: "option2", display:"Device"},
		{data: "option3", display:"Imaging"},
		{data: "option4", display:"Surgical"},
		{data: "option5", display:"Radiotherapy"},
		{data: "option6", display:"Cell Therapeutic"},
		{data: "option7", display:"Gene Therapy"}
		]}', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'protAttUpld';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'protAttUpld', 'Protocol attachment uploaded', 
		    'N', 473, 'dropdown', '<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'stdRemCriteria';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'stdRemCriteria', 'Criteria for removal of study', 
		    'N', 476, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'wthdrwSubHow';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'wthdrwSubHow', 'When and how to withdraw subjects', 
		    'N', 474, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'wthdrwFollow';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'wthdrwFollow', 'Data collection and follow-up for withdrawn subjects', 
		    'N', 475, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'trtmntPlan';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'trtmntPlan', 'Treatment Plan', 
		    'N', 477, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'rplSubject';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'rplSubject', 'Replacement of Subjects', 
		    'N', 486, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'blndTrtGrp';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'blndTrtGrp', 'Blinding notes', 
		    'N', 484, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'accrualHead';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'accrualHead', '<h4>Accrual</>', 
		    'N', 481, 'splfld', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'scndryEndPts';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'scndryEndPts', 'Secondary Endpoints', 
		    'N', 479, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'stdDesEndPts';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'stdDesEndPts', '<h4>Study Design and Endpoints</>', 
		    'N', 478, 'splfld', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'expEndPts';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'expEndPts', 'Exploratory Endpoints', 
		    'N', 480, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'trtmntGrpHead';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'trtmntGrpHead', '<h4>Methods for Assigning Subjects to Treatment Groups</>', 
		    'N', 482, 'splfld', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'rndmnTrtGrp';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'rndmnTrtGrp', 'Randomization notes', 
		    'N', 483, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'unblnTrtGrp';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'unblnTrtGrp', 'Unblinding notes', 
		    'N', 485, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'statAnalPlanHd';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'statAnalPlanHd', '<h4>Statistical Analysis Plan</>', 
		    'N', 487, 'splfld', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'statAnalPlan';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'statAnalPlan', 'Statistical Analysis Plan', 
		    'N', 488, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'intrMonHead';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'intrMonHead', '<h4>Interim Monitoring Plan</>', 
		    'N', 489, 'splfld', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundnSrc';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'fundnSrc', '<h4>Funding Source</>', 
		    'N', 501, 'splfld', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'repIRB';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'repIRB', 'Reporting to IRB', 
		    'N', 496, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'repSAEs';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'repSAEs', 'Reports of SAEs and Unanticipated Problems', 
		    'N', 494, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'defSAE';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'defSAE', 'Define SAEs', 
		    'N', 492, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'cohrtSummHead';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'cohrtSummHead', '<h4>Cohort Summaries</>', 
		    'N', 490, 'splfld', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'cohrtSummHead';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'cohrtSumm', 'Cohort Summaries', 
		    'N', 491, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'recrdAE';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'recrdAE', 'Recording of AEs', 
		    'N', 493, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'repSponsr';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'repSponsr', 'Reporting to Sponsor', 
		    'N', 495, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dsmbExt';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'dsmbExt', 'Independent/External', 
		    'N', 497, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'omcrReviewed';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studystat', 'omcrReviewed', 'OMCR Reviewed', 
		    'N', NULL, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'indReviewed';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studystat', 'indReviewed', 'IND Reviewed', 
		    'N', NULL, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'biostatReviewed';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studystat', 'biostatReviewed', 'Biostatistician Reviewed', 
		    'N', NULL, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'deptSignoff';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studystat', 'deptSignoff', 'Departmental Signoff', 
		    'N', NULL, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'amendApproved';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studystat', 'amendApproved', 'Amendment Approved', 
		    'N', NULL, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'irbRenewal';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studystat', 'irbRenewal', 'IRB Renewal Approved', 
		    'N', NULL, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'publPlan';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'publPlan', 'Publication Plan', 
		    'N', 503, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dataConfPlan';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'dataConfPlan', '<h4>Data Confidentiality Plan</>', 
		    'N', 498, 'splfld', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'recRetention';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'recRetention', '<h4>Records Retention</>', 
		    'N', 500, 'splfld', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dataCollTool';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'dataCollTool', '<h4>Data Collection Tool</>', 
		    'N', 499, 'splfld', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'subStipndPay';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'subStipndPay', '<h4>Subject Stipends/Payments</>', 
		    'N', 502, 'splfld', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'selMoonshot';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'selMoonshot', 'Select a Moonshot', 
		    'N', 505, 'dropdown', '<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "option1">APOLLO</OPTION>
		<OPTION value = "option2">Big Data</OPTION>
		<OPTION value = "option3">Cancer Control and Prevention</OPTION>
		<OPTION value = "option4">Center for the Co-Clinical (CCCT)</OPTION>
		<OPTION value = "option5">Clinical Genomics</OPTION>
		<OPTION value = "option6">Immunotherapy</OPTION>
		<OPTION value = "option7">Institute for Applied Cancer Science</OPTION>
		<OPTION value = "option8">Institute for Personal Cancer Therapy</OPTION>
		<OPTION value = "option9">Proteomics</OPTION>
		</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'references';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'references', 'References', 
		    'N', 504, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'medclDevcsHd';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'medclDevcsHd', '<h4>Medical Devices</>', 
		    'N', 515, 'splfld', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'subCmplMntrngHd';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'subCmplMntrngHd', '<h4>Subject Compliance Monitoring</>', 
		    'N', 513, 'splfld', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drgMngmntHd';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'drgMngmntHd', '<h4>Drug Management</>', 
		    'N', 511, 'splfld', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'concomTheraHd';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'concomTheraHd', '<h4>Concomitant Therapy</>', 
		    'N', 509, 'splfld', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'blndinProcdrsHd';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'blndinProcdrsHd', '<h4>Blinding of Procedures</>', 
		    'N', 510, 'splfld', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'devMangmntHd';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'devMangmntHd', '<h4>Device Management</>', 
		    'N', 512, 'splfld', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'radioisotopeHd';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'radioisotopeHd', '<h4>Contrast Agents or Radioisotopes</>', 
		    'N', 514, 'splfld', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'invstgnDevHd';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'invstgnDevHd', '<h4>Investigational Devices</>', 
		    'N', 516, 'splfld', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'labCorrStdsHd';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'labCorrStdsHd', '<h4>Laboratory Correlative Studies</>', 
		    'N', 518, 'splfld', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'biomrkrStdsHd';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'biomrkrStdsHd', '<h4>Biomarker Studies</>', 
		    'N', 517, 'splfld', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'trtmntRegPlanHd';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'trtmntRegPlanHd', '<h4>Treatment Regimen/Plan</>', 
		    'N', 508, 'splfld', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'ideInfrmtnHd';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'ideInfrmtnHd', '<h4>IDE Information</>', 
		    'N', 507, 'splfld', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'indInfrmtnHd';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'indInfrmtnHd', '<h4>IND Information</>', 
		    'N', 506, 'splfld', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drgSupMfgr';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'drgSupMfgr', 'Drug Supplier/Manufacturer', 
		    'N', 526, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drgMngntFile';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'drgMngntFile', 'Do you have a drug management file?', 
		    'N', 525, 'dropdown', '<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'subComplMontr';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'subComplMontr', 'Subject Compliance Monitoring', 
		    'N', 521, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'concomThera';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'concomThera', 'Concomitant Therapy', 
		    'N', 519, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'trtmntDuration';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'trtmntDuration', 'Treatment duration', 
		    'N', 520, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'radstpAddnCmn';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'radstpAddnCmn', 'Additional Comments', 
		    'N', 522, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'commIncCritra';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'commIncCritra', 'Common Inclusion Criteria', 
		    'N', 523, 'checkbox', '{chkArray:[{data: ''option1'', display:''Age >/= 18 years old''},
		{data: ''option2'', display:''Subjects must have histologically or cytologically confirmed disease''},
		{data: ''option3'', display:''Left Ventricular Ejection fraction (LVEF) >/= Lower Limit of Normal Range (LLN)''},
		{data: ''option4'', display:''ECOG performance status 0-2''},
		{data: ''option5'', display:''Capable of understanding and complying with the protocol requirements and has signed the informed consent document''},
		{data: ''option6'', display:''Women of childbearing potential must have a negative pregnancy test at screening''},
		{data: ''option7'', display:'' Measurable disease by Response Evaluation Criteria in Solid Tumors (RECIST) 1.1 criteria''},
		{data: ''option8'', display:''Disease progression in the past 14 months''},
		{data: ''option9'', display:''Adequate thyroid stimulating hormone (TSH) suppression (<0.5 mIU/L)''}]}', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'commExcCritra';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'commExcCritra', 'Common Exclusion Criteria', 
		    'N', 524, 'checkbox', '{chkArray:[{data: ''option1'', display:''Unable to swallow tablets''},
		{data: ''option2'', display:''Pregnant or breastfeeding''},
		{data: ''option3'', display:''Unable or unwilling to abide by the study protocol or cooperate fully with the investigator or designee''},
		{data: ''option4'', display:''Patients may not be receiving any other investigational agents''},
		{data: ''option5'', display:''Patients with known untreated brain metastases''},
		{data: ''option6'', display:''Patients with uncontrolled malabsorption syndromes''},
		{data: ''option7'', display:''Patients with a history of congestive heart failure of any New York Heart Association class''},
		{data: ''option8'', display:"Any medical or psychiatric illness which, in the opinion of the principal investigator, would compromise the patient’s ability to tolerate this treatment regimen"},
		{data: ''option9'', display:''Hypertension (defined as a systolic blood pressure [SBP] >140 mm Hg and/or diastolic blood pressure [DBP] > 90 mm Hg, which cannot be controlled by anti-hypertensive therapy)''}]}', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'receiptDrgSup';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'receiptDrgSup', 'Receipt of Drug Supplies', 
		    'N', 527, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drgStorage';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'drgStorage', 'Storage', 
		    'N', 529, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'retDestStdDrg';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'retDestStdDrg', 'Return/Destruction of Study Drug', 
		    'N', 531, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'stdyDrgDispn';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'stdyDrgDispn', 'Dispensing of Study Drug', 
		    'N', 530, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'devMngntFile';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'devMngntFile', 'Do you have a device management file?', 
		    'N', 532, 'dropdown', '<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'coopGrpNum';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'coopGrpNum', 'Cooperative Group Number', 
		    'N', 560, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'consrtmNum';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'consrtmNum', 'Consortium Number', 
		    'N', 561, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nciEtctnNum';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'nciEtctnNum', 'NCI ETCTN Number', 
		    'N', 562, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'randomization' AND CODELST_SUBTYP = 'bayesian';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'randomization', 'bayesian', 'Bayesian Model Averaging, Continuous Reassessment Method', 
		    'N', 3, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'randomization' AND CODELST_SUBTYP = 'BMA-CRM';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'randomization', 'BMA-CRM', 'BMA-CRM', 
		    'N', 4, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'randomization' AND CODELST_SUBTYP = 'blckRndmztn';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'randomization', 'blckRndmztn', 'Block Randomization', 
		    'N', 5, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'randomization' AND CODELST_SUBTYP = 'effTox';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'randomization', 'effTox', 'EffTox Dose-finding', 
		    'N', 6, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'randomization' AND CODELST_SUBTYP = 'equlRndmztn';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'randomization', 'equlRndmztn', 'Equal Randomization', 
		    'N', 7, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'randomization' AND CODELST_SUBTYP = 'lstRndmztn';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'randomization', 'lstRndmztn', 'List Randomization', 
		    'N', 8, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'randomization' AND CODELST_SUBTYP = 'PocockRndm';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'randomization', 'PocockRndm', 'Pocock-Simon Stratified Randomization', 
		    'N', 10, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'blinding' AND CODELST_SUBTYP = 'blinded';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'blinding', 'blinded', 'Blinded', 
		    'N', 1, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'blinding' AND CODELST_SUBTYP = 'prtlBlnd';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'blinding', 'prtlBlnd', 'Partial-Blind', 
		    'N', 4, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'subm_status' AND CODELST_SUBTYP = 'unlock_med';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'subm_status', 'unlock_med', 'Unlock Study for medAncRevPend', 
		    'N', NULL, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'subm_status' AND CODELST_SUBTYP = 'unlock_irb';
	IF (v_CodeExists = 0) THEN
INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE)
VALUES ( SEQ_ER_CODELST.NEXTVAL, 'subm_status' , 'unlock_irb', 'Unlock Study for irb_add_info','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'subm_status' AND CODELST_SUBTYP = 'unlock_crc';
	IF (v_CodeExists = 0) THEN
INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE)
VALUES (SEQ_ER_CODELST.NEXTVAL, 'subm_status' , 'unlock_crc', 'Unlock Study for crcReturn','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'prepAdmDrgPkg';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'prepAdmDrgPkg', 'Preparation and Administration of Drug and Packaging', 
		    'N', 528, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'devSupMfgr';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'devSupMfgr', 'Device Supplier/Manufacturer', 
		    'N', 533, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'devStorage';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'devStorage', 'Storage of Device', 
		    'N', 535, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'receiptDevice';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'receiptDevice', 'Receipt of Device', 
		    'N', 534, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'returnDevice';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'returnDevice', 'Return of Device', 
		    'N', 536, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'labCorrStdies';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'labCorrStdies', 'Laboratory Correlative Studies', 
		    'N', 537, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'payProratd';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'payProratd', 'Will payments be prorated?', 
		    'N', 539, 'dropdown', '<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'subCompReimb';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'subCompReimb', 'Will subjects receive compensation or reimbursements?', 
		    'N', 538, 'dropdown', '<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'descrbProRatd';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'descrbProRatd', 'Describe', 
		    'N', 540, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'othPrRevFnd';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'othPrRevFnd', 'Other peer-reviewed funding', 
		    'N', 541, 'dropdown', '<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "option1">Industry</OPTION>
		<OPTION value = "option2">Departmental Funds</OPTION>
		<OPTION value = "option3">Donor Funds</OPTION>
		<OPTION value = "option4">Unfunded</OPTION>
		</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'othrNumbrs';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'othrNumbrs', '<h4>Other Numbers</h4>', 
		    'N', 543, 'splfld', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'siteNumber';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'siteNumber', 'Site Number', 
		    'N', 544, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'ps_cfs';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'ps_cfs', 'PS CFS (Resource One)', 
		    'N', 545, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fredNum';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'fredNum', 'FReD#', 
		    'N', 546, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'labTestsHead';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'labTestsHead', '<h4>Laboratory Tests</h4>', 
		    'N', 547, 'splfld', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'labTstPatMat';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'labTstPatMat', 'Will laboratory tests be performed on patient materials?', 
		    'N', 548, 'dropdown', '<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'cliaAttch';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'cliaAttch', 'If Other, provide name of the performing laboratory identification and contact information (attach CLIA certificate)', 
		    'N', 551, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'patEnrEligTrtmt';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'patEnrEligTrtmt', 'Will these tests be used for diagnosis, prevention, or treatment or health assessment purposes, including biomarker or genomic tests, for determining patient enrollment/eligibility/treatment assignment?', 
		    'N', 549, 'dropdown', '<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'patEnrEliTrtLab';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'patEnrEliTrtLab', 'Please select the laboratories that will be used for conducting tests for diagnosis, prevention, or treatment or health assessment purposes, including biomarker or genomic tests, for determining patient enrollment/eligibility/treatment assignment', 
		    'N', 550, 'checkbox', '{chkArray:[{data: ''option1'', display:''Division of Pathology & Laboratory Medicine (CLIA Certified Laboratory)''},
		{data: ''option2'', display:''Cellular Therapies CLIA Certified Laboratory (Department of Stem Cell Therapy CLIA Certified Laboratory)''},
		{data: ''option3'', display:''Other''}]}', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'namePurTest';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'namePurTest', 'Provide the name of the test(s), the purpose of the test, and the patient materials required (e.g. archived tissue, slides, and/or blood)', 
		    'N', 552, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'patMatTestOthr';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'patMatTestOthr', 'Specify Other', 
		    'N', 554, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'patMatTest';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'patMatTest', 'What type of patient materials will be collected/used for test for diagnosis, prevention or treatment or health assessment purposes, including for determining Patient enrollment/eligibility/treatment assignment?', 
		    'N', 553, 'checkbox', '{chkArray:[{data: ''option1'', display:''Archived pathology material (e.g. tissue sections on unstained slides)''},
		{data: ''option2'', display:''Fresh Tissue in media''},
		{data: ''option3'', display:''Fresh Frozen Tissue''},
		{data: ''option4'', display:''Serum/Blood''},
		{data: ''option5'', display:''Other body fluids''}]}', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'patMatResLab';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'patMatResLab', 'Will patient materials (blood, tissue sections on unstained slides, slides) need to be sent to a research (non-CLIA certified) laboratory?', 
		    'N', 555, 'dropdown', '<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'patMatResLabY2';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'patMatResLabY2', 'Is this a research non-CLIA laboratory outside of MD Anderson?', 
		    'N', 557, 'dropdown', '<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'patMatResLabY1';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'patMatResLabY1', 'Provide the laboratory name and address that specimens will be sent to', 
		    'N', 556, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'patMatResLabY3';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'patMatResLabY3', 'What type of patient materials need to be sent to a research (non-CLIA certified) laboratory?', 
		    'N', 558, 'checkbox', '{chkArray:[{data: ''option1'', display:''Archived pathology material (e.g. tissue sections on unstained slides)''},
		{data: ''option2'', display:''Fresh Tissue''},
		{data: ''option3'', display:''Serum/Blood''},
		{data: ''option4'', display:''Other body fluids''}]}', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nonCliaOthr';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'nonCliaOthr', 'Specify Other', 
		    'N', 559, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'rev_board' AND CODELST_SUBTYP = 'ancillary';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'rev_board', 'ancillary', 'Ancillary Review', 
		    'N', 6, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'rev_board' AND CODELST_SUBTYP = 'medical';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'rev_board', 'medical', 'Medical Review', 
		    'N', 7, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'rev_board' AND CODELST_SUBTYP = 'vp_cr';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'rev_board', 'vp_cr', 'Vice President of Clinical Research', 
		    'N', 1, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'rev_board' AND CODELST_SUBTYP = 'irb_cc';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'rev_board', 'irb_cc', 'IRB Committee Chair', 
		    'N', 4, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'rev_board' AND CODELST_SUBTYP = 'crc_coord';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'rev_board', 'crc_coord', 'CRC Coordinator', 
		    'N', 3, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'rev_board' AND CODELST_SUBTYP = 'irb_coord';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'rev_board', 'irb_coord', 'IRB Coordinator', 
		    'N', 5, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'rev_board' AND CODELST_SUBTYP = 'crc_cc';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'rev_board', 'crc_cc', 'CRC Committee Chair', 
		    'N', 2, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'medAncRevPend';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studystat', 'medAncRevPend', 'Medical or Ancillary Review Pending', 
		    'N', 305, NULL, 'departmental', 'default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'medAncRevCmp';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studystat', 'medAncRevCmp', 'Medical or Ancillary Review Completed', 
		    'N', 300, NULL, 'departmental', 'default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcInRev';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studystat', 'crcInRev', 'CRC in review', 
		    'N', 207, NULL, 'departmental', 'default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'irb_defer';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studystat', 'irb_defer', 'IRB: Deferred', 
		    'N', 132, NULL, 'irb', 'default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'irb_not_human';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studystat', 'irb_not_human', 'IRB: Not Human Research', 
		    'N', 143, NULL, 'irb', 'default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'irb_human';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studystat', 'irb_human', 'IRB: Human Research, Not Engaged', 
		    'N', 137, NULL, 'irb', 'default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'irb_in_rev';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studystat', 'irb_in_rev', 'IRB: In Review', 
		    'N', 139, NULL, 'irb', 'default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'irb_temp_hold';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studystat', 'irb_temp_hold', 'IRB: Temporary Hold', 
		    'N', 180, NULL, 'irb', 'default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'newdashboard' AND CODELST_SUBTYP = 'dbstudyreview';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'newdashboard', 'dbstudyreview', 'Study Review Cycle', 
		    'N', 2, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyvercat' AND CODELST_SUBTYP = 'dept_chair';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyvercat', 'dept_chair', 'Department Chair Review', 
		    'N', 5, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcTempHold';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studystat', 'crcTempHold', 'CRC temporary hold', 
		    'N', 227, NULL, 'departmental', 'default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'irb_terminated';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studystat', 'irb_terminated', 'IRB: Terminated', 
		    'N', 185, NULL, 'irb', 'default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'irb_withdrawn';
	IF (v_CodeExists = 0) THEN
		Insert into ERES.ER_CODELST
		   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
		    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
		 Values
		   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studystat', 'irb_withdrawn', 'IRB: Withdrawn', 
		    'N', 195, NULL, 'irb', 'default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'dataPoints1';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'dataPoints1', 'What types of data points will be collected, received or recorded?', 
    'N', 101, 'checkbox', '{chkArray:[{data: ''option1'', display:''Names or initials''},{data: ''option2'', display:''ALL geographic subdivisions smaller such as city, county, neighborhood''},{data: ''option3'', display:''All elements of dates smaller than a year (e.g., birth date, admission, discharge, death, etc.)''},{data: ''option4'', display:''Phone numbers''},{data: ''option5'', display:''Fax numbers''},{data: ''option6'', display:''E-mail addresses''},{data: ''option7'', display:''Social security numbers''},{data: ''option8'', display:''Medical record Number''},{data: ''option9'', display:''Health plan beneficiary number or code''},{data: ''option10'', display:''Any other account numbers''},{data: ''option11'', display:''Certificate/license numbers''},{data: ''option12'', display:''Vehicle identifiers''},{data: ''option13'', display:''Device identification numbers''},{data: ''option14'', display:''Web URL�s''},{data: ''option15'', display:''Internet IP address numbers''},{data: ''option16'', display:''Biometric identifiers (e.g., fingerprints, voice prints, retina scans, etc.)''},{data: ''option17'', display:''Full face photographs or comparable images''},{data: ''option18'', display:''Any other unique number, characteristic or code, such as employee ID number or student ID number''},{data: ''option19'', display:''None of the Above''}]}', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'describeText';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'describeText', 'Please describe: ', 
    'N', 102, 'input', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'describeRecord';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'describeRecord', 'Describe how data will be recorded', 
    'N', 103, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'inclusion';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'inclusion', 'Inclusion', 
    'N', 105, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'exclusion';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'exclusion', 'Exclusion', 
    'N', 106, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'juvinileElig';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'juvinileElig', 'Pediatrics', 
    'N', 107, 'dropdown', '<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT> ', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'elderElig';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'elderElig', 'Elderly', 
    'N', 108, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'pregEligiblity';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'pregEligiblity', 'Pregnant Women', 
    'N', 109, 'dropdown', '<SELECT id="alternateId"  NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'prisonorElig';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'prisonorElig', 'Prisoners', 
    'N', 110, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'dose';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'dose', 'Dose', 
    'N', 113, 'input', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'EKG';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'EKG', 'Will this trial require EKGs in which the sponsor will be providing an EKG machine?
', 
    'N', 120, 'checkbox', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'lab';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'lab', 'Are there any send out labs to a Central Laboratory?', 
    'N', 119, 'checkbox', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'cycles';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'cycles', 'How many cycles will be covered in the Coverage Analysis?', 
    'N', 118, 'input', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'requirements';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'requirements', '<i>Requirements for Medical Coverage of Routine Costs</>', 
    'N', 117, 'splfld', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'nonDrugStudy';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'nonDrugStudy', 'Non-Drug Study', 
    'N', 115, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'route';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'route', 'Route of Administration', 
    'N', 114, 'input', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'reimbursement';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'reimbursement', 'Will this trial require the sponsor to reimburse MDACC for commercially available drugs used as investigational agents?', 
    'N', 121, 'checkbox', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'blndnUnbln';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'blndnUnbln', 'Blinding/Unblinding', 
    'N', 125, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'independentSOC';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'independentSOC', 'If applicable, will research personnel draw all Non - Standard of Care (SOC) labs associated with the trial that are collected independently of SOC lab work?', 
    'N', 124, 'checkbox', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'radiology';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'radiology', 'Will this trial require the use of research- related items or services in Interventional Radiology?', 
    'N', 123, 'checkbox', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'diagImaging';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'diagImaging', 'Will this trial require the use of research- related items or services in Diagnostic Imaging?', 
    'N', 122, 'checkbox', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'biosafety1';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'biosafety1', 'Does the study involve the use of Recombinant DNA technology?', 
    'N', 51, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'biosafety2';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'biosafety2', 'Does this study involve the use of organisms that are infectious to humans?', 
    'N', 52, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'biosafety3';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'biosafety3', 'Does the study involve human/animal tissue other than blood derived hematopoietic stem cells?', 
    'N', 53, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
<OPTION value = "NA">N/A</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'cmpnsation1';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'cmpnsation1', 'Describe', 
    'N', 55, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'confdlty1';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'confdlty1', 'Will there be a link to identify subjects?', 
    'N', 57, 'dropdown', '<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'confdlty2';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'confdlty2', 'Describe how you will protect the subject''s identity.', 
    'N', 58, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'confdlty3';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'confdlty3', 'Describe provisions taken to maintain confidentiality of specimens/data.', 
    'N', 59, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'confdlty4';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'confdlty4', 'Describe the security plan for data including where data will be stored, how long the data will be stored and what will be don''t with the data once the study is terminated.', 
    'N', 60, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'confsecprod';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'confsecprod', 'Please confirm that these data will be collected, managed, and distributed in accordance with the M.D. Anderson Confidentiality Policy (ADM0264).', 
    'N', 61, 'checkbox', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'confsecprod1';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'confsecprod1', 'If No,  Please Describe:', 
    'N', 62, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'confsecprod2';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'confsecprod2', 'Select all data storage procedures/mechanisms that apply.', 
    'N', 63, 'checkbox', '{chkArray:[{data: ''option1'', display:''Paper records''},{data: ''option2'', display:''PC/Laptop/etc.''},{data: ''option3'', display:''External Storage (CD/Disk/USB Drive/etc)''},{data: ''option4'', display:''Server''}]}', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'confsecprod3';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'confsecprod3', 'Select all data storage procedures/mechanisms that apply.', 
    'N', 64, 'checkbox', '{chkArray:[{data: ''option1'', display:''Secured by locked file cabinet''},{data: ''option2'', display:''Secured by a locked office or other secured area.''},{data: ''option3'', display:''Secured by password protection''}]}', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'confsecprod4';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'confsecprod4', 'List other data storage types and procedures/mechanisms:', 
    'N', 65, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'fundSrc';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'fundSrc', 'Type of Support - Industry Funding, Grant, Other', 
    'N', 152, 'checkbox', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'spnsrName';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'spnsrName', 'Name of Sponsor, Supporter or Granting Agency:', 
    'N', 154, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'finSupp';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'finSupp', 'How is the proposed study to be financially supported?', 
    'N', 156, 'dropdown', '<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'spnsrshp';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'spnsrshp', 'Type(s) of support', 
    'N', 158, 'checkbox', '{chkArray:[{data: ''option1'', display:''Option 1''},{data: ''option2'', display:''Option 2''},{data: ''option3'', display:''Option 3''}]}', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'spnsrshp1';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'spnsrshp1', 'Indicate if the sponsor/supporter/granting agency will receive data', 
    'N', 160, 'checkbox', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'finSupp1';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'finSupp1', 'Type of funding: ', 
    'N', 162, 'dropdown', '<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "option1">NCI</OPTION>
<OPTION value = "option2">NIH (other than NCI)</OPTION>
<OPTION value = "option3">DOD</OPTION>
<OPTION value = "option4">Other peer-reviewed funding (e.g. NSF, ACS, etc.)</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'finSupp2';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'finSupp2', 'Source of Agent/Device:', 
    'N', 164, 'input', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'finSupp3';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'finSupp3', 'List any device(s) which will be used (if applicable):  ', 
    'N', 166, 'input', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'grant';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'grant', 'Grant Serial#/Contract#', 
    'N', 168, 'input', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'homeCare1';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'homeCare1', 'Specify what, if any, treatment may be given at home.', 
    'N', 172, 'input', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'intellProp1';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'intellProp1', 'Intellectual Property', 
    'N', 176, 'checkbox', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'intellProp2';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'intellProp2', '(If Yes) You may need to submit an Invention Disclosure Report to the Office of Technology Commercialization at 713-745-9602 or visit the Intranet site', 
    'N', 178, 'splfld', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'intellProp3';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'intellProp3', '1. Does this study include any agents, devices, or radioactive compound (or drug) manufactured at MD Anderson Cancer Center or by a contract manufacturer? ', 
    'N', 180, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'intellProp4';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'intellProp4', '(If Yes) 2. Is the technology licensed by MD Anderson Cancer Center? ', 
    'N', 182, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'intellProp5';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'intellProp5', '(If Yes) 4. Where is "drug" being manufactured?', 
    'N', 184, 'input', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'ide1';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'ide1', 'Intended as an implant? ', 
    'N', 188, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'ide2';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'ide2', 'Purported or represented to be for use supporting or sustaining human life? ', 
    'N', 190, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'ide3';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'ide3', 'For use of substantial importance in diagnosing, curing, mitigating, or treating disease, or otherwise preventing impairment of human health? ', 
    'N', 192, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'ide4';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'ide4', 'Has the device been modified in a manner not approved by the FDA?', 
    'N', 194, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
<OPTION value = "NA">N/A</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'ide5';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'ide5', 'Who will be responsible for the costs associated with the use of the device?', 
    'N', 196, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "PrtcpntOrThrdPrtyProv">Participant/Third Party Provider</OPTION>
<OPTION value = "Spnsr">Sponsor</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'break';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'break', '<br>', 
    'N', 197, 'splfld', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'nci';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'nci', 'Is this an NCI-Cancer Therapy Evaluation Protocol (CTEP)? ', 
    'N', 198, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'nci1';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'nci1', 'Is this an NCI-Division of Cancer Prevention Protocol (DCP)? ', 
    'N', 200, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'mdacc';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'mdacc', 'Is this MDACC Study Initiated?', 
    'N', 202, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'labtest';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'labtest', 'Is there any biomarker testing in this study being used to determine patient/participant eligibility, treatment assignment, or management of patient/participant care?', 
    'N', 204, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
<OPTION value = "NA">Not applicable for this protocol</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'stayDurationn';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'stayDurationn', 'Length of Stay', 
    'N', 206, 'input', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'mfg1';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'mfg1', 'Will you manufacture in full or in part (split manufacturing) a drug or biological product at the MD Anderson Cancer Center for the proposed clinical study?', 
    'N', 209, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'mta1';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'mta1', 'Materials Transfer Agreement (select one)', 
    'N', 211, 'dropdown', '<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'mta2';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'mta2', 'An MTA is a contract that governs the transfer of tangible research materials such as biological materials (e.g., reagents, cell lines, plasmids, and vectors).  MTAs may also be used for chemical compounds and even some types of software. Contact RESEARCH ADMINISTRATION (713-745-6633) for Assistance.', 
    'N', 212, 'splfld', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'nseq1';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'nseq1', 'Will non-MDACC approved equipment be used in this study? ', 
    'N', 222, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'nseq2';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'nseq2', 'Yes,Has a plan been devised for training personnel?', 
    'N', 224, 'checkbox', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'nseq3';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'nseq3', 'Describe the plan to train personnel.', 
    'N', 226, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'nseq4';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'nseq4', 'Has the Patient Care: Materials, Use, and Evaluation (MUE) Committee approved the non-standard equipment for use?', 
    'N', 228, 'checkbox', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'orderSet1';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'orderSet1', 'Will a research protocol require a physician order set?', 
    'N', 231, 'checkbox', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'preCli1';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'preCli1', 'Preclinical Drug Development', 
    'N', 234, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'protoMontr1';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'protoMontr1', 'Does this protocol have a schedule for interim analysis?', 
    'N', 236, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'dataSftyMntrPln';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'dataSftyMntrPln', 'Data and Safety Monitoring Plan', 
    'N', 237, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'protoMontr3';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'protoMontr3', 'Provide a summary or schedule of interim analysis', 
    'N', 238, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'protoMontr4';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'protoMontr4', 'Provide a rationale for no interim analysis', 
    'N', 239, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'radMat1';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'radMat1', 'Does the study involve the administration of radioisotopes or a radioisotope labeled agent?', 
    'N', 241, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
<OPTION value = "NA">N/A</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'resLoc1';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'resLoc1', 'Is MDACC the lead institution? ', 
    'N', 243, 'dropdown', '<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'resLoc2';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'resLoc2', 'If MDACC is the lead institution is this a consortium study? ', 
    'N', 244, 'dropdown', '<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'resloc3';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'resloc3', 'Are there international sites participating?', 
    'N', 245, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'resPop2';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'resPop2', 'Does this research include MDACC employees as participants?', 
    'N', 251, 'dropdown', '<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'resPop3';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'resPop3', 'Explain if the responses are anonymous, who will be recruiting, and when will recruitment occur', 
    'N', 252, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'ST';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'ST', 'Short Title', 
    'N', NULL, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'id_sponsor';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'id_sponsor', 'Sponsor Protocol Number', 
    'N', NULL, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'templateType';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'templateType', 'Select Template', 
    'N', 1, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "bioMed">Biomedical</OPTION>
<OPTION value = "socBehv">Social Behavioral</OPTION>
<OPTION value = "bioSpec">Biospecimen/Data Use</OPTION>
<OPTION value = "humUse">Compassionate Use/Humanitarian Use</OPTION>
<OPTION value = "emergUse">Emergency Use</OPTION>
<OPTION value = "nonHsr">Non-HSR</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'studyLocAddInfo';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'studyLocAddInfo', 'Name and location for sites where research will be conducted', 
    'N', 254, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'partPop1';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'partPop1', 'Expected Age Range of Participants:  ', 
    'N', 256, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'specimen1';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'specimen1', 'Will this include embryonic stem cells?', 
    'N', 258, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'specimen2';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'specimen2', 'List the source of specimens/data:  (Select all that apply) (requires at least 1 choice)', 
    'N', 259, 'dropdown', '<SELECT NAME="alternateId"><OPTION value = "">Select an option</OPTION><OPTION value = "Yes">option1</OPTION><OPTION value = "No">option2</OPTION><OPTION value = "In Process">option3</OPTION></SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'specimen3';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'specimen3', 'Institutional Tissue Bank: (Select all that apply)', 
    'N', 260, 'dropdown', '<SELECT NAME="alternateId"><OPTION value = "">Select an option</OPTION><OPTION value = "Yes">option1</OPTION><OPTION value = "No">option2</OPTION><OPTION value = "In Process">option3</OPTION></SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'specimen4';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'specimen4', 'If any of the asterisked (*) choices are selected, please provide name of database or source of specimens/data: ', 
    'N', 261, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'resPop1';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'resPop1', 'Total Expected Number of Prospective Participants (for example, one participant may have 10 samples, 10 participants would have 100 samples):', 
    'N', 250, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'specimen5';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'specimen5', 'Will specimens/data be shared with an entity, person or organization outside of MD Anderson?', 
    'N', 262, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'specimen6';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'specimen6', 'If Yes,  Please list each entity, person or organization that the data will be shared with and indicate if a data use agreement or a materials transfer agreement has been implemented.', 
    'N', 263, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'specimen7';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'specimen7', 'Enter Name of Entity, Person or Organization Rationale for Data Sharing:  (Select All That Apply) (requires at least 1 choice)', 
    'N', 264, 'dropdown', '<SELECT NAME="alternateId"><OPTION value = "">Select an option</OPTION><OPTION value = "Yes">option1</OPTION><OPTION value = "No">option2</OPTION><OPTION value = "In Process">option3</OPTION></SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'specimen8';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'specimen8', 'Data analysis', 
    'N', 265, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'specimen9';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'specimen9', 'Requirement Procedures', 
    'N', 266, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'specimen10';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'specimen10', 'Testing', 
    'N', 267, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'specimen11';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'specimen11', 'Administrative Survey', 
    'N', 268, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'specimen12';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'specimen12', 'Development of New Product or Project', 
    'N', 269, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'specimen13';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'specimen13', 'Other: Please describe:', 
    'N', 270, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'specimen14';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'specimen14', 'Data Use Agreement: (Select One)', 
    'N', 271, 'dropdown', '<SELECT NAME="alternateId"><OPTION value = "">Select an option</OPTION><OPTION value = "Yes">Yes</OPTION><OPTION value = "No">No</OPTION><OPTION value = "In Process">In Process</OPTION></SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'specimen15';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'specimen15', 'A written agreement between a health care component and a person requesting a disclosure of protected health information (PHI) contained in a limited data set. Data use agreements must meet the requirements of limited data set procedure. Contact LEGAL SERVICES (713-745-6633) for Assistance', 
    'N', 272, 'splfld', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'icd1';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'icd1', 'Select process', 
    'N', 275, 'dropdown', '<SELECT NAME="alternateId"><OPTION value = "">Select an option</OPTION><OPTION value = "option1">Project will utilize a waiver of informed consent/authorization (e.g. subjects previously signed consent for future use of specimens/data)</OPTION><OPTION value = "option2">Project will utilize the questionnaire statement for surveys (attach survey or questionnaire)</OPTION><OPTION value = "option3">Project staff will obtain verbal consent (e.g. telephone or in person) (attach verbal script or information sheet)</OPTION></SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'icd2';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'icd2', 'Describe the consent process and the staff involved.', 
    'N', 276, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'icd4';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'icd4', 'Remember to compose Research Information Document for Exempt Research Long & Short PA will include this choice:', 
    'N', 278, 'dropdown', '<SELECT NAME="alternateId"><OPTION value = "">Select an option</OPTION><OPTION value = "option1">Project staff will obtain prospective written consent</OPTION><OPTION value = "option2"> Other</OPTION></SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'icd5';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'icd5', 'If selected ''Other'', please explain', 
    'N', 279, 'textbox', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'fundSrc1';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'fundSrc1', 'Does the Study have a Sponsor Supporter or Granting Agency? ', 
    'N', 281, 'dropdown', '<SELECT NAME="alternateId"><OPTION value = "">Select an option</OPTION><OPTION value = "Yes">Yes</OPTION><OPTION value = "No">No</OPTION></SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'fundSrc2';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'fundSrc2', 'Supporter', 
    'N', 282, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'fundSrc3';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'fundSrc3', 'Type of Support', 
    'N', 283, 'dropdown', '<SELECT NAME="alternateId"><OPTION value = "">Select an option</OPTION><OPTION value = "option1">Industry Funding</OPTION><OPTION value = "option2">Grant</OPTION><OPTION value = "option3">Other</OPTION></SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'resDesc1';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'resDesc1', 'Does this project involve analysis or collection of data relating to the performance of clinical procedures, interventions, or therapies?', 
    'N', 286, 'dropdown', '<SELECT NAME="alternateId"><OPTION value = "">Select an option</OPTION><OPTION value = "option1">Yes</OPTION><OPTION value = "option2">No</OPTION></SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'resDesc2';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'resDesc2', 'Are any of the clinical procedures, interventions, or therapies being analyzed considered non-standard of care at the MD Anderson main campus? ', 
    'N', 287, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'audtnInspctn';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'audtnInspctn', 'Auditing and Inspecting', 
    'N', 292, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'accrual1';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'accrual1', 'Estimated number enrolled per month at MDACC', 
    'N', 294, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'accrual2';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'accrual2', 'Total number enrolled at MDACC', 
    'N', 295, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'accrual5';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'accrual5', 'Total number screened at MDACC', 
    'N', 298, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'accrual6';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'accrual6', 'Estimated number screened per month at MDACC', 
    'N', 299, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'accrual7';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'accrual7', 'Total number screened at all sites', 
    'N', 300, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'accrual8';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'accrual8', 'Accrual comments', 
    'N', 301, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'accrual9';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'accrual9', 'Only at other sites, Is this a cooperative study?', 
    'N', 302, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'accrual10';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'accrual10', 'Cooperative Group Name', 
    'N', 303, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'accrual11';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'accrual11', 'Principal Investigator for the Cooperative Group', 
    'N', 304, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'accrual12';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'accrual12', 'Total number enrolled at all sites', 
    'N', 305, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'statCons1';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'statCons1', 'Primary Endpoints', 
    'N', 307, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'statCons2';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'statCons2', 'Sample size justification', 
    'N', 308, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'statCons3';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'statCons3', 'Study Design', 
    'N', 309, '(null)', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'statCons4';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'statCons4', 'Stopping rules for futility, efficacy, and/or toxicity', 
    'N', 310, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'studyChair';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'studyChair', 'Study Chair', 
    'N', 313, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'secObjective';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'secObjective', 'Secondary Objective', 
    'N', 314, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'stdyRationale';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'stdyRationale', 'Study Rationale', 
    'N', 315, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'treatmnts';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'treatmnts', 'Treatment Agents/Devices/Interventions
', 
    'N', 316, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'dsmbInt';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'dsmbInt', 'Internal', 
    'N', 317, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'resLoc6';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'resLoc6', 'Is this a cooperative study?', 
    'N', 245, 'dropdown', '<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'resLoc7';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'resLoc7', 'Collaborating Site and Institution IRB Approval Letter', 
    'N', 246, 'checkbox', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'indReqYes';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'indReqYes', 'Who is the IND Holder/Regulatory Sponsor?', 
    'N', 319, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'indReq';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'indReq', 'Does this protocol require an IND?', 
    'N', 320, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'ind3';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'ind3', 'IND Required, please attach the Investigator�s Brochure', 
    'N', 321, 'splfld', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'ind4';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'ind4', 'No IND Required, Select Exemption', 
    'N', 322, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'ide6';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'ide6', 'Does this study utilize an Investigational Device?', 
    'N', 324, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
<OPTION value = "NA">N/A</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'ide7';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'ide7', 'If No or N/A, Comments', 
    'N', 325, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'ide8';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'ide8', 'Yes, Name of Device', 
    'N', 326, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'ide9';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'ide9', 'Yes, Manufacturer', 
    'N', 327, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'ide10';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'ide10', 'Risk Assessment Attachment (from sponsor or manufacturer)', 
    'N', 328, 'checkbox', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'ide11';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'ide11', 'Is the study being conducted under an Investigational Device Exemption (IDE)? ', 
    'N', 329, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'ide12';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'ide12', 'Exemption, IDE Holder', 
    'N', 330, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'ide13';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'ide13', 'Exemption,IDE Number', 
    'N', 331, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'ide14';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'ide14', 'Exemption, Please Attach the FDA Approval Letter', 
    'N', 332, 'splfld', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'traineeInfo1';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'traineeInfo1', 'Is this research being conducted as a partial fulfillment for completion of a degree?', 
    'N', 334, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'traineeInfo2';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'traineeInfo2', 'Please provide the following information for the Thesis/Dissertation Project Committee that is overseeing this research', 
    'N', 335, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'traineeInfo3';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'traineeInfo3', 'Name of Chair or Advisor', 
    'N', 336, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'traineeInfo4';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'traineeInfo4', 'Institution Name', 
    'N', 337, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'traineeInfo5';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'traineeInfo5', 'Address', 
    'N', 338, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'traineeInfo6';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'traineeInfo6', 'Telephone Number', 
    'N', 339, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'traineeInfo7';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'traineeInfo7', 'Email Address', 
    'N', 340, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'typeOfStudy';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'typeOfStudy', 'What is the type of study?', 
    'N', 341, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'studyCond1';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'studyCond1', 'Role of MDACC Principal Investigator', 
    'N', 343, 'checkbox', '{chkArray:[{data: ''option1'', display:'' Major role in writing the protocol''},
{data: ''option2'', display:''Assisted in design of the study''},
{data: ''option3'', display:''Study Principal Investigator (including anticipated first-author)''},
{data: ''option4'', display:''Study Co-Principal Investigator (including anticipated co-author)''},
{data: ''option5'', display:''Participation only (justification for M.D. Anderson participation only as required)''}]}', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'studyCond2';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'studyCond2', 'Administrative Department', 
    'N', 344, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'studyCond3';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'studyCond3', 'Who initiated this protocol?', 
    'N', 345, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "MDAACInv">MDACC Investigator</OPTION>
<OPTION value = "NCINIH">NCI/NIH</OPTION>
<OPTION value = "IndstryOrOthr">Industry/Other</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'studyCond4';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'studyCond4', 'Department', 
    'N', 346, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'studyCond5';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'studyCond5', 'Phone', 
    'N', 347, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'studyCond6';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'studyCond6', 'HR Data- Division', 
    'N', 348, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'studyCond7';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'studyCond7', 'Co-Investigators', 
    'N', 349, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'studyCond8';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'studyCond8', 'Study Chairperson Email address', 
    'N', 350, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'treatLoc';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'treatLoc', 'Location of Treatment', 
    'N', 351, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'retVisits';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'retVisits', 'How often must participants come to MDACC?', 
    'N', 352, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'hmeCare';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'hmeCare', 'Specify what, if any, treatment may be given at home.', 
    'N', 353, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'pubDisp';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'pubDisp', 'Public Display', 
    'N', 354, 'checkbox', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'studyTeam1';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'studyTeam1', 'Name of Person at MDACC Responsible for Data Management:', 
    'N', 356, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'studyTeam2';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'studyTeam2', 'Additional Contact', 
    'N', 357, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'studyTeam3';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'studyTeam3', 'Study Team: Roles', 
    'N', 358, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'studyTeam4';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'studyTeam4', 'Collaborators', 
    'N', 359, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'finSupp4';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'finSupp4', 'Which Cancer Center Support Grant (CCSG) - NCI Core Grant Program does this protocol relate to?', 
    'N', 164, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'proDes1';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'proDes1', 'Did you participate in the design of this study?', 
    'N', 361, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "YesComp">Yes -- Completely</OPTION>
<OPTION value = "YesPart">Yes -- Partially</OPTION>
<OPTION value = "NoSponsrDes">No -- Sponsor Designed</OPTION>
<OPTION value = "NoProsStat">No prospective statistical design in this study</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'proDes2';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'proDes2', 'Did an MDACC Biostatistician participate in the design of this study?', 
    'N', 362, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "YesComp">Yes -- Completely</OPTION>
<OPTION value = "YesPart">Yes -- Partially</OPTION>
<OPTION value = "NoSponsrDes">No -- Sponsor provided biostatistical design</OPTION>
<OPTION value = "NoProsStat">No prospective statistical design in this study</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'ctrc1';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'ctrc1', 'Will the Clinical and Translational Research Center (CTRC) be used in this study?', 
    'N', 364, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'gmpFacility';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'gmpFacility', 'Will the protocol require the services of the Cell Therapy Lab Good Manufacturing Practice (GMP) facility or products to be manufactured using the GMP facility?', 
    'N', 365, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'labData1';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'labData1', 'Will a UTMDACC investigator perform pharmacologic, molecular or other laboratory studies on patient specimens?', 
    'N', 367, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'labData2';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'labData2', 'Yes, What type of tissues will be used?', 
    'N', 368, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'labData3';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'labData3', 'What is the type of study?', 
    'N', 369, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'bioMark1';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'bioMark1', 'Are Biological Markers being evaluated in this study?', 
    'N', 371, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'bioMark2';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'bioMark2', 'Yes, What type of tissues will be used?', 
    'N', 372, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'bioMark3';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'bioMark3', 'Yes,The marker(s) are being evaluated in (check all that apply):', 
    'N', 373, 'dropdown', '<SELECT NAME="alternateId"><OPTION value = "">Select an option</OPTION><OPTION value = "option1">Option1</OPTION><OPTION value = "option2">Option2</OPTION><OPTION value = "option3">Option3</OPTION></SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'bioMark4';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'bioMark4', 'The marker(s) are being evaluated to predict (check all that apply):', 
    'N', 374, 'dropdown', '<SELECT NAME="alternateId"><OPTION value = "">Select an option</OPTION><OPTION value = "option1">Option1</OPTION><OPTION value = "option2">Option2</OPTION><OPTION value = "option3">Option3</OPTION></SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'bioMark5';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'bioMark5', 'Would there be an opportunity to consider marker evaluation as a component of this study if necessary funding were available?', 
    'N', 375, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'priorProto';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'priorProto', 'Prior protocol at M. D. Anderson:', 
    'N', 376, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'addnMemoRecpnt';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'addnMemoRecpnt', 'Additional Memo Recipients', 
    'N', 377, '(null)', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'unit';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'unit', 'Unit', 
    'N', 378, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'protoType';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'protoType', 'Protocol Type', 
    'N', 379, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'protoPhase';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'protoPhase', 'Protocol Phase', 
    'N', 380, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'versionStatus';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'versionStatus', 'Version Status', 
    'N', 381, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'bioPharm';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'bioPharm', 'Is this a Pharma/Biotech sponsored trial that requires research biopsies and/or imaging?', 
    'N', 382, 'checkbox', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'specCategories';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'specCategories', 'Does this research fit into one of the following categories below:
1. Phase 1 first in man trial of novel agents or devices
2. Phase 1 trial of a novel agent or device
3�..', 
    'N', 383, 'checkbox', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'discountOffer';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'discountOffer', 'Your protocol may be eligible for the ReCINT program funding, would you like to apply for the discount rate of 59% to research biopsies and/or scan charges?', 
    'N', 384, 'checkbox', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'icd6';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'icd6', 'Are you requiring subjects to sign a consent form for this protocol?', 
    'N', 279, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'ccsg';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'ccsg', 'CCSG Study Source', 
    'N', 244, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "ExtPeerRev">Externally Peer-Reviewed</OPTION>
<OPTION value = "Industrial">Industrial</OPTION>
<OPTION value = "Institutional">Institutional</OPTION>
<OPTION value = "National">National</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'doseModfctn';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'doseModfctn', 'Dose Modification', 
    'N', 385, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'sysGenCommRev';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'sysGenCommRev', 'Committee Review Date', 
    'N', 386, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'sysGenIRBApprv';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'sysGenIRBApprv', 'IRB Approved Date', 
    'N', 387, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'sysGenSubm';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'sysGenSubm', 'Protocol Submission', 
    'N', 388, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'sysGenSubmBy';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'sysGenSubmBy', 'Submitted by', 
    'N', 389, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'medCov1';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'medCov1', 'Is the purpose of the study to evaluate an intervention or service that falls within a Medicare benefit category (e.g., physicians'' service, inpatient hospital services, diagnostic tests, etc.) and that is not excluded from coverage (e.g., cosmetic surgery, hearing aids, etc.)?', 
    'N', 390, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No -- the Study is Not a Qualifying Trial</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'medCov2';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'medCov2', 'Does the trial have therapeutic intent and is not designed to exclusively test toxicity or disease pathophysiology?', 
    'N', 391, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No -- the Study is Not a Qualifying Trial</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'medCov3';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'medCov3', 'Does the trial enroll patients with diagnosed disease (excepts include diagnostic clinical trials which may enroll healthy volunteers in control group)?', 
    'N', 392, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No -- the Study is Not a Qualifying Trial</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = '21CFR1';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', '21CFR1', 'Do you store your data electronically?', 
    'N', 393, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = '21CFR2';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', '21CFR2', 'Is your data used in IND, IDE or FDA regulated Activity?', 
    'N', 394, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = '21CFR3';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', '21CFR3', 'Is your data stored in Microsoft Excel or Microsoft Access?', 
    'N', 395, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = '21CFR4';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', '21CFR4', 'If Yes, skip questions #4-6 below', 
    'N', 396, 'splfld', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = '21CFR5';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', '21CFR5', 'What is the name of your database? ', 
    'N', 397, 'dropdown', '<SELECT NAME="alternateId"><OPTION value = "">Select an option</OPTION><OPTION value = "option1">Option1</OPTION><OPTION value = "option2">Option2</OPTION><OPTION value = "option3">Option3</OPTION></SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = '21CFR6';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', '21CFR6', 'If database name not found in the list please write the name of your database.', 
    'N', 398, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = '21CFR7';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', '21CFR7', 'Name, phone number and email address of your database technical contact.', 
    'N', 399, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'consentForm';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'consentForm', 'Are you requiring subjects to sign a consent form for this protocol?', 
    'N', 400, 'dropdown', '<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'consentFormType';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'consentFormType', 'If yes', 
    'N', 401, 'dropdown', '<SELECT NAME="alternateId"><OPTION value = "">Select an option</OPTION><OPTION value = "option1">Clinical Research Category Interventional (INT)</OPTION><OPTION value = "option2">Observational(OBS) Ancillary</OPTION><OPTION value = "option3">Ccorrelative (ANC/COR)</OPTION></SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'protApp1';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'protApp1', '1. Is the activity a systematic investigation of multiple human subjects, including research testing and evaluation?      Yes= proceed to #2, No=1a question below', 
    'N', 402, 'checkbox', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'protApp2';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'protApp2', '2. Is the activity designed to test a hypothesis, permit conclusions to be drawn, and thereby to develop or contribute to generalizable knowledge?  Yes= proceed to #3, No =2a question below', 
    'N', 403, 'checkbox', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'protApp3';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'protApp3', '3. Are all specimen/data collected from subjects who are known to be deceased?  Yes= proceed to #4, No = Proceed to exemption questions', 
    'N', 404, 'checkbox', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'protApp4';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'protApp4', '4. Will the research involve the collection of analysis of genetic information from specimens/data of deceased subjects?  Yes = proceed to exemption questions, No = question 4a below', 
    'N', 405, 'checkbox', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'protApp1a';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'protApp1a', '1a. Will the data be listed or described separately for each subject, ie, no aggregate data will be compiled? Yes= case report No= Practice of Medicine Provide summary or description accordingly. ', 
    'N', 406, 'checkbox', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'protApp2a';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'protApp2a', '2a. Will the data be listed or described separately for each subject, ie, no aggregate data will be compiled? Yes= case report No= Quality Improvement Provide description or summary accordingly. ', 
    'N', 407, 'checkbox', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'protApp4a';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'protApp4a', '4a. Will the research impact living individuals? Yes or No = Proceed to question No.5 ', 
    'N', 408, 'checkbox', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'protApp5';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'protApp5', '5. Please describe how you will verify that subjects are deceased?  NOT HUMAN SUBJECTS RESEARCH, Provide a description to verify Not huma subjects research', 
    'N', 409, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'exemp45CFR1';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'exemp45CFR1', '1. Will the research be conducted in established or commonly accepted educational settings, involving normal education practices?  (This may include schools,  colleges, and other sites where educational activities regularly occur.)   Yes or No= proceed to #2', 
    'N', 410, 'checkbox', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'exemp45CFR2';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'exemp45CFR2', ' 2. Will the research involve the use of educational tests, survey procedures, or observation of public behavior? Yes=  Question 2a. or No = Proceed to #4', 
    'N', 411, 'checkbox', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'exemp45CFR3';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'exemp45CFR3', '3. Not applicable to MDACC research (Contact the IRB if you feel this applies) ', 
    'N', 412, 'checkbox', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'exemp45CFR4';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'exemp45CFR4', '4. Does the research only involve the use or study of existing (already available or on the shelf) data, documents, records, or pathological or diagnostic specimens?   Yes= 4a question below proceed to #5, No = proceed to question 5', 
    'N', 413, 'checkbox', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'exemp45CFR5';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'exemp45CFR5', '5. Does the research evaluate or examine public benefit or service programs, such as demonstration or dissemination projects? Y/N= proceed to #6', 
    'N', 414, 'checkbox', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'exemp45CFR6';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'exemp45CFR6', '6. Does the research involve taset and food quality evaluations?  Y/N= proceed with Additional Information.', 
    'N', 415, 'checkbox', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'exemp45CFR2a';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'exemp45CFR2a', '2a.  Will the research involve survey procedures, interview procedures, or observation of public behavior where the investigatore or research staff partifcipates in the activities being observed?Yes= questions 2b No = proceed to to question #4', 
    'N', 416, 'checkbox', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'exemp45CFR2b';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'exemp45CFR2b', '2b. Will the information obtained be recorded in such a manner that human subjects can be identified, directly or through identifiers linked to the subjects?   Y/N  = proceed to question 4 ', 
    'N', 417, 'checkbox', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'exemp45CFR4a';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'exemp45CFR4a', '4a. Are the data, documents, records, or pathological or diagnostic specimens publicly available?   Yes = proceed to question 5 or no= question 4b, then proceed to #5', 
    'N', 418, 'checkbox', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'exemp45CFR4b';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'exemp45CFR4b', ' 4b.  Will the existing information be recorded in such a manner that human subjects can e identified, directly or through identifiers linked to the subjects? **(see help text for a list of identifiers) Y/N  proceed to question #5', 
    'N', 419, 'checkbox', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'protApp6';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'protApp6', 'If yes, Is the activity designed to test a hypothesis, permit conclusions to be drawn, and thereby to develop or contribute to generalizable knowledge?', 
    'N', 420, 'checkbox', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'protApp7';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'protApp7', 'If no, Will data be listed or described separately  for each subject, i.e., no aggregate data 
will be compiled?
Yes=Case report No=Practice of Medicine
', 
    'N', 421, 'checkbox', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'protApp8';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'protApp8', 'If yes to 217 Are all specimen/data collected from subjects who are known to be deceased?', 
    'N', 422, 'checkbox', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'resPop4';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'resPop4', 'Is there any direct interaction or direct observation of subjects?', 
    'N', 423, 'dropdown', '<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'resPop5';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'resPop5', 'Describe your direct interaction or direct observation of subjects', 
    'N', 424, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'resPop6';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'resPop6', '(from resPop5, proceed to next section) Location Where Research Will be Conducted', 
    'N', 425, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'resPop7';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'resPop7', 'If No= Skip to Section, Participant Population', 
    'N', 426, 'splfld', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'resPop8';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'resPop8', 'Location where research will be conducted', 
    'N', 427, 'checkbox', '{chkArray:[{data: ''option1'', display:''MDACC/Regional Centers''},
{data: ''option2'', display:''Non-MDACC Hospital/Clinic Healthcare Facility/Harris County Hospital District Facility''},
{data: ''option3'', display:''Community Center/Shopping Mall (e.g. religious facility)''},
{data: ''option4'', display:''Educational Campus (e.g. elementary or secondary school or a university campus''},
{data: ''option5'', display:"Subject''s Home (list information on city; county; state; or country where the home is located below)"},
{data: ''option6'', display:''Internet Research (list website names below)''},
{data: ''option7'', display:''International Site''},
{data: ''option8'', display:''Other''}
]}', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'resPop9';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'resPop9', 'Identify the name and location for each site selected above (list locations, Regional Cancer Centers, web sites, etc.)', 
    'N', 428, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'juvinileEligNo';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'juvinileEligNo', 'Studies that exclude children must have appropriate justification.  Please select all that apply: ', 
    'N', 428, 'checkbox', '{chkArray:[{data: ''option1'', display:''Phase I or Phase II study targeting cancer that is very unusual in pediatrics (e.g., prostate, lung, breast, chronic lymophocytic leukemia,etc.)''},
{data: ''option2'', display:''FDA has required sponsor to limit to age 18 and above (applicable only to Phase I studies).''},
{data: ''option3'', display:''Phase II or III study with no Phase I data for the drug in pediatrics. Please provide a letter from the Sponsor stating if a Phase I study planned for patients <18 years of age. (May include file attachment)''},
{data: ''option4'', display:''Phase I study to include participants with diseases that occur in pediatrics (e.g., leukemia, lymphoma, sarcomas, brain tumors), but sponsor will not allow patients <18 years of age. Please include a justification letter from the Sponsor. (May include file attachment)''},
{data: ''option5'', display:''Other''}
]}', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'juvinileEligNo1';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'juvinileEligNo1', 'If other:', 
    'N', 429, NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'biosafety4';	
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'biosafety4', 'If Yes, Please provide the IBC protocol number or contact the IBC coordinator for information at 713-563-3879.
IBC Protocol Number:', 
    'N', 430, 'textarea', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'biosafety5';
	IF (v_CodeExists = 0) THEN
Insert into ERES.ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 Values
   (SEQ_ER_CODELST.NEXTVAL, NULL, 'studyidtype', 'biosafety5', 'If yes, Are you using Tisseel or Evicel or similar?', 
    'N', 431, 'checkbox', NULL, NULL);	
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
COMMIT;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,318,930,'05_MSDscript2.sql',sysdate,'v9.3.0 #719');

commit;