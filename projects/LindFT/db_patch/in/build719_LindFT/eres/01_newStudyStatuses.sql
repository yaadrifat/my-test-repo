set define off;

declare
v_CodeExists NUMBER :=0;
begin
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'omcrReviewed';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studystat', 'omcrReviewed', 'OMCR Reviewed');
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'indReviewed';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studystat', 'indReviewed', 'IND Reviewed');
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'biostatReviewed';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studystat', 'biostatReviewed', 'Biostatistician Reviewed');
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'deptSignoff';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studystat', 'deptSignoff', 'Departmental Signoff');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcApproved';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studystat', 'crcApproved', 'CRC Approved');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'amendApproved';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studystat', 'amendApproved', 'Amendment Approved');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'irbRenewal';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studystat', 'irbRenewal', 'IRB Renewal Approved');
	END IF;

	commit;
end;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,318,930,'01_newStudyStatuses.sql',sysdate,'v9.3.0 #719');

commit;
