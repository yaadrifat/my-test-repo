set define off;

create or replace PACKAGE "ERES"."PKG_WORKFLOW_RULES_MDA"
IS
  FUNCTION F_studyInit_protDef(studyId NUMBER) RETURN NUMBER;
  
  FUNCTION F_studyInit_OMCR_Review(studyId NUMBER) RETURN NUMBER;

  FUNCTION F_studyInit_IND_Review(studyId NUMBER) RETURN NUMBER;
  
  FUNCTION F_studyInit_biostatReview(studyId NUMBER) RETURN NUMBER;
  
  FUNCTION F_studyInit_deptSignoff(studyId NUMBER) RETURN NUMBER;
  
  FUNCTION F_studyInit_crcReview(studyId NUMBER) RETURN NUMBER;
  
  FUNCTION F_studyInit_irbReview(studyId NUMBER) RETURN NUMBER;
  --------------------------------------------------------------------

  FUNCTION F_studyActive_protActivation(studyId NUMBER) RETURN NUMBER;
  --------------------------------------------------------------------
  
  FUNCTION F_studyConduct_protAmendment(studyId NUMBER) RETURN NUMBER;
  
  FUNCTION F_studyConduct_irbRenewal(studyId NUMBER) RETURN NUMBER;
  --------------------------------------------------------------------
  pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'PKG_WORKFLOW', pLEVEL  => Plog.LFATAL);
END PKG_WORKFLOW_RULES_MDA;
/

create or replace PACKAGE BODY "ERES"."PKG_WORKFLOW_RULES_MDA"
AS
  FUNCTION F_studyInit_protDef(studyId NUMBER) RETURN NUMBER
  AS
  v_RETURN_NO NUMBER := 0;
  BEGIN
    SELECT COUNT(*) INTO v_RETURN_NO from ER_STUDY where PK_STUDY= studyId;
    RETURN v_RETURN_NO;
  END;
  ------------------------------------------------------------------------------
  FUNCTION F_studyInit_OMCR_Review(studyId NUMBER) RETURN NUMBER
  AS
  v_statusExists NUMBER := 0;
  v_isMultiSite NUMBER := 0;
  v_RETURN_NO NUMBER := 0;
  BEGIN
    SELECT count(*) INTO v_statusExists FROM ER_STUDYSTAT WHERE FK_STUDY = studyId AND FK_CODELST_STUDYSTAT = (
    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'omcrReviewed');
        
    IF (v_statusExists > 0) THEN
      v_RETURN_NO := 1; --Done
    ELSE
      BEGIN
        SELECT COUNT(*) INTO v_isMultiSite from ER_STUDY where PK_STUDY= studyId AND FK_CODELST_SCOPE = (
        SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'bothMDAnOther');
        
        IF v_isMultiSite > 0 THEN
          v_RETURN_NO := 0; --Not Done
        ELSE 
          v_RETURN_NO := -1; --Not Required
        END IF;
      END;
    END IF;

    RETURN v_RETURN_NO;
  
  EXCEPTION WHEN NO_DATA_FOUND THEN
    BEGIN
      RETURN 0; --Not Done
    END;
  END;
  ------------------------------------------------------------------------------
  FUNCTION F_studyInit_IND_Review(studyId NUMBER) RETURN NUMBER
  AS
  v_statusExists NUMBER := 0;
  v_isHolderMDACC NUMBER := 0;
  v_RETURN_NO NUMBER := 0;
  BEGIN
    SELECT count(*) INTO v_statusExists FROM ER_STUDYSTAT WHERE FK_STUDY = studyId AND FK_CODELST_STUDYSTAT = (
    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'indReviewed');
        
    IF (v_statusExists > 0) THEN
      v_RETURN_NO := 1; --Done
    ELSE
      BEGIN
        SELECT COUNT(*) INTO v_isHolderMDACC from ER_STUDY_INDIDE where FK_STUDY= studyId AND FK_CODELST_INDIDE_HOLDER = (
        SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'INDIDEHolder' AND CODELST_SUBTYP = 'organization');
        
        IF v_isHolderMDACC > 0 THEN
          v_RETURN_NO := 0; --Not Done
        ELSE 
          v_RETURN_NO := -1; --Not Required
        END IF;
      END;
    END IF;

    RETURN v_RETURN_NO;

  EXCEPTION WHEN NO_DATA_FOUND THEN
    BEGIN
      RETURN 0; --Not Done
    END;
  END;
  ------------------------------------------------------------------------------
  
  FUNCTION F_studyInit_biostatReview(studyId NUMBER) RETURN NUMBER
  AS
  v_statusExists NUMBER := 0;
  v_isPiMajAuthor NUMBER := 0;

  v_RETURN_NO NUMBER := 0;
  BEGIN
    SELECT count(*) INTO v_statusExists FROM ER_STUDYSTAT WHERE FK_STUDY = studyId AND FK_CODELST_STUDYSTAT = (
    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'biostatReviewed');
        
    IF (v_statusExists > 0) THEN
      v_RETURN_NO := 1; --Done
    ELSE
      BEGIN
        SELECT COUNT(*) INTO v_isPiMajAuthor from ER_STUDY where PK_STUDY= studyId AND NVL(STUDY_MAJ_AUTH,'N') = 'Y';
        
        IF v_isPiMajAuthor > 0 THEN
          v_RETURN_NO := 0; --Not Done
        ELSE 
          v_RETURN_NO := -1; --Not Required
        END IF;
      END;
    END IF;

    RETURN v_RETURN_NO;

  EXCEPTION WHEN NO_DATA_FOUND THEN
    BEGIN
      RETURN 0; --Not Done
    END;
  END;
  ------------------------------------------------------------------------------
  
  FUNCTION F_studyInit_deptSignoff(studyId NUMBER) RETURN NUMBER
  AS
  v_statusExists NUMBER := 0;
  v_isPiMajAuthor NUMBER := 0;

  v_RETURN_NO NUMBER := 0;
  BEGIN
    SELECT count(*) INTO v_statusExists FROM ER_STUDYSTAT WHERE FK_STUDY = studyId AND FK_CODELST_STUDYSTAT = (
    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'deptSignoff');
        
    IF (v_statusExists > 0) THEN
      v_RETURN_NO := 1; --Done
    ELSE
      v_RETURN_NO := 0; --Not Done
    END IF;

    RETURN v_RETURN_NO;

  EXCEPTION WHEN NO_DATA_FOUND THEN
    BEGIN
      v_RETURN_NO := 0; --Not Done
      RETURN 0;
    END;
  END;
  ------------------------------------------------------------------------------
  
  FUNCTION F_studyInit_crcReview(studyId NUMBER) RETURN NUMBER
  AS
  v_statusExists NUMBER := 0;
  v_isPiMajAuthor NUMBER := 0;

  v_RETURN_NO NUMBER := 0;
  BEGIN
    SELECT count(*) INTO v_statusExists FROM ER_STUDYSTAT WHERE FK_STUDY = studyId AND FK_CODELST_STUDYSTAT = (
    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcApproved');
        
    IF (v_statusExists > 0) THEN
      v_RETURN_NO := 1; --Done
    ELSE
      v_RETURN_NO := 0; --Not Done
    END IF;

    RETURN v_RETURN_NO;

  EXCEPTION WHEN NO_DATA_FOUND THEN
    BEGIN
      v_RETURN_NO := 0; --Not Done
      RETURN 0;
    END;
  END;
  ------------------------------------------------------------------------------
  
  FUNCTION F_studyInit_irbReview(studyId NUMBER) RETURN NUMBER
  AS
  v_statusExists NUMBER := 0;
  v_isPiMajAuthor NUMBER := 0;

  v_RETURN_NO NUMBER := 0;
  BEGIN
    SELECT count(*) INTO v_statusExists FROM ER_STUDYSTAT WHERE FK_STUDY = studyId AND FK_CODELST_STUDYSTAT = (
    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'app_CHR');
        
    IF (v_statusExists > 0) THEN
      v_RETURN_NO := 1; --Done
    ELSE
      v_RETURN_NO := 0; --Not Done
    END IF;

    RETURN v_RETURN_NO;

  EXCEPTION WHEN NO_DATA_FOUND THEN
    BEGIN
      v_RETURN_NO := 0; --Not Done
      RETURN 0;
    END;
  END;
  ------------------------------------------------------------------------------
  ------------------------------------------------------------------------------
  
  FUNCTION F_studyActive_protActivation(studyId NUMBER) RETURN NUMBER
  AS
  v_statusExists NUMBER := 0;
  v_isPiMajAuthor NUMBER := 0;

  v_RETURN_NO NUMBER := 0;
  BEGIN
    SELECT count(*) INTO v_statusExists FROM ER_STUDYSTAT WHERE FK_STUDY = studyId AND FK_CODELST_STUDYSTAT = (
    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'active');
        
    IF (v_statusExists > 0) THEN
      v_RETURN_NO := 1; --Done
    ELSE
      v_RETURN_NO := 0; --Not Done
    END IF;

    RETURN v_RETURN_NO;

  EXCEPTION WHEN NO_DATA_FOUND THEN
    BEGIN
      v_RETURN_NO := 0; --Not Done
      RETURN 0;
    END;
  END;
  ------------------------------------------------------------------------------
  ------------------------------------------------------------------------------
  
  FUNCTION F_studyConduct_protAmendment(studyId NUMBER) RETURN NUMBER
  AS
  v_statusExists NUMBER := 0;
  v_isPiMajAuthor NUMBER := 0;

  v_RETURN_NO NUMBER := 0;
  BEGIN
    SELECT count(*) INTO v_statusExists FROM ER_STUDYSTAT WHERE FK_STUDY = studyId AND FK_CODELST_STUDYSTAT = (
    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'amendApproved');
        
    IF (v_statusExists > 0) THEN
      v_RETURN_NO := 1; --Done
    ELSE
      v_RETURN_NO := 0; --Not Done
    END IF;

    RETURN v_RETURN_NO;

  EXCEPTION WHEN NO_DATA_FOUND THEN
    BEGIN
      v_RETURN_NO := 0; --Not Done
      RETURN 0;
    END;
  END;
  ------------------------------------------------------------------------------
  
  FUNCTION F_studyConduct_irbRenewal(studyId NUMBER) RETURN NUMBER
  AS
  v_statusExists NUMBER := 0;
  v_isPiMajAuthor NUMBER := 0;

  v_RETURN_NO NUMBER := 0;
  BEGIN
    SELECT count(*) INTO v_statusExists FROM ER_STUDYSTAT WHERE FK_STUDY = studyId AND FK_CODELST_STUDYSTAT = (
    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'irbRenewal');
        
    IF (v_statusExists > 0) THEN
      v_RETURN_NO := 1; --Done
    ELSE
      v_RETURN_NO := 0; --Not Done
    END IF;

    RETURN v_RETURN_NO;

  EXCEPTION WHEN NO_DATA_FOUND THEN
    BEGIN
      v_RETURN_NO := 0; --Not Done
      RETURN 0;
    END;
  END;
  ------------------------------------------------------------------------------
END PKG_WORKFLOW_RULES_MDA;
/