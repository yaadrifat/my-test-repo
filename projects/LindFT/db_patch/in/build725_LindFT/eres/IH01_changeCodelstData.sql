set define off;
set serveroutput on;

declare
v_CodeExists number := 0;
begin
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nom_rev_id';	
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC,
		 CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'nom_rev_id', 'Nominated Reviewer User ID', 'readonly-input', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nom_alt_rev_id';	
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC,
		 CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'nom_alt_rev_id', 'Nominated Reviewer (alternate) User ID', 'readonly-input', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'app_CHR';
	IF (v_CodeExists = 0) THEN
		INSERT into ERES.ER_CODELST
		 (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, 
		  CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 		VALUES
   		 (SEQ_ER_CODELST.NEXTVAL, 'studystat', 'app_CHR', 'IRB: Approved', 
         'N', 130, 'browser', 'irb', 'default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		UPDATE ERES.ER_CODELST set CODELST_DESC='IRB: Approved', 
		 CODELST_CUSTOM_COL='browser', CODELST_CUSTOM_COL1='irb', CODELST_STUDY_ROLE='default_data' WHERE
		 CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'app_CHR';
		dbms_output.put_line('Row updated');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'rej_CHR';
	IF (v_CodeExists = 0) THEN
		INSERT into ERES.ER_CODELST
		 (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, 
		  CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE)
 		VALUES
   		 (SEQ_ER_CODELST.NEXTVAL, 'studystat', 'rej_CHR', 'IRB: Disapproved', 
         'N', 135, 'browser', 'irb', 'default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		UPDATE ERES.ER_CODELST set CODELST_DESC='IRB: Disapproved', 
		 CODELST_CUSTOM_COL='browser', CODELST_CUSTOM_COL1='irb', CODELST_STUDY_ROLE='default_data' WHERE
		 CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'rej_CHR';
		dbms_output.put_line('Row updated');
	END IF;
	
	COMMIT;
end;
/
