set define off;

update WORKFLOW_ACTIVITY  set wa_sequence = 2 where WA_NAME = 'Institutional Requirements' ;
update WORKFLOW_ACTIVITY  set wa_sequence = 3 where WA_NAME = 'Data Handling/ Record Keeping' ;
update WORKFLOW_ACTIVITY  set wa_sequence = 5 where WA_NAME = 'Departmental Signoff' ;
update WORKFLOW_ACTIVITY  set wa_sequence = 6 where WA_NAME = 'Biostat Review' ;
update WORKFLOW_ACTIVITY  set wa_sequence = 7 where WA_NAME = 'Collaborator Signoff' ;
update WORKFLOW_ACTIVITY  set wa_sequence = 8 where WA_NAME = 'Multicenter Review' ;
update WORKFLOW_ACTIVITY  set wa_sequence = 9 where WA_NAME = 'IND Review' ;
update WORKFLOW_ACTIVITY  set wa_sequence = 10 where WA_NAME = 'Nominated Reviewer Signoff' ;
update WORKFLOW_ACTIVITY  set wa_sequence = 11 where WA_NAME = 'CRC Review' ;
update WORKFLOW_ACTIVITY  set wa_sequence = 12 where WA_NAME = 'IRB Review' ;

commit;