SET DEFINE OFF;
set serveroutput on;

declare
v_ItemExists number := 0;
v_FK_LKPCOL number := 0;
v_MAX_LKPVIEWCOL number := 0;

begin
	SELECT COUNT(*) INTO v_ItemExists FROM ER_LKPCOL WHERE FK_LKPLIB = 6000 AND LKPCOL_NAME = 'usr_fullname';	
	IF (v_ItemExists = 0) THEN
		Insert into ERES.ER_LKPCOL
   		(PK_LKPCOL, FK_LKPLIB, LKPCOL_NAME, LKPCOL_DISPVAL, LKPCOL_TABLE, LKPCOL_KEYWORD)
 		Values
   		(SEQ_ER_LKPCOL.nextval, 6000, 'usr_fullname', 'Full Name', 'erv_usrprofile', 'USRFULLNAME');
		COMMIT;
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_ItemExists FROM ER_LKPCOL WHERE FK_LKPLIB = 6000 AND LKPCOL_NAME = 'usrlogin';	
	IF (v_ItemExists = 0) THEN
		Insert into ERES.ER_LKPCOL
   		(PK_LKPCOL, FK_LKPLIB, LKPCOL_NAME, LKPCOL_DISPVAL, LKPCOL_TABLE, LKPCOL_KEYWORD)
 		Values
   		(SEQ_ER_LKPCOL.nextval, 6000, 'usrlogin', 'User Login', 'erv_usrprofile', 'USRLOGIN');
		COMMIT;
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	select (max(PK_LKPVIEWCOL)+1) into v_MAX_LKPVIEWCOL from ER_LKPVIEWCOL;
	select PK_LKPCOL into v_FK_LKPCOL from ER_LKPCOL where FK_LKPLIB = 6000 and LKPCOL_NAME = 'usr_fullname';
	SELECT COUNT(*) INTO v_ItemExists from ER_LKPVIEWCOL where FK_LKPCOL = v_FK_LKPCOL and FK_LKPVIEW = 6000;
	IF (v_ItemExists = 0) THEN
		Insert into ERES.ER_LKPVIEWCOL
   		(PK_LKPVIEWCOL, FK_LKPCOL, LKPVIEW_IS_SEARCH, LKPVIEW_SEQ, FK_LKPVIEW, LKPVIEW_DISPLEN, LKPVIEW_IS_DISPLAY)
 		Values
   		(v_MAX_LKPVIEWCOL, v_FK_LKPCOL, 'N', 8, 6000, '5%', 'N');
		COMMIT;
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		
	select (max(PK_LKPVIEWCOL)+1) into v_MAX_LKPVIEWCOL from ER_LKPVIEWCOL;
	select PK_LKPCOL into v_FK_LKPCOL from ER_LKPCOL where FK_LKPLIB = 6000 and LKPCOL_NAME = 'usrlogin';
	SELECT COUNT(*) INTO v_ItemExists from ER_LKPVIEWCOL where FK_LKPCOL = v_FK_LKPCOL and FK_LKPVIEW = 6000;
	IF (v_ItemExists = 0) THEN
		Insert into ERES.ER_LKPVIEWCOL
   		(PK_LKPVIEWCOL, FK_LKPCOL, LKPVIEW_IS_SEARCH, LKPVIEW_SEQ, FK_LKPVIEW, LKPVIEW_DISPLEN, LKPVIEW_IS_DISPLAY)
 		Values
   		(v_MAX_LKPVIEWCOL, v_FK_LKPCOL, 'N', 9, 6000, '5%', 'N');
		COMMIT;
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
END;
/

