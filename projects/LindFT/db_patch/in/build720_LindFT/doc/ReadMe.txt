/********** ReadMe specific to v9.3.0 build #720_LindFT **********/
-------------------------------------------------------------------
1. Configuration of status filter in Drafts and Submissions
Go to eResearch_jboss510\conf\ folder and open configBundle_custom.properties in an editor.
Find this line:
Workflow_FlexStudy_WfTypes=STUDY_INITIATION,STUDY_ACTIVATION

Everything below that line should be subm_status related or study.savedItems. 
Replace all of them with the following set:
# -----------------------------------------------
study.savedItems=Not Submitted

# Subm status color codes
subm_status.submitted.colorcode=Pink
subm_status.resubmitted.colorcode=Salmon
subm_status.crc_submd.colorcode=PowderBlue
subm_status.crc_resubmd.colorcode=Lavender
subm_status.crc_in_rev.colorcode=RosyBrown
subm_status.crc_approved.colorcode=Thistle
subm_status.crc_disapproved.colorcode=YellowGreen
subm_status.crc_temp_hold.colorcode=PaleGoldenRod
subm_status.unlock_med.colorcode=DarkGoldenRod
subm_status.unlock_medcmp.colorcode=Yellow
subm_status.unlock_crc.colorcode=LimeGreen
subm_status.unlock_irb.colorcode=SandyBrown
subm_status.irb_in_rev_subm.colorcode=Khaki
subm_status.approved.colorcode=Gold
subm_status.rejected.colorcode=Wheat
subm_status.irb_defer_subm.colorcode=LightYellow
subm_status.irb_human_subm.colorcode=HoneyDew
subm_status.irbnothum_subm.colorcode=Beige
subm_status.irb_termin_subm.colorcode=LightGreen
subm_status.withdrawn.colorcode=LightGrey

# Hide these from protocol dashboard
#subm_status.submitted.display=N
#subm_status.resubmitted.display=N
subm_status.pi_respond.display=N
subm_status.pi_resp_req.display=N
subm_status.reviewer.display=N
subm_status.admin_review.display=N
subm_status.admin_rev_comp.display=N
subm_status.asgn_pr.display=N
subm_status.savedItems.display=N
# -----------------------------------------------

2. In this build we are releasing the Workflow activity icon click functionality.
For now, 'All Studies' form can be opened by clicking the Workflow activity icon. A new column 'WA_ONCLICK_JSON' is introduced in table WORKFLOW_ACTIVITY.

The icon onclick can be configured as below-
For an activity in a specific workflow, WA_ONCLICK_JSON column should contain a config JSON in following format.
{'moduleId':'ERES_FORM', 'moduleDet':{'formId':123}}

In the config JSON, 
#1. value for the key 'moduleId' has to be 'ERES_FORM'. This indicates that you plan to open a Velos eResearch form using the configuration.
#2. value for the key 'formId' should be the primary key of form (ER_FORMLIB.PK_FORMLIB) you intend to open using the configuration.
