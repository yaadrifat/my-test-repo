set define off;
set serveroutput on;

declare
v_CodeExists NUMBER :=0;
begin
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'subm_status' AND CODELST_SUBTYP = 'crc_submd';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'subm_status', 'crc_submd', 'CRC submitted', 5);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'subm_status' AND CODELST_SUBTYP = 'crc_resubmd';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'subm_status', 'crc_resubmd', 'CRC resubmitted', 15);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'subm_status' AND CODELST_SUBTYP = 'crc_in_rev';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_SEQ)
		VALUES ( SEQ_ER_CODELST.NEXTVAL, 'subm_status' , 'crc_in_rev', 'CRC in review', 20);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'subm_status' AND CODELST_SUBTYP = 'crc_approved';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_SEQ)
		VALUES ( SEQ_ER_CODELST.NEXTVAL, 'subm_status' , 'crc_approved', 'CRC approved', 30);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'subm_status' AND CODELST_SUBTYP = 'crc_disapproved';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_SEQ)
		VALUES ( SEQ_ER_CODELST.NEXTVAL, 'subm_status' , 'crc_disapproved', 'CRC disapproved', 35);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'subm_status' AND CODELST_SUBTYP = 'crc_temp_hold';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_SEQ)
		VALUES ( SEQ_ER_CODELST.NEXTVAL, 'subm_status' , 'crc_temp_hold', 'CRC temporary hold', 40);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'subm_status' AND CODELST_SUBTYP = 'unlock_medcmp';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_SEQ)
		VALUES ( SEQ_ER_CODELST.NEXTVAL, 'subm_status' , 'unlock_medcmp', 'Medical or Ancillary Review Completed', 50);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'subm_status' AND CODELST_SUBTYP = 'irb_in_rev_subm';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_SEQ)
		VALUES ( SEQ_ER_CODELST.NEXTVAL, 'subm_status' , 'irb_in_rev_subm', 'IRB: In Review', 55);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'subm_status' AND CODELST_SUBTYP = 'irb_human_subm';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_SEQ)
		VALUES ( SEQ_ER_CODELST.NEXTVAL, 'subm_status' , 'irb_human_subm', 'IRB: Human Research, Not Engaged', 80);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'subm_status' AND CODELST_SUBTYP = 'irb_defer_subm';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_SEQ)
		VALUES ( SEQ_ER_CODELST.NEXTVAL, 'subm_status' , 'irb_defer_subm', 'IRB: Deferred', 75);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'subm_status' AND CODELST_SUBTYP = 'irbnothum_subm';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_SEQ)
		VALUES ( SEQ_ER_CODELST.NEXTVAL, 'subm_status' , 'irbnothum_subm', 'IRB: Not Human Research', 85);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'subm_status' AND CODELST_SUBTYP = 'irb_termin_subm';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_SEQ)
		VALUES ( SEQ_ER_CODELST.NEXTVAL, 'subm_status' , 'irb_termin_subm', 'IRB: Terminated', 90);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

commit;

end;
/

