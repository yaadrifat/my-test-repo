set define off;

update ER_CODELST set CODELST_DESC = 'CRC submitting', CODELST_SEQ=1 where CODELST_TYPE = 'subm_status' and 
CODELST_SUBTYP = 'submitted';

update ER_CODELST set CODELST_DESC = 'CRC resubmitting', CODELST_SEQ=10 where CODELST_TYPE = 'subm_status' and 
CODELST_SUBTYP = 'resubmitted';

update ER_CODELST set CODELST_DESC = 'Medical or Ancillary Review Pending', CODELST_SEQ=45 where CODELST_TYPE = 'subm_status' and 
CODELST_SUBTYP = 'unlock_med';

update ER_CODELST set CODELST_DESC = 'IRB: Withdrawn', CODELST_SEQ=95 where CODELST_TYPE = 'subm_status' and 
CODELST_SUBTYP = 'withdrawn';

update ER_CODELST set CODELST_DESC = 'IRB: Approved', CODELST_SEQ=65 where CODELST_TYPE = 'subm_status' and 
CODELST_SUBTYP = 'approved';

update ER_CODELST set CODELST_DESC = 'IRB: Disapproved', CODELST_SEQ=70 where CODELST_TYPE = 'subm_status' and 
CODELST_SUBTYP = 'rejected';

update ER_CODELST set CODELST_DESC = 'CRC modifications required', CODELST_SEQ=25 where CODELST_TYPE = 'subm_status' and 
CODELST_SUBTYP = 'unlock_crc';

update ER_CODELST set CODELST_DESC = 'IRB: Modifications Required', CODELST_SEQ=60 where CODELST_TYPE = 'subm_status' and 
CODELST_SUBTYP = 'unlock_irb';

commit;
