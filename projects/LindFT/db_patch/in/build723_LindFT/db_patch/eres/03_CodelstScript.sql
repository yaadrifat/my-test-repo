set define off;
set serveroutput on;

declare
v_CodeExists number := 0;
begin
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crc_amend';	
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_SEQ,
		CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studystat', 'crc_amend', 'Amendment in draft', NULL, NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat_type' AND CODELST_SUBTYP = 'departmental';	
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_SEQ,
		CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studystat_type', 'departmental', 'Departmental', 10, NULL, 'default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat_type' AND CODELST_SUBTYP = 'irb';	
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_SEQ,
		CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studystat_type', 'irb', 'IRB', 15, NULL, 'default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcAppealed';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_SEQ,
		CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studystat', 'crcAppealed', 'CRC appealed', 175, 'departmental', 'default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcApproved';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_SEQ,
		CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studystat', 'crcApproved', 'CRC approved', 180, 'departmental', 'default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcDeferred';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_SEQ,
		CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studystat', 'crcDeferred', 'CRC deferred', 200, 'departmental', 'default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcDisapproved';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_SEQ,
		CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studystat', 'crcDisapproved', 'CRC disapproved', 205, 'departmental', 'default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcResubmitted';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_SEQ,
		CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studystat', 'crcResubmitted', 'CRC resubmitted', 215, 'departmental', 'default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcSentReviewer';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_SEQ,
		CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studystat', 'crcSentReviewer', 'CRC sent to reviewers', 220, 'departmental', 'default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcSubmitted';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_SEQ,
		CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studystat', 'crcSubmitted', 'CRC submitted', 225, 'departmental', 'default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'irb_work_start';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_SEQ,
		CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studystat', 'irb_work_start', 'IRB: Application Work Started', 125, 'irb', 'default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'app_CHR';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_SEQ,
		CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studystat', 'app_CHR', 'IRB: Approved', 130, 'irb', 'default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'rej_CHR';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_SEQ,
		CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studystat', 'rej_CHR', 'IRB: Disapproved', 135, 'irb', 'default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'irb_init_subm';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_SEQ,
		CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studystat', 'irb_init_subm', 'IRB: Initial Submission', 140, 'irb', 'default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'irb_add_info';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_SEQ,
		CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studystat', 'irb_add_info', 'IRB: Modifications Required', 142, 'irb', 'default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'pi_resp_rev';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_SEQ,
		CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studystat', 'pi_resp_rev', 'IRB: PI Response Sent to Reviewers', 145, 'irb', 'default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'irb_ammend';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_SEQ,
		CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studystat', 'irb_ammend', 'IRB: Submission - Amendment', 160, 'irb', 'default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'irb_renew';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_SEQ,
		CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studystat', 'irb_renew', 'IRB: Submission - Renewal', 165, 'irb', 'default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'sys_err_soa';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_SEQ,
		CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studystat', 'sys_err_soa', 'System Error - SOA', 500, 'irb', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'sys_err_click';
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_SEQ,
		CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studystat', 'sys_err_click', 'System Error - Click', 501, 'irb', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
COMMIT;
END;
/

update ER_CODELST set CODELST_STUDY_ROLE = NULL where CODELST_DESC = 'IRB Activities' and CODELST_TYPE = 'studystat_type';

update ER_OBJECT_SETTINGS set OBJECT_VISIBLE = 0 where OBJECT_NAME = 'study_menu' AND 
OBJECT_SUBTYPE = 'new_subm_menu' and OBJECT_VISIBLE = 1;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,322,930,'03_CodelstScript.sql',sysdate,'v9.3.0 #723LindFT4');

commit;
