/********** ReadMe specific to v9.3.0 build #727_LindFT custom package #8 **********/

1) To hide the "New" and "Search" menus under Manage > Studies for To-Do item #8, execute these statements:

update ER_OBJECT_SETTINGS set OBJECT_VISIBLE = 0 where OBJECT_NAME = 'manage_accnt' and OBJECT_SUBTYPE = 'study_menu';
update ER_OBJECT_SETTINGS set OBJECT_VISIBLE = 0 where OBJECT_NAME = 'study_menu' and OBJECT_SUBTYPE = 'new_menu';
update ER_OBJECT_SETTINGS set OBJECT_VISIBLE = 0 where OBJECT_NAME = 'study_menu' and OBJECT_SUBTYPE = 'open_menu';
commit;

2) To rename the 'Initial Details' tab for the P1 Bug (Bug 20708), execute:

update ER_OBJECT_SETTINGS set OBJECT_DISPLAYTEXT='Study Details' where 
OBJECT_NAME = 'irb_new_tab' and OBJECT_SUBTYPE = 'irb_init_tab';
commit;

3) To add the onlick json to the workflow activities:
Step1. get the primary key of the form by joining the er_formlib and er_linkedforms table
Step2. once you get the primary key for the required form , update the corresponding activity row in the workflow activity table , 
       setting the form id in the onclick json.
       In the onClick Json string, the value for the key "module Id" can be hardcoded to ERES_FORM .
	   The value for the key "formId" would be the primary key value retrieved in the select that happens before the update for each of the activities 
	   as seen in the script MT-01.updateWorkflowActvityOnClickJsonScript.sql
	   
To reuse the script for a different set of activities /forms 
1.change the value of the constant c_formName_1(or c_formName_2 or c_formName_3 or c_formName_4)to the new form that you want to open at the onclick of the corresponding activity.
2.change the value of the constant c_activity_Name_1(or c_activity_Name_2 or c_activity_Name_3 or c_activity_Name_4)to the name of the activity which you want to add the onclick json to.

	