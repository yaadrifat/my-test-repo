set define off;
set serveroutput on;

declare
v_ItemExists number := 0;
begin
	SELECT COUNT(*) INTO v_ItemExists FROM ER_OBJECT_SETTINGS WHERE OBJECT_NAME = 'irb_menu' AND OBJECT_SUBTYPE = 'study_menu';	
	IF (v_ItemExists = 0) THEN
		Insert into ERES.ER_OBJECT_SETTINGS
		   (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
		   OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
		 Values
		   (SEQ_ER_OBJECT_SETTINGS.nextval, 'M', 'study_menu', 'irb_menu', 10, 
		    1, 'All Protocols', 0);
	    COMMIT;
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
end;
/

update ER_OBJECT_SETTINGS set OBJECT_VISIBLE = 0 where OBJECT_NAME = 'study_menu' and OBJECT_SUBTYPE = 'new_menu';
update ER_OBJECT_SETTINGS set OBJECT_VISIBLE = 0 where OBJECT_NAME = 'study_menu' and OBJECT_SUBTYPE = 'open_menu';
update ER_OBJECT_SETTINGS set OBJECT_VISIBLE = 0 where OBJECT_NAME = 'manage_accnt' and OBJECT_SUBTYPE = 'study_menu';
update ER_OBJECT_SETTINGS set OBJECT_DISPLAYTEXT='Study Details' where 
OBJECT_NAME = 'irb_new_tab' and OBJECT_SUBTYPE = 'irb_init_tab';
update ER_CODELST set CODELST_DESC='HRPP submitted', CODELST_SEQ=295 where CODELST_SUBTYP = 'crcSubmitted' and CODELST_TYPE = 'studystat';		
update ER_CODELST set CODELST_DESC='HRPP resubmitted', CODELST_SEQ=294 where CODELST_SUBTYP = 'crcResubmitted' and CODELST_TYPE = 'studystat';
update ER_CODELST set CODELST_CUSTOM_COL1=
'<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<!--
<OPTION value = "coopGrp">Cooperative Group</OPTION>
<OPTION value = "consrtm">Consortium</OPTION>
<OPTION value = "nciEtctn">NCI ETCTN</OPTION>
-->
<OPTION value = "indstrStdy">Industry Study</OPTION>
<OPTION value = "MDACCInvIni">MDACC Investigator Initiated</OPTION>
</SELECT>'
where CODELST_SUBTYP='spnsrdProt' and CODELST_TYPE='studyidtype';

commit;
