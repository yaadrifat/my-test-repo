1) Label customization -- Add the following entries to labelBundle_custom.properties:

	L_App_Complete=Protocol is complete
	L_App_Incomplete=Protocol is incomplete
	
2) Message customization -- Add the following to messageBundle_custom.properties
	M_ResComp_NewUpldDocu=Upload Documents
	M_ResCompApp_UploadDoc=Upload Documents