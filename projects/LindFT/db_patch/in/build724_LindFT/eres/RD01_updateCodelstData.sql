set define off;
set serveroutput on;

declare
v_CodeExists number := 0;
begin
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'coopGrpNum';	
	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC,
		 CODELST_CUSTOM_COL CODELST_CUSTOM_COL1)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'coopGrpNum', 'Cooperative Group Number', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'consrtmNum';	
	IF (v_CodeExists = 0) THEN
		INSERT	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC,
		 CODELST_CUSTOM_COL CODELST_CUSTOM_COL1)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'consrtmNum', 'Consortium Number', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nciEtctnNum';	
	IF (v_CodeExists = 0) THEN
		INSERT	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC,
		 CODELST_CUSTOM_COL CODELST_CUSTOM_COL1)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'nciEtctnNum', 'NCI ETCTN Number', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'randomization' AND CODELST_SUBTYP = 'bayesian011';	
	IF (v_CodeExists = 0) THEN
		INSERT	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC,
		 CODELST_CUSTOM_COL CODELST_CUSTOM_COL1)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'randomization', 'bayesian', 'Bayesian Model Averaging, Continuous Reassessment Method', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'randomization' AND CODELST_SUBTYP = 'BMA-CRM';	
	IF (v_CodeExists = 0) THEN
		INSERT	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC,
		 CODELST_CUSTOM_COL CODELST_CUSTOM_COL1)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'randomization', 'BMA-CRM', 'BMA-CRM', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'randomization' AND CODELST_SUBTYP = 'blckRndmztn';	
	IF (v_CodeExists = 0) THEN
		INSERT	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC,
		 CODELST_CUSTOM_COL CODELST_CUSTOM_COL1)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'randomization', 'blckRndmztn', 'Block Randomization', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'randomization' AND CODELST_SUBTYP = 'effTox';	
	IF (v_CodeExists = 0) THEN
		INSERT	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC,
		 CODELST_CUSTOM_COL CODELST_CUSTOM_COL1)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'randomization', 'effTox', 'EffTox Dose-finding', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'randomization' AND CODELST_SUBTYP = 'equlRndmztn';	
	IF (v_CodeExists = 0) THEN
		INSERT	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC,
		 CODELST_CUSTOM_COL CODELST_CUSTOM_COL1)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'randomization', 'equlRndmztn', 'Equal Randomization', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'randomization' AND CODELST_SUBTYP = 'lstRndmztn';	
	IF (v_CodeExists = 0) THEN
		INSERT	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC,
		 CODELST_CUSTOM_COL CODELST_CUSTOM_COL1)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'randomization', 'lstRndmztn', 'List Randomization', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'randomization' AND CODELST_SUBTYP = 'PocockRndm';	
	IF (v_CodeExists = 0) THEN
		INSERT	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC,
		 CODELST_CUSTOM_COL CODELST_CUSTOM_COL1)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'randomization', 'PocockRndm', 'Pocock-Simon Stratified Randomization', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'blinding' AND CODELST_SUBTYP = 'blinded';	
	IF (v_CodeExists = 0) THEN
		INSERT	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC,
		 CODELST_CUSTOM_COL CODELST_CUSTOM_COL1)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'blinding', 'blinded', 'Blinded', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'blinding' AND CODELST_SUBTYP = 'prtlBlnd';	
	IF (v_CodeExists = 0) THEN
		INSERT	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC,
		 CODELST_CUSTOM_COL CODELST_CUSTOM_COL1)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'blinding', 'prtlBlnd', 'Partial-Blind', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'offStdyCri';	
	IF (v_CodeExists = 0) THEN
		INSERT	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC,
		 CODELST_CUSTOM_COL CODELST_CUSTOM_COL1)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'offStdyCri', 'Off Study Criteria', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'othrOffStdyCri';	
	IF (v_CodeExists = 0) THEN
		INSERT	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC,
		 CODELST_CUSTOM_COL CODELST_CUSTOM_COL1)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'othrOffStdyCri', 'Specify other criteria', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'OtrStdRemCrit';	
	IF (v_CodeExists = 0) THEN
		INSERT	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC,
		 CODELST_CUSTOM_COL CODELST_CUSTOM_COL1)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'OtrStdRemCrit', 'Specify other criteria', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'selOthrObjEntry';	
	IF (v_CodeExists = 0) THEN
		INSERT	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC,
		 CODELST_CUSTOM_COL CODELST_CUSTOM_COL1)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'selOthrObjEntry', 'Select other objectives for entry', 'checkbox', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'biomrkrObj';	
	IF (v_CodeExists = 0) THEN
		INSERT	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC,
		 CODELST_CUSTOM_COL CODELST_CUSTOM_COL1)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'biomrkrObj', 'Biomarker Objective', 'textarea', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'knownBiomrkr';	
	IF (v_CodeExists = 0) THEN
		INSERT	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC,
		 CODELST_CUSTOM_COL CODELST_CUSTOM_COL1)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'knownBiomrkr', 'Are they known Biomarkers?', 'dropdown', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'biomrkrType';	
	IF (v_CodeExists = 0) THEN
		INSERT	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC,
		 CODELST_CUSTOM_COL CODELST_CUSTOM_COL1)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'biomrkrType', 'Biomarker type', 'dropdown', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'biomrkrSpeOth';	
	IF (v_CodeExists = 0) THEN
		INSERT	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC,
		 CODELST_CUSTOM_COL CODELST_CUSTOM_COL1)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'biomrkrSpeOth', 'Specify other', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'testNameQ';	
	IF (v_CodeExists = 0) THEN
		INSERT	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC,
		 CODELST_CUSTOM_COL CODELST_CUSTOM_COL1)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'testNameQ', 'What are you calling the test? Use commonly accepted test.', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'ifGeneList';	
	IF (v_CodeExists = 0) THEN
		INSERT	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC,
		 CODELST_CUSTOM_COL CODELST_CUSTOM_COL1)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'ifGeneList', 'If gene, use www.genecards.org to pull name from this list.', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'biomrkrCall';	
	IF (v_CodeExists = 0) THEN
		INSERT	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC,
		 CODELST_CUSTOM_COL CODELST_CUSTOM_COL1)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'biomrkrCall', 'What are you calling the biomarker?', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'levelQues';	
	IF (v_CodeExists = 0) THEN
		INSERT	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC,
		 CODELST_CUSTOM_COL CODELST_CUSTOM_COL1)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'levelQues', 'What level are you trying to look at?', 'dropdown', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'measureQues';	
	IF (v_CodeExists = 0) THEN
		INSERT	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC,
		 CODELST_CUSTOM_COL CODELST_CUSTOM_COL1)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'measureQues', 'Where are you measuring?', 'checkbox', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'timePoints';	
	IF (v_CodeExists = 0) THEN
		INSERT	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC,
		 CODELST_CUSTOM_COL CODELST_CUSTOM_COL1)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'timePoints', 'Time points', 'checkbox', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'methodology';	
	IF (v_CodeExists = 0) THEN
		INSERT	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC,
		 CODELST_CUSTOM_COL CODELST_CUSTOM_COL1)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'methodology', 'Methodology- How?', 'textarea', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dsmbCompostn';	
	IF (v_CodeExists = 0) THEN
		INSERT	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC,
		 CODELST_CUSTOM_COL CODELST_CUSTOM_COL1)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'dsmbCompostn', 'DSMB Composition', 'textarea', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'bayesianComp';	
	IF (v_CodeExists = 0) THEN
		INSERT	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC,
		 CODELST_CUSTOM_COL CODELST_CUSTOM_COL1)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'bayesianComp', 'Bayesian component included in protocol', 'dropdown', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'expdBiostat';	
	IF (v_CodeExists = 0) THEN
		INSERT	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC,
		 CODELST_CUSTOM_COL CODELST_CUSTOM_COL1)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'expdBiostat', 'Are you requesting an expedited 
		biostatistical review for this protocol?', 'checkbox', NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'authrzdUsr';	
	IF (v_CodeExists = 0) THEN
		INSERT	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC,
		 CODELST_CUSTOM_COL CODELST_CUSTOM_COL1)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'authrzdUsr', 'Authorized User', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'authrztnNum';	
	IF (v_CodeExists = 0) THEN
		INSERT	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC,
		 CODELST_CUSTOM_COL CODELST_CUSTOM_COL1)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'authrztnNum', 'Authorization Number (or state that authorization application is under Radiation Safety Committee Review)', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'incldAdminstr';	
	IF (v_CodeExists = 0) THEN
		INSERT	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC,
		 CODELST_CUSTOM_COL CODELST_CUSTOM_COL1)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'incldAdminstr', '<br>Does this protocol include the administration of a radioactive compound (or drug) to a patient intended to obtain basic information regarding metabolism (including kinetics, distribution, and localization) of the drug or regarding human physiology, pathophysiology, or biochemistry, but not intended for immediate therapeutic, diagnostic, or similar purposes or to determine the safety and effectiveness of the drug in humans for such purposes (i.e. to carry out a clinical study)?', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'faciltyInvstg';	
	IF (v_CodeExists = 0) THEN
		INSERT	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC,
		 CODELST_CUSTOM_COL CODELST_CUSTOM_COL1)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'faciltyInvstg', 'Name the facility where the investigational agent is labeled or bound to the radioisotope (include the name of the laboratory at MDACC or the name of the company if outside MDACC)', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nihRacRev';	
	IF (v_CodeExists = 0) THEN
		INSERT	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC,
		 CODELST_CUSTOM_COL CODELST_CUSTOM_COL1)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'nihRacRev', 'Does the study require NIH/RAC review?', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nihRacRevComm';	
	IF (v_CodeExists = 0) THEN
		INSERT	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC,
		 CODELST_CUSTOM_COL CODELST_CUSTOM_COL1)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'nihRacRevComm', 'Comment', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'biosafety2Comm';	
	IF (v_CodeExists = 0) THEN
		INSERT	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC,
		 CODELST_CUSTOM_COL CODELST_CUSTOM_COL1)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'biosafety2Comm', 'Comment', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'invAntcpAEs';	
	IF (v_CodeExists = 0) THEN
		INSERT	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC,
		 CODELST_CUSTOM_COL CODELST_CUSTOM_COL1)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'invAntcpAEs', 'If investigational, provide list of anticipated Aes', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'commDrugInsrt';	
	IF (v_CodeExists = 0) THEN
		INSERT	IF (v_CodeExists = 0) THEN
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC,
		 CODELST_CUSTOM_COL CODELST_CUSTOM_COL1)
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'commDrugInsrt', 'If commercially available, include drug insert', NULL, NULL);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
COMMIT;
END;
/

UPDATE ER_CODELST
SET CODELST_DESC='Protocol Template', CODELST_CUSTOM_COL='dropdown',
CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "coopGrp">Cooperative Group</OPTION>
<OPTION value = "consrtm">Consortium</OPTION>
<OPTION value = "nciEtctn">NCI ETCTN</OPTION>
<OPTION value = "indstrStdy">Industry Study</OPTION>
<OPTION value = "MDACCInvIni">MDACC Investigator Initiated</OPTION>
</SELECT>'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='spnsrdProt';

UPDATE ER_CODELST
SET CODELST_CUSTOM_COL1='{chkArray:[
{data: "option1", display:"Drug"},
{data: "option2", display:"Device"},
{data: "option3", display:"Imaging"},
{data: "option4", display:"Surgical"},
{data: "option5", display:"Radiotherapy"},
{data: "option6", display:"Cell Therapeutic"},
{data: "option7", display:"Gene Therapy"}
]}'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='stdyIncludes';

UPDATE ER_CODELST
SET CODELST_DESC='Study Rationale'
WHERE CODELST_TYPE='section' AND CODELST_SUBTYP='section_3_2';

UPDATE ER_CODELST
SET CODELST_DESC='Pre-clinical Data'
WHERE CODELST_TYPE='section' AND CODELST_SUBTYP='section_1_2';

UPDATE ER_CODELST
SET CODELST_DESC='Clinical Data to Date'
WHERE CODELST_TYPE='section' AND CODELST_SUBTYP='section_1_3';

UPDATE ER_CODELST
SET CODELST_DESC='Other'
WHERE CODELST_TYPE='randomization' AND CODELST_DESC='Non - Randomized';

UPDATE ER_CODELST
SET CODELST_DESC='Adaptive Randomization'
WHERE CODELST_TYPE='randomization' AND CODELST_SUBTYP='randomized';

UPDATE ER_CODELST
SET CODELST_SUBTYP='other'
WHERE CODELST_TYPE='randomization' AND CODELST_DESC='Other';

UPDATE ER_CODELST
SET CODELST_SUBTYP='none'
WHERE CODELST_TYPE='randomization' AND CODELST_DESC='None';

UPDATE ER_CODELST
SET CODELST_SEQ='1'
WHERE CODELST_TYPE='randomization' AND CODELST_SUBTYP='none';

UPDATE ER_CODELST
SET CODELST_SEQ='2'
WHERE CODELST_TYPE='randomization' AND CODELST_SUBTYP='randomized';

UPDATE ER_CODELST
SET CODELST_SEQ='3'
WHERE CODELST_TYPE='randomization' AND CODELST_SUBTYP='bayesian';

UPDATE ER_CODELST
SET CODELST_SEQ='4'
WHERE CODELST_TYPE='randomization' AND CODELST_SUBTYP='BMA-CRM';

UPDATE ER_CODELST
SET CODELST_SEQ='5'
WHERE CODELST_TYPE='randomization' AND CODELST_SUBTYP='blckRndmztn';

UPDATE ER_CODELST
SET CODELST_SEQ='6'
WHERE CODELST_TYPE='randomization' AND CODELST_SUBTYP='effTox';

UPDATE ER_CODELST
SET CODELST_SEQ='7'
WHERE CODELST_TYPE='randomization' AND CODELST_SUBTYP='equlRndmztn';

UPDATE ER_CODELST
SET CODELST_SEQ='8'
WHERE CODELST_TYPE='randomization' AND CODELST_SUBTYP='lstRndmztn';

UPDATE ER_CODELST
SET CODELST_SEQ='9'
WHERE CODELST_TYPE='randomization' AND CODELST_SUBTYP='other';

UPDATE ER_CODELST
SET CODELST_SEQ='10'
WHERE CODELST_TYPE='randomization' AND CODELST_SUBTYP='PocockRndm';

UPDATE ER_CODELST
SET CODELST_DESC='Single-Blind'
WHERE CODELST_TYPE='blinding' AND CODELST_SUBTYP='bindingSingle';

UPDATE ER_CODELST
SET CODELST_SUBTYP='doubleBlind'
WHERE CODELST_TYPE='blinding' AND CODELST_DESC='Double';

UPDATE ER_CODELST
SET CODELST_DESC='Double-Blinded'
WHERE CODELST_TYPE='blinding' AND CODELST_SUBTYP='doubleBlind';

UPDATE ER_CODELST
SET CODELST_DESC='Triple-Blind'
WHERE CODELST_TYPE='blinding' AND CODELST_SUBTYP='bindingTriple';

UPDATE ER_CODELST
SET CODELST_DESC='Open-Label'
WHERE CODELST_TYPE='blinding' AND CODELST_SUBTYP='bindingNone';

UPDATE ER_CODELST
SET CODELST_SEQ='1'
WHERE CODELST_TYPE='blinding' AND CODELST_DESC='Blinded';

UPDATE ER_CODELST
SET CODELST_SEQ='2'
WHERE CODELST_TYPE='blinding' AND CODELST_DESC='Double-Blinded';

UPDATE ER_CODELST
SET CODELST_SEQ='3'
WHERE CODELST_TYPE='blinding' AND CODELST_DESC='Open-Label';

UPDATE ER_CODELST
SET CODELST_SEQ='4'
WHERE CODELST_TYPE='blinding' AND CODELST_DESC='Partial-Blind';

UPDATE ER_CODELST
SET CODELST_SEQ='5'
WHERE CODELST_TYPE='blinding' AND CODELST_DESC='Single-Blind';

UPDATE ER_CODELST
SET CODELST_SEQ='6'
WHERE CODELST_TYPE='blinding' AND CODELST_DESC='Triple-Blind';

UPDATE ER_CODELST
SET CODELST_DESC='Only at MDACC'
WHERE CODELST_TYPE='studyscope' AND CODELST_SUBTYP='std_cen_single';

UPDATE ER_CODELST
SET CODELST_DESC='Multi Center, MDACC lead'
WHERE CODELST_TYPE='studyscope' AND CODELST_SUBTYP='studyscope_3';

UPDATE ER_CODELST
SET CODELST_DESC='Multi Center'
WHERE CODELST_TYPE='studyscope' AND CODELST_SUBTYP='std_cen_multi';

UPDATE ER_CODELST
SET CODELST_DESC='Randomization notes'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='rndmnTrtGrp';

UPDATE ER_CODELST
SET CODELST_DESC='Blinding notes'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='blndTrtGrp';

UPDATE ER_CODELST
SET CODELST_DESC='Unblinding notes'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='unblnTrtGrp';

UPDATE ER_CODELST
SET CODELST_DESC='Inclusion Criteria (institutional required options)'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='commIncCritra';

UPDATE ER_CODELST 
SET CODELST_CUSTOM_COL1='{chkArray:[
{data: "option1", display:"Age >/= 18 years old"},
{data: "option2", display:"All ages"},
{data: "option3", display:"Male"},
{data: "option4", display:"Female"},
{data: "option5", display:"Pregnant women"}
]}'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='commIncCritra';

UPDATE ER_CODELST
SET CODELST_DESC='Exclusion Criteria (institutional required options)'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='commExcCritra';

UPDATE ER_CODELST 
SET CODELST_CUSTOM_COL1='{chkArray:[
{data: "option1", display:"Age >/= 18 years old"},
{data: "option2", display:"Male"},
{data: "option3", display:"Female"},
{data: "option4", display:"Pregnant women"}
]}'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='commExcCritra';

UPDATE ER_CODELST
SET CODELST_DESC='Inclusion Criteria (other)'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='inclusion';

UPDATE ER_CODELST
SET CODELST_CUSTOM_COL='lookup',
CODELST_CUSTOM_COL1='{lookupPK:2, selection:"single", mapping:[{source:"Short Name", target:"alternateId"}]}'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='inclusion';

UPDATE ER_CODELST
SET CODELST_DESC='Exclusion Criteria (other)'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='exclusion';

UPDATE ER_CODELST
SET CODELST_CUSTOM_COL='lookup',
CODELST_CUSTOM_COL1='{lookupPK:2, selection:"single", mapping:[{source:"Short Name", target:"alternateId"}]}'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='exclusion';

UPDATE ER_CODELST 
SET CODELST_CUSTOM_COL='checkbox',
CODELST_CUSTOM_COL1='{chkArray:[
{data: "option1", display:"Contaminated product"},
{data: "option2", display:"DEATH"},
{data: "option3", display:"DELAY IN Randomization"},
{data: "option4", display:"Disease PROGRESSION, relapse"},
{data: "option5", display:"DRUG RESISTANT"},
{data: "option6", display:"End of follow-up period"},
{data: "option7", display:"INTERCURRENT ILLNESS"},
{data: "option8", display:"LOST TO FU"},
{data: "option9", display:"NEW malignancy"},
{data: "option10", display:"NO Treatment RESPONSE"},
{data: "option11", display:"Not able to perform a study procedure"},
{data: "option12", display:"Patient noncompliance"},
{data: "option13", display:"Patient requires further treatment"},
{data: "option14", display:"Patients withdrawal of consent to participate"},
{data: "option15", display:"PREGNANCY"},
{data: "option16", display:"Protocol VIOLATION"},
{data: "option17", display:"TOXICITY"},
{data: "option18", display:"Treatment completion"},
{data: "option19", display:"Other"}
]}'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='offStdyCri';

UPDATE ER_CODELST
SET CODELST_DESC='Data collection'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='wthdrwFollow';

UPDATE ER_CODELST
SET CODELST_DESC='Criteria for removal of participants',
CODELST_CUSTOM_COL='checkbox',
CODELST_CUSTOM_COL1='{chkArray:[
{data: "option1", display:"Option1"},
{data: "option2", display:"Option2"},
{data: "option3", display:"Other"}
]}'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='stdRemCriteria';

UPDATE ER_CODELST
SET CODELST_DESC='Criteria for Replacement of Participants'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='rplSubject';

UPDATE ER_CODELST
SET CODELST_DESC='Criteria for Removal of Participants'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='stdRemCriteria';

UPDATE ER_CODELST 
SET CODELST_CUSTOM_COL1='{chkArray:[
{data: "option1", display:"Secondary Objective"},
{data: "option2", display:"Exploratory Objective"},
{data: "option3", display:"Biomarker Objective"}
]}'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='selOthrObjEntry';

UPDATE ER_CODELST
SET CODELST_DESC='Screening/Baseline'
WHERE CODELST_TYPE='section' AND CODELST_SUBTYP='section_4_5_1';

UPDATE ER_CODELST
SET CODELST_DESC='On-study/Treatment Evaluations'
WHERE CODELST_TYPE='section' AND CODELST_SUBTYP='section_4_5_2';

UPDATE ER_CODELST
SET CODELST_DESC='End of Therapy Evaluations'
WHERE CODELST_TYPE='section' AND CODELST_SUBTYP='section_4_5_3';

UPDATE ER_CODELST
SET CODELST_DESC='Follow-up'
WHERE CODELST_TYPE='section' AND CODELST_SUBTYP='section_4_5_4';

UPDATE ER_CODELST
SET CODELST_DESC='Study Calendar/Schedule of Events'
WHERE CODELST_TYPE='section' AND CODELST_SUBTYP='section_4_5';

UPDATE ER_CODELST
SET CODELST_DESC='Are you measuring Biomarkers?'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='bioMark1';

UPDATE ER_CODELST 
SET CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "yes">Yes</OPTION>
<OPTION value = "no">No</OPTION>
</SELECT>'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='knownBiomrkr';

UPDATE ER_CODELST 
SET CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "gene">Gene</OPTION>
<OPTION value = "microrna">Micro RNA</OPTION>
<OPTION value = "snp">SNP-Small Nucleotide Polymorphism</OPTION>
<OPTION value = "other">Other</OPTION>
</SELECT>'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='biomrkrType';

UPDATE ER_CODELST 
SET CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "dna">DNA</OPTION>
<OPTION value = "rna">RNA</OPTION>
<OPTION value = "protein">Protein</OPTION>
<OPTION value = "methyl">Methylation</OPTION>
<OPTION value = "mrna">mRNA</OPTION>
<OPTION value = "snp">SNP</OPTION>
<OPTION value = "metabolo">Metabolomics</OPTION>
<OPTION value = "other">Other</OPTION>
</SELECT>'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='levelQues';

UPDATE ER_CODELST 
SET CODELST_CUSTOM_COL1='{chkArray:[
{data: "option1", display:"Biopsy- Tumor tissue"},
{data: "option2", display:"Biopsy- Normal tissue"},
{data: "option3", display:"Urine"},
{data: "option4", display:"Blood - Serum"},
{data: "option5", display:"Blood - Plasma"},
{data: "option6", display:"CSF"},
{data: "option7", display:"Pleural effusion"},
{data: "option8", display:"Other"}
]}'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='measureQues';

UPDATE ER_CODELST 
SET CODELST_CUSTOM_COL1='{chkArray:[
{data: "option1", display:"Pretreatment"},
{data: "option2", display:"During treatment"},
{data: "option3", display:"Post treatment"}
]}'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='timePoints';

UPDATE ER_CODELST
SET CODELST_DESC='Did an MDACC Biostatistician participate in the study design?'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='proDes2';

UPDATE ER_CODELST
SET CODELST_DESC='Does this protocol have a planned interim analysis?'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='protoMontr1';

UPDATE ER_CODELST
SET CODELST_DESC='Summary of planned interim analyses including stopping rules for futility, efficacy and/or toxicity'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='protoMontr3';

UPDATE ER_CODELST
SET CODELST_DESC='Rationale for no interim analyses'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='protoMontr4';

UPDATE ER_CODELST 
SET CODELST_DESC='Data Safety and Monitoring Board', CODELST_CUSTOM_COL='dropdown',
CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "mdaDsmb">MD ANDERSON DSMB</OPTION>
<OPTION value = "mdaExtdsmb">MD ANDERSON  External DSMB </OPTION>
<OPTION value = "indpDsmb">Independent DSMB - This is a DSMB comprised of members who are not affiliated with the research or sponsor in any way.</OPTION>
<OPTION value = "intDsmb">Internal DSMB � This is a DSMB comprised of study investigators and/or sponsor employees/contractors.</OPTION>
<OPTION value = "notAppl">Not Applicable</OPTION>
</SELECT>'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='dsmbInt';

UPDATE ER_CODELST 
SET CODELST_CUSTOM_COL='dropdown',
CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "yes">Yes</OPTION>
<OPTION value = "no">No</OPTION>
</SELECT>'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='bayesianComp';

UPDATE ER_CODELST 
SET CODELST_CUSTOM_COL1='{chkArray:[
{data: "option1", display:"Bypass CRC/PBHSRC Review<br>This study is NCI-sponsored, CTEP-approved, and/or NCI cooperative group-approved trial, or peer-reviwed by an NIH-approved mechanism and IRB-approved at an NCI designated cancer center."},
{data: "option2", display:"Expedited Biostatistical Review<br>This study is an industry-sponsored, multi-center clinical trial already activated at an NCI designated cancer center and/or has been approved by the Office of the Vice Provost for Fast-Track designation?"}
]}'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='expdBiostat';

UPDATE ER_CODELST
SET CODELST_DESC = 'Request for expedited biostatistical review'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='expdBiostat';

UPDATE ER_CODELST
SET CODELST_DESC='Simple Randomization'
WHERE CODELST_TYPE='randomization' AND CODELST_SUBTYP='lstRndmztn';

UPDATE ER_CODELST
SET CODELST_DESC='Stratified Randomization'
WHERE CODELST_TYPE='randomization' AND CODELST_SUBTYP='PocockRndm';

UPDATE ER_CODELST
SET CODELST_CUSTOM_COL='dropdown',
CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "inPat">In Patient</OPTION>
<OPTION value = "outPat">Out Patient</OPTION>
<OPTION value = "inAndOut">In Patient and Out Patient</OPTION>
</SELECT>'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='treatLoc';

UPDATE ER_CODELST
SET CODELST_CUSTOM_COL='dropdown',
CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Aural">Aural</OPTION>
<OPTION value = "Epidural">Epidural</OPTION>
<OPTION value = "Inhalation">Inhalation</OPTION>
<OPTION value = "Intraarterial">Intraarterial</OPTION>
<OPTION value = "Intradermal">Intradermal</OPTION>
<OPTION value = "Intralesional">Intralesional</OPTION>
<OPTION value = "Intramucosal">Intramucosal</OPTION>
<OPTION value = "Intramuscular">Intramuscular</OPTION>
<OPTION value = "Intranasal">Intranasal</OPTION>
<OPTION value = "Intraocular">Intraocular</OPTION>
<OPTION value = "Intraosseous">Intraosseous</OPTION>
<OPTION value = "Intraperitoneal">Intraperitoneal</OPTION>
<OPTION value = "Intrapleural">Intrapleural</OPTION>
<OPTION value = "Intrathecal">Intrathecal</OPTION>
<OPTION value = "Intratumoral">Intratumoral</OPTION>
<OPTION value = "Intravenous">Intravenous</OPTION>
<OPTION value = "Intravesicular">Intravesicular</OPTION>
<OPTION value = "Ocular">Ocular</OPTION>
<OPTION value = "Oral">Oral</OPTION>
<OPTION value = "Rectal">Rectal</OPTION>
<OPTION value = "Subcutaneous">Subcutaneous</OPTION>
<OPTION value = "Sublingual">Sublingual</OPTION>
<OPTION value = "Topical">Topical</OPTION>
<OPTION value = "Transdermal">Transdermal</OPTION>
<OPTION value = "Vaginal">Vaginal</OPTION>
</SELECT>'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='route';

UPDATE ER_CODELST
SET CODELST_CUSTOM_COL=null
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='doseRationale';

UPDATE ER_CODELST
SET CODELST_CUSTOM_COL='dropdown',
CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='incldAdminstr';

UPDATE ER_CODELST
SET CODELST_DESC = '<br>Is the radioactive compound (or drug) FDA approved and/or commercially available?'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP = 'radioCompAvail';

UPDATE ER_CODELST
SET CODELST_CUSTOM_COL='dropdown',
CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='nihRacRev';

UPDATE ER_CODELST
SET CODELST_CUSTOM_COL=null,
CODELST_DESC='Provide the IBC protocol number or contact the IBC coordinator for information at 713-563-3879.'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='biosafety4';

UPDATE ER_CODELST
SET CODELST_CUSTOM_COL='textarea'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='biosafety2Comm';

UPDATE ER_CODELST
SET CODELST_CUSTOM_COL='textarea'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='nihRacRevComm';

UPDATE ER_CODELST
SET CODELST_DESC='Are you using Tisseel or Evicel or similar?'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='biosafety5';

UPDATE ER_CODELST
SET CODELST_CUSTOM_COL='dropdown',
CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
<OPTION value = "NA">N/A</OPTION>
</SELECT>'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='biosafety5';

UPDATE ER_CODELST
SET CODELST_CUSTOM_COL1='{chkArray:[
{data: "option1", display:"Contaminated product"},
{data: "option2", display:"Death"},
{data: "option3", display:"Delay in randomization"},
{data: "option4", display:"Disease progression, relapse"},
{data: "option5", display:"Drug resistant"},
{data: "option6", display:"End of follow-up period"},
{data: "option7", display:"Intercurrent illness"},
{data: "option8", display:"Lost to follow-up"},
{data: "option9", display:"New malignancy"},
{data: "option10", display:"No treatment response"},
{data: "option11", display:"Not able to perform a study procedure"},
{data: "option12", display:"Patient noncompliance"},
{data: "option13", display:"Patient requires further treatment"},
{data: "option14", display:"Patients withdrawal of consent to participate"},
{data: "option15", display:"Pregnancy"},
{data: "option16", display:"Protocol violation"},
{data: "option17", display:"Toxicity"},
{data: "option18", display:"Treatment completion"},
{data: "option19", display:"Other"}
]}'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='offStdyCri';

UPDATE ER_CODELST
SET CODELST_CUSTOM_COL='textarea'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='commDrugInsrt';

UPDATE ER_CODELST
SET CODELST_CUSTOM_COL='textarea'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='invAntcpAEs';

UPDATE ER_CODELST
SET CODELST_DESC='Data Safety Monitoring Plan'
WHERE CODELST_TYPE='section' AND CODELST_SUBTYP='section_6_5';

DELETE FROM ER_CODELST
WHERE CODELST_TYPE='studyscope' AND CODELST_SUBTYP='bothMDAnOther';

DELETE FROM ER_CODELST
WHERE CODELST_TYPE='studyscope' AND CODELST_SUBTYP='onlyMDACC';

DELETE FROM ER_CODELST
WHERE CODELST_TYPE='randomization' AND CODELST_SUBTYP='bayesian';

DELETE FROM ER_CODELST
WHERE CODELST_TYPE='randomization' AND CODELST_SUBTYP='BMA-CRM';

DELETE FROM ER_CODELST
WHERE CODELST_TYPE='randomization' AND CODELST_SUBTYP='effTox';

DELETE FROM ER_CODELST
WHERE CODELST_TYPE='randomization' AND CODELST_SUBTYP='equlRndmztn';

COMMIT;