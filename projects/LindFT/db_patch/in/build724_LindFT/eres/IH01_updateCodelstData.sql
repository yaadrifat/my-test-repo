set define off;

update ER_CODELST set CODELST_DESC='New Protocol' where 
CODELST_TYPE='submission' and CODELST_SUBTYP='new_app';

update ER_CODELST set CODELST_DESC='Secondary Department' where 
CODELST_TYPE='studyidtype' and CODELST_SUBTYP='studyCond2';

insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col, codelst_custom_col1 )
values (seq_er_codelst.nextval, 'studyidtype', 'nom_rev', 'Nominated Reviewer', 'lookup', '{lookupPK:6000, selection:"single", mapping:[{source:"Short Name", target:"alternateId"}]}' );

insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col, codelst_custom_col1 )
values (seq_er_codelst.nextval, 'studyidtype', 'nom_alt_rev', 'Nominated Reviewer (alternate)', 'lookup', '{lookupPK:6000, selection:"single", mapping:[{source:"Short Name", target:"alternateId"}]}' );

insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
values (seq_er_codelst.nextval, 'studyidtype', 'prot_ready', 'Protocol ready for pre-reviews and signoffs', 'checkbox');

insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
values (seq_er_codelst.nextval, 'studyidtype', 'prot_req_ind', 'Does this protocol require an IND?', 'dropdown');

update er_codelst 
set codelst_custom_col1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "yes">Yes</OPTION>
<OPTION value = "no">No</OPTION>
</SELECT>'
where codelst_type='studyidtype' and codelst_subtyp='prot_req_ind';

insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
values (seq_er_codelst.nextval, 'studyidtype', 'meet_exem_crit', 'Confirm that the protocol <br>meets all criteria for exemption according to 21CFR312.2(b) <br>noted here?', 'checkbox');

update er_codelst 
set codelst_custom_col1='{chkArray:[{data: "option1", display:"<br>(b) Exemptions (1) The clinical investigation of a drug product that is lawfully marketed in the <br>United States is exempt from the requirements of <br>this part if all of the following apply:<br>(i) The investigation is not intended to be reported to FDA as a well-controlled study in support of a new indication for use not intended to be used to suport any other significant change in the labeling for this drug<br>(ii) If the drug that is undergoing investigation is lawfully marketed as a prescription drug product, the investigation is not intended to support a significant change in the advertising for the product<br>(iii) The investigation does not involve a route of administration of dosage level or use in a patient population or other factor that significantly increases the risks (or decreases the acceptability of the risks) associated with the use of the drug product<br>(iv) The investigation is conducted in compliance with the requirements for institutional review set forth in part 56 and with the requirements for informed consent set forth in part 50; and<br>(v) The investigation is conducted in compliance with the requirements of 312.7<br>at an NCI designated cancer center."}]}'
where codelst_type='studyidtype' and codelst_subtyp='meet_exem_crit';

insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
values (seq_er_codelst.nextval, 'studyidtype', 'exem_ratnl', 'Rationale for exemption', 'textarea');