SET DEFINE OFF;
set serveroutput on;

declare
v_ItemExists number := 0;
begin
	SELECT COUNT(*) INTO v_ItemExists FROM ER_LKPCOL WHERE FK_LKPLIB = 6000 AND LKPCOL_NAME = 'usr_fullname';	
	IF (v_ItemExists = 0) THEN
		Insert into ERES.ER_LKPCOL
   		(PK_LKPCOL, FK_LKPLIB, LKPCOL_NAME, LKPCOL_DISPVAL, LKPCOL_TABLE, LKPCOL_KEYWORD)
 		Values
   		(SEQ_ER_LKPCOL.nextval, 6000, 'usr_fullname', 'Full Name', 'erv_usrprofile', 'USRFULLNAME');
		COMMIT;
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
END;
/

