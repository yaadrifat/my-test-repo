set define off;
declare
v_CodeExists number := 0;
begin
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'templateType';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'templateType', 'Select Template','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "bioMed">Biomedical</OPTION>
		<OPTION value = "socBehv">Social Behavioral</OPTION>
		<OPTION value = "bioSpec">Biospecimen/Data Use</OPTION>
		<OPTION value = "humUse">Compassionate Use/Humanitarian Use</OPTION>
		<OPTION value = "emergUse">Emergency Use</OPTION>
		<OPTION value = "nonHsr">Non-HSR</OPTION>
		</SELECT>',1);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "bioMed">Biomedical</OPTION>
		<OPTION value = "socBehv">Social Behavioral</OPTION>
		<OPTION value = "bioSpec">Biospecimen/Data Use</OPTION>
		<OPTION value = "humUse">Compassionate Use/Humanitarian Use</OPTION>
		<OPTION value = "emergUse">Emergency Use</OPTION>
		<OPTION value = "nonHsr">Non-HSR</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='templateType'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyidtype_270';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'studyidtype_270', 'CMS Qualifying Clinical Trial?','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT> ',1);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT> ' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='studyidtype_270'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyidtype_1';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'studyidtype_1', 'FDA approved drug, used in approved manner: a.',null,null,2);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='studyidtype_1'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyidtype_2';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'studyidtype_2', 'FDA approved drug, used in approved manner: b.',null,null,2);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='studyidtype_2'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyidtype_263';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'studyidtype_263', 'Diagnosis Code',null,null,3);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='studyidtype_263'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'cancer_ctr_num';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'cancer_ctr_num', 'Cancer Center Number',null,null,10);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='cancer_ctr_num'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyidtype_0';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'studyidtype_0', 'Short Title',null,null,11);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='studyidtype_0'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'irb_number';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'irb_number', 'IRB Number',null,null,11);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='irb_number'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'grant_number';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'grant_number', 'Grant Number',null,null,20);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='grant_number'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'id_ccg';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'id_ccg', 'Coordinating Group Number',null,null,50);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='id_ccg'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'biosafety1';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'biosafety1', 'Does the study involve the use of Recombinant DNA technology?','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',51);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='biosafety1'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'biosafety2';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'biosafety2', 'Does this study involve the use of organisms that are infectious to humans?','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',52);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='biosafety2'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'biosafety3';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'biosafety3', 'Does the study involve human/animal tissue other than blood derived hematopoietic stem cells?','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		<OPTION value = "NA">N/A</OPTION>
		</SELECT>',53);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		<OPTION value = "NA">N/A</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='biosafety3'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'cmpnsation1';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'cmpnsation1', 'Describe','textarea',null,55);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='cmpnsation1'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'confdlty1';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'confdlty1', 'Will there be a link to identify subjects?','dropdown','<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',57);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='confdlty1'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'confdlty2';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'confdlty2', 'Describe how you will protect the subject''s identity.','textarea',null,58);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='confdlty2'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'confdlty3';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'confdlty3', 'Describe provisions taken to maintain confidentiality of specimens/data.','textarea',null,59);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='confdlty3'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'id_ccn';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'id_ccn', 'NCI Number',null,null,60);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='id_ccn'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'confdlty4';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'confdlty4', 'Describe the security plan for data including where data will be stored, how long the data will be stored and what will be done with the data once the study is terminated.','textarea',null,60);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='confdlty4'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'confsecprod';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'confsecprod', 'Please confirm that these data will be collected, managed, and distributed in accordance with the M.D. Anderson Confidentiality Policy (ADM0264).','checkbox',null,61);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='confsecprod'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'confsecprod1';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'confsecprod1', 'If No, Please Describe:','textarea',null,62);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='confsecprod1'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'confsecprod2';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'confsecprod2', 'Select all data storage procedures/mechanisms that apply.','checkbox',
		'{chkArray:[{data:"option1", display:"Paper records"},{data:"option2", display:"PC/Laptop/etc."},{data:"option3", display:"External Storage (CD/Disk/USB Drive/etc)"},{data:"option4", display:"Server"}]}',63);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1=
		'{chkArray:[{data:"option1", display:"Paper records"},{data:"option2", display:"PC/Laptop/etc."},{data:"option3", display:"External Storage (CD/Disk/USB Drive/etc)"},{data:"option4", display:"Server"}]}' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='confsecprod2'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'confsecprod3';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'confsecprod3', 'Select all data storage procedures/mechanisms that apply.','checkbox',
		'{chkArray:[{data:"option1", display:"Secured by locked file cabinet"},{data:"option2", display:"Secured by a locked office or other secured area."},{data:"option3", display:"Secured by password protection"}]}',64);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1=
		'{chkArray:[{data:"option1", display:"Secured by locked file cabinet"},{data:"option2", display:"Secured by a locked office or other secured area."},{data:"option3", display:"Secured by password protection"}]}' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='confsecprod3'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'confsecprod4';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'confsecprod4', 'List other data storage types and procedures/mechanisms:','textarea',null,65);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='confsecprod4'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'id_swog';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'id_swog', 'SWOG Number',null,null,70);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='id_swog'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'id_other';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'id_other', 'Other Number',null,null,80);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='id_other'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundingSource';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'fundingSource', 'Additional Funding',null,null,81);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='fundingSource'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dataPoints1';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'dataPoints1', 'What types of data points will be collected, received or recorded?','checkbox',
		'{chkArray:[{data:"option1", display:"Names or initials"},{data:"option2", display:"ALL geographic subdivisions smaller such as city, county, neighborhood"},{data:"option3", display:"All elements of dates smaller than a year (e.g., birth date, admission, discharge, death, etc.)"},{data:"option4", display:"Phone numbers"},{data:"option5", display:"Fax numbers"},{data:"option6", display:"E-mail addresses"},{data:"option7", display:"Social security numbers"},{data:"option8", display:"Medical record Number"},{data:"option9", display:"Health plan beneficiary number or code"},{data:"option10", display:"Any other account numbers"},{data:"option11", display:"Certificate/license numbers"},{data:"option12", display:"Vehicle identifiers"},{data:"option13", display:"Device identification numbers"},{data:"option14", display:"Web URL�s"},{data:"option15", display:"Internet IP address numbers"},{data:"option16", display:"Biometric identifiers (e.g., fingerprints, voice prints, retina scans, etc.)"},{data:"option17", display:"Full face photographs or comparable images"},{data:"option18", display:"Any other unique number, characteristic or code, such as employee ID number or student ID number"},{data:"option19", display:"None of the Above"}]}',101);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1=
		'{chkArray:[{data:"option1", display:"Names or initials"},{data:"option2", display:"ALL geographic subdivisions smaller such as city, county, neighborhood"},{data:"option3", display:"All elements of dates smaller than a year (e.g., birth date, admission, discharge, death, etc.)"},{data:"option4", display:"Phone numbers"},{data:"option5", display:"Fax numbers"},{data:"option6", display:"E-mail addresses"},{data:"option7", display:"Social security numbers"},{data:"option8", display:"Medical record Number"},{data:"option9", display:"Health plan beneficiary number or code"},{data:"option10", display:"Any other account numbers"},{data:"option11", display:"Certificate/license numbers"},{data:"option12", display:"Vehicle identifiers"},{data:"option13", display:"Device identification numbers"},{data:"option14", display:"Web URL�s"},{data:"option15", display:"Internet IP address numbers"},{data:"option16", display:"Biometric identifiers (e.g., fingerprints, voice prints, retina scans, etc.)"},{data:"option17", display:"Full face photographs or comparable images"},{data:"option18", display:"Any other unique number, characteristic or code, such as employee ID number or student ID number"},{data:"option19", display:"None of the Above"}]}' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='dataPoints1'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'describeText';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'describeText', 'Please describe: ','input',null,102);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='input', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='describeText'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'describeRecord';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'describeRecord', 'Describe how data will be recorded','textarea',null,103);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='describeRecord'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'inclusion';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'inclusion', 'Inclusion','textarea',null,105);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='inclusion'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'exclusion';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'exclusion', 'Exclusion','textarea',null,106);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='exclusion'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'juvinileElig';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'juvinileElig', 'Pediatrics','dropdown','<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT> ',107);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT> ' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='juvinileElig'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'elderElig';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'elderElig', 'Elderly','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',108);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='elderElig'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'pregEligiblity';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'pregEligiblity', 'Pregnant Women','dropdown','<SELECT id="alternateId"  NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',109);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT id="alternateId"  NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='pregEligiblity'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'prisonorElig';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'prisonorElig', 'Prisoners','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',110);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='prisonorElig'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dose';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'dose', 'Dose','input',null,113);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='input', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='dose'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'route';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'route', 'Route of Administration','input',null,114);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='input', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='route'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nonDrugStudy';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'nonDrugStudy', 'Non-Drug Study','textarea',null,115);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='nonDrugStudy'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'requirements';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'requirements', '<i>Requirements for Medical Coverage of Routine Costs</>','splfld',null,117);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='splfld', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='requirements'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'cycles';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'cycles', 'How many cycles will be covered in the Coverage Analysis?','input',null,118);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='input', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='cycles'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'lab';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'lab', 'Are there any send out labs to a Central Laboratory?','checkbox',null,119);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='lab'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'EKG';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'EKG', 'Will this trial require EKGs in which the sponsor will be providing an EKG machine?
	','checkbox',null,120);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='EKG'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'reimbursement';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'reimbursement', 'Will this trial require the sponsor to reimburse MDACC for commercially available drugs used as investigational agents?','checkbox',null,121);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='reimbursement'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'diagImaging';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'diagImaging', 'Will this trial require the use of research- related items or services in Diagnostic Imaging?','checkbox',null,122);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='diagImaging'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'radiology';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'radiology', 'Will this trial require the use of research- related items or services in Interventional Radiology?','checkbox',null,123);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='radiology'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'independentSOC';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'independentSOC', 'If applicable, will research personnel draw all Non - Standard of Care (SOC) labs associated with the trial that are collected independently of SOC lab work?','checkbox',null,124);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='independentSOC'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'blndnUnbln';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'blndnUnbln', 'Blinding/Unblinding','textarea',null,125);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='blndnUnbln'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundSrc';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'fundSrc', 'Type of Support - Industry Funding, Grant, Other','checkbox',null,152);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='fundSrc'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'spnsrName';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'spnsrName', 'Name of Sponsor, Supporter or Granting Agency:','textarea',null,154);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='spnsrName'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'finSupp';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'finSupp', 'How is the proposed study to be financially supported?','dropdown','<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',156);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='finSupp'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'spnsrshp';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'spnsrshp', 'Type(s) of support','checkbox',
		'{chkArray:[{data:"option1", display:"Option 1"},{data:"option2", display:"Option 2"},{data:"option3", display:"Option 3"}]}',158);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1=
		'{chkArray:[{data:"option1", display:"Option 1"},{data:"option2", display:"Option 2"},{data:"option3", display:"Option 3"}]}' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='spnsrshp'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'spnsrshp1';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'spnsrshp1', 'Indicate if the sponsor/supporter/granting agency will receive data','checkbox',null,160);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='spnsrshp1'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'finSupp1';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'finSupp1', 'Type of funding: ','dropdown','<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "option1">NCI</OPTION>
		<OPTION value = "option2">NIH (other than NCI)</OPTION>
		<OPTION value = "option3">DOD</OPTION>
		<OPTION value = "option4">Other peer-reviewed funding (e.g. NSF, ACS, etc.)</OPTION>
		</SELECT>',162);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "option1">NCI</OPTION>
		<OPTION value = "option2">NIH (other than NCI)</OPTION>
		<OPTION value = "option3">DOD</OPTION>
		<OPTION value = "option4">Other peer-reviewed funding (e.g. NSF, ACS, etc.)</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='finSupp1'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'finSupp4';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'finSupp4', 'Which Cancer Center Support Grant (CCSG) - NCI Core Grant Program does this protocol relate to?',null,null,164);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='finSupp4'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'finSupp2';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'finSupp2', 'Source of Agent/Device:','input',null,164);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='input', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='finSupp2'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'finSupp3';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'finSupp3', 'List any device(s) which will be used (if applicable):  ','input',null,166);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='input', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='finSupp3'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'grant';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'grant', 'Grant Serial#/Contract#','input',null,168);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='input', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='grant'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'homeCare1';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'homeCare1', 'Specify what, if any, treatment may be given at home.','input',null,172);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='input', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='homeCare1'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'intellProp1';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'intellProp1', 'Intellectual Property','checkbox',null,176);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='intellProp1'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'intellProp2';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'intellProp2', '(If Yes) You may need to submit an Invention Disclosure Report to the Office of Technology Commercialization at 713-745-9602 or visit the Intranet site','splfld',null,178);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='splfld', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='intellProp2'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'intellProp3';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'intellProp3', '1. Does this study include any agents, devices, or radioactive compound (or drug) manufactured at MD Anderson Cancer Center or by a contract manufacturer? ','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',180);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='intellProp3'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'intellProp4';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'intellProp4', '(If Yes) 2. Is the technology licensed by MD Anderson Cancer Center? ','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',182);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='intellProp4'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'intellProp5';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'intellProp5', '(If Yes) 4. Where is "drug" being manufactured?','input',null,184);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='input', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='intellProp5'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'ide1';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'ide1', 'Intended as an implant? ','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',188);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='ide1'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'ide2';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'ide2', 'Purported or represented to be for use supporting or sustaining human life? ','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',190);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='ide2'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'ide3';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'ide3', 'For use of substantial importance in diagnosing, curing, mitigating, or treating disease, or otherwise preventing impairment of human health? ','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',192);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='ide3'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'ide4';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'ide4', 'Has the device been modified in a manner not approved by the FDA?','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		<OPTION value = "NA">N/A</OPTION>
		</SELECT>',194);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		<OPTION value = "NA">N/A</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='ide4'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'ide5';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'ide5', 'Who will be responsible for the costs associated with the use of the device?','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "PrtcpntOrThrdPrtyProv">Participant/Third Party Provider</OPTION>
		<OPTION value = "Spnsr">Sponsor</OPTION>
		</SELECT>',196);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "PrtcpntOrThrdPrtyProv">Participant/Third Party Provider</OPTION>
		<OPTION value = "Spnsr">Sponsor</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='ide5'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'break';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'break', '<br>','splfld',null,197);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='splfld', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='break'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nci';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'nci', 'Is this an NCI-Cancer Therapy Evaluation Protocol (CTEP)? ','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',198);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='nci'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nci1';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'nci1', 'Is this an NCI-Division of Cancer Prevention Protocol (DCP)? ','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',200);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='nci1'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'mdacc';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'mdacc', 'Is this MDACC Study Initiated?','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',202);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='mdacc'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'labtest';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'labtest', 'Is there any biomarker testing in this study being used to determine patient/participant eligibility, treatment assignment, or management of patient/participant care?','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		<OPTION value = "NA">Not applicable for this protocol</OPTION>
		</SELECT>',204);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		<OPTION value = "NA">Not applicable for this protocol</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='labtest'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'stayDurationn';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'stayDurationn', 'Length of Stay','input',null,206);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='input', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='stayDurationn'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'mfg1';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'mfg1', 'Will you manufacture in full or in part (split manufacturing) a drug or biological product at the MD Anderson Cancer Center for the proposed clinical study?','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',209);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='mfg1'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'mta1';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'mta1', 'Materials Transfer Agreement (select one)','dropdown','<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',211);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='mta1'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'mta2';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'mta2', 'An MTA is a contract that governs the transfer of tangible research materials such as biological materials (e.g., reagents, cell lines, plasmids, and vectors).  MTAs may also be used for chemical compounds and even some types of software. Contact RESEARCH ADMINISTRATION (713-745-6633) for Assistance.','splfld',null,212);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='splfld', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='mta2'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nseq1';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'nseq1', 'Will non-MDACC approved equipment be used in this study? ','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',222);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='nseq1'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nseq2';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'nseq2', 'Yes,Has a plan been devised for training personnel?','checkbox',null,224);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='nseq2'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nseq3';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'nseq3', 'Describe the plan to train personnel.','textarea',null,226);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='nseq3'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nseq4';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'nseq4', 'Has the Patient Care: Materials, Use, and Evaluation (MUE) Committee approved the non-standard equipment for use?','checkbox',null,228);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='nseq4'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'sum4_agent';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'sum4_agent', 'Study type (Summary 4): Agent or Device','chkbox',null,230);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='chkbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='sum4_agent'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'orderSet1';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'orderSet1', 'Will a research protocol require a physician order set?','checkbox',null,231);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='orderSet1'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'preCli1';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'preCli1', 'Preclinical Drug Development','textarea',null,234);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='preCli1'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'protoMontr1';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'protoMontr1', 'Does this protocol have a schedule for interim analysis?','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',236);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='protoMontr1'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dataSftyMntrPln';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'dataSftyMntrPln', 'Data and Safety Monitoring Plan','textarea',null,237);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='dataSftyMntrPln'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'protoMontr3';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'protoMontr3', 'Provide a summary or schedule of interim analysis','textarea',null,238);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='protoMontr3'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'protoMontr4';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'protoMontr4', 'Provide a rationale for no interim analysis','textarea',null,239);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='protoMontr4'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'sum4_beh';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'sum4_beh', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Trials Involving other Interventions','chkbox',null,240);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='chkbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='sum4_beh'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'radMat1';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'radMat1', 'Does the study involve the administration of radioisotopes or a radioisotope labeled agent?','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		<OPTION value = "NA">N/A</OPTION>
		</SELECT>',241);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		<OPTION value = "NA">N/A</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='radMat1'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'resLoc1';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'resLoc1', 'Is MDACC the lead institution? ','dropdown','<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',243);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='resLoc1'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'ccsg';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'ccsg', 'CCSG Study Source','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "ExtPeerRev">Externally Peer-Reviewed</OPTION>
		<OPTION value = "Industrial">Industrial</OPTION>
		<OPTION value = "Institutional">Institutional</OPTION>
		<OPTION value = "National">National</OPTION>
		</SELECT>',244);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "ExtPeerRev">Externally Peer-Reviewed</OPTION>
		<OPTION value = "Industrial">Industrial</OPTION>
		<OPTION value = "Institutional">Institutional</OPTION>
		<OPTION value = "National">National</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='ccsg'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'resLoc2';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'resLoc2', 'If MDACC is the lead institution is this a consortium study? ','dropdown','<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',244);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='resLoc2'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'resLoc6';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'resLoc6', 'Is this a cooperative study?','dropdown','<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',245);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='resLoc6'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'resloc3';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'resloc3', 'Are there international sites participating?','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',245);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='resloc3'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'resLoc7';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'resLoc7', 'Collaborating Site and Institution IRB Approval Letter','checkbox',null,246);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='resLoc7'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'sum4_na';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'sum4_na', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Epidemiologic or other Observational Studies','chkbox',null,250);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='chkbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='sum4_na'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'resPop1';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'resPop1', 'Total Expected Number of Prospective Participants (for example, one participant may have 10 samples, 10 participants would have 100 samples):',null,null,250);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='resPop1'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'resPop2';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'resPop2', 'Does this research include MDACC employees as participants?','dropdown','<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',251);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='resPop2'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'resPop3';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'resPop3', 'Explain if the responses are anonymous, who will be recruiting, and when will recruitment occur','textarea',null,252);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='resPop3'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyLocAddInfo';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'studyLocAddInfo', 'Name and location for sites where research will be conducted','textarea',null,254);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='studyLocAddInfo'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'partPop1';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'partPop1', 'Expected Age Range of Participants:  ',null,null,256);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='partPop1'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'specimen1';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'specimen1', 'Will this include embryonic stem cells?','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',258);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='specimen1'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'specimen2';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'specimen2', 'List the source of specimens/data:  (Select all that apply) (requires at least 1 choice)','dropdown','<SELECT NAME="alternateId"><OPTION value="">Select an option</OPTION><OPTION value = "Yes">option1</OPTION><OPTION value = "No">option2</OPTION><OPTION value = "In Process">option3</OPTION></SELECT>',259);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId"><OPTION value="">Select an option</OPTION><OPTION value = "Yes">option1</OPTION><OPTION value = "No">option2</OPTION><OPTION value = "In Process">option3</OPTION></SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='specimen2'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'specimen3';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'specimen3', 'Institutional Tissue Bank: (Select all that apply)','dropdown','<SELECT NAME="alternateId"><OPTION value="">Select an option</OPTION><OPTION value = "Yes">option1</OPTION><OPTION value = "No">option2</OPTION><OPTION value = "In Process">option3</OPTION></SELECT>',260);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId"><OPTION value="">Select an option</OPTION><OPTION value = "Yes">option1</OPTION><OPTION value = "No">option2</OPTION><OPTION value = "In Process">option3</OPTION></SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='specimen3'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'sum4_prg';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'sum4_prg', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Program Code',null,null,260);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='sum4_prg'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'specimen4';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'specimen4', 'If any of the asterisked (*) choices are selected, please provide name of database or source of specimens/data:','textarea',null,261);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='specimen4'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'investiDevice';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'investiDevice', 'Does the study include an investigational device?','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes (Complete Investigational Device Form by clicking on the Study Form tab)</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT> ',261);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes (Complete Investigational Device Form by clicking on the Study Form tab)</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT> ' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='investiDevice'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyidtype_261';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'studyidtype_261', 'ClinicalTrials.Gov #',null,null,261);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='studyidtype_261'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'specimen5';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'specimen5', 'Will specimens/data be shared with an entity, person or organization outside of MD Anderson?','dropdown','<SELECT NAME="alternateId">
		<OPTION value = ">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',262);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='specimen5'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyOccurs';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'studyOccurs', 'Study occurs in an in - patient setting','chkbox',null,262);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='chkbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='studyOccurs'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyidtype_262';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'studyidtype_262', 'COEUS#',null,null,262);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='studyidtype_262'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyProvide';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'studyProvide', 'Study provides radiation','chkbox',null,263);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='chkbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='studyProvide'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'specimen6';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'specimen6', 'If Yes,  Please list each entity, person or organization that the data will be shared with and indicate if a data use agreement or a materials transfer agreement has been implemented.','textarea',null,263);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='specimen6'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'specimen7';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'specimen7', 'Enter Name of Entity, Person or Organization Rationale for Data Sharing:  (Select All That Apply) (requires at least 1 choice)','dropdown','<SELECT NAME="alternateId"><OPTION value="">Select an option</OPTION><OPTION value = "Yes">option1</OPTION><OPTION value = "No">option2</OPTION><OPTION value = "In Process">option3</OPTION></SELECT>',264);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId"><OPTION value="">Select an option</OPTION><OPTION value = "Yes">option1</OPTION><OPTION value = "No">option2</OPTION><OPTION value = "In Process">option3</OPTION></SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='specimen7'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'specimen8';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'specimen8', 'Data analysis','textarea',null,265);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='specimen8'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'specimen9';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'specimen9', 'Requirement Procedures','textarea',null,266);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='specimen9'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'specimen10';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'specimen10', 'Testing','textarea',null,267);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='specimen10'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'specimen11';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'specimen11', 'Administrative Survey','textarea',null,268);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='specimen11'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'specimen12';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'specimen12', 'Development of New Product or Project','textarea',null,269);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='specimen12'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'specimen13';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'specimen13', 'Other: Please describe:','textarea',null,270);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='specimen13'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'specimen14';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'specimen14', 'Data Use Agreement: (Select One)','dropdown','<SELECT NAME="alternateId"><OPTION value="">Select an option</OPTION><OPTION value = "Yes">Yes</OPTION><OPTION value = "No">No</OPTION><OPTION value = "In Process">In Process</OPTION></SELECT>',271);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId"><OPTION value="">Select an option</OPTION><OPTION value = "Yes">Yes</OPTION><OPTION value = "No">No</OPTION><OPTION value = "In Process">In Process</OPTION></SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='specimen14'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'specimen15';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'specimen15', 'A written agreement between a health care component and a person requesting a disclosure of protected health information (PHI) contained in a limited data set. Data use agreements must meet the requirements of limited data set procedure. Contact LEGAL SERVICES (713-745-6633) for Assistance','splfld',null,272);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='splfld', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='specimen15'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'icd1';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'icd1', 'Select process','dropdown','<SELECT NAME="alternateId"><OPTION value="">Select an option</OPTION><OPTION value = "option1">Project will utilize a waiver of informed consent/authorization (e.g. subjects previously signed consent for future use of specimens/data)</OPTION><OPTION value = "option2">Project will utilize the questionnaire statement for surveys (attach survey or questionnaire)</OPTION><OPTION value = "option3">Project staff will obtain verbal consent (e.g. telephone or in person) (attach verbal script or information sheet)</OPTION></SELECT>',275);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId"><OPTION value="">Select an option</OPTION><OPTION value = "option1">Project will utilize a waiver of informed consent/authorization (e.g. subjects previously signed consent for future use of specimens/data)</OPTION><OPTION value = "option2">Project will utilize the questionnaire statement for surveys (attach survey or questionnaire)</OPTION><OPTION value = "option3">Project staff will obtain verbal consent (e.g. telephone or in person) (attach verbal script or information sheet)</OPTION></SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='icd1'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'icd2';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'icd2', 'Describe the consent process and the staff involved.','textarea',null,276);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='icd2'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'icd4';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'icd4', 'Remember to �Compose� Research Information Document for Exempt Research Long & Short PA will include this choice:','dropdown','<SELECT NAME="alternateId"><OPTION value="">Select an option</OPTION><OPTION value = "option1">Project staff will obtain prospective written consent</OPTION><OPTION value = "option2"> Other</OPTION></SELECT>',278);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId"><OPTION value="">Select an option</OPTION><OPTION value = "option1">Project staff will obtain prospective written consent</OPTION><OPTION value = "option2"> Other</OPTION></SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='icd4'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'icd6';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'icd6', 'Are you requiring subjects to sign a consent form for this protocol?','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',279);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='icd6'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'icd5';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'icd5', 'If selected ''Othernull, please explain','textbox',null,279);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='icd5'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundSrc1';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'fundSrc1', 'Does the Study have a Sponsor Supporter or Granting Agency? ','dropdown','<SELECT NAME="alternateId"><OPTION value="">Select an option</OPTION><OPTION value = "Yes">Yes</OPTION><OPTION value = "No">No</OPTION></SELECT>',281);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId"><OPTION value="">Select an option</OPTION><OPTION value = "Yes">Yes</OPTION><OPTION value = "No">No</OPTION></SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='fundSrc1'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundSrc2';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'fundSrc2', 'Name of Sponsor, Supporter or Granting Agency',null,null,282);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='fundSrc2'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundSrc3';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'fundSrc3', 'Type of Support','dropdown','<SELECT NAME="alternateId"><OPTION value="">Select an option</OPTION><OPTION value = "option1">Industry Funding</OPTION><OPTION value = "option2">Grant</OPTION><OPTION value = "option3">Other</OPTION></SELECT>',283);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId"><OPTION value = "">Select an option</OPTION><OPTION value = "option1">Industry Funding</OPTION><OPTION value = "option2">Grant</OPTION><OPTION value = "option3">Other</OPTION></SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='fundSrc3'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'resDesc1';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'resDesc1', 'Does this project involve analysis or collection of data relating to the performance of clinical procedures, interventions, or therapies?','dropdown','<SELECT NAME="alternateId"><OPTION value="">Select an option</OPTION><OPTION value = "option1">Yes</OPTION><OPTION value = "option2">No</OPTION></SELECT>',286);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId"><OPTION value="">Select an option</OPTION><OPTION value = "option1">Yes</OPTION><OPTION value = "option2">No</OPTION></SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='resDesc1'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'resDesc2';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'resDesc2', 'Are any of the clinical procedures, interventions, or therapies being analyzed considered non-standard of care at the MD Anderson main campus? ','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',287);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='resDesc2'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'audtnInspctn';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'audtnInspctn', 'Auditing and Inspecting','textarea',null,292);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='audtnInspctn'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'accrual1';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'accrual1', 'Estimated number enrolled per month at MDACC',null,null,294);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='accrual1'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'accrual2';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'accrual2', 'Total number enrolled at MDACC',null,null,295);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='accrual2'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'accrual5';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'accrual5', 'Total number screened at MDACC',null,null,298);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='accrual5'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'accrual6';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'accrual6', 'Estimated number screened per month at MDACC',null,null,299);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='accrual6'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'accrual7';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'accrual7', 'Total number screened at all sites',null,null,300);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='accrual7'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'accrual8';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'accrual8', 'Accrual comments','textarea',null,301);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='accrual8'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'accrual9';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'accrual9', 'Only at other sites, Is this a cooperative study?',null,null,302);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='accrual9'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'accrual10';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'accrual10', 'Cooperative Group Name',null,null,303);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='accrual10'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'accrual11';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'accrual11', 'Principal Investigator for the Cooperative Group',null,null,304);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='accrual11'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'accrual12';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'accrual12', 'Total number enrolled at all sites',null,null,305);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='accrual12'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'statCons1';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'statCons1', 'Primary Endpoints','textarea',null,307);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='statCons1'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'statCons2';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'statCons2', 'Sample size justification','textarea',null,308);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='statCons2'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'statCons3';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'statCons3', 'Study Design','(null)',null,309);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='(null)', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='statCons3'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'statCons4';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'statCons4', 'Stopping rules for futility, efficacy, and/or toxicity','textarea',null,310);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='statCons4'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyChair';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'studyChair', 'Study Chair',null,null,313);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='studyChair'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'secObjective';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'secObjective', 'Secondary Objective','textarea',null,314);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='secObjective'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'stdyRationale';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'stdyRationale', 'Study Rationale','textarea',null,315);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='stdyRationale'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'treatmnts';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'treatmnts', 'Treatment Agents/Devices/Interventions
	',null,null,316);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='treatmnts'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dsmbInt';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'dsmbInt', 'Internal','textarea',null,317);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='dsmbInt'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'indReqYes';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'indReqYes', 'Who is the IND Holder/Regulatory Sponsor?',null,null,319);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='indReqYes'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'indReq';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'indReq', 'Does this protocol require an IND?','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',320);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='indReq'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'ind3';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'ind3', 'IND Required, please attach the Investigator�s Brochure','splfld',null,321);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='splfld', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='ind3'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'ind4';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'ind4', 'No IND Required, Select Exemption',null,null,322);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='ind4'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'ide6';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'ide6', 'Does this study utilize an Investigational Device?','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		<OPTION value = "NA">N/A</OPTION>
		</SELECT>',324);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		<OPTION value = "NA">N/A</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='ide6'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'ide7';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'ide7', 'If No or N/A, Comments','textarea',null,325);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='ide7'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'ide8';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'ide8', 'Yes, Name of Device','textarea',null,326);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='ide8'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'ide9';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'ide9', 'Yes, Manufacturer',null,null,327);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='ide9'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'ide10';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'ide10', 'Risk Assessment Attachment (from sponsor or manufacturer)','checkbox',null,328);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='ide10'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'ide11';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'ide11', 'Is the study being conducted under an Investigational Device Exemption (IDE)? ','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',329);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='ide11'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'ide12';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'ide12', 'Exemption, IDE Holder',null,null,330);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='ide12'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'ide13';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'ide13', 'Exemption,IDE Number',null,null,331);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='ide13'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'ide14';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'ide14', 'Exemption, Please Attach the FDA Approval Letter','splfld',null,332);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='splfld', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='ide14'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'traineeInfo1';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'traineeInfo1', 'Is this research being conducted as a partial fulfillment for completion of a degree?','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',334);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='traineeInfo1'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'traineeInfo2';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'traineeInfo2', 'Please provide the following information for the Thesis/Dissertation Project Committee that is overseeing this research','textarea',null,335);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='traineeInfo2'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'traineeInfo3';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'traineeInfo3', 'Name of Chair or Advisor',null,null,336);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='traineeInfo3'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'traineeInfo4';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'traineeInfo4', 'Institution Name',null,null,337);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='traineeInfo4'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'traineeInfo5';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'traineeInfo5', 'Address','textarea',null,338);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='traineeInfo5'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'traineeInfo6';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'traineeInfo6', 'Telephone Number',null,null,339);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='traineeInfo6'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'traineeInfo7';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'traineeInfo7', 'Email Address',null,null,340);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='traineeInfo7'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'typeOfStudy';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'typeOfStudy', 'What is the type of study?',null,null,341);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='typeOfStudy'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyCond1';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'studyCond1', 'Role of MDACC Principal Investigator','checkbox',
		'{chkArray:[{data:"option1", display:" Major role in writing the protocol"},
		{data:"option2", display:"Assisted in design of the study"},
		{data:"option3", display:"Study Principal Investigator (including anticipated first-author)"},
		{data:"option4", display:"Study Co-Principal Investigator (including anticipated co-author)"},
		{data:"option5", display:"Participation only (justification for M.D. Anderson participation only as required)"}]}',343);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1=
		'{chkArray:[{data:"option1", display:" Major role in writing the protocol"},
		{data:"option2", display:"Assisted in design of the study"},
		{data:"option3", display:"Study Principal Investigator (including anticipated first-author)"},
		{data:"option4", display:"Study Co-Principal Investigator (including anticipated co-author)"},
		{data:"option5", display:"Participation only (justification for M.D. Anderson participation only as required)"}]}' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='studyCond1'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyCond2';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'studyCond2', 'Administrative Department',null,null,344);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='studyCond2'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyCond3';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'studyCond3', 'Who initiated this protocol?','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "MDAACInv">MDACC Investigator</OPTION>
		<OPTION value = "NCINIH">NCI/NIH</OPTION>
		<OPTION value = "IndstryOrOthr">Industry/Other</OPTION>
		</SELECT>',345);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "MDAACInv">MDACC Investigator</OPTION>
		<OPTION value = "NCINIH">NCI/NIH</OPTION>
		<OPTION value = "IndstryOrOthr">Industry/Other</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='studyCond3'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyCond4';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'studyCond4', 'Department',null,null,346);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='studyCond4'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyCond5';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'studyCond5', 'Phone',null,null,347);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='studyCond5'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyCond6';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'studyCond6', 'HR Data- Division',null,null,348);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='studyCond6'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyCond7';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'studyCond7', 'Co-Investigators',null,null,349);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='studyCond7'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyCond8';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'studyCond8', 'Study Chairperson Email address',null,null,350);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='studyCond8'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'treatLoc';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'treatLoc', 'Location of Treatment','textarea',null,351);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='treatLoc'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'retVisits';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'retVisits', 'How often must participants come to MDACC?',null,null,352);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='retVisits'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'hmeCare';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'hmeCare', 'Specify what, if any, treatment may be given at home.','textarea',null,353);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='hmeCare'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'pubDisp';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'pubDisp', 'Public Display','checkbox',null,354);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='pubDisp'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyTeam1';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'studyTeam1', 'Name of Person at MDACC Responsible for Data Management:',null,null,356);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='studyTeam1'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyTeam2';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'studyTeam2', 'Additional Contact',null,null,357);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='studyTeam2'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyTeam3';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'studyTeam3', 'Study Team: Roles',null,null,358);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='studyTeam3'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyTeam4';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'studyTeam4', 'Collaborators',null,null,359);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='studyTeam4'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'proDes1';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'proDes1', 'Did you participate in the design of this study?','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "YesComp">Yes -- Completely</OPTION>
		<OPTION value = "YesPart">Yes -- Partially</OPTION>
		<OPTION value = "NoSponsrDes">No -- Sponsor Designed</OPTION>
		<OPTION value = "NoProsStat">No prospective statistical design in this study</OPTION>
		</SELECT>',361);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "YesComp">Yes -- Completely</OPTION>
		<OPTION value = "YesPart">Yes -- Partially</OPTION>
		<OPTION value = "NoSponsrDes">No -- Sponsor Designed</OPTION>
		<OPTION value = "NoProsStat">No prospective statistical design in this study</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='proDes1'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'proDes2';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'proDes2', 'Did an MDACC Biostatistician participate in the design of this study?','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "YesComp">Yes -- Completely</OPTION>
		<OPTION value = "YesPart">Yes -- Partially</OPTION>
		<OPTION value = "NoSponsrDes">No -- Sponsor provided biostatistical design</OPTION>
		<OPTION value = "NoProsStat">No prospective statistical design in this study</OPTION>
		</SELECT>',362);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "YesComp">Yes -- Completely</OPTION>
		<OPTION value = "YesPart">Yes -- Partially</OPTION>
		<OPTION value = "NoSponsrDes">No -- Sponsor provided biostatistical design</OPTION>
		<OPTION value = "NoProsStat">No prospective statistical design in this study</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='proDes2'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'ctrc1';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'ctrc1', 'Will the Clinical and Translational Research Center (CTRC) be used in this study?','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',364);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='ctrc1'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'gmpFacility';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'gmpFacility', 'Will the protocol require the services of the Cell Therapy Lab Good Manufacturing Practice (GMP) facility or products to be manufactured using the GMP facility?','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',365);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='gmpFacility'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'labData1';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'labData1', 'Will a UTMDACC investigator perform pharmacologic, molecular or other laboratory studies on patient specimens?','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',367);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='labData1'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'labData2';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'labData2', 'Yes, What type of tissues will be used?','textarea',null,368);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='labData2'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'labData3';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'labData3', 'What is the type of study?',null,null,369);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='labData3'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'bioMark1';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'bioMark1', 'Are Biological Markers being evaluated in this study?','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',371);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='bioMark1'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'bioMark2';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'bioMark2', 'Yes, What type of tissues will be used?','textarea',null,372);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='bioMark2'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'bioMark3';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'bioMark3', 'Yes,The marker(s) are being evaluated in (check all that apply):','dropdown','<SELECT NAME="alternateId"><OPTION value="">Select an option</OPTION><OPTION value = "option1">Option1</OPTION><OPTION value = "option2">Option2</OPTION><OPTION value = "option3">Option3</OPTION></SELECT>',373);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId"><OPTION value="">Select an option</OPTION><OPTION value = "option1">Option1</OPTION><OPTION value = "option2">Option2</OPTION><OPTION value = "option3">Option3</OPTION></SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='bioMark3'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'bioMark4';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'bioMark4', 'The marker(s) are being evaluated to predict (check all that apply):','dropdown','<SELECT NAME="alternateId"><OPTION value="">Select an option</OPTION><OPTION value = "option1">Option1</OPTION><OPTION value = "option2">Option2</OPTION><OPTION value = "option3">Option3</OPTION></SELECT>',374);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId"><OPTION value="">Select an option</OPTION><OPTION value = "option1">Option1</OPTION><OPTION value = "option2">Option2</OPTION><OPTION value = "option3">Option3</OPTION></SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='bioMark4'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'bioMark5';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'bioMark5', 'Would there be an opportunity to consider marker evaluation as a component of this study if necessary funding were available?','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',375);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='bioMark5'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'priorProto';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'priorProto', 'Prior protocol at M. D. Anderson:',null,null,376);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='priorProto'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'addnMemoRecpnt';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'addnMemoRecpnt', 'Additional Memo Recipients','(null)',null,377);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='(null)', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='addnMemoRecpnt'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'unit';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'unit', 'Unit',null,null,378);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='unit'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'protoType';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'protoType', 'Protocol Type',null,null,379);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='protoType'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'protoPhase';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'protoPhase', 'Protocol Phase',null,null,380);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='protoPhase'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'versionStatus';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'versionStatus', 'Version Status',null,null,381);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='versionStatus'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'bioPharm';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'bioPharm', 'Is this a Pharma/Biotech sponsored trial that requires research biopsies and/or imaging?','checkbox',null,382);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='bioPharm'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'specCategories';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'specCategories', 'Does this research fit into one of the following categories below:
	1. Phase 1 first in man trial of novel agents or devices
	2. Phase 1 trial of a novel agent or device
	3�..
	','checkbox',null,383);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='specCategories'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'discountOffer';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'discountOffer', 'Your protocol may be eligible for the ReCINT program funding, would you like to apply for the discount rate of 59% to research biopsies and/or scan charges?','checkbox',null,384);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='discountOffer'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'doseModfctn';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'doseModfctn', 'Dose Modification',null,null,385);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='doseModfctn'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'sysGenCommRev';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'sysGenCommRev', 'Committee Review Date',null,null,386);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='sysGenCommRev'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'sysGenIRBApprv';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'sysGenIRBApprv', 'IRB Approved Date',null,null,387);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='sysGenIRBApprv'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'sysGenSubm';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'sysGenSubm', 'Protocol Submission',null,null,388);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='sysGenSubm'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'sysGenSubmBy';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'sysGenSubmBy', 'Submitted by',null,null,389);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='sysGenSubmBy'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'medCov1';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'medCov1', 'Is the purpose of the study to evaluate an intervention or service that falls within a Medicare benefit category (e.g., physicians'' service, inpatient hospital services, diagnostic tests, etc.) and that is not excluded from coverage (e.g., cosmetic surgery, hearing aids, etc.)?','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No -- the Study is Not a Qualifying Trial</OPTION>
		</SELECT>',390);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No -- the Study is Not a Qualifying Trial</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='medCov1'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'medCov2';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'medCov2', 'Does the trial have therapeutic intent and is not designed to exclusively test toxicity or disease pathophysiology?','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No -- the Study is Not a Qualifying Trial</OPTION>
		</SELECT>',391);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No -- the Study is Not a Qualifying Trial</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='medCov2'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'medCov3';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'medCov3', 'Does the trial enroll patients with diagnosed disease (excepts include diagnostic clinical trials which may enroll healthy volunteers in control group)?','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No -- the Study is Not a Qualifying Trial</OPTION>
		</SELECT>',392);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No -- the Study is Not a Qualifying Trial</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='medCov3'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = '21CFR1';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', '21CFR1', 'Do you store your data electronically?','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',393);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='21CFR1'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = '21CFR2';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', '21CFR2', 'Is your data used in IND, IDE or FDA regulated Activity?','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',394);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='21CFR2'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = '21CFR3';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', '21CFR3', 'Is your data stored in Microsoft Excel or Microsoft Access?','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',395);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='21CFR3'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = '21CFR4';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', '21CFR4', 'If Yes, skip questions #4-6 below','splfld',null,396);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='splfld', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='21CFR4'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = '21CFR5';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', '21CFR5', 'What is the name of your database? ','dropdown','<SELECT NAME="alternateId"><OPTION value="">Select an option</OPTION><OPTION value = "option1">Option1</OPTION><OPTION value = "option2">Option2</OPTION><OPTION value = "option3">Option3</OPTION></SELECT>',397);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId"><OPTION value="">Select an option</OPTION><OPTION value = "option1">Option1</OPTION><OPTION value = "option2">Option2</OPTION><OPTION value = "option3">Option3</OPTION></SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='21CFR5'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = '21CFR6';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', '21CFR6', 'If database name not found in the list please write the name of your database.',null,null,398);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='21CFR6'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = '21CFR7';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', '21CFR7', 'Name, phone number and email address of your database technical contact.','textarea',null,399);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='21CFR7'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'consentForm';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'consentForm', 'Are you requiring subjects to sign a consent form for this protocol?','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',400);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='consentForm'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'consentFormType';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'consentFormType', 'If yes','dropdown','<SELECT NAME="alternateId"><OPTION value="">Select an option</OPTION><OPTION value = "option1">Clinical Research Category Interventional (INT)</OPTION><OPTION value = "option2">Observational(OBS) Ancillary</OPTION><OPTION value = "option3">Ccorrelative (ANC/COR)</OPTION></SELECT>',401);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId"><OPTION value="">Select an option</OPTION><OPTION value = "option1">Clinical Research Category Interventional (INT)</OPTION><OPTION value = "option2">Observational(OBS) Ancillary</OPTION><OPTION value = "option3">Ccorrelative (ANC/COR)</OPTION></SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='consentFormType'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'protApp1';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'protApp1', '1. Is the activity a systematic investigation of multiple human subjects, including research testing and evaluation?      Yes= proceed to #2, No=1a question below','checkbox',null,402);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='protApp1'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'protApp2';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'protApp2', '2. Is the activity designed to test a hypothesis, permit conclusions to be drawn, and thereby to develop or contribute to generalizable knowledge?  Yes= proceed to #3, No =2a question below','checkbox',null,403);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='protApp2'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'protApp3';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'protApp3', '3. Are all specimen/data collected from subjects who are known to be deceased?  Yes= proceed to #4, No = Proceed to exemption questions','checkbox',null,404);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='protApp3'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'protApp4';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'protApp4', '4. Will the research involve the collection of analysis of genetic information from specimens/data of deceased subjects?  Yes = proceed to exemption questions, No = question 4a below','checkbox',null,405);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='protApp4'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'protApp1a';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'protApp1a', '1a. Will the data be listed or described separately for each subject, ie, no aggregate data will be compiled? Yes= case report No= Practice of Medicine Provide summary or description accordingly. ','checkbox',null,406);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='protApp1a'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'protApp2a';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'protApp2a', '2a. Will the data be listed or described separately for each subject, ie, no aggregate data will be compiled? Yes= case report No= Quality Improvement Provide description or summary accordingly. ','checkbox',null,407);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='protApp2a'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'protApp4a';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'protApp4a', '4a. Will the research impact living individuals? Yes or No = Proceed to question No.5 ','checkbox',null,408);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='protApp4a'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'protApp5';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'protApp5', '5. Please describe how you will verify that subjects are deceased?  NOT HUMAN SUBJECTS RESEARCH, Provide a description to verify Not huma subjects research','textarea',null,409);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='protApp5'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'exemp45CFR1';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'exemp45CFR1', '1. Will the research be conducted in established or commonly accepted educational settings, involving normal education practices?  (This may include schools,  colleges, and other sites where educational activities regularly occur.)   Yes or No= proceed to #2','checkbox',null,410);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='exemp45CFR1'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'exemp45CFR2';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'exemp45CFR2', ' 2. Will the research involve the use of educational tests, survey procedures, or observation of public behavior? Yes=  Question 2a. or No = Proceed to #4','checkbox',null,411);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='exemp45CFR2'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'exemp45CFR3';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'exemp45CFR3', '3. Not applicable to MDACC research (Contact the IRB if you feel this applies) ','checkbox',null,412);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='exemp45CFR3'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'exemp45CFR4';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'exemp45CFR4', '4. Does the research only involve the use or study of existing (already available or on the shelf) data, documents, records, or pathological or diagnostic specimens?   Yes= 4a question below proceed to #5, No = proceed to question 5','checkbox',null,413);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='exemp45CFR4'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'exemp45CFR5';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'exemp45CFR5', '5. Does the research evaluate or examine public benefit or service programs, such as demonstration or dissemination projects? Y/N= proceed to #6','checkbox',null,414);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='exemp45CFR5'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'exemp45CFR6';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'exemp45CFR6', '6. Does the research involve taset and food quality evaluations?  Y/N= proceed with Additional Information.','checkbox',null,415);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='exemp45CFR6'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'exemp45CFR2a';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'exemp45CFR2a', '2a.  Will the research involve survey procedures, interview procedures, or observation of public behavior where the investigatore or research staff partifcipates in the activities being observed?Yes= questions 2b No = proceed to to question #4','checkbox',null,416);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='exemp45CFR2a'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'exemp45CFR2b';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'exemp45CFR2b', '2b. Will the information obtained be recorded in such a manner that human subjects can be identified, directly or through identifiers linked to the subjects?   Y/N  = proceed to question 4 ','checkbox',null,417);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='exemp45CFR2b'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'exemp45CFR4a';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'exemp45CFR4a', '4a. Are the data, documents, records, or pathological or diagnostic specimens publicly available?   Yes = proceed to question 5 or no= question 4b, then proceed to #5','checkbox',null,418);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='exemp45CFR4a'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'exemp45CFR4b';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'exemp45CFR4b', ' 4b.  Will the existing information be recorded in such a manner that human subjects can e identified, directly or through identifiers linked to the subjects? **(see help text for a list of identifiers) Y/N  proceed to question #5','checkbox',null,419);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='exemp45CFR4b'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'protApp6';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'protApp6', 'If yes, Is the activity designed to test a hypothesis, permit conclusions to be drawn, and thereby to develop or contribute to generalizable knowledge?','checkbox',null,420);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='protApp6'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'protApp7';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'protApp7', 'If no, Will data be listed or described separately  for each subject, i.e., no aggregate data 
	will be compiled?
	Yes=Case report No=Practice of Medicine
	','checkbox',null,421);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='protApp7'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'protApp8';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'protApp8', 'If yes to 217 Are all specimen/data collected from subjects who are known to be deceased?','checkbox',null,422);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='protApp8'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'resPop4';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'resPop4', 'Is there any direct interaction or direct observation of subjects?','dropdown','<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',423);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='resPop4'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'resPop5';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'resPop5', 'Describe your direct interaction or direct observation of subjects','textarea',null,424);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='resPop5'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'resPop6';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'resPop6', '(from resPop5, proceed to next section) Location Where Research Will be Conducted','textarea',null,425);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='resPop6'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'resPop7';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'resPop7', 'If No= Skip to Section, Participant Population','splfld',null,426);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='splfld', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='resPop7'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'resPop8';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'resPop8', 'Location where research will be conducted','checkbox',
		'{chkArray:[{data:"option1", display:"MDACC/Regional Centers"},
		{data:"option2", display:"Non-MDACC Hospital/Clinic Healthcare Facility/Harris County Hospital District Facility"},
		{data:"option3", display:"Community Center/Shopping Mall (e.g. religious facility)"},
		{data:"option4", display:"Educational Campus (e.g. elementary or secondary school or a university campus"},
		{data:"option5", display:"Subject''s Home (list information on city; county; state; or country where the home is located below)"},
		{data:"option6", display:"Internet Research (list website names below)"},
		{data:"option7", display:"International Site"},
		{data:"option8", display:"Other"}]}',427);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1='{chkArray:[{data:"option1", display:"MDACC/Regional Centers"},
		{data:"option2", display:"Non-MDACC Hospital/Clinic Healthcare Facility/Harris County Hospital District Facility"},
		{data:"option3", display:"Community Center/Shopping Mall (e.g. religious facility)"},
		{data:"option4", display:"Educational Campus (e.g. elementary or secondary school or a university campus"},
		{data:"option5", display:"Subject''s Home (list information on city; county; state; or country where the home is located below)"},
		{data:"option6", display:"Internet Research (list website names below)"},
		{data:"option7", display:"International Site"},
		{data:"option8", display:"Other"}]}' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='resPop8'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'resPop9';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'resPop9', 'Identify the name and location for each site selected above (list locations, Regional Cancer Centers, web sites, etc.)','textarea',null,428);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='resPop9'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'juvinileEligNo';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'juvinileEligNo', 'Studies that exclude children must have appropriate justification.  Please select all that apply: ','checkbox',
		'{chkArray:[{data:"option1", display:"Phase I or Phase II study targeting cancer that is very unusual in pediatrics (e.g., prostate, lung, breast, chronic lymophocytic leukemia,etc.)"},
		{data:"option2", display:"FDA has required sponsor to limit to age 18 and above (applicable only to Phase I studies)."},
		{data:"option3", display:"Phase II or III study with no Phase I data for the drug in pediatrics. Please provide a letter from the Sponsor stating if a Phase I study planned for patients <18 years of age. (May include file attachment)"},
		{data:"option4", display:"Phase I study to include participants with diseases that occur in pediatrics (e.g., leukemia, lymphoma, sarcomas, brain tumors), but sponsor will not allow patients <18 years of age. Please include a justification letter from the Sponsor. (May include file attachment)"},
		{data:"option5", display:"Other"}]}',428);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1=
		'{chkArray:[{data:"option1", display:"Phase I or Phase II study targeting cancer that is very unusual in pediatrics (e.g., prostate, lung, breast, chronic lymophocytic leukemia,etc.)"},
		{data:"option2", display:"FDA has required sponsor to limit to age 18 and above (applicable only to Phase I studies)."},
		{data:"option3", display:"Phase II or III study with no Phase I data for the drug in pediatrics. Please provide a letter from the Sponsor stating if a Phase I study planned for patients <18 years of age. (May include file attachment)"},
		{data:"option4", display:"Phase I study to include participants with diseases that occur in pediatrics (e.g., leukemia, lymphoma, sarcomas, brain tumors), but sponsor will not allow patients <18 years of age. Please include a justification letter from the Sponsor. (May include file attachment)"},
		{data:"option5", display:"Other"}]}' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='juvinileEligNo'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'juvinileEligNo1';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'juvinileEligNo1', 'If other:',null,null,429);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='juvinileEligNo1'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'biosafety4';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'biosafety4', 'If Yes, Please provide the IBC protocol number or contact the IBC coordinator for information at 713-563-3879.
	IBC Protocol Number:','textarea',null,430);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='biosafety4'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'biosafety5';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'biosafety5', 'If yes, Are you using Tisseel or Evicel or similar?','checkbox',null,431);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='biosafety5'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'juvinileEligYes';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'juvinileEligYes', 'Additional Comment','textarea',null,432);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='juvinileEligYes'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'recrutPopIncarc';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'recrutPopIncarc', 'Will the recruitment population at M. D. Anderson include persons who are incarcerated at time of enrollment (e.g., prisoners) or likely to become incarcerated during the study?','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',433);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='recrutPopIncarc'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'grantingAgency';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'grantingAgency', 'Does the Study have a Sponsor, Supporter or Granting Agency?','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',434);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='grantingAgency'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'grntnAgncyData';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'grntnAgncyData', 'Will the Sponsor/Supporter/Granting Agency receive data?','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',435);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='grntnAgncyData'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'radioComp';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'radioComp', 'Does this protocol include the administration of a radioactive compound (or drug) to a patient intended to obtain basic information regarding metabolism (including kinetics, distribution, and localization) of the drug or regarding human physiology, pathophysiology, biochemistry, but not intended for immediate therapeutic, diagnostic, or similar purposes or to determine the safety and effectiveness of the drug in humans for such purposes (i.e. to carry out a clinical study)?','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',436);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='radioComp'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'radioCompAvail';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'radioCompAvail', 'Is the radioactive compound (or drug) FDA approved and/or commercially available?','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',437);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='radioCompAvail'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'clicResrCtgry';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'clicResrCtgry', 'Clinical Research Category','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "INT">Interventional (INT)</OPTION>
		<OPTION value = "OBS">Observational (OBS)</OPTION>
		<OPTION value = "ANCCOR">Ancillary or Correlative (ANC/COR)</OPTION>
		</SELECT>',438);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "INT">Interventional (INT)</OPTION>
		<OPTION value = "OBS">Observational (OBS)</OPTION>
		<OPTION value = "ANCCOR">Ancillary or Correlative (ANC/COR)</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='clicResrCtgry'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'preCliDrugDev';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'preCliDrugDev', 'Was preclinical work to develop the therapy or methodology being tested in this protocol performed at UTMDACC or by UTMDACC faculty?','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',439);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='preCliDrugDev'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'stdyDisease';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'stdyDisease', '<A href="#"><img src="../images/jpg/help.jpg" border="0" 
	onmouseover="return overlib(''This section should contain a background discussion of the target disease state to which the investigational product(s) hold promise, and any pathophysiology relevant to potential study treatment action.'',
	CAPTION,''Study Disease'',LEFT, ABOVE);" onmouseout="return nd();" 	alt=""></img></A> Study Disease (Description of Disease)','textarea',null,440);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='stdyDisease'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'preClincData';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'preClincData', '<A href="#"><span onmouseover="return overlib(''Summarize the available non-clinical data (published or available unpublished data) that could have clinical significance..'',
	CAPTION,''Pre-clinical Data'',LEFT, ABOVE);" onmouseout="return nd();" alt="">Pre-clinical Data</span></A>','textarea',null,441);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='preClincData'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'clincDataToDate';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'clincDataToDate', 'Clinical Data to Date<br>
	<i><small>Summarize the available clinical study data (published or available unpublished data) with relevance to the protocol under construction -- if none is available, include a statement that there is no available clinical research data to date on the investigational product.</small></i>','textarea',null,442);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='clincDataToDate'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'corrStdBkgrnd';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'corrStdBkgrnd', 'Correlative Studies Background','textarea',null,443);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='corrStdBkgrnd'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'explrtObjctv';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'explrtObjctv', 'Exploratory Objective','textarea',null,444);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='explrtObjctv'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'elgbltyCriteria';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'elgbltyCriteria', '<h4>Eligibility Criteria</h4>','splfld',null,445);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='splfld', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='elgbltyCriteria'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'womenPartici';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'womenPartici', 'Women','dropdown','<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',446);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='womenPartici'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'subRecrScreen';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'subRecrScreen', '<h4>Subject Recruitment and Screening</h4>','splfld',null,447);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='splfld', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='subRecrScreen'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'stdyCalndr';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'stdyCalndr', 'Study Calendar/Schedule of Events','textarea',null,448);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='stdyCalndr'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'patAssessmnt';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'patAssessmnt', 'Patient Assessments','splfld',null,449);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='splfld', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='patAssessmnt'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'baselineEvals';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'baselineEvals', '<li>Screening/Baseline','textarea',null,450);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='baselineEvals'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'medMontrng';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'medMontrng', 'Medical Monitoring','textarea',null,451);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='medMontrng'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'onstdyEvals';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'onstdyEvals', '<li>On-study/Treatment Evaluations','textarea',null,452);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='onstdyEvals'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'endTheraEvals';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'endTheraEvals', '<li>End of Therapy Evaluations','textarea',null,453);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='endTheraEvals'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'followUp';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'followUp', '<li>Follow-up','textarea',null,454);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='followUp'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'lngTrmFollowUp';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'lngTrmFollowUp', 'Long Term Follow-up','textarea',null,455);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='lngTrmFollowUp'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'womenParticiYes';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'womenParticiYes', 'Additional Comment','textarea',null,456);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='womenParticiYes'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'minorPartici';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'minorPartici', 'Minorities','dropdown','<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',457);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='minorPartici'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'minorParticiYes';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'minorParticiYes', 'Additional Comment','textarea',null,458);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='minorParticiYes'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'pregEligbltyYes';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'pregEligbltyYes', 'Additional Comment','textarea',null,459);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='pregEligbltyYes'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'consortiName';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'consortiName', 'Consortium Name',null,null,460);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='consortiName'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'priInvConsor';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'priInvConsor', 'Principal Investigator for the Consortium',null,null,461);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='priInvConsor'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'descDrug';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'descDrug', 'Description of Drug','textarea',null,462);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='descDrug'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'descDevice';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'descDevice', 'Description of Device','textarea',null,463);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='descDevice'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'doseRationale';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'doseRationale', 'Dose Rationale','textarea',null,464);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='doseRationale'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'trtmntArms';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'trtmntArms', 'Treatment Arms','textarea',null,465);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='trtmntArms'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'diseaseGrp';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'diseaseGrp', '<i>Disease Group</i>','splfld',null,466);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='splfld', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='diseaseGrp'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'subRecrLoc';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'subRecrLoc', '<i>Location of Subject Recruitment</i>','splfld',null,467);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='splfld', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='subRecrLoc'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'recrStrategy';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'recrStrategy', '<i>Recruitment Strategy</i>','splfld',null,468);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='splfld', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='recrStrategy'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'constProced';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'constProced', '<i>Consenting Procedures</i>','splfld',null,469);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='splfld', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='constProced'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'tarPopPat';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'tarPopPat', 'Targeted Population of Patients','textarea',null,470);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='tarPopPat'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'stdyIncludes';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'stdyIncludes', 'Study includes','checkbox',
		'{chkArray:[{data:"option1", display:"Drug"},
		{data:"option2", display:"Device"},
		{data:"option3", display:"Imaging"}]}',471);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1=
		'{chkArray:[{data:"option1", display:"Drug"},
		{data:"option2", display:"Device"},
		{data:"option3", display:"Imaging"}]}' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='stdyIncludes'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'spnsrdProt';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'spnsrdProt', 'Sponsored protocol','dropdown','<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',472);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='spnsrdProt'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'protAttUpld';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'protAttUpld', 'Protocol attachment uploaded','dropdown','<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',473);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='protAttUpld'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'wthdrwSubHow';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'wthdrwSubHow', 'When and how to withdraw subjects','textarea',null,474);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='wthdrwSubHow'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'wthdrwFollow';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'wthdrwFollow', 'Data collection and follow-up for withdrawn subjects','textarea',null,475);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='wthdrwFollow'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'stdRemCriteria';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'stdRemCriteria', 'Criteria for removal of study','textarea',null,476);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='stdRemCriteria'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'trtmntPlan';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'trtmntPlan', 'Treatment Plan','textarea',null,477);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='trtmntPlan'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'stdDesEndPts';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'stdDesEndPts', '<h4>Study Design and Endpoints</>','splfld',null,478);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='splfld', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='stdDesEndPts'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'scndryEndPts';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'scndryEndPts', 'Secondary Endpoints','textarea',null,479);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='scndryEndPts'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'expEndPts';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'expEndPts', 'Exploratory Endpoints','textarea',null,480);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='expEndPts'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'accrualHead';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'accrualHead', '<h4>Accrual</>','splfld',null,481);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='splfld', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='accrualHead'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'trtmntGrpHead';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'trtmntGrpHead', '<h4>Methods for Assigning Subjects to Treatment Groups</>','splfld',null,482);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='splfld', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='trtmntGrpHead'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'rndmnTrtGrp';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'rndmnTrtGrp', 'Randomization','textarea',null,483);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='rndmnTrtGrp'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'blndTrtGrp';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'blndTrtGrp', 'Blinding','textarea',null,484);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='blndTrtGrp'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'unblnTrtGrp';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'unblnTrtGrp', 'Unblinding','textarea',null,485);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='unblnTrtGrp'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'rplSubject';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'rplSubject', 'Replacement of Subjects','textarea',null,486);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='rplSubject'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'statAnalPlanHd';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'statAnalPlanHd', '<h4>Statistical Analysis Plan</>','splfld',null,487);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='splfld', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='statAnalPlanHd'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'statAnalPlan';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'statAnalPlan', 'Statistical Analysis Plan','textarea',null,488);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='statAnalPlan'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'intrMonHead';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'intrMonHead', '<h4>Interim Monitoring Plan</>','splfld',null,489);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='splfld', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='intrMonHead'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'cohrtSummHead';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'cohrtSummHead', '<h4>Cohort Summaries</>','splfld',null,490);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='splfld', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='cohrtSummHead'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'cohrtSumm';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'cohrtSumm', 'Cohort Summaries','textarea',null,491);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='cohrtSumm'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'defSAE';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'defSAE', 'Define SAEs','textarea',null,492);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='defSAE'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'recrdAE';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'recrdAE', 'Recording of AEs','textarea',null,493);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='recrdAE'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'repSAEs';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'repSAEs', 'Reports of SAEs and Unanticipated Problems','textarea',null,494);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='repSAEs'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'repSponsr';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'repSponsr', 'Reporting to Sponsor','textarea',null,495);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='repSponsr'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'repIRB';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'repIRB', 'Reporting to IRB','textarea',null,496);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='repIRB'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dsmbExt';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'dsmbExt', 'Independent/External','textarea',null,497);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='dsmbExt'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dataConfPlan';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'dataConfPlan', '<h4>Data Confidentiality Plan</>','splfld',null,498);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='splfld', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='dataConfPlan'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dataCollTool';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'dataCollTool', '<h4>Data Collection Tool</>','splfld',null,499);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='splfld', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='dataCollTool'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'recRetention';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'recRetention', '<h4>Records Retention</>','splfld',null,500);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='splfld', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='recRetention'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundnSrc';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'fundnSrc', '<h4>Funding Source</>','splfld',null,501);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='splfld', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='fundnSrc'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'subStipndPay';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'subStipndPay', '<h4>Subject Stipends/Payments</>','splfld',null,502);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='splfld', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='subStipndPay'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'publPlan';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'publPlan', 'Publication Plan','textarea',null,503);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='publPlan'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'references';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'references', 'References','textarea',null,504);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='references'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'selMoonshot';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'selMoonshot', 'Select a Moonshot','dropdown','<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "option1">APOLLO</OPTION>
		<OPTION value = "option2">Big Data</OPTION>
		<OPTION value = "option3">Cancer Control and Prevention</OPTION>
		<OPTION value = "option4">Center for the Co-Clinical (CCCT)</OPTION>
		<OPTION value = "option5">Clinical Genomics</OPTION>
		<OPTION value = "option6">Immunotherapy</OPTION>
		<OPTION value = "option7">Institute for Applied Cancer Science</OPTION>
		<OPTION value = "option8">Institute for Personal Cancer Therapy</OPTION>
		<OPTION value = "option9">Proteomics</OPTION>
		</SELECT>',505);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "option1">APOLLO</OPTION>
		<OPTION value = "option2">Big Data</OPTION>
		<OPTION value = "option3">Cancer Control and Prevention</OPTION>
		<OPTION value = "option4">Center for the Co-Clinical (CCCT)</OPTION>
		<OPTION value = "option5">Clinical Genomics</OPTION>
		<OPTION value = "option6">Immunotherapy</OPTION>
		<OPTION value = "option7">Institute for Applied Cancer Science</OPTION>
		<OPTION value = "option8">Institute for Personal Cancer Therapy</OPTION>
		<OPTION value = "option9">Proteomics</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='selMoonshot'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'indInfrmtnHd';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'indInfrmtnHd', '<h4>IND Information</>','splfld',null,506);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='splfld', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='indInfrmtnHd'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'ideInfrmtnHd';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'ideInfrmtnHd', '<h4>IDE Information</>','splfld',null,507);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='splfld', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='ideInfrmtnHd'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'trtmntRegPlanHd';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'trtmntRegPlanHd', '<h4>Treatment Regimen/Plan</>','splfld',null,508);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='splfld', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='trtmntRegPlanHd'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'concomTheraHd';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'concomTheraHd', '<h4>Concomitant Therapy</>','splfld',null,509);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='splfld', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='concomTheraHd'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'blndinProcdrsHd';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'blndinProcdrsHd', '<h4>Blinding of Procedures</>','splfld',null,510);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='splfld', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='blndinProcdrsHd'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drgMngmntHd';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'drgMngmntHd', '<h4>Drug Management</>','splfld',null,511);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='splfld', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='drgMngmntHd'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'devMangmntHd';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'devMangmntHd', '<h4>Device Management</>','splfld',null,512);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='splfld', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='devMangmntHd'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'subCmplMntrngHd';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'subCmplMntrngHd', '<h4>Subject Compliance Monitoring</>','splfld',null,513);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='splfld', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='subCmplMntrngHd'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'radioisotopeHd';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'radioisotopeHd', '<h4>Contrast Agents or Radioisotopes</>','splfld',null,514);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='splfld', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='radioisotopeHd'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'medclDevcsHd';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'medclDevcsHd', '<h4>Medical Devices</>','splfld',null,515);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='splfld', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='medclDevcsHd'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'invstgnDevHd';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'invstgnDevHd', '<h4>Investigational Devices</>','splfld',null,516);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='splfld', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='invstgnDevHd'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'biomrkrStdsHd';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'biomrkrStdsHd', '<h4>Biomarker Studies</>','splfld',null,517);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='splfld', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='biomrkrStdsHd'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'labCorrStdsHd';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'labCorrStdsHd', '<h4>Laboratory Correlative Studies</>','splfld',null,518);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='splfld', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='labCorrStdsHd'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'concomThera';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'concomThera', 'Concomitant Therapy','textarea',null,519);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='concomThera'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'trtmntDuration';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'trtmntDuration', 'Treatment duration',null,null,520);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='trtmntDuration'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'subComplMontr';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'subComplMontr', 'Subject Compliance Monitoring','textarea',null,521);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='subComplMontr'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'radstpAddnCmn';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'radstpAddnCmn', 'Additional Comments','textarea',null,522);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='radstpAddnCmn'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'commIncCritra';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'commIncCritra', 'Common Inclusion Criteria','checkbox',
		'{chkArray:[{data:"option1", display:"Age >/= 18 years old"},
		{data:"option2", display:"Subjects must have histologically or cytologically confirmed disease"},
		{data:"option3", display:"Left Ventricular Ejection fraction (LVEF) >/= Lower Limit of Normal Range (LLN)"},
		{data:"option4", display:"ECOG performance status 0-2"},
		{data:"option5", display:"Capable of understanding and complying with the protocol requirements and has signed the informed consent document"},
		{data:"option6", display:"Women of childbearing potential must have a negative pregnancy test at screening"},
		{data:"option7", display:" Measurable disease by Response Evaluation Criteria in Solid Tumors (RECIST) 1.1 criteria"},
		{data:"option8", display:"Disease progression in the past 14 months"},
		{data:"option9", display:"Adequate thyroid stimulating hormone (TSH) suppression (<0.5 mIU/L)"}]}',523);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1=
		'{chkArray:[{data:"option1", display:"Age >/= 18 years old"},
		{data:"option2", display:"Subjects must have histologically or cytologically confirmed disease"},
		{data:"option3", display:"Left Ventricular Ejection fraction (LVEF) >/= Lower Limit of Normal Range (LLN)"},
		{data:"option4", display:"ECOG performance status 0-2"},
		{data:"option5", display:"Capable of understanding and complying with the protocol requirements and has signed the informed consent document"},
		{data:"option6", display:"Women of childbearing potential must have a negative pregnancy test at screening"},
		{data:"option7", display:" Measurable disease by Response Evaluation Criteria in Solid Tumors (RECIST) 1.1 criteria"},
		{data:"option8", display:"Disease progression in the past 14 months"},
		{data:"option9", display:"Adequate thyroid stimulating hormone (TSH) suppression (<0.5 mIU/L)"}]}' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='commIncCritra'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'commExcCritra';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'commExcCritra', 'Common Exclusion Criteria','checkbox',
		'{chkArray:[{data:"option1", display:"Unable to swallow tablets"},
		{data:"option2", display:"Pregnant or breastfeeding"},
		{data:"option3", display:"Unable or unwilling to abide by the study protocol or cooperate fully with the investigator or designee"},
		{data:"option4", display:"Patients may not be receiving any other investigational agents"},
		{data:"option5", display:"Patients with known untreated brain metastases"},
		{data:"option6", display:"Patients with uncontrolled malabsorption syndromes"},
		{data:"option7", display:"Patients with a history of congestive heart failure of any New York Heart Association class"},
		{data:"option8", display:"Any medical or psychiatric illness which, in the opinion of the principal investigator, would compromise the patient�s ability to tolerate this treatment regimen"},
		{data:"option9", display:"Hypertension (defined as a systolic blood pressure [SBP] >140 mm Hg and/or diastolic blood pressure [DBP] > 90 mm Hg, which cannot be controlled by anti-hypertensive therapy)"}]}',524);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1=
		'{chkArray:[{data:"option1", display:"Unable to swallow tablets"},
		{data:"option2", display:"Pregnant or breastfeeding"},
		{data:"option3", display:"Unable or unwilling to abide by the study protocol or cooperate fully with the investigator or designee"},
		{data:"option4", display:"Patients may not be receiving any other investigational agents"},
		{data:"option5", display:"Patients with known untreated brain metastases"},
		{data:"option6", display:"Patients with uncontrolled malabsorption syndromes"},
		{data:"option7", display:"Patients with a history of congestive heart failure of any New York Heart Association class"},
		{data:"option8", display:"Any medical or psychiatric illness which, in the opinion of the principal investigator, would compromise the patient�s ability to tolerate this treatment regimen"},
		{data:"option9", display:"Hypertension (defined as a systolic blood pressure [SBP] >140 mm Hg and/or diastolic blood pressure [DBP] > 90 mm Hg, which cannot be controlled by anti-hypertensive therapy)"}]}' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='commExcCritra'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drgMngntFile';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'drgMngntFile', 'Do you have a drug management file?','dropdown','<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',525);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='drgMngntFile'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drgSupMfgr';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'drgSupMfgr', 'Drug Supplier/Manufacturer','textarea',null,526);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='drgSupMfgr'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'receiptDrgSup';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'receiptDrgSup', 'Receipt of Drug Supplies','textarea',null,527);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='receiptDrgSup'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'prepAdmDrgPkg';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'prepAdmDrgPkg', 'Preparation and Administration of Drug and Packaging','textarea',null,528);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='prepAdmDrgPkg'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drgStorage';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'drgStorage', 'Storage','textarea',null,529);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='drgStorage'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'stdyDrgDispn';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'stdyDrgDispn', 'Dispensing of Study Drug','textarea',null,530);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='stdyDrgDispn'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'retDestStdDrg';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'retDestStdDrg', 'Return/Destruction of Study Drug','textarea',null,531);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='retDestStdDrg'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'devMngntFile';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'devMngntFile', 'Do you have a device management file?','dropdown','<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',532);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='devMngntFile'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'devSupMfgr';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'devSupMfgr', 'Device Supplier/Manufacturer','textarea',null,533);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='devSupMfgr'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'receiptDevice';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'receiptDevice', 'Receipt of Device','textarea',null,534);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='receiptDevice'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'devStorage';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'devStorage', 'Storage of Device','textarea',null,535);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='devStorage'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'returnDevice';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'returnDevice', 'Return of Device','textarea',null,536);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='returnDevice'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'labCorrStdies';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'labCorrStdies', 'Laboratory Correlative Studies','textarea',null,537);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='labCorrStdies'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'subCompReimb';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'subCompReimb', 'Will subjects receive compensation or reimbursements?','dropdown','<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',538);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='subCompReimb'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'payProratd';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'payProratd', 'Will payments be prorated?','dropdown','<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',539);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='payProratd'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'descrbProRatd';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'descrbProRatd', 'Describe','textarea',null,540);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='descrbProRatd'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'othPrRevFnd';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'othPrRevFnd', 'Other peer-reviewed funding','dropdown','<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "option1">Industry</OPTION>
		<OPTION value = "option2">Departmental Funds</OPTION>
		<OPTION value = "option3">Donor Funds</OPTION>
		<OPTION value = "option4">Unfunded</OPTION>
		</SELECT>',541);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "option1">Industry</OPTION>
		<OPTION value = "option2">Departmental Funds</OPTION>
		<OPTION value = "option3">Donor Funds</OPTION>
		<OPTION value = "option4">Unfunded</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='othPrRevFnd'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'othrNumbrs';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'othrNumbrs', '<h4>Other Numbers</h4>','splfld',null,543);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='splfld', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='othrNumbrs'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'siteNumber';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'siteNumber', 'Site Number',null,null,544);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='siteNumber'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'ps_cfs';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'ps_cfs', 'PS CFS (Resource One)',null,null,545);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='ps_cfs'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fredNum';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'fredNum', 'FReD#',null,null,546);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='fredNum'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'labTestsHead';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'labTestsHead', '<h4>Laboratory Tests</h4>','splfld',null,547);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='splfld', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='labTestsHead'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'labTstPatMat';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'labTstPatMat', 'Will laboratory tests be performed on patient materials?','dropdown','<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',548);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='labTstPatMat'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'patEnrEligTrtmt';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'patEnrEligTrtmt', 'Will these tests be used for diagnosis, prevention, or treatment or health assessment purposes, including biomarker or genomic tests, for determining patient enrollment/eligibility/treatment assignment?','dropdown','<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',549);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='patEnrEligTrtmt'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'patEnrEliTrtLab';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'patEnrEliTrtLab', 'Please select the laboratories that will be used for conducting tests for diagnosis, prevention, or treatment or health assessment purposes, including biomarker or genomic tests, for determining patient enrollment/eligibility/treatment assignment','checkbox',
		'{chkArray:[{data:"option1", display:"Division of Pathology & Laboratory Medicine (CLIA Certified Laboratory)"},
		{data:"option2", display:"Cellular Therapies CLIA Certified Laboratory (Department of Stem Cell Therapy CLIA Certified Laboratory)"},
		{data:"option3", display:"Other"}]}',550);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1=
		'{chkArray:[{data:"option1", display:"Division of Pathology & Laboratory Medicine (CLIA Certified Laboratory)"},
		{data:"option2", display:"Cellular Therapies CLIA Certified Laboratory (Department of Stem Cell Therapy CLIA Certified Laboratory)"},
		{data:"option3", display:"Other"}]}' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='patEnrEliTrtLab'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'cliaAttch';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'cliaAttch', 'If Other, provide name of the performing laboratory identification and contact information (attach CLIA certificate)','textarea',null,551);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='cliaAttch'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'namePurTest';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'namePurTest', 'Provide the name of the test(s), the purpose of the test, and the patient materials required (e.g. archived tissue, slides, and/or blood)','textarea',null,552);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='namePurTest'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'patMatTest';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'patMatTest', 'What type of patient materials will be collected/used for test for diagnosis, prevention or treatment or health assessment purposes, including for determining Patient enrollment/eligibility/treatment assignment?','checkbox',
		'{chkArray:[{data:"option1", display:"Archived pathology material (e.g. tissue sections on unstained slides)"},
		{data:"option2", display:"Fresh Tissue in media"},
		{data:"option3", display:"Fresh Frozen Tissue"},
		{data:"option4", display:"Serum/Blood"},
		{data:"option5", display:"Other body fluids"}]}',553);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1=
		'{chkArray:[{data:"option1", display:"Archived pathology material (e.g. tissue sections on unstained slides)"},
		{data:"option2", display:"Fresh Tissue in media"},
		{data:"option3", display:"Fresh Frozen Tissue"},
		{data:"option4", display:"Serum/Blood"},
		{data:"option5", display:"Other body fluids"}]}' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='patMatTest'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'patMatTestOthr';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'patMatTestOthr', 'Specify Other','textarea',null,554);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='patMatTestOthr'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'patMatResLab';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'patMatResLab', 'Will patient materials (blood, tissue sections on unstained slides, slides) need to be sent to a research (non-CLIA certified) laboratory?','dropdown','<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',555);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='patMatResLab'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'patMatResLabY1';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'patMatResLabY1', 'Provide the laboratory name and address that specimens will be sent to',null,null,556);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL= null, CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='patMatResLabY1'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'patMatResLabY2';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'patMatResLabY2', 'Is this a research non-CLIA laboratory outside of MD Anderson?','dropdown','<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>',557);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='dropdown', CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
		<OPTION value = "">Select an option</OPTION>
		<OPTION value = "Yes">Yes</OPTION>
		<OPTION value = "No">No</OPTION>
		</SELECT>' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='patMatResLabY2'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'patMatResLabY3';	
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'patMatResLabY3', 'What type of patient materials need to be sent to a research (non-CLIA certified) laboratory?','checkbox',
		'{chkArray:[{data:"option1", display:"Archived pathology material (e.g. tissue sections on unstained slides)"},
		{data:"option2", display:"Fresh Tissue"},
		{data:"option3", display:"Serum/Blood"},
		{data:"option4", display:"Other body fluids"}]}',558);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='checkbox', CODELST_CUSTOM_COL1=
		'{chkArray:[{data:"option1", display:"Archived pathology material (e.g. tissue sections on unstained slides)"},
		{data:"option2", display:"Fresh Tissue"},
		{data:"option3", display:"Serum/Blood"},
		{data:"option4", display:"Other body fluids"}]}' WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='patMatResLabY3'; 
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nonCliaOthr';
	IF (v_CodeExists = 0) THEN 
		INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_SEQ) 
		VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'nonCliaOthr', 'Specify Other','textarea',null,559);
	ELSE
		UPDATE ER_CODELST SET CODELST_CUSTOM_COL='textarea', CODELST_CUSTOM_COL1= null WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP='nonCliaOthr'; 
	END IF;

	commit;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,317,1,'05_MSDscript.sql',sysdate,'v9.3.0 #718');

commit;