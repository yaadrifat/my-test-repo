set define off;

declare
v_exists NUMBER := 0;
v_nextVal NUMBER := 0;

v_PK_WORKFLOW NUMBER := 0;
v_PK_ACTIVITY NUMBER := 0;
v_PK_WORKFLOWACTIVITY NUMBER := 0;

begin
	v_PK_WORKFLOW := 0;
	SELECT count(*) INTO v_exists FROM WORKFLOW WHERE WORKFLOW_TYPE = 'STUDY_ACTIVATION';
	
	IF (v_exists = 0) THEN
		v_PK_WORKFLOW := SQ_WORKFLOW.NEXTVAL();

		Insert into WORKFLOW (PK_WORKFLOW,WORKFLOW_NAME,WORKFLOW_DESC,WORKFLOW_TYPE) 
		values (v_PK_WORKFLOW,'Study Activation','Study Activation','STUDY_ACTIVATION');
		COMMIT;
	ELSE 
		SELECT PK_WORKFLOW INTO v_PK_WORKFLOW FROM WORKFLOW WHERE WORKFLOW_TYPE = 'STUDY_ACTIVATION';
	END IF;
	---------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------
	
	v_PK_ACTIVITY := 0;
	SELECT count(*) INTO v_exists FROM ACTIVITY WHERE ACTIVITY_NAME = 'protocolActivation';
	
	IF (v_exists = 0) THEN
		v_PK_ACTIVITY := SQ_ACTIVITY.NEXTVAL();
		
		Insert into ACTIVITY (PK_ACTIVITY,ACTIVITY_NAME,ACTIVITY_DESC) 
		values (v_PK_ACTIVITY,'protocolActivation','Protocol Activation');
		COMMIT;
	ELSE
		SELECT PK_ACTIVITY INTO v_PK_ACTIVITY FROM ACTIVITY WHERE ACTIVITY_NAME = 'protocolActivation';
	END IF;
	
	v_PK_WORKFLOWACTIVITY := 0;
	IF (v_PK_ACTIVITY > 0) THEN
		SELECT COUNT(*) INTO v_exists FROM WORKFLOW_ACTIVITY WHERE FK_WORKFLOWID = v_PK_WORKFLOW AND FK_ACTIVITYID = v_PK_ACTIVITY;
	
		IF (v_exists = 0) THEN
			v_PK_WORKFLOWACTIVITY := SQ_WORKFLOWACTIVITY.NEXTVAL();
			
			Insert into WORKFLOW_ACTIVITY (PK_WORKFLOWACTIVITY,WA_NAME,FK_WORKFLOWID,FK_ACTIVITYID,WA_SEQUENCE,WA_TERMINATEFLAG,WA_DISPLAYQUERY) 
			values (v_PK_WORKFLOWACTIVITY,'Protocol Activation',v_PK_WORKFLOW,v_PK_ACTIVITY,1,0,'SELECT PKG_WORKFLOW_RULES_MDA.F_studyActive_protActivation(:1) FROM DUAL');
			COMMIT;
		END IF;
	END IF;
	---------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,317,1,'04_FlexStudyPageWorkflow2.sql',sysdate,'v9.3.0 #718');

commit;