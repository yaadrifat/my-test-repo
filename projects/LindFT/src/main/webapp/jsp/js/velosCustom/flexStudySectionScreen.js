var flexStudyScreenJS = {
		protAttUpld : null,
		resLoc2 : null,
		studyBlinding : null,
		selOthrObjEntryPk : null,
		secObjective : null,
		explrtObjctv : null,
		biomrkrObj : null,
		commIncCriPk : null,
		commIncCriteriaPk : null,
		juvinileEligNo : null,
		juvinileEligNoPk : null,
		juvinileEligNo1 : null,
		offStdyCriPk : null,
		stdRemCriteria : null,
		OtrStdRemCrit : null,
		clickSelOthrObj : [],
		clickCommIncCri : [],
		clickJuvEligNo : [],
		clickOffStdyCri : [],
		clickStdRemCriteria : [],
		handleClick: {},
		showHideFields: {},
		showHideForLocAndAccrual: {},
		runInterFieldActions: {},
		callAjaxGetTareaDD: {},
		setValueCCSGReport: {},
		runLocAndAccuralOnReady: {},
		checkboxActions: {},
		biosafety1 : {},
		biosafety4 : {},
		biosafety3 : {},
		nihRacRev : {},
		nihRacRevComm : {},
		othrOffStdyCri : {},
		loopThroughInputElementGroup : {},
		showHideTextFieldsOnPageLoad : {},
		attachEventHandlerCheckbox : {},
		uncheckCheckboxGroup : {},
		showHideObjFields : {},
		setTextFieldsEmpty :{}
};
/*****************************************************************************************************
 * This function is used to trigger some actions when a checkbox is checked from a group of checkboxes
 * 
 * @Param : {obj}  the object representing the checkbox group which is checked.
*/

flexStudyScreenJS.checkboxActions = function(obj){
		if(obj.name == "alternateId"+flexStudyScreenJS.selOthrObjEntryPk+"Checks"){
			flexStudyScreenJS.showHideObjFields(obj.name);
		}
		
		if(obj.name == "alternateId"+flexStudyScreenJS.offStdyCriPk+"Checks"){
			flexStudyScreenJS.clickOffStdyCri = flexStudyScreenJS.loopThroughInputElementGroup(obj.name);
			
			if(flexStudyScreenJS.clickOffStdyCri.toString().indexOf("option19") > -1) {
				flexStudyScreenJS.runInterFieldActions(flexStudyScreenJS.othrOffStdyCri, 'show');
			} else {
				flexStudyScreenJS.runInterFieldActions(flexStudyScreenJS.othrOffStdyCri, 'hide');
				//when the Other checkbox is unchecked , the other textbox should be hidden and the text should be cleared out
				//flexStudyScreenJS.setTextFieldsEmpty(flexStudyScreenJS.othrOffStdyCri)
			}
		}
		
		if(obj.name == "alternateId"+flexStudyScreenJS.stdRemCriteria+"Checks"){
			flexStudyScreenJS.clickStdRemCriteria = flexStudyScreenJS.loopThroughInputElementGroup(obj.name);
			
			if(flexStudyScreenJS.clickStdRemCriteria.toString().indexOf("option3") > -1) {
				flexStudyScreenJS.runInterFieldActions(flexStudyScreenJS.OtrStdRemCrit, 'show');
			} else {
				flexStudyScreenJS.runInterFieldActions(flexStudyScreenJS.OtrStdRemCrit, 'hide');
				//when the Other checkbox is unchecked , the other textbox should be hidden and the text should be cleared out
				//flexStudyScreenJS.setTextFieldsEmpty(flexStudyScreenJS.OtrStdRemCrit);
			}
		}
		
		if(obj.name == "alternateId"+flexStudyScreenJS.commIncCriteriaPk+"Checks"){
			flexStudyScreenJS.clickCommIncCri = flexStudyScreenJS.loopThroughInputElementGroup(obj.name);
			
			if(flexStudyScreenJS.clickCommIncCri.toString().indexOf("option1") > -1) {
				flexStudyScreenJS.runInterFieldActions(flexStudyScreenJS.juvinileEligNo, 'show');
			} else {
				flexStudyScreenJS.runInterFieldActions(flexStudyScreenJS.juvinileEligNo, 'hide');
				flexStudyScreenJS.runInterFieldActions(flexStudyScreenJS.juvinileEligNo1, 'hide');
				flexStudyScreenJS.uncheckCheckboxGroup(flexStudyScreenJS.juvinileEligNoPk);
				//flexStudyScreenJS.setTextFieldsEmpty(flexStudyScreenJS.juvinileEligNo1);
			}
		}
		if(obj.name == "alternateId"+flexStudyScreenJS.juvinileEligNoPk+"Checks"){
			flexStudyScreenJS.clickJuvEligNo = flexStudyScreenJS.loopThroughInputElementGroup(obj.name);
			if(flexStudyScreenJS.clickJuvEligNo.toString().indexOf("option5") > -1){
				flexStudyScreenJS.runInterFieldActions(flexStudyScreenJS.juvinileEligNo1, 'show');
			} else {
				flexStudyScreenJS.runInterFieldActions(flexStudyScreenJS.juvinileEligNo1, 'hide');
				//flexStudyScreenJS.setTextFieldsEmpty(flexStudyScreenJS.juvinileEligNo1);
			}
		}
	};
	
	
/*****************************************************************************************************
	  * This function is used to set the value of free text fields to empty, in the event that the 
	  * field is hidden.
	  * 
	  * @Param : {ele}  the field that needs to be set to empty
*/
	flexStudyScreenJS.setTextFieldsEmpty = function(ele){
		if (ele != null && ele != 'undefined'){
			ele.value = "" ;
		}
	}
/*****************************************************************************************************
	  * This function is used to select all the checked checkboxes from a checkbox group
	  * 
	  * @Param : {eleName}  the name of the checkbox group
*/
	
 flexStudyScreenJS.loopThroughInputElementGroup = function(name){
	   var sel = document.getElementsByName(name);
	   var options = [];
		
	   for (var i = 0 ; i < sel.length; i++){
	     if (sel[i].checked)
		  {
		    options.push(sel[i].value);
		  }
		}	
	   return options;
	}
 
 /*****************************************************************************************************
  * This function is used to show hide fields on page load (especially free text fields)
  * that are dependent on the interfield actions for a checkbox.
  * 
  * @Param : {checkBoxArray} the array that represents the checkbox
  * @Param : {eleName}  the name of the checkbox group
  * @Param : {field} the field that needs to be hidden or shown depending on whether
  *        a particular option has been checked in the checkbox group
  */
 
 flexStudyScreenJS.showHideTextFieldsOnPageLoad = function(checkBoxArray, eleName, field, optionOther) {

		for (var i= 0; i< checkBoxArray.length; i++){
			var id = eleName + i;
			if ((document.getElementById(id).checked ) && (document.getElementById(id).value == optionOther)){
				flexStudyScreenJS.runInterFieldActions(field, 'show');
				break;
			}
			else{
				flexStudyScreenJS.runInterFieldActions(field, 'hide');
			}
		}
		
	}
 /*****************************************************************************************************
  * This function is used to attach onChange event handlers to a checkbox group
  * 
  * @Param : {eleName}  the name of the checkbox group
  */
 
 flexStudyScreenJS.attachEventHandlerCheckbox = function(eleName){
 var indx=0;
	$j(document).find("input[id*='alternateId"+eleName+"Checks']").each(function(){
		$j("#alternateId"+eleName+"Checks"+indx).change(function(){
			flexStudyScreenJS.checkboxActions(this);
		});
		
		indx++;
	});
 }
 
 /*****************************************************************************************************
  * This function is used to implement the show hide logic for the free textboxes in the objective section
  * 
  * @Param : {eleName}  the name of the select other objectives checkbox group
  */
 
 flexStudyScreenJS.showHideObjFields = function(eleName){
    flexStudyScreenJS.clickSelOthrObj = flexStudyScreenJS.loopThroughInputElementGroup(eleName);
    if(flexStudyScreenJS.clickSelOthrObj.toString().indexOf("option1") > -1){
		flexStudyScreenJS.runInterFieldActions(flexStudyScreenJS.secObjective, 'show');
	}
	else {
			 flexStudyScreenJS.runInterFieldActions(flexStudyScreenJS.secObjective, 'hide');
			 //flexStudyScreenJS.setTextFieldsEmpty(flexStudyScreenJS.secObjective);
	} 
	if(flexStudyScreenJS.clickSelOthrObj.toString().indexOf("option2") > -1){
		flexStudyScreenJS.runInterFieldActions(flexStudyScreenJS.explrtObjctv, 'show');
	} 
	else {
		flexStudyScreenJS.runInterFieldActions(flexStudyScreenJS.explrtObjctv, 'hide');
		//flexStudyScreenJS.setTextFieldsEmpty(flexStudyScreenJS.explrtObjctv);
	}
	if(flexStudyScreenJS.clickSelOthrObj.toString().indexOf("option3") > -1){
		flexStudyScreenJS.runInterFieldActions(flexStudyScreenJS.biomrkrObj, 'show');
	} 
	else {
		flexStudyScreenJS.runInterFieldActions(flexStudyScreenJS.biomrkrObj, 'hide');
		//flexStudyScreenJS.setTextFieldsEmpty(flexStudyScreenJS.biomrkrObj);
	}
 }

 /*****************************************************************************************************
  * This function is used to uncheck all the checkboxes in a checkbox group
  * 
  * @Param : {eleName}  the name of the checkbox group
  */
 
 flexStudyScreenJS.uncheckCheckboxGroup = function(eleName){
	  var name = "alternateId"+eleName+"Checks";
      $j('input[name="' + name + '"]').attr("checked" , false);   
 	}
 
flexStudyScreenJS.callAjaxGetTareaDD = function(formobj){
	   var selval = formobj.studyDivision.value;

	   new VELOS.ajaxObject("getCodeDropDown.jsp", {
		urlData:"selectedVal="+selval + "&ddName=studyTArea&codeType=tarea&ddType=child" ,
		   reqType:"POST",
		   outputElement: "span_tarea" }
		).startRequest();
	};
	
flexStudyScreenJS.setValueCCSGReport = function(formobj){
		value=formobj.studyDivision.value;
		if (value=="7801"){
			formobj.ccsgdt.checked=true;
		}else{
			formobj.ccsgdt.checked=false;
			}	
	};

flexStudyScreenJS.runInterFieldActions = function(fld, action, type){
	if (fld == null || fld == 'undefined')
		return false;
	
	if(action == 'hide'){
		if(type == 'splfld'){
			fld.style.display = "none";
		}
		else{
			fld.parentNode.parentNode.style.display = "none";
		}
	}
	else if(action == 'show'){
		if(type == 'splfld'){
			fld.style.display = "";
		}
		else{
			fld.parentNode.parentNode.style.display = "";
		}
	}
};

flexStudyScreenJS.showHideFields = function(fldType, fldVal){
	
	var coopGrpNum = moreStudyDetFunctions.getMoreStudyDetField("coopGrpNum");
	var consrtmNum = moreStudyDetFunctions.getMoreStudyDetField("consrtmNum");
	var nciEtctnNum = moreStudyDetFunctions.getMoreStudyDetField("nciEtctnNum");
	var studySponsorIdInfo = document.getElementById("studySponsorIdInfo");
	var rndmnTrtGrp = moreStudyDetFunctions.getMoreStudyDetField("rndmnTrtGrp");
	var blndTrtGrp = moreStudyDetFunctions.getMoreStudyDetField("blndTrtGrp");
	var unblnTrtGrp = moreStudyDetFunctions.getMoreStudyDetField("unblnTrtGrp");
	var accrual6 = moreStudyDetFunctions.getMoreStudyDetField("accrual6");
	var accrual1 = moreStudyDetFunctions.getMoreStudyDetField("accrual1");
	var accrual5 = moreStudyDetFunctions.getMoreStudyDetField("accrual5");
	var stdSample = document.getElementById("nssLink");
	var accrual7 = moreStudyDetFunctions.getMoreStudyDetField("accrual7");
	var accrual12 = moreStudyDetFunctions.getMoreStudyDetField("accrual12");
	var womenParticiYes = moreStudyDetFunctions.getMoreStudyDetField("womenParticiYes");	
	var minorParticiYes = moreStudyDetFunctions.getMoreStudyDetField("minorParticiYes");	
	var pregEligbltyYes = moreStudyDetFunctions.getMoreStudyDetField("pregEligbltyYes");	
	var resPop3 = moreStudyDetFunctions.getMoreStudyDetField("resPop3");
	var resPop5 = moreStudyDetFunctions.getMoreStudyDetField("resPop5");
	var confdlty2 = moreStudyDetFunctions.getMoreStudyDetField("confdlty2");
	var icd1 = moreStudyDetFunctions.getMoreStudyDetField("icd1");
	var icd2 = moreStudyDetFunctions.getMoreStudyDetField("icd2");
	var resDesc2 = moreStudyDetFunctions.getMoreStudyDetField("resDesc2");
	var confdlty3 = moreStudyDetFunctions.getMoreStudyDetField("confdlty3");
	var confdlty4 = moreStudyDetFunctions.getMoreStudyDetField("confdlty4");
	var protoMontr3 = moreStudyDetFunctions.getMoreStudyDetField("protoMontr3");
	var protoMontr4 = moreStudyDetFunctions.getMoreStudyDetField("protoMontr4");
	var stdyIncludes = moreStudyDetFunctions.getMoreStudyDetField("stdyIncludes");
	var drgSupMfgr = moreStudyDetFunctions.getMoreStudyDetField("drgSupMfgr");
	var receiptDrgSup = moreStudyDetFunctions.getMoreStudyDetField("receiptDrgSup");
	var prepAdmDrgPkg = moreStudyDetFunctions.getMoreStudyDetField("prepAdmDrgPkg");
	var drgStorage = moreStudyDetFunctions.getMoreStudyDetField("drgStorage");
	var stdyDrgDispn = moreStudyDetFunctions.getMoreStudyDetField("stdyDrgDispn");
	var retDestStdDrg = moreStudyDetFunctions.getMoreStudyDetField("retDestStdDrg");
	var devSupMfgr = moreStudyDetFunctions.getMoreStudyDetField("devSupMfgr");
	var receiptDevice = moreStudyDetFunctions.getMoreStudyDetField("receiptDevice");
	var devStorage = moreStudyDetFunctions.getMoreStudyDetField("devStorage");
	var returnDevice = moreStudyDetFunctions.getMoreStudyDetField("returnDevice");
	var cmpnsation1 = moreStudyDetFunctions.getMoreStudyDetField("cmpnsation1");
	var descrbProRatd = moreStudyDetFunctions.getMoreStudyDetField("descrbProRatd");
	
	var authrzdUsr = moreStudyDetFunctions.getMoreStudyDetField("authrzdUsr");
	var authrztnNum = moreStudyDetFunctions.getMoreStudyDetField("authrztnNum");
	var incldAdminstr = moreStudyDetFunctions.getMoreStudyDetField("incldAdminstr");
	var radioCompAvail = moreStudyDetFunctions.getMoreStudyDetField("radioCompAvail");
	var faciltyInvstg = moreStudyDetFunctions.getMoreStudyDetField("faciltyInvstg");
	var biosafety4 = moreStudyDetFunctions.getMoreStudyDetField("biosafety4");
	var nihRacRev =  moreStudyDetFunctions.getMoreStudyDetField("nihRacRev");
	var nihRacRevComm =  moreStudyDetFunctions.getMoreStudyDetField("nihRacRevComm");
	var biosafety2Comm =  moreStudyDetFunctions.getMoreStudyDetField("biosafety2Comm");
	var biosafety5 = moreStudyDetFunctions.getMoreStudyDetField("biosafety5");
	var meet_exem_crit = moreStudyDetFunctions.getMoreStudyDetField("meet_exem_crit");
	var exem_ratnl = moreStudyDetFunctions.getMoreStudyDetField("exem_ratnl");
	var ide7 = moreStudyDetFunctions.getMoreStudyDetField("ide7");
	var ide8 = moreStudyDetFunctions.getMoreStudyDetField("ide8");
	var ide9 = moreStudyDetFunctions.getMoreStudyDetField("ide9");
		
	switch(fldVal){
	case 'spnsrdProt':
		if(fldType == 'MDACCInvIni') {
			$j('#sectionHead4').show();
			$j('#sectionHead11').show();
			$j('#sectionHead12').show();
			$j('#sectionHead13').show();
			$j('#sectionHead14').show();
			$j('#sectionHead15').show();
			$j('#sectionHead16').show();
			$j('#sectionHead18').show();
			$j('#sectionHead19').show();
			flexStudyScreenJS.runInterFieldActions(coopGrpNum, 'hide');
			flexStudyScreenJS.runInterFieldActions(consrtmNum, 'hide');
			flexStudyScreenJS.runInterFieldActions(nciEtctnNum, 'hide');
			flexStudyScreenJS.runInterFieldActions(studySponsorIdInfo, 'hide', 'no');
		} else {
			flexStudyScreenJS.runInterFieldActions(coopGrpNum, 'hide');
			flexStudyScreenJS.runInterFieldActions(consrtmNum, 'hide');
			flexStudyScreenJS.runInterFieldActions(nciEtctnNum, 'hide');
			flexStudyScreenJS.runInterFieldActions(studySponsorIdInfo, 'hide', 'no');
			$j('#sectionHead4').hide();
			$j('#sectionHead11').hide();
			$j('#sectionHead12').hide();
			$j('#sectionHead13').hide();
			$j('#sectionHead14').hide();
			$j('#sectionHead15').hide();
			$j('#sectionHead16').hide();
			$j('#sectionHead18').hide();
			$j('#sectionHead19').hide();
			if(fldType == 'coopGrp') {
				flexStudyScreenJS.runInterFieldActions(coopGrpNum, 'show');
			}
			else if(fldType == 'consrtm') {
				flexStudyScreenJS.runInterFieldActions(consrtmNum, 'show');
			}
			else if(fldType == 'nciEtctn') {
				flexStudyScreenJS.runInterFieldActions(nciEtctnNum, 'show');
			}
			else if(fldType == 'indstrStdy') {
				flexStudyScreenJS.runInterFieldActions(studySponsorIdInfo, 'show', 'no');
			}
		}
		break;
	case 'protAttUpld':
		var pk = moreStudyDetFunctions.getMoreStudyDetCodePK("stdyIncludes");
		var stdyIncludes = document.getElementsByName("alternateId"+pk+"Checks");
		if(fldType == 'Yes') {
			$j('#sectionHead4').hide();
			$j('#sectionHead11').hide();
			$j('#sectionHead12').hide();
			$j('#sectionHead15').hide();
			$j('#sectionHead16').hide();
			$j('#sectionHead17').hide();
			$j('#sectionHead19').hide();
			$j('#sectionHead20').hide();
			for (var indx=0; indx < stdyIncludes.length; indx++){
				flexStudyScreenJS.runInterFieldActions(stdyIncludes[indx], 'hide');
			}
		} else {
			$j('#sectionHead4').show();
			$j('#sectionHead11').show();
			$j('#sectionHead12').show();
			$j('#sectionHead15').show();
			$j('#sectionHead16').show();
			$j('#sectionHead17').show();
			$j('#sectionHead19').show();
			$j('#sectionHead20').show();
			for (var indx=0; indx < stdyIncludes.length; indx++){
				flexStudyScreenJS.runInterFieldActions(stdyIncludes[indx], 'show');
			}
		}
		break;
	case 'studyRandom':
		if(fldType == 'Select an option' || fldType == 'None') {
			flexStudyScreenJS.runInterFieldActions(rndmnTrtGrp, 'hide');
			flexStudyScreenJS.runInterFieldActions(flexStudyScreenJS.studyBlinding, 'hide', 'no');
			flexStudyScreenJS.runInterFieldActions(blndTrtGrp, 'hide');
			//commenting this since this field is not present in the configuration
			//flexStudyScreenJS.runInterFieldActions(unblnTrtGrp, 'hide');
		} else {
			flexStudyScreenJS.runInterFieldActions(rndmnTrtGrp, 'show');
			flexStudyScreenJS.runInterFieldActions(flexStudyScreenJS.studyBlinding, 'show', 'no');
		}
		break;
	case 'biosafety1':
		if(fldType == 'Yes') {
			flexStudyScreenJS.runInterFieldActions(biosafety4, 'show');
			flexStudyScreenJS.runInterFieldActions(nihRacRev, 'show');
		} else {
			flexStudyScreenJS.runInterFieldActions(biosafety4, 'hide');
			flexStudyScreenJS.runInterFieldActions(nihRacRev, 'hide');
		}
		break;
	case 'biosafety2':
		if(fldType == 'Yes') {
			flexStudyScreenJS.runInterFieldActions(biosafety2Comm, 'show');
		} else {
			flexStudyScreenJS.runInterFieldActions(biosafety2Comm, 'hide');
		}
		break;	
	case 'biosafety3':
		if(fldType == 'Yes') {
			flexStudyScreenJS.runInterFieldActions(biosafety5, 'show');
		}
		else {
			flexStudyScreenJS.runInterFieldActions(biosafety5, 'hide');
		}
		break;
	case 'nihRacRev':
		if(fldType == 'Yes') {
			flexStudyScreenJS.runInterFieldActions(nihRacRevComm, 'show');
		} else {
			flexStudyScreenJS.runInterFieldActions(nihRacRevComm, 'hide');
		}
		break;
	case 'studyBlinding':
		if(fldType == 'Select an option') {
			flexStudyScreenJS.runInterFieldActions(blndTrtGrp, 'hide');
		} else {
			flexStudyScreenJS.runInterFieldActions(blndTrtGrp, 'show');
		}
		break;
	case 'studyScope':
		if(fldType == 'Select an option') {
			flexStudyScreenJS.runInterFieldActions(accrual1, 'hide');
			flexStudyScreenJS.runInterFieldActions(stdSample, 'hide', 'no');
			flexStudyScreenJS.runInterFieldActions(accrual12, 'hide');
		} else if(fldType == 'Only at Other Sites') {
			flexStudyScreenJS.runInterFieldActions(accrual1, 'hide');
			flexStudyScreenJS.runInterFieldActions(stdSample, 'hide', 'no');
			flexStudyScreenJS.runInterFieldActions(accrual12, 'show');
		} else if(fldType == 'Only at MDACC') {
			flexStudyScreenJS.runInterFieldActions(accrual1, 'show');
			flexStudyScreenJS.runInterFieldActions(stdSample, 'show', 'no');
			flexStudyScreenJS.runInterFieldActions(accrual12, 'hide');
		} else {
			flexStudyScreenJS.runInterFieldActions(accrual1, 'show');
			flexStudyScreenJS.runInterFieldActions(stdSample, 'show', 'no');
			flexStudyScreenJS.runInterFieldActions(accrual12, 'show');
		}
		break;
	case 'resLoc1':
		if(fldType == 'Yes') {
			flexStudyScreenJS.runInterFieldActions(flexStudyScreenJS.resLoc2, 'show');
		} else {
			flexStudyScreenJS.runInterFieldActions(flexStudyScreenJS.resLoc2, 'hide');
		}
		break;
	case 'resPop2':
		if(fldType == 'Yes') {
			flexStudyScreenJS.runInterFieldActions(resPop3, 'show');
		} else {
			flexStudyScreenJS.runInterFieldActions(resPop3, 'hide');
		}
		break;
	case 'resPop4':
		if(fldType == 'Yes') {
			flexStudyScreenJS.runInterFieldActions(resPop5, 'show');
		} else {
			flexStudyScreenJS.runInterFieldActions(resPop5, 'hide');
		}
		break;
	case 'confdlty1':
		if(fldType == 'Yes') {
			flexStudyScreenJS.runInterFieldActions(confdlty2, 'show');
		} else {
			flexStudyScreenJS.runInterFieldActions(confdlty2, 'hide');
		}
		break;
	case 'icd6':
		if(fldType == 'Yes') {
			flexStudyScreenJS.runInterFieldActions(icd2, 'show');
			flexStudyScreenJS.runInterFieldActions(icd1, 'hide');
		} else {
			flexStudyScreenJS.runInterFieldActions(icd2, 'hide');
			flexStudyScreenJS.runInterFieldActions(icd1, 'show');
		}
		break;
	case 'resDesc1':
		if(fldType == 'Yes') {
			flexStudyScreenJS.runInterFieldActions(resDesc2, 'show');
			flexStudyScreenJS.runInterFieldActions(confdlty3, 'show');
			flexStudyScreenJS.runInterFieldActions(confdlty4, 'show');
		} else {
			flexStudyScreenJS.runInterFieldActions(resDesc2, 'hide');
			flexStudyScreenJS.runInterFieldActions(confdlty3, 'hide');
			flexStudyScreenJS.runInterFieldActions(confdlty4, 'hide');
		}
		break;
	case 'protoMontr1':
		if(fldType == 'Yes') {
			flexStudyScreenJS.runInterFieldActions(protoMontr3, 'show');
			flexStudyScreenJS.runInterFieldActions(protoMontr4, 'hide');
		} else if (fldType == 'No'){
			flexStudyScreenJS.runInterFieldActions(protoMontr3, 'hide');
			flexStudyScreenJS.runInterFieldActions(protoMontr4, 'show');
		} else {
			flexStudyScreenJS.runInterFieldActions(protoMontr3, 'hide');
			flexStudyScreenJS.runInterFieldActions(protoMontr4, 'hide');
		}
		break;
	case 'spnsrdProt':
		if(fldType == 'Yes') {
			flexStudyScreenJS.runInterFieldActions(flexStudyScreenJS.protAttUpld, 'show');
		} else {
			flexStudyScreenJS.runInterFieldActions(flexStudyScreenJS.protAttUpld, 'hide');
		}
		break;
	case 'drgMngntFile':
		if(fldType == 'No') {
			flexStudyScreenJS.runInterFieldActions(drgSupMfgr, 'show');
			flexStudyScreenJS.runInterFieldActions(receiptDrgSup, 'show');
			flexStudyScreenJS.runInterFieldActions(prepAdmDrgPkg, 'show');
			flexStudyScreenJS.runInterFieldActions(drgStorage, 'show');
			flexStudyScreenJS.runInterFieldActions(stdyDrgDispn, 'show');
			flexStudyScreenJS.runInterFieldActions(retDestStdDrg, 'show');
		} else {
			flexStudyScreenJS.runInterFieldActions(drgSupMfgr, 'hide');
			flexStudyScreenJS.runInterFieldActions(receiptDrgSup, 'hide');
			flexStudyScreenJS.runInterFieldActions(prepAdmDrgPkg, 'hide');
			flexStudyScreenJS.runInterFieldActions(drgStorage, 'hide');
			flexStudyScreenJS.runInterFieldActions(stdyDrgDispn, 'hide');
			flexStudyScreenJS.runInterFieldActions(retDestStdDrg, 'hide');
		}
		break;
	case 'devMngntFile':
		if(fldType == 'No') {
			flexStudyScreenJS.runInterFieldActions(devSupMfgr, 'show');
			flexStudyScreenJS.runInterFieldActions(receiptDevice, 'show');
			flexStudyScreenJS.runInterFieldActions(devStorage, 'show');
			flexStudyScreenJS.runInterFieldActions(returnDevice, 'show');
		} else {
			flexStudyScreenJS.runInterFieldActions(devSupMfgr, 'hide');
			flexStudyScreenJS.runInterFieldActions(receiptDevice, 'hide');
			flexStudyScreenJS.runInterFieldActions(devStorage, 'hide');
			flexStudyScreenJS.runInterFieldActions(returnDevice, 'hide');
		}
		break;
	case 'subCompReimb':
		if(fldType == 'Yes') {
			flexStudyScreenJS.runInterFieldActions(cmpnsation1, 'show');
		} else {
			flexStudyScreenJS.runInterFieldActions(cmpnsation1, 'hide');
		}
		break;
	case 'payProratd':
		if(fldType == 'Yes') {
			flexStudyScreenJS.runInterFieldActions(descrbProRatd, 'show');
		} else {
			flexStudyScreenJS.runInterFieldActions(descrbProRatd, 'hide');
		}
		break;
	case 'radMat1':
		if(fldType == 'Yes') {
			flexStudyScreenJS.runInterFieldActions(authrzdUsr, 'show');
			flexStudyScreenJS.runInterFieldActions(authrztnNum, 'show');
			flexStudyScreenJS.runInterFieldActions(incldAdminstr, 'show');
			flexStudyScreenJS.runInterFieldActions(radioCompAvail, 'show');
			flexStudyScreenJS.runInterFieldActions(faciltyInvstg, 'show');
		} else {
			flexStudyScreenJS.runInterFieldActions(authrzdUsr, 'hide');
			flexStudyScreenJS.runInterFieldActions(authrztnNum, 'hide');
			flexStudyScreenJS.runInterFieldActions(incldAdminstr, 'hide');
			flexStudyScreenJS.runInterFieldActions(radioCompAvail, 'hide');
			flexStudyScreenJS.runInterFieldActions(faciltyInvstg, 'hide');
		}
		break;
		
	case 'prot_req_ind' :
		if (fldType == 'No'){
			flexStudyScreenJS.runInterFieldActions(meet_exem_crit , 'show');
			flexStudyScreenJS.runInterFieldActions(exem_ratnl , 'show');
		}
		else {
			flexStudyScreenJS.runInterFieldActions(meet_exem_crit , 'hide');
			flexStudyScreenJS.runInterFieldActions(exem_ratnl , 'hide');
		}
		break;
		
	case 'investiDevice' :
		if (fldType.indexOf('Yes') > -1) {
			flexStudyScreenJS.runInterFieldActions(ide7 , 'hide');
			flexStudyScreenJS.runInterFieldActions(ide8 , 'show');
			flexStudyScreenJS.runInterFieldActions(ide9 , 'show');
		}
		else if (fldType == 'No') {
			flexStudyScreenJS.runInterFieldActions(ide7 , 'show');
			flexStudyScreenJS.runInterFieldActions(ide8 , 'hide');
			flexStudyScreenJS.runInterFieldActions(ide9 , 'hide');
		}
		else {
			flexStudyScreenJS.runInterFieldActions(ide7 , 'hide');
			flexStudyScreenJS.runInterFieldActions(ide8 , 'hide');
			flexStudyScreenJS.runInterFieldActions(ide9 , 'hide');
		}
		break;
	case 'dsmbInt':
		var dsmbCompostn  = moreStudyDetFunctions.getMoreStudyDetField("dsmbCompostn");
		if(fldType != 'Not Applicable') {
			flexStudyScreenJS.runInterFieldActions(dsmbCompostn, 'show');
		} 
		else {
			 flexStudyScreenJS.runInterFieldActions(dsmbCompostn, 'hide');
		}
		break;
		
		default: break;
	}
};


$j(document).ready(function(){	
	var spnsrdProt = moreStudyDetFunctions.getMoreStudyDetField("spnsrdProt");
	flexStudyScreenJS.spnsrdProt = moreStudyDetFunctions.getMoreStudyDetField("spnsrdProt");
	
	flexStudyScreenJS.biosafety1 = moreStudyDetFunctions.getMoreStudyDetField("biosafety1");
	flexStudyScreenJS.biosafety3 = moreStudyDetFunctions.getMoreStudyDetField("biosafety3");
	
	var tAreaFld = document.getElementsByName("studyTArea")[0];
	if (tAreaFld){
		tAreaFld.onchange = function(e){
			var disSitenameFld = document.getElementsByName("disSitename")[0];
			disSitenameFld.value = '';
			var disSiteIdFld = document.getElementsByName("disSiteid")[0];
			disSiteIdFld.value = '';
		};
	}
	
	var studyRandom = document.getElementsByName("studyRandom")[0];
	flexStudyScreenJS.studyBlinding = document.getElementsByName("studyBlinding")[0];
	var studyScope = document.getElementById("studyScope");
	
	flexStudyScreenJS.protAttUpld = moreStudyDetFunctions.getMoreStudyDetField("protAttUpld");
	flexStudyScreenJS.resLoc2 = moreStudyDetFunctions.getMoreStudyDetField("resLoc2");
	
	flexStudyScreenJS.selOthrObjEntryPk = moreStudyDetFunctions.getMoreStudyDetCodePK("selOthrObjEntry");
	flexStudyScreenJS.secObjective = moreStudyDetFunctions.getMoreStudyDetField("secObjective");
	flexStudyScreenJS.explrtObjctv = moreStudyDetFunctions.getMoreStudyDetField("explrtObjctv");
	flexStudyScreenJS.biomrkrObj = moreStudyDetFunctions.getMoreStudyDetField("biomrkrObj");
	
	if (flexStudyScreenJS.selOthrObjEntryPk != null && flexStudyScreenJS.selOthrObjEntryPk != 'undefined') {
		
		
			var name = "alternateId"+flexStudyScreenJS.selOthrObjEntryPk+"Checks" ;
			flexStudyScreenJS.showHideObjFields(name);
	}
	flexStudyScreenJS.attachEventHandlerCheckbox(flexStudyScreenJS.selOthrObjEntryPk);
	
    
	
	//subject selection section
	flexStudyScreenJS.commIncCriteriaPk = moreStudyDetFunctions.getMoreStudyDetCodePK("commIncCritra");
	
	flexStudyScreenJS.juvinileEligNo = moreStudyDetFunctions.getMoreStudyDetField("juvinileEligNo");
	var name = "alternateId"+flexStudyScreenJS.commIncCriteriaPk+"Checks" ;
	var checkBoxArray = document.getElementsByName(name) ;
	flexStudyScreenJS.runInterFieldActions(flexStudyScreenJS.juvinileEligNo, 'show');
	flexStudyScreenJS.showHideTextFieldsOnPageLoad(checkBoxArray, name, flexStudyScreenJS.juvinileEligNo, "option1");
	flexStudyScreenJS.attachEventHandlerCheckbox(flexStudyScreenJS.commIncCriteriaPk);
	
	flexStudyScreenJS.juvinileEligNoPk = moreStudyDetFunctions.getMoreStudyDetCodePK("juvinileEligNo");
	flexStudyScreenJS.juvinileEligNo1 = moreStudyDetFunctions.getMoreStudyDetField("juvinileEligNo1");
	var name = "alternateId"+flexStudyScreenJS.juvinileEligNoPk+"Checks" ;
	var checkBoxArray = document.getElementsByName(name) ;
	flexStudyScreenJS.showHideTextFieldsOnPageLoad(checkBoxArray, name, flexStudyScreenJS.juvinileEligNo1, "option5");
	flexStudyScreenJS.attachEventHandlerCheckbox(flexStudyScreenJS.juvinileEligNoPk);

	flexStudyScreenJS.offStdyCriPk = moreStudyDetFunctions.getMoreStudyDetCodePK("offStdyCri");
	flexStudyScreenJS.othrOffStdyCri = moreStudyDetFunctions.getMoreStudyDetField("othrOffStdyCri");
	var name = "alternateId"+flexStudyScreenJS.offStdyCriPk+"Checks" ;
	var checkBoxArray = document.getElementsByName(name) ;
	
	flexStudyScreenJS.showHideTextFieldsOnPageLoad(checkBoxArray, name, flexStudyScreenJS.othrOffStdyCri, "option19");
	flexStudyScreenJS.attachEventHandlerCheckbox(flexStudyScreenJS.offStdyCriPk);
	
	flexStudyScreenJS.stdRemCriteria = moreStudyDetFunctions.getMoreStudyDetCodePK("stdRemCriteria");
	flexStudyScreenJS.OtrStdRemCrit = moreStudyDetFunctions.getMoreStudyDetField("OtrStdRemCrit");
	var name = "alternateId"+flexStudyScreenJS.stdRemCriteria+"Checks" ;
	var checkBoxArray = document.getElementsByName(name) ;
	
	flexStudyScreenJS.showHideTextFieldsOnPageLoad(checkBoxArray, name, flexStudyScreenJS.OtrStdRemCrit, "option3");
	flexStudyScreenJS.attachEventHandlerCheckbox(flexStudyScreenJS.stdRemCriteria);
	
	var protoMontr1 = moreStudyDetFunctions.getMoreStudyDetField("protoMontr1");
	var resLoc1 = moreStudyDetFunctions.getMoreStudyDetField("resLoc1");
	var resLoc6 = moreStudyDetFunctions.getMoreStudyDetField("resLoc6");
	var resPop2 = moreStudyDetFunctions.getMoreStudyDetField("resPop2");
	var resPop4 = moreStudyDetFunctions.getMoreStudyDetField("resPop4");
	var confdlty1 = moreStudyDetFunctions.getMoreStudyDetField("confdlty1");
	var icd6 = moreStudyDetFunctions.getMoreStudyDetField("icd6");
	var resDesc1 = moreStudyDetFunctions.getMoreStudyDetField("resDesc1");
	var drgMngntFile = moreStudyDetFunctions.getMoreStudyDetField("drgMngntFile");
	var devMngntFile = moreStudyDetFunctions.getMoreStudyDetField("devMngntFile");
	var subCompReimb = moreStudyDetFunctions.getMoreStudyDetField("subCompReimb");
	var payProratd = moreStudyDetFunctions.getMoreStudyDetField("payProratd");
	
	var radMat1 = moreStudyDetFunctions.getMoreStudyDetField("radMat1");
	var biosafety4 = moreStudyDetFunctions.getMoreStudyDetField("biosafety4");
	var nihRacRev =  moreStudyDetFunctions.getMoreStudyDetField("nihRacRev");
	var nihRacRevComm =  moreStudyDetFunctions.getMoreStudyDetField("nihRacRevComm");
	var prot_req_ind = moreStudyDetFunctions.getMoreStudyDetField("prot_req_ind");
	var biosafety2 = moreStudyDetFunctions.getMoreStudyDetField("biosafety2");
	var investiDevice = moreStudyDetFunctions.getMoreStudyDetField("investiDevice");
	var dsmbInt  = moreStudyDetFunctions.getMoreStudyDetField("dsmbInt"); 
	
	
   if (dsmbInt != null && dsmbInt != "undefined" ){
		
		flexStudyScreenJS.showHideFields(dsmbInt.options[dsmbInt.selectedIndex].text, 'dsmbInt'); 
		if (!dsmbInt.addEventListener) {
			dsmbInt.on('change', function(e)
			{
				flexStudyScreenJS.showHideFields(dsmbInt.options[dsmbInt.selectedIndex].text, 'dsmbInt');
				return false;
			});
		}
 		else {
			  dsmbInt.addEventListener('change', function(e)
				{
				  flexStudyScreenJS.showHideFields(dsmbInt.options[dsmbInt.selectedIndex].text, 'dsmbInt');
				  return false;
			    });
		    }
	}
	if (flexStudyScreenJS.biosafety1 != null && flexStudyScreenJS.biosafety1 != "undefined" ){
		
		var biosafety1 = flexStudyScreenJS.biosafety1 ; 
		flexStudyScreenJS.runInterFieldActions(biosafety4, 'hide');
		flexStudyScreenJS.runInterFieldActions(nihRacRev, 'hide');
		flexStudyScreenJS.runInterFieldActions(nihRacRevComm, 'hide');
		
		
		flexStudyScreenJS.showHideFields(biosafety1.options[biosafety1.selectedIndex].text, 'biosafety1'); 
		if (!biosafety1.addEventListener) {
			biosafety1.on('change', function(e)
			{
				flexStudyScreenJS.showHideFields(biosafety1.options[biosafety1.selectedIndex].text, 'biosafety1');
				return false;
			});
		}
		else {
			biosafety1.addEventListener('change', function(e)
					{
				flexStudyScreenJS.showHideFields(biosafety1.options[biosafety1.selectedIndex].text, 'biosafety1');
				return false;
			});
		}
	}
	
if (flexStudyScreenJS.biosafety3 != null && flexStudyScreenJS.biosafety3 != "undefined" ){
		
		var biosafety3 = flexStudyScreenJS.biosafety3 ; 
		
		flexStudyScreenJS.showHideFields(biosafety3.options[biosafety3.selectedIndex].text, 'biosafety3'); 
		if (!biosafety3.addEventListener) {
			biosafety3.on('change', function(e)
			{
				flexStudyScreenJS.showHideFields(biosafety3.options[biosafety3.selectedIndex].text, 'biosafety3');
				return false;
			});
		}
		else {
			biosafety3.addEventListener('change', function(e)
					{
				flexStudyScreenJS.showHideFields(biosafety3.options[biosafety3.selectedIndex].text, 'biosafety3');
				return false;
			});
		}
	}
	
	if (nihRacRev != null && nihRacRev != "undefined" ){
		flexStudyScreenJS.showHideFields(nihRacRev.options[nihRacRev.selectedIndex].text, 'nihRacRev'); 
			
			if (!nihRacRev.addEventListener) {
				nihRacRev.on('change', function(e)
						{
					flexStudyScreenJS.showHideFields(nihRacRev.options[nihRacRev.selectedIndex].text, 'nihRacRev');
					return false;
				});
			}
			else {
				nihRacRev.addEventListener('change', function(e)
						{
					flexStudyScreenJS.showHideFields(nihRacRev.options[nihRacRev.selectedIndex].text, 'nihRacRev');
					return false;
				});
			}
		}
	
	
	if (biosafety2 != null && biosafety2 != "undefined" ){
		flexStudyScreenJS.showHideFields(biosafety2.options[biosafety2.selectedIndex].text, 'biosafety2'); 
			
			if (!biosafety2.addEventListener) {
				biosafety2.on('change', function(e)
						{
					flexStudyScreenJS.showHideFields(biosafety2.options[biosafety2.selectedIndex].text, 'biosafety2');
					return false;
				});
			}
			else {
				biosafety2.addEventListener('change', function(e)
						{
					flexStudyScreenJS.showHideFields(biosafety2.options[biosafety2.selectedIndex].text, 'biosafety2');
					return false;
				});
			}
		}
	
	if (prot_req_ind != null && prot_req_ind != "undefined" ){
		flexStudyScreenJS.showHideFields(prot_req_ind.options[prot_req_ind.selectedIndex].text, 'prot_req_ind'); 
			
			if (!prot_req_ind.addEventListener) {
				prot_req_ind.on('change', function(e)
						{
					flexStudyScreenJS.showHideFields(prot_req_ind.options[prot_req_ind.selectedIndex].text, 'prot_req_ind');
					return false;
				});
			}
			else {
				prot_req_ind.addEventListener('change', function(e)
				{
					flexStudyScreenJS.showHideFields(prot_req_ind.options[prot_req_ind.selectedIndex].text, 'prot_req_ind');
					return false;
				});
			}
		}

	if (investiDevice != null && investiDevice != "undefined" ){
		flexStudyScreenJS.showHideFields(investiDevice.options[investiDevice.selectedIndex].text, 'investiDevice'); 
			
			if (!investiDevice.addEventListener) {
				investiDevice.on('change', function(e)
						{
					flexStudyScreenJS.showHideFields(investiDevice.options[investiDevice.selectedIndex].text, 'investiDevice');
					return false;
				});
			}
			else {
				investiDevice.addEventListener('change', function(e)
						{
					flexStudyScreenJS.showHideFields(investiDevice.options[investiDevice.selectedIndex].text, 'investiDevice');
					return false;
				});
			}
		}
	
	if(flexStudyScreenJS.spnsrdProt) {
		flexStudyScreenJS.showHideFields((flexStudyScreenJS.spnsrdProt).value, 'spnsrdProt');

		if (!(spnsrdProt).addEventListener) {
			(flexStudyScreenJS.spnsrdProt).on('change', function(e){
				flexStudyScreenJS.showHideFields(this.value, 'spnsrdProt');
				return false;
			});
		} else {
			document.querySelector('#'+(spnsrdProt).id).addEventListener('change', function(e){
				flexStudyScreenJS.showHideFields(this.value, 'spnsrdProt');
				return false;
			});
		}
	}
	
	if(studyRandom) {
		flexStudyScreenJS.showHideFields(studyRandom.options[studyRandom.selectedIndex].text, 'studyRandom');

		if (!studyRandom.addEventListener) {
			(studyRandom.value).on('change', function(e){
				studyRandom = document.getElementsByName("studyRandom")[0];
				flexStudyScreenJS.showHideFields(studyRandom.options[studyRandom.selectedIndex].text, 'studyRandom');
				return false;
			});
		} else {
			document.querySelector('#'+studyRandom.id).addEventListener('change', function(e){
				studyRandom = document.getElementsByName("studyRandom")[0];
				flexStudyScreenJS.showHideFields(studyRandom.options[studyRandom.selectedIndex].text, 'studyRandom');
				return false;
			});
		}
	}
	
	if(flexStudyScreenJS.studyBlinding) {
		flexStudyScreenJS.showHideFields(flexStudyScreenJS.studyBlinding.options[flexStudyScreenJS.studyBlinding.selectedIndex].text, 'studyBlinding');

		if (!flexStudyScreenJS.studyBlinding.addEventListener) {
			(flexStudyScreenJS.studyBlinding.value).on('change', function(e){
				flexStudyScreenJS.studyBlinding = document.getElementsByName("studyBlinding")[0];
				flexStudyScreenJS.showHideFields(flexStudyScreenJS.studyBlinding.options[flexStudyScreenJS.studyBlinding.selectedIndex].text, 'studyBlinding');
				return false;
			});
		} else {
			document.querySelector('#'+flexStudyScreenJS.studyBlinding.id).addEventListener('change', function(e){
				flexStudyScreenJS.studyBlinding = document.getElementsByName("studyBlinding")[0];
				flexStudyScreenJS.showHideFields(flexStudyScreenJS.studyBlinding.options[flexStudyScreenJS.studyBlinding.selectedIndex].text, 'studyBlinding');
				return false;
			});
		}
	}
	
	if(protoMontr1 != null && protoMontr1 != "undefined") {
		flexStudyScreenJS.showHideFields(protoMontr1.value, 'protoMontr1');

		if (!protoMontr1.addEventListener) {
			protoMontr1.on('change', function(e){
				flexStudyScreenJS.showHideFields(protoMontr1.value, 'protoMontr1');
				return false;
			});
		} else {
			protoMontr1.addEventListener('change', function(e){
				flexStudyScreenJS.showHideFields(protoMontr1.value, 'protoMontr1');
				return false;
			});
		}
	}
	
	if(studyScope) {
		flexStudyScreenJS.showHideFields(studyScope.options[studyScope.selectedIndex].text, 'studyScope');

		if (!studyScope.addEventListener) {
			studyScope.on('change', function(e){
				flexStudyScreenJS.showHideFields(studyScope.options[studyScope.selectedIndex].text, 'studyScope');
				return false;
			});
		} else {
			    studyScope.addEventListener('change', function(e){
				flexStudyScreenJS.showHideFields(studyScope.options[studyScope.selectedIndex].text, 'studyScope');
				return false;
			});
		}
	}
	
	if(resLoc1) {
		flexStudyScreenJS.showHideFields(resLoc1.value, 'resLoc1');

		if (!resLoc1.addEventListener) {
			resLoc1.on('change', function(e){
				resLoc1 = moreStudyDetFunctions.getMoreStudyDetField("resLoc1");
				flexStudyScreenJS.showHideFields(resLoc1.value, 'resLoc1');
				return false;
			});
		} else {
			document.querySelector('#'+resLoc1.id).addEventListener('change', function(e){
				resLoc1 = moreStudyDetFunctions.getMoreStudyDetField("resLoc1");
				flexStudyScreenJS.showHideFields(resLoc1.value, 'resLoc1');
				return false;
			});
		}
	}
	
	if(flexStudyScreenJS.resLoc2) {
		flexStudyScreenJS.showHideFields((flexStudyScreenJS.resLoc2).value, 'resLoc2');

		if (!(flexStudyScreenJS.resLoc2).addEventListener) {
			(flexStudyScreenJS.resLoc2).on('change', function(e){
				flexStudyScreenJS.resLoc2 = moreStudyDetFunctions.getMoreStudyDetField("resLoc2");
				flexStudyScreenJS.showHideFields((flexStudyScreenJS.resLoc2).value, 'resLoc2');
				return false;
			});
		} else {
			document.querySelector('#'+(flexStudyScreenJS.resLoc2).id).addEventListener('change', function(e){
				flexStudyScreenJS.resLoc2 = moreStudyDetFunctions.getMoreStudyDetField("resLoc2");
				flexStudyScreenJS.showHideFields((flexStudyScreenJS.resLoc2).value, 'resLoc2');
				return false;
			});
		}
	}
	
	if(resLoc6) {
		flexStudyScreenJS.showHideFields(resLoc6.value, 'resLoc6');

		if (!resLoc6.addEventListener) {
			resLoc6.on('change', function(e){
				resLoc6 = moreStudyDetFunctions.getMoreStudyDetField("resLoc6");
				flexStudyScreenJS.showHideFields(resLoc6.value, 'resLoc6');
				return false;
			});
		} else {
			document.querySelector('#'+resLoc6.id).addEventListener('change', function(e){
				resLoc6 = moreStudyDetFunctions.getMoreStudyDetField("resLoc6");
				flexStudyScreenJS.showHideFields(resLoc6.value, 'resLoc6');
				return false;
			});
		}
	}
	
	if(resPop2) {
		flexStudyScreenJS.showHideFields(resPop2.value, 'resPop2');

		if (!resPop2.addEventListener) {
			resPop2.on('change', function(e){
				resPop2 = moreStudyDetFunctions.getMoreStudyDetField("resPop2");
				flexStudyScreenJS.showHideFields(resPop2.value, 'resPop2');
				return false;
			});
		} else {
			document.querySelector('#'+resPop2.id).addEventListener('change', function(e){
				resPop2 = moreStudyDetFunctions.getMoreStudyDetField("resPop2");
				flexStudyScreenJS.showHideFields(resPop2.value, 'resPop2');
				return false;
			});
		}
	}
	
	if(resPop4) {
		flexStudyScreenJS.showHideFields(resPop4.value, 'resPop4');

		if (!resPop4.addEventListener) {
			resPop4.on('change', function(e){
				resPop4 = moreStudyDetFunctions.getMoreStudyDetField("resPop4");
				flexStudyScreenJS.showHideFields(resPop4.value, 'resPop4');
				return false;
			});
		} else {
			document.querySelector('#'+resPop4.id).addEventListener('change', function(e){
				resPop4 = moreStudyDetFunctions.getMoreStudyDetField("resPop4");
				flexStudyScreenJS.showHideFields(resPop4.value, 'resPop4');
				return false;
			});
		}
	}
	
	if(confdlty1) {
		flexStudyScreenJS.showHideFields(confdlty1.value, 'confdlty1');

		if (!confdlty1.addEventListener) {
			confdlty1.on('change', function(e){
				confdlty1 = moreStudyDetFunctions.getMoreStudyDetField("confdlty1");
				flexStudyScreenJS.showHideFields(confdlty1.value, 'confdlty1');
				return false;
			});
		} else {
			document.querySelector('#'+confdlty1.id).addEventListener('change', function(e){
				confdlty1 = moreStudyDetFunctions.getMoreStudyDetField("confdlty1");
				flexStudyScreenJS.showHideFields(confdlty1.value, 'confdlty1');
				return false;
			});
		}
	}
	
	/*if(icd6) {
		flexStudyScreenJS.showHideFields(icd6.value, 'icd6');

		if (!icd6.addEventListener) {
			icd6.on('change', function(e){
				icd6 = moreStudyDetFunctions.getMoreStudyDetField("icd6");
				flexStudyScreenJS.showHideFields(icd6.value, 'icd6');
				return false;
			});
		} else {
			document.querySelector('#'+icd6.id).addEventListener('change', function(e){
				icd6 = moreStudyDetFunctions.getMoreStudyDetField("icd6");
				flexStudyScreenJS.showHideFields(icd6.value, 'icd6');
				return false;
			});
		}
	}*/
	
	if(resDesc1) {
		flexStudyScreenJS.showHideFields(resDesc1.value, 'resDesc1');

		if (!resDesc1.addEventListener) {
			resDesc1.on('change', function(e){
				resDesc1 = moreStudyDetFunctions.getMoreStudyDetField("resDesc1");
				flexStudyScreenJS.showHideFields(resDesc1.value, 'resDesc1');
				return false;
			});
		} else {
			document.querySelector('#'+resDesc1.id).addEventListener('change', function(e){
				resDesc1 = moreStudyDetFunctions.getMoreStudyDetField("resDesc1");
				flexStudyScreenJS.showHideFields(resDesc1.value, 'resDesc1');
				return false;
			});
		}
	}
	
	
	if(drgMngntFile) {
		flexStudyScreenJS.showHideFields(drgMngntFile.value, 'drgMngntFile');

		if (!drgMngntFile.addEventListener) {
			drgMngntFile.on('change', function(e){
				drgMngntFile = moreStudyDetFunctions.getMoreStudyDetField("drgMngntFile");
				flexStudyScreenJS.showHideFields(drgMngntFile.value, 'drgMngntFile');
				return false;
			});
		} else {
			document.querySelector('#'+drgMngntFile.id).addEventListener('change', function(e){
				drgMngntFile = moreStudyDetFunctions.getMoreStudyDetField("drgMngntFile");
				flexStudyScreenJS.showHideFields(drgMngntFile.value, 'drgMngntFile');
				return false;
			});
		}
	}
	
	if(devMngntFile) {
		flexStudyScreenJS.showHideFields(devMngntFile.value, 'devMngntFile');

		if (!devMngntFile.addEventListener) {
			devMngntFile.on('change', function(e){
				devMngntFile = moreStudyDetFunctions.getMoreStudyDetField("devMngntFile");
				flexStudyScreenJS.showHideFields(devMngntFile.value, 'devMngntFile');
				return false;
			});
		} else {
			document.querySelector('#'+devMngntFile.id).addEventListener('change', function(e){
				devMngntFile = moreStudyDetFunctions.getMoreStudyDetField("devMngntFile");
				flexStudyScreenJS.showHideFields(devMngntFile.value, 'devMngntFile');
				return false;
			});
		}
	}
	
	if(subCompReimb) {
		flexStudyScreenJS.showHideFields(subCompReimb.value, 'subCompReimb');

		if (!subCompReimb.addEventListener) {
			subCompReimb.on('change', function(e){
				subCompReimb = moreStudyDetFunctions.getMoreStudyDetField("subCompReimb");
				flexStudyScreenJS.showHideFields(subCompReimb.value, 'subCompReimb');
				return false;
			});
		} else {
			document.querySelector('#'+subCompReimb.id).addEventListener('change', function(e){
				subCompReimb = moreStudyDetFunctions.getMoreStudyDetField("subCompReimb");
				flexStudyScreenJS.showHideFields(subCompReimb.value, 'subCompReimb');
				return false;
			});
		}
	}
	
	if(payProratd) {
		flexStudyScreenJS.showHideFields(payProratd.value, 'payProratd');

		if (!payProratd.addEventListener) {
			payProratd.on('change', function(e){
				payProratd = moreStudyDetFunctions.getMoreStudyDetField("payProratd");
				flexStudyScreenJS.showHideFields(payProratd.value, 'payProratd');
				return false;
			});
		} else {
			document.querySelector('#'+payProratd.id).addEventListener('change', function(e){
				payProratd = moreStudyDetFunctions.getMoreStudyDetField("payProratd");
				flexStudyScreenJS.showHideFields(payProratd.value, 'payProratd');
				return false;
			});
		}
	}
	
	if(radMat1) {
		
		flexStudyScreenJS.showHideFields(radMat1.value, 'radMat1');
		
		radMat1 = moreStudyDetFunctions.getMoreStudyDetField("radMat1");
		if (!radMat1.addEventListener) {
			radMat1.on('change', function(e){
				flexStudyScreenJS.showHideFields(radMat1.value, 'radMat1');
				return false;
			});
		} else {
			radMat1.addEventListener('change', function(e){
				flexStudyScreenJS.showHideFields(radMat1.value, 'radMat1');
				return false;
			});
		}
	}
});


flexStudyScreenJS.showHideForLocAndAccrual = function() {
	var studyScope = $j('#studyScope');
	if(!studyScope) {
		return;
	}
	var subtyp = null;
	for (iX in moreStudyDetFunctions.studyScopeJSON) {
		if (moreStudyDetFunctions.studyScopeJSON[iX].pk == studyScope.val()) {
			subtyp = moreStudyDetFunctions.studyScopeJSON[iX].subtyp;
			break;
		}
	}
	
	var resLoc1 = moreStudyDetFunctions.getMoreStudyDetField("resLoc1");
	var resLoc2 = moreStudyDetFunctions.getMoreStudyDetField("resLoc2");
	var resloc3 = moreStudyDetFunctions.getMoreStudyDetField("resloc3");
	var resLoc6 = moreStudyDetFunctions.getMoreStudyDetField("resLoc6");
	var accrual5 = moreStudyDetFunctions.getMoreStudyDetField("accrual5");
	var accrual6 = moreStudyDetFunctions.getMoreStudyDetField("accrual6");
	var accrual7 = moreStudyDetFunctions.getMoreStudyDetField("accrual7");
	var accrual8 = moreStudyDetFunctions.getMoreStudyDetField("accrual8");
	var resLoc7 = moreStudyDetFunctions.getMoreStudyDetField("resLoc7");
	var studyLocAddInfo = moreStudyDetFunctions.getMoreStudyDetField("studyLocAddInfo");
	
	var bothMDAnOtherShow = [resLoc1, resloc3, resLoc6, resLoc7, studyLocAddInfo, accrual5, accrual6, accrual8, accrual7];
	var bothMDAnOtherHide = [resLoc2];
	var onlyMDACCShow = [accrual5, accrual6, accrual8];
	var onlyMDACCHide = [resLoc1, resLoc2, resloc3, resLoc6, accrual7, resLoc7, studyLocAddInfo];
	var onlyOtherShow = [resLoc1, resloc3, resLoc6, resLoc7, studyLocAddInfo, accrual8, accrual7];
	var onlyOtherHide = [resLoc2, accrual6, accrual5];
	
	var defHide= [accrual5, accrual6, accrual8, resLoc1, resLoc2, resloc3, resLoc6, accrual7, resLoc7, studyLocAddInfo];

	
	switch (subtyp) {
	case 'bothMDAnOther':
		for(var count= 0; count<bothMDAnOtherShow.length; count++) {
			flexStudyScreenJS.runInterFieldActions(bothMDAnOtherShow[count], 'show');
		}
		for(var count= 0; count<bothMDAnOtherHide.length; count++) {
			flexStudyScreenJS.runInterFieldActions(bothMDAnOtherHide[count], 'hide');
		}
		break;
	case 'onlyMDACC':
		for(var count1= 0; count1<onlyMDACCHide.length; count1++) {
			flexStudyScreenJS.runInterFieldActions(onlyMDACCHide[count1], 'hide');
		}
		for(var count= 0; count<onlyMDACCShow.length; count++) {
			flexStudyScreenJS.runInterFieldActions(onlyMDACCShow[count], 'show');
		}
		break;
	case 'onlyOther':
		for(var count= 0; count<onlyOtherShow.length; count++) {
			flexStudyScreenJS.runInterFieldActions(onlyOtherShow[count], 'show');
		}
		for(var count= 0; count<onlyOtherHide.length; count++) {
			flexStudyScreenJS.runInterFieldActions(onlyOtherHide[count], 'hide');
		}
		break;
	default:
		for(var count= 0; count<defHide.length; count++) {
			flexStudyScreenJS.runInterFieldActions(defHide[count], 'hide');
		}
		break;
	};
	
};


flexStudyScreenJS.runLocAndAccuralOnReady = function() {
	var studyScope = $j('#studyScope');
	if(!studyScope) {
		return;
	}
	//flexStudyScreenJS.showHideForLocAndAccrual(studyScope.val());
	studyScope.change(function(){
		//Commented for testing
		//flexStudyScreenJS.showHideForLocAndAccrual();
	});
};

moreStudyDetFunctions.setDD = function(formobj){
	//var optvalue=document.getElementsByName("ddlist")[0].value;
	
	if (!document.getElementsByName("ddlist")[0]) return;
	var optvalue=formobj.ddlist.value;
	if (optvalue.length>0)
	{
		var arrayofStrings=optvalue.split("||");
		if (arrayofStrings.length>1) {
			for (var j=0;j<arrayofStrings.length;j++)
			{
				var ddStr=arrayofStrings[j];
				var arrayofDD=ddStr.split(":");
				var ddcount=arrayofDD[0];
				var selvalue=arrayofDD[1];
				var ddFld = formobj['alternateId'+ddcount];
				ddFld.id = ddFld.name;
				if (ddFld && ddFld.options) {
					var opt = ddFld.options;
					for (var i=0;i<opt.length;i++){
						if (opt[i].value==selvalue){
							ddFld.selectedIndex=i ;
						}
					}
				}
			}
		} else {
			var ddStr=arrayofStrings[0];
			var arrayofDD=ddStr.split(":");
			var ddcount=arrayofDD[0];
			var selvalue=arrayofDD[1];
			var ddFld = formobj['alternateId'+ddcount];
			ddFld.id = ddFld.name;
			if (ddFld && ddFld.options) {
			 	var opt = ddFld.options; 
				if (opt == undefined)
			    	opt = ddFld.options; 
				for (var i=0;i<opt.length;i++){
					if (opt[i].value==selvalue){
						ddFld.selectedIndex=i ;
					}
				}
			}
		}// end else
	}//optvalue.length>0	
};

moreStudyDetFunctions.fixTextAreas = function(){
	
	//Maximum possible database limit is 4000 charcters
	var characters= 4000;
	$j(".mdTextArea").keyup(function(){
	    
		if($j(this).val().length > characters){
	        $j(this).val($j(this).val().substr(0, characters));
		}	
	});
}

try {
	flexStudyScreenJS.runLocAndAccuralOnReady();
} catch(e) {
	//alert(e);
}
moreStudyDetFunctions.formObj = document.studyScreenForm;
try {
	moreStudyDetFunctions.setDD(moreStudyDetFunctions.formObj);
} catch(e) {}
try {
	moreStudyDetFunctions.fixTextAreas();
} catch(e) {}