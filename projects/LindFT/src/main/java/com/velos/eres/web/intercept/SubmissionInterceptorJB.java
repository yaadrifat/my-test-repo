package com.velos.eres.web.intercept;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.velos.eres.service.util.CFG;
import com.velos.eres.web.study.StudyJB;

public class SubmissionInterceptorJB extends InterceptorJB {
	public static final String STUDY_NUMBER_STR = "studyNumber";
	public static final String TBD_STR = "TBD";
	public static final String TYPE_PROTOCOL = "Protocol";
	public static final String TYPE_AMENDMENT = "Amendment";
	public static final String GET_STR = "GET";
	public static final String POST_STR = "POST";
	public static final String INTL_DATE_FORMAT = "yyyy-MM-dd";

	public void handle(Object...args) {
		HttpServletRequest request = (HttpServletRequest) args[0];
		StudyJB studyJB = (StudyJB) args[1];
		String studyNumber = studyJB.getStudyNumber();
		System.out.println("In SubmissionInterceptorJB for "+studyNumber);
		if (TBD_STR.equals(CFG.Submission_Teaugo_Url)) {
			return;
		}
	    String today = null;
	    try {
	      Date now = Calendar.getInstance().getTime();
	      SimpleDateFormat dateFormat = new SimpleDateFormat(INTL_DATE_FORMAT);
	      today = dateFormat.format(now);
	    } catch (Exception e) {}
		try {
			String type = TYPE_PROTOCOL;
			String fullVersion = (String)args[2];
			if (fullVersion.indexOf(".") > 0) {
				int minorVerInt = 0;
				if (fullVersion.length() > fullVersion.indexOf(".")) {
					minorVerInt = Integer.parseInt(fullVersion.substring(fullVersion.indexOf(".")+1));
				}
				System.out.println("minorVersion="+minorVerInt);
				if (minorVerInt > 0) {
					type = TYPE_AMENDMENT;
				}
			}
			System.out.println("type="+type+" version="+args[2]);
			StringBuffer fullUrl = new StringBuffer(CFG.Submission_Teaugo_Url);
			fullUrl.append("?studyNumber=").append(URLEncoder.encode(studyNumber, "UTF-8"))
			.append("&version=").append(args[2])
			.append("&type=").append(type)
			.append("&submissionDate=").append(today)
			.append("&username=").append(CFG.Submission_Username)
			.append("&token=").append(CFG.Submission_Token);
			URL url = new URL(fullUrl.toString());
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setUseCaches(false);
			conn.setConnectTimeout(15000);
			conn.setReadTimeout(15000);
			conn.setRequestMethod(POST_STR);
			// conn.setRequestProperty("Cookie", cookieValue);
			// conn.setRequestProperty("Content-Type", contentType);
			String respStr = readResponseContent(conn);
			System.out.println("respStr="+respStr);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

    private String readResponseContent(HttpURLConnection conn) throws IOException {
        StringBuffer sb = new StringBuffer();
        char[] buffer = new char[1];
        try {
            if (conn.getInputStream() != null) {
                InputStreamReader reader = new InputStreamReader(conn.getInputStream(), "UTF-8");
                int iRead = 0;
                while ( true ) {
                    iRead = reader.read(buffer);
                    if (iRead < 0) break;
                    sb.append(buffer);
                }
            }
        } catch (IOException e) {} 
        finally {
            try { conn.getInputStream().close(); } catch(IOException e) {}
        }
        return sb.toString();
    }
	
}
