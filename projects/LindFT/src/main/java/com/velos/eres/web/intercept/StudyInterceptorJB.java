package com.velos.eres.web.intercept;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.velos.eres.service.util.CFG;
import com.velos.eres.web.study.StudyJB;
import com.velos.esch.service.util.StringUtil;

public class StudyInterceptorJB extends InterceptorJB {
	public static final String STUDY_NUMBER_STR = "studyNumber";
	public static final String STUDY_ID = "studyId";
	public static final String TBD_STR = "TBD";
	public static final String UTF8_STR = "UTF-8";
	public static final String PRESUBMISSION_STR = "Presubmission";
	public static final String GET_STR = "GET";
	public static final String POST_STR = "POST";
	public static final String INTL_DATE_FORMAT = "yyyy-MM-dd";
	
	public void handle(Object...args) {
		HttpServletRequest request = (HttpServletRequest) args[0];
		Integer studySaveFlag = (Integer) args[1];
		String studyNumber = (String)request.getParameter(STUDY_NUMBER_STR);
		System.out.println("In StudyInterceptorJB for "+studyNumber);
		if (StringUtil.isEmpty(studyNumber)) {
			String studyId = (String)request.getAttribute(STUDY_ID);
			System.out.println(STUDY_ID+studyId);
			StudyJB studyJB = new StudyJB();
			studyJB.setId(StringUtil.stringToNum(studyId));
			studyJB.getStudyDetails();
			studyNumber = studyJB.getStudyNumber();
		}
		if (TBD_STR.equals(CFG.Submission_Teaugo_Url)) {
			return;
		}
	    String today = null;
	    try {
	      Date now = Calendar.getInstance().getTime();
	      SimpleDateFormat dateFormat = new SimpleDateFormat(INTL_DATE_FORMAT);
	      today = dateFormat.format(now);
	    } catch (Exception e) {}
		try {
			StringBuffer fullUrl = new StringBuffer(CFG.Submission_Teaugo_Url);
			fullUrl.append("?studyNumber=").append(URLEncoder.encode(studyNumber, UTF8_STR))
			//.append("&version=").append(args[2])
			.append("&type=").append(PRESUBMISSION_STR) // type to indicate CAT trigger
			.append("&submissionDate=").append(today)
			.append("&username=").append(CFG.Submission_Username)
			.append("&token=").append(CFG.Submission_Token);
			URL url = new URL(fullUrl.toString());
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setUseCaches(false);
			conn.setConnectTimeout(15000);
			conn.setReadTimeout(15000);
			conn.setRequestMethod(POST_STR);
			// conn.setRequestProperty("Cookie", cookieValue);
			// conn.setRequestProperty("Content-Type", contentType);
			String respStr = readResponseContent(conn);
			System.out.println("respStr="+respStr);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

    private String readResponseContent(HttpURLConnection conn) throws IOException {
        StringBuffer sb = new StringBuffer();
        char[] buffer = new char[1];
        try {
            if (conn.getInputStream() != null) {
                InputStreamReader reader = new InputStreamReader(conn.getInputStream(), UTF8_STR);
                int iRead = 0;
                while ( true ) {
                    iRead = reader.read(buffer);
                    if (iRead < 0) break;
                    sb.append(buffer);
                }
            }
        } catch (IOException e) {} 
        finally {
            try { conn.getInputStream().close(); } catch(IOException e) {}
        }
        return sb.toString();
    }
	
}
