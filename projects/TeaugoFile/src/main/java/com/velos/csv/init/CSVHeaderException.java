package com.velos.csv.init;



public class CSVHeaderException extends Exception{
private static final long serialVersionUID = 7718828512143293558L;
	
private String responeType;
private String emailNotfMsg;

public CSVHeaderException() {
	super();
}

public CSVHeaderException(String message) {
	super(message);
}


public CSVHeaderException(String message, String responeType) {
	super(message);
	this.responeType = responeType;
}
public String getResponeType() {
	return responeType;
}

public void setResponeType(String responeType) {
	this.responeType = responeType;
}

public CSVHeaderException(String message,String emailNotfMsg, String responeType) {
	super(message);
	this.responeType = responeType;
	this.emailNotfMsg= emailNotfMsg;
}

public String getEmailNotfMsg() {
	return emailNotfMsg;
}

public void setEmailNotfMsg(String emailNotfMsg) {
	this.emailNotfMsg = emailNotfMsg;
}
	
}
