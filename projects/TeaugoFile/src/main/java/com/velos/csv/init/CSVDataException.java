package com.velos.csv.init;

public class CSVDataException extends Exception {
	private String responseType;
	private String emailNotfMsg;

	public CSVDataException() {
		super();
	}

	public CSVDataException(String message) {
		super(message);
	}


	public CSVDataException(String message, String responeType) {
		super(message);
		this.responseType = responeType;
	}
	public String getResponeType() {
		return responseType;
	}

	public void setResponeType(String responeType) {
		this.responseType = responeType;
	}

	public CSVDataException(String message,String emailNotfMsg, String responeType) {
		super(message);
		this.responseType = responeType;
		this.emailNotfMsg= emailNotfMsg;
	}

	public String getEmailNotfMsg() {
		return emailNotfMsg;
	}

	public void setEmailNotfMsg(String emailNotfMsg) {
		this.emailNotfMsg = emailNotfMsg;
	}
		
}
