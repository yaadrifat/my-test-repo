package com.velos.notifications;


import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

import com.velos.processor.FTPCopier;

public class SendMailWithVelocityTemplate {
	
	Logger logger=Logger.getLogger(SendMailWithVelocityTemplate.class);
	//Map<String, List> headers = null;
	java.util.List headers = null;

	public void sendMail(Properties properties, String filename, String date,
			String notification, Map<String, java.util.List> errors)
			throws Exception {
		//sheaders = new ArrayList();
		Properties props = new Properties();
		Map<String, List> map = new HashMap<String, List>();

	  headers =  errors.get("headers");

		props.put("mail.smtp.host", properties.getProperty("mail.smtp.host")
				.trim());

		props.put("mail.smtp.auth", properties.getProperty("mail.smtp.auth")
				.trim());
		props.put("mail.smtp.port", properties.getProperty("mail.smtp.port")
				.trim());

		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.ssl.protocols", "TLSv1.2");

		props.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");

		String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss")
				.format(new Date());
		final Properties prop = System.getProperties();

		Session session = Session.getDefaultInstance(props);
		// compose message
		try {

			logger.info("in send mail  ");
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(properties
					.getProperty("NotificationFrom")));

			if (notification == "success") {

				String to = properties
						.getProperty("SucessfulImportNotificationTO");
				String tos[] = to.split(",");

				for (String tt : tos) {

					message.addRecipient(Message.RecipientType.TO,
							new InternetAddress(tt));
				}
			} else {

				String to = properties.getProperty("ErrorNotificationTO");
				String tos[] = to.split(",");
				for (String tt : tos) {

					message.addRecipient(Message.RecipientType.TO,
							new InternetAddress(tt));
				}
			}

			message.setSubject("Teaugo_File NOTIFICATION");

			// Map <String,String>map=new HashMap<String,String>();

			BodyPart body = new MimeBodyPart();

			VelocityEngine ve = new VelocityEngine();
			ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
			ve.setProperty("classpath.resource.loader.class",
					ClasspathResourceLoader.class.getName());
			ve.init();

			/* next, get the Template */
			Template t = ve.getTemplate("templates/mail-body-template.vm");
			VelocityContext context = new VelocityContext();

			context.put("timestamp", date);
			context.put("requesttype", "dignity file interface");
			context.put("filename", filename);
			context.put("errors", errors);

			if (notification == "success") {
				context.put("requeststatus", notification);
				context.put("errordescription", "transferred successfully");
			} else {
				context.put("requeststatus", notification);
				context.put("errordescription", notification);
			}
			StringWriter out = new StringWriter();
			t.merge(context, out);

			body.setContent(out.toString(), "text/html");

			message.setContent(out.toString(), "text/html");

			// send message
			Transport.send(message);
			logger.info("Mail sent successfully  ");

		} catch (Exception e) {
			//throw new Exception(e);
			logger.error("Failed to send email",e);
		}

	}

}
