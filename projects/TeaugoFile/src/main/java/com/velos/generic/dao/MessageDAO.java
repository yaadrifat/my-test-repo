package com.velos.generic.dao;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;



public class MessageDAO {
	
	JdbcTemplate jdbcTemplate;
	public MessageDAO(JdbcTemplate jdbcTemplate){
		this.jdbcTemplate=jdbcTemplate;
		
		
	}
	final static  Logger logger=Logger.getLogger(MessageDAO.class);;
	// DataSource vgdbDataSource;
	
// SingleConnectionDataSource ds = null;
//	JdbcTemplate jt = null;
	Properties config=new Properties();

	public void readCSV(String path) throws IOException {
		
		
/*InputStream propertie=this.getClass().getClassLoader().getResourceAsStream("config.properties");
config.load(propertie);
		ds = new SingleConnectionDataSource();
		ds.setDriverClassName(config.getProperty("vgdbDataSource.driverClassName"));
		ds.setUrl(config.getProperty("vgdbDataSource.url"));
		ds.setUsername(config.getProperty("vgdbDataSource.username"));
		
		ds.setPassword(config.getProperty("vgdbDataSource.password"));

		jt = new JdbcTemplate(ds);
*/
		
		
		
		/*
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(ds)
				.withFunctionName("export_audit");

		SqlParameterSource in = new MapSqlParameterSource().addValue(
				"file_name", "/home/kmohanbabu/Document/audit.csv");*/
		//jdbcCall.executeFunction(String.class, in);

		
		
	}
	
	public String createTable(StringBuilder createQuery,String tableName) throws IOException
	{
		String query=null;
		
		
		/*InputStream propertie=this.getClass().getClassLoader().getResourceAsStream("config.properties");
		config.load(propertie);
				ds = new SingleConnectionDataSource();
				ds.setDriverClassName(config.getProperty("vgdbDataSource.driverClassName"));
				ds.setUrl(config.getProperty("vgdbDataSource.url"));
				ds.setUsername(config.getProperty("vgdbDataSource.username"));
				
				ds.setPassword(config.getProperty("vgdbDataSource.password"));

				jt = new JdbcTemplate(ds);
				String flag=null;
				String query=null;*/
				
				
		try{
			
			query=createQuery.substring(0, createQuery.length()-1);
			query+=")";
			jdbcTemplate.execute(query);
			
			
			/* flag=jt.queryForObject("SELECT '"+tableName+"'::regclass", String.class);
	 logger.info("flaaaaaaaag"+flag);
		//logger.info("flaaaaaaaag"+flag);
*/		
		
		
		}catch(Exception e){
			
			e.printStackTrace();
			/*if(flag!=null&&flag==tableName){
				logger.info("in catch block of creating tablesss");
				jt.execute(query);
				logger.info("in catch block of creating tablesss2");
			}
			else
			jt.execute(query);
			logger.info("in else block of creating tablesss2");*/
		}
			

		
		
		return query+")";
		
	}
	
	
	public void insertAudit(Map map)
	{
		StringBuilder insert_audit = new StringBuilder();
	
	
	String query = "insert into vgdb_file_audit_details(creation_date,file_name,response,clientname)values( ";
		try{
			insert_audit.append(query);
			insert_audit.append("'");
			insert_audit.append(map.get("creation_date"));
			insert_audit.append("'");
			insert_audit.append(",");
			insert_audit.append("'");
			insert_audit.append(map.get("file_name"));
			insert_audit.append("'");
			insert_audit.append(",");
			insert_audit.append("'");
			insert_audit.append(map.get("response"));
			insert_audit.append("'");
			insert_audit.append(",");
			insert_audit.append("'");
			insert_audit.append(map.get("clientname"));
			insert_audit.append("'");
			insert_audit.append(")");
			jdbcTemplate.execute(insert_audit.toString());
			
		}catch(Exception e){
			e.printStackTrace();
			logger.info("in catch 1");
			throw e;
		}
		
	}	
	
	public void insertData(String csvdata,Properties conf) throws IOException, SQLException{
		String DB_Name=null;
		//logger.info(csvdata);
		StringBuilder insertQuery=new StringBuilder();
		
		
		
	/*//	DB_Name=conf.getProperty(DB_Name);
		if(DB_Name=="postgres")
		{
			insertQuery.append("insert into(");
			
			for(String header:resultHeaders)
			{
				insertQuery.append(header);
				insertQuery.append(",");
				
				
			}
			
			String finalquery=insertQuery.substring(0, insertQuery.length()-1);
			
			
			
		}
		*/
		
		
		
		/*InputStream propertie=this.getClass().getClassLoader().getResourceAsStream("config.properties");
		config.load(propertie);
				ds = new SingleConnectionDataSource();
				ds.setDriverClassName(config.getProperty("vgdbDataSource.driverClassName"));
				ds.setUrl(config.getProperty("vgdbDataSource.url"));
				ds.setUsername(config.getProperty("vgdbDataSource.username"));
				
				ds.setPassword(config.getProperty("vgdbDataSource.password"));

				jt = new JdbcTemplate(ds);
		*/
		try{
			//logger.info("in try "+csvdata);
			//logger.info("||||||||||||Query ||||||||||n"+csvdata);
			jdbcTemplate.execute(csvdata);
			
			
		}catch(Exception ee){
			ee.printStackTrace();
			logger.info("in catch 1");
			throw ee;
		
			
			
		}
		
	}

	

}
