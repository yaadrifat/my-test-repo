package com.velos.processor;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;

import com.opencsv.CSVReader;
import com.velos.csv.init.CSVDataException;
import com.velos.csv.init.CSVHeaderException;
import com.velos.generic.dao.MessageDAO;
import com.velos.notifications.SendMailWithVelocityTemplate;

public class FileProcessor {

	MessageDAO dao;
	Properties props = null;
	Map<String, List> errors = null;

	public FileProcessor(MessageDAO dao) {

		this.dao = dao;
	}

	public SendMailWithVelocityTemplate template;

	public SendMailWithVelocityTemplate getTemplate() {
		return template;
	}

	public void setTemplate(SendMailWithVelocityTemplate template) {
		this.template = template;
	}

	Logger logger = Logger.getLogger(FileProcessor.class);
	String localerrorfiles = null;
	String localparsedfiles = null;
	String sourcePath = null;
	String destDir = null;
	String errorfiles = null;
	String parsedfiles = null;
	String pattern = "yyyy/MM/dd HH:mm:ss";
	public String clientname;
	/*
	 * String host=null; String port=null; String auth=null; String
	 * notificationfrom=null; String errornotificationto=null; String
	 * successfulnotificationto=null;
	 */
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
	Map<String, String> audit = new HashMap<String, String>();
	String date = simpleDateFormat.format(new Date());

	// String sourcePath="/home/kmohanbabu/Document/ftpFiles/";
	CSVFileReader reader;

	List<File> files = null;
	FTPCopier copier = null;

	public CSVFileReader getReader() {
		return reader;
	}

	public void setReader(CSVFileReader reader) {
		this.reader = reader;
	}

	public void getConfigs() {

	}

	public void processFiles() throws CSVDataException, CSVHeaderException,
			Exception {

		logger.info("in process files=================");
		Map<String, String> map = new HashMap<String, String>();

		/*
		 * InputStream
		 * props1=this.getClass().getClassLoader().getResourceAsStream
		 * ("field-config.properties"); try { properties1.load(props1);
		 * 
		 * for (final String name: properties1.stringPropertyNames())
		 * map.put(name, properties1.getProperty(name));
		 * 
		 * for(String key:map.keySet()){
		 * 
		 * 
		 * }
		 * 
		 * 
		 * for (Map.Entry<String, String> entry : map.entrySet()) {
		 * //System.out.println(entry.getKey() + "/" + entry.getValue()); }
		 * 
		 * 
		 * // System.out.println("field mapping for db================"+map);
		 * 
		 * 
		 * 
		 * } catch (IOException e) {
		 * 
		 * e.printStackTrace(); }
		 */

		Properties properties = reader.getConfigs("config");

		errorfiles = properties.getProperty("sftp.errorfiles");
		parsedfiles = properties.getProperty("sftp.parsedfiles");
		String sourcePath1 = properties.getProperty("sftp.sourcePath_upload");
		destDir = properties.getProperty("sftp.sourcePath");
		String forward_slash = properties.getProperty("sftp.forward_slash");
		String remoteAddress = properties.getProperty("sftp.remoteAddress");
		localerrorfiles = properties.getProperty("local.errorfiles");
		localparsedfiles = properties.getProperty("local.parsedfiles");

		try {

			if (reader.getConfigs("config").getProperty("source").equalsIgnoreCase("NTFS")) {
				sourcePath = properties.getProperty("local.sourcePath");
				logger.info("source path------------------" + sourcePath);
				getLocalCSVFiles(sourcePath);

			}

			if (reader.getConfigs("config").getProperty("source").equalsIgnoreCase("SFTP")) {
				int remotePort = Integer.parseInt(properties.getProperty("sftp.remotePort"));
				String userName = properties.getProperty("sftp.userName");
				String password = properties.getProperty("sftp.password");
				sourcePath = properties.getProperty("local.sourcePath");

				copier = new FTPCopier(remoteAddress, remotePort, userName,password);

				files = copier.downloadFile(destDir, sourcePath, forward_slash);

				getLocalCSVFiles(sourcePath);

				// readFiles(files, sourcePath, copier, destDir);
			}

		} catch (IOException e) {
			e.printStackTrace();
			throw e;
		}

		catch (CSVDataException | CSVHeaderException e)

		{
			e.printStackTrace();

			throw new CSVDataException(e.getMessage());

		}

		catch (Exception e) {
			e.printStackTrace();
			throw new CSVDataException(e.getMessage());

		} finally {
			if (reader.getConfigs("config").getProperty("source").equalsIgnoreCase("sftp")) {
				copier.closeCOnnection();
			}
		}
	}

	private int getHeaderLocation(String[] headers, String columnName) {
		return Arrays.asList(headers).indexOf(columnName);
	}

	public void getLocalCSVFiles(String sourceValue) throws Exception {

		String notification = null;
		Map<String, String> mailmap = new HashMap<String, String>();
		logger.info(" *****Checking downloaded  Local CSV files  to parse *****");
		String newFileName = null;
		String fileName = null;
		File file = new File(sourceValue);
		logger.info("source value-------" + sourceValue);
		File[] listFiles = file.listFiles();

		logger.info("list of files-----" + listFiles);

		int filexitVal = listFiles.length;
		ArrayList<String> list = new ArrayList<String>();
		list.add("SITE ID");
		list.add("Site ID");
		list.add("SITEID");
		list.add("SiteID");
		logger.info("Checking Local files exist or not ......" + filexitVal);

		if (filexitVal != 0) {
			for (File localFile : listFiles) {
				try {
					logger.info("Local FileName to parse ==>"+ localFile.getName());
					boolean flag = localFile.getName().endsWith(".csv");

					if (localFile != null && localFile.isFile()) {
						if (flag) {

							CSVReader csvreader = new CSVReader(new FileReader(localFile), ',', '"', 0);
							String headers[] = csvreader.readNext();

							int location = getHeaderLocation(headers, "Site ID");

							CSVReader data = new CSVReader(new FileReader(localFile), ',', '"', 1);

							String dd[] = data.readNext();

							String confFilename = dd[location];

							logger.info("configuration file name-----------------"+ confFilename);

							fileName = localFile.getName();

							if (confFilename != null) {

								props = reader.getConfigs(confFilename.trim());
								clientname = props.getProperty("clientname");
							}
							errors = new HashMap<String, List>();
							reader.readCSV(localFile, sourceValue,confFilename.trim(),errors);

							System.out.println(errors);
							//
							if (filexitVal != 0) {

								audit.put("file_name", fileName);

								audit.put("clientname", clientname);
								audit.put("creation_date", date);
								int i = 0;

								/*if (errors.size() > 0) {
									logger.info("|||||Error = " + errors);
									for (String name : errors.keySet())

										if (i == 0) {
											notification = "failed";
											audit.put("response", name);
											i++;

											template.sendMail(props, fileName,date, notification, errors);

										}

								}*/ 
								if (!errors.isEmpty()) {
									logger.info("Error = " + errors);
									logger.info("Error size = " + errors.size());
										
									audit.put("response", "Failed to import the file");
											
									if (reader.getConfigs("config").getProperty("source").equalsIgnoreCase("sftp")) {	
										copier.moveFiles(fileName,errorfiles.concat(fileName));
										moveFile(fileName, sourceValue, localerrorfiles);
									} else {
										moveFile(fileName, sourceValue, localerrorfiles);
									}
								}else {

									audit.put("response","file saved successfully");

									notification = "success";
									template.sendMail(props, fileName, date,notification, errors);
									
									if (reader.getConfigs("config").getProperty("source").equalsIgnoreCase("sftp")) {
										copier.moveFiles(fileName,parsedfiles.concat(fileName));
										moveFile(fileName, sourceValue,localparsedfiles);
									} else {
										moveFile(fileName, sourceValue,	localparsedfiles);
									}
									logger.info("file name to move----- "+ parsedfiles.concat(fileName));
								}

								dao.insertAudit(audit);
								// template.sendMail(props, fileName,
								// date,notification,errors);

								/*if (reader.getConfigs("config")
										.getProperty("source")
										.equalsIgnoreCase("sftp")) {

									copier.moveFiles(fileName,
											parsedfiles.concat(fileName));
									moveFile(fileName, sourceValue,
											localparsedfiles);
								} else {
									moveFile(fileName, sourceValue,
											localparsedfiles);
								}
								logger.info("file name to move----- "
										+ parsedfiles.concat(fileName));*/

							}

						}

						else {
							fileName = localFile.getName();
							throw new CSVDataException(
									"Unsupported file  format ."
											+ FilenameUtils
													.getExtension(localFile
															.getAbsolutePath()));
						}

					}
				} catch (Exception e) {
					e.printStackTrace();

					audit.put("file_name", fileName);
					audit.put("response", e.getMessage());
					audit.put("clientname", clientname);
					audit.put("creation_date", date);
					//
					notification = "fail";
					if (filexitVal != 0) {
						dao.insertAudit(audit);
						if (reader.getConfigs("config").getProperty("source").equalsIgnoreCase("sftp")) {
							copier.moveFiles(fileName,errorfiles.concat(fileName));
							moveFile(fileName, sourceValue, localerrorfiles);
						} else {
							moveFile(fileName, sourceValue, localerrorfiles);
						}

						if (!(e.getMessage().contains("Unsupported file  format")))
							logger.info("Unsupported file  formaat = "+ errors);
							//template.sendMail(props, fileName, date,e.getMessage(), errors);

					}
				}

			}

		} else {
			logger.info(" ************* Local CSV Files Doesn't exist ********* ");
		}

	}

	/*
	 * for reading files
	 */
	/*
	 * public void readFiles(List<File> files, String sourcePath, FTPCopier
	 * copier, String dest) throws Exception {
	 * 
	 * logger.info("list of files downloaded ......" + files.toString());
	 * 
	 * logger.info("list of files ===========" + files);
	 * 
	 * for (File file : files) {
	 * 
	 * String[] fileName = file.getName().split("_"); String newFileName =
	 * fileName[0];
	 * 
	 * logger.info("file name===============================" + file.getName());
	 * 
	 * try {
	 * 
	 * List create = reader.readCSV(file, sourcePath, newFileName);
	 * if(create=="error") //copier.moveFiles(dest.concat(file.getName()), //
	 * errorfiles.concat(file.getName())); audit.put("file_name",
	 * file.getName()); audit.put("acknowledge_type", "AA");
	 * audit.put("clientname", "dignity"); audit.put("creation_date", date);
	 * dao.insertAudit(audit);
	 * 
	 * }
	 * 
	 * catch (Exception e) {
	 * 
	 * e.printStackTrace();
	 * 
	 * //copier.moveFiles(dest.concat(file.getName()), //
	 * errorfiles.concat(file.getName()));
	 * 
	 * }
	 * 
	 * }
	 * 
	 * }
	 */
	public void moveFile(String fileName, String downloadedXmlFile,
			String parsedFileLoc) throws Exception {
		try {
			logger.info("***************moving Local CSV files******** >"
					+ downloadedXmlFile + " ****** TO******** " + parsedFileLoc);
			Path src = Paths.get(downloadedXmlFile + fileName.trim());
			Path dest = Paths.get(parsedFileLoc + fileName.trim());
			Files.move(src, dest, StandardCopyOption.REPLACE_EXISTING);
			logger.info("local CSV file moved successfully");
		}

		catch (Exception e) {
			e.printStackTrace();
			logger.info("moveFile ===>" + e.getMessage());
			throw e;
		}
	}

	public void writeFiles() {

	}

}
