 package com.velos.processor;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Vector;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.log4j.Logger;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpATTRS;
import com.jcraft.jsch.SftpException;

public class FTPCopier {

	Logger logger=Logger.getLogger(FTPCopier.class);
	private final static String ARCHIVE_DIR = "archive/";

	private ChannelSftp channelSftp;
	private Session session=null;
	private Channel channel=null;

	private final String userName;
	private final String remoteAddress;
	private final String password;
	private final int remotePort;
	private  String fileNames;



	public FTPCopier(final String remoteAddress, final int remotePort,
			final String userName, final String password) {
		
		this.remoteAddress = remoteAddress;
		this.remotePort = remotePort;
		this.userName = userName;
		this.password = password;
		
			
	}

	
	/*public void downloadFTP(){
		
		String server = "192.168.192.101";
        int port = 20202;
        String user = "chnftp";
        String pass = "2@ccessa1th(hn";
 
        FTPClient ftpClient = new FTPClient();
        try {
 
            ftpClient.connect(server, port);
            ftpClient.login(user, pass);
            ftpClient.enterLocalPassiveMode();
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
 logger.info("-------------------in download FTP-----------");
            // APPROACH #1: using retrieveFile(String, OutputStream)
            String remoteFile1 = "/ftp/chnftp/aithin/Interfaces/CSVfiles/errorfiles/Dignity002_PCHSampleDatarcvd2018Aug08editsAJ20180815.csv";
            File downloadFile1 = new File("/home/kmohanbabu/Document/ftpFiles/Dignity002_PCHSampleDatarcvd2018Aug08editsAJ20180815.csv");
            OutputStream outputStream1 = new BufferedOutputStream(new FileOutputStream(downloadFile1));
            boolean success = ftpClient.retrieveFile(remoteFile1, outputStream1);
            outputStream1.close();
 
            if (success) {
                System.out.println("File #1 has been downloaded successfully.");
            }
        } catch (IOException ex) {
            System.out.println("Error: " + ex.getMessage());
            ex.printStackTrace();
        } finally {
            try {
                if (ftpClient.isConnected()) {
                    ftpClient.logout();
                    ftpClient.disconnect();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        
            
            
		
	}
	*/
	

	public List<File> downloadFile(final String sourceFilePath, String destDir,String forward_slash)
			throws Exception {
		final List<File> downloadedFileList = new ArrayList<File>();
		try {
			if (session == null) {
				logger.info("in download file..............");
				connectToFTP();
			}
			// Session connect begin
			if (!session.isConnected()) {
				logger.info(session);
				session.setConfig("PreferredAuthentications", 
		                  "publickey,keyboard-interactive,password");
				session.connect();
			}
			// Session connect end

			logger.info(session);
			String protocol = "sftp";
			
			channel = session.openChannel(protocol);
			
			channel.connect();
			channelSftp = (ChannelSftp) channel;
			
			logger.info("source file path----"+sourceFilePath+"   destination path-----"+destDir);
			
			
			//final String archiveDir = "/ftp/chnftp/aithtemp/InterfaceTeam";

			//createSftpDir(archiveDir);

			channelSftp.cd("//");
			try {
				// cd relative Dir
				channelSftp.cd(sourceFilePath);
			
			} catch (final SftpException e) {
				e.printStackTrace();
				throw new SftpException(1, e.getLocalizedMessage());
			}

			if (!destDir.endsWith("/") && !destDir.endsWith("\\")) {
			
				destDir = destDir.concat(File.separator);
				logger.info("dsetination dir=============="+destDir);
			}

			final Vector<LsEntry> fileList = channelSftp.ls("*");
			final Iterator<LsEntry> it = fileList.iterator();
			logger.info("fileList===="+fileList);
			
			
			while (it.hasNext()) {
				final LsEntry entry = it.next();
				logger.info("atttrrr=="+entry.getAttrs());
				if (entry.getAttrs().isDir()) {
					continue;
				}
				final File file = new File(destDir.concat(entry.getFilename()));
				final FileOutputStream fileOutputStream = new FileOutputStream(
						file);
				channelSftp.get(entry.getFilename(), fileOutputStream);
				downloadedFileList.add(file);
				
				logger.info("downloadedFileList======="+downloadedFileList);
				fileOutputStream.close();
				final DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
				final String fileName = entry.getFilename().replaceAll(".txt",
						"_" + df.format(new Date()) + ".txt");
				/*
				logger.info("source extension file name========="+entry.getFilename());
				Files.copy(file.toPath(),
						new File(destDir.concat("archive/" + fileName))
								.toPath());*/
				
				
				logger.info("destDir============"+destDir+"file============ "+file.getName());

				final String oldPath = sourceFilePath.concat("//").concat(
						entry.getFilename());
				
				
				/*channelSftp.rename(
						oldPath,
						archiveDir.concat(entry.getFilename() + "."
								+ System.currentTimeMillis()));
				logger.info("oldPath============"+oldPath);*/
				
				
			}
		} catch (final Exception ex) {
			logger.info("Error in downloading file from FTP Server" + ex+" "+session);
			ex.printStackTrace();
			throw new Exception(ex);
		}
		return downloadedFileList;
	}
	
	public void closeCOnnection(){
		
		if (channelSftp != null) {
			channelSftp.exit();
		}
		if (channel != null) {
			channel.disconnect();
		}
		if (session != null) {
			session.disconnect();
		}
		
	}
		
		
		
		
	
	
	
public void moveFiles(String Sourcefile,String destinationfie){
	
	try {
		
		String dest=destinationfie+"."+ System.currentTimeMillis();
		logger.info("in the try of move files");
		logger.info("source sftp file---------"+Sourcefile);
		logger.info("Moving sftp file destination = "+destinationfie);
		channelSftp.rename(	Sourcefile,dest );
		
		logger.info("**************sftp files moved successfully******************");
	} catch (SftpException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

}
	
	private void connectToFTP() throws Exception {
		try {
			final JSch jsch = new JSch();
			// Get Session
			logger.info("connectiong to sftp...........");
			session = jsch.getSession(userName, remoteAddress, remotePort);
			session.setPassword(password);
			final Properties config = new Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
		} catch (final Exception ex) {
			
			ex.printStackTrace();
			throw new Exception("Cannot connect to FTP Server");
		}

	}
	
	
	
	private void createSftpDir(final String path) throws SftpException {
		try {
			final SftpATTRS lstat = channelSftp.lstat(path);
			if (lstat == null || !lstat.isDir()) {
				logger.info("The directory " + path
						+ " doesn't exist. Creating...");
				channelSftp.mkdir(path);
				
				//logger.info("Created the directory : " + path+ " - Done");
				logger.info("Created the directory : " + path+ " - Done");
			}
		} catch (final SftpException e) {
			logger.info("The directory " + path
					+ " doesn't exist. Creating...");
			try {
				channelSftp.mkdir(path);
				logger.info("  the directory : " + path
						+ " - Done");
			} catch (final Exception e2) {
				logger.info(e2);
			}
		}
	}

}

