package com.velos.processor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.opencsv.CSVReader;
import com.velos.csv.init.CSVDataException;
import com.velos.csv.init.CSVHeaderException;
import com.velos.generic.dao.MessageDAO;

public class CSVFileReader {
	Map<String,List> modelmap=null;
	 boolean flag=false;
	List <String>errors=null;
	String create = null;
	Logger logger = Logger.getLogger(CSVFileReader.class);
	StringBuilder createQuery = null;
	MessageDAO dao;
	String csvdata = null;

	public CSVFileReader(MessageDAO dao) {

		this.dao = dao;
	}

	public Properties getConfigs(String FileName) {
		logger.info("property file name============= " + FileName);
		Properties properties = null;
		InputStream configs = null;
		if ("".equals(FileName) || FileName != null) {
			configs = this.getClass().getClassLoader()
					.getResourceAsStream(FileName + ".properties");
			properties = new Properties();

		}
		

		try {

			logger.info("configs object============ " + configs);
			if (configs != null)
				properties.load(configs);
			else
				throw new NullPointerException(
						"There is no  suitable configuration file");
		} catch (IOException e) {
			e.printStackTrace();
			logger.error(e.getCause());

		}
		return properties;
	}

	boolean completed = false;
	String headers[] = null;
	StringBuffer dataquery = null;

	StringBuilder newquery = null;
	StringBuilder finalQuery = null;

	public String[] getHeaders(File file, List<String> list)
			throws CSVHeaderException, IOException, CSVDataException {

		String[] nextLine;

		CSVReader reader = new CSVReader(new FileReader(file), ',', '"', 0);

		nextLine = reader.readNext();
modelmap=new HashMap();
modelmap.put("headers",Arrays.asList(nextLine));
		
		for(String header : nextLine){
			//logger.info("Header = "+header);
		}
		
		logger.info("Header size from XML ="+nextLine.length);
		logger.info("Header size from CONF ="+list.size());

		if (nextLine != null) {

			if (nextLine.length != list.size())
				throw new CSVDataException("the header count is excess/less");

			headers = new String[nextLine.length];

			for (int j = 0; j < nextLine.length; j++) {

				if ("".equals(nextLine[j]) || nextLine[j] == null) {

					throw new CSVHeaderException(
							"header value should not be null");
				}

				if (nextLine[j] != null && !"".equals(nextLine[j])) {

					if (list.contains(nextLine[j].trim())) {

						headers[j] = nextLine[j].trim();

					} else {
						throw new CSVHeaderException(
								"wrong header value-------->" + nextLine[j]);

					}

				}

			}

		}

		return headers;

	}

	public StringBuilder getData(String headers[], Map configmap,
			CSVReader reader, Properties conf, String insert,Map<String, List> errorsMap)
			throws IOException, CSVDataException, SQLException {

		 errors = new ArrayList<String>();

		List<String[]> csvdata = reader.readAll();

		finalQuery = new StringBuilder();

		String dbname = conf.getProperty("DB");

		logger.info("dbname============================" + dbname);
		logger.info("flag============================" + flag);

		if ((!"".equals(headers)) && (headers != null)) {
			List<String> headersList = Arrays.asList(headers);
			int k = 0;
			for (String arr[] : csvdata) {
				k++;
				//logger.info("|||||Final Query ="+finalQuery.toString());
				//logger.info("\n|||||Row = "+k);
				if (arr.length != headers.length)
					throw new CSVDataException(
							"  headers  are less or excess , send required values only");

				if (arr != null)
					if (arr.length != headersList.size())
						throw new CSVDataException(
								"headers count and values count not equal ,send equal no of values ");
				dataquery = new StringBuffer();
				if (arr != null) {

				}
				StringBuilder col = new StringBuilder();
				for (int i = 0; i < arr.length; i++) {
					String value = arr[i].trim();
					String mandatory = (String) configmap.get(headersList
							.get((i)));

					//logger.info("|||Value = "+value);
					
					if (mandatory != null && !"".equals(mandatory)) {

						if (i == 0
								&& (conf.getProperty("DB")
										.equalsIgnoreCase("postgres")))
							dataquery.append("(");
						if (mandatory.equalsIgnoreCase("y")
								&& (!"".equals(value) || value != null)) {

							dataquery.append("'"
									+ value.replaceAll("'", "`").trim() + "'");
							dataquery.append(",");
						}

						if (mandatory.equalsIgnoreCase("y")
								&& ("".equals(value) || value == null)) {

							//	errors.add("field is missing"+"Line number:" + k + " column name:" +value);
								flag=true;
								//modelmap.put("field is missing"+"Line number:" + k + " column name:" +headersList.get((i)), Arrays.asList(arr));
								errorsMap.put("field is missing "+" Line number:" + k + " column name:" +headersList.get((i)), Arrays.asList(arr));
								
								//	System.out.println("valueeeeeee column name-----"+headersList.get((i)));
							

						}

						if (mandatory.equalsIgnoreCase("N")
								&& ("".equals(value) || value == null)) {

							dataquery.append("'" + "null" + "'");
							dataquery.append(",");

						}

						if (mandatory.equalsIgnoreCase("N") && (value != null)
								&& (!"".equals(value))) {

							dataquery.append("'"
									+ value.replaceAll("'", "`").trim() + "'");
							dataquery.append(",");
						}

					}
					//logger.info("|||i="+i+"arr="+(arr.length-1));
					if (i == arr.length - 1) {
						//logger.info("|||||Data Query ="+dataquery);
						//logger.info("|||||flag ="+flag);
						if (flag==false) {
							
							
							
							if (conf.getProperty("DB").equalsIgnoreCase(
									"Oracle")) {
								
								
								
							
								//logger.info("|||||Data Query ="+dataquery.toString());
								
								dataquery.insert(0, insert);
								//logger.info("|||||Data Query ="+dataquery.toString());
								String ss = dataquery.substring(0,
										dataquery.length() - 1);

								//logger.info("|||||Data Query ="+ss);
								
								//logger.info("|||||Final Query ="+finalQuery.toString());
								finalQuery.append(ss);
								finalQuery.append(")");
								finalQuery.append("\n");
								//logger.info("|||||Final Query ="+finalQuery.toString());

							}
							
							

							else if (conf.getProperty("DB").equalsIgnoreCase(
									"postgres")) {

								//logger.info("|||||Data Query ="+dataquery.toString());
								
								String ss = dataquery.substring(0,
										dataquery.length() - 1);
								//logger.info("|||||Data Query ="+ss);
								finalQuery.append(ss);
								//logger.info("|||||Final Query ="+finalQuery.toString());
								finalQuery.append(")");
								finalQuery.append(",");
								//logger.info("|||||Final Query ="+finalQuery.toString());
								finalQuery.append("\n");
								//logger.info("|||||Final Query ="+finalQuery.toString());
							}
						}
						
						
						else
							flag=false;
						
					}
				}

			}
		}
		
		
		if (conf.getProperty("DB").equalsIgnoreCase("postgres"))
			return finalQuery.insert(0, insert);
		else
			return finalQuery;
	}

	public String getCreate() {

		return create;
	}

	public void getSiteID() {

	}

	public Map<String,List> readCSV(File file, String sourcePath, String fileName,Map<String, List> errorsMap)
			throws IOException, CSVHeaderException, CSVDataException {
		Map<String, String> map = new<String, String> HashMap();
		List<String> list = new ArrayList();
		String resultHeaders[] = null;
		List<String> dbFields = new ArrayList<String>();
		Properties conf = getConfigs(fileName);

		String dbname = conf.getProperty("DB");

		logger.info("dbname============================" + dbname);

		CSVReader reader = null;
		reader = new CSVReader(new FileReader(file), ',', '"', 1);

		Properties fields = getConfigs(conf.getProperty("field-config-file"));

		Map<String, String> dbcols = new HashMap<String, String>();

		for (String name : fields.stringPropertyNames()) {
			String values[] = fields.getProperty(name).split(",");
			map.put(values[0], values[1]);
			dbcols.put(values[0], name);
			// logger.info(" key field values=============" +
			// values[0]+values[1]);
			list.add(values[0].trim());
		}

		try {

			resultHeaders = getHeaders(file, list);

			logger.info("result headers"
					+ Arrays.asList(resultHeaders));

			StringBuilder sb = new StringBuilder();

			

			CSVReader readerH = new CSVReader(new FileReader(file), ',', '"', 0);

			String[] nextLine = readerH.readNext();

			
			for (String keys : nextLine) {
				//logger.info("|||header="+keys+"|||column="+dbcols.get(keys.trim()));
				sb.append(dbcols.get(keys.trim()));
				sb.append(",");

			}

			StringBuilder data = null;
			StringBuilder oracledata = null;

			create = sb.toString();
			//logger.info("||||Query = "+create);

			if (conf.getProperty("DB").equalsIgnoreCase("oracle")) {
				sb.insert(0, "into storefront(");
				//logger.info("||||Query = "+sb.toString());
				String s = sb.substring(0, sb.length() - 1);
				//logger.info("||||Query = "+s);
				s = s + ")values(";
				//logger.info("||||Query = "+s);
				oracledata = getData(resultHeaders, map, reader, conf, s,errorsMap);
				//logger.info("|||Query after getting data="+oracledata.toString());
				
				oracledata.insert(0, "insert all ");
				//logger.info("|||Query after getting data="+oracledata.toString());
				oracledata.append("select * from dual");
				//logger.info("|||Query after getting data="+oracledata.toString());
				csvdata = oracledata.toString();

			}
			if (conf.getProperty("DB").equalsIgnoreCase("postgres")) {

				sb.insert(0, "insert into storefront(");
				//logger.info("||||Query = "+sb.toString());
				String s = sb.substring(0, sb.length() - 1);
				//logger.info("||||Query = "+s);
				s = s + ")values";
				//logger.info("||||Query = "+s);
				data = getData(resultHeaders, map, reader, conf, s,errorsMap);
				//logger.info("|||Query after getting data="+data.toString());
				csvdata = data.substring(0, data.length() - 2);

			}

			logger.info("inserting data ...........");
			if(errors.size()>0){
				logger.info("||||||||Errore size = "+errors.size());
				//dao.insertData(csvdata, conf);
				
			}
			else 
				dao.insertData(csvdata, conf);
			

			logger.info("*******data inserted successfully*************");

		} catch (CSVDataException | CSVHeaderException | FileNotFoundException
				| SQLException | NullPointerException e) {
			e.printStackTrace();

			throw new CSVDataException(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			throw new CSVDataException(e.getMessage());

		}
		if(!(modelmap.size()==0)){
			return modelmap;
			
		}
		else return modelmap;
		
	}

}
