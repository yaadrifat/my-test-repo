package test;
import javax.xml.bind.annotation.XmlAttribute;

import javax.xml.bind.annotation.XmlElement;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Listbypis")
public class Listbypis {

	private String PI ;

	private String Key ;

	private String EresearchUser ;

	// Must have no-argument constructor

	public Listbypis() {

	}

	public Listbypis(String PI, String Key, String EresearchUser ) {

		this.PI = PI;

		this.Key = Key;

		this.EresearchUser = EresearchUser;

	}

	
	public String getPI() {
		return PI;
	}
	
	@XmlElement
	public void setPI(String pI) {
		PI = pI;
	}

	public String getKey() {
		return Key;
	}
	
	@XmlElement
	public void setKey(String key) {
		Key = key;
	}

	public String getEresearchUser() {
		return EresearchUser;
	}

	@XmlElement
	public void setEresearchUser(String eresearchUser) {
		EresearchUser = eresearchUser;
	}

	@Override
	public String toString() {

		return new StringBuffer(" PI : ").append(this.PI)

		.append(" Key : ").append(this.Key)

		.append(" EresearchUser : ").append(this.EresearchUser).toString();

	}

}
