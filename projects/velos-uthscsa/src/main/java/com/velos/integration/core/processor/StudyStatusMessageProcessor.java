package com.velos.integration.core.processor;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;

import org.apache.camel.CamelContext;
import org.apache.camel.ExchangePattern;
import org.apache.camel.ProducerTemplate;
import org.apache.cxf.message.MessageContentsList;
import org.apache.cxf.ws.security.wss4j.WSS4JOutInterceptor;
import org.apache.log4j.Logger;
import org.apache.ws.security.WSConstants;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.json.JSONObject;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import com.velos.integration.core.messaging.Change;
import com.velos.integration.core.messaging.CustomHandler;
import com.velos.integration.core.messaging.IdentifierConstants;
import com.velos.services.NvPair;
import com.velos.services.ObjectInfo;
import com.velos.services.OperationException_Exception;
import com.velos.services.SimpleIdentifier;
import com.velos.services.Study;
import com.velos.services.StudyIdentifier;
import com.velos.services.StudyStatus;

@Component
public class StudyStatusMessageProcessor implements
		VelosEspSystemAdministrationEndpoint, VelosEspGetStudyEndpoint {

	private static Logger logger = Logger.getLogger(StudyStatusMessageProcessor.class);
	
	private static final String externalAppKey = "e8ef34f0-b1e1-4bf6-9e28-63a76776781e";

	private Map<String, String> requestMap = new HashMap<String, String>();

	private ClassPathXmlApplicationContext clientContext;

	private Map<String, String> map = new HashMap<String, String>();

	private CamelContext context;

	public void setContext(CamelContext context) {
		this.context = context;
	}

	public void setClientContext(ClassPathXmlApplicationContext clientContext) {
		this.clientContext = clientContext;
	}

	private void configureCamel() {
		if (this.context != null) {
			return;
		}
		@SuppressWarnings("resource")
		ClassPathXmlApplicationContext springContext = new ClassPathXmlApplicationContext(
				"camel-config.xml");
		this.setContext((CamelContext) springContext.getBean("camel"));

		/*
		 * @SuppressWarnings("resource") ClassPathXmlApplicationContext
		 * clientContext = new ClassPathXmlApplicationContext(
		 * "velos-mdaclient-req.xml"); this.setClientContext(clientContext);
		 */
	}

	public void processStudyStatMessage(CustomHandler handler, String xml)
			throws OperationException_Exception {
		SimpleIdentifier simpleIdentifier = (SimpleIdentifier) handler
				.getParentOID().get(IdentifierConstants.SIMPLE_IDENTIFIER);
		List<Change> childHandlers = handler.getChanges();
		for (Change childHandler : childHandlers) {
			requestMap.put("childOID", childHandler.getIdentifier().getOID());
			requestMap.put("action", childHandler.getAction());
		}

		requestMap.put("OID", simpleIdentifier.getOID());
		try {
			this.configureCamel();
		} catch (Exception e) {
			e.printStackTrace();
		}

		studyRequest(requestMap, xml);
	}

	@Bean
	public WSS4JOutInterceptor wssInterceptor() {
		Properties prop = new Properties();
		try {
			prop.load(this.getClass().getClassLoader()
					.getResourceAsStream("velos-espclient-req.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}

		Map<String, Object> props = new HashMap<String, Object>();
		props.put(WSHandlerConstants.ACTION, WSHandlerConstants.USERNAME_TOKEN);
		props.put(WSHandlerConstants.PASSWORD_TYPE, WSConstants.PW_TEXT);
		props.put(WSHandlerConstants.USER, prop.getProperty("velos.userID"));
		props.put(WSHandlerConstants.PW_CALLBACK_CLASS,
				SimpleAuthCallbackHandler.class.getName());
		WSS4JOutInterceptor wssBean = new WSS4JOutInterceptor(props);
		return wssBean;
	}

	public void studyRequest(Map<String, String> requestMap, String xml)
			throws OperationException_Exception {

		String studyOID = requestMap.get("OID");
		System.out.println("Study OID : " + studyOID);
		String statusOID = requestMap.get("childOID");
		System.out.println("Study Status OID : " + statusOID);
		String action = requestMap.get("action");
		System.out.println("Change Action : " + action);

		// Call getStudy to get Study and Current Status information
		StudyIdentifier studyIdentifier = new StudyIdentifier();
		studyIdentifier.setOID(requestMap.get("OID"));
		Study study = getStudy(studyIdentifier);
		String studyNumber = study.getStudySummary().getStudyNumber();
		System.out.println("Study Number : "+ studyNumber);
		System.out.println("Study Title : " + study.getStudySummary().getStudyTitle());
		String orcaId = "";
		List<NvPair> msdList = study.getStudySummary().getMoreStudyDetails();
		if (msdList != null) {
			for (NvPair nvPair : msdList) {
				if ("orca_id".equals(nvPair.getKey())) {
					orcaId = String.valueOf(nvPair.getValue());
					break;
				}
			}
		}

		String statusCode = "";
		String statusDescription = "";
		for (StudyStatus studyStatus : study.getStudyStatuses().getStudyStatus()) {
			if (studyStatus.isCurrentForStudy()) {
				statusDescription = studyStatus.getStatus().getDescription();
				statusCode = studyStatus.getStatus().getCode();
				System.out.println("Study Status Description : " + statusDescription);
				System.out.println("Study Status Subtype : " + statusCode);
			}
		}

		JSONObject json = new JSONObject();
		try {
			json.put("Key", externalAppKey);
			json.put("ORCA_ID", orcaId);
			json.put("StudyNumber", studyNumber);
			json.put("Action", "notify");
			json.put("XML", xml);
		} catch (Exception e) {
			e.printStackTrace();
		}
		callExternal("updatebyids.json", json);
	}
	
    private void callExternal(String service, JSONObject input) {
		Properties prop = new Properties();
		try {
			prop.load(this.getClass().getClassLoader()
					.getResourceAsStream("velos-externalclient-req.properties"));
			String scheme = prop.getProperty("client.scheme");
			String host = prop.getProperty("client.host");
			String port = prop.getProperty("client.port");
			String context = prop.getProperty("client.context");
			String baseUrl = null;
			if (port != null && port.length() > 0) {
				baseUrl = scheme+"://"+host+":"+port+"/"+context;
			} else {
				baseUrl = scheme+"://"+host+"/"+context;
			}
			URL url = new URL(baseUrl+"/"+service);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			OutputStream os = conn.getOutputStream();
			os.write(input.toString().getBytes());
			os.flush();
			os.close();
			Scanner scanner;
			if (conn.getResponseCode() != 200) {
				scanner = new Scanner(conn.getErrorStream());
			} else {
				scanner = new Scanner(conn.getInputStream());
			}
			scanner.useDelimiter("\\Z");
			String reply = scanner.next();
			System.out.println("Received from external:\n"+reply);
			scanner.close();
			conn.disconnect();
		} catch (Exception e) {
			e.printStackTrace();
		}
    }

	@Override
	public ObjectInfo getObjectInfoFromOID(SimpleIdentifier arg0)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList list = (MessageContentsList) producer.sendBody(
				"cxf:bean:espSysAdminEndpoint", ExchangePattern.InOut, arg0);
		return (ObjectInfo) list.get(0);
	}

	@Override
	public Study getStudy(StudyIdentifier studyIdentifier)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList list = (MessageContentsList) producer.sendBody(
				"cxf:bean:espGetStudyEndpoint", ExchangePattern.InOut,
				studyIdentifier);
		return (Study) list.get(0);
	}

}
