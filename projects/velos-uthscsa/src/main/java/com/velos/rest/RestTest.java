package com.velos.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

@Path("/test")
public class RestTest {

	@GET
	@Path("/getter/{name}")
	public String getTest(@PathParam(value = "name") String name)
	{
		return "Hiiii " + name;
	}
}
