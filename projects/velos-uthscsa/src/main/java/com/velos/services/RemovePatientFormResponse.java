
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for removePatientFormResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="removePatientFormResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FormResponseIdentifier" type="{http://velos.com/services/}patientFormResponseIdentifier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "removePatientFormResponse", propOrder = {
    "formResponseIdentifier"
})
public class RemovePatientFormResponse {

    @XmlElement(name = "FormResponseIdentifier")
    protected PatientFormResponseIdentifier formResponseIdentifier;

    /**
     * Gets the value of the formResponseIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link PatientFormResponseIdentifier }
     *     
     */
    public PatientFormResponseIdentifier getFormResponseIdentifier() {
        return formResponseIdentifier;
    }

    /**
     * Sets the value of the formResponseIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientFormResponseIdentifier }
     *     
     */
    public void setFormResponseIdentifier(PatientFormResponseIdentifier value) {
        this.formResponseIdentifier = value;
    }

}
