package hl7test;

import java.util.Map;

public class HL7InterfaceProcessor {
	
	public void process(String tableName,Map resultMap)
	{
		//Call the insert insertMessageRecord() function from here. This will be your default behaviour for all the interfaces.
		//Any processor which needs this default behaviour doesnot need to override this process method.
		//But in case of ADT, we need to check if the message contains merge, than apply the pre processing logic, else directly call super.process(tableName, resultMap)
	}
}

