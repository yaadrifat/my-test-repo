package com.velos.integration.gateway;

import java.util.ArrayList;
import java.util.Map;

import com.velos.integration.mapping.ProtocolKeys;
import com.velos.integration.mapping.VelosKeys;

public class EnrollPatientRequestOutput {
	private static final String outputTemplate = 
			"<EnrollPatientRequestResponse xmlns:ns2=\"urn:ihe:qrph:rpe:2009\" xmlns=\"urn:hl7-org:v3\">"+
					"<patient>"+
					"<candidateID></candidateID>"+
					"<CandidateID root=\"8.3.5.6.7.78\" extension=\"%s\" assigningAuthorityName=\"%s\" />"+
					"<subjectID></subjectID>"+
					"<name><given>%s</given><family>%s</family></name>"+
					"<dob value=\"%s\"></dob>"+
					"</patient>"+
					"<processState>%s</processState>"+
					"<study><id root=\"8.3.5.6.7.78\" extension=\"%s\" assigningAuthorityName=\"%s\"/></study>"+
					"</EnrollPatientRequestResponse>";
	
	public String generateOutput(Map<VelosKeys, String> dataMap) {
		ArrayList<String> argList = new ArrayList<String>();
		argList.add(dataMap.get(ProtocolKeys.PatientId));
		argList.add(dataMap.get(ProtocolKeys.SiteName));
		argList.add(dataMap.get(ProtocolKeys.FirstName));
		argList.add(dataMap.get(ProtocolKeys.LastName));
		argList.add(dataMap.get(ProtocolKeys.Dob));
		argList.add(dataMap.get(ProtocolKeys.CompletedAction));
		argList.add(dataMap.get(ProtocolKeys.StudyNumber));
		argList.add(dataMap.get(ProtocolKeys.SiteName));
		return String.format(outputTemplate, argList.toArray());
	}
}
