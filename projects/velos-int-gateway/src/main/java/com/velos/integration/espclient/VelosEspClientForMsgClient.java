package com.velos.integration.espclient;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.integration.Message;
import org.springframework.integration.MessageChannel;
import org.springframework.integration.MessageHandlingException;
import org.springframework.integration.core.MessagingTemplate;
import org.springframework.integration.core.PollableChannel;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.stereotype.Component;
import org.springframework.ws.soap.SoapFaultDetail;
import org.springframework.ws.soap.SoapFaultDetailElement;
import org.springframework.ws.soap.client.SoapFaultClientException;

import com.velos.integration.mapping.EndpointKeys;
import com.velos.integration.mapping.ProtocolKeys;
import com.velos.integration.mapping.VelosKeys;
import com.velos.services.Code;
import com.velos.services.GetObjectInfoFromOIDResponse;
import com.velos.services.GetPatientStudyInfoResponse;
import com.velos.services.GetStudyPatientStatusHistoryResponse;
import com.velos.services.ObjectInfo;
import com.velos.services.PatientProtocolIdentifier;
import com.velos.services.PatientStudyStatusIdentifier;
import com.velos.services.StudyPatientStatus;
import com.velos.services.StudyPatientStatuses;

@Component
public class VelosEspClientForMsgClient {
	private static final HashMap<String, String> patStudyStatusHash;
	static
    {
		patStudyStatusHash = new HashMap<String, String>();
		patStudyStatusHash.put("enrolled", "PROCESS_ACTIVE");
		patStudyStatusHash.put("scrfail", "PROCESS_SUSPENDED");
		patStudyStatusHash.put("sComplete", "PROCESS_COMPLETED");
		patStudyStatusHash.put("offstudy", "PROCESS_EXITED");
		patStudyStatusHash.put("offtreat", "ACTIVITY_EXITED");
    }

	private static final String studyRequestTemplate =
		"<ser:getStudySummary xmlns:ser=\"http://velos.com/services/\">"
		+	"<StudyIdentifier>" 
		+	"   <PK>%s</PK>"
		+	"</StudyIdentifier>"
		+ "</ser:getStudySummary>";

	private static final String SysAdminObjectInfoRequestTemplate =
		"<ser:getObjectInfoFromOID xmlns:ser=\"http://velos.com/services/\">"
		+	"<arg0>"
		+ 		"<OID>%s</OID>"
		+		"<PK>%s</PK>"
		+ 	"</arg0>"
		+ "</ser:getObjectInfoFromOID>";
	
	private static final String patStudyInfoRequestTemplate =
			"<ser:getPatientStudyInfo xmlns:ser=\"http://velos.com/services/\">"
			+ 	"<patProtId>%s</patProtId>"
			+ "</ser:getPatientStudyInfo>";

	private static final String patStudyStatusHistoryRequestTemplate =
			"<ser:getStudyPatientStatusHistory xmlns:ser=\"http://velos.com/services/\">"
			+	"<StudyIdentifier>"
			+		"<PK>%s</PK>"
			+	"</StudyIdentifier>"
			+	"<PatientIdentifier>"
			+		"<PK>%s</PK>"
			+	"</PatientIdentifier>"
			+"</ser:getStudyPatientStatusHistory>";
    
	
	public static void main(String[] args) {
		System.out.println("Hello World");
		
		/*Map<VelosKeys, String> requestMap = new HashMap<VelosKeys, String>();
		requestMap.put(ProtocolKeys.OID, "589faa21-f257-409b-a51f-0eb0623d0c88");
		requestMap.put(ProtocolKeys.PK, "");

		Map<VelosKeys, String> outputMap = new HashMap<VelosKeys, String>();
		VelosEspClientForMsgClient client = new VelosEspClientForMsgClient();
		client.testPatStudyRequest(requestMap);*/
	}

	public Message<?> handleRequest(VelosEspMethods method, Map<VelosKeys, String> requestMap)  {
    	if (requestMap == null) { return null; }
		MessageChannel channel = null;
		String body = null;
        @SuppressWarnings("resource")
		ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("velos-espclient-req.xml");
        // Call Velos WS
		switch(method) {
		case SysAdminGetObjectInfoFromOID: 
			channel = context.getBean("sysAdminRequests", MessageChannel.class);
			body = String.format(SysAdminObjectInfoRequestTemplate, requestMap.get(ProtocolKeys.OID), requestMap.get(ProtocolKeys.PK));
			break;
		case PatientStudyGetPatientStudyInfo:
			channel = context.getBean("patientStudyRequests", MessageChannel.class);
			body = String.format(patStudyInfoRequestTemplate, requestMap.get(ProtocolKeys.PatProtId));
			break;
		case PatientStudyGetStudyPatientStatusHistory:
			channel = context.getBean("studyPatientRequests", MessageChannel.class);
			body = String.format(patStudyStatusHistoryRequestTemplate, requestMap.get(ProtocolKeys.StudyId), requestMap.get(ProtocolKeys.PatientId));
			break;
		case StudyGetStudySummary:
	        channel = context.getBean("studyRequests", MessageChannel.class);
	        body = String.format(studyRequestTemplate, requestMap.get(ProtocolKeys.StudyNumber));
			break;
		default:
			break;
		}
        MessagingTemplate messagingTemplate = new MessagingTemplate();
        Message<?> message = null;
        try {
        	message = messagingTemplate.sendAndReceive(
                channel, MessageBuilder.withPayload(body).build());
        } catch(MessageHandlingException e) {
        	message = extractOperationException(e);
        }
        
        System.out.println("Message received-" + message.getPayload());

        // Transform XML to object to Map
    	//return transformMessageToMap(message, requestMap.get(EndpointKeys.Endpoint));
        return message;
	}
	
	private final TransformerFactory tfactory = TransformerFactory.newInstance();
	
	private Message<?> extractOperationException(MessageHandlingException messageException) {
    	SoapFaultClientException se = (SoapFaultClientException) messageException.getCause();
    	SoapFaultDetail detail = se.getSoapFault().getFaultDetail();
    	SoapFaultDetailElement elem = detail.getDetailEntries().next();
    	Source source = elem.getSource();
    	StringWriter writer = new StringWriter();
        Result result = new StreamResult(writer);
        Transformer xform = null;
        try {
        	xform = tfactory.newTransformer();
            xform.transform(source, result);
        } catch(Exception e1) {
        	e1.printStackTrace();
        }
        System.out.println(writer.toString());
        Message<?> message = MessageBuilder.withPayload(writer.toString()).build();
		return message;
	}
	
	/*
	private Map<VelosKeys, Object> transformMessageToMap(Message<?> message, String endpoint) {
        @SuppressWarnings("resource")
		ClassPathXmlApplicationContext context = 
				new ClassPathXmlApplicationContext("velos-espclient-resp.xml");
        MessageChannel channelXml = context.getBean("input-xml", MessageChannel.class);
        PollableChannel output = context.getBean("output", PollableChannel.class);
        
        // Transform XML to object
        channelXml.send(message);
        Message<?> reply = output.receive();
        
        // Transform object to map
        VelosMessageHandler messageHandler = context.getBean(VelosMessageHandler.class);
        messageHandler.setEndpoint(endpoint);
        messageHandler.handleMessage(reply);
        return messageHandler.getDataMap();
    }
	 */

	private Object transformMessageToObject(Message<?> message, String endpoint) {
        @SuppressWarnings("resource")
		ClassPathXmlApplicationContext context = 
				new ClassPathXmlApplicationContext("velos-espclient-resp.xml");
        MessageChannel channelXml = context.getBean("input-xml", MessageChannel.class);
        PollableChannel output = context.getBean("output", PollableChannel.class);
        
        // Transform XML to object
        channelXml.send(message);
        Message<?> reply = output.receive();
        return reply.getPayload();
    }
	
 	public Map<VelosKeys, Object> testPatStudyRequest(Map<VelosKeys, String> requestMap) {
    	if (requestMap == null) { return null; }
    	
        // Call Velos WS
    	Message<?> message = handleRequest(VelosEspMethods.SysAdminGetObjectInfoFromOID, requestMap);

    	Integer patProtId = null;
    	Object obj = transformMessageToObject(message, EndpointKeys.Epic.toString());
        if (obj instanceof GetObjectInfoFromOIDResponse){
        	ObjectInfo objInfo = ((GetObjectInfoFromOIDResponse) obj).getObjectInfo();
        	patProtId = objInfo.getTablePk();
        }
    	
   	    // Call PatientStudyService here
   	    Map<VelosKeys, Object> outputMap = new HashMap<VelosKeys, Object>();
   	    if (null ==  patProtId) { return outputMap; }
   	    
   	    Map<VelosKeys, String> patStudyInfoReqMap = new HashMap<VelosKeys, String>();
   	    patStudyInfoReqMap.put(ProtocolKeys.PatProtId, patProtId.toString());
   	    Message<?> patStudyInfoMessage = handleRequest(VelosEspMethods.PatientStudyGetPatientStudyInfo, patStudyInfoReqMap);
   	    
   	    Integer studyId = null;
   	    Integer patientId = null;
   	    obj = transformMessageToObject(patStudyInfoMessage, EndpointKeys.Epic.toString());
   	    if (obj instanceof GetPatientStudyInfoResponse) {
   	    	PatientProtocolIdentifier patientProtocolIdentifier = ((GetPatientStudyInfoResponse) obj).getPatientStudy();
   	    	studyId = patientProtocolIdentifier.getStudyIdentifier().getPK();
   	    	System.out.println("Patient Study studyId= " + studyId);

   	    	patientId = patientProtocolIdentifier.getPatientIdentifier().getPK();
   	    	System.out.println("Patient Study patientId= " + patientId);
   	    }
  	    
   	    if (null == studyId || null == patientId) { return outputMap; }
   	    
   	    Map<VelosKeys, String> patStudyHxReqMap = new HashMap<VelosKeys, String>();
   	    patStudyHxReqMap.put(ProtocolKeys.StudyId, studyId.toString());
   	    patStudyHxReqMap.put(ProtocolKeys.PatientId, patientId.toString());

   	    Message<?> patStudyHxMessage = handleRequest(VelosEspMethods.PatientStudyGetStudyPatientStatusHistory, patStudyHxReqMap);

   	    obj = transformMessageToObject(patStudyHxMessage, EndpointKeys.Epic.toString());
   	    if (obj instanceof GetStudyPatientStatusHistoryResponse){
   	    	StudyPatientStatuses studyPatientStatuses = ((GetStudyPatientStatusHistoryResponse) obj).getPatientStudyStatuses();
   	    	if (null == studyPatientStatuses){
   	    		System.out.println("Patient not yet Enrolled on study");
   	    	}

   	    	List<StudyPatientStatus> studyPatientStatusList = new ArrayList<StudyPatientStatus>();
   	    	studyPatientStatusList = studyPatientStatuses.getStudyPatientStatus();

   	    	for (StudyPatientStatus studyPatientStatus : studyPatientStatusList){
   	    		if (studyPatientStatus.isCurrentStatus()){
   	    			System.out.println("I found a current Patient Study Status");
   	    			PatientStudyStatusIdentifier  patientStudyStatusIdentifier = studyPatientStatus.getStudyPatStatId();
   	    			System.out.println("Patient Study Status PK: "+ patientStudyStatusIdentifier.getPK());
   	    			Code patStudyStatusCode = studyPatientStatus.getStudyPatStatus();

   	    			System.out.println("Patient Study Status: "+ patStudyStatusCode.getDescription());
   	    			System.out.println("Patient Study Status subtype: "+ patStudyStatusCode.getCode());
   	    			System.out.println("Patient Study Status Date: "+ studyPatientStatus.getStatusDate());

   	    			break;
   	    		}
   	    	}

   	    }
   	    
    	return outputMap;
    }
}
