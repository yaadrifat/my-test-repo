/*
 * Copyright 2002-2011 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.velos.integration.gateway;

import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.xml.source.DomSourceFactory;
import org.springframework.stereotype.Component;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.velos.integration.espclient.VelosEspClient;
import com.velos.integration.espclient.VelosEspMethods;
import com.velos.integration.mapping.EndpointKeys;
import com.velos.integration.mapping.EpicMaps;
import com.velos.integration.mapping.MapUtil;
import com.velos.integration.mapping.ProtocolKeys;
import com.velos.integration.mapping.VelosKeys;

import javax.xml.soap.Detail;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;
import javax.xml.ws.soap.SOAPFaultException;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ProtocolEndpoint {
	private static final String HARDCODE_ROOT = "1.3.6.1.4.1.12559.11.1.4.1.2";
	private static final String ALREADY_ENROLLED_MSG = "Patient is already enrolled to this Study";
	private static final String ALREADY_ASSOC_MSG = "Patient is already linked to this study";
	private static final String EMPTY_STR = "";
	private static final String CREATED_STR = "CREATED";
	private static final String ENROLL_PENDING_STR = "enroll_pending";
    
    @ServiceActivator
    public Source handleRequest(DOMSource source)
            throws Exception {
    	
    	String endpoint = EndpointKeys.Epic.toString();
    	String localName = source.getNode().getLocalName();

        if (ProtocolKeys.RetrieveProtocol.toString().equals(localName)) {
        	return handleRetrieveProtocol(source, endpoint);
        } else if (ProtocolKeys.RetrievePatient.toString().equals(localName)) {
        	return handleRetrievePatient(source, endpoint);
        } else if (ProtocolKeys.EnrollPatientRequest.toString().equals(localName)) {
        	return handleEnrollPatient(source, endpoint);
        } else if (ProtocolKeys.AlertProtocolState.toString().equals(localName)) {
        	return handleAddStudyPatientStatus(source, endpoint);
        }
        
        return null;
    }
    
    private Source handleEnrollPatient(DOMSource source, String endpoint) throws Exception {
    	Map<VelosKeys, String> requestMap = new HashMap<VelosKeys, String>();
    	requestMap.put(EndpointKeys.Endpoint, endpoint);
    	NodeList nodeList = source.getNode().getChildNodes();
    	
    	for (int topX = 0; topX < nodeList.getLength(); topX++) {
    		Node node = nodeList.item(topX);
    		if (node.getLocalName() == null) { continue; }
    		if (ProtocolKeys.Patient.toString().equals(node.getLocalName())) {
    			NodeList childNodeList = node.getChildNodes();
    			for (int cX = 0; cX < childNodeList.getLength(); cX++) {
    				Node childNode = childNodeList.item(cX);
    				if (ProtocolKeys.CandidateIdUpper.toString().equals(childNode.getLocalName())) {
    					NamedNodeMap attributeMap = childNode.getAttributes();
    					if (attributeMap.getNamedItem(ProtocolKeys.Extension.toString()) != null) {
    						Node extension = attributeMap.getNamedItem(ProtocolKeys.Extension.toString()); 
    						requestMap.put(ProtocolKeys.PatientID, extension.getNodeValue());
    						System.out.println("patient Id is "+requestMap.get(ProtocolKeys.PatientID));
    					}
    				} else if (ProtocolKeys.Name.toString().equals(childNode.getLocalName())) {
    					NodeList grandChildNodeList = childNode.getChildNodes();
    					for (int gX = 0; gX < grandChildNodeList.getLength(); gX++) {
    						Node grandChildNode = grandChildNodeList.item(gX);
    						if (ProtocolKeys.Family.toString().equals(grandChildNode.getLocalName())) {
    							requestMap.put(ProtocolKeys.LastName, grandChildNode.getTextContent());
    							System.out.println("last name is "+requestMap.get(ProtocolKeys.LastName));
    						} else if (ProtocolKeys.Given.toString().equals(grandChildNode.getLocalName())) {
    							requestMap.put(ProtocolKeys.FirstName, grandChildNode.getTextContent());
    							System.out.println("first name is "+requestMap.get(ProtocolKeys.FirstName));
    						}
    					}
    				} else if (ProtocolKeys.Dob.toString().toLowerCase().equals(childNode.getLocalName())) {
    					NamedNodeMap attributeMap = childNode.getAttributes();
    					String dob = MapUtil.formatDateOfBirth(attributeMap.getNamedItem(
    							ProtocolKeys.Value.toString()).getNodeValue());
    					if (dob != null) {
    						requestMap.put(ProtocolKeys.Dob, dob);
    						System.out.println("DOB is "+requestMap.get(ProtocolKeys.Dob));
    					}
    				}
    			}
    		} else if (ProtocolKeys.Study.toString().equals(node.getLocalName())) {
    			
    			NodeList childNodeList0 = node.getChildNodes();
    			
    			for (int cX0 = 0; cX0 < childNodeList0.getLength(); cX0++) {
    				
    				Node childNode0 = childNodeList0.item(cX0);
    				
    				if (ProtocolKeys.Instantiation.toString().equals(childNode0.getLocalName())) {
    					
    					NodeList childNodeList1 = childNode0.getChildNodes();
    					
    					for (int cX1 = 0; cX1 < childNodeList1.getLength(); cX1++) {
    	    				
    						Node childNode1 = childNodeList1.item(cX1);
    					
    	    				if (ProtocolKeys.PlannedStudy.toString().equals(childNode1.getLocalName())) {
    	    					
    	    					NodeList childNodeList2 = childNode1.getChildNodes();
    	    					
    	    					for (int cX2 = 0; cX2 < childNodeList2.getLength(); cX2++) {
    	    	    				
    	    						Node childNode3 = childNodeList2.item(cX2);
    	    	    				
    	    						if (ProtocolKeys.Id.toString().equals(childNode3.getLocalName())) {
    	    							NamedNodeMap attributeMap = childNode3.getAttributes();
    	    						   	    	    				
    	    							if (attributeMap.getNamedItem(ProtocolKeys.Extension.toString()) != null) {
    	    								Node extension = attributeMap.getNamedItem(ProtocolKeys.Extension.toString());
    	    								requestMap.put(ProtocolKeys.StudyNumber, extension.getNodeValue());
    	    								System.out.println("Study number is "+requestMap.get(ProtocolKeys.StudyNumber));
    	    							}	}
    	    					}
    	    				}	
    					}
    				}
    			}			
    			/*if (attributeMap.getNamedItem(
    							ProtocolKeys.AssigningAuthorityName.toString()) != null) {
    						Node siteName = attributeMap.getNamedItem(ProtocolKeys.AssigningAuthorityName.toString());
    						requestMap.put(ProtocolKeys.SiteName, siteName.getNodeValue());
    						System.out.println("Org name is "+requestMap.get(ProtocolKeys.SiteName));
    					}*/
    		}
    	}
    	
        // Call Velos eSP
        VelosEspClient client = new VelosEspClient();
    	EnrollPatientRequestOutput output = new EnrollPatientRequestOutput();
    	Map<VelosKeys, String> resultMap = new HashMap<VelosKeys, String>();
    	
    	// search for patient by patient ID, First name, Last name, and DOB in request match with those in search result
    	Map<VelosKeys, Object> dataMap = client.handleRequest(VelosEspMethods.PatDemogSearchPatient, requestMap);
    	if (dataMap.get(ProtocolKeys.FaultString) != null) {
    		throw createSOAPFaultException(dataMap);
    	}
    	resultMap.put(ProtocolKeys.SiteName, requestMap.get(ProtocolKeys.SiteName));
    	resultMap.put(ProtocolKeys.StudyNumber, requestMap.get(ProtocolKeys.StudyNumber));
    	resultMap.put(ProtocolKeys.PatientId, (String)dataMap.get(ProtocolKeys.PatientID));
    	resultMap.put(ProtocolKeys.FirstName, (String)dataMap.get(ProtocolKeys.FirstName));
    	resultMap.put(ProtocolKeys.LastName, (String)dataMap.get(ProtocolKeys.LastName));
    	String dob = MapUtil.nullToEmpty((String)dataMap.get(ProtocolKeys.Dob));
    	resultMap.put(ProtocolKeys.Dob, dob.replaceAll("/", EMPTY_STR));
    	
    	// call enroll patient to study
    	requestMap.put(ProtocolKeys.PatientId, (String)dataMap.get(ProtocolKeys.PatientID)); // ID to Id
    	dataMap = client.handleRequest(VelosEspMethods.StudyPatEnrollPatientToStudy, requestMap);
    	if (dataMap.get(ProtocolKeys.FaultString) != null) {
    		if (ALREADY_ENROLLED_MSG.equals(dataMap.get(ProtocolKeys.FaultString))) {
    			dataMap.put(ProtocolKeys.FaultString, ALREADY_ASSOC_MSG);
    		}
    		throw createSOAPFaultException(dataMap);
    	}
    	String completedAction = (String)dataMap.get(ProtocolKeys.CompletedAction);
    	if (completedAction != null) { completedAction = ENROLL_PENDING_STR; }
    	resultMap.put(ProtocolKeys.CompletedAction, MapUtil.nullToEmpty(completedAction));
    	String xml = output.generateOutput(resultMap);
    	return new DomSourceFactory().createSource(xml);
    }
    
    private Source handleAddStudyPatientStatus(DOMSource source, String endpoint) throws Exception {
    	Map<VelosKeys, String> requestMap = new HashMap<VelosKeys, String>();
    	requestMap.put(EndpointKeys.Endpoint, endpoint);
    	NodeList nodeList = source.getNode().getChildNodes();
    	
    	for (int topX = 0; topX < nodeList.getLength(); topX++) {
    		Node node = nodeList.item(topX);
    		if (node.getLocalName() == null) { continue; }
    		if (ProtocolKeys.Patient.toString().equals(node.getLocalName())) {
    			NodeList childNodeList = node.getChildNodes();
    			for (int cX = 0; cX < childNodeList.getLength(); cX++) {
    				Node childNode = childNodeList.item(cX);
    				if (ProtocolKeys.CandidateId.toString().equals(childNode.getLocalName())) {
    					NamedNodeMap attributeMap = childNode.getAttributes();
    					if (attributeMap.getNamedItem(ProtocolKeys.Root.toString()) != null) {
    						if (HARDCODE_ROOT.equals(attributeMap.getNamedItem(ProtocolKeys.Root.toString()).getTextContent())){
	    						if (attributeMap.getNamedItem(ProtocolKeys.Extension.toString()) != null) {
	        						Node extension = attributeMap.getNamedItem(ProtocolKeys.Extension.toString()); 
	        						requestMap.put(ProtocolKeys.PatientID, extension.getNodeValue());
	        						System.out.println("patient Id is "+requestMap.get(ProtocolKeys.PatientID));
	        					}
    						}
    					}
    				} else if (ProtocolKeys.Name.toString().equals(childNode.getLocalName())) {
    					NodeList grandChildNodeList = childNode.getChildNodes();
    					for (int gX = 0; gX < grandChildNodeList.getLength(); gX++) {
    						Node grandChildNode = grandChildNodeList.item(gX);
    						if (ProtocolKeys.Family.toString().equals(grandChildNode.getLocalName())) {
    							requestMap.put(ProtocolKeys.LastName, grandChildNode.getTextContent());
    							System.out.println("last name is "+requestMap.get(ProtocolKeys.LastName));
    						} else if (ProtocolKeys.Given.toString().equals(grandChildNode.getLocalName())) {
    							requestMap.put(ProtocolKeys.FirstName, grandChildNode.getTextContent());
    							System.out.println("first name is "+requestMap.get(ProtocolKeys.FirstName));
    						}
    					}
    				} else if (ProtocolKeys.Dob.toString().toLowerCase().equals(childNode.getLocalName())) {
    					NamedNodeMap attributeMap = childNode.getAttributes();
    					String dob = MapUtil.formatDateOfBirth(attributeMap.getNamedItem(
    							ProtocolKeys.Value.toString()).getNodeValue());
    					if (dob != null) {
    						requestMap.put(ProtocolKeys.Dob, dob);
    						System.out.println("DOB is "+requestMap.get(ProtocolKeys.Dob));
    					}
    				}
    			}
    		} else if (ProtocolKeys.Study.toString().equals(node.getLocalName())) {
    			
    			NodeList childNodeList0 = node.getChildNodes();
    			
    			for (int cX0 = 0; cX0 < childNodeList0.getLength(); cX0++) {
    				
    				Node childNode0 = childNodeList0.item(cX0);
    				
    				if (ProtocolKeys.Instantiation.toString().equals(childNode0.getLocalName())) {
    					
    					NodeList childNodeList1 = childNode0.getChildNodes();
    					
    					for (int cX1 = 0; cX1 < childNodeList1.getLength(); cX1++) {
    	    				
    						Node childNode1 = childNodeList1.item(cX1);
    					
    	    				if (ProtocolKeys.PlannedStudy.toString().equals(childNode1.getLocalName())) {
    	    					
    	    					NodeList childNodeList2 = childNode1.getChildNodes();
    	    					
    	    					for (int cX2 = 0; cX2 < childNodeList2.getLength(); cX2++) {
    	    	    				
    	    						Node childNode3 = childNodeList2.item(cX2);
    	    	    				
    	    						if (ProtocolKeys.Id.toString().equals(childNode3.getLocalName())) {
    	    							NamedNodeMap attributeMap = childNode3.getAttributes();
    	    						   	    	    				
    	    							if (attributeMap.getNamedItem(ProtocolKeys.Extension.toString()) != null) {
    	    								Node extension = attributeMap.getNamedItem(ProtocolKeys.Extension.toString());
    	    								requestMap.put(ProtocolKeys.StudyNumber, extension.getNodeValue());
    	    								System.out.println("Study number is "+requestMap.get(ProtocolKeys.StudyNumber));
    	    							}	}
    	    					}
    	    				}	
    					}
    				}
    			}
    		} else if (ProtocolKeys.ProcessState.toString().equals(node.getLocalName())) {
    			String patStudyStat = EpicMaps.getEpicToVelosPatStudyStatMap().get(node.getFirstChild().getTextContent());
    			requestMap.put(ProtocolKeys.StudyPatStatus, patStudyStat);
    			System.out.println("New PatStudyStat is "+requestMap.get(ProtocolKeys.StudyPatStatus));
    		}
    	}
    	
        // Call Velos eSP
        VelosEspClient client = new VelosEspClient();
        AlertProtocolStateOutput output = new AlertProtocolStateOutput();
    	Map<VelosKeys, String> resultMap = new HashMap<VelosKeys, String>();
    	
    	// search for patient by patient ID, First name, Last name, and DOB in request match with those in search result
    	Map<VelosKeys, Object> dataMap = client.handleRequest(VelosEspMethods.PatDemogSearchPatient, requestMap);
    	if (dataMap.get(ProtocolKeys.FaultString) != null) {
    		throw createSOAPFaultException(dataMap);
    	}
    	resultMap.put(ProtocolKeys.SiteName, requestMap.get(ProtocolKeys.SiteName));
    	resultMap.put(ProtocolKeys.StudyNumber, requestMap.get(ProtocolKeys.StudyNumber));
    	resultMap.put(ProtocolKeys.PatientId, (String)dataMap.get(ProtocolKeys.PatientID));
    	resultMap.put(ProtocolKeys.FirstName, (String)dataMap.get(ProtocolKeys.FirstName));
    	resultMap.put(ProtocolKeys.LastName, (String)dataMap.get(ProtocolKeys.LastName));
    	String dob = MapUtil.nullToEmpty((String)dataMap.get(ProtocolKeys.Dob));
    	resultMap.put(ProtocolKeys.Dob, dob.replaceAll("/", EMPTY_STR));
    	
    	// call Add patient study status
    	requestMap.put(ProtocolKeys.PatientId, (String)dataMap.get(ProtocolKeys.PatientID)); // ID to Id
    	dataMap = client.handleRequest(VelosEspMethods.StudyPatAddStudyPatientStatus, requestMap);
    	if (dataMap.get(ProtocolKeys.FaultString) != null) {
    		if (ALREADY_ENROLLED_MSG.equals(dataMap.get(ProtocolKeys.FaultString))) {
    			dataMap.put(ProtocolKeys.FaultString, ALREADY_ASSOC_MSG);
    		}
    		throw createSOAPFaultException(dataMap);
    	}
    	String completedAction = (String)dataMap.get(ProtocolKeys.CompletedAction);
    	if (completedAction != null) { completedAction = requestMap.get(ProtocolKeys.StudyPatStatus); }
    	resultMap.put(ProtocolKeys.CompletedAction, MapUtil.nullToEmpty(completedAction));
    	String xml = output.generateOutput(resultMap);
    	return new DomSourceFactory().createSource(xml);
    }
    private SOAPFaultException createSOAPFaultException(Map<VelosKeys, Object> dataMap) {
        SOAPFaultException sfe = null;
        MessageFactory messageFactory;
		try {
			messageFactory = MessageFactory.newInstance();
	        SOAPMessage message = messageFactory.createMessage();
	        SOAPFault soapFault = message.getSOAPBody().addFault();
	        SOAPPart sp = message.getSOAPPart();
	        SOAPEnvelope se = sp.getEnvelope();
	        Name qname = se.createName("Client", null, SOAPConstants.URI_NS_SOAP_1_2_ENVELOPE);
	        soapFault.setFaultCode(qname);
	        soapFault.setFaultString((String)dataMap.get(ProtocolKeys.FaultString));
	        Detail detail = soapFault.addDetail();
	        detail.setTextContent((String)dataMap.get(ProtocolKeys.FaultString));
	        sfe = new SOAPFaultException(soapFault);
		} catch (SOAPException e) {
			e.printStackTrace();
		}
		return sfe;
    }

    private Source handleRetrievePatient(DOMSource source, String endpoint) throws Exception {
        Map<VelosKeys, String> requestMap = new HashMap<VelosKeys, String>();
        requestMap.put(EndpointKeys.Endpoint, endpoint);
        
        System.out.println("Implement get study patients here");
        
//        Map<VelosKeys, Object> dataMap = new HashMap<VelosKeys, Object>();
//        dataMap.put(ProtocolKeys.FaultString, "study not found");
//        if (1 == (2 - 1)) throw createSOAPFaultException(dataMap);
        
        String xml = "<RetrievePatientRequestResponse />";
        return new DomSourceFactory().createSource(xml);
    }
    
    private Source handleRetrieveProtocol(DOMSource source, String endpoint) throws Exception {
        Map<VelosKeys, String> requestMap = new HashMap<VelosKeys, String>();
        requestMap.put(EndpointKeys.Endpoint, endpoint);
        
        // Use this block if the input data is in the attributes
      /*  NamedNodeMap attributeMap = source.getNode().getAttributes();
        if (attributeMap.getNamedItem(ProtocolKeys.Extension.toString()) != null) {
        	Node extension = attributeMap.getNamedItem(ProtocolKeys.Extension.toString());
        	requestMap.put(ProtocolKeys.StudyNumber, extension.getNodeValue());
        }
*/        
        
        // Use this block if the input data is in the child node
        NodeList nodeList = source.getNode().getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            if (ProtocolKeys.Query.toString().equals(node.getLocalName())) {
            	Node studyNumber = node.getAttributes().getNamedItem(ProtocolKeys.Extension.toString());
            	requestMap.put(ProtocolKeys.StudyNumber, studyNumber.getNodeValue());
            } else if (ProtocolKeys.Description.toString().equals(node.getLocalName())) {
            	requestMap.put(ProtocolKeys.Description, node.getFirstChild().getNodeValue());
            }
        }
        

        RetrieveProtocolDefOutput output = new RetrieveProtocolDefOutput();

        // Call Velos eSP
        VelosEspClient client = new VelosEspClient();
        Map<VelosKeys, Object> studyDataMap = client.handleStudyRequest(requestMap);
        Map<VelosKeys, Object> studyCalendarListDataMap = client.handleStudyCalendarListRequest(requestMap);
        
        if (studyDataMap.get(ProtocolKeys.FaultString) != null) {
    		throw createSOAPFaultException(studyDataMap);
		} else {

			@SuppressWarnings("unchecked")
			List<Map<VelosKeys, Object>> calendarIdList = (List<Map<VelosKeys, Object>>) studyCalendarListDataMap
					.get(ProtocolKeys.CalendarIdList);
			StringBuffer calendarSB = new StringBuffer();

			for (Map<VelosKeys, Object> calendarMap : calendarIdList) {
				requestMap.put(ProtocolKeys.CalendarId, String
						.valueOf(calendarMap.get(ProtocolKeys.CalendarId)));
				Map<VelosKeys, Object> calendarDataMap = client
						.handleStudyCalendarRequest(requestMap);
				@SuppressWarnings("unchecked")
				List<Map<VelosKeys, Object>> visitList = (List<Map<VelosKeys, Object>>) calendarDataMap
						.get(ProtocolKeys.VisitList);
				calendarMap.put(ProtocolKeys.CalendarDuration, calendarDataMap.get(ProtocolKeys.CalendarDuration));
				calendarMap.put(ProtocolKeys.CalendarDurUnit, calendarDataMap.get(ProtocolKeys.CalendarDurUnit));
				StringBuffer visitSB = new StringBuffer();
				StringBuffer visitForPart1SubSB = new StringBuffer();
				int visitSeq = 1;
				// First loop through visits to determine whether or not it is a day-zero calendar
				boolean isDayZeroCal = false;
				for (Map<VelosKeys, Object> visitMap : visitList) {
					if ((Integer) visitMap.get(ProtocolKeys.VisitDisplacement) == 0) {
						isDayZeroCal = true;
						break;
					}
				}
				for (Map<VelosKeys, Object> visitMap : visitList) {
					visitMap.put(ProtocolKeys.CalendarName,
							calendarMap.get(ProtocolKeys.CalendarName));
					int displacement = (Integer) visitMap.get(ProtocolKeys.VisitDisplacement);
					if (!isDayZeroCal) { --displacement; }
					Date displacementDate = addDays(getReferenceDate(), displacement);
					int visitWindowBefore = (Integer) visitMap.get(ProtocolKeys.VisitWindowBefore);
					int visitWindowAfter = (Integer) visitMap.get(ProtocolKeys.VisitWindowAfter);
					visitMap.put(
							ProtocolKeys.VisitWindowBeforeFormatted,
							formatDate(addDays(displacementDate, -1 * visitWindowBefore)));
					visitMap.put(
							ProtocolKeys.VisitWindowAfterFormatted,
							formatDate(addDays(displacementDate, visitWindowAfter)));
					visitMap.put(
							ProtocolKeys.VisitDisplacementFormatted,
							formatDate(addDays(getReferenceDate(), displacement)));
					visitSB.append(output.generateComponent4Part2(visitMap));
					visitForPart1SubSB.append(output
							.generateComponent4Part1SubTemplate(
									(String) calendarMap
											.get(ProtocolKeys.CalendarName),
									(String) visitMap
											.get(ProtocolKeys.VisitName),
									visitSeq++));
				}
				calendarSB.append(output.generateComponent4Part1(calendarMap,
						visitForPart1SubSB.toString()));
				calendarSB.append(visitSB.toString());
     	}
    	//System.out.println(calendarSB);
    	
        // Return XML response
        String xml = output.generateFinalOutput(studyDataMap, calendarSB.toString());
        return new DomSourceFactory().createSource(xml);
    	}
    }
    
    // Our test reference date with Epic is 2014-01-01
    private static Date getReferenceDate() {
		Calendar cal = GregorianCalendar.getInstance();
		cal.set(2014, 0, 1);
		return cal.getTime();
    }

    private static Date addDays(Date date, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days);
        return cal.getTime();
    }
    
    private static String formatDate(Date date) {
    	SimpleDateFormat f = new SimpleDateFormat("yyyyMMdd");
    	return f.format(date);
    }
    
}

