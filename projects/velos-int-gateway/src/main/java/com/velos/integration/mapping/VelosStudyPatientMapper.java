package com.velos.integration.mapping;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.velos.services.AddStudyPatientStatusResponse;
import com.velos.services.CompletedAction;
import com.velos.services.CrudAction;
import com.velos.services.EnrollPatientToStudyResponse;
import com.velos.services.GetStudyPatientsResponse;
import com.velos.services.ResponseHolder;
import com.velos.services.Results;
import com.velos.services.StudyPatient;

public class VelosStudyPatientMapper {
	private String endpoint = null;
	private Map<VelosKeys, Object> dataMap = new HashMap<VelosKeys, Object>();
	
	public VelosStudyPatientMapper(String endpoint) {
		this.endpoint = endpoint;
	}
	
	public Map<VelosKeys, Object> mapStudyPatientList(GetStudyPatientsResponse resp) {
		if (EndpointKeys.Epic.toString().equals(endpoint)) {
			mapStudyPatientListForEpic(resp);
		}
		return dataMap;
	}
	
	private void mapStudyPatientListForEpic(GetStudyPatientsResponse resp) {
		List< Map<VelosKeys, Object> > dataStudyPatientList = new ArrayList< Map<VelosKeys, Object> >();
		List<StudyPatient> list = resp.getStudyPatient();
		for (StudyPatient studyPatient : list) {
			Map<VelosKeys, Object> map = new HashMap<VelosKeys, Object>();
			map.put(ProtocolKeys.Mrn, studyPatient.getPatientIdentifier().getPatientId());
			map.put(ProtocolKeys.StudyPatId, studyPatient.getStudyPatId());
			dataStudyPatientList.add(map);
		}
		dataMap.put(ProtocolKeys.StudyPatientList, dataStudyPatientList);
	}
	
	public Map<VelosKeys, Object> mapEnrollPatientToStudy(EnrollPatientToStudyResponse resp) {
		if (EndpointKeys.Epic.toString().equals(endpoint)) {
			mapEnrollPatientToStudyForEpic(resp);
		}
		return dataMap;
	}
		
	private void mapEnrollPatientToStudyForEpic(EnrollPatientToStudyResponse resp) {
		ResponseHolder holder = resp.getResponse();
		Results results = holder.getResults();
		List<CompletedAction> resultList = results.getResult();
		for (CompletedAction action : resultList) {
			CrudAction crud = action.getAction();
			dataMap.put(ProtocolKeys.CompletedAction, crud.name());
		}
	}

	public Map<VelosKeys, Object> mapAddStudyPatientStatus(AddStudyPatientStatusResponse resp) {
		if (EndpointKeys.Epic.toString().equals(endpoint)) {
			mapAddStudyPatientStatusForEpic(resp);
		}
		return dataMap;
	}

	private void mapAddStudyPatientStatusForEpic(AddStudyPatientStatusResponse resp) {
		ResponseHolder holder = resp.getResponse();
		Results results = holder.getResults();
		List<CompletedAction> resultList = results.getResult();
		for (CompletedAction action : resultList) {
			CrudAction crud = action.getAction();
			dataMap.put(ProtocolKeys.CompletedAction, crud.name());
		}
	}
}
