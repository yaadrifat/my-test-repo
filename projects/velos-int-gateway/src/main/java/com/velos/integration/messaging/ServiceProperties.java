package com.velos.integration.messaging;

import java.util.ResourceBundle;

public class ServiceProperties {
	
	//JNDI properties
	public static final String JNDI_NAMING_FACTORY_INITIAL = "java.naming.factory.initial"; 
	public static final String JNDI_NAMING_PROVIDER_URL = "java.naming.provider.url"; 
	public static final String JNDI_NAMING_FACTORY_URL_PKGS  = "java.naming.factory.url.pkgs";
	
	public static final String STUDY_TOPIC_CONNECTION_FACTORY_JNDI= "study.topic.connectionfactory.jndi"; 
	public static final String STUDY_TOPIC_JNDI = "study.topic.jndi"; 
	public static final String STUDY_TOPIC_USERNAME = "study.topic.username"; 
	public static final String STUDY_TOPIC_PASSWORD = "study.topic.password";
	
	private static ResourceBundle bundle= ResourceBundle.getBundle("velos-messaging");
	
	public static String getProperty(String key) {
	    return bundle.getString(key);
	}

}
