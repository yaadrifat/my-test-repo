package com.velos.integration.mapping;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.velos.services.GetStudySummaryResponse;
import com.velos.services.NvPair;

public class VelosStudyMapper {
	private String endpoint = null;
	private Map<VelosKeys, Object> dataMap = new HashMap<VelosKeys, Object>();
	
	public VelosStudyMapper(String endpoint) {
		this.endpoint = endpoint;
	}
	
	public Map<VelosKeys, Object> mapStudySummary(GetStudySummaryResponse resp) {
		if (EndpointKeys.Epic.toString().equals(endpoint)) {
			mapStudySummaryForEpic(resp);
		}
		return dataMap;
	}
	
	private void mapStudySummaryForEpic(GetStudySummaryResponse resp) {
		dataMap.put(ProtocolKeys.Title, MapUtil.escapeSpecial(resp.getStudySummary().getStudyTitle()));
		dataMap.put(ProtocolKeys.Text, MapUtil.escapeSpecial(resp.getStudySummary().getSummaryText()));
		dataMap.put(ProtocolKeys.IdExtension, MapUtil.escapeSpecial(resp.getStudySummary().getStudyNumber()));
		List<NvPair> moreList = resp.getStudySummary().getMoreStudyDetails();
		for (NvPair more : moreList) {
			if (ProtocolKeys.IrbNumber.toString().equalsIgnoreCase(more.getKey())) {
				dataMap.put(ProtocolKeys.IrbNumber, more.getValue());
			} else if (ProtocolKeys.Irb.toString().equalsIgnoreCase(more.getKey())) {
				dataMap.put(ProtocolKeys.Irb, more.getValue());
			} else if (ProtocolKeys.NctNumber.toString().equalsIgnoreCase(more.getKey())) {
				dataMap.put(ProtocolKeys.NctNumber, more.getValue());
			} else if (ProtocolKeys.Nct.toString().equalsIgnoreCase(more.getKey())) {
				dataMap.put(ProtocolKeys.Nct, more.getValue());
			}
		}
	}
}
