package com.velos.integration.mapping;

public enum ProtocolKeys implements VelosKeys {
	// Convention: Each enum type should begin with upper case and use camel case for the rest.
	//             The corresponding string can be in any case to meet the business need.
	//             If the string contains a period, use _ for that in the corresponding enum type.
	AlertProtocolState("AlertProtocolState"),
	AssigningAuthorityName("assigningAuthorityName"),
	CalendarId("calendarId"),
	CalendarIdList("calendarIdList"),
	CalendarName("calendarName"),
	CalendarDuration("calendarDuration"),
	CalendarDurUnit("calendarDurUnit"),
	CandidateId("candidateID"),
	CandidateIdUpper("CandidateID"),
	CompletedAction("completedAction"),
	CoverageType("coverageType"),
	Description("description"),
	Dob("DOB"),
	EffectiveStartTime("effectiveStartTime"),
	EffectiveEndTime("effectiveEndTime"),
	EnrollPatientRequest("EnrollPatientRequestRequest"),
	ErrorList("errorList"),
	EventCpt("eventCpt"),
	EventList("eventList"),
	EventName("eventName"),
	EventSequence("eventSequence"),
	Extension("extension"),
	Family("family"),
	FaultString("faultstring"),
	FirstName("firstName"),
	Id("id"),
	IdExtension("idExtension"),
	Irb("IRB"),
	IrbNumber("irb_number"),
	Given("given"),
	LastName("lastName"),
	LetterA("A"),
	Mrn("MRN"),
	MultiplePatientsFoundMsg("Multiple patients found"),
	Name("name"),
	Nct("NCT"),
	NctNumber("nctNumber"),
	Patient("patient"),
	PatientId("patientId"),
	PatientNotFoundMsg("Patient not found"),
	ProcessState("processState"),
	RequestType("requestType"),
	RetrievePatient("RetrievePatientRequest"),
	RetrieveProtocol("RetrieveProtocolDefRequest"),	
	Root("root"),
	Study("study"),
	StudyNumber("studyNumber"),
	StudyPatId("studypatId"),
	StudyPatStatus("studyPatStatus"),
	StudyPatientList("studyPatientList"),
	Text("text"),
	Title("title"),
	TotalCount("totalCount"),
	Value("value"),
	VisitDisplacement("visitDisplacement"),
	VisitDisplacementFormatted("visitDisplacementFormatted"),
	VisitList("visitList"),
	VisitName("visitName"),
	VisitWindowBefore("visitWindowBefore"),
	VisitWindowAfter("visitWindowAfter"),
	VisitWindowBeforeFormatted("visitWindowBeforeFormatted"),
	VisitWindowAfterFormatted("visitWindowAfterFormatted"),
	PatientID("patientID"), // This is for searchPatient method
	SiteName("siteName"),

	OID("OID"),
	PK("PK"),
	PatProtId("patProtId"),
	StudyId("studyId"),
	Instantiation("instantiation"),
	PlannedStudy("plannedStudy"),
	Query("query");
	;
	
	ProtocolKeys(String key) {
		this.key = key;
	}
	
	private String key;
	
	@Override
	public String toString() {
		return key;
	}
}
