
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for patientSchedule complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="patientSchedule">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="endDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="patientIdentifier" type="{http://velos.com/services/}patientIdentifier" minOccurs="0"/>
 *         &lt;element name="patientScheduleSummary" type="{http://velos.com/services/}patientScheduleSummary" minOccurs="0"/>
 *         &lt;element name="startDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="studyIdentifier" type="{http://velos.com/services/}studyIdentifier" minOccurs="0"/>
 *         &lt;element name="visits" type="{http://velos.com/services/}visits" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "patientSchedule", propOrder = {
    "endDate",
    "patientIdentifier",
    "patientScheduleSummary",
    "startDate",
    "studyIdentifier",
    "visits"
})
public class PatientSchedule
    extends ServiceObject
{

    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar endDate;
    protected PatientIdentifier patientIdentifier;
    protected PatientScheduleSummary patientScheduleSummary;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar startDate;
    protected StudyIdentifier studyIdentifier;
    protected Visits visits;

    /**
     * Gets the value of the endDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEndDate(XMLGregorianCalendar value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the patientIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link PatientIdentifier }
     *     
     */
    public PatientIdentifier getPatientIdentifier() {
        return patientIdentifier;
    }

    /**
     * Sets the value of the patientIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientIdentifier }
     *     
     */
    public void setPatientIdentifier(PatientIdentifier value) {
        this.patientIdentifier = value;
    }

    /**
     * Gets the value of the patientScheduleSummary property.
     * 
     * @return
     *     possible object is
     *     {@link PatientScheduleSummary }
     *     
     */
    public PatientScheduleSummary getPatientScheduleSummary() {
        return patientScheduleSummary;
    }

    /**
     * Sets the value of the patientScheduleSummary property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientScheduleSummary }
     *     
     */
    public void setPatientScheduleSummary(PatientScheduleSummary value) {
        this.patientScheduleSummary = value;
    }

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDate(XMLGregorianCalendar value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the studyIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link StudyIdentifier }
     *     
     */
    public StudyIdentifier getStudyIdentifier() {
        return studyIdentifier;
    }

    /**
     * Sets the value of the studyIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyIdentifier }
     *     
     */
    public void setStudyIdentifier(StudyIdentifier value) {
        this.studyIdentifier = value;
    }

    /**
     * Gets the value of the visits property.
     * 
     * @return
     *     possible object is
     *     {@link Visits }
     *     
     */
    public Visits getVisits() {
        return visits;
    }

    /**
     * Sets the value of the visits property.
     * 
     * @param value
     *     allowed object is
     *     {@link Visits }
     *     
     */
    public void setVisits(Visits value) {
        this.visits = value;
    }

}
