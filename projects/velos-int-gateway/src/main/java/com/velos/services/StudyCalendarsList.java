
package com.velos.services;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for studyCalendarsList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="studyCalendarsList">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="studyCalendars" type="{http://velos.com/services/}studyCalendars" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "studyCalendarsList", propOrder = {
    "studyCalendars"
})
public class StudyCalendarsList
    extends ServiceObject
{

    @XmlElement(nillable = true)
    protected List<StudyCalendars> studyCalendars;

    /**
     * Gets the value of the studyCalendars property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the studyCalendars property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStudyCalendars().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StudyCalendars }
     * 
     * 
     */
    public List<StudyCalendars> getStudyCalendars() {
        if (studyCalendars == null) {
            studyCalendars = new ArrayList<StudyCalendars>();
        }
        return this.studyCalendars;
    }

}
