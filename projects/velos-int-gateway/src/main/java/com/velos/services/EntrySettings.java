
package com.velos.services;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for entrySettings.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="entrySettings">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="MULTIPLE"/>
 *     &lt;enumeration value="ONCE"/>
 *     &lt;enumeration value="ONCE_EDITABLE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "entrySettings")
@XmlEnum
public enum EntrySettings {

    MULTIPLE,
    ONCE,
    ONCE_EDITABLE;

    public String value() {
        return name();
    }

    public static EntrySettings fromValue(String v) {
        return valueOf(v);
    }

}
