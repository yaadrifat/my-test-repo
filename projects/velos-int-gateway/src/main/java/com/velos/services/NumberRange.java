
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for numberRange complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="numberRange">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="firstOperator" type="{http://velos.com/services/}operator" minOccurs="0"/>
 *         &lt;element name="firstValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="logicOperator" type="{http://velos.com/services/}logicalOperator" minOccurs="0"/>
 *         &lt;element name="secondOperator" type="{http://velos.com/services/}operator" minOccurs="0"/>
 *         &lt;element name="secondValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "numberRange", propOrder = {
    "firstOperator",
    "firstValue",
    "logicOperator",
    "secondOperator",
    "secondValue"
})
public class NumberRange
    extends ServiceObject
{

    protected Operator firstOperator;
    protected String firstValue;
    protected LogicalOperator logicOperator;
    protected Operator secondOperator;
    protected String secondValue;

    /**
     * Gets the value of the firstOperator property.
     * 
     * @return
     *     possible object is
     *     {@link Operator }
     *     
     */
    public Operator getFirstOperator() {
        return firstOperator;
    }

    /**
     * Sets the value of the firstOperator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Operator }
     *     
     */
    public void setFirstOperator(Operator value) {
        this.firstOperator = value;
    }

    /**
     * Gets the value of the firstValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstValue() {
        return firstValue;
    }

    /**
     * Sets the value of the firstValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstValue(String value) {
        this.firstValue = value;
    }

    /**
     * Gets the value of the logicOperator property.
     * 
     * @return
     *     possible object is
     *     {@link LogicalOperator }
     *     
     */
    public LogicalOperator getLogicOperator() {
        return logicOperator;
    }

    /**
     * Sets the value of the logicOperator property.
     * 
     * @param value
     *     allowed object is
     *     {@link LogicalOperator }
     *     
     */
    public void setLogicOperator(LogicalOperator value) {
        this.logicOperator = value;
    }

    /**
     * Gets the value of the secondOperator property.
     * 
     * @return
     *     possible object is
     *     {@link Operator }
     *     
     */
    public Operator getSecondOperator() {
        return secondOperator;
    }

    /**
     * Sets the value of the secondOperator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Operator }
     *     
     */
    public void setSecondOperator(Operator value) {
        this.secondOperator = value;
    }

    /**
     * Gets the value of the secondValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecondValue() {
        return secondValue;
    }

    /**
     * Sets the value of the secondValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecondValue(String value) {
        this.secondValue = value;
    }

}
