
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updateMPatientStudyStatus complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updateMPatientStudyStatus">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UpdateMPatientStudyStatus" type="{http://velos.com/services/}updateMPatientStudyStatuses" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateMPatientStudyStatus", propOrder = {
    "updateMPatientStudyStatus"
})
public class UpdateMPatientStudyStatus {

    @XmlElement(name = "UpdateMPatientStudyStatus")
    protected UpdateMPatientStudyStatuses updateMPatientStudyStatus;

    /**
     * Gets the value of the updateMPatientStudyStatus property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateMPatientStudyStatuses }
     *     
     */
    public UpdateMPatientStudyStatuses getUpdateMPatientStudyStatus() {
        return updateMPatientStudyStatus;
    }

    /**
     * Sets the value of the updateMPatientStudyStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateMPatientStudyStatuses }
     *     
     */
    public void setUpdateMPatientStudyStatus(UpdateMPatientStudyStatuses value) {
        this.updateMPatientStudyStatus = value;
    }

}
