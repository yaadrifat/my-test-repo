
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for patientEnrollmentDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="patientEnrollmentDetails">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="assignedTo" type="{http://velos.com/services/}userIdentifier" minOccurs="0"/>
 *         &lt;element name="causeOfDeath" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="currentStatus" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="dateOfDeath" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="deathRelatedToTrial" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="emptyDeathDate" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="emptyNextFollowUpDate" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="enrolledBy" type="{http://velos.com/services/}userIdentifier" minOccurs="0"/>
 *         &lt;element name="enrollingSite" type="{http://velos.com/services/}organizationIdentifier" minOccurs="0"/>
 *         &lt;element name="evaluationFlag" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="evaluationStatus" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="inevaluationStatus" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="informedConsentVersionNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nextFollowUpDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="notes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="patientStudyId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="physician" type="{http://velos.com/services/}userIdentifier" minOccurs="0"/>
 *         &lt;element name="randomizationNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="reasonForChange" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="reasonOfDeathRelatedToTrial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="screenNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="screenedBy" type="{http://velos.com/services/}userIdentifier" minOccurs="0"/>
 *         &lt;element name="screeningOutcome" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="specifyCause" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="status" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="statusDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="statusReason" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="survivalStatus" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="treatingOrganization" type="{http://velos.com/services/}organizationIdentifier" minOccurs="0"/>
 *         &lt;element name="treatmentLocation" type="{http://velos.com/services/}code" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "patientEnrollmentDetails", propOrder = {
    "assignedTo",
    "causeOfDeath",
    "currentStatus",
    "dateOfDeath",
    "deathRelatedToTrial",
    "emptyDeathDate",
    "emptyNextFollowUpDate",
    "enrolledBy",
    "enrollingSite",
    "evaluationFlag",
    "evaluationStatus",
    "inevaluationStatus",
    "informedConsentVersionNumber",
    "nextFollowUpDate",
    "notes",
    "patientStudyId",
    "physician",
    "randomizationNumber",
    "reasonForChange",
    "reasonOfDeathRelatedToTrial",
    "screenNumber",
    "screenedBy",
    "screeningOutcome",
    "specifyCause",
    "status",
    "statusDate",
    "statusReason",
    "survivalStatus",
    "treatingOrganization",
    "treatmentLocation"
})
public class PatientEnrollmentDetails
    extends ServiceObject
{

    protected UserIdentifier assignedTo;
    protected Code causeOfDeath;
    protected boolean currentStatus;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateOfDeath;
    protected Code deathRelatedToTrial;
    protected boolean emptyDeathDate;
    protected boolean emptyNextFollowUpDate;
    protected UserIdentifier enrolledBy;
    protected OrganizationIdentifier enrollingSite;
    protected Code evaluationFlag;
    protected Code evaluationStatus;
    protected Code inevaluationStatus;
    protected String informedConsentVersionNumber;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar nextFollowUpDate;
    protected String notes;
    protected String patientStudyId;
    protected UserIdentifier physician;
    protected String randomizationNumber;
    protected String reasonForChange;
    protected String reasonOfDeathRelatedToTrial;
    protected String screenNumber;
    protected UserIdentifier screenedBy;
    protected Code screeningOutcome;
    protected String specifyCause;
    protected Code status;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar statusDate;
    protected Code statusReason;
    protected Code survivalStatus;
    protected OrganizationIdentifier treatingOrganization;
    protected Code treatmentLocation;

    /**
     * Gets the value of the assignedTo property.
     * 
     * @return
     *     possible object is
     *     {@link UserIdentifier }
     *     
     */
    public UserIdentifier getAssignedTo() {
        return assignedTo;
    }

    /**
     * Sets the value of the assignedTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserIdentifier }
     *     
     */
    public void setAssignedTo(UserIdentifier value) {
        this.assignedTo = value;
    }

    /**
     * Gets the value of the causeOfDeath property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getCauseOfDeath() {
        return causeOfDeath;
    }

    /**
     * Sets the value of the causeOfDeath property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setCauseOfDeath(Code value) {
        this.causeOfDeath = value;
    }

    /**
     * Gets the value of the currentStatus property.
     * 
     */
    public boolean isCurrentStatus() {
        return currentStatus;
    }

    /**
     * Sets the value of the currentStatus property.
     * 
     */
    public void setCurrentStatus(boolean value) {
        this.currentStatus = value;
    }

    /**
     * Gets the value of the dateOfDeath property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateOfDeath() {
        return dateOfDeath;
    }

    /**
     * Sets the value of the dateOfDeath property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateOfDeath(XMLGregorianCalendar value) {
        this.dateOfDeath = value;
    }

    /**
     * Gets the value of the deathRelatedToTrial property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getDeathRelatedToTrial() {
        return deathRelatedToTrial;
    }

    /**
     * Sets the value of the deathRelatedToTrial property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setDeathRelatedToTrial(Code value) {
        this.deathRelatedToTrial = value;
    }

    /**
     * Gets the value of the emptyDeathDate property.
     * 
     */
    public boolean isEmptyDeathDate() {
        return emptyDeathDate;
    }

    /**
     * Sets the value of the emptyDeathDate property.
     * 
     */
    public void setEmptyDeathDate(boolean value) {
        this.emptyDeathDate = value;
    }

    /**
     * Gets the value of the emptyNextFollowUpDate property.
     * 
     */
    public boolean isEmptyNextFollowUpDate() {
        return emptyNextFollowUpDate;
    }

    /**
     * Sets the value of the emptyNextFollowUpDate property.
     * 
     */
    public void setEmptyNextFollowUpDate(boolean value) {
        this.emptyNextFollowUpDate = value;
    }

    /**
     * Gets the value of the enrolledBy property.
     * 
     * @return
     *     possible object is
     *     {@link UserIdentifier }
     *     
     */
    public UserIdentifier getEnrolledBy() {
        return enrolledBy;
    }

    /**
     * Sets the value of the enrolledBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserIdentifier }
     *     
     */
    public void setEnrolledBy(UserIdentifier value) {
        this.enrolledBy = value;
    }

    /**
     * Gets the value of the enrollingSite property.
     * 
     * @return
     *     possible object is
     *     {@link OrganizationIdentifier }
     *     
     */
    public OrganizationIdentifier getEnrollingSite() {
        return enrollingSite;
    }

    /**
     * Sets the value of the enrollingSite property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganizationIdentifier }
     *     
     */
    public void setEnrollingSite(OrganizationIdentifier value) {
        this.enrollingSite = value;
    }

    /**
     * Gets the value of the evaluationFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getEvaluationFlag() {
        return evaluationFlag;
    }

    /**
     * Sets the value of the evaluationFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setEvaluationFlag(Code value) {
        this.evaluationFlag = value;
    }

    /**
     * Gets the value of the evaluationStatus property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getEvaluationStatus() {
        return evaluationStatus;
    }

    /**
     * Sets the value of the evaluationStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setEvaluationStatus(Code value) {
        this.evaluationStatus = value;
    }

    /**
     * Gets the value of the inevaluationStatus property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getInevaluationStatus() {
        return inevaluationStatus;
    }

    /**
     * Sets the value of the inevaluationStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setInevaluationStatus(Code value) {
        this.inevaluationStatus = value;
    }

    /**
     * Gets the value of the informedConsentVersionNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInformedConsentVersionNumber() {
        return informedConsentVersionNumber;
    }

    /**
     * Sets the value of the informedConsentVersionNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInformedConsentVersionNumber(String value) {
        this.informedConsentVersionNumber = value;
    }

    /**
     * Gets the value of the nextFollowUpDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getNextFollowUpDate() {
        return nextFollowUpDate;
    }

    /**
     * Sets the value of the nextFollowUpDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setNextFollowUpDate(XMLGregorianCalendar value) {
        this.nextFollowUpDate = value;
    }

    /**
     * Gets the value of the notes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNotes() {
        return notes;
    }

    /**
     * Sets the value of the notes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNotes(String value) {
        this.notes = value;
    }

    /**
     * Gets the value of the patientStudyId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPatientStudyId() {
        return patientStudyId;
    }

    /**
     * Sets the value of the patientStudyId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPatientStudyId(String value) {
        this.patientStudyId = value;
    }

    /**
     * Gets the value of the physician property.
     * 
     * @return
     *     possible object is
     *     {@link UserIdentifier }
     *     
     */
    public UserIdentifier getPhysician() {
        return physician;
    }

    /**
     * Sets the value of the physician property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserIdentifier }
     *     
     */
    public void setPhysician(UserIdentifier value) {
        this.physician = value;
    }

    /**
     * Gets the value of the randomizationNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRandomizationNumber() {
        return randomizationNumber;
    }

    /**
     * Sets the value of the randomizationNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRandomizationNumber(String value) {
        this.randomizationNumber = value;
    }

    /**
     * Gets the value of the reasonForChange property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReasonForChange() {
        return reasonForChange;
    }

    /**
     * Sets the value of the reasonForChange property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReasonForChange(String value) {
        this.reasonForChange = value;
    }

    /**
     * Gets the value of the reasonOfDeathRelatedToTrial property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReasonOfDeathRelatedToTrial() {
        return reasonOfDeathRelatedToTrial;
    }

    /**
     * Sets the value of the reasonOfDeathRelatedToTrial property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReasonOfDeathRelatedToTrial(String value) {
        this.reasonOfDeathRelatedToTrial = value;
    }

    /**
     * Gets the value of the screenNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScreenNumber() {
        return screenNumber;
    }

    /**
     * Sets the value of the screenNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScreenNumber(String value) {
        this.screenNumber = value;
    }

    /**
     * Gets the value of the screenedBy property.
     * 
     * @return
     *     possible object is
     *     {@link UserIdentifier }
     *     
     */
    public UserIdentifier getScreenedBy() {
        return screenedBy;
    }

    /**
     * Sets the value of the screenedBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserIdentifier }
     *     
     */
    public void setScreenedBy(UserIdentifier value) {
        this.screenedBy = value;
    }

    /**
     * Gets the value of the screeningOutcome property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getScreeningOutcome() {
        return screeningOutcome;
    }

    /**
     * Sets the value of the screeningOutcome property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setScreeningOutcome(Code value) {
        this.screeningOutcome = value;
    }

    /**
     * Gets the value of the specifyCause property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpecifyCause() {
        return specifyCause;
    }

    /**
     * Sets the value of the specifyCause property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpecifyCause(String value) {
        this.specifyCause = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setStatus(Code value) {
        this.status = value;
    }

    /**
     * Gets the value of the statusDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStatusDate() {
        return statusDate;
    }

    /**
     * Sets the value of the statusDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStatusDate(XMLGregorianCalendar value) {
        this.statusDate = value;
    }

    /**
     * Gets the value of the statusReason property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getStatusReason() {
        return statusReason;
    }

    /**
     * Sets the value of the statusReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setStatusReason(Code value) {
        this.statusReason = value;
    }

    /**
     * Gets the value of the survivalStatus property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getSurvivalStatus() {
        return survivalStatus;
    }

    /**
     * Sets the value of the survivalStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setSurvivalStatus(Code value) {
        this.survivalStatus = value;
    }

    /**
     * Gets the value of the treatingOrganization property.
     * 
     * @return
     *     possible object is
     *     {@link OrganizationIdentifier }
     *     
     */
    public OrganizationIdentifier getTreatingOrganization() {
        return treatingOrganization;
    }

    /**
     * Sets the value of the treatingOrganization property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganizationIdentifier }
     *     
     */
    public void setTreatingOrganization(OrganizationIdentifier value) {
        this.treatingOrganization = value;
    }

    /**
     * Gets the value of the treatmentLocation property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getTreatmentLocation() {
        return treatmentLocation;
    }

    /**
     * Sets the value of the treatmentLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setTreatmentLocation(Code value) {
        this.treatmentLocation = value;
    }

}
