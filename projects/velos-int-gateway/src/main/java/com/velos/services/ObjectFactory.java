
package com.velos.services;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.velos.services package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CreateNonSystemUserResponse_QNAME = new QName("http://velos.com/services/", "createNonSystemUserResponse");
    private final static QName _OperationException_QNAME = new QName("http://velos.com/services/", "OperationException");
    private final static QName _CheckESignatureResponse_QNAME = new QName("http://velos.com/services/", "checkESignatureResponse");
    private final static QName _KillUserSession_QNAME = new QName("http://velos.com/services/", "killUserSession");
    private final static QName _GetUserGroupsResponse_QNAME = new QName("http://velos.com/services/", "getUserGroupsResponse");
    private final static QName _CheckESignature_QNAME = new QName("http://velos.com/services/", "checkESignature");
    private final static QName _GetUserGroups_QNAME = new QName("http://velos.com/services/", "getUserGroups");
    private final static QName _CreateUser_QNAME = new QName("http://velos.com/services/", "createUser");
    private final static QName _GetAllGroups_QNAME = new QName("http://velos.com/services/", "getAllGroups");
    private final static QName _SearchUserResponse_QNAME = new QName("http://velos.com/services/", "searchUserResponse");
    private final static QName _GetAllGroupsResponse_QNAME = new QName("http://velos.com/services/", "getAllGroupsResponse");
    private final static QName _GetAllOrganizations_QNAME = new QName("http://velos.com/services/", "getAllOrganizations");
    private final static QName _KillUserSessionResponse_QNAME = new QName("http://velos.com/services/", "killUserSessionResponse");
    private final static QName _SearchUser_QNAME = new QName("http://velos.com/services/", "searchUser");
    private final static QName _CreateNonSystemUser_QNAME = new QName("http://velos.com/services/", "createNonSystemUser");
    private final static QName _ChangeUserStatusResponse_QNAME = new QName("http://velos.com/services/", "changeUserStatusResponse");
    private final static QName _CreateUserResponse_QNAME = new QName("http://velos.com/services/", "createUserResponse");
    private final static QName _GetAllOrganizationsResponse_QNAME = new QName("http://velos.com/services/", "getAllOrganizationsResponse");
    private final static QName _ChangeUserStatus_QNAME = new QName("http://velos.com/services/", "changeUserStatus");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.velos.services
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetAllOrganizations }
     * 
     */
    public GetAllOrganizations createGetAllOrganizations() {
        return new GetAllOrganizations();
    }

    /**
     * Create an instance of {@link Organizations }
     * 
     */
    public Organizations createOrganizations() {
        return new Organizations();
    }

    /**
     * Create an instance of {@link GetUserGroups }
     * 
     */
    public GetUserGroups createGetUserGroups() {
        return new GetUserGroups();
    }

    /**
     * Create an instance of {@link GetAllGroups }
     * 
     */
    public GetAllGroups createGetAllGroups() {
        return new GetAllGroups();
    }

    /**
     * Create an instance of {@link SimpleIdentifier }
     * 
     */
    public SimpleIdentifier createSimpleIdentifier() {
        return new SimpleIdentifier();
    }

    /**
     * Create an instance of {@link Results }
     * 
     */
    public Results createResults() {
        return new Results();
    }

    /**
     * Create an instance of {@link GetAllGroupsResponse }
     * 
     */
    public GetAllGroupsResponse createGetAllGroupsResponse() {
        return new GetAllGroupsResponse();
    }

    /**
     * Create an instance of {@link CompletedAction }
     * 
     */
    public CompletedAction createCompletedAction() {
        return new CompletedAction();
    }

    /**
     * Create an instance of {@link CreateUser }
     * 
     */
    public CreateUser createCreateUser() {
        return new CreateUser();
    }

    /**
     * Create an instance of {@link ChangeUserStatus }
     * 
     */
    public ChangeUserStatus createChangeUserStatus() {
        return new ChangeUserStatus();
    }

    /**
     * Create an instance of {@link CreateNonSystemUser }
     * 
     */
    public CreateNonSystemUser createCreateNonSystemUser() {
        return new CreateNonSystemUser();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link KillUserSession }
     * 
     */
    public KillUserSession createKillUserSession() {
        return new KillUserSession();
    }

    /**
     * Create an instance of {@link UserIdentifier }
     * 
     */
    public UserIdentifier createUserIdentifier() {
        return new UserIdentifier();
    }

    /**
     * Create an instance of {@link GroupIdentifier }
     * 
     */
    public GroupIdentifier createGroupIdentifier() {
        return new GroupIdentifier();
    }

    /**
     * Create an instance of {@link SearchUser }
     * 
     */
    public SearchUser createSearchUser() {
        return new SearchUser();
    }

    /**
     * Create an instance of {@link CheckESignatureResponse }
     * 
     */
    public CheckESignatureResponse createCheckESignatureResponse() {
        return new CheckESignatureResponse();
    }

    /**
     * Create an instance of {@link CreateUserResponse }
     * 
     */
    public CreateUserResponse createCreateUserResponse() {
        return new CreateUserResponse();
    }

    /**
     * Create an instance of {@link Code }
     * 
     */
    public Code createCode() {
        return new Code();
    }

    /**
     * Create an instance of {@link SearchUserResponse }
     * 
     */
    public SearchUserResponse createSearchUserResponse() {
        return new SearchUserResponse();
    }

    /**
     * Create an instance of {@link Issues }
     * 
     */
    public Issues createIssues() {
        return new Issues();
    }

    /**
     * Create an instance of {@link GetUserGroupsResponse }
     * 
     */
    public GetUserGroupsResponse createGetUserGroupsResponse() {
        return new GetUserGroupsResponse();
    }

    /**
     * Create an instance of {@link NonSystemUser }
     * 
     */
    public NonSystemUser createNonSystemUser() {
        return new NonSystemUser();
    }

    /**
     * Create an instance of {@link OrganizationIdentifier }
     * 
     */
    public OrganizationIdentifier createOrganizationIdentifier() {
        return new OrganizationIdentifier();
    }

    /**
     * Create an instance of {@link Issue }
     * 
     */
    public Issue createIssue() {
        return new Issue();
    }

    /**
     * Create an instance of {@link UserSearch }
     * 
     */
    public UserSearch createUserSearch() {
        return new UserSearch();
    }

    /**
     * Create an instance of {@link CreateNonSystemUserResponse }
     * 
     */
    public CreateNonSystemUserResponse createCreateNonSystemUserResponse() {
        return new CreateNonSystemUserResponse();
    }

    /**
     * Create an instance of {@link KillUserSessionResponse }
     * 
     */
    public KillUserSessionResponse createKillUserSessionResponse() {
        return new KillUserSessionResponse();
    }

    /**
     * Create an instance of {@link Groups }
     * 
     */
    public Groups createGroups() {
        return new Groups();
    }

    /**
     * Create an instance of {@link OperationException }
     * 
     */
    public OperationException createOperationException() {
        return new OperationException();
    }

    /**
     * Create an instance of {@link CheckESignature }
     * 
     */
    public CheckESignature createCheckESignature() {
        return new CheckESignature();
    }

    /**
     * Create an instance of {@link StudyTeamIdentifier }
     * 
     */
    public StudyTeamIdentifier createStudyTeamIdentifier() {
        return new StudyTeamIdentifier();
    }

    /**
     * Create an instance of {@link UserSearchResults }
     * 
     */
    public UserSearchResults createUserSearchResults() {
        return new UserSearchResults();
    }

    /**
     * Create an instance of {@link ChangeUserStatusResponse }
     * 
     */
    public ChangeUserStatusResponse createChangeUserStatusResponse() {
        return new ChangeUserStatusResponse();
    }

    /**
     * Create an instance of {@link GetAllOrganizationsResponse }
     * 
     */
    public GetAllOrganizationsResponse createGetAllOrganizationsResponse() {
        return new GetAllOrganizationsResponse();
    }

    /**
     * Create an instance of {@link ResponseHolder }
     * 
     */
    public ResponseHolder createResponseHolder() {
        return new ResponseHolder();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateNonSystemUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "createNonSystemUserResponse")
    public JAXBElement<CreateNonSystemUserResponse> createCreateNonSystemUserResponse(CreateNonSystemUserResponse value) {
        return new JAXBElement<CreateNonSystemUserResponse>(_CreateNonSystemUserResponse_QNAME, CreateNonSystemUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OperationException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "OperationException")
    public JAXBElement<OperationException> createOperationException(OperationException value) {
        return new JAXBElement<OperationException>(_OperationException_QNAME, OperationException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckESignatureResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "checkESignatureResponse")
    public JAXBElement<CheckESignatureResponse> createCheckESignatureResponse(CheckESignatureResponse value) {
        return new JAXBElement<CheckESignatureResponse>(_CheckESignatureResponse_QNAME, CheckESignatureResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link KillUserSession }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "killUserSession")
    public JAXBElement<KillUserSession> createKillUserSession(KillUserSession value) {
        return new JAXBElement<KillUserSession>(_KillUserSession_QNAME, KillUserSession.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserGroupsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "getUserGroupsResponse")
    public JAXBElement<GetUserGroupsResponse> createGetUserGroupsResponse(GetUserGroupsResponse value) {
        return new JAXBElement<GetUserGroupsResponse>(_GetUserGroupsResponse_QNAME, GetUserGroupsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckESignature }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "checkESignature")
    public JAXBElement<CheckESignature> createCheckESignature(CheckESignature value) {
        return new JAXBElement<CheckESignature>(_CheckESignature_QNAME, CheckESignature.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserGroups }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "getUserGroups")
    public JAXBElement<GetUserGroups> createGetUserGroups(GetUserGroups value) {
        return new JAXBElement<GetUserGroups>(_GetUserGroups_QNAME, GetUserGroups.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "createUser")
    public JAXBElement<CreateUser> createCreateUser(CreateUser value) {
        return new JAXBElement<CreateUser>(_CreateUser_QNAME, CreateUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllGroups }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "getAllGroups")
    public JAXBElement<GetAllGroups> createGetAllGroups(GetAllGroups value) {
        return new JAXBElement<GetAllGroups>(_GetAllGroups_QNAME, GetAllGroups.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "searchUserResponse")
    public JAXBElement<SearchUserResponse> createSearchUserResponse(SearchUserResponse value) {
        return new JAXBElement<SearchUserResponse>(_SearchUserResponse_QNAME, SearchUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllGroupsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "getAllGroupsResponse")
    public JAXBElement<GetAllGroupsResponse> createGetAllGroupsResponse(GetAllGroupsResponse value) {
        return new JAXBElement<GetAllGroupsResponse>(_GetAllGroupsResponse_QNAME, GetAllGroupsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllOrganizations }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "getAllOrganizations")
    public JAXBElement<GetAllOrganizations> createGetAllOrganizations(GetAllOrganizations value) {
        return new JAXBElement<GetAllOrganizations>(_GetAllOrganizations_QNAME, GetAllOrganizations.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link KillUserSessionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "killUserSessionResponse")
    public JAXBElement<KillUserSessionResponse> createKillUserSessionResponse(KillUserSessionResponse value) {
        return new JAXBElement<KillUserSessionResponse>(_KillUserSessionResponse_QNAME, KillUserSessionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "searchUser")
    public JAXBElement<SearchUser> createSearchUser(SearchUser value) {
        return new JAXBElement<SearchUser>(_SearchUser_QNAME, SearchUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateNonSystemUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "createNonSystemUser")
    public JAXBElement<CreateNonSystemUser> createCreateNonSystemUser(CreateNonSystemUser value) {
        return new JAXBElement<CreateNonSystemUser>(_CreateNonSystemUser_QNAME, CreateNonSystemUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeUserStatusResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "changeUserStatusResponse")
    public JAXBElement<ChangeUserStatusResponse> createChangeUserStatusResponse(ChangeUserStatusResponse value) {
        return new JAXBElement<ChangeUserStatusResponse>(_ChangeUserStatusResponse_QNAME, ChangeUserStatusResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "createUserResponse")
    public JAXBElement<CreateUserResponse> createCreateUserResponse(CreateUserResponse value) {
        return new JAXBElement<CreateUserResponse>(_CreateUserResponse_QNAME, CreateUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllOrganizationsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "getAllOrganizationsResponse")
    public JAXBElement<GetAllOrganizationsResponse> createGetAllOrganizationsResponse(GetAllOrganizationsResponse value) {
        return new JAXBElement<GetAllOrganizationsResponse>(_GetAllOrganizationsResponse_QNAME, GetAllOrganizationsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeUserStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://velos.com/services/", name = "changeUserStatus")
    public JAXBElement<ChangeUserStatus> createChangeUserStatus(ChangeUserStatus value) {
        return new JAXBElement<ChangeUserStatus>(_ChangeUserStatus_QNAME, ChangeUserStatus.class, null, value);
    }

}
