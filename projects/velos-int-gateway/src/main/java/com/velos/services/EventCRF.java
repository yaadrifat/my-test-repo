
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for eventCRF complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="eventCRF">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="formIdentifier" type="{http://velos.com/services/}formIdentifier" minOccurs="0"/>
 *         &lt;element name="formName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "eventCRF", propOrder = {
    "formIdentifier",
    "formName"
})
public class EventCRF
    extends ServiceObject
{

    protected FormIdentifier formIdentifier;
    protected String formName;

    /**
     * Gets the value of the formIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link FormIdentifier }
     *     
     */
    public FormIdentifier getFormIdentifier() {
        return formIdentifier;
    }

    /**
     * Sets the value of the formIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link FormIdentifier }
     *     
     */
    public void setFormIdentifier(FormIdentifier value) {
        this.formIdentifier = value;
    }

    /**
     * Gets the value of the formName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormName() {
        return formName;
    }

    /**
     * Sets the value of the formName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormName(String value) {
        this.formName = value;
    }

}
