
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updateMStudyPatientStatus complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updateMStudyPatientStatus">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="patientEnrollmentDetails" type="{http://velos.com/services/}patientEnrollmentDetails" minOccurs="0"/>
 *         &lt;element name="patientStudyStatusIdentifier" type="{http://velos.com/services/}patientStudyStatusIdentifier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateMStudyPatientStatus", propOrder = {
    "patientEnrollmentDetails",
    "patientStudyStatusIdentifier"
})
public class UpdateMStudyPatientStatus
    extends ServiceObject
{

    protected PatientEnrollmentDetails patientEnrollmentDetails;
    protected PatientStudyStatusIdentifier patientStudyStatusIdentifier;

    /**
     * Gets the value of the patientEnrollmentDetails property.
     * 
     * @return
     *     possible object is
     *     {@link PatientEnrollmentDetails }
     *     
     */
    public PatientEnrollmentDetails getPatientEnrollmentDetails() {
        return patientEnrollmentDetails;
    }

    /**
     * Sets the value of the patientEnrollmentDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientEnrollmentDetails }
     *     
     */
    public void setPatientEnrollmentDetails(PatientEnrollmentDetails value) {
        this.patientEnrollmentDetails = value;
    }

    /**
     * Gets the value of the patientStudyStatusIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link PatientStudyStatusIdentifier }
     *     
     */
    public PatientStudyStatusIdentifier getPatientStudyStatusIdentifier() {
        return patientStudyStatusIdentifier;
    }

    /**
     * Sets the value of the patientStudyStatusIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientStudyStatusIdentifier }
     *     
     */
    public void setPatientStudyStatusIdentifier(PatientStudyStatusIdentifier value) {
        this.patientStudyStatusIdentifier = value;
    }

}
