create or replace
PACKAGE      PKG_SUBMISSION_LOGIC AS
/******************************************************************************
   NAME:       PKG_SUBMISSION_LOGIC
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        5/28/2009   Sonia Abrol          1. Created this package.
******************************************************************************/


  Function f_check_irb(p_study IN NUMBER) return t_nested_results_table;

  Function f_check_cro(p_study IN NUMBER) return t_nested_results_table;

  Function f_check_rsc(p_study IN NUMBER) return t_nested_results_table;


  Function f_check_cic(p_study IN NUMBER) return t_nested_results_table;

  Function f_check_src(p_study IN NUMBER) return t_nested_results_table;

  Function f_check_inst_reqmts_done(p_study IN NUMBER) return tab_submission_results;

  Function f_check_data_handlng_done(p_study IN NUMBER) return tab_submission_results;

  Function f_check_dept_approval_done(p_study IN NUMBER) return tab_submission_results;

  Function f_check_bioStat_done(p_study IN NUMBER) return tab_submission_results;

  Function f_check_collab_signoff_done(p_study IN NUMBER) return tab_submission_results;

  Function f_check_multi_signoff_done(p_study IN NUMBER) return tab_submission_results;

  Function f_check_ind_review_done(p_study IN NUMBER) return tab_submission_results;

  Function f_check_nom_review_done(p_study IN NUMBER) return tab_submission_results;

  Function f_check_fin_support_done(p_study IN NUMBER) return tab_submission_results;

  Function f_check_risk_Assmt_done(p_study IN NUMBER) return tab_submission_results;

  Function f_check_prot_actvtn_done(p_study IN NUMBER) return tab_submission_results;

END PKG_SUBMISSION_LOGIC;