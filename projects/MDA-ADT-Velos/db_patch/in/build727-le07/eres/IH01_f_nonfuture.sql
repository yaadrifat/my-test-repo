CREATE OR REPLACE FUNCTION ERES."F_NON_FUTURE_DATE" (p_date DATE ) RETURN DATE
AS
v_today DATE := trunc(sysdate);
BEGIN
 if (p_date > v_today) then
   return v_today;
 end if;
return trunc(p_date);
END ;
/
