SET DEFINE OFF;
SET serveroutput ON;

DECLARE
v_InstReqmts_formId  varchar2(10);
v_DataHndlng_formId  varchar2(10);
v_Dept_Signoff_formId  varchar2(10);
v_Prot_Actv_formId  varchar2(10);

c_formName_1 CONSTANT varchar2(50):= 'Institutional Requirements Form';
c_activity_Name_1 CONSTANT varchar2(50):= 'Institutional Requirements';

c_formName_2 CONSTANT varchar2(50):= 'Data Handling and Record Keeping';
c_activity_Name_2 CONSTANT varchar2(50):= 'Data Handling/ Record Keeping';

c_formName_8 CONSTANT varchar2(50):= 'Department / Divisional Approval';
c_activity_Name_8 CONSTANT varchar2(50):= 'Departmental Signoff';

c_formName_9 CONSTANT varchar2(50):= 'Protocol Activation';
c_activity_Name_9 CONSTANT varchar2(50):= 'Protocol Activation';


BEGIN
	--steps to do for each activity that needs to be updated
	--step1. get the primary key of the form by joining the er_formlib and er_linkedforms table
	--step2. once you get the primary key  for the required form , update the corresponding activity row in the workflow activity table , setting the form id in the onclick json
	--in the onClick Json string, the value for the key "module Id" can be hardcoded to ERES_FORM .
	--the value for the key "formId" would be the primary key retrieved in the select that happens before the update for each of the activities as seen in this script--
	
	
	 SELECT to_char(PK_FORMLIB) into v_InstReqmts_formId  FROM ER_FORMLIB , ER_LINKEDFORMS WHERE PK_FORMLIB = ER_LINKEDFORMS.FK_FORMLIB 
     AND ER_FORMLIB.RECORD_TYPE <> 'D' AND ER_LINKEDFORMS.LF_DISPLAYTYPE = 'SA' AND FORM_NAME = c_formName_1;
	
	 IF (v_InstReqmts_formId is NOT NULL) THEN
       UPDATE WORKFLOW_ACTIVITY SET wa_onclick_json = '{"moduleId":"ERES_FORM", "moduleDet":{"formId":'||v_InstReqmts_formId||'}}' WHERE WA_NAME = c_activity_Name_1 ;
	    COMMIT;
	 END IF;
		
	 SELECT to_char(PK_FORMLIB) into v_DataHndlng_formId  FROM ER_FORMLIB , ER_LINKEDFORMS WHERE PK_FORMLIB = ER_LINKEDFORMS.FK_FORMLIB 
     AND ER_FORMLIB.RECORD_TYPE <> 'D' AND ER_LINKEDFORMS.LF_DISPLAYTYPE = 'SA' AND FORM_NAME = c_formName_2;
	
     IF (v_DataHndlng_formId is NOT NULL) THEN
	   UPDATE WORKFLOW_ACTIVITY SET wa_onclick_json = '{"moduleId":"ERES_FORM", "moduleDet":{"formId":'||v_DataHndlng_formId||'}}' WHERE WA_NAME = c_activity_Name_2 ;
	   COMMIT;
	 END IF;
	
	SELECT to_char(PK_FORMLIB) into v_Dept_Signoff_formId  FROM ER_FORMLIB , ER_LINKEDFORMS WHERE PK_FORMLIB = ER_LINKEDFORMS.FK_FORMLIB 
    AND ER_FORMLIB.RECORD_TYPE <> 'D' AND ER_LINKEDFORMS.LF_DISPLAYTYPE = 'SA' AND FORM_NAME = c_formName_8;
	
    IF (v_Dept_Signoff_formId is NOT NULL) THEN
	  UPDATE WORKFLOW_ACTIVITY SET wa_onclick_json = '{"moduleId":"ERES_FORM", "moduleDet":{"formId":'||v_Dept_Signoff_formId||'}}' WHERE WA_NAME = c_activity_Name_8 ;
	  COMMIT;
	END IF;
	
	SELECT to_char(PK_FORMLIB) into v_Prot_Actv_formId  FROM ER_FORMLIB , ER_LINKEDFORMS WHERE PK_FORMLIB = ER_LINKEDFORMS.FK_FORMLIB 
    AND ER_FORMLIB.RECORD_TYPE <> 'D' AND ER_LINKEDFORMS.LF_DISPLAYTYPE = 'SA' AND FORM_NAME = c_formName_9;
	
    IF (v_Prot_Actv_formId is NOT NULL) THEN
	  UPDATE WORKFLOW_ACTIVITY SET wa_onclick_json = '{"moduleId":"ERES_FORM", "moduleDet":{"formId":'||v_Prot_Actv_formId||'}}' WHERE WA_NAME = c_activity_Name_9 ;
	  COMMIT;
	END IF;
	
END;
/