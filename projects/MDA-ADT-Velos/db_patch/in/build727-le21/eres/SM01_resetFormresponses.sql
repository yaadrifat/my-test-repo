set define off;

update ui_flx_pagever set PAGE_FORMRESPONSES = null;
update er_resubm_draft set PAGE_FORMRESPONSES = null;

commit;

