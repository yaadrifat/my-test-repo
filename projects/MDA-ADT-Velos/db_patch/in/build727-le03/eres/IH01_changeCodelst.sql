set define off;
set serveroutput on;

declare
v_CodeExists number := 0;
v_PkCode number := 0;
begin
  SELECT PK_CODELST INTO v_PkCode FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nom_rev_id';
  if (v_PkCode is not null) then
    update ER_CODELST set CODELST_CUSTOM_COL1=
    '{lookupPK:6000, selection:"single", mapping:[{source:"USRFULLNAME", target:"alternateId"},{source:"USRLOGIN", target:"alternateId'||v_PkCode||'"}]}' where 
    CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nom_rev';
      COMMIT;
    dbms_output.put_line('One row updated');
  end if;
  SELECT PK_CODELST INTO v_PkCode FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nom_alt_rev_id';
  if (v_PkCode is not null) then
    update ER_CODELST set CODELST_CUSTOM_COL1=
    '{lookupPK:6000, selection:"single", mapping:[{source:"USRFULLNAME", target:"alternateId"},{source:"USRLOGIN", target:"alternateId'||v_PkCode||'"}]}' where 
    CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nom_alt_rev';
      COMMIT;
    dbms_output.put_line('One row updated');
  end if;
end;
/
