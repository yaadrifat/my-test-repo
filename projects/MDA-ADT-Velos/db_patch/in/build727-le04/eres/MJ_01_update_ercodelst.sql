update er_codelst set codelst_custom_col1= '<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "mdaDsmb">MD ANDERSON DSMB</OPTION>
<OPTION value = "mdaExtdsmb">MD ANDERSON  External DSMB </OPTION>
<OPTION value = "indpDsmb">Independent DSMB - This is a DSMB comprised of members who are not affiliated with the research or sponsor in any way.</OPTION>
<OPTION value = "intDsmb">Internal DSMB - This is a DSMB comprised of study investigators and/or sponsor employees/contractors.</OPTION>
<OPTION value = "notAppl">Not Applicable</OPTION>
</SELECT>' where codelst_subtype='dsmbInt' and codelst_desc='Data Safety and Monitoring Board'; 

commit;