set define off;
set serveroutput on;

-->What criteria are you using for classifying Adverse Events
-->Other : Displayed if 'What critera are you using for classifying adverse events' is 'Other'


declare
v_CodeExists number := 0;
begin
	
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'critAdvEvents';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'critAdvEvents', 'What criteria are you using for classifying Adverse Events?', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'critAdvEvntsOth';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc)
        values (seq_er_codelst.nextval, 'studyidtype', 'critAdvEvntsOth', 'Other');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
COMMIT;
END;
/

