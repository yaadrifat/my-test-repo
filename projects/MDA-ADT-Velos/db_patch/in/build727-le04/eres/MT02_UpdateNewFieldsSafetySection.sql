SET define off;
---adding the dropdown options to the What critera are you using for classifying adverse events dropdown.

UPDATE ER_CODELST
SET CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "CTC2.0">CTC2.0</OPTION>
<OPTION value = "CTCAE3.0">CTCAE3.0</OPTION>
<OPTION value = "CTCAE3.M10">CTCAE3.M10</OPTION>
<OPTION value = "CTCAE4.03">CTCAE4.03</OPTION>
<OPTION value = "Other">Other</OPTION>
</SELECT>'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP = 'critAdvEvents';


UPDATE ER_CODELST
SET CODELST_CUSTOM_COL = 'textarea'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP = 'critAdvEvntsOth';
COMMIT;



