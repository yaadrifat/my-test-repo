SET define off;
---Changing the label for this field , as specified in the doc for LINDFT submission logic


UPDATE ER_CODELST
SET CODELST_DESC = 'Are you requesting an expedited biostatistical review for this protocol?'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP = 'expdBiostat';
COMMIT;



