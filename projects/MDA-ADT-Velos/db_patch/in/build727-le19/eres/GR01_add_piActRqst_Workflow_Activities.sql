set define off;

declare
v_exists NUMBER := 0;
v_nextVal NUMBER := 0;

v_PK_WORKFLOW NUMBER := 0;
v_PK_ACTIVITY NUMBER := 0;
v_PK_WORKFLOWACTIVITY NUMBER := 0;

begin
	v_PK_WORKFLOW := 0;
	SELECT count(*) INTO v_exists FROM WORKFLOW WHERE WORKFLOW_TYPE = 'STUDY_ACTIVATION';
	
	IF (v_exists = 0) THEN
		v_PK_WORKFLOW := SQ_WORKFLOW.NEXTVAL();

		Insert into WORKFLOW (PK_WORKFLOW,WORKFLOW_NAME,WORKFLOW_DESC,WORKFLOW_TYPE) 
		values (v_PK_WORKFLOW,'Study Activation','Study Activation','STUDY_ACTIVATION');
		COMMIT;
	ELSE 
		SELECT PK_WORKFLOW INTO v_PK_WORKFLOW FROM WORKFLOW WHERE WORKFLOW_TYPE = 'STUDY_ACTIVATION';
	END IF;
	
	---------------------------------------------------------------------------------------------------------------------
	--Adding the data for the activity named "PI Activation Request" in the activity and workflow_activity table            ---
	---------------------------------------------------------------------------------------------------------------------
	
	v_PK_ACTIVITY := 0;
	SELECT count(*) INTO v_exists FROM ACTIVITY WHERE ACTIVITY_NAME = 'piActRqst';
	
	IF (v_exists = 0) THEN
		v_PK_ACTIVITY := SQ_ACTIVITY.NEXTVAL();
		
		Insert into ACTIVITY (PK_ACTIVITY,ACTIVITY_NAME,ACTIVITY_DESC) 
		values (v_PK_ACTIVITY,'piActRqst','PI Activation Request');
		COMMIT;
	ELSE
		SELECT PK_ACTIVITY INTO v_PK_ACTIVITY FROM ACTIVITY WHERE ACTIVITY_NAME = 'piActRqst';
	END IF;
	
	v_PK_WORKFLOWACTIVITY := 0;
	IF (v_PK_ACTIVITY > 0) THEN
		SELECT COUNT(*) INTO v_exists FROM WORKFLOW_ACTIVITY WHERE FK_WORKFLOWID = v_PK_WORKFLOW AND FK_ACTIVITYID = v_PK_ACTIVITY;
	
		IF (v_exists = 0) THEN
			v_PK_WORKFLOWACTIVITY := SQ_WORKFLOWACTIVITY.NEXTVAL();
			
			Insert into WORKFLOW_ACTIVITY (PK_WORKFLOWACTIVITY,WA_NAME,FK_WORKFLOWID,FK_ACTIVITYID,WA_SEQUENCE,WA_TERMINATEFLAG,WA_DISPLAYQUERY) 
			values (v_PK_WORKFLOWACTIVITY,'PI Activation Request',v_PK_WORKFLOW,v_PK_ACTIVITY,2,0,''--'SELECT PKG_WORKFLOW_RULES_MDA.F_studyActive_PiActRqst(:1) FROM DUAL'
			);
			COMMIT;
		END IF;
	END IF;
	
end;
/
