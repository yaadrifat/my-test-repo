set define off;

DECLARE
  v_table_exists number := 0;  
BEGIN
  select count(*) into v_table_exists from sys.user_tables where table_name = 'ER_RESUBM_DRAFT';

  if (v_table_exists = 0) then
      execute immediate 'CREATE TABLE ERES.ER_RESUBM_DRAFT AS '
      || 'SELECT * FROM ERES.UI_FLX_PAGEVER WHERE PAGE_MOD_TABLE = ''ABC''';
  end if;
end;
/

COMMENT ON TABLE 
ERES.ER_RESUBM_DRAFT IS 
'This table stores re-submission draft data.';

