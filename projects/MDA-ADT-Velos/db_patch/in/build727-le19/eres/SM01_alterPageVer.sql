set define off;

DECLARE
  v_column_exists number := 0;  
BEGIN
  select count(*) into v_column_exists from user_tab_cols 
  where column_name = 'PAGE_FORMRESPONSES' and table_name = 'UI_FLX_PAGEVER';

  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.UI_FLX_PAGEVER ADD (PAGE_FORMRESPONSES CLOB)';
  end if;
end;
/

COMMENT ON COLUMN 
ERES.UI_FLX_PAGEVER.PAGE_FORMRESPONSES IS 
'This column stores the form responses archives in json array format';


DECLARE
  v_column_exists number := 0;  
BEGIN
  select count(*) into v_column_exists from user_tab_cols 
  where column_name = 'PAGE_FORMRESPDIFF' and table_name = 'UI_FLX_PAGEVER';

  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.UI_FLX_PAGEVER ADD (PAGE_FORMRESPDIFF CLOB)';
  end if;
end;
/

COMMENT ON COLUMN 
ERES.UI_FLX_PAGEVER.PAGE_FORMRESPDIFF IS 
'This column stores the form responses diff in json array format';


DECLARE
  v_column_exists number := 0;  
BEGIN
  select count(*) into v_column_exists from user_tab_cols 
  where column_name = 'PAGE_COMMENTS' and table_name = 'UI_FLX_PAGEVER';

  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.UI_FLX_PAGEVER ADD (PAGE_COMMENTS CLOB)';
  end if;
end;
/

COMMENT ON COLUMN 
ERES.UI_FLX_PAGEVER.PAGE_COMMENTS IS 
'This column stores the version comments';
