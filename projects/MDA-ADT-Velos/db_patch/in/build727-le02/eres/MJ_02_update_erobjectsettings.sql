update ER_OBJECT_SETTINGS set OBJECT_DISPLAYTEXT = 'New Protocol' where OBJECT_NAME = 'irb_menu' and OBJECT_SUBTYPE = 'new_menu';

-- Fixed Bug # 20707 
update ER_OBJECT_SETTINGS set object_sequence= 7  where object_name='irb_new_tab' and object_subtype='irb_approv_tab';

-- Fixed Bug # 20516
update ER_OBJECT_SETTINGS set OBJECT_DISPLAYTEXT = 'Attachments' where object_name='irb_new_tab' and object_subtype='irb_upload_tab' and fk_account=0;

commit;