INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1)
VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'dept_chairs', 'Department Chair',1, 'lookup', '{lookupPK:6000, selection:"multi", mapping:[{source:"USRFULLNAME", target:"alternateId"},{source:"USRLOGIN", target:"alternateId16804"}]}');

INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL)
VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'dept_chairs_ids', 'Department Chair User ID','N',1, 'readonly-input');

commit;