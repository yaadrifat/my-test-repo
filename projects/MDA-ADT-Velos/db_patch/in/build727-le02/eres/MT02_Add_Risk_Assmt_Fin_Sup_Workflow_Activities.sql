set define off;

declare
v_exists NUMBER := 0;
v_nextVal NUMBER := 0;

v_PK_WORKFLOW NUMBER := 0;
v_PK_ACTIVITY NUMBER := 0;
v_PK_WORKFLOWACTIVITY NUMBER := 0;

begin
	v_PK_WORKFLOW := 0;
	SELECT count(*) INTO v_exists FROM WORKFLOW WHERE WORKFLOW_TYPE = 'STUDY_INITIATION';
	
	IF (v_exists = 0) THEN
		v_PK_WORKFLOW := SQ_WORKFLOW.NEXTVAL();

		Insert into WORKFLOW (PK_WORKFLOW,WORKFLOW_NAME,WORKFLOW_DESC,WORKFLOW_TYPE) 
		values (v_PK_WORKFLOW,'Study Initiation','Study Initiation','STUDY_INITIATION');
		COMMIT;
	ELSE 
		SELECT PK_WORKFLOW INTO v_PK_WORKFLOW FROM WORKFLOW WHERE WORKFLOW_TYPE = 'STUDY_INITIATION';
	END IF;
	
	---------------------------------------------------------------------------------------------------------------------
	--Adding the data for the activity named "Risk Assessment" in the activity and workflow_activity table            ---
	---------------------------------------------------------------------------------------------------------------------
	
	v_PK_ACTIVITY := 0;
	SELECT count(*) INTO v_exists FROM ACTIVITY WHERE ACTIVITY_NAME = 'RiskAssmt';
	
	IF (v_exists = 0) THEN
		v_PK_ACTIVITY := SQ_ACTIVITY.NEXTVAL();
		
		Insert into ACTIVITY (PK_ACTIVITY,ACTIVITY_NAME,ACTIVITY_DESC) 
		values (v_PK_ACTIVITY,'RiskAssmt','Risk Assessment');
		COMMIT;
	ELSE
		SELECT PK_ACTIVITY INTO v_PK_ACTIVITY FROM ACTIVITY WHERE ACTIVITY_NAME = 'RiskAssmt';
	END IF;
	
	v_PK_WORKFLOWACTIVITY := 0;
	IF (v_PK_ACTIVITY > 0) THEN
		SELECT COUNT(*) INTO v_exists FROM WORKFLOW_ACTIVITY WHERE FK_WORKFLOWID = v_PK_WORKFLOW AND FK_ACTIVITYID = v_PK_ACTIVITY;
	
		IF (v_exists = 0) THEN
			v_PK_WORKFLOWACTIVITY := SQ_WORKFLOWACTIVITY.NEXTVAL();
			
			Insert into WORKFLOW_ACTIVITY (PK_WORKFLOWACTIVITY,WA_NAME,FK_WORKFLOWID,FK_ACTIVITYID,WA_SEQUENCE,WA_TERMINATEFLAG,WA_DISPLAYQUERY) 
			values (v_PK_WORKFLOWACTIVITY,'Risk Assessment',v_PK_WORKFLOW,v_PK_ACTIVITY,2,0,'SELECT PKG_WORKFLOW_RULES_MDA.F_studyInit_RiskAssmt(:1) FROM DUAL');
			COMMIT;
		END IF;
	END IF;
	
	---------------------------------------------------------------------------------------------------------------------
	--Adding the data for the activity named "Financial Support" in the activity and workflow_activity table            ---
	---------------------------------------------------------------------------------------------------------------------
	v_PK_ACTIVITY := 0;
	SELECT count(*) INTO v_exists FROM ACTIVITY WHERE ACTIVITY_NAME = 'FinSupport';

	IF (v_exists = 0) THEN
		v_PK_ACTIVITY := SQ_ACTIVITY.NEXTVAL();
		
		Insert into ACTIVITY (PK_ACTIVITY,ACTIVITY_NAME,ACTIVITY_DESC) 
		values (v_PK_ACTIVITY,'FinSupport','Financial Support');
		COMMIT;
	ELSE
		SELECT PK_ACTIVITY INTO v_PK_ACTIVITY FROM ACTIVITY WHERE ACTIVITY_NAME = 'FinSupport';
	END IF;
	
	v_PK_WORKFLOWACTIVITY := 0;
	IF (v_PK_ACTIVITY > 0) THEN
		SELECT COUNT(*) INTO v_exists FROM WORKFLOW_ACTIVITY WHERE FK_WORKFLOWID = v_PK_WORKFLOW AND FK_ACTIVITYID = v_PK_ACTIVITY;
		
		IF (v_exists = 0) THEN
			v_PK_WORKFLOWACTIVITY := SQ_WORKFLOWACTIVITY.NEXTVAL();
			
			Insert into WORKFLOW_ACTIVITY (PK_WORKFLOWACTIVITY,WA_NAME,FK_WORKFLOWID,FK_ACTIVITYID,WA_SEQUENCE,WA_TERMINATEFLAG,WA_DISPLAYQUERY) 
			values (v_PK_WORKFLOWACTIVITY,'Financial Support',v_PK_WORKFLOW,v_PK_ACTIVITY,3,0,'SELECT PKG_WORKFLOW_RULES_MDA.F_studyInit_FinSupport(:1) FROM DUAL');		
			COMMIT;
		END IF;
	END IF;
	
end;
/
