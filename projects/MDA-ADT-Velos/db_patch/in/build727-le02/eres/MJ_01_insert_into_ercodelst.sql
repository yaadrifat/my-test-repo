-- Fixed bug # 20818
set define off;
set serveroutput on;

declare
v_CodeExists number := 0;
begin
	
SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'role' AND CODELST_SUBTYP = 'Collaborators';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_hide)
        values (seq_er_codelst.nextval, 'role', 'Collaborators', 'Collaborators', 'N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	
COMMIT;
END;
/