set define off;

update WORKFLOW_ACTIVITY  set wa_sequence = 11 where WA_NAME = 'Risk Assessment' ;
update WORKFLOW_ACTIVITY  set wa_sequence = 12 where WA_NAME = 'Financial Support' ;
update WORKFLOW_ACTIVITY  set wa_sequence = 13 where WA_NAME = 'CRC Review' ;
update WORKFLOW_ACTIVITY  set wa_sequence = 14 where WA_NAME = 'IRB Review' ;

commit;