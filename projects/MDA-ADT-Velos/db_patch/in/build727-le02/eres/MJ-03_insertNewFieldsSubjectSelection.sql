set define off;
set serveroutput on;

-- Added these 3 fields in Subject selection.
-->Inclusion Criteria - textarea
-->Inclusion Criteria N/A - textarea
-->Exclusion Criteria - textarea
-->Exclusion Criteria N/A - textarea

declare
v_CodeExists number := 0;
begin
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'incluCriteria';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'incluCriteria', 'Inclusion Criteria', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'incluCriteriaNa';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'incluCriteriaNa', 'Inclusion Criteria N/A', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;	
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'excluCriteria';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'excluCriteria', 'Exclusion Criteria', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'excluCriteria';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'excluCriteriaNa', 'Exclusion Criteria N/A', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;	
	
COMMIT;
END;
/

