Made changes as per Stefi Panit's email:

As discussed during our Project Management meeting on Friday, we will be removing the current fields in 
Subject Selection, and instead have four free text boxes labeled as Inclusion Criteria, 
Inclusion Criteria N/A, Exclusion Criteria, and Exclusion Criteria N/A. 