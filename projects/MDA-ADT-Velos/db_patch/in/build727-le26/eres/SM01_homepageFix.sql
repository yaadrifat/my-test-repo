set define off;

declare
v_id NUMBER :=0;
begin
	select pk_codelst into v_id from er_codelst where codelst_type = 'theme' and codelst_subtyp = 'th_default';
	
	if (v_id > 0) then
		update er_user set fk_codelst_theme = v_id where fk_codelst_theme in (select pk_codelst from er_codelst where codelst_type = 'theme' and codelst_subtyp = 'th_irb');
	end if;

	update er_codelst set codelst_hide = 'Y' where codelst_type = 'theme' and codelst_subtyp = 'th_irb';
	commit;
end;
/