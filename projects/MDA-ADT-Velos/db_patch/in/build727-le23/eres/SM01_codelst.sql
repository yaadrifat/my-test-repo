set define off;

update er_codelst set codelst_custom_col='hidden-input' where codelst_type = 'studyidtype' and codelst_subtyp='nom_alt_rev'; 

commit;


update er_codelst set codelst_desc = 'Data Safety and Monitoring Plan' where codelst_type ='section' and codelst_subtyp ='section_6_5';

commit;