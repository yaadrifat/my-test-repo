set define off;

update er_codelst set codelst_desc = '{chkArray:[
{data: "option1", display:"Bypass CRC/PBHSRC Review<br>This study is NCI-sponsored, CTEP-approved, and/or NCI cooperative group-approved trial, or peer-reviwed by an NIH-approved mechanism and IRB-approved at an NCI designated cancer center. ---- Please attach appropriate documentation (e.g. NCI-approval letter or IRB approval memo) to support the request to bypass CRC/PBHSRC review.----"},
{data: "option2", display:"Expedited Biostatistical Review<br>This study is an industry-sponsored, multi-center clinical trial already activated at an NCI designated cancer center and/or has been approved by the Office of the Vice Provost for Fast-Track designation? ----- Please attached appropriate documentation (e.g. Sponsor memorandum,ClinicalTrial.gov listing verifying activation at an NCI-designated cancer center, OVP Fast Track Approval memorandum) to support the request for expedited biostatistical review.-----"}
]}' where codelst_type = 'studyidtype' and codelst_subtyp = 'expdBiostat';

commit;