-- Fixed Bug # 21272

--Dropdown values should be:  
-- Old ones:
-- Protocol
-- Investigational Brochure
-- Protocol Prioritization

-- New ones
-- External Collaborator Signature
-- Toxicity Form
-- Sponsor Provided Files
-- Other Documents

set define off;
set serveroutput on;

declare
v_CodeExists number := 0;
begin
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyvercat' AND CODELST_SUBTYP = 'collab_sign';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_hide, codelst_seq)
        values (seq_er_codelst.nextval, 'studyvercat', 'collab_sign', 'External Collaborator Signature', 'N', 2);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyvercat' AND CODELST_SUBTYP = 'other_docs';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_hide, codelst_seq)
        values (seq_er_codelst.nextval, 'studyvercat', 'other_docs', 'Other Documents', 'N', 6);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyvercat' AND CODELST_SUBTYP = 'toxic_form';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_hide, codelst_seq)
        values (seq_er_codelst.nextval, 'studyvercat', 'toxic_form', 'Toxicity Form', 'N', 3);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyvercat' AND CODELST_SUBTYP = 'spons_file';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_hide, codelst_seq)
        values (seq_er_codelst.nextval, 'studyvercat', 'spons_file', 'Sponsor Provided Files', 'N', 4);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	
COMMIT;
END;
/