-- Fixed Bug # 21272

--Dropdown values should be:  
-- Old ones:
-- Protocol
-- Investigational Brochure
-- Protocol Prioritization
--Updates: Jerd Phichitkul 2015-02-12 13:21:59 PST (Currently 'Department Chair Review' category is passing the document to Click
--instead of the 'Protocol Prioritization' category. )
--1. Hide 'Protocol Prioritization' category.
--2. Rename 'Department Chair Review' to 'Protocol Prioritization'.

update er_codelst
set codelst_hide='Y'
where codelst_type='studyvercat'
and codelst_subtyp='prot_priority';

update er_codelst
set codelst_hide='Y'
where codelst_type='studyvercat'
and codelst_subtyp='inf_consent';

update er_codelst
set codelst_desc='Protocol Prioritization'
where codelst_type='studyvercat'
and codelst_subtyp='dept_chair_id';

commit;

