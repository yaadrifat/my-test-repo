set define off;
set serveroutput on;

update ER_CODELST set CODELST_HIDE='N' where
CODELST_TYPE='randomization' and CODELST_SUBTYP='blckRndmztn';

update ER_CODELST set CODELST_DESC = 'If investigational, provide list of anticipated AEs' where
CODELST_TYPE='studyidtype' and CODELST_SUBTYP='invAntcpAEs';

update ER_CODELST set CODELST_DESC = 'Report of SAEs and Unanticipated Problems' where
CODELST_TYPE='studyidtype' and CODELST_SUBTYP='repSAEs';

update ER_CODELST set CODELST_HIDE = 'Y' where
CODELST_TYPE='blinding' and CODELST_SUBTYP='blinded';

declare
v_CodeExists number := 0;
begin
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'blinding' AND CODELST_SUBTYP = 'doubleBlind';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_hide, codelst_seq)
        values (seq_er_codelst.nextval, 'blinding', 'doubleBlind', 'Double-Blind', 'N', 2);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	commit;
end;
/
