set define off;

update er_codelst set codelst_custom_col1 = '<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>' where codelst_type ='studyidtype' and codelst_subtyp = 'knownBiomrkr';

commit;

update er_codelst set codelst_custom_col1 = '<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>' where codelst_type ='studyidtype' and codelst_subtyp = 'prot_req_ind';

commit;

declare
v_exists number :=0;
begin
	select count(*) into v_exists from er_codelst where codelst_type='LindBiomrkrType' and codelst_subtyp ='gene'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindBiomrkrType' ,'gene','Gene',1);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindBiomrkrType' and codelst_subtyp ='microrna'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindBiomrkrType' ,'microrna','Micro RNA',2);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindBiomrkrType' and codelst_subtyp ='snp'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindBiomrkrType' ,'snp','SNP-Small Nucleotide Polymorphism',3);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindBiomrkrType' and codelst_subtyp ='other'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindBiomrkrType' ,'other','Other',4);
	END IF;
	commit;
end;
/


update er_codelst set codelst_custom_col1 = '{codelst_type:"LindBiomrkrType"}' where codelst_type='studyidtype' and codelst_subtyp='biomrkrType';

commit;

