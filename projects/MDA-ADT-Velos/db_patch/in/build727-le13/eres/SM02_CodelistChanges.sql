set define off;

update er_codelst set codelst_desc = 'COEUS Number' where codelst_type = 'studyidtype' and codelst_subtyp = 'grant';

update er_codelst set codelst_desc = 'If gene, use www.genecard.org to pull name from list' where codelst_type = 'studyidtype' and codelst_subtyp = 'ifGeneList';

commit;

--NCI program codes

Update er_codelst set codelst_seq = 1 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and codelst_subtyp = 'CCR';
Update er_codelst set codelst_seq = 3 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and codelst_subtyp = 'CDP';
Update er_codelst set codelst_seq = 4 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and codelst_subtyp = 'CIP';
Update er_codelst set codelst_seq = 5 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and codelst_subtyp = 'CTEP';
Update er_codelst set codelst_seq = 6 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and codelst_subtyp = 'DCB';
Update er_codelst set codelst_seq = 7 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and codelst_subtyp = 'DCCPS';
Update er_codelst set codelst_seq = 8 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and codelst_subtyp = 'DCEG';
Update er_codelst set codelst_seq = 9 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and codelst_subtyp = 'DCP';
Update er_codelst set codelst_seq = 10 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and codelst_subtyp = 'DEA';
Update er_codelst set codelst_seq = 11 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and codelst_subtyp = 'DTP';
Update er_codelst set codelst_seq = 12 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and codelst_subtyp = 'N/A';
Update er_codelst set codelst_seq = 14, codelst_desc='OSB/SPORE' where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and codelst_subtyp = 'OSB/SPOREs';
Update er_codelst set codelst_seq = 15 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and codelst_subtyp = 'RRP';
Update er_codelst set codelst_seq = 16 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and codelst_subtyp = 'TRP';
commit;

declare
v_exists integer := 0;
begin
	select count(*) into v_exists from er_codelst where CODELST_TYPE ='ProgramCode' AND CODELST_DESC ='CCT/CTB';
	if (v_exists <= 0) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, codelst_seq ) 
		values (seq_er_codelst.nextval,'ProgramCode' ,'CCTCTB', 'CCT/CTB', 'NCI',2); 
	else 
		Update er_codelst set codelst_seq = 2, codelst_subtyp = 'CCTCTB' where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and CODELST_DESC ='CCT/CTB';
	end if;
	commit;

	select count(*) into v_exists from er_codelst where CODELST_TYPE ='ProgramCode' AND CODELST_DESC ='OD';
	if (v_exists <= 0) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, codelst_seq ) 
		values (seq_er_codelst.nextval,'ProgramCode','OD', 'OD', 'NCI',13); 
	else
		Update er_codelst set codelst_seq = 13, codelst_subtyp = 'OD' where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and CODELST_DESC ='OD';
	end if;
	commit;
end;
/

--NIH program codes

Update er_codelst set codelst_desc='NIH Office of the Director (OD)',codelst_seq = 1 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'OD';
Update er_codelst set codelst_desc='National Eye Institute (NEI)',codelst_seq = 3 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NEI';
Update er_codelst set codelst_desc='National Heart, Lung, and Blood Institute (NHLBI)',codelst_seq = 4 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NHLBI';
Update er_codelst set codelst_desc='National Human Genome Research Institute (NHGRI)',codelst_seq = 5 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NHGRI';
Update er_codelst set codelst_desc='National Institute on Aging (NIA)',codelst_seq = 6 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NIA';
Update er_codelst set codelst_desc='National Institute on Alcohol Abuse and Alcoholism (NIAAA)',codelst_seq = 7 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NIAAA';
Update er_codelst set codelst_desc='National Institute of Allergy and Infectious Diseases (NIAID)',codelst_seq = 8 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NIAID';
Update er_codelst set codelst_desc='National Institute of Arthritis and Musculoskeletal and Skin Diseases (NIAMS)',codelst_seq = 9 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NIAMS';
Update er_codelst set codelst_desc='National Institute of Biomedical Imaging and Bioengineering (NIBIB)',codelst_seq = 10 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NIBIB';
Update er_codelst set codelst_desc='Eunice Kennedy Shriver National Institute of Child Health and Human Development (NICHD)',codelst_seq = 11 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NICHD';
Update er_codelst set codelst_desc='National Institute on Deafness and Other Communication Disorders (NIDCD)',codelst_seq = 12 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NIDCD';
Update er_codelst set codelst_desc='National Institute of Dental and Craniofacial Research (NIDCR)',codelst_seq = 13 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NIDCR';
Update er_codelst set codelst_desc='National Institute of Diabetes and Digestive and Kidney Diseases (NIDDK)',codelst_seq = 14 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NIDDK';
Update er_codelst set codelst_desc='National Institute on Drug Abuse (NIDA) ',codelst_seq = 15 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NIDA';
Update er_codelst set codelst_desc='National Institute of Environmental Health Sciences (NIEHS)',codelst_seq = 16 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NIEHS';
Update er_codelst set codelst_desc='National Institute of General Medical Sciences (NIGMS)',codelst_seq = 17 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NIGMS';
Update er_codelst set codelst_desc='National Institute of Mental Health (NIMH)',codelst_seq = 18 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NIMH';
Update er_codelst set codelst_desc='National Institute of Neurological Disorders and Stroke (NINDS) ',codelst_seq = 20 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NINDS';
Update er_codelst set codelst_desc='National Institute of Nursing Research (NINR)',codelst_seq = 21 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NINR';
Update er_codelst set codelst_desc='National Library of Medicine (NLM)',codelst_seq = 22 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NLM';
Update er_codelst set codelst_desc='Center for Information Technology (CIT)',codelst_seq = 23 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'CIT';
Update er_codelst set codelst_desc='Center for Scientific Review (CSR)',codelst_seq = 24 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'CSR';
Update er_codelst set codelst_desc='Fogarty International Center (FIC)',codelst_seq = 25 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'FIC';
Update er_codelst set codelst_desc='NIH Clinical Center (CC)',codelst_seq = 28 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'CC';
commit;


declare
v_exists integer := 0;
begin
	select count(*) into v_exists from er_codelst where CODELST_TYPE ='ProgramCode' AND CODELST_CUSTOM_COL='NIH' AND CODELST_DESC ='National Cancer Institute (NCI)';
	if (v_exists <= 0) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, codelst_seq ) 
		values (seq_er_codelst.nextval,'ProgramCode','NCI', 'National Cancer Institute (NCI)', 'NIH',2);
	else 
		Update er_codelst set codelst_seq = 2, codelst_subtyp = 'NCI' where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' 
		and CODELST_DESC ='National Cancer Institute (NCI)';
	end if;

	select count(*) into v_exists from er_codelst where CODELST_TYPE ='ProgramCode' AND CODELST_CUSTOM_COL='NIH' AND CODELST_DESC ='National Institute on Minority Health and Health Disparities (NIMHD)';
	if (v_exists <= 0) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, codelst_seq ) 
		values (seq_er_codelst.nextval,'ProgramCode','NIMHD', 'National Institute on Minority Health and Health Disparities (NIMHD)', 'NIH',19);
	else 
		Update er_codelst set codelst_seq = 19, codelst_subtyp = 'NIMHD' where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' 
		and CODELST_DESC ='National Institute on Minority Health and Health Disparities (NIMHD)';
	end if;

	select count(*) into v_exists from er_codelst where CODELST_TYPE ='ProgramCode' AND CODELST_CUSTOM_COL='NIH' 
	AND trim(CODELST_DESC) ='National Center for Complementary and Integrative Health (NCCIH)';
	if (v_exists <= 0) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, codelst_seq ) 
		values (seq_er_codelst.nextval,'ProgramCode','NCCIH', 'National Center for Complementary and Integrative Health (NCCIH)', 'NIH',26);
	else
		Update er_codelst set codelst_seq = 26, codelst_subtyp = 'NCCIH' where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and trim(CODELST_DESC) ='National Center for Complementary and Integrative Health (NCCIH)';
	end if;
	
	select count(*) into v_exists from er_codelst where CODELST_TYPE ='ProgramCode' AND CODELST_CUSTOM_COL='NIH' 
	AND CODELST_DESC ='National Center for Advancing Translational Sciences (NCATS)';
	if (v_exists <= 0) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, codelst_seq ) 
		values (seq_er_codelst.nextval,'ProgramCode','NCATS', 'National Center for Advancing Translational Sciences (NCATS)', 'NIH',27);
	else
		Update er_codelst set codelst_seq = 27, codelst_subtyp = 'NCATS' where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and CODELST_DESC ='National Center for Advancing Translational Sciences (NCATS)';
	end if;
	commit;
end;
/

Update er_codelst set codelst_hide='Y' where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NCCAM';
Update er_codelst set codelst_hide='Y' where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NCMHD';
Update er_codelst set codelst_hide='Y' where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NCRR';
commit;


