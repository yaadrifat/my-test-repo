set define off;

update ER_CODELST set CODELST_HIDE='Y' where
CODELST_TYPE='studyvercat' and CODELST_SUBTYP='studyvercat_5';

update ER_CODELST set CODELST_HIDE='Y' where
CODELST_TYPE='studyvercat' and CODELST_SUBTYP='studyvercat_6';

commit;
