set define off;

DECLARE
  v_column_exists number := 0;  
BEGIN
  select count(*) into v_column_exists from user_tab_cols 
  where column_name = 'PAGE_MOD_STAT_SUBTYP' and table_name = 'UI_FLX_PAGEVER';

  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.UI_FLX_PAGEVER ADD (PAGE_MOD_STAT_SUBTYP VARCHAR(15))';
  end if;
end;
/

COMMENT ON COLUMN 
ERES.UI_FLX_PAGEVER.PAGE_MOD_STAT_SUBTYP IS 
'This column stores the codelst subtyp of the status of the respective module mentioned in the PAGE_MOD_TABLE column of this table';
