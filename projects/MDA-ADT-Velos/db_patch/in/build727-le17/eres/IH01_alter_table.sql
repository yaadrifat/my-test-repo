set define off;
set serveroutput on;

DECLARE
  v_item_exists number := 0;  
BEGIN
select count(*) into v_item_exists from user_tab_cols where TABLE_NAME = 'ER_STUDYVER'
and COLUMN_NAME = 'STUDYVER_MAJORVER';
if (v_item_exists = 0) then
  execute immediate 'ALTER TABLE ERES.ER_STUDYVER ADD (STUDYVER_MAJORVER NUMBER DEFAULT 0)';
  dbms_output.put_line('Col STUDYVER_MAJORVER added');
else
  dbms_output.put_line('Col STUDYVER_MAJORVER already exists');
end if;

END;
/

DECLARE
  v_item_exists number := 0;  
BEGIN
select count(*) into v_item_exists from user_tab_cols where TABLE_NAME = 'ER_STUDYVER'
and COLUMN_NAME = 'STUDYVER_MINORVER';
if (v_item_exists = 0) then
  execute immediate 'ALTER TABLE ERES.ER_STUDYVER ADD (STUDYVER_MINORVER NUMBER DEFAULT 0)';
  dbms_output.put_line('Col STUDYVER_MINORVER added');
else
  dbms_output.put_line('Col STUDYVER_MINORVER already exists');
end if;

END;
/
