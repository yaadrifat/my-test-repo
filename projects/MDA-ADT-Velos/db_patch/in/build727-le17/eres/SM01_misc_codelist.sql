set define off;

update er_codelst set codelst_custom_col = 'textarea' where 
codelst_type ='studyidtype' and codelst_subtyp in ('dose','doseRationale');

commit;