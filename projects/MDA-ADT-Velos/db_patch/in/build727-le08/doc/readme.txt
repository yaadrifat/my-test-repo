In the file labelBundle_custom.properties , make sure you have the following key value pairs:

    L_CCSG_DT_Reportable=Protocol requires signed informed consent?
	L_Study_Type=Clinical Research Category
	L_ByStd_Type=By Study Clinical Research Category
	L_Research_Type=Study Source
	L_ByResearch_Type=By Study Source	
	
If the keys exist, but they have different values, please change them to what is provided above.

This takes care of the following bugs :
Bug id 20867 : Field: "Study Type" :Rename to Clinical Research Category    
Bug id 20848 : Field: "CCSG Data Table 4 Reportable" Rename the field to Protocol requires signed informed consent?    