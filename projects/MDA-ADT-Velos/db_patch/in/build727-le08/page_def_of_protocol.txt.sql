    {
        "name": "Study Screen",
        "sections": [
            {
                "title": "Study IDs",
                "secType": "generic",
                "seq": 1,
                "secSpecial": true,
                "fields": [
                    {
                        "flxFieldId": "spnsrdProt",
                        "keyword": "spnsrdProt",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "STUDY_SPONSORID",
                        "keyword": "STUDY_SPONSORID",
                        "type": "STD"
                    },
                    {
                        "flxFieldId": "STUDY_STUDYNUMBER",
                        "keyword": "STUDY_STUDYNUMBER",
                        "type": "STD"
                    },
                    {
                        "flxFieldId": "STUDY_STUDYTITLE",
                        "keyword": "STUDY_STUDYTITLE",
                        "type": "STD"
                    },
                    {
                        "flxFieldId": "STUDY_DATAMANAGER",
                        "keyword": "STUDY_DATAMANAGER",
                        "type": "STD"
                    },
                    {
                        "flxFieldId": "STUDY_STUDYCONTACT",
                        "keyword": "STUDY_STUDYCONTACT",
                        "type": "STD"
                    },
                    {
                        "flxFieldId": "STUDY_PIAUTHOR",
                        "keyword": "STUDY_PIAUTHOR",
                        "mandatory": 1,
                        "type": "STD"
                    },
                    {
                        "flxFieldId": "STUDY_DIVISION",
                        "keyword": "STUDY_DIVISION",
                        "type": "STD"
                    },
                    {
                        "flxFieldId": "STUDY_TAREA",
                        "keyword": "STUDY_TAREA",
                        "type": "STD"
                    },
                    {
                        "flxFieldId": "studyAdmDiv",
                        "keyword": "studyAdmDiv",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "studyCond2",
                        "keyword": "studyCond2",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "studyCond1",
                        "keyword": "studyCond1",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "STUDY_CCSG",
                        "keyword": "STUDY_CCSG",
                        "type": "STD"
                    },
                    {
                        "flxFieldId": "STUDY_RESEARCHTYPE",
                        "keyword": "STUDY_RESEARCHTYPE",
                        "type": "STD"
                    },
                    {
                        "flxFieldId": "STUDY_STUDYTYPE",
                        "keyword": "STUDY_STUDYTYPE",
                        "type": "STD"
                    },
                    {
                        "flxFieldId": "STUDY_PPURPOSE",
                        "keyword": "STUDY_PPURPOSE",
                        "type": "STD"
                    },
                    {
                        "flxFieldId": "stdyIncludes",
                        "keyword": "stdyIncludes",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "expdBiostat",
                        "keyword": "expdBiostat",
                        "type": "MSD"
                    }
                ]
            },
            {
                "title": "Study Objectives",
                "secType": "generic",
                "seq": 2,
                "fields": [
                    {
                        "flxFieldId": "STUDY_OBJECTIVE",
                        "keyword": "STUDY_OBJECTIVE",
                        "type": "STD"
                    },
                    {
                        "flxFieldId": "selOthrObjEntry",
                        "keyword": "selOthrObjEntry",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "secObjective",
                        "keyword": "secObjective",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "explrtObjctv",
                        "keyword": "explrtObjctv",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "biomrkrObj",
                        "keyword": "biomrkrObj",
                        "type": "MSD"
                    }
                ]
            },
            {
                "title": "Study Design",
                "seq": 3,
                "fields": [
                    {
                        "flxFieldId": "STUDY_PHASE",
                        "keyword": "STUDY_PHASE",
                        "type": "STD"
                    },
                    {
                        "flxFieldId": "STUDY_RANDOMIZATION",
                        "keyword": "STUDY_RANDOMIZATION",
                        "type": "STD"
                    },
                    {
                        "flxFieldId": "rndmnTrtGrp",
                        "keyword": "rndmnTrtGrp",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "STUDY_BLINDING",
                        "keyword": "STUDY_BLINDING",
                        "type": "STD"
                    },
                    {
                        "flxFieldId": "blndTrtGrp",
                        "keyword": "blndTrtGrp",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "STUDY_STUDYSCOPE",
                        "keyword": "STUDY_STUDYSCOPE",
                        "type": "STD"
                    },
                    {
                        "flxFieldId": "accrual1",
                        "keyword": "accrual1",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "STUDY_SAMPLESIZE",
                        "keyword": "STUDY_SAMPLESIZE",
                        "type": "STD"
                    },
                    {
                        "flxFieldId": "accrual12",
                        "keyword": "accrual12",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "statCons2",
                        "keyword": "statCons2",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "trtmntArms",
                        "keyword": "trtmntArms",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "STUDY_DISEASESITES",
                        "keyword": "STUDY_DISEASESITES",
                        "type": "STD"
                    },
                    {
                        "flxFieldId": "STUDY_SPECIFICSITES1",
                        "keyword": "STUDY_SPECIFICSITES1",
                        "type": "STD"
                    },
                    {
                        "flxFieldId": "STUDY_SPECIFICSITES2",
                        "keyword": "STUDY_SPECIFICSITES2",
                        "type": "STD"
                    }
                ]
            },
            {
                "title": "Background",
                "secType": "generic",
                "seq": 4,
                "fields": [
                    {
                        "flxFieldId": "section_3_2",
                        "keyword": "section_3_2",
                        "type": "studySection"
                    },
                    {
                        "flxFieldId": "section_1_2",
                        "keyword": "section_1_2",
                        "type": "studySection"
                    },
                    {
                        "flxFieldId": "section_1_3",
                        "keyword": "section_1_3",
                        "type": "studySection"
                    }
                ]
            },
            {
                "title": "Locations, Study Team and Collaborators",
                "secModule": "STUDY_STUDYTEAM",
                "secType": "predef",
                "seq": 5,
                "fields": []
            },
            {
                "title": "Subject Selection",
                "secType": "generic",
                "seq": 6,
                "fields": [
                    {
                        "flxFieldId": "incluCriteria",
                        "keyword": "incluCriteria",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "incluCriteriaNa",
                        "keyword": "incluCriteriaNa",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "excluCriteria",
                        "keyword": "excluCriteria",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "excluCriteriaNa",
                        "keyword": "excluCriteriaNa",
                        "type": "MSD"
                    }
                ]
            },
            {
                "title": "IND/IDE Information",
                "secModule": "STUDY_STUDYINDIDE",
                "secType": "predef",
                "seq": 7,
                "fields": []
            },
            {
                "title": "IND Exempt",
                "secType": "generic",
                "seq": 8,
                "fields": [
                    {
                        "flxFieldId": "prot_req_ind",
                        "keyword": "prot_req_ind",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "meet_exem_crit",
                        "keyword": "meet_exem_crit",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "exem_ratnl",
                        "keyword": "exem_ratnl",
                        "type": "MSD"
                    }
                ]
            },
            {
                "title": "Study Drug",
                "secType": "generic",
                "seq": 9,
                "fields": [
                    {
                        "flxFieldId": "drugAgent1",
                        "keyword": "drugAgent1",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "dose",
                        "keyword": "dose",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "doseRationale",
                        "keyword": "doseRationale",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "route",
                        "keyword": "route",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "drugMnftr1",
                        "keyword": "drugMnftr1",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "drugAgent2",
                        "keyword": "drugAgent2",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "dose2",
                        "keyword": "dose2",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "doseRatnl2",
                        "keyword": "doseRatnl2",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "routeAdmn2",
                        "keyword": "routeAdmn2",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "drugMnftr2",
                        "keyword": "drugMnftr2",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "drugAgent3",
                        "keyword": "drugAgent3",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "dose3",
                        "keyword": "dose3",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "doseRatnl3",
                        "keyword": "doseRatnl3",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "routeAdmn3",
                        "keyword": "routeAdmn3",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "drugMnftr3",
                        "keyword": "drugMnftr3",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "drugAgent4",
                        "keyword": "drugAgent4",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "dose4",
                        "keyword": "dose4",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "doseRatnl4",
                        "keyword": "doseRatnl4",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "routeAdmn4",
                        "keyword": "routeAdmn4",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "drugMnftr4",
                        "keyword": "drugMnftr4",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "drugAgent5",
                        "keyword": "drugAgent5",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "dose5",
                        "keyword": "dose5",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "doseRatnl5",
                        "keyword": "doseRatnl5",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "routeAdmn5",
                        "keyword": "routeAdmn5",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "drugMnftr5",
                        "keyword": "drugMnftr5",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "drugAgent6",
                        "keyword": "drugAgent6",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "dose6",
                        "keyword": "dose6",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "doseRatnl6",
                        "keyword": "doseRatnl6",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "routeAdmn6",
                        "keyword": "routeAdmn6",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "drugMnftr6",
                        "keyword": "drugMnftr6",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "drugAgent7",
                        "keyword": "drugAgent7",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "dose7",
                        "keyword": "dose7",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "doseRatnl7",
                        "keyword": "doseRatnl7",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "routeAdmn7",
                        "keyword": "routeAdmn7",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "drugMnftr7",
                        "keyword": "drugMnftr7",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "drugAgent8",
                        "keyword": "drugAgent8",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "dose8",
                        "keyword": "dose8",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "doseRatnl8",
                        "keyword": "doseRatnl8",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "routeAdmn8",
                        "keyword": "routeAdmn8",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "drugMnftr8",
                        "keyword": "drugMnftr8",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "drugAgent9",
                        "keyword": "drugAgent9",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "dose9",
                        "keyword": "dose9",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "doseRatnl9",
                        "keyword": "doseRatnl9",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "routeAdmn9",
                        "keyword": "routeAdmn9",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "drugMnftr9",
                        "keyword": "drugMnftr9",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "drugAgent10",
                        "keyword": "drugAgent10",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "dose10",
                        "keyword": "dose10",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "doseRatnl10",
                        "keyword": "doseRatnl10",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "routeAdmn10",
                        "keyword": "routeAdmn10",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "drugMnftr10",
                        "keyword": "drugMnftr10",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "trtmntDuration",
                        "keyword": "trtmntDuration",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "subComplMontr",
                        "keyword": "subComplMontr",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "radMat1",
                        "keyword": "radMat1",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "authrzdUsr",
                        "keyword": "authrzdUsr",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "authrztnNum",
                        "keyword": "authrztnNum",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "incldAdminstr",
                        "keyword": "incldAdminstr",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "radioCompAvail",
                        "keyword": "radioCompAvail",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "faciltyInvstg",
                        "keyword": "faciltyInvstg",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "biosafety1",
                        "keyword": "biosafety1",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "biosafety4",
                        "keyword": "biosafety4",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "nihRacRev",
                        "keyword": "nihRacRev",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "nihRacRevComm",
                        "keyword": "nihRacRevComm",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "biosafety2",
                        "keyword": "biosafety2",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "biosafety2Comm",
                        "keyword": "biosafety2Comm",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "biosafety3",
                        "keyword": "biosafety3",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "biosafety5",
                        "keyword": "biosafety5",
                        "type": "MSD"
                    }
                ]
            },
            {
                "title": "Medical and Investigational Devices",
                "secType": "generic",
                "seq": 10,
                "fields": [
                    {
                        "flxFieldId": "deviceAgent1",
                        "keyword": "deviceAgent1",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "devMnftr1",
                        "keyword": "devMnftr1",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "riskAssmt1",
                        "keyword": "riskAssmt1",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "deviceAgent2",
                        "keyword": "deviceAgent2",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "devMnftr2",
                        "keyword": "devMnftr2",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "riskAssmt2",
                        "keyword": "riskAssmt2",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "deviceAgent3",
                        "keyword": "deviceAgent3",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "devMnftr3",
                        "keyword": "devMnftr3",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "riskAssmt3",
                        "keyword": "riskAssmt3",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "deviceAgent4",
                        "keyword": "deviceAgent4",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "devMnftr4",
                        "keyword": "devMnftr4",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "riskAssmt4",
                        "keyword": "riskAssmt4",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "deviceAgent5",
                        "keyword": "deviceAgent5",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "devMnftr5",
                        "keyword": "devMnftr5",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "riskAssmt5",
                        "keyword": "riskAssmt5",
                        "type": "MSD"
                    }
                ]
            },
            {
                "title": "Study Evaluation and Procedures",
                "secType": "generic",
                "seq": 11,
                "fields": [
                    {
                        "flxFieldId": "freshBiopsyNeed",
                        "keyword": "freshBiopsyNeed",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "freshBiopsyYes",
                        "keyword": "freshBiopsyYes",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "section_4_5_1",
                        "keyword": "section_4_5_1",
                        "type": "studySection"
                    },
                    {
                        "flxFieldId": "section_4_5_2",
                        "keyword": "section_4_5_2",
                        "type": "studySection"
                    },
                    {
                        "flxFieldId": "section_4_5_3",
                        "keyword": "section_4_5_3",
                        "type": "studySection"
                    },
                    {
                        "flxFieldId": "section_4_5_4",
                        "keyword": "section_4_5_4",
                        "type": "studySection"
                    },
                    {
                        "flxFieldId": "section_4_5",
                        "keyword": "section_4_5",
                        "type": "studySection"
                    }
                ]
            },
            {
                "title": "Withdrawal and Replacement of Participants",
                "secType": "generic",
                "seq": 12,
                "fields": [
                    {
                        "flxFieldId": "offStdyCri",
                        "keyword": "offStdyCri",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "OtrStdRemCrit",
                        "keyword": "OtrStdRemCrit",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "rplSubject",
                        "keyword": "rplSubject",
                        "type": "MSD"
                    }
                ]
            },
            {
                "title": "Biomarker and Correlative Studies",
                "secType": "generic",
                "seq": 13,
                "fields": [
                    {
                        "flxFieldId": "bioMark1",
                        "keyword": "bioMark1",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "knownBiomrkr",
                        "keyword": "knownBiomrkr",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "biomrkrType",
                        "keyword": "biomrkrType",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "biomrkrSpeOth",
                        "keyword": "biomrkrSpeOth",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "testNameQ",
                        "keyword": "testNameQ",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "ifGeneList",
                        "keyword": "ifGeneList",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "biomrkrCall",
                        "keyword": "biomrkrCall",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "levelQues",
                        "keyword": "levelQues",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "measureQues",
                        "keyword": "measureQues",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "timePoints",
                        "keyword": "timePoints",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "methodology",
                        "keyword": "methodology",
                        "type": "MSD"
                    }
                ]
            },
            {
                "title": "Statistical Considerations",
                "secType": "generic",
                "seq": 14,
                "fields": [
                    {
                        "flxFieldId": "statCons1",
                        "keyword": "statCons1",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "scndryEndPts",
                        "keyword": "scndryEndPts",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "expEndPts",
                        "keyword": "expEndPts",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "proDes2",
                        "keyword": "proDes2",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "protoMontr1",
                        "keyword": "protoMontr1",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "protoMontr3",
                        "keyword": "protoMontr3",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "protoMontr4",
                        "keyword": "protoMontr4",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "dsmbInt",
                        "keyword": "dsmbInt",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "dsmbCompostn",
                        "keyword": "dsmbCompostn",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "statAnalPlan",
                        "keyword": "statAnalPlan",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "bayesianComp",
                        "keyword": "bayesianComp",
                        "type": "MSD"
                    }
                ]
            },
            {
                "title": "Safety and Adverse Events",
                "secType": "generic",
                "seq": 15,
                "fields": [
                    {
                        "flxFieldId": "invAntcpAEs",
                        "keyword": "invAntcpAEs",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "commDrugInsrt",
                        "keyword": "commDrugInsrt",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "repSAEs",
                        "keyword": "repSAEs",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "repSponsr",
                        "keyword": "repSponsr",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "critAdvEvents",
                        "keyword": "critAdvEvents",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "critAdvEvntsOth",
                        "keyword": "critAdvEvntsOth",
                        "type": "MSD"
                    }
                ]
            },
            {
                "title": "Data Safety Monitoring Plan",
                "secType": "generic",
                "seq": 16,
                "fields": [
                    {
                        "flxFieldId": "section_6_5",
                        "keyword": "section_6_5",
                        "type": "studySection"
                    }
                ]
            },
            {
                "title": "Financial Support",
                "secType": "generic",
                "seq": 17,
                "fields": [
                    {
                        "flxFieldId": "fundnSrc",
                        "keyword": "fundnSrc",
                        "type": "MSD",
                        "labelOnly":1
                    },
                    {
                        "flxFieldId": "sponsorName1",
                        "keyword": "sponsorName1",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "fundingType1",
                        "keyword": "fundingType1",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "fundProvChk1",
                        "keyword": "fundProvChk1",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "sponsorName2",
                        "keyword": "sponsorName2",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "fundingType2",
                        "keyword": "fundingType2",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "fundProvChk2",
                        "keyword": "fundProvChk2",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "sponsorName3",
                        "keyword": "sponsorName3",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "fundingType3",
                        "keyword": "fundingType3",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "fundProvChk3",
                        "keyword": "fundProvChk3",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "sponsorName4",
                        "keyword": "sponsorName4",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "fundingType4",
                        "keyword": "fundingType4",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "fundProvChk4",
                        "keyword": "fundProvChk4",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "sponsorName5",
                        "keyword": "sponsorName5",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "fundingType5",
                        "keyword": "fundingType5",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "fundProvChk5",
                        "keyword": "fundProvChk5",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "finSupp4",
                        "keyword": "finSupp4",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "grant_number",
                        "keyword": "grant_number",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "grant",
                        "keyword": "grant",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "nci",
                        "keyword": "nci",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "nci1",
                        "keyword": "nci1",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "spnsrshp1",
                        "keyword": "spnsrshp1",
                        "type": "MSD"
                    }
                ]
            },
            {
                "title": "References",
                "secType": "generic",
                "seq": 18,
                "fields": [
                    {
                        "flxFieldId": "references",
                        "keyword": "references",
                        "type": "MSD"
                    }
                ]
            },
            {
                "title": "Reviewers and Pre-Reviews",
                "secType": "generic",
                "seq": 19,
                "fields": [
                    {
                        "flxFieldId": "dept_chairs",
                        "keyword": "dept_chairs",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "dept_chairs_ids",
                        "keyword": "dept_chairs_ids",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "nom_rev",
                        "keyword": "nom_rev",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "nom_rev_id",
                        "keyword": "nom_rev_id",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "nom_alt_rev",
                        "keyword": "nom_alt_rev",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "nom_alt_rev_id",
                        "keyword": "nom_alt_rev_id",
                        "type": "MSD"
                    },
                    {
                        "flxFieldId": "prot_ready",
                        "keyword": "prot_ready",
                        "type": "MSD"
                    }
                ]
            }
        ]
    }