set define off;

--bug 20896
update er_codelst set codelst_hide = 'Y' where codelst_type = 'INDIDEHolder' and codelst_subtyp = 'PI';

commit;