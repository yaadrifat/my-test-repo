set define off;

--bug 21141
--updating subtypes to Velos supported subtypes
update er_codelst set codelst_subtyp = 'phaseIII' where codelst_type = 'phase' and (codelst_desc = 'Phase III');
update er_codelst set codelst_subtyp = 'phaseI/II' where codelst_type = 'phase' and (codelst_desc = 'Phase I/II');
update er_codelst set codelst_subtyp = 'phaseIV' where codelst_type = 'phase' and (codelst_desc = 'Phase IV');
update er_codelst set codelst_subtyp = 'phaseIV/V' where codelst_type = 'phase' and (codelst_desc = 'Phase IV/V');
update er_codelst set codelst_subtyp = 'phaseII' where codelst_type = 'phase' and (codelst_desc = 'Phase II');
update er_codelst set codelst_subtyp = 'phaseII/III' where codelst_type = 'phase' and (codelst_desc = 'Phase II/III');
update er_codelst set codelst_subtyp = 'phaseI' where codelst_type = 'phase' and (codelst_desc = 'Phase I');

update er_codelst set codelst_subtyp = 'phaseZero' where codelst_type = 'phase' and (codelst_desc = 'Phase 0');
commit;

--hiding Not applicable
update er_codelst set codelst_hide = 'Y' where codelst_type = 'phase' and codelst_subtyp = 'phaseNA';
commit;

--Create or update code-items
DECLARE
  v_item_exists number := 0;  
BEGIN
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'phase' and codelst_desc = 'Phase 0';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'phase','phaseZero','Phase 0','N',5, null);
		dbms_output.put_line('Code-list item Type:phase Subtype:phaseZero  inserted');
	else
		update er_codelst set codelst_subtyp = 'phaseZero', codelst_seq = 5 where codelst_type = 'phase' and (codelst_desc = 'Phase 0');
		dbms_output.put_line('Code-list item Type:phase Subtype:phaseZero already exists');
	end if;
	
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'phase' and codelst_desc = 'Pilot';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'phase','pilot','Pilot','N',40, null);
		dbms_output.put_line('Code-list item Type:phase Subtype:pilot inserted');
	else
		update er_codelst set codelst_subtyp = 'pilot', codelst_seq = 40 where codelst_type = 'phase' and (codelst_desc = 'Pilot');
		dbms_output.put_line('Code-list item Type:phase Subtype:pilot already exists');
	end if;
	
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'phase' and codelst_desc = 'Other';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'phase','other','Other','N',45, null);
		dbms_output.put_line('Code-list item Type:phase Subtype:other inserted');
	else
		update er_codelst set codelst_subtyp = 'other', codelst_seq = 45 where codelst_type = 'phase' and (codelst_desc = 'Other');
		dbms_output.put_line('Code-list item Type:phase Subtype:other already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'phase' and codelst_desc = 'Current Practice';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'phase','currPractice','Current Practice','N',50, null);
		dbms_output.put_line('Code-list item Type:phase Subtype:currPractice inserted');
	else
		update er_codelst set codelst_subtyp = 'currPractice', codelst_seq = 50 where codelst_type = 'phase' and (codelst_desc = 'Current Practice');
		dbms_output.put_line('Code-list item Type:phase Subtype:currPractice already exists');
	end if;
	
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'phase' and codelst_desc = 'Compassionate';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'phase','compassionate','Compassionate','N',55, null);
		dbms_output.put_line('Code-list item Type:phase Subtype:compassionate inserted');
	else
		update er_codelst set codelst_subtyp = 'compassionate', codelst_seq = 55 where codelst_type = 'phase' and (codelst_desc = 'Compassionate');
		dbms_output.put_line('Code-list item Type:phase Subtype:compassionate already exists');
	end if;
	
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'phase' and codelst_desc = 'Lab';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'phase','lab','Lab','N',60, null);
		dbms_output.put_line('Code-list item Type:phase Subtype:Lab inserted');
	else
		update er_codelst set codelst_subtyp = 'lab', codelst_seq = 60 where codelst_type = 'phase' and (codelst_desc = 'Lab');
		dbms_output.put_line('Code-list item Type:phase Subtype:lab already exists');
	end if;
	
	commit;

end;
/

--updating code-item descriptions and sequence
update er_codelst set codelst_desc = 'Phase 1', codelst_seq = 10 where codelst_type = 'phase' and codelst_subtyp = 'phaseI';
update er_codelst set codelst_desc = 'Phase 2', codelst_seq = 15 where codelst_type = 'phase' and codelst_subtyp = 'phaseII';
update er_codelst set codelst_desc = 'Phase 3', codelst_seq = 20 where codelst_type = 'phase' and codelst_subtyp = 'phaseIII';
update er_codelst set codelst_desc = 'Phase 1/2', codelst_seq = 25 where codelst_type = 'phase' and codelst_subtyp = 'phaseI/II';
update er_codelst set codelst_desc = 'Phase 2-3', codelst_seq = 30 where codelst_type = 'phase' and codelst_subtyp = 'phaseII/III';
update er_codelst set codelst_desc = 'Phase 4', codelst_seq = 35 where codelst_type = 'phase' and codelst_subtyp = 'phaseIV';
update er_codelst set codelst_seq = 40 where codelst_type = 'phase' and codelst_subtyp = 'pilot';
update er_codelst set codelst_desc = 'Feasability with N/A', codelst_seq = 65 where codelst_type = 'phase' and codelst_subtyp = 'pilotFeasibilty';
commit;

--hiding code-items which are extra
update er_codelst set codelst_hide = 'Y' where codelst_type = 'phase' and codelst_subtyp = 'phaseIIIorIV';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'phase' and codelst_subtyp = 'phaseIV/V';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'phase' and codelst_subtyp = 'phaseV';
commit;

