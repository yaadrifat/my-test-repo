set define off;
set serveroutput on;

update ER_CODELST set
CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "DICRC">DICRC</OPTION>
<OPTION value = "dept">Department</OPTION>
<OPTION value = "fedFund">Federal Funding</OPTION>
<OPTION value = "foundation">Foundation</OPTION>
<OPTION value = "privInd">Private Industry</OPTION>
<OPTION value = "privRecint">Private Industry /ReCINT</OPTION>
<OPTION value = "RPCCC">RPCCC</OPTION>
</SELECT>' where CODELST_TYPE='studyidtype' and 
CODELST_SUBTYP in ('fundingType1', 'fundingType2', 'fundingType3',
'fundingType4', 'fundingType5');

-- Study Division
---- Hide QA items
update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='study_division' and
CODELST_SUBTYP in ('cc', 'cardio', 'med', 'neuro', 'surg', 'other',
'study_divisi_31', 'pulmonary');
commit;
---- Add new items if not existing
declare
v_CodeExists number := 0;
begin
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'study_division' AND CODELST_SUBTYP = 'study_divisio_6';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_seq)
        values (seq_er_codelst.nextval, 'study_division', 'study_divisio_6', 'Anesthesiology & Critical Care', 6);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'study_division' AND CODELST_SUBTYP = 'study_divisio_7';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_seq)
        values (seq_er_codelst.nextval, 'study_division', 'study_divisio_7', 'Basic Science', 7);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'study_division' AND CODELST_SUBTYP = 'study_divisio_8';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_seq)
        values (seq_er_codelst.nextval, 'study_division', 'study_divisio_8', 'Cancer Medicine', 8);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'study_division' AND CODELST_SUBTYP = 'study_divisio_9';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_seq)
        values (seq_er_codelst.nextval, 'study_division', 'study_divisio_9', 'Cancer Prevention & Population Sciences', 9);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'study_division' AND CODELST_SUBTYP = 'study_divisi_10';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_seq)
        values (seq_er_codelst.nextval, 'study_division', 'study_divisi_10', 'Diagnostic Imaging', 10);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'study_division' AND CODELST_SUBTYP = 'study_divisi_11';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_seq)
        values (seq_er_codelst.nextval, 'study_division', 'study_divisi_11', 'EVP Physician in Chief Area', 11);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'study_division' AND CODELST_SUBTYP = 'study_divisi_12';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_seq)
        values (seq_er_codelst.nextval, 'study_division', 'study_divisi_12', 'Internal Medicine', 12);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'study_division' AND CODELST_SUBTYP = 'study_divisi_13';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_seq)
        values (seq_er_codelst.nextval, 'study_division', 'study_divisi_13', 'Pathology/Lab Medicine', 13);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'study_division' AND CODELST_SUBTYP = 'study_divisi_14';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_seq)
        values (seq_er_codelst.nextval, 'study_division', 'study_divisi_14', 'Pediatrics', 14);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'study_division' AND CODELST_SUBTYP = 'study_divisi_15';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_seq)
        values (seq_er_codelst.nextval, 'study_division', 'study_divisi_15', 'Radiation Oncology', 15);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'study_division' AND CODELST_SUBTYP = 'sur';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_seq)
        values (seq_er_codelst.nextval, 'study_division', 'sur', 'Surgery', 20);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	commit;
end;
/
-- End of Study Division

-- TArea = Department
---- Hide QA items
update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='tarea' and
CODELST_SUBTYP in ('lip', 'aging', 'alds', 'allergy', 'alzheimer', 'blood',
'brain', 'tareaBreast', 'cancer', 'dental', 'diabetes', 'ent', 'esm', 'eye',
'gastro', 'heart', 'hypertension', 'impotence', 'infertility', 'lipid', 'liver',
'lung', 'mentalhealth', 'obesity', 'other', 'pain', 'pediatric', 'prostate',
'renal', 'reproductive', 'seizures', 'std', 'urogenital');
---- Fix QA items
update ER_CODELST set CODELST_DESC='Genetics',CODELST_HIDE='N',CODELST_SEQ=130,CODELST_CUSTOM_COL1='study_divisio_7' where 
CODELST_TYPE='tarea' and CODELST_SUBTYP in ('tarea_36');
update ER_CODELST set CODELST_DESC='Genitourinary Medical Oncology',CODELST_HIDE='N',CODELST_SEQ=135,CODELST_CUSTOM_COL1='study_divisio_8' where 
CODELST_TYPE='tarea' and CODELST_SUBTYP in ('tarea_37');
update ER_CODELST set CODELST_DESC='Genomic Medicine',CODELST_HIDE='N',CODELST_SEQ=140,CODELST_CUSTOM_COL1='study_divisio_8' where 
CODELST_TYPE='tarea' and CODELST_SUBTYP in ('tarea_38');

commit;
---- Add new items if not existing
declare
v_CodeExists number := 0;
begin
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_14';	
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_14','Anesthesiology & Perioperative Medicine','N',1,'study_divisio_6');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_15';	
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_15','Behavioral Science','N',15,'study_divisio_9');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_16';	
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_16','Bioinformatics & Computational Biology','N',20,'study_divisio_7');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_17';	
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_17','Biostatistics','N',25,'study_divisio_7');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_18';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_18','Breast Medical Oncology','N',35,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_20';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_20','Cancer Biology','N',40,'study_divisio_7');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_21';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_21','Cancer System Imaging','N',45,'study_divisi_10');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_22';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_22','Cardiology','N',50,'study_divisi_12');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_23';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_23','Clinical Cancer Prevention','N',55,'study_divisio_9');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_24';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_24','Critical Care','N',60,'study_divisio_6');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_25';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_25','Dermatology','N',70,'study_divisi_12');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_26';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_26','Diagnostic Radiology','N',75,'study_divisi_10');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_27';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_27','Emergency Medicine','N',80,'study_divisi_12');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_28';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_28','Endocrine Neoplasia and HD','N',85,'study_divisi_12');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_29';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_29','Epidemiology','N',90,'study_divisio_9');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_30';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_30','Experimental Radiation Oncology','N',95,'study_divisi_15');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_31';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_31','Experimental Therapeutics','N',100,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_32';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_32','Gastroenterology Hepat & Nutr','N',105,'study_divisi_12');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_33';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_33','Gastrointestinal Medical Oncology','N',115,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_34';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_34','General Internal Medicine','N',120,'study_divisi_12');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_35';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_35','General Oncology','N',125,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_36';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_36','Genetics','N',130,'study_divisio_7');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_37';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_37','Genitourinary Medical Oncology','N',135,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_38';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_38','Genomic Medicine','N',140,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_39';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_39','Gynecologic Oncology & Reproductive Medicine','N',150,'sur');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_40';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_40','Head & Neck Surgery','N',160,'sur');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_41';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_41','Health Disparities Research','N',165,'study_divisio_9');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_42';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_42','Health Services Research','N',170,'study_divisio_9');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_43';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_43','Hematopathology','N',175,'study_divisi_13');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_44';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_44','Imaging Physics','N',185,'study_divisi_10');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_45';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_45','Immunology','N',190,'study_divisio_7');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_46';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_46','Infectious Diseases','N',195,'study_divisi_12');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_47';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_47','Interventional Radiology','N',200,'study_divisi_10');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_48';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_48','Investigational Cancer Therapeutics','N',205,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_49';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_49','Laboratory Medicine','N',210,'study_divisi_13');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_50';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_50','Leukemia','N',215,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_51';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_51','Lymphoma/Myeloma','N',220,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_52';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_52','Melanoma Medical Oncology','N',225,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_53';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_53','Molecular Carcinogenesis','N',230,'study_divisio_7');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_54';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_54','Molecular Pathology','N',235,'study_divisi_13');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_55';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_55','Molecular and Cellular Oncology','N',240,'study_divisio_7');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_56';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_56','Neuro-Oncology','N',245,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_57';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_57','Neurosurgery','N',255,'sur');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_58';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_58','Nuclear Medicine','N',265,'study_divisi_10');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_59';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_59','Orthopaedic Oncology','N',275,'sur');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_61';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_61','Pain Medicine','N',280,'study_divisio_6');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_62';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_62','Palliative Care & Rehabilitation Medicine','N',285,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_63';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_63','Pathology','N',290,'study_divisi_13');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_64';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_64','Pediatrics','N',295,'study_divisi_14');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_65';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_65','Plastic Surgery','N',300,'sur');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_67';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_67','Proton Therapy','N',305,'study_divisi_15');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_68';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_68','Psychiatry','N',310,'study_divisi_11');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_69';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_69','Pulmonary Medicine','N',315,'study_divisi_12');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_70';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_70','Radiation Oncology','N',320,'study_divisi_15');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_71';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_71','Radiation Physics','N',325,'study_divisi_15');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_73';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_73','Respiratory Care','N',330,'study_divisio_6');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_74';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_74','Sarcoma Medical Oncology - Cytokine & Supportive Oncology','N',335,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_75';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_75','Stem Cell Transplantation','N',340,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_76';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_76','Surgical Oncology','N',345,'sur');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_77';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_77','Symptom Research','N',350,'study_divisi_12');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_78';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_78','Systems Biology','N',355,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_79';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_79','Thoracic & Cardiovascular Surgery','N',360,'sur');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_80';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_80','Thoracic/Head & Neck Medical Oncology','N',370,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_81';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_81','Urology','N',380,'sur');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_83';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_83','Veterinary Medicine & Surgery','N',385,'study_divisio_7');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_82';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_82','Veterinary Sciences','N',390,'study_divisio_7');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
-- End of TArea = Department
commit;
end;
/

update ER_CODELST set CODELST_DESC='Administrative Department' where CODELST_TYPE='studyidtype' and CODELST_SUBTYP='studyCond2';
commit;

declare
v_CodeExists number := 0;
begin
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyAdmDiv';	
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL) values (seq_er_codelst.nextval,null,'studyidtype','studyAdmDiv','Administrative Division','N',1,'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
end;
/

update ER_CODELST set CODELST_DESC='Cooperative Group / Other Externally Peer Reviewed' where CODELST_TYPE='research_type' and CODELST_SUBTYP='coop';
update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='research_type' and CODELST_SUBTYP='none';

--Bug 21213 : Field: Blinding Change drop down choice Double - Blinded to Double - Blind 
update er_codelst set codelst_desc = 'Double-Blind' where codelst_type = 'blinding' and codelst_subtyp = 'doubleBlind' ;

commit;
