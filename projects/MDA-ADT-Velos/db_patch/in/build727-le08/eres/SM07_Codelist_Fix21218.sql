set define off;

--updating the code-item descriptions 
update er_codelst set codelst_desc = 'VP of Clinical Resaerch Adminsitration Determination', codelst_seq = 13 where codelst_type = 'rev_board' and codelst_subtyp = 'vp_cr';
update er_codelst set codelst_desc = 'Clinical Research Committee', codelst_seq = 2 where codelst_type = 'rev_board' and codelst_subtyp = 'crc_cc';
update er_codelst set codelst_desc = 'Institutional Review Board', codelst_seq = 6 where codelst_type = 'rev_board' and codelst_subtyp = 'irb_cc';
update er_codelst set codelst_desc = 'Ancillary Review Response', codelst_seq = 1 where codelst_type = 'rev_board' and codelst_subtyp = 'ancillary';
update er_codelst set codelst_desc = 'Medical Review Response', codelst_seq = 7 where codelst_type = 'rev_board' and codelst_subtyp = 'medical';
commit;

--Create or update code-items
DECLARE
  v_item_exists number := 0;  
BEGIN
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'rev_board' and codelst_desc = 'Data and Safety Monitoring Board';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'rev_board','rev_dataSafety','Data and Safety Monitoring Board','N',3, null);
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_dataSafety  inserted');
	else
		update er_codelst set codelst_subtyp = 'rev_dataSafety', codelst_seq = 3 where codelst_type = 'rev_board' and (codelst_desc = 'Data and Safety Monitoring Board');
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_dataSafety already exists');
	end if;
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'rev_board' and codelst_desc = 'EPAAC';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'rev_board','rev_EPAAC','EPAAC','N',4, null);
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_EPAAC  inserted');
	else
		update er_codelst set codelst_subtyp = 'rev_EPAAC', codelst_seq = 4 where codelst_type = 'rev_board' and (codelst_desc = 'EPAAC');
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_EPAAC already exists');
	end if;
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'rev_board' and codelst_desc = 'Exeuctive Session - IRB3';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'rev_board','rev_ExecSession','Exeuctive Session - IRB3','N',5, null);
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_ExecSession  inserted');
	else
		update er_codelst set codelst_subtyp = 'rev_ExecSession', codelst_seq = 5 where codelst_type = 'rev_board' and (codelst_desc = 'Exeuctive Session - IRB3');
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_ExecSession already exists');
	end if;
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'rev_board' and codelst_desc = 'Office of Protocol Research';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'rev_board','rev_ProtRes','Office of Protocol Research','N',8, null);
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_ProtRes  inserted');
	else
		update er_codelst set codelst_subtyp = 'rev_ProtRes', codelst_seq = 8 where codelst_type = 'rev_board' and (codelst_desc = 'Office of Protocol Research');
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_ProtRes already exists');
	end if;
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'rev_board' and codelst_desc = 'PI Initiated';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'rev_board','rev_PIInit','PI Initiated','N',9, null);
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_PIInit  inserted');
	else
		update er_codelst set codelst_subtyp = 'rev_PIInit', codelst_seq = 9 where codelst_type = 'rev_board' and (codelst_desc = 'PI Initiated');
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_PIInit already exists');
	end if;
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'rev_board' and codelst_desc = 'Psychosocial Behavior Health Services Research Committee';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'rev_board','rev_PBHSRC','Psychosocial Behavior Health Services Research Committee','N',10, null);
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_PBHSRC  inserted');
	else
		update er_codelst set codelst_subtyp = 'rev_PBHSRC', codelst_seq = 10 where codelst_type = 'rev_board' and (codelst_desc = 'Psychosocial Behavior Health Services Research Committee');
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_PBHSRC already exists');
	end if;
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'rev_board' and codelst_desc = 'Response To Compliance Audit/Monitoring Report';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'rev_board','rev_CompAudit','Response To Compliance Audit/Monitoring Report','N',11, null);
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_CompAudit  inserted');
	else
		update er_codelst set codelst_subtyp = 'rev_CompAudit', codelst_seq = 11 where codelst_type = 'rev_board' and (codelst_desc = 'Response To Compliance Audit/Monitoring Report');
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_CompAudit already exists');
	end if;
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'rev_board' and codelst_desc = 'Sponsor Request';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'rev_board','rev_SponsorReq','Sponsor Request','N',12, null);
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_SponsorReq  inserted');
	else
		update er_codelst set codelst_subtyp = 'rev_SponsorReq', codelst_seq = 12 where codelst_type = 'rev_board' and (codelst_desc = 'Sponsor Request');
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_SponsorReq already exists');
	end if;
	commit;
end;
/

--Hiding code-items
update er_codelst set codelst_hide = 'Y' where codelst_type = 'rev_board' and codelst_subtyp not in ('vp_cr','crc_cc', 'irb_cc','ancillary','medical','rev_dataSafety',
'rev_EPAAC','rev_ExecSession', 'rev_ProtRes', 'rev_PIInit', 'rev_PBHSRC', 'rev_CompAudit', 'rev_SponsorReq');
commit;

