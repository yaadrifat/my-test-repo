set define off;

--bug 21140
update er_codelst set codelst_desc = 'Diagnostic (DIA)', codelst_seq = 5 where codelst_type = 'studyPurpose' and codelst_subtyp = 'diagnostic';
update er_codelst set codelst_desc = 'Basic Science (BAS)', codelst_seq = 10 where codelst_type = 'studyPurpose' and codelst_subtyp = 'basicScience';
update er_codelst set codelst_desc = 'Health Services Research (HSR)', codelst_seq = 20 where codelst_type = 'studyPurpose' and codelst_subtyp = 'healthSvcRsrch';
update er_codelst set codelst_desc = 'Prevention (PRE)', codelst_seq = 25 where codelst_type = 'studyPurpose' and codelst_subtyp = 'prevention';
update er_codelst set codelst_desc = 'Screening (SCR)', codelst_seq = 30 where codelst_type = 'studyPurpose' and codelst_subtyp = 'screening';
update er_codelst set codelst_desc = 'Supportive Care (SUP)', codelst_seq = 35 where codelst_type = 'studyPurpose' and codelst_subtyp = 'supportiveCare';
update er_codelst set codelst_desc = 'Treatment (TRE)', codelst_seq = 40 where codelst_type = 'studyPurpose' and codelst_subtyp = 'treatment';
update er_codelst set codelst_desc = 'Other (OTH)', codelst_seq = 50 where codelst_type = 'studyPurpose' and codelst_subtyp = 'other';

commit;

--Create or update code-items
DECLARE
  v_item_exists number := 0;
BEGIN
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'studyPurpose' and codelst_desc = 'Epidemiologic Trial (EPI)';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'studyPurpose','epidemiologic','Epidemiologic Trial (EPI)','N',15, null);
		dbms_output.put_line('Code-list item Type:studyPurpose Subtype:epidemiologic  inserted');
	else
		update er_codelst set codelst_subtyp = 'epidemiologic', codelst_seq = 15 where codelst_type = 'studyPurpose' and (codelst_desc = 'Epidemiologic Trial (EPI)');
		dbms_output.put_line('Code-list item Type:studyPurpose Subtype:epidemiologic already exists');
	end if;
	
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'studyPurpose' and codelst_desc = 'Observational Trial (Obs)';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'studyPurpose','observational','Observational Trial (Obs)','N',45, null);
		dbms_output.put_line('Code-list item Type:studyPurpose Subtype:observational  inserted');
	else
		update er_codelst set codelst_subtyp = 'observational', codelst_seq = 45 where codelst_type = 'studyPurpose' and (codelst_desc = 'Observational Trial (Obs)');
		dbms_output.put_line('Code-list item Type:studyPurpose Subtype:observational already exists');
	end if;
	
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'studyPurpose' and codelst_desc = 'Outcome Trial (Out)';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'studyPurpose','outcomeTrial','Outcome Trial (Out)','N',55, null);
		dbms_output.put_line('Code-list item Type:studyPurpose Subtype:outcomeTrial  inserted');
	else
		update er_codelst set codelst_subtyp = 'outcomeTrial', codelst_seq = 55 where codelst_type = 'studyPurpose' and (codelst_desc = 'Outcome Trial (Out)');
		dbms_output.put_line('Code-list item Type:studyPurpose Subtype:outcomeTrial already exists');
	end if;
	commit;
end;
/

