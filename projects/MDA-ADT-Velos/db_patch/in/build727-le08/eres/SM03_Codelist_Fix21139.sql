set define off;

--bug 21139
--updating code subtype
update er_codelst set codelst_subtyp = 'ancillary' where codelst_type = 'study_type' and codelst_desc = 'Ancillary';
update er_codelst set codelst_subtyp = 'diagnostic' where codelst_type = 'study_type' and codelst_desc = 'Diagnostic';
commit;

--hiding code-items which are extra
update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp = 'ancillary';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp = 'behavioral';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp = 'companion';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp = 'compassionUse';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp = 'diagnostic';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp = 'earlyDetection';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp = 'epidemiological';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp = 'studyTypeNone';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp = 'study';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp = 'prevention';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp = 'qol';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp = 'screening';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp = 'standardofCare';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp = 'therapeutic';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp = 'tissueBankLab';
commit;

update er_codelst set codelst_desc = 'Observational (OBS)', codelst_seq = 10 where codelst_type = 'study_type' and codelst_subtyp = 'observational';
update er_codelst set codelst_desc = 'Ancillary or Correlative (ANC/COR)', codelst_seq = 15 where codelst_type = 'study_type' and codelst_subtyp = 'correlative';
commit;

--Create or update code-items
DECLARE
  v_item_exists number := 0;  
BEGIN
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'study_type' and codelst_desc = 'Interventional (INT)';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'study_type','interventional','Interventional (INT)','N',5, null);
		dbms_output.put_line('Code-list item Type:study_type Subtype:interventional  inserted');
	else
		update er_codelst set codelst_subtyp = 'interventional', codelst_seq = 5 where codelst_type = 'study_type' and (codelst_desc = 'Interventional (INT)');
		dbms_output.put_line('Code-list item Type:study_type Subtype:interventional already exists');
	end if;
	commit;
end;
/

update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp not in ('interventional','observational','correlative');
commit;