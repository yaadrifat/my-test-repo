--for bug id : 20988
SET define off;

UPDATE ER_CODELST 
SET CODELST_CUSTOM_COL='checkbox',
CODELST_CUSTOM_COL1='{chkArray:[
{data: "option1", display:"Death"},
{data: "option2", display:"Disease Progression"},
{data: "option3", display:"Disease Relapse"},
{data: "option4", display:"Drug Resistant"},
{data: "option5", display:"Intercurrent Illness"},
{data: "option6", display:"Lost to Follow-Up"},
{data: "option7", display:"New Malignancy"},
{data: "option8", display:"Not Able to Comply with Study Requirements"},
{data: "option9", display:"Patient Decision to Discontinue Study"},
{data: "option10", display:"Patient Non-Compliance"},
{data: "option11", display:"Patient Withdrawal of Consent"},
{data: "option12", display:"Physician Discretion"},
{data: "option13", display:"Pregnancy"},
{data: "option14", display:"Toxicity"},
{data: "option15", display:"Treatment Completion"},
{data: "option16", display:"Other"}
]}'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='offStdyCri';

--for bug id 20984
update er_codelst set codelst_seq = '1' where CODELST_TYPE='studyscope' and codelst_subtyp = 'std_cen_single' ;
update er_codelst set codelst_seq = '2' where CODELST_TYPE='studyscope' and codelst_subtyp = 'studyscope_3' ;
update er_codelst set codelst_seq = '5' where CODELST_TYPE='studyscope' and codelst_subtyp = 'std_cen_multi' ;

COMMIT;