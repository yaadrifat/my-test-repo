set define off;

update er_codelst set codelst_hide = 'Y' where codelst_type = 'randomization' and codelst_desc = 'Bayesian Model Averaging, Continuous Reassessment Method (BMA-CRM)';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'randomization' and codelst_subtyp = 'blckRndmztn' and codelst_desc = 'Block Randomization';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'randomization' and codelst_desc = 'EffTox Dose-finding';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'randomization' and codelst_desc = 'Equal Randomization';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'randomization' and codelst_desc = 'List Randomization';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'randomization' and codelst_desc = 'Pocock-Simon Stratified Randomization';

commit;

update er_codelst set codelst_seq = 10 where codelst_type = 'randomization' and codelst_subtyp = 'other';
update er_codelst set codelst_seq = 9 where codelst_type = 'randomization' and codelst_desc = 'Stratified Randomization';

commit;
