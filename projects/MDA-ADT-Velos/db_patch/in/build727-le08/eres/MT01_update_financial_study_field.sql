--for the following config change:  
--In Financial Support section: "Indicate if the sponsor/supporter/granting agency will receive data": 
--To be changed to a dropdown with values Yes and No   
--bug id :21156
--bug id: 21154

SET define off;
update er_codelst set codelst_custom_col = 'dropdown'  where codelst_subtyp = 'spnsrshp1' ;
---Indicate if the sponsor/supporter/granting agency will receive data -- Should be a dropdown instead of check box.
UPDATE ER_CODELST
SET CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "yes">Yes</OPTION>
<OPTION value = "no">No</OPTION>
</SELECT>'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP = 'spnsrshp1';

---adding the dropdown options to the funding type dropdown. There are 5 of these dropdowns on the screen.
UPDATE ER_CODELST
SET CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "NCI">NCI</OPTION>
<OPTION value = "nih">NIH (other than NCI)</OPTION>
<OPTION value = "dod">DOD</OPTION>
<OPTION value = "peerRevFund">Other peer reviewed funding (e.g. NSF, ACS, etc.)</OPTION>
<OPTION value = "privInd">Private Industry</OPTION>
<OPTION value = "privFound">Private Foundation</OPTION>
<OPTION value = "deptFunds">Departmental Funds</OPTION>
<OPTION value = "donorFunds">Donor Funds</OPTION>
<OPTION value = "unfunded">Unfunded</OPTION>
<OPTION value = "coopGrp">Cooperative group</OPTION>
</SELECT>'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP IN(
'fundingType1',
'fundingType2',
'fundingType3',
'fundingType4',
'fundingType5');

--adding the options to the checkbox
UPDATE ER_CODELST 
SET CODELST_CUSTOM_COL1='{chkArray:[
{data: "option1", display:"Personnel"},
{data: "option2", display:"Patient Care"},
{data: "option3", display:"Startup Costs"},
{data: "option4", display:"Study Agent / Device"},
{data: "option5", display:"Participant Remunerations"},
{data: "option6", display:"Ancillary Services"},
]}'
WHERE CODELST_TYPE='studyidtype'  AND CODELST_SUBTYP IN(
'fundProvChk1',
'fundProvChk2',
'fundProvChk3',
'fundProvChk4',
'fundProvChk5');

COMMIT;




