SET define off;
---hiding some of the dropdown options to match the values given in the config doc -bug id:20981 

update er_codelst set codelst_hide = 'Y' where pk_codelst = '7801' and codelst_type = 'study_division'  and codelst_subtyp = 'CANCER';
update er_codelst set codelst_hide = 'Y' where pk_codelst = '7802' and codelst_type = 'study_division'  and codelst_subtyp = 'PEDS';
update er_codelst set codelst_hide = 'Y' where pk_codelst = '7803' and codelst_type = 'study_division'  and codelst_subtyp = 'MEDICINE';
update er_codelst set codelst_hide = 'Y' where pk_codelst = '7805' and codelst_type = 'study_division'  and codelst_subtyp = 'SURGERY';


--Bug 21213 : Field: Blinding Change drop down choice Double - Blinded to Double - Blind 
update er_codelst set codelst_desc = 'Double-Blind' where codelst_type = 'blinding' and codelst_subtyp = 'doubleBlind' ;

COMMIT;


