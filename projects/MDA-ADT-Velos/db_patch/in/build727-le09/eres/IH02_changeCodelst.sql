SET DEFINE OFF;
SET SERVEROUTPUT ON;

DECLARE
  v_item_exists number := 0;  
BEGIN
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'phase' and CODELST_SUBTYP='pilotFeasibilty';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'phase','pilotFeasibilty','Feasability with N/A','N', 65, null);
		dbms_output.put_line('New row inserted');
	else
		dbms_output.put_line('New row inserted');
	end if;
	
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'studyscope' and CODELST_SUBTYP='studyscope_3';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'studyscope','studyscope_3','Multi-Center, MDACC lead','N', 3, null);
		dbms_output.put_line('New row inserted');
	else
		dbms_output.put_line('Row already exists');
	end if;
END;
/

update ER_CODELST set CODELST_CUSTOM_COL1='interventional' where codelst_type='studyPurpose' and 
CODELST_SUBTYP in ('diagnostic','prevention','screening','supportiveCare','treatment');
update ER_CODELST set CODELST_CUSTOM_COL1='interventional,correlative,observational' where codelst_type='studyPurpose' and 
CODELST_SUBTYP in ('healthSvcRsrch');
update ER_CODELST set CODELST_CUSTOM_COL1='correlative,observational' where codelst_type='studyPurpose' and 
CODELST_SUBTYP in ('epidemiologic','observational','other','outcomeTrial');
update ER_CODELST set CODELST_CUSTOM_COL1='correlative' where codelst_type='studyPurpose' and 
CODELST_SUBTYP in ('basicScience');

update ER_CODELST set CODELST_CUSTOM_COL1='irb' where codelst_type='studystat' and 
CODELST_SUBTYP in ('irb_add_info');

commit;
