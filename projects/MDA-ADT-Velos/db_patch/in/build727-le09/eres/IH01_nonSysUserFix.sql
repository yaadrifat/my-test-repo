SET DEFINE OFF;
SET SERVEROUTPUT ON;

DECLARE
   CURSOR c_nonsystem_nologin IS SELECT * FROM ER_USER WHERE USR_TYPE = 'N' and USR_LOGNAME is null;
   r_er_user er_user%ROWTYPE;
BEGIN
    OPEN c_nonsystem_nologin;
    LOOP
        FETCH c_nonsystem_nologin INTO r_er_user;
        EXIT WHEN c_nonsystem_nologin%NOTFOUND;
        BEGIN
            --dbms_output.put_line(r_er_user.usr_lastname);
            update ER_USER set USR_LOGNAME = 'nonsys-'||r_er_user.PK_USER where PK_USER = r_er_user.PK_USER;
        END;
    END LOOP;
    COMMIT;
    CLOSE c_nonsystem_nologin;
END;
/
