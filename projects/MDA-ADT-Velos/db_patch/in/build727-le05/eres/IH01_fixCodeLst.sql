set define off;
set serveroutput on;
declare
v_CodeExists number := 0;
v_CodeSeq number := 0;
begin
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'role' AND CODELST_SUBTYP = 'role_21' AND codelst_desc = 'Collaborators';
	IF (v_CodeExists > 0) THEN
		SELECT CODELST_SEQ INTO v_CodeSeq FROM ER_CODELST WHERE CODELST_TYPE = 'role' AND CODELST_SUBTYP = 'role_21' AND codelst_desc = 'Collaborators';
		update ER_CODELST set CODELST_HIDE='Y' WHERE CODELST_TYPE = 'role' AND CODELST_SUBTYP = 'role_21' AND codelst_desc = 'Collaborators';
		update ER_CODELST set CODELST_SEQ=v_CodeSeq WHERE CODELST_TYPE = 'role' AND CODELST_SUBTYP = 'Collaborators' AND codelst_desc = 'Collaborators';
		commit;
		dbms_output.put_line('Collaborators codelst row fixed');
	END IF;
end;
/
