SET define off;
--Biomarker and Correlative Studies : �Methodology � How?� to �Methodology�

update er_codelst set codelst_desc = 'Methodology' where CODELST_TYPE='studyidtype' AND codelst_subtyp = 'methodology' ;
 
-- In �Did an MDACC Biostatistician participate in the study design?�  remove double hyphens from values
UPDATE ER_CODELST
SET CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "YesComp">Yes Completely</OPTION>
<OPTION value = "YesPart">Yes Partially</OPTION>
<OPTION value = "NoSponsrDes">No Sponsor provided biostatistical design</OPTION>
<OPTION value = "NoProsStat">No prospective statistical design in this study</OPTION>
</SELECT>'

where CODELST_TYPE='studyidtype' AND codelst_subtyp = 'proDes2' ;

--Make first field into �Sponsor Name 1� (currently Sponsor Name1)
update er_codelst set codelst_desc = 'Sponsor Name 1' where codelst_type = 'studyidtype' and codelst_subtyp = 'sponsorName1' ;
update er_codelst set codelst_desc = 'Sponsor Name 2' where codelst_type = 'studyidtype' and codelst_subtyp = 'sponsorName2' ;
update er_codelst set codelst_desc = 'Sponsor Name 3' where codelst_type = 'studyidtype' and codelst_subtyp = 'sponsorName3' ;
update er_codelst set codelst_desc = 'Sponsor Name 4' where codelst_type = 'studyidtype' and codelst_subtyp = 'sponsorName4' ;
update er_codelst set codelst_desc = 'Sponsor Name 5' where codelst_type = 'studyidtype' and codelst_subtyp = 'sponsorName5' ;

--Device 1, Device 2, Device 3, etc. turns into Device-1, Device-2, etc. to stay consistent with Agent
update er_codelst set codelst_desc = 'Device-1' where codelst_type = 'studyidtype' and codelst_subtyp = 'deviceAgent1' ;
update er_codelst set codelst_desc = 'Device-2' where codelst_type = 'studyidtype' and codelst_subtyp = 'deviceAgent2' ;
update er_codelst set codelst_desc = 'Device-3' where codelst_type = 'studyidtype' and codelst_subtyp = 'deviceAgent3' ;
update er_codelst set codelst_desc = 'Device-4' where codelst_type = 'studyidtype' and codelst_subtyp = 'deviceAgent4' ;
update er_codelst set codelst_desc = 'Device-5' where codelst_type = 'studyidtype' and codelst_subtyp = 'deviceAgent5' ;

--For Agent 1, Agent 2, etc. fields should be relabeled into Agent-1, Agent-2, etc
update er_codelst set codelst_desc = 'Agent-1' where codelst_type = 'studyidtype' and codelst_subtyp = 'drugAgent1' ;
update er_codelst set codelst_desc = 'Agent-2' where codelst_type = 'studyidtype' and codelst_subtyp = 'drugAgent2' ;
update er_codelst set codelst_desc = 'Agent-3' where codelst_type = 'studyidtype' and codelst_subtyp = 'drugAgent3' ;
update er_codelst set codelst_desc = 'Agent-4' where codelst_type = 'studyidtype' and codelst_subtyp = 'drugAgent4' ;
update er_codelst set codelst_desc = 'Agent-5' where codelst_type = 'studyidtype' and codelst_subtyp = 'drugAgent5' ;
update er_codelst set codelst_desc = 'Agent-6' where codelst_type = 'studyidtype' and codelst_subtyp = 'drugAgent6' ;
update er_codelst set codelst_desc = 'Agent-7' where codelst_type = 'studyidtype' and codelst_subtyp = 'drugAgent7' ;
update er_codelst set codelst_desc = 'Agent-8' where codelst_type = 'studyidtype' and codelst_subtyp = 'drugAgent8' ;
update er_codelst set codelst_desc = 'Agent-9' where codelst_type = 'studyidtype' and codelst_subtyp = 'drugAgent9' ;
update er_codelst set codelst_desc = 'Agent-10' where codelst_type = 'studyidtype' and codelst_subtyp = 'drugAgent10' ;

COMMIT;