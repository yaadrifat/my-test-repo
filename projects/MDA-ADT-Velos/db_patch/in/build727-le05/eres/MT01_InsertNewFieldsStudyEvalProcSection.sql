set define off;
set serveroutput on;

-->Are fresh biopsies needed?
-->If yes, please describe : Displayed if 'Are fresh biopsies needed' is 'Yes'


declare
v_CodeExists number := 0;
begin
	
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'freshBiopsyNeed';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'freshBiopsyNeed', 'Are fresh biopsies needed?', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'freshBiopsyYes';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'freshBiopsyYes', 'If yes, please describe' , 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
COMMIT;
END;
/

