SET define off;
---adding the dropdown options to Are fresh biopsies needed dropdown

UPDATE ER_CODELST
SET CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP = 'freshBiopsyNeed';

COMMIT;



