set define off;
set serveroutput on;

-->funding provided checkbox


declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundProvChk1';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'fundProvChk1', 'Funding Provided', 'checkbox');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	--2--
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundProvChk2';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'fundProvChk2', 'Funding Provided', 'checkbox');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	--3--
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundProvChk3';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'fundProvChk3', 'Funding Provided', 'checkbox');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	--4--
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundProvChk4';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'fundProvChk4', 'Funding Provided', 'checkbox');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	--5--
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundProvChk5';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'fundProvChk5', 'Funding Provided', 'checkbox');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	
	
COMMIT;
END;
/

