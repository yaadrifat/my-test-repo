create or replace
PACKAGE BODY      PKG_SUBMISSION_LOGIC AS
/******************************************************************************
   NAME:       PKG_SUBMISSION_LOGIC
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        5/28/2009   Sonia Abrol          1. Created this package.

   This package is designed to club together custom logic needed to validate submissions
   There can be one or more functions written to perform validation checks. All functions defined in this package will
   follow following conventions:

   1. Pls see template function - f_check_cro(p_study IN NUMBER) as an example.
   2. The Function will take one parameter : PK of the study
   3. The function will return a table - t_nested_results_table
   4. The table will contain following information:
          result: Pass/Fail information.Possible values:
                  0 : Pass
                 -1 : Fail
                 -2 : Not Required

          result_text : Text to be displayed for results
          TestText - Descriptive text (if any) for the checks performed
   5. The table can return one or more rows to send check results


******************************************************************************/

Function f_check_irb(p_study IN NUMBER) return t_nested_results_table
AS
    v_tab_submission_results t_nested_results_table ;

    v_result number;
    v_result_text varchar2(1000);
    v_doc_count number;
    v_testText varchar2(1000);
    v_isPiMajAuthor NUMBER := 0;
    v_isMdaccSelect NUMBER := 0;
    v_mdacc_count number;
    v_industry_count number;
    v_version_number number;
    v_indide_count number;
    v_schevnt_count number;
begin

    v_tab_submission_results := t_nested_results_table();

    v_testText := 'Check for Uploaded Document in Protocol Prioritization Category';

    select MAX(f_to_number(studyver_number)) into v_version_number   from er_studyver where fk_study=p_study ;

	  select count(*)
	  into v_doc_count
	  from er_studyver v,er_studyapndx x
	  where v.fk_study = p_study and v.studyver_category = (select pk_codelst from er_codelst where codelst_type = 'studyvercat'
	   and codelst_subtyp = 'dept_chair') and x.fk_studyver = pk_studyver and studyapndx_type = 'file' and f_to_number(v.studyver_number)=v_version_number and v.studyver_status='W';

	   if v_doc_count > 0 then
			v_result := 0;
			v_result_text := 'Pass';
		else
			v_result := -1;
			v_result_text := 'Fail: No Document Uploaded in Protocol Prioritization Category';
		end if;

	  v_tab_submission_results.extend;
	  v_tab_submission_results(v_tab_submission_results.count) := tab_submission_results(v_result, v_result_text,v_testText );

     v_testText := 'Check for Uploaded Document in Study Calendar/Schedule of Events Category';
    
     select count(*)
	  into v_schevnt_count
	  from er_studyver v,er_studyapndx x
	  where v.fk_study = p_study and v.studyver_category = (select pk_codelst from er_codelst where codelst_type = 'studyvercat'
	   and codelst_subtyp = 'studCalSchEvnts') and x.fk_studyver = pk_studyver and studyapndx_type = 'file' and f_to_number(v.studyver_number)=v_version_number and v.studyver_status='W';

	   if v_schevnt_count > 0 then
			v_result := 0;
			v_result_text := 'Pass';
		else
			v_result := -1;
			v_result_text := 'Fail: No Document Uploaded in Study Calendar/Schedule of Events Category';
		end if;

	  v_tab_submission_results.extend;
	  v_tab_submission_results(v_tab_submission_results.count) := tab_submission_results(v_result, v_result_text,v_testText );
    
    
    select count(*)
    into v_industry_count
    from er_studyid
    where fk_study=p_study and studyid_id='indstrStdy';

    IF v_industry_count > 0 THEN

      v_testText := 'Check for Uploaded Document in Protocol Category';

      select count(*)
      into v_doc_count
      from er_studyver v,er_studyapndx x
      where v.fk_study = p_study and v.studyver_category = (select pk_codelst from er_codelst where codelst_type = 'studyvercat'
       and codelst_subtyp = 'protocol') and x.fk_studyver = pk_studyver and studyapndx_type = 'file' and f_to_number(v.studyver_number)=v_version_number and v.studyver_status='W';

       if v_doc_count > 0 then
          v_result := 0;
          v_result_text := 'Pass';
      else
          v_result := -1;
          v_result_text := 'Fail: No Document Uploaded in Protocol Category';
      end if;

      v_tab_submission_results.extend;
      v_tab_submission_results(v_tab_submission_results.count) := tab_submission_results(v_result, v_result_text,v_testText );

    END IF;

  select count(*)
  into v_mdacc_count
  from er_study_indide esi, er_codelst ec
  where esi.fk_study= p_study
  and indide_type=1
  and esi.fk_codelst_indide_holder = (select pk_codelst from er_codelst where codelst_type='INDIDEHolder' and codelst_desc='MDACC');

  IF v_mdacc_count > 0 THEN
    v_testText := 'Check for Uploaded Document in Investigator Brochure Category';

	  select count(*)
	  into v_doc_count
	  from er_studyver v,er_studyapndx x
	  where v.fk_study = p_study and v.studyver_category = (select pk_codelst from er_codelst where codelst_type = 'studyvercat'
	   and codelst_subtyp = 'inv_brochure') and x.fk_studyver = pk_studyver and studyapndx_type = 'file' and f_to_number(v.studyver_number)=v_version_number and v.studyver_status='W';

	   if v_doc_count > 0 then
			v_result := 0;
			v_result_text := 'Pass';
		else
			v_result := -1;
			v_result_text := 'Fail: No Document Uploaded in Investigator Brochure Category';
		end if;

	  v_tab_submission_results.extend;
	  v_tab_submission_results(v_tab_submission_results.count) := tab_submission_results(v_result, v_result_text,v_testText );

  END IF;

  v_tab_submission_results.extend;
  v_tab_submission_results(v_tab_submission_results.count) := f_check_inst_reqmts_done(p_study);
  v_tab_submission_results.extend;
  v_tab_submission_results(v_tab_submission_results.count) :=  f_check_data_handlng_done(p_study);
  v_tab_submission_results.extend;
  v_tab_submission_results(v_tab_submission_results.count) :=  f_check_dept_approval_done(p_study);

   SELECT COUNT(*) INTO v_isPiMajAuthor from ER_STUDYID where FK_STUDY= p_study AND STUDYID_ID = 'MDACCInvIni';
    IF v_isPiMajAuthor > 0 THEN
     v_tab_submission_results.extend;
     v_tab_submission_results(v_tab_submission_results.count) :=  f_check_bioStat_done(p_study);
   end if;

   SELECT COUNT(*) INTO v_isPiMajAuthor from er_studyteam t, er_codelst where t.fk_study = p_study
        and t.fk_codelst_tmrole = pk_codelst and codelst_type = 'role' and codelst_subtyp = 'Collaborators';
   IF v_isPiMajAuthor > 0 THEN
     v_tab_submission_results.extend;
     v_tab_submission_results(v_tab_submission_results.count) :=  f_check_collab_signoff_done(p_study);
   END IF;

   SELECT COUNT(*) INTO v_isPiMajAuthor from ER_STUDY where PK_STUDY= p_study AND FK_CODELST_SCOPE IN (
        SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'studyscope_3');
   SELECT COUNT(*) INTO v_isMdaccSelect from ER_STUDYID where FK_STUDY= p_study AND STUDYID_ID = 'MDACCInvIni';
   IF (v_isPiMajAuthor > 0 AND v_isMdaccSelect > 0) THEN
     v_tab_submission_results.extend;
     v_tab_submission_results(v_tab_submission_results.count) :=  f_check_multi_signoff_done(p_study);
  END IF;
  
  SELECT COUNT(*) INTO v_isPiMajAuthor from ER_STUDY where PK_STUDY= p_study AND FK_CODELST_SCOPE IN (
        SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'studyscope_3');
  SELECT COUNT(*) INTO v_isMdaccSelect from ER_STUDYID where FK_STUDY= p_study AND STUDYID_ID = 'MDACCInvIni';
   IF (v_isPiMajAuthor > 0 AND v_isMdaccSelect > 0) THEN
     v_tab_submission_results.extend;
     v_tab_submission_results(v_tab_submission_results.count) :=  f_check_multiConsider_done(p_study);
  END IF;

   SELECT COUNT(*) INTO v_isPiMajAuthor from ER_STUDY_INDIDE where FK_STUDY= p_study AND FK_CODELST_INDIDE_HOLDER = (
        SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'INDIDEHolder' AND CODELST_SUBTYP = 'organization');
   IF v_isPiMajAuthor > 0 THEN      
        select count(*)
    	into v_indide_count
    	from er_study_indide  
    	where fk_study=p_study and indide_type=1;
    	
    	IF v_indide_count > 0 THEN   
     		v_tab_submission_results.extend;
     		v_tab_submission_results(v_tab_submission_results.count) :=  f_check_ind_review_done(p_study);
     	END IF;
   END IF;

  v_tab_submission_results.extend;
  v_tab_submission_results(v_tab_submission_results.count) :=  f_check_nom_review_done(p_study);
  v_tab_submission_results.extend;
  v_tab_submission_results(v_tab_submission_results.count) :=  f_check_fin_support_done(p_study);

    select COUNT(*) INTO v_isPiMajAuthor from er_studyid where  fk_study = p_study  and pk_studyid in (Select pk_studyid from er_studyid, er_codelst
        where fk_study = p_study and  pk_codelst = FK_CODELST_IDTYPE and codelst_subtyp = 'stdyIncludes') and studyId_id like '%option2%';
   IF v_isPiMajAuthor > 0 THEN
     v_tab_submission_results.extend;
     v_tab_submission_results(v_tab_submission_results.count) :=  f_check_risk_Assmt_done(p_study);
   END IF ;
   
   select COUNT(*) INTO v_isPiMajAuthor from er_studyid where  fk_study = p_study  and pk_studyid in (Select pk_studyid from er_studyid, er_codelst
        where fk_study = p_study and  pk_codelst = FK_CODELST_IDTYPE and codelst_subtyp = 'freshBiopsyNeed') and studyId_id like '%Yes%';
   IF v_isPiMajAuthor > 0 THEN
     v_tab_submission_results.extend;
     v_tab_submission_results(v_tab_submission_results.count) :=  f_check_intervRadiology_done(p_study);
   END IF ;
   
   SELECT count(*) INTO v_isPiMajAuthor FROM ER_STUDYSTAT WHERE FK_STUDY = p_study AND FK_CODELST_STUDYSTAT = (
        SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'app_CHR');
  IF v_isPiMajAuthor > 0 THEN
    v_tab_submission_results.extend;
    v_tab_submission_results(v_tab_submission_results.count) :=  f_check_piActivation_done(p_study);
  END IF;

   SELECT count(*) INTO v_isPiMajAuthor FROM ER_STUDYSTAT WHERE FK_STUDY = p_study AND FK_CODELST_STUDYSTAT = (
        SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'app_CHR');
  IF v_isPiMajAuthor > 0 THEN
    v_tab_submission_results.extend;
    v_tab_submission_results(v_tab_submission_results.count) :=  f_check_prot_actvtn_done(p_study);
  END IF;

--------------
/*
      v_testText := 'Check for Uploaded Document in Informed Consent Category';

  select count(*)
  into v_doc_count
  from er_studyver v,er_studyapndx x
  where v.fk_study = p_study and v.studyver_category = (select pk_codelst from er_codelst where codelst_type = 'studyvercat'
   and codelst_subtyp = 'inf_consent') and x.fk_studyver = pk_studyver and studyapndx_type = 'file';

   if v_doc_count > 0 then
        v_result := 0;
        v_result_text := 'Pass';
    else
        v_result := -1;
        v_result_text := 'Fail: No Document Uploaded in Informed Consent Category';
    end if;

  v_tab_submission_results.extend;
  v_tab_submission_results(v_tab_submission_results.count) := tab_submission_results(v_result, v_result_text,v_testText );

*/
    return v_tab_submission_results;

end;

Function f_check_cro(p_study IN NUMBER) return t_nested_results_table
AS
    v_tab_submission_results t_nested_results_table ;

    v_result number;
    v_result_text varchar2(1000);

    v_testText varchar2(1000);
begin

    v_tab_submission_results := t_nested_results_table();

    v_testText := 'Check for Central Research Office';

    v_result := 0;
    v_result_text := 'Pass';

    v_tab_submission_results.extend;
    v_tab_submission_results(v_tab_submission_results.count) := tab_submission_results(v_result, v_result_text,v_testText );


    return v_tab_submission_results;

end;



Function f_check_rsc(p_study IN NUMBER) return t_nested_results_table
AS
    v_tab_submission_results t_nested_results_table ;

    v_result number;
    v_result_text varchar2(1000);

    v_testText varchar2(1000);
begin

    v_tab_submission_results := t_nested_results_table();

    v_testText := 'Radiation Safety Committee';

    v_result := 0;
    v_result_text := 'Pass';

    v_tab_submission_results.extend;
    v_tab_submission_results(v_tab_submission_results.count) := tab_submission_results(v_result, v_result_text,v_testText );


    return v_tab_submission_results;

end;

Function f_check_cic(p_study IN NUMBER) return t_nested_results_table
AS
    v_tab_submission_results t_nested_results_table ;

    v_result number;
    v_result_text varchar2(1000);

    v_testText varchar2(1000);
begin

    v_tab_submission_results := t_nested_results_table();

    v_testText := 'Conflict of Interest Committee';

    v_result := 0;
    v_result_text := 'Pass';

    v_tab_submission_results.extend;
    v_tab_submission_results(v_tab_submission_results.count) := tab_submission_results(v_result, v_result_text,v_testText );


    return v_tab_submission_results;

end;


Function f_check_src(p_study IN NUMBER) return t_nested_results_table
AS
    v_tab_submission_results t_nested_results_table ;

    v_result number;
    v_result_text varchar2(1000);

    v_testText varchar2(1000);
begin

    v_tab_submission_results := t_nested_results_table();

    v_testText := 'Scientific Review Committee';

    v_result := 0;
    v_result_text := 'Pass';

    v_tab_submission_results.extend;
    v_tab_submission_results(v_tab_submission_results.count) := tab_submission_results(v_result, v_result_text,v_testText );


    return v_tab_submission_results;

end;


Function f_check_inst_reqmts_done(p_study IN NUMBER) return tab_submission_results
AS

    v_result number;
    v_result_text varchar2(1000);
    v_doc_count number;
    v_testText varchar2(1000);
	v_isCompleted number;
	
begin


    v_testText := 'Check if Institutional Requirements Form saved for this study';

   SELECT count(*) INTO v_doc_count FROM ER_STUDYFORMS WHERE FK_STUDY = p_study
    AND FK_FORMLIB = (
    SELECT PK_FORMLIB from ER_FORMLIB , ER_LINKEDFORMS where PK_FORMLIB = ER_LINKEDFORMS.FK_FORMLIB
    AND ER_FORMLIB.RECORD_TYPE <> 'D' and ER_LINKEDFORMS.LF_DISPLAYTYPE = 'SA' and FORM_NAME = 'Institutional Requirements Form');
	
	SELECT COUNT(*) INTO v_isCompleted from ER_STUDYFORMS where FK_STUDY= p_study AND FORM_COMPLETED IN (
    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'fillformstat' AND CODELST_SUBTYP IN('complete','lockdown'));

   if (v_doc_count > 0 AND v_isCompleted > 0) then
        v_result := 0;
        v_result_text := 'Pass';
    else
        v_result := -1;
        v_result_text := 'Fail: Institutional Requirements Form not saved for this study';
    end if;


  return tab_submission_results(v_result, v_result_text,v_testText );

 end;

  Function f_check_data_handlng_done(p_study IN NUMBER) return tab_submission_results
AS
    v_tab_submission_results t_nested_results_table ;

    v_result number;
    v_result_text varchar2(1000);
    v_doc_count number;
    v_testText varchar2(1000);
	v_isCompleted number;
	
begin

    v_tab_submission_results := t_nested_results_table();

    v_testText := 'Check if Data Handling and Record Keeping Form saved for this study';

 SELECT count(*) INTO v_doc_count FROM ER_STUDYFORMS WHERE FK_STUDY = p_study
    AND FK_FORMLIB = (
    SELECT PK_FORMLIB from ER_FORMLIB , ER_LINKEDFORMS where PK_FORMLIB = ER_LINKEDFORMS.FK_FORMLIB
    AND ER_FORMLIB.RECORD_TYPE <> 'D' and ER_LINKEDFORMS.LF_DISPLAYTYPE = 'SA'and FORM_NAME = 'Data Handling and Record Keeping');
	
	SELECT COUNT(*) INTO v_isCompleted from ER_STUDYFORMS where FK_STUDY= p_study AND FORM_COMPLETED IN (
    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'fillformstat' AND CODELST_SUBTYP IN('complete','lockdown'));

   if (v_doc_count > 0 AND v_isCompleted > 0) then
        v_result := 0;
        v_result_text := 'Pass';
    else
        v_result := -1;
        v_result_text := 'Fail: Data Handling and Record Keeping Form not saved for this study';
    end if;


  return tab_submission_results(v_result, v_result_text,v_testText );
 end;

  Function f_check_dept_approval_done(p_study IN NUMBER) return tab_submission_results
AS
    v_tab_submission_results t_nested_results_table ;

    v_result number;
    v_result_text varchar2(1000);
    v_doc_count number;
    v_testText varchar2(1000);
	v_isCompleted number;
begin

    v_tab_submission_results := t_nested_results_table();

    v_testText := 'Check if Department / Divisional Approval Form saved for this study';

    SELECT count(*) INTO v_doc_count FROM ER_STUDYFORMS WHERE FK_STUDY = p_study
    AND FK_FORMLIB = (
    SELECT PK_FORMLIB from ER_FORMLIB , ER_LINKEDFORMS where PK_FORMLIB = ER_LINKEDFORMS.FK_FORMLIB
    AND ER_FORMLIB.RECORD_TYPE <> 'D' and ER_LINKEDFORMS.LF_DISPLAYTYPE = 'SA' and FORM_NAME = 'Department / Divisional Approval');
	
	SELECT COUNT(*) INTO v_isCompleted from ER_STUDYFORMS where FK_STUDY= p_study AND FORM_COMPLETED IN (
    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'fillformstat' AND CODELST_SUBTYP IN('complete','lockdown'));

   if (v_doc_count > 0 AND v_isCompleted > 0) then
        v_result := 0;
        v_result_text := 'Pass';
    else
        v_result := -1;
        v_result_text := 'Fail: Department / Divisional Approval Form not saved for this study';
    end if;

  return tab_submission_results(v_result, v_result_text,v_testText );
 end;

  Function f_check_bioStat_done(p_study IN NUMBER) return tab_submission_results
AS
    v_tab_submission_results t_nested_results_table ;

    v_result number;
    v_result_text varchar2(1000);
    v_doc_count number;
    v_testText varchar2(1000);
    v_isPiMajAuthor NUMBER := 0;
	v_isCompleted number;
begin

    v_tab_submission_results := t_nested_results_table();


     v_testText := 'Check if Biostat Signoff Form saved for this study';
    BEGIN
      SELECT count(*) INTO v_doc_count FROM ER_STUDYFORMS WHERE FK_STUDY = p_study
      AND FK_FORMLIB = (
      SELECT PK_FORMLIB from ER_FORMLIB , ER_LINKEDFORMS where PK_FORMLIB = ER_LINKEDFORMS.FK_FORMLIB
      AND ER_FORMLIB.RECORD_TYPE <> 'D' and ER_LINKEDFORMS.LF_DISPLAYTYPE = 'SA' and FORM_NAME = 'Biostat Signoff');
	  
	  SELECT COUNT(*) INTO v_isCompleted from ER_STUDYFORMS where FK_STUDY= p_study AND FORM_COMPLETED IN (
      SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'fillformstat' AND CODELST_SUBTYP IN('complete','lockdown'));

     if (v_doc_count > 0 AND v_isCompleted > 0) then
        v_result := 0;
        v_result_text := 'Pass';
     else
        v_result := -1;
        v_result_text := 'Fail: Biostat Signoff Form not saved for this study';
     end if;
     END;

  return tab_submission_results(v_result, v_result_text,v_testText );
 end;



  Function f_check_collab_signoff_done(p_study IN NUMBER) return tab_submission_results
AS
    v_tab_submission_results t_nested_results_table ;

    v_result number;
    v_result_text varchar2(1000);
    v_doc_count number;
    v_testText varchar2(1000);
    v_isCollaborator NUMBER := 0;
	v_isCompleted number;
begin

    v_tab_submission_results := t_nested_results_table();

    v_testText := 'Check if Collaborator Signoff Form saved for this study';
      BEGIN
        SELECT count(*) INTO v_doc_count FROM ER_STUDYFORMS WHERE FK_STUDY = p_study
        AND FK_FORMLIB = (
        SELECT PK_FORMLIB from ER_FORMLIB , ER_LINKEDFORMS where PK_FORMLIB = ER_LINKEDFORMS.FK_FORMLIB
        AND ER_FORMLIB.RECORD_TYPE <> 'D' and ER_LINKEDFORMS.LF_DISPLAYTYPE = 'SA' and FORM_NAME = 'Collaborator Signoff');
		
		SELECT COUNT(*) INTO v_isCompleted from ER_STUDYFORMS where FK_STUDY= p_study AND FORM_COMPLETED IN (
        SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'fillformstat' AND CODELST_SUBTYP IN('complete','lockdown'));

        if (v_doc_count > 0 AND v_isCompleted > 0) then
          v_result := 0;
          v_result_text := 'Pass';
       else
        v_result := -1;
        v_result_text := 'Fail: Collaborator Signoff Form not saved for this study';
       end if;
     END;

  return tab_submission_results(v_result, v_result_text,v_testText );
 end;



  Function f_check_multi_signoff_done(p_study IN NUMBER) return tab_submission_results
AS
    v_tab_submission_results t_nested_results_table ;

    v_result number;
    v_result_text varchar2(1000);
    v_doc_count number;
    v_testText varchar2(1000);
    v_isMultiSite NUMBER := 0;
	v_isCompleted number;
begin

    v_tab_submission_results := t_nested_results_table();

    SELECT COUNT(*) INTO v_isMultiSite from ER_STUDY where PK_STUDY= p_study AND FK_CODELST_SCOPE IN (
        SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP IN('studyscope_3','std_cen_multi','onlyOther'));
     IF v_isMultiSite > 0 THEN
     BEGIN
     v_testText := 'Check if Multicenter Signoff Form saved for this study';
        SELECT count(*) INTO v_doc_count FROM ER_STUDYFORMS WHERE FK_STUDY = p_study
        AND FK_FORMLIB = (
        SELECT PK_FORMLIB from ER_FORMLIB , ER_LINKEDFORMS where PK_FORMLIB = ER_LINKEDFORMS.FK_FORMLIB
        AND ER_FORMLIB.RECORD_TYPE <> 'D' and ER_LINKEDFORMS.LF_DISPLAYTYPE = 'SA' and FORM_NAME = 'Multicenter Signoff');
		
		SELECT COUNT(*) INTO v_isCompleted from ER_STUDYFORMS where FK_STUDY= p_study AND FORM_COMPLETED IN (
        SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'fillformstat' AND CODELST_SUBTYP IN('complete','lockdown'));

     if (v_doc_count > 0 AND v_isCompleted > 0) then
          v_result := 0;
          v_result_text := 'Pass';
     else
         v_result := -1;
         v_result_text := 'Fail: Multicenter Signoff Form not saved for this study';
     end if;
    END;
  END IF;

  return tab_submission_results(v_result, v_result_text,v_testText );
 end;

  Function f_check_ind_review_done(p_study IN NUMBER) return tab_submission_results
AS
    v_tab_submission_results t_nested_results_table ;

    v_result number;
    v_result_text varchar2(1000);
    v_doc_count number;
    v_testText varchar2(1000);
    v_isHolderMDACC NUMBER := 0;
	v_isCompleted number;
begin
    v_tab_submission_results := t_nested_results_table();
    v_testText := 'Check if IND Review Signoff Form saved for this study';
    BEGIN
      SELECT count(*) INTO v_doc_count FROM ER_STUDYFORMS WHERE FK_STUDY = p_study
      AND FK_FORMLIB = (
      SELECT PK_FORMLIB from ER_FORMLIB , ER_LINKEDFORMS where PK_FORMLIB = ER_LINKEDFORMS.FK_FORMLIB
      AND ER_FORMLIB.RECORD_TYPE <> 'D' and ER_LINKEDFORMS.LF_DISPLAYTYPE = 'SA' and FORM_NAME = 'IND Signoff');
	  
	  SELECT COUNT(*) INTO v_isCompleted from ER_STUDYFORMS where FK_STUDY= p_study AND FORM_COMPLETED IN (
      SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'fillformstat' AND CODELST_SUBTYP IN('complete','lockdown'));

     if (v_doc_count > 0 AND v_isCompleted > 0) then
          v_result := 0;
          v_result_text := 'Pass';
     else
         v_result := -1;
         v_result_text := 'Fail: IND Review Form not saved for this study';
      end if;
    END;  
   
  return tab_submission_results(v_result, v_result_text,v_testText );
 
end;


  Function f_check_nom_review_done(p_study IN NUMBER) return tab_submission_results
AS
    v_tab_submission_results t_nested_results_table ;

    v_result number;
    v_result_text varchar2(1000);
    v_doc_count number;
    v_testText varchar2(1000);
	v_isCompleted number;
begin

    v_tab_submission_results := t_nested_results_table();

    v_testText := 'Check if Nominated Reviewer Signoff Form saved for this study';

    SELECT count(*) INTO v_doc_count FROM ER_STUDYFORMS WHERE FK_STUDY = p_study
    AND FK_FORMLIB = (
    SELECT PK_FORMLIB from ER_FORMLIB , ER_LINKEDFORMS where PK_FORMLIB = ER_LINKEDFORMS.FK_FORMLIB
    AND ER_FORMLIB.RECORD_TYPE <> 'D' and ER_LINKEDFORMS.LF_DISPLAYTYPE = 'SA' and FORM_NAME = 'Nominated Reviewer Signoff' );
	
	SELECT COUNT(*) INTO v_isCompleted from ER_STUDYFORMS where FK_STUDY= p_study AND FORM_COMPLETED IN (
    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'fillformstat' AND CODELST_SUBTYP IN('complete','lockdown'));

   if (v_doc_count > 0 AND v_isCompleted > 0) then
        v_result := 0;
        v_result_text := 'Pass';
    else
        v_result := -1;
        v_result_text := 'Fail: Nominated Reviewer Signoff form not saved for this study';
    end if;

  return tab_submission_results(v_result, v_result_text,v_testText );

end;

  Function f_check_fin_support_done(p_study IN NUMBER) return tab_submission_results
AS
    v_tab_submission_results t_nested_results_table ;

    v_result number;
    v_result_text varchar2(1000);
    v_doc_count number;
    v_testText varchar2(1000);
	v_isCompleted number;
begin

    v_tab_submission_results := t_nested_results_table();

    v_testText := 'Check if Financial Support Form saved for this study';

     SELECT count(*) INTO v_doc_count FROM ER_STUDYFORMS WHERE FK_STUDY = p_study
    AND FK_FORMLIB = (
    SELECT PK_FORMLIB from ER_FORMLIB , ER_LINKEDFORMS where PK_FORMLIB = ER_LINKEDFORMS.FK_FORMLIB
    AND ER_FORMLIB.RECORD_TYPE <> 'D' and ER_LINKEDFORMS.LF_DISPLAYTYPE = 'SA' and FORM_NAME = 'Financial Support Form' );
	
	SELECT COUNT(*) INTO v_isCompleted from ER_STUDYFORMS where FK_STUDY= p_study AND FORM_COMPLETED IN (
    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'fillformstat' AND CODELST_SUBTYP IN('complete','lockdown'));

   if (v_doc_count > 0 AND v_isCompleted > 0) then
        v_result := 0;
        v_result_text := 'Pass';
    else
        v_result := -1;
        v_result_text := 'Fail: Financial Support Form not saved for this study';
    end if;

  return tab_submission_results(v_result, v_result_text,v_testText );
 end;

  Function f_check_risk_Assmt_done(p_study IN NUMBER) return tab_submission_results
AS
    v_tab_submission_results t_nested_results_table ;

    v_result number;
    v_result_text varchar2(1000);
    v_doc_count number;
    v_testText varchar2(1000);
    v_isDeviceSelected NUMBER := 0;
	v_isCompleted number;
begin

    v_tab_submission_results := t_nested_results_table();
    v_testText := 'Check if Risk Assessment Form saved for this study';
     BEGIN
      SELECT count(*) INTO v_doc_count FROM ER_STUDYFORMS WHERE FK_STUDY = p_study
      AND FK_FORMLIB = (
      SELECT PK_FORMLIB from ER_FORMLIB , ER_LINKEDFORMS where PK_FORMLIB = ER_LINKEDFORMS.FK_FORMLIB
      AND ER_FORMLIB.RECORD_TYPE <> 'D' and ER_LINKEDFORMS.LF_DISPLAYTYPE = 'SA' and FORM_NAME = 'Risk Assessment Form');
	  
	  SELECT COUNT(*) INTO v_isCompleted from ER_STUDYFORMS where FK_STUDY= p_study AND FORM_COMPLETED IN (
      SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'fillformstat' AND CODELST_SUBTYP IN('complete','lockdown'));

        if (v_doc_count > 0 AND v_isCompleted > 0) then
          v_result := 0;
          v_result_text := 'Pass';
        else
          v_result := -1;
          v_result_text := 'Fail: Risk Assessment Form not saved for this study';
        end if;
       END;

  return tab_submission_results(v_result, v_result_text,v_testText );
 end;

  Function f_check_prot_actvtn_done(p_study IN NUMBER) return tab_submission_results
AS
    v_tab_submission_results t_nested_results_table ;

    v_result number;
    v_result_text varchar2(1000);
    v_doc_count number;
    v_testText varchar2(1000);
    v_isIrbApproved NUMBER := 0;
	v_isCompleted number;
begin

    v_tab_submission_results := t_nested_results_table();
    v_testText := 'Check if Protocol Activation Form saved for this study';
      BEGIN
        SELECT count(*) INTO v_doc_count FROM ER_STUDYFORMS WHERE FK_STUDY = p_study
        AND FK_FORMLIB = (
        SELECT PK_FORMLIB from ER_FORMLIB , ER_LINKEDFORMS where PK_FORMLIB = ER_LINKEDFORMS.FK_FORMLIB
        AND ER_FORMLIB.RECORD_TYPE <> 'D' and ER_LINKEDFORMS.LF_DISPLAYTYPE = 'SA' and FORM_NAME = 'Protocol Activation');
		
		SELECT COUNT(*) INTO v_isCompleted from ER_STUDYFORMS where FK_STUDY= p_study AND FORM_COMPLETED IN (
        SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'fillformstat' AND CODELST_SUBTYP IN('complete','lockdown'));

       if (v_doc_count > 0 AND v_isCompleted > 0) then
          v_result := 0;
          v_result_text := 'Pass';
      else
        v_result := -1;
        v_result_text := 'Fail: Protocol Activation form not saved for this study';
      end if;
     END;

  return tab_submission_results(v_result, v_result_text,v_testText );
 end;


 Function f_check_piActivation_done(p_study IN NUMBER) return tab_submission_results
AS
    v_tab_submission_results t_nested_results_table ;

    v_result number;
    v_result_text varchar2(1000);
    v_doc_count number;
    v_testText varchar2(1000);
    v_isIrbApproved NUMBER := 0;
	v_isCompleted number;
begin

    v_tab_submission_results := t_nested_results_table();
    v_testText := 'Check if PI Activation Request Form saved for this study';
      BEGIN
        SELECT count(*) INTO v_doc_count FROM ER_STUDYFORMS WHERE FK_STUDY = p_study
        AND FK_FORMLIB = (
        SELECT PK_FORMLIB from ER_FORMLIB , ER_LINKEDFORMS where PK_FORMLIB = ER_LINKEDFORMS.FK_FORMLIB
        AND ER_FORMLIB.RECORD_TYPE <> 'D' and ER_LINKEDFORMS.LF_DISPLAYTYPE = 'SA' and FORM_NAME = 'PI Activation Request');
		
		SELECT COUNT(*) INTO v_isCompleted from ER_STUDYFORMS where FK_STUDY= p_study AND FORM_COMPLETED IN (
        SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'fillformstat' AND CODELST_SUBTYP IN('complete','lockdown'));

       if (v_doc_count > 0 AND v_isCompleted > 0) then
          v_result := 0;
          v_result_text := 'Pass';
      else
        v_result := -1;
        v_result_text := 'Fail: PI Activation Request form not saved for this study';
      end if;
     END;

  return tab_submission_results(v_result, v_result_text,v_testText );
 end;
 
 
 Function f_check_intervRadiology_done(p_study IN NUMBER) return tab_submission_results
AS

    v_result number;
    v_result_text varchar2(1000);
    v_doc_count number;
    v_testText varchar2(1000);
	v_isCompleted number;
begin


    v_testText := 'Check if Interventional Radiology Form saved for this study';

   SELECT count(*) INTO v_doc_count FROM ER_STUDYFORMS WHERE FK_STUDY = p_study
    AND FK_FORMLIB = (
    SELECT PK_FORMLIB from ER_FORMLIB , ER_LINKEDFORMS where PK_FORMLIB = ER_LINKEDFORMS.FK_FORMLIB
    AND ER_FORMLIB.RECORD_TYPE <> 'D' and ER_LINKEDFORMS.LF_DISPLAYTYPE = 'SA' and FORM_NAME = 'Interventional Radiology');
	
	SELECT COUNT(*) INTO v_isCompleted from ER_STUDYFORMS where FK_STUDY= p_study AND FORM_COMPLETED IN (
    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'fillformstat' AND CODELST_SUBTYP IN('complete','lockdown'));

   if (v_doc_count > 0 AND v_isCompleted > 0) then
        v_result := 0;
        v_result_text := 'Pass';
    else
        v_result := -1;
        v_result_text := 'Fail: Interventional Radiology Form not saved for this study';
    end if;


  return tab_submission_results(v_result, v_result_text,v_testText );

 end; 
 
 
Function f_check_multiConsider_done(p_study IN NUMBER) return tab_submission_results
AS

    v_result number;
    v_result_text varchar2(1000);
    v_doc_count number;
    v_testText varchar2(1000);
	v_isCompleted number;
begin


    v_testText := 'Check if Multicenter Considerations Form saved for this study';

   SELECT count(*) INTO v_doc_count FROM ER_STUDYFORMS WHERE FK_STUDY = p_study
    AND FK_FORMLIB = (
    SELECT PK_FORMLIB from ER_FORMLIB , ER_LINKEDFORMS where PK_FORMLIB = ER_LINKEDFORMS.FK_FORMLIB
    AND ER_FORMLIB.RECORD_TYPE <> 'D' and ER_LINKEDFORMS.LF_DISPLAYTYPE = 'SA' and FORM_NAME = 'Multicenter Considerations');
	
	SELECT COUNT(*) INTO v_isCompleted from ER_STUDYFORMS where FK_STUDY= p_study AND FORM_COMPLETED IN (
    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'fillformstat' AND CODELST_SUBTYP IN('complete','lockdown'));

   if (v_doc_count > 0 AND v_isCompleted > 0) then
        v_result := 0;
        v_result_text := 'Pass';
    else
        v_result := -1;
        v_result_text := 'Fail: Multicenter Considerations Form not saved for this study';
    end if;


  return tab_submission_results(v_result, v_result_text,v_testText );

 end;
 
END PKG_SUBMISSION_LOGIC;