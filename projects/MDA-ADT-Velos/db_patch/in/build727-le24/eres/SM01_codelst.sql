set define off;

update er_codelst set codelst_desc = 'Partial Blind' where codelst_type = 'blinding' and codelst_subtyp ='prtlBlnd';

update er_codelst set codelst_desc = 'Triple Blind' where codelst_type = 'blinding' and codelst_subtyp ='bindingTriple';

commit;