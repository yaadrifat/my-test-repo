SET define OFF;
DECLARE
  v_record_exists NUMBER := 0;
BEGIN
  SELECT COUNT(*) INTO v_record_exists FROM er_codelst WHERE CODELST_DESC = 'Resubmission Memo Response' and Codelst_Type='studyvercat';
  IF (v_record_exists = 0) THEN
    INSERT INTO ER_CODELST 
		(PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ)
	VALUES(
		SEQ_ER_CODELST.nextval,  'studyvercat', 'resubMemoResp', 'Resubmission Memo Response', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studyvercat'));
    COMMIT;
  END IF;

END;
/