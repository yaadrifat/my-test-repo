SET DEFINE OFF;

update ERES.ER_CODELST set CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "YesComp">Yes -- Completely</OPTION>
<OPTION value = "YesPart">Yes -- Partially</OPTION>
<OPTION value = "NoSponsrDes">No -- Sponsor provided biostatistical design</OPTION>
<OPTION value = "NoProsStat">No prospective statistical design in this study</OPTION>
</SELECT>'
where CODELST_TYPE='studyidtype' and CODELST_SUBTYP='proDes2';

update ERES.ER_CODELST set CODELST_DESC='Risk Assessment Form 1' where
CODELST_TYPE='studyidtype' and CODELST_SUBTYP='riskAssmt1';
update ERES.ER_CODELST set CODELST_DESC='Risk Assessment Form 2' where
CODELST_TYPE='studyidtype' and CODELST_SUBTYP='riskAssmt2';
update ERES.ER_CODELST set CODELST_DESC='Risk Assessment Form 3' where
CODELST_TYPE='studyidtype' and CODELST_SUBTYP='riskAssmt3';
update ERES.ER_CODELST set CODELST_DESC='Risk Assessment Form 4' where
CODELST_TYPE='studyidtype' and CODELST_SUBTYP='riskAssmt4';
update ERES.ER_CODELST set CODELST_DESC='Risk Assessment Form 5' where
CODELST_TYPE='studyidtype' and CODELST_SUBTYP='riskAssmt5';

update ERES.ER_CODELST set CODELST_DESC='Multi-Center' where
CODELST_TYPE='studyscope' and CODELST_SUBTYP='std_cen_multi';
update ERES.ER_CODELST set CODELST_DESC='Multi-Center, MDACC lead' where
CODELST_TYPE='studyscope' and CODELST_SUBTYP='studyscope_3';

update ERES.ER_CODELST set CODELST_CUSTOM_COL1=
'<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>' where
CODELST_TYPE='studyidtype' and CODELST_SUBTYP='biosafety3';

update ERES.ER_CODELST set CODELST_HIDE='Y' where
CODELST_TYPE='studySiteType' and CODELST_SUBTYP='responsible';

update ERES.ER_CODELST set CODELST_DESC='Feasibility with N/A' where
CODELST_TYPE='phase' and CODELST_SUBTYP='pilotFeasibilty';

update ER_CODELST set CODELST_DESC='Phase 5', CODELST_HIDE='Y' where
CODELST_TYPE='phase' and CODELST_SUBTYP='phase_5';

update ER_CODELST set CODELST_DESC='Phase 1/2', CODELST_HIDE='N' where
CODELST_TYPE='phase' and CODELST_SUBTYP='phaseI/II';

update ER_CODELST set CODELST_SUBTYP='phaseII/III', CODELST_DESC='Phase 2-3', 
CODELST_HIDE='N',  CODELST_SEQ = 30 where
CODELST_TYPE='phase' and CODELST_SUBTYP='phase_3';

update ER_CODELST set CODELST_HIDE='N' where
CODELST_TYPE='randomization' and CODELST_SUBTYP='blckRndmztn';

commit;
