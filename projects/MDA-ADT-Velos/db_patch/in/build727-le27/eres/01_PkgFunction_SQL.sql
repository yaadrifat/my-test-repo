set define off;

CREATE OR REPLACE PACKAGE "PKG_MDAREPORT"
IS
  FUNCTION getMDAReport(pk_study IN VARCHAR2) RETURN CLOB;
   FUNCTION f_INDTYPE
(
    p_cursor sys_refcursor,
    p_del VARCHAR2 := ','
) RETURN VARCHAR2;

FUNCTION f_EXPANDED_EXEMPT
(
    p_cursor sys_refcursor,
    p_del VARCHAR2 := ','
) RETURN VARCHAR2;

FUNCTION F_GetProtcol_Versn 
   (
   arg1 NUMBER
   ) RETURN VARCHAR2;
END PKG_MDAREPORT;
/
CREATE OR REPLACE SYNONYM ESCH.PKG_MDAREPORT FOR PKG_MDAREPORT;

CREATE OR REPLACE PACKAGE BODY "ERES"."PKG_MDAREPORT"
AS
 FUNCTION getMDAReport(pk_study IN VARCHAR2) RETURN CLOB IS v_sql CLOB;
                initData CLOB;--this is a test function
                retdata CLOB;
                Begin
                initData:='SELECT es.study_title as STUDY_TITLE,es.CCSG_REPORTABLE_STUDY as SIGNEDINFO,es.STUDY_NSAMPLSIZE as TOTENROLLATMDACC,es.study_number,es.study_sponsorid,es.study_icdcode1,es.study_icdcode2,es.study_obj_clob,
                (SELECT usr.USR_FIRSTNAME || '' '' || usr.USR_LASTNAME FROM er_study,er_user usr WHERE er_study.pk_study IN (' || pk_study || ') AND er_study.STUDY_PRINV=usr.pk_user
                  ) AS principal_investigator,
                  (SELECT usr.USR_FIRSTNAME || '' '' || usr.USR_LASTNAME FROM er_study,er_user usr WHERE er_study.pk_study IN (' || pk_study || ') AND er_study.STUDY_COORDINATOR=usr.pk_user
                  ) AS STD_CNTCT,

                  (SELECT f_codelst_desc(fk_codelst_studystat)as STUDYSTATUS from er_studystat where er_studystat.fk_study in(' || pk_study || ') and er_studystat.current_stat=1 ) AS STUDYSTAT,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Secondary Objective''
                  ) AS Secondary_Objective,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Exploratory Objective''
                  ) AS EXPLORATORYOBJ,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Biomarker Objective''
                  ) AS BIOMARKEROBJ,

                  (SELECT F_DISPOPTION(ers.studyid_id,ers.fk_codelst_idtype) FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_subtyp=''stdyIncludes''
                  ) AS STDINCLDS,

                (SELECT F_DISPOPTION(ers.studyid_id,ers.fk_codelst_idtype) FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_subtyp=''offStdyCri''
                  ) AS OFFSTUDYCRITERIA,

                  (SELECT F_DISPOPTION(ers.studyid_id,ers.fk_codelst_idtype) FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_subtyp=''selOthrObjEntry''
                  ) AS SLCTOTROBJFORENTRY,

                  (SELECT F_DISPOPTION(ers.studyid_id,ers.fk_codelst_idtype) FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_subtyp=''measureQues''
                  ) AS WHATRYUMEASURNG,

                  (SELECT F_DISPOPTION(ers.studyid_id,ers.fk_codelst_idtype) FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_subtyp=''timePoints''
                  ) AS TIMEPTS,

                 (SELECT pkg_mdareport.F_GetProtcol_Versn(' || pk_study || ') FROM dual) AS PROTVERSN,
                  
                  (select F_GETDIS_SITE(study_disease_site) from er_study where pk_study IN (' || pk_study || ')) AS Disease_Site,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Randomization notes''
                  ) AS Randomization_Notes,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Blinding Description''
                  ) AS Blinding_Description,
                   (SELECT erc.codelst_desc FROM er_study es,er_codelst erc WHERE es.pk_study IN (' || pk_study || ') AND es.FK_CODELST_SCOPE=erc.pk_codelst
                  and erc.codelst_type=''studyscope''
                  ) AS Study_Scope,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_subtyp=''accrual1''
                  ) AS ACCURAL1,
                  (select F_GETDIS_SITE(study_disease_site) from er_study where pk_study IN (' || pk_study || ')) AS Disease_Site,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Treatment Arms''
                  ) AS Treatment_Arms,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Total number enrolled at all sites''
                  ) AS Tot_nmbr_enrolatallsites,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Inclusion Criteria''
                  ) AS Inclusion_Criteria,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Exclusion Criteria''
                  ) AS Exclusion_Criteria,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Inclusion Criteria N/A''
                  ) AS Inclusion_Criteria_NA ,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Exclusion Criteria N/A''
                  ) AS Exclusion_CriteriaNA,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                    and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Does this protocol require an IND?''
                    ) AS Ds_ths_prt_reqnIND,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                    and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Confirm that the protocol <br>meets all criteria for exemption according to 21CFR312.2(b) <br>noted here?''
                    ) AS Cnfrm_tht_th_prt,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Rationale for exemption''
                  ) AS Rationale_for_exemption,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                 and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Agent-1''
                  ) AS Agent1,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                 and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Dose'' and erc.codelst_subtyp=''dose''
                  ) AS Dose,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                 and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Dose Rationale'' and erc.codelst_subtyp=''doseRationale''
                  ) AS Dose_Rationale,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                 and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Route of Administration'' and erc.codelst_subtyp=''route''
                  ) AS Rut_f_Admin,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                 and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Agent-2'' and erc.codelst_subtyp=''drugAgent2''
                  ) AS Agent2,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                 and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Dose'' and erc.codelst_subtyp=''dose2''
                  ) AS Dose2,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                 and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Dose Rationale'' and erc.codelst_subtyp=''doseRatnl2''
                  ) AS Dose_Rationale2,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                 and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Route of Administration'' and erc.codelst_subtyp=''routeAdmn2''
                  ) AS Rut_f_Admin2,
                  (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                 and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Agent-3'' and erc.codelst_subtyp=''drugAgent3''
                  ) AS Agent3,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                 and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Dose'' and erc.codelst_subtyp=''dose3''
                  ) AS Dose3,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                 and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Dose Rationale'' and erc.codelst_subtyp=''doseRatnl3''
                  ) AS Dose_Rationale3,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                 and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Route of Administration'' and erc.codelst_subtyp=''routeAdmn3''
                  ) AS Rut_f_Admin3,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                 and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Agent-4'' and erc.codelst_subtyp=''drugAgent4''
                  ) AS Agent4,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                 and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Dose'' and erc.codelst_subtyp=''dose4''
                  ) AS Dose4,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                 and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Dose Rationale'' and erc.codelst_subtyp=''doseRatnl4''
                  ) AS Dose_Rationale4,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                 and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Route of Administration'' and erc.codelst_subtyp=''routeAdmn4''
                  ) AS Rut_f_Admin4,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                 and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Agent-5'' and erc.codelst_subtyp=''drugAgent5''
                  ) AS Agent5,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                 and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Dose'' and erc.codelst_subtyp=''dose5''
                  ) AS Dose5,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                 and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Dose Rationale'' and erc.codelst_subtyp=''doseRatnl5''
                  ) AS Dose_Rationale5,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                 and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Route of Administration'' and erc.codelst_subtyp=''routeAdmn5''
                  ) AS Rut_f_Admin5,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                 and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Agent-6'' and erc.codelst_subtyp=''drugAgent6''
                  ) AS Agent6,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                 and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Dose'' and erc.codelst_subtyp=''dose6''
                  ) AS Dose6,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                 and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Dose Rationale'' and erc.codelst_subtyp=''doseRatnl6''
                  ) AS Dose_Rationale6,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                 and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Route of Administration'' and erc.codelst_subtyp=''routeAdmn6''
                  ) AS Rut_f_Admin6,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                 and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Agent-7'' and erc.codelst_subtyp=''drugAgent7''
                  ) AS Agent7,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                 and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Dose'' and erc.codelst_subtyp=''dose7''
                  ) AS Dose7,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                 and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Dose Rationale'' and erc.codelst_subtyp=''doseRatnl7''
                  ) AS Dose_Rationale7,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                 and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Route of Administration'' and erc.codelst_subtyp=''routeAdmn7''
                  ) AS Rut_f_Admin7,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                 and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Agent-8'' and erc.codelst_subtyp=''drugAgent8''
                  ) AS Agent8,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                 and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Dose'' and erc.codelst_subtyp=''dose8''
                  ) AS Dose8,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                 and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Dose Rationale'' and erc.codelst_subtyp=''doseRatnl8''
                  ) AS Dose_Rationale8,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                 and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Route of Administration'' and erc.codelst_subtyp=''routeAdmn8''
                  ) AS Rut_f_Admin8,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                 and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Agent-9'' and erc.codelst_subtyp=''drugAgent9''
                  ) AS Agent9,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                 and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Dose'' and erc.codelst_subtyp=''dose9''
                  ) AS Dose9,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                 and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Dose Rationale'' and erc.codelst_subtyp=''doseRatnl9''
                  ) AS Dose_Rationale9,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                 and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Route of Administration'' and erc.codelst_subtyp=''routeAdmn9''
                  ) AS Rut_f_Admin9,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                 and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Agent-10'' and erc.codelst_subtyp=''drugAgent10''
                  ) AS Agent10,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                 and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Dose'' and erc.codelst_subtyp=''dose10''
                  ) AS Dose10,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                 and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Dose Rationale'' and erc.codelst_subtyp=''doseRatnl10''
                  ) AS Dose_Rationale10,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                 and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Route of Administration'' and erc.codelst_subtyp=''routeAdmn10''
                  ) AS Rut_f_Admin10,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Treatment duration''
                  ) AS Treatment_duration,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_subtyp=''subComplMontr''
                  ) AS Sbjct_Cmpl_Mntr,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_subtyp=''radMat1''
                  ) AS Dost_std_invlv_tadmin,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_subtyp=''authrzdUsr''
                  ) AS Authorized_User,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_subtyp=''authrztnNum''
                  ) AS Ath_Nmbr,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_subtyp=''incldAdminstr''
                  ) AS to_cary_ot_a_clncl_std,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_subtyp=''radioCompAvail''
                  ) AS RADIOACT_COMPND,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_subtyp=''faciltyInvstg''
                  ) AS if_outside_MDACC,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_subtyp=''biosafety1''
                  ) AS RCMBNT_DNA_TECH,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_subtyp=''biosafety4''
                  ) AS BIOSFTY4,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_subtyp=''nihRacRev''
                  ) AS NIHRACREV,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_subtyp=''nihRacRevComm''
                  ) AS NIHRACREVCOMM,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_subtyp=''biosafety2''
                  ) AS INFCUSTO_HUMNS,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_subtyp=''biosafety2Comm''
                  ) AS BIOSAFETY2COMM,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_subtyp=''biosafety3''
                  ) AS TISSUE_TYP,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Are you using Tisseel or Evicel or similar?''
                  ) AS TISSUEL_EVICEL,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Device-1''
                  ) AS DEVICE1,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Device-2''
                  ) AS DEVICE2,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Device-3''
                  ) AS DEVICE3,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Device-4''
                  ) AS DEVICE4,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Device-5''
                  ) AS DEVICE5,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_subtyp=''devMnftr1''
                  ) AS DEVMNFTR1,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_subtyp=''devMnftr2''
                  ) AS DEVMNFTR2,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_subtyp=''devMnftr3''
                  ) AS DEVMNFTR3,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_subtyp=''devMnftr4''
                  ) AS DEVMNFTR4,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_subtyp=''devMnftr5''
                  ) AS DEVMNFTR5,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_subtyp=''riskAssmt1''
                  ) AS RISKASSMNTFRM1,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_subtyp=''riskAssmt2''
                  ) AS RISKASSMNTFRM2,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_subtyp=''riskAssmt3''
                  ) AS RISKASSMNTFRM3,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_subtyp=''riskAssmt4''
                  ) AS RISKASSMNTFRM4,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_subtyp=''riskAssmt5''
                  ) AS RISKASSMNTFRM5,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_subtyp=''OtrStdRemCrit''
                  ) AS OTHRSTDREMCRIT,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_subtyp=''freshBiopsyNeed''
                  ) AS FRESHBIOPSYNEEDED,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_subtyp=''freshBiopsyYes''
                  ) AS FRESHBIOPSYYES,
                  (SELECT ess.STUDYSEC_TEXT FROM er_studysec ess WHERE ess.fk_study IN (' || pk_study || ')
                  and ess.STUDYSEC_NAME=''Study Rationale''
                  ) AS STDRATIONALE,
                (SELECT ess.STUDYSEC_TEXT FROM er_studysec ess WHERE ess.fk_study IN (' || pk_study || ')
                  and ess.STUDYSEC_NAME=''Pre-clinical Data''
                  ) AS PRECLNCLDTA,
                (SELECT ess.STUDYSEC_TEXT FROM er_studysec ess WHERE ess.fk_study IN (' || pk_study || ')
                  and ess.STUDYSEC_NAME=''Clinical Data to Date''
                  ) AS CLNCLDTATODT,
                (SELECT ess.STUDYSEC_TEXT FROM er_studysec ess WHERE ess.fk_study IN (' || pk_study || ')
                  and ess.STUDYSEC_NAME=''Screening/Baseline''
                  ) AS SCREENG_BSLN,
                  (SELECT ess.STUDYSEC_TEXT FROM er_studysec ess WHERE ess.fk_study IN (' || pk_study || ')
                  and ess.STUDYSEC_NAME=''Study Rationale''
                  ) AS STD_RATNL,
				        (SELECT ess.STUDYSEC_TEXT FROM er_studysec ess WHERE ess.fk_study IN (' || pk_study || ')
                  and ess.STUDYSEC_NAME=''On-study/Treatment Evaluations''
                  ) AS ONSTDEVALUTNS,
				        (SELECT ess.STUDYSEC_TEXT FROM er_studysec ess WHERE ess.fk_study IN (' || pk_study || ')
                  and ess.STUDYSEC_NAME=''End of Therapy Evaluations''
                  ) AS ENDOFTHRPYEVALNS,
                  
                NVL((pkg_MDAREPORT.f_INDTYPE(cursor(SELECT esi.INDIDE_TYPE FROM ER_STUDY_INDIDE esi WHERE esi.fk_study IN (' || pk_study || ')
                  and esi.FK_STUDY != ''0'' ), '', '')),''N/A'') AS INDIDEINFOCHK,
                pkg_MDAREPORT.f_INDTYPE(cursor(SELECT esi.INDIDE_TYPE FROM ER_STUDY_INDIDE esi WHERE esi.fk_study IN (' || pk_study || ')
                  and esi.FK_STUDY != ''0'' ), '', '') AS INDIDE_TYPE,
                pkg_MDAREPORT.f_EXPANDED_EXEMPT(cursor(SELECT esi.INDIDE_EXPAND_ACCESS FROM ER_STUDY_INDIDE esi WHERE esi.fk_study IN (' || pk_study || ')
                  and esi.FK_STUDY != ''0'' ), '', '') AS EXPANDED,
                pkg_MDAREPORT.f_EXPANDED_EXEMPT(cursor(SELECT esi.INDIDE_EXEMPT FROM ER_STUDY_INDIDE esi WHERE esi.fk_study IN (' || pk_study || ')
                  and esi.FK_STUDY != ''0'' ), '', '') AS EXEMPT,
                pkg_util.f_join(cursor(SELECT esi.INDIDE_NUMBER FROM ER_STUDY_INDIDE esi WHERE esi.fk_study IN (' || pk_study || ')
                  and esi.FK_STUDY != ''0'' ), '', '') AS INDIDE_NUMBER1,
                  pkg_util.f_join(cursor(select codelst_desc from er_codelst ec, er_study_indide esi where esi.fk_study IN (' || pk_study || ')
                  and esi.fk_codelst_indide_grantor = ec.pk_codelst), '', '') AS INDIDEGRANTOR,
                  pkg_util.f_join(cursor(select codelst_desc from er_codelst ec, er_study_indide esi where esi.fk_study IN (' || pk_study || ')
                  and esi.fk_codelst_indide_holder = ec.pk_codelst), '', '') AS INDIDEHOLDERTYP,
                  pkg_util.f_join(cursor(select codelst_desc from er_codelst ec, er_study_indide esi where esi.fk_study IN (' || pk_study || ')
                  and esi.fk_codelst_program_code = ec.pk_codelst), '', '') AS NICPRGCODE,
                  pkg_util.f_join(cursor(select codelst_desc from er_codelst ec, er_study_indide esi where esi.fk_study IN (' || pk_study || ')
                  and esi.fk_codelst_access_type = ec.pk_codelst), '', '') AS CDLSTACCSTYP,
				        (SELECT ess.STUDYSEC_TEXT FROM er_studysec ess WHERE ess.fk_study IN (' || pk_study || ')
                  and ess.STUDYSEC_NAME=''Follow-up''
                  ) AS FOLLOWUP,
				        (SELECT ess.STUDYSEC_TEXT FROM er_studysec ess WHERE ess.fk_study IN (' || pk_study || ')
                  and ess.STUDYSEC_NAME=''Study Calendar/Schedule of Events''
                  ) AS STDCLNDRSCHOFEVNT,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Are you measuring Biomarkers?''
                  ) AS MEASURBIOMRKR,
                 (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Are they known Biomarkers?''
                  ) AS ARTHYKNWNBIOMRKR,
                 (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Biomarker type''
                  ) AS BIOMRKRTYP,
                 (SELECT codelst_desc FROM er_codelst WHERE pk_codelst IN (select FK_CODELST_RANDOM from er_study where er_study.pk_study in (' || pk_study || '))
                  )AS RANDOMIZATION,
                 (SELECT codelst_desc FROM er_codelst WHERE pk_codelst IN (select FK_CODELST_BLIND from er_study where er_study.pk_study in (' || pk_study || '))
                  ) AS BLINDING,
                 (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_subtyp=''biomrkrSpeOth''
                  ) AS SPECIFYOTHR,
                 (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''What are you calling the test? Use commonly accepted test.''
                  ) AS COMMACCTEST,
                 (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''If gene, use www.genecard.org to pull name from list''
                  ) AS WWWGENORG,
                 (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''What are you calling the biomarker?''
                  ) AS WHATRUCALBIOMRKR,
                 (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''What level are you trying to look at?''
                  ) AS WHATLVLTRNGTOLOKAT,
                 (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Methodology''
                  ) AS METHODOLOGY,
				        (SELECT ess.STUDYSEC_TEXT FROM er_studysec ess WHERE ess.fk_study IN (' || pk_study || ')
                  and ess.STUDYSEC_NAME=''Data Safety Monitoring Plan''
                  ) AS DTSFTYMONTRPLN,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Primary Endpoints''
                  ) AS PRIMARYENDPTS,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Secondary Endpoints''
                  ) AS SECENDPTS,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Exploratory Endpoints''
                  ) AS EXPLTRYENDPTS,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Does this protocol have a planned interim analysis?''
                  ) AS PLNDINTRMANLS,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Data Safety and Monitoring Board''
                  ) AS DTASFTMONTBRD,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Rationale for no interim analyses''
                  ) AS RATNOINTRANLS,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Statistical Analysis Plan''
                  ) AS STSTANLSPLN,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Bayesian component included in protocol''
                  ) AS BSYNCMPNTINCLPRTCL,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''Summary of planned interim analyses including stopping rules for futility, efficacy and/or toxicity''
                  ) AS SMRYPLNANLS,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_subtyp=''invAntcpAEs''
                  ) AS INVSTGTNLAES,
                (SELECT ers.studyid_id FROM er_studyid ers,er_codelst erc WHERE ers.fk_study IN (' || pk_study || ') AND ers.fk_codelst_idtype=erc.pk_codelst
                  and erc.codelst_type=''studyidtype'' and erc.codelst_desc=''What criteria are you using for classifying Adverse Events?''
                  ) AS CRTCLSADVEVNTS
                from er_study es
                where pk_study =' || pk_study ;
                retData:=f_getxml(initData);
                return retdata;
                End getMDAReport;
-------------------------------------------------------------------------------------------------------------------------------------------------------
				FUNCTION f_INDTYPE (p_cursor sys_refcursor, p_del VARCHAR2 := ',')
      RETURN VARCHAR2
   IS
      l_value    VARCHAR2 (32767);
      l_result   VARCHAR2 (32767);
   BEGIN
      LOOP
         FETCH p_cursor
          INTO l_value;

         EXIT WHEN p_cursor%NOTFOUND;

         IF l_result IS NOT NULL
         THEN
            l_result := l_result || p_del;
         END IF;
         if(l_value = '1') then
            l_result := l_result || 'IND';
         END IF;
         if(l_value = '2') then
            l_result := l_result || 'IDE';
          END IF;
          if(l_value = '3') then
            l_result := l_result || 'IND EXEMPT';
         END IF;
         if(l_value = '4') then
            l_result := l_result || 'IDE EXEMPT';
          END IF;
          if(l_value = '5') then
            l_result := l_result || 'IND/IDE';        
        END IF;
      END LOOP;

      CLOSE p_cursor;

      RETURN l_result;
   END f_INDTYPE; 
	-----------------------------------------------------------------------------------------------------------------------------------------------------------			
	 FUNCTION f_EXPANDED_EXEMPT (p_cursor sys_refcursor, p_del VARCHAR2 := ',')
      RETURN VARCHAR2
   IS
      l_value    VARCHAR2 (32767);
      l_result   VARCHAR2 (32767);
   BEGIN
      LOOP
         FETCH p_cursor
          INTO l_value;

         EXIT WHEN p_cursor%NOTFOUND;

         IF l_result IS NOT NULL
         THEN
            l_result := l_result || p_del;
         END IF;
         if(l_value = '1') then
            l_result := l_result || 'Checked';
         END IF;
         if(l_value = '0') then
            l_result := l_result || 'Unchecked';
          END IF;
      END LOOP;

      CLOSE p_cursor;

      RETURN l_result;
   END f_EXPANDED_EXEMPT;
---------------------------------------------------------------------------------------------------------------------------------------------------------   
	FUNCTION F_GetProtcol_Versn(arg1 NUMBER) RETURN VARCHAR2
      IS
      l_len1 NUMBER;
      l_len2 NUMBER;
      l_len3 NUMBER;
      l_len4 VARCHAR2(4000) := '';
      BEGIN
    select PAGE_VER, Max(PAGE_MINOR_VER) into l_len1, l_len2 from (select max(PAGE_VER) PAGE_VER, 
    PAGE_MINOR_VER from ui_flx_pagever where PAGE_MOD_PK=arg1 group by PAGE_MINOR_VER) group by PAGE_VER;

    -- l_len1 := PAGE_VER;
    -- l_len2 := PAGE_MINOR_VER;
     l_len3 := length(l_len2);
     if(l_len3 >1)then 
       l_len4 := l_len1 ||'.'||l_len2;
        else 
         l_len4 := l_len1 ||'.'|| '0'||l_len2;
        end if;
     if(l_len2 = 0) then
      l_len4 := l_len1 ||'.'||'0';
    end if;
   return l_len4;
     END F_GetProtcol_Versn;			
END PKG_MDAREPORT;
/
commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,327,930,'01_PkgFunction_SQL.sql',sysdate,'v9.3.0 #727-le27');

commit;
