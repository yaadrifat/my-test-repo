set define off;

update er_studyid set studyid_id ='panobinostat' where studyid_id = 'panobinostat (LBH-589)' and fk_codelst_idtype in (select pk_codelst from er_codelst 
where codelst_type ='studyidtype' and codelst_subtyp in ('drugAgent1', 'drugAgent2', 'drugAgent3', 'drugAgent4', 'drugAgent5',
'drugAgent6','drugAgent7','drugAgent8','drugAgent9','drugAgent10'));

commit;

declare
v_exists number :=0;
begin
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='abiraterone';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'abiraterone','abiraterone',1);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='abraxane';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'abraxane','abraxane',2);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='ABT-199';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'ABT-199','ABT-199',3);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='ABT-888';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'ABT-888','ABT-888 (veliparib)',4);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='AC220';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'AC220','AC220',5);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='AG-013736';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'AG-013736','AG-013736',6);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='AMG-337';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'AMG-337','AMG-337',7);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='AP-24534';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'AP-24534','AP-24534',8);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='ARRY-614';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'ARRY-614','ARRY-614',9);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='AZD-1208';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'AZD-1208','AZD-1208',10);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='birinipant';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'birinipant','birinipant',11);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='BKM-120';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'BKM-120','BKM-120',12);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='BMS-833923';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'BMS-833923','BMS-833923',13);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='bortezomib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'bortezomib','bortezomib',14);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='Bosutinib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'Bosutinib','Bosutinib',15);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='CC-223';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'CC-223','CC-223',16);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='cediranib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'cediranib','cediranib (AZD-2171)',17);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='CMX-001';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'CMX-001','CMX-001',18);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='CPX-351';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'CPX-351','CPX-351',19);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='crenolanib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'crenolanib','Crenolanib',20);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='CWP-23229';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'CWP-23229','CWP-232291',21);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='dasatinib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'dasatinib','dasatinib',22);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='denileukin';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'denileukin','denileukin diftitox',23);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='E-75acetate';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'E-75acetate','E-75 acetate',24);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='eflornithine';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'eflornithine','eflornithine',25);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='entinostat';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'entinostat','Entinostat',26);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='erlotinib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'erlotinib','erlotinib',27);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='everolimus';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'everolimus','everolimus (RAD-001)',28);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='GDC-0941';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'GDC-0941','GDC-0941',29);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='GS-1101';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'GS-1101','GS-1101',30);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='GSK-1120212';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'GSK-1120212','GSK-1120212',31);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='dabrafenib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'dabrafenib','GSK-2118436 (dabrafenib)',32);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='GSK-525762';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'GSK-525762','GSK-525762',33);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='ibrutinib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'ibrutinib','ibrutinib',34);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='ICL-670';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'ICL-670','ICL-670',35);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='INCB-018424';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'INCB-018424','INCB-018424',36);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='KB-004';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'KB-004','KB-004',37);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='lacosamide';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'lacosamide','lacosamide',38);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='Lapatinib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'Lapatinib','Lapatinib',39);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='LDE-225';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'LDE-225','LDE-225',40);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='LEE-011';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'LEE-011','LEE-011',41);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='lenalidomide';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'lenalidomide','lenalidomide(CC-5013)',42);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='LY-2784544';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'LY-2784544','LY-2784544',43);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='LY-2940680';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'LY-2940680','LY-2940680',44);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='LY-3009120';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'LY-3009120','LY-3009120',45);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='masatinib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'masatinib','masatinib',46);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='MEK-162';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'MEK-162','MEK-162',47);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='MK-1775';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'MK-1775','MK-1775',48);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='MK-2206';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'MK-2206','MK-2206',49);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='MRX-34';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'MRX-34','MRX-34',50);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='olaparib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'olaparib','olaparib',51);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='Omacetaxine';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'Omacetaxine','Omacetaxine',52);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='panobinostat';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'panobinostat','panobinostat (LBH-589)',53);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='pazopanib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'pazopanib','pazopanib',54);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='PF-0449913';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'PF-0449913','PF-0449913',55);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='PKC-412';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'PKC-412','PKC-412',56);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='pomalidomide';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'pomalidomide','pomalidomide',57);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='Ponatinib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'Ponatinib','Ponatinib',58);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='Ponatinib-AP';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'Ponatinib-AP','Ponatinib(AP-24534)',59);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='posaconazole';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'posaconazole','posaconazole',60);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='pracinostat';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'pracinostat','pracinostat',61);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='PRI0724';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'PRI0724','PRI0724',62);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='Rigosertib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'Rigosertib','Rigosertib',63);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='rucaparib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'rucaparib','rucaparib',64);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='Ruxolitinib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'Ruxolitinib','Ruxolitinib',65);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='S-1';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'S-1','S-1',66);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='SAR-302503';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'SAR-302503','SAR-302503',67);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='topiramate';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'topiramate','topiramate',68);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='volasertib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'volasertib','volasertib',69);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='vorinostat';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'vorinostat','vorinostat',70);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='VST-1001';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'VST-1001','VST-1001',71);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='XL-184';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'XL-184','XL-184',72);
	END IF;
	commit;
end;
/


update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDrugAgent"}' where codelst_type='studyidtype' and codelst_subtyp='drugAgent1';
update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDrugAgent"}' where codelst_type='studyidtype' and codelst_subtyp='drugAgent2';
update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDrugAgent"}' where codelst_type='studyidtype' and codelst_subtyp='drugAgent3';
update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDrugAgent"}' where codelst_type='studyidtype' and codelst_subtyp='drugAgent4';
update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDrugAgent"}' where codelst_type='studyidtype' and codelst_subtyp='drugAgent5';
update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDrugAgent"}' where codelst_type='studyidtype' and codelst_subtyp='drugAgent6';
update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDrugAgent"}' where codelst_type='studyidtype' and codelst_subtyp='drugAgent7';
update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDrugAgent"}' where codelst_type='studyidtype' and codelst_subtyp='drugAgent8';
update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDrugAgent"}' where codelst_type='studyidtype' and codelst_subtyp='drugAgent9';
update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDrugAgent"}' where codelst_type='studyidtype' and codelst_subtyp='drugAgent10';
commit;