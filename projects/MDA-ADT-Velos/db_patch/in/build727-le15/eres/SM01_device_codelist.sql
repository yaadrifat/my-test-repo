set define off;

declare
v_exists number := 0;
begin
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='adapthence'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'adapthence','Adapter henceforth',1);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='affyGene'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'affyGene','Affymetrix Gene Chip U133A',2);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='bipapVision'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'bipapVision','BIPAP Vision Ventilatory Support System',3);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='biocomp'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'biocomp','Biocompatibles LC Bead (100-300) loaded with Doxorubicin HCl',4);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='bioSeal'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'bioSeal','Bio-Seal Track Plug',5);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='block'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'block','B-lock solution',6);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='caphosol'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'caphosol','Caphosol',7);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='cavSpine'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'cavSpine','Cavity SpineWand',8);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='cliniMACS'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'cliniMACS','CliniMACS device',9);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='cryocare'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'cryocare','Cryocare',10);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='daVinci'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'daVinci','da Vinci Robotic Surgical System',11);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='exAblate'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'exAblate','ExAblate device',12);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='galilMed'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'galilMed','Galil Medical Cryoprobes',13);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='goldFid'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'goldFid','Gold Fiducial Marker',14);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='imageGuide'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'imageGuide','Image Guide Microscope - a high-resolution microendoscope (HRME)',15);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='imagio'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'imagio','Imagio Breast Imaging System',16);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='indocyanine'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'indocyanine','Indocyanine green fluorescence lymphography system',17);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='infrared'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'infrared','Infrared camera',18);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='inSpectra'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'inSpectra','InSpectra Tissue Oxygenation Monitor Model 650 and Spot Check Monitor Model 300',19);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='integrated'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'integrated','Integrated BRACAnalysis',20);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='intensity'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'intensity','Intensity Modulated Radiotherapy',21);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='iodine125'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'iodine125','Iodine-125 radioactive seeds',22);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='louis3D'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'louis3D','LOUIS-3D (Laser OPtoacoustic Ultrasonic Imaging System',23);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='lowCoh'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'lowCoh','Low Coherence Enhanced Backscattering (LEBS)',24);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='mdaApp'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'mdaApp','MDA Applicator',25);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='mirena'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'mirena','Mirena (Levonorgestrel Contraceptive IUD)',26);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='pemFlex'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'pemFlex','PEMFlex Solo II High Resolution PET Scanner',27);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='pointSpectro'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'pointSpectro','Point spectroscopy probe(POS) and PS2',28);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='prostateImm'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'prostateImm','Prostate Immobilization Treatment Device',29);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='senoRxCon'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'senoRxCon','SenoRx Contura MLB applicator',30);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='SERI'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'SERI','SERI Surgical Scaffold',31);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='SIR'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'SIR','SIR-spheres',32);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='sonablate'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'sonablate','Sonablate 500 (Sonablate)',33);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='spectra'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'spectra','Spectra Optia Apheresis System',34);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='vapotherm'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'vapotherm','The Vapotherm 2000i Respiratory Therapy Device',35);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='strattice'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'strattice','Strattice Tissue Matrix',36);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='syntheCel'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'syntheCel','SyntheCelTM Dura Replacement',37);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='ISLLC'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'ISLLC','The ISLLC Implantable Drug Delivery System (IDDS)',38);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='visica'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'visica','The Visica 2 Treatment System',39);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='theraSphere'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'theraSphere','TheraSphere',40);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='transducer'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'transducer','Transducer-tipped catheter Mikro-Tip SPC-320, transducer Millar, and Power Lab 8/30 amplifier',41);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='truFreeze'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'truFreeze','truFreezeTM System',42);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='UB500'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'UB500','UB500',43);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='viOptix'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'viOptix','ViOptix continuous transcutaneous tissue oximetry device',44);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='vendys'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'vendys','VENDYS-Model 6000 B/C',45);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='veridex'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'veridex','Veridex CellSearch assay',46);
	end if;
	commit;
end;
/

update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDeviceAgent"}' where codelst_type='studyidtype' and codelst_subtyp='deviceAgent1';
update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDeviceAgent"}' where codelst_type='studyidtype' and codelst_subtyp='deviceAgent2';
update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDeviceAgent"}' where codelst_type='studyidtype' and codelst_subtyp='deviceAgent3';
update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDeviceAgent"}' where codelst_type='studyidtype' and codelst_subtyp='deviceAgent4';
update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDeviceAgent"}' where codelst_type='studyidtype' and codelst_subtyp='deviceAgent5';
commit;

