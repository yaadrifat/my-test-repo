set define off;

declare
v_exists number :=0;
begin
	select count(*) into v_exists from er_codelst where codelst_type='LindTemplateTyp' and codelst_subtyp ='coopGrp';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq, CODELST_HIDE)
		values (seq_er_codelst.nextval,'LindTemplateTyp' ,'coopGrp','Cooperative Group',1,'Y');
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindTemplateTyp' and codelst_subtyp ='consrtm';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq, CODELST_HIDE )
		values (seq_er_codelst.nextval,'LindTemplateTyp' ,'consrtm','Consortium',2, 'Y');
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindTemplateTyp' and codelst_subtyp ='nciEtctn';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq, CODELST_HIDE )
		values (seq_er_codelst.nextval,'LindTemplateTyp' ,'nciEtctn','NCI ETCTN',3, 'Y');
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindTemplateTyp' and codelst_subtyp ='indstrStdy';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindTemplateTyp' ,'indstrStdy','Industry Study',4);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindTemplateTyp' and codelst_subtyp ='MDACCInvIni';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindTemplateTyp' ,'MDACCInvIni','MDACC Investigator Initiated',5);
	END IF;
	commit;
end;
/


update er_codelst set codelst_custom_col1 = '{codelst_type:"LindTemplateTyp"}' where codelst_type='studyidtype' and codelst_subtyp='spnsrdProt';

commit;
