set define off;

declare
v_exists number :=0;
begin
	select count(*) into v_exists from er_codelst where codelst_type='LindProDes2' and codelst_subtyp ='YesComp';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq)
		values (seq_er_codelst.nextval,'LindProDes2' ,'YesComp','Yes -- Completely',1);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindProDes2' and codelst_subtyp ='YesPart';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindProDes2' ,'YesPart','Yes -- Partially',2);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindProDes2' and codelst_subtyp ='NoSponsrDes';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindProDes2' ,'NoSponsrDes','No -- Sponsor provided biostatistical design',3);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindProDes2' and codelst_subtyp ='NoProsStat';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindProDes2' ,'NoProsStat','No prospective statistical design in this study',4);
	END IF;
	commit;
end;
/


update er_codelst set codelst_custom_col1 = '{codelst_type:"LindProDes2"}' where codelst_type='studyidtype' and codelst_subtyp='proDes2';

commit;
