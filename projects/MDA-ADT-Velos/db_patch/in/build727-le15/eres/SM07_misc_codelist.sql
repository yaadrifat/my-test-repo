set define off;

update er_codelst set codelst_custom_col1 = '<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>' where codelst_type ='studyidtype' and codelst_subtyp = 'bayesianComp';

commit;

update er_codelst set codelst_custom_col1 = '<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>' where codelst_type ='studyidtype' and codelst_subtyp = 'spnsrshp1';

commit;
