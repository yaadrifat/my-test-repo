set define off;

declare
v_exists number :=0;
begin
	select count(*) into v_exists from er_codelst where codelst_type='LindDSMB' and codelst_subtyp ='mdaDsmb'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDSMB' ,'mdaDsmb','MD ANDERSON DSMB',1);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDSMB' and codelst_subtyp ='mdaExtdsmb'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDSMB' ,'mdaExtdsmb','MD ANDERSON  External DSMB ',2);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDSMB' and codelst_subtyp ='indpDsmb'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDSMB' ,'indpDsmb','Independent DSMB - This is a DSMB comprised of members who are not affiliated with the research or sponsor in any way.',3);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDSMB' and codelst_subtyp ='intDsmb'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDSMB' ,'intDsmb','Internal DSMB � This is a DSMB comprised of study investigators and/or sponsor employees/contractors.',4);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDSMB' and codelst_subtyp ='notAppl'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDSMB' ,'notAppl','Not Applicable',5);
	END IF;
	commit;
end;
/


update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDSMB"}' where codelst_type='studyidtype' and codelst_subtyp='dsmbInt';

commit;
