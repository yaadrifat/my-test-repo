set define off;

declare
v_exists number :=0;
begin
		select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Aural'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Aural','Aural',1);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Epidural'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Epidural','Epidural',2);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Inhalation'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Inhalation','Inhalation',3);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Intraarterial'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Intraarterial','Intraarterial',4);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Intradermal'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Intradermal','Intradermal',5);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Intralesional'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Intralesional','Intralesional',6);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Intramucosal'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Intramucosal','Intramucosal',7);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Intramuscular'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Intramuscular','Intramuscular',8);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Intranasal'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Intranasal','Intranasal',9);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Intraocular'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Intraocular','Intraocular',10);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Intraosseous'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Intraosseous','Intraosseous',11);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Intraperitoneal'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Intraperitoneal','Intraperitoneal',12);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Intrapleural'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Intrapleural','Intrapleural',13);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Intrathecal'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Intrathecal','Intrathecal',14);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Intratumoral'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Intratumoral','Intratumoral',15);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Intravenous'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Intravenous','Intravenous',16);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Intravesicular'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Intravesicular','Intravesicular',17);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Ocular'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Ocular','Ocular',18);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Oral'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Oral','Oral',19);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Rectal'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Rectal','Rectal',20);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Subcutaneous'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Subcutaneous','Subcutaneous',21);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Sublingual'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Sublingual','Sublingual',22);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Topical'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Topical','Topical',23);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Transdermal'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Transdermal','Transdermal',24);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Vaginal'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Vaginal','Vaginal',25);
	end if;

	commit;
end;
/


update er_codelst set codelst_custom_col1 = '{codelst_type:"LindRouteAdmin"}' where codelst_type='studyidtype' and codelst_subtyp like 'route%';

commit;
