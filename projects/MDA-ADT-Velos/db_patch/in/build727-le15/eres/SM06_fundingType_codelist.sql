set define off;

declare
v_exists number :=0;
begin
	select count(*) into v_exists from er_codelst where codelst_type='LindFundingType' and codelst_subtyp ='NCI'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values(seq_er_codelst.nextval,'LindFundingType' ,'NCI','NCI',1);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindFundingType' and codelst_subtyp ='nih'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values(seq_er_codelst.nextval,'LindFundingType' ,'nih','NIH (other than NCI)',2);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindFundingType' and codelst_subtyp ='dod'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values(seq_er_codelst.nextval,'LindFundingType' ,'dod','DOD',3);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindFundingType' and codelst_subtyp ='peerRevFund'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values(seq_er_codelst.nextval,'LindFundingType' ,'peerRevFund','Other peer reviewed funding (e.g. NSF, ACS, etc.)',4);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindFundingType' and codelst_subtyp ='privInd'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values(seq_er_codelst.nextval,'LindFundingType' ,'privInd','Private Industry',5);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindFundingType' and codelst_subtyp ='privFound'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values(seq_er_codelst.nextval,'LindFundingType' ,'privFound','Private Foundation',6);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindFundingType' and codelst_subtyp ='deptFunds'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values(seq_er_codelst.nextval,'LindFundingType' ,'deptFunds','Departmental Funds',7);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindFundingType' and codelst_subtyp ='donorFunds'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values(seq_er_codelst.nextval,'LindFundingType' ,'donorFunds','Donor Funds',8);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindFundingType' and codelst_subtyp ='unfunded'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values(seq_er_codelst.nextval,'LindFundingType' ,'unfunded','Unfunded',9);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindFundingType' and codelst_subtyp ='coopGrp'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values(seq_er_codelst.nextval,'LindFundingType' ,'coopGrp','Cooperative group',10);
	END IF;

	commit;
end;
/


update er_codelst set codelst_custom_col1 = '{codelst_type:"LindFundingType"}' where codelst_type='studyidtype' and codelst_subtyp like 'fundingType%';

commit;
