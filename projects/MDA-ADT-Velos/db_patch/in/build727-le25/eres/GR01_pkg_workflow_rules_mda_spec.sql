create or replace PACKAGE "ERES"."PKG_WORKFLOW_RULES_MDA"
IS
  FUNCTION F_studyInit_protDef(studyId NUMBER) RETURN NUMBER;
  
  FUNCTION F_studyInit_OMCR_Review(studyId NUMBER) RETURN NUMBER;

  FUNCTION F_studyInit_IND_Review(studyId NUMBER) RETURN NUMBER;
  
  FUNCTION F_studyInit_biostatReview(studyId NUMBER) RETURN NUMBER;
  
  FUNCTION F_studyInit_deptSignoff(studyId NUMBER) RETURN NUMBER;
  
  FUNCTION F_studyInit_crcReview(studyId NUMBER) RETURN NUMBER;
  
  FUNCTION F_studyInit_irbReview(studyId NUMBER) RETURN NUMBER;
  
  FUNCTION F_studyInit_InstReqmts(studyId NUMBER) RETURN NUMBER;
  
  FUNCTION F_studyInit_DataHndlng(studyId NUMBER) RETURN NUMBER;
  
  FUNCTION F_studyInit_collabSignoff(studyId NUMBER) RETURN NUMBER;
  
  FUNCTION F_studyInit_NomRevSignoff(studyId NUMBER) RETURN NUMBER;
  
  FUNCTION F_studyInit_FinSupport(studyId NUMBER) RETURN NUMBER;
  
  FUNCTION F_studyInit_RiskAssmt(studyId NUMBER) RETURN NUMBER;
  
  FUNCTION F_studyInit_IntervRadiology(studyId NUMBER) RETURN NUMBER;
  
  FUNCTION F_studyInit_MultiConsider(studyId NUMBER) RETURN NUMBER;
  --------------------------------------------------------------------

  FUNCTION F_studyActive_protActivation(studyId NUMBER) RETURN NUMBER;
  
  FUNCTION F_studyActive_PiActRqst(studyId NUMBER) RETURN NUMBER;
  --------------------------------------------------------------------
  
  FUNCTION F_studyConduct_protAmendment(studyId NUMBER) RETURN NUMBER;
  
  FUNCTION F_studyConduct_irbRenewal(studyId NUMBER) RETURN NUMBER;
  --------------------------------------------------------------------
  pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'PKG_WORKFLOW', pLEVEL  => Plog.LFATAL);
END PKG_WORKFLOW_RULES_MDA;
/
