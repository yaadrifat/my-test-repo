create or replace PACKAGE BODY        "PKG_WORKFLOW_RULES_MDA"
AS
  FUNCTION F_studyInit_protDef(studyId NUMBER) RETURN NUMBER
  AS
  v_RETURN_NO NUMBER := 0;
  BEGIN
    SELECT COUNT(*) INTO v_RETURN_NO from ER_STUDY where PK_STUDY= studyId;
    RETURN v_RETURN_NO;
  END;
  ----------------------------------------------------------------------------------------------------------------------
  -- Activity:Multicenter Review ; Marked Done:Upon saving �Multicenter SIgnoff� form for the study under consideration
  --------------------------------------- ------------------------------------------------------------------------------
  FUNCTION F_studyInit_OMCR_Review(studyId NUMBER) RETURN NUMBER
  AS
  v_statusExists NUMBER := 0;
  v_isMultiSite NUMBER := 0;
  v_isPiMajAuthor NUMBER := 0;
  v_RETURN_NO NUMBER := 0;
  BEGIN
    SELECT count(*) INTO v_statusExists FROM ER_STUDYFORMS WHERE FK_STUDY = studyId
    AND FK_FORMLIB = (
    SELECT PK_FORMLIB from ER_FORMLIB , ER_LINKEDFORMS where PK_FORMLIB = ER_LINKEDFORMS.FK_FORMLIB
    AND ER_FORMLIB.RECORD_TYPE <> 'D' and ER_LINKEDFORMS.LF_DISPLAYTYPE = 'SA' and FORM_NAME = 'Multicenter Signoff')
    AND FORM_COMPLETED IN (
    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'fillformstat' AND CODELST_SUBTYP = 'complete');

    IF v_statusExists > 0 THEN
      v_RETURN_NO := 1; --Done
    ELSE
      BEGIN
        SELECT COUNT(*) INTO v_isMultiSite from ER_STUDY where PK_STUDY= studyId AND FK_CODELST_SCOPE IN (
        SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'studyscope_3');

        SELECT COUNT(*) INTO v_isPiMajAuthor from ER_STUDYID where FK_STUDY= studyId AND STUDYID_ID = 'MDACCInvIni';

        IF (v_isMultiSite > 0 AND v_isPiMajAuthor > 0) THEN
          v_RETURN_NO := 0; --Not Done
        ELSE
          v_RETURN_NO := -1; --Not Required
        END IF;
      END;
    END IF;

    RETURN v_RETURN_NO;

  EXCEPTION WHEN NO_DATA_FOUND THEN
    BEGIN
      RETURN 0; --Not Done
    END;
  END;

  ----------------------------------------------------------------------------------------------------------------------
  -- Activity:IND Review ; Marked Done:Upon saving �IND Signoff� form for the study under consideration
  -----------------------------------------------------------------------------------------------------------------------
  FUNCTION F_studyInit_IND_Review(studyId NUMBER) RETURN NUMBER
  AS
  v_statusExists NUMBER := 0;
  v_isHolderMDACC NUMBER := 0;
  v_RETURN_NO NUMBER := 0;
  BEGIN

    SELECT count(*) INTO v_statusExists FROM ER_STUDYFORMS WHERE FK_STUDY = studyId
    AND FK_FORMLIB = (
    SELECT PK_FORMLIB from ER_FORMLIB , ER_LINKEDFORMS where PK_FORMLIB = ER_LINKEDFORMS.FK_FORMLIB
    AND ER_FORMLIB.RECORD_TYPE <> 'D' and ER_LINKEDFORMS.LF_DISPLAYTYPE = 'SA' and FORM_NAME = 'IND Signoff')
    AND FORM_COMPLETED IN (
    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'fillformstat' AND CODELST_SUBTYP = 'complete');

    IF v_statusExists > 0 THEN
      v_RETURN_NO := 1; --Done
    ELSE
      BEGIN
        SELECT COUNT(*) INTO v_isHolderMDACC from ER_STUDY_INDIDE where FK_STUDY= studyId
        AND NVL(INDIDE_TYPE, 0) = 1 AND FK_CODELST_INDIDE_HOLDER = (
        SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'INDIDEHolder' AND CODELST_SUBTYP = 'organization');

        IF v_isHolderMDACC > 0 THEN
          v_RETURN_NO := 0; --Not Done
        ELSE
          v_RETURN_NO := -1; --Not Required
        END IF;
      END;
    END IF;

    RETURN v_RETURN_NO;

  EXCEPTION WHEN NO_DATA_FOUND THEN
    BEGIN
      RETURN 0; --Not Done
    END;
  END;

  ----------------------------------------------------------------------------------------------------------------------
  -- Activity:Biostat Review ; Marked Done:Upon saving �Biostat Signoff� form for the study under consideration
  -----------------------------------------------------------------------------------------------------------------------

  FUNCTION F_studyInit_biostatReview(studyId NUMBER) RETURN NUMBER
  AS
  v_statusExists NUMBER := 0;
  v_isPiMajAuthor NUMBER := 0;
  v_RETURN_NO NUMBER := 0;
  BEGIN
    SELECT count(*) INTO v_statusExists FROM ER_STUDYFORMS WHERE FK_STUDY = studyId
    AND FK_FORMLIB = (
    SELECT PK_FORMLIB from ER_FORMLIB , ER_LINKEDFORMS where PK_FORMLIB = ER_LINKEDFORMS.FK_FORMLIB
    AND ER_FORMLIB.RECORD_TYPE <> 'D' and ER_LINKEDFORMS.LF_DISPLAYTYPE = 'SA' and FORM_NAME = 'Biostat Signoff')
    AND FORM_COMPLETED IN (
    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'fillformstat' AND CODELST_SUBTYP = 'complete');

    IF v_statusExists > 0 THEN
      v_RETURN_NO := 1; --Done
    ELSE
      BEGIN
        SELECT COUNT(*) INTO v_isPiMajAuthor from ER_STUDYID where FK_STUDY= studyId AND STUDYID_ID = 'MDACCInvIni';

        IF v_isPiMajAuthor > 0 THEN
          v_RETURN_NO := 0; --Not Done
        ELSE
          v_RETURN_NO := -1; --Not Required
        END IF;
      END;
    END IF;

    RETURN v_RETURN_NO;

  EXCEPTION WHEN NO_DATA_FOUND THEN
    BEGIN
      RETURN 0; --Not Done
    END;
  END;

  -----------------------------------------------------------------------------------------------------------------------------------
  -- Activity:Departmental Signoff ; Marked Done:Upon saving �Department/Divisional Approval� form for the study under consideration
  -----------------------------------------------------------------------------------------------------------------------------------

  FUNCTION F_studyInit_deptSignoff(studyId NUMBER) RETURN NUMBER
  AS
  v_statusExists NUMBER := 0;
  v_RETURN_NO NUMBER := 0;
  BEGIN

    SELECT count(*) INTO v_statusExists FROM ER_STUDYFORMS WHERE FK_STUDY = studyId
    AND FK_FORMLIB = (
    SELECT PK_FORMLIB from ER_FORMLIB , ER_LINKEDFORMS where PK_FORMLIB = ER_LINKEDFORMS.FK_FORMLIB
    AND ER_FORMLIB.RECORD_TYPE <> 'D' and ER_LINKEDFORMS.LF_DISPLAYTYPE = 'SA' and FORM_NAME = 'Department / Divisional Approval')
    AND FORM_COMPLETED IN (
    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'fillformstat' AND CODELST_SUBTYP = 'complete');

    IF v_statusExists > 0 THEN
      v_RETURN_NO := 1; --Done
    ELSE
      v_RETURN_NO := 0; --Not Done
    END IF;

    RETURN v_RETURN_NO;

  EXCEPTION WHEN NO_DATA_FOUND THEN
    BEGIN
      v_RETURN_NO := 0; --Not Done
      RETURN 0;
    END;
  END;
  ------------------------------------------------------------------------------

  FUNCTION F_studyInit_crcReview(studyId NUMBER) RETURN NUMBER
  AS
  v_statusExists NUMBER := 0;
  v_isPiMajAuthor NUMBER := 0;

  v_RETURN_NO NUMBER := 0;
  BEGIN
    SELECT count(*) INTO v_statusExists FROM ER_STUDYSTAT WHERE FK_STUDY = studyId AND FK_CODELST_STUDYSTAT = (
    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcApproved');

    IF (v_statusExists > 0) THEN
      v_RETURN_NO := 1; --Done
    ELSE
      v_RETURN_NO := 0; --Not Done
    END IF;

    RETURN v_RETURN_NO;

  EXCEPTION WHEN NO_DATA_FOUND THEN
    BEGIN
      v_RETURN_NO := 0; --Not Done
      RETURN 0;
    END;
  END;
  ------------------------------------------------------------------------------

  FUNCTION F_studyInit_irbReview(studyId NUMBER) RETURN NUMBER
  AS
  v_statusExists NUMBER := 0;
  v_isPiMajAuthor NUMBER := 0;
  v_RETURN_NO NUMBER := 0;
  v_form_id  NUMBER := 0;

  BEGIN
    SELECT count(*) INTO v_statusExists FROM ER_STUDYSTAT WHERE FK_STUDY = studyId AND FK_CODELST_STUDYSTAT = (
    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'app_CHR');

    IF (v_statusExists > 0) THEN
      v_RETURN_NO := 1; --Done
    ELSE
      v_RETURN_NO := 0; --Not Done
    END IF;

    RETURN v_RETURN_NO;

  EXCEPTION WHEN NO_DATA_FOUND THEN
    BEGIN
      v_RETURN_NO := 0; --Not Done
      RETURN 0;
    END;
  END;

  -----------------------------------------------------------------------------------------------------------------------------------
  -- Activity:Institutional Requirements ; Marked Done:Upon saving �Institutional Requirements� form for the study under consideration
  -----------------------------------------------------------------------------------------------------------------------------------

  FUNCTION F_studyInit_InstReqmts(studyId NUMBER) RETURN NUMBER
  AS
  v_statusExists NUMBER := 0;
  v_RETURN_NO NUMBER := 0;
  BEGIN

    SELECT count(*) INTO v_statusExists FROM ER_STUDYFORMS WHERE FK_STUDY = studyId
    AND FK_FORMLIB = (
    SELECT PK_FORMLIB from ER_FORMLIB , ER_LINKEDFORMS where PK_FORMLIB = ER_LINKEDFORMS.FK_FORMLIB
    AND ER_FORMLIB.RECORD_TYPE <> 'D' and ER_LINKEDFORMS.LF_DISPLAYTYPE = 'SA' and FORM_NAME = 'Institutional Requirements Form')
    AND FORM_COMPLETED IN (
    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'fillformstat' AND CODELST_SUBTYP = 'complete');

    IF v_statusExists > 0 THEN
      v_RETURN_NO := 1; --Done
    ELSE
      v_RETURN_NO := 0; --Not Done
    END IF;

    RETURN v_RETURN_NO;

  EXCEPTION WHEN NO_DATA_FOUND THEN
    BEGIN
      v_RETURN_NO := 0; --Not Done
      RETURN 0;
    END;
  END;


  -----------------------------------------------------------------------------------------------------------------------------------
  -- Activity:Data Handling/ Record Keeping ; Marked Done:Upon saving �Data Handling/ Record Keeping� form for the study under consideration
  -----------------------------------------------------------------------------------------------------------------------------------

  FUNCTION F_studyInit_DataHndlng(studyId NUMBER) RETURN NUMBER
  AS
  v_statusExists NUMBER := 0;
  v_RETURN_NO NUMBER := 0;
  BEGIN

    SELECT count(*) INTO v_statusExists FROM ER_STUDYFORMS WHERE FK_STUDY = studyId
    AND FK_FORMLIB = (
    SELECT PK_FORMLIB from ER_FORMLIB , ER_LINKEDFORMS where PK_FORMLIB = ER_LINKEDFORMS.FK_FORMLIB
    AND ER_FORMLIB.RECORD_TYPE <> 'D' and ER_LINKEDFORMS.LF_DISPLAYTYPE = 'SA'and FORM_NAME = 'Data Handling and Record Keeping')
    AND FORM_COMPLETED IN (
    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'fillformstat' AND CODELST_SUBTYP = 'complete');

    IF v_statusExists > 0 THEN
      v_RETURN_NO := 1; --Done
    ELSE
      v_RETURN_NO := 0; --Not Done
    END IF;

    RETURN v_RETURN_NO;

  EXCEPTION WHEN NO_DATA_FOUND THEN
    BEGIN
      v_RETURN_NO := 0; --Not Done
      RETURN 0;
    END;
  END;

  ------------------------------------------------------------------------------------------------------------------------------------------
  -- Activity:Collaborator Signoff ; Marked Done:Upon saving �Collaborator Signoff� form for the study under consideration
  ------------------------------------------------------------------------------------------------------------------------------------------

  FUNCTION F_studyInit_collabSignoff(studyId NUMBER) RETURN NUMBER
  AS
  v_statusExists NUMBER := 0;
  v_RETURN_NO NUMBER := 0;
  v_isCollaborator NUMBER := 0;

 BEGIN

    SELECT count(*) INTO v_statusExists FROM ER_STUDYFORMS WHERE FK_STUDY = studyId
    AND FK_FORMLIB = (
    SELECT PK_FORMLIB from ER_FORMLIB , ER_LINKEDFORMS where PK_FORMLIB = ER_LINKEDFORMS.FK_FORMLIB
    AND ER_FORMLIB.RECORD_TYPE <> 'D' and ER_LINKEDFORMS.LF_DISPLAYTYPE = 'SA' and FORM_NAME = 'Collaborator Signoff')
    AND FORM_COMPLETED IN (
    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'fillformstat' AND CODELST_SUBTYP = 'complete');

    IF v_statusExists > 0 THEN
      v_RETURN_NO := 1; --Done
    ELSE
      BEGIN
        SELECT COUNT(*) INTO v_isCollaborator from er_studyteam t, er_codelst where t.fk_study = studyId
        and t.fk_codelst_tmrole = pk_codelst and codelst_type = 'role' and codelst_subtyp = 'Collaborators';

        IF v_isCollaborator > 0 THEN
          v_RETURN_NO := 0; --Not Done
        ELSE
          v_RETURN_NO := -1; --Not Required
        END IF;
      END;
    END IF;

    RETURN v_RETURN_NO;

  EXCEPTION WHEN NO_DATA_FOUND THEN
    BEGIN
      v_RETURN_NO := 0; --Not Done
      RETURN 0;
    END;
  END;

  ------------------------------------------------------------------------------------------------------------------------------------------
  -- Activity:Nominated Reviewer Signoff ; Marked Done:Upon saving �Nominated Reviewer Signoff� form for the study under consideration
  ------------------------------------------------------------------------------------------------------------------------------------------

  FUNCTION F_studyInit_NomRevSignoff(studyId NUMBER) RETURN NUMBER
  AS
  v_statusExists NUMBER := 0;
  v_RETURN_NO NUMBER := 0;
  BEGIN

    SELECT count(*) INTO v_statusExists FROM ER_STUDYFORMS WHERE FK_STUDY = studyId
    AND FK_FORMLIB = (
    SELECT PK_FORMLIB from ER_FORMLIB , ER_LINKEDFORMS where PK_FORMLIB = ER_LINKEDFORMS.FK_FORMLIB
    AND ER_FORMLIB.RECORD_TYPE <> 'D' and ER_LINKEDFORMS.LF_DISPLAYTYPE = 'SA' and FORM_NAME = 'Nominated Reviewer Signoff' )
    AND FORM_COMPLETED IN (
    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'fillformstat' AND CODELST_SUBTYP = 'complete');

    IF v_statusExists > 0 THEN
      v_RETURN_NO := 1; --Done
    ELSE
      v_RETURN_NO := 0; --Not Done
    END IF;

    RETURN v_RETURN_NO;

  EXCEPTION WHEN NO_DATA_FOUND THEN
    BEGIN
      v_RETURN_NO := 0; --Not Done
      RETURN 0;
    END;
  END;

   ------------------------------------------------------------------------------------------------------------------------------------------
  -- Activity:Financial Support ; Marked Done:Upon saving �Financial Support Form� form for the study under consideration
  ------------------------------------------------------------------------------------------------------------------------------------------

  FUNCTION F_studyInit_FinSupport(studyId NUMBER) RETURN NUMBER
  AS
  v_statusExists NUMBER := 0;
  v_RETURN_NO NUMBER := 0;
  BEGIN

    SELECT count(*) INTO v_statusExists FROM ER_STUDYFORMS WHERE FK_STUDY = studyId
    AND FK_FORMLIB = (
    SELECT PK_FORMLIB from ER_FORMLIB , ER_LINKEDFORMS where PK_FORMLIB = ER_LINKEDFORMS.FK_FORMLIB
    AND ER_FORMLIB.RECORD_TYPE <> 'D' and ER_LINKEDFORMS.LF_DISPLAYTYPE = 'SA' and FORM_NAME = 'Financial Support Form' )
    AND FORM_COMPLETED IN (
    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'fillformstat' AND CODELST_SUBTYP = 'complete');

    IF v_statusExists > 0 THEN
      v_RETURN_NO := 1; --Done
    ELSE
      v_RETURN_NO := 0; --Not Done
    END IF;

    RETURN v_RETURN_NO;

  EXCEPTION WHEN NO_DATA_FOUND THEN
    BEGIN
      v_RETURN_NO := 0; --Not Done
      RETURN 0;
    END;
  END;

  ---------------------------------------------------------------------------------------------------------------------------------------------------
  -- Activity:Risk Assessment ; Marked Done:Upon saving �Risk Assessment� form for the study under consideration.The actvity 'Risk Assessment' ------
  --should be visible only  when 'device' is one of the options selected in the checkbox group called 'Study includes' in the section 'Study Ids' ----
  ----------------------------------------------------------------------------------------------------------------------------------------------------

  FUNCTION F_studyInit_RiskAssmt(studyId NUMBER) RETURN NUMBER
  AS
  v_statusExists NUMBER := 0;
  v_RETURN_NO NUMBER := 0;
  v_isDeviceSelected NUMBER := 0;

 BEGIN

    SELECT count(*) INTO v_statusExists FROM ER_STUDYFORMS WHERE FK_STUDY = studyId
    AND FK_FORMLIB = (
    SELECT PK_FORMLIB from ER_FORMLIB , ER_LINKEDFORMS where PK_FORMLIB = ER_LINKEDFORMS.FK_FORMLIB
    AND ER_FORMLIB.RECORD_TYPE <> 'D' and ER_LINKEDFORMS.LF_DISPLAYTYPE = 'SA' and FORM_NAME = 'Risk Assessment Form')
    AND FORM_COMPLETED IN (
    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'fillformstat' AND CODELST_SUBTYP = 'complete');

    IF v_statusExists > 0 THEN
      v_RETURN_NO := 1; --Done
    ELSE
      BEGIN
        select COUNT(*) INTO v_isDeviceSelected from er_studyid where  fk_study = studyId  and pk_studyid in (Select pk_studyid from er_studyid, er_codelst
        where fk_study = studyId and  pk_codelst = FK_CODELST_IDTYPE and codelst_subtyp = 'stdyIncludes') and studyId_id like '%option2%';

        IF v_isDeviceSelected > 0 THEN
          v_RETURN_NO := 0; --Not Done
        ELSE
          v_RETURN_NO := -1; --Not Required
        END IF;
      END;
    END IF;

    RETURN v_RETURN_NO;

  EXCEPTION WHEN NO_DATA_FOUND THEN
    BEGIN
      v_RETURN_NO := 0; --Not Done
      RETURN 0;
    END;
  END;

  ----------------------------------------------------------------------------------------------------------------------
  -- Activity:Interventional Radiology ; Marked Done:Upon saving �Interventional Radiology� form for the study under consideration
  --------------------------------------- ------------------------------------------------------------------------------

  FUNCTION F_studyInit_IntervRadiology(studyId NUMBER) RETURN NUMBER
  AS
  v_statusExists NUMBER := 0;
  v_freshBiopsyYes NUMBER := 0;
  v_RETURN_NO NUMBER := 0;
  BEGIN
    SELECT count(*) INTO v_statusExists FROM ER_STUDYFORMS WHERE FK_STUDY = studyId
    AND FK_FORMLIB = (
    SELECT PK_FORMLIB from ER_FORMLIB , ER_LINKEDFORMS where PK_FORMLIB = ER_LINKEDFORMS.FK_FORMLIB
    AND ER_FORMLIB.RECORD_TYPE <> 'D' and ER_LINKEDFORMS.LF_DISPLAYTYPE = 'SA' and FORM_NAME = 'Interventional Radiology')
    AND FORM_COMPLETED IN (
    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'fillformstat' AND CODELST_SUBTYP = 'complete');

    IF v_statusExists > 0 THEN
      v_RETURN_NO := 1; --Done
    ELSE
      BEGIN
        select COUNT(*) INTO v_freshBiopsyYes from er_studyid where  fk_study = studyId  and pk_studyid in (Select pk_studyid from er_studyid, er_codelst
        where fk_study = studyId and  pk_codelst = FK_CODELST_IDTYPE and codelst_subtyp = 'freshBiopsyNeed')
		and studyId_id like '%Yes%';

        IF v_freshBiopsyYes > 0 THEN
          v_RETURN_NO := 0; --Not Done
        ELSE
          v_RETURN_NO := -1; --Not Required
        END IF;
      END;
    END IF;

    RETURN v_RETURN_NO;

  EXCEPTION WHEN NO_DATA_FOUND THEN
    BEGIN
      RETURN 0; --Not Done
    END;
  END;

  ----------------------------------------------------------------------------------------------------------------------
  -- Activity:Multicenter Considerations ; Marked Done:Upon saving �Multicenter Considerations� form for the study under consideration
  --------------------------------------- ------------------------------------------------------------------------------

  FUNCTION F_studyInit_MultiConsider(studyId NUMBER) RETURN NUMBER
  AS
  v_statusExists NUMBER := 0;
  v_isMultiSiteLead NUMBER := 0;
  v_isPiMajAuthor NUMBER := 0;
  v_RETURN_NO NUMBER := 0;

  BEGIN
    SELECT count(*) INTO v_statusExists FROM ER_STUDYFORMS WHERE FK_STUDY = studyId
    AND FK_FORMLIB = (
    SELECT PK_FORMLIB from ER_FORMLIB , ER_LINKEDFORMS where PK_FORMLIB = ER_LINKEDFORMS.FK_FORMLIB
    AND ER_FORMLIB.RECORD_TYPE <> 'D' and ER_LINKEDFORMS.LF_DISPLAYTYPE = 'SA' and FORM_NAME = 'Multicenter Considerations')
    AND FORM_COMPLETED IN (
    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'fillformstat' AND CODELST_SUBTYP = 'complete');

    IF v_statusExists > 0 THEN
      v_RETURN_NO := 1; --Done
    ELSE
      BEGIN
        SELECT COUNT(*) INTO v_isMultiSiteLead from ER_STUDY where PK_STUDY= studyId AND FK_CODELST_SCOPE IN (
        SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'studyscope_3');

        SELECT COUNT(*) INTO v_isPiMajAuthor from ER_STUDYID where FK_STUDY= studyId AND STUDYID_ID = 'MDACCInvIni';

        IF (v_isMultiSiteLead > 0 AND v_isPiMajAuthor > 0) THEN
          v_RETURN_NO := 0; --Not Done
        ELSE
          v_RETURN_NO := -1; --Not Required
        END IF;
      END;
    END IF;

    RETURN v_RETURN_NO;

  EXCEPTION WHEN NO_DATA_FOUND THEN
    BEGIN
      RETURN 0; --Not Done
    END;
  END;

  ------------------------------------------------------------------------------
  ------------------------------------------------------------------------------

  FUNCTION F_studyActive_protActivation(studyId NUMBER) RETURN NUMBER
  AS
  v_statusExists NUMBER := 0;
  v_isIrbApproved NUMBER := 0;
  v_RETURN_NO NUMBER := 0;

  BEGIN
    SELECT count(*) INTO v_statusExists FROM ER_STUDYFORMS WHERE FK_STUDY = studyId
    AND FK_FORMLIB = (
    SELECT PK_FORMLIB from ER_FORMLIB , ER_LINKEDFORMS where PK_FORMLIB = ER_LINKEDFORMS.FK_FORMLIB
    AND ER_FORMLIB.RECORD_TYPE <> 'D' and ER_LINKEDFORMS.LF_DISPLAYTYPE = 'SA' and FORM_NAME = 'Protocol Activation')
    AND FORM_COMPLETED IN (
    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'fillformstat' AND CODELST_SUBTYP = 'complete');

    IF v_statusExists > 0 THEN
      v_RETURN_NO := 1; --Done
    ELSE
      BEGIN
        SELECT count(*) INTO v_isIrbApproved FROM ER_STUDYSTAT WHERE FK_STUDY = studyId AND FK_CODELST_STUDYSTAT = (
        SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'app_CHR');

        IF v_isIrbApproved > 0 THEN
          v_RETURN_NO := 0; --Not Done
        ELSE
          v_RETURN_NO := -1; --Not Required
        END IF;
      END;
    END IF;

    RETURN v_RETURN_NO;

  EXCEPTION WHEN NO_DATA_FOUND THEN
    BEGIN
      v_RETURN_NO := 0; --Not Done
      RETURN 0;
    END;
  END;

  ------------------------------------------------------------------------------
  ------------------------------------------------------------------------------

  FUNCTION F_studyActive_PiActRqst(studyId NUMBER) RETURN NUMBER
  AS
  v_statusExists NUMBER := 0;
  v_isIrbApproved NUMBER := 0;

  v_RETURN_NO NUMBER := 0;
  BEGIN
    SELECT count(*) INTO v_statusExists FROM ER_STUDYFORMS WHERE FK_STUDY = studyId
    AND FK_FORMLIB = (
    SELECT PK_FORMLIB from ER_FORMLIB , ER_LINKEDFORMS where PK_FORMLIB = ER_LINKEDFORMS.FK_FORMLIB
    AND ER_FORMLIB.RECORD_TYPE <> 'D' and ER_LINKEDFORMS.LF_DISPLAYTYPE = 'SA' and FORM_NAME = 'PI Activation Request')
    AND FORM_COMPLETED IN (
    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'fillformstat' AND CODELST_SUBTYP = 'complete');

    IF v_statusExists > 0 THEN
      v_RETURN_NO := 1; --Done
    ELSE
      BEGIN
        SELECT count(*) INTO v_isIrbApproved FROM ER_STUDYSTAT WHERE FK_STUDY = studyId AND FK_CODELST_STUDYSTAT = (
        SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'app_CHR');

        IF v_isIrbApproved > 0 THEN
          v_RETURN_NO := 0; --Not Done
        ELSE
          v_RETURN_NO := -1; --Not Required
        END IF;
      END;
    END IF;

    RETURN v_RETURN_NO;

  EXCEPTION WHEN NO_DATA_FOUND THEN
    BEGIN
      v_RETURN_NO := 0; --Not Done
      RETURN 0;
    END;
  END;

  ------------------------------------------------------------------------------
  ------------------------------------------------------------------------------

  FUNCTION F_studyConduct_protAmendment(studyId NUMBER) RETURN NUMBER
  AS
  v_statusExists NUMBER := 0;
  v_isPiMajAuthor NUMBER := 0;

  v_RETURN_NO NUMBER := 0;
  BEGIN
    SELECT count(*) INTO v_statusExists FROM ER_STUDYSTAT WHERE FK_STUDY = studyId AND FK_CODELST_STUDYSTAT = (
    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'amendApproved');

    IF (v_statusExists > 0) THEN
      v_RETURN_NO := 1; --Done
    ELSE
      v_RETURN_NO := 0; --Not Done
    END IF;

    RETURN v_RETURN_NO;

  EXCEPTION WHEN NO_DATA_FOUND THEN
    BEGIN
      v_RETURN_NO := 0; --Not Done
      RETURN 0;
    END;
  END;
  ------------------------------------------------------------------------------

  FUNCTION F_studyConduct_irbRenewal(studyId NUMBER) RETURN NUMBER
  AS
  v_statusExists NUMBER := 0;
  v_isPiMajAuthor NUMBER := 0;

  v_RETURN_NO NUMBER := 0;
  BEGIN
    SELECT count(*) INTO v_statusExists FROM ER_STUDYSTAT WHERE FK_STUDY = studyId AND FK_CODELST_STUDYSTAT = (
    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'irbRenewal');

    IF (v_statusExists > 0) THEN
      v_RETURN_NO := 1; --Done
    ELSE
      v_RETURN_NO := 0; --Not Done
    END IF;

    RETURN v_RETURN_NO;

  EXCEPTION WHEN NO_DATA_FOUND THEN
    BEGIN
      v_RETURN_NO := 0; --Not Done
      RETURN 0;
    END;
  END;
  ------------------------------------------------------------------------------
END PKG_WORKFLOW_RULES_MDA;