set define off;

update er_codelst set codelst_desc='Data Safety and Monitoring Plan' where codelst_type = 'studyidtype' and codelst_subtyp='dataSftyMntrPln'; 

commit;

declare
v_id varchar(1000);
begin
	select 'alternateId'|| PK_CODELST into v_id from er_codelst where codelst_type = 'studyidtype' and codelst_subtyp='nom_rev_id';
	
	update er_codelst set 
	codelst_custom_col1='{lookupPK:6000, selection:"multi", mapping:[{source:"USRFULLNAME", target:"alternateId"},{source:"USRLOGIN", target:"'|| v_id ||'"}]}' 
	where codelst_type = 'studyidtype' and codelst_subtyp='nom_rev'; 

commit;
end;
/

update er_codelst set codelst_custom_col='hidden-input' where codelst_type = 'studyidtype' and codelst_subtyp='nom_alt_rev'; 

commit;


