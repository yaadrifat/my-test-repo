set define off;

update ER_CODELST set CODELST_TYPE='studyvercat1' where
CODELST_TYPE='studyvercat' and CODELST_SUBTYP='studyvercat_5';

update ER_CODELST set CODELST_TYPE='studyvercat1' where
CODELST_TYPE='studyvercat' and CODELST_SUBTYP='studyvercat_6';

update ER_CODELST set CODELST_DESC='VP of Clinical Research Administration Determination' where
CODELST_TYPE='rev_board' and CODELST_SUBTYP='vp_cr';

commit;
