SET define off;
---adding the dropdown options to the route of admn dropdown. There are 10 of these dropdowns on the screen.
--2----
UPDATE ER_CODELST
SET CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Aural">Aural</OPTION>
<OPTION value = "Epidural">Epidural</OPTION>
<OPTION value = "Inhalation">Inhalation</OPTION>
<OPTION value = "Intraarterial">Intraarterial</OPTION>
<OPTION value = "Intradermal">Intradermal</OPTION>
<OPTION value = "Intralesional">Intralesional</OPTION>
<OPTION value = "Intramucosal">Intramucosal</OPTION>
<OPTION value = "Intramuscular">Intramuscular</OPTION>
<OPTION value = "Intranasal">Intranasal</OPTION>
<OPTION value = "Intraocular">Intraocular</OPTION>
<OPTION value = "Intraosseous">Intraosseous</OPTION>
<OPTION value = "Intraperitoneal">Intraperitoneal</OPTION>
<OPTION value = "Intrapleural">Intrapleural</OPTION>
<OPTION value = "Intrathecal">Intrathecal</OPTION>
<OPTION value = "Intratumoral">Intratumoral</OPTION>
<OPTION value = "Intravenous">Intravenous</OPTION>
<OPTION value = "Intravesicular">Intravesicular</OPTION>
<OPTION value = "Ocular">Ocular</OPTION>
<OPTION value = "Oral">Oral</OPTION>
<OPTION value = "Rectal">Rectal</OPTION>
<OPTION value = "Subcutaneous">Subcutaneous</OPTION>
<OPTION value = "Sublingual">Sublingual</OPTION>
<OPTION value = "Topical">Topical</OPTION>
<OPTION value = "Transdermal">Transdermal</OPTION>
<OPTION value = "Vaginal">Vaginal</OPTION>
</SELECT>'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP IN(
'routeAdmn2',
'routeAdmn3',
'routeAdmn4',
'routeAdmn5',
'routeAdmn6',
'routeAdmn7',
'routeAdmn8',
'routeAdmn9',
'routeAdmn10');

COMMIT;



