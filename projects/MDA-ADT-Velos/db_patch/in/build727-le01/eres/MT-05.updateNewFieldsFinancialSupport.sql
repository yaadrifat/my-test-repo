SET define off;
---adding the dropdown options to the funding type dropdown. There are 5 of these dropdowns on the screen.
UPDATE ER_CODELST
SET CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "deptFunds">Departmental Funds</OPTION>
<OPTION value = "dod">DOD</OPTION>
<OPTION value = "donorFunds">Donor Funds</OPTION>
<OPTION value = "industry">Industry</OPTION>
<OPTION value = "NCI">NCI</OPTION>
<OPTION value = "nih">NIH (other than NCI)</OPTION>
<OPTION value = "peerRevFund">Other peer reviewed funding (e.g. NSF, ACS, etc.)</OPTION>
<OPTION value = "unfunded">Unfunded</OPTION>
</SELECT>'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP IN(
'fundingType1',
'fundingType2',
'fundingType3',
'fundingType4',
'fundingType5');

--setting the lookup field
update er_codelst  set codelst_custom_col1 = '{lookupPK:100040, selection:"single", mapping:[{source:"Sponsor_Name", target:"alternateId"}]}' 
where codelst_subtyp in ('sponsorName1', 'sponsorName2', 'sponsorName3', 'sponsorName4', 'sponsorName5'); 

	
--changing the name of the field that says Grant Serial#/Contract# to COEUS#
update er_codelst set codelst_desc = ' COEUS#' where  codelst_subtyp = 'grant' ;

COMMIT;




