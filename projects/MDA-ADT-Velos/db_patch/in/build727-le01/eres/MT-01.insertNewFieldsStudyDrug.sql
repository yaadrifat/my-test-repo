set define off;
set serveroutput on;


-->agent -dropdown
-->dose -free text
-->Dose rationale --free text
-->Route of administration--dropdown
-->manufacturer--lookup


declare
v_CodeExists number := 0;
begin
	
	---1---
	-- inserting the second set of drug/device repeating fields--(part of the first set already exists , so will not be adding those again:
	--the fields that already existed are codelst_subtyp = 'dose' , codelst_subtyp = 'doseRationale' , codelst_subtyp = 'route'
	--so we are just going to have these fields as is and then the sequence starts from 2, 3 ..and so on.
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugAgent1';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugAgent1', 'Agent1', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugMnftr1';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugMnftr1', 'Manufacturer', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	---2------
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugAgent2';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugAgent2', 'Agent2', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dose2';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'dose2', 'Dose', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'doseRatnl2';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'doseRatnl2', 'Dose Rationale', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'routeAdmn2';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'routeAdmn2', 'Route of Administration', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugMnftr2';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugMnftr2', 'Manufacturer', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

--- 3-----

SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugAgent3';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugAgent3', 'Agent3', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dose3';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'dose3', 'Dose', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'doseRatnl3';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'doseRatnl3', 'Dose Rationale', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'routeAdmn3';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'routeAdmn3', 'Route of Administration', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugMnftr3';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugMnftr3', 'Manufacturer', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

--4--
SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugAgent4';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugAgent4', 'Agent4', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dose4';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'dose4', 'Dose', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'doseRatnl4';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'doseRatnl4', 'Dose Rationale', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'routeAdmn4';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'routeAdmn4', 'Route of Administration', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugMnftr4';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugMnftr4', 'Manufacturer', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

--5--
SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugAgent5';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugAgent5', 'Agent5', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dose5';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'dose5', 'Dose', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'doseRatnl5';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'doseRatnl5', 'Dose Rationale', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'routeAdmn5';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'routeAdmn5', 'Route of Administration', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugMnftr5';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugMnftr5', 'Manufacturer', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;


--6--

SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugAgent6';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugAgent6', 'Agent6', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dose6';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'dose6', 'Dose', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'doseRatnl6';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'doseRatnl6', 'Dose Rationale', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'routeAdmn6';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'routeAdmn6', 'Route of Administration', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugMnftr6';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugMnftr6', 'Manufacturer', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;


--7--

SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugAgent7';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugAgent7', 'Agent7', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dose7';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'dose7', 'Dose', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'doseRatnl7';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'doseRatnl7', 'Dose Rationale', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'routeAdmn7';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'routeAdmn7', 'Route of Administration', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugMnftr7';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugMnftr7', 'Manufacturer', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

--8--

SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugAgent8';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugAgent8', 'Agent8', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dose8';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'dose8', 'Dose', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'doseRatnl8';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'doseRatnl8', 'Dose Rationale', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'routeAdmn8';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'routeAdmn8', 'Route of Administration', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugMnftr8';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugMnftr8', 'Manufacturer', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
		
	END IF;

--9--
SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugAgent9';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugAgent9', 'Agent9', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dose9';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'dose9', 'Dose', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'doseRatnl9';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'doseRatnl9', 'Dose Rationale', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'routeAdmn9';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'routeAdmn9', 'Route of Administration', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugMnftr9';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugMnftr9', 'Manufacturer', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;



--10--
SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugAgent10';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugAgent10', 'Agent10', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dose10';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'dose10', 'Dose', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'doseRatnl10';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'doseRatnl10', 'Dose Rationale', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'routeAdmn10';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'routeAdmn10', 'Route of Administration', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugMnftr10';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugMnftr10', 'Manufacturer', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;


COMMIT;
END;
/

