SET define off;
---adding the dropdown options to the device agent dropdown. There are 5 of these dropdowns on the screen.
UPDATE ER_CODELST
SET CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "adapthence">Adapter henceforth</OPTION>
<OPTION value = "affyGene">Affymetrix Gene Chip U133A</OPTION>
<OPTION value = "bipapVision">BIPAP Vision Ventilatory Support System</OPTION>
<OPTION value = "biocomp">Biocompatibles LC Bead (100-300) loaded with Doxorubicin HCl</OPTION>
<OPTION value = "bioSeal">Bio-Seal Track Plug</OPTION>
<OPTION value = "block">B-lock solution</OPTION>
<OPTION value = "caphosol">Caphosol</OPTION>
<OPTION value = "cavSpine">Cavity SpineWand</OPTION>
<OPTION value = "cliniMACS">CliniMACS device</OPTION>
<OPTION value = "cryocare">Cryocare</OPTION>
<OPTION value = "daVinci">da Vinci Robotic Surgical System</OPTION>
<OPTION value = "exAblate">ExAblate device</OPTION>
<OPTION value = "galilMed">Galil Medical Cryoprobes</OPTION>
<OPTION value = "goldFid">Gold Fiducial Marker</OPTION>
<OPTION value = "imageGuide">Image Guide Microscope - a high-resolution microendoscope (HRME)</OPTION>
<OPTION value = "imagio">Imagio Breast Imaging System</OPTION>
<OPTION value = "indocyanine">Indocyanine green fluorescence lymphography system</OPTION>
<OPTION value = "infrared">Infrared camera</OPTION>
<OPTION value = "inSpectra">InSpectra Tissue Oxygenation Monitor Model 650 and Spot Check Monitor Model 300</OPTION>
<OPTION value = "integrated">Integrated BRACAnalysis</OPTION>
<OPTION value = "intensity">Intensity Modulated Radiotherapy</OPTION>
<OPTION value = "iodine125">Iodine-125 radioactive seeds</OPTION>
<OPTION value = "louis3D">LOUIS-3D (Laser OPtoacoustic Ultrasonic Imaging System</OPTION>
<OPTION value = "lowCoh">Low Coherence Enhanced Backscattering (LEBS)</OPTION>
<OPTION value = "mdaApp">MDA Applicator</OPTION>
<OPTION value = "mirena">Mirena (Levonorgestrel Contraceptive IUD)</OPTION>
<OPTION value = "pemFlex">PEMFlex Solo II High Resolution PET Scanner</OPTION>
<OPTION value = "pointSpectro">Point spectroscopy probe(POS) and PS2</OPTION>
<OPTION value = "prostateImm">Prostate Immobilization Treatment Device</OPTION>
<OPTION value = "senoRxCon">SenoRx Contura MLB applicator</OPTION>
<OPTION value = "SERI">SERI Surgical Scaffold</OPTION>
<OPTION value = "SIR">SIR-spheres</OPTION>
<OPTION value = "sonablate">Sonablate 500 (Sonablate)</OPTION>
<OPTION value = "spectra">Spectra Optia Apheresis System</OPTION>
<OPTION value = "vapotherm">The Vapotherm 2000i Respiratory Therapy Device</OPTION>
<OPTION value = "strattice">Strattice Tissue Matrix</OPTION>
<OPTION value = "syntheCel">SyntheCelTM Dura Replacement</OPTION>
<OPTION value = "ISLLC">The ISLLC Implantable Drug Delivery System (IDDS)</OPTION>
<OPTION value = "visica">The Visica 2 Treatment System</OPTION>
<OPTION value = "theraSphere">TheraSphere</OPTION>
<OPTION value = "transducer">Transducer-tipped catheter Mikro-Tip SPC-320, transducer Millar, and Power Lab 8/30 amplifier</OPTION>
<OPTION value = "truFreeze">truFreezeTM System</OPTION>
<OPTION value = "UB500">UB500</OPTION>
<OPTION value = "viOptix">ViOptix continuous transcutaneous tissue oximetry device</OPTION>
<OPTION value = "vendys">VENDYS-Model 6000 B/C</OPTION>
<OPTION value = "veridex">Veridex CellSearch assay</OPTION>
<OPTION value = "visica">Visica 2 Treatment System</OPTION>
</SELECT>'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP IN('deviceAgent1' , 'deviceAgent2', 'deviceAgent3', 'deviceAgent4', 'deviceAgent5');

--setting up the look up field
update er_codelst  set codelst_custom_col1 = '{lookupPK:100040, selection:"single", mapping:[{source:"Sponsor_Name", target:"alternateId"}]}' where 
codelst_subtyp in ('devMnftr1', 'devMnftr2', 'devMnftr3', 'devMnftr4', 'devMnftr5');

COMMIT;



