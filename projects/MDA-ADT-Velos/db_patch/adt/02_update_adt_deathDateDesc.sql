DECLARE
  l_deathDateDescExists INTEGER;
  l_deathDateExists INTEGER;
  l_fk                  INTEGER;
BEGIN
  SELECT COUNT(*)
  INTO l_deathDateDescExists
  FROM ER_BROWSERCONF
  WHERE browserconf_colname = 'DEATH_DATE_DESC'
  AND fk_browser            =
    (SELECT PK_BROWSER FROM er_browser WHERE BROWSER_MODULE = 'allPatientEMR' and rownum=1
    );
  IF l_deathDateDescExists = 0 THEN
    SELECT PK_BROWSER
    INTO l_fk
    FROM ER_BROWSER
    WHERE BROWSER_MODULE = 'allPatientEMR' and rownum=1;
    SELECT COUNT(*)
    INTO l_deathDateExists
    FROM ER_BROWSERCONF
    WHERE browserconf_colname = 'DEATH_DATE'
    AND fk_browser            =
      (SELECT PK_BROWSER FROM er_browser WHERE BROWSER_MODULE = 'allPatientEMR' and rownum=1
      );
    IF l_deathDateExists = 0 THEN
      INSERT
      INTO ER_BROWSERCONF
        (
          PK_BROWSERCONF,
          FK_BROWSER,
          BROWSERCONF_COLNAME,
          BROWSERCONF_SEQ,
          BROWSERCONF_SETTINGS,
          BROWSERCONF_EXPLABEL
        )
        VALUES
        (
          SEQ_ER_BROWSERCONF.nextval,
          l_fk,
          'DEATH_DATE_DESC',
          NULL,
          NULL,
          NULL
        );
    ELSE
      UPDATE ER_BROWSERCONF
      SET BROWSERCONF_COLNAME   = 'DEATH_DATE_DESC'
      WHERE BROWSERCONF_COLNAME = 'DEATH_DATE'
      AND fk_browser            = l_fk;
    END IF;
    COMMIT;
  END IF;
END;
/

UPDATE ER_BROWSERCONF
SET browserconf_settings  = '{"key":"FIRSTNAME", "label":"First Name","text":"Person First Name", "sortable":true, "resizeable":true, "hideable":true, "format":"firstName"}'
WHERE browserconf_colname = 'FIRSTNAME'
AND fk_browser            =
  (SELECT pk_browser FROM er_browser WHERE browser_module = 'allPatientEMR' and rownum=1
  )
/
  
UPDATE ER_BROWSERCONF
SET browserconf_settings  = '{"key":"LASTNAME", "label":"Last Name","text":"Person Last Name", "sortable":true, "resizeable":true, "hideable":true, "format":"lastName"}'
WHERE browserconf_colname = 'LASTNAME'
AND fk_browser            =
  (SELECT pk_browser FROM er_browser WHERE browser_module = 'allPatientEMR' and rownum=1
  )
/

COMMIT;
/