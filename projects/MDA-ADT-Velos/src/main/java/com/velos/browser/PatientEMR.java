package com.velos.browser;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.StringTokenizer;

import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.PatientDao;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.service.util.PatientEMRDetailBean;

public class PatientEMR {
	private String mainSQL;
	private String countSQL;
	private HashMap paramMap;
	private String startDate;
	private String endDate;

	/**
	 * @return the mainSQL
	 */
	public String getMainSQL() {
		return mainSQL;
	}

	/**
	 * @param mainSQL
	 *            the mainSQL to set
	 */
	public void setMainSQL(String mainSQL) {
		this.mainSQL = StringUtil.htmlUnicodePoint(mainSQL);
	}

	/**
	 * @return the countSQL
	 */
	public String getCountSQL() {
		return countSQL;
	}

	/**
	 * @param countSQL
	 *            the countSQL to set
	 */
	public void setCountSQL(String countSQL) {
		this.countSQL = countSQL;
	}

	/**
	 * @return the paramMap
	 */
	public HashMap getParamMap() {
		return paramMap;
	}

	/**
	 * @param paramMap
	 *            the paramMap to set
	 */
	public void setParamMap(HashMap paramMap) {
		this.paramMap = paramMap;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public void getAllPatientsEMRSQL() {

		Calendar cal = new GregorianCalendar();
		CodeDao codeDao = new CodeDao();
		String codeSubType = "";
		PatientDao pdao = new PatientDao();

		int minPHIRight = 0;

		String patName = (String) paramMap.get("patName");
		patName = patName.replace('`' , '\'');
		String pstat = (String) paramMap.get("pstat");

		String patCode = (String) paramMap.get("patCode");
		String gender = (String) paramMap.get("gender");

		String agePeriod = (String) paramMap.get("age");

		StringBuilder whereClause = new StringBuilder();

		String minDate = "";
		String maxDate = "";
		String ageFilter = "";
		Format formatter = new SimpleDateFormat("yyyy/MM/dd");
		Date date1 = new java.util.Date();
		Date date2 = new java.util.Date();

		if (!EJBUtil.isEmpty(agePeriod)) {
			if (agePeriod.equals("0,15")) {
				ageFilter = "AGE_td:[0+TO+15]" + "+AND+";
			} else if (agePeriod.equals("16,30")) {
				ageFilter = "AGE_td:[16+TO+30]" + "+AND+";
			} else if (agePeriod.equals("31,45")) {
				ageFilter = "AGE_td:[31+TO+45]" + "+AND+";
			} else if (agePeriod.equals("46,60")) {
				ageFilter = "AGE_td:[46+TO+60]" + "+AND+";
			} else if (agePeriod.equals("61,75")) {
				ageFilter = "AGE_td:[61+TO+75]" + "+AND+";
			} else if (agePeriod.equals("76,Over")) {
				ageFilter = "AGE_td:[76+TO+*]" + "+AND+";
			}
			whereClause.append(ageFilter);
		}

		String nameFilter = "";
		String delim = "";
		StringBuilder patNameStr = new StringBuilder();
		if (!EJBUtil.isEmpty(patName)) {
			StringTokenizer st = new StringTokenizer(patName, " ");
			while (st.hasMoreElements()) {
				patNameStr.append(delim);
				patNameStr.append(st.nextElement());
				delim = "*";
			}
			nameFilter = "FULLNAME:*" + patNameStr + "*" + "+AND+";
			whereClause.append(nameFilter);
		}

		String survivalStatFilter = "";
		if (!EJBUtil.isEmpty(pstat)) {
			codeSubType = codeDao.getCodeSubtype(Integer.parseInt(pstat));
			survivalStatFilter = "SURVIVAL_STATUS:" + codeSubType + "+AND+";
			whereClause.append(survivalStatFilter);
		}

		String genderFilter = "";
		if (!EJBUtil.isEmpty(gender)) {
			codeSubType = codeDao.getCodeSubtype(Integer.parseInt(gender));
			genderFilter = "GENDER:" + codeSubType + "+AND+";
			whereClause.append(genderFilter);
		}

		String patientCodeFilter = "";
		if (!EJBUtil.isEmpty(patCode)) {
			patientCodeFilter = "PERSON_CODE:*" + patCode + "*" + "+AND+";
			whereClause.append(patientCodeFilter);
		}

		String whereCondition = "ALREADY_FETCHED:0";
		// Trim the last AND
		if (!EJBUtil.isEmpty(whereClause.toString())) {
			whereCondition = whereClause.substring(0,
					whereClause.lastIndexOf("+AND+")) + "+AND+ALREADY_FETCHED:0";
		}
		
		String query = "q=" + whereCondition ;
		this.setMainSQL(query);
	}

	public void getPatientEmrDtls(PatientEMRDetailBean patientEMRDetailBean) {
		CodeDao codeDao = new CodeDao();
		String codeType = "";
		int codeId = 0;
		String delim = "";

		if (patientEMRDetailBean.getDOB() != null)
			patientEMRDetailBean.setDOB(patientEMRDetailBean.getDOB());
		
		if (patientEMRDetailBean.getDEATH_DATE() != null)
			patientEMRDetailBean.setDEATH_DATE(patientEMRDetailBean.getDEATH_DATE());

		if (!EJBUtil.isEmpty(patientEMRDetailBean.getGENDER())) {
			codeId = codeDao.getCodeId("gender",
					patientEMRDetailBean.getGENDER());
			patientEMRDetailBean.setGENDER_CODE(codeId);
			patientEMRDetailBean.setGENDER_DESC(codeDao.getCodeDescription());
		}

		if (!EJBUtil.isEmpty(patientEMRDetailBean.getMARITAL_STATUS())) {
			codeId = codeDao.getCodeId("marital_st",
					patientEMRDetailBean.getMARITAL_STATUS());
			patientEMRDetailBean.setMARITAL_STATUS_CODE(codeId);
			patientEMRDetailBean.setMARITAL_STATUS_DESC(codeDao
					.getCodeDescription());
		}

		if (!EJBUtil.isEmpty(patientEMRDetailBean.getSURVIVAL_STATUS())) {
			codeId = codeDao.getCodeId("patient_status",
					patientEMRDetailBean.getSURVIVAL_STATUS());
			patientEMRDetailBean.setSURVIVAL_STATUS_CODE(codeId);
			patientEMRDetailBean.setSURVIVAL_STATUS_DESC(codeDao
					.getCodeDescription());
		}

		if (!EJBUtil.isEmpty(patientEMRDetailBean.getRACE())) {
			codeId = codeDao.getCodeId("race", patientEMRDetailBean.getRACE());
			patientEMRDetailBean.setRACE_CODE(codeId);
			patientEMRDetailBean.setRACE_DESC(codeDao.getCodeDescription());
		}

		if (!EJBUtil.isEmpty(patientEMRDetailBean.getETHNICITY())) {
			codeId = codeDao.getCodeId("ethnicity",
					patientEMRDetailBean.getETHNICITY());
			patientEMRDetailBean.setETHNICITY_CODE(codeId);
			patientEMRDetailBean
					.setETHNICITY_DESC(codeDao.getCodeDescription());
		}

		if (!EJBUtil.isEmpty(patientEMRDetailBean.getADDL_RACE())) {
			delim = "";
			String token = "";
			StringBuilder addRaceCode = new StringBuilder();
			StringBuilder addRaceDesc = new StringBuilder();
			StringTokenizer st = new StringTokenizer(
					patientEMRDetailBean.getADDL_RACE(), ",");
			while (st.hasMoreElements()) {
				token = st.nextToken().trim();
				codeId = codeDao.getCodeId("race", token);
				addRaceCode.append(delim);
				addRaceCode.append(codeId);
				addRaceDesc.append(delim);
				addRaceDesc.append(codeDao.getCodeDescription());
				delim = ",";
			}
			patientEMRDetailBean.setADDL_RACE_CODE(addRaceCode.toString());
			patientEMRDetailBean.setADDL_RACE_DESC(addRaceDesc.toString());
		}

		if (!EJBUtil.isEmpty(patientEMRDetailBean.getADDL_ETHNICITY())) {
			delim = "";
			String token = "";
			StringBuilder addEthnicityCode = new StringBuilder();
			StringBuilder addEthnicityDesc = new StringBuilder();
			StringTokenizer st = new StringTokenizer(
					patientEMRDetailBean.getADDL_ETHNICITY(), ",");
			while (st.hasMoreElements()) {
				token = st.nextToken().trim();
				codeId = codeDao.getCodeId("ethnicity", token);
				addEthnicityCode.append(delim);
				addEthnicityCode.append(codeId);
				addEthnicityDesc.append(delim);
				addEthnicityDesc.append(codeDao.getCodeDescription());
				delim = ",";
			}
			patientEMRDetailBean.setADDL_ETHNICITY_CODE(addEthnicityCode
					.toString());
			patientEMRDetailBean.setADDL_ETHNICITY_DESC(addEthnicityDesc
					.toString());
		}
	}
}
