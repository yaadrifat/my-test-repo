package com.velos.eres.service.util;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import com.velos.eres.service.util.DateUtil;

public class PatientEMRDetailBean {

	private String PERSON_CODE;
	private String LASTNAME;
	private String FIRSTNAME;
	private String MIDDLENAME;
	private Date DOB;
	private String DOB_DESC;
	private String GENDER;
	private int GENDER_CODE;
	private String GENDER_DESC;
	private String AGE;
	private String AGE_DTL;
	private String RACE;
	private int RACE_CODE;
	private String RACE_DESC;
	private String ADDRESS1;
	private String ADDRESS2;
	private String CITY;
	private String STATE;
	private String ZIPCODE;
	private String COUNTRY;
	private String COUNTY;
	private String WORK_PHONE;
	private String HOME_PHONE;
	private String ETHNICITY;
	private int ETHNICITY_CODE;
	private String ETHNICITY_DESC;
	private Date DEATH_DATE;
	private String DEATH_DATE_DESC;
	private String SURVIVAL_STATUS;
	private int SURVIVAL_STATUS_CODE;
	private String SURVIVAL_STATUS_DESC;
	private String MARITAL_STATUS;
	private int MARITAL_STATUS_CODE;
	private String MARITAL_STATUS_DESC;
	private String SSN;
	private String ADDL_RACE;
	private String ADDL_RACE_CODE;
	private String ADDL_RACE_DESC;
	private String ADDL_ETHNICITY;
	private String ADDL_ETHNICITY_CODE;
	private String ADDL_ETHNICITY_DESC;

	public String getPERSON_CODE() {
		return PERSON_CODE;
	}

	public void setPERSON_CODE(String pERSON_CODE) {
		PERSON_CODE = pERSON_CODE;
	}

	public String getLASTNAME() {
		return LASTNAME;
	}

	public void setLASTNAME(String lASTNAME) {
		LASTNAME = lASTNAME;
	}

	public String getFIRSTNAME() {
		return FIRSTNAME;
	}

	public void setFIRSTNAME(String fIRSTNAME) {
		FIRSTNAME = fIRSTNAME;
	}

	public String getMIDDLENAME() {
		return MIDDLENAME;
	}

	public void setMIDDLENAME(String mIDDLENAME) {
		MIDDLENAME = mIDDLENAME;
	}

	public Date getDOB() {
		return DOB;
	}

	public void setDOB(Date dOB) {
		DOB = dOB;
		setDOB_DESC(DateUtil.dateToString(dOB));
	}

	public String getGENDER() {
		return GENDER;
	}

	public void setGENDER(String gENDER) {
		GENDER = gENDER;
	}

	public int getGENDER_CODE() {
		return GENDER_CODE;
	}

	public void setGENDER_CODE(int gENDER_CODE) {
		GENDER_CODE = gENDER_CODE;
	}

	public String getGENDER_DESC() {
		return GENDER_DESC;
	}

	public void setGENDER_DESC(String gENDER_DESC) {
		GENDER_DESC = gENDER_DESC;
	}

	public String getAGE() {
		return AGE;
	}

	public void setAGE(String aGE) {
		AGE = aGE;
	}

	public String getRACE() {
		return RACE;
	}

	public void setRACE(String rACE) {
		RACE = rACE;
	}

	public int getRACE_CODE() {
		return RACE_CODE;
	}

	public void setRACE_CODE(int rACE_CODE) {
		RACE_CODE = rACE_CODE;
	}

	public String getRACE_DESC() {
		return RACE_DESC;
	}

	public void setRACE_DESC(String rACE_DESC) {
		RACE_DESC = rACE_DESC;
	}

	public String getADDRESS1() {
		return ADDRESS1;
	}

	public void setADDRESS1(String aDDRESS1) {
		ADDRESS1 = aDDRESS1;
	}

	public String getADDRESS2() {
		return ADDRESS2;
	}

	public void setADDRESS2(String aDDRESS2) {
		ADDRESS2 = aDDRESS2;
	}

	public String getCITY() {
		return CITY;
	}

	public void setCITY(String cITY) {
		CITY = cITY;
	}

	public String getSTATE() {
		return STATE;
	}

	public void setSTATE(String sTATE) {
		STATE = sTATE;
	}

	public String getZIPCODE() {
		return ZIPCODE;
	}

	public void setZIPCODE(String zIPCODE) {
		ZIPCODE = zIPCODE;
	}

	public String getCOUNTRY() {
		return COUNTRY;
	}

	public void setCOUNTRY(String cOUNTRY) {
		COUNTRY = cOUNTRY;
	}

	public String getCOUNTY() {
		return COUNTY;
	}

	public void setCOUNTY(String cOUNTY) {
		COUNTY = cOUNTY;
	}

	public String getWORK_PHONE() {
		return WORK_PHONE;
	}

	public void setWORK_PHONE(String wORK_PHONE) {
		WORK_PHONE = wORK_PHONE;
	}

	public String getHOME_PHONE() {
		return HOME_PHONE;
	}

	public void setHOME_PHONE(String hOME_PHONE) {
		HOME_PHONE = hOME_PHONE;
	}

	public String getETHNICITY() {
		return ETHNICITY;
	}

	public void setETHNICITY(String eTHNICITY) {
		ETHNICITY = eTHNICITY;
	}

	public int getETHNICITY_CODE() {
		return ETHNICITY_CODE;
	}

	public void setETHNICITY_CODE(int eTHNICITY_CODE) {
		ETHNICITY_CODE = eTHNICITY_CODE;
	}

	public String getETHNICITY_DESC() {
		return ETHNICITY_DESC;
	}

	public void setETHNICITY_DESC(String eTHNICITY_DESC) {
		ETHNICITY_DESC = eTHNICITY_DESC;
	}

	public Date getDEATH_DATE() {
		return DEATH_DATE;
	}

	public void setDEATH_DATE(Date dEATH_DATE) {
		DEATH_DATE = dEATH_DATE;
		setDEATH_DATE_DESC(DateUtil.dateToString(dEATH_DATE));
	}

	public String getSURVIVAL_STATUS() {
		return SURVIVAL_STATUS;
	}

	public void setSURVIVAL_STATUS(String sURVIVAL_STATUS) {
		SURVIVAL_STATUS = sURVIVAL_STATUS;
	}

	public int getSURVIVAL_STATUS_CODE() {
		return SURVIVAL_STATUS_CODE;
	}

	public void setSURVIVAL_STATUS_CODE(int sURVIVAL_STATUS_CODE) {
		SURVIVAL_STATUS_CODE = sURVIVAL_STATUS_CODE;
	}

	public String getSURVIVAL_STATUS_DESC() {
		return SURVIVAL_STATUS_DESC;
	}

	public void setSURVIVAL_STATUS_DESC(String sURVIVAL_STATUS_DESC) {
		SURVIVAL_STATUS_DESC = sURVIVAL_STATUS_DESC;
	}

	public String getMARITAL_STATUS() {
		return MARITAL_STATUS;
	}

	public void setMARITAL_STATUS(String mARITAL_STATUS) {
		MARITAL_STATUS = mARITAL_STATUS;
	}

	public int getMARITAL_STATUS_CODE() {
		return MARITAL_STATUS_CODE;
	}

	public void setMARITAL_STATUS_CODE(int mARITAL_STATUS_CODE) {
		MARITAL_STATUS_CODE = mARITAL_STATUS_CODE;
	}

	public String getMARITAL_STATUS_DESC() {
		return MARITAL_STATUS_DESC;
	}

	public void setMARITAL_STATUS_DESC(String mARITAL_STATUS_DESC) {
		MARITAL_STATUS_DESC = mARITAL_STATUS_DESC;
	}

	public String getSSN() {
		return SSN;
	}

	public void setSSN(String sSN) {
		SSN = sSN;
	}

	public String getADDL_RACE() {
		return ADDL_RACE;
	}

	public void setADDL_RACE(String aDDL_RACE) {
		ADDL_RACE = aDDL_RACE;
	}

	public String getADDL_RACE_CODE() {
		return ADDL_RACE_CODE;
	}

	public void setADDL_RACE_CODE(String aDDL_RACE_CODE) {
		ADDL_RACE_CODE = aDDL_RACE_CODE;
	}

	public String getADDL_RACE_DESC() {
		return ADDL_RACE_DESC;
	}

	public void setADDL_RACE_DESC(String aDDL_RACE_DESC) {
		ADDL_RACE_DESC = aDDL_RACE_DESC;
	}

	public String getADDL_ETHNICITY() {
		return ADDL_ETHNICITY;
	}

	public void setADDL_ETHNICITY(String aDDL_ETHNICITY) {
		ADDL_ETHNICITY = aDDL_ETHNICITY;
	}

	public String getADDL_ETHNICITY_CODE() {
		return ADDL_ETHNICITY_CODE;
	}

	public void setADDL_ETHNICITY_CODE(String aDDL_ETHNICITY_CODE) {
		ADDL_ETHNICITY_CODE = aDDL_ETHNICITY_CODE;
	}

	public String getADDL_ETHNICITY_DESC() {
		return ADDL_ETHNICITY_DESC;
	}

	public void setADDL_ETHNICITY_DESC(String aDDL_ETHNICITY_DESC) {
		ADDL_ETHNICITY_DESC = aDDL_ETHNICITY_DESC;
	}

	public String getAGE_DTL() {
		return AGE_DTL;
	}

	public void setAGE_DTL(String aGE_DTL) {
		AGE_DTL = aGE_DTL;
	}

	public String getDOB_DESC() {
		return DOB_DESC;
	}

	public void setDOB_DESC(String dOB_DESC) {
		DOB_DESC = dOB_DESC;
	}

	public String getDEATH_DATE_DESC() {
		return DEATH_DATE_DESC;
	}

	public void setDEATH_DATE_DESC(String dEATH_DATE_DESC) {
		DEATH_DATE_DESC = dEATH_DATE_DESC;
	}

	public Date returnDate(String dateString) {
		Date parsed = null;
		if (dateString == null || "".equals(dateString)) {
			return null;
		} else {
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");

			try {
				parsed = format.parse(dateString);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return parsed;
	}
}
