package com.velos.eres.web.intercept;

import com.velos.eres.service.util.LC;

	 public enum SubmissionErrorMsg {
	        PRINCIPAL_INV_MISSING("Study IDs: Principal Investigator is missing", "Principal Investigator"), 
	        STUDY_CONTACT_MISSING("Study IDs: Study Contact is missing", "Study Contact"), 
	        STUDY_ROLE_MDACC_PI_MISSING("Study IDs: Role of MDACC Principal Investigator is missing", "Role of MDACC Principal Investigator"), 
	        STUDY_DIVISION_MISSING("Study IDs: Division is missing", "Division"), 
	        STUDY_TEMPLATE_MISSING("Study IDs: Study template is missing", "Study template"), 
	        PROTOCOL_REQUIRES_SIGNED_INFORMED_CONSENT_MISSING("Study IDs: Protocol requires signed informed consent is missing",
	            "Protocol requires signed informed consent"),
	        RESEARCH_TYPE_MISSING("Study IDs: "+LC.L_Research_Type+" is missing" , LC.L_Research_Type),
	        CLINICAL_RESEARCH_CATEGORY_MISSING("Study IDs: Clinical Research category is missing","Clinical Research Category"),
	        PRIMARY_PURPOSE_MISSING("Study IDs: Primary Purpose is missing" , "Primary Purpose"),
	        STUDY_DEPARTMENT_MISSING("Study IDs: Department is missing", "Department"), 
	        STUDY_INCLUDES_MISSING("Study IDs: Study includes is missing", "Study includes"), 
	        STUDY_PRIMARY_OBJECTIVE_MISSING("Study Objectives: Primary Objective is missing" , "Primary Objective"),
	        STUDY_SEC_OBJ_TEXT_MISSING("Study Objectives: Secondary Objective is missing", "Secondary Objective"),
	        STUDY_EXP_OBJ_TEXT_MISSING("Study Objectives: Exploratory Objective is missing", "Exploratory Objective"),
	        STUDY_BIO_OBJ_TEXT_MISSING("Study Objectives: Biomarker Objective is missing", "Biomarker Objective"), 
	        STUDY_PHASE_MISSING("Study Design: "+LC.L_Phase+" is missing", LC.L_Phase),
	        STUDY_RANDOMIZATION_MISSING("Study Design: "+LC.L_Randomization+" is missing", LC.L_Randomization), 
	        STUDY_RANDOMIZATION_NOTES_MISSING("Study Design: Randomization Notes is missing", "Randomization Notes"), 
	        STUDY_BLINDING_MISSING("Study Design: Blinding is missing", "Blinding"),
	        STUDY_BLINDING_NOTES_MISSING("Study Design: Blinding Description is missing" , "Blinding Description" ),
	        STUDY_SCOPE_MISSING("Study Design: Study Scope is missing", "Study Scope"), 
	        STUDY_ESTIMATED_ENROLLED_MDACC_MISSING("Study Design: Estimated number enrolled per month at MDACC is missing", 
	        		"Estimated number enrolled per month at MDACC"), 
	        STUDY_TOTAL_ENROLLED_MDACC_MISSING("Study Design: Total number enrolled at MDACC is missing", 
	        		"Total number enrolled at MDACC"), 
	        STUDY_TOTAL_ENROLLED_ALL_SITES_MISSING("Study Design: Total number enrolled at all sites is missing", 
	        		"Total number enrolled at all sites"), 
	        STUDY_DRUG_AGENT_MISSING("Study Drug: Agent-1 is missing", "Agent-1"), 
	        STUDY_DRUG_AGENT_DOSE_MISSING("Study Drug: Dose is missing", "Dose"), 
	        STUDY_DRUG_AGENT_DOSE_RATNL_MISSING("Study Drug: Dose Rationale is missing", "Dose Rationale"), 
	        STUDY_DRUG_AGENT_ROUTE_MISSING("Study Drug: Route of Administration is missing", "Route of Administration"), 
	        STUDY_DRUG_MANUFACTURER_MISSING("Study Drug: Manufacturer is missing", "Manufacturer"), 
	        STUDY_INOLVE_HUMAN_ANIMAL_TISSUE_MISSING("Study Drug: Does the study involve human/animal tissue other than blood derived hematopoietic stem cells is missing", 
	        		"Does the study involve human/animal tissue other than blood derived hematopoietic stem cells"),
	        STUDY_INVOLVE_RECOMB_DNA_MISSING("Study Drug: Does the study involve the use of Recombinant DNA technology is missing", "Does the study involve the use of Recombinant DNA technology"), 
	        STUDY_INVOLVE_RADIO_ISOTOPE_MISSING("Study Drug: Does the study involve the administration of radioisotopes or a radioisotope labeled agent is missing", 
	        		"Does the study involve the administration of radioisotopes or a radioisotope labeled agent"), 
	        STUDY_TREATMENT_DURATION_MISSING ("Study Drug: Treatment Duration is missing", 
	        		"Treatment Duration"),
	        AUTHORIZED_USER_MISSING("Study Drug: Authorized user is missing", 
	        		"Authorized user"),
	        AUTHORIZED_NUMBER_MISSING("Study Drug: Authorization number (or state that authorization application is under Radiation Safety Committee Review) is missing", 
	        		"Authorization number (or state that authorization application is under Radiation Safety Committee Review)"), 
	        STUDY_ADMINISTER_RADIO_ACTIVE_MISSING("Study Drug: Does this protocol include the administration of a radioactive compound (or drug) to a patient intended to obtain basic information regarding metabolism (including kinetics, distribution, and localization) of the drug or regarding human physiology, pathophysiology, or biochemistry, but not intended for immediate therapeutic, diagnostic, or similar purposes or to determine the safety and effectiveness of the drug in humans for such purposes (i.e. to carry out a clinical study) is missing", 
	        		"Does this protocol include the administration of a radioactive compound (or drug) to a patient intended to obtain basic information regarding metabolism (including kinetics, distribution, and localization) of the drug or regarding human physiology, pathophysiology, or biochemistry, but not intended for immediate therapeutic, diagnostic, or similar purposes or to determine the safety and effectiveness of the drug in humans for such purposes (i.e. to carry out a clinical study)"), 
	        RADIO_ACTIVE_FDA_APPROVED_MISSING("Study Drug: Is the radioactive compound (or drug) FDA approved and/or commercially available is missing", 
	        		"Is the radioactive compound (or drug) FDA approved and/or commercially available"), 
	        FACILITY_INVESTIGATIONAL_AGENT_BOUND_MISSING("Study Drug: Name the facility where the investigational agent is labeled or bound to the radioisotope (include the name of the laboratory at MDACC or the name of the company if outside MDACC) is missing", 
	        		"Name the facility where the investigational agent is labeled or bound to the radioisotope (include the name of the laboratory at MDACC or the name of the company if outside MDACC)"), 
	        IBC_PROTOCOL_NUMBER_MISSING("Study Drug: Provide the IBC protocol number or contact the IBC coordinator for information at 713-563-3879 is missing", 
	        		"Provide the IBC protocol number or contact the IBC coordinator for information at 713-563-3879"), 
	        STUDY_REQUIRE_NIH_RAC_REVIEW_MISSING("Study Drug: Does the study require NIH/RAC review is missing", 
	        		"Does the study require NIH/RAC review"),
	        STUDY_REQUIRE_NIH_RAC_REVIEW_COMMENT_MISSING("Study Drug: Does the study require NIH/RAC review comment is missing" , 
	            "Does the study require NIH/RAC review comment"),
	            
	        STUDY_INVOLVE_ORGANISMS_INFECTIOUS_TO_HUMANS_MISSING("Study Drug: Does this study involve the use of organisms that are infectious to humans is missing",
	            "Does this study involve the use of organisms that are infectious to humans"),
	        STUDY_INVOLVE_ORGANISMS_INFECTIOUS_TO_HUMANS_COMMENT_MISSING("Study Drug: Does this study involve the use of organisms that are infectious to humans comment is missing",
	            "Does this study involve the use of organisms that are infectious to humans Comment"),
	            
	        USING_TISSEL_OR_EVICEL_MISSING("Study Drug: Are you using Tisseel or Evicel or similar is missing" , "Are you using Tisseel or Evicel or similar"),
	        STUDY_DEVICE1_MISSING("Medical and Investigational Devices: Device-1 is missing", "Device-1 "),
	        STUDY_DEVICE_MANUFACTURER1_MISSING("Medical and Investigational Devices: Manufacturer is missing", "Manufacturer "),
	        OFF_STUDY_CRITERIA_MISSING("Withdrawal and replacement of participants: Off Study Criteria is missing" , "Off Study Criteria"),
	        CRITERIA_FOR_REMOVAL_MISSING("Withdrawal and replacement of participants: Criteria for removal of participants is missing" , "Criteria for removal of participants"),
	        SPECIFY_OTHER_CRITERIA_MISSING("Withdrawal and replacement of participants: specify other criteria for removal of participants is missing" ,
	        		"Specify other criteria for Withdrawal and replacement of participants"),
	        CRITERIA_FOR_REPLACEMENT_MISSING("Withdrawal and replacement of participants: Criteria for Replacement of Participants missing", 
	            "Criteria for Replacement of Participants"),
	        SCREENING_BASELINE_MISSING("Study Evaluation and Procedures: Screening/Baseline is missing", "Screening/Baseline"),
	        
	        ON_STUDY_TREATMENT_EVALUATIONS_MISSING("Study Evaluation and Procedures: On-study/Treatment Evaluations is missing" , "On-study/Treatment Evaluations"),
	        END_OF_THERAPY_MISSING("Study Evaluation and Procedures: End of Therapy Evaluations is missing", "End of Therapy Evaluations"),
	        FOLLOW_UP_MISSING("Study Evaluation and Procedures: Follow-up is missing", "Follow-up"),
	        STUDY_CALENDER_MISSING("Study Evaluation and Procedures: Study Calendar/Schedule of Events is missing", "Study Calendar/Schedule of Events"),
	        FRESH_BIOPSY_NEEDED_MISSING("Study Evaluation and Procedures: Are fresh biopsies needed? is missing", "Are fresh biopsies needed?"),
            FRESH_BIOPSY_NEEDED_YES_MISSING("Study Evaluation and Procedures: If fresh biopsies needed is yes, please describe is missing" , 
                "If fresh biopsies needed is yes,please describe"),
	        REPORTING_TO_SPONSOR_MISSING("Safety and adverse events: Reporting to sponsor is missing" , "Reporting to sponsor"),
	        DATA_SAFETY_MONITORING_PLAN_MISSING("Data Safety and Monitoring Plan: Data Safety and Monitoring Plan is missing", "Data Safety and Monitoring Plan"),
	        FINANCIAL_SUPPORT_SPONSOR_NAME_MISSING("Financial Support: Sponsor Name1 is missing","Sponsor Name1"  ),
	        FINANCIAL_SUPPORT_TYPE_OF_FUNDING_MISSING("Financial Support: Funding Type is missing" , "Funding Type"),
	        FINANCIAL_SUPPORT_CANCER_SUPPORT_GRANT_MISSING("Financial Support: Which Cancer Center Support Grant (CCSG) - NCI Core Grant Program does this protocol relate to? is missing",
	        	"Which Cancer Center Support Grant (CCSG) - NCI Core Grant Program does this protocol relate to?"	),
	        REFERENCES_MISSING("References: References is missing", "References"),
	        NOMINATED_REVIEWER_MISSING("Reviewers and Pre-Reviews: Nominated Reviewer is missing" , "Nominated Reviewer"),
	        ALTERNATE_NOMINATED_REVIEWER_MISSING("Reviewers and Pre-Reviews: Nominated Reviewer (alternate) is missing" , "Nominated Reviewer (alternate)"),
	        PRIMARY_END_POINT_MISSING("Statistical Considerations: Primary Endpoints is missing", "Primary Endpoints"),
	        DID_MCDACC_BIOSTAT_PARTICIPATE_MISSING( "Statistical Considerations: Did an MDACC Biostatistician participate in the study design is missing",
	        		"Did an MDACC Biostatistician participate in the study design?"),
	        IS_PLANNED_INTERIM_ANALYSIS_MISSING("Statistical Considerations: Does this protocol have a planned interim analysis is missing" ,
	        		"Does this protocol have a planned interim analysis" ),
	        DATA_SAFETY_MONITORING_BOARD_MISSING("Statistical Considerations: Data Safety and Monitoring Board is missing"
	        		,"Data Safety and Monitoring Board is missing" ),
	        STATISTICAL_ANALYSIS_PLAN_MISSING("Statistical Considerations: Statistical Analysis Plan is missing", "Statistical Analysis Plan"),
	        BAYSEAN_COMPONENT_INCLUDED_MISSING("Statistical Considerations: Bayesian component included in protocol is missing" , "Bayesian component included in protocol"),
	        SUMMARY_OF_PLANNED_INTERIM_ANALYSIS_MISSING("Statistical Considerations: Summary of planned interim analysis is missing" ,"Summary of planned interim analysis is missing" ),
	        RATIONALE_FOR_NO_INTERIM_ANALYSIS_MISSING("Statistical Considerations: Rationale for no interim analyses is missing","Rationale for no interim analyses is missing" ),
	        DSMB_COMPOSITION_MISSING("Statistical Considerations: DSMB Composition is missing" , "DSMB Composition"),
	        ARE_YOU_MEASURING_BIOMARKERS_MISSING("Biomarker and correlative studies: Are you measuring Biomarkers is missing" , "Are you measuring Biomarkers"),
	        ARE_THEY_KNOWN_BIOMARKERS_MISSING("Biomarker and correlative studies: Are they known Biomarkers is missing", "Are they known Biomarkers"),
	        BIOMARKER_TYPE_MISSING("Biomarker and correlative studies: Biomarker type is missing", "Biomarker type"),
	        SPECIFY_OTHER_BIOMARKER_MISSING("Biomarker and correlative studies: Specify other biomarker is missing", "Specify other biomarker" ),
	        TEST_NAME_MISSING("Biomarker and correlative studies: What are you calling the test? Use commonly accepted test is missing", 
	        		"What are you calling the test? Use commonly accepted test"),
	        BIOMARKER_NAME_MISSING("Biomarker and correlative studies: What are you calling the biomarker is missing" , "What are you calling the biomarker" ),
	        BIOMARKER_LEVEL_MISSING("Biomarker and correlative studies: What level are you trying to look at is missing" , "What level are you trying to look at"),
	        WHAT_ARE_YOU_MEASURING_MISSING("Biomarker and correlative studies: Where are you measuring is missing" , "Where are you measuring"),
	        TIMEPOINTS_MISSING("Biomarker and correlative studies: Time points is missing" , "Time points"),
	        METHODOLOGY_MISSING("Biomarker and correlative studies: Methodology is missing" , "Methodology"), 
	        SELECT_OTHER_OBJECTIVES_MISSING("Study Objective: Select other objectives for entry is missing", "Select other objectives for entry"),
	        IND_IDE_INFO_AVAILABLE_MISSING("IND/IDE Information: IND/IDE Information Available is missing" , "IND/IDE Information Available"),
	        IND_IDE_TYPE_MISSING("IND/IDE Information: IND/IDE Types is missing", "IND/IDE Types"),
	        IND_IDE_NUMBER_MISSING("IND/IDE Information: IND/IDE number is missing" , "IND/IDE number" ),
	        IND_IDE_GRANTOR_MISSING("IND/IDE Information: IND/IDE Grantor is missing", "IND/IDE Grantor is missing"),
	        IND_IDE_HOLDER_MISSING("IND/IDE Information: IND/IDE Holder Type is missing", "IND/IDE Holder Type"),
	        NIH_NCI_DIVISION_MISSING("IND/IDE Information: NIH Institution, NCI Division/Program Code (If applicable) is missing" , 
	        		"NIH Institution, NCI Division/Program Code (If applicable)"),
	        EXPANDED_ACCESS_MISSING("IND/IDE Information: Expanded Access is missing" , "Expanded Access"),
	        EXPANDED_ACCESS_TYPE_MISSING("IND/IDE Information: Expanded Access Type (If applicable) is missing", "Expanded Access Type (If applicable)"),
	        STUDY_RATIONALE_MISSING("Study Background: Study Rationale is missing" , "Study Rationale"),
	        CLINICAL_DATA_TO_DATE_MISSING("Study Background: Clinical Data to Date is missing" , "Clinical Data to Date"), 
	        SPONSOR_PROTOCOL_NUMBER_MISSING("Study IDs: Sponsor protocol number is missing" , "Sponsor Protocol Number"),
	        SECONDARY_DEPARTMENT_MISSING("Study IDs: Administrative Department is missing" ,"Administrative Department"), 
	        STUDY_SPECIFIC_SITE_MISSING("Study Design: Specific site is missing" , "Specific site"), 
	        STUDY_SPECIFIC_SITE2_MISSING("Study Design: Specific site 2 is missing" , "Specific site 2"), 
	        STUDY_DISEASE_SITE_MISSING("Study Design: Disease site is missing" , "Disease site"),
	        //IND -EXEMPT SECTION
	        DOES_PROTOCOL_REQUIRE_IND_MISSING("IND Exempt: Does this protocol require an IND is missing" , "Does this protocol require an IND?"),
	        //SAFETY AND ADVERSE EVENTS SECTION
	        WHAT_CRITERIA_ADVERSE_EVENTS_MISSING ("Safety and adverse events: What criteria are you using for classifying Adverse Events is missing", 
	            "What criteria are you using for classifying Adverse Events"),
	        WHAT_CRITERIA_ADVERSE_EVENTS_OTHER_MISSING("Safety and adverse events: What criteria are you using for classifying Adverse Events Other is missing", 
                "What criteria are you using for classifying Adverse Events Other"),
	        PROVIDE_LIST_ANTICIPATED_AE_MISSING("Safety and adverse events: If investigational, provide list of anticipated AEs is missing" ,
	            "If investigational, provide list of anticipated AEs"),
	        REQUEST_EXPEDITED_BIOSTAT_REVIEW_MISSING("Study IDs: Are you requesting an expedited biostatistical review for this protocol is missing" , 
	            "Are you requesting an expedited biostatistical review for this protocol?"),
	        TREATMENT_ARMS_MISSING("Study Design: Treatment Arms is missing" , "Treatment Arms"),
	        SAMPLE_SIZE_JUSTIFICATION_MISSING("Study Design: Sample size justification is missing" , "Sample size justification"),
	        COMMERCIAL_DRUG_INSERT_MISSING("Safety and adverse events: If commercially available, include drug insert is missing", 
	            "If commercially available, include drug insert"),
	        REPORT_OF_SAE_UNANTICIPATED_PROBLEMS_MISSING("Safety and adverse events: Reports of SAEs and Unanticipated Problems is missing",
	            "Reports of SAEs and Unanticipated Problems"),
	        //SUBJECT SELECTION SECTION
	        SUBJECT_INCLUSION_CREITERIA_MISSING("Subject Selection: Inclusion Criteria is missing", "Inclusion Criteria"),
	        SUBJECT_INCLUSION_CREITERIA_NA_MISSING("Subject Selection: Inclusion Criteria N/A is missing" ,"Inclusion Criteria N/A"),  
	        SUBJECT_EXCLUSION_CREITERIA_MISSING("Subject Selection: Exclusion Criteria is missing" ,"Exclusion Criteria"), 
	        SUBJECT_EXCLUSION_CREITERIA_NA_MISSING("Subject Selection: Exclusion Criteria N/A is missing","Exclusion Criteria N/A"),
	        
	        DEPARTMENT_CHAIR_MISSING("Reviewers and Pre-Reviews: Department Chair is missing" , "Department Chair");
	        
	        private String result_text;
	        
	        private String test_text ;

	        private SubmissionErrorMsg(String result_text , String test_text) {
	                this.result_text = result_text;
	                this.test_text = test_text;
	        }
	        
	        public String getResultText(){
	        	
	        	return this.result_text;
	        }
	        
            public String getTestText(){
	        	
	        	return this.test_text;
	        }
	        
	};  

