package com.velos.eres.web.intercept;

import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.FormCompareUtilityDao;
import com.velos.eres.service.util.SessionMaint;
import com.velos.eres.web.studyId.StudyIdJB;
import com.velos.esch.service.util.StringUtil;
import com.velos.services.OperationException;
import com.velos.services.outbound.MessageQueueBean;
import com.velos.services.patientstudy.PatientStudyDAO;

public class CustomPatStudyStatInterceptorJB extends InterceptorJB {
	@Override
	public void handle(Object... args) {
		if (args == null || args.length < 1) {
			return;
		}
		MessageQueueBean bean = (MessageQueueBean) args[0];
		if ("0".equals(bean.getRootFK()) && "er_patprot".equals(bean.getRootTableName())) {
			try {
				bean.setRootFK(PatientStudyDAO
						.getPatProtPkFromPatStudyStatusPk(bean.getTablePK())
						.toString());
			} catch (OperationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}