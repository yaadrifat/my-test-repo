<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></HEAD>
	<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
	<Link Rel=STYLESHEET HREF="common.css" type=text/css>
</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
	<jsp:useBean id="prefB" scope="request" class="com.velos.eres.web.pref.PrefJB"/>
	<jsp:useBean id="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>
	<jsp:useBean id="siteB" scope="request" class="com.velos.eres.web.site.SiteJB"/>
	<jsp:useBean id="studyStatB" scope="page" class="com.velos.eres.web.studyStatus.StudyStatusJB" />
	<jsp:useBean id="commonB" scope="request" class="com.velos.eres.web.common.CommonJB"/>
	<%@ page language = "java" import = "com.velos.eres.business.common.*,com.velos.eres.service.util.*,java.util.*,com.velos.eres.business.pref.impl.*,com.velos.eres.business.person.impl.*"%>

<%
	String eSign = request.getParameter("eSign");
	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession)){
%>
	<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
	String oldESign = (String) tSession.getAttribute("eSign");
	if(!oldESign.equals(eSign)) {
%>
	<jsp:include page="incorrectesign.jsp" flush="true"/>
<%
	} else {
	String ipAdd = (String) tSession.getAttribute("ipAdd");
	String usr = null;
	usr = (String) tSession.getAttribute("userId");
	String acc=(String)tSession.getAttribute("accountId");
	PrefBean psk1 = null;
	PrefBean psk2 = null;
	int personPK  = 0;
	String mode = null;
	String patientID = "";
	String fname = "";
	String lname = "";
	String mname = "";
	String aname = "";
	String dob = "";
	String gender = "";
	String ethnicity = "";
	String race = "";
	String ssn = "";
	String organization = "";
	String patDeathDate = "";
	String regBy = "";
	String status = "";
	String account = "";
	String statDesc="";
	String statid="";
	String studyVer="";
	String calledFrom = "quick";
	String studyId = "";
	String phyOther="";
	String orgOther="";
	String specialityIds = "";
	String addEthnicityIds = "";
	String addRaceIds = "";
	String dthCause = "";
	String othDthCause = "";
	String patientFacilityId = "";
	studyId  = request.getParameter("selstudyId");
	studyId=(studyId==null)?"":studyId;
	studyB.setId(EJBUtil.stringToNum(studyId));
	studyB.getStudyDetails();
	String studyNum = studyB.getStudyNumber();
	studyNum=(studyNum==null)?"":studyNum;
	organization = request.getParameter("patorganization");
	organization=(organization==null)?"":organization;

	String add1 = "",add2 = "", city = "", state = "",zip = "",country = "",county = "", bphone = "",hphone= "",email = "",personMarital="", personSSN="";

	   add1 = request.getParameter("patadd1");
	   add1=(add1==null)?"":add1;
	   add2 = request.getParameter("patadd2");
	   add2=(add2==null)?"":add2;
	   city = request.getParameter("patcity");
	   city=(city==null)?"":city;
	   state = request.getParameter("patstate");
	   state=(state==null)?"":state;
	   zip = request.getParameter("patzip");
	   zip=(zip==null)?"":zip;
	   country = request.getParameter("patcountry");
	   country=(country==null)?"":country;
	   county = request.getParameter("patcounty");
	   county=(county==null)?"":county;
	   bphone = request.getParameter("patbphone");
	   bphone=(bphone==null)?"":bphone;
	   hphone= request.getParameter("pathphone");
	   hphone=(hphone==null)?"":hphone;
	   email = request.getParameter("patemail");
	   email=(email==null)?"":email;
	   personMarital= request.getParameter("maritalStat");
	   personMarital=(personMarital==null)?"":personMarital;
	   personSSN = request.getParameter("ssn");
	   personSSN=(personSSN==null)?"":personSSN;
	   mname = request.getParameter("mname");
	   mname=(mname==null)?"":mname;

//	Check if the Organization study combination is open for enrolling
	StudyStatusDao statDao=null;
	int iStudyId=EJBUtil.stringToNum(studyId);
	int index=-1,stopEnrollCount=0;
	String studyAccrualFlag="";
	ArrayList statList=null;
	if (studyId.length()>0)
	{
	 SettingsDao settingsDao=commonB.retrieveSettings(iStudyId,3);
	 ArrayList settingsValue=settingsDao.getSettingValue();
	 ArrayList settingsKeyword=settingsDao.getSettingKeyword();
	 index=settingsKeyword.indexOf("STUDY_ACCRUAL_FLAG");
		if (index>=0)
		{
		 studyAccrualFlag=(String)settingsValue.get(index);
		 studyAccrualFlag=(studyAccrualFlag==null)?"":studyAccrualFlag;
		}
		else
		{
			studyAccrualFlag="D";
		}
	  if ( (studyAccrualFlag.equals("D")) || (studyAccrualFlag.length()==0) ) {
//		Disable this for now, with Default settings everything will work as it was in ver 6.2
		  //stopEnrollCount=studyStatB.getCountByOrgStudyStat(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(organization),"'active_cls'","A");
   		stopEnrollCount=0;
	  }
	  else if (studyAccrualFlag.equals("O"))
	  {
		 statDao=studyStatB.getDAOByOrgStudyStat(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(organization),"'active_cls','active','prmnt_cls'","A");
		 statList=statDao.getStudyStatList();
		 if (statList.size()==0) stopEnrollCount=0;

		 if (statList.indexOf("active_cls")>=0)
			 stopEnrollCount=1;

		 if (statList.indexOf("active")<0)
			 stopEnrollCount=1;

		 if (statList.indexOf("prmnt_cls")>0)
			 stopEnrollCount=1;


	  }
	}

	//End check for Organization study combination is open for enrolling


	siteB.setSiteId(EJBUtil.stringToNum(organization));
	siteB.getSiteDetails();
	String siteName = siteB.getSiteName();
	int cnt = studyB.getOrgAccessForStudy(EJBUtil.stringToNum(usr), EJBUtil.stringToNum(studyId), EJBUtil.stringToNum(organization) );
	if((cnt>0) || (studyId.length()==0)){
		String src = request.getParameter("srcmenu");
		String srcmenu=request.getParameter("srcmenu");
		mode = request.getParameter("mode");
		String pkey = request.getParameter("pkey");
		statDesc= request.getParameter("statDesc");
		statid= request.getParameter("statid");
		studyVer= request.getParameter("studyVer");
		//othDthCause = request.getParameter("othDthCause");
		othDthCause = request.getParameter("otherdthCause");
		//get values from request object
		patientID = request.getParameter("patID");
		patientFacilityId = request.getParameter("patFacilityID");
		if (StringUtil.isEmpty(patientFacilityId )){
			patientFacilityId  = patientID ;
		}
		fname = request.getParameter("patfname");
		lname = request.getParameter("patlname");
		aname = request.getParameter("aliasname");
		if(aname==null)
			aname="";
		dob = request.getParameter("patdob");
		gender = request.getParameter("patgender");
		ethnicity = request.getParameter("patethnicity");
		race = request.getParameter("patrace");
		regBy = request.getParameter("patregby");
		patDeathDate = request.getParameter("patdeathdate");
		status = request.getParameter("patstatus");
		account = request.getParameter("pataccount");
		phyOther=request.getParameter("phyOther");
		//specialityIds = request.getParameter("selSpecialityIds");
		specialityIds = request.getParameter("selSpecialityIds");
		if(specialityIds.equals("null")){
		   specialityIds="";
		}
		addEthnicityIds = request.getParameter("selAddEthnicityIds");
		dthCause = request.getParameter("dthCause");
		//String sids=request.getParameter("selSpecialityID");
		if(addEthnicityIds.equals("")){
			addEthnicityIds=null;
		}
		addRaceIds = request.getParameter("selAddRaceIds");
		if(addRaceIds.equals("")){
			addRaceIds=null;
		}
		if (gender.equals("")) gender = null;
		if (ethnicity.equals("")) ethnicity = null;
		if(race!=null){
			if (race.equals("")) race = null;
		}
		if (organization.equals("")) organization = null;
		if (status.equals("")) status= null;

		int saved = 0;
		if (mode.equals("M")){
			//out.print("key" +request.getParameter("pkey"));
			personPK = EJBUtil.stringToNum(request.getParameter("pkey"));
			person.setPersonPKId(personPK);
			person.getPersonDetails();
		}
		boolean codeExists = false;
		patientID = patientID.trim();
		if((!mode.equals("M")) || (mode.equals("M") && !(person.getPersonPId().equals(patientID)))) {
			codeExists = prefB.patientCodeExistsOneOrg(patientID, organization);
		}
		if(codeExists){
%>
	<br><br><br><br><br>

			<p class = "successfulmsg" align = center> <%=MC.M_PatIdExst_OrgEtrNew%><%-- The <%=LC.Pat_Patient%> ID you have entered already exists for the selected organization. Please click on back and enter a new ID.*****--%>
			</p>
			<p align = center><button onclick="window.history.back();"><%=LC.L_Back%></button>
			</p>
<%		}else if (stopEnrollCount>0)
			{ %>
	<br><br><br><br><br>

	<p class = "successfulmsg" align = center> <%=MC.M_EnrlNotPrmtd_CnctStdAdmin %><%-- Enrollment is not permitted at this time. The <%=LC.Std_Study_Lower%> may not be open at this organization, may be closed or temporarily on hold. Please contact the <%=LC.Std_Study%> Administrator for more information.*****--%>
	</p>
	<p align = center><button onclick="window.history.back();"><%=LC.L_Back%></button>
	</p>


			<% }

		else {
	   	person.setPersonPId(patientID);
		person.setPersonAccount(account);
		person.setPersonLocation(organization);
        person.setPersonDob(dob);
        person.setPersonFname(fname);
		person.setPersonGender(gender);
        person.setPersonLname(lname);
        person.setPersonAlias(aname);
        person.setPersonEthnicity(ethnicity);
        person.setPersonRace(race);
    	person.setPersonStatus(status);
		person.setPersonDeathDate(patDeathDate);
		person.setPersonRegBy(regBy);
		person.setPhyOther(phyOther);
		person.setPersonSplAccess(specialityIds);
		person.setPersonAddEthnicity(addEthnicityIds);
		person.setPersonAddRace(addRaceIds);
		person.setPatDthCause(dthCause);
		person.setPatientFacilityId(patientFacilityId);
	
		//set Address information
		person.setPersonAddress1(add1);
	    person.setPersonAddress2(add2);
		person.setPersonCity(city);
		person.setPersonCountry(country);
		person.setPersonCounty(county);

		person.setPersonEmail(email);
		person.setPersonHphone(hphone);
		person.setPersonBphone(bphone);
		person.setPersonState(state);

		person.setPersonZip(zip);

		//End address informatin

		person.setPersonMarital(personMarital);
		person.setPersonSSN(personSSN);
		person.setPersonMname(mname);
		if(othDthCause!=null )
			person.setDthCauseOther(othDthCause);
		else
			person.setDthCauseOther("");
		PersonBean pers = null;
		if (mode.equals("M")){
			person.setModifiedBy(usr);
			person.setIpAdd(ipAdd);
			saved = person.updatePerson();
		}else{
			//Added by Gopu for July-August'06 Enhancement (U2) - Admin Settings for Patient Time zone
			SettingsDao settingsDao=commonB.getSettingsInstance();
			int modname=1;
			String keyword="ACC_PAT_TZ";
			settingsDao.retrieveSettings(EJBUtil.stringToNum(acc),modname,keyword);
			ArrayList setvalues=settingsDao.getSettingValue();
			String setvalue="";
			if(setvalues!=null){
				setvalue=(setvalues.get(0)==null)?setvalue:(setvalues.get(0)).toString();
			}
			if (!(setvalue.equals(""))){
				 person.setTimeZoneId(setvalue);
			}
			//Commented by Manimaran to fix the Bug2956
			//person.setModifiedBy(usr);
			person.setCreator(usr);
			person.setIpAdd(ipAdd);
			pers = person.setPersonDetails();
			if (pers !=null){
				saved = 0;
				personPK =  pers.getPersonPKId();
			}else{
				saved = -1;
			}
		}
		if (saved == 0){
%>
		<br><br><br><br><br>
		<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%-- Data was saved successfully*****--%> </p>
	<%	if ( mode.equals("N") && EJBUtil.stringToNum(studyId) > 0){
				%>
				 <!--km -to fix the Bug 2408 -->
				<META HTTP-EQUIV=Refresh CONTENT="1; URL=enrollpatient.jsp?mode=N&srcmenu=tdmenubaritem5&patientCode=<%=StringUtil.encodeString(patientID)%>&patProtId=&pkey=<%=personPK%>&page=patientEnroll&selectedTab=2&studyId=<%=studyId%>" >
				<%
				}else{
				%>
				<META HTTP-EQUIV=Refresh CONTENT="1; URL=patientdetails.jsp?srcmenu=tdmenubaritem5&othDthCause=<%=othDthCause%>&patientCode=<%=StringUtil.encodeString(patientID)%>&mode=M&selectedTab=1&pkey=<%=personPK%>&specialityIds=<%=specialityIds%>&page=patient">
	<%			}
		}else{
	%>
	<br><br><br><br><br>
	<p class = "successfulmsg" align = center> <%=MC.M_DataCnt_Svd %><%-- Data could not be saved.*****--%></p>
	<%	}
		}
	}//cnt
	else
	{%>
	<br><br><br><br><br>
		<p class = "successfulmsg" align = center><%Object[] arguments1 = {siteName,studyNum}; %>
	    <%=VelosResourceBundle.getMessageString("M_UsrNoRgt_PatOrgStd",arguments1)%><%-- User doesn't have right to enroll <%=LC.Pat_Patient%> from Organization: <%=siteName%> Study to <%=LC.Std_Study%>: <%=studyNum%>*****--%></p>
		<p align = center><button onclick="window.history.back();"><%=LC.L_Back%></button></p>
	<%}
	}//end of if for eSign check
	}//end of if body for session
	else{
	%>
	  <jsp:include page="timeout.html" flush="true"/>
	<%}%>
</BODY>
</HTML>












