This is a custom project of Velos eRerearch for MD Anderson ADT interface and all the other MDA interfaces combined.

The base eResearch is v9.3.0 for the trunk (CVS HEAD) of this project. This project started with the version compatible with
eResearch v9.2.0. The branch of this project which is compatible with eResearch v9.2.0 is GA9_2.

1) Put this in labelBundle_custom.properties.

# For Core Interface
Config_PatStudyStat_Interceptor_Class=com.velos.eres.web.intercept.CustomPatStudyStatInterceptorJB
# For ADT Interface
Config_PT_HOST=http://localhost
Config_PT_EMR_URL=/PatientEMRS/PatientEMR
Config_PT_USER=tenseUser
Config_PT_PASSWORD=tensePassword

2) The following files were merged with velos (for both 9.2 and head) and therefore CVS-removed:
LC.java
MC.java
FormDesignDAO.java
FormField.java (in com.velos.services.model)
UserIdentifier.java
