var searchtype = "PatientEMR";
var currentSelc = "";
//xml variable
var moduleTag = "Module";
var webserviceTag = "WebService";
var urlTag = "URL";
var widgetTag = "Widget";
var parameterTag = "Parameter";
var outputTag = "Output";
var labelTag = "Label";
var noFieldsTag = "NoFields";
var stylesTag = "Styles";
var resultWidgetTag = "ResultWidget";
var styleHeaderTag = "Header";
var styleListWidgetTag = "ListWidget";
var styleTag = "Style";
var typeTag = "Type";
var tabularType = "TabularForm";
var freeFormType = "FreeForm";
var googleFormType = "GoogleForm";
var noOfColumnTag = "NoOfColumn";
var noOfColumnToshow = "NoOfColumnToshow";
var conditionANDOR = "ANDORcondition";
var hyperLinkTag = "LinkParameters";
var hyperLinkUrl = "LinkURL";
var widgetGroupTag = "WidgetGroup";
var groupTag = "Group";
var uniqueField = "UniqueField";

var xhttp = null;
var xmlDoc = null;

//End

var browserIE = ($.browser.msie);
var browserFF = ($.browser.mozilla);
var browserSafari = ($.browser.webkit);
var browserOpera = ($.browser.opera);
var browserChrome = ($.browser.chrome);
var version = ($.browser.version);

var guiFileName = "PatientEMRGUI.xml";

if (browserIE == true && (version == "8.0" || version == "9.0")) {
	xhttp = new ActiveXObject("Msxml2.XMLHTTP");
	xmlDoc = new ActiveXObject("microsoft.xmldom");
	xhttp.open("GET", guiFileName, false);
	xhttp.send("");
	xmlDoc.loadXML(xhttp.responseText);

} else {

	if (window.XMLHttpRequest) {
		xhttp = new XMLHttpRequest();
	} else {
		// for older IE 5/6
		xhttp = new ActiveXObject("Microsoft.XMLHTTP");
		xmlDoc = new ActiveXObject("microsoft.xmldom");

		if (typeof xhttp == "undefined") {
			xhttp = new ActiveXObject("Msxml2.XMLHTTP");
			xmlDoc = new ActiveXObject("microsoft.xmldom");
		}
	}

	xhttp.open("GET", guiFileName, false);
	xhttp.send("");

	if (window.XMLHttpRequest)
		xmlDoc = xhttp.responseXML;
	else
		xmlDoc.loadXML(xhttp.responseText);
}

currentSelc = "PatientEMR";
var moduleObj = xmlDoc.getElementsByTagName(moduleTag);
var webserviceObj = xmlDoc.getElementsByTagName(webserviceTag);
var urlObj = webserviceObj[0].getElementsByTagName(urlTag);
var outputobject = xmlDoc.getElementsByTagName(outputTag);
var hyperLinkTagObj = xmlDoc.getElementsByTagName(hyperLinkTag);
var tagObj = outputobject[0].getElementsByTagName(labelTag);
var tagsize = outputobject[0].getElementsByTagName(noFieldsTag)[0].childNodes[0].nodeValue;
var styleObj = outputobject[0].getElementsByTagName(stylesTag);
var displaytypeObj = outputobject[0].getElementsByTagName(typeTag);
var noOfColumnObj = outputobject[0].getElementsByTagName(noOfColumnTag);
var conditionANDORObj = outputobject[0].getElementsByTagName(conditionANDOR);
var noOfColumnToshowObj = outputobject[0]
		.getElementsByTagName(noOfColumnToshow);
var resultStyleObj = styleObj[0].getElementsByTagName(resultWidgetTag);
var rstyleObj = resultStyleObj[0].getElementsByTagName(styleTag);
var stylee = rstyleObj[0].childNodes[0].nodeValue;
var widgetGroupObj = moduleObj[0].getElementsByTagName(widgetGroupTag)[0]
		.getElementsByTagName(groupTag);
var uniqFieldObj = webserviceObj[0].getElementsByTagName(uniqueField);

//Tag value for comparion
var pagerWidgetValue = "PagerWidget";
var seqTagName = "seq";
var visibilityTagName = "visibility";
var visibility_true = "true";
var repeat = "repeat";
var repeatTrue = "true";

function loadData(type) {
	searchtype = type;
	currentSelc = type;
	
	if (currentSelc == "PatientEMR") {
		$('#PatientEMRMenuList').prev().css("background-color", "#5690B6");
	}

	$("#displayValues").empty();
	$('.menuFilterulContentDiv').empty();

	if (type == "PatientEMR") {

		guiFileName = "PatientEMRGUI.xml";
	}

	xhttp.open("GET", guiFileName, false);
	xhttp.send("");

	if (browserIE == true)
		xmlDoc.loadXML(xhttp.responseText);
	else if (window.XMLHttpRequest)
		xmlDoc = xhttp.responseXML;
	else
		xmlDoc.loadXML(xhttp.responseText);

	moduleObj = xmlDoc.getElementsByTagName(moduleTag);
	webserviceObj = xmlDoc.getElementsByTagName(webserviceTag);
	urlObj = webserviceObj[0].getElementsByTagName(urlTag);
	outputobject = xmlDoc.getElementsByTagName(outputTag);
	hyperLinkTagObj = xmlDoc.getElementsByTagName(hyperLinkTag);
	tagObj = outputobject[0].getElementsByTagName(labelTag);
	tagsize = outputobject[0].getElementsByTagName(noFieldsTag)[0].childNodes[0].nodeValue;
	styleObj = outputobject[0].getElementsByTagName(stylesTag);
	displaytypeObj = outputobject[0].getElementsByTagName(typeTag);
	noOfColumnObj = outputobject[0].getElementsByTagName(noOfColumnTag);
	conditionANDORObj = outputobject[0].getElementsByTagName(conditionANDOR);
	noOfColumnToshowObj = outputobject[0]
			.getElementsByTagName(noOfColumnToshow);
	resultStyleObj = styleObj[0].getElementsByTagName(resultWidgetTag);
	rstyleObj = resultStyleObj[0].getElementsByTagName(styleTag);
	stylee = rstyleObj[0].childNodes[0].nodeValue;
	widgetGroupObj = moduleObj[0].getElementsByTagName(widgetGroupTag)[0]
			.getElementsByTagName(groupTag);
	uniqFieldObj = webserviceObj[0].getElementsByTagName(uniqueField);
	loadDefaultValues();
	loadurl(ipvalue + urlObj[0].childNodes[0].nodeValue, moduleObj,
			webserviceObj);
}

function loadDefaultValues() {
	getStyleInfo();
	var groupLen = widgetGroupObj.length;
	var ulStr = "";
	for (var i = 0; i < groupLen; i++) {
		ulStr += "<h3>&nbsp;&nbsp;&nbsp;&nbsp;"
				+ widgetGroupObj[i].childNodes[0].nodeValue
				+ "</h3><div class='accrodionDiv' ><ul class='groupUL' id='"
				+ widgetGroupObj[i].attributes[0].nodeValue + "'  ></ul></div>";
	}
	$('#singleIndexDiv').html(ulStr);

	$("#printDialog").dialog({
		modal : false,
		autoOpen : false,
		title : 'Print Search Result',
		position : 'center',
		resizable : false,
		width : '35%',
		height : 'auto',
		buttons : {
			"Close" : function() {
				$(this).dialog("close");
			},
			"Print" : function() {
				doPrint();
			}
		}
	});
	$('#printDialog :text').mask("?999");
	$('#printDialog :text').attr("disabled", true);
	$('#printDialog :radio').click(function() {
		var selection = $('#printDialog :radio:checked').attr("id");
		if (selection == "all") {
			$('#printDialog :text').attr("disabled", true);
		} else {
			$('#printDialog :text').attr("disabled", false);
		}
	});

	$("#exportDialog").dialog({
		modal : true,
		autoOpen : false,
		title : 'Export Search Result',
		position : 'center',
		resizable : false,
		width : '35%',
		height : 'auto',
		buttons : {
			"Close" : function() {
				$(this).dialog("close");
			},
			"Export" : function() {
				doExport();
			}
		}
	});
	$('#exportDialog :text').mask("?999");
	$('#exportDialog :text').attr("disabled", true);
	$('#exportDialog :radio').click(function() {
		var selection = $('#exportDialog :radio:checked').attr("id");
		if (selection == "exportAll") {
			$('#exportDialog :text').attr("disabled", true);
		} else {
			$('#exportDialog :text').attr("disabled", false);
		}
	});
}