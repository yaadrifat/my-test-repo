jQuery.expr[':'].contains = function(a, i, m) { 
	  	  return jQuery(a).text().toUpperCase().indexOf(m[3].toUpperCase()) >= 0; 
		};
		
	var qsavedvalues=[];
	var fqsavedvalues=[];
	var saveindex=0;
	var gloqval=[];
	var resultType=[];
	var saveorarray=[];
	var saveqarray=[];
	var saveIndexName=[];
	var selcDocForComp = {};
/*
	var studyArray = ['studyNumber_tense','studyEnteredBy_tense','studyPi_tense','studyCoordinator_tense','studyMajAuth_tense','studyTitle_tense','studyObjective_tense','studySummary_tense','studyAgentDevice_tense','studyDivision_tense','studyTarea_tense','studyDiseaseSite_tense','studyIcdcode1_tense','studyIcdcode2_tense','studyNatsampsize_tense','studyDuration_tense','studyDuratnUnit_tense','studyEstBeginDate_tense','studyPhase_tense','studyResearchType_tense','studyType_tense','studyLinkedTo_tense','studyBlinding_tense','studyRandom_tense','studySponsor_tense','sponsorContact_tense','studyInfo_tense','studyKeywrds_tense','fkAccount_tense','studyCreatedOn_tense','studyLastModifiedBy_tense','studyLastModifiedDate_tense','studyCreator_tense','sponsorDd_tense','studySponsorid_tense','studyActualdt_tense','studyInvindFlag_tense','studyStartDate_tense','studyEndDate_tense','sstatSiteName_tense','sstatStudyStatus_tense','sstatValidFrom_tense','sstatValidUntil_tense','sstatNotes_tense','sstatDocumntdBy_tense','sstatEndDateSystem_tense','sstatCreator_tense','sstatCreatedOn_tense','sstatLastModifiedBy_tense','sstatLastModifiedDate_tense','sstatCurrentStat_tense'];
	var patientStatArray = ['pstatStatus_tense','pstatReason_tense','pstatStatDate_tense','pstatNotes_tense','pstatPatStudId_tense','pstatAssignedTo_tense','pstatPhysician_tense','pstatTreatLocat_tense','pstatEvalFlag_tense','pstatEvalStat_tense','pstatInevalStat_tense','pstatSurvivalStat_tense','pstatRandomNum_tense','pstatEnrolledBy_tense','pstatEnrollSite_tense','pstatNextFuDate_tense','pstatIcVersNum_tense','pstatScreenNum_tense','pstatScreenBy_tense','pstatScreenOutcome_tense','creator_tense','lastModifiedBy_tense','lastModifiedDate_tense','createdOn_tense','pstatCurrentStat_tense','studyNumber_tense','studyPi_tense','studyTitle_tense','studyAgentDevice_tense','studyDivision_tense','studyTarea_tense','studyDiseaseSite_tense','studyActualdt_tense','ptdemGender_tense','ptdemMarstat_tense','ptdemBloodgrp_tense','ptdemPriEthny_tense','ptdemPriRace_tense','ptdemAddEthny_tense','ptdemAddRace_tense','ptdemState_tense','ptdemCountry_tense'];
	var patientScheduleArray = ['patStudyId_tense','ptschVisit_tense','ptschSugschDate_tense','ptschActschDate_tense','ptschEvent_tense','ptschEventStat_tense','creator_tense','lastModifiedBy_tense','lastModifiedDate_tense','createdOn_tense','eventstatDate_tense','eventstatNotes_tense','patprotStat_tense','fkSiteEnrolling_tense','eventSequence_tense','serviceSiteId_tense','facilityId_tense','coverageType_tense','calendarName_tense','ptschVisitWin_tense','eventWindowStart_tense','eventWindowEnd_tense','visitName_tense','siteName_tense','outOfWindow_tense','studyNumber_tense','studyPi_tense','studyTitle_tense','studyAgentDevice_tense','studyDivision_tense','studyTarea_tense','studyDiseaseSite_tense','studyActualdt_tense'];
	var patientQueriesArray = ['patientId_tense','patientStudyId_tense','studyNumber_tense','formName_tense','formsecName_tense','field_tense','queryType_tense','querydesc_tense','queryNotes_tense','queryStatus_tense','queryCreator_tense','queryCreatedOn_tense','patformsFilldate_tense','fieldId_tense','fkSiteEnrolling_tense','enrollingSiteName_tense','queryStatusDate_tense','studyPi_tense','studyTitle_tense','studyAgentDevice_tense','studyDivision_tense','studyTarea_tense','studyDiseaseSite_tense','studyActualdt_tense'];
	var PatientFacilitiesArray = ['patientId_tense','siteName_tense','patFacilityId_tense','regDate_tense','provider_tense','specialtyGroup_tense','facilityAccess_tense','facilityDefault_tense','createdOn_tense','lastModifiedDate_tense','creator_tense','lastModifiedBy_tense'];
	var AccrualBySummaryArray = ['pstatPatStudId_tense','pstatAssignedTo_tense','pstatPhysician_tense','pstatTreatLocat_tense','pstatEvalFlag_tense','pstatEvalStat_tense','pstatInevalStat_tense','pstatSurvivalStat_tense','pstatRandomNum_tense','pstatEnrolledBy_tense','pstatEnrollSite_tense','creator_tense','lastModifiedBy_tense','lastModifiedDate_tense','createdOn_tense','patprotEnroldt_tense','patprotTreatingorg_tense','ptdemPatId_tense','ptdemSurvivstat_tense','ptdemGender_tense','ptdemMarstat_tense','ptdemBloodgrp_tense','ptdemPriEthny_tense','ptdemPriRace_tense','ptdemAddEthny_tense','ptdemAddRace_tense','ptdemState_tense','ptdemCountry_tense','ptdemPriSite_tense','ptdemEmploy_tense','ptdemEducation_tense','ptdemNotes_tense','ptdemCreator_tense','ptdemLastModifiedBy_tense','ptdemLastModifiedDate_tense','ptdemCreatedOn_tense','ptdemPhyother_tense','ptdemSplaccess_tense','ptdemRegby_tense','ptdemRegdateYr_tense','studyNumber_tense','studyEnteredBy_tense','studyPi_tense','studyCoordinator_tense','studyMajAuth_tense','studyTitle_tense','studyObjective_tense','studySummary_tense','studyAgentDevice_tense','studyDivision_tense','studyTarea_tense','studyDiseaseSite_tense','studyIcdcode1_tense','studyIcdcode2_tense','studyNatsampsize_tense','studyDuration_tense','studyDuratnUnit_tense','studyEstBeginDate_tense','studyPhase_tense','studyResearchType_tense','studyType_tense','studyLinkedTo_tense','studyBlinding_tense','studyRandom_tense','studySponsor_tense','sponsorContact_tense','studyInfo_tense','studyKeywrds_tense','studyCreatedOn_tense','studyLastModifiedBy_tense','studyLastModifiedDate_tense','studyCreator_tense','sponsorDd_tense','studySponsorid_tense','studyActualdt_tense','studyInvindFlag_tense'];

*/
	function getStyleInfo(){
		 //alert("getstyleInfo");
			var stylrObj = styleObj[0].getElementsByTagName(styleTag);
			//var hstyleObj = headerStyleObj[0].getElementsByTagName(styleTag);
			//var headerStyle = hstyleObj[0].childNodes[0].nodeValue;
			for(i=0;i<stylrObj.length;i++){
					var divid = stylrObj[i].attributes[0].nodeValue;
					//alert(divid);
				if(document.getElementById(divid) != null && typeof(document.getElementById(divid)) != 'undefined'){
						document.getElementById(divid).className = 	stylrObj[i].childNodes[0].nodeValue;
			   }
			}
		}



function cleartext(){
	if($('#query').val()[0] == ' ')
	{
		$('#query').val('');
	}	
}
function loaddata(columToShowArr, urlString){
	/*
	var len = urlString.length;
	if( urlString[(len-1)]=='/')
		urlString = urlString.substring(0,(len-1));
	var urlArray = urlString.split("/");
	len = urlArray.length;
	var lastString = urlArray[(len-1)];
	var secondLastString = urlArray[(len-2)];
	var coreName = "";
	if(lastString.toLowerCase() == "select")
		coreName = secondLastString;
	else
		coreName = lastString;

	if(coreName == "study")
	{
		hideMenuValues(patientStatArray);
		hideMenuValues(patientScheduleArray);
		hideMenuValues(patientQueriesArray);
		hideMenuValues(PatientFacilitiesArray);
		hideMenuValues(AccrualBySummaryArray);
	}
	else 
	if(coreName == "patientStat")
	{
		hideMenuValues(studyArray);
		hideMenuValues(patientScheduleArray);
		hideMenuValues(patientQueriesArray);
		hideMenuValues(PatientFacilitiesArray);
		hideMenuValues(AccrualBySummaryArray);
	}
	else 
	if(coreName == "patientSchedule")
	{
		hideMenuValues(studyArray);
		hideMenuValues(patientStatArray);
		hideMenuValues(patientQueriesArray);
		hideMenuValues(PatientFacilitiesArray);
		hideMenuValues(AccrualBySummaryArray);
	}
	else 
	if(coreName == "patientQueries")
	{
		hideMenuValues(studyArray);
		hideMenuValues(patientStatArray);
		hideMenuValues(patientScheduleArray);
		hideMenuValues(PatientFacilitiesArray);
		hideMenuValues(AccrualBySummaryArray);
	}
	else 
	if(coreName == "PatientFacilities")
	{
		hideMenuValues(studyArray);
		hideMenuValues(patientStatArray);
		hideMenuValues(patientScheduleArray);
		hideMenuValues(patientQueriesArray);
		hideMenuValues(AccrualBySummaryArray);
	}
	else 
	if(coreName == "AccrualBySummary")
	{
		hideMenuValues(studyArray);
		hideMenuValues(patientStatArray);
		hideMenuValues(patientScheduleArray);
		hideMenuValues(patientQueriesArray);
		hideMenuValues(PatientFacilitiesArray);
	}

	*/
	$('#docs').columnManager({
		listTargetID:'targetall',
		onClass: 'advon',
		offClass: 'advoff',
		hideInList:[1] ,
		saveState: true,
		colsHidden: columToShowArr
	});

//create the clickmenu from the target
	$('#ulSelectColumn').clickMenu({onClick: function(){}});
	$('#wrap').click(function(){
		$('#topsearchResult').hide();
		$('#listPopUp').hide();
	});

}

 
 function loadAfterPagination(perpage){
	    var recordList="<li style='float:left'><select id='recorsPerPage' name='recorsPerPage' onchange='showRecords();' >"+
					   "<option value='10'>Show 10 results per page</option>"+
					   "<option value='25'>Show 25 results per page</option>"+
					   "<option value='50'>Show 50 results per page</option>"+
					   "<option value='75'>Show 75 results per page</option>"+
					   "<option value='100'>Show 100 results per page</option>"+
					   "</select></li>";
		$('#pager').append(recordList);
		if(perpage>10){
			$("option[value='"+recorsPerPage+"']").attr('selected', 'selected');
			}
	}