call mvn install:install-file -Dfile=ojdbc14.jar -DgroupId=com.oracle -DartifactId=ojdbc14 -Dversion=10.2.0 -Dpackaging=jar
call mvn install:install-file -Dfile=jboss-javaee.jar -DgroupId=jboss -DartifactId=jboss-javaee -Dversion=5.1.0.GA -Dpackaging=jar
call mvn install:install-file -Dfile=ejb3-persistence.jar -DgroupId=jboss -DartifactId=ejb3-persistence -Dversion=5.1.0.GA -Dpackaging=jar
call mvn install:install-file -Dfile=opencsv-2.3.0.jar -DgroupId=au.com.bytecode -DartifactId=opencsv -Dversion=2.3.0 -Dpackaging=jar
call mvn install:install-file -Dfile=mimeutil-2.1.1.jar -DgroupId=eu.medsea -DartifactId=mimeutil -Dversion=2.1.1 -Dpackaging=jar

pause
