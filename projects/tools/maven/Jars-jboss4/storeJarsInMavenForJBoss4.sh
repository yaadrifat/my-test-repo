#!/bin/sh
mvn install:install-file -Dfile=classes10G.jar -DgroupId=com.oracle -DartifactId=classes10G -Dversion=10.2.0 -Dpackaging=jar
mvn install:install-file -Dfile=parser.jar -DgroupId=com.sun -DartifactId=parser -Dversion=1.0.0 -Dpackaging=jar
mvn install:install-file -Dfile=commons-logging-1.0.4.jar -DgroupId=org.apache.commons -DartifactId=commons-logging -Dversion=1.0.4 -Dpackaging=jar
mvn install:install-file -Dfile=mule-1.3-rc4.jar -DgroupId=org.mule -DartifactId=mule -Dversion=1.3-rc4 -Dpackaging=jar
mvn install:install-file -Dfile=jboss-ejb3x.jar -DgroupId=org.jboss -DartifactId=jboss-ejb3x -Dversion=4.0 -Dpackaging=jar
mvn install:install-file -Dfile=jboss-j2ee.jar -DgroupId=org.jboss -DartifactId=jboss-j2ee -Dversion=4.0 -Dpackaging=jar
mvn install:install-file -Dfile=activation.jar -DgroupId=javax -DartifactId=activation -Dversion=1.0.0 -Dpackaging=jar
mvn install:install-file -Dfile=backport-util-concurrent-2.1.jar -DgroupId=edu.emory.mathcs.backport -DartifactId=backport-util-concurrent -Dversion=2.1 -Dpackaging=jar
mvn install:install-file -Dfile=xmlparserv2.jar -DgroupId=oracle -DartifactId=xmlparserv2 -Dversion=1.0.0 -Dpackaging=jar
mvn install:install-file -Dfile=ejb3-persistence.jar -DgroupId=jboss -DartifactId=ejb3-persistence -Dversion=4.0 -Dpackaging=jar
mvn install:install-file -Dfile=barcode4j.jar -DgroupId=org.krysalis -DartifactId=barcode4j -Dversion=2.0 -Dpackaging=jar
mvn install:install-file -Dfile=jconfig.jar -DgroupId=org.jconfig -DartifactId=jconfig -Dversion=1.0.0 -Dpackaging=jar
mvn install:install-file -Dfile=fop.jar -DgroupId=org.apache.fop -DartifactId=fop -Dversion=0.95 -Dpackaging=jar
mvn install:install-file -Dfile=cryptix32.jar -DgroupId=cryptix -DartifactId=cryptix32 -Dversion=1.0 -Dpackaging=jar
mvn install:install-file -Dfile=ldap.jar -DgroupId=com.novell -DartifactId=ldap -Dversion=1.0 -Dpackaging=jar
mvn install:install-file -Dfile=proxool-0.9.0RC3.jar -DgroupId=org.logicalcobwebs -DartifactId=proxool -Dversion=0.9.0RC3 -Dpackaging=jar
mvn install:install-file -Dfile=jdbcappender.jar -DgroupId=org.apache.log4j.jdbcplus -DartifactId=jdbcappender -Dversion=2.1.01 -Dpackaging=jar
mvn install:install-file -Dfile=jboss-system.jar -DgroupId=jboss -DartifactId=jboss-system -Dversion=4.0 -Dpackaging=jar
mvn install:install-file -Dfile=jboss-jmx.jar -DgroupId=jboss -DartifactId=jboss-jmx -Dversion=4.0 -Dpackaging=jar
mvn install:install-file -Dfile=parser.jar -DgroupId=com.sun -DartifactId=parser -Dversion=1.0.0 -Dpackaging=jar
mvn install:install-file -Dfile=json.jar -DgroupId=org.json -DartifactId=json -Dversion=50 -Dpackaging=jar

echo Hit return to exit
read a
