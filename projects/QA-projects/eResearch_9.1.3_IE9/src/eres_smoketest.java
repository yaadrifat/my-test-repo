/* 
Login
Add the organization
Add Groups
Add User (Non system User)
Add User (system User)
Add Account Links
Add Field
Add Forms
Add Events
Add Study
Add Portal
Add Calendar
Add Patient
Enroll Patient 
Add Patient Schedule
Add Patient Adverse Event 
Add Multiple Adverse Events
Add Patient Budget
Add Milestones
AdHoc Query
 */



import java.io.FileInputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
//import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.Select;
import org.apache.log4j.Logger;

public class eres_smoketest {
	private WebDriver driver;
	private FileInputStream fin;
	private StringBuffer verificationErrors = new StringBuffer();
	Properties prop =  new Properties();
	Logger ob = Logger.getLogger("dLogger");
	private int flag=0;
	
	@Before
	public void setUp() throws Exception {
		
		driver = new InternetExplorerDriver();
		
		fin=new FileInputStream("info_9.1.properties");
		prop.load(fin);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@Test
	public void eResearch() throws Exception {
			
		driver.get(prop.getProperty("testurl") + "/velos/jsp/ereslogin.jsp");
		driver.findElement(By.name("username")).clear();
		driver.findElement(By.name("username")).sendKeys(prop.getProperty("username"));
		driver.findElement(By.name("password")).clear();
		driver.findElement(By.name("password")).sendKeys(prop.getProperty("password"));
		Thread.sleep(2000);
		driver.findElement(By.name("password")).submit();
		
		Thread.sleep(18000);
		
		Set<String> winid = driver.getWindowHandles();
			
		Iterator iter =winid.iterator();
		String main  =  (String) iter.next();
		String tab = (String) iter.next();
			
		driver.switchTo().window(tab);
		Thread.sleep(2000);
		
		
		if (driver.getCurrentUrl().contains("loggedon_err.jsp")){
			flag=0;
			ob.debug("You are on " + driver.getTitle());
			//driver.findElement(By.id("submit_btn")).click();
			WebElement Submit_Btn = driver.findElement(By.id("submit_btn"));
			Thread.sleep(1000);
		    ((JavascriptExecutor) driver).executeScript("arguments[0].click();", Submit_Btn);
		    Thread.sleep(2000);
			ob.debug("Clicked submit Button");
			
			}
		else{
			flag=1;
			driver.findElement(By.id("homePage")).click();
			Thread.sleep(2000);
		}
		
		Thread.sleep(7000);		
		
		Set<String> winid1 = driver.getWindowHandles();
		
		Iterator iter1 =	winid1.iterator();
		String main1  =  (String) iter1.next();
		String tab1 = (String) iter1.next();
		
		driver.switchTo().window(tab1);
		Thread.sleep(3000);
	
		
// #######################################################################################################################################################################
// #Add Organization
// #################################Beginning Of Add Organization Block################################################################################################## 		

		ob.debug("-----------------------------------------------Adding a New Organization / Entering Organization Module-----------------------------------------------");
		
		//Thread.sleep(10000);
		//driver.get(prop.getProperty("testurl") + "/velos/jsp/sitebrowser.jsp?srcmenu=tdMenuBarItem2&selectedTab=1");
			
		//driver.findElement(By.id("homePage")).click();
		WebElement Homepg_cbu = driver.findElement(By.id("homePage"));
		Thread.sleep(3000);
	    ((JavascriptExecutor) driver).executeScript("arguments[0].click();", Homepg_cbu);
	    Thread.sleep(5000);
		//Thread.sleep(7000);
	
		ob.debug("You are on " + driver.getTitle()+ " page");	
		
		driver.findElement(By.linkText("Manage")).click();
		//Thread.sleep(2000);
		
		driver.findElement(By.linkText("OrganizationsQA")).click();
		Thread.sleep(2000);		
		
//		WebElement Click_Org = driver.findElement(By.linkText("OrganizationsQA"));
//		Thread.sleep(3000);
//	    ((JavascriptExecutor) driver).executeScript("arguments[0].click();", Click_Org);
//	    Thread.sleep(5000);
		
		ob.debug("You are on " + driver.getTitle()+ " page");	
		
		//Thread.sleep(10000);
		driver.findElement(By.linkText("ADD A NEW ORGANIZATION")).click();
				
		ob.debug("You are on " + driver.getTitle()+ " page");
		
		ob.debug("Adding a new Organization");
		
		//Thread.sleep(2000);
		driver.findElement(By.name("siteName")).sendKeys(prop.getProperty("orgname"));
		
		//Thread.sleep(2000);
		new Select(driver.findElement(By.name("siteType"))).selectByVisibleText("Hospital");
						
		//Thread.sleep(2000);
		driver.findElement(By.id("submit_btn")).click();
		
		ob.debug("Added a new Organization");
		
		//Thread.sleep(10000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on " + driver.getTitle()+ " page");
	
		//Verifying if the organization saved successfully

		ob.debug("***Checking if the Organization created above is saved or not***");
		
		//Thread.sleep(5000);
		//driver.get(prop.getProperty("testurl") + "/velos/jsp/sitebrowser.jsp?srcmenu=tdMenuBarItem2&selectedTab=1");

		driver.findElement(By.id("homePage")).click();
		Thread.sleep(2000);
	
		ob.debug("You are on " + driver.getTitle()+ " page");	
		
		driver.findElement(By.linkText("Manage")).click();
		//Thread.sleep(5000);
		
		driver.findElement(By.linkText("OrganizationsQA")).click();
		Thread.sleep(2000);
		
		ob.debug("You are on " + driver.getTitle()+ " page");
			
		//Thread.sleep(5000);
		List<WebElement> link = driver.findElements(By.tagName("a"));
		
		for(int i = 0;i<link.size();i++)
			{
				if (link.get(i).getText().equalsIgnoreCase(prop.getProperty("orgname")))
				{
					ob.debug("Organization is saved successfully - " + link.get(i).getText());
					flag=1;
				}	
			}
		if(flag==0)
		{
			ob.debug("Organization not saved, please try to save the organziation");
		}
		
		ob.debug("Exiting Organization Module");
				
		//Thread.sleep(5000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();	

		ob.debug("-----------------------------------------------Ending Organization Module");
		
//##########################################Ending Of Add Organization Block#############################################################################################
		//#Add group
//#########################################Beginning of Add Group Block##################################################################################################

		ob.debug("-----------------------------------------------Adding a New Group / Entering Group Module-----------------------------------------------------------");
		
		//Thread.sleep(10000);	
		//driver.get(prop.getProperty("testurl") + "/velos/jsp/groupbrowserpg.jsp?srcmenu=tdMenuBarItem2&selectedTab=2");
		
		flag=0;
		
		driver.findElement(By.id("homePage")).click();
		Thread.sleep(2000);
		
		ob.debug("You are on " + driver.getTitle()+ " page");	
		
		driver.findElement(By.linkText("Manage")).click();
		//Thread.sleep(2000);
		
		driver.findElement(By.linkText("Groups")).click();
		Thread.sleep(2000);
		
		ob.debug("You are on " + driver.getTitle()+" page");
		
		//Thread.sleep(10000);
		driver.findElement(By.linkText("ADD A NEW GROUP")).click();
		
		ob.debug("Adding group with name "+ prop.getProperty("groupname"));
		
		//Thread.sleep(2000);
		driver.findElement(By.name("groupName")).sendKeys(prop.getProperty("groupname"));
	
		//Thread.sleep(2000);
		driver.findElement(By.id("submit_btn")).click();
		
		ob.debug("Assigning Admin rights to group name " + prop.getProperty("groupname"));
		
		Thread.sleep(10000);
		
		driver.findElement(By.name("All")).click();
		
		//Thread.sleep(6000);
		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
		
		//Thread.sleep(2000);
		driver.findElement(By.id("submit_btn")).click();
						
		//Thread.sleep(20000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on "+ driver.getTitle()+" page");
				
		//Verifying if the group data is saved successfully

		//Thread.sleep(10000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on "+ driver.getTitle()+" page");
		
		//Thread.sleep(7000);	
		//driver.get(prop.getProperty("testurl") + "/velos/jsp/groupbrowserpg.jsp?srcmenu=tdMenuBarItem2&selectedTab=2");

		driver.findElement(By.id("homePage")).click();
		Thread.sleep(2000);
		
		ob.debug("You are on " + driver.getTitle()+ " page");	
		
		driver.findElement(By.linkText("Manage")).click();
		//Thread.sleep(2000);
		
		driver.findElement(By.linkText("Groups")).click();
		Thread.sleep(2000);
		
		ob.debug("Verifying group data was saved correctly");
		
		ob.debug("You are on "+ driver.getTitle()+" page");
		
		//Thread.sleep(5000);
		List<WebElement> link1 = driver.findElements(By.tagName("a"));
		
		for(int i = 0;i<link1.size();i++)
		{
			if (link1.get(i).getText().equalsIgnoreCase(prop.getProperty("groupname")))
			{
				ob.debug("Group saved successfully - " + link1.get(i).getText());
				flag=1;
			}	
		}
		if(flag==0) 
		{
			ob.debug("Group not saved successfully");
		}
		
		ob.debug("Exiting Group Module");
		
		//Thread.sleep(5000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on " + driver.getTitle()+" page");
		
		ob.debug("-----------------------------------------------Ending Group Module"); 
		
//############################################Ending Of Add Group Block#################################################################################################
//			#Add non-system user
//############################################Beginning Of Add Non-System User Block####################################################################################
		
		ob.debug("-----------------------------------------------Adding a New Non-System User / Entering User Module-------------------------------------------------");
		
		flag=0;
		
		//Thread.sleep(10000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();	
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		driver.findElement(By.linkText("Manage")).click();
		//Thread.sleep(2000);
		
		driver.findElement(By.linkText("Users")).click();
		//Thread.sleep(5000);
		
		ob.debug("Adding Non System User");
		
		//Thread.sleep(2000);		
		//driver.get(prop.getProperty("testurl") + "/velos/jsp/accountbrowser.jsp?srcmenu=tdMenuBarItem2&selectedTab=3");
				
		ob.debug("You are on " + driver.getTitle()+" page");
		
		Thread.sleep(5000);
		new Select(driver.findElement(By.name("cmbViewList"))).selectByVisibleText("Non System Users");
		
		//Thread.sleep(5000);		
		driver.findElement(By.xpath("//button[@type='submit']")).submit();

		//Thread.sleep(2000);
		driver.findElement(By.linkText("ADD A NEW NON-SYSTEM USER")).click();
		
		ob.debug("adding a non system user with first name as "+ prop.getProperty("nonfname"));
		
		//Thread.sleep(2000);
		driver.findElement(By.name("userFirstName")).sendKeys(prop.getProperty("nonfname"));
		
		//Thread.sleep(2000);
		driver.findElement(By.name("userLastName")).sendKeys(prop.getProperty("nonlname"));
		
		//Thread.sleep(2000);
		new Select(driver.findElement(By.name("accsites"))).selectByVisibleText(prop.getProperty("orgname"));
		
		//Thread.sleep(2000);
		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
		
		//Thread.sleep(2000);
		driver.findElement(By.id("submit_btn")).click();
		
		ob.debug("Added Non System User");
		
		//Thread.sleep(10000);
		//driver.get(prop.getProperty("testurl")+ "/velos/jsp/myHome.jsp?srcmenu=tdMenuBarItem1");
		
		ob.debug("You are on " + driver.getTitle()+" page");

		//verifying non system user saved correctly
		
		//Thread.sleep(5000);
		//driver.get(prop.getProperty("testurl")+ "/velos/jsp/myHome.jsp?srcmenu=tdMenuBarItem1");
		
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();	
		Thread.sleep(2000);
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		driver.findElement(By.linkText("Manage")).click();
		//Thread.sleep(2000);
		
		driver.findElement(By.linkText("Users")).click();
		//Thread.sleep(2000);
		
		ob.debug("You are on " + driver.getTitle()+" page");
		
		ob.debug("Verifying that non system user was added in user list");
		
		//Thread.sleep(5000);		
		//driver.get(prop.getProperty("testurl") + "/velos/jsp/accountbrowser.jsp?srcmenu=tdMenuBarItem2&selectedTab=3");		
				
		ob.debug("You are on " + driver.getTitle()+" page");
		
		//Thread.sleep(2000);
		new Select(driver.findElement(By.name("cmbViewList"))).selectByVisibleText("Non System Users");
		
		//Thread.sleep(2000);
		driver.findElement(By.xpath("//button[@type='submit']")).submit();
		
		//Thread.sleep(2000);
		driver.findElement(By.name("firstName")).sendKeys(prop.getProperty("nonfname"));
		
		
		
		String s=new String();
		
		s=prop.getProperty("nonfname") + " " +prop.getProperty("nonlname");
		
		//Thread.sleep(5000);
		List<WebElement> link2 = driver.findElements(By.tagName("a"));
		
		for(int i = 0;i<link2.size();i++)
		{
			if (link2.get(i).getText().equalsIgnoreCase(s))
			{
					ob.debug("Non Active User Saved Successfully - " + link2.get(i).getText());
					flag=1;
			}	
		}	
		if (flag==0)
		{
			ob.debug("Non System User is not added in eResearch, Please check again");
		}
		
		ob.debug("Exiting Non System User");
		
		//Thread.sleep(5000);
		//driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();	
		
		ob.debug("----------------------------------------------------------------------------------------------------------");
	
//#############################################Ending Of The Non-System User Block########################################################################################
//			#Add system user
//#############################################Beginning Of The System User Block#################################################################################################
		
		ob.debug("-----------------------------------------------Adding a New System User -----------------------------------------------------------");

		flag=0;
		//Thread.sleep(15000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		ob.debug("Adding new System User");
		
		//Thread.sleep(2000);
		//driver.get(prop.getProperty("testurl") + "/velos/jsp/accountbrowser.jsp?srcmenu=tdMenuBarItem2&selectedTab=3");
		
		driver.findElement(By.linkText("Manage")).click();
		//Thread.sleep(2000);
		
		driver.findElement(By.linkText("Users")).click();
		Thread.sleep(2000);
		
		ob.debug("You are on " + driver.getTitle()+" page");
		
		//Thread.sleep(5000);
		driver.findElement(By.linkText("ADD A NEW USER")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		ob.debug("adding a system user with first name as "+ prop.getProperty("sysfname"));
		
		//Thread.sleep(2000);
		driver.findElement(By.name("userFirstName")).sendKeys(prop.getProperty("sysfname"));
		
		//Thread.sleep(2000);		
		driver.findElement(By.name("userLastName")).sendKeys(prop.getProperty("syslname"));
		
		//Thread.sleep(2000);
		driver.findElement(By.name("userEmail")).sendKeys(prop.getProperty("emailid"));
		
		//Thread.sleep(2000);
		new Select(driver.findElement(By.id("timeZone"))).selectByVisibleText(prop.getProperty("timezone"));
		
		//Thread.sleep(2000);
		new Select(driver.findElement(By.name("accsites"))).selectByVisibleText(prop.getProperty("orgname"));
		
		//Thread.sleep(2000);
		new Select(driver.findElement(By.name("usrgrps"))).selectByVisibleText(prop.getProperty("groupname"));
		
		//Thread.sleep(2000);
		driver.findElement(By.name("userLogin")).sendKeys(prop.getProperty("userlogin"));
		
		//Thread.sleep(2000);
		driver.findElement(By.name("userPassword")).sendKeys(prop.getProperty("syspass"));
		
		//Thread.sleep(2000);
		driver.findElement(By.name("userConfirmPassword")).sendKeys(prop.getProperty("syspass"));
		
		//Thread.sleep(2000);
		driver.findElement(By.name("userESign")).sendKeys(prop.getProperty("esign"));
		
		//Thread.sleep(2000);
		driver.findElement(By.name("userConfirmESign")).sendKeys(prop.getProperty("esign"));
		
		//Thread.sleep(2000);
		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
		
		//Thread.sleep(2000);
		driver.findElement(By.id("submit_btn")).click();
	
		ob.debug("Added a new user with first name " + prop.getProperty("sysfname"));
		
		//Thread.sleep(5000);
		//driver.get(prop.getProperty("testurl")+ "/velos/jsp/myHome.jsp?srcmenu=tdMenuBarItem1");
		
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		Thread.sleep(2000);
		//Verifying if the Active User Saved successfully
		
		ob.debug("Verifying data saved for System user");
		
		//Thread.sleep(2000);
		//driver.get(prop.getProperty("testurl") + "/velos/jsp/accountbrowser.jsp?srcmenu=tdMenuBarItem2&selectedTab=3");
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		//Thread.sleep(2000);
		//driver.get(prop.getProperty("testurl") + "/velos/jsp/accountbrowser.jsp?srcmenu=tdMenuBarItem2&selectedTab=3");
		
		driver.findElement(By.linkText("Manage")).click();
		//Thread.sleep(2000);
		
		driver.findElement(By.linkText("Users")).click();
		Thread.sleep(2000);
			
		ob.debug("You are on " + driver.getTitle()+" page");
		
		//Thread.sleep(2000);
		driver.findElement(By.name("firstName")).sendKeys(prop.getProperty("sysfname"));
		
		String s1=new String();
		
		s1=prop.getProperty("sysfname") + " " +prop.getProperty("syslname");		
				
		//Thread.sleep(5000);
		List<WebElement> link11 = driver.findElements(By.tagName("a"));
	
		for(int i = 0;i<link11.size();i++)
		{
			if (link11.get(i).getText().equalsIgnoreCase(s1))
			{
				ob.debug("System user saved successfully - "+link11.get(i).getText());
				flag=1;
			}	
		}	
		if(flag==0)
		{
			ob.debug("System User not saved successfully");
		}
		
		//Thread.sleep(5000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
	
		ob.debug("-----------------------------------------------Ending User Module");
		
//########################################Beginning Of Account Links################################################################################################

		ob.debug("-----------------------------------------------Adding Account Links Module-------------------------------------------------");
		
		flag=0;
		
		Thread.sleep(2000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();	
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		ob.debug("Adding Account Links");
		
		//Thread.sleep(2000);		
		//driver.get(prop.getProperty("testurl") + "/velos/jsp/accountlinkbrowser.jsp?srcmenu=tdMenuBarItem2&selectedTab=4"); 
			
		driver.findElement(By.linkText("Manage")).click();
		//Thread.sleep(2000);
		
		driver.findElement(By.linkText("Links")).click();
		Thread.sleep(2000);
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		Thread.sleep(5000);
		driver.findElement(By.linkText("ADD A NEW LINK")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		ob.debug("adding a link as "+ prop.getProperty("lnkURI"));
		
		//Thread.sleep(2000);
		driver.findElement(By.name("lnkURI")).sendKeys(prop.getProperty("lnkURL"));
		
		Thread.sleep(5000);
		driver.findElement(By.name("lnk_type")).sendKeys(prop.getProperty("lnk_type"));
		
		Thread.sleep(5000);
		driver.findElement(By.name("lnkDesc")).sendKeys(prop.getProperty("lnkDesc"));
		
		//Thread.sleep(5000);
		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
		
		Thread.sleep(2000);
		driver.findElement(By.id("submit_btn")).click();
		Thread.sleep(5000);
		ob.debug("Exiting Account link");
						
		Thread.sleep(2000);
		//driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		//driver.findElement(By.id("homePage")).click();
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();	
		Thread.sleep(2000);
		
		ob.debug("----------------------------------------------------------------------------------------------------------");
	
//########################################End OF Add Account Links##########################################################################################################
//		#Add fields
//#######################################Beginning Of The Add Fields Block################################################################################################
		
		ob.debug("-----------------------------------------------Entering Library >> Field Library Module-------------------------------------------------");
	
		flag=0;
		//Thread.sleep(15000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		ob.debug("******Entering Field Library Module******");
		
		//Thread.sleep(2000);
//		driver.get(prop.getProperty("testurl") + "/velos/jsp/fieldLibraryBrowser.jsp?srcmenu=tdmenubaritem4&mode=initial&selectedTab=4");
//		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		driver.findElement(By.linkText("Libraries")).click();
		//Thread.sleep(2000);
		
		driver.findElement(By.linkText("Search")).click();
		//Thread.sleep(2000);
		
		driver.findElement(By.linkText("Field Library")).click();
		Thread.sleep(2000);
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		ob.debug("******Adding a New Field Category******");
		
		//Thread.sleep(5000);
		driver.findElement(By.cssSelector("img[title=\"New Field Category\"]")).click();
		
		Thread.sleep(4000);
		winid = driver.getWindowHandles();
		iter =winid.iterator();
		main  =  (String) iter.next();
		tab = (String) iter.next();
		String tab11 = (String) iter.next();
		
		driver.switchTo().window(tab11);
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		ob.debug("Adding a new field category with name " + prop.getProperty("fieldcategory"));
		
		Thread.sleep(9000);
		driver.findElement(By.name("categoryName")).sendKeys(prop.getProperty("fieldcategory"));
		//driver.findElement(By.xpath(".//*[@id='fieldcatform']/table/tbody/tr[1]/td[2]/input")).sendKeys(prop.getProperty("fieldcategory"));

		//Thread.sleep(2000);
		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
		
		//Thread.sleep(2000);
		driver.findElement(By.id("submit_btn")).click();
		
		//Thread.sleep(4000);
		
		driver.switchTo().window(tab);
			
		ob.debug("added a field category with " + prop.getProperty("fieldcategory"));
		
		//Thread.sleep(15000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
				
		//creating a new number field

		ob.debug("******Adding a new edit field with category created above******");
		
		//Thread.sleep(5000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		//Thread.sleep(5000);
		//driver.get(prop.getProperty("testurl") + "/velos/jsp/fieldLibraryBrowser.jsp?srcmenu=tdmenubaritem4&mode=initial&selectedTab=4");
		
		driver.findElement(By.linkText("Libraries")).click();
		//Thread.sleep(2000);
		
		driver.findElement(By.linkText("Search")).click();
		//Thread.sleep(2000);
		
		driver.findElement(By.linkText("Field Library")).click();
		Thread.sleep(2000);	
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		//Thread.sleep(2000);
		driver.findElement(By.linkText("ADD EDIT FIELD")).click();
		
		ob.debug("Adding a field named " + prop.getProperty("fieldname"));
		
		Thread.sleep(4000);
		winid = driver.getWindowHandles();
		iter =winid.iterator();
		main  =  (String) iter.next();
		tab = (String) iter.next();
		String tab2 = (String) iter.next();
		
		//Thread.sleep(2000);
		driver.switchTo().window(tab2);
		
		ob.debug("adding mandatory fields data");
		
		Thread.sleep(4000);
		new Select(driver.findElement(By.name("category"))).selectByVisibleText(prop.getProperty("fieldcategory"));
		
		System.out.println("abc");
		
		//Thread.sleep(2000);
		driver.findElement(By.name("name")).sendKeys(prop.getProperty("fieldname"));
		
		//Thread.sleep(2000);
		driver.findElement(By.name("uniqueId")).sendKeys(prop.getProperty("fieldname")+"_id");
		
		//Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@name='editBoxType' and @value='EN']")).click();
		
		//Thread.sleep(2000);
		driver.findElement(By.name("overRideRange")).click();
		
		//Thread.sleep(2000);
		new Select(driver.findElement(By.name("greaterless1"))).selectByVisibleText("Greater Than");
		
		//Thread.sleep(2000);
		driver.findElement(By.name("val1")).clear();
		driver.findElement(By.name("val1")).sendKeys("1");
		
		//Thread.sleep(2000);
		new Select(driver.findElement(By.name("andor"))).selectByVisibleText("And");
		
		//Thread.sleep(2000);
		new Select(driver.findElement(By.name("greaterless2"))).selectByVisibleText("Less Than");
		
		//Thread.sleep(2000);
		driver.findElement(By.name("val2")).clear();
		driver.findElement(By.name("val2")).sendKeys("10.8");
		
		//Thread.sleep(2000);
		driver.findElement(By.id("eSign")).clear();
		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
		
		//Thread.sleep(2000);
		driver.findElement(By.id("submit_btn")).click();
		
		//Thread.sleep(2000);
		driver.switchTo().window(tab);
		
		ob.debug("added new field with name " + prop.getProperty("fieldname"));
	
		//creating a new text field
		
		//Thread.sleep(10000);
		driver.findElement(By.linkText("ADD EDIT FIELD")).click();
		ob.debug("Adding a field named " + prop.getProperty("fieldname1"));
		
		Thread.sleep(4000);
		
		winid = driver.getWindowHandles();
		iter =winid.iterator();
		main  =  (String) iter.next();
		tab = (String) iter.next();
		String tab250 = (String) iter.next();
		
		Thread.sleep(2000);
		driver.switchTo().window(tab250);
		
		Thread.sleep(4000);
		new Select(driver.findElement(By.name("category"))).selectByVisibleText(prop.getProperty("fieldcategory"));
		
		//Thread.sleep(2000);
		driver.findElement(By.name("name")).sendKeys(prop.getProperty("fieldname1"));
		
		//Thread.sleep(2000);
		driver.findElement(By.name("uniqueId")).sendKeys(prop.getProperty("fieldname1")+"_id");
		
		//Thread.sleep(2000);
		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
		
		//Thread.sleep(2000);
		driver.findElement(By.id("submit_btn")).click();
		
		//Thread.sleep(2000);
		driver.switchTo().window(tab);
		
		//add a new Date field
		
		//Thread.sleep(10000);
		driver.findElement(By.linkText("ADD EDIT FIELD")).click();
		
		Thread.sleep(4000);
		winid = driver.getWindowHandles();
		iter =winid.iterator();
		main  =  (String) iter.next();
		tab = (String) iter.next();
		String tab252 = (String) iter.next();
		
		//Thread.sleep(2000);
		driver.switchTo().window(tab252);
		
		Thread.sleep(5000);
		new Select(driver.findElement(By.name("category"))).selectByVisibleText(prop.getProperty("fieldcategory"));
		
		Thread.sleep(4000);
		driver.findElement(By.name("name")).sendKeys(prop.getProperty("fieldname2"));
		
		Thread.sleep(2000);
		driver.findElement(By.name("uniqueId")).sendKeys(prop.getProperty("fieldname2")+"_id");
		
		//Thread.sleep(2000);
		driver.findElement(By.xpath("(//input[@name='editBoxType'])[3]")).click();
		
		//Thread.sleep(2000);
		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
		
		//Thread.sleep(2000);
		driver.findElement(By.id("submit_btn")).click();
		
		//Thread.sleep(5000);
		driver.switchTo().window(tab);
		
		//add a new CB field
		
		//Thread.sleep(10000);
		driver.findElement(By.linkText("ADD MULTIPLE CHOICE FIELD")).click();
		
		Thread.sleep(5000);
		winid = driver.getWindowHandles();
		iter =winid.iterator();
		main  =  (String) iter.next();
		tab = (String) iter.next();
		String tab253 = (String) iter.next();
		
		//Thread.sleep(2000);
		driver.switchTo().window(tab253);
		
		Thread.sleep(5000);
		new Select(driver.findElement(By.name("category"))).selectByVisibleText(prop.getProperty("fieldcategory"));
		
		Thread.sleep(2000);
		driver.findElement(By.name("name")).sendKeys(prop.getProperty("fieldname3"));
		
		Thread.sleep(2000);
		driver.findElement(By.name("uniqueId")).sendKeys(prop.getProperty("fieldname3")+"_id");
		
		//Thread.sleep(2000);
		driver.findElement(By.xpath("(//input[@name='editBoxType'])[2]")).click();
		
		//Thread.sleep(2000);
		driver.findElement(By.name("newTxtDispVal")).sendKeys(prop.getProperty("Value1"));
		
		//Thread.sleep(2000);
		driver.findElement(By.xpath("(//input[@name='newTxtDispVal'])[2]")).sendKeys(prop.getProperty("Value2"));
		
		//Thread.sleep(2000);
		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
		
		//Thread.sleep(2000);
		driver.findElement(By.id("submit_btn")).click();
		
		//Thread.sleep(2000);
		driver.switchTo().window(tab);
	
		//add a new DD Field
		
		//Thread.sleep(10000);
		driver.findElement(By.linkText("ADD MULTIPLE CHOICE FIELD")).click();
		
		Thread.sleep(4000);
		winid = driver.getWindowHandles();
		iter =winid.iterator();
		main  =  (String) iter.next();
		tab = (String) iter.next();
		String tab254 = (String) iter.next();
		
		//Thread.sleep(2000);
		driver.switchTo().window(tab254);
		
		Thread.sleep(5000);
		new Select(driver.findElement(By.name("category"))).selectByVisibleText(prop.getProperty("fieldcategory"));
		
		//Thread.sleep(2000);
		driver.findElement(By.name("name")).sendKeys(prop.getProperty("fieldname4"));
		
		//Thread.sleep(2000);
		driver.findElement(By.name("uniqueId")).sendKeys(prop.getProperty("fieldname4")+"_id");
		
		//Thread.sleep(2000);
		driver.findElement(By.name("newTxtDispVal")).sendKeys(prop.getProperty("Value1"));
		
		//Thread.sleep(2000);
		driver.findElement(By.xpath("(//input[@name='newTxtDispVal'])[2]")).sendKeys(prop.getProperty("Value2"));
		
		//Thread.sleep(2000);
		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
		
		//Thread.sleep(2000);
		driver.findElement(By.id("submit_btn")).click();
		
		//Thread.sleep(2000);
		driver.switchTo().window(tab);
		
		//add a new RD Field
		
		//Thread.sleep(10000);
		driver.findElement(By.linkText("ADD MULTIPLE CHOICE FIELD")).click();
		
		Thread.sleep(5000);
		winid = driver.getWindowHandles();
		iter =winid.iterator();
		main  =  (String) iter.next();
		tab = (String) iter.next();
		String tab255 = (String) iter.next();
		
		//Thread.sleep(2000);
		driver.switchTo().window(tab255);
		
		Thread.sleep(5000);
		new Select(driver.findElement(By.name("category"))).selectByVisibleText(prop.getProperty("fieldcategory"));
		
		//Thread.sleep(2000);
		driver.findElement(By.name("name")).sendKeys(prop.getProperty("fieldname5"));
		
		//Thread.sleep(2000);
		driver.findElement(By.name("uniqueId")).sendKeys(prop.getProperty("fieldname5")+"_id");
		
		//Thread.sleep(2000);
		driver.findElement(By.xpath("(//input[@name='editBoxType'])[3]")).click();
		
		//Thread.sleep(2000);
		driver.findElement(By.name("newTxtDispVal")).sendKeys(prop.getProperty("Value1"));
		
		//Thread.sleep(2000);
		driver.findElement(By.xpath("(//input[@name='newTxtDispVal'])[2]")).sendKeys(prop.getProperty("Value2"));
	
		//Thread.sleep(2000);
		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
		
		//Thread.sleep(2000);
		driver.findElement(By.id("submit_btn")).click();
		
		//Thread.sleep(2000);
		driver.switchTo().window(tab);
				
		//Thread.sleep(15000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");

		//Verifying if the data is created or not
		
		ob.debug("******Verfiying the field category saved******");
		
		//Thread.sleep(5000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
				
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		//Thread.sleep(2000);
		//driver.get(prop.getProperty("testurl") + "/velos/jsp/fieldLibraryBrowser.jsp?srcmenu=tdmenubaritem4&mode=initial&selectedTab=4");
		
		ob.debug("******Entering Field Library Module******");
		
		driver.findElement(By.linkText("Libraries")).click();
		//Thread.sleep(2000);
		
		driver.findElement(By.linkText("Search")).click();
		//Thread.sleep(2000);
		
		driver.findElement(By.linkText("Field Library")).click();
		Thread.sleep(2000);
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		//Thread.sleep(5000);
		new Select(driver.findElement(By.name("category"))).selectByVisibleText(prop.getProperty("fieldcategory"));
		
		//Thread.sleep(2000);
		driver.findElement(By.xpath("//button[@type='submit']")).submit();
				
		//Thread.sleep(5000);
		List<WebElement> link3 = driver.findElements(By.tagName("a"));
		
		for(int i = 0;i<link3.size();i++)
		{
			if (link3.get(i).getText().equalsIgnoreCase(prop.getProperty("fieldcategory")))
			{
				ob.debug("Field Category Saved Successfully - " + link3.get(i).getText());
				flag=1;
			}	
		}
			
		if(flag==0)
				{
			ob.debug("Field Category was not saved successfully");
		}
		flag=0;
		
		for(int i = 0;i<link3.size();i++)
		{
			if (link3.get(i).getText().equalsIgnoreCase(prop.getProperty("fieldname")))
			{
				ob.debug("Field Saved Successfully - " + link3.get(i).getText());
				flag=1;
			}	
		}
		if(flag==0)
		{
			ob.debug("Field not saved, please check again");
		}
		flag=0;
		for(int i = 0;i<link3.size();i++)
		{
			if (link3.get(i).getText().equalsIgnoreCase(prop.getProperty("fieldname1")))
			{
				ob.debug("Field Saved Successfully - " + link3.get(i).getText());
				flag=1;
			}	
		}
		if(flag==0)
		{
			ob.debug("Field not saved, please check again");
		}
		flag=0;
		for(int i = 0;i<link3.size();i++)
		{
			if (link3.get(i).getText().equalsIgnoreCase(prop.getProperty("fieldname2")))
			{
				ob.debug("Field Saved Successfully - " + link3.get(i).getText());
				flag=1;
			}	
		}
		if(flag==0)
		{
			ob.debug("Field not saved, please check again");
		}
		flag=0;
		for(int i = 0;i<link3.size();i++)
		{
			if (link3.get(i).getText().equalsIgnoreCase(prop.getProperty("fieldname3")))
			{
				ob.debug("Field Saved Successfully - " + link3.get(i).getText());
				flag=1;
			}	
		}
		if(flag==0)
		{
			ob.debug("Field not saved, please check again");
		}
		flag=0;
		for(int i = 0;i<link3.size();i++)
		{
			if (link3.get(i).getText().equalsIgnoreCase(prop.getProperty("fieldname4")))
			{
				ob.debug("Field Saved Successfully - " + link3.get(i).getText());
				flag=1;
			}	
		}
		if(flag==0)
		{
			ob.debug("Field not saved, please check again");
		}
		flag=0;
		for(int i = 0;i<link3.size();i++)
		{
			if (link3.get(i).getText().equalsIgnoreCase(prop.getProperty("fieldname5")))
			{
				ob.debug("Field Saved Successfully - " + link3.get(i).getText());
				flag=1;
			}	
		}
		if(flag==0)
		{
			ob.debug("Field not saved, please check again");
		}
		//Thread.sleep(5000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		ob.debug("-----------------------------------------------Ending Field Library Module");
		
//##########################################Ending Of The Add Fields Block############################################################################################	
//		#Add forms
//#########################################Beginning Of The Add Forms Block############################################################################################

		ob.debug("-----------------------------------------------Entering Library >> Form Library Module-------------------------------------------------");
	
		flag=0;

		//Thread.sleep(15000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		ob.debug("*****Form Library Module*****");
		
		//Thread.sleep(2000);
		//driver.get(prop.getProperty("testurl") + "/velos/jsp/formLibraryBrowser.jsp?srcmenu=tdmenubaritem4&selectedTab=3");
		
		driver.findElement(By.linkText("Libraries")).click();
		//Thread.sleep(2000);
		
		driver.findElement(By.linkText("Search")).click();
		//Thread.sleep(2000);
		
		driver.findElement(By.linkText("Form Library")).click();
		Thread.sleep(2000);
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		//Thread.sleep(5000);
		driver.findElement(By.cssSelector("img[title=\"New Form Category\"]")).click();
		
		ob.debug("*****Adding New Form Type*****");
		
		Thread.sleep(5000);
		winid = driver.getWindowHandles();
		iter =winid.iterator();
		main  =  (String) iter.next();
		tab = (String) iter.next();
		String tab3 = (String) iter.next();
				
		driver.switchTo().window(tab3);

		ob.debug("adding new form Category " + prop.getProperty("formCategory"));
		
		Thread.sleep(4000);
		driver.findElement(By.name("categoryName")).sendKeys(prop.getProperty("formCategory"));

		//Thread.sleep(5000);
		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
		
		//Thread.sleep(2000);
		driver.findElement(By.id("submit_btn")).click();
		
		ob.debug("added new form Category " + prop.getProperty("formCategory"));
		
		Thread.sleep(5000);
		driver.switchTo().window(tab);
		
		Thread.sleep(8000);
		
		//Creating a new Form
			
		ob.debug("*****Adding a Form with form type created above*****");
		
		//Thread.sleep(5000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		//Thread.sleep(2000);
		//driver.get(prop.getProperty("testurl") + "/velos/jsp/formLibraryBrowser.jsp?srcmenu=tdmenubaritem4&selectedTab=3");
		
		ob.debug("******Entering Field Library Module******");
		
		driver.findElement(By.linkText("Libraries")).click();
		//Thread.sleep(2000);
		
		driver.findElement(By.linkText("Search")).click();
		//Thread.sleep(2000);
		
		driver.findElement(By.linkText("Form Library")).click();
		Thread.sleep(2000);
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		//Thread.sleep(5000);
		driver.findElement(By.linkText("CREATE A NEW FORM")).click();
		
		ob.debug("adding a new form with name " + prop.getProperty("formName"));
		
		//Thread.sleep(5000);
		driver.findElement(By.name("txtName")).sendKeys(prop.getProperty("formName"));
		
		//Thread.sleep(5000);	
		new Select(driver.findElement(By.name("formType"))).selectByVisibleText(prop.getProperty("formCategory"));
		
		//Thread.sleep(5000);
		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
		
		//Thread.sleep(4000);
		driver.findElement(By.id("submit_btn")).click();
		
		//Thread.sleep(4000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		//Verifying data that was created in Form Module
		
		ob.debug("*****Verifying data that was created*****");
		//Thread.sleep(5000);
		 
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		//Thread.sleep(5000);
		//driver.get(prop.getProperty("testurl") + "/velos/jsp/formLibraryBrowser.jsp?srcmenu=tdmenubaritem4&selectedTab=3");
		
		ob.debug("******Entering Field Library Module******");
		
		driver.findElement(By.linkText("Libraries")).click();
		//Thread.sleep(2000);
		
		driver.findElement(By.linkText("Search")).click();
		//Thread.sleep(2000);
		
		driver.findElement(By.linkText("Form Library")).click();
		//Thread.sleep(2000);
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		//Thread.sleep(5000);
		driver.findElement(By.name("txtName")).sendKeys(prop.getProperty("formName"));
		
		//Thread.sleep(5000);
		new Select(driver.findElement(By.name("ByFormType"))).selectByVisibleText(prop.getProperty("formCategory"));
		
		//Thread.sleep(5000);
		driver.findElement(By.xpath("//button[@type='submit']")).submit();
				
		//Thread.sleep(5000);
		List<WebElement> link4 = driver.findElements(By.tagName("a"));
		
		for(int i = 0;i<link4.size();i++)
		{
			if (link4.get(i).getText().equalsIgnoreCase(prop.getProperty("formCategory")))
			{
				ob.debug("Form Category Saved Successfully - " + link4.get(i).getText());
				flag=1;
			}	
		}
		if(flag==0)
		{
			ob.debug("Form Category not saved successfully");
		}
		
		flag=0;
		for(int i = 0;i<link4.size();i++)
		{
			if (link4.get(i).getText().equalsIgnoreCase(prop.getProperty("formName")))
			{
				ob.debug(" Form Saved Successfully - " +link4.get(i).getText());
				flag=1;
			}	
		}
		if(flag==0)
		{
			ob.debug("Form not saved successfully");
		}
		
		//Thread.sleep(5000);
		driver.findElement(By.linkText(prop.getProperty("formName"))).click();
				
		ob.debug("adding fields to the form");
		
		Thread.sleep(10000);
		driver.findElement(By.linkText("Add Fields")).click();
								
		//Thread.sleep(5000);
		driver.findElement(By.cssSelector("img[alt=\"Select from Field Library\"]")).click();
	
		Thread.sleep(5000);
		winid = driver.getWindowHandles();
		iter =winid.iterator();
		main  =  (String) iter.next();
		tab = (String) iter.next();
		String tab5 = (String) iter.next();
				
		driver.switchTo().window(tab5);
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		ob.debug("Adding field from Library");
		
		Thread.sleep(5000);
		new Select(driver.findElement(By.name("category"))).selectByVisibleText(prop.getProperty("fieldcategory"));
		
		//Thread.sleep(5000);
		driver.findElement(By.xpath("//button[@type='submit']")).submit();
		
		//Thread.sleep(2000);
		driver.findElement(By.name("selected")).click();
		
		//Thread.sleep(2000);
		driver.findElement(By.cssSelector("tr.browserOddRow > td > input[name=\"selected\"]")).click();
		
		//Thread.sleep(2000);
		driver.findElement(By.xpath("(//input[@name='selected'])[3]")).click();
		
		//Thread.sleep(2000);
		driver.findElement(By.xpath("(//input[@name='selected'])[4]")).click();
		
		//Thread.sleep(2000);
		driver.findElement(By.xpath("(//input[@name='selected'])[5]")).click();
		
		//Thread.sleep(2000);
		driver.findElement(By.xpath("(//input[@name='selected'])[6]")).click();
		
		//Thread.sleep(2000);
		driver.findElement(By.cssSelector("img")).click();
						
		//Thread.sleep(2000);
		new Select(driver.findElement(By.cssSelector("select[name=\"section\"]"))).selectByVisibleText("Section 1");
		
		//Thread.sleep(2000);
		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
		
		//Thread.sleep(2000);
		driver.findElement(By.id("submit_btn")).click();
		
		ob.debug("Added Fields from library");
		
		//Thread.sleep(7000);
		driver.switchTo().window(tab);
		
		//adding a non tabular and tabular field
		
		//Thread.sleep(10000);
		driver.findElement(By.cssSelector("img[alt=\"Add a Section\"]")).click();
		
		Thread.sleep(5000);
		winid = driver.getWindowHandles();
		iter =winid.iterator();
		main  =  (String) iter.next();
		tab = (String) iter.next();
		String tab259 = (String) iter.next();
		
		//Thread.sleep(5000);
		driver.switchTo().window(tab259);
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		ob.debug("Adding Non-Tabular/ Tabular Sections to the Form");
		
		//Thread.sleep(2000);
		driver.findElement(By.name("sectionSequence")).sendKeys(prop.getProperty("secseq"));
		
		//Thread.sleep(2000);
		driver.findElement(By.name("sectionName")).sendKeys(prop.getProperty("scname"));

		//Thread.sleep(2000);
		driver.findElement(By.name("repeatNum")).clear();
		//Thread.sleep(1000);
		driver.findElement(By.name("repeatNum")).sendKeys(prop.getProperty("repeatfield"));
		
		//Thread.sleep(2000);
		new Select(driver.findElement(By.name("sectionFormat"))).selectByVisibleText("Tabular");
				
		int seq2=Integer.parseInt(prop.getProperty("secseq"))+1;
				
		//Thread.sleep(2000);
		driver.findElement(By.xpath("(//input[@name='sectionSequence'])[2]")).sendKeys(String.valueOf(seq2));
		
		//Thread.sleep(2000);
		driver.findElement(By.xpath("(//input[@name='sectionName'])[2]")).sendKeys(prop.getProperty("scname2"));
		
		//Thread.sleep(2000);
		driver.findElement(By.xpath("(//input[@name='repeatNum'])[2]")).clear();
		//Thread.sleep(1000);
		driver.findElement(By.xpath("(//input[@name='repeatNum'])[2]")).sendKeys(prop.getProperty("repeatfield"));
		
		//Thread.sleep(2000);
		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
		
		//Thread.sleep(2000);
		driver.findElement(By.id("submit_btn")).click();
		
		ob.debug("Added Non-Tabular/ Tabular Sections to the Form");
		
		//Thread.sleep(5000);
		driver.switchTo().window(tab);
				
		//adding field from add field page
		
		ob.debug("Adding Edit field from Form -> Add Fields Page");
		
		//Thread.sleep(7000);
		driver.findElement(By.cssSelector("img[alt=\"Add an Edit Box Field\"]")).click();
		
		Thread.sleep(5000);
		winid = driver.getWindowHandles();
		iter =winid.iterator();
		main  =  (String) iter.next();
		tab = (String) iter.next();
		String tab256 = (String) iter.next();
		
		//Thread.sleep(5000);
		driver.switchTo().window(tab256);
		
		Thread.sleep(5000);
		new Select(driver.findElement(By.name("section"))).selectByVisibleText(prop.getProperty("scname"));
		
		//Thread.sleep(2000);
		driver.findElement(By.name("sequence")).sendKeys(prop.getProperty("seq"));
		
		//Thread.sleep(2000);
		driver.findElement(By.name("name")).sendKeys(prop.getProperty("fieldform"));
		
		//Thread.sleep(2000);
		driver.findElement(By.name("uniqueId")).sendKeys(prop.getProperty("fieldform")+"_id");
		
		//Thread.sleep(2000);
		driver.findElement(By.xpath("(//input[@name='editBoxType'])[2]")).click();
		
		//Thread.sleep(2000);
		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
		
		//Thread.sleep(2000);
		driver.findElement(By.id("submit_btn")).click();
		
		//Thread.sleep(5000);
		driver.switchTo().window(tab);
		
		ob.debug("Added Edit field from Form -> Add Fields Page");
		
		//adding Multiple field from Add Fields
		
		ob.debug("Adding Multiple field from Form -> Add Fields Page");
		
		//Thread.sleep(5000);
		driver.findElement(By.cssSelector("img[alt=\"Add a Multiple Choice Field\"]")).click();
		
		Thread.sleep(5000);
		winid = driver.getWindowHandles();
		iter =winid.iterator();
		main  =  (String) iter.next();
		tab = (String) iter.next();
		String tab257 = (String) iter.next();
		
		//Thread.sleep(5000);
		driver.switchTo().window(tab257);
		
		Thread.sleep(2000);
		new Select(driver.findElement(By.name("section"))).selectByVisibleText(prop.getProperty("scname2"));
		
		int seq1=Integer.parseInt(prop.getProperty("seq"))+1;
		
		//Thread.sleep(2000);
		driver.findElement(By.name("sequence")).sendKeys(String.valueOf(seq1));
		
		//Thread.sleep(2000);
		driver.findElement(By.name("name")).sendKeys(prop.getProperty("fieldmultform"));

		//Thread.sleep(2000);
		driver.findElement(By.name("uniqueId")).sendKeys(prop.getProperty("fieldmultform")+"_id");
		
		//Thread.sleep(2000);
		driver.findElement(By.xpath("(//input[@name='editBoxType'])[3]")).click();

		//Thread.sleep(2000);
		driver.findElement(By.name("newTxtDispVal")).sendKeys(prop.getProperty("Value1"));
		
		//Thread.sleep(2000);
		driver.findElement(By.xpath("(//input[@name='newTxtDispVal'])[2]")).sendKeys(prop.getProperty("Value2"));
		
		//Thread.sleep(2000);
		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
		
		//Thread.sleep(2000);
		driver.findElement(By.id("submit_btn")).click();
		
		//Thread.sleep(2000);
		driver.switchTo().window(tab);
		
		ob.debug("Adding Multiple field from Form -> Add Fields Page");
		
		//add a lookup form
		
		ob.debug("Adding Lookup field from Form -> Add Fields Page");
		
		//Thread.sleep(5000);
		driver.findElement(By.cssSelector("img[alt=\"Add a Lookup Field\"]")).click();
		
		Thread.sleep(5000);
		winid = driver.getWindowHandles();
		iter =winid.iterator();
		main  =  (String) iter.next();
		tab = (String) iter.next();
		String tab258 = (String) iter.next();
		
		//Thread.sleep(5000);
		driver.switchTo().window(tab258);
		
		Thread.sleep(2000);
		new Select(driver.findElement(By.name("section"))).selectByVisibleText(prop.getProperty("scname"));
		
		seq1=Integer.parseInt(prop.getProperty("seq"))+1;
		
		//Thread.sleep(2000);
		driver.findElement(By.name("sequence")).sendKeys(String.valueOf(seq1));
		
		//Thread.sleep(2000);
		driver.findElement(By.name("name")).sendKeys(prop.getProperty("lookupform"));
		
		//Thread.sleep(2000);
		driver.findElement(By.name("sameLine")).click();
		
		//Thread.sleep(2000);
		new Select(driver.findElement(By.name("lkpview"))).selectByVisibleText("Study Versions");
		
		//Thread.sleep(2000);
		driver.findElement(By.linkText("Select")).click();
			
		//Thread.sleep(2000);
		driver.findElement(By.name("assignLkpFld")).click();
		
		//Thread.sleep(2000);
		new Select(driver.findElement(By.name("viewfld"))).selectByVisibleText("Version Number");
		
		//Thread.sleep(2000);
		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
		
		//Thread.sleep(2000);
		driver.findElement(By.id("submit_btn")).click();
		
		//Thread.sleep(5000);
		driver.switchTo().window(tab);
		
		ob.debug("Added Lookup field from Form -> Add Fields Page");
		
		//adding line break
		
		ob.debug("Adding Line break to Form");
		
		//Thread.sleep(5000);
		driver.findElement(By.cssSelector("img[alt=\"Add a Line Break\"]")).click();
		
		Thread.sleep(5000);
		winid = driver.getWindowHandles();
		iter =winid.iterator();
		main  =  (String) iter.next();
		tab = (String) iter.next();
		String tab265 = (String) iter.next();
		
		//Thread.sleep(5000);
		driver.switchTo().window(tab265);

		Thread.sleep(2000);
		new Select(driver.findElement(By.name("section"))).selectByVisibleText(prop.getProperty("scname"));
		
		Thread.sleep(2000);
		driver.findElement(By.name("sequence")).sendKeys(prop.getProperty("seq"));
		
		Thread.sleep(2000);
		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
		
		Thread.sleep(2000);
		driver.findElement(By.id("submit_btn")).click();
		
		//Thread.sleep(5000);
		driver.switchTo().window(tab);
		
		ob.debug("Adding Line break to Form");
		
		//verifying if all the field are saved correctly
		
		ob.debug("verifying if all the field are saved correctly");
		
		//Thread.sleep(5000);
		link4.clear();
		
		link4 = driver.findElements(By.tagName("td"));
		
		flag=0;
		for (int i=0; i<link4.size(); i++)
		{
			if (link4.get(i).getText().equals(prop.getProperty("fieldname")))
			{
				ob.debug("Field named "+ link4.get(i).getText()+ " added in form");
				flag=1;
			}
		}
		if(flag==0)
		{
			ob.debug("Field not added in Form");
		}
		
		flag=0;
		for (int i=0; i<link4.size(); i++)
		{
			if (link4.get(i).getText().equals(prop.getProperty("fieldname1")))
			{
				ob.debug("Field named "+ link4.get(i).getText()+ " added in form");
				flag=1;
			}
		}
		if(flag==0)
		{
			ob.debug("Field not added in Form");
		}
		
		flag=0;
		for (int i=0; i<link4.size(); i++)
		{
			if (link4.get(i).getText().equals(prop.getProperty("fieldname2")))
			{
				ob.debug("Field named "+ link4.get(i).getText()+ " added in form");
				flag=1;
			}
		}
		if(flag==0)
		{
			ob.debug("Field not added in Form");
		}
		
		flag=0;
		for (int i=0; i<link4.size(); i++)
		{
			if (link4.get(i).getText().equals(prop.getProperty("fieldname3")))
			{
				ob.debug("Field named "+ link4.get(i).getText()+ " added in form");
				flag=1;
			}
		}
		if(flag==0)
		{
			ob.debug("Field not added in Form");
		}
		
		flag=0;
		for (int i=0; i<link4.size(); i++)
		{
			if (link4.get(i).getText().equals(prop.getProperty("fieldname4")))
			{
				ob.debug("Field named "+ link4.get(i).getText()+ " added in form");
				flag=1;
			}
		}
		if(flag==0)
		{
			ob.debug("Field not added in Form");
		}
		
		flag=0;
		for (int i=0; i<link4.size(); i++)
		{
			if (link4.get(i).getText().equals(prop.getProperty("fieldname5")))
			{
				ob.debug("Field named "+ link4.get(i).getText()+ " added in form");
				flag=1;
			}
		}
		if(flag==0)
		{
			ob.debug("Field not added in Form");
		}
		
		flag=0;
		for (int i=0; i<link4.size(); i++)
		{
			if (link4.get(i).getText().equals(prop.getProperty("scname")))
			{
				ob.debug("Section named "+ link4.get(i).getText()+ " added in form");
				flag=1;
			}
		}
		if(flag==0)
		{
			ob.debug("Field not added in Form");
		}
		
		flag=0;
		for (int i=0; i<link4.size(); i++)
		{
			if (link4.get(i).getText().equals(prop.getProperty("scname2")))
			{
				ob.debug("Section named "+ link4.get(i).getText()+ " added in form");
				flag=1;
			}
		}
		if(flag==0)
		{
			ob.debug("Field not added in Form");
		}
		
		flag=0;
		for (int i=0; i<link4.size(); i++)
		{
			if (link4.get(i).getText().equals(prop.getProperty("fieldform")))
			{
				ob.debug("Field named "+ link4.get(i).getText()+ " added in form");
				flag=1;
			}
		}
		if(flag==0)
		{
			ob.debug("Field not added in Form");
		}
		flag=0;
		for (int i=0; i<link4.size(); i++)
		{
			if (link4.get(i).getText().equals(prop.getProperty("fieldmultform")))
			{
				ob.debug("Field named "+ link4.get(i).getText()+ " added in form");
				flag=1;
			}
		}
		if(flag==0)
		{
			ob.debug("Field not added in Form");
		}
			
		flag=0;
		for (int i=0; i<link4.size(); i++)
		{
			if (link4.get(i).getText().equals(prop.getProperty("lookupform")))
			{
				ob.debug("Field named "+ link4.get(i).getText()+ " added in form");
				flag=1;
			}
		}
		if(flag==0)
		{
			ob.debug("Field not added in Form");
		}
		
		flag=0;
		for (int i=0; i<link4.size(); i++)
		{
			if (link4.get(i).getText().equals("Line Break"))
			{
				ob.debug("Line break added to the form");
				flag=1;
			}
		}
		if(flag==0)
		{
			ob.debug("Field not added in Form");
		}
		
		ob.debug("verified if all the field are saved correctly");
		
		ob.debug("working on Form Settings");
		
		//Thread.sleep(3000);
		driver.findElement(By.linkText("Form Settings")).click();
		
		//Thread.sleep(3000);
		driver.findElement(By.cssSelector("img[title=\"Edit\"]")).click();
		
		Thread.sleep(3000);
		winid = driver.getWindowHandles();
		iter =winid.iterator();
		main  =  (String) iter.next();
		tab = (String) iter.next();
		String tab6 = (String) iter.next();
				
		driver.switchTo().window(tab6);
		
		//Thread.sleep(5000);
		driver.findElement(By.name("chkBrowserFlg")).click();
		
		//Thread.sleep(5000);
		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
		
		//Thread.sleep(5000);
		driver.findElement(By.id("submit_btn")).click();
		
		Thread.sleep(7000);
		driver.findElement(By.xpath("//button[@onclick='flag0()']")).click();
		
		Thread.sleep(6000);
		driver.switchTo().window(tab);
		
		ob.debug("Added Form with name " + prop.getProperty("formName"));
		
		//Thread.sleep(15000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		ob.debug("-----------------------------------------------Ending Form Library Module");
		
		
//#############################################Ending Of The Add Forms Block############################################################################################
//	#Add events and calendar
//#############################################Beginning Of The Add Events Block###########################################################################		

		ob.debug("-----------------------------------------------Entering Library >> Event Library Module-------------------------------------------------");
	
		flag=0;
		Thread.sleep(15000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		ob.debug("Entering Event Library");
		
		Thread.sleep(2000);
		//driver.get(prop.getProperty("testurl") + "/velos/jsp/eventlibrary.jsp?srcmenu=tdmenubaritem4&calledFrom=L&duration=&protocolId=&mode=M&calStatus=W&selectedTab=2");
		
		driver.findElement(By.linkText("Libraries")).click();
		//Thread.sleep(2000);
		
		driver.findElement(By.xpath("(//a[contains(text(),'Add Category')])[3]")).click();
		Thread.sleep(5000);
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		//Thread.sleep(7000);
		//driver.findElement(By.cssSelector("img[title=\"New Event Category\"]")).click();
				
		ob.debug("Adding a new Event Category " + prop.getProperty("eventcategory")+ "under " + prop.getProperty("Eventtype"));
		
		//Thread.sleep(5000);
		new Select(driver.findElement(By.name("cmbLibType"))).selectByVisibleText(prop.getProperty("Eventtype"));
		
		//Thread.sleep(5000);
		driver.findElement(By.name("categoryName")).sendKeys(prop.getProperty("eventcategory"));
		
		//Thread.sleep(5000);
		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
		
		//Thread.sleep(20000);
		driver.findElement(By.id("submit_btn")).click();

		ob.debug("Added a new Event Category " + prop.getProperty("eventcategory")+ "under " + prop.getProperty("Eventtype"));

		Thread.sleep(6000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		Thread.sleep(5000);
		//Adding a New Event using category created above
		
		driver.findElement(By.linkText("Libraries")).click();
		//Thread.sleep(2000);
		
		//driver.get(prop.getProperty("testurl") + "/velos/jsp/eventlibrary.jsp?srcmenu=tdmenubaritem4&calledFrom=L&duration=&protocolId=&mode=M&calStatus=W&selectedTab=2");
		
		driver.findElement(By.xpath("(//a[contains(text(),'Add Event')])")).click();
		Thread.sleep(5000);
		
		new Select(driver.findElement(By.name("cmbLibType"))).selectByVisibleText(prop.getProperty("Eventtype"));
		Thread.sleep(5000);
		
		new Select(driver.findElement(By.name("categoryId"))).selectByVisibleText(prop.getProperty("eventcategory"));
		Thread.sleep(2000);	
		
		driver.findElement(By.name("eventName")).sendKeys(prop.getProperty("eventname"));
		Thread.sleep(2000);
		
		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
		
		driver.findElement(By.xpath("//button[@type='submit']")).submit();
		Thread.sleep(3000);
		
		// Extra block - Do not Un-Comment
		
//		//driver.findElement(By.cssSelector("img[title=\"New Event\"]")).click();
//		//Thread.sleep(2000);
//		
//		ob.debug("You are on "+ driver.getTitle()+ " page");
//		
//		ob.debug("Adding new Event using the category created above");
//		
//		Thread.sleep(5000);
//		new Select(driver.findElement(By.name("cmbLibType"))).selectByVisibleText(prop.getProperty("Eventtype"));
//				
//		Thread.sleep(2000);
//		driver.findElement(By.name("catId")).sendKeys(prop.getProperty("eventcategory"));
//		
//		driver.findElement(By.name("eventName")).sendKeys(prop.getProperty("eventname"));
//		Thread.sleep(2000);
//		
//		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
//		
//		//Thread.sleep(2000);
//		driver.findElement(By.id("submit_btn")).click();
//		Thread.sleep(6000);
//		
//		//Thread.sleep(10000);
//		driver.findElement(By.cssSelector("img[title=\"New Event\"]")).click();
//		
//		ob.debug("You are on "+ driver.getTitle()+ " page");
//		
//		Thread.sleep(7000);
//		
//		new Select(driver.findElement(By.name("cmbLibType"))).selectByVisibleText(prop.getProperty("Eventtype"));
//		
//		Thread.sleep(5000);
//		driver.findElement(By.name("categoryId")).sendKeys(prop.getProperty("eventcategory"));
//		
//		Thread.sleep(2000);
//		driver.findElement(By.name("eventName")).sendKeys(prop.getProperty("eventname"));
//		
//		//Thread.sleep(2000);		
//		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
//		
//		Thread.sleep(2000);
//		driver.findElement(By.id("submit_btn")).click();
//		
//		ob.debug("added mandatory fields for event and saved the event");
		
		Thread.sleep(20000);
		driver.findElement(By.linkText("Cost")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		ob.debug("Adding cost to event");
		
		//Thread.sleep(10000);
		
		driver.findElement(By.linkText("Specify Cost")).click();

		//Values to this need to be changed as per Clients environment Cost Values		
		Thread.sleep(5000);
		new Select(driver.findElement(By.name("costDesc"))).selectByVisibleText(prop.getProperty("costcat1"));

		//Values to this need to be changed as per Clients environment Cost Values		
		Thread.sleep(2000);
		new Select(driver.findElement(By.xpath("//form[@id='addcostfrm']/table/tbody/tr[3]/td/select"))).selectByVisibleText(prop.getProperty("costcat2"));
	
		//Values to this need to be changed as per Clients environment Cost Values		
		Thread.sleep(2000);
		new Select(driver.findElement(By.xpath("//form[@id='addcostfrm']/table/tbody/tr[4]/td/select"))).selectByVisibleText(prop.getProperty("costcat3"));
		
		//Thread.sleep(2000);
		driver.findElement(By.name("costNum")).sendKeys(prop.getProperty("cost1"));
		
		Thread.sleep(2000);
		driver.findElement(By.xpath("//form[@id='addcostfrm']/table/tbody/tr[3]/td[3]/input[2]")).sendKeys(prop.getProperty("cost2"));

		Thread.sleep(2000);
		driver.findElement(By.xpath("//form[@id='addcostfrm']/table/tbody/tr[4]/td[3]/input[2]")).sendKeys(prop.getProperty("cost3"));

		Thread.sleep(5000);
		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
		
		Thread.sleep(5000);
		driver.findElement(By.id("submit_btn")).click();


		Thread.sleep(15000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");

		//Verifying if the data was saved correctly
		
		Thread.sleep(5000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		Thread.sleep(5000);
		//driver.get(prop.getProperty("testurl") + "/velos/jsp/eventlibrary.jsp?srcmenu=tdmenubaritem4&calledFrom=L&duration=&protocolId=&mode=M&calStatus=W&selectedTab=2");
		
		driver.findElement(By.linkText("Libraries")).click();
		//Thread.sleep(2000);
		
		driver.findElement(By.xpath("(//a[contains(text(),'Search')])[7]")).click();
		Thread.sleep(2000);
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		ob.debug("Verifying the data created in Event Library");
		
		Thread.sleep(5000);
		new Select(driver.findElement(By.name("cmbLibType"))).selectByVisibleText(prop.getProperty("Eventtype"));
				
		Thread.sleep(2000);
		driver.findElement(By.name("catId")).sendKeys(prop.getProperty("eventcategory"));
		
		Thread.sleep(2000);
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		
		Thread.sleep(5000);
		List<WebElement> link5 = driver.findElements(By.tagName("a"));
		flag=0;
		for(int i = 0;i<link5.size();i++)
		{
			if (link5.get(i).getText().equalsIgnoreCase(prop.getProperty("eventcategory")))
			{
				ob.debug("Event Category Saved Successfully - " + link5.get(i).getText());
				flag=1;
			}	
		}
		if(flag==0) 
		{
			ob.debug("Event Category not saved successfully");
		}
				
		for(int i = 0;i<link5.size();i++)
		{
			if (link5.get(i).getText().equalsIgnoreCase(prop.getProperty("eventname")))
			{
				ob.debug("Event Saved Successfully - " + link5.get(i).getText());
				flag=1;
			}	
		}
		if(flag==0)
		{
			ob.debug("Event was not saved successfully");
		}
		
		Thread.sleep(5000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		ob.debug("-----------------------------------------------Ending Event Library Module"); 

		
//#############################################End Of The Add Events Block###########################################################################
//		# Add New Calendar
//#######################################Beginning Of The Add Calendar Block#########################################################################
		
		ob.debug("-----------------------------------------------Entering Library >> Calendar Library Module-------------------------------------------");
		
		flag=0;

		Thread.sleep(15000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		Thread.sleep(5000);
		//driver.get(prop.getProperty("testurl") + "/velos/jsp/protocollist.jsp?mode=N&srcmenu=tdmenubaritem4&calledFrom=P&selectedTab=1");
		
		driver.findElement(By.linkText("Libraries")).click();
		//Thread.sleep(2000);
		
		driver.findElement(By.linkText("Add Category")).click();
		Thread.sleep(2000);
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
				
		//Thread.sleep(5000);
		//driver.findElement(By.cssSelector("img[title=\"New Calendar Category\"]")).click();
		
		ob.debug("Adding a new Calendar Type with name " + prop.getProperty("calendarCategory"));
		
		Thread.sleep(5000);
		winid = driver.getWindowHandles();
		iter =winid.iterator();
		main  =  (String) iter.next();
		tab = (String) iter.next();
		String tab7 = (String) iter.next();
				
		driver.switchTo().window(tab7);
		
		Thread.sleep(5000);
		driver.findElement(By.name("categoryName")).sendKeys(prop.getProperty("calendarCategory"));
		
		ob.debug("added a new calendar category with name "+ prop.getProperty("calendarCategory"));
		
		Thread.sleep(2000);
		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
		
		Thread.sleep(2000);
		driver.findElement(By.id("submit_btn")).click();
		
		Thread.sleep(5000);
		driver.switchTo().window(tab);

		Thread.sleep(15000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");

		//Adding a New Calendar using category above
		
		ob.debug("*****Adding a New Calendar*****");

		Thread.sleep(5000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		Thread.sleep(2000);
		//driver.get(prop.getProperty("testurl")+ "/velos/jsp/protocollist.jsp?mode=N&srcmenu=tdmenubaritem4&calledFrom=P&selectedTab=1");
		
		driver.findElement(By.linkText("Libraries")).click();
		//Thread.sleep(2000);
		
		driver.findElement(By.linkText("Add Calendar")).click();
		Thread.sleep(2000);
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		ob.debug("*****Adding a new calendar*****");
		
		//Thread.sleep(5000);
		//driver.findElement(By.linkText("CREATE A NEW CALENDAR")).click();
		
		ob.debug("Adding a new calendar with name " + prop.getProperty("calname"));
		
		Thread.sleep(10000);
		driver.findElement(By.name("protocolName")).sendKeys(prop.getProperty("calname"));
		
		//Thread.sleep(2000);
		new Select(driver.findElement(By.name("caltype"))).selectByVisibleText(prop.getProperty("calendarCategory"));
		
		Thread.sleep(2000);
		new Select(driver.findElement(By.name("calStatus"))).selectByVisibleText(prop.getProperty("calstatus"));
		
		//Thread.sleep(2000);
		driver.findElement(By.name("durNum")).sendKeys(prop.getProperty("calduration"));
		
		//Thread.sleep(2000);
		new Select(driver.findElement(By.name("durUnit"))).selectByVisibleText(prop.getProperty("caldurtime"));
		
		Thread.sleep(2000);
		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
		
		//Thread.sleep(2000);
		driver.findElement(By.id("submit_btn")).click();
		
		ob.debug("Saving the Calendar");
		
		Thread.sleep(10000);
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		// Adding event to the Calender.
		
		Thread.sleep(2000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		Thread.sleep(4000);
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		driver.findElement(By.linkText("Libraries")).click();
		//Thread.sleep(2000);
		
		driver.findElement(By.linkText("Search")).click();
		Thread.sleep(2000);
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		driver.findElement(By.name("searchName")).sendKeys(prop.getProperty("calname"));
		
		new Select(driver.findElement(By.name("ByCalType"))).selectByVisibleText(prop.getProperty("calendarCategory"));
		
		//Thread.sleep(5000);
		driver.findElement(By.xpath("//button[@type='submit']")).submit();
		
		driver.findElement(By.linkText(prop.getProperty("calname"))).click();
		Thread.sleep(5000);
		
		driver.findElement(By.linkText("Select Events")).click();
		
		ob.debug("***adding event to the calendar created above***");
		
		new Select(driver.findElement(By.name("cmbLibType"))).selectByVisibleText(prop.getProperty("Eventtype"));
				
		//Thread.sleep(15000); // pop-up window DP
		driver.findElement(By.name("categoryId")).sendKeys(prop.getProperty("eventcategory"));		
		
		//Thread.sleep(5000);
		driver.findElement(By.xpath("//button[@type='button']")).click();
		
		Thread.sleep(5000);
		winid = driver.getWindowHandles();
		iter =winid.iterator();
		main  =  (String) iter.next();
		tab = (String) iter.next();
		String tab8 = (String) iter.next();
				
		driver.switchTo().window(tab8);

		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		Thread.sleep(8000);
		driver.findElement(By.name("srhevent")).sendKeys(prop.getProperty("eventname"));
		
		//Thread.sleep(5000);
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		
		Thread.sleep(5000);
		driver.findElement(By.name("eventcheckbox_list")).click();
		
		//Thread.sleep(5000);
		driver.findElement(By.cssSelector("a[name=\"thebutton\"] > img")).click();
		
		Thread.sleep(2000);
		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
		
		Thread.sleep(5000);
		driver.findElement(By.id("submit_btn")).click();
		
		ob.debug("adding event named " + prop.getProperty("eventname") + " to the calendar");
		
		Thread.sleep(5000);
		driver.switchTo().window(tab);
		
		ob.debug("***Manage Visit Link - Adding Visit to the calendar***");
		
		Thread.sleep(40000);
		driver.findElement(By.linkText("Manage Visits")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		//Thread.sleep(5000);
		driver.findElement(By.id("rowCount")).clear();
		driver.findElement(By.id("rowCount")).sendKeys("1");
		Thread.sleep(2000);
		
		driver.findElement(By.id("addRows")).click();
		
		ob.debug("Adding Visit with name " + prop.getProperty("VisitName"));
		
		Thread.sleep(7000); 
		driver.findElement(By.xpath(".//*[@id='yui-rec0']/td[6]/div")).click();

		Thread.sleep(2000);
		driver.findElement(By.xpath(".//*[@id='yui-textboxceditor0-container']/input")).sendKeys(prop.getProperty("VisitName"));
		
		Thread.sleep(2000);
		driver.findElement(By.xpath(".//*[@id='yui-rec0']/td[14]/div")).click();		
		
		//Thread.sleep(2000);
		new Select(driver.findElement(By.xpath(".//*[@id='yui-dropdownceditor6-container']/select"))).selectByVisibleText("Fixed Time Point");
		
		//Thread.sleep(2000);
		driver.findElement(By.xpath(".//*[@id='yui-rec0']/td[17]/div")).click();
		
		//Thread.sleep(2000);
		driver.findElement(By.xpath(".//*[@id='yui-textboxceditor7-container']/input")).sendKeys(prop.getProperty("Month"));
		Thread.sleep(2000);
		
		driver.findElement(By.xpath("//tr[@id='yui-rec0']/td[18]/div")).click();
		//Thread.sleep(2000);
		
		driver.findElement(By.xpath(".//*[@id='yui-textboxceditor8-container']/input")).sendKeys(prop.getProperty("Week"));
		//Thread.sleep(2000);
		
		driver.findElement(By.xpath("//tr[@id='yui-rec0']/td[19]")).click();
		//Thread.sleep(2000);
		driver.findElement(By.xpath(".//*[@id='yui-textboxceditor9-container']/input")).sendKeys(prop.getProperty("day"));
		//Thread.sleep(2000);
		driver.findElement(By.id("div2")).click();
		//Thread.sleep(2000);
		driver.findElement(By.id("save_changes")).click();
		Thread.sleep(2000);
		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
		
		Thread.sleep(2000);
		driver.findElement(By.id("yui-gen0-button")).click();
		
		ob.debug("*****Assigning Visits to events*****");
		
		Thread.sleep(8000);
		driver.findElement(By.linkText("Event-Visit Grid")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		Thread.sleep(2000);
		driver.findElement(By.cssSelector("input.yui-dt-checkbox")).click();
		
		//Thread.sleep(2000);
		driver.findElement(By.id("save_changes")).click();
		
		Thread.sleep(2000);
		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
		
		Thread.sleep(2000);
		driver.findElement(By.id("yui-gen0-button")).click();
		
		ob.debug("*****Assigned Visits to events*****");
		
		ob.debug("***Adding Patient Cost to the Calendar***");
		
		Thread.sleep(5000);
		driver.findElement(By.linkText("Patient Cost Items")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		Thread.sleep(5000);
		driver.findElement(By.id("rowCount")).sendKeys(prop.getProperty("patrows"));
		
		Thread.sleep(5000);
		driver.findElement(By.id("addRows")).click();
		
		Thread.sleep(5000);
		driver.findElement(By.xpath("//tr[@id='yui-rec0']/td[7]")).click();
		
		//Thread.sleep(5000);
		driver.findElement(By.cssSelector("#yui-textboxceditor1-container > input[type=\"text\"]")).clear();
		
		//Thread.sleep(5000);
		driver.findElement(By.cssSelector("#yui-textboxceditor1-container > input[type=\"text\"]")).sendKeys(prop.getProperty("patcostname"));
		
		//Thread.sleep(5000);
		driver.findElement(By.xpath("//tr[@id='yui-rec0']/td[8]/div")).click();
		
		//Thread.sleep(5000);
		driver.findElement(By.cssSelector("#yui-textboxceditor2-container > input[type=\"text\"]")).clear();
		
		//Thread.sleep(5000);
		driver.findElement(By.cssSelector("#yui-textboxceditor2-container > input[type=\"text\"]")).sendKeys(prop.getProperty("cost"));
		
		//Thread.sleep(5000);
		driver.findElement(By.xpath("//tr[@id='yui-rec0']/td[9]/div")).click();
		
		//Thread.sleep(5000);
		driver.findElement(By.cssSelector("input[type=\"text\"]")).sendKeys(prop.getProperty("unit"));
		
		Thread.sleep(5000);
		driver.findElement(By.id("save_changes")).click();

		Thread.sleep(5000);
		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
		
		Thread.sleep(5000);
		driver.findElement(By.id("yui-gen0-button")).click();
		
		Thread.sleep(15000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		//Verification of data created in eResearch

		Thread.sleep(5000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		Thread.sleep(2000);
		//driver.get(prop.getProperty("testurl")+ "/velos/jsp/protocollist.jsp?mode=N&srcmenu=tdmenubaritem4&calledFrom=P&selectedTab=1");
				
		driver.findElement(By.linkText("Libraries")).click();
		//Thread.sleep(2000);
		
		driver.findElement(By.linkText("Search")).click();
		//Thread.sleep(3000);
		
		driver.findElement(By.linkText("Calendar Library")).click();
		Thread.sleep(3000);			
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		ob.debug("*****Verifying if the calendar data saved correctly*****");
		
		//Thread.sleep(5000);
		driver.findElement(By.name("searchName")).sendKeys(prop.getProperty("calname"));
		
		//Thread.sleep(5000);
		new Select(driver.findElement(By.name("ByCalType"))).selectByVisibleText(prop.getProperty("calendarCategory"));
		
		//Thread.sleep(5000);
		driver.findElement(By.xpath("//button[@type='submit']")).submit();
		
		//Thread.sleep(5000);
		List<WebElement> link6 = driver.findElements(By.tagName("a"));
		
		
		for(int i = 0;i<link6.size();i++)
		{
			
			if (link6.get(i).getText().equalsIgnoreCase(prop.getProperty("calendarCategory")))
			{
				ob.debug("Calendar Type Saved Successfully - " + link6.get(i).getText());
				flag=1;
			}	
		}
		if(flag==0)
		{
			ob.debug("Calendar Type not saved successfully");
		}

		flag=0;
		
		for(int i = 0;i<link6.size();i++)
		{
			if (link6.get(i).getText().equalsIgnoreCase(prop.getProperty("calname")))
			{
				ob.debug("Calendar Saved Successfully - " + link6.get(i).getText());
				flag=1;
			}	
		}
		if(flag==0)
		{
			ob.debug("Calendar not Saved Successfully");
		}
				
		Thread.sleep(5000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		ob.debug("-----------------------------------------------Exiting Calnedar Library Module");

//#######################################Ending Of The Calendar Block################################################################################   
//       #Add study
//#######################################Beginning Of The Add Study and Calendar Study Set Up Block##################################################
	
		ob.debug("-----------------------------------------------Entering Manage >> Study Module-------------------------------------------------");
		
		flag=0;
		
		Thread.sleep(15000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		Thread.sleep(5000);
		//driver.get(prop.getProperty("testurl") + "/velos/jsp/study.jsp?srcmenu=tdMenuBarItem3&selectedTab=1&mode=N");
		
		driver.findElement(By.linkText("Manage")).click();
		//Thread.sleep(1000);
		
		driver.findElement(By.linkText("New")).click();
		Thread.sleep(1000);
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		ob.debug("*****Entering Manage >> Study Module*****");
		
		ob.debug("Adding a new study in eResearch with name "+ prop.getProperty("Studyname"));
		
		//Thread.sleep(5000);
		driver.findElement(By.name("studyNumber")).sendKeys(prop.getProperty("Studyname"));
		
		//Thread.sleep(2000);
		driver.findElement(By.name("studyTitle")).sendKeys(prop.getProperty("Studytitle"));
		
		Thread.sleep(2000);
		new Select(driver.findElement(By.name("studyDivision"))).selectByVisibleText(prop.getProperty("division"));
		
		Thread.sleep(2000);
		driver.findElement(By.name("studyTArea")).sendKeys(prop.getProperty("tarea"));
		
		ob.debug("Adding mandatory Fields for Study");
		
		Thread.sleep(2000);
		new Select(driver.findElement(By.name("studyPhase"))).selectByVisibleText(prop.getProperty("phase"));
		
		Thread.sleep(2000);
		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
		
		Thread.sleep(2000);
		driver.findElement(By.id("submit_btn")).click();
		
		ob.debug("Added a new study in eResearch with name "+ prop.getProperty("Studyname"));
		
		Thread.sleep(15000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		

		//Verifying if the study was saved correctly
	
		ob.debug("*****Verifying if the study was saved correctly*****");
		
		Thread.sleep(5000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();	
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		Thread.sleep(5000);
		//driver.get(prop.getProperty("testurl") + "/velos/jsp/studybrowserpg.jsp?srcmenu=tdMenuBarItem3");
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		driver.findElement(By.linkText("Manage")).click();
		//Thread.sleep(2000);
		
		driver.findElement(By.linkText("Search")).click();
		//Thread.sleep(2000);
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		//Thread.sleep(2000);
		driver.findElement(By.name("searchCriteria")).sendKeys(prop.getProperty("Studyname"));
		
		//Thread.sleep(2000);
		driver.findElement(By.xpath("//button[@type='submit']")).submit();
		
		Thread.sleep(5000);
		List<WebElement> link7 = driver.findElements(By.tagName("td"));
		
		Thread.sleep(5000);
		for(int i = 0;i<link7.size();i++)
		{
			if ((link7.get(i).getText()).trim().equalsIgnoreCase(prop.getProperty("Studyname")))
			{
				ob.debug("*****Study saved successfully - " + link7.get(i).getText()+ " in eResearch*****");
				flag=1;
			}	
		}
		
		if (flag==0)
		{
			ob.debug("Study was not saved successfully");
		}
		
		Thread.sleep(10000);
		driver.findElement(By.cssSelector("img[alt=\"Study Summary\"]")).click();
			
		Thread.sleep(10000);
		driver.findElement(By.linkText("Study Setup")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		ob.debug("Adding calendar named "+ prop.getProperty("calname")+ " in the Study Setup tab");
		
		//Thread.sleep(5000);
		driver.findElement(By.linkText("SELECT A CALENDAR FROM YOUR LIBRARY")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		Thread.sleep(5000);
		winid = driver.getWindowHandles();
		iter =winid.iterator();
		main  =  (String) iter.next();
		tab = (String) iter.next();
		String tab9 = (String) iter.next();
				
		driver.switchTo().window(tab9);
		
		//Thread.sleep(5000);
		driver.findElement(By.name("searchName")).sendKeys(prop.getProperty("calname"));
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		//Thread.sleep(5000);
		driver.findElement(By.xpath("//button[@type='submit']")).submit();
		
		//Thread.sleep(5000);
		driver.findElement(By.linkText("Select")).click();
		
		ob.debug("Added calendar named "+ prop.getProperty("calname")+ " in the Study Setup tab");
		
		//Thread.sleep(2000);
		driver.switchTo().window(tab);
		
		ob.debug("****Activating the calendar named "+ prop.getProperty("calname")+ "with activated date on " +prop.getProperty("StatusDate")+"*****" );
		
		//Thread.sleep(10000); 
		driver.findElement(By.linkText(prop.getProperty("calstatus"))).click();
		
		//Thread.sleep(10000);  
		new Select(driver.findElement(By.name("calStatus"))).selectByVisibleText(prop.getProperty("calsetstatus"));
		
		//Thread.sleep(10000);
		driver.findElement(By.name("StatusDate")).click();

		//Thread.sleep(5000);
		driver.findElement(By.linkText(prop.getProperty("StatusDate"))).click();
		
		Thread.sleep(2000);
		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
		
		Thread.sleep(4000);
		driver.findElement(By.id("submit_btn")).click();
		
		ob.debug("Activated the calendar named "+ prop.getProperty("calname")+ "with activated date as " +prop.getProperty("StatusDate") );
		
		Thread.sleep(15000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		//Associating Form to study
		
		Thread.sleep(5000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
 		Thread.sleep(5000);
		//driver.get(prop.getProperty("testurl") + "/velos/jsp/studybrowserpg.jsp?srcmenu=tdMenuBarItem3");
		
 		driver.findElement(By.linkText("Manage")).click();
		//Thread.sleep(2000);
		
		driver.findElement(By.linkText("Search")).click();
		//Thread.sleep(3000);
 		
		ob.debug("You are on "+ driver.getTitle()+ " page");
 		
 		//Thread.sleep(5000);
		driver.findElement(By.name("searchCriteria")).sendKeys(prop.getProperty("Studyname"));
		
		Thread.sleep(2000);
		driver.findElement(By.xpath("//button[@type='submit']")).submit();
		
		//Thread.sleep(5000);
		driver.findElement(By.cssSelector("img[alt=\"Study Summary\"]")).click();
		
		ob.debug("***Clicking on the study created above***");
		
		Thread.sleep(5000);
		driver.findElement(By.linkText("Study Setup")).click();

		ob.debug("Adding form named "+ prop.getProperty("formName")+ " in the Study Setup tab");
		
		Thread.sleep(2000);
		driver.findElement(By.linkText("SELECT A FORM FROM YOUR LIBRARY")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		Thread.sleep(2000);
		winid = driver.getWindowHandles();
		iter =winid.iterator();
		main  =  (String) iter.next();
		tab = (String) iter.next();
		String tab14 = (String) iter.next();
				
		driver.switchTo().window(tab14);
		
		//Thread.sleep(5000);
		driver.findElement(By.name("formName")).sendKeys(prop.getProperty("formName"));
		
		//Thread.sleep(2000);
		driver.findElement(By.xpath("//button[@type='submit']")).submit();
		
		//Thread.sleep(2000);
		driver.findElement(By.name("selForms")).click();
		
		//Thread.sleep(2000);
		driver.findElement(By.cssSelector("img")).click();
		
		//Thread.sleep(2000);
		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
		
		Thread.sleep(2000);
		driver.findElement(By.id("submit_btn")).click();
		
		ob.debug("Added calendar named "+ prop.getProperty("calname")+ " in the Study Setup tab");
		
		Thread.sleep(5000);
		driver.switchTo().window(tab);
		
		ob.debug("Activating the form named "+ prop.getProperty("formName"));
		
		//Thread.sleep(10000);
		driver.findElement(By.linkText(prop.getProperty("studyformstat"))).click();
		
		//Thread.sleep(2000);
		new Select(driver.findElement(By.id("formStat"))).selectByVisibleText(prop.getProperty("studyformstat1"));
		
		//Thread.sleep(2000);
		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
		Thread.sleep(4000);
		
		driver.findElement(By.id("submit_btn")).click();
		
		ob.debug("Activated the form named "+ prop.getProperty("formName"));
				
		Thread.sleep(8000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
	
		//Creating a Combined Budget
	
		Thread.sleep(5000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		Thread.sleep(5000);
		//driver.get(prop.getProperty("testurl") + "/velos/jsp/studybrowserpg.jsp?srcmenu=tdMenuBarItem3");
		
		driver.findElement(By.linkText("Manage")).click();
		//Thread.sleep(2000);
		
		driver.findElement(By.linkText("Search")).click();
		//Thread.sleep(3000);
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
 		
 		Thread.sleep(5000);
		driver.findElement(By.name("searchCriteria")).sendKeys(prop.getProperty("Studyname"));
		
		//Thread.sleep(2000);
		driver.findElement(By.xpath("//button[@type='submit']")).submit();
		
		//Thread.sleep(5000);
		driver.findElement(By.cssSelector("img[alt=\"Study Summary\"]")).click();
		Thread.sleep(5000);
		
		driver.findElement(By.linkText("Budget")).click();
		Thread.sleep(5000);
		
		ob.debug("Creating a Combined Budget");
			
		driver.findElement(By.id("bgtAllCal")).click();
		Thread.sleep(5000);
		
		new Select(driver.findElement(By.id("calstatus"))).selectByVisibleText(prop.getProperty("calsetstatus"));
		Thread.sleep(5000);
		
		driver.findElement(By.cssSelector("option[value=\"A\"]")).click();
		
		Thread.sleep(5000);
		new Select(driver.findElement(By.name("budgetTemplate"))).selectByVisibleText("Patient");
		
		Thread.sleep(5000);
		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
			
		Thread.sleep(7000);
		driver.findElement(By.id("submit_btn")).click();
		
		ob.debug("Created a Combined Budget");
		
		ob.debug("Editing Budget");
		
		Thread.sleep(9000);
		driver.findElement(By.linkText("Edit Calculation Attributes")).click();
				
		Thread.sleep(4000);
		winid = driver.getWindowHandles();
		iter =winid.iterator();
		main  =  (String) iter.next();
		tab = (String) iter.next();
		String tab111 = (String) iter.next();
				
		driver.switchTo().window(tab111);
			
		Thread.sleep(4000);
		driver.findElement(By.name("fringeBenefit")).clear();
		driver.findElement(By.name("fringeBenefit")).sendKeys("10");
		
		Thread.sleep(2000);
		driver.findElement(By.name("costDiscount")).clear();
		driver.findElement(By.name("costDiscount")).sendKeys("20");
		
		Thread.sleep(2000);
		driver.findElement(By.name("sponsorOHead")).clear();
		driver.findElement(By.name("sponsorOHead")).sendKeys("30");
		
		Thread.sleep(2000);
		driver.findElement(By.id("submit_btn")).click();
		
		ob.debug("Edited Budget");
	
		Thread.sleep(5000);
		driver.switchTo().window(tab);
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
				
		Thread.sleep(20000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
	
		//Adding a new Study Status

		Thread.sleep(5000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();

		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		Thread.sleep(5000);
 		//driver.get(prop.getProperty("testurl") + "/velos/jsp/studybrowserpg.jsp?srcmenu=tdMenuBarItem3");
 		
		driver.findElement(By.linkText("Manage")).click();
		//Thread.sleep(2000);
		
		driver.findElement(By.linkText("Search")).click();
		//Thread.sleep(3000);
		
 		ob.debug("You are on "+ driver.getTitle()+ " page");
 		
 		Thread.sleep(2000);
		driver.findElement(By.name("searchCriteria")).sendKeys(prop.getProperty("Studyname"));
		
		Thread.sleep(2000);
		driver.findElement(By.xpath("//button[@type='submit']")).submit();
		
		ob.debug("Clicking on the study created above");
		
		Thread.sleep(5000);
		driver.findElement(By.cssSelector("img[alt=\"Study Summary\"]")).click();
		
		Thread.sleep(5000);
		driver.findElement(By.linkText("Study Status")).click();
		
		ob.debug("Adding a new Study Status " + prop.getProperty("studysetstatus"));
		
		Thread.sleep(2000);
		driver.findElement(By.linkText("ADD NEW STATUS")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		//Thread.sleep(5000);
		//new Select(driver.findElement(By.name("studyStatusType"))).selectByVisibleText(prop.getProperty("studystattype")); DP
		
		//Thread.sleep(5000);
		new Select(driver.findElement(By.name("protocolStatus"))).selectByVisibleText(prop.getProperty("studysetstatus"));
		
		//Thread.sleep(2000);
		driver.findElement(By.name("startDate")).click();
		
		//Thread.sleep(2000);
		driver.findElement(By.linkText(prop.getProperty("studystatusdate"))).click();
		
		//Thread.sleep(2000);
		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
		
		Thread.sleep(2000);
		driver.findElement(By.id("submit_btn")).click();
		
		ob.debug("Added a new Study Status " + prop.getProperty("studysetstatus"));
		
		Thread.sleep(2000);
		driver.switchTo().window(tab);
		
		Thread.sleep(15000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");

		//Adding form Response to a study

		Thread.sleep(5000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		Thread.sleep(5000);
		//driver.get(prop.getProperty("testurl") + "/velos/jsp/studybrowserpg.jsp?srcmenu=tdMenuBarItem3");
		
		driver.findElement(By.linkText("Manage")).click();
		//Thread.sleep(2000);
		
		driver.findElement(By.linkText("Search")).click();
		//Thread.sleep(3000);
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		Thread.sleep(2000);
		driver.findElement(By.name("searchCriteria")).sendKeys(prop.getProperty("Studyname"));
		
		ob.debug("Clicking on Study created above");
		
		Thread.sleep(2000);
		driver.findElement(By.xpath("//button[@type='submit']")).submit();
		
		Thread.sleep(5000);
		driver.findElement(By.cssSelector("img[alt=\"Study Summary\"]")).click();
		
		Thread.sleep(5000);
		driver.findElement(By.xpath("(//a[contains(text(),'Forms')])[2]")).click();
		
		ob.debug("Adding Form Responses to form named "+ prop.getProperty("formName"));
		
		Thread.sleep(2000);
		new Select(driver.findElement(By.id("formPullDown"))).selectByVisibleText(prop.getProperty("formName"));
		
		//Thread.sleep(2000);
		driver.findElement(By.cssSelector("span.ui-button-text")).click();
		
		ob.debug("Adding form response for mandatory field");
		
		//Thread.sleep(2000);
		driver.findElement(By.id("er_def_date_01")).click();
		
		//Thread.sleep(2000);
		driver.findElement(By.linkText(prop.getProperty("dataentrydate"))).click();
		
		//Thread.sleep(2000);
		new Select(driver.findElement(By.id("er_def_formstat"))).selectByVisibleText(prop.getProperty("formstatus"));
		
		//Thread.sleep(2000);
		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
		
		Thread.sleep(2000);
		driver.findElement(By.xpath("(//button[@type='submit'])[2]")).click();
		
		ob.debug("Added Form Reponses");
		
		Thread.sleep(15000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();	
		ob.debug("You are on "+ driver.getTitle()+ " page");		

		ob.debug("-----------------------------------------------Exiting Study Module");


//#######################################Ending Of The Study Block################################################################################
//		// Add Portal  - Added by DP
//##########################################Beginning Of The Add Portal Admin Module##############################################################################################
	
		ob.debug("-----------------------------------------------Entering Manage >> Portal Admin Module-------------------------------------------------");
				
		flag=0;
		Thread.sleep(15000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		ob.debug("******Entering Portal Admin Module******");
		
		Thread.sleep(2000);
		//driver.get(prop.getProperty("testurl") + "/velos/jsp/portal.jsp?srcmenu=tdMenuBarItem2&selectedTab=6");				
		
		driver.findElement(By.linkText("Manage")).click();
		//Thread.sleep(2000);
		
		driver.findElement(By.linkText("Portals")).click();
		Thread.sleep(3000);
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		ob.debug("******Creating a New Portal******");
		
		//Thread.sleep(5000);
		driver.findElement(By.linkText("Create New Portal")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		ob.debug("Creating a Portal as "+ prop.getProperty("portalname"));
		
		//Thread.sleep(2000);
		driver.findElement(By.name("portalName")).sendKeys(prop.getProperty("portal_name"));
		
		ob.debug("click on select study");
		
		Thread.sleep(5000);
		driver.findElement(By.linkText("Select Study")).click(); 		
				
		Thread.sleep(4000);
		winid = driver.getWindowHandles();
		iter =winid.iterator();
		main  =  (String) iter.next();
		tab = (String) iter.next();
		String tab001 = (String) iter.next();
		
		Thread.sleep(4000); 			
		driver.switchTo().window(tab001);
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
				
		//Handling frames
		
		ob.debug("Handling frames");
		
		//Thread.sleep(10000);
		driver.switchTo().frame("lookupFrame");
		
		ob.debug("Adding studylookup>>" + prop.getProperty("studylookup")); 
		
		Thread.sleep(2000); 
		driver.findElement(By.name("search_data")).sendKeys(prop.getProperty("Studyname")); //DP
					
		//Thread.sleep(2000);
		driver.findElement(By.xpath("//button[@type='submit']")).submit();
		
		Thread.sleep(2000);
		driver.findElement(By.xpath("//a/span")).click();
		
		ob.debug("Handling Submit frame.");
		
		//Thread.sleep(4000); 			
		driver.switchTo().window(tab001); // added by DP
		
		//Thread.sleep(10000);
		driver.switchTo().frame("selectFrame");
				
		//Thread.sleep(2000);
		driver.findElement(By.id("submit_btn")).click();
				
		Thread.sleep(4000);
		driver.switchTo().window(tab);
		
		//Thread.sleep(2000);
		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
		
		Thread.sleep(2000);
		driver.findElement(By.id("submit_btn")).click();
		
		ob.debug("Exiting Portal Admin Module ");
						
		Thread.sleep(10000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		//Verifying if the Portal Saved successfully. added by DP
		
		ob.debug("Verifying data saved for System user");
		
		Thread.sleep(2000);
		//driver.get(prop.getProperty("testurl") + "/velos/jsp/portal.jsp?srcmenu=tdMenuBarItem2&selectedTab=6");	
		
		driver.findElement(By.linkText("Manage")).click();
		//Thread.sleep(2000);
		
		driver.findElement(By.linkText("Portals")).click();
		Thread.sleep(3000);
		
		ob.debug("You are on " + driver.getTitle()+" page");
		
		Thread.sleep(5000);
		List<WebElement> link001 = driver.findElements(By.tagName("a"));
		
		for(int i = 0;i<link001.size();i++)
			{
				if (link001.get(i).getText().equalsIgnoreCase(prop.getProperty("portal_name")))
				{
					ob.debug("Portal saved successfully - " + link001.get(i).getText());
					flag=1;
				}	
			}
		if(flag==0)
		{
			ob.debug("Link not saved, please try to save the Link");
		}
			
		ob.debug("-----------------------------------------------Exiting Portal Admin Module-------------------------------------------------");
		
		
//#######################################End Of The Add Portal Admin Module#######################################################################		
//      #Add Patient
//#######################################Beginning Of The Add Patient Block#######################################################################		
  	
		ob.debug("-----------------------------------------------Entering Manage >> Patient Module-------------------------------------------------");

		Thread.sleep(15000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();	
		
		ob.debug("You are on "+ driver.getTitle()+ " page");	
		
		ob.debug("Entering Patient Module");
		
		Thread.sleep(5000);
		//driver.get(prop.getProperty("testurl") + "/velos/jsp/patientdetailsquick.jsp?srcmenu=tdmenubaritem5&mode=N&selectedTab=1&pkey=&page=patient");
		
		driver.findElement(By.linkText("Manage")).click();
		//Thread.sleep(2000);
		
		driver.findElement(By.xpath("(//a[contains(text(),'New')])[3]")).click();
		Thread.sleep(2000);
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		ob.debug("Adding Patient name "+ prop.getProperty("patid"));
		
		//Thread.sleep(5000);
		driver.findElement(By.id("patid")).sendKeys(prop.getProperty("patid"));
		
		//Thread.sleep(2000);
		driver.findElement(By.id("patfname")).sendKeys(prop.getProperty("patfname"));
		
		//Thread.sleep(2000);
		driver.findElement(By.id("patfname")).sendKeys(Keys.TAB);
		
		//Thread.sleep(2000);
		driver.findElement(By.id("patlname")).sendKeys(prop.getProperty("patlname"));
				
		//Thread.sleep(2000);
		driver.findElement(By.id("patdob")).sendKeys(prop.getProperty("dob"));
							
		//Thread.sleep(2000);
		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
		
		Thread.sleep(5000);
		driver.findElement(By.id("submit_btn")).click();
		
		ob.debug("Added new patient with patient id as "+ prop.getProperty("patid"));

		Thread.sleep(15000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();	
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		//Verifying if the patient is created
		
		//Thread.sleep(5000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();	
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		//Thread.sleep(5000);
		//driver.get(prop.getProperty("testurl")+"/velos/jsp/allPatient.jsp?srcmenu=tdmenubaritem5&selectedTab=1&searchFrom=initial");
		
		driver.findElement(By.linkText("Manage")).click();
		//Thread.sleep(2000);
		
		driver.findElement(By.xpath("(//a[contains(text(),'Search')])[2]")).click();
		Thread.sleep(2000);	
		
		//Thread.sleep(2000);
		driver.findElement(By.id("patCode")).sendKeys(prop.getProperty("patid"));
		
		//Thread.sleep(2000);
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		
		Thread.sleep(10000);
		List<WebElement> link8 = driver.findElements(By.tagName("a"));
		flag=0;
		for(int i = 0;i<link8.size();i++)
		{
			
			if (link8.get(i).getText().equalsIgnoreCase(prop.getProperty("patid")))
			{
				ob.debug("Patient Saved Successfully - " + link8.get(i).getText());
				flag=1;
			}	
		}
		if(flag==0)
		{
			ob.debug("Patient Not saved successfully");
		}
			
		Thread.sleep(5000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();	
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		ob.debug("-----------------------------------------------Created a new Patient");

		
//#############################################Ending Of The Add Patient Block###########################################################################################
			//#! Patient Enroll
//#############################################Beginning Of The Patient Enroll Block#####################################################################################

		ob.debug("-----------------------------------------------Enrolling Patient created above to the Study--------------------------------------------");
		
		Thread.sleep(5000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();	
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		//Thread.sleep(5000);
		//driver.get(prop.getProperty("testurl")+"/velos/jsp/allPatient.jsp?srcmenu=tdmenubaritem5&selectedTab=1&searchFrom=initial");
	
		driver.findElement(By.linkText("Manage")).click();
		//Thread.sleep(2000);
		
		driver.findElement(By.xpath("(//a[contains(text(),'Search')])[2]")).click();
		Thread.sleep(2000);	
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		ob.debug("****Searching for Patient to be enrolled in Study****");
		
		Thread.sleep(2000);
		driver.findElement(By.id("patCode")).sendKeys(prop.getProperty("patid"));
		
		Thread.sleep(2000);
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		
		Thread.sleep(5000);
		driver.findElement(By.linkText(prop.getProperty("patid"))).click();
		
		ob.debug("Enrolling Patients "+prop.getProperty("patid") + " to Study " + prop.getProperty("Studyname"));
		
		Thread.sleep(5000);
		new Select(driver.findElement(By.name("selstudyId"))).selectByVisibleText(prop.getProperty("Studyname"));
		
		Thread.sleep(5000);
		driver.findElement(By.id("submit_btn")).click();
				
		Thread.sleep(7000);
		winid = driver.getWindowHandles();
		iter =winid.iterator();
		main  =  (String) iter.next();
		tab = (String) iter.next();
		String tab10 = (String) iter.next();
		
		//Thread.sleep(5000);		
		driver.switchTo().window(tab10);
		
		ob.debug("Adding "+ prop.getProperty("patstudystatus")+ " status as Patient Study Status");
		
		//Thread.sleep(5000);
		new Select(driver.findElement(By.name("patstatus"))).selectByVisibleText(prop.getProperty("patstudystatus"));
		
		//Thread.sleep(15000);
		driver.findElement(By.xpath("html/body/div[2]/form/table[2]/tbody/tr[4]/td[2]/input")).click();
		
		//Thread.sleep(5000);
		driver.findElement(By.linkText(prop.getProperty("enrolldate"))).click();
		
		//Thread.sleep(5000);
		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
		
		Thread.sleep(5000);
		driver.findElement(By.id("submit_btn")).click();
		
		ob.debug("Added "+ prop.getProperty("patstudystatus")+ " status as Patient Study Status");
		
		//Thread.sleep(5000);
		driver.switchTo().window(tab);
		
		//verifying if the patient study status is saved successfully
		
		Thread.sleep(10000);
		List<WebElement> link16 = driver.findElements(By.tagName("a"));
		
		flag=0;
		
		for(int i = 0;i<link16.size();i++)
		{
			
			if (link16.get(i).getText().equalsIgnoreCase(prop.getProperty("patstudystatus"))){
				
				ob.debug("Patient Study Status saved Successfully - " + link16.get(i).getText());
				flag=1;
			}	
		}
		if(flag==0)
		{
			ob.debug("Patient Study Status not Saved");
		}
				
		//Thread.sleep(15000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();	
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		
		ob.debug("-----------------------------------------------Enrolled a Patient to Study"); 
		
//################################################Ending Of The Patient Enroll Block######################################################################################
//#!Add Patient Schedule
//################################################Beginning Of The Add Patient Schedule Block#############################################################################		

		ob.debug("-----------------------------------------------Generating schedule for Patient--------------------------------------------");
		
		Thread.sleep(5000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();	
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		ob.debug("Clicking on Patient Enrollment link through Study");
		
		Thread.sleep(5000);
		driver.findElement(By.name("searchCriteria")).sendKeys(prop.getProperty("Studyname"));
		
		//Thread.sleep(2000);
		driver.findElement(By.xpath("//button[@type='submit']")).submit();
		
		//Thread.sleep(5000);
		driver.findElement(By.cssSelector("img[alt=\"Patient Enrollment\"]")).click();
		
		//Thread.sleep(25000);
		driver.findElement(By.linkText(prop.getProperty("patid"))).click();
		
		ob.debug("***Generating Schedule for Patient using Calendar activated in Study Setup tab***");
		
		//Thread.sleep(5000);
		driver.findElement(By.linkText("Edit Calendar/Date")).click();
		
		Thread.sleep(5000); //dp
		winid = driver.getWindowHandles();
		iter =winid.iterator();
		main  =  (String) iter.next();
		tab = (String) iter.next();
		String tab1111 = (String) iter.next(); 
				
		driver.switchTo().window(tab1111);  
		
		Thread.sleep(5000);
		new Select(driver.findElement(By.name("protocolId"))).selectByVisibleText(prop.getProperty("calname"));
		
		//Thread.sleep(2000);
		driver.findElement(By.name("protStDate")).click();
		
		//Thread.sleep(2000);
		driver.findElement(By.linkText(prop.getProperty("dateofactive"))).click();
		
		//Thread.sleep(2000);
		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
		
		Thread.sleep(2000);
		driver.findElement(By.id("submit_btn")).click();
		
		ob.debug("***Generated Schedule for Patient***");
		
		driver.switchTo().window(tab);
		
		Thread.sleep(15000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();	
		
		ob.debug("You are on "+ driver.getTitle()+ " page");

		ob.debug("-----------------------------------------------Generated a schedule for Patient");
		
//########################################Ending Of The Add Patient Schedule Block###################################################################
//#ADD Patient Adverse Event
//########################################Beginning Of The Add Patient Adverse Event Block###########################################################		
	
		ob.debug("-----------------------------------------------Adding One Adverse Event for Patient--------------------------------------------");
		
		Thread.sleep(15000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();	
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		ob.debug("Clicking on Patient Enrollment link through Study");
		
		Thread.sleep(5000);
		driver.findElement(By.name("searchCriteria")).sendKeys(prop.getProperty("Studyname"));
		
		Thread.sleep(2000);
		driver.findElement(By.xpath("//button[@type='submit']")).submit();
		
		Thread.sleep(5000);
		driver.findElement(By.cssSelector("img[alt=\"Patient Enrollment\"]")).click();
		
		Thread.sleep(10000);
		driver.findElement(By.linkText(prop.getProperty("patid"))).click();
		
		ob.debug("adding a new adverse event");
		
		Thread.sleep(2000);
		driver.findElement(By.linkText("ADVERSE EVENTS")).click();
		
		Thread.sleep(2000);
		driver.findElement(By.linkText("Add New AE")).click();
		
		Thread.sleep(2000);
		new Select(driver.findElement(By.name("adve_type"))).selectByVisibleText(prop.getProperty("adversetype"));
		
		//Thread.sleep(5000);
		driver.findElement(By.name("startDt")).click();
		
		//Thread.sleep(5000);
		driver.findElement(By.linkText(prop.getProperty("adversestartdate"))).click();
		
		//Thread.sleep(2000);
		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
		
		Thread.sleep(2000);
		driver.findElement(By.id("submit_btn")).click();
		
		ob.debug("Added Adverse Event");
		
		Thread.sleep(15000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();	
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		//Verifying if the Adverse Event has been saved properly
		
		//Thread.sleep(5000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();	
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		ob.debug("Clicking on Patient Enrollment link through Study");
		
		//Thread.sleep(5000);
		driver.findElement(By.name("searchCriteria")).sendKeys(prop.getProperty("Studyname"));
		
		//Thread.sleep(2000);
		driver.findElement(By.xpath("//button[@type='submit']")).submit();
		
		//Thread.sleep(10000);
		driver.findElement(By.cssSelector("img[alt=\"Patient Enrollment\"]")).click();
		
		//Thread.sleep(20000);
		driver.findElement(By.linkText(prop.getProperty("patid"))).click();
		
		ob.debug("Checking if the Adverse Event was saved properly");
		
		//Thread.sleep(2000);
		driver.findElement(By.linkText("ADVERSE EVENTS")).click();
		
		Thread.sleep(5000);
		List<WebElement> link10 = driver.findElements(By.tagName("a"));
		flag=0;
		for(int i = 0;i<link10.size();i++)
		{
			if (link10.get(i).getText().equalsIgnoreCase(prop.getProperty("adversetype")))
			{
				ob.debug("Adverse Event Saved Successfully - " + link10.get(i).getText());
				flag=1;
			}	
		}
		if(flag==0)
		{
			ob.debug("Adverse Event saved successfully");
		}
		Thread.sleep(5000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();	
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		ob.debug("-----------------------------------------------Added one Adverse Event");
		
//#############################################Ending Of The Add Patient Adverse Event Block##############################################################################
//#ADD Multiple Adverse Events
//#############################################Beginning Of The Add Multiple Adverse Events Block#########################################################################
	
		ob.debug("-----------------------------------------------Adding Multiple Adverse Event for Patient--------------------------------------------");
		
		Thread.sleep(15000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();	
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		ob.debug("Clicking on Patient Enrollment link through Study");
		
		Thread.sleep(5000);
		driver.findElement(By.name("searchCriteria")).sendKeys(prop.getProperty("Studyname"));
		
		Thread.sleep(2000);
		driver.findElement(By.xpath("//button[@type='submit']")).submit();
		
		//Thread.sleep(2000);
		driver.findElement(By.cssSelector("img[alt=\"Patient Enrollment\"]")).click();
		
		Thread.sleep(5000);
		driver.findElement(By.linkText(prop.getProperty("patid"))).click();

		ob.debug("Adding Multiple Adverse Event");
		
		//Thread.sleep(2000);
		driver.findElement(By.linkText("ADVERSE EVENTS")).click();
		
		Thread.sleep(5000);
		driver.findElement(By.linkText("Add Multiple AE's")).click();

		Thread.sleep(5000);
		winid = driver.getWindowHandles();
		iter =winid.iterator();
		main  =  (String) iter.next();
		tab = (String) iter.next();
		String tab12 = (String) iter.next();
				
		driver.switchTo().window(tab12);
		
		ob.debug("Added First Adverse Event");
		Thread.sleep(5000);
 		new Select(driver.findElement(By.name("advEtype0"))).selectByVisibleText(prop.getProperty("adversetype1"));
 		
 		Thread.sleep(5000);
 		driver.findElement(By.name("startDt0")).click();
		
		Thread.sleep(5000);
		driver.findElement(By.linkText(prop.getProperty("adversestartdate1"))).click();
		
		ob.debug("Added Second Adverse Event");
		
		//Thread.sleep(5000);
		new Select(driver.findElement(By.name("advEtype1"))).selectByVisibleText(prop.getProperty("adversetype2"));
		
		//Thread.sleep(5000);
		driver.findElement(By.name("startDt1")).click();
		
		//Thread.sleep(5000);
		driver.findElement(By.linkText(prop.getProperty("adversestartdate2"))).click();
		
		//Thread.sleep(2000);
		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
		
		//Thread.sleep(2000);
		driver.findElement(By.id("submit_btn")).click();
		
		ob.debug("Added 2 Adverse Events");

		Thread.sleep(2000);
		driver.switchTo().window(tab);
		
		Thread.sleep(15000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();	
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		//Verifying the Multiple events has been saved
		
		//Thread.sleep(15000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();	
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		ob.debug("Clicking on Patient Enrollment link through Study");
		
		//Thread.sleep(5000);
		driver.findElement(By.name("searchCriteria")).sendKeys(prop.getProperty("Studyname"));
		
		//Thread.sleep(2000);
		driver.findElement(By.xpath("//button[@type='submit']")).submit();
		
		//Thread.sleep(2000);
		driver.findElement(By.cssSelector("img[alt=\"Patient Enrollment\"]")).click();
		
		//Thread.sleep(5000);
		driver.findElement(By.linkText(prop.getProperty("patid"))).click();

		ob.debug("****Verifying if Multiple Adverse Event saved successfully***");
		
		//Thread.sleep(2000);
		driver.findElement(By.linkText("ADVERSE EVENTS")).click();
		
		Thread.sleep(5000);
		List<WebElement> link9 = driver.findElements(By.tagName("a"));
		
		for(int i = 0;i<link9.size();i++)
		{
			
			if (link9.get(i).getText().equalsIgnoreCase(prop.getProperty("adversetype1")))
			{
				ob.debug("First Adverse Event saved Successfully - " +link9.get(i).getText());
				flag=1;
			}	
		}
		if(flag==0)
		{
			ob.debug("First Adverse Event not saved Successfully");
		}
		flag=0;
		for(int i = 0;i<link9.size();i++)
		{
			if (link9.get(i).getText().equalsIgnoreCase(prop.getProperty("adversetype2")))
			{
				ob.debug("Second Adverse Event Saved Successfully " +link9.get(i).getText());
				flag=1;
			}	
		}
		if(flag==0)
		{
			ob.debug("Second Adverse Event not saved Successfully");
		}		
		
		Thread.sleep(5000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();	
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		ob.debug("-----------------------------------------------Exiting Patient Module");
	
// **********************************Ending Of The Add Multiple Adverse Events Block**********************************
// ADD Patient Budget 
// **********************************Beginning Of The Add Patient Budget Block******************************************************   
		
 		ob.debug("-----------------------------------------------Entering Manage >> Budget Module-------------------------------------------------");
		
		Thread.sleep(15000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		Thread.sleep(10000);
		//driver.get(prop.getProperty("testurl") + "/velos/jsp/budget.jsp?srcmenu=tdmenubaritem6&mode=N");
		
		driver.findElement(By.linkText("Manage")).click();
		//Thread.sleep(2000);
		
	    driver.findElement(By.xpath("(//a[contains(text(),'New')])[4]")).click();
	    Thread.sleep(2000);
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		ob.debug("Adding new Budget named " + prop.getProperty("bgtname"));
		
		//Thread.sleep(25000);
		driver.findElement(By.name("budgetName")).sendKeys(prop.getProperty("bgtname"));
		
		//Thread.sleep(2000);
		new Select(driver.findElement(By.name("budgetTemplate"))).selectByVisibleText("Patient");
		
		//Thread.sleep(2000);
		new Select(driver.findElement(By.name("dBudgetCodeStatus"))).selectByVisibleText(prop.getProperty("bgtstatus"));
		
		//Thread.sleep(2000);
		new Select(driver.findElement(By.name("budgetStudyId"))).selectByVisibleText(prop.getProperty("Studyname"));
		
		//Thread.sleep(2000);
		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
		
		Thread.sleep(2000);
		driver.findElement(By.id("submit_btn")).click();
		
		Thread.sleep(6000);
		driver.findElement(By.cssSelector("img[alt=\"Select Calendar from Study\"]")).click();
		
		Thread.sleep(5000);
		winid = driver.getWindowHandles();
		iter =winid.iterator();
		main  =  (String) iter.next();
		tab = (String) iter.next();
		String tab112 = (String) iter.next();
				
		driver.switchTo().window(tab112);
		
		Thread.sleep(4000);
		//driver.findElement(By.name("bgtcalcheckbox")).click();
		
		driver.findElement(By.xpath(".//*[@id='bgtprotlist']/table[3]/tbody/tr[2]/td[1]/input")).click();
		
		Thread.sleep(2000);
		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
		
		Thread.sleep(2000);
		driver.findElement(By.id("submit_btn")).click();
		
		ob.debug("Added a new Patient Budget in eResearch");
		
		Thread.sleep(2000);
		driver.switchTo().window(tab);
	
		Thread.sleep(7000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
 		
//Verifying if the Budget saved successfully
		
		//Thread.sleep(15000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		//Thread.sleep(5000);
		//driver.get(prop.getProperty("testurl") + "/velos/jsp/budgetbrowserpg.jsp?srcmenu=tdmenubaritem6");
		
		driver.findElement(By.linkText("Manage")).click();
		//Thread.sleep(2000);
		
	    driver.findElement(By.xpath("(//a[contains(text(),'Search')])[4]")).click();
	    Thread.sleep(10000);
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
				
		//Thread.sleep(10000);
		driver.findElement(By.name("searchCriteria")).sendKeys(prop.getProperty("bgtname"));
		
		Thread.sleep(2000);
		driver.findElement(By.xpath("//button[@type='submit']")).submit();
		
		Thread.sleep(5000);
		List<WebElement> link12 = driver.findElements(By.tagName("a"));
		flag=0;
		for(int i = 0;i<link12.size();i++)
		{
			
			if (link12.get(i).getText().equalsIgnoreCase(prop.getProperty("bgtname")))
			{
				ob.debug("Budget Saved Successfully - " + link12.get(i).getText());
				flag=1;
			}	
		}
		if(flag==0)
		{
			ob.debug("Budget not saved Successfully");
		}
	
		Thread.sleep(5000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");			
		
		ob.debug("-----------------------------------------------Exiting Manage >> Budget Module");

// **********************************Ending Of The Budget Block**********************************
// Milestones 
// **********************************Beginning Of The Add Milestones Block******************************************************		
	
		ob.debug("-----------------------------------------------Entering Manage >> Milestones Module-------------------------------------------------");
		
		Thread.sleep(15000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
					
		Thread.sleep(10000);
		//driver.get(prop.getProperty("testurl") + "/velos/jsp/milestonehome.jsp?srcmenu=tdmenubaritem7");
		
		driver.findElement(By.linkText("Manage")).click();
		//Thread.sleep(1000);
		
	    driver.findElement(By.xpath("(//a[contains(text(),'Search')])[3]")).click();
	    Thread.sleep(1000);	
		
		Thread.sleep(5000);
		driver.findElement(By.name("searchCriteria")).sendKeys(prop.getProperty("Studyname"));
		
		Thread.sleep(5000);
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		
		Thread.sleep(5000);
		driver.findElement(By.linkText(prop.getProperty("Studyname"))).click();
		
		//Patient Milestone
		
		//Thread.sleep(5000);
		driver.findElement(By.cssSelector("img[title=\"Add New\"]")).click();
		
		Thread.sleep(5000);
		winid = driver.getWindowHandles();
		iter =winid.iterator();
		main  =  (String) iter.next();
		tab = (String) iter.next();
		String tab113 = (String) iter.next();
		
		//Thread.sleep(2000);
		driver.switchTo().window(tab113);
		
		Thread.sleep(5000);
		driver.findElement(By.name("count")).sendKeys("1");
		
		//Thread.sleep(2000);
		new Select(driver.findElement(By.name("dStatus"))).selectByVisibleText(prop.getProperty("patstudystatus"));
				
		//Thread.sleep(2000);
		driver.findElement(By.name("amountNum")).sendKeys(prop.getProperty("mileamountPM"));
				
		//Thread.sleep(2000);
		new Select(driver.findElement(By.name("milestoneStat"))).selectByVisibleText(prop.getProperty("milestatus"));
		
		//Thread.sleep(2000);
		driver.findElement(By.id("submit_btn")).click();

		Thread.sleep(2000);
		driver.switchTo().window(tab);
		
	    //visit milestones
		
		Thread.sleep(5000);
		driver.findElement(By.xpath("(//img[@title='Add New'])[2]")).click();
		
		Thread.sleep(5000);
		winid = driver.getWindowHandles();
		iter =winid.iterator();
		main  =  (String) iter.next();
		tab = (String) iter.next();
		String tab114 = (String) iter.next();
			
		Thread.sleep(2000);
		driver.switchTo().window(tab114);
		
		Thread.sleep(5000);
		new Select(driver.findElement(By.name("protocolId"))).selectByVisibleText(prop.getProperty("calname"));
		
		Thread.sleep(2000);
		driver.findElement(By.xpath("//button[@type='submit']")).submit();
		
		//Thread.sleep(5000);
		new Select(driver.findElement(By.name("dVisit"))).selectByVisibleText(prop.getProperty("VisitName"));
		
		//Thread.sleep(5000);
		new Select(driver.findElement(By.name("rule"))).selectByVisibleText(prop.getProperty("VMmileRule"));
		
		//Thread.sleep(5000);
		new Select(driver.findElement(By.name("dEventStatus"))).selectByVisibleText(prop.getProperty("visitstatus"));
		
		//Thread.sleep(5000);
		driver.findElement(By.name("amountNum")).sendKeys(prop.getProperty("mileamountVM"));
		
		//Thread.sleep(5000);
		new Select(driver.findElement(By.name("milestoneStat"))).selectByVisibleText(prop.getProperty("milestatus"));
		
		//Thread.sleep(5000);
		driver.findElement(By.id("submit_btn")).click(); 
		
		Thread.sleep(2000);
		driver.switchTo().window(tab);
		
		//event Milestones
		
		Thread.sleep(5000);
		driver.findElement(By.xpath("(//img[@title='Add New'])[3]")).click();
		
		Thread.sleep(5000);
		winid = driver.getWindowHandles();
		iter =winid.iterator();
		main  =  (String) iter.next();
		tab = (String) iter.next();
		String tab115 = (String) iter.next();
			
		Thread.sleep(2000);
		driver.switchTo().window(tab115);
		
		Thread.sleep(5000);
		new Select(driver.findElement(By.name("protocolId"))).selectByVisibleText(prop.getProperty("calname"));
		
		Thread.sleep(2000);
		driver.findElement(By.xpath("//button[@type='submit']")).click();
			
		Thread.sleep(5000);
		//new Select(driver.findElement(By.id("id_eventid_0"))).selectByVisibleText(prop.getProperty("eventname")+"["+prop.getProperty("VisitName")+"]");
		
		String EventName = prop.getProperty("eventname")+"["+prop.getProperty("VisitName")+"]";
		System.out.println(EventName);
		String EventName1 = EventName.substring(0, 27)+ "...";
		System.out.println(EventName1);
		
		new Select(driver.findElement(By.id("id_eventid_0"))).selectByVisibleText(EventName1);
	
		//Thread.sleep(5000);
		new Select(driver.findElement(By.name("rule"))).selectByVisibleText(prop.getProperty("mileeventtype"));

		//Thread.sleep(5000);
		new Select(driver.findElement(By.name("dEventStatus"))).selectByVisibleText(prop.getProperty("visitstatus"));
		
		//Thread.sleep(5000);
		driver.findElement(By.name("amountNum")).sendKeys(prop.getProperty("mileamountEM"));
		
		//Thread.sleep(5000);
		new Select(driver.findElement(By.name("milestoneStat"))).selectByVisibleText(prop.getProperty("milestatus"));
		
		//Thread.sleep(5000);
		driver.findElement(By.id("submit_btn")).click();
		
		Thread.sleep(2000);
		driver.switchTo().window(tab); 
				
		//Study Milestone
		Thread.sleep(5000);
		driver.findElement(By.xpath("(//img[@title='Add New'])[4]")).click();
		
		Thread.sleep(5000);
		winid = driver.getWindowHandles();
		iter =winid.iterator();
		main  =  (String) iter.next();
		tab = (String) iter.next();
		String tab116 = (String) iter.next();
			
		Thread.sleep(2000);
		driver.switchTo().window(tab116);
		
		Thread.sleep(5000);
		new Select(driver.findElement(By.name("dStatus"))).selectByVisibleText(prop.getProperty("studysetstatus"));
		
		Thread.sleep(2000);
		driver.findElement(By.name("amountNum")).sendKeys(prop.getProperty("mileamountSM"));

		//Thread.sleep(2000);
		new Select(driver.findElement(By.name("milestoneStat"))).selectByVisibleText(prop.getProperty("milestatus"));
		
		//Thread.sleep(2000);
		driver.findElement(By.id("submit_btn")).click();
		
		Thread.sleep(2000);
		driver.switchTo().window(tab);
		
		//Additional Milestone
		
		Thread.sleep(5000);
		driver.findElement(By.xpath("(//img[@title='Add New'])[5]")).click();
		
		Thread.sleep(5000);
		winid = driver.getWindowHandles();
		iter =winid.iterator();
		main  =  (String) iter.next();
		tab = (String) iter.next();
		String tab121 = (String) iter.next();
			
		Thread.sleep(2000);
		driver.switchTo().window(tab121);
		
		Thread.sleep(5000);
		driver.findElement(By.name("description")).sendKeys(prop.getProperty("addmilestonename"));

		//Thread.sleep(5000);
		driver.findElement(By.name("amountNum")).sendKeys(prop.getProperty("mileamountAM"));
		
		//Thread.sleep(5000);
		new Select(driver.findElement(By.name("milestoneStat"))).selectByVisibleText(prop.getProperty("milestatus"));
		
		//Thread.sleep(5000);
		driver.findElement(By.id("submit_btn")).click();
		
		//Thread.sleep(2000);
		driver.switchTo().window(tab);
		
		Thread.sleep(15000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
		
		//verifying if the Milestones are saved
		
		Thread.sleep(15000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
				
		Thread.sleep(10000);
		//driver.get(prop.getProperty("testurl") + "/velos/jsp/milestonehome.jsp?srcmenu=tdmenubaritem7");
		
		driver.findElement(By.linkText("Manage")).click();
		//Thread.sleep(1000);
		
	    driver.findElement(By.xpath("(//a[contains(text(),'Search')])[3]")).click();
	    Thread.sleep(1000);	
		
		Thread.sleep(5000);
		driver.findElement(By.name("searchCriteria")).sendKeys(prop.getProperty("Studyname"));
		
		Thread.sleep(5000);
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		
		Thread.sleep(5000);
		driver.findElement(By.linkText(prop.getProperty("Studyname"))).click();
		
		ob.debug("Verifying if the Milestone is saved or not");
		
		Thread.sleep(10000);
		new Select(driver.findElement(By.name("milestoneType"))).selectByVisibleText("Patient Status Milestones");
		
		//Thread.sleep(2000);
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		
		Thread.sleep(5000);
		List<WebElement> link100 = driver.findElements(By.tagName("td"));
		flag=0;
		
		for(int i = 0;i<link100.size();i++)
		{
			if (link100.get(i).getText().equalsIgnoreCase(prop.getProperty("milestatus")))
			{
				ob.debug("Patient Milestone Saved Successfully with status - " + link100.get(i).getText());
				flag=1;
			}	
		}
		if(flag==0)
		{
			ob.debug("Patient Milestone not saved Successfully");
		}
		
		Thread.sleep(10000);
		new Select(driver.findElement(By.name("milestoneType"))).selectByVisibleText("Visit Milestones");
		
		Thread.sleep(2000);
		driver.findElement(By.xpath("//button[@type='submit']")).click();
			
		List<WebElement> link101 = driver.findElements(By.tagName("td"));
		flag=0;
		
		for(int i = 0;i<link101.size();i++)
		{
			if (link101.get(i).getText().equalsIgnoreCase(prop.getProperty("VisitName")))
			{
				ob.debug("Visit Milestone Saved Successfully - " + link101.get(i).getText());
				flag=1;
			}	
		}
		if(flag==0)
		{
			ob.debug("Study Milestone not saved Successfully");
		}
		
		Thread.sleep(10000);
		new Select(driver.findElement(By.name("milestoneType"))).selectByVisibleText("Event Milestones");
		
		Thread.sleep(2000);
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		
		Thread.sleep(5000);
		
		link101.clear();
		
		link101 = driver.findElements(By.tagName("td"));
		flag=0;
		
		for(int i = 0;i<link101.size();i++)
		{
			if (link101.get(i).getText().equalsIgnoreCase(prop.getProperty("eventname")))
			{
				ob.debug("Event Milestone Saved Successfully - " + link101.get(i).getText());
				flag=1;
			}	
		}
		if(flag==0)
		{
			ob.debug("Event Milestone not saved Successfully");
		}		
		
		String mile="Status: " +"'"+prop.getProperty("studysetstatus")+"'";
		
		//Thread.sleep(10000);
		new Select(driver.findElement(By.name("milestoneType"))).selectByVisibleText("Study Status Milestones");
		
		Thread.sleep(2000);
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		
		Thread.sleep(5000);
		
		link101.clear();
		
		link101 = driver.findElements(By.tagName("a"));
		flag=0;
		
		for(int i = 0;i<link101.size();i++)
		{
			if (link101.get(i).getText().equalsIgnoreCase(mile))
			{
				ob.debug("Study Milestone Saved Successfully - " + link101.get(i).getText());
				flag=1;
			}	
		}
		if(flag==0)
		{
			ob.debug("Study Milestone not saved Successfully");
		}
		
		Thread.sleep(10000);
		new Select(driver.findElement(By.name("milestoneType"))).selectByVisibleText("Additional Milestones");
		
		//Thread.sleep(2000);
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		
		Thread.sleep(5000);
		
		link101.clear();
		
		link101 = driver.findElements(By.tagName("a"));
		flag=0;
		
		for(int i = 0;i<link101.size();i++)
		{
			if (link101.get(i).getText().equalsIgnoreCase(prop.getProperty("addmilestonename")))
			{
				ob.debug("Additional Milestone Saved Successfully - " + link101.get(i).getText());
				flag=1;
			}	
		}
		if(flag==0)
		{
			ob.debug("Additional Milestone not saved Successfully");
		}

		Thread.sleep(15000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
				
		//adding notifications
		
		Thread.sleep(15000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");
				
		Thread.sleep(10000);
		//driver.get(prop.getProperty("testurl") + "/velos/jsp/milestonehome.jsp?srcmenu=tdmenubaritem7");
		
		driver.findElement(By.linkText("Manage")).click();
		//Thread.sleep(1000);
		
	    driver.findElement(By.xpath("(//a[contains(text(),'Search')])[3]")).click();
	    Thread.sleep(1000);	
		
		Thread.sleep(5000);
		driver.findElement(By.name("searchCriteria")).sendKeys(prop.getProperty("Studyname"));
		
		Thread.sleep(5000);
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		
		Thread.sleep(5000);
		driver.findElement(By.linkText(prop.getProperty("Studyname"))).click();
		
		ob.debug("Adding Notifications to Milestones");
		
		Thread.sleep(5000);
		driver.findElement(By.linkText("Notifications")).click();
		
		Thread.sleep(5000);
		driver.findElement(By.linkText("Select/Deselect User(s)")).click();
		
		Thread.sleep(5000);
		winid = driver.getWindowHandles();
		iter =winid.iterator();
		main  =  (String) iter.next();
		tab = (String) iter.next();
		String tab117 = (String) iter.next();
			
		Thread.sleep(2000);
		driver.switchTo().window(tab117);
		
		ob.debug("Adding Notifications to Patient Milestones");
	
		Thread.sleep(5000);
		driver.findElement(By.name("usercheckall")).click();
		
		//Thread.sleep(2000);
		driver.findElement(By.id("submit_btn")).click();
		
		Thread.sleep(2000);
		driver.switchTo().window(tab);
		
		ob.debug("Adding Visit Milestone notification");
		
		Thread.sleep(5000);
		driver.findElement(By.xpath("(//a[contains(text(),'Select/Deselect User(s)')])[2]")).click();
		
		Thread.sleep(5000);
		winid = driver.getWindowHandles();
		iter =winid.iterator();
		main  =  (String) iter.next();
		tab = (String) iter.next();
		String tab118 = (String) iter.next();
			
		Thread.sleep(2000);
		driver.switchTo().window(tab118);
		
		Thread.sleep(5000);
		driver.findElement(By.name("usercheckall")).click();
		
		Thread.sleep(2000);
		driver.findElement(By.id("submit_btn")).click();
		
		Thread.sleep(2000);
		driver.switchTo().window(tab);

		Thread.sleep(2000);
		ob.debug("Adding Event Milestone Notification");
		
		Thread.sleep(5000);
		driver.findElement(By.xpath("(//a[contains(text(),'Select/Deselect User(s)')])[3]")).click();
		
		Thread.sleep(5000);
		winid = driver.getWindowHandles();
		iter =winid.iterator();
		main  =  (String) iter.next();
		tab = (String) iter.next();
		String tab119 = (String) iter.next();
			
		Thread.sleep(2000);
		driver.switchTo().window(tab119);
		
		//Thread.sleep(5000);
		driver.findElement(By.name("usercheckall")).click();
		
		Thread.sleep(5000);
		driver.findElement(By.id("submit_btn")).click();
		
		Thread.sleep(2000);
		driver.switchTo().window(tab);
		
		ob.debug("Adding Study Milestone");
		
		//Thread.sleep(5000);
		driver.findElement(By.xpath("(//a[contains(text(),'Select/Deselect User(s)')])[4]")).click();
		
		Thread.sleep(5000);
		winid = driver.getWindowHandles();
		iter =winid.iterator();
		main  =  (String) iter.next();
		tab = (String) iter.next();
		String tab120 = (String) iter.next();
			
		Thread.sleep(2000);
		driver.switchTo().window(tab120);
		
		//Thread.sleep(5000);
		driver.findElement(By.name("usercheckall")).click();
		
		Thread.sleep(2000);
		driver.findElement(By.id("submit_btn")).click();
		
		Thread.sleep(2000);
		driver.switchTo().window(tab);
		
		//adding Additional Milestone notification
		
		Thread.sleep(2000);
		ob.debug("adding Additional Miletone notification");
		
		//Thread.sleep(2000);
		driver.findElement(By.xpath("(//a[contains(text(),'Select/Deselect User(s)')])[5]")).click();
		
		Thread.sleep(5000);
		winid = driver.getWindowHandles();
		iter =winid.iterator();
		main  =  (String) iter.next();
		tab = (String) iter.next();
		String tab122 = (String) iter.next();
			
		Thread.sleep(2000);
		driver.switchTo().window(tab122);
		
		Thread.sleep(5000);
		driver.findElement(By.name("usercheckall")).click();
		
		//Thread.sleep(2000);
		driver.findElement(By.id("submit_btn")).click();
		
		Thread.sleep(2000);
		driver.switchTo().window(tab);
		
		ob.debug("Added Milestone notication");
				
		Thread.sleep(5000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("You are on "+ driver.getTitle()+ " page");

		ob.debug("-----------------------------------------------Exiting Manage >> Milestones Module");

		// ##########################################Ending Of The Budget Block#######################################################
		//ADHoc Queries
		//##########################################Starting Of the AdHoc Query Module############################################### 
	/*	
		ob.debug("---------------------------------Adding The AdHoc Query Module-----------------------------------------------");
		
		Thread.sleep(10000);
		driver.get(prop.getProperty("testurl") + "/velos/jsp/dynrepbrowse.jsp?srcmenu=tdmenubaritem12&selectedTab=1");			
		ob.debug("You are on " + driver.getTitle()+ " page");	
		
		Thread.sleep(5000);
		driver.findElement(By.linkText("CREATE REPORT")).click();
		
		ob.debug("Adding a New Report");
		
		Thread.sleep(2000);
		driver.findElement(By.name("dynType")).sendKeys(prop.getProperty("Patient"));
		
		Thread.sleep(2000);
		driver.findElement(By.xpath("//button[@type='submit']")).click();
				
		ob.debug("You are on Select Fields Page");
		
		Thread.sleep(5000);
		driver.findElement(By.name("repUseUnique")).click();
		
		Thread.sleep(5000);
		driver.findElement(By.name("repUseDataVal")).click();
		
		Thread.sleep(5000);
		driver.findElement(By.name("All")).click();
		
		Thread.sleep(2000);
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		
		ob.debug("You are on Filter Criteria Page");
		
		Thread.sleep(2000);
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		
		ob.debug("You are on Sort Criteria Page");
		
		Thread.sleep(2000);
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		
		ob.debug("You are on Preview & Save Page");
		
		ob.debug("adding new Report " + prop.getProperty("ReportName"));
		
		Thread.sleep(4000);
		driver.findElement(By.name("repName")).sendKeys(prop.getProperty("ReportName"));
		
		Thread.sleep(2000);
		driver.findElement(By.id("eSign")).sendKeys(prop.getProperty("esign"));
		
		Thread.sleep(2000);
		driver.findElement(By.id("submit_btn")).click();
		
		//Verifying if the Report saved successfully

		ob.debug("***Checking if the Report created above is saved or not***");
		
		Thread.sleep(5000);
		driver.get(prop.getProperty("testurl") + "/velos/jsp/dynrepbrowse.jsp?srcmenu=tdmenubaritem12&selectedTab=1");
		
		ob.debug("You are on " + driver.getTitle()+ " page");
			
		Thread.sleep(5000);
		
		//List<WebElement> link = driver.findElements(By.tagName("a")); //hanged "a" tag to "td"
		List<WebElement> link123 = driver.findElements(By.tagName("td"));
		
		
		for(int i = 0;i<link123.size();i++)
			{
				if (link123.get(i).getText().equalsIgnoreCase(prop.getProperty("ReportName")))
				{
					ob.debug("Report is saved successfully - " + link123.get(i).getText());
					flag=1;
				}	
			}
		if(flag==0)
		{
			ob.debug("Report is not saved, please try to save again.");
		}
		
		ob.debug("Exiting AdHoc Query Module");
		
		Thread.sleep(10000);
		driver.findElement(By.cssSelector("img[title=\"Homepage\"]")).click();
		
		ob.debug("-------------------Exiting AdHoc Query Module---------------------------");
*/
		// #######################################Ending Of The Adhoc Block###############################################
		
		ob.debug("Ending Test here");
		
		Thread.sleep(5000);
		driver.findElement(By.cssSelector("img[title=\"Logout\"]")).click();

	}

	@After
	public void tearDown() throws Exception {
		
		Thread.sleep(5000);
		driver.quit();
		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			fail(verificationErrorString);
		}
	}

	private void fail(String verificationErrorString) {
		// TODO Auto-generated method stub
		
	}

	private boolean isElementPresent(By by) {
		try {
			driver.findElement(by);
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}
}