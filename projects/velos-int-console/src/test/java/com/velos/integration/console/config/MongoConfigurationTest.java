package com.velos.integration.console.config;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MongoConfigurationTest {

	@Test
	public void bootstrapAppFromXml() {
		ApplicationContext context = new ClassPathXmlApplicationContext("META-INF/spring/repository-config.xml");
		assertThat(context, is(notNullValue()));
	}

}
