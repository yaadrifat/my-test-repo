package com.velos.integration.console.repository;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mongodb.Mongo;
import com.velos.integration.console.domain.Customer;
import com.velos.integration.console.domain.EmailAddress;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:META-INF/spring/repository-config.xml"})
public class MongoCustomerRepositoryTest {
	
	@Autowired
	Mongo mongo;
	
	@Autowired
	MongoCustomerRepository repository;
	
	@Autowired
	CustomerRepository customRepository;

	@Test
	public void clearsCustomer() {
		Customer adam = repository.findByFirstname("Adam");
		if (adam != null && "Test".equals(adam.getLastname())) {
			repository.delete(adam);
		}
	}
	
	@Test
	public void savesCustomer() {
		Customer adam = new Customer("Adam", "Test");
		EmailAddress emailAddress = new EmailAddress("no-reply@velos.com");
		adam.setEmailAddress(emailAddress);
		Customer result = repository.save(adam);
		assertThat(result.getId(), is(notNullValue()));
	}
	
	@Test
	public void checksFirstName() {
		Customer adam = repository.findByFirstname("Adam");
		assertThat(adam.getId(), is(notNullValue()));
	}
	
	@Test
	public void checksEmailAddress() {
		EmailAddress emailAddress = new EmailAddress("no-reply@velos.com");
		Customer adam = customRepository.findByEmailAddress(emailAddress);
		assertThat(adam.getId(), is(notNullValue()));
	}
	
}
