package com.velos.integration.console.repository;

import org.springframework.data.repository.Repository;

import com.velos.integration.console.domain.Account;


/**
 * Repository for working with {@link Account} domain objects
 * 
 * @author Marten Deinum
 * @author Koen Serneels
 *
 */
public interface AccountRepository extends Repository<Account, Long>{

    Account findByUsername(String username);

    Account findById(Long id);

    Account save(Account account);

}
