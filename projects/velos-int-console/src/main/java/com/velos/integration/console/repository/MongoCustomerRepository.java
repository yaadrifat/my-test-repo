package com.velos.integration.console.repository;


import org.springframework.data.mongodb.repository.MongoRepository;

import com.velos.integration.console.domain.Customer;

public interface MongoCustomerRepository extends MongoRepository<Customer, String> {
	
    public Customer findByFirstname(String firstname);

}
