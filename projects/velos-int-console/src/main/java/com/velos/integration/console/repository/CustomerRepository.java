package com.velos.integration.console.repository;

import org.springframework.data.repository.Repository;

import com.velos.integration.console.domain.Customer;
import com.velos.integration.console.domain.EmailAddress;

public interface CustomerRepository extends Repository<Customer, Long> {
	Customer findOne(Long id);
	Customer save(Customer customer);
	Customer findByEmailAddress(EmailAddress emailAddress);
}
