package com.velos.integration.console.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.velos.integration.console.domain.Account;

public interface MongoAccountRepository extends MongoRepository<Account, Long> {
    Account findByUsername(String username);

    Account findById(Long id);
    
    @SuppressWarnings("unchecked")
	public Account save(Account account);
}
