package com.velos.integration.console.service;

import java.util.List;

import com.velos.integration.console.domain.Category;

/**
 * Contract for services that work with an {@link Category}.
 * 
 * @author Marten Deinum
 * @author Koen Serneels
 * 
 */
public interface CategoryService {

	Category findById(long id);

	List<Category> findAll();

	void addCategory(Category category);

}
