package com.velos.integration.console.web.controller;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.velos.integration.console.domain.Book;
import com.velos.integration.console.domain.BookSearchCriteria;
import com.velos.integration.console.domain.Category;
import com.velos.integration.console.service.BookstoreService;

/**
 * Controller to handle book search requests.
 * 
 * @author Marten Deinum
 * @author Koen Serneels
 *
 */
@Controller
public class BookSearchController {

    @Autowired
    private BookstoreService bookstoreService;

    @ModelAttribute
    public BookSearchCriteria criteria() {
        return new BookSearchCriteria();
    }

    @ModelAttribute("categories")
    public List<Category> getCategories() {
        return this.bookstoreService.findAllCategories();
    }

    /**
     * This method searches our database for books based on the given {@link BookSearchCriteria}. 
     * Only books matching the criteria are returned.
     * 
     * @param criteria the criteria used for searching
     * @return the found books
     * 
     * @see com.velos.integration.console.repository.BookRepository#findBooks(BookSearchCriteria)
     */
    @RequestMapping(value = "/book/search", method = { RequestMethod.GET })
    public Collection<Book> list(@ModelAttribute("bookSearchCriteria") BookSearchCriteria criteria) {
        return this.bookstoreService.findBooks(criteria);
    }

}
