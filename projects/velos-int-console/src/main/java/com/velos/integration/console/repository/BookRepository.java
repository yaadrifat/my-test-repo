package com.velos.integration.console.repository;

import java.util.List;

import com.velos.integration.console.domain.Book;
import com.velos.integration.console.domain.BookSearchCriteria;
import com.velos.integration.console.domain.Category;

/**
 * Repository for working with {@link Book} domain objects
 * 
 * @author Marten Deinum
 * @author Koen Serneels
 *
 */
public interface BookRepository {

	Book findById(long id);

	List<Book> findByCategory(Category category);

	List<Book> findRandom(int count);

	List<Book> findBooks(BookSearchCriteria bookSearchCriteria);

	void storeBook(Book book);

}
