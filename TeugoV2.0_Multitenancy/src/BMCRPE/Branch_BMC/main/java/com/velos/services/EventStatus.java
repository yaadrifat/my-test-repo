
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for eventStatus complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="eventStatus">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="coverageType" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="eventName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="eventStatusCode" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="notes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="reasonForChange" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="reasonForChangeCoverType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="siteOfService" type="{http://velos.com/services/}organizationIdentifier" minOccurs="0"/>
 *         &lt;element name="statusValidFrom" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "eventStatus", propOrder = {
    "coverageType",
    "eventName",
    "eventStatusCode",
    "notes",
    "reasonForChange",
    "reasonForChangeCoverType",
    "siteOfService",
    "statusValidFrom"
})
public class EventStatus {

    protected Code coverageType;
    protected String eventName;
    protected Code eventStatusCode;
    protected String notes;
    protected String reasonForChange;
    protected String reasonForChangeCoverType;
    protected OrganizationIdentifier siteOfService;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar statusValidFrom;

    /**
     * Gets the value of the coverageType property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getCoverageType() {
        return coverageType;
    }

    /**
     * Sets the value of the coverageType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setCoverageType(Code value) {
        this.coverageType = value;
    }

    /**
     * Gets the value of the eventName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventName() {
        return eventName;
    }

    /**
     * Sets the value of the eventName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventName(String value) {
        this.eventName = value;
    }

    /**
     * Gets the value of the eventStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getEventStatusCode() {
        return eventStatusCode;
    }

    /**
     * Sets the value of the eventStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setEventStatusCode(Code value) {
        this.eventStatusCode = value;
    }

    /**
     * Gets the value of the notes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNotes() {
        return notes;
    }

    /**
     * Sets the value of the notes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNotes(String value) {
        this.notes = value;
    }

    /**
     * Gets the value of the reasonForChange property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReasonForChange() {
        return reasonForChange;
    }

    /**
     * Sets the value of the reasonForChange property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReasonForChange(String value) {
        this.reasonForChange = value;
    }

    /**
     * Gets the value of the reasonForChangeCoverType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReasonForChangeCoverType() {
        return reasonForChangeCoverType;
    }

    /**
     * Sets the value of the reasonForChangeCoverType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReasonForChangeCoverType(String value) {
        this.reasonForChangeCoverType = value;
    }

    /**
     * Gets the value of the siteOfService property.
     * 
     * @return
     *     possible object is
     *     {@link OrganizationIdentifier }
     *     
     */
    public OrganizationIdentifier getSiteOfService() {
        return siteOfService;
    }

    /**
     * Sets the value of the siteOfService property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganizationIdentifier }
     *     
     */
    public void setSiteOfService(OrganizationIdentifier value) {
        this.siteOfService = value;
    }

    /**
     * Gets the value of the statusValidFrom property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStatusValidFrom() {
        return statusValidFrom;
    }

    /**
     * Sets the value of the statusValidFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStatusValidFrom(XMLGregorianCalendar value) {
        this.statusValidFrom = value;
    }

}
