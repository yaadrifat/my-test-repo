
package com.velos.services;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for linkedTo.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="linkedTo">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="LIBRARY"/>
 *     &lt;enumeration value="STUDY"/>
 *     &lt;enumeration value="ACCOUNT"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "linkedTo")
@XmlEnum
public enum LinkedTo {

    LIBRARY,
    STUDY,
    ACCOUNT;

    public String value() {
        return name();
    }

    public static LinkedTo fromValue(String v) {
        return valueOf(v);
    }

}
