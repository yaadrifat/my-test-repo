package com.velos.integration.espclient;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import com.velos.services.ObjectInfo;
import com.velos.services.OperationException_Exception;
import com.velos.services.SimpleIdentifier;

@WebService(targetNamespace="http://velos.com/services/", name="PatientStudySEI")
public interface VelosEspPatStudyEndpoint {
		
	@WebResult(name = "PatientStudy", targetNamespace = "")
	@RequestWrapper(localName = "getPatientStudyInfo", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetPatientStudyInfo")
	@WebMethod
	@ResponseWrapper(localName = "getPatientStudyInfoResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetPatientStudyInfoResponse")
	public com.velos.services.PatientProtocolIdentifier getPatientStudyInfo(
			@WebParam(name = "patProtId", targetNamespace = "")
			java.lang.Integer patProtId
			) throws OperationException_Exception;
	
}
