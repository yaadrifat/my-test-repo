
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for studyFormDesign complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="studyFormDesign">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}linkedFormDesign">
 *       &lt;sequence>
 *         &lt;element name="studyIdentifier" type="{http://velos.com/services/}studyIdentifier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "studyFormDesign", propOrder = {
    "studyIdentifier"
})
public class StudyFormDesign
    extends LinkedFormDesign
{

    protected StudyIdentifier studyIdentifier;

    /**
     * Gets the value of the studyIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link StudyIdentifier }
     *     
     */
    public StudyIdentifier getStudyIdentifier() {
        return studyIdentifier;
    }

    /**
     * Sets the value of the studyIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyIdentifier }
     *     
     */
    public void setStudyIdentifier(StudyIdentifier value) {
        this.studyIdentifier = value;
    }

}
