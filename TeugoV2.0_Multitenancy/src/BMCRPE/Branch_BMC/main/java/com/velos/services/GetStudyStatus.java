
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getStudyStatus complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getStudyStatus">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StudyStatusIdentifier" type="{http://velos.com/services/}studyStatusIdentifier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getStudyStatus", propOrder = {
    "studyStatusIdentifier"
})
public class GetStudyStatus {

    @XmlElement(name = "StudyStatusIdentifier")
    protected StudyStatusIdentifier studyStatusIdentifier;

    /**
     * Gets the value of the studyStatusIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link StudyStatusIdentifier }
     *     
     */
    public StudyStatusIdentifier getStudyStatusIdentifier() {
        return studyStatusIdentifier;
    }

    /**
     * Sets the value of the studyStatusIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyStatusIdentifier }
     *     
     */
    public void setStudyStatusIdentifier(StudyStatusIdentifier value) {
        this.studyStatusIdentifier = value;
    }

}
