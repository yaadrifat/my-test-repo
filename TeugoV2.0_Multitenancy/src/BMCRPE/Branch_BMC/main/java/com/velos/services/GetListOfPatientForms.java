
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getListOfPatientForms complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getListOfPatientForms">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PatientIdentifier" type="{http://velos.com/services/}patientIdentifier" minOccurs="0"/>
 *         &lt;element name="formHasResponses" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ignoreDisplayInPatFlag" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="pageNumber" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="pageSize" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getListOfPatientForms", propOrder = {
    "patientIdentifier",
    "formHasResponses",
    "ignoreDisplayInPatFlag",
    "pageNumber",
    "pageSize"
})
public class GetListOfPatientForms {

    @XmlElement(name = "PatientIdentifier")
    protected PatientIdentifier patientIdentifier;
    protected boolean formHasResponses;
    protected boolean ignoreDisplayInPatFlag;
    protected int pageNumber;
    protected int pageSize;

    /**
     * Gets the value of the patientIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link PatientIdentifier }
     *     
     */
    public PatientIdentifier getPatientIdentifier() {
        return patientIdentifier;
    }

    /**
     * Sets the value of the patientIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientIdentifier }
     *     
     */
    public void setPatientIdentifier(PatientIdentifier value) {
        this.patientIdentifier = value;
    }

    /**
     * Gets the value of the formHasResponses property.
     * 
     */
    public boolean isFormHasResponses() {
        return formHasResponses;
    }

    /**
     * Sets the value of the formHasResponses property.
     * 
     */
    public void setFormHasResponses(boolean value) {
        this.formHasResponses = value;
    }

    /**
     * Gets the value of the ignoreDisplayInPatFlag property.
     * 
     */
    public boolean isIgnoreDisplayInPatFlag() {
        return ignoreDisplayInPatFlag;
    }

    /**
     * Sets the value of the ignoreDisplayInPatFlag property.
     * 
     */
    public void setIgnoreDisplayInPatFlag(boolean value) {
        this.ignoreDisplayInPatFlag = value;
    }

    /**
     * Gets the value of the pageNumber property.
     * 
     */
    public int getPageNumber() {
        return pageNumber;
    }

    /**
     * Sets the value of the pageNumber property.
     * 
     */
    public void setPageNumber(int value) {
        this.pageNumber = value;
    }

    /**
     * Gets the value of the pageSize property.
     * 
     */
    public int getPageSize() {
        return pageSize;
    }

    /**
     * Sets the value of the pageSize property.
     * 
     */
    public void setPageSize(int value) {
        this.pageSize = value;
    }

}
