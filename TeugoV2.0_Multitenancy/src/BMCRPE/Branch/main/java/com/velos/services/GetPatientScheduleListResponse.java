
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getPatientScheduleListResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getPatientScheduleListResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PatientSchedules" type="{http://velos.com/services/}patientSchedules" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getPatientScheduleListResponse", propOrder = {
    "patientSchedules"
})
@XmlRootElement(name="getPatientScheduleListResponse", namespace = "http://velos.com/services/")
public class GetPatientScheduleListResponse {

    @XmlElement(name = "PatientSchedules")
    protected PatientSchedules patientSchedules;

    /**
     * Gets the value of the patientSchedules property.
     * 
     * @return
     *     possible object is
     *     {@link PatientSchedules }
     *     
     */
    public PatientSchedules getPatientSchedules() {
        return patientSchedules;
    }

    /**
     * Sets the value of the patientSchedules property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientSchedules }
     *     
     */
    public void setPatientSchedules(PatientSchedules value) {
        this.patientSchedules = value;
    }

}
