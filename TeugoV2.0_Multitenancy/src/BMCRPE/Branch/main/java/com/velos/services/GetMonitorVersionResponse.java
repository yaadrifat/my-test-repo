
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getMonitorVersionResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getMonitorVersionResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MonitorVersion" type="{http://velos.com/services/}monitorVersion" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getMonitorVersionResponse", propOrder = {
    "monitorVersion"
})
public class GetMonitorVersionResponse {

    @XmlElement(name = "MonitorVersion")
    protected MonitorVersion monitorVersion;

    /**
     * Gets the value of the monitorVersion property.
     * 
     * @return
     *     possible object is
     *     {@link MonitorVersion }
     *     
     */
    public MonitorVersion getMonitorVersion() {
        return monitorVersion;
    }

    /**
     * Sets the value of the monitorVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link MonitorVersion }
     *     
     */
    public void setMonitorVersion(MonitorVersion value) {
        this.monitorVersion = value;
    }

}
