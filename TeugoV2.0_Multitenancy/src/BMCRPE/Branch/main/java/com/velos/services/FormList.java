
package com.velos.services;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for formList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="formList">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="formCount" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="formInfo" type="{http://velos.com/services/}formInfo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "formList", propOrder = {
    "formCount",
    "formInfo"
})
public class FormList
    extends ServiceObject
{

    protected int formCount;
    @XmlElement(nillable = true)
    protected List<FormInfo> formInfo;

    /**
     * Gets the value of the formCount property.
     * 
     */
    public int getFormCount() {
        return formCount;
    }

    /**
     * Sets the value of the formCount property.
     * 
     */
    public void setFormCount(int value) {
        this.formCount = value;
    }

    /**
     * Gets the value of the formInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the formInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFormInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FormInfo }
     * 
     * 
     */
    public List<FormInfo> getFormInfo() {
        if (formInfo == null) {
            formInfo = new ArrayList<FormInfo>();
        }
        return this.formInfo;
    }

}
