package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getPatientDemographicsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getPatientDemographicsResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PatientDemographics" type="{http://velos.com/services/}patientDemographics" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getPatientDemographicsResponse", namespace = "http://velos.com/services/", propOrder = {
    "patientDemographics"
} )
@XmlRootElement(name="getPatientDemographicsResponse", namespace = "http://velos.com/services/")
public class GetPatientDemographicsResponse {

    @XmlElement(name = "PatientDemographics")
    protected PatientDemographics patientDemographics;

    /**
     * Gets the value of the patientDemographics property.
     * 
     * @return
     *     possible object is
     *     {@link PatientDemographics }
     *     
     */
    public PatientDemographics getPatientDemographics() {
        return patientDemographics;
    }

    /**
     * Sets the value of the patientDemographics property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientDemographics }
     *     
     */
    public void setPatientDemographics(PatientDemographics value) {
        this.patientDemographics = value;
    }

}
