
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createStudyPatientFormResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createStudyPatientFormResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StudyPatientFormResponse" type="{http://velos.com/services/}studyPatientFormResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createStudyPatientFormResponse", propOrder = {
    "studyPatientFormResponse"
})
public class CreateStudyPatientFormResponse {

    @XmlElement(name = "StudyPatientFormResponse")
    protected StudyPatientFormResponse studyPatientFormResponse;

    /**
     * Gets the value of the studyPatientFormResponse property.
     * 
     * @return
     *     possible object is
     *     {@link StudyPatientFormResponse }
     *     
     */
    public StudyPatientFormResponse getStudyPatientFormResponse() {
        return studyPatientFormResponse;
    }

    /**
     * Sets the value of the studyPatientFormResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyPatientFormResponse }
     *     
     */
    public void setStudyPatientFormResponse(StudyPatientFormResponse value) {
        this.studyPatientFormResponse = value;
    }

}
