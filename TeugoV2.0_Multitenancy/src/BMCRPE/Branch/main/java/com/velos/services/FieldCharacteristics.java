
package com.velos.services;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for fieldCharacteristics.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="fieldCharacteristics">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="VISIBLE"/>
 *     &lt;enumeration value="HIDE"/>
 *     &lt;enumeration value="DISABLED"/>
 *     &lt;enumeration value="READONLY"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "fieldCharacteristics")
@XmlEnum
public enum FieldCharacteristics {

    VISIBLE,
    HIDE,
    DISABLED,
    READONLY;

    public String value() {
        return name();
    }

    public static FieldCharacteristics fromValue(String v) {
        return valueOf(v);
    }

}
