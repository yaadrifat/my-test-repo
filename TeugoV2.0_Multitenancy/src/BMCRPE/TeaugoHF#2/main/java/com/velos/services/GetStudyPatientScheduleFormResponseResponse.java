
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getStudyPatientScheduleFormResponseResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getStudyPatientScheduleFormResponseResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StudyPatientScheduleFormResponse" type="{http://velos.com/services/}studyPatientScheduleFormResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getStudyPatientScheduleFormResponseResponse", propOrder = {
    "studyPatientScheduleFormResponse"
})
public class GetStudyPatientScheduleFormResponseResponse {

    @XmlElement(name = "StudyPatientScheduleFormResponse")
    protected StudyPatientScheduleFormResponse studyPatientScheduleFormResponse;

    /**
     * Gets the value of the studyPatientScheduleFormResponse property.
     * 
     * @return
     *     possible object is
     *     {@link StudyPatientScheduleFormResponse }
     *     
     */
    public StudyPatientScheduleFormResponse getStudyPatientScheduleFormResponse() {
        return studyPatientScheduleFormResponse;
    }

    /**
     * Sets the value of the studyPatientScheduleFormResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyPatientScheduleFormResponse }
     *     
     */
    public void setStudyPatientScheduleFormResponse(StudyPatientScheduleFormResponse value) {
        this.studyPatientScheduleFormResponse = value;
    }

}
