
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for removeStudyTeamMember complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="removeStudyTeamMember">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StudyIdentifier" type="{http://velos.com/services/}studyIdentifier" minOccurs="0"/>
 *         &lt;element name="StudyTeamUserIdentifier" type="{http://velos.com/services/}userIdentifier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "removeStudyTeamMember", propOrder = {
    "studyIdentifier",
    "studyTeamUserIdentifier"
})
public class RemoveStudyTeamMember {

    @XmlElement(name = "StudyIdentifier")
    protected StudyIdentifier studyIdentifier;
    @XmlElement(name = "StudyTeamUserIdentifier")
    protected UserIdentifier studyTeamUserIdentifier;

    /**
     * Gets the value of the studyIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link StudyIdentifier }
     *     
     */
    public StudyIdentifier getStudyIdentifier() {
        return studyIdentifier;
    }

    /**
     * Sets the value of the studyIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyIdentifier }
     *     
     */
    public void setStudyIdentifier(StudyIdentifier value) {
        this.studyIdentifier = value;
    }

    /**
     * Gets the value of the studyTeamUserIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link UserIdentifier }
     *     
     */
    public UserIdentifier getStudyTeamUserIdentifier() {
        return studyTeamUserIdentifier;
    }

    /**
     * Sets the value of the studyTeamUserIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserIdentifier }
     *     
     */
    public void setStudyTeamUserIdentifier(UserIdentifier value) {
        this.studyTeamUserIdentifier = value;
    }

}
