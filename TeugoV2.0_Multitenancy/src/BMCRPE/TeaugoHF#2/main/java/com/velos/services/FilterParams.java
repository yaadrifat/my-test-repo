
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for filterParams complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="filterParams">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="fieldIDs" type="{http://velos.com/services/}fieldID" minOccurs="0"/>
 *         &lt;element name="formPK" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "filterParams", propOrder = {
    "fieldIDs",
    "formPK"
})
public class FilterParams
    extends ServiceObject
{

    protected FieldID fieldIDs;
    protected Integer formPK;

    /**
     * Gets the value of the fieldIDs property.
     * 
     * @return
     *     possible object is
     *     {@link FieldID }
     *     
     */
    public FieldID getFieldIDs() {
        return fieldIDs;
    }

    /**
     * Sets the value of the fieldIDs property.
     * 
     * @param value
     *     allowed object is
     *     {@link FieldID }
     *     
     */
    public void setFieldIDs(FieldID value) {
        this.fieldIDs = value;
    }

    /**
     * Gets the value of the formPK property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFormPK() {
        return formPK;
    }

    /**
     * Sets the value of the formPK property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFormPK(Integer value) {
        this.formPK = value;
    }

}
