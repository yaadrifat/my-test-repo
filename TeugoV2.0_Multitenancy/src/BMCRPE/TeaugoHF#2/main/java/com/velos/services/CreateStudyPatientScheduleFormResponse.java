
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createStudyPatientScheduleFormResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createStudyPatientScheduleFormResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StudyPatientScheduleFormResponse" type="{http://velos.com/services/}studyPatientScheduleFormResponse" minOccurs="0"/>
 *         &lt;element name="CalendarIdentifier" type="{http://velos.com/services/}calendarIdentifier" minOccurs="0"/>
 *         &lt;element name="CalendarName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VisitIdentifier" type="{http://velos.com/services/}visitIdentifier" minOccurs="0"/>
 *         &lt;element name="VisitName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EventIdentifier" type="{http://velos.com/services/}eventIdentifier" minOccurs="0"/>
 *         &lt;element name="EventName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createStudyPatientScheduleFormResponse", propOrder = {
    "studyPatientScheduleFormResponse",
    "calendarIdentifier",
    "calendarName",
    "visitIdentifier",
    "visitName",
    "eventIdentifier",
    "eventName"
})
public class CreateStudyPatientScheduleFormResponse {

    @XmlElement(name = "StudyPatientScheduleFormResponse")
    protected StudyPatientScheduleFormResponse studyPatientScheduleFormResponse;
    @XmlElement(name = "CalendarIdentifier")
    protected CalendarIdentifier calendarIdentifier;
    @XmlElement(name = "CalendarName")
    protected String calendarName;
    @XmlElement(name = "VisitIdentifier")
    protected VisitIdentifier visitIdentifier;
    @XmlElement(name = "VisitName")
    protected String visitName;
    @XmlElement(name = "EventIdentifier")
    protected EventIdentifier eventIdentifier;
    @XmlElement(name = "EventName")
    protected String eventName;

    /**
     * Gets the value of the studyPatientScheduleFormResponse property.
     * 
     * @return
     *     possible object is
     *     {@link StudyPatientScheduleFormResponse }
     *     
     */
    public StudyPatientScheduleFormResponse getStudyPatientScheduleFormResponse() {
        return studyPatientScheduleFormResponse;
    }

    /**
     * Sets the value of the studyPatientScheduleFormResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyPatientScheduleFormResponse }
     *     
     */
    public void setStudyPatientScheduleFormResponse(StudyPatientScheduleFormResponse value) {
        this.studyPatientScheduleFormResponse = value;
    }

    /**
     * Gets the value of the calendarIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link CalendarIdentifier }
     *     
     */
    public CalendarIdentifier getCalendarIdentifier() {
        return calendarIdentifier;
    }

    /**
     * Sets the value of the calendarIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link CalendarIdentifier }
     *     
     */
    public void setCalendarIdentifier(CalendarIdentifier value) {
        this.calendarIdentifier = value;
    }

    /**
     * Gets the value of the calendarName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCalendarName() {
        return calendarName;
    }

    /**
     * Sets the value of the calendarName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCalendarName(String value) {
        this.calendarName = value;
    }

    /**
     * Gets the value of the visitIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link VisitIdentifier }
     *     
     */
    public VisitIdentifier getVisitIdentifier() {
        return visitIdentifier;
    }

    /**
     * Sets the value of the visitIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link VisitIdentifier }
     *     
     */
    public void setVisitIdentifier(VisitIdentifier value) {
        this.visitIdentifier = value;
    }

    /**
     * Gets the value of the visitName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVisitName() {
        return visitName;
    }

    /**
     * Sets the value of the visitName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVisitName(String value) {
        this.visitName = value;
    }

    /**
     * Gets the value of the eventIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link EventIdentifier }
     *     
     */
    public EventIdentifier getEventIdentifier() {
        return eventIdentifier;
    }

    /**
     * Sets the value of the eventIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link EventIdentifier }
     *     
     */
    public void setEventIdentifier(EventIdentifier value) {
        this.eventIdentifier = value;
    }

    /**
     * Gets the value of the eventName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventName() {
        return eventName;
    }

    /**
     * Sets the value of the eventName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventName(String value) {
        this.eventName = value;
    }

}
