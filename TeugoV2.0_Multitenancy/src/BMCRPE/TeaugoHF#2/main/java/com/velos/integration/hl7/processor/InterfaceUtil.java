package com.velos.integration.hl7.processor;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.velos.integration.hl7.bean.FieldMapBean;
import com.velos.integration.hl7.bean.MessageFieldBean;
import com.velos.integration.hl7.bean.SegmentBean;
import com.velos.integration.hl7.dao.MessageConfiguration;
import com.velos.integration.hl7.dao.MessageDao;
import com.velos.integration.hl7.notifications.HL7CustomException;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.util.Terser;


public class InterfaceUtil {
	private static Logger logger = Logger
			.getLogger(Hl7Processor.class);

	private MessageConfiguration messageconfiguration;
	Properties prop=new Properties();
	private MessageDao messagedao;

	public MessageConfiguration getMessageconfiguration() {
		return messageconfiguration;
	}
	public void setMessageconfiguration(MessageConfiguration messageconfiguration) {
		this.messageconfiguration = messageconfiguration;
	}
	public MessageDao getMessagedao() {
		return messagedao;
	}
	public void setMessagedao(MessageDao messagedao) {
		this.messagedao = messagedao;
	}
	
	

	public List<String> segmentList(Message input){
		
		List<String> segList=new ArrayList<String>();

		String[] segments =  input.toString().split("\r");

		for(String segName:segments){

			segList.add(segName.substring(0,3));
		}

		return segList;
	}
	

	// Getting Field Values  based on whether it is a single occurance or repeated field/segment
	public Map<String, Map> getFieldValue(Message input,Map messageMap) throws Exception,IOException{

		Map<String, Map> finalMap = new HashMap<String, Map>();
		Map<String,String> resultMap=new HashMap<String,String>();
		Map<String,String[]> repeatedValueMap=new HashMap<String,String[]>();
		Map<String,String> tableNameMap=new HashMap<String,String>();

		  
		try{

			InputStream inputStream = getClass().getClassLoader().getResourceAsStream("adt.properties");
			prop.load(inputStream);
			String messagetype=(String)messageMap.get("messagetype");
			
		List<String>segList=segmentList(input);


			for(MessageFieldBean mf:getMessageconfiguration().getMessageFieldsConfiguration()){
		
				if(mf.getMessageType().equals(messagetype)) {
					
					String terserPath=mf.getTerserPath();

					int  repeated = Integer.parseInt(mf.getRepeated()); 
					if(terserPath==null && mf.getMappingType() !=null){
						// Getting Field Mapping Values
						String mappedValue=getFieldValues(mf.getMappingType(),mf.getMappingField(),input);
						
						resultMap.put(mf.getColumnName(),mappedValue);
						
						

					}else if(segList.contains(mf.getSegmentName())){
						
						int repeat=0;
						String repeatedSegment="";
						String fieldValue = "";
						String repSegTableName="";
						for(SegmentBean sb:getMessageconfiguration().getSegmentConfigurationBean()){

							//segment Repeated value
							repeat=Integer.parseInt(sb.getRepeated());
							
							if(repeat>1 && sb.getSegmentName().equalsIgnoreCase(mf.getSegmentName())){
								repeatedSegment=sb.getSegmentName();
								repSegTableName=sb.getRepeatedSegmentTable();

								// method for Repeated Segment values
								String[] result=getRepeatedSegments(terserPath,input,repeat);
							
								repeatedValueMap.put(mf.getColumnName(),result);
								tableNameMap.put("RepSegTableName",repSegTableName);
								tableNameMap.put("RepeatedNo",repeat+"");

							}else{
								
								tableNameMap.put("RepeatedNo",repeat+"");
							}
						}
						if(repeated>1){

							//Repeated Field Values
							fieldValue= getFieldValues(terserPath,input,repeated);

							String[] field=fieldValue.split("~");
							String value="";

							resultMap.put(mf.getColumnName(),field[0]);
							
						

							for(int i=1;i<field.length;i++){
								if(i==1){
									value=value+field[i];
								}
								else{
									value=value+"~"+field[i];
								}

							}
							resultMap.put(mf.getRepeatedColumn(),value);

						}else if(!mf.getSegmentName().equals(repeatedSegment)){

							// Getting Single Occuance Field Value
							fieldValue=getFieldValues(terserPath,input);
							
							resultMap.put(mf.getColumnName(),fieldValue);
						}
					}
				}
			}
			finalMap.put("ResultMap", resultMap);
		
			finalMap.put("RepeatedValueMap", repeatedValueMap);
			finalMap.put("RepSegTableNameMap",tableNameMap);
			System.out.println("FinalMap--->"+finalMap.size());
			inputStream.close();
			return finalMap;
		} catch (Exception e) {
			logger.info("********Error while getting result Map values**********");
			throw new HL7CustomException(e.getMessage(),prop.getProperty("AcknowledgementReject"));
		}



	}




	// Repeated Segment Values
	public String[] getRepeatedSegments(String terserPath,Message input,int repeat) throws Exception  {
		String result[]=new String[repeat];
		logger.info("RepeatedSegment Values");
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream("adt.properties");
		prop.load(inputStream);
		try {
			Terser terser = new Terser(input);
			String terserPathArr[] = terserPath.split("-");
			String newPath ="";
			int count=0;
			for(int i=0;i<repeat;i++){
				newPath = terserPathArr[0]+"("+i+")-"+terserPathArr[1];
				result[i]=terser.get(newPath);

				if(result[i]==null || result[i].equals("\"\"")){
					result[i]=null;
				}
				logger.info("Repeated segment Value--->"+result[i]);
		
			} inputStream.close();
		} catch (Exception e) {
			logger.error("Error while getting repeated segment field values");
			throw new HL7CustomException(e.getMessage(),prop.getProperty("AcknowledgementReject"));
		}
		return result;

	}

	//Single Occurance Field Value
	public String getFieldValues(String terserPath,Message input) throws Exception{

		String result="";  
		Terser terser = new Terser(input);
		logger.info("Single Occurance Field Values  "+terserPath);
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream("adt.properties");
		prop.load(inputStream);
		try {
			if(terserPath!=null){
				result =terser.get(terserPath);
				  logger.info("Incoming Field Value-->"+result+"--> "+terserPath);
				if(result==null || result.equals("\"\"")){
					result=null;
				}
				if(result != null && (result.contains("'") || result.contains("\"") )){
					result=result.replace("'", "`");
					if(result.contains("\"") || result.contains("\"\"")){
						result=result.replace("\"", "`");
						  logger.info("Replaced Field Result value-->"+result+"--> "+terserPath);
					}
				   logger.info("Result-->"+result+"--> "+terserPath);
				System.out.println("Result-->"+result+"--> "+terserPath);
			   }
			}
		inputStream.close();
		} catch (Exception e) {
			logger.error("Error While getting Single Occurance Filed Values");
			throw new HL7CustomException(e.getMessage(),prop.getProperty("AcknowledgementReject"));
			
		}

		return result;
	}

	//Repeated Field Values
	public String getFieldValues(String terserPath,Message input,int repeated) throws Exception,IOException {

		String result=null;  
		Terser terser = new Terser(input);
		logger.info("RepatedFieldValues");
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream("adt.properties");
		prop.load(inputStream);

		try {
			String terserArr[] = terserPath.split("-");
			String newPath ="";
			int count=0;
			for(int i=0;i<repeated;i++){
				newPath = terserArr[0]+"-"+terserArr[1]+"("+i+")-"+terserArr[2];

				if(count==0){
					result =terser.get(newPath);

					if(result==null || result.equals("\"\"")){
						result=null;
					}
				}else{
					String newResult = terser.get(newPath);
					if(newResult==null || newResult.equals("\"\"")){
						result=null;
					}else if(newResult!=null){
						
						result=result+"~"+newResult;
					}
				}
				count++;
				//System.out.println("result="+result);
			}
			inputStream.close();
		} catch (Exception e) {
			logger.error("Error While getting Repeated Filed values");
			throw new HL7CustomException(e.getMessage(),prop.getProperty("AcknowledgementReject"));
		}
			logger.info("Repeated Field Values--->"+result);
		return result;
	}

	//Mapping Field Values
	public String getFieldValues(String mappingType,String mappingField,Message input) throws Exception,IOException {

		try{
			logger.info("Mapping Field values " +mappingType);
			System.out.println("Mapping Field values " +mappingType);
			
			InputStream inputStream = getClass().getClassLoader().getResourceAsStream("ackcodes.properties");
			prop.load(inputStream);
			
			

			String mappedValue=null;
			String orgMapValue=null;
			Terser terser = new Terser(input);
			String fieldName=null;
			
			for(MessageFieldBean mf:getMessageconfiguration().getMessageFieldsConfiguration()){
				System.out.println("FieldName---->"+mf.getFieldName()+"     "+mappingField);
				if(mappingField.equalsIgnoreCase(mf.getFieldName())){
					
					orgMapValue=terser.get(mf.getTerserPath());
					fieldName=mf.getFieldName();
					
					System.out.println("OriginalMapValue---->"+orgMapValue+"  "+fieldName);
				}
			}
			

			for(FieldMapBean fmb:getMessageconfiguration().getFieldmap()){
				
				if(orgMapValue !=null){
				if(mappingType.equalsIgnoreCase(fmb.getMappingType()) && orgMapValue.equalsIgnoreCase(fmb.getOriginalValue())){
					logger.info("Mapping Values--->"+fmb.getOriginalValue()+"  "+ fmb.getMappedValue());
					System.out.println("Mapping Values--->"+fmb.getOriginalValue()+"  "+ fmb.getMappedValue());
					mappedValue=fmb.getMappedValue();
					break;
				}
				}
			}     
			     
			      if(mappedValue == null && orgMapValue != null){
			    	  throw new HL7CustomException("No Mapping Value for - "+orgMapValue+" - in "+mappingType,prop.getProperty("AcknowledgementReject"));
			      }inputStream.close();

			return mappedValue;
		} catch (Exception e) {
			logger.error("Error while getting Mapping Field Values");
			throw new HL7CustomException(e.getMessage(),prop.getProperty("AcknowledgementReject"));
		}

	}







	/*


	//Method for getting Field Values
	public void getFieldValues(Message input,Map messageMap){


		try{
			String field_Function;
			String[] fieldMethod;

			String messagetype=(String)messageMap.get("messagetype");
			String tablename=(String)messageMap.get("tablename");


			for(MessageFieldBean mf:getmessageconfiguration().getMessageFieldsConfiguration()){
				segmentName = input.get(mf.getSegment_name());


				field_Function= mf.getField_function();
				fieldMethod=field_Function.split("\\.");


				if(mf.getMessage_type().equals(messagetype)) {

					if(fieldMethod[0].equals("null")){

						// Getting Field Mapping Values
						getMappedValues(mf.getMapping_type(),mf.getColumn_name());

					}else{



						for(SegmentBean sb:getmessageconfiguration().getSegmentConfigurationBean()){


							if(!(sb.getRepeated().equals("1")) && sb.getMessageType().equalsIgnoreCase(messagetype)){


								repeatedSegment= input.getAll(sb.getSegmentName());

								if(repeatedSegment[0].equals(segmentName)){

									// method for Repeated Segment values
									getRepeatedSegments(repeatedSegment,field_Function,mf.getColumn_name());

									break;
								}

							}

						}

						if(mf.getRepeated().equals("1")){
							//System.out.println( "columnName---->"+mf.getColumn_name());

							if(!segmentName.toString().equalsIgnoreCase(repeatedSegment[0].toString())){

								//method for single occurence field
								getField(segmentName,field_Function,mf.getColumn_name());
							}
						}else{
							// Method for Repeated Field Values
							getRepeatedFields(segmentName,field_Function,mf.getColumn_name());
						}



					}

				}
			}

			// Inserting Patient Records in to DB table
			messagedao.insertPatientRecord(tablename,resultMap);

			//Insert request,response message in Audit table
			messagedao.messageAudit(messagetype, input);

		}catch(Exception e){

			e.printStackTrace();

		}

	}



	//Repated Segment Values
	private String getRepeatedSegments(Structure[] repeatedSegment,String field_Function, String column_name) {

		String result=null;

		String[] fieldMethod;

		System.out.println("Repeated Segment");

		fieldMethod=field_Function.split("\\.");
		//Repeated Segments

		for (Structure struct : repeatedSegment) {
			try {
				Class messageSegmentClass =struct.getClass();
				Method m = messageSegmentClass
						.getMethod(getFunctionName(fieldMethod[0]));

				if(!getFunctionName(fieldMethod[1]).equals("encode")){

					Object messageSegmentM = m.invoke(segmentName);
					Class messageSegmentClassM = messageSegmentM.getClass();
					Method m1 = messageSegmentClassM.getMethod(getFunctionName(fieldMethod[1]),Integer.TYPE);

				}


				Object messageSegmentM = m.invoke(struct);
				Class messageSegmentClassM = messageSegmentM.getClass();
				Method m1 = messageSegmentClassM.getMethod("encode");
				System.out.println("Repeated Segment :: "+column_name+ m1.invoke(messageSegmentM));
				result=(String)m1.invoke(messageSegmentM);
			} catch (Exception e) {
				System.out.println(e);
			}

		}

		return result;
	}





	//single Occurrence Field Values
	public String getField(Structure segmentName,String field_Function,String columnName){
		String[] fieldMethod;
		String result = null;
		try{
			fieldMethod=field_Function.split("\\.");


			if(!getFunctionName(fieldMethod[0]).equals(null)){

				Class messageSegmentClass = segmentName.getClass();
				Method m = messageSegmentClass.getMethod(getFunctionName(fieldMethod[0]),noparam);
				System.out.println("fieldMethod =="+fieldMethod[0]);

				if(!getFunctionName(fieldMethod[1]).equals("encode")){
					Object messageSegmentM = m.invoke(segmentName);
					Class messageSegmentClassM = messageSegmentM.getClass();
					Method m1 = messageSegmentClassM.getMethod(getFunctionName(fieldMethod[1]),Integer.TYPE);

				}

				Object messageSegmentM1 = m.invoke(segmentName);
				Class messageSegmentClassM1 = messageSegmentM1.getClass();
				Method m2 = messageSegmentClassM1.getMethod("encode");
				//System.out.println(mf.getColumn_name()+"  Value ::  " + m2.invoke(messageSegmentM1));

				System.out.println(columnName+"   "+m2.invoke(messageSegmentM1));


				resultMap.put(columnName, m2.invoke(messageSegmentM1).toString());

				result = (String) m2.invoke(messageSegmentM1);


			}else{
				System.out.println("Null Field values");

			}
		} catch(Exception e){
			e.printStackTrace();
		}
		return result;

	}


	//Repeated Field Values
	public String getRepeatedFields(Structure segmentName,String field_Function,String columnName){

		String result="";
		String[] fieldMethod;

		try{

			fieldMethod=field_Function.split("\\.");

			if(!getFunctionName(fieldMethod[0]).equals(null)){
				Class messageSegmentClass=segmentName.getClass();
				Method m = messageSegmentClass.getMethod(getFunctionName(fieldMethod[0]));
				System.out.println("FieldMethod ::  "+fieldMethod[0]);
				Object[] messageSegmentMList = (Object[]) m.invoke(segmentName);
				//Object messageSegmentM1 = m.invoke(segmentName);

				int count=0;

				for(Object obj : messageSegmentMList)
				{
					Class	messageSegmentClassM = obj.getClass();
					Method   m1 = messageSegmentClassM.getMethod(getFunctionName(fieldMethod[1]),
							Integer.TYPE);

					Object messageSegmentM1 = m1.invoke(obj,0);
					Class	messageSegmentClassM1 = messageSegmentM1.getClass();
					Method	m2 = messageSegmentClassM1.getMethod("encode");
					//System.out.println(mf.getColumn_name() +" Values :: " + m2.invoke(messageSegmentM1));

					if(count==0){
						result= result+m2.invoke(messageSegmentM1).toString();
					}else{
						result= result+"~"+m2.invoke(messageSegmentM1).toString();

					}

					count++;
					System.out.println("Result===>"+result);

				}
				resultMap.put(columnName,result);
			}else{
				System.out.println("Null Field values");
			}

		}catch(Exception e){
			e.printStackTrace();
		}

		return result;
	}

	//Getting FieldFunction and removing braces
	private String getFunctionName(String function) throws Exception{
		//System.out.println("function=="+function);
		String result = null;
		try{
			int length = function.indexOf("(");
			//System.out.println("length=="+length);
			int param=function.indexOf(")");

			int len=(param-1)-length+1;

			//System.out.println("Param lent==="+len);

			if(length>=0){
				result = function.substring(0, length);


			}else{
				result=function;
			}

			if(len>=2){

				String	resultf=function.substring(length+1,function.length()-1);
				//System.out.println("resultparam "+resultf);

				}

		}catch(Exception e){
			throw e;
		}
		return result;
	}


	// Getting Field Mapping Values
	public String getMappedValues(String mappingType,String columnName){

		String mappedValue=null;

		for(FieldMapBean fmb:getmessageconfiguration().getFieldmap()){

			if(mappingType.equalsIgnoreCase(fmb.getMappingType())){
				logger.info("Mapping Values--->"+fmb.getOriginalValue()+"  "+ fmb.getMappedValue());

				mappedValue=fmb.getMappedValue();


				resultMap.put(columnName,mappedValue);
				break;
			}

		}



		return mappedValue;

	}

	 */

}




