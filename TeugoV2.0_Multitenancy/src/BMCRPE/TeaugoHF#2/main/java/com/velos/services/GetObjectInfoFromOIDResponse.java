
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getObjectInfoFromOIDResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getObjectInfoFromOIDResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ObjectInfo" type="{http://velos.com/services/}objectInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getObjectInfoFromOIDResponse", propOrder = {
    "objectInfo"
})
@XmlRootElement(name="getObjectInfoFromOIDResponse", namespace = "http://velos.com/services/")
public class GetObjectInfoFromOIDResponse {

    @XmlElement(name = "ObjectInfo")
    protected ObjectInfo objectInfo;

    /**
     * Gets the value of the objectInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectInfo }
     *     
     */
    public ObjectInfo getObjectInfo() {
        return objectInfo;
    }

    /**
     * Sets the value of the objectInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectInfo }
     *     
     */
    public void setObjectInfo(ObjectInfo value) {
        this.objectInfo = value;
    }

}
