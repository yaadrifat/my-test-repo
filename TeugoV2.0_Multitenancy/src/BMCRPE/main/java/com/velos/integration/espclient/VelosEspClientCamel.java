package com.velos.integration.espclient;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import org.apache.camel.CamelContext;
import org.apache.camel.CamelExecutionException;
import org.apache.camel.ExchangePattern;
import org.apache.camel.ProducerTemplate;
import org.apache.cxf.message.MessageContentsList;
import org.apache.cxf.ws.security.wss4j.WSS4JOutInterceptor;
import org.apache.ws.security.WSConstants;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import com.velos.integration.adtx.webservice.SimpleAuthCallbackHandler;
import com.velos.integration.adtx.webservice.VelosEspSearchUserEndpoint;
import com.velos.integration.mapping.EndpointKeys;
import com.velos.integration.mapping.ProtocolKeys;
import com.velos.integration.mapping.VelosKeys;
import com.velos.integration.mapping.VelosStudyMapper;
import com.velos.services.FormIdentifier;
import com.velos.services.Issue;
import com.velos.services.ObjectInfo;
import com.velos.services.OperationException_Exception;
import com.velos.services.OrganizationIdentifier;
import com.velos.services.PatientIdentifier;
import com.velos.services.PatientProtocolIdentifier;
import com.velos.services.SimpleIdentifier;
import com.velos.services.StudyIdentifier;
import com.velos.services.StudyPatientFormResponse;
import com.velos.services.StudyPatientFormResponses;
import com.velos.services.StudyPatientStatuses;
import com.velos.services.StudySummary;
import com.velos.services.UserSearch;
import com.velos.services.UserSearchResults;

@Component
public class VelosEspClientCamel implements VelosEspStudyEndpoint, VelosEspSystemAdministrationEndpoint,
		VelosEspPatStudyEndpoint, VelosEspPatStudyStatusInfoEndpoint, VelosEspFormResponseEndpoint,VelosEspSearchUserEndpoint {

	public static void main(String[] args) {
		VelosEspClientCamel client = new VelosEspClientCamel();
    	client.configureCamel();
		
		// Create test data
    	Map<VelosKeys, String> requestMap = new HashMap<VelosKeys, String>();
    	requestMap.put(EndpointKeys.Endpoint, EndpointKeys.Epic.toString());
    	requestMap.put(ProtocolKeys.StudyNumber, "2010-01");
    	
    	// Test study summary
    	//Map<VelosKeys, Object> respMap = client.handleRequest(VelosEspMethods.StudyGetStudySummary, requestMap);
    	//System.out.println("Study title is "+respMap.get(ProtocolKeys.Title));
    	
    	// Test object info from OID
    	//requestMap.put(ProtocolKeys.OID, "219dcdf6-fd2e-47e4-9d14-a0efa8ec7087");
    	// client.handleRequest(VelosEspMethods.SysAdminGetObjectInfoFromOID, requestMap);
    	
    	// Test form response service
    	Map<String, String> inputMap = new HashMap<String, String>();
    	inputMap.put("pkForm", "866");
    	inputMap.put("siteName", "New York College of Medicine");
    	inputMap.put("patientId", "12345");
    	inputMap.put("pkStudy", "344");
    	client.testGetListOfStudyFormResponses(inputMap);
    }
	
	public void configureCamel() {
		if (this.context != null) { return; }
		
		
		ClassPathXmlApplicationContext springContext = null;
				try{
         springContext=  new ClassPathXmlApplicationContext("camel-config.xml");
    	this.setContext((CamelContext)springContext.getBean("camel"));		
				}finally{
					springContext.close();
				}
	}
	
	protected CamelContext context;
	
	public void setContext(CamelContext context) {
		this.context = context;
	}
    
	public Map<VelosKeys, Object> handleRequest(VelosEspMethods method, Map<VelosKeys, String> requestMap)  {
    	if (requestMap == null) { return null; }
    	this.configureCamel();
    	
        // Call Velos WS
		switch(method) {
		case StudyGetStudySummary:
			return callGetStudySummary(requestMap);
		case SysAdminGetObjectInfoFromOID:
			return callGetObjectInfoFromOID(requestMap);
		default:
			break;
		}
		return null;
	}
	
	public void testGetListOfStudyFormResponses(Map<String, String> requestMap) {
		FormIdentifier formIdentifier = new FormIdentifier();
		formIdentifier.setPK(Integer.parseInt(requestMap.get("pkForm")));
		OrganizationIdentifier organizationIdentifier = new OrganizationIdentifier();
		organizationIdentifier.setSiteName(requestMap.get("siteName"));
		PatientIdentifier patientIdentifier = new PatientIdentifier();
		patientIdentifier.setPatientId(requestMap.get("patientId"));
		patientIdentifier.setOrganizationId(organizationIdentifier);
		StudyIdentifier studyIdentifier = new StudyIdentifier();
		studyIdentifier.setPK(Integer.parseInt(requestMap.get("pkStudy")));
		try {
			StudyPatientFormResponses studyPatientFormResponses =
					getListOfStudyPatientFormResponses(formIdentifier, patientIdentifier, studyIdentifier, 1, 100);
			for (StudyPatientFormResponse resp : studyPatientFormResponses.getStudyPatientFormResponses()) {
				System.out.println("Got OID="+resp.getSystemID().getOID());
			}
		} catch (OperationException_Exception e) {
			e.printStackTrace();
			List<Issue> issueList = e.getFaultInfo().getIssues().getIssue();
			if (issueList == null) { return; }
		} catch (Exception e) {
			e.printStackTrace();
		}
		return;
	}
	
	public Map<VelosKeys, Object> callGetObjectInfoFromOID(Map<VelosKeys, String> requestMap) {
		SimpleIdentifier simpleIdentifier = new SimpleIdentifier();
		simpleIdentifier.setOID(requestMap.get(ProtocolKeys.OID));
		ObjectInfo objectInfo = null;
		try {
			objectInfo = getObjectInfoFromOID(simpleIdentifier);
			System.out.println("table name="+objectInfo.getTableName()+", table pk="+objectInfo.getTablePk());
		} catch (OperationException_Exception e) {
			e.printStackTrace();
			return null;
		}
		return null;
	}
	
	public Map<VelosKeys, Object> callGetPatientStudyInfo(Map<VelosKeys, String> requestMap) {
		String patProtIdStr = requestMap.get(ProtocolKeys.PatProtId);
		try {
			Integer patProtId = Integer.parseInt(patProtIdStr);
			PatientProtocolIdentifier patientProtocolIdentifier = getPatientStudyInfo(patProtId);
		} catch (OperationException_Exception e) {
			e.printStackTrace();
			List<Issue> issueList = e.getFaultInfo().getIssues().getIssue();
			if (issueList == null) { return null; }
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return null;
	}
	
	public StudySummary getStudySummary(StudyIdentifier request)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList list =
				(MessageContentsList) producer.sendBody("cxf:bean:espStudyEndpoint", 
						ExchangePattern.InOut, request);
		return (StudySummary) list.get(0);
	}
	
    public Map<VelosKeys, Object> callGetStudySummary(Map<VelosKeys, String> requestMap) {
    	StudyIdentifier studyIdentifier = new StudyIdentifier();
    	studyIdentifier.setStudyNumber(requestMap.get(ProtocolKeys.StudyNumber));
    	StudySummary studySummary = null;
    	try {
    		studySummary = getStudySummary(studyIdentifier);
    	} catch(OperationException_Exception e) {
    		e.printStackTrace();
    	}
    	VelosStudyMapper mapper = new VelosStudyMapper(requestMap.get(EndpointKeys.Endpoint));
    	return mapper.mapStudySummary(studySummary);
    }
    
	@Bean
	public WSS4JOutInterceptor wssInterceptor() {
		Map<String, Object> props = new HashMap<String, Object>();
		props.put(WSHandlerConstants.ACTION, WSHandlerConstants.USERNAME_TOKEN);
		props.put(WSHandlerConstants.PASSWORD_TYPE, WSConstants.PW_TEXT);
		props.put(WSHandlerConstants.USER, "hvchahal");
		props.put(WSHandlerConstants.PW_CALLBACK_CLASS, SimpleAuthCallbackHandler.class.getName());
		WSS4JOutInterceptor wssBean = new WSS4JOutInterceptor(props);
		return wssBean;
	}

	@Override
	public ObjectInfo getObjectInfoFromOID(SimpleIdentifier arg0)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList list =
				(MessageContentsList) producer.sendBody("cxf:bean:espSystemAdministrationEndpoint", 
						ExchangePattern.InOut, arg0);
		return (ObjectInfo) list.get(0);
	}
	
	@Override
	public PatientProtocolIdentifier getPatientStudyInfo(Integer patProtId)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList list = null;
		try {
			list = (MessageContentsList) producer.sendBody(
					"cxf:bean:espPatStudyEndpoint", ExchangePattern.InOut,
					patProtId);
		} catch(CamelExecutionException e) {
			if (e.getCause() instanceof OperationException_Exception) {
				throw(OperationException_Exception)e.getCause();
			}
			e.printStackTrace();
			return null;
		}
		return (PatientProtocolIdentifier) list.get(0);
	}
	
	@Override
	public StudyPatientStatuses getStudyPatientStatusHistory(
			StudyIdentifier studyIdentifier, PatientIdentifier patientIdentifier)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList inpList = new MessageContentsList();
		inpList.add(studyIdentifier);
		inpList.add(patientIdentifier);
		MessageContentsList list = (MessageContentsList) producer.sendBody(
				"cxf:bean:espPatStudyStatusInfoEndpoint", ExchangePattern.InOut,
				inpList);
		return (StudyPatientStatuses) list.get(0);
	}

	@Override
	public StudyPatientFormResponses getListOfStudyPatientFormResponses(
			FormIdentifier formIdentifier, PatientIdentifier patientIdentifier,
			StudyIdentifier studyIdentifier, int pageNumber, int pageSize)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList inpList = new MessageContentsList();
		inpList.add(formIdentifier);
		inpList.add(patientIdentifier);
		inpList.add(studyIdentifier);
		inpList.add(pageNumber);
		inpList.add(pageSize);
		MessageContentsList list = (MessageContentsList) producer.sendBody(
				"cxf:bean:espFormResponseEndpoint", ExchangePattern.InOut,
				inpList);
		return (StudyPatientFormResponses) list.get(0);
	}

	@Override
	@WebResult(name = "UserSearchResults", targetNamespace = "")
	@RequestWrapper(localName = "searchUser", targetNamespace = "http://velos.com/services/", className = "com.velos.services.SearchUser")
	@WebMethod
	@ResponseWrapper(localName = "searchUserResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.SearchUserResponse")
	public UserSearchResults searchUser(
			@WebParam(name = "UserSearch", targetNamespace = "") UserSearch userSearch)
			throws OperationException_Exception {
		
		UserSearchResults usreSearchResults = new  UserSearchResults();
		return usreSearchResults;
	}
	
}
