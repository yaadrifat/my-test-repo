package com.velos.integration.mapping;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.log4j.Logger;

import scala.annotation.meta.getter;

import com.ibm.icu.text.SimpleDateFormat;
import com.velos.outbound.servlet.EpicServlet;
import com.velos.services.CompletedAction;
import com.velos.services.CreatePatientResponse;
import com.velos.services.GetPatientDetailsResponse;
import com.velos.services.Issue;
import com.velos.services.Issues;
import com.velos.services.Patient;
import com.velos.services.PatientDataBean;
import com.velos.services.PatientDemographics;
import com.velos.services.PatientSearchResponse;
import com.velos.services.ResponseHolder;
import com.velos.services.Results;
import com.velos.services.SearchPatientResponse;

public class VelosPatientMapper {
	private String endpoint = null;
	private Map<VelosKeys, Object> dataMap = new HashMap<VelosKeys, Object>();
	private static Logger logger = Logger.getLogger("epicLogger");   
	public VelosPatientMapper(String endpoint) {
		this.endpoint = endpoint;
	}
	
	public Map<VelosKeys, Object> mapSearchPatient(SearchPatientResponse resp) {
		logger.info("\n***********VelosPatientMapper*********\n");
		if (EndpointKeys.Epic.toString().equals(endpoint)) {
			mapSearchPatientForEpic(resp);
		}
		return dataMap;
	}
	
	private void mapSearchPatientForEpic(SearchPatientResponse resp) {
		logger.info("**********mapSearchPatientForEpic_Method********");
		PatientSearchResponse searchResp = resp.getPatientSearchResponse();
		int totalCount = searchResp.getTotalCount();
		if (totalCount < 1) {
			dataMap.put(ProtocolKeys.FaultString, ProtocolKeys.PatientNotFoundMsg.toString());
			return;
		}
		if (totalCount > 1) {
			dataMap.put(ProtocolKeys.FaultString, ProtocolKeys.MultiplePatientsFoundMsg.toString());
			return;
		}
		dataMap.put(ProtocolKeys.TotalCount, searchResp.getTotalCount());
		List<PatientDataBean> patList = searchResp.getPatDataBean();
		logger.info("patList=="+patList);
		for (PatientDataBean patBean : patList) {
			dataMap.put(ProtocolKeys.PatientID, patBean.getPatientIdentifier().getPatientId());
			dataMap.put(ProtocolKeys.PatientId, patBean.getPatientIdentifier().getPatientId());
			dataMap.put(ProtocolKeys.OID, patBean.getPatientIdentifier().getOID());
			dataMap.put(ProtocolKeys.FirstName, patBean.getPatFirstName());
			dataMap.put(ProtocolKeys.LastName, patBean.getPatLastName());
			dataMap.put(ProtocolKeys.Dob, patBean.getPatDateofBirth());
			dataMap.put(ProtocolKeys.TotalCount, 1);
		}
		logger.info("\n**********mapSearchPatientForEpic_Method =======>"+dataMap);
	}

	public Map<VelosKeys, Object> mapCreatePatient(CreatePatientResponse resp) {
		logger.info("\n***********VelosPatientMapper*********\n");
		if (EndpointKeys.Epic.toString().equals(endpoint)) {
			mapCreatePatientForEpic(resp);
		}
		return dataMap;
	}

	private void mapCreatePatientForEpic(CreatePatientResponse resp) {
		logger.info("**********mapCreatePatientForEpic_Method********");
		ResponseHolder patientResp = resp.getResponse();
		Results results = patientResp.getResults();
		Issues issues = patientResp.getIssues();
		
		if(issues!=null){
			List<Issue> issueList = issues.getIssue();
			if(issueList!=null && !issueList.isEmpty()){
				for(Issue i:issueList){
					dataMap.put(ProtocolKeys.FaultString, i.getType());
					return;
				}
			}
		}
		
		if(results!=null){
			List<CompletedAction> resultList= results.getResult();
			if(resultList!=null && !resultList.isEmpty()){
				CompletedAction action = resultList.get(0);
				dataMap.put(ProtocolKeys.OID,action.getObjectId().getOID());
				dataMap.put(ProtocolKeys.PK,action.getObjectId().getPK());
				//dataMap.put(ProtocolKeys.PatientId,action.getObjectId().get);
			}
		}
	}

	public Map<VelosKeys, Object> mapPatientDetails(GetPatientDetailsResponse resp) {
		logger.info("\n***********VelosPatientMapper*********\n");
		if (EndpointKeys.Epic.toString().equals(endpoint)) {
			mapPatientDetailsForEpic(resp);
		}
		return dataMap;
	}

	private void mapPatientDetailsForEpic(GetPatientDetailsResponse resp) {
		logger.info("\n**********mapPatientDetailsForEpic_Method********");
		Patient patient = resp.getPatientDetails();
		PatientDemographics patientDemo= patient.getPatientDemographics();
		
		dataMap.put(ProtocolKeys.PatientFacilityId,patientDemo.getPatFacilityId());
		dataMap.put(ProtocolKeys.FirstName,patientDemo.getFirstName());
		dataMap.put(ProtocolKeys.LastName,patientDemo.getLastName());
		dataMap.put(ProtocolKeys.StreetAddressLine,patientDemo.getAddress1());
		dataMap.put(ProtocolKeys.City, patientDemo.getCity());
		dataMap.put(ProtocolKeys.State,patientDemo.getState());
		dataMap.put(ProtocolKeys.PostalCode,patientDemo.getZipCode());
		dataMap.put(ProtocolKeys.Country,patientDemo.getCountry());
		dataMap.put(ProtocolKeys.OID,patientDemo.getPatientIdentifier().getOID());
				
		String Dob =toDate(patientDemo.getDateOfBirth());
		logger.info("Date  -------->"+Dob);
		/*SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMdd");
		String input= sdf.format(date);
		String Dob = input.substring(0,8);*/
		String formatedDob =Dob.replace("-","");
		dataMap.put(ProtocolKeys.Dob,formatedDob);
		logger.info("After date format====>"+formatedDob);
		logger.info("\n**********mapPatientDetailsForEpic =======>"+dataMap);
		
	}
	
	  public static String toDate(XMLGregorianCalendar calendar){
	        if(calendar == null) {
	            return null;
	        }
	        calendar.setTime(DatatypeConstants.FIELD_UNDEFINED,
	        	       DatatypeConstants.FIELD_UNDEFINED,
	        	       DatatypeConstants.FIELD_UNDEFINED,
	        	       DatatypeConstants.FIELD_UNDEFINED);
	       
	      //  System.out.println("String Date =====>"+ calendar.toString().substring(0,10));
	        logger.info("String Date =====>"+ calendar);
	        String espdobValue = calendar.toString().substring(0,10);
	       // return calendar.toGregorianCalendar().getTime();
	        return espdobValue ;
	    }	
}
