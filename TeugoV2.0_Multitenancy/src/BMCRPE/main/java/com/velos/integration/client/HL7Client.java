package com.velos.integration.client;

import java.lang.reflect.Method;
import java.util.ArrayList;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.Structure;
import ca.uhn.hl7v2.parser.PipeParser;

public class HL7Client {

	public static void main(String ar[]) throws HL7Exception,
			java.io.IOException {
		String line1 = "MSH|^~%&|ADT|PLW||A01-I|201112131606||ADT^A04|PLW21219057037581622|P|2.4||AL|NE|";
		String line2 = "EVN||201112131606||ADM|RICHALA^test^test^a";
		String line3 = "PID||1001836|1001039||PRECARE^BERNADETTEEE||19710621|F||W~I~A|1515 HOLCOMBE BLVD^^HOUSTON^TX^77030^US|101|(733)499-9999^^^^^733^4999999~(723)499-9999^^^^^723^4999999|(713)299-9999^^^^^713^2999999||S|NSP|9093000638~9093000639||||N~H";
		String line4 = "PV1||ACUTE|PATC^PATC^^MDA||||||||||||||||10505519700|<None>||||||||||||||||||||||||20131216123100||||||";
		String line5 = "FT1|1|||20140325|20140325|DR|0141890|Bevacizumab^INJ^880.0000^mg|28^1^50242-0061-01^N^36.00|88|18578.40|||||PATC^PATC^^MDA||||00152^Ater^Joann^L^^Dr|00152^Ater^Joann^L^^Dr|7010.71|";
		String line6 = "FT1|2|||20140325|20140325|DR|0140316|Sodium Chloride^INJ^100.0000^mL|28^1^00338-0049-04^Y^1.00|1|7.10|||||PATC^PATC^^MDA||||00152^Ater^Joann^L^^Dr|00152^Ater^Joann^L^^Dr|2.00|";
		StringBuilder messageString = new StringBuilder();
		messageString.append(line1);
		messageString.append(System.getProperty("line.separator"));
		messageString.append(line2);
		messageString.append(System.getProperty("line.separator"));
		messageString.append(line3);
		messageString.append(System.getProperty("line.separator"));
		messageString.append(line4);
		messageString.append(System.getProperty("line.separator"));
		messageString.append(line5);
		messageString.append(System.getProperty("line.separator"));
		messageString.append(line6);
		PipeParser pipeParser = new PipeParser();
		Message output = null;
		Message input = pipeParser.parse(messageString.toString());
		try {
			
			//Repeated Segments
			Structure[] ft1s = input.getAll("FT1");
			for (Structure ft1 : ft1s) {
				try {
					Class messageSegmentClass = ft1.getClass();
					Method m = messageSegmentClass
							.getMethod("getFt17_TransactionCode");

					Object messageSegmentM = m.invoke(ft1);
					Class messageSegmentClassM = messageSegmentM.getClass();
					Method m1 = messageSegmentClassM.getMethod("encode");
					System.out.println("Transaction Code : "
							+ m1.invoke(messageSegmentM));
				} catch (Exception e) {
					System.out.println(e);
				}
			}

			Structure pid = input.get("PID");
			
			//SubComponent
			Class messageSegmentClass = pid.getClass();
			Method m = messageSegmentClass.getMethod("getPatientName",
					Integer.TYPE);

			Object messageSegmentM = m.invoke(pid, 0);
			Class messageSegmentClassM = messageSegmentM.getClass();
			Method m1 = messageSegmentClassM.getMethod("getComponent",
					Integer.TYPE);

			Object messageSegmentM1 = m1.invoke(messageSegmentM, 0);
			Class messageSegmentClassM1 = messageSegmentM1.getClass();
			Method m2 = messageSegmentClassM1.getMethod("encode");
			System.out.println("Last Name : " + m2.invoke(messageSegmentM1));
			
			//Repeated field
			m = messageSegmentClass.getMethod("getRace");
			Object[] messageSegmentMList = (Object[]) m.invoke(pid);
			
			for(Object obj : messageSegmentMList)
			{
				messageSegmentClassM = obj.getClass();
				m1 = messageSegmentClassM.getMethod("getComponent",
						Integer.TYPE);

				messageSegmentM1 = m1.invoke(obj, 0);
				messageSegmentClassM1 = messageSegmentM1.getClass();
				m2 = messageSegmentClassM1.getMethod("encode");
				System.out.println("Race : " + m2.invoke(messageSegmentM1));
			}
			
		} catch (Exception e) {
			System.out.println(e);
		}
	}

}