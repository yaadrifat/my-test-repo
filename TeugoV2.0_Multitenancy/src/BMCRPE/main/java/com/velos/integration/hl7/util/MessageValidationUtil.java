package com.velos.integration.hl7.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.PipeParser;

import com.velos.integration.hl7.bean.MessageBean;
import com.velos.integration.hl7.bean.MessageFieldBean;
import com.velos.integration.hl7.bean.SegmentBean;
import com.velos.integration.hl7.notifications.HL7CustomException;
import com.velos.integration.hl7.processor.HL7ValidationContext;
import com.velos.integration.hl7.processor.InterfaceUtil;

public class MessageValidationUtil {
	
	private static Logger logger = Logger.getLogger(MessageValidationUtil.class);
	
	private InterfaceUtil interfaceUtil = null;

	public void setInterfaceUtil(InterfaceUtil interfaceUtil) {
		this.interfaceUtil = interfaceUtil;
	}

	public void validateMessage(String msg,String messageType,List<MessageBean> messageBeanList,List<MessageFieldBean> messageFieldBeanList,List<SegmentBean> segmentBeanList) throws Exception{
		try{
			logger.info("-----validation starts----------");
			//List<MessageFieldBean> messageBeanList = configDao.getMessageConfigDetails();
			//List<SegmentBean> segmentBeanList = configDao.getsegmentList();
			PipeParser pipeParser = new PipeParser();
			pipeParser.setValidationContext(new HL7ValidationContext());
			Message input = pipeParser.parse(msg);
			ArrayList<String> messageTypeList = getMessageTypeList(messageBeanList);
			logger.info("messageTypeList");
			displayNameList(messageTypeList);
			validateMessageType(messageType,messageTypeList);
			
			ArrayList<String> segmentListFromMsg = getSegmentNameListFromMessage(msg);
			logger.info("segmentListFromMsg");
			displayNameList(segmentListFromMsg);
			ArrayList<String> segmentListFromDB = getSegmentNameListFromDB(messageType,segmentBeanList);
			logger.info("segmentListFromDB");
			displayNameList(segmentListFromDB);
			ArrayList<String> requiredSegmentNameList = getRequiredSegmentNameListFromDB(messageType,segmentBeanList); 
			logger.info("requiredSegmentNameList");
			displayNameList(requiredSegmentNameList);
			//validateSegmentName
			//validateSegmentName(segmentListFromDB,segmentListFromMsg);
			//validateRequirdSegmentName
			validateRequirdSegmentName(requiredSegmentNameList,segmentListFromMsg);
			
			HashMap<String,String> segmentCountMapFromMsg = getSegementCountMapFromMessage(msg);
			HashMap<String,String> segmentCountMapFromDB = getSegmentCountMapFromDB(messageType,segmentBeanList);
			//validateSegmentCount
			validateSegmentCount(segmentCountMapFromMsg,segmentCountMapFromDB);
			//validate segment sequence
			validateSegmentSequence(messageType,segmentListFromMsg,segmentBeanList);
			
			//validateRequiredSegmentComponent
			HashMap<String,SegmentBean> segmentBeanMap = getSegmentBeanMapFromDB(messageType, segmentBeanList);
			//validateRequiredSegmentComponent(input,messageType,segmentBeanMap,messageBeanList);
			validateMessageFields(input,messageType,segmentBeanMap,messageFieldBeanList,segmentCountMapFromMsg,segmentListFromMsg);
			
			logger.info("---------validation ends-------------");
		}catch(HL7CustomException e){
			throw e;
		}
		//return true;
	}
	
	private void validateSegmentSequence(String messageType,ArrayList<String> segmentListFromMsg,List<SegmentBean> segmentBeanList) throws Exception{
		try{
			if(segmentBeanList!=null && !segmentBeanList.isEmpty()){
				for(SegmentBean segmentBean:segmentBeanList){
					String segmentName = segmentBean.getSegmentName();
					//int repeated = Integer.parseInt((validateNull(segmentBean.getRepeated(),"1")));
					String msgTypeFromDB = segmentBean.getMessageType();
					if(messageType!=null && msgTypeFromDB!=null && messageType.trim().equalsIgnoreCase(msgTypeFromDB)){
						int seqOrderFromDB = segmentBean.getSegmentSequence();
						if(seqOrderFromDB>0){
							if(segmentListFromMsg!=null && segmentListFromMsg.indexOf(segmentName)>=0){
								int seqOrderFromMsg = segmentListFromMsg.indexOf(segmentName)+1;
								if(seqOrderFromDB!=seqOrderFromMsg){
									throw new HL7CustomException(segmentName+" order is not correct","AE");
								}
							}
						}
					}
				}
			}
		}catch(HL7CustomException e){
			throw e;
		}
	}

	private void validateMessageType(String messageType,ArrayList<String> messageTypeList) throws Exception {
		try{
			if(messageType!=null){
				if(messageTypeList!=null && !messageTypeList.contains(messageType)){
					throw new HL7CustomException("MessageType is not valid","AE");
				}
			}
		}catch(HL7CustomException e){
			throw e;
		}
	}

	private ArrayList<String> getMessageTypeList(List<MessageBean> messageBeanList) throws Exception{
		ArrayList<String> messageTypeList = new ArrayList<String>();
		try{
			if(messageBeanList!=null && !messageBeanList.isEmpty()){
				String messageType = null;
				for(MessageBean messageBean:messageBeanList){
					messageType = messageBean.getMesssageType();
					if(messageType!=null && !messageType.trim().equals("")){
						messageTypeList.add(messageType);
					}
				}
			}
		}catch(Exception e){
			throw new HL7CustomException();
		}
		return messageTypeList;
	}

	private void validateMessageFields(Message input,String messageType,HashMap<String,SegmentBean> segmentBeanMap,List<MessageFieldBean> messageFieldBeanList,HashMap<String,String> segmentCountMapFromMsg,ArrayList<String> segmentListFromMsg) throws Exception{
		try{
			if(messageFieldBeanList!=null && !messageFieldBeanList.isEmpty()){
				String msgTypeFromDB = null;
				String segmentName = null;
				for(MessageFieldBean fldBean:messageFieldBeanList){
					msgTypeFromDB = fldBean.getMessageType();
					segmentName = fldBean.getSegmentName();
					if(segmentListFromMsg.contains(segmentName)){
						if(messageType!=null && msgTypeFromDB!=null && messageType.trim().equalsIgnoreCase(msgTypeFromDB)){
							validateFieldIsMandatory(input, fldBean, segmentBeanMap,segmentCountMapFromMsg);
							validateFieldLength(input, fldBean, segmentBeanMap,segmentCountMapFromMsg);
						}
					}
				}
			}else{
				logger.info("messageBeanList is null or empty in validateMessageFields");
			}
		}catch(HL7CustomException e){
			throw e;
		}
	}

	private void validateFieldIsMandatory(Message input,MessageFieldBean fldBean,HashMap<String,SegmentBean> segmentBeanMap,HashMap<String,String> segmentCountMapFromMsg) throws Exception{
		//logger.info("validateFieldIsMandatory");
		try{
			if(fldBean!=null){
				String segmentName = fldBean.getSegmentName();
				String fieldName = fldBean.getFieldName();
				int repeated = Integer.parseInt(validateNull(fldBean.getRepeated(),"1"));
				int mandatory = fldBean.getIsMandatory();
				String mappingType = fldBean.getMappingType(); 
				String terserPath = fldBean.getTerserPath();
				SegmentBean segmentBean = segmentBeanMap.get(segmentName);
				if(segmentBean!=null){
					//int segmentRepeatedCount = Integer.parseInt(validateNull(segmentBean.getRepeated(),"0"));
					int segmentCountFromMsg = Integer.parseInt(validateNull(segmentCountMapFromMsg.get(segmentName),"1"));
					if(mappingType==null || mappingType.trim().equals("")){
						if(mandatory==1 && repeated==1 && segmentCountFromMsg==1){
							String componentValue = interfaceUtil.getFieldValues(terserPath, input);
							
							if(componentValue==null || componentValue.trim().equals("")){
								throw new HL7CustomException(fieldName+" is mandatory","AE");
							}
						}else if(mandatory==1 && repeated>1 && segmentCountFromMsg==1){
							String componentValue = interfaceUtil.getFieldValues(terserPath, input, repeated);
						
							if(componentValue==null || componentValue.trim().equals("")){
								throw new HL7CustomException(fieldName+" is mandatory","AE");
							}
						}else if(mandatory==1 && repeated==1 && segmentCountFromMsg>1){
							String terserPathArr[] = terserPath.split("-");
							int count =1;
							for(int i=0;i<segmentCountFromMsg;i++){
								//String newTerserPath = terserPathArr[0]+"("+i+")-"+terserPathArr[1];
								String newTerserPath = getTerserPath(terserPathArr,i);
							
								String componentValue = interfaceUtil.getFieldValues(newTerserPath, input);
							
								if(componentValue==null || componentValue.trim().equals("")){
									throw new HL7CustomException(fieldName+" value in "+count+" row of "+segmentName+" is mandatory","AE");
								}
								count++;
							}
						}else if(mandatory==1 && repeated>1 && segmentCountFromMsg>1){
							String terserPathArr[] = terserPath.split("-");
							int count =1;
							for(int i=0;i<segmentCountFromMsg;i++){
								//String newTerserPath = terserPathArr[0]+"("+i+")-"+terserPathArr[1];
								String newTerserPath = getTerserPath(terserPathArr,i);
								
								String componentValue = interfaceUtil.getFieldValues(newTerserPath, input,repeated);
							
								if(componentValue==null || componentValue.trim().equals("")){
									throw new HL7CustomException(fieldName+" value in "+count+" row of "+segmentName+" is mandatory","AE");
								}
								count++;
							}
						}
					}
				}else{
					logger.info("segmentBean is null in validateFieldIsMandatory");
				}
			}
		}catch(HL7CustomException e){
			throw e;
		}
	}
	
	private void validateFieldLength(Message input,MessageFieldBean fldBean,HashMap<String,SegmentBean> segmentBeanMap,HashMap<String,String> segmentCountMapFromMsg) throws Exception{
		//System.out.println("validateFieldLength");
		try{
			if(fldBean!=null){
				String segmentName = fldBean.getSegmentName();
				String fieldName = fldBean.getFieldName();
				int repeated = Integer.parseInt(validateNull(fldBean.getRepeated(),"1"));
				int maxLength = fldBean.getMaxLength();
				String terserPath = fldBean.getTerserPath();
				String mappingType = fldBean.getMappingType();
				SegmentBean segmentBean = segmentBeanMap.get(segmentName);
				if(segmentBean!=null){
					//int segmentRepeatedCount =Integer.parseInt(validateNull(segmentBean.getRepeated(),"0"));
					int segmentCountFromMsg = Integer.parseInt(validateNull(segmentCountMapFromMsg.get(segmentName),"1"));
					if(mappingType==null || mappingType.trim().equals("")){
						if(repeated==1 && segmentCountFromMsg==1){
							String componentValue = interfaceUtil.getFieldValues(terserPath, input);
							
							if(componentValue!=null && componentValue.length()>maxLength){
								throw new HL7CustomException(fieldName+" should not contain more than "+maxLength+" characters","AE");
							} 
						}else if(repeated>1 && segmentCountFromMsg==1){
							String componentValue = interfaceUtil.getFieldValues(terserPath, input,repeated);
					
							//int length = findRepeatedComponentLength(componentValue);
							int count =1;
							String componentValuArr[] = componentValue.split("~");
							for(String value:componentValuArr){
								if(componentValue!=null && value.length()>maxLength){
									throw new HL7CustomException(fieldName+""+count+" should not contain more than "+maxLength+" characters","AE");
								} 
								count++;
							}
						}else if(repeated==1 && segmentCountFromMsg>1){
							String terserPathArr[] = terserPath.split("-");
							int count =1;
							for(int i=0;i<segmentCountFromMsg;i++){
								//String newTerserPath = terserPathArr[0]+"("+i+")-"+terserPathArr[1];
								String newTerserPath = getTerserPath(terserPathArr,i);
								logger.info("newTerserPath=="+newTerserPath);
								String componentValue = interfaceUtil.getFieldValues(newTerserPath, input);
								logger.info("componentValue=="+componentValue);
								if(componentValue!=null && componentValue.length()>maxLength){
									throw new HL7CustomException(fieldName+" in "+count+" row of "+segmentName+" should not contain more than "+maxLength+" characters","AE");
								}
								count++;
							}
						}else if(repeated>1 && segmentCountFromMsg>1){
							String terserPathArr[] = terserPath.split("-");
							int count =1;
							for(int i=0;i<segmentCountFromMsg;i++){
								//String newTerserPath = terserPathArr[0]+"("+i+")-"+terserPathArr[1];
								String newTerserPath = getTerserPath(terserPathArr,i);
								logger.info("newTerserPath=="+newTerserPath);
								String componentValue = interfaceUtil.getFieldValues(newTerserPath, input,repeated);
								logger.info("componentValue=="+componentValue);
								//int length = findRepeatedComponentLength(componentValue);
								String componentValueArr[] = componentValue.split("~");
								int rowCount = 1;
								for(String value:componentValueArr){
									if(componentValue!=null && value.length()>maxLength){
										throw new HL7CustomException(fieldName+""+rowCount+" in "+count+" row of "+segmentName+" should not contain more than "+maxLength+" characters","AE");
									}
									rowCount++;
								}
								count++;
							}
						}
					}
				}else{
					logger.info("segmentBean is null in validateFieldIsMandatory");
				}
				
			}
		}catch(HL7CustomException e){
			throw e;
		}
	}
	
	private String getTerserPath(String[] terserPathArr, int rowCount) {
		String terserPath = "";
		if(terserPathArr!=null && terserPathArr.length==2){
			terserPath = terserPathArr[0]+"("+rowCount+")-"+terserPathArr[1];
		}else if(terserPathArr!=null && terserPathArr.length==3){
			terserPath = terserPathArr[0]+"("+rowCount+")-"+terserPathArr[1]+"-"+terserPathArr[2];
		}
		return terserPath;
	}

	private int findRepeatedComponentLength(String componentValue) {
		int length = 0;
		if(componentValue!=null){
			String componentValueArr[] = componentValue.split("~");
			for(String value:componentValueArr){
				length = length+value.length();
			}
		}
		return length;
	}

	private void displaySegmentMap(HashMap<String,String> segmentMap) throws Exception{
		try{
			Iterator it = segmentMap.entrySet().iterator();
			String segmentName = null;
			String segmentCount = null; 
			while(it.hasNext()){
				Map.Entry<String,String> me = (Map.Entry<String,String>)it.next();
				segmentName = me.getKey();
				segmentCount = me.getValue();
				
			}
		}catch(Exception e){
			throw  new HL7CustomException();
		}
	}
	
	private void validateSegmentCount(HashMap<String, String> segmentMapFromMsg,HashMap<String, String> segmentMapFromDB) throws Exception {
		try{
			Iterator it = segmentMapFromDB.entrySet().iterator();
			String segmentName = null;
			String segmentCount = null;
			while(it.hasNext()){
				Map.Entry<String, String> me=(Map.Entry<String, String>) it.next();
				segmentName = me.getKey();
				segmentCount = validateNull(me.getValue(),"0");
				String segmentCountFromMsg = validateNull(segmentMapFromMsg.get(segmentName),"0");
				
				if(Integer.parseInt(segmentCountFromMsg)>Integer.parseInt(segmentCount)){
					throw new HL7CustomException(segmentName+" should not be more than "+segmentCount+" row","AE");
				}
			}
		}catch(Exception e){
			throw new HL7CustomException();
		}
	}

	private ArrayList<String> getRequiredSegmentNameListFromDB(String messageType,List<SegmentBean> segmentBeanList) throws Exception {
		ArrayList<String> segmentNameList = new ArrayList<String>();
		//System.out.println("getRequiredSegmentNameListFromDB");
		try{
			if(segmentBeanList!=null && !segmentBeanList.isEmpty()){
				String segmentName = null;
				String msgTypeFromDB = null;
				for(SegmentBean segmentBean:segmentBeanList){
					segmentName = segmentBean.getSegmentName();
					//System.out.println("segmentName=="+segmentName);
					msgTypeFromDB = segmentBean.getMessageType();
					//System.out.println("Mandatory=="+segmentBean.getIsMandatory());
					if(messageType!=null && msgTypeFromDB!=null && messageType.trim().equalsIgnoreCase(msgTypeFromDB)){
						if(segmentBean.getIsMandatory()==1){
							segmentNameList.add(segmentName);
						}
					}
				}
			}else{
				logger.info("segmentBeanList is null or empty in getRequiredSegmentNameListFromDB");
			}
		}catch(Exception e){
			throw new HL7CustomException();
		}
		return segmentNameList;
	}

	private void validateSegmentName(ArrayList<String> segmentListFromDB,ArrayList<String> segmentListFromMsg) throws Exception {
		//boolean validationSuccess = false;
		try{
			for(String segmentName:segmentListFromMsg){
				if(!segmentListFromDB.contains(segmentName)){
					throw new HL7CustomException(segmentName+" is not a correct segment;","AE");
				}
			}
		}catch(Exception e){
			throw new HL7CustomException();
		}
	}
	
	private void validateRequirdSegmentName(ArrayList<String> requiredSegmentNameList,ArrayList<String> segmentListFromMsg) throws Exception {
		try{
			for(String segmentName:requiredSegmentNameList){
				if(!segmentListFromMsg.contains(segmentName)){
					throw new HL7CustomException(segmentName+" is Mandatory.It is not available in the Message","AE");
				}
			}
		}catch(Exception e){
			throw new HL7CustomException();
		}
	}

	private void displayNameList(ArrayList<String> nameList) throws Exception{
		try{
			for(String name:nameList){
				logger.info(name);
			}
		}catch(Exception e){
			throw new HL7CustomException();
		}
	}
	
	private ArrayList<String> getSegmentNameListFromDB(String messageType,List<SegmentBean> beanList) throws Exception{
		ArrayList<String> segmentNameList = new ArrayList<String>();
		try{
			if(beanList!=null && !beanList.isEmpty()){
				String segmentName = null;
				String msgTypeFromDB = null;
				for(SegmentBean segmentBean:beanList){
					segmentName = segmentBean.getSegmentName();
					msgTypeFromDB = segmentBean.getMessageType();
					if(msgTypeFromDB!=null && messageType!=null && messageType.trim().equalsIgnoreCase(msgTypeFromDB)){
						if(segmentName!=null && !segmentNameList.contains(segmentName)){
							segmentNameList.add(segmentName);
						}
					}
				}
			}else{
				logger.info("beanList is null or empty in getSegmentNameListFromDB");
			}
		}catch(Exception e){
			throw new HL7CustomException();
		}
		return segmentNameList;
	}
	
	private ArrayList<String> getSegmentNameListFromMessage(String msg) throws Exception {
		ArrayList<String> segmentList = new ArrayList<String>();
		try{
			if(msg!=null && !msg.trim().equals("")){
				String segmentArray[] = msg.split("\r");
				String segmentName = null;
				for(String segment:segmentArray){
					segmentName = getSegmentName(segment);
					//logger.info("segmentName=="+segmentName);
					if(segmentName!=null && !segmentList.contains(segmentName)){
						segmentList.add(segmentName);
					}
				}
			}else{
				logger.info("msg is null or empty in getSegmentNameListFromMessage");
			}
		}catch(HL7CustomException e){
			throw e;
		}
		return segmentList;
	}

	private HashMap<String, String> getSegementCountMapFromMessage(String msg) throws Exception{
		HashMap<String,String> segmentMap = new HashMap<String,String>();
		try{
			if(msg!=null && !msg.trim().equals("")){
				String segmentArray[] = msg.split("\r");
				String segmentName = null;
				String oldSegmentName = null;
				ArrayList segmentNameList = new ArrayList();
				for(String segment:segmentArray){
					segmentName = getSegmentName(segment);
					if(segmentName!=null && oldSegmentName!=null && !segmentName.trim().equals(oldSegmentName)){
						segmentMap.put(oldSegmentName, String.valueOf(segmentNameList.size()));
						segmentNameList = new ArrayList();
					}
					segmentNameList.add(segmentName);
					oldSegmentName = segmentName;
				}
				if(oldSegmentName!=null){
					segmentMap.put(oldSegmentName,String.valueOf(segmentNameList.size()));
				}
			}else{
				logger.info("msg is null or empty in getSegementMapFromMessage");
			}
		}catch(HL7CustomException e){
			throw e;
		}
		return segmentMap;
	}
	
	private HashMap<String, String> getSegmentCountMapFromDB(String messageType,List<SegmentBean> segmentBeanList) throws Exception{
		HashMap<String,String> segmentMap = new HashMap<String,String>();
		try{
			if(segmentBeanList!=null && !segmentBeanList.isEmpty()){
				String segmentName = null;
				String repeated = null;
				String msgTypeFromDB = null;
				for(SegmentBean segmentBean:segmentBeanList){
					segmentName = segmentBean.getSegmentName();
					repeated = segmentBean.getRepeated();
					msgTypeFromDB = segmentBean.getMessageType();
					if(messageType!=null && msgTypeFromDB!=null && messageType.trim().equalsIgnoreCase(msgTypeFromDB)){
						segmentMap.put(segmentName, repeated);
					}
				}
			}else{
				logger.info("segmentBeanList is null or empty in getSegmentMapFromDB");
			}
		}catch(Exception e){
			throw new HL7CustomException();
		}
		return segmentMap;
	}
	
	private HashMap<String,SegmentBean> getSegmentBeanMapFromDB(String messageType,List<SegmentBean> segmentBeanList) throws Exception{
		HashMap<String,SegmentBean> segmentBeanMap = new HashMap<String,SegmentBean>();
		try{
			if(segmentBeanList!=null && !segmentBeanList.isEmpty()){
				String segmentName = null;
				String msgTypeFromDB = null;
				for(SegmentBean segmentBean:segmentBeanList){
					segmentName = segmentBean.getSegmentName();
					msgTypeFromDB = segmentBean.getMessageType();
					
					if(messageType!=null && msgTypeFromDB!=null && messageType.trim().equalsIgnoreCase(msgTypeFromDB)){
						segmentBeanMap.put(segmentName, segmentBean);
					}
				}
			}else{
				logger.info("segmentBeanList is null or empty in getSegmentBeanMapFromDB");
			}
		}catch(Exception e){
			throw new HL7CustomException();
		}
		return segmentBeanMap;
	}
	
	private String getSegmentName(String segment) throws Exception{
		String segmentName = null;
		try{
			if(segment!=null && !segment.trim().equals("")){
				int length = segment.indexOf("|");
				segmentName = segment.substring(0, length);
			}else{
				logger.info("segment is null or empty in getSegmentName");
			}
		}catch(Exception e){
			throw new HL7CustomException(e.getMessage());
		}
		return segmentName;
	}
	
	private String validateNull(String value,String defaultVal) throws Exception{
		if(value==null){
			return defaultVal;
		}else{
			return value;
		}
	}
	
	private String validateNull(String value) throws Exception{
		if(value==null){
			return "";
		}else{
			return value;
		}
	}
}
