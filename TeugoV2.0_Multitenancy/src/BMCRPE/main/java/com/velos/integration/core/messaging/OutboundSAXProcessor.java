package com.velos.integration.core.messaging;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.velos.integration.core.processor.StudyStatusMessageProcessor;
import com.velos.services.OperationException_Exception;

public class OutboundSAXProcessor {
	private StudyStatusMessageProcessor processor;

	public StudyStatusMessageProcessor getProcessor() {
		return processor;
	}

	public void setProcessor(StudyStatusMessageProcessor processor) {
		this.processor = processor;
	}

	public void processBySAXParser(String xml) {
		try {
			byte[] byteString = xml.getBytes();
			ByteArrayInputStream in = new ByteArrayInputStream(byteString);
			InputSource inputSource = new InputSource(in);
			SAXParserFactory factory = SAXParserFactory.newInstance();

			SAXParser parser;

			parser = factory.newSAXParser();

			CustomHandler handler = new CustomHandler();
			parser.parse(inputSource, handler);

			if (handler.getParentOID().size() == 1
					&& handler.getParentOID().containsKey(
							IdentifierConstants.SIMPLE_IDENTIFIER)) {
				this.getProcessor().processStudyStatMessage(handler, xml);
			}
			//commented by Rajasekhar 
			/*if (handler.getParentOID().size() == 1
					&& handler.getParentOID().containsKey(
							IdentifierConstants.PATIENT_IDENTIFIER)) {
				this.getProcessor().processEnrollpatStudyStatMessage(xml);
			}*/
			
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (OperationException_Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static void main(String args[]){
		try {
			
			
					String xml =	"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
							+"<changes>"
							+"<change>"
							+"<action>CREATE</action>"
							+"<statusSubType>ReadyforEpic</statusSubType>"
							+"<statusCodeDesc>Ready for Epic Build</statusCodeDesc>"
							+"<identifier xsi:type=\"studyStatusIdentifier\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"
							+"<OID>5a4319e5-5a91-4764-9c5b-a0f56eeba205</OID>"
							+"</identifier>"
							+"<module>study_status</module>"
							+"<timeStamp>10/12/2017 12:15:03</timeStamp>"
							+"</change>"
							+"<parentIdentifier>"
							+"<id xsi:type=\"studyIdentifier\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"
							+"<OID>8412a14a-ba60-4098-a072-247bd7157849</OID>"
							+"<studyNumber>velosTestStudy</studyNumber>"
							+"</id>"
							+"</parentIdentifier>"
							+"</changes>";
					/*
					<changes>
				    <change>
				        <action>CREATE</action>
				        <statusSubType>active on-inter</statusSubType>
				        <statusCodeDesc>On-Intervention</statusCodeDesc>
				        <identifier xsi:type="patientStudyStatusIdentifier"
				xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
				            <OID>0856ebe4-2d70-4104-91ca-dfa5292587da</OID>
				        </identifier>
				        <module>patstudy_status</module>
				        <timeStamp>01/16/2018 02:21:40</timeStamp>
				    </change>
				    <parentIdentifier>
				        <id xsi:type="patientProtocolIdentifier"
				xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
				            <OID>e7f54134-e490-41f8-b182-9b299cc585f7</OID>
				            <studyIdentifier>
				                <studyNumber>H-11011</studyNumber>
				            </studyIdentifier>
				            <patientIdentifier>
				                <patientId>VCT-0000575</patientId>
				                <organizationId>
				                    <siteName>Boston Medical Center</siteName>
				                </organizationId>
				            </patientIdentifier>
				        </id>
				    </parentIdentifier>
				</changes>*/
			//EPIC Test H-11111D Device Trial
			StudyStatusMessageProcessor processor =new StudyStatusMessageProcessor();
			byte[] byteString = xml.getBytes();
			ByteArrayInputStream in = new ByteArrayInputStream(byteString);
			InputSource inputSource = new InputSource(in);
			SAXParserFactory factory = SAXParserFactory.newInstance();

			SAXParser parser;

			parser = factory.newSAXParser();

			CustomHandler handler = new CustomHandler();
			parser.parse(inputSource, handler);

			if (handler.getParentOID().size() == 1
					&& handler.getParentOID().containsKey(
							IdentifierConstants.SIMPLE_IDENTIFIER)) {
				processor.processStudyStatMessage(handler, xml);
			}
			//commented by Rajasekhar 
			/*if (handler.getParentOID().size() == 1
					&& handler.getParentOID().containsKey(
							IdentifierConstants.PATIENT_IDENTIFIER)) {
				this.getProcessor().processEnrollpatStudyStatMessage(xml);
			}*/
			
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (OperationException_Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
}
