package com.velos.epic.service;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.addressing.client.ActionCallback;
import org.springframework.ws.soap.addressing.version.Addressing10;
import org.springframework.context.ApplicationContext;

import com.velos.epic.XmlProcess;
import com.velos.integration.dao.MessageDAO;
import com.velos.integration.hl7.notifications.OperationCustomException;
import com.velos.integration.mapping.ProtocolKeys;
import com.velos.integration.mapping.VelosKeys;

@Component
public class EpicEndpointClient {

	private static Logger logger = Logger.getLogger("epicLogger");   
	private static ResourceBundle bundle = ResourceBundle.getBundle("epic-testclient-req");
	Properties prop = null;
	private MessageDAO messageDao = null;
	XmlProcess xp=new XmlProcess();
	
   // Map<String,String> messageMap=new HashMap<String,String>();
	public String sendRequest(Map<VelosKeys,Object> resultMap) throws Exception  {
		logger.info("*** RetrieveProtocolDefRequest Outbound starts ***");
		String requestXml = null;
		String studyId = (String) resultMap.get(ProtocolKeys.StudyId);
		requestXml=xp.xmlGenerating(studyId);
		//RetrieveProtocolDefRequest Outbound XML Generating.
		/*EpicEndpointClient client = new EpicEndpointClient();
		client.handleRequest(ProtocolKeys.RetrieveProtocol, requestXml);*/
		return requestXml;
	}
	public String enrollPatientRequest(Map<VelosKeys,Object> resultMap) throws OperationCustomException{
		logger.info("*** EnrollPatientRequest Outbound starts ***");
		String	requestXml=xp.enrollPatientMessage(resultMap);
	/*	EpicEndpointClient client = new EpicEndpointClient();
		client.handleRequest(ProtocolKeys.EnrollPatientRequest, requestXml);*/
		return requestXml;
	}
	public String alertProtocolState(Map<VelosKeys, Object> dataMap) throws OperationCustomException{
		logger.info("*** AlertProtocolStateRequest Outbound starts ***");
		String	requestXml=xp.alertProtocolState(dataMap);
		/*EpicEndpointClient client = new EpicEndpointClient();
		client.handleRequest(ProtocolKeys.AlertProtocolState,requestXml);*/
		return requestXml;
	}
	@SuppressWarnings("resource")
	public String handleRequest(ProtocolKeys request, Map<VelosKeys, Object> dataMap) throws OperationCustomException, Exception   {
		/*if (requestXml == null) { return null; }*/
		logger.info("\n Outbound Request ===>"+request+"\n Map Values ====>"+dataMap);
		System.out.println("\n Outbound Request ===>"+request+"\n Map Values ====>"+dataMap);
		String body = null;
		String protocol=null;
		StringWriter writer = new StringWriter();
		ClassPathXmlApplicationContext context = null,epicContext = null;
		try{
			context=new ClassPathXmlApplicationContext("epic-testclient-req.xml");
			epicContext = new ClassPathXmlApplicationContext("epic-context.xml");
			messageDao = (MessageDAO) epicContext.getBean("messageDao");
			switch (request) {
			case RetrieveProtocol:
				body=sendRequest(dataMap);
				protocol="RetrieveProtocolDefResponse";
				break;
			case EnrollPatientRequest: 
			//	System.out.println("EnrollPatientRequest Starts ***********************");
				logger.info("EnrollPatientRequest Starts ***********************");
				body= enrollPatientRequest(dataMap);
				protocol="EnrollPatientRequest";
				break;
			case AlertProtocolState:
				body=alertProtocolState(dataMap);
				protocol="AlertProtocolState";
				break;
			default: body = ""; break;
			}
			logger.info("\n\n********** Request Outbound Message *******\n\n"+body);
			WebServiceTemplate webServiceTemplate = (WebServiceTemplate)context.getBean("webServiceTemplate");
			StreamSource source = new StreamSource(new StringReader(body));
			StreamResult result = new StreamResult(writer);
			logger.info(" ************ Message send starts *************");
			sslJavaTrustStore();
			webServiceTemplate.sendSourceAndReceiveToResult(source,wasHeader(protocol),result);
			//Auditing Request and Response Message into vgdb_epicmessaging_audit_values table
			messageDao.saveMessageDetails("Outbound",body,writer.toString());
			logger.info("\n\n************ Response Outbound Message  *************\n\n"+writer.toString());
			
		}catch(OperationCustomException e){
			e.printStackTrace();
			logger.info(" OutboundMessage********OpearationCustomException Block*******"+e.getLocalizedMessage()+"\n"+e.getResponseType());
			//messageDao.saveMessageDetails("Outbound",e.getLocalizedMessage(),e.getResponseType());
			messageDao.saveMessageDetails("Outbound",body,e.getResponseType());
			throw new OperationCustomException(e.getMessage());
		}catch(Exception e){
			e.printStackTrace();
			logger.info("Outbound Message********Exception Block*******"+e.getLocalizedMessage()+"\n"+e.getMessage());
			//messageDao.saveMessageDetails("Outbound",e.getLocalizedMessage(),e.getMessage());
			String errorMsg=e.getLocalizedMessage();
			logger.info("Exception block Request sent Body ====>"+errorMsg);
			/*if(errorMsg != null){
				body=errorMsg;
			}*/System.out.println("Body of the Message =====>"+body);
			messageDao.saveMessageDetails("Outbound",body,e.getMessage());
			//throw new OperationCustomException(e.getMessage());
		}finally{
			context.close();
			epicContext.close();
			logger.info("closed classpath application context on EpicEndpointClient");
		}
		return writer.toString();
	}
	public static String getProperty(String key) {
		return bundle.getString(key);
	}
	@Bean
	public ActionCallback wasHeader(String protocol) throws Exception {
		ActionCallback header = null;
		try {					  
			URI action = new URI("urn:ihe:qrph:rpe:2009:"+protocol);
			Addressing10 version = new Addressing10();
			String scheme = getProperty("epic.scheme");
			String host = getProperty("epic.host");
			String port = getProperty("epic.port");
			String context = getProperty("epic.context");
			logger.info("The EPIC  URL===================>"+scheme+host+port+context);
			if (context != null && !context.startsWith("/")) {
				context = "/" + context;
			}
			URI to = null;
			int portInt = -1;
			try {
				portInt = Integer.parseInt(port);
				logger.info("PortInt =================>"+portInt);
			} catch(NumberFormatException e) {
				logger.error("*************Exception Caught form EPIC *************");
				portInt = 443;
			}
			to = (new URL(scheme, host, portInt, context)).toURI();
			header = new ActionCallback(action, version, to);
		} catch(Exception e) {
			logger.info("Error Response in wasHeaderMethod -->"+ e.getMessage());
			e.printStackTrace();
			throw new OperationCustomException(e.getMessage());
		}
		return header;
	}
	
	
	/*@SuppressWarnings("finally")
    public   void validateUser(){
		
		
		
        System.setProperty("javax.net.ssl.keyStore", "/velos/Teaugo_RPE_Server/jdk1.8.0_74/jre/lib/security/teaugorpe2.jks");
        
        System.setProperty("javax.net.ssl.keyStorePassword", "changeit");

        System.setProperty("javax.net.ssl.keyStoreType", "jks");

        System.setProperty("javax.net.ssl.trustStore", "/velos/Teaugo_RPE_Server/jdk1.8.0_74/jre/lib/security/teaugorpe2.jks");

        System.setProperty("javax.net.ssl.trustStorePassword", "changeit");

        System.setProperty("javax.net.ssl.trustStoreType", "jks");

        }*/
	
	@SuppressWarnings("finally")
	public   void sslJavaTrustStore(){

		try {
			prop = new Properties();
			prop.load(this.getClass().getClassLoader()
					.getResourceAsStream("epic.properties"));

			String sslEnable = prop.getProperty("SSL_Enable");

			if(!prop.containsKey("SSL_Enable") || prop.getProperty("SSL_Enable").equalsIgnoreCase("No")
					|| prop.getProperty("SSL_Enable").equals("")){
				logger.info("############# SSL IS DISABLED ############");
			}else if(prop.containsKey("SSL_Enable") && prop.getProperty("SSL_Enable").equalsIgnoreCase("yes")
					&& !prop.getProperty("SSL_Enable").equals("")){

				String keyStore = prop.getProperty("keyStore");
				System.setProperty("javax.net.ssl.keyStore",keyStore);

				String keyStorePassword = prop.getProperty("keyStorePassword");
				System.setProperty("javax.net.ssl.keyStorePassword",keyStorePassword);

				String keyStoreType = prop.getProperty("keyStoreType");
				System.setProperty("javax.net.ssl.keyStoreType",keyStoreType);

				String trustStore = prop.getProperty("trustStore");
				System.setProperty("javax.net.ssl.trustStore",trustStore);

				String trustStorePassword = prop.getProperty("trustStorePassword");
				System.setProperty("javax.net.ssl.trustStorePassword", trustStorePassword);

				String trustStoreType = prop.getProperty("trustStoreType");
				System.setProperty("javax.net.ssl.trustStoreType",trustStoreType);
			}
		} catch (Exception e) {
			logger.error("**epic.properties file load error on sslJavaTruststore method**==>"+e.getMessage());
			e.printStackTrace();
		}finally{
			prop = null;
		}

	}
}





