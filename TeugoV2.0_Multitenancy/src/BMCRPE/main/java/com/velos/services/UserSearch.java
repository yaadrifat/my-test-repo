
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for userSearch complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="userSearch">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="firstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="group" type="{http://velos.com/services/}groupIdentifier" minOccurs="0"/>
 *         &lt;element name="jobType" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="lastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="organization" type="{http://velos.com/services/}organizationIdentifier" minOccurs="0"/>
 *         &lt;element name="pageNumber" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="pageSize" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="sortBy" type="{http://velos.com/services/}userSearchOrderBy" minOccurs="0"/>
 *         &lt;element name="sortOrder" type="{http://velos.com/services/}sortOrder" minOccurs="0"/>
 *         &lt;element name="studyTeam" type="{http://velos.com/services/}studyTeamIdentifier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "userSearch", propOrder = {
    "firstName",
    "group",
    "jobType",
    "lastName",
    "organization",
    "pageNumber",
    "pageSize",
    "sortBy",
    "sortOrder",
    "studyTeam"
})
public class UserSearch
    extends ServiceObject
{

    protected String firstName;
    protected GroupIdentifier group;
    protected Code jobType;
    protected String lastName;
    protected OrganizationIdentifier organization;
    protected int pageNumber;
    protected int pageSize;
    protected UserSearchOrderBy sortBy;
    protected SortOrder sortOrder;
    protected StudyTeamIdentifier studyTeam;

    /**
     * Gets the value of the firstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the value of the firstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstName(String value) {
        this.firstName = value;
    }

    /**
     * Gets the value of the group property.
     * 
     * @return
     *     possible object is
     *     {@link GroupIdentifier }
     *     
     */
    public GroupIdentifier getGroup() {
        return group;
    }

    /**
     * Sets the value of the group property.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupIdentifier }
     *     
     */
    public void setGroup(GroupIdentifier value) {
        this.group = value;
    }

    /**
     * Gets the value of the jobType property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getJobType() {
        return jobType;
    }

    /**
     * Sets the value of the jobType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setJobType(Code value) {
        this.jobType = value;
    }

    /**
     * Gets the value of the lastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the value of the lastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastName(String value) {
        this.lastName = value;
    }

    /**
     * Gets the value of the organization property.
     * 
     * @return
     *     possible object is
     *     {@link OrganizationIdentifier }
     *     
     */
    public OrganizationIdentifier getOrganization() {
        return organization;
    }

    /**
     * Sets the value of the organization property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganizationIdentifier }
     *     
     */
    public void setOrganization(OrganizationIdentifier value) {
        this.organization = value;
    }

    /**
     * Gets the value of the pageNumber property.
     * 
     */
    public int getPageNumber() {
        return pageNumber;
    }

    /**
     * Sets the value of the pageNumber property.
     * 
     */
    public void setPageNumber(int value) {
        this.pageNumber = value;
    }

    /**
     * Gets the value of the pageSize property.
     * 
     */
    public int getPageSize() {
        return pageSize;
    }

    /**
     * Sets the value of the pageSize property.
     * 
     */
    public void setPageSize(int value) {
        this.pageSize = value;
    }

    /**
     * Gets the value of the sortBy property.
     * 
     * @return
     *     possible object is
     *     {@link UserSearchOrderBy }
     *     
     */
    public UserSearchOrderBy getSortBy() {
        return sortBy;
    }

    /**
     * Sets the value of the sortBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserSearchOrderBy }
     *     
     */
    public void setSortBy(UserSearchOrderBy value) {
        this.sortBy = value;
    }

    /**
     * Gets the value of the sortOrder property.
     * 
     * @return
     *     possible object is
     *     {@link SortOrder }
     *     
     */
    public SortOrder getSortOrder() {
        return sortOrder;
    }

    /**
     * Sets the value of the sortOrder property.
     * 
     * @param value
     *     allowed object is
     *     {@link SortOrder }
     *     
     */
    public void setSortOrder(SortOrder value) {
        this.sortOrder = value;
    }

    /**
     * Gets the value of the studyTeam property.
     * 
     * @return
     *     possible object is
     *     {@link StudyTeamIdentifier }
     *     
     */
    public StudyTeamIdentifier getStudyTeam() {
        return studyTeam;
    }

    /**
     * Sets the value of the studyTeam property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyTeamIdentifier }
     *     
     */
    public void setStudyTeam(StudyTeamIdentifier value) {
        this.studyTeam = value;
    }

}
