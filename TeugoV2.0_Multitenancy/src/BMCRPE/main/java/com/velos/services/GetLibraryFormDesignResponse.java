
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getLibraryFormDesignResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getLibraryFormDesignResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FormDesign" type="{http://velos.com/services/}formDesign" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getLibraryFormDesignResponse", propOrder = {
    "formDesign"
})
public class GetLibraryFormDesignResponse {

    @XmlElement(name = "FormDesign")
    protected FormDesign formDesign;

    /**
     * Gets the value of the formDesign property.
     * 
     * @return
     *     possible object is
     *     {@link FormDesign }
     *     
     */
    public FormDesign getFormDesign() {
        return formDesign;
    }

    /**
     * Sets the value of the formDesign property.
     * 
     * @param value
     *     allowed object is
     *     {@link FormDesign }
     *     
     */
    public void setFormDesign(FormDesign value) {
        this.formDesign = value;
    }

}
