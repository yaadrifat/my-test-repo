package com.velos.epic.service;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.velos.integration.core.processor.StudyStatusMessageProcessor;
import com.velos.integration.espclient.VelosEspClient;
import com.velos.integration.espclient.VelosEspMethods;
import com.velos.integration.hl7.notifications.OperationCustomException;
import com.velos.integration.mapping.EndpointKeys;
import com.velos.integration.mapping.ProtocolKeys;
import com.velos.integration.mapping.VelosKeys;

public class StudyProcess {
	private static Logger logger = Logger.getLogger("epicLogger");
	
	
	Map<VelosKeys,String> resultMap=new HashMap<VelosKeys,String>();
	EpicEndpointClient eepc=new EpicEndpointClient();

	//get study details for outbound RetriveProtocolDef Response service
	public String getStudyDetails(String studyNumber,Map<VelosKeys, Object> dataMap) throws Exception{
		try {
		EpicEndpointClient eepc=null ; 
		eepc = new EpicEndpointClient();
		logger.info("****************RetriveProtocolDef Response Starts **********************");
			/*
			  Map<VelosKeys, Object> dataMap1 =null; dataMap1 = new HashMap<VelosKeys,
			  Object>();
			  dataMap1.put(ProtocolKeys.StudyId,studyNumber);
			 */
			return eepc.handleRequest(ProtocolKeys.RetrieveProtocol,dataMap);
			//eepc.sendRequest(studyNumber);
			
		}catch(OperationCustomException e){
			throw e;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	//get EnrollPatienttoStudy Details for outbound Service
	public String getEnrollPatientStudyDetails(String studyId, String patientId,Map<VelosKeys,Object> OutboundMap) throws Exception{
		logger.info(" **************EnrollPatientStudy Starts ******************");
		//resultMap= new HashMap<VelosKeys,String>();
		if(studyId != null && patientId != null){
			resultMap.put(ProtocolKeys.StudyId,studyId);
			resultMap.put(ProtocolKeys.PatientID, patientId);
			logger.info("StudyId--->"+studyId);
			logger.info("*******************Incoming Values ---->"+resultMap);
			

			VelosEspClient client = null;
			client = new VelosEspClient();
			resultMap.put(EndpointKeys.Endpoint, EndpointKeys.Epic.toString());

			Map<VelosKeys, Object> dataMap = client.handleRequest(VelosEspMethods.PatientDetails,resultMap);
			//Throwing FaultException;
			if(dataMap.get(ProtocolKeys.FaultString) != null){
				String faultString = (String) dataMap.get(ProtocolKeys.FaultString);
				throw new OperationCustomException(faultString);
			}

			resultMap.put(ProtocolKeys.OID,(String) dataMap.get(ProtocolKeys.OID));
			Map<VelosKeys, Object>	dataMap1 = client.handleRequest(VelosEspMethods.PatientStudyGetStudyPatientStatusHistory,resultMap);
			//Throwing FaultException;
			if(dataMap1.get(ProtocolKeys.FaultString) != null){
				String faultString = (String) dataMap.get(ProtocolKeys.FaultString);
				throw new OperationCustomException(faultString);
			}
			
			
			resultMap.put(ProtocolKeys.StudyPatOID,(String) dataMap1.get(ProtocolKeys.OID));	
			System.out.println("StudyPatStatus="+dataMap1.get(ProtocolKeys.StudyPatStatus));

			Map<VelosKeys, Object>	dataMap2 = client.handleRequest(VelosEspMethods.StudyPatientStatus,resultMap);
			//Throwing FaultException;
			if(dataMap2.get(ProtocolKeys.FaultString) != null){
				String faultString = (String) dataMap.get(ProtocolKeys.FaultString);
				throw new OperationCustomException(faultString);
			}
			logger.info("Patient Data --->"+dataMap.isEmpty());
			logger.info(dataMap);
			dataMap.put(ProtocolKeys.StudyId,studyId);
			dataMap.put(ProtocolKeys.StudyPatStatus,dataMap1.get(ProtocolKeys.StudyPatStatus));
			dataMap.put(ProtocolKeys.StudyPatId,dataMap2.get(ProtocolKeys.StudyPatId));
			dataMap.put(ProtocolKeys.OutboundOID,OutboundMap.get(ProtocolKeys.OutboundOID));
			dataMap.put(ProtocolKeys.PatientId,OutboundMap.get(ProtocolKeys.PatientId));
			dataMap.put(ProtocolKeys.ModuleName,OutboundMap.get(ProtocolKeys.ModuleName));
			dataMap.put(ProtocolKeys.PkId,OutboundMap.get(ProtocolKeys.PkId));
			//eepc.enrollPatientRequest(dataMap);
			return eepc.handleRequest(ProtocolKeys.EnrollPatientRequest,dataMap);
		}
		else{
			logger.info("StudyId or PatientId should not null");
		}
		logger.info(" **************EnrollPatientStudy Ends ******************");
		return "true";
	}
	public String getAlertProtocolState(String studyId, String patientId,Map<VelosKeys,Object> OutboundMap) throws Exception{
		logger.info(" **************AlertProtocolState Starts ******************");
		if(studyId != null && patientId != null){
			resultMap.put(ProtocolKeys.StudyId,studyId);
			resultMap.put(ProtocolKeys.PatientID, patientId);
			logger.info("StudyId--->"+studyId);
			logger.info("PatientId--->"+patientId);
			logger.info("Incoming Values ---->"+resultMap);
			Map<VelosKeys, Object> dataMap = null;
			Map<VelosKeys, Object> dataMap1 = null;
			Map<VelosKeys, Object> dataMap2 = null;
			VelosEspClient client = null;
			client = new VelosEspClient();
			resultMap.put(EndpointKeys.Endpoint, EndpointKeys.Epic.toString());
			 dataMap = client.handleRequest(VelosEspMethods.PatientDetails,resultMap);
			if(dataMap.get(ProtocolKeys.FaultString) != null){
				String faultString = (String) dataMap.get(ProtocolKeys.FaultString);
				throw new OperationCustomException(faultString);
			}
			resultMap.put(ProtocolKeys.OID,(String) dataMap.get(ProtocolKeys.OID));
			 dataMap1 = client.handleRequest(VelosEspMethods.PatientStudyGetStudyPatientStatusHistory,resultMap);
			if(dataMap1.get(ProtocolKeys.FaultString) != null){
				String faultString = (String) dataMap.get(ProtocolKeys.FaultString);
				throw new OperationCustomException(faultString);
			}
			resultMap.put(ProtocolKeys.StudyPatOID,(String) dataMap1.get(ProtocolKeys.OID));	
			 dataMap2 = client.handleRequest(VelosEspMethods.StudyPatientStatus,resultMap);
			if(dataMap2.get(ProtocolKeys.FaultString) != null){
				String faultString = (String) dataMap.get(ProtocolKeys.FaultString);
				throw new OperationCustomException(faultString);
			}
			System.out.println("Patient Data --->"+dataMap.isEmpty());
			System.out.println(dataMap);
			dataMap.put(ProtocolKeys.StudyId,studyId);
			dataMap.put(ProtocolKeys.StudyPatStatus,dataMap1.get(ProtocolKeys.StudyPatStatus));
			dataMap.put(ProtocolKeys.StudyPatId,dataMap2.get(ProtocolKeys.StudyPatId));
			dataMap.put(ProtocolKeys.OutboundOID,OutboundMap.get(ProtocolKeys.OutboundOID));
			dataMap.put(ProtocolKeys.PatientId,OutboundMap.get(ProtocolKeys.PatientId));
			dataMap.put(ProtocolKeys.ModuleName,OutboundMap.get(ProtocolKeys.ModuleName));
			dataMap.put(ProtocolKeys.PkId,OutboundMap.get(ProtocolKeys.PkId));
			return eepc.handleRequest(ProtocolKeys.AlertProtocolState,dataMap);
			//eepc.alertProtocolState(dataMap);
		}
		else{
			logger.info("AlertProtocolState StudyId or PatientId should not null");
		}
		logger.info(" **************AlertProtocolState ends ******************");
		return "true";
	}
}
