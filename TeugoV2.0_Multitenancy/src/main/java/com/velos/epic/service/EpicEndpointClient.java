package com.velos.epic.service;

import java.io.StringReader;
import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import java.io.StringWriter;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.integration.Message;
import org.springframework.integration.MessageChannel;
import org.springframework.integration.core.MessagingTemplate;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.addressing.client.ActionCallback;
import org.springframework.ws.soap.addressing.version.Addressing10;
import org.springframework.context.ApplicationContext;

import com.velos.epic.XmlProcess;
import com.velos.integration.dao.MessageDAO;
import com.velos.integration.hl7.notifications.OperationCustomException;
import com.velos.integration.mapping.ProtocolKeys;
import com.velos.integration.mapping.VelosKeys;

@Component
public class EpicEndpointClient  {

	private static Logger logger = Logger.getLogger("epicLogger");   
	private static ResourceBundle bundle = ResourceBundle.getBundle("epic");
	private MessageDAO messageDao = null;
	int dbCount =0;
	String body = null;
	//Properties prop = null;
	StringWriter writer = null;
    Map<String,String> messageMap=new HashMap<String,String>();
    String requestXml = null;
	public String sendRequest(Map<VelosKeys,Object> resultMap) throws Exception  {
		
		String studyId = (String) resultMap.get(ProtocolKeys.StudyId);
		XmlProcess xp= null;
		xp = new XmlProcess();
		requestXml=xp.xmlGenerating(studyId);
		/*EpicEndpointClient client = new EpicEndpointClient();
		client.handleRequest(ProtocolKeys.RetrieveProtocol, requestXml);*/
		return requestXml;
	}
	public String enrollPatientRequest(Map<VelosKeys,Object> resultMap) throws OperationCustomException{
		XmlProcess xp= null;
		xp = new XmlProcess();
		 requestXml = null;
		requestXml=xp.enrollPatientMessage(resultMap);
	/*	EpicEndpointClient client = new EpicEndpointClient();
		client.handleRequest(ProtocolKeys.EnrollPatientRequest, requestXml);*/
		return requestXml;
	}
	public String alertProtocolState(Map<VelosKeys, Object> dataMap) throws OperationCustomException{
		XmlProcess xp= null;
		xp = new XmlProcess();
		requestXml=xp.alertProtocolState(dataMap);
		/*EpicEndpointClient client = new EpicEndpointClient();
		client.handleRequest(ProtocolKeys.AlertProtocolState,requestXml);*/
		return requestXml;
	}
	public String studyCalendar(Map<VelosKeys, Object> dataMap) throws OperationCustomException{
		XmlProcess xp= null;
		xp = new XmlProcess();
		 requestXml = null;
		requestXml=xp.studyCalendarMessage(dataMap);
		/*EpicEndpointClient client = new EpicEndpointClient();
		client.handleRequest(ProtocolKeys.AlertProtocolState,requestXml);*/
		return requestXml;
	}
	@SuppressWarnings("resource")
	public String handleRequest(ProtocolKeys request, Map<VelosKeys, Object> dataMap) throws Exception   {
		/*if (requestXml == null) { return null; }*/
		logger.info("\n"+request+"\n Map Values ====>"+dataMap);
	
		String protocol=null;
		String response = null;
		String simulator = null;
		simulator = getProperty("simulator").trim();
		ClassPathXmlApplicationContext context = null,epicContext = null;
		try{
			context=new ClassPathXmlApplicationContext("epic-testclient-req.xml");
			epicContext = new ClassPathXmlApplicationContext("epic-context.xml");
			messageDao = (MessageDAO) epicContext.getBean("messageDao");
			switch (request) {
			case RetrieveProtocol:
				logger.info("Study Summary Request starts ...........");
				body=sendRequest(dataMap);
				protocol="RetrieveProtocolDefResponse";
				break;
			case EnrollPatientRequest: 
				logger.info("EnrollPatientRequest Starts   ***********************");
				body= enrollPatientRequest(dataMap);
				protocol="EnrollPatientRequest";
				break;
			case AlertProtocolState:
				logger.info("AlertProtocol Request Starts ....................");
				body=alertProtocolState(dataMap);
				protocol="AlertProtocolState";
				break;
			case RetrieveProtocolStudyCalendar:
				logger.info("RetrieveProtocolStudyCalendar starts ................");
				body=studyCalendar(dataMap);
				protocol="RetrieveProtocolDefResponse";
				break;
			default: body = ""; break;
			}
			logger.info("\n\n********** Request Outbound Message to Client *******\n\n"+body);
			/*
			 * WebServiceTemplate webServiceTemplate =
			 * (WebServiceTemplate)context.getBean("webServiceTemplate"); StreamSource
			 * source = new StreamSource(new StringReader(body)); StreamResult result = new
			 * StreamResult(writer); logger.
			 * info(" ************ Message send starts and Checking SSL Cert*************");
			 * sslJavaTrustStore(); boolean resultVal =
			 * webServiceTemplate.sendSourceAndReceiveToResult(source,wasHeader(protocol),
			 * result);
			 */
			  dbCount=0;
			//  response =  sendAndReceive(dataMap,body,protocol,context);
			
			  WebServiceTemplate webServiceTemplate = (WebServiceTemplate)context.getBean("webServiceTemplate");
				StreamSource source = new StreamSource(new StringReader(body));
				StreamResult result = new StreamResult(writer);
				logger.info(" ************ Message send starts and Checking SSL Cert*************");
				sslJavaTrustStore();
				boolean resultVal =true;
				 
				/***
				***Simulator for Testing outbound Message.
				****Added by Surendra M 10 July 2019
				****/
				if(simulator.equalsIgnoreCase("Yes")) {
					logger.info("Simulator Testing ON............");
					////Simulator Method
					response = process(body);
					if(response.equalsIgnoreCase("false")) {
						throw new OperationCustomException("Simulator not started or not responding.........");
					}
				logger.info("Simulator Result =======>"+response);
				logger.info("Response =======>"+response);
				//Auditing Request and Response Message into vgdb_epicmessaging_audit_values table
				messageDao.saveMessageDetails("Outbound",body,response,dataMap);
				/********************************************/
				}else if(simulator.equalsIgnoreCase("No")) {
					logger.info("Sending to Epic ON............");
				 resultVal = webServiceTemplate.sendSourceAndReceiveToResult(source,wasHeader(protocol),result);
				 logger.info("Response =======>"+writer.toString());
				//Auditing Request and Response Message into vgdb_epicmessaging_audit_values table
				 messageDao.saveMessageDetails("Outbound",body,writer.toString(),dataMap);
				 response =  writer.toString();
					
				}
				
				

			
			
		}catch(OperationCustomException e){
			e.printStackTrace();
			logger.info(" OutboundMessage********OpearationCustomException Block*******"+e.getLocalizedMessage()+"\n"+e.getResponseType());
			//messageDao.saveMessageDetails("Outbound",e.getLocalizedMessage(),e.getResponseType());
			dataMap.put(ProtocolKeys.FaultString,e.getResponseType());
			dataMap.put(ProtocolKeys.Body,e.getResponseType());
			//messageDao.saveMessageDetails("Outbound",body,e.getResponseType(),dataMap);
			if(!response.equalsIgnoreCase("false") && simulator.equalsIgnoreCase("No"))
			response =  sendAndReceive(dataMap,body,protocol,context);
			
			String result = "false";
			return result;
		}catch(Exception e){
			//System.out.println("EnrollPatientRequest Starts ***********************");
			e.printStackTrace();
			logger.info("Outbound Message********Exception Block*******"+e.getLocalizedMessage()+"\n"+e.getMessage());
			//messageDao.saveMessageDetails("Outbound",e.getLocalizedMessage(),e.getMessage());
			dataMap.put(ProtocolKeys.FaultString,e.getMessage());
			dataMap.put(ProtocolKeys.Body,e.getMessage());
			//messageDao.saveMessageDetails("Outbound",body,e.getMessage(),dataMap);
			response =  sendAndReceive(dataMap,body,protocol,context);
			String result = "false";
			return result;
		}finally{
			//((ClassPathXmlApplicationContext) context).close();
			//((ClassPathXmlApplicationContext) epicContext).close();
			if(context != null && epicContext != null){
			context.close();
			//epicContext.close();
			logger.info("closed classpath application context in epicendpointclient");
			}
			 
		}
		return response;
	}
	public static String getProperty(String key) {
		return bundle.getString(key);
	}
	@Bean
	public ActionCallback wasHeader(String protocol) throws Exception {
		ActionCallback header = null;
		try {					  
			URI action = new URI("urn:ihe:qrph:rpe:2009:"+protocol);
			Addressing10 version = new Addressing10();
			String scheme = getProperty("epic.scheme");
			String host = getProperty("epic.host");
			String port = getProperty("epic.port");
			String context = getProperty("epic.context");
			if (context != null && !context.startsWith("/")) {
				context = "/" + context;
			}
			URI to = null;
			int portInt = -1;
			try {
				portInt = Integer.parseInt(port);
			} catch(NumberFormatException e) {
				portInt = 443;
			}
			to = (new URL(scheme, host, portInt, context)).toURI();
			header = new ActionCallback(action, version, to);
		} catch(Exception e) {
			logger.info("Error Response in wasHeaderMethod -->"+ e.getMessage());
			e.printStackTrace();
			throw new OperationCustomException(e.getMessage());
		}
		return header;
	}
	
	
	
	@SuppressWarnings("finally")
	public   void sslJavaTrustStore(){

		try {
			/*prop = new Properties();
			prop.load(this.getClass().getClassLoader()
					.getResourceAsStream("epic.properties"));*/

			String sslEnable = getProperty("epic_SSL_Enable");

			if(getProperty("epic_SSL_Enable").equalsIgnoreCase("No")
					||getProperty("epic_SSL_Enable").equals("")){
				logger.info("############# EPIC URL SSL IS DISABLED ############");
			}else if( getProperty("epic_SSL_Enable").equalsIgnoreCase("yes")
					&& !getProperty("epic_SSL_Enable").equals("")){

				String keyStore = getProperty("epic_keyStore");
				System.setProperty("javax.net.ssl.keyStore",keyStore);

				String keyStorePassword = getProperty("epic_keyStorePassword");
				System.setProperty("javax.net.ssl.keyStorePassword",keyStorePassword);

				String keyStoreType = getProperty("epic_keyStoreType");
				System.setProperty("javax.net.ssl.keyStoreType",keyStoreType);

				String trustStore = getProperty("epic_trustStore");
				System.setProperty("javax.net.ssl.trustStore",trustStore);

				String trustStorePassword = getProperty("epic_trustStorePassword");
				System.setProperty("javax.net.ssl.trustStorePassword", trustStorePassword);

				String trustStoreType = getProperty("epic_trustStoreType");
				System.setProperty("javax.net.ssl.trustStoreType",trustStoreType);
			}
		} catch (Exception e) {
			logger.error("**epic.properties file load error on sslJavaTruststore method**==>"+e.getMessage());
			e.printStackTrace();
		}finally{
			
		}

	}
	
	
	public String  sendAndReceive(Map<VelosKeys, Object> dataMap,String body,String protocol,ClassPathXmlApplicationContext context) throws Exception{		
	
		writer = null;
		String response = null;
		try {
		WebServiceTemplate webServiceTemplate = (WebServiceTemplate)context.getBean("webServiceTemplate");
		StreamSource source = new StreamSource(new StringReader(body));
		StreamResult result = new StreamResult(writer);
		logger.info(" ************ Message send starts and Checking SSL Cert*************");
		if(dataMap != null) {
			
		}
		sslJavaTrustStore();
		boolean resultVal = webServiceTemplate.sendSourceAndReceiveToResult(source,wasHeader(protocol),result);
		messageDao.saveMessageDetails("Outbound",body,writer.toString(),dataMap);
		logger.info("\n\n************ Response Message  *************\n\n"+resultVal+"\n"+writer.toString());
		}catch(Exception e) {
			e.printStackTrace();
			logger.info("Exception Caught in ==========>"+e.getMessage());
			int outboundMsgCount =0;
			outboundMsgCount = Integer.parseInt(getProperty("outboundMsgCount"));
			dbCount = messageDao.getCount(dataMap,0);
			logger.info("DbCount ===>"+dbCount);
			if(dbCount < outboundMsgCount) {
				messageDao.saveMessageDetails("Outbound",body,(String)dataMap.get(ProtocolKeys.FaultString),dataMap);
				logger.info("Trying to message sending ==> "+dbCount + " times");
				dbCount = dbCount+1;
				messageDao.getCount(dataMap,dbCount);
				sendAndReceive(dataMap,body,protocol,context);
			}
			
			String result = "false";
			
			return result;

		}
		
		response = writer.toString();
		return response;
		
	
	}

	public String process(String body) throws Exception {

		String ipAddress = getProperty("simulatorIPAddress").trim();
		String port      = getProperty("simulatorPort").trim();
		String ipPort = ipAddress+":"+port;

		CamelContext camelContext = new DefaultCamelContext();
		try {
			camelContext.addRoutes(new RouteBuilder() {
				public void configure() {
					from("direct:start")
					.to("mina2:tcp://"+ipPort+"?sync=true");
				}
			});
			camelContext.start();
			ProducerTemplate template = camelContext.createProducerTemplate();
			Thread.sleep(5000);
			return (String) template.requestBody("direct:start", body);

			
		}catch(Exception e) {
			e.printStackTrace();
			logger.info("Process method===>"+e.getMessage());
			return "false";
		}finally {
			camelContext.stop();
			
		}
		
	}

}





