package com.velos.epic.beans;

public class OutboundMessageBean {
	
	private int pk_vgdb_rpe_outbound_message;
	private String messageType ;
	private String processFlag;  
	private String moduleName; 
	private String requestMessage; 
	private String StudyNumber; 
	private String PatientId; 
	private String StatusCode; 
	private String action; 
	private Object recordCreationDate;
	private Object msgTimeStamp;
	private String calendarName;
	private String OID;
	
	
	
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	public String getProcessFlag() {
		return processFlag;
	}
	public void setProcessFlag(String processFlag) {
		this.processFlag = processFlag;
	}
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public String getRequestMessage() {
		return requestMessage;
	}
	public void setRequestMessage(String requestMessage) {
		this.requestMessage = requestMessage;
	}
	public String getStudyNumber() {
		return StudyNumber;
	}
	public void setStudyNumber(String studyNumber) {
		StudyNumber = studyNumber;
	}
	public String getPatientId() {
		return PatientId;
	}
	public void setPatientId(String patientId) {
		PatientId = patientId;
	}
	public String getStatusCode() {
		return StatusCode;
	}
	public void setStatusCode(String statusCode) {
		StatusCode = statusCode;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public Object getRecordCreationDate() {
		return recordCreationDate;
	}
	public void setRecordCreationDate(Object recordCreationDate) {
		this.recordCreationDate = recordCreationDate;
	}
	public Object getMsgTimeStamp() {
		return msgTimeStamp;
	}
	public void setMsgTimeStamp(Object msgTimeStamp) {
		this.msgTimeStamp = msgTimeStamp;
	}
	public String getCalendarName() {
		return calendarName;
	}
	public void setCalendarName(String calendarName) {
		this.calendarName = calendarName;
	}
	public String getOID() {
		return OID;
	}
	public void setOID(String oID) {
		OID = oID;
	}
	public int getPk_vgdb_rpe_outbound_message() {
		return pk_vgdb_rpe_outbound_message;
	}
	public void setPk_vgdb_rpe_outbound_message(int pk_vgdb_rpe_outbound_message) {
		this.pk_vgdb_rpe_outbound_message = pk_vgdb_rpe_outbound_message;
	}
	


	
	

}
