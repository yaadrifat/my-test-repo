package com.velos.integration.core.processor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.velos.integration.mapping.ProtocolKeys;
import com.velos.integration.mapping.VelosKeys;

public class StudyCalendarProcessor {
	
	List<String> calNameList = new ArrayList<String>();
	public List<String> getStudyCalendarsList(Map<VelosKeys, Object> resultMap){
		
		List< Map<VelosKeys, Object> > dataList = (List<Map<VelosKeys, Object>>) resultMap.get(ProtocolKeys.CalendarIdList);
	      
		for(Map<VelosKeys, Object> calMap : dataList){
			
			calNameList.add(calMap.get(ProtocolKeys.CalendarName).toString());
		}
		
		
		
		//studyCalendartoEpic(calNameList);
		
		
		return calNameList;
	}
	
	public void studyCalendartoEpic(Map<VelosKeys, Object> resultMap){
		
		
		
	}

}
