package com.velos.integration.core.messaging;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Properties;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;
import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.velos.epic.XmlParser;
import com.velos.epic.beans.OutboundMessageBean;
import com.velos.epic.service.EpicEndpointClient;
import com.velos.epic.service.StudyProcess;
import com.velos.integration.core.processor.StudyCalendarProcessor;
import com.velos.integration.dao.MessageDAO;
import com.velos.integration.espclient.VelosEspClient;
import com.velos.integration.espclient.VelosEspMethods;
import com.velos.integration.hl7.bean.MessageBean;
import com.velos.integration.hl7.notifications.OperationCustomException;
import com.velos.integration.mapping.EndpointKeys;
import com.velos.integration.mapping.ProtocolKeys;
import com.velos.integration.mapping.VelosKeys;
import com.velos.services.Change;
import com.velos.services.CrudAction;
import com.velos.services.StudyStatus;

public class OutboundScheduler {
	private static Logger logger = Logger.getLogger("epicLogger");
	private MessageDAO messageDao = null;
	Properties prop= null,prop1= null;
	ClassPathXmlApplicationContext epicContext = null;
	ClassLoader classLoader = null;
	PropertiesConfiguration outBoundProp = null;
	public OutboundScheduler() throws IOException{
		prop = new Properties();
		//prop1 = new Properties();
		prop.load(this.getClass().getClassLoader().getResourceAsStream("epic.properties"));
		//prop1.load(this.getClass().getClassLoader().getResourceAsStream("outbound.properties"));
		epicContext = new ClassPathXmlApplicationContext("epic-context.xml");
		messageDao = (MessageDAO) epicContext.getBean("messageDao");
	}

	public void run() throws Exception {
		Thread.sleep(6000);
		logger.info("**************** Schedular Start **************");

		// from datetime stamp in outbound.properties
		String fromDate = getPropertyValue("outbound.properties","last_outbound_from_time");
		String toDate = getDate();
		// calling client name from properties file
		String calledFrom = prop.getProperty("calledfrom").trim();
		String moduleName = "";
		String studyNumber = null;
		String patientId = null;
		String statuSubTyp = null;
		//String timeStamp = null;
		String action = null;
		String statusCodedesc = null;
		String module = null;
		String moduleKey = null;
		String moduleOrder [] = null;
		int  pk_rpe_outbound_message = 0 ;
		String calendarName = null;
		String oid = null;
		//Date dbRectimeStamp = null;
		//Date msgTimeStamp = null;
		String resultFlag = null;
		String request_message = null;
		String messageType = prop.getProperty("outbound_msgtype").trim();
		Map<VelosKeys, String> requestMap = null ;
		int orderCount = 1;
		List<OutboundMessageBean> outbondMsgList = null;
		Map<VelosKeys, Object> resultMap = null;
		List<String> moduleList =null;
		EpicEndpointClient  eeClient = null ;
		//try{
		eeClient = new EpicEndpointClient();
		moduleList = new ArrayList<String>();
		resultMap = new HashMap<VelosKeys, Object>();
		VelosEspClient espClient = null ;
		espClient = new VelosEspClient();
		XmlParser xmlParser = null;
		String endpoint = EndpointKeys.Epic.toString();
		try{
			//Getting Outbound Module Names from property File
			for(Entry<Object,Object> e : prop.entrySet()){
				if("select".equalsIgnoreCase(e.getValue().toString()) &&  
						e.getKey().toString().contains("outbound_moduleName.")){
					moduleList.add(e.getKey().toString());
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Outboundschedular getModuleNames ==>"+e.getMessage());
		}
		//Looping Module by Module untill list zero
		System.out.println("OutBound Module Size ======>"+moduleList.size());
		if(moduleList.size()>0)
			for(int i=0; i<=moduleList.size()-1;){
				moduleKey =moduleList.get(i);
				moduleOrder =	moduleKey.substring(20).split("~");

				//Cecking Module Name Order and process by order 1.2.3......
				if( Integer.valueOf(moduleOrder[0]) == orderCount){
					String faultString = null;
					try{
						moduleName = moduleOrder[1];
						logger.info("ModuleName & Module Order ========>"+ moduleName +"_"+moduleOrder[0]);

						requestMap = null ;
						requestMap = new HashMap<VelosKeys, String>();
						requestMap.put(EndpointKeys.Endpoint, endpoint);
						requestMap.put(ProtocolKeys.FromDate,fromDate);
						requestMap.put(ProtocolKeys.ToDate,toDate);
						requestMap.put(ProtocolKeys.ModuleName,moduleName);
						requestMap.put(ProtocolKeys.CalledFrom,calledFrom);

						//Getting Outbound Changes Message Deatils
						resultMap = new HashMap<VelosKeys, Object>();
						
						resultMap = espClient.handleOutboundMessageRequest(VelosEspMethods.OutboundMessages, requestMap);
						System.out.println("REsultMap=====>"+resultMap);
						//List<Change> changeList = null;
						//changeList = (List<Change>) resultMap.get(ProtocolKeys.ChangesList);

						String changesMsg = null;
						List<String> changesMsgList = null;
						
						if(resultMap == null || resultMap.isEmpty()) {
							request_message = "Request Message Not Generated because of connection issue / errors while getting changes message";
							faultString ="Please check ESP service up / Down ...!";
							throw new Exception();
						}else if(resultMap != null ) {
						changesMsg = (String)resultMap.get(ProtocolKeys.ChangesMessage);
						}

						if(changesMsg != null){
							changesMsgList = 	parseRequestChangesMessage(changesMsg);
							resultMap.put(ProtocolKeys.ChangesMsgList, changesMsgList);
							messageDao.insertOutboundMessageDetails(messageType, resultMap);
						}else{
							logger.info("***** No Changes Messages ******");
						}

						//Getting Outbound MessageBean List from Database
						outbondMsgList = messageDao.getOutboundMessageBean(moduleName);
					}catch(Exception e){
						e.printStackTrace();
						resultMap = new HashMap<VelosKeys, Object>();
						resultMap.put(ProtocolKeys.ModuleName,moduleName);
						messageDao.saveMessageDetails("OutBound",request_message, faultString,resultMap);
						resultMap=null;
						System.gc();
						//	System.out.println("outbondMsgList size  ==================>"+outbondMsgList.size());
					}


					if(outbondMsgList != null){
						OUTBOUNDLABEL:	for(OutboundMessageBean omb : outbondMsgList){
							try{
								//System.out.println("MessageBean =====>"+omb);


								studyNumber 	=	  omb.getStudyNumber();
								statuSubTyp 	=     omb.getStatusCode();
								//dbRectimeStamp  =	  (Date)omb.getRecordCreationDate();
								patientId       =  	  omb.getPatientId();
								module 			=     omb.getModuleName();
								action          =     omb.getAction();
								//msgTimeStamp    =     (Date)omb.getMsgTimeStamp();
								request_message =     omb.getRequestMessage();
								calendarName   =      omb.getCalendarName();
								oid				=	   omb.getOID();
								pk_rpe_outbound_message =omb.getPk_vgdb_rpe_outbound_message();
								logger.info("Module Name ##################>"+module);
								if(module!=null)
									switch(ProtocolKeys.valueOf(module)){
									case  study_summary:
										logger.info(" **************Study Summary Module Starts *******************");
										resultMap.clear();
										resultMap.put(ProtocolKeys.StudyId, studyNumber);
										resultMap.put(ProtocolKeys.StudyNumber, studyNumber);
										resultMap.put(ProtocolKeys.OutboundOID, oid);
										//resultMap.put(ProtocolKeys.StatusCodeDesc, statusCodedesc);
										resultMap.put(ProtocolKeys.StudyAction, action);
										resultMap.put(ProtocolKeys.ModuleName,module);
										resultMap.put(EndpointKeys.Endpoint, endpoint);
										resultMap.put(ProtocolKeys.PkId,pk_rpe_outbound_message);
										xmlParser = new XmlParser();
										resultFlag = xmlParser.StudyXMLParser(studyNumber,resultMap,prop);
										//	eeClient.handleRequest(ProtocolKeys.RetrieveProtocol, resultMap);
										// resultFlag = false because message not reached to client. 
										if(!resultFlag.equals("false")){
											messageDao.updateOutboundMessageInfo(omb);
										}
										logger.info(" **************Study Summary Module END *******************");
										break;	
									case calendar_status :
										logger.info(" ************** Calendar_Status Module Starts ****************");
										resultMap.clear();
										resultMap.put(ProtocolKeys.StudyNumber, studyNumber);
										resultMap.put(ProtocolKeys.StudyId, studyNumber);
										resultMap.put(ProtocolKeys.StudyCalenStatus,statuSubTyp);
										resultMap.put(ProtocolKeys.OutboundOID, oid);
										resultMap.put(ProtocolKeys.ModuleName,module);
										//resultMap.put(ProtocolKeys.StatusCodeDesc, statusCodedesc);
										resultMap.put(ProtocolKeys.CalendarName,calendarName);
										resultMap.put(ProtocolKeys.PkId,pk_rpe_outbound_message);
										resultMap.put(EndpointKeys.Endpoint, EndpointKeys.Epic.toString());
										 StudyStatus studyStatus = null;
										 String result = null;
										 requestMap.put(ProtocolKeys.StudyNumber, studyNumber);
										 requestMap.put(ProtocolKeys.StudyId, studyNumber);
										 requestMap.put(EndpointKeys.Endpoint, EndpointKeys.Epic.toString());
										if(studyNumber != null) {
											Map<VelosKeys, Object> studyStatusResultMap = espClient.handleRequest(VelosEspMethods.StudyGetStudyStat, requestMap);
												
											List<StudyStatus> studyStatusList  = (List<StudyStatus>) studyStatusResultMap.get(ProtocolKeys.StudyStatusList);
											logger.info("Study Status List ===>"+studyStatusList);
										 studyStatus=	studyStatusList.stream().filter(p->p.getStatus().getCode().equalsIgnoreCase(prop.getProperty("study.statusSubType"))).findAny().orElseThrow(()->new IllegalArgumentException("Status code not found in study"));
										logger.info("Study Status List ===>"+studyStatus);
											
										}
										logger.info("Study Status List ===>"+studyStatus);
										 if(studyStatus != null) {
										 result = eeClient.handleRequest(ProtocolKeys.RetrieveProtocolStudyCalendar, resultMap);
										 }if(studyStatus == null) {
											 logger.info("Study Status List Does Not Contains ==>"+prop.getProperty("study.statusSubType")+" Status");
										 }
										if(!result.equals("false")){
											messageDao.updateOutboundMessageInfo(omb);
											//throw new OperationCustomException();
										}
										logger.info(" ************** Calendar_Status Module END ****************");
										break;
									case patstudy_status :
										logger.info(" ************ PatStudyStatus Module Starts *********************");
										resultMap.clear();
										resultMap.put(ProtocolKeys.StudyId, studyNumber);
										resultMap.put(ProtocolKeys.StudyNumber, studyNumber);
										resultMap.put(ProtocolKeys.PatientId,patientId);
										resultMap.put(ProtocolKeys.ModuleName,module);
										resultMap.put(ProtocolKeys.PkId,pk_rpe_outbound_message);
										StudyProcess studyProcess = new StudyProcess();
										String studyID = (String)resultMap.get(ProtocolKeys.StudyId);
										patientId =(String)resultMap.get(ProtocolKeys.PatientId);
										resultMap.put(ProtocolKeys.OutboundOID, oid);
										// Based on patient study status  passing control flow to Enrollpatient or AlertProtocol
										if(statuSubTyp.equalsIgnoreCase(prop.getProperty("patientStudyStatusCode"))){
											logger.info("*********Enroll Patient Request Starts ****************");
											resultFlag =	studyProcess.getEnrollPatientStudyDetails(studyID,patientId,resultMap);
											logger.info("*********Enroll Patient Request END ****************");
											if(!resultFlag.equals("false")){
												messageDao.updateOutboundMessageInfo(omb);
											}
										}if(!statuSubTyp.equalsIgnoreCase(prop.getProperty("patientStudyStatusCode"))){
											logger.info("****** Alert Protocol State Request starts *****");
											resultFlag = studyProcess.getAlertProtocolState(studyNumber,patientId,resultMap);
											if(!resultFlag.equals("false")){
												messageDao.updateOutboundMessageInfo(omb);
											}
											logger.info("****** Alert Protocol State Request starts *****");
										}
										studyProcess = null;
										break;
									case study_status:
										logger.info(" **************Study Status Module Starts *******************");
										resultMap.clear();
										resultMap.put(ProtocolKeys.StudyId, studyNumber);
										resultMap.put(ProtocolKeys.StudyNumber, studyNumber);
										resultMap.put(ProtocolKeys.study_status, statuSubTyp);
										resultMap.put(ProtocolKeys.StudyAction, action);
										resultMap.put(ProtocolKeys.OutboundOID, oid);
										resultMap.put(ProtocolKeys.ModuleName,module);
										resultMap.put(ProtocolKeys.PkId,pk_rpe_outbound_message);
										resultMap.put(EndpointKeys.Endpoint, endpoint);
										xmlParser = new XmlParser();
										resultFlag = xmlParser.StudyXMLParser(studyNumber,resultMap,prop);
										//	eeClient.handleRequest(ProtocolKeys.RetrieveProtocol, resultMap);
										if(!resultFlag.equals("false")){
											messageDao.updateOutboundMessageInfo(omb);
										}
										logger.info(" **************Study Status Module END *******************");
										break;
									default:

									}

							}catch(OperationCustomException e){
								e.printStackTrace();
								 faultString = e.getMessage();
								if(e.getResponseType() != null){
									request_message = e.getResponseType();
								}
								if(request_message == null || "".equals(request_message)) {
									request_message = "Request Message Not Generated because of errors while generating  message";
								}
								if(!resultFlag.equals(false)) {
								messageDao.saveMessageDetails("OutBound",request_message, faultString,resultMap);
								}
								if(outbondMsgList != null){
									logger.info("size list ==================>"+outbondMsgList.size());
									outbondMsgList.remove(omb);
									logger.info("After size list ==================>"+outbondMsgList.size());
									continue OUTBOUNDLABEL;
								}
							}catch(Exception ee){
								ee.printStackTrace();
								 faultString = ee.getMessage();
								if(request_message == null || "".equals(request_message)) {
									request_message = "Request Message Not Generated because of errors while generating";
								}
								
								messageDao.saveMessageDetails("OutBound",request_message, faultString,resultMap);
								if(outbondMsgList != null){
									outbondMsgList.remove(omb);
									logger.info("After size list ==================>"+outbondMsgList.size());
									continue OUTBOUNDLABEL;
								}
							}
							/*
							 * if(outbondMsgList != null){ outbondMsgList.remove(omb);
							 * logger.info("After size list ==================>"+outbondMsgList.size());
							 * continue OUTBOUNDLABEL; }
							 */
						}
					orderCount = orderCount+1;
					moduleList.remove(moduleList.get(i));
					i = 0;
					}
					//Catch Block
					//Thread.sleep(10000);
				}else{
					//increment the loop if it order not matches
					i=i+1;
				}
			}
		updateOuboundProperties(toDate);
		System.gc();	
	}

	public static void main(String[] args) throws Exception{
		/*OutboundScheduler outb= new OutboundScheduler();
		outb.updateOuboundProperties();*/
	}

	public void updateOuboundProperties(String schStartDate) throws Exception {
		FileOutputStream out = null;
		//Date todate =null;
		//SimpleDateFormat simpleDateForm = null ;
		//String outBoundDate = null ;

		try {
			logger.info(" *********Updating Outbound Properties..............");

			classLoader = getClass().getClassLoader();
			out = new FileOutputStream(classLoader.getResource("outbound.properties").getFile());
			outBoundProp.setProperty("last_outbound_from_time", schStartDate);
			logger.info("Last_Outbound_From_Date==>"+schStartDate);
			outBoundProp.save(out);

		} catch (Exception e) {
			logger.info("Error in update OutboundProperties ==>"+e.getMessage());
			e.printStackTrace();
			throw e;
		}finally{
			if(out != null){
				out.close();
			}
		}
	}
	public String getPropertyValue(String propertyFileName, String keyName) throws ConfigurationException{
		String keyList = null;
		try {
			classLoader = getClass().getClassLoader();
			File file = new File(classLoader.getResource(propertyFileName).getFile());
			outBoundProp = new PropertiesConfiguration(file);
			outBoundProp.setReloadingStrategy(new FileChangedReloadingStrategy());
			keyList = (String)outBoundProp.getProperty(keyName);
		}catch(Exception e){
			e.printStackTrace();
			logger.info("Get Property Value ==>"+e.getMessage());
		}
		return keyList;

	}

	public List<String> parseRequestChangesMessage(String changesMessage){

		logger.info("********* Parse Request Changes Message ******");
		List<String> changesMsgList = null;
		Pattern pattrn = null ;
		Matcher matcher =null ; 

		try{
			pattrn = Pattern.compile("<changes>(.+?)</changes>",Pattern.DOTALL);
			matcher = pattrn.matcher(changesMessage);
			changesMsgList = new ArrayList<String>();

			while(matcher.find()){
				changesMsgList.add(matcher.group());
			}
		}catch(Exception e){
			//	throw e;
		}
		return changesMsgList;
	}

	public String getDate() throws Exception{

		try {
			SimpleDateFormat simpleDateForm = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			Date todate = new Date();
			String outBoundDate = simpleDateForm.format(todate);
			return outBoundDate;
		} catch (Exception e) {
			logger.info("Error in updateOutboundProperties  getDate() ==>"+e.getMessage());
			throw e;
		}
	}

}

/*String	changesMsg = "<changes>"+
    "<change>"+
       "<action>CREATE</action>"+
        "<statusSubType>A</statusSubType>"+
        "<statusCodeDesc>Active</statusCodeDesc>"+
            "<OID>c1972b23-6c45-4832-91fa-7a1d40c7f4db</OID>"+
        "</identifier>"+
        "<module>calendar_status</module>"+
        "<timeStamp>11/15/2018 13:16:23</timeStamp>"+
    "</change>"+
    "<parentIdentifier>"+
            "<OID>2658d01a-3ee5-4847-9016-a1f255ad0e58</OID>"+
            "<studyIdentifier>"+
                "<studyNumber>1111</studyNumber>"+
            "</studyIdentifier>"+
            "<calendarName>RPECal</calendarName>"+
        "</id>"+
    "</parentIdentifier>"+
"</changes>";
 */
/*			}

	if(changeList != null){
		for(Change change : changeList){

			studyNumber 	=	   change.getIdentifier().getStudyNumber();
			statuSubTyp 	=      change.getStatusSubType();
			timeStamp  		=	   change.getTimeStamp();
			statusCodedesc  =  	   change.getStatusCodeDesc();
			module 			=      change.getModule();

			CrudAction 	  crudAction 		=	   change.getAction();

			action = crudAction.value();
			System.out.println(studyNumber+ statuSubTyp);*/


/*		
for(Entry<Object,Object> e : prop.entrySet()){
System.out.println("Module Name =====>"+e.getKey());
if("select".equalsIgnoreCase(e.getValue().toString()) &&  
		e.getKey().toString().contains("outbound_moduleName.")){

	logger.info("OutBound module Name ###=========>"+e.getKey().toString());*/

//System.out.println("ModuleList=====>"+moduleList);
