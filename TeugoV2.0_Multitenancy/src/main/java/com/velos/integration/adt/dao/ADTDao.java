package com.velos.integration.adt.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.jboss.util.threadpool.BasicThreadPoolMBean;

import com.velos.integration.hl7.dao.MessageDao;
import com.velos.integration.hl7.notifications.HL7CustomException;

public class ADTDao extends MessageDao {

	private static Logger logger = Logger.getLogger(ADTDao.class);

	public ADTDao(MessageDao messageDao) {
		super(messageDao);
	}

	public Map<String, Object> getRecord(String sql) throws Exception {
		Map<String, Object> recordFields = null;
		try {
			
			 System.out.println("**********ADTDAO**************");
			List<Map<String, Object>> record = getJdbcTemplate()
					.queryForList(sql);

			if (!record.isEmpty())
				recordFields = record.get(0);

		} catch (Exception e) {
			logger.error("Error While getting record in getRecord inside ADTDao"
					+ e.getMessage());
			throw new HL7CustomException(e.getMessage(),"AE");
		}
		return recordFields;
	}

	public void updateRecord(String sql) throws Exception {
		try {
			
			 System.out.println("**********UPdate ADTDAO**************");
			getJdbcTemplate().update(sql);
		} catch (Exception e) {
			logger.error("Error While updating record in updateRecord inside ADTDao"
					+ e.getMessage());
			throw new HL7CustomException(e.getMessage(),"AE");
		}
	}

}
