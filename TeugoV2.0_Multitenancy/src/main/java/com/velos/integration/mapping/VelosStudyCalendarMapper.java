package com.velos.integration.mapping;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import com.velos.services.CalendarEvent;
import com.velos.services.CalendarEvents;
import com.velos.services.CalendarVisit;
import com.velos.services.CalendarVisits;
import com.velos.services.Duration;
import com.velos.services.GetStudyCalendarListResponse;
import com.velos.services.GetStudyCalendarResponse;
import com.velos.services.StudyCalendars;
import com.velos.services.StudyCalendarsList;

public class VelosStudyCalendarMapper {
	Logger logger = Logger.getLogger("epicLogger");
	private String endpoint = "epic";
	private Map<VelosKeys, Object> dataMap = new HashMap<VelosKeys, Object>();
	
	public VelosStudyCalendarMapper(String endpoint) {
		this.endpoint = endpoint;
	}
	
	
	
	public Map<VelosKeys, Object> mapStudyCalendarList(GetStudyCalendarListResponse resp) {
		logger.info("*****************StudyCalendarMapper ***************");
		
		if (EndpointKeys.Epic.toString().equals(endpoint)) {
			mapStudyCalendarListForEpic(resp);
		}
		return dataMap;
	}
	
	private void mapStudyCalendarListForEpic(GetStudyCalendarListResponse resp) {
		logger.info("*****************StudyCalendarMapperList ***************");
		List< Map<VelosKeys, Object> > dataList = new ArrayList< Map<VelosKeys, Object> >();
		StudyCalendarsList list = resp.getStudyCalendarList();
		for (StudyCalendars calendar : list.getStudyCalendars()) {
			if (calendar.getCalendarStatus() == null) { continue; }
			if (!ProtocolKeys.LetterA.toString().equals(calendar.getCalendarStatus().getCode())) {
				continue;
			}
			Map<VelosKeys, Object> map = new HashMap<VelosKeys, Object>();
			map.put(ProtocolKeys.CalendarName, MapUtil.escapeSpecial(calendar.getCalendarName()));
			map.put(ProtocolKeys.CalendarId, calendar.getCalendarIdentifier().getPK());
			dataList.add(map);
		}
		dataMap.put(ProtocolKeys.CalendarIdList, dataList);
	}

	public Map<VelosKeys, Object> mapStudyCalendar(GetStudyCalendarResponse resp) {
		if (EndpointKeys.Epic.toString().equals(endpoint)) {
			mapStudyCalendarForEpic(resp);
		}
		return dataMap;
	}
	
	private void mapStudyCalendarForEpic(GetStudyCalendarResponse resp) {
		logger.info("*****************StudyCalendarMapper for Epic***************");
		List< Map<VelosKeys, Object> > dataVisitList = new ArrayList< Map<VelosKeys, Object> >();
		Duration calDuration = resp.getStudyCalendar().getCalendarSummary().getCalendarDuration();
		dataMap.put(ProtocolKeys.CalendarDuration, calDuration.getValue());
		dataMap.put(ProtocolKeys.CalendarDurUnit, calDuration.getUnit().toString());
		System.out.println("Cal duration is "+calDuration.getValue()+" "+calDuration.getUnit().toString());
		CalendarVisits list = resp.getStudyCalendar().getVisits();
		for (CalendarVisit visit : list.getVisit()) {
			CalendarEvents eventList = visit.getEvents();
			List< Map<VelosKeys, Object> > dataEventList = new ArrayList< Map<VelosKeys, Object> >();
			for (CalendarEvent event : eventList.getEvent()) {
				Map<VelosKeys, Object> map = new HashMap<VelosKeys, Object>();
				map.put(ProtocolKeys.EventName, MapUtil.escapeSpecial(event.getCalendarEventSummary().getEventName()));
				map.put(ProtocolKeys.EventCpt, event.getCalendarEventSummary().getCPTCode());
				map.put(ProtocolKeys.EventSequence, event.getCalendarEventSummary().getSequence());
				if (event.getCalendarEventSummary().getCoverageType() != null) {
					map.put(ProtocolKeys.CoverageType, 
							event.getCalendarEventSummary().getCoverageType().getCode());
				}
				dataEventList.add(map);
			}
			Map<VelosKeys, Object> map = new HashMap<VelosKeys, Object>();
			map.put(ProtocolKeys.VisitName, MapUtil.escapeSpecial(visit.getCalendarVisitSummary().getVisitName()));
			map.put(ProtocolKeys.EventList, dataEventList);
			map.put(ProtocolKeys.VisitWindowBefore, visit.getCalendarVisitSummary().getVisitWindowBefore().getValue());
			map.put(ProtocolKeys.VisitWindowAfter, visit.getCalendarVisitSummary().getVisitWindowAfter().getValue());
			map.put(ProtocolKeys.VisitDisplacement, 
					visit.getCalendarVisitSummary().getAbsoluteInterval() == null ? 
							0 : visit.getCalendarVisitSummary().getAbsoluteInterval().getValue());
			dataVisitList.add(map);
		}
		System.out.println("DataVisit List ================>"+dataVisitList);
		dataMap.put(ProtocolKeys.VisitList, dataVisitList);
	}

}
