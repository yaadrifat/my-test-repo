package com.velos.integration.mapping;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.velos.services.Change;
import com.velos.services.Changes;
import com.velos.services.ChangesList;
import com.velos.services.GetOutboundMessages;
import com.velos.services.GetOutboundMessagesResponse;
import com.velos.services.OutboundIdentifier;
import com.velos.services.OutboundPatientIdentifier;
import com.velos.services.OutboundPatientProtocolIdentifier;
import com.velos.services.OutboundStudyIdentifier;

public class VelosOutboundMessageMapper {
	
	Logger logger = Logger.getLogger("epicLogger");
	
	private String endPoint = null;
	private Map<VelosKeys, Object> dataMap = new HashMap<VelosKeys, Object>();
	
	public VelosOutboundMessageMapper(String endPoint){

		this.endPoint = endPoint;
	}

	public Map<VelosKeys, Object> mapOutboundMessageService(GetOutboundMessagesResponse getOutboundMessageResponse){
		try{
			List<Change> changeList =  null;
			 changeList = new ArrayList<Change>();

			ChangesList changesList = null;
			changesList = getOutboundMessageResponse.getChangesList();
			List<Changes> listChange = changesList.getChanges();
			int i = listChange.size();
			logger.info("Changes size of list ====>"+i);
			if(i>0){

				for(Changes changs : listChange){
					List<Change> change = 	changs.getChange();


					OutboundPatientProtocolIdentifier parentIdentifier = changs.getParentIdentifier();
					OutboundIdentifier outboundId = parentIdentifier.getId();
					if(outboundId.getStudyNumber() !=null){
						String studyNumber = outboundId.getStudyNumber();
						logger.info("StudyNumber ======>"+studyNumber);
						dataMap.put(ProtocolKeys.StudyId,studyNumber);
					}

					if(outboundId.getCalendarName() != null){
						String calendarName = outboundId.getCalendarName();
						logger.info("Calendar Name =====>"+calendarName);
						dataMap.put(ProtocolKeys.CalendarName, calendarName);
					}

					if(outboundId.getPatientIdentifier()!= null){
						OutboundPatientIdentifier patientIdentifier = outboundId.getPatientIdentifier();
						String patientId = patientIdentifier.getPatientId();
						dataMap.put(ProtocolKeys.PatientId,patientId);
					}

					if(outboundId.getStudyIdentifier() != null){
						OutboundStudyIdentifier studyIdentifier = outboundId.getStudyIdentifier();
						String studyNumber = studyIdentifier.getStudyNumber();
						logger.info("StudyNumber ========>"+studyNumber);
						dataMap.put(ProtocolKeys.StudyId,studyNumber);
						dataMap.put(ProtocolKeys.StudyNumber,studyNumber);
					}
					//System.out.println("Change Size =====>"+change.size()+""+change);

					for(Change chang : change){
						changeList.add(chang);

					}


				}
			}else{
				logger.info("list length"+listChange.size());
			}

			//System.out.println("ChangesList ====>"+changeList);
			dataMap.put(ProtocolKeys.ChangesList, changeList);
		}catch(Exception e){
			logger.info("Outbound Message Mapper ===>"+e.getMessage());
			//throw e;
		}

		return dataMap;

	}



}
