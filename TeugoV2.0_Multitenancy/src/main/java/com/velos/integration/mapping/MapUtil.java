package com.velos.integration.mapping;

public class MapUtil {
	
	private static final String EMPTY_STR = "";
	
	public static String escapeSpecial(String str) {
		if (str == null) { return null; }
		return str.replaceAll("&", "&#038;").replaceAll("\"", "'");
	}
	
	public static String formatDateOfBirth(String dob) {
		if (dob == null) { return null; }
		String formattedDob = null;
		if (dob.length() == 8) { // Input is yyyyMMdd
			try {
				Integer.parseInt(dob);
				formattedDob = dob.substring(0, 4)+"-"+dob.substring(4,6)+"-"+dob.substring(6,8);
			} catch (Exception e) {
				formattedDob = null;
			}
		}
		return formattedDob;
	}
	
	public static String nullToEmpty(Object object) {
		return (String) (object == null ? EMPTY_STR : object);
	}

}
