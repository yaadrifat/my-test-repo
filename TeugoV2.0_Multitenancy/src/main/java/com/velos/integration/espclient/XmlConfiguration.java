package com.velos.integration.espclient;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;



@Configuration
public class XmlConfiguration {
	private static Logger logger = Logger.getLogger(XmlConfiguration.class);
	@Bean
    public Jaxb2Marshaller marshaller() {
		
	Jaxb2Marshaller m = null ;
		
	if(m == null ){
		logger.info("Creting New Marshallar object");
		m = new Jaxb2Marshaller();
	}
	//ClassLoader cl = com.velos.services.ObjectFactory.class.getClassLoader();
		String[] packagesToScan = {"com.velos.services"};
		m.setPackagesToScan(packagesToScan);
        return m;
    }

}
