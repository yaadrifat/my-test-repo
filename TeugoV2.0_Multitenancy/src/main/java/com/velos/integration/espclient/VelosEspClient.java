package com.velos.integration.espclient;

import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.integration.Message;
import org.springframework.integration.MessageChannel;
import org.springframework.integration.MessageHandlingException;
import org.springframework.integration.core.MessagingTemplate;
import org.springframework.integration.core.PollableChannel;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.integration.xml.source.DomSourceFactory;
import org.springframework.stereotype.Component;
import org.springframework.ws.soap.SoapFaultDetail;
import org.springframework.ws.soap.SoapFaultDetailElement;
import org.springframework.ws.soap.client.SoapFaultClientException;
import org.springframework.ws.soap.security.wss4j.Wss4jSecurityInterceptor;
import org.w3c.dom.NodeList;

import com.velos.integration.filemanager.PropertyFileManager;
import com.velos.integration.gateway.ProtocolEndpoint;
import com.velos.integration.hl7.notifications.OperationCustomException;
import com.velos.integration.mapping.EndpointKeys;
import com.velos.integration.mapping.MapUtil;
import com.velos.integration.mapping.ProtocolKeys;
import com.velos.integration.mapping.VelosKeys;


@Component
public class VelosEspClient {
	
	private static Logger logger = Logger.getLogger("epicLogger");
	public static final String HARDCODED_ORG = "Velos";
	//public static final String HARDCODED_ORG = "C C Cancer Center";
	public static final String HARDCODED_PATSTUDYSTAT = "ACTIVITY_EXITED";
	Properties prop=null;
	Properties espServiceprop=null;
	PropertiesConfiguration configuration = null;
	Map<VelosKeys, Object> dataMap= null;
	public VelosEspClient(){
		try {
			prop = new Properties();
			espServiceprop = new Properties();
			prop.load(this.getClass().getClassLoader()
					.getResourceAsStream("config.properties"));
			prop.load(this.getClass().getClassLoader()
					.getResourceAsStream("epic.properties"));
			espServiceprop.load(this.getClass().getClassLoader()
					.getResourceAsStream("esp_services.properties"));

		} catch (IOException e) {

		}
	}

	private static final String FAULT_STR = "Fault";
	
	private static final String groupUserService="<ser:searchUser xmlns:ser=\"http://velos.com/services/\">"
			+ "<UserSearch>"
            + "<group>"
            + "<groupName>%s</groupName>"
            + "</group>"
            + "<organization>"
            + "<siteName>%s</siteName>"
            + "</organization>"
            + "<pageSize>%s</pageSize>"
            + "</UserSearch>"
            + "</ser:searchUser>";

	private static final String getStudystatusesTemplate=" <ser:getStudyStatuses xmlns:ser=\"http://velos.com/services/\">"
			+ "<StudyIdentifier>"
			+ "<studyNumber>%s</studyNumber>"
			+ "</StudyIdentifier>"
			+ "</ser:getStudyStatuses>";

	private static final String createPatientRequestTemplate = "<ser:createPatient xmlns:ser=\"http://velos.com/services/\">"+
			"<Patient>"+
			"<patientDemographics>"+
			"<dateOfBirth>%s</dateOfBirth>"+
			"<firstName>%s</firstName><lastName>%s</lastName><middleName>%s</middleName>"+
			"<address1>%s</address1>"+
			"<city>%s</city>"+
			"<state>%s</state>"+
			"<zipCode>%s</zipCode>"+
			"<country>%s</country>"+
			"<organization><siteName>%s</siteName></organization>"+
			"<patFacilityId>%s</patFacilityId>"+
			"<patientCode>%s</patientCode>"+//not required
			"<survivalStatus><code>A</code></survivalStatus>"+
			"</patientDemographics>"+
			"</Patient>"+
			"</ser:createPatient>";

	private static final String getPatientDetailsTemplate = "<ser:getPatientDetails xmlns:ser=\"http://velos.com/services/\">"+
			"<PatientIdentifier>"+
			"<OID>%s</OID>"+
			"<organizationId>"
			+"<siteName>%s</siteName>"
			+"</organizationId>"+
			"<patientId>%s</patientId>"+
			"</PatientIdentifier>"+
			"</ser:getPatientDetails>";

	private static final String studyPatStatusHistoryTemplate =" <ser:getStudyPatientStatusHistory xmlns:ser=\"http://velos.com/services/\">"+
			"<StudyIdentifier><studyNumber>%s</studyNumber></StudyIdentifier>"+	
			"<PatientIdentifier><OID>%s</OID></PatientIdentifier>"+
			"</ser:getStudyPatientStatusHistory>";


	private static final String studyRequestTemplate =
			"<ser:getStudySummary xmlns:ser=\"http://velos.com/services/\">" +
					"<StudyIdentifier>" +
					"   <studyNumber>%s</studyNumber>" +
					"</StudyIdentifier>" +
					"</ser:getStudySummary>";
	
	private static final String studyCalendarListRequestTemplate =
			"<ser:getStudyCalendarList xmlns:ser=\"http://velos.com/services/\">" +
					"<StudyIdentifier>" +
					"<studyNumber>%s</studyNumber>" +
					"</StudyIdentifier>" +
					"</ser:getStudyCalendarList>";

	private static final String studyCalendarRequestTemplate =
			"<ser:getStudyCalendar xmlns:ser=\"http://velos.com/services/\">" +
					"<CalendarIdentifier><PK>%s</PK></CalendarIdentifier>" +
					"</ser:getStudyCalendar>";
	
	private static final String studyCalendarNameRequestTemplate =
			"<ser:getStudyCalendar xmlns:ser=\"http://velos.com/services/\">" +
					"<StudyIdentifier>"+
					" <studyNumber>%s</studyNumber>"+
					"</StudyIdentifier>"+
					"<CalendarName>%s</CalendarName>"+
					"</ser:getStudyCalendar>";

	private static final String studyPatientRequestTemplate =
			"<ser:getStudyPatients xmlns:ser=\"http://velos.com/services/\">"+
					 "<StudyPatientSearch>"+
					"<pageNumber>1</pageNumber>"+
			        "<pageSize>10</pageSize>"+
					"<studyIdentifier>"+
					"<studyNumber>%s</studyNumber>"+
					"</studyIdentifier>"+
					 "</StudyPatientSearch>"+
					"</ser:getStudyPatients>";

	private static final String addStudyPatientStatusRequestTemplate =
			"<ser:addStudyPatientStatus xmlns:ser=\"http://velos.com/services/\">"+
					"<PatientIdentifier>"+
					"<organizationId><siteName>%s</siteName></organizationId>"+
					"<patientId>%s</patientId>"+
					"</PatientIdentifier>"+
					"<StudyIdentifier><studyNumber>%s</studyNumber></StudyIdentifier>"+
					"<PatientStudyStatus>"+
					"<currentStatus>true</currentStatus>"+
					"<enrollingSite><siteName>%s</siteName></enrollingSite>"+
					"<patientStudyId>%s</patientStudyId>"+
					"<enrolledBy><userLoginName>%s</userLoginName></enrolledBy>"+
					"<status><code>%s</code><type>patStatus</type></status>"+
					"<statusDate>%s</statusDate>"+
					"</PatientStudyStatus>"+
					"</ser:addStudyPatientStatus>";
	
	private static final String pendingEnrollPatientRequestTemplate =
			"<ser:enrollPatientToStudy xmlns:ser=\"http://velos.com/services/\">"+
					"<PatientIdentifier>"+
					"<OID>%s</OID>"+
					"<organizationId><siteName>%s</siteName></organizationId>"+
					//"<patientId>%s</patientId>"+
					"</PatientIdentifier>"+
					"<StudyIdentifier><studyNumber>%s</studyNumber></StudyIdentifier>"+
					"<PatientEnrollmentDetails>"+
					"<currentStatus>true</currentStatus>"+
					"<enrolledBy><userLoginName>%s</userLoginName></enrolledBy>"+
					"<enrollingSite><siteName>%s</siteName></enrollingSite>"+
					//"<patientStudyId>%s</patientStudyId>"+
					"<status><code>%s</code><type>patStatus</type></status>"+
					"<statusDate>%s</statusDate>"+
					"</PatientEnrollmentDetails>"+
					"</ser:enrollPatientToStudy>";
	/*	
	private static final String patDemogSearchPatientRequestTemplate1 =
			"<ser:searchPatient xmlns:ser=\"http://velos.com/services/\"><PatientSearch>"
					+ "<exactSearch>true</exactSearch>"
					+ "<patFirstName>%s</patFirstName><patLastName>%s</patLastName>"
					+ "<patOrganization><siteName>%s</siteName></patOrganization>"
					+ "<patientID>%s</patientID>"
					+"</PatientSearch></ser:searchPatient>";*/
	private static final String patDemogSearchPatientRequestTemplate =
			"<ser:searchPatient xmlns:ser=\"http://velos.com/services/\"><PatientSearch>"
					+ "<exactSearch>true</exactSearch>"
					+ "<patOrganization><siteName>%s</siteName></patOrganization>"
					+ "<patientID>%s</patientID>"
					+ "</PatientSearch></ser:searchPatient>";

	private static final String patDemogSearchPatientRequestTemplate1 =
			"<ser:searchPatient xmlns:ser=\"http://velos.com/services/\"><PatientSearch>"
					+ "<patFirstName>%s</patFirstName><patLastName>%s</patLastName>"
					+ "<patOrganization><siteName>%s</siteName></patOrganization>"
					//+ "<patientFacilityID>%s</patientFacilityID>"
					+ "<patientID>%s</patientID>"
					+ "</PatientSearch></ser:searchPatient>";
	
	private static final String patDemogSearchPatientRequestTemplate2 =
			"<ser:searchPatient xmlns:ser=\"http://velos.com/services/\"><PatientSearch>"
			//+ "<exactSearch>true</exactSearch>"
			+ "<patDateofBirth>%s</patDateofBirth>"
			+ "<patFirstName>%s</patFirstName><patLastName>%s</patLastName>"
			+ "<patOrganization><siteName>%s</siteName></patOrganization>"
			//+ "<patientFacilityID>%s</patientFacilityID>"
			+ "<patientID>%s</patientID>"
			+ "</PatientSearch></ser:searchPatient>";
	private static final String studyPatientSatusTemplate = 
			"<ser:getStudyPatientStatus xmlns:ser=\"http://velos.com/services/\">"
					+ "<PatientStudyStatusIdentifier>"
					+ "<OID>%s</OID>"
					+ "</PatientStudyStatusIdentifier>"
					+ "</ser:getStudyPatientStatus>";

	//getOutboundMessage Service
	private static final String getOutboundMessagesTemplate ="<ser:getOutboundMessages xmlns:ser=\"http://velos.com/services/\">"
			+ ""+"<fromDate>%s</fromDate>"+
			"<toDate>%s</toDate>"+
			"<moduleName>%s</moduleName>"+
			"<calledFrom>%s</calledFrom>"+
			"</ser:getOutboundMessages>";


	public static void main(String[] args) throws Exception {
		//VelosEspClient client = new VelosEspClient();
		//Map<VelosKeys, String> requestMap = new HashMap<VelosKeys, String>();
		//requestMap.put(ProtocolKeys.StudyNumber, "Adam-1");

		// Test study summary
		//client.testStudySummary(requestMap);

		//getFormattedCurrentDate();
		System.out.println("result=>"+(Math.random()*100));
		// Test study patients
		//client.testStudyPatient(requestMap);

		// Test study calendar
		//client.testStudyCalendar(requestMap);

		// Test add new patient study status
		//requestMap.put(ProtocolKeys.PatientId, "9998");
		//requestMap.put(ProtocolKeys.SiteName, HARDCODED_ORG);
		//String patStudyStat = EpicMaps.getEpicToVelosPatStudyStatMap().get(HARDCODED_PATSTUDYSTAT);
		//if (null != patStudyStat){
		// 	requestMap.put(ProtocolKeys.StudyPatStatus, patStudyStat);
		//	client.testAddStudyPatientStatus(requestMap);
		//}

		// Test enroll patient to study
		//requestMap.put(ProtocolKeys.PatientId, "444");
		//requestMap.put(ProtocolKeys.SiteName, HARDCODED_ORG);
		//client.testEnrollPatient(requestMap);

		// Test search patient
		//requestMap.put(ProtocolKeys.Dob, "19861202");
		//requestMap.put(ProtocolKeys.LastName, "Krystal");
		//requestMap.put(ProtocolKeys.PatientID, "444");
		//requestMap.put(ProtocolKeys.SiteName, HARDCODED_ORG);
		//client.testSearchPatient(requestMap);
	}

	private void testStudySummary(Map<VelosKeys, String> requestMap) {
		Map<VelosKeys, Object> dataMap = handleRequest(VelosEspMethods.StudyGetStudySummary, requestMap);
		for (VelosKeys key : dataMap.keySet()) {
			System.out.println("key="+key+"; value="+dataMap.get(key));
		}
	}

	private void testAddStudyPatientStatus(Map<VelosKeys, String> requestMap) {
		handleRequest(VelosEspMethods.StudyPatAddStudyPatientStatus, requestMap);
	}

	private void testEnrollPatient(Map<VelosKeys, String> requestMap) {
		handleRequest(VelosEspMethods.StudyPatEnrollPatientToStudy, requestMap);
	}

	@SuppressWarnings("unchecked")
	private void testStudyPatient(Map<VelosKeys, String> requestMap) {
		Map<VelosKeys, Object> dataMap = handleStudyPatientRequest(requestMap);
		List<Map<VelosKeys, Object>> studyPatientMap = (List<Map<VelosKeys, Object>>)dataMap.get(ProtocolKeys.StudyPatientList);
		for (Map<VelosKeys, Object> map : studyPatientMap) {
			System.out.println("study patient ID: "+map.get(ProtocolKeys.StudyPatId));
		}
	}

	@SuppressWarnings("unchecked")
	private void testStudyCalendar(Map<VelosKeys, String> requestMap) {
		Map<VelosKeys, Object> studyDataMap = handleStudyRequest(requestMap);
		System.out.println("Study title: "+studyDataMap.get(ProtocolKeys.Title));
		Map<VelosKeys, Object> calendarListDataMap = handleStudyCalendarListRequest(requestMap);
		List< Map<VelosKeys, Object> > calendarIdList = (List< Map<VelosKeys, Object> >)
				calendarListDataMap.get(ProtocolKeys.CalendarIdList);
		for (Map<VelosKeys, Object> calendarMap : calendarIdList) {
			System.out.println("Cal_Id="+calendarMap.get(ProtocolKeys.CalendarId)
					+" Cal_Name="+calendarMap.get(ProtocolKeys.CalendarName));
			requestMap.put(ProtocolKeys.CalendarId,
					String.valueOf(calendarMap.get(ProtocolKeys.CalendarId)));
			Map<VelosKeys, Object> calendarDataMap = handleStudyCalendarRequest(requestMap);
			List<Map<VelosKeys, Object>> visitList = (List<Map<VelosKeys, Object>>)
					calendarDataMap.get(ProtocolKeys.VisitList);
			for (Map<VelosKeys, Object> visitMap : visitList) {
				System.out.println("Visit_Name="+visitMap.get(ProtocolKeys.VisitName));
				List<Map<VelosKeys, Object>> eventList = (List<Map<VelosKeys, Object>>)
						visitMap.get(ProtocolKeys.EventList);
				if (eventList == null) { continue; }
				for (Map<VelosKeys, Object> eventMap : eventList) {
					System.out.println("Event_name="+eventMap.get(ProtocolKeys.EventName));
					System.out.println("Event_coverage="+eventMap.get(ProtocolKeys.CoverageType));
				}
			}
		}
	}

	private void testSearchPatient(Map<VelosKeys, String> requestMap) {
		Map<VelosKeys, Object> dataMap = handleRequest(VelosEspMethods.PatDemogSearchPatient, requestMap);
		for (VelosKeys key : dataMap.keySet()) {
			System.out.println("key="+key+"; value="+dataMap.get(key));
		}
	}

	public Map<VelosKeys, Object> handleRequest(VelosEspMethods method, Map<VelosKeys, String> requestMap)  {
		if (requestMap == null) { return null; }
		MessageChannel channel = null;
		String userGroupName = null;
		String groupOrg = null;
		String pagNum = null;
		String body = null;
		@SuppressWarnings("resource")
		ClassPathXmlApplicationContext context1 = null;
		boolean isAlertProtocolRequest = false;
		try{
			context1= new ClassPathXmlApplicationContext("velos-espclient-req.xml");

			ArrayList<String> argList = null ;
			argList = new ArrayList<String>();
			ArrayList<String> eventargList = null ;
			eventargList = new ArrayList<String>();
			ArrayList<String> visitargList = null ;
			visitargList = new ArrayList<String>();
			// Call Velos WS
			logger.info("********VelosEspClient handleRequest************");
			String siteName = prop.getProperty("velos.organization");
			String userId = prop.getProperty("velos.userID");
			logger.info("userId="+userId);
			logger.info("siteName=="+siteName);
			//System.out.println("StudyStatuses=="+VelosEspMethods.StudyStat);
			logger.info("method=="+method);
			switch(method) {
			
			case GroupUserService:
				
		        channel = context1.getBean("searchUserRequests", MessageChannel.class);
		       logger.info("Group User Services getting properties from epic.properties****");
		       userGroupName =  prop.getProperty("groupName").trim();
		       argList.add(userGroupName);
		       groupOrg = prop.getProperty("groupOrganization").trim();
		       argList.add(groupOrg);
		       pagNum = prop.getProperty("pageNumber").trim();
		       argList.add(pagNum);
		        body = String.format(groupUserService, argList.toArray());
		        
				break;
			case StudyGetStudySummary:
				logger.info(" ******** Study Get Study Summary **********");
				channel = context1.getBean("studyRequests", MessageChannel.class);
				System.out.println("StudySummary");
				body = String.format(studyRequestTemplate, requestMap.get(ProtocolKeys.StudyNumber));		
				break;
			case StudyCalGetStudyCalendar:
				logger.info(" ******** Study Get Study Calendar **********");
				channel = context1.getBean("studyCalendarRequests", MessageChannel.class);
				argList.add(requestMap.get(ProtocolKeys.StudyNumber));
				argList.add(requestMap.get(ProtocolKeys.CalendarName));
				body = String.format(studyCalendarNameRequestTemplate,argList.toArray());
				break;
			case StudyCalGetStudyCalendarList:
				logger.info(" ******** Study Get Study CalendarList **********");
				channel = context1.getBean("studyCalendarRequests", MessageChannel.class);
				//  argList.add(requestMap.get(ProtocolKeys.StudyNumber));
				body = String.format(studyCalendarListRequestTemplate, requestMap.get(ProtocolKeys.StudyNumber));
				break;
			case StudyPatGetStudyPatients:
				logger.info(" ******** Study Get Study Patients **********");
				channel = context1.getBean("studyPatientRequests", MessageChannel.class);
				body = String.format(studyPatientRequestTemplate, requestMap.get(ProtocolKeys.StudyNumber));
				break;
			case StudyPatAddStudyPatientStatus:
				logger.info(" ******** Study Add  Study Patient Status **********");
				isAlertProtocolRequest = true;
				channel = context1.getBean("studyPatientRequests", MessageChannel.class);
				//argList.add(HARDCODED_ORG);
				argList.add(siteName);
				argList.add(requestMap.get(ProtocolKeys.PatientId));
				argList.add(requestMap.get(ProtocolKeys.StudyNumber));
				//argList.add(HARDCODED_ORG);
				argList.add(siteName);

				if("NO".equalsIgnoreCase(prop.getProperty("UpdateSubjectID").toString().trim()) ||requestMap.get(ProtocolKeys.SubjectID) == null ){
					handleRequest(VelosEspMethods.StudyPatGetStudyPatients,requestMap);
					if (dataMap.get(ProtocolKeys.FaultString) != null) {
						return dataMap;
					}
					//logger.info("STUDY PATIENT ID ===================>"+requestMap.get(ProtocolKeys.StudyPatId).toString());
					argList.add((String)requestMap.get(ProtocolKeys.StudyPatId));
				}
				else if(requestMap.get(ProtocolKeys.SubjectID).equals("")){
					logger.info("**************** Subject ID is Empty **************");
					argList.add("");
				}
				else if("YES".equalsIgnoreCase(prop.getProperty("UpdateSubjectID").toString().trim())) {
					logger.info("**************** Subject ID is Added **************");
					argList.add(requestMap.get(ProtocolKeys.SubjectID));
				}
				//argList.add(requestMap.get(ProtocolKeys.PatientId)+"-"+((int)(Math.random()*100)));
				//argList.add(requestMap.get(ProtocolKeys.StudyPatId));
				argList.add(userId);
				argList.add(requestMap.get(ProtocolKeys.StudyPatStatus));
				argList.add(this.getFormattedCurrentDate());

				body = String.format(addStudyPatientStatusRequestTemplate, argList.toArray());
				logger.info("\nAlerportocol status Message Request body ===>"+body.toString()+"\n");
				break;
			case StudyPatEnrollPatientToStudy:
				logger.info(" ******** Study Enroll Patient to Study **********");
				channel = context1.getBean("studyPatientRequests", MessageChannel.class);
				//argList.add(HARDCODED_ORG);
				argList.add(requestMap.get(ProtocolKeys.OID));
				argList.add(siteName);
				//argList.add(requestMap.get(ProtocolKeys.PatientId));
				argList.add(requestMap.get(ProtocolKeys.StudyNumber));
				//argList.add(HARDCODED_ORG);
				//argList.add(requestMap.get(ProtocolKeys.PatientId)+"-"+((int)(Math.random()*100)));
				argList.add(userId);
				argList.add(siteName);
				argList.add(requestMap.get(ProtocolKeys.ProcessState));
				argList.add(this.getFormattedCurrentDate());
				body = String.format(pendingEnrollPatientRequestTemplate, argList.toArray());


				break;
			case PatDemogSearchPatient:
				logger.info(" ******** Patient Demographic Search  **********");
				channel = context1.getBean("patientDemographicsRequests", MessageChannel.class);
				/*String formattedDob = requestMap.get(ProtocolKeys.Dob);
			if (formattedDob != null) {
				argList.add(formattedDob);
			}
			argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.FirstName)));
			argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.LastName)));*/
				//argList.add(MapUtil.nullToEmpty(requestMap.get(mapPatientDetailsForEpicProtocolKeys.SiteName)));
				//argList.add(MapUtil.nullToEmpty(HARDCODED_ORG));
				//argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.PatientID)));
				/*body = String.format(formattedDob == null ? patDemogSearchPatientRequestTemplate1 : 
					patDemogSearchPatientRequestTemplate2, argList.toArray());*/
				argList.add(MapUtil.nullToEmpty(siteName));
				argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.PatientFacilityId)));
				body = String.format(patDemogSearchPatientRequestTemplate, argList.toArray());
				break;
			case PatientStudyGetStudyPatientStatusHistory:

				channel = context1.getBean("studyPatientStatusHistoryRequests", MessageChannel.class);
				/*	argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.FirstName)));
			argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.LastName)));*/
				argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.StudyId)));
				argList.add(requestMap.get(ProtocolKeys.OID));
				//argList.add(MapUtil.nullToEmpty(siteName));
				//argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.SiteName)));
				//argList.add(MapUtil.nullToEmpty(HARDCODED_ORG));
				//argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.PatientID)));

				body = String.format(studyPatStatusHistoryTemplate, argList.toArray());

				//logger.info("Soap Boady ---->"+body.toString());

				break;
			case CreatePatient:
				//createPatientRequests
				logger.info("********* Create Pateint ************");
				channel = context1.getBean("createPatientRequests", MessageChannel.class);
				argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.Dob)));
				argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.FirstName)));
				argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.LastName)));
				argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.MiddleName)));
				argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.StreetAddressLine)));
				argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.City)));
				argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.State)));
				argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.PostalCode)));
				argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.Country)));
				argList.add(MapUtil.nullToEmpty(siteName));
				argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.PatientFacilityId)));
				argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.PatientFacilityId)));
				body = String.format(createPatientRequestTemplate, argList.toArray());

				break;
			case PatientDetails:
				channel = context1.getBean("getPatientDetailsRequests", MessageChannel.class);
				argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.OID)));
				argList.add(MapUtil.nullToEmpty(siteName));
				argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.PatientID)));
				body = String.format(getPatientDetailsTemplate, argList.toArray());
				break;	

			case StudyPatientStatus:
				channel = context1.getBean("getStudyPatientStatus", MessageChannel.class);
				argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.StudyPatOID)));
				body = String.format(studyPatientSatusTemplate, argList.toArray());

				break;

			case StudyGetStudyStat:
				logger.info("***********StudyStat******************");
				channel = context1.getBean("studyStatusRequests", MessageChannel.class);
				body = String.format(getStudystatusesTemplate, requestMap.get(ProtocolKeys.StudyNumber));
				logger.info("StudyStatuses request="+body);
				break;
			case OutboundMessages:
				logger.info("***********calling OutboundMessages******************");
				channel = context1.getBean("outBoundMessageRequests", MessageChannel.class);
				argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.FromDate).trim()));
				argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.ToDate).trim()));
				argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.ModuleName).trim()));
				argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.CalledFrom).trim()));

				body = String.format(getOutboundMessagesTemplate,argList.toArray());
				break;
			case StudyCreation:

				channel =context1.getBean("studyStatusRequests",MessageChannel.class);

				System.out.println("*************************StudyCreation****************");
				final String studyCreationTemplate = espServiceprop.getProperty("createStudyTemplate").toString().trim();

				System.out.println("StudyTemplate====>"+studyCreationTemplate);

				argList.add(MapUtil.nullToEmpty(espServiceprop.getProperty("createStudy_organization").toString().trim()));
				argList.add(MapUtil.nullToEmpty(espServiceprop.getProperty("createStudy_organization").toString().trim()));
				argList.add(MapUtil.nullToEmpty(espServiceprop.getProperty("createStudy_userName").toString().trim()));
				argList.add(MapUtil.nullToEmpty(espServiceprop.getProperty("createStudy_organization").toString().trim()));
				argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.Status).trim()));
				argList.add(MapUtil.nullToEmpty(getFormattedCurrentDate().toString()));
				argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.Phase)));
				argList.add(MapUtil.nullToEmpty(espServiceprop.getProperty("createStudy_userName").toString().trim()));
				argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.StudyNumber).trim()));
				argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.Title)));
				argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.Text)));
				argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.Division)));
				argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.TherapeuticArea)));
				argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.Status)));
				argList.add(MapUtil.nullToEmpty(espServiceprop.getProperty("createStudy_groupName").toString().trim()));
				argList.add(MapUtil.nullToEmpty(espServiceprop.getProperty("createStudy_organization").toString().trim()));
				argList.add(MapUtil.nullToEmpty(espServiceprop.getProperty("createStudy_userName").toString().trim()));

				System.out.println("Study ArgList =========#>"+argList);

				body = String.format(studyCreationTemplate, argList.toArray());




				break;

			case StudyCalendarCreation:

				channel =context1.getBean("studyCalendarRequests",MessageChannel.class);
				argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.CalendarName)));
				argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.StudyNumber)));
				String eventVisitList = requestMap.get(ProtocolKeys.EventList);
				ArrayList<String> eventList = new ArrayList<String>();
				String[] eventVist = eventVisitList.split("-");
				String eventTemplate = espServiceprop.getProperty("event_Template");
				StringBuffer sb = new StringBuffer();
				for(String event : eventVist ){
					String [] ev = event.split("~");
					String eventName = ev[1];
					System.out.println("EVENT Name =============>"+eventName);
					eventargList.add(eventName);
					String eventTemp = String.format(eventTemplate,eventargList.toArray());
					//eventList.add(eventTemp);
					sb.append(eventTemp);
				}

				argList.add(sb.toString());
				final String studyCalendarTemplate = espServiceprop.getProperty("createStudyCalendar_Template");

				String visit_Template = espServiceprop.getProperty("visit_Template");

				String visitlist = requestMap.get(ProtocolKeys.VisitList);
				System.out.println("VisitTemplate==>"+visit_Template);
				String[] vis = visitlist.split("~");
				StringBuffer sb1 = new StringBuffer();
				for(String vi : eventVist){

					String[] ev = vi.split("~");
					visitargList = new ArrayList<String>();
					visitargList.add(ev[0]);
					eventargList = new ArrayList<String>();
					eventargList.add(ev[1]);
					String eventTemp = String.format(eventTemplate,eventargList.toArray());
					visitargList.add(eventTemp);
					String visitTemp = String.format(visit_Template,visitargList.toArray());
					System.out.println("VisitE===>"+visitTemp);
					sb1.append(visitTemp);
				}
				argList.add(sb1.toString());
				body = String.format(studyCalendarTemplate, argList.toArray());
				logger.info("StudyCalendar Request =====> "+ body);
				break;
			default:

				logger.info("***************default***************");
				channel = context1.getBean("studyStatusRequests", MessageChannel.class);
				body = String.format(getStudystatusesTemplate, requestMap.get(ProtocolKeys.StudyNumber));
				logger.info("StudyStatuses request="+body);
				break;
			}

		}catch(Exception e){
			e.printStackTrace();
			logger.info("Error message ======:>"+e.getMessage());
		}

		
		MessagingTemplate messagingTemplate =null; 
		Message<?> message = null;
		messagingTemplate =  new MessagingTemplate();

		try {
			logger.info("**************** Sending esp Request message **********");
			sslJavaTrustStore();
			if(body !=null){
				logger.info("Sent=============:>>"+body.toString());
			message = messagingTemplate.sendAndReceive(
					channel, MessageBuilder.withPayload(body).build());
			}else if(body ==null){
				logger.info("Request message is not get because of issues");
			}
			/*if(method.equals("createStudy")){
        		DOMSource source = (DOMSource)new DomSourceFactory().createSource(message.getPayload());
        		if (FAULT_STR.equals(source.getNode().getLocalName())) {

        		}
        	}*/
		} catch(MessageHandlingException e) {
			logger.info("Exception Caught for Esp Message Response========:>"+e.getMessage());
			e.printStackTrace();
			try{
				message = extractOperationException(e);
			}catch(Exception eee){
				System.out.println("Message Caught Excepiton ===>"+ eee.getMessage());
				eee.printStackTrace();
			}
		
			if(message != null) {
				logger.info("message="+message);
				DOMSource source = (DOMSource)new DomSourceFactory().createSource(message.getPayload());
				if (FAULT_STR.equals(source.getNode().getLocalName())) {
					System.out.println("Fault String *****************");
					NodeList nodeList = source.getNode().getChildNodes();
					for (int iX = 0; iX < nodeList.getLength(); iX++) {
						System.out.println("Fault String *****************");
						if (ProtocolKeys.FaultString.toString().equals(nodeList.item(iX).getLocalName())) {
							Map<VelosKeys, Object> retMap = new HashMap<VelosKeys, Object>();
							System.out.println("Fault String *****************");
							retMap.put(ProtocolKeys.FaultString, nodeList.item(iX).getTextContent());
							return retMap;
						}
					}
				}
			}
		}finally{
			if(context1 != null){
			context1.close();
			}
		}

		//logger.info("Got Response---------->:"+message.getPayload());



		String responseMessage = null; 
		if(message != null) {
		responseMessage = (String) message.getPayload();
		System.out.println("StudyMethod ==========>"+responseMessage);
		}
		try{
			if(message != null &&  method.equals(method.StudyCreation)){
				dataMap= new HashMap<VelosKeys,Object>();
				if(responseMessage.contains("CREATE")){
					dataMap.put(ProtocolKeys.StudyResponse, "Study Created");
					return dataMap;
				}
			}
		}catch(Exception e){
			//throw e;
		}finally{
			/*
			 * if(dataMap!=null){ dataMap.clear(); }
			 */
		}
		try{
			if(message != null &&  method.equals(method.StudyCalendarCreation)){
				dataMap= new HashMap<VelosKeys,Object>();
				if(responseMessage.contains("CREATE")){
					dataMap.put(ProtocolKeys.StudyResponse, "Aleart Received");
					return dataMap;
				}
			}
		}catch(Exception e){
			//throw e;
		}finally{
			//dataMap = null;
		}
		// Transform XML to object to Map
		if(message != null) {
		dataMap = transformMessageToMap(message, requestMap.get(EndpointKeys.Endpoint));
		logger.info("DATA MAP in velosespclient----->"+dataMap);
		}
		//return transformMessageToMap(message, requestMap.get(EndpointKeys.Endpoint));
		if(message != null){
			if(responseMessage.contains("changes")){
				dataMap.put(ProtocolKeys.ChangesMessage, responseMessage);
				logger.info("Changes Message ========>"+responseMessage);
			}
		}
		

		if(dataMap != null && method.equals(VelosEspMethods.StudyPatGetStudyPatients)){
			List< Map<VelosKeys, Object> > stdypatlist= null;
			stdypatlist =(List<Map<VelosKeys, Object>>) dataMap.get(ProtocolKeys.StudyPatientList);
			int lisize = 0;
			if(stdypatlist != null) {
			 lisize = stdypatlist.size();
			}

			for(int i=0; i<lisize; i++){
				Map<VelosKeys, Object> map =  stdypatlist.get(i);
				if(map.get(ProtocolKeys.Mrn).equals(requestMap.get(ProtocolKeys.PatientId))){
					requestMap.put(ProtocolKeys.StudyPatId, map.get(ProtocolKeys.StudyPatId).toString());
				}

			}


		}

		if(isAlertProtocolRequest){
			if(dataMap!=null && dataMap.containsKey(ProtocolKeys.ErrorList)){
				ArrayList errorList = (ArrayList)dataMap.get(ProtocolKeys.ErrorList);
				if(errorList==null || errorList.isEmpty()){
					dataMap.put(ProtocolKeys.FaultString, "Patient is not enrolled to this study number...");
				}
			}
		}else{
			//System.out.println();
		}
		System.out.println("************************************************");
		return dataMap;
	}

	private final TransformerFactory tfactory = TransformerFactory.newInstance();

	private Message<?> extractOperationException(MessageHandlingException messageException) {
		StringWriter writer = null;
		Message<?> message = null;
		try {
		SoapFaultClientException se = (SoapFaultClientException) messageException.getCause();
		SoapFaultDetail detail = se.getSoapFault().getFaultDetail();
		//System.out.println("Extract Opertaion Exception ===>"+detail.getResult());
		 writer = new StringWriter();
		Source source = null;
		Result result = new StreamResult(writer);
		if (detail == null || detail.getDetailEntries() == null) {
			source = se.getSoapFault().getSource(); 
		} else {
			SoapFaultDetailElement elem = detail.getDetailEntries().next();
			source = elem.getSource();
		}
		Transformer xform = null;
		
			xform = tfactory.newTransformer();
			xform.transform(source, result);
		} catch(Exception e1) {
			System.out.println("Extract Opertaion Exception ===>");
			e1.printStackTrace();
			return message;
		}
		System.out.println(writer.toString());
		 message = MessageBuilder.withPayload(writer.toString()).build();
		return message;
	}

	public Map<VelosKeys, Object> handleStudyRequest(Map<VelosKeys, String> requestMap) {
		return handleRequest(VelosEspMethods.StudyGetStudySummary, requestMap);
	}

	public Map<VelosKeys, Object> handleStudyCalendarRequest(Map<VelosKeys, String> requestMap) {
		return handleRequest(VelosEspMethods.StudyCalGetStudyCalendar, requestMap);
	}

	public Map<VelosKeys, Object> handleStudyPatientRequest(Map<VelosKeys, String> requestMap) {
		return handleRequest(VelosEspMethods.StudyPatGetStudyPatients, requestMap);
	}

	public Map<VelosKeys, Object> handleStudyCalendarListRequest(Map<VelosKeys, String> requestMap) {
		return handleRequest(VelosEspMethods.StudyCalGetStudyCalendarList, requestMap);
	}

	public Map<VelosKeys, Object> handleOutboundMessageRequest(VelosEspMethods method,Map<VelosKeys, String> requestMap) {
		return handleRequest(method,requestMap);
	}



	private Map<VelosKeys, Object> transformMessageToMap(Message<?> message, String endpoint) {
		logger.info("transformMessageToMap starts="+message);
		System.out.println("transformMessageToMap starts="+message);
		logger.info("transformMessageToMap endpoint"+endpoint);
		@SuppressWarnings("resource")
		ClassPathXmlApplicationContext context = null;
		VelosMessageHandler messageHandler = null;
		try{
			context= new ClassPathXmlApplicationContext("velos-espclient-resp.xml");
			MessageChannel channelXml = context.getBean("input-xml", MessageChannel.class);
			PollableChannel output = context.getBean("output", PollableChannel.class);
			// Transform XML to object
			channelXml.send(message);
			Message<?> reply = output.receive();
			// Transform object to map
			messageHandler = context.getBean(VelosMessageHandler.class);
			messageHandler.setEndpoint(endpoint);
			messageHandler.handleMessage(reply);

		}catch(Exception e){
			e.printStackTrace();
			logger.info("Transform message to object===>"+e.getMessage());
			//throw e;
		}finally{
			if(context != null){
			context.close();
			}
			System.gc();
		}
		return messageHandler.getDataMap();
	}

	/*private String getFormattedCurrentDate() {
    	SimpleDateFormat f = new SimpleDateFormat("MM-dd-yyyy'T'HH:mm:ss");
    	return f.format(Calendar.getInstance().getTime());
    }
	 */
	private String getFormattedCurrentDate() {
		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
		return f.format(Calendar.getInstance().getTime());
	}
	@Bean
	public Wss4jSecurityInterceptor wssBean() {
		logger.info("***********Adding Security Tokens ************");
		String userId = prop.getProperty("velos.userID");
		String password = prop.getProperty("velos.password");
		Wss4jSecurityInterceptor wssBean = new Wss4jSecurityInterceptor();
		wssBean.setSecurementActions("UsernameToken");
		wssBean.setSecurementUsername(userId);
		wssBean.setSecurementPassword(password);
		wssBean.setSecurementPasswordType("PasswordText");
		return wssBean;
	}

	public Map<VelosKeys, Object> handleStudyRequest(VelosEspMethods method, Map<Object,Object> studyRequestMap)  {
		dataMap = new HashMap<VelosKeys, Object>();
		Map<String,Map> componentMap = null;
		if (studyRequestMap == null) { return null; }
		MessageChannel channel = null;
		String body = null;
		studyRequestMap.put("EndpointKeys.Endpoint", "endpoint");
		@SuppressWarnings("resource")
		ClassPathXmlApplicationContext context2 = null;
		Map<VelosKeys, String> requestMap = null;
		componentMap = new HashMap<String,Map>();
		try{
			context2= new ClassPathXmlApplicationContext("velos-espclient-req.xml");

			ArrayList<String> argList = null ;
			argList = new ArrayList<String>();
			ArrayList<String> eventargList = null ;

			ArrayList<String> visitargList = null ;
			visitargList = new ArrayList<String>();



			// Call Velos WS
			logger.info("********VelosEspClient handleRequest************");
			String siteName = prop.getProperty("velos.organization");
			String userId = prop.getProperty("velos.userID");
			logger.info("userId="+userId);
			logger.info("siteName=="+siteName);
			//System.out.println("StudyStatuses=="+VelosEspMethods.StudyStat);
			logger.info("method=="+method);
			switch(method) {

			case StudyCreation:
				
				channel =context2.getBean("studyStatusRequests",MessageChannel.class);

				logger.info("*************************StudyCreation****************");
				/* java.net.URL url = ClassLoader.getSystemResource("esp_services.properties");
		  espServiceprop = new Properties();
  	      espServiceprop.load(url.openStream());*/
				final String studyCreationTemplate = espServiceprop.getProperty("createStudyTemplate").toString().trim();

				System.out.println("StudyTemplate====>"+studyCreationTemplate);

				List keyList =getPropertyKeyList("esp_services.properties","select");

				logger.info("Checking ESP Request XML");
				for(Object key : keyList){

					if(!studyCreationTemplate.toLowerCase().contains((key.toString().toLowerCase()))){
						if(!key.equals("CurrentDate")){
							dataMap.put(ProtocolKeys.FaultString,"Element "+key+" Missing in ESP Request XML Please add in esp_services.properties file");
							return dataMap;
						}
					}
				}
				logger.info("After Checking ESP Request XML");


				studyRequestMap.put("userLoginName",configuration.getProperty("userLoginName"));
				studyRequestMap.put("siteName",configuration.getProperty("siteName"));
				studyRequestMap.put("groupName",configuration.getProperty("groupName"));
				studyRequestMap.put("CurrentDate",getFormattedCurrentDate());
				studyRequestMap.put("userType", configuration.getProperty("userType"));
				studyRequestMap.put("teamRole", configuration.getProperty("teamRoleCode"));
				studyRequestMap.put("statusType", configuration.getProperty("studyStatusTypeCodelstSubtype"));



				for(Object keyValue : keyList){
					System.out.println("Keysssss==>"+keyValue+"-"+keyList.size());

					argList.add((String) MapUtil.nullToEmpty(studyRequestMap.get(keyValue)));
				}

				

				System.out.println("Study ArgList =========#>"+argList);

				body = String.format(studyCreationTemplate, argList.toArray());
				requestMap = (Map<VelosKeys, String>) studyRequestMap.get("requestMap");
				configuration = null;
				break;
			case  StudyCalendarCreation :
				channel =context2.getBean("studyCalendarRequests",MessageChannel.class);

				requestMap = (Map<VelosKeys, String>) studyRequestMap.get("requestMap");
				componentMap =  (Map) studyRequestMap.get("componentMap");
				List<String> eventVisitListNames = (List) studyRequestMap.get("eventVistList");
				
				List eventVisitNames = new ArrayList();
				eventVisitNames.addAll(eventVisitListNames);
				logger.info("Event & Visit Names ===>"+eventVisitListNames +"\n All field Names==>"+eventVisitNames);
				
				Map<String,String> eventMap = new HashMap<String,String>();
				argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.CalendarName)));
				argList.add(MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.StudyNumber)));
				String eventVisitList = requestMap.get(ProtocolKeys.EventList);
				ArrayList<String> eventList = new ArrayList<String>();
				String[] eventVist = eventVisitList.split("-");
				String eventTemplate = espServiceprop.getProperty("event_Template");
				StringBuffer sb = new StringBuffer();
				
			
				//Event Template creating
				if(eventVist != null){
					for(String event : eventVist ){
						String [] ev = event.split("~");
						String eventName = ev[1];
						String visitName = ev[0];
						eventargList = new ArrayList<String>();
						logger.info("EVENT Name =============>"+ev[1]);
						 Iterator<String> it = eventVisitNames.iterator();
							while(it.hasNext())	{
								String evenViName = it.next();
							if(evenViName.contains(ev[1])){
								System.out.println(" ************* True ***********"+ evenViName);

								Map comValMap = componentMap.get(evenViName);

								System.out.println("component Map ====>"+componentMap.get(evenViName));

								String cptCode =	(String) comValMap.get("cptCode");
								String sequenceNumber=	(String) comValMap.get("sequenceNumber");
								String billModifier = (String) comValMap.get("bill_modifier");
								eventargList.add(isEmpty(cptCode));
								eventargList.add(isEmpty(billModifier));
								eventargList.add(eventName);
								eventVisitListNames.remove(evenViName);
							}
						}

						String eventTemp = String.format(eventTemplate,eventargList.toArray());
						String eve ="";
						System.out.println("EventMap Zise ===============>"+eventMap.size());
						System.out.println("Event Name in Map======>"+eventMap + "---"+ visitName);
						//Appending events based on Visit Names.
						if(eventMap.size() == 0) {
							eventMap.put(visitName,eventTemp);
						}else if(eventMap.size() >=1) {
							if( eventMap.containsKey((visitName))){
								eve = eventMap.get(visitName);
								eve = eve+""+eventTemp;
								eventMap.put(visitName, eve);
							}else if(!eventMap.containsKey((visitName))) {
								eventMap.put(visitName,eventTemp);
							}
							}
						sb.append(eventTemp);
					}
				}


				logger.info("EventTemplate Map ====>"+eventMap);




				argList.add(sb.toString());
				final String studyCalendarTemplate = espServiceprop.getProperty("createStudyCalendar_Template");

				String visit_Template = espServiceprop.getProperty("visit_Template");

				String visitlist = requestMap.get(ProtocolKeys.VisitList);
				System.out.println("VisitTemplate==>"+visit_Template);
				String[] vis = visitlist.split("~");
				StringBuffer sb1 = new StringBuffer();
				String sequenceNumber = null;
				//Visit Template Creating.
				if(eventVist != null){
					for(String vi : eventVist){

						String[] ev = vi.split("~");
						visitargList = new ArrayList<String>();
						visitargList.add(ev[0]);
						System.out.println("VIST & EVENT==>"+ev[0] +"--"+ev[1]);
						eventargList = new ArrayList<String>();
						System.out.println("EventVisit Length====>"+eventVisitNames.size());

						/*for(int j = 0; j< eventVisitNames.size(); j++){
							String evenViName = (String) eventVisitNames.get(j);
							//System.out.println("EVENT Name2 =============>"+ eventVisitListNames.get(j)+"===>"+evenViName);
							if(evenViName.contains(ev[0])){
								System.out.println(" ************* True ***********"+ evenViName);

								Map comValMap = componentMap.get(evenViName);

								System.out.println("component Map ====>"+componentMap.get(evenViName));
								//Getting CPT Code
								String cptCode =	(String) comValMap.get("cptCode");
								sequenceNumber=	(String) comValMap.get("sequenceNumber");
								//Getting Billing Modifier.
								String billModifier = (String) comValMap.get("bill_modifier");
								
								eventargList.add(isEmpty(cptCode));
								eventargList.add(isEmpty(billModifier));
								System.out.println("EVent====>"+ev[1]);
								eventargList.add(ev[1]);
								// ***Hear is the mistake event is coming in Visit sequence tag we need change logic in Visit sequence template creation
								eventVisitNames.remove(evenViName);
							}
						}
*/
						//System.out.println("EventArgList"+eventargList);
						//String eventTemp = String.format(eventTemplate,eventargList.toArray());
						//System.out.println("EVent XML ===>"+eventTemp);
						//Adding Sequence Number
						visitargList.add(isEmpty(sequenceNumber));
						//Adding event to Vist template
						String eventTemp = eventMap.get(ev[0]);
						visitargList.add(eventTemp);
						
						sequenceNumber = null;
						String visitTemp = String.format(visit_Template,visitargList.toArray());
						
						System.out.println("Index OF ===>"+sb1.indexOf(ev[0])+" ##> "+sb1.toString().contains(ev[0]));
					  // Need to check vistname exist or not in the stringBuffer.
						if(!sb1.toString().contains(ev[0])) {
							System.out.println("VisitE===>"+visitTemp);
						sb1.append(visitTemp);
						}
					  
					}
				}
				argList.add(sb1.toString());
				body = String.format(studyCalendarTemplate, argList.toArray());
				logger.info("StudyCalendar Request =====> "+ body);
				eventMap = null;
				sb1 = null;
				break;

			default:
			}

		}catch(Exception e){
			e.printStackTrace();
		}
		logger.info("Sent=============:>>"+body.toString());
		MessagingTemplate messagingTemplate =null; 
		Message<?> message = null;
		messagingTemplate =  new MessagingTemplate();

		try {
			logger.info("**************** Sending esp Request message **********");
			sslJavaTrustStore();
			message = messagingTemplate.sendAndReceive(
					channel, MessageBuilder.withPayload(body).build());
			/*if(method.equals("createStudy")){
	        		DOMSource source = (DOMSource)new DomSourceFactory().createSource(message.getPayload());
	        		if (FAULT_STR.equals(source.getNode().getLocalName())) {

	        		}
	        	}*/
		} catch(MessageHandlingException e) {
			logger.info("Exception Caught for Esp Message Response========:>"+e.getMessage());
			//e.printStackTrace();
			try{
				message = extractOperationException(e);
			}catch(Exception eee){
				//System.out.println("Message Caught Excepiton ===>"+ eee.getMessage());
				//eee.printStackTrace();
				return dataMap;
			}
			
			if(message == null) {
				return dataMap;
			}else if(message !=null) {
				System.out.println("message="+message);
				DOMSource source = (DOMSource)new DomSourceFactory().createSource(message.getPayload());
				if (FAULT_STR.equals(source.getNode().getLocalName())) {
					NodeList nodeList = source.getNode().getChildNodes();
					for (int iX = 0; iX < nodeList.getLength(); iX++) {
						if (ProtocolKeys.FaultString.toString().equals(nodeList.item(iX).getLocalName())) {
							Map<VelosKeys, Object> retMap = new HashMap<VelosKeys, Object>();
							retMap.put(ProtocolKeys.FaultString, nodeList.item(iX).getTextContent());
							return retMap;
						}
					}
				}
			}
		}finally{
			if(context2 != null){
			context2.close();
			}
			System.gc();
		}

		String responseMessage = null; 
		if(message != null) {
		logger.info("Got Response---------->:"+message.getPayload());
		
		responseMessage = (String) message.getPayload();
		System.out.println("StudyMethod ==========>"+responseMessage);
		}
		try{
			if(message != null &&  method.equals(method.StudyCreation)){
				//	dataMap= new HashMap<VelosKeys,Object>();
				if(responseMessage.contains("CREATE")){
					dataMap.put(ProtocolKeys.StudyResponse, "Study Created");
					return dataMap;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			//throw e;
		}finally{
			//dataMap = null;
		}
		
		try{
			if(message != null &&  method.equals(method.StudyCalendarCreation)){
				dataMap= new HashMap<VelosKeys,Object>();
				if(responseMessage.contains("CREATE")){
					dataMap.put(ProtocolKeys.StudyResponse, "AleartReceived");
					return dataMap;
				}
			}
		}catch(Exception e){
			//throw e;
		}finally{
			//dataMap = null;
		}
		
		if(message != null) {
			System.out.println("Message====>"+message);
		dataMap = transformMessageToMap(message, (String) studyRequestMap.get(EndpointKeys.Endpoint));
		}

		return dataMap;

	}

	public List<String> getPropertyKeyList(String propertyFileName, String keyName) throws ConfigurationException{

		configuration = new PropertiesConfiguration(propertyFileName);
		configuration.setReloadingStrategy(new FileChangedReloadingStrategy());
		List keyList =configuration.getList(keyName);
		return keyList;

	}

	@SuppressWarnings("finally")
	public   void sslJavaTrustStore(){
	//	Properties	prop = new Properties();
		try {
		
			/*prop.load(this.getClass().getClassLoader()
					.getResourceAsStream("epic.properties"));*/

			String sslEnable = prop.getProperty("SSL_Enable");

			if(!prop.containsKey("SSL_Enable") || prop.getProperty("SSL_Enable").equalsIgnoreCase("No")
					|| prop.getProperty("SSL_Enable").equals("")){
				logger.info("############# SSL IS DISABLED ############");
			}else if(prop.containsKey("SSL_Enable") && prop.getProperty("SSL_Enable").equalsIgnoreCase("yes")
					&& !prop.getProperty("SSL_Enable").equals("")){

				String keyStore = prop.getProperty("keyStore");
				System.setProperty("javax.net.ssl.keyStore",keyStore);

				String keyStorePassword = prop.getProperty("keyStorePassword");
				System.setProperty("javax.net.ssl.keyStorePassword",keyStorePassword);

				String keyStoreType = prop.getProperty("keyStoreType");
				System.setProperty("javax.net.ssl.keyStoreType",keyStoreType);

				String trustStore = prop.getProperty("trustStore");
				System.setProperty("javax.net.ssl.trustStore",trustStore);

				String trustStorePassword = prop.getProperty("trustStorePassword");
				System.setProperty("javax.net.ssl.trustStorePassword", trustStorePassword);

				String trustStoreType = prop.getProperty("trustStoreType");
				System.setProperty("javax.net.ssl.trustStoreType",trustStoreType);
			}
		} catch (Exception e) {
			logger.error("**epic.properties file load error on sslJavaTruststore method**==>"+e.getMessage());
			e.printStackTrace();
		}finally{
			
		}
	}

	
	  public String isEmpty(String s){
		  System.out.println("*********Checking Empty Values******");
		  if(s == null || s.equals("?")) {
			  s="";
			  return s;
		  }
		  
		  return s;
	  }
}
