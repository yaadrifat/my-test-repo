package com.velos.integration.espclient;

public enum VelosEspMethods {
	StudyGetStudyStat("getStudyStatuses"),
	StudyGetStudySummary("getStudySummary"),
	StudyCalGetStudyCalendar("getStudyCalendar"),
	StudyCalGetStudyCalendarList("getStudyCalendarList"),
	StudyPatAddStudyPatientStatus("addStudyPatientStatus"),
	StudyPatGetStudyPatients("getStudyPatients"),
	StudyPatEnrollPatientToStudy("enrollPatientToStudy"),
	StudyCreation("studyCreation"),
	StudyCalendarCreation("studyCalendarCreation"),
	PatDemogSearchPatient("searchPatient"),
	CreatePatient("createPatient"),
	CreateStudy("createStudy"),
	StudyPatientStatus("studyPatientStatus"),
	SysAdminGetObjectInfoFromOID("getObjectInfoFromOID"),
	PatientDetails("getPatientDtails"),
	PatientStudyGetPatientStudyInfo("getPatientStudyInfo"),
	OutboundMessages("getOutboundMessages"),
	PatientStudyGetStudyPatientStatusHistory("getStudyPatientStatusHistory"),
	GroupUserService("groupUserService");
	private String key;
	
	VelosEspMethods(String key) {
		this.key = key;
	}
	
	@Override
	public String toString() {
		return key;
	}
}
