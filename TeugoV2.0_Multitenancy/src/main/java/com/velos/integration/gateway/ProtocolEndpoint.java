package com.velos.integration.gateway;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;
import org.apache.ws.commons.schema.utils.NodeNamespaceContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.integration.Message;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.xml.source.DomSourceFactory;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.velos.integration.dao.MessageDAO;
import com.velos.integration.espclient.VelosEspClient;
import com.velos.integration.espclient.VelosEspClientCamel;
import com.velos.integration.espclient.VelosEspMethods;
import com.velos.integration.mapping.EndpointKeys;
import com.velos.integration.mapping.MapUtil;
import com.velos.integration.mapping.ProtocolKeys;
import com.velos.integration.mapping.VelosKeys;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.Detail;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.ws.soap.SOAPFaultException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;

@Component
public class ProtocolEndpoint implements Processor {

	Properties prop=null;
	private static Logger logger = Logger.getLogger("epicLogger");   

	private static final String HARDCODE_ROOT = "1.3.6.1.4.1.12559.11.1.4.1.2";
	private static final String ALREADY_ENROLLED_MSG = "Patient is already enrolled to this Study";
	private static final String ALREADY_ASSOC_MSG = "Patient is already linked to this study";
	private static final String EMPTY_STR = "";
	private static final String ENROLL_PENDING_STR = "enroll_pending";
	private static final String PROCESS_STATE_EMPTY = "Process State value should not be an empty value";
	private static final String PROCESS_STATE_NOT_CORRECT = "Process State value is not correct";
	private static final String PROCESS_STATE_1055 = "Process State value can be only 1055 (enrolled)";
	private static final String DOB_VALUE_EMPTY ="DOB should not be an empty value";
	String localName = null;
	private String requestMsg = null;
	private String responseMsg = null;
	private MessageDAO messageDao = null;
	Object sourcexml = null;
	Properties prop1 = null;

	public ProtocolEndpoint() throws IOException{
		try {
			prop = new Properties();
			prop1 = new Properties();
			prop.load(this.getClass().getClassLoader()
					.getResourceAsStream("config.properties"));
			prop1.load(this.getClass().getClassLoader()
					.getResourceAsStream("epic.properties"));

		} catch (IOException e) {
			throw e;
		}
	}
	
	//Thread t = new Thread(() -> {System.out.println("Runnable....!");});
	
	@ServiceActivator
	public Source handleRequest(DOMSource source)
			throws Exception {
		System.out.println("handleRequest");
		String endpoint = EndpointKeys.Epic.toString();
		 localName = source.getNode().getLocalName();
		if (ProtocolKeys.RetrieveProtocol.toString().equals(localName)) {
			return handleRetrieveProtocol(source, endpoint);
		} else if (ProtocolKeys.RetrievePatient.toString().equals(localName)) {
			return handleRetrievePatient(source, endpoint);
		} else if (ProtocolKeys.EnrollPatientRequest.toString().equals(localName)) {
			return handleEnrollPatient(source, endpoint);
		} else if (ProtocolKeys.AlertProtocolState.toString().equals(localName)) {
			return handleAddStudyPatientStatus(source, endpoint);
		} else if (ProtocolKeys.RetrieveProtocolRequest.toString().equals(localName)) {
			return handleRetrieveProtocolRequest(source, endpoint);
		}
		return null;
	}

	synchronized public void process(Exchange exchange) throws Exception {
		System.out.println("process");
		//MessageDAO dao = new MessageDAO();
		//Source response = null;
		ClassPathXmlApplicationContext context = null;
		try{
			context = new ClassPathXmlApplicationContext("epic-context.xml");
			messageDao = (MessageDAO)context.getBean("messageDao");
			DOMSource request = (DOMSource) exchange.getIn().getBody();
			sourcexml = exchange.getIn().getBody();
			requestMsg = exchange.getIn().getBody(String.class);
			logger.info("\n\nRequest Message\n ==========================\n"+requestMsg);
			//System.out.println("\n\nRequest Message\n ==========================\n"+requestMsg);
			//response = handleRequest(request);
			// System.out.println("Response="+response);
			

			exchange.getOut().setBody(handleRequest(request));
		}finally{
			context.close();
		}
		//messageDao.saveMessageDetails("Inbound",requestMsg, responseMsg);
	}

	private Source handleEnrollPatient(DOMSource source, String endpoint) throws Exception {
		System.out.println("handleEnrollPatient");
		Map<VelosKeys, String> requestMap = new HashMap<VelosKeys, String>();
		requestMap.put(EndpointKeys.Endpoint, endpoint);
		NodeList nodeList = source.getNode().getChildNodes();
		for (int topX = 0; topX < nodeList.getLength(); topX++) {
			Node node = nodeList.item(topX);
			if (node.getLocalName() == null) { continue; }
			if (ProtocolKeys.Patient.toString().equals(node.getLocalName())) {
				NodeList childNodeList = node.getChildNodes();
				for (int cX = 0; cX < childNodeList.getLength(); cX++) {
					Node childNode = childNodeList.item(cX);
					System.out.println("childNode="+childNode.getLocalName());
					if (ProtocolKeys.CandidateId.toString().equals(childNode.getLocalName())) {
						System.out.println("candidateID");
						NamedNodeMap attributeMap = childNode.getAttributes();
						logger.info("attributeMap=="+attributeMap);
						if (attributeMap.getNamedItem(ProtocolKeys.Extension.toString()) != null) {
							Node extension = attributeMap.getNamedItem(ProtocolKeys.Extension.toString()); 
							//requestMap.put(ProtocolKeys.PatientID, extension.getNodeValue());
							requestMap.put(ProtocolKeys.PatientFacilityId, extension.getNodeValue());
							logger.info("PatientFacilityId is "+requestMap.get(ProtocolKeys.PatientFacilityId));
						}
					} else if (ProtocolKeys.Name.toString().equals(childNode.getLocalName())) {
						System.out.println("name");
						NodeList grandChildNodeList = childNode.getChildNodes();
						for (int gX = 0; gX < grandChildNodeList.getLength(); gX++) {
							Node grandChildNode = grandChildNodeList.item(gX);
							if (ProtocolKeys.Family.toString().equals(grandChildNode.getLocalName())) {
								requestMap.put(ProtocolKeys.LastName, grandChildNode.getTextContent());
								logger.info("last name is "+requestMap.get(ProtocolKeys.LastName));
							} else if (ProtocolKeys.Given.toString().equals(grandChildNode.getLocalName())) {
								requestMap.put(ProtocolKeys.FirstName, grandChildNode.getTextContent());
								System.out.println("first name is "+requestMap.get(ProtocolKeys.FirstName));
							}
						}
					} else if (ProtocolKeys.Dob.toString().toLowerCase().equals(childNode.getLocalName())) {
						System.out.println("dob");
						NamedNodeMap attributeMap = childNode.getAttributes();
						String dob = MapUtil.formatDateOfBirth(attributeMap.getNamedItem(ProtocolKeys.Value.toString()).getNodeValue());
						logger.info("dob=="+dob);
						Map<VelosKeys, Object> dataMap = new HashMap<VelosKeys, Object>();
						if (dob != null) {
							requestMap.put(ProtocolKeys.Dob, dob);
							logger.info("DOB is "+requestMap.get(ProtocolKeys.Dob));
						} else if(dob == null){
							dataMap.put(ProtocolKeys.FaultString, DOB_VALUE_EMPTY);
							throw createSOAPFaultException(dataMap);
						}
					}else if(ProtocolKeys.SubjectID.toString().equals(childNode.getLocalName())){
						System.out.println("SubjectID");
						NamedNodeMap attributeMap = childNode.getAttributes();
						System.out.println("attributeMap=="+attributeMap);
						if (attributeMap.getNamedItem(ProtocolKeys.Extension.toString()) != null) {
							Node extension = attributeMap.getNamedItem(ProtocolKeys.Extension.toString()); 
							requestMap.put(ProtocolKeys.SubjectID, extension.getNodeValue());
							logger.info("SubjectID is "+requestMap.get(ProtocolKeys.SubjectID));
						}
					}else if (ProtocolKeys.Address.toString().equals(childNode.getLocalName())) {
						System.out.println("Address");
						NodeList grandChildNodeList = childNode.getChildNodes();
						for (int gX = 0; gX < grandChildNodeList.getLength(); gX++) {
							Node grandChildNode = grandChildNodeList.item(gX);
							if(ProtocolKeys.StreetAddressLine.toString().equals(grandChildNode.getLocalName())) {
								requestMap.put(ProtocolKeys.StreetAddressLine, grandChildNode.getTextContent());
								logger.info("street address is "+requestMap.get(ProtocolKeys.StreetAddressLine));
							}else if (ProtocolKeys.City.toString().equals(grandChildNode.getLocalName())) {
								requestMap.put(ProtocolKeys.City, grandChildNode.getTextContent());
								logger.info("city is "+requestMap.get(ProtocolKeys.City));
							}else if (ProtocolKeys.State.toString().equals(grandChildNode.getLocalName())) {
								requestMap.put(ProtocolKeys.State, grandChildNode.getTextContent());
								logger.info("State is "+requestMap.get(ProtocolKeys.State));
							}else if (ProtocolKeys.PostalCode.toString().equals(grandChildNode.getLocalName())) {
								requestMap.put(ProtocolKeys.PostalCode, grandChildNode.getTextContent());
								logger.info("PostalCode is "+requestMap.get(ProtocolKeys.PostalCode));
							}else if (ProtocolKeys.Country.toString().equals(grandChildNode.getLocalName())) {
								requestMap.put(ProtocolKeys.Country, grandChildNode.getTextContent());
								logger.info("Country is "+requestMap.get(ProtocolKeys.Country));
							}
						}
					}
				}
			} else if (ProtocolKeys.Study.toString().equals(node.getLocalName())) {
				System.out.println("study");
				NodeList childNodeList0 = node.getChildNodes();
				for (int cX0 = 0; cX0 < childNodeList0.getLength(); cX0++) {
					Node childNode0 = childNodeList0.item(cX0);
					if (ProtocolKeys.Instantiation.toString().equals(childNode0.getLocalName())) {
						NodeList childNodeList1 = childNode0.getChildNodes();
						for (int cX1 = 0; cX1 < childNodeList1.getLength(); cX1++) {
							Node childNode1 = childNodeList1.item(cX1);
							if (ProtocolKeys.PlannedStudy.toString().equals(childNode1.getLocalName())) {
								NodeList childNodeList2 = childNode1.getChildNodes();
								for (int cX2 = 0; cX2 < childNodeList2.getLength(); cX2++) {
									Node childNode3 = childNodeList2.item(cX2);
									if (ProtocolKeys.Id.toString().equals(childNode3.getLocalName())) {
										NamedNodeMap attributeMap = childNode3.getAttributes();
										if (attributeMap.getNamedItem(ProtocolKeys.Extension.toString()) != null) {
											Node extension = attributeMap.getNamedItem(ProtocolKeys.Extension.toString());
											requestMap.put(ProtocolKeys.StudyNumber, extension.getNodeValue());
											requestMap.put(ProtocolKeys.StudyId, extension.getNodeValue());
											logger.info("Study number is "+requestMap.get(ProtocolKeys.StudyNumber));
										}
									}
								}
							}
						}
					}
				}
			}else if (ProtocolKeys.ProcessState.toString().equals(node.getLocalName())) {
				//String patStudyStat = EpicMaps.getEpicToVelosPatStudyStatMap().get(node.getFirstChild().getTextContent());
				Map<VelosKeys, Object> dataMap = new HashMap<VelosKeys, Object>();
				Node n = node.getFirstChild();
				String processState = "";
				if(n!=null){
					processState = n.getTextContent();
				}
				//logger.info("processState="+processState);
				if(processState.trim().equals("")){
					dataMap.put(ProtocolKeys.FaultString, PROCESS_STATE_EMPTY);
					throw createSOAPFaultException(dataMap);
				}

				if(processState!=null && !processState.trim().equals("1055")){
					dataMap.put(ProtocolKeys.FaultString, PROCESS_STATE_1055);
					throw createSOAPFaultException(dataMap);
				}

				String newState = prop1.getProperty(processState);
				requestMap.put(ProtocolKeys.ProcessState, newState);
				logger.info("ProcessState is "+requestMap.get(ProtocolKeys.ProcessState));
			}
		}

		// Call Velos eSP
		VelosEspClient client = new VelosEspClient();
		EnrollPatientRequestOutput output = new EnrollPatientRequestOutput();
		Map<VelosKeys, String> resultMap = new HashMap<VelosKeys, String>();
		logger.info("call PatDemogSearchPatient starts");
		// search for patient by patient ID, First name, Last name, and DOB in request match with those in search result
		Map<VelosKeys, Object> dataMap = client.handleRequest(VelosEspMethods.PatDemogSearchPatient, requestMap);
		logger.info("call PatDemogSearchPatient ends");
		 if(dataMap ==null) {
			dataMap = new HashMap<VelosKeys, Object>();
				dataMap.put(ProtocolKeys.FaultString,"Please check ESP server up or down....!");
				dataMap.put(ProtocolKeys.ModuleName,localName);
				dataMap.put(ProtocolKeys.StudyNumber, requestMap.get(ProtocolKeys.StudyNumber));
				dataMap.put(ProtocolKeys.PatientId, requestMap.get(ProtocolKeys.PatientFacilityId));
				throw createSOAPFaultException(dataMap);
			}
		
		boolean patientNotFound = false;
		if (dataMap.get(ProtocolKeys.FaultString) != null && (dataMap.get(ProtocolKeys.FaultString)).equals(ProtocolKeys.PatientNotFoundMsg.toString())) {
			//throw createSOAPFaultException(dataMap);
			patientNotFound = true;
		}else if(dataMap.get(ProtocolKeys.FaultString) != null && (dataMap.get(ProtocolKeys.FaultString)).equals(ProtocolKeys.MultiplePatientsFoundMsg.toString())){
			throw createSOAPFaultException(dataMap);
		}else{
			//GET FROM DATAMAP
			//request
			requestMap.put(ProtocolKeys.PatientId, (String)dataMap.get(ProtocolKeys.PatientID)); // ID to Id
			requestMap.put(ProtocolKeys.OID, (String)dataMap.get(ProtocolKeys.OID));
			requestMap.put(ProtocolKeys.FirstName, (String)dataMap.get(ProtocolKeys.FirstName));
			requestMap.put(ProtocolKeys.LastName, (String)dataMap.get(ProtocolKeys.LastName));
			String dob = MapUtil.nullToEmpty((String)dataMap.get(ProtocolKeys.Dob));
			dob = dob.replaceAll("/", EMPTY_STR);
			dob = MapUtil.formatDateOfBirth(dob);
			requestMap.put(ProtocolKeys.Dob, dob);
			//response
			resultMap.put(ProtocolKeys.PatientId, (String)dataMap.get(ProtocolKeys.PatientID));
			resultMap.put(ProtocolKeys.FirstName, (String)dataMap.get(ProtocolKeys.FirstName));
			resultMap.put(ProtocolKeys.LastName, (String)dataMap.get(ProtocolKeys.LastName));
			dob = MapUtil.nullToEmpty((String)dataMap.get(ProtocolKeys.Dob));
			resultMap.put(ProtocolKeys.Dob, dob.replaceAll("/", EMPTY_STR));
		}
		logger.info("call create patient and demographics starts ");
		System.out.println("call create patient and demographics starts ");
		if (patientNotFound) {
			dataMap = client.handleRequest(VelosEspMethods.CreatePatient, requestMap);

			if(dataMap.get(ProtocolKeys.FaultString) != null){
				throw createSOAPFaultException(dataMap);
			}else{
				String oid = (String)dataMap.get(ProtocolKeys.OID);
				logger.info("OID from createPatient----"+oid);
				requestMap.put(ProtocolKeys.OID, (String)dataMap.get(ProtocolKeys.OID));

				dataMap = client.handleRequest(VelosEspMethods.PatDemogSearchPatient, requestMap);
				//GET FROM DATAMAP
				//request
				requestMap.put(ProtocolKeys.PatientId, (String)dataMap.get(ProtocolKeys.PatientID)); // ID to Id
				requestMap.put(ProtocolKeys.OID, (String)dataMap.get(ProtocolKeys.OID));
				logger.info("OID from searchPatient----"+dataMap.get(ProtocolKeys.OID));
				requestMap.put(ProtocolKeys.FirstName, (String)dataMap.get(ProtocolKeys.FirstName));
				requestMap.put(ProtocolKeys.LastName, (String)dataMap.get(ProtocolKeys.LastName));
				String dob = MapUtil.nullToEmpty((String)dataMap.get(ProtocolKeys.Dob));
				dob = dob.replaceAll("/", EMPTY_STR);
				dob = MapUtil.formatDateOfBirth(dob);
				requestMap.put(ProtocolKeys.Dob, dob);
				//response
				resultMap.put(ProtocolKeys.PatientId, (String)dataMap.get(ProtocolKeys.PatientID));
				resultMap.put(ProtocolKeys.FirstName, (String)dataMap.get(ProtocolKeys.FirstName));
				resultMap.put(ProtocolKeys.LastName, (String)dataMap.get(ProtocolKeys.LastName));
				dob = MapUtil.nullToEmpty((String)dataMap.get(ProtocolKeys.Dob));
				resultMap.put(ProtocolKeys.Dob, dob.replaceAll("/", EMPTY_STR));

			}
		}
		System.out.println("call createpatient and demographics ends");
		logger.info("call createpatient and demographics ends");    	
		resultMap.put(ProtocolKeys.PatientFacilityId, MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.PatientFacilityId)));
		resultMap.put(ProtocolKeys.SiteName, MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.SiteName)));
		resultMap.put(ProtocolKeys.StudyNumber, MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.StudyNumber)));
		//rk starts
		resultMap.put(ProtocolKeys.StreetAddressLine, MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.StreetAddressLine)));
		resultMap.put(ProtocolKeys.City, MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.City)));
		resultMap.put(ProtocolKeys.State, MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.State)));
		resultMap.put(ProtocolKeys.PostalCode, MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.PostalCode)));
		resultMap.put(ProtocolKeys.Country, MapUtil.nullToEmpty(requestMap.get(ProtocolKeys.Country)));
		resultMap.put(ProtocolKeys.ProcessState, "Active");
		//rk ends

		// call enroll patient to study
		//requestMap.put(ProtocolKeys.PatientId, requestMap.get(ProtocolKeys.PatientID));
		dataMap = client.handleRequest(VelosEspMethods.StudyPatEnrollPatientToStudy, requestMap);
		logger.info("error=="+dataMap.get(ProtocolKeys.FaultString));
		if (dataMap.get(ProtocolKeys.FaultString) != null) {
			/*if (ALREADY_ENROLLED_MSG.equals(dataMap.get(ProtocolKeys.FaultString))) {
    			dataMap.put(ProtocolKeys.FaultString, ALREADY_ASSOC_MSG);
    		}*/
			//TODO
			throw createSOAPFaultException(dataMap);
		}else{
			//String completedAction = (String)dataMap.get(ProtocolKeys.CompletedAction);
			//if (completedAction != null) { completedAction = ENROLL_PENDING_STR; }
			//resultMap.put(ProtocolKeys.CompletedAction, MapUtil.nullToEmpty(completedAction));

			//call getStudyPatientStatusHistory
			System.out.println("call getStudyPatientStatusHistory starts");
			dataMap = client.handleRequest(VelosEspMethods.PatientStudyGetStudyPatientStatusHistory, requestMap);
			System.out.println("call getStudyPatientStatusHistory ends");
			String studyStatus = (String)dataMap.get(ProtocolKeys.StudyPatStatus);
			logger.info("studyStatus="+studyStatus);
			resultMap.put(ProtocolKeys.CompletedAction, "1055");
			String studyPatOID=(String)dataMap.get(ProtocolKeys.OID);
			requestMap.put(ProtocolKeys.StudyPatOID,studyPatOID);
			Map<VelosKeys, Object> dataMap2 = client.handleRequest(VelosEspMethods.StudyPatientStatus,requestMap);
			String patStudyID=(String) dataMap2.get(ProtocolKeys.StudyPatId);
			logger.info("*****StudyPatientID***************>"+patStudyID);
			resultMap.put(ProtocolKeys.StudyPatId,patStudyID);
		}

		String xml = output.generateOutput(resultMap);
		responseMsg = xml;
		messageDao.saveMessageDetails("Inbound",requestMsg, responseMsg);
		return new DomSourceFactory().createSource(xml);
	}

	private Source handleAddStudyPatientStatus(DOMSource source, String endpoint) throws Exception {
		Map<VelosKeys, String> requestMap = new HashMap<VelosKeys, String>();
		requestMap.put(EndpointKeys.Endpoint, endpoint);
		NodeList nodeList = source.getNode().getChildNodes();

		for (int topX = 0; topX < nodeList.getLength(); topX++) {
			Node node = nodeList.item(topX);
			if (node.getLocalName() == null) { continue; }
			if (ProtocolKeys.Patient.toString().equals(node.getLocalName())) {
				NodeList childNodeList = node.getChildNodes();
				for (int cX = 0; cX < childNodeList.getLength(); cX++) {
					Node childNode = childNodeList.item(cX);
					if (ProtocolKeys.CandidateId.toString().equals(childNode.getLocalName())) {
						NamedNodeMap attributeMap = childNode.getAttributes();
						/*if (attributeMap.getNamedItem(ProtocolKeys.Root.toString()) != null) {
    						if (HARDCODE_ROOT.equals(attributeMap.getNamedItem(ProtocolKeys.Root.toString()).getTextContent())){
	    						if (attributeMap.getNamedItem(ProtocolKeys.Extension.toString()) != null) {
	        						Node extension = attributeMap.getNamedItem(ProtocolKeys.Extension.toString()); 
	        						requestMap.put(ProtocolKeys.PatientID, extension.getNodeValue());
	        						System.out.println("patient Id is "+requestMap.get(ProtocolKeys.PatientID));
	        					}
    						}
    					}*/

						if (attributeMap.getNamedItem(ProtocolKeys.Extension.toString()) != null) {
							Node extension = attributeMap.getNamedItem(ProtocolKeys.Extension.toString()); 
							//requestMap.put(ProtocolKeys.PatientID, extension.getNodeValue());
							requestMap.put(ProtocolKeys.PatientFacilityId, extension.getNodeValue());
							logger.info("PatientFacilityId is "+requestMap.get(ProtocolKeys.PatientFacilityId));
						}
						//subject Id added by surendra 23Feb2017
					}else if (ProtocolKeys.SubjectID.toString().equals(childNode.getLocalName())) {
						logger.info("************SubJect ID Parsing *************************");
						NamedNodeMap attributeMap = childNode.getAttributes();
						String SubjectID = "";

						if (attributeMap.getNamedItem(ProtocolKeys.Extension.toString()) != null) {
							Node extension = attributeMap.getNamedItem(ProtocolKeys.Extension.toString()); 
							//requestMap.put(ProtocolKeys.PatientID, extension.getNodeValue());
							SubjectID = extension.getNodeValue();
							requestMap.put(ProtocolKeys.SubjectID,SubjectID);
							logger.info("SubjectID is "+requestMap.get(ProtocolKeys.SubjectID));
						}

						Map<VelosKeys, Object> dataMap = new HashMap<VelosKeys, Object>();

						/*if(SubjectID.trim().equals("")){
    	    				dataMap.put(ProtocolKeys.FaultString,SUBJECT_ID);
    	    				throw createSOAPFaultException(dataMap);
    		    		}*/

						/*String patStudyStat= prop.getProperty(SubjectID);
    		    		if(patStudyStat==null){
    		    			dataMap.put(ProtocolKeys.FaultString, PROCESS_STATE_NOT_CORRECT);
    	    				throw createSOAPFaultException(dataMap);
    		    		}*/
						requestMap.put(ProtocolKeys.SubjectID, SubjectID);
						logger.info("New PatStudyID/SubjectID is "+requestMap.get(ProtocolKeys.SubjectID));
					} else if (ProtocolKeys.Name.toString().equals(childNode.getLocalName())) {
						NodeList grandChildNodeList = childNode.getChildNodes();
						for (int gX = 0; gX < grandChildNodeList.getLength(); gX++) {
							Node grandChildNode = grandChildNodeList.item(gX);
							if (ProtocolKeys.Family.toString().equals(grandChildNode.getLocalName())) {
								requestMap.put(ProtocolKeys.LastName, grandChildNode.getTextContent());
								logger.info("last name is "+requestMap.get(ProtocolKeys.LastName));
							} else if (ProtocolKeys.Given.toString().equals(grandChildNode.getLocalName())) {
								requestMap.put(ProtocolKeys.FirstName, grandChildNode.getTextContent());
								logger.info("first name is "+requestMap.get(ProtocolKeys.FirstName));
							}
						}
					} 
					else if (ProtocolKeys.Dob.toString().toLowerCase().equals(childNode.getLocalName())) {
						NamedNodeMap attributeMap = childNode.getAttributes();
						String dob = MapUtil.formatDateOfBirth(attributeMap.getNamedItem(
								ProtocolKeys.Value.toString()).getNodeValue());
						if (dob != null) {
							requestMap.put(ProtocolKeys.Dob, dob);
							logger.info("DOB is "+requestMap.get(ProtocolKeys.Dob));
						}
					}
				}
			} else if (ProtocolKeys.Study.toString().equals(node.getLocalName())) {
				NodeList childNodeList0 = node.getChildNodes();
				for (int cX0 = 0; cX0 < childNodeList0.getLength(); cX0++) {
					Node childNode0 = childNodeList0.item(cX0);
					if (ProtocolKeys.Instantiation.toString().equals(childNode0.getLocalName())) {
						NodeList childNodeList1 = childNode0.getChildNodes();
						for (int cX1 = 0; cX1 < childNodeList1.getLength(); cX1++) {
							Node childNode1 = childNodeList1.item(cX1);
							if (ProtocolKeys.PlannedStudy.toString().equals(childNode1.getLocalName())) {
								NodeList childNodeList2 = childNode1.getChildNodes();
								for (int cX2 = 0; cX2 < childNodeList2.getLength(); cX2++) {
									Node childNode3 = childNodeList2.item(cX2);
									if (ProtocolKeys.Id.toString().equals(childNode3.getLocalName())) {
										NamedNodeMap attributeMap = childNode3.getAttributes();
										if (attributeMap.getNamedItem(ProtocolKeys.Extension.toString()) != null) {
											Node extension = attributeMap.getNamedItem(ProtocolKeys.Extension.toString());
											requestMap.put(ProtocolKeys.StudyNumber, extension.getNodeValue());
											requestMap.put(ProtocolKeys.StudyId, extension.getNodeValue());
											logger.info("Study number is "+requestMap.get(ProtocolKeys.StudyNumber));
										}
									}
								}
							}
						}
					}
				}
			} else if (ProtocolKeys.ProcessState.toString().equals(node.getLocalName())) {
				//String patStudyStat = EpicMaps.getEpicToVelosPatStudyStatMap().get(node.getFirstChild().getTextContent());
				//String patStudyStat = prop.getProperty(node.getFirstChild().getTextContent());
				String processState = "";
				Node n = node.getFirstChild();
				if(n!=null){
					processState =n.getTextContent();
					System.out.println("Process State =====>"+processState);
				}
				Map<VelosKeys, Object> dataMap = new HashMap<VelosKeys, Object>();
				if(processState.trim().equals("")){
					dataMap.put(ProtocolKeys.FaultString, PROCESS_STATE_EMPTY);
					throw createSOAPFaultException(dataMap);
				}

				String patStudyStat= prop1.getProperty(processState);
				if(patStudyStat==null){
					dataMap.put(ProtocolKeys.FaultString, PROCESS_STATE_NOT_CORRECT);
					throw createSOAPFaultException(dataMap);
				}
				requestMap.put(ProtocolKeys.StudyPatStatus, patStudyStat);
				logger.info("New PatStudyStat is "+requestMap.get(ProtocolKeys.StudyPatStatus));
			}
		}

		// Call Velos eSP
		VelosEspClient client = new VelosEspClient();
		AlertProtocolStateOutput output =null;
		output = new AlertProtocolStateOutput();
		Map<VelosKeys, String> resultMap =null;
		resultMap = new HashMap<VelosKeys, String>();
		
		resultMap.put(ProtocolKeys.SiteName, requestMap.get(ProtocolKeys.SiteName));
		resultMap.put(ProtocolKeys.StudyNumber, requestMap.get(ProtocolKeys.StudyNumber));
		resultMap.put(ProtocolKeys.PatientId, requestMap.get(ProtocolKeys.PatientFacilityId));
		
		// search for patient by patient ID, First name, Last name, and DOB in request match with those in search result
		Map<VelosKeys, Object> dataMap = client.handleRequest(VelosEspMethods.PatDemogSearchPatient, requestMap);
		System.out.println("FalutString ==========>"+ProtocolKeys.FaultString);
		if(dataMap !=null) {
			if (dataMap.get(ProtocolKeys.FaultString) != null) {
				dataMap.put(ProtocolKeys.ModuleName,localName);
				dataMap.put(ProtocolKeys.StudyNumber, requestMap.get(ProtocolKeys.StudyNumber));
				dataMap.put(ProtocolKeys.PatientId, resultMap.get(ProtocolKeys.PatientId));
				throw createSOAPFaultException(dataMap);
			}
		}else if(dataMap ==null) {
			dataMap = new HashMap<VelosKeys, Object>();
				dataMap.put(ProtocolKeys.FaultString,"Please check ESP server up or down....!");
				dataMap.put(ProtocolKeys.ModuleName,localName);
				dataMap.put(ProtocolKeys.StudyNumber, resultMap.get(ProtocolKeys.StudyNumber));
				dataMap.put(ProtocolKeys.PatientId, resultMap.get(ProtocolKeys.PatientId));
				throw createSOAPFaultException(dataMap);
			}
		
		
		resultMap.put(ProtocolKeys.PatientId, (String)dataMap.get(ProtocolKeys.PatientID));
		resultMap.put(ProtocolKeys.FirstName, (String)dataMap.get(ProtocolKeys.FirstName));
		resultMap.put(ProtocolKeys.LastName, (String)dataMap.get(ProtocolKeys.LastName));
		String dob = MapUtil.nullToEmpty((String)dataMap.get(ProtocolKeys.Dob));
		resultMap.put(ProtocolKeys.Dob, dob.replaceAll("/", EMPTY_STR));
		//request map
		requestMap.put(ProtocolKeys.PatientId, (String)dataMap.get(ProtocolKeys.PatientID)); // ID to Id
		requestMap.put(ProtocolKeys.OID, (String)dataMap.get(ProtocolKeys.OID));
		//call getStudyPatientStatusHistory
		//dataMap = client.handleRequest(VelosEspMethods.PatientStudyGetStudyPatientStatusHistory, requestMap);
		//String studyPatOID=(String)dataMap.get(ProtocolKeys.OID);
		//requestMap.put(ProtocolKeys.StudyPatOID,studyPatOID);

		//call getStudyPatientStatus
		//dataMap = client.handleRequest(VelosEspMethods.StudyPatientStatus,requestMap);
		//String patStudyID=(String) dataMap.get(ProtocolKeys.StudyPatId);
		//requestMap.put(ProtocolKeys.StudyPatId, patStudyID);

		// call Add patient study status
		//requestMap.put(ProtocolKeys.PatientId, (String)dataMap.get(ProtocolKeys.PatientID)); // ID to Id
		dataMap = client.handleRequest(VelosEspMethods.StudyPatAddStudyPatientStatus, requestMap);
		System.out.println("FalutString ==========>"+ProtocolKeys.FaultString);
		if (dataMap.get(ProtocolKeys.FaultString) != null) {
			if (ALREADY_ENROLLED_MSG.equals(dataMap.get(ProtocolKeys.FaultString))) {
				dataMap.put(ProtocolKeys.FaultString, ALREADY_ASSOC_MSG);
			}else {
				dataMap.put(ProtocolKeys.FaultString,"Add Study patient status issues Please check with Velos...!");
			}
			throw createSOAPFaultException(dataMap);
		}
		String completedAction = (String)dataMap.get(ProtocolKeys.CompletedAction);
		if (completedAction != null) { completedAction = requestMap.get(ProtocolKeys.StudyPatStatus); }
		resultMap.put(ProtocolKeys.CompletedAction, MapUtil.nullToEmpty(completedAction));
		String xml = output.generateOutput(resultMap);
		responseMsg = xml;
		messageDao.saveMessageDetails("Inbound", requestMsg, responseMsg);
		logger.info("\n\nResponse Xml\n==============================\n"+responseMsg);
		return new DomSourceFactory().createSource(xml);
	}
	public SOAPFaultException createSOAPFaultException(Map<VelosKeys, Object> dataMap) throws Exception {
		System.out.println("*************SoapFaultException*********************");
		SOAPFaultException sfe = null;
		MessageFactory messageFactory;
		try {
			messageFactory = MessageFactory.newInstance();
			SOAPMessage message = messageFactory.createMessage();
			SOAPFault soapFault = message.getSOAPBody().addFault();
			SOAPPart sp = message.getSOAPPart();
			SOAPEnvelope se = sp.getEnvelope();
			Name qname = se.createName("Client", null, SOAPConstants.URI_NS_SOAP_1_2_ENVELOPE);
			soapFault.setFaultCode(qname);
			soapFault.setFaultString((String)dataMap.get(ProtocolKeys.FaultString));
			Detail detail = soapFault.addDetail();
			detail.setTextContent((String)dataMap.get(ProtocolKeys.FaultString));
			sfe = new SOAPFaultException(soapFault);
			responseMsg = sfe.getMessage();
			messageDao.saveMessageDetails("Inbound",requestMsg, responseMsg,dataMap);
		} catch (SOAPException e) {
			logger.error("SoapExcepiton==>"+e.getMessage());
			e.printStackTrace();
		}finally {
			dataMap = null;
		}
		return sfe;
	}

	private Source handleRetrievePatient(DOMSource source, String endpoint) throws Exception {
		Map<VelosKeys, String> requestMap = new HashMap<VelosKeys, String>();
		requestMap.put(EndpointKeys.Endpoint, endpoint);

		System.out.println("Implement get study patients here");

		//        Map<VelosKeys, Object> dataMap = new HashMap<VelosKeys, Object>();
		//        dataMap.put(ProtocolKeys.FaultString, "study not found");
		//        if (1 == (2 - 1)) throw createSOAPFaultException(dataMap);

		String xml = "<RetrievePatientRequestResponse />";
		return new DomSourceFactory().createSource(xml);
	}

	/*
	 * In bound Study Creation 
	 * M.Surendra 24/01/2019
	 */
	private Source handleRetrieveProtocolRequest(DOMSource source, String endpoint) throws Exception {
		Map<VelosKeys, Object> resultMap = null;
		Map<Object,Object> requestMap = null;

		resultMap = new HashMap<VelosKeys, Object>();
		requestMap = new HashMap<Object,Object>();


		requestMap.put(EndpointKeys.Endpoint, endpoint);
		XPath xpath = XPathFactory.newInstance().newXPath();
		String title,text =null;
		XPathExpression expr =null;
		String expression = "/RetrieveProtocolDefRequest/protocolDef/plannedStudy/subjectOf/studyCharacteristic";
		expr = xpath.compile("//*[local-name()='query']/@extension");
		String studyNumber = expr.evaluate(source.getNode());
		requestMap.put("StudyNumber", studyNumber);
		System.out.println("Study Number ======>"+studyNumber);
		if(studyNumber == null || studyNumber.equals("")|| studyNumber.equals("?")){
			resultMap.put(ProtocolKeys.FaultString,"Study Number Should not be null");
			throw createSOAPFaultException(resultMap);	
		}
		expr = xpath.compile("//*[local-name()='title']");
		System.out.println("Title ======>"+ expr.evaluate(source.getNode()));
		title = expr.evaluate(source.getNode());
		requestMap.put("Title",title);
		if(title == null || title.equals("") || title.equals("?")){
			resultMap.put(ProtocolKeys.FaultString,"Title Should Not be Empty");
			throw createSOAPFaultException(resultMap);
		}
		expr = xpath.compile("//*[local-name()='text']");
		System.out.println("Text ======>"+ expr.evaluate(source.getNode()));
		text = expr.evaluate(source.getNode());
		if(text == null || text.equals("") ||text.equals("?")){
			resultMap.put(ProtocolKeys.FaultString,"Text Should Not be Empty");
			throw createSOAPFaultException(resultMap);
		}
		requestMap.put("Text",text);

		StringWriter writer=new StringWriter();
		StreamResult result=new StreamResult(writer);
		TransformerFactory tf=TransformerFactory.newInstance();
		Transformer transformer=tf.newTransformer();
		transformer.transform(source,result);
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		org.w3c.dom.Document doc = builder.parse(new InputSource(new StringReader(writer.toString())));  


		// String expression = "/RetrieveProtocolDefResponse/protocolDef/plannedStudy/subjectOf/studyCharacteristic";
		// NodeList nodeList =source.getNode().getChildNodes();

		NodeList nodeList =   (NodeList) xpath.compile(expression).evaluate(doc,XPathConstants.NODESET);

		System.out.println("NodeListLength ====>"+nodeList.getLength());
		String codes = null;
		codes = prop1.getProperty("create_study.subjfields").toString().trim();
		List<String> codeList = null;
		List<String> elementList = null ; 
		elementList = new ArrayList<String>();
		List<String> codelstFields = null;
		codeList = getPropertyList(codes);

		for (int i = 0; i < nodeList.getLength(); i++) {
			Node nNode = nodeList.item(i);
			System.out.println("\nCurrent Element :" + nNode.getNodeName()+"-------"+nNode.getChildNodes().getLength());
			NodeList nodlist= nNode.getChildNodes();
			logger.info("Nodes====->"+nodlist.item(1).getAttributes().item(0));
			logger.info("Nodes====#>"+nodlist.item(3).getAttributes().item(1));
			Node nod1 = nodlist.item(1).getAttributes().item(0);
			String code =  nod1.getNodeValue();
			elementList.add(code);
			Node nod2 = nodlist.item(3).getAttributes().item(1);
			String value = nod2.getNodeValue();
			if(codeList.contains(code)){
				if(value == null || value.equals("") || value.equals("?")){
					resultMap.put(ProtocolKeys.FaultString, code+" Should Not be Empty");
					throw createSOAPFaultException(resultMap);
				}
			}

			/*   ProtocolKeys codekey = ProtocolKeys.valueOf(code);
	               if(codekey == null){
	            		  resultMap.put(ProtocolKeys.FaultString,"Please Contact System Administrator to add new code "+code+" value");
	        	          throw createSOAPFaultException(resultMap);
	               }*/


			requestMap.put(code,value);
			requestMap.put("elementList",elementList);

		}

		VelosEspClient client = new VelosEspClient();
		List<String> keyList = client.getPropertyKeyList("esp_services.properties","select");
		for(String element : elementList){
			if(!keyList.contains(element)){
				resultMap.put(ProtocolKeys.FaultString,"Code "+element+" Does Not Exist Please Add In Interface....");
				throw   createSOAPFaultException(resultMap);
			}else if(keyList.contains(element)){

			}
		}

		String codelstSubTypeFields = prop1.getProperty("codelst_sutyp_fields");
		codelstFields = getPropertyList(codelstSubTypeFields);
		String mappedValue = null;
		for(String  keyCode : codelstFields)
		{  
			String mapValue = (String)requestMap.get(keyCode);
			System.out.println("Mapping Value ===>"+ mapValue);
			if( mapValue != null){

				mappedValue = prop1.getProperty(mapValue);
				System.out.println("Mapped Value ===>"+ mappedValue);
				if(mappedValue == null){
					resultMap.put(ProtocolKeys.FaultString,"Mapping Code "+mapValue+" Does Not Exist Please Add In Interface....");
					throw   createSOAPFaultException(resultMap);

				}
				requestMap.put(keyCode,mappedValue);
			}
		}


		System.out.println("RequestMap=====#>"+requestMap);
		Map<VelosKeys,Object> dataMap =   client.handleStudyRequest(VelosEspMethods.StudyCreation, requestMap);
		
		
		System.out.println("RequestMap====---=#>"+dataMap);
		if(dataMap == null || dataMap.isEmpty()) {
			 dataMap = new HashMap<VelosKeys, Object>();
				dataMap.put(ProtocolKeys.FaultString,"Please check ESP server up or down....!");
				dataMap.put(ProtocolKeys.ModuleName,localName);
				dataMap.put(ProtocolKeys.StudyNumber, requestMap.get("StudyNumber"));
				throw createSOAPFaultException(dataMap);
			}

		ArrayList<String> errorList = null; 
		System.out.println("Error List =======>"+dataMap.get(ProtocolKeys.ErrorList));
		if(dataMap.get(ProtocolKeys.ErrorList) != null){
			System.out.println("Error List =======>"+dataMap.get(ProtocolKeys.ErrorList));
			errorList = (ArrayList<String>) dataMap.get(ProtocolKeys.ErrorList);
		}
		if(errorList != null){
			String error = errorList.get(0);
			resultMap.put(ProtocolKeys.FaultString,error);
			throw createSOAPFaultException(resultMap);
		}else if(dataMap.get(ProtocolKeys.FaultString) != null){
			resultMap.put(ProtocolKeys.FaultString,resultMap.get(ProtocolKeys.FaultString));
			throw createSOAPFaultException(resultMap);
		}

		return source;

	}
	//Inbound Study Calendar Request
	private Source handleRetrieveProtocol(DOMSource source, String endpoint) throws Exception {
		Map<VelosKeys, String> requestMap = new HashMap<VelosKeys, String>();
		requestMap.put(EndpointKeys.Endpoint, endpoint);

		XPath xpath = XPathFactory.newInstance().newXPath();
		XPathExpression expr =null;
		expr = xpath.compile("//*[local-name()='query']/@extension");
		String studyNumberStr = expr.evaluate(source.getNode());
		requestMap.put(ProtocolKeys.StudyNumber, studyNumberStr);
		System.out.println("Study Number ======>"+studyNumberStr);
		RetrieveProtocolDefOutput output = new RetrieveProtocolDefOutput();
		expr = null;
		expr = xpath.compile("//*[local-name()='id']/@extension");
		String calendarName = expr.evaluate(source.getNode());
		System.out.println("Calendar Name =========>"+expr+"====>"+calendarName);
		requestMap.put(ProtocolKeys.CalendarName,calendarName);
		expr = xpath.compile("//*[local-name()='title']/@extension");
		String stdCalTitle = expr.evaluate(source.getNode());
		requestMap.put(ProtocolKeys.StudyCalenTitle,stdCalTitle);
		expr = xpath.compile("//*[local-name()='text']/@extension");
		requestMap.put(ProtocolKeys.StudyCalenText,expr.evaluate(source.getNode()));


		expr = xpath.compile("//*[local-name()='id']/@extension");

		System.out.println("Value ========>"+expr.evaluate(source.getNode()));


		// Call Velos eSP
		VelosEspClientCamel camelClient = new VelosEspClientCamel();
		VelosEspClient client = new VelosEspClient();
		Map<VelosKeys, Object> studyDataMap = client.handleRequest(VelosEspMethods.StudyGetStudySummary, requestMap);

		boolean studyNotfound = false;
		if (studyDataMap.get(ProtocolKeys.FaultString) != null && studyDataMap.get(ProtocolKeys.FaultString).toString().contains("Study not found")) {
			studyNotfound =true;
			throw createSOAPFaultException(studyDataMap);

		}


		Map<VelosKeys,Object> studyCalMap = parseStudyCalendarRequest(source,requestMap); 
		
		
			if(studyCalMap.get(ProtocolKeys.StudyResponse)!= null){
			
			if(studyCalMap.get(ProtocolKeys.StudyResponse).equals("AleartReceived")){
				
				RetrieveProtocolDefResponse   response = new RetrieveProtocolDefResponse();
				
				
				String xml = response.generateOutput(requestMap);
				
				messageDao.saveMessageDetails("Inbound", requestMsg, xml);
				logger.info("\n\nResponse Xml\n==============================\n"+responseMsg);
				
				return new DomSourceFactory().createSource(xml);
			}
		}
		

		if (studyDataMap.get(ProtocolKeys.FaultString) != null && studyDataMap.get(ProtocolKeys.FaultString).toString().contains("Study not found")) {
			
			messageDao.saveMessageDetails("Inbound", requestMsg, studyDataMap.get(ProtocolKeys.FaultString).toString());
			throw createSOAPFaultException(studyDataMap);

		}

		return source;

		/*		if(studyNotfound){
			Map<VelosKeys, Object> createStudyDataMap = client.handleRequest(VelosEspMethods.CreateStudy, requestMap);
		}


		Map<VelosKeys, Object> studyCalendarListDataMap = client.handleStudyCalendarListRequest(requestMap);



		if(studyCalendarListDataMap.get(ProtocolKeys.ErrorList) != null || studyCalendarListDataMap.get(ProtocolKeys.FaultString)!= null){
			ArrayList<String> errorList =  (ArrayList<String>) studyCalendarListDataMap.get(ProtocolKeys.ErrorList);
			if(errorList != null){
				String error = errorList.get(0);
				studyCalendarListDataMap.put(ProtocolKeys.FaultString,error);
				throw createSOAPFaultException(studyCalendarListDataMap);
			}else if(errorList == null){
				String faultString =  (String) studyCalendarListDataMap.get(ProtocolKeys.FaultString);
				if(faultString != null){
					throw createSOAPFaultException(studyCalendarListDataMap); 
				}
			}
		}
		@SuppressWarnings("unchecked")
		List<Map<VelosKeys, Object>> calendarIdList = (List<Map<VelosKeys, Object>>) studyCalendarListDataMap
		.get(ProtocolKeys.CalendarIdList);
		StringBuffer calendarSB = new StringBuffer();


		if(calendarIdList.size()>0 || calendarIdList != null){
		for (Map<VelosKeys, Object> calendarMap : calendarIdList) {
			requestMap.put(ProtocolKeys.CalendarId, String
					.valueOf(calendarMap.get(ProtocolKeys.CalendarId)));
			Map<VelosKeys, Object> calendarDataMap = client
					.handleStudyCalendarRequest(requestMap);
			@SuppressWarnings("unchecked")
			List<Map<VelosKeys, Object>> visitList = (List<Map<VelosKeys, Object>>) calendarDataMap
			.get(ProtocolKeys.VisitList);
			calendarMap.put(ProtocolKeys.CalendarDuration, calendarDataMap.get(ProtocolKeys.CalendarDuration));
			calendarMap.put(ProtocolKeys.CalendarDurUnit, calendarDataMap.get(ProtocolKeys.CalendarDurUnit));
			StringBuffer visitSB = new StringBuffer();
			StringBuffer visitForPart1SubSB = new StringBuffer();
			int visitSeq = 1;
			// First loop through visits to determine whether or not it is a day-zero calendar
			boolean isDayZeroCal = false;
			for (Map<VelosKeys, Object> visitMap : visitList) {
				if ((Integer) visitMap.get(ProtocolKeys.VisitDisplacement) == 0) {
					isDayZeroCal = true;
					break;
				}
			}
			for (Map<VelosKeys, Object> visitMap : visitList) {
				visitMap.put(ProtocolKeys.CalendarName,
						calendarMap.get(ProtocolKeys.CalendarName));
				int displacement = (Integer) visitMap.get(ProtocolKeys.VisitDisplacement);
				if (!isDayZeroCal) { --displacement; }
				Date displacementDate = addDays(getReferenceDate(), displacement);
				int visitWindowBefore = (Integer) visitMap.get(ProtocolKeys.VisitWindowBefore);
				int visitWindowAfter = (Integer) visitMap.get(ProtocolKeys.VisitWindowAfter);
				visitMap.put(
						ProtocolKeys.VisitWindowBeforeFormatted,
						formatDate(addDays(displacementDate, -1 * visitWindowBefore)));
				visitMap.put(
						ProtocolKeys.VisitWindowAfterFormatted,
						formatDate(addDays(displacementDate, visitWindowAfter)));
				visitMap.put(
						ProtocolKeys.VisitDisplacementFormatted,
						formatDate(addDays(getReferenceDate(), displacement)));
				visitSB.append(output.generateComponent4Part2(visitMap));
				visitForPart1SubSB.append(output
						.generateComponent4Part1SubTemplate(
								(String) calendarMap
								.get(ProtocolKeys.CalendarName),
								(String) visitMap
								.get(ProtocolKeys.VisitName),
								visitSeq++));
			}
			calendarSB.append(output.generateComponent4Part1(calendarMap,
					visitForPart1SubSB.toString()));
			calendarSB.append(visitSB.toString());
		}

	}	
		System.out.println(calendarSB);

		// Return XML response
		String xml = output.generateFinalOutput(studyDataMap, calendarSB.toString());
		return new DomSourceFactory().createSource(xml);*/
	}

	// Our test reference date with Epic is 2014-01-01
	private static Date getReferenceDate() {
		Calendar cal = GregorianCalendar.getInstance();
		cal.set(2014, 0, 1);
		return cal.getTime();
	}

	private static Date addDays(Date date, int days) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, days);
		return cal.getTime();
	}

	private static String formatDate(Date date) {
		SimpleDateFormat f = new SimpleDateFormat("yyyyMMdd");
		return f.format(date);
	}

	public List<String> getPropertyList(String propertyName){
		List <String> codeList = null;
		codeList = new ArrayList<String>();
		try{
			for(String cod :  propertyName.split(",")){
				codeList.add(cod);
			}
			return codeList;
		}finally{
			codeList =null;

		}


	}

	public Map<VelosKeys, Object> parseStudyCalendarRequest(DOMSource source, Map<VelosKeys, String> requestMap) throws Exception{

		Map<VelosKeys, Object> resultMap = null;
		StringWriter writer = null;
		StreamResult result = null;
		TransformerFactory tf= null;
		Transformer transformer = null;
		DocumentBuilderFactory factory = null;
		DocumentBuilder builder = null;
		org.w3c.dom.Document doc = null;
		NodeList nodeList = null;
		String codes = null;
		List<String> codeList = null;
		List<String> elementList = null ; 
		List<String> codelstFields = null;
		
		resultMap = new HashMap<VelosKeys, Object>();
		System.out.println("StudyCalendarResut Parsing********************************8");


		XPath xpath = XPathFactory.newInstance().newXPath();
		String title,text =null;
		XPathExpression expr =null;
		String expression = "/RetrieveProtocolDefRequest/protocolDef/plannedStudy/subjectOf/studyCharacteristic";
		writer=new StringWriter();
		result=new StreamResult(writer);
		tf=TransformerFactory.newInstance();
		transformer=tf.newTransformer();
		transformer.transform(source,result);
		factory = DocumentBuilderFactory.newInstance();
		builder = factory.newDocumentBuilder();

		doc = builder.parse(new InputSource(new StringReader(writer.toString()))); 
		
		 XPath xPath = XPathFactory.newInstance().newXPath();
		 List<String> visitList = new ArrayList<String>();
		//CalendarName+VisitName	
	String expression1 = "//RetrieveProtocolDefResponse/protocolDef/plannedStudy/component4[1]/timePointEventDefinition/component1[1]/timePointEventDefinition[1]/component1/timePointEventDefinition/id[local-name()='id']/@extension";
		
		NodeList idNodList = (NodeList) xPath.compile(expression1).evaluate(
				doc, XPathConstants.NODESET);
		for(int k=0 ; k<idNodList.getLength() ;k++){
			System.out.println("idextValue == "+ idNodList.item(k).getNodeValue());
			visitList.add(idNodList.item(k).getNodeValue());
			
		}
		List<String> eventVisitList = new ArrayList<String>();
		//Calendar Name + VisitName+EventName
		String expression2 = "//RetrieveProtocolDefResponse/protocolDef/plannedStudy[1]/component4/timePointEventDefinition/component1/timePointEventDefinition/id[local-name()='id']/@extension";	
		
		NodeList idNodList1 = (NodeList) xPath.compile(expression2).evaluate(
				doc, XPathConstants.NODESET);

		for(int k=0 ; k<idNodList1.getLength() ;k++){
			System.out.println("idextValue 2==> "+ idNodList1.item(k).getNodeValue());
			eventVisitList.add(idNodList1.item(k).getNodeValue());
		}
		//requestMap.
				Map<Object,Object> studyrequestMap = new HashMap<Object,Object>();
		String visitNames = stringSplit(visitList);
		
		//System.out.println("Visit Namessss======>"+visitNames);
		requestMap.put(ProtocolKeys.VisitList,visitNames);
		
		String eventNames =  eventVisitSplit(eventVisitList);
		//System.out.println("Event Visit Namessss======>"+eventNames);
		
		studyrequestMap.put("eventVistList",eventVisitList);
	
		requestMap.put(ProtocolKeys.EventList,eventNames);
		
		
		
		
		XPath xPath1 =  XPathFactory.newInstance().newXPath();


/*
		String expression3 = "/RetrieveProtocolDefResponse/protocolDef/plannedStudy/component4/timePointEventDefinition";
		NodeList nodeList1 = (NodeList) xPath1.compile(expression3).evaluate(
				doc, XPathConstants.NODESET);*/

	Map<String,Map> componentMap	= getComponentValues(doc);
		
		
		
		
		studyrequestMap.put("componentMap",componentMap);
		studyrequestMap.put("requestMap",requestMap);
		VelosEspClient client  = null ;
		client = new VelosEspClient();
		
		Map<VelosKeys, Object> studyDataMap =client.handleStudyRequest(VelosEspMethods.StudyCalendarCreation, studyrequestMap);
		//Map<VelosKeys, Object> studyDataMap = client.handleRequest(VelosEspMethods.StudyCalendarCreation, requestMap);
		
		if (studyDataMap.get(ProtocolKeys.FaultString) != null) {
			
			throw createSOAPFaultException(studyDataMap);

		}
		
		if(studyDataMap.get(ProtocolKeys.StudyResponse)!= null){
			
			if(studyDataMap.get(ProtocolKeys.StudyResponse).equals("AleartReceived")){
				
				return studyDataMap;
			}
		}
		
		return studyDataMap;
		

	}
	
	public String stringSplit(List<String> list){
		String temp = "";
		String vi="";
		int count =1;
		for(String visit : list){
			String [] vis  = visit.split("\\.");
			      int j = vis.length;
			      temp = vis[1];
			      if(j!=count){
			       vi = temp; 
			        vi = vi+"~";
			        count++;
			      }else if (j==count){
			    	vi = vi+temp;
			      }
		}
		
		return vi;
	}

	public String eventVisitSplit(List<String> eventVistlist){
		String temp = "";
		String vi="";
		String evenVist="";
		int count =1;
		//System.out.println("VisitList==>"+eventVistlist);
		List<String> eventVisitList = new ArrayList<String>();
		for(String visit : eventVistlist){
			String [] vis  = visit.split("\\.");
			      
			  int j = vis.length;
			  if(j>=3) {
			      temp = vis[1]+"~"+vis[2];
			      eventVisitList.add(temp);
			  }
		}
		
		int k=1;
		String vist="";
		int l = eventVisitList.size();
		for(String tmp : eventVisitList) {
			System.out.println("lenth=====>"+l);
			if(k ==1) {
				vist = tmp+"-";
			}else {
				if(l<=1) {
					vist = vist+tmp;
				}else {
				vist = vist+tmp+"-";
				}
			}
			l--;
			k++;
			
		}
		k=1;
		//System.out.println("Vist EVE =====>#"+vist);
		return vist;
	/*	
		for(String visit : eventVistlist){
			String [] vis  = visit.split("\\.");
			      int j = vis.length;
			      System.out.println("Length====>"+j + "count");
			      if(j>=3){
			      temp = vis[1]+"~"+vis[2];
			      if(j!=count){
			       vi = temp;
			        vi = vi+"-";
			      j=0;
			      }else{
			    	vi = vi+temp;
			    	j=0;
			      }
			      }
			      System.out.println("Vist&Event =========>"+vi);
			      count = count+1;
		}*/
		
		
	}
	
	/**
	 *  To get retrieve protocol component element values
	 * @param nodeList
	 * @param doc
	 * @return
	 * @throws XPathExpressionException
	 */
	public Map<String,Map>	getComponentValues(Document doc) throws XPathExpressionException{
		
		String eventCode = null;
		String eventValue = null;
		Map<String,Map> comp1Map = new HashMap<String,Map>();
		Map<String,Object>  comp1ValMap = new HashMap<String,Object>();
		XPath xPath = XPathFactory.newInstance().newXPath();

		String expression3 = "//RetrieveProtocolDefResponse/protocolDef/plannedStudy[1]/component4/timePointEventDefinition/component1";
		NodeList idNodList2 = (NodeList) xPath.compile(expression3).evaluate(
				doc, XPathConstants.NODESET);
		
		System.out.println(idNodList2.getLength());
		String eventVisitCalName=null;
		String sequenceNumber = null;
		String cptCode = null;
		System.out.println("ID NODEList===>"+idNodList2.getLength());
		
		for(int k=0 ; k<idNodList2.getLength() ;k++){
			NodeList  timeNodList = idNodList2.item(k).getChildNodes();

			for(int j=0; j<timeNodList.getLength() ; j++){
				
				if(timeNodList.item(j).getNodeName().equalsIgnoreCase("urn:sequenceNumber")){
					NamedNodeMap attributeNode = timeNodList.item(j).getAttributes();
					sequenceNumber = attributeNode.getNamedItem("value").getNodeValue();
					//System.out.println("SequenceNumber ==>"+sequenceNumber);
				}
				if(timeNodList.item(j).getNodeName().equals("urn:timePointEventDefinition")){
					NodeList chnodes = timeNodList.item(j).getChildNodes();
					for(int z=0; z<chnodes.getLength();z++){
						
						if(chnodes.item(z).getNodeName().equals("urn:id")){
							String extension = chnodes.item(z).getAttributes().getNamedItem("extension").getNodeValue();

							if(extension.contains("."))
							//	System.out.println("ID extension =====>"+chnodes.item(z).getAttributes().getNamedItem("extension").getNodeValue());
							    eventVisitCalName = chnodes.item(z).getAttributes().getNamedItem("extension").getNodeValue();
						}
						if(chnodes.item(z).getNodeName().equalsIgnoreCase("urn:component2")){
							NodeList component2Node = chnodes.item(z).getChildNodes();
							for(int h=0 ; h<component2Node.getLength() ; h++){
								//System.out.println("component2 nodes ==>"+component2Node.item(h));
								if(component2Node.item(h).getNodeName().equals("urn:procedure")){
									NodeList comp2ChildNodes = component2Node.item(h).getChildNodes();
									for(int l=0;l<comp2ChildNodes.getLength();l++){
										//System.out.println("Child Nodes====>"+comp2ChildNodes.item(l).getNodeName());
										if(comp2ChildNodes.item(l).getNodeName().equals("urn:code")){
											//System.out.println("Code Value ===>"+comp2ChildNodes.item(l).getAttributes().getNamedItem("urn:code").getNodeValue());
											cptCode = comp2ChildNodes.item(l).getAttributes().getNamedItem("value").getNodeValue(); 
											System.out.println("**********");
										}
										
									}
								}
							}

						}if(chnodes.item(z).getNodeName().equalsIgnoreCase("urn:subjectOf")){
							
							NodeList subjectOfnodes = chnodes.item(z).getChildNodes();
							
							for(int s = 0 ;s < subjectOfnodes.getLength() ; s++){
								
							if(	subjectOfnodes.item(s).getNodeName().equalsIgnoreCase("urn:timePointEventCharacteristic")){
								NodeList timepointeventnodes = subjectOfnodes.item(s).getChildNodes();
								   for(int r = 0 ;r< timepointeventnodes.getLength() ; r++){
									 
								if(timepointeventnodes.item(r).getNodeName().equals("urn:code")){
									
									eventCode = timepointeventnodes.item(r).getAttributes().getNamedItem("code").getNodeValue();
									System.out.println("EventCode =====>"+ eventCode);
								}if(timepointeventnodes.item(r).getNodeName().equals("urn:value")){
									eventValue = 	timepointeventnodes.item(r).getAttributes().getNamedItem("value").getNodeValue();
									
									System.out.println("eventValue ====>"+ eventValue);
								}
								   }
								}
							}
							
							
						}
					}


				}
								if(eventVisitCalName != null && eventVisitCalName.contains(".")){
				            
				 				comp1ValMap = new HashMap<String,Object>();
								comp1ValMap.put("sequenceNumber",sequenceNumber);
								comp1ValMap.put("cptCode",cptCode);
								comp1ValMap.put("bill_modifier",eventValue);
								comp1Map.put(eventVisitCalName,comp1ValMap);
								}eventVisitCalName = null;
							
								
			}


		}
				System.out.println("Comp1ValueMap====#>"+comp1Map);
						return comp1Map;

	}
}

