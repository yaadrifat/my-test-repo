package com.velos.integration.gateway;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.xml.soap.Detail;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;
import javax.xml.ws.soap.SOAPFaultException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.Logger;
import org.springframework.integration.xml.source.DomSourceFactory;

import com.velos.integration.dao.MessageDAO;
import com.velos.integration.espclient.VelosEspClient;
import com.velos.integration.espclient.VelosEspClientCamel;
import com.velos.integration.espclient.VelosEspMethods;
import com.velos.integration.mapping.EndpointKeys;
import com.velos.integration.mapping.ProtocolKeys;
import com.velos.integration.mapping.VelosKeys;

public class RetrieveProtocolDefOutput {
	private static Logger logger = Logger.getLogger("epicLogger");
	private String responseMsg = null;
	private MessageDAO messageDao = null;
	public static String root ;
	public static  String root1;
	public static String codesystem;
	 
	
     static {
    	 Properties  prop = new Properties();
		try {
			logger.info("**************Retrieve Protocol Constructor********");
			prop.load(RetrieveProtocolDefOutput.class.getClassLoader()
					.getResourceAsStream("epic.properties"));
			root = prop.getProperty("calendar_root").trim();
			root1 = prop.getProperty("calendar_root1").trim();
			codesystem = prop.getProperty("calendar_codesystem").trim(); 
			logger.info("Values=====>"+root+root1+codesystem);
		} catch (IOException e) {
			logger.info("Getting root vale from properties"+e.getMessage());
		}finally {
			prop = null;
		}
     }
	private  String outputTemplate =
			"<RetrieveProtocolDefResponse xmlns:ns2=\"urn:ihe:qrph:rpe:2009\" xmlns=\"urn:hl7-org:v3\">"+
					"<query extension=\"%s\" root=\""+ root+"\""+"/>"+
					"<protocolDef>"+
					"<plannedStudy classCode=\"CLNTRL\" moodCode=\"DEF\" ITSVersion=\"XML_1.0\" "+
					"xmlns=\"urn:hl7-org:v3\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "+
					"xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">"+
				    "<templateId><item root=\""+root+"\""+"/></templateId>"+
				    "<id root=\""+root+"\""+ "extension=\"%s\"/>"+
				    "<title>%s</title>"+
				    "<text>%s</text>"+
				    "%s"+ // <= Component4 added here
				    "%s"+ // <= SubjectOf added here
				    "</plannedStudy>"+
				    "</protocolDef>"+
					"</RetrieveProtocolDefResponse>";
	
	private  String component4Part1Template =
			"<!-- Timing events, one component4 for each calendar in the study-->"+
					"<component4 typeCode=\"COMP\">"+
					"<timePointEventDefinition classCode=\"CTTEVENT\" moodCode=\"DEF\">"+
					"<templateId><item root=\""+root1+"\""+"/></templateId>"+
					"<id root=\""+root1+"\""+" extension=\"%s\"/>"+
					"<code code=\"CELL\" codeSystem=\""+root1+"\""+">"+
					"<displayName value=\"%s\"/>"+
					"</code>"+
					"<component1 typeCode=\"COMP\">"+
					"<splitCode code=\"\"/>"+
					"<joinCode code=\"\"/>"+
					"<timePointEventDefinition classCode=\"CTTEVENT\" moodCode=\"DEF\">"+
					"<id root=\""+root1+"\""+"extension=\"%s\"/>"+
					"<code code=\"CYCLE\" codeSystem=\""+root1+"\""+">"+
					"<displayName value=\"%s\"/>"+
					"</code>"+
					"%s"+ // <= sub part added here
					"<effectiveTime>"+
					"<low value=\"%s\"/>"+
					"<high value=\"%s\"/>"+
					"</effectiveTime>"+
					"</timePointEventDefinition>"+
					"</component1>"+
					"</timePointEventDefinition>"+
					"</component4>";
	
	private  String component4Part1SubTemplate =
			"<component1 typeCode=\"COMP\">"+
					"<sequenceNumber value=\"%s\"/>"+
					"<timePointEventDefinition classCode=\"CTTEVENT\" moodCode=\"DEF\">"+
					"<id root=\""+root1+"\""+" extension=\"%s.%s\"/>"+
					"</timePointEventDefinition>"+
					"</component1>";
	
	private  String component4Part2Template =
			"<!--Activity Events, add a component4 for each visit in the calendar-->"+
					"<component4 typeCode=\"COMP\">"+
					"<timePointEventDefinition classCode=\"CTTEVENT\" moodCode=\"DEF\">"+
					"<id root=\""+root1+"\""+"extension=\"%s.%s\"/>"+
					"<code code=\"VISIT\" codeSystem=\"%s\">"+
					"<displayName value=\"%s\"/>"+
					"</code>"+
					"<!--Visit with Tolerance-->"+
					"<component2 typeCode=\"COMP\">"+
					"<encounter classCode=\"ENC\" moodCode=\"DEF\">"+
					"<effectiveTime>"+
					"<low value=\"%s\"/>"+
					"<high value=\"%s\"/>"+
					"</effectiveTime>"+
					"<activityTime value=\"%s\"/>"+
					"</encounter>"+
					"</component2>"+
					"%s"+ // <= Component 1 added here
					"</timePointEventDefinition>"+
					"</component4>";
	
	private static final String subjectOfForStudyTemplate = 
			"<subjectOf typeCode=\"SUBJ\">"+
					"<studyCharacteristic classCode=\"OBS\" moodCode=\"EVN\">"+
					"<code code=\"%s\"/>"+
					"<value value=\"%s\"/>"+
					"</studyCharacteristic>"+
					"</subjectOf>";
	
	private  String subjectOfForComponent1Template =
			"<!--Billing modifier, coverag emodifier-->"+
					"<subjectOf typeCode=\"SUBJ\">"+
					"<timePointEventCharacteristic classCode=\"VERIF\" moodCode=\"EVN\">"+
					"<code code=\"BILL_MODIFIER\" codeSystem=\""+root1+"\""+" />"+
					"<!--Stored to [research billing modifier type] using a translation table-->"+
					"<value value=\"%s\"/>"+
					"</timePointEventCharacteristic>"+
					"</subjectOf>";
	
	private  String component1Template =
			"<component1 typeCode=\"COMP\">"+
					"<sequenceNumber value=\"%s\"/>"+
					"<timePointEventDefinition classCode=\"CTTEVENT\" moodCode=\"DEF\">"+
					"<id root=\""+root1 +"\""+"extension=\"%s.%s.%s\"/>"+
					"<code code=\"PROC\" codeSystem=\""+root1+"\""+"/>"+
					"<!--Procedure code-->"+
					"<component2 typeCode=\"COMP\">"+
					"<procedure classCode=\"PROC\" moodCode=\"EVN\">"+
					"<!--Used with EAP lookup (identity, procedure master #, or otherwise)"+
					" to find EAP for [billing procedure id]; send value of CPT code -->"+
					"<code code=\"%s\" codeSystem=\""+codesystem+"\""+"/>"+
					"</procedure>"+
					"</component2>"+
					"%s"+ // <= subjectOf for coverage type added here
					"</timePointEventDefinition>"+
					"</component1>";
	
	public String generateFinalOutput(Map<VelosKeys, Object> studyDataMap, String calendarData) {
		logger.info("******Generate FinalOutput of Retrieve Protocol End Point **************");
		ArrayList<String> argList = new ArrayList<String>();
		argList.add((String)studyDataMap.get(ProtocolKeys.IdExtension));
		argList.add((String)studyDataMap.get(ProtocolKeys.IdExtension));
		argList.add((String)studyDataMap.get(ProtocolKeys.Title));
		argList.add((String)studyDataMap.get(ProtocolKeys.Text));
		argList.add(calendarData);
		StringBuffer moreDetailsSB = new StringBuffer();
		if (studyDataMap.get(ProtocolKeys.IrbNumber) != null) {
			moreDetailsSB.append(
					generateSubjectOfForStudy(ProtocolKeys.Irb.toString(), (String)studyDataMap.get(ProtocolKeys.IrbNumber)));
		}
		if (studyDataMap.get(ProtocolKeys.Irb) != null) {
			moreDetailsSB.append(
					generateSubjectOfForStudy(ProtocolKeys.Irb.toString(), (String)studyDataMap.get(ProtocolKeys.Irb)));
		}
		if (studyDataMap.get(ProtocolKeys.NctNumber) != null) {
			moreDetailsSB.append(
					generateSubjectOfForStudy(ProtocolKeys.Nct.toString(), (String)studyDataMap.get(ProtocolKeys.NctNumber)));
		}
		if (studyDataMap.get(ProtocolKeys.Nct) != null) {
			moreDetailsSB.append(
					generateSubjectOfForStudy(ProtocolKeys.Nct.toString(), (String)studyDataMap.get(ProtocolKeys.Nct)));
		}
		argList.add(moreDetailsSB.toString());
		return String.format(outputTemplate, argList.toArray());
	}
	
	public String generateComponent4Part1(Map<VelosKeys, Object> calendarMap, String subPart) {
		ArrayList<String> argList = new ArrayList<String>();
		argList.add(((String)calendarMap.get(ProtocolKeys.CalendarName)).toUpperCase());
		argList.add(((String)calendarMap.get(ProtocolKeys.CalendarName)).toUpperCase());
		argList.add(((String)calendarMap.get(ProtocolKeys.CalendarName)).toUpperCase());
		argList.add(((String)calendarMap.get(ProtocolKeys.CalendarName)).toUpperCase());
		argList.add(subPart);
		argList.add(formatDate(getReferenceDate()));
		argList.add(formatDate(addDays(getReferenceDate(), (Integer)calendarMap.get(ProtocolKeys.CalendarDuration))));
		argList.add(((String)calendarMap.get(ProtocolKeys.CalendarName)).toUpperCase());
		return String.format(component4Part1Template, argList.toArray());
	}
	
	public String generateComponent4Part2(Map<VelosKeys, Object> visitMap) {
		ArrayList<String> argList = new ArrayList<String>();
		String calName = (String)visitMap.get(ProtocolKeys.CalendarName);
		String visitName = (String)visitMap.get(ProtocolKeys.VisitName);
		logger.info("VistNameeeee======>"+visitName);
		argList.add(calName);
		argList.add(visitName);
		argList.add(calName);
		argList.add(calName);
		argList.add((String)visitMap.get(ProtocolKeys.VisitWindowBeforeFormatted));
		argList.add((String)visitMap.get(ProtocolKeys.VisitWindowAfterFormatted));
		argList.add((String)visitMap.get(ProtocolKeys.VisitDisplacementFormatted));
		StringBuffer eventSB = new StringBuffer();
		@SuppressWarnings("unchecked")
		List<Map<VelosKeys, Object>> eventList = (List<Map<VelosKeys, Object>>)visitMap.get(ProtocolKeys.EventList);
		for (Map<VelosKeys, Object> eventMap : eventList) {
			eventSB.append(generateComponent1(eventMap, calName, visitName));
		}
		argList.add(eventSB.toString());
		return String.format(component4Part2Template, argList.toArray());
	}
	
	private String generateComponent1(Map<VelosKeys, Object> eventMap, String calNameUpper, String visitName) {
		ArrayList<String> argList = new ArrayList<String>();
		argList.add(String.valueOf((Integer)eventMap.get(ProtocolKeys.EventSequence)));
		argList.add(calNameUpper);
		argList.add(visitName);
		argList.add((String)eventMap.get(ProtocolKeys.EventName));
		argList.add((String)eventMap.get(ProtocolKeys.EventCpt));
		StringBuffer coverageSB = new StringBuffer();
		if (eventMap.get(ProtocolKeys.CoverageType) != null) {
			Object [] covArgs = { (String)eventMap.get(ProtocolKeys.CoverageType) };
			coverageSB.append(String.format(subjectOfForComponent1Template, covArgs));
		}
		argList.add(coverageSB.toString());
		return String.format(component1Template, argList.toArray());
	}
	
	private String generateSubjectOfForStudy(String key, String value) {
		ArrayList<String> argList = new ArrayList<String>();
		argList.add(key);
		argList.add(value);
		return String.format(subjectOfForStudyTemplate, argList.toArray());
	}
	
	public String generateComponent4Part1SubTemplate(String calendarName,
			String visitName, int visitSeq) {
		ArrayList<String> argList = new ArrayList<String>();
		argList.add(String.valueOf(visitSeq));
		argList.add(calendarName);
		argList.add(visitName);
		return String.format(component4Part1SubTemplate, argList.toArray());
	}
	
	public String handleRetrieveProtocol(Map<VelosKeys,Object> resultMap) throws Exception {
	
		Map<VelosKeys, String> requestMap = new HashMap<VelosKeys, String>();		
		String studyNumberStr = (String) resultMap.get(ProtocolKeys.StudyNumber);
		String endpoint = (String) resultMap.get(EndpointKeys.Endpoint);
		requestMap.put(ProtocolKeys.StudyNumber, studyNumberStr);
		requestMap.put(EndpointKeys.Endpoint,endpoint);
		String calendarName = (String) resultMap.get(ProtocolKeys.CalendarName);
		requestMap.put(ProtocolKeys.CalendarName,calendarName);

	//	RetrieveProtocolDefOutput output = new RetrieveProtocolDefOutput();
		/*
		XPath xpath = XPathFactory.newInstance().newXPath();
		XPathExpression expr = xpath.compile("//*[local-name()='query']/@extension");
		String studyNumberStr = expr.evaluate(source.getNode());
	*/
		// Call Velos eSP
		VelosEspClient client = null;
		client = new VelosEspClient();
		//Getting Study Summary Details
		Map<VelosKeys, Object> studyDataMap = client.handleRequest(VelosEspMethods.StudyGetStudySummary, requestMap);
		
		//Getting Study Calendar Details
		Map<VelosKeys, Object> studyCalendarListDataMap = client.handleStudyCalendarListRequest(requestMap);
		
		if (studyDataMap.get(ProtocolKeys.FaultString) != null) {
			throw createSOAPFaultException(studyDataMap);
		}
		System.out.println("StudyCalendarListMap ===>"+studyCalendarListDataMap);

		@SuppressWarnings("unchecked")
		List<Map<VelosKeys, Object>> calendarIdList = (List<Map<VelosKeys, Object>>) studyCalendarListDataMap
		.get(ProtocolKeys.CalendarIdList);
		StringBuffer calendarSB = new StringBuffer();
		if(calendarIdList != null)
		for (Map<VelosKeys, Object> calendarMap : calendarIdList) {
			requestMap.put(ProtocolKeys.CalendarId, String
					.valueOf(calendarMap.get(ProtocolKeys.CalendarId)));
			Map<VelosKeys, Object> calendarDataMap = client
					.handleStudyCalendarRequest(requestMap);
			@SuppressWarnings("unchecked")
			List<Map<VelosKeys, Object>> visitList = (List<Map<VelosKeys, Object>>) calendarDataMap
			.get(ProtocolKeys.VisitList);
			calendarMap.put(ProtocolKeys.CalendarDuration, calendarDataMap.get(ProtocolKeys.CalendarDuration));
			calendarMap.put(ProtocolKeys.CalendarDurUnit, calendarDataMap.get(ProtocolKeys.CalendarDurUnit));
			StringBuffer visitSB = new StringBuffer();
			StringBuffer visitForPart1SubSB = new StringBuffer();
			int visitSeq = 1;
			// First loop through visits to determine whether or not it is a day-zero calendar
			boolean isDayZeroCal = false;
			if(visitList != null)
			for (Map<VelosKeys, Object> visitMap : visitList) {
				if ((Integer) visitMap.get(ProtocolKeys.VisitDisplacement) == 0) {
					isDayZeroCal = true;
					break;
				}
			}
			
			if(visitList != null)
				logger.info("VisitList ======>"+visitList);
			for (Map<VelosKeys, Object> visitMap : visitList) {
				visitMap.put(ProtocolKeys.CalendarName,
						calendarMap.get(ProtocolKeys.CalendarName));
				int displacement = (Integer) visitMap.get(ProtocolKeys.VisitDisplacement);
				if (!isDayZeroCal) { --displacement; }
				Date displacementDate = addDays(getReferenceDate(), displacement);
				int visitWindowBefore = (Integer) visitMap.get(ProtocolKeys.VisitWindowBefore);
				logger.info("Visit Window Before ========>"+visitMap.get(ProtocolKeys.VisitWindowBefore));
				
				int visitWindowAfter = (Integer) visitMap.get(ProtocolKeys.VisitWindowAfter);
				
				logger.info("Visit Window After ========>"+visitMap.get(ProtocolKeys.VisitWindowAfter));
				visitMap.put(
						ProtocolKeys.VisitWindowBeforeFormatted,
						formatDate(addDays(displacementDate, -1 * visitWindowBefore)));
				visitMap.put(
						ProtocolKeys.VisitWindowAfterFormatted,
						formatDate(addDays(displacementDate, visitWindowAfter)));
				visitMap.put(
						ProtocolKeys.VisitDisplacementFormatted,
						formatDate(addDays(getReferenceDate(), displacement)));
				visitSB.append(generateComponent4Part2(visitMap));
				visitForPart1SubSB.append(generateComponent4Part1SubTemplate(
								(String) calendarMap
								.get(ProtocolKeys.CalendarName),
								(String) visitMap
								.get(ProtocolKeys.VisitName),
								visitSeq++));
			}
			calendarSB.append(generateComponent4Part1(calendarMap,
					visitForPart1SubSB.toString()));
			calendarSB.append(visitSB.toString());
		}
		System.out.println(calendarSB);

		// Return XML response
		String xml = generateFinalOutput(studyDataMap, calendarSB.toString());
		//return new DomSourceFactory().createSource(xml);
		logger.info("Calendar xml =================>"+xml);
		return xml;
	}
	
	private SOAPFaultException createSOAPFaultException(Map<VelosKeys, Object> dataMap) {
		SOAPFaultException sfe = null;
		MessageFactory messageFactory;
		try {
			messageFactory = MessageFactory.newInstance();
			SOAPMessage message = messageFactory.createMessage();
			SOAPFault soapFault = message.getSOAPBody().addFault();
			SOAPPart sp = message.getSOAPPart();
			SOAPEnvelope se = sp.getEnvelope();
			Name qname = se.createName("Client", null, SOAPConstants.URI_NS_SOAP_1_2_ENVELOPE);
			soapFault.setFaultCode(qname);
			soapFault.setFaultString((String)dataMap.get(ProtocolKeys.FaultString));
			Detail detail = soapFault.addDetail();
			detail.setTextContent((String)dataMap.get(ProtocolKeys.FaultString));
			sfe = new SOAPFaultException(soapFault);
			responseMsg = sfe.getMessage();
			//messageDao.saveMessageDetails("Inbound",requestMsg, responseMsg);
		} catch (SOAPException e) {
			e.printStackTrace();
			logger.info("SoapFaultException =====>"+e.getMessage());
		}
		return sfe;
	}

	
    // Our test reference date with Epic is 2014-01-01
    private static Date getReferenceDate() {
		Calendar cal = GregorianCalendar.getInstance();
		//cal.set(2014, 0, 1);
		return cal.getTime();
    }

    private static Date addDays(Date date, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days);
        logger.info("Calendar Days========>"+cal.getTime());
        return cal.getTime();
    }
    
    private static String formatDate(Date date) {
    	SimpleDateFormat f = new SimpleDateFormat("yyyyMMdd");
    	logger.info("Formate Date ====>"+f.format(date));
    	return f.format(date);
    }	
}
