package com.velos.integration.gateway;

import java.util.ArrayList;
import java.util.Map;

import com.velos.integration.mapping.VelosKeys;

public class RetrieveProtocolDefResponse {
	private static final String outputTemplate = 
			"<RetrieveProtocolDefResponse xmlns:ns2=\"urn:ihe:qrph:rpe:2009\" xmlns=\"urn:hl7-org:v3\">"
				+ "<responseCode>ALERT_RECEIVED</responseCode>"
				+"</RetrieveProtocolDefResponse>";
	
	public String generateOutput(Map<VelosKeys, String> dataMap) {
		ArrayList<String> argList = new ArrayList<String>();
		return String.format(outputTemplate, argList.toArray());
	}
}
