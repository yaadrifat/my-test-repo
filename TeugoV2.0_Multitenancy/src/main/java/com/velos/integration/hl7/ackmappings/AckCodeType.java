package com.velos.integration.hl7.ackmappings;

public enum AckCodeType {
	
	AcknowledgementAccept("AA"),
	AcknowledgementReject("AR"),
	AcknowledgementError("AE"),
	AcknowledgementInProcess("AP")
	
	;
	
	private String Key;
	
	AckCodeType(String key){
		this.Key=key;
	}
	
	public String toString(){
		return Key;
	}

}
