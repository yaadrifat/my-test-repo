package com.velos.integration.hl7.dao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import ca.uhn.hl7v2.HL7Exception;

import com.velos.integration.hl7.bean.FieldMapBean;
import com.velos.integration.hl7.bean.MessageBean;
import com.velos.integration.hl7.bean.MessageFieldBean;
import com.velos.integration.hl7.bean.ResponseMessageBean;
import com.velos.integration.hl7.bean.SegmentBean;


public class MessageConfiguration {

	private static Logger logger = Logger.getLogger(MessageConfiguration.class);

	private DataSource vgdbDataSource;
	private JdbcTemplate jdbcTemplate;
	Properties  prop = new Properties();

	public DataSource getVgdbDataSource() {
		return vgdbDataSource;
	}

	public void setVgdbDataSource(DataSource vgdbDataSource) {
		
			this.vgdbDataSource = vgdbDataSource;
			this.jdbcTemplate = new JdbcTemplate(vgdbDataSource);
			jdbcTemplate.setResultsMapCaseInsensitive(true);
			logger.info("Jdbc Template created successfully");
	
	}



	@Cacheable("messageconfiguration")
	public List<MessageBean> getMessageConfigurationBean() throws Exception ,IOException{

		List<MessageBean> messagelist=new ArrayList<MessageBean>();
		
		try {
			prop.load(this.getClass().getClassLoader().getResourceAsStream("config.properties"));
		} catch (IOException e) {
			logger.error("Config.Properties file doesn't Exist");
		}				

		if(prop.containsKey("enable_ADT_Interface") == true && prop.getProperty("enable_ADT_Interface").equalsIgnoreCase("y")
				&& !"".equals(prop.getProperty("enable_ADT_Interface"))){



			try{
				logger.info("Message Configurations Loaded");
				System.out.println("executing getMessageConfigurationBean() Method");

				String sql="select * from vgdb_message";
				List<Map<String, Object>> rows=jdbcTemplate.queryForList(sql);
				logger.info("Query for Messages :"+sql);
				for(Map row:rows)
				{
					MessageBean mb =new MessageBean();
					mb.setMesssageType((String)row.get("message_type"));
					mb.setMessageDataTable((String)row.get("message_data_table"));
					mb.setEmailNotification((Integer)row.get("email_notification"));
					mb.setRealTimeNotfication((Integer)row.get("realtime_notification"));

					//logger.info("Messages Data "+mb.getMesssageType());

					messagelist.add(mb);
				}


			} catch (Exception e) {
				logger.error("Error While getting Message Configuration Bean");
				e.printStackTrace();
				throw e;

			}
		}
		return messagelist;
	}

	@Cacheable("segmentconfiguration")
	public List<SegmentBean> getSegmentConfigurationBean() throws Exception,IOException {

		List<SegmentBean> segmentlist=new ArrayList<SegmentBean>();
	
		try {
			prop.load(this.getClass().getClassLoader().getResourceAsStream("config.properties"));
		} catch (IOException e) {
			logger.error("Config.Properties file doesn't Exist");
		}				

		if(prop.containsKey("enable_ADT_Interface") == true && prop.getProperty("enable_ADT_Interface").equalsIgnoreCase("y")
				&& !"".equals(prop.getProperty("enable_ADT_Interface"))){

			try{


				System.out.println("executing getSegmentConfigurationBean() Method");
				logger.info("Segment Configurations Loading ");

				String sql="select * from vgdb_message_segment";
				List<Map<String, Object>> rows=jdbcTemplate.queryForList(sql);
				logger.info("Query for Segments :"+sql);

				for(Map row:rows)
				{
					SegmentBean sb =new SegmentBean();
					sb.setMessageType((String)row.get("message_type"));
					sb.setSegmentName((String)row.get("segment_name"));
					sb.setRepeatedSegmentTable((String)row.get("repeated_segment_table"));
					sb.setRepeated((String)row.get("repeated"));
					sb.setSegmentType((Integer)row.get("segment_type"));
					String segSequence =String.valueOf(row.get("segment_sequence"));
					//System.out.println("segSequence="+segSequence);
					sb.setSegmentSequence(Integer.parseInt(validateNull(segSequence,"0")));
					sb.setIsMandatory((Integer)row.get("is_mandatory"));
					segmentlist.add(sb);
				}
				logger.info("Segment Configurations Loaded");

			}catch (Exception e) {
				logger.error("Error While getting Segment Configuration Bean "+e.getMessage());
				throw e;

			}
		}
		return segmentlist;
	}


	@Cacheable("messageFieldsConfiguration")
	public List<MessageFieldBean> getMessageFieldsConfiguration() throws Exception,IOException{

		List<MessageFieldBean> messagefields=new ArrayList<MessageFieldBean>();
		
		try {
			prop.load(this.getClass().getClassLoader().getResourceAsStream("config.properties"));
		} catch (IOException e) {
			logger.error("Config.Properties file doesn't Exist");
		}				

		if(prop.containsKey("enable_ADT_Interface") == true && prop.getProperty("enable_ADT_Interface").equalsIgnoreCase("y")
				&& !"".equals(prop.getProperty("enable_ADT_Interface"))){

			try{

				MessageFieldBean messagefield;

				String sql="select * from vgdb_message_fields";
				List<Map<String, Object>> rows=jdbcTemplate.queryForList(sql);

				for(Map row:rows){


					messagefield=new MessageFieldBean();

					messagefield.setMessageType((String)row.get("message_type"));
					messagefield.setSegmentName((String)row.get("segment_name"));
					messagefield.setFieldName((String)row.get("field_name"));
					messagefield.setRepeated((String)row.get("repeated"));
					messagefield.setColumn_name((String)row.get("column_name"));
					messagefield.setRepeatedColumn((String)row.get("repeated_column"));
					messagefield.setIsMandatory(Integer.parseInt(row.get("is_mandatory").toString()));
					messagefield.setRepeatedDelimiter((String)row.get("repeated_delimiter"));
					messagefield.setMappingType((String)row.get("mapping_type"));
					messagefield.setMappingField((String)row.get("mapping_field"));
					messagefield.setMaxLength(Integer.parseInt(row.get("max_length").toString()));
					messagefield.setTerserPath((String)row.get("terser_path"));
					messagefield.setFieldDatatype((String)row.get("field_datatype"));

					messagefields.add(messagefield);


				}
				logger.info("Message Field  Configurations Loaded");

			} catch (Exception e) {
				logger.error("Error While getting MessageFields Configuration Bean");
				e.printStackTrace();
				throw e;
			}
		}
		return messagefields;

	}

	@Cacheable("fieldmappingconfiguration")
	public List<FieldMapBean> getFieldmap() throws Exception,IOException {

		List<FieldMapBean> fieldmap=new ArrayList<FieldMapBean>();
		
		try {
			prop.load(this.getClass().getClassLoader().getResourceAsStream("config.properties"));
		} catch (IOException e) {
			logger.error("Config.Properties file doesn't Exist");
		}				

		if(prop.containsKey("enable_ADT_Interface") == true && prop.getProperty("enable_ADT_Interface").equalsIgnoreCase("y")
				&& !"".equals(prop.getProperty("enable_ADT_Interface"))){

			try{


				FieldMapBean fieldMapBean;

				String sql="select * from vgdb_field_mapping_values";
				List<Map<String, Object>> rows=jdbcTemplate.queryForList(sql);

				for(Map row:rows){

					fieldMapBean=new FieldMapBean();
					fieldMapBean.setMappingType((String)row.get("mapping_type"));
					fieldMapBean.setMappedValue((String)row.get("mapped_value"));
					fieldMapBean.setOriginalValue((String)row.get("original_value"));

					fieldmap.add(fieldMapBean);


				}

				logger.info("Message Field Mapping  Configurations Loaded");
			} catch (Exception e) {
				logger.error("Error While getting Message Fields Mapping Configuration Bean");
				throw e;
			}
		}
		return fieldmap;
	}

	@Cacheable("ResponseMessageBeanConfiguration")
	public List<ResponseMessageBean> getResponseMessageBeanList() throws Exception{
		List<ResponseMessageBean> resmesbeanlist=new ArrayList<ResponseMessageBean>();
	
		try {
			prop.load(this.getClass().getClassLoader().getResourceAsStream("config.properties"));
		} catch (IOException e) {
			logger.error("Config.Properties file doesn't Exist");
		}				

		if(prop.containsKey("enable_ADT_Interface") == true && prop.getProperty("enable_ADT_Interface").equalsIgnoreCase("y")
				&& !"".equals(prop.getProperty("enable_ADT_Interface"))){

			try{

				String sql="select * from vgdb_message_response";
				List<Map<String, Object>> rows=jdbcTemplate.queryForList(sql);


				for(Map row:rows){
					ResponseMessageBean rmb=new ResponseMessageBean();
					rmb.setMessageType((String)row.get("message_type"));
					rmb.setSegmentName((String)row.get("segment_name"));
					rmb.setFieldName((String)row.get("field_name"));
					rmb.setFieldValue((String)row.get("field_value"));
					rmb.setTerserPath((String)row.get("tensor_field"));
					rmb.setFieldFrom(Integer.parseInt((String)row.get("field_from")));

					resmesbeanlist.add(rmb);
				}

				logger.info("Response Message  Configurations Loaded");

			}catch(Exception e){
				logger.error("Error While getting Response Message Configuration Bean "+e.getMessage());
				throw e;

			}
		}
		return resmesbeanlist;
	}

	private String validateNull(String value,String defaultValue ) throws  Exception{
		if(value==null || value.trim().equals("null")){
			return defaultValue;
		}else{
			return value;
		}
	}



}
