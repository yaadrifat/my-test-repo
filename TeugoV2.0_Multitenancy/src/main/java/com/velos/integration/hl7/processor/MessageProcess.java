package com.velos.integration.hl7.processor;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.io.IOException;

import org.apache.log4j.Logger;

import com.velos.integration.hl7.ackmappings.AckResponseType;
import com.velos.integration.hl7.bean.MessageBean;
import com.velos.integration.hl7.bean.ResponseMessageBean;
import com.velos.integration.hl7.bean.SegmentBean;
import com.velos.integration.hl7.dao.MessageConfiguration;
import com.velos.integration.hl7.dao.MessageDao;
import com.velos.integration.hl7.notifications.EmailNotification;
import com.velos.integration.hl7.notifications.HL7CustomException;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.DefaultModelClassFactory;
import ca.uhn.hl7v2.util.Terser;

public class MessageProcess {

	private static Logger logger = Logger.getLogger(Hl7Processor.class);

	private ComparisonUtil comparisionutil;
	private MessageConfiguration messageConfiguration;
	private Message output;
	private InterfaceUtil interfaceutil;
	private MessageDao messageDao;
	private EmailNotification emailNotification;
	Properties prop = new Properties();
	Map<String, Object> messageMap;

	public ComparisonUtil getComparisionutil() {
		return comparisionutil;
	}

	public void setComparisionutil(ComparisonUtil comparisionutil) {
		this.comparisionutil = comparisionutil;
	}

	public InterfaceUtil getInterfaceutil() {
		return interfaceutil;
	}

	public void setInterfaceutil(InterfaceUtil interfaceutil) {
		this.interfaceutil = interfaceutil;
	}

	public MessageConfiguration getMessageConfiguration() {
		return messageConfiguration;
	}

	public void setMessageConfiguration(
			MessageConfiguration messageConfiguration) {
		this.messageConfiguration = messageConfiguration;
	}

	public MessageDao getMessageDao() {
		return messageDao;
	}

	public void setMessageDao(MessageDao messageDao) {
		this.messageDao = messageDao;
	}

	public MessageProcess()
	{

	}

	public EmailNotification getEmailNotification() {
		return emailNotification;

	}

	public void setEmailNotification(EmailNotification emailNotification) {
		this.emailNotification = emailNotification;
	}


	public MessageProcess(MessageProcess messageProcess) {
		this.comparisionutil = messageProcess.comparisionutil;
		this.messageConfiguration = messageProcess.messageConfiguration;
		this.output = messageProcess.output;
		this.interfaceutil = messageProcess.interfaceutil;
		this.messageDao = messageProcess.messageDao;
		this.emailNotification=messageProcess.emailNotification;
	}

	public Message processMessage(Message input)  throws Exception {
		try {
			InputStream inputStream = getClass().getClassLoader().getResourceAsStream("ackcodes.properties");
			prop.load(inputStream);


			if (input != null) {

				Terser t = new Terser(input);
				String messageType = t.get("/MSH-9-1");

				if(messageType == null ){
					throw new HL7CustomException(prop.getProperty("missedmessageType"),"AE");
				}
				System.out.println("Message Type -->"+messageType);
				boolean flag=comparisionutil.messageTypeComparison(messageType);

				if (flag == true) {

					logger.info("-----Processing " + messageType+ " Message type-----");

					messageMap=loadMsgTypeConfiguration( messageType,input);
					logger.info("Full Message Map =========># "+messageMap);
					if(messageMap.containsValue(messageType)){
						persistMessage(input, messageMap);
					}

					logger.info("Message Map------->"+messageMap);

					output = getResponseMessage(input,
							prop.getProperty("AckAccept"),
							prop.getProperty("AcknowledgementAccept"));

				} 
			}
			inputStream.close();
		} catch (HL7CustomException e) {
			logger.error("Error While Checking message Type");

			throw e;
		}
		return output;
	}





	public boolean preProcess(String tablename, Map<String, String> resultMap,Message input)
			throws Exception {
		return true;
	}

	public boolean postProcess(String tablename, Map<String, String> resultMap)
			throws Exception {
		return true;
	}




	public boolean persistMessage(Message input, Map<String, Object> messageMap) throws Exception
	{
		// mttablecreation.createTable(messageType);
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream("adt.properties");
		prop.load(inputStream);

		String tablename = (String) messageMap.get("tablename");

		Map<String, Map> finalMap;
		try {
			finalMap = interfaceutil.getFieldValue(input,
					messageMap);


			Map<String, String> resultMap = finalMap.get("ResultMap");

			System.out.println("Result Map size--->"+resultMap.size());
			Map<String, String[]> repeatedValueMap = finalMap
					.get("RepeatedValueMap");
			/*Map<String, String> repSegtableName = finalMap
					.get("RepSegTableNameMap");
			 */	Map<String,Map> repSegValMap = finalMap.get("RepSegValMap");
			 //mpreprocess.preProcess(input,tablename,segList,resultMap);

			 //Add by Surendra
			 //Survival status For Death Date
			 //addSurvivalStatus(resultMap);

			 boolean flag1 = preProcess(tablename, resultMap,input);
			 logger.info("After pre Processing ");


			 boolean	flag = postProcess(tablename, resultMap);

			 System.out.println("PostProcessing Flag Value ---->"+ flag);

			 logger.info("After Post Processing ");

			 if (flag == true) {
				 // method returning parent table Sequence ID
				 int parseqId = messageDao.insertMessageRecord(tablename, resultMap);


				 List<String> segList = interfaceutil.segmentList(input);
				 Set<String> repSegList = interfaceutil.repeatedSegmentList(segList);
				 //int repeatsegNo = Integer.parseInt(repSegtableName.get("RepeatedNo"));


				 String repSegTableName=null;
				 String messageType = null;
				 messageType = (String) messageMap.get("messagetype");
				 System.out.println("Boolean =======>"+!repSegList.isEmpty());
				 //Repeated Segment Values Inserting into their repeated segment names tables 
				 if(!repSegList.isEmpty()){
					 for(SegmentBean sb:getMessageConfiguration().getSegmentConfigurationBean()){
						 if(sb.getMessageType().equals(messageType)){
							 int segmentCount=0;
							 for(String seg : segList){
								 if(seg.equalsIgnoreCase(sb.getSegmentName()) && Integer.parseInt(sb.getRepeated())>1 && sb.getMessageType().equals(messageType)){
									 segmentCount = segmentCount+1;
								 }
							 }
							 // check whether that repeated segment is available or not in incoming message.
							 System.out.println("SegList =====>"+segList);
							 if(segList.contains(sb.getSegmentName())){
								 if(Integer.parseInt(sb.getRepeated())>1){
									 Map<String,String[]> repSegValues = repSegValMap.get(sb.getSegmentName());
									 repSegTableName = sb.getRepeatedSegmentTable();
									 System.out.println("Count =====>"+segmentCount);
									 System.out.println("values =====>"+repSegValues);
									 messageDao.insertRepeatedSegmentValue(repSegTableName,
											 repSegValues, parseqId, tablename,segmentCount);
									 logger.info("values inserted into repeated segment table");

								 }
							 }
						 }
					 }
					 logger.info("************************Message Process End Successfully******************");
				 }
			 }
			 //commented by surendra
			 /*flag = postProcess(tablename, resultMap);
			System.out.println("After Post Processing ");*/
			 inputStream.close();
			 return true;

		} catch (HL7CustomException e) {
			e.printStackTrace();
			throw e;
		}
	}



	//Survival status For Death Date
	public void addSurvivalStatus(Map<String, String> resultMap) throws IOException{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream("adt.properties");
		prop.load(inputStream);

		logger.info("addsurvivalStatus Method in MessageProcess");
		if ("Y".equals(prop.getProperty("postUpdateReq").toString())) {
			if (prop.containsKey("updateEvents")
					&& resultMap.containsKey("eventid")
					&& (prop.getProperty("updateEvents").toString()
							.contains((CharSequence) resultMap.get("eventid")))) {
				System.out.println("Survival Status2");
				logger.info("value----->"+  resultMap.get(prop.getProperty("deathdate_db_colmName")));
				logger.info(prop.containsKey("deathdate_db_colmName"));
				logger.info(resultMap.containsKey(prop.getProperty("deathdate_db_colmName")));
				logger.info(resultMap.get("survivalstatus"));
				logger.info(resultMap.get("DeathDate"));



				if(prop.containsKey("deathdate_db_colmName")&& resultMap.containsKey(prop.getProperty("deathdate_db_colmName"))
						&& resultMap.get(prop.getProperty("deathdate_db_colmName")) != null
						&& prop.containsKey("survivalstatus_code_for_death")
						&& prop.containsKey("survivalstatus_db_colmName")){


					resultMap.put(prop.getProperty("survivalstatus_db_colmName").toString().trim(),
							prop.getProperty("survivalstatus_code_for_death"));
					logger.info("Added Survival Status to resultMap");
				}
			}
		}inputStream.close();
	}

	// Generating Response Message
	public Message getResponseMessage(Message input, String ackmessage,
			String ackCode)  throws HL7CustomException,IOException {
		Message out = null;

		logger.info("\nGenerating Response Message");

		System.out.println("Ack Code  ==>" + ackCode);
		try {
			InputStream inputStream = getClass().getClassLoader()
					.getResourceAsStream("ackcodes.properties");
			prop.load(inputStream);
			Terser t1 = new Terser(input);
			String version = t1.get("/MSH-12-1");
			String ackversion = version;
			// Make an ACK of the same version as the inbound message:
			version = "2.6";
			String ackClassName = DefaultModelClassFactory
					.getVersionPackageName(version) + "message.ACK";
			Class ackClass = Class.forName(ackClassName);
			out = (Message) ackClass.newInstance();
			Terser t = new Terser(out);
			List<ResponseMessageBean> messageBeanList = messageConfiguration
					.getResponseMessageBeanList();

			t.set("/MSH-1", AckResponseType.Pipe.toString());
			t.set("/MSH-2", AckResponseType.EncodingCharacters.toString());
			t.set("/MSH-2", "^~\\&");

			for (ResponseMessageBean responseMsgBean : messageBeanList) {

				int fieldFrom = responseMsgBean.getFieldFrom();
				if (fieldFrom == 0) {
					t.set(responseMsgBean.getTerserPath(),
							responseMsgBean.getFieldValue());
				} else if (fieldFrom == 1) {
					if (responseMsgBean.getFieldName().equals("MessageID")) {
						t.set(responseMsgBean.getTerserPath(),
								AckResponseType.ResponseMessageType.toString()
								+ t1.get("/MSH-10"));
					} else if (responseMsgBean.getFieldName().equals(
							"AcknowledgementCode")) {
						t.set(responseMsgBean.getTerserPath(), ackCode);
					} else {
						t.set(responseMsgBean.getTerserPath(), ackmessage);
					}
				} else if (fieldFrom == 2) {
					t.set(responseMsgBean.getTerserPath(),
							t1.get(responseMsgBean.getFieldValue()));
				}
			}
			logger.info("\nResponse message :: \n" + out);
			inputStream.close();
		} catch (Exception e) {

			logger.error("Error While Generating Response Messages");
			throw new HL7CustomException(e.getMessage(),"",prop.getProperty("AcknowledgementReject"));
		}
		return out;
	}




	//Loading Message type configuration
	public Map<String, Object> loadMsgTypeConfiguration(String messageType,Message input)  throws HL7CustomException,IOException{
		messageMap = new HashMap<String, Object>();
		try {
			for (MessageBean mb : getMessageConfiguration()
					.getMessageConfigurationBean()) {

				// Checking request message type and from config to load
				// configurations


				if (mb.getMesssageType().equalsIgnoreCase(messageType)) {

					logger.info("\nMessageType--->" + messageType);

					System.out.println("MessageType---->"+messageType);

					// Loading the Message Type Configurations
					messageMap.put("messagetype", mb.getMesssageType());
					messageMap.put("tablename",
							mb.getMessageDataTable());
					messageMap.put("email", mb.getEmailNotification());
					messageMap.put("realtime",
							mb.getRealTimeNotfication());

					//persistMessage(input, messageMap);

					break;
				}// if ends

			}
		}  catch (Exception e) {
			throw new HL7CustomException(e.getMessage(),"",prop.getProperty("AcknowledgementReject"));
		}

		return messageMap;

	}




	// Generating ErrorResponse
	public Message getErrorResponse(String message,String errorMsg,String respType) throws HL7CustomException
	{

		logger.info("\nGenerating Error Response" + errorMsg);

		String[] segments1 = message.split("\r");

		String[] segments2 = segments1[0].split("\\|");

		String sendingApplication = segments2[3];
		String sendingFacility = segments2[4];
		String receivingApplication = segments2[5];
		String receivingFacility = segments2[6];
		String version = segments2[11];
		String messageId = segments2[9];
		String ackVersion =null;
		ackVersion =version;
		Message out = null;
		try {

			InputStream inputStream = getClass().getClassLoader()
					.getResourceAsStream("ackcodes.properties");
			prop.load(inputStream);
			version="2.6";
			String ackClassName = DefaultModelClassFactory
					.getVersionPackageName(version) + "message.ACK";
			
			Class ackClass = Class.forName(ackClassName);
			out = (Message) ackClass.newInstance();

			Terser t = new Terser(out);
			t.set("/MSH-1", AckResponseType.Pipe.toString());
			t.set("/MSH-2", AckResponseType.EncodingCharacters.toString());
			t.set("/MSH-3-1", receivingApplication);
			t.set("/MSH-4-1", receivingFacility);
			t.set("/MSH-5-1", sendingApplication);
			t.set("/MSH-6-1", sendingFacility);
			t.set("/MSH-9-1", "ACK");
			t.set("/MSH-10-1", AckResponseType.ResponseMessageType.toString()
					+ messageId);
			t.set("/MSH-12-1", ackVersion);
			t.set("/MSA-1-1",respType);
			t.set("/MSA-2-1", messageId);
			t.set("/MSA-3-1", errorMsg);
			logger.info("Error OutPut---->" + out);
			inputStream.close();
		} catch (Exception e) {
			logger.error("Error While Generating Error Response");
			throw new HL7CustomException(e.getMessage(),"",prop.getProperty("AcknowledgementReject"));
		}

	

		return out;
	}



	//Catching Exceptions from all Classes
	public Message getFinalException(String messageStrings,String errorMsg,String responseType,String eMailMessage) throws Exception{
		logger.error("\nError while parsing the incoming message :: \n");
		logger.error("Error Message  "+errorMsg);
		System.out.println("Error Message  "+errorMsg);
		Message out=null;

		try {
			InputStream inputStream = getClass().getClassLoader()
					.getResourceAsStream("adt.properties");
			prop.load(inputStream);

			out=getErrorResponse(messageStrings,errorMsg,responseType);
			Terser t=new Terser(out);
			String msgId=t.get("/MSA-2-1");
			String mrn = null, msgdate=null, temp=null;
			System.out.println("MESSAGE-->"+out);
			String[] segments1 = messageStrings.split("\r");
			for(String seg:segments1){
				if(seg.startsWith("PID")){
					String[] field=seg.split("\\|");
					temp=field[Integer.parseInt(prop.getProperty("pid_mrn_field_positionNo"))];
					mrn=temp.split("[^A-Za-z0-9]")[0];
					break;
				}
			}
			messageDao.insertMessageAudit(messageStrings,out,mrn);
			msgdate = messageDao.getDate();

			if("Y".equalsIgnoreCase(prop.getProperty("emailNotf_Require").toString().trim())){
				if(eMailMessage != null){
					logger.info("eMailMessage ------>"+eMailMessage);
					System.out.println("eMailMessage ------>"+eMailMessage);
					emailNotification.sendMail(out.toString(),eMailMessage, msgdate, mrn, msgId);
				}
			}inputStream.close();
		} catch (Exception e) {
			logger.info("\nFinal Method in Message process Exception");
			e.printStackTrace();
			throw e;
		}

		return out;
	}



}
