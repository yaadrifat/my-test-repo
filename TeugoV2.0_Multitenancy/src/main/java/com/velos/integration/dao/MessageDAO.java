package com.velos.integration.dao;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;

import com.velos.epic.beans.OutboundMessageBean;
import com.velos.integration.hl7.bean.MessageBean;
import com.velos.integration.hl7.notifications.OperationCustomException;
import com.velos.integration.mapping.ProtocolKeys;
import com.velos.integration.mapping.VelosKeys;

public class MessageDAO {

	private static Logger logger = Logger.getLogger("epicLogger");

	private DataSource eresDataSource;
	private JdbcTemplate jdbcTemplate;
	Properties prop = null;
	

	public void setEresDataSource(DataSource eresDataSource) {
		this.eresDataSource = eresDataSource;
		this.jdbcTemplate = new JdbcTemplate(eresDataSource);
		 jdbcTemplate.setResultsMapCaseInsensitive(true);
		logger.info("*******Jdbc Connection created successfully");
		
		}

	public boolean saveMessageDetails(String messageType,String requestXML,String responseXML) {
		
		logger.info(" ****** Saving Audit Message Details **************");
		
		String requestxml= null;
		String responsexml = null;
		boolean isSuccess = false;
		
		logger.info("Audit RequestXML ==========>"+requestXML);
		logger.info("Audit RespnseXML ==========>"+responseXML);
		
		
		try{
			
			if(requestXML!=null){
				requestxml=requestXML.replace("\"","");
			}
			if(responseXML != null){
				responsexml=responseXML.replace("\"","");
			}

			String sql = "INSERT INTO VGDB_RPE_AUDIT_DETAILS"
					+"(pk_msg_audit,MESSAGE_TYPE,REQUEST_MESSAGE,RESPONSE_MESSAGE,CREATION_DATE)"
					+" VALUES(nextval('seq_vgdb_rpe_audit_details'),?,?,?,current_timestamp)";
			jdbcTemplate.update(sql,new Object[]{messageType,requestxml,responsexml});
			
			isSuccess = true;
			logger.info("DB Update Success ===>"+isSuccess);
			logger.info("Audit Query ==========>"+sql);
			
		}catch(Exception e){
			e.printStackTrace();
			isSuccess = false;
			//throw e;
		}
		return isSuccess;
	}
	
	

	public boolean saveMessageDetails(String messageType,String requestXML,String responseXML,Map<VelosKeys,Object> dataMap) throws Exception {
		
		logger.info(" ****** Saving Audit Message Details **************");
		
		String requestxml= null;
		String responsexml = null;
		String patientId = null;
		String studyID = null;
		Integer fk_id = 0;
		boolean isSuccess = false;
		String moduleName = null;
		String outbound_oid = null;
		String sql = null;
		try{
			

			if(requestXML!=null){
				requestxml=requestXML.replace("\"","");
			}
			if(responseXML != null){
				responsexml=responseXML.replace("\"","");
			}
			
		if(dataMap != null) {
			if(messageType.equalsIgnoreCase("Outbound")) {
			outbound_oid = (String)dataMap.get(ProtocolKeys.OutboundOID);
			studyID = (String)dataMap.get(ProtocolKeys.StudyId);
		     patientId =(String)dataMap.get(ProtocolKeys.PatientId);
		     moduleName = (String)dataMap.get(ProtocolKeys.ModuleName);
		     fk_id =  (Integer) dataMap.get(ProtocolKeys.PkId);
		      sql = "INSERT INTO VGDB_RPE_AUDIT_DETAILS"
						+"(pk_msg_audit,MESSAGE_TYPE,REQUEST_MESSAGE,RESPONSE_MESSAGE,CREATION_DATE,outbound_oid,studynumber,patientid,modulename,fk_vgdb_rpe_outbound_message)"
						+" VALUES(nextval('seq_vgdb_rpe_audit_details'),?,?,?,current_timestamp,?,?,?,?,?)";
				jdbcTemplate.update(sql,new Object[]{messageType,requestxml,responsexml,outbound_oid,studyID,patientId,moduleName,fk_id});
			
			}else if(messageType.equalsIgnoreCase("Inbound"))  {
				
				studyID = (String)dataMap.get(ProtocolKeys.StudyNumber);
			     patientId =(String)dataMap.get(ProtocolKeys.PatientId);
			     moduleName = (String)dataMap.get(ProtocolKeys.ModuleName);
			      sql = "INSERT INTO VGDB_RPE_AUDIT_DETAILS"
							+"(pk_msg_audit,MESSAGE_TYPE,REQUEST_MESSAGE,RESPONSE_MESSAGE,CREATION_DATE,outbound_oid,studynumber,patientid,modulename)"
							+" VALUES(nextval('seq_vgdb_rpe_audit_details'),?,?,?,current_timestamp,?,?,?,?)";
					jdbcTemplate.update(sql,new Object[]{messageType,requestxml,responsexml,outbound_oid,studyID,patientId,moduleName});
				
			}
		}	
			isSuccess = true;
			logger.info("DB Update Success ===>"+isSuccess);
			logger.info("Audit Query ==========>"+sql);
			
		}catch(Exception e){
			logger.info("save audit details exception block==>"+e.getMessage());
			e.printStackTrace();
			isSuccess = false;
			throw e;
		}finally {
			dataMap = null;
		}
		return isSuccess;
	}
	



	public boolean  insertOutboundMessageDetails(String messageType,Map<VelosKeys,Object> dataMap) throws ParseException {

		logger.info(" **** Saving Outbound Message Details ********");
		
		try {
			prop = new Properties();
			prop.load(this.getClass().getClassLoader().getResourceAsStream("epic.properties"));
		} catch (IOException e1) {
			logger.info("** Save MessageDetails in DAO class **");
			e1.printStackTrace();
		}

		String requestxml= null;
		String responsexml = null;
		String studyNumber = null;
		String patientId = null;
		String statusCode = null;
		String action = null;
		String requestXML=null;
		String process_flag = null;
		String moduleName = null;
		Date trigger_timestamp = null;
		int count =0;
		boolean isSuccess = false;
		List<String> changesMsgList = null;
		SimpleDateFormat sdf = null;
		int pk_vgdb_rpe_outbound_message = 0;
		try{
			sdf = new SimpleDateFormat("yyyy/MM/dd HH:MM:SS");

			if(dataMap != null){
				changesMsgList =(List<String>) dataMap.get(ProtocolKeys.ChangesMsgList);
			}

			for(String changesMsg : changesMsgList){

				requestxml = changesMsg;
				studyNumber = parseChangeMessage("studyNumber", changesMsg);
				patientId = parseChangeMessage("patientId", changesMsg);
				statusCode = parseChangeMessage("statusSubType", changesMsg);
				action = parseChangeMessage("action",changesMsg);
				moduleName = parseChangeMessage("module",changesMsg);
				trigger_timestamp = sdf.parse(parseChangeMessage("timeStamp",changesMsg));
				String calendar_name = parseChangeMessage("calendarName",changesMsg);
				String oid = parseChangeMessage("OID",changesMsg);
				process_flag = "false";
				
				
				
				
				if(requestxml!=null){
					requestXML=requestxml.replace("\"","");
				}
				logger.info("outbound changes RequestXML "+requestxml);

				if(moduleName.equalsIgnoreCase(prop.getProperty("calendar_module_name").trim())) {
					if(statusCode.trim().equalsIgnoreCase(prop.getProperty("calendarStatus_codelst_subtype").trim())) {
						String sql = "INSERT INTO VGDB_RPE_OUTBOUND_MESSAGE"
								+"(pk_VGDB_RPE_OUTBOUND_MESSAGE,MESSAGE_TYPE,PROCESS_FLAG,MODULE_NAME,REQUEST_MESSAGE,STUDYNUMBER,PATIENTID,STATUSCODE,ACTION,TRIGGER_TIMESTAMP,CALENDAR_NAME,OID,RECORD_CREATION_DATE,COUNT)"
								+" VALUES(nextval('seq_vgdb_rpe_outbound_message'),?,?,?,?,?,?,?,?,?,?,?,current_timestamp,?)";
						jdbcTemplate.update(sql,new Object[]{messageType,process_flag,moduleName,requestXML,studyNumber,patientId,statusCode,action,trigger_timestamp,calendar_name,oid,count});
						logger.info("*************Db inserted successfully********");
						isSuccess = true;
					}else {
						logger.info("Calendar Status is "+statusCode.trim() +"it should be "+prop.getProperty("calendarStatus_codelst_subtype").trim());
					}
				}else if(moduleName.trim().equalsIgnoreCase("patstudy_status")) {
				String sql = "INSERT INTO VGDB_RPE_OUTBOUND_MESSAGE"
						+"(pk_VGDB_RPE_OUTBOUND_MESSAGE,MESSAGE_TYPE,PROCESS_FLAG,MODULE_NAME,REQUEST_MESSAGE,STUDYNUMBER,PATIENTID,STATUSCODE,ACTION,TRIGGER_TIMESTAMP,CALENDAR_NAME,OID,RECORD_CREATION_DATE,COUNT)"
						+" VALUES(nextval('seq_vgdb_rpe_outbound_message'),?,?,?,?,?,?,?,?,?,?,?,current_timestamp,?)";
				jdbcTemplate.update(sql,new Object[]{messageType,process_flag,moduleName,requestXML,studyNumber,patientId,statusCode,action,trigger_timestamp,calendar_name,oid,count});
				isSuccess = true;
				logger.info("DB Update Success ===>"+isSuccess);
				logger.info("Query ===>"+sql);
				}else if(!moduleName.trim().equalsIgnoreCase("calendar_status") && !moduleName.trim().equalsIgnoreCase("patstudy_status")) {
					String sql = "INSERT INTO VGDB_RPE_OUTBOUND_MESSAGE"
							+"(pk_VGDB_RPE_OUTBOUND_MESSAGE,MESSAGE_TYPE,PROCESS_FLAG,MODULE_NAME,REQUEST_MESSAGE,STUDYNUMBER,PATIENTID,STATUSCODE,ACTION,TRIGGER_TIMESTAMP,CALENDAR_NAME,OID,RECORD_CREATION_DATE,COUNT)"
							+" VALUES(nextval('seq_vgdb_rpe_outbound_message'),?,?,?,?,?,?,?,?,?,?,?,current_timestamp,?)";
					jdbcTemplate.update(sql,new Object[]{messageType,process_flag,moduleName,requestXML,studyNumber,patientId,statusCode,action,trigger_timestamp,calendar_name,oid,count});
					logger.info("*************Db inserted successfully********");
					isSuccess = true;
				}
			}
		}catch(Exception e){
				e.printStackTrace();
				isSuccess = false;
			//	throw e;
			}
		
		return isSuccess;

	}

	public String	parseChangeMessage(String tagName,String changesMsg){

		logger.info(" *************** Parsing Changes Message **************");

		String resultValue = null;
		Matcher matcher = null;
		Pattern pattern = null;

		try{
			String regx = "<"+tagName+">"+"(.+?)"+"</"+tagName+">";
			logger.info("TagName ======>"+regx);
			pattern = Pattern.compile(regx,Pattern.DOTALL);

			if(pattern != null){
				matcher =pattern.matcher(changesMsg);
				if(matcher.find()){
					resultValue = matcher.group(1);
				}
			}
		}catch(Exception e){
			//throw e;
		}
		return  resultValue;
	}

	public List<OutboundMessageBean> getOutboundMessageBean(String moduleName){

		logger.info(" ********** Outbound Message Dao Configurations Loading *************");
		
		try {
			prop = new Properties();
			prop.load(this.getClass().getClassLoader().getResourceAsStream("epic.properties"));
		} catch (IOException e1) {
			logger.info("** Save MessageDetails in DAO class **");
			e1.printStackTrace();
		}

		List<OutboundMessageBean> messagelist= null ;
		OutboundMessageBean omb = null;
		int outboundMsgCount =0;
		outboundMsgCount = Integer.parseInt(prop.getProperty("outboundMsgCount"));

		try{

			messagelist = 	new CopyOnWriteArrayList<OutboundMessageBean>();
			String sql="select * from vgdb_rpe_outbound_message where process_flag = 'false' "+" and module_name = '"+moduleName +"'  and count <= "+outboundMsgCount;
			logger.info("Getting Outbound Message Query ==>"+ sql);
			@SuppressWarnings("unchecked")
			List<Map<String,Object>> rows=jdbcTemplate.queryForList(sql);
			
			List  rowss= jdbcTemplate.queryForList(sql);
			
			for(Map row:rows)
			{
				omb =new OutboundMessageBean();
				omb.setMessageType((String)row.get("message_type"));
				omb.setAction((String)row.get("action"));
				omb.setModuleName((String)row.get("module_name"));
				omb.setPatientId((String)row.get("patientId"));
				omb.setProcessFlag((String)row.get("process_flag"));
				omb.setRecordCreationDate(row.get("record_creation_time"));
				omb.setRequestMessage((String)row.get("request_message"));
				omb.setStudyNumber((String)row.get("studyNumber"));
				omb.setStatusCode((String)row.get("statusCode"));
				omb.setMsgTimeStamp(row.get("trigger_timestamp"));
				omb.setRecordCreationDate(row.get("record_creation_date"));
				omb.setCalendarName((String)row.get("calendar_name"));
				omb.setOID((String)row.get("oid"));
				omb.setPk_vgdb_rpe_outbound_message((Integer)row.get("pk_vgdb_rpe_outbound_message"));

				messagelist.add(omb);
			}
			return messagelist ;
		}catch(Exception ee){
			logger.info("************OutBoundMessageBean Dao Exception block ************"+ee.getMessage());
			ee.printStackTrace();
			//throw ee;
		}finally{
			omb = null;
			messagelist = null;
			prop = null;
		}
		return messagelist ;

	}

	public void updateOutboundMessageInfo(OutboundMessageBean omb) throws OperationCustomException{

		logger.info(" ************** Update Outbound Message Query ********");

		String studyId = null;
		String patientId = null;
		Date msgtimeStamp = null;
		Date recordCreationtimeStamp = null;
		String moduleName = null;
		String query = null;
		String calendarName = null;

		try{

			moduleName = omb.getModuleName();
			studyId = omb.getStudyNumber();
			msgtimeStamp = (Date) omb.getMsgTimeStamp();
			recordCreationtimeStamp = (Date) omb.getRecordCreationDate();

			if(moduleName.equals("patstudy_status")){
				patientId = omb.getPatientId();
				query = "update vgdb_rpe_outbound_message set process_flag='true',count=0 where process_flag = 'false' and studyNumber='"+studyId+"'and patientId ='"+patientId+"'and trigger_timestamp ='"+msgtimeStamp+"'";
			}else if(moduleName.equals("study_summary") || moduleName.equals("study_status")){
				query = "update vgdb_rpe_outbound_message set process_flag='true',count=0 where process_flag = 'false' and studyNumber='"+studyId+"'and trigger_timestamp ='"+msgtimeStamp+"'and record_creation_date = '"+recordCreationtimeStamp+"'";
			}else if(moduleName.equals("calendar_status")){
				calendarName = omb.getCalendarName();
				query = "update vgdb_rpe_outbound_message set process_flag='true',count=0 where process_flag = 'false' and studyNumber='"+studyId+"'and calendar_name ='"+calendarName+"' and trigger_timestamp ='"+msgtimeStamp+"'and record_creation_date = '"+recordCreationtimeStamp+"'";
			}

			logger.info("Update Query ====>"+ query);
			jdbcTemplate.update(query);

		}catch(Exception e){
			e.printStackTrace();
			throw new OperationCustomException(e.getMessage());
		}
	}
	
public int	getCount(Map<VelosKeys, Object> requestMap, int flag){
	int i = 0;
	try {
	  int pk_id = (Integer) requestMap.get(ProtocolKeys.PkId);
	  String sql = null;
	  if(flag == 0) {
	  sql =" select count from vgdb_rpe_outbound_message where pk_vgdb_rpe_outbound_message ="+pk_id;
	  i = jdbcTemplate.queryForInt(sql);
	  }if(flag > 0) {
		  sql = "update vgdb_rpe_outbound_message set count = '" + flag+ "' where pk_vgdb_rpe_outbound_message ='"+pk_id+"'";
		 i = jdbcTemplate.update(sql);
	  }
	
	}catch(Exception e) {
		e.printStackTrace();
	}
	return i;
		
	}
	

}
