package com.velos.outbound.servlet;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.velos.integration.hl7.util.PropertyFileManager;

/**
 * Servlet implementation class AdtServlet
 */

public class AdtServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdtServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	System.out.println("Entered into servelet");
	String intfRes = request.getParameter("result");
	String criteria = request.getParameter("criteria");

	 HashMap<String, String> settingsMap = new HashMap<String, String>();

	
	if(criteria.equals("ADT")){
		
		String key = "enable_ADT_Interface";
		 settingsMap.put(key,intfRes);
		
	} else if(criteria.equals("Epic")){
		String key = "enable_Epic_Interface";
		 settingsMap.put(key,intfRes);
		
	}
	
	
	
	
	PropertyFileManager propertyFileManager = new PropertyFileManager();
	
	propertyFileManager.getProperties(settingsMap);
	
	System.out.println("ADT Value == "+intfRes +"\n"+ "Criteria =="+criteria);
	
	
	
	
	}

}
