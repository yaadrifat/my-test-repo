
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for searchUserResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="searchUserResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UserSearchResults" type="{http://velos.com/services/}userSearchResults" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "searchUserResponse", propOrder = {
    "userSearchResults"
})
@XmlRootElement(name="searchUserResponse", namespace = "http://velos.com/services/")
public class SearchUserResponse {

    @XmlElement(name = "UserSearchResults")
    protected UserSearchResults userSearchResults;

    /**
     * Gets the value of the userSearchResults property.
     * 
     * @return
     *     possible object is
     *     {@link UserSearchResults }
     *     
     */
    public UserSearchResults getUserSearchResults() {
        return userSearchResults;
    }

    /**
     * Sets the value of the userSearchResults property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserSearchResults }
     *     
     */
    public void setUserSearchResults(UserSearchResults value) {
        this.userSearchResults = value;
    }

}
