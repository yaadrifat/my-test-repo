package com.velos.services;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 2.7.3
 * 2018-12-19T17:54:19.489+05:30
 * Generated source version: 2.7.3
 * 
 */
@WebService(targetNamespace = "http://velos.com/services/", name = "OutboundMessageSEI")
public interface OutboundMessageSEI {

    @WebResult(name = "ChangesList", targetNamespace = "")
    @RequestWrapper(localName = "getOutboundMessages", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetOutboundMessages")
    @WebMethod
    @ResponseWrapper(localName = "getOutboundMessagesResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetOutboundMessagesResponse")
    public com.velos.services.ChangesList getOutboundMessages(
        @WebParam(name = "fromDate", targetNamespace = "")
        javax.xml.datatype.XMLGregorianCalendar fromDate,
        @WebParam(name = "toDate", targetNamespace = "")
        javax.xml.datatype.XMLGregorianCalendar toDate,
        @WebParam(name = "moduleName", targetNamespace = "")
        java.lang.String moduleName,
        @WebParam(name = "calledFrom", targetNamespace = "")
        java.lang.String calledFrom
    ) throws OperationException_Exception;
}
