
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for removeVisitsFromStudyCalendar complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="removeVisitsFromStudyCalendar">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CalendarIdentifier" type="{http://velos.com/services/}calendarIdentifier" minOccurs="0"/>
 *         &lt;element name="VisitIdentifiers" type="{http://velos.com/services/}visitIdentifiers" minOccurs="0"/>
 *         &lt;element name="VisitNames" type="{http://velos.com/services/}visitNames" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "removeVisitsFromStudyCalendar", propOrder = {
    "calendarIdentifier",
    "visitIdentifiers",
    "visitNames"
})
public class RemoveVisitsFromStudyCalendar {

    @XmlElement(name = "CalendarIdentifier")
    protected CalendarIdentifier calendarIdentifier;
    @XmlElement(name = "VisitIdentifiers")
    protected VisitIdentifiers visitIdentifiers;
    @XmlElement(name = "VisitNames")
    protected VisitNames visitNames;

    /**
     * Gets the value of the calendarIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link CalendarIdentifier }
     *     
     */
    public CalendarIdentifier getCalendarIdentifier() {
        return calendarIdentifier;
    }

    /**
     * Sets the value of the calendarIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link CalendarIdentifier }
     *     
     */
    public void setCalendarIdentifier(CalendarIdentifier value) {
        this.calendarIdentifier = value;
    }

    /**
     * Gets the value of the visitIdentifiers property.
     * 
     * @return
     *     possible object is
     *     {@link VisitIdentifiers }
     *     
     */
    public VisitIdentifiers getVisitIdentifiers() {
        return visitIdentifiers;
    }

    /**
     * Sets the value of the visitIdentifiers property.
     * 
     * @param value
     *     allowed object is
     *     {@link VisitIdentifiers }
     *     
     */
    public void setVisitIdentifiers(VisitIdentifiers value) {
        this.visitIdentifiers = value;
    }

    /**
     * Gets the value of the visitNames property.
     * 
     * @return
     *     possible object is
     *     {@link VisitNames }
     *     
     */
    public VisitNames getVisitNames() {
        return visitNames;
    }

    /**
     * Sets the value of the visitNames property.
     * 
     * @param value
     *     allowed object is
     *     {@link VisitNames }
     *     
     */
    public void setVisitNames(VisitNames value) {
        this.visitNames = value;
    }

}
