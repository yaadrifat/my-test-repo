
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for outboundIdentifier complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="outboundIdentifier">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}simpleIdentifier">
 *       &lt;sequence>
 *         &lt;element name="budgetName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="calendarName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="formName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="patientIdentifier" type="{http://velos.com/services/}outboundPatientIdentifier" minOccurs="0"/>
 *         &lt;element name="studyIdentifier" type="{http://velos.com/services/}outboundStudyIdentifier" minOccurs="0"/>
 *         &lt;element name="studyNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "outboundIdentifier", propOrder = {
    "budgetName",
    "calendarName",
    "formName",
    "patientIdentifier",
    "studyIdentifier",
    "studyNumber"
})
public class OutboundIdentifier
    extends SimpleIdentifier
{

    protected String budgetName;
    protected String calendarName;
    protected String formName;
    protected OutboundPatientIdentifier patientIdentifier;
    protected OutboundStudyIdentifier studyIdentifier;
    protected String studyNumber;

    /**
     * Gets the value of the budgetName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBudgetName() {
        return budgetName;
    }

    /**
     * Sets the value of the budgetName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBudgetName(String value) {
        this.budgetName = value;
    }

    /**
     * Gets the value of the calendarName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCalendarName() {
        return calendarName;
    }

    /**
     * Sets the value of the calendarName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCalendarName(String value) {
        this.calendarName = value;
    }

    /**
     * Gets the value of the formName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormName() {
        return formName;
    }

    /**
     * Sets the value of the formName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormName(String value) {
        this.formName = value;
    }

    /**
     * Gets the value of the patientIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link OutboundPatientIdentifier }
     *     
     */
    public OutboundPatientIdentifier getPatientIdentifier() {
        return patientIdentifier;
    }

    /**
     * Sets the value of the patientIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link OutboundPatientIdentifier }
     *     
     */
    public void setPatientIdentifier(OutboundPatientIdentifier value) {
        this.patientIdentifier = value;
    }

    /**
     * Gets the value of the studyIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link OutboundStudyIdentifier }
     *     
     */
    public OutboundStudyIdentifier getStudyIdentifier() {
        return studyIdentifier;
    }

    /**
     * Sets the value of the studyIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link OutboundStudyIdentifier }
     *     
     */
    public void setStudyIdentifier(OutboundStudyIdentifier value) {
        this.studyIdentifier = value;
    }

    /**
     * Gets the value of the studyNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStudyNumber() {
        return studyNumber;
    }

    /**
     * Sets the value of the studyNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStudyNumber(String value) {
        this.studyNumber = value;
    }

}
