
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for visit complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="visit">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="events" type="{http://velos.com/services/}events" minOccurs="0"/>
 *         &lt;element name="noIntervalDefined" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="visitDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="visitIdentifier" type="{http://velos.com/services/}visitIdentifier" minOccurs="0"/>
 *         &lt;element name="visitName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="visitScheduledDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="visitSuggestedDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="visitWindow" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "visit", propOrder = {
    "events",
    "noIntervalDefined",
    "visitDescription",
    "visitIdentifier",
    "visitName",
    "visitScheduledDate",
    "visitSuggestedDate",
    "visitWindow"
})
@XmlRootElement(name="visit", namespace = "http://velos.com/services/")
public class Visit
    extends ServiceObject
{

    protected Events events;
    protected boolean noIntervalDefined;
    protected String visitDescription;
    protected VisitIdentifier visitIdentifier;
    protected String visitName;
    protected String visitScheduledDate;
    protected String visitSuggestedDate;
    protected String visitWindow;

    /**
     * Gets the value of the events property.
     * 
     * @return
     *     possible object is
     *     {@link Events }
     *     
     */
    public Events getEvents() {
        return events;
    }

    /**
     * Sets the value of the events property.
     * 
     * @param value
     *     allowed object is
     *     {@link Events }
     *     
     */
    public void setEvents(Events value) {
        this.events = value;
    }

    /**
     * Gets the value of the noIntervalDefined property.
     * 
     */
    public boolean isNoIntervalDefined() {
        return noIntervalDefined;
    }

    /**
     * Sets the value of the noIntervalDefined property.
     * 
     */
    public void setNoIntervalDefined(boolean value) {
        this.noIntervalDefined = value;
    }

    /**
     * Gets the value of the visitDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVisitDescription() {
        return visitDescription;
    }

    /**
     * Sets the value of the visitDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVisitDescription(String value) {
        this.visitDescription = value;
    }

    /**
     * Gets the value of the visitIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link VisitIdentifier }
     *     
     */
    public VisitIdentifier getVisitIdentifier() {
        return visitIdentifier;
    }

    /**
     * Sets the value of the visitIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link VisitIdentifier }
     *     
     */
    public void setVisitIdentifier(VisitIdentifier value) {
        this.visitIdentifier = value;
    }

    /**
     * Gets the value of the visitName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVisitName() {
        return visitName;
    }

    /**
     * Sets the value of the visitName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVisitName(String value) {
        this.visitName = value;
    }

    /**
     * Gets the value of the visitScheduledDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVisitScheduledDate() {
        return visitScheduledDate;
    }

    /**
     * Sets the value of the visitScheduledDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVisitScheduledDate(String value) {
        this.visitScheduledDate = value;
    }

    /**
     * Gets the value of the visitSuggestedDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVisitSuggestedDate() {
        return visitSuggestedDate;
    }

    /**
     * Sets the value of the visitSuggestedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVisitSuggestedDate(String value) {
        this.visitSuggestedDate = value;
    }

    /**
     * Gets the value of the visitWindow property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVisitWindow() {
        return visitWindow;
    }

    /**
     * Sets the value of the visitWindow property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVisitWindow(String value) {
        this.visitWindow = value;
    }

}
