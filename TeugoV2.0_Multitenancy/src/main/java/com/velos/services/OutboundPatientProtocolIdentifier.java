
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for outboundPatientProtocolIdentifier complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="outboundPatientProtocolIdentifier">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://velos.com/services/}outboundIdentifier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "outboundPatientProtocolIdentifier", propOrder = {
    "id"
})
public class OutboundPatientProtocolIdentifier {

    protected OutboundIdentifier id;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link OutboundIdentifier }
     *     
     */
    public OutboundIdentifier getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link OutboundIdentifier }
     *     
     */
    public void setId(OutboundIdentifier value) {
        this.id = value;
    }

}
