
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for stafa_UpdatePatientEMR complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="stafa_UpdatePatientEMR">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="partientemr" type="{http://velos.com/services/}patientEMR" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "stafa_UpdatePatientEMR", propOrder = {
    "partientemr"
})
public class StafaUpdatePatientEMR {

    protected PatientEMR partientemr;

    /**
     * Gets the value of the partientemr property.
     * 
     * @return
     *     possible object is
     *     {@link PatientEMR }
     *     
     */
    public PatientEMR getPartientemr() {
        return partientemr;
    }

    /**
     * Sets the value of the partientemr property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientEMR }
     *     
     */
    public void setPartientemr(PatientEMR value) {
        this.partientemr = value;
    }

}
