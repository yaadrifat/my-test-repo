
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for searchUser complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="searchUser">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UserSearch" type="{http://velos.com/services/}userSearch" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "searchUser", propOrder = {
    "userSearch"
})
@XmlRootElement(name="searchUser", namespace = "http://velos.com/services/")
public class SearchUser {

    @XmlElement(name = "UserSearch")
    protected UserSearch userSearch;

    /**
     * Gets the value of the userSearch property.
     * 
     * @return
     *     possible object is
     *     {@link UserSearch }
     *     
     */
    public UserSearch getUserSearch() {
        return userSearch;
    }

    /**
     * Sets the value of the userSearch property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserSearch }
     *     
     */
    public void setUserSearch(UserSearch value) {
        this.userSearch = value;
    }

}
