
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for change complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="change">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="action" type="{http://velos.com/services/}crudAction" minOccurs="0"/>
 *         &lt;element name="formResponseType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="formType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="identifier" type="{http://velos.com/services/}studyIdentifier" minOccurs="0"/>
 *         &lt;element name="module" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="statusCodeDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="statusSubType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="teamRoleDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="teamRoleType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="timeStamp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="userIdentifier" type="{http://velos.com/services/}userIdentifier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "change", propOrder = {
    "action",
    "formResponseType",
    "formType",
    "identifier",
    "module",
    "statusCodeDesc",
    "statusSubType",
    "teamRoleDesc",
    "teamRoleType",
    "timeStamp",
    "userIdentifier"
})
public class Change {

    protected CrudAction action;
    protected String formResponseType;
    protected String formType;
    protected StudyIdentifier identifier;
    protected String module;
    protected String statusCodeDesc;
    protected String statusSubType;
    protected String teamRoleDesc;
    protected String teamRoleType;
    protected String timeStamp;
    protected UserIdentifier userIdentifier;

    /**
     * Gets the value of the action property.
     * 
     * @return
     *     possible object is
     *     {@link CrudAction }
     *     
     */
    public CrudAction getAction() {
        return action;
    }

    /**
     * Sets the value of the action property.
     * 
     * @param value
     *     allowed object is
     *     {@link CrudAction }
     *     
     */
    public void setAction(CrudAction value) {
        this.action = value;
    }

    /**
     * Gets the value of the formResponseType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormResponseType() {
        return formResponseType;
    }

    /**
     * Sets the value of the formResponseType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormResponseType(String value) {
        this.formResponseType = value;
    }

    /**
     * Gets the value of the formType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormType() {
        return formType;
    }

    /**
     * Sets the value of the formType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormType(String value) {
        this.formType = value;
    }

    /**
     * Gets the value of the identifier property.
     * 
     * @return
     *     possible object is
     *     {@link StudyIdentifier }
     *     
     */
    public StudyIdentifier getIdentifier() {
        return identifier;
    }

    /**
     * Sets the value of the identifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyIdentifier }
     *     
     */
    public void setIdentifier(StudyIdentifier value) {
        this.identifier = value;
    }

    /**
     * Gets the value of the module property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModule() {
        return module;
    }

    /**
     * Sets the value of the module property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModule(String value) {
        this.module = value;
    }

    /**
     * Gets the value of the statusCodeDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusCodeDesc() {
        return statusCodeDesc;
    }

    /**
     * Sets the value of the statusCodeDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusCodeDesc(String value) {
        this.statusCodeDesc = value;
    }

    /**
     * Gets the value of the statusSubType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusSubType() {
        return statusSubType;
    }

    /**
     * Sets the value of the statusSubType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusSubType(String value) {
        this.statusSubType = value;
    }

    /**
     * Gets the value of the teamRoleDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTeamRoleDesc() {
        return teamRoleDesc;
    }

    /**
     * Sets the value of the teamRoleDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTeamRoleDesc(String value) {
        this.teamRoleDesc = value;
    }

    /**
     * Gets the value of the teamRoleType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTeamRoleType() {
        return teamRoleType;
    }

    /**
     * Sets the value of the teamRoleType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTeamRoleType(String value) {
        this.teamRoleType = value;
    }

    /**
     * Gets the value of the timeStamp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimeStamp() {
        return timeStamp;
    }

    /**
     * Sets the value of the timeStamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeStamp(String value) {
        this.timeStamp = value;
    }

    /**
     * Gets the value of the userIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link UserIdentifier }
     *     
     */
    public UserIdentifier getUserIdentifier() {
        return userIdentifier;
    }

    /**
     * Sets the value of the userIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserIdentifier }
     *     
     */
    public void setUserIdentifier(UserIdentifier value) {
        this.userIdentifier = value;
    }

}
