
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OperationException complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OperationException">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="issues" type="{http://velos.com/services/}issues"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OperationException", propOrder = {
    "issues"
})
@XmlRootElement(name="OperationException", namespace = "http://velos.com/services/")
public class OperationException {

    @XmlElement(name = "issues",required = true, nillable = true)
    protected Issues issues;

    /**
     * Gets the value of the issues property.
     * 
     * @return
     *     possible object is
     *     {@link Issues }
     *     
     */
    public Issues getIssues() {
        return issues;
    }

    /**
     * Sets the value of the issues property.
     * 
     * @param value
     *     allowed object is
     *     {@link Issues }
     *     
     */
    public void setIssues(Issues value) {
        this.issues = value;
    }

}
