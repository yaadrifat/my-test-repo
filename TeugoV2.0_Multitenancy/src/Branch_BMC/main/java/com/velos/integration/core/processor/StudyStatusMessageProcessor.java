package com.velos.integration.core.processor;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import com.velos.epic.XmlParser;
import com.velos.integration.core.messaging.Change;
import com.velos.integration.core.messaging.CustomHandler;
import com.velos.integration.core.messaging.IdentifierConstants;
import com.velos.services.OperationException_Exception;
import com.velos.services.SimpleIdentifier;

public class StudyStatusMessageProcessor  {

	private static Logger logger = Logger.getLogger("epicLogger");
	
	//private Map<String, String> requestMap = new HashMap<String, String>();


	public void processStudyStatMessage(CustomHandler handler, String xml)
			throws OperationException_Exception {
		Map<String, String> requestMap = null;
		try{
			SimpleIdentifier simpleIdentifier = (SimpleIdentifier) handler
					.getParentOID().get(IdentifierConstants.SIMPLE_IDENTIFIER);
			List<Change> childHandlers = handler.getChanges();
			loop_a: for (Change childHandler : childHandlers) {
				logger.info("StudyStatusMessageProcessor - processStudyStatMessage() Module == "+childHandler.getModule());
				if("create".equalsIgnoreCase(childHandler.getAction()) && "study_status".equalsIgnoreCase(childHandler.getModule())){
					logger.info("StudyStatusMessageProcessor - processStudyStatMessage() Status Child Identifier OID == "+childHandler.getIdentifier().getOID());
					logger.info("StudyStatusMessageProcessor - processStudyStatMessage() Status Action == "+childHandler.getAction());
					requestMap = new HashMap<String, String>();
					requestMap.put("childOID", childHandler.getIdentifier().getOID());
					requestMap.put("action", childHandler.getAction());
					requestMap.put("module", childHandler.getModule());
					requestMap.put("statusSubType",childHandler.getAdditionalTags().get("statusSubType"));
					requestMap.put("statusCodeDesc",childHandler.getAdditionalTags().get("statusCodeDesc"));
					break loop_a;
				}
				if("update".equalsIgnoreCase(childHandler.getAction()) && "study_summary".equalsIgnoreCase(childHandler.getModule())){
					logger.info("StudyStatusMessageProcessor - processStudyStatMessage()  Child Identifier OID == "+childHandler.getIdentifier().getOID());
					logger.info("StudyStatusMessageProcessor - processStudyStatMessage() for UPDATE Action == "+childHandler.getAction());
					requestMap = new HashMap<String, String>();
					requestMap.put("childOID", childHandler.getIdentifier().getOID());
					requestMap.put("action", childHandler.getAction());
					requestMap.put("module", childHandler.getModule());
				
					break loop_a;
				}
			
			}
			if(requestMap!=null){
				requestMap.put("OID", simpleIdentifier.getOID());
		
				studyRequest(requestMap, xml);
			}else{
				logger.info(requestMap);
				logger.info("Study Action is not either CREATE or UPDATE OR Study Module is not study_status or study_summary module");
			}
		}catch(Exception e){
			logger.error("Error while setting action and module details : "+e);
		}
		
	}

	//Commented by Rajasekhar
	/*public void processEnrollpatStudyStatMessage(String epsXml){
		XmlParser xmlParser = new XmlParser();
		try {
			xmlParser.enrollPatStudyXmlParser(epsXml);
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
*/

	public void studyRequest(Map<String, String> requestMap, String xml)
			throws OperationException_Exception {
	
		logger.info("StudyStatusMessageProcessor -In  studyRequest()");

		try{
			
			Properties prop = new Properties();
			
			prop.load(this.getClass().getClassLoader().getResourceAsStream("epic.properties"));
			
			if("create".equalsIgnoreCase(requestMap.get("action")) && "study_status".equalsIgnoreCase(requestMap.get("module"))){
			
				String studyOID = requestMap.get("OID");
				logger.info("Study OID : " + studyOID);
				String statusOID = requestMap.get("childOID");
				logger.info("Study Status OID : " + statusOID);
				String action = requestMap.get("action");
				logger.info("Change Action : " + action);
			
				String module = requestMap.get("module");
				String statusSubType = requestMap.get("statusSubType");
				String statusCodeDesc = requestMap.get("statusCodeDesc");
				logger.info("Study module     : "+module);
				logger.info("StatusSubType    : "+statusSubType);
				logger.info("StatusCodeDesc   : "+statusCodeDesc);
			
				if(statusSubType.equals(prop.getProperty("study.statusSubType")) && statusCodeDesc.equals(prop.getProperty("study.status"))){
					//Call the appropriate methods to send study creation message to epic by using Study OID
					XmlParser xmlParser = new XmlParser();
					try {
						xmlParser.studyXmlParser(requestMap,xml);
					} catch (ParserConfigurationException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (SAXException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else{
					logger.info("Study Status is not "+prop.getProperty("study.status"));
					logger.info("Study Status is "+statusCodeDesc);
				}
			}
			
			else if("update".equalsIgnoreCase(requestMap.get("action")) && "study_summary".equalsIgnoreCase(requestMap.get("module"))){
					String studyOID = requestMap.get("OID");
					logger.info("Study OID : " + studyOID);
					
				
					String action = requestMap.get("action");
					logger.info("Change Action : " + action);
					requestMap.put("StudyAction",action);
					String module = requestMap.get("module");
					logger.info("Study module     : "+module);
					
					//Call the appropriate methods to send study  message with updated study data to epic by using Study OID
					XmlParser xmlParser = new XmlParser();
					try {
						xmlParser.studyXmlParser(requestMap,xml);
					} catch (ParserConfigurationException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (SAXException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
			//Commented by Rajasekhar
			/*String studyNumber = getStudyNumber(xml);
			EpicEndpointClient eepc=new EpicEndpointClient();
			try {
				eepc.sendRequest(studyNumber);
			} catch (Exception e) {
				e.printStackTrace();
			}*/
		}catch(Exception e){
			logger.error("Error while processing the Study : "+e);
		}
		
	}
	
	//Commented by Rajasekhar
	/*private static String getStudyNumber(String xml) {
		String studyNumber = "";
		try{
			DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xml));
			XPath xPath =  XPathFactory.newInstance().newXPath();
			  
			String path = "/changes/change/studyNumber";
			Document doc = db.parse(is);
			NodeList nodeList = (NodeList) xPath.compile(path).evaluate(doc, XPathConstants.NODESET);
			  
			System.out.println("getStudyNumber");
			int count = nodeList.getLength();
			System.out.println(count);
			Node n =nodeList.item(0);
			//String nodeName = n.getNodeName();
			studyNumber = n.getTextContent();
			//System.out.println(nodeName+"=="+nodeValue);
		}catch(Exception e){
			e.printStackTrace();
		}
		return studyNumber;
	}*/
   
}