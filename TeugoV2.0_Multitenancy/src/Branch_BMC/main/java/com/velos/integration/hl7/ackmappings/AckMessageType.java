package com.velos.integration.hl7.ackmappings;

public enum AckMessageType {
	
	MessageTypeRequired("Message Type Is a mandatoryfield"),
	AckError("Error while processing the message"),
	AckAccept("Acknowledgement Accepted")
	;
	
	private String key;
	private AckMessageType(String key) {
		this.key=key;
	}
	
	public String toString(){
		return key;
	}

}
