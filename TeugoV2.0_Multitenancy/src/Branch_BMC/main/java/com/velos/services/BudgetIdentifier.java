
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for budgetIdentifier complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="budgetIdentifier">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}simpleIdentifier">
 *       &lt;sequence>
 *         &lt;element name="budgetName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="budgetVersion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "budgetIdentifier", propOrder = {
    "budgetName",
    "budgetVersion"
})
public class BudgetIdentifier
    extends SimpleIdentifier
{

    protected String budgetName;
    protected String budgetVersion;

    /**
     * Gets the value of the budgetName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBudgetName() {
        return budgetName;
    }

    /**
     * Sets the value of the budgetName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBudgetName(String value) {
        this.budgetName = value;
    }

    /**
     * Gets the value of the budgetVersion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBudgetVersion() {
        return budgetVersion;
    }

    /**
     * Sets the value of the budgetVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBudgetVersion(String value) {
        this.budgetVersion = value;
    }

}
