
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for addMPatientSchedules complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="addMPatientSchedules">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PatientSchedules" type="{http://velos.com/services/}mPatientSchedules" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "addMPatientSchedules", propOrder = {
    "patientSchedules"
})
public class AddMPatientSchedules {

    @XmlElement(name = "PatientSchedules")
    protected MPatientSchedules patientSchedules;

    /**
     * Gets the value of the patientSchedules property.
     * 
     * @return
     *     possible object is
     *     {@link MPatientSchedules }
     *     
     */
    public MPatientSchedules getPatientSchedules() {
        return patientSchedules;
    }

    /**
     * Sets the value of the patientSchedules property.
     * 
     * @param value
     *     allowed object is
     *     {@link MPatientSchedules }
     *     
     */
    public void setPatientSchedules(MPatientSchedules value) {
        this.patientSchedules = value;
    }

}
