
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for studyPatientFormResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="studyPatientFormResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}formResponse">
 *       &lt;sequence>
 *         &lt;element name="patientIdentifier" type="{http://velos.com/services/}patientIdentifier" minOccurs="0"/>
 *         &lt;element name="patientProtocolIdentifier" type="{http://velos.com/services/}patientProtocolIdentifier" minOccurs="0"/>
 *         &lt;element name="studyIdentifier" type="{http://velos.com/services/}studyIdentifier" minOccurs="0"/>
 *         &lt;element name="systemID" type="{http://velos.com/services/}simpleIdentifier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "studyPatientFormResponse", propOrder = {
    "patientIdentifier",
    "patientProtocolIdentifier",
    "studyIdentifier",
    "systemID"
})
@XmlSeeAlso({
    StudyPatientScheduleFormResponse.class
})
public class StudyPatientFormResponse
    extends FormResponse
{

    protected PatientIdentifier patientIdentifier;
    protected PatientProtocolIdentifier patientProtocolIdentifier;
    protected StudyIdentifier studyIdentifier;
    protected StudyPatientFormResponseIdentifier systemID;

    /**
     * Gets the value of the patientIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link PatientIdentifier }
     *     
     */
    public PatientIdentifier getPatientIdentifier() {
        return patientIdentifier;
    }

    /**
     * Sets the value of the patientIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientIdentifier }
     *     
     */
    public void setPatientIdentifier(PatientIdentifier value) {
        this.patientIdentifier = value;
    }

    /**
     * Gets the value of the patientProtocolIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link PatientProtocolIdentifier }
     *     
     */
    public PatientProtocolIdentifier getPatientProtocolIdentifier() {
        return patientProtocolIdentifier;
    }

    /**
     * Sets the value of the patientProtocolIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientProtocolIdentifier }
     *     
     */
    public void setPatientProtocolIdentifier(PatientProtocolIdentifier value) {
        this.patientProtocolIdentifier = value;
    }

    /**
     * Gets the value of the studyIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link StudyIdentifier }
     *     
     */
    public StudyIdentifier getStudyIdentifier() {
        return studyIdentifier;
    }

    /**
     * Sets the value of the studyIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyIdentifier }
     *     
     */
    public void setStudyIdentifier(StudyIdentifier value) {
        this.studyIdentifier = value;
    }

    /**
     * Gets the value of the systemID property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleIdentifier }
     *     
     */
    public StudyPatientFormResponseIdentifier getSystemID() {
        return systemID;
    }

    /**
     * Sets the value of the systemID property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleIdentifier }
     *     
     */
    public void setSystemID(StudyPatientFormResponseIdentifier value) {
        this.systemID = value;
    }

}
