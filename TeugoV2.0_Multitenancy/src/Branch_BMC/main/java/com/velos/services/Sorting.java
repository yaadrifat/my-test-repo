
package com.velos.services;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for sorting.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="sorting">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PatientID"/>
 *     &lt;enumeration value="PatFirstName"/>
 *     &lt;enumeration value="PatLastName"/>
 *     &lt;enumeration value="Age"/>
 *     &lt;enumeration value="Gender"/>
 *     &lt;enumeration value="SurvivalStatus"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "sorting")
@XmlEnum
public enum Sorting {

    @XmlEnumValue("PatientID")
    PATIENT_ID("PatientID"),
    @XmlEnumValue("PatFirstName")
    PAT_FIRST_NAME("PatFirstName"),
    @XmlEnumValue("PatLastName")
    PAT_LAST_NAME("PatLastName"),
    @XmlEnumValue("Age")
    AGE("Age"),
    @XmlEnumValue("Gender")
    GENDER("Gender"),
    @XmlEnumValue("SurvivalStatus")
    SURVIVAL_STATUS("SurvivalStatus");
    private final String value;

    Sorting(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static Sorting fromValue(String v) {
        for (Sorting c: Sorting.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
