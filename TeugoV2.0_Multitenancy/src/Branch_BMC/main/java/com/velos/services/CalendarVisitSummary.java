
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for calendarVisitSummary complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="calendarVisitSummary">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="absoluteInterval" type="{http://velos.com/services/}duration" minOccurs="0"/>
 *         &lt;element name="noIntervalDefined" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="parentIdentifier" type="{http://velos.com/services/}parentIdentifier" minOccurs="0"/>
 *         &lt;element name="relativeVisitInterval" type="{http://velos.com/services/}duration" minOccurs="0"/>
 *         &lt;element name="relativeVisitName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="visitDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="visitIdentifier" type="{http://velos.com/services/}visitIdentifier" minOccurs="0"/>
 *         &lt;element name="visitName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="visitSequence" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="visitWindowAfter" type="{http://velos.com/services/}duration" minOccurs="0"/>
 *         &lt;element name="visitWindowBefore" type="{http://velos.com/services/}duration" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "calendarVisitSummary", propOrder = {
    "absoluteInterval",
    "noIntervalDefined",
    "parentIdentifier",
    "relativeVisitInterval",
    "relativeVisitName",
    "visitDescription",
    "visitIdentifier",
    "visitName",
    "visitSequence",
    "visitWindowAfter",
    "visitWindowBefore"
})
public class CalendarVisitSummary
    extends ServiceObject
{

    protected Duration absoluteInterval;
    protected Boolean noIntervalDefined;
    protected ParentIdentifier parentIdentifier;
    protected Duration relativeVisitInterval;
    protected String relativeVisitName;
    protected String visitDescription;
    protected VisitIdentifier visitIdentifier;
    protected String visitName;
    protected Integer visitSequence;
    protected Duration visitWindowAfter;
    protected Duration visitWindowBefore;

    /**
     * Gets the value of the absoluteInterval property.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getAbsoluteInterval() {
        return absoluteInterval;
    }

    /**
     * Sets the value of the absoluteInterval property.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setAbsoluteInterval(Duration value) {
        this.absoluteInterval = value;
    }

    /**
     * Gets the value of the noIntervalDefined property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNoIntervalDefined() {
        return noIntervalDefined;
    }

    /**
     * Sets the value of the noIntervalDefined property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNoIntervalDefined(Boolean value) {
        this.noIntervalDefined = value;
    }

    /**
     * Gets the value of the parentIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link ParentIdentifier }
     *     
     */
    public ParentIdentifier getParentIdentifier() {
        return parentIdentifier;
    }

    /**
     * Sets the value of the parentIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link ParentIdentifier }
     *     
     */
    public void setParentIdentifier(ParentIdentifier value) {
        this.parentIdentifier = value;
    }

    /**
     * Gets the value of the relativeVisitInterval property.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getRelativeVisitInterval() {
        return relativeVisitInterval;
    }

    /**
     * Sets the value of the relativeVisitInterval property.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setRelativeVisitInterval(Duration value) {
        this.relativeVisitInterval = value;
    }

    /**
     * Gets the value of the relativeVisitName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelativeVisitName() {
        return relativeVisitName;
    }

    /**
     * Sets the value of the relativeVisitName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelativeVisitName(String value) {
        this.relativeVisitName = value;
    }

    /**
     * Gets the value of the visitDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVisitDescription() {
        return visitDescription;
    }

    /**
     * Sets the value of the visitDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVisitDescription(String value) {
        this.visitDescription = value;
    }

    /**
     * Gets the value of the visitIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link VisitIdentifier }
     *     
     */
    public VisitIdentifier getVisitIdentifier() {
        return visitIdentifier;
    }

    /**
     * Sets the value of the visitIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link VisitIdentifier }
     *     
     */
    public void setVisitIdentifier(VisitIdentifier value) {
        this.visitIdentifier = value;
    }

    /**
     * Gets the value of the visitName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVisitName() {
        return visitName;
    }

    /**
     * Sets the value of the visitName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVisitName(String value) {
        this.visitName = value;
    }

    /**
     * Gets the value of the visitSequence property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getVisitSequence() {
        return visitSequence;
    }

    /**
     * Sets the value of the visitSequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setVisitSequence(Integer value) {
        this.visitSequence = value;
    }

    /**
     * Gets the value of the visitWindowAfter property.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getVisitWindowAfter() {
        return visitWindowAfter;
    }

    /**
     * Sets the value of the visitWindowAfter property.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setVisitWindowAfter(Duration value) {
        this.visitWindowAfter = value;
    }

    /**
     * Gets the value of the visitWindowBefore property.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getVisitWindowBefore() {
        return visitWindowBefore;
    }

    /**
     * Sets the value of the visitWindowBefore property.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setVisitWindowBefore(Duration value) {
        this.visitWindowBefore = value;
    }

}
