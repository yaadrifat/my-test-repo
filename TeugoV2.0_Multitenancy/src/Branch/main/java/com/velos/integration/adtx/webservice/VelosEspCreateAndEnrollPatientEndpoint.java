package com.velos.integration.adtx.webservice;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlType;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import com.velos.services.OperationException_Exception;
import com.velos.services.OperationRolledBackException_Exception;
import com.velos.services.ResponseHolder;

@WebService(targetNamespace="http://velos.com/services/", name="StudyPatientSEI")
public interface VelosEspCreateAndEnrollPatientEndpoint {
	
	@WebResult(name="ResponseHolder", targetNamespace="")
	@RequestWrapper(localName="createAndEnrollPatient",targetNamespace="http://velos.com/services/",className="com.velos.services.CreateAndEnrollPatient")
	@WebMethod
	@ResponseWrapper(localName="createAndEnrollPatientResponse",targetNamespace="",className="com.velos.services.CreateAndEnrollPatientResponse")
	
	public com.velos.services.ResponseHolder createAndEnrollPatient(
			@WebParam( name ="Patient",targetNamespace = "") com.velos.services.Patient patient, 
			@WebParam( name ="StudyIdentifier",targetNamespace = "") com.velos.services.StudyIdentifier studyIdentifier,
			@WebParam( name ="PatientEnrollmentDetails",targetNamespace = "")com.velos.services.PatientEnrollmentDetails patientEnrollmentDetails
			)throws OperationException_Exception, OperationRolledBackException_Exception;

}
