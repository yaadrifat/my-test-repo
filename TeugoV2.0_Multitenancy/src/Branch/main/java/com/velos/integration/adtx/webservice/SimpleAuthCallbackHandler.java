package com.velos.integration.adtx.webservice;

import java.io.IOException;
import java.util.Properties;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;

import org.apache.log4j.Logger;
import org.apache.ws.security.WSPasswordCallback;



public class SimpleAuthCallbackHandler implements CallbackHandler {

	public void handle(Callback[] callbacks) {
		Properties prop = new Properties();
		try {
			prop.load(this.getClass().getClassLoader()
					.getResourceAsStream("config.properties"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*logger.info("inside SimpleAuthCallbackHandler");
		logger.info("user - " + prop
				.getProperty("velos.userID"));
		logger.info("password - " + prop
				.getProperty("velos.password"));*/
		WSPasswordCallback pc = (WSPasswordCallback) callbacks[0];
		if (pc.getIdentifier().equals(prop
				.getProperty("velos.userID"))) {
			pc.setPassword(prop
					.getProperty("velos.password"));
		}
	}
}
