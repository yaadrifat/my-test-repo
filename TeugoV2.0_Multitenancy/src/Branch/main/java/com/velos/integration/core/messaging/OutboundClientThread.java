package com.velos.integration.core.messaging;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Properties;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.jms.TopicSubscriber;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class OutboundClientThread implements Runnable {

	private static Logger logger = Logger.getLogger("epicLogger");

	@Override
	public void run() {
		Properties prop = new Properties();
		
		try {
			prop.load(this.getClass().getClassLoader().getResourceAsStream("config.properties"));
		} catch (IOException e1) {
			logger.error("***config.properties file missmatched***");		}
		if(prop.containsKey("enable_Epic_Interface") == true && prop.getProperty("enable_Epic_Interface").equalsIgnoreCase("y")
				&& !"".equals(prop.getProperty("enable_Epic_Interface"))){
		
		logger.info("Inside run method : ");
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
				"ctms-config.xml");
		OutboundClient client = (OutboundClient) context
				.getBean("outboundClient");
		logger.info("OutboundClient");
		
		try {
			client.run();
		} catch (Exception e) {
			logger.info("error : " + e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			context.close();
		}
		}
	}
}
