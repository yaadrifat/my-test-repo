
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getSitesOfServiceResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getSitesOfServiceResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getSitesOfService" type="{http://velos.com/services/}sitesOfService" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getSitesOfServiceResponse", propOrder = {
    "getSitesOfService"
})
@XmlRootElement(name="getSitesOfServiceResponse", namespace = "http://velos.com/services/")
public class GetSitesOfServiceResponse {

    protected SitesOfService getSitesOfService;

    /**
     * Gets the value of the getSitesOfService property.
     * 
     * @return
     *     possible object is
     *     {@link SitesOfService }
     *     
     */
    public SitesOfService getGetSitesOfService() {
        return getSitesOfService;
    }

    /**
     * Sets the value of the getSitesOfService property.
     * 
     * @param value
     *     allowed object is
     *     {@link SitesOfService }
     *     
     */
    public void setGetSitesOfService(SitesOfService value) {
        this.getSitesOfService = value;
    }

}
