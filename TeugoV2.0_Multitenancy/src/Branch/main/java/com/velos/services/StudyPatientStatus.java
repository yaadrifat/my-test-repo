
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for studyPatientStatus complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="studyPatientStatus">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="CUD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="currentStatus" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="patStudyId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="statusDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="statusNote" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="statusReason" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="studyPatStatId" type="{http://velos.com/services/}patientStudyStatusIdentifier" minOccurs="0"/>
 *         &lt;element name="studyPatStatus" type="{http://velos.com/services/}code" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "studyPatientStatus", propOrder = {
    "cud",
    "currentStatus",
    "patStudyId",
    "statusDate",
    "statusNote",
    "statusReason",
    "studyPatStatId",
    "studyPatStatus"
})
public class StudyPatientStatus
    extends ServiceObject
{

    @XmlElement(name = "CUD")
    protected String cud;
    protected boolean currentStatus;
    protected String patStudyId;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar statusDate;
    protected String statusNote;
    protected Code statusReason;
    protected PatientStudyStatusIdentifier studyPatStatId;
    protected Code studyPatStatus;

    /**
     * Gets the value of the cud property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCUD() {
        return cud;
    }

    /**
     * Sets the value of the cud property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCUD(String value) {
        this.cud = value;
    }

    /**
     * Gets the value of the currentStatus property.
     * 
     */
    public boolean isCurrentStatus() {
        return currentStatus;
    }

    /**
     * Sets the value of the currentStatus property.
     * 
     */
    public void setCurrentStatus(boolean value) {
        this.currentStatus = value;
    }

    /**
     * Gets the value of the patStudyId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPatStudyId() {
        return patStudyId;
    }

    /**
     * Sets the value of the patStudyId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPatStudyId(String value) {
        this.patStudyId = value;
    }

    /**
     * Gets the value of the statusDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStatusDate() {
        return statusDate;
    }

    /**
     * Sets the value of the statusDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStatusDate(XMLGregorianCalendar value) {
        this.statusDate = value;
    }

    /**
     * Gets the value of the statusNote property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusNote() {
        return statusNote;
    }

    /**
     * Sets the value of the statusNote property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusNote(String value) {
        this.statusNote = value;
    }

    /**
     * Gets the value of the statusReason property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getStatusReason() {
        return statusReason;
    }

    /**
     * Sets the value of the statusReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setStatusReason(Code value) {
        this.statusReason = value;
    }

    /**
     * Gets the value of the studyPatStatId property.
     * 
     * @return
     *     possible object is
     *     {@link PatientStudyStatusIdentifier }
     *     
     */
    public PatientStudyStatusIdentifier getStudyPatStatId() {
        return studyPatStatId;
    }

    /**
     * Sets the value of the studyPatStatId property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientStudyStatusIdentifier }
     *     
     */
    public void setStudyPatStatId(PatientStudyStatusIdentifier value) {
        this.studyPatStatId = value;
    }

    /**
     * Gets the value of the studyPatStatus property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getStudyPatStatus() {
        return studyPatStatus;
    }

    /**
     * Sets the value of the studyPatStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setStudyPatStatus(Code value) {
        this.studyPatStatus = value;
    }

}
