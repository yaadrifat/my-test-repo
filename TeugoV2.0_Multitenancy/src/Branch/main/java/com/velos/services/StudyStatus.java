
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for studyStatus complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="studyStatus">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="assignedTo" type="{http://velos.com/services/}userIdentifier" minOccurs="0"/>
 *         &lt;element name="currentForStudy" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="documentedBy" type="{http://velos.com/services/}userIdentifier" minOccurs="0"/>
 *         &lt;element name="meetingDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="notes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="organizationId" type="{http://velos.com/services/}organizationIdentifier" minOccurs="0"/>
 *         &lt;element name="outcome" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="parentIdentifier" type="{http://velos.com/services/}studyIdentifier" minOccurs="0"/>
 *         &lt;element name="reviewBoard" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="status" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="statusType" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="studyStatusId" type="{http://velos.com/services/}studyStatusIdentifier" minOccurs="0"/>
 *         &lt;element name="validFromDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="validUntilDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "studyStatus", propOrder = {
    "assignedTo",
    "currentForStudy",
    "documentedBy",
    "meetingDate",
    "notes",
    "organizationId",
    "outcome",
    "parentIdentifier",
    "reviewBoard",
    "status",
    "statusType",
    "studyStatusId",
    "validFromDate",
    "validUntilDate"
})
public class StudyStatus
    extends ServiceObject
{

    protected UserIdentifier assignedTo;
    protected boolean currentForStudy;
    protected UserIdentifier documentedBy;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar meetingDate;
    protected String notes;
    protected OrganizationIdentifier organizationId;
    protected Code outcome;
    protected StudyIdentifier parentIdentifier;
    protected Code reviewBoard;
    protected Code status;
    protected Code statusType;
    protected StudyStatusIdentifier studyStatusId;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar validFromDate;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar validUntilDate;

    /**
     * Gets the value of the assignedTo property.
     * 
     * @return
     *     possible object is
     *     {@link UserIdentifier }
     *     
     */
    public UserIdentifier getAssignedTo() {
        return assignedTo;
    }

    /**
     * Sets the value of the assignedTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserIdentifier }
     *     
     */
    public void setAssignedTo(UserIdentifier value) {
        this.assignedTo = value;
    }

    /**
     * Gets the value of the currentForStudy property.
     * 
     */
    public boolean isCurrentForStudy() {
        return currentForStudy;
    }

    /**
     * Sets the value of the currentForStudy property.
     * 
     */
    public void setCurrentForStudy(boolean value) {
        this.currentForStudy = value;
    }

    /**
     * Gets the value of the documentedBy property.
     * 
     * @return
     *     possible object is
     *     {@link UserIdentifier }
     *     
     */
    public UserIdentifier getDocumentedBy() {
        return documentedBy;
    }

    /**
     * Sets the value of the documentedBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserIdentifier }
     *     
     */
    public void setDocumentedBy(UserIdentifier value) {
        this.documentedBy = value;
    }

    /**
     * Gets the value of the meetingDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getMeetingDate() {
        return meetingDate;
    }

    /**
     * Sets the value of the meetingDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMeetingDate(XMLGregorianCalendar value) {
        this.meetingDate = value;
    }

    /**
     * Gets the value of the notes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNotes() {
        return notes;
    }

    /**
     * Sets the value of the notes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNotes(String value) {
        this.notes = value;
    }

    /**
     * Gets the value of the organizationId property.
     * 
     * @return
     *     possible object is
     *     {@link OrganizationIdentifier }
     *     
     */
    public OrganizationIdentifier getOrganizationId() {
        return organizationId;
    }

    /**
     * Sets the value of the organizationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganizationIdentifier }
     *     
     */
    public void setOrganizationId(OrganizationIdentifier value) {
        this.organizationId = value;
    }

    /**
     * Gets the value of the outcome property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getOutcome() {
        return outcome;
    }

    /**
     * Sets the value of the outcome property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setOutcome(Code value) {
        this.outcome = value;
    }

    /**
     * Gets the value of the parentIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link StudyIdentifier }
     *     
     */
    public StudyIdentifier getParentIdentifier() {
        return parentIdentifier;
    }

    /**
     * Sets the value of the parentIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyIdentifier }
     *     
     */
    public void setParentIdentifier(StudyIdentifier value) {
        this.parentIdentifier = value;
    }

    /**
     * Gets the value of the reviewBoard property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getReviewBoard() {
        return reviewBoard;
    }

    /**
     * Sets the value of the reviewBoard property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setReviewBoard(Code value) {
        this.reviewBoard = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setStatus(Code value) {
        this.status = value;
    }

    /**
     * Gets the value of the statusType property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getStatusType() {
        return statusType;
    }

    /**
     * Sets the value of the statusType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setStatusType(Code value) {
        this.statusType = value;
    }

    /**
     * Gets the value of the studyStatusId property.
     * 
     * @return
     *     possible object is
     *     {@link StudyStatusIdentifier }
     *     
     */
    public StudyStatusIdentifier getStudyStatusId() {
        return studyStatusId;
    }

    /**
     * Sets the value of the studyStatusId property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyStatusIdentifier }
     *     
     */
    public void setStudyStatusId(StudyStatusIdentifier value) {
        this.studyStatusId = value;
    }

    /**
     * Gets the value of the validFromDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getValidFromDate() {
        return validFromDate;
    }

    /**
     * Sets the value of the validFromDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setValidFromDate(XMLGregorianCalendar value) {
        this.validFromDate = value;
    }

    /**
     * Gets the value of the validUntilDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getValidUntilDate() {
        return validUntilDate;
    }

    /**
     * Sets the value of the validUntilDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setValidUntilDate(XMLGregorianCalendar value) {
        this.validUntilDate = value;
    }

}
