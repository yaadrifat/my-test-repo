package com.velos.integration.hl7.processor;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Logger;

import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.util.Terser;

import com.velos.integration.hl7.bean.FieldMapBean;
import com.velos.integration.hl7.bean.MessageFieldBean;
import com.velos.integration.hl7.bean.SegmentBean;
import com.velos.integration.hl7.dao.MessageConfiguration;
import com.velos.integration.hl7.dao.MessageDao;
import com.velos.integration.hl7.notifications.EmailNotification;
import com.velos.integration.hl7.notifications.HL7CustomException;


public class InterfaceUtil {
	private static Logger logger = Logger
			.getLogger(Hl7Processor.class);

	private MessageConfiguration messageconfiguration;
	Properties prop=new Properties();
	int count=0;
	private MessageDao messagedao;
	private EmailNotification emailNotification ;

	public MessageConfiguration getMessageconfiguration() {
		return messageconfiguration;
	}
	public void setMessageconfiguration(MessageConfiguration messageconfiguration) {
		this.messageconfiguration = messageconfiguration;
	}
	public MessageDao getMessagedao() {
		return messagedao;
	}
	public void setMessagedao(MessageDao messagedao) {
		this.messagedao = messagedao;
	}
	public EmailNotification getEmailNotification() {
		return emailNotification;
	}
	public void setEmailNotification(EmailNotification emailNotification) {
		this.emailNotification = emailNotification;
	}


	public List<String> segmentList(Message input){

		List<String> segList=new ArrayList<String>();

		String[] segments =  input.toString().split("\r");

		for(String segName:segments){

			segList.add(segName.substring(0,3));
		}

		return segList;
	}

	public Set<String> repeatedSegmentList(List<String> segList){

		Set<String> repSegList=new HashSet<String>();
		try {
			for(SegmentBean sb:getMessageconfiguration().getSegmentConfigurationBean()){


				for(String segName:segList){
					if(Integer.parseInt(sb.getRepeated())>1 && sb.getSegmentName().equals(segName)){
						repSegList.add(segName);
						//System.out.println("Repeated Segments =========>"+ segName);
					}
				}

			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		return repSegList;
	}


	// Getting Field Values  based on whether it is a single occurance or repeated field/segment
	public Map<String, Map> getFieldValue(Message input,Map messageMap) throws Exception,IOException{

		Map<String, Map> finalMap = new HashMap<String, Map>();
		Map<String,String> resultMap=new HashMap<String,String>();
		//Map<String,String[]> repeatedValueMap= new HashMap<String, String[]>();
		Map<String,String[]> repeatedValueMap=null;

		//Map<String,String> tableNameMap=new HashMap<String,String>();
		Map<String,Map> repSegValMap=new HashMap<String,Map>();


		try{

			InputStream inputStream = getClass().getClassLoader().getResourceAsStream("adt.properties");
			prop.load(inputStream);
			String messagetype=(String)messageMap.get("messagetype");

			List<String>segList=segmentList(input);
			Set<String>repSegList = repeatedSegmentList(segList);
			System.out.println("********** After Repeated Segment List **********"+repSegList);

			for(MessageFieldBean mf:getMessageconfiguration().getMessageFieldsConfiguration()){

				if(mf.getMessageType().equals(messagetype)) {

					String terserPath=mf.getTerserPath();

					int  repeated = Integer.parseInt(mf.getRepeated()); 
					if(terserPath==null && mf.getMappingType() !=null){
						// Getting Field Mapping Values
						if(segList.contains(mf.getSegmentName())){
							String mappedValue=getFieldValues(mf.getMappingType(),mf.getMappingField(),input);
							resultMap.put(mf.getColumnName(),mappedValue);
						}
					}else if(segList.contains(mf.getSegmentName())){
						String[] result = null;
						int repeat=0;
						String repeatedSegment="";
						String fieldValue = "";
						String repSegTableName="";List<String> repeatedList = new ArrayList<String>();
						for(SegmentBean sb:getMessageconfiguration().getSegmentConfigurationBean()){
							if(sb.getMessageType().equals(messagetype)){
								int segmentCount=0;
								if(repSegList.contains(sb.getSegmentName())){	
									for(String seg : segList){
										if(seg.equalsIgnoreCase(sb.getSegmentName())){
											segmentCount = segmentCount+1;
										}
									}
									System.out.println("SegmentCount=====>"+segmentCount);
									if(repSegList.contains(sb.getSegmentName())){
										repeatedValueMap = new HashMap<String, String[]>();
										for(MessageFieldBean mfb:getMessageconfiguration().getMessageFieldsConfiguration()){
											if(mfb.getMessageType().equals(messagetype)){
												if(mfb.getSegmentName().equals(sb.getSegmentName()) ){
													//GetRepeatedSegmentValues
													result=getRepeatedSegments(mfb.getTerserPath(),input,segmentCount,mfb.getMappingField(),mfb.getMappingType(),mfb.getRepeated(),mfb.getFieldName());
													if(mfb.getRepeatedColumn() != null){
														repeatedValueMap.put(mfb.getColumnName(),result);
													}else{
														repeatedValueMap.put(mfb.getColumnName(),result);
													}

													System.out.println("Column Name=====>"+mfb.getColumnName()+"==="+"TerserPath=====>"+mfb.getTerserPath());
												}
											}
										}
										repSegList.remove(sb.getSegmentName());
									}



									repSegValMap.put(sb.getSegmentName(),repeatedValueMap);

									//	tableNameMap.put("RepSegTableName",repSegTableName);
									//	tableNameMap.put("RepeatedNo",repeat+"");

								}else{

									//	tableNameMap.put("RepeatedNo",repeat+"");
								}
							}
						}
						if(repeated>1){

							//Repeated Field Values
							fieldValue= getFieldValues(terserPath,input,repeated);
							System.out.println("RepeatedField Value ========>"+fieldValue);
							logger.info("RepeatedField Value ========>"+fieldValue);
							if(fieldValue !=null){
							String[] field=fieldValue.split("~");
							String value="";

							resultMap.put(mf.getColumnName(),field[0]);



							for(int i=1;i<field.length;i++){
								if(i==1){
									value=value+field[i];
								}
								else{
									value=value+"~"+field[i];
								}

							}
							resultMap.put(mf.getRepeatedColumn(),value);
						}
						}else if(!mf.getSegmentName().equals(repeatedSegment)){

							// Getting Single Occuance Field Value
							fieldValue=getFieldValues(terserPath,input);
							//Checking and converting into date format 
							if(mf.getFieldDatatype()!=null && mf.getFieldDatatype().equalsIgnoreCase("date")&&fieldValue!=null){
								fieldValue=ChangeDateFormat(fieldValue);
							}
							resultMap.put(mf.getColumnName(),fieldValue);
						}
					}
				}
			}
			finalMap.put("ResultMap", resultMap);

			finalMap.put("RepeatedValueMap", repeatedValueMap);
			//finalMap.put("RepSegTableNameMap",tableNameMap);
			finalMap.put("RepSegValMap",repSegValMap);
			System.out.println("repeateFValMapFinalMap--==============->"+repSegValMap);
			inputStream.close();
			return finalMap;
		} catch (Exception e) {
			logger.info("********Error while getting result Map values**********");
			e.printStackTrace();
			throw new HL7CustomException(e.getMessage(),prop.getProperty("AcknowledgementReject"));
		}



	}


	/*****
	 * @ Repeated segment Method
	 * @param terserPath
	 * @param input
	 * @param repeat
	 * @param mappingField
	 * @param mappingType
	 * @return
	 * @throws Exception
	 */

	// Repeated Segment Values
	public String[] getRepeatedSegments(String terserPath,Message input,int repeat,String mappingField,String mappingType,String repeatedFildCnt,String repFieldName) throws Exception  {
		String[] result = null;
		result=new String[repeat];
		logger.info("RepeatedSegment Values");
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream("adt.properties");
		prop.load(inputStream);
		//String orgMapValue = null;
		String fieldName = null;
		int repeatedFieldCnt= 0;

		try {
			Terser terser = new Terser(input);

			if(terserPath == null){

				for(MessageFieldBean mf:getMessageconfiguration().getMessageFieldsConfiguration()){
					//System.out.println("FieldName---->"+mf.getFieldName()+"     "+mappingField);
					if(mappingField.equalsIgnoreCase(mf.getFieldName())){
						terserPath = mf.getTerserPath();
						//orgMapValue=terser.get(mf.getTerserPath());
						fieldName=mf.getFieldName();

						break;
						//System.out.println("OriginalMapValue---->"+orgMapValue+"  "+fieldName);
					}
				}

			}else{
				for(MessageFieldBean mf:getMessageconfiguration().getMessageFieldsConfiguration()){
					int repfldcnt = Integer.parseInt(mf.getRepeated());
					if(repfldcnt>1 && mf.getRepeatedColumn()!=null && mf.getFieldName().equalsIgnoreCase(repFieldName) ){
						terserPath = mf.getTerserPath();
						fieldName=mf.getFieldName();
						repeatedFieldCnt =Integer.parseInt(mf.getRepeated());
						break;

					}
				}
			}

			System.out.println("Repeated Terser Path ===============================>##>"+terserPath);

			String terserPathArr[] = terserPath.split("-");
			String newPath ="";
			//int count=0;
			for(int i=1;i<repeat;i++){// to save all records in repeated segment table change i=0 & change in Messagedao 
									  // repeated insertRepeatedSegmentValue method.
				logger.info(" \n RepeatedterserPathArrLength=======>"+terserPathArr.length);
				if(terserPathArr.length>=3){
					
					newPath = terserPathArr[0]+"("+i+")-"+terserPathArr[1]+"-"+terserPathArr[2];
					
					//Getting Repeated field values from Repeated Segments
					if(repeatedFieldCnt>1 && fieldName.equalsIgnoreCase(repFieldName) ){
						result[i] =	getFieldValues(newPath,input, repeatedFieldCnt);
						logger.info("RepertedFieldTerserPath====>"+newPath+""+ result[i]);
					}
				}
				else{
					
					newPath = terserPathArr[0]+"("+i+")-"+terserPathArr[1];	
					
					//Getting Repeated field values from Repeated Segments
					if(repeatedFieldCnt>1 && fieldName.equalsIgnoreCase(repFieldName) ){
						logger.info("RepertedFieldTerserPath====>"+newPath);
						result[i] =	getFieldValues(newPath,input, repeatedFieldCnt);
					}
					
				}
				//System.out.println("terserPathArr[0]====================>"+terserPathArr[0]+"\n terserPathArr[1]=========>"+terserPathArr[1]+"\n NewPath======>"+newPath);
				logger.info("New TerserPath =======>"+newPath);
				//Getting Repeated field values from Repeated Segments
				if(repeatedFieldCnt>1 && fieldName.equalsIgnoreCase(repFieldName) ){
					result[i] =	getFieldValues(newPath,input, repeatedFieldCnt);
				}
				
				//Mapping Field Value from Repeated segmentField values.
				else if(mappingField != null){
					result[i]=	getRepeatedFieldMappingValues(mappingType, mappingField, input,newPath,fieldName);
				}
				
				else if(mappingField == null){
					result[i]=terser.get(newPath);
				}
				
				if(result[i] != null && (result[i].contains("'") || result[i].contains("\"") )){
					result[i]=result[i].replace("'", "`");
					if(result[i].contains("\"") || result[i].contains("\"\"")){
						result[i]=result[i].replace("\"", "`");
						logger.info("Inside special character replace Field====>"+newPath+"==========>"+result[i]);
					}

				}

				/*	if( result[i] != null && result[i].contains("/")){
					result[i]=result[i].replace("/", "-");
				}*/
				//logger.info("After changed Result Value====>"+newPath+"==========>"+result[i]);
				logger.info("Checnking Contains Value");
				if(result[i]==null || result[i].equals("\"\"")){
					result[i]=null;
				}
				//	logger.info("Repeated segment Value--->"+newPath+"=============>"+result[i]);

			} inputStream.close();
		} catch (Exception e) {
			logger.error("Error while getting repeated segment field values");
			e.printStackTrace();
			throw new HL7CustomException(e.getMessage(),prop.getProperty("AcknowledgementReject"));
		}
		return result;

	}

	//Single Occurance Field Value
	public String getFieldValues(String terserPath,Message input) throws Exception{

		String result="";  
		Terser terser = new Terser(input);
		logger.info("Single Occurance Field Values  "+terserPath);
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream("adt.properties");
		prop.load(inputStream);
		try {
			if(terserPath!=null){
				result =terser.get(terserPath);
				logger.info("Incoming Field Value-->"+result+"--> "+terserPath);
				if(result==null || result.equals("\"\"")){
					result=null;
				}
				if(result != null && (result.contains("'") || result.contains("\"") )){
					result=result.replace("'", "`");
					if(result.contains("\"") || result.contains("\"\"")){
						result=result.replace("\"", "`");
						logger.info("Replaced Field Result value-->"+result+"--> "+terserPath);
					}
					logger.info("Result-->"+result+"--> "+terserPath);
					System.out.println("Result-->"+result+"--> "+terserPath);
				}
			}
			inputStream.close();
		} catch (Exception e) {
			logger.error("Error While getting Single Occurance Filed Values");
			throw new HL7CustomException(e.getMessage(),prop.getProperty("AcknowledgementReject"));

		}

		return result;
	}
	/***
	 * Repeated Field values
	 * @param terserPath
	 * @param input
	 * @param repeated
	 * @return
	 * @throws Exception
	 * @throws IOException
	 */
	//Repeated Field Values
	public String getFieldValues(String terserPath,Message input,int repeated) throws Exception,IOException {

		String result=null;  
		Terser terser = new Terser(input);
		logger.info("RepatedFieldValues");
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream("adt.properties");
		prop.load(inputStream);
		System.out.println("Repeated terser Path ====>"+terserPath);
		logger.info("Repeated terser Path ====>"+terserPath);
		try {
			String terserArr[] = terserPath.split("-");
			String newPath ="";
			int count=0;
			for(int i=0;i<repeated;i++){
				newPath = terserArr[0]+"-"+terserArr[1]+"("+i+")-"+terserArr[2];
				logger.info("Repeated Field TerserPath=====>"+newPath);
				if(count==0){
					result =terser.get(newPath);

					if(result==null || result.equals("\"\"")){
						//result=null;
						return result;
					}
				}else{
					String newResult = terser.get(newPath);
					if(newResult==null || newResult.equals("\"\"")){
						//result=null;
						return result;
					}else if(newResult!=null){

						result=result+"~"+newResult;
						logger.info("Repeated field Values ==============>"+result);
					}
				}
				count++;
				//System.out.println("result="+result);
			}
			inputStream.close();
		} catch (Exception e) {
			logger.error("Error While getting Repeated Filed values");
			throw new HL7CustomException(e.getMessage(),prop.getProperty("AcknowledgementReject"));
		}
		logger.info("Repeated Field Values--->"+result);
		return result;
	}

	//Mapping Field Values
	/****
	 * 
	 * @param mappingType
	 * @param mappingField
	 * @param input
	 * @  Mapping Field Method
	 * @throws Exception
	 * @throws IOException
	 */
	public String getFieldValues(String mappingType,String mappingField,Message input) throws Exception,IOException {
		InputStream inputStream = null;
		try{
			logger.info("Mapping Field values " +mappingType);
			System.out.println("Mapping Field values " +mappingType);
			
			 inputStream = getClass().getClassLoader().getResourceAsStream("ackcodes.properties");
			prop.load(inputStream);
			
			Properties prop1 = new Properties();
			
			prop1.load(getClass().getClassLoader().getResourceAsStream("adt.properties"));

			String mappedValue=null;
			String orgMapValue=null;
			Terser terser = new Terser(input);
			String fieldName=null;
			String emptyMap =null; 
			int mandatory = 0 ;
			for(MessageFieldBean mf:getMessageconfiguration().getMessageFieldsConfiguration()){
				System.out.println("FieldName---->"+mf.getFieldName()+"     "+mappingField);
				if(mappingField.equalsIgnoreCase(mf.getFieldName())){
					mandatory = mf.getIsMandatory();
					orgMapValue=terser.get(mf.getTerserPath());
					fieldName=mf.getFieldName();
					
					System.out.println("OriginalMapValue---->"+orgMapValue+"  "+fieldName);
				}
			}
			String dbOriginalMapValue = null;
		
			emptyMap = prop1.getProperty("emptyMap").trim();
			for(FieldMapBean fmb:getMessageconfiguration().getFieldmap()){
				dbOriginalMapValue =fmb.getOriginalValue();
				
				if(orgMapValue == null){
					if(emptyMap.equalsIgnoreCase("Y") && prop1.getProperty("emptyMapFields").contains(fmb.getMappingType())){
						if(mappingType.equalsIgnoreCase(fmb.getMappingType()) ){
							if(fmb.getOriginalValue() == null){
								mappedValue=fmb.getMappedValue();
								
								logger.info("Empty Mapped Value=========>"+mappedValue+"   "+fmb.getOriginalValue());
							}
						}
					}
				}
				if(orgMapValue !=null){
				if(mappingType.equalsIgnoreCase(fmb.getMappingType()) && orgMapValue.equalsIgnoreCase(fmb.getOriginalValue())){
					logger.info("Mapping Values--->"+fmb.getOriginalValue()+"  "+ fmb.getMappedValue());
					//System.out.println("Mapping Values--->"+fmb.getOriginalValue()+"  "+ fmb.getMappedValue());
					mappedValue=fmb.getMappedValue();
					
					break;
				}
				}
			}     
			
			if(emptyMap.equalsIgnoreCase("Y")){
			    if(dbOriginalMapValue != null && orgMapValue != null ) 
			      if(!dbOriginalMapValue.equals(orgMapValue)){
			    	  logger.info("mappingType for unknown=========>"+mappingType);
			    	  mappedValue = 	prop.getProperty(mappingType).trim();
			    	 if("N".equalsIgnoreCase(prop.getProperty("disable_mapping_exception").trim())) {
			    	  throw new HL7CustomException("No Mapping Value for - "+orgMapValue+" - in "+fieldName+"_eResearch",prop.getProperty("AcknowledgementReject"),mandatory+"");
			    	 }
			      }
			} else if(!emptyMap.equalsIgnoreCase("Y")){
				  if(mappedValue == null && orgMapValue != null){
					  if("N".equalsIgnoreCase(prop.getProperty("disable_mapping_exception").trim())) {
			    	  throw new HL7CustomException("No Mapping Value for - "+orgMapValue+" - in "+fieldName+"_eResearch",prop.getProperty("AcknowledgementReject"),mandatory+"");
					  }
					  logger.info("mappingType for unknown =========>"+mappingType);
					  mappedValue = 	prop.getProperty(mappingType).trim();
			      }
	
			}

			return mappedValue;
		} catch (HL7CustomException e) {
			
			e.printStackTrace();
			logger.error("Error while getting Mapping Field Values");
			throw e;
		}finally{
			if(inputStream != null){
				inputStream.close();
			}
		}

	}



	//Mapping Field Values
	/****
	 * 
	 * @param mappingType
	 * @param mappingField
	 * @param input
	 * @Repeated field Mapping value  Method
	 * @throws Exception
	 * @throws IOException
	 */
	public String getRepeatedFieldMappingValues(String mappingType,String mappingField,Message input,String terserPath,String fieldName) throws Exception,IOException {
		InputStream inputStream = null;
		InputStream inputStream1 = null;
		try{
			logger.info("Repeated Field Mapping  values " +mappingType);

			inputStream = getClass().getClassLoader().getResourceAsStream("ackcodes.properties");
			prop.load(inputStream);

			inputStream1 = getClass().getClassLoader().getResourceAsStream("adt.properties");
			prop.load(inputStream1);
			Terser t=new Terser(input);
			String msgId=t.get("/.MSH-7-1");
			String mrn = null, msgdate=null, temp=null;
			msgdate = messagedao.getDate();
			mrn = t.get("/.PID-3-1");
			String mappedValue=null;
			String orgMapValue=null;
			Terser terser = new Terser(input);


			orgMapValue=terser.get(terserPath);

			/*	for(MessageFieldBean mf:getMessageconfiguration().getMessageFieldsConfiguration()){
				//System.out.println("FieldName---->"+mf.getFieldName()+"     "+mappingField);
				if(mappingField.equalsIgnoreCase(mf.getFieldName())){

					orgMapValue=terser.get(mf.getTerserPath());
					fieldName=mf.getFieldName();

					//System.out.println("OriginalMapValue---->"+orgMapValue+"  "+fieldName);
				}
			}*/


			for(FieldMapBean fmb:getMessageconfiguration().getFieldmap()){

				if(orgMapValue !=null){
					if(mappingType.equalsIgnoreCase(fmb.getMappingType()) && orgMapValue.equalsIgnoreCase(fmb.getOriginalValue())){
						logger.info("Repeated Field  Mapping Values for Unknown--->"+fmb.getOriginalValue()+"  "+ fmb.getMappedValue());
						System.out.println("Mapping Values--->"+fmb.getOriginalValue()+"  "+ fmb.getMappedValue());
						mappedValue=fmb.getMappedValue();
						break;
					}
				}
			}     

			if(mappedValue == null && orgMapValue != null){
				//throw new HL7CustomException("No Mapping Value for - "+orgMapValue+" - in "+mappingType,prop.getProperty("AcknowledgementReject"));
				mappedValue = 	prop.getProperty("default_mapped_value");
				String errorMsg = "No Mapping Value for - " +orgMapValue+ " - in " +mappingType;
				logger.info("Mapping ==>"+errorMsg);
				//emailNotification.sendMail("Un known Mapping Value", errorMsg,msgdate,mrn,msgId);

			}

			return mappedValue;
		} catch (Exception e) {
			logger.error("Error while getting Mapping Field Values");
			throw new HL7CustomException(e.getMessage(),prop.getProperty("AcknowledgementReject"));
		}finally{
			inputStream.close();
			inputStream1.close();
		}

	}



	private  final Map<String, String> DATE_FORMAT_REGEXPS = new HashMap<String, String>() {
		{
			put("^\\d{8}$", "yyyyMMdd");
			put("^\\d{1,2}-\\d{1,2}-\\d{4}$", "dd-MM-yyyy");
			put("^\\d{4}-\\d{1,2}-\\d{1,2}$", "yyyy-MM-dd");
			put("^\\d{1,2}/\\d{1,2}/\\d{4}$", "MM/dd/yyyy");
			put("^\\d{4}/\\d{1,2}/\\d{1,2}$", "yyyy/MM/dd");
			put("^\\d{1,2}\\s[a-z]{3}\\s\\d{4}$", "dd MMM yyyy");
			put("^\\d{1,2}\\s[a-z]{4,}\\s\\d{4}$", "dd MMMM yyyy");
			put("^\\d{12}$", "yyyyMMddHHmm");
			put("^\\d{8}\\s\\d{4}$", "yyyyMMdd HHmm");
			put("^\\d{1,2}-\\d{1,2}-\\d{4}\\s\\d{1,2}:\\d{2}$",
					"dd-MM-yyyy HH:mm");
			put("^\\d{4}-\\d{1,2}-\\d{1,2}\\s\\d{1,2}:\\d{2}$",
					"yyyy-MM-dd HH:mm");
			put("^\\d{1,2}/\\d{1,2}/\\d{4}\\s\\d{1,2}:\\d{2}$",
					"MM/dd/yyyy HH:mm");

			put("^\\d{1,2}/[a-zA-Z]{3}/\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$",
					"dd/MMM/yyyy hh:mm:ss");
			put("^\\d{4}/\\d{1,2}/\\d{1,2}\\s\\d{1,2}:\\d{2}$",
					"yyyy/MM/dd HH:mm");
			put("^\\d{1,2}\\s[a-z]{3}\\s\\d{4}\\s\\d{1,2}:\\d{2}$",
					"dd MMM yyyy HH:mm");
			put("^\\d{1,2}\\s[a-z]{4,}\\s\\d{4}\\s\\d{1,2}:\\d{2}$",
					"dd MMMM yyyy HH:mm");
			put("^\\d{14}$", "yyyyMMddHHmmss");
			put("^\\d{4}$", "yyyyMMddHHmmss");
			put("^\\d{8}\\s\\d{6}$", "yyyyMMdd HHmmss");
			put("^\\d{1,2}-\\d{1,2}-\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$",
					"dd-MM-yyyy HH:mm:ss");
			put("^\\d{4}-\\d{1,2}-\\d{1,2}\\s\\d{1,2}:\\d{2}:\\d{2}$",
					"yyyy-MM-dd HH:mm:ss");
			put("^\\d{1,2}/\\d{1,2}/\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$",
					"MM/dd/yyyy HH:mm:ss");
			put("^\\d{4}/\\d{1,2}/\\d{1,2}\\s\\d{1,2}:\\d{2}:\\d{2}$",
					"yyyy/MM/dd HH:mm:ss");
			put("^\\d{1,2}\\s[a-z]{3}\\s\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$",
					"dd MMM yyyy HH:mm:ss");
			put("^\\d{1,2}\\s[a-z]{4,}\\s\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$",
					"dd MMMM yyyy HH:mm:ss");

		}
	};

	public  String determineDateFormat(String dateString) {
		for (String regexp : DATE_FORMAT_REGEXPS.keySet()) {
			if (dateString.toLowerCase().matches(regexp)) {
				return DATE_FORMAT_REGEXPS.get(regexp);
			}
		}
		return null; // Unknown format.
	}



	/***
	 * this methos returns postgres equalent string
	 * input is date as string. 
	 * 
	 *    accepted input  formats are..
	 *      String yymmddhhmmss = "20180129084456";
			String ddmmyyhhmmss = "12/FEB/2010 01:23:21";
			String yymmdd = "19561211";

	 */

	@SuppressWarnings("deprecation")
	public  String ChangeDateFormat(String datevalue) throws ParseException, Exception {


		// for format yyyymmddHHmmss
		logger.info("Date Value===============>"+datevalue);
		InputStream inputstream = null;
		String dateFormat = null;

		inputstream = getClass().getClassLoader().getResourceAsStream("adt.properties");
		try {
			prop.load(inputstream);
		} catch (IOException e) {
			logger.info("adt.propety file load exception");
			e.printStackTrace();
			throw e;
		}
try{
		dateFormat = prop.getProperty("dateFormat").trim();

		if(dateFormat == null){
			dateFormat = "yyyyMMdd";
		}

		String format = determineDateFormat(datevalue);
		logger.info("DateFormat===---->" + format);

		if (format != null && !format.isEmpty())

		{
			switch(format) {

			case "yyyyMMddhhmmss":
				String psqldate1 =null;
				SimpleDateFormat originalFormat = new SimpleDateFormat(
						"yyyymmddhhmmss");
				originalFormat.setLenient(false);
				try{
					Date dd1 = originalFormat.parse(datevalue);
					System.out.println("ddddd " + dd1);
					SimpleDateFormat formatter = new SimpleDateFormat(
							dateFormat, Locale.ENGLISH);
					psqldate1 = formatter.format(dd1);
					System.out.println(psqldate1);

				}catch(ParseException pe){

					throw new ParseException("invalid date format", 2);
				}
				logger.info("After Date converstion ===>"+psqldate1);
				return psqldate1;


			case "dd/MMM/yyyy hh:mm:ss":
				SimpleDateFormat formatter1=null;
				String psqldate2 =null;
				SimpleDateFormat originalFormat1 = new SimpleDateFormat(
						"dd/MMM/yyyy hh:mm:ss");
				originalFormat1.applyPattern("ddMMMyyyy hh:mm:ss");

				originalFormat1.setLenient(false);
				try{
					Date dd2 = originalFormat1.parse(datevalue);

					formatter1 = new SimpleDateFormat(
							dateFormat, Locale.ENGLISH);
					psqldate2 = formatter1.format(dd2);
				}catch(ParseException pe){
					psqldate2=null;
					throw  new ParseException("invalid date ", 1);
				}
				return psqldate2;

			case "yyyyMMdd":

				SimpleDateFormat formatter2 = null;

				String psqldate3 = null;
				Date dd3 = null;
				try {
					// for format (YYYYMMDD) 19561022
					SimpleDateFormat originalFormat2 = new SimpleDateFormat(
							"yyyyMMdd");
					originalFormat2.applyPattern("yyyyMMdd");
					originalFormat2.setLenient(false);

					dd3 = originalFormat2.parse(datevalue);

					formatter2 = new SimpleDateFormat(dateFormat,Locale.ENGLISH);

					psqldate3 = formatter2.format(dd3);
					System.out.println(psqldate3);
				} catch (ParseException pp) {
					psqldate3=null;
					throw new ParseException("invalid date ", 1);

				}
				if(psqldate3 != null && !psqldate3.isEmpty())
				{
					String arr[] = psqldate3.split("-");

					int i = 0;
					Integer[] intarray = new Integer[arr.length];

					for (String str : arr) {

						intarray[i] = Integer.parseInt(str.trim());

						if (i == 1) {
							if (intarray[1] > 12) {

								throw new ParseException(
										str.concat("  invalid month"), i);
							}

							i++;
						}
					}
					logger.info("After Date converstion ===>"+psqldate3);
					return psqldate3;
				}
			case "yyyyMMddHHmmss":
				SimpleDateFormat formatter4=null;
				String psqldate4 =null;
				SimpleDateFormat originalFormat4 = new SimpleDateFormat(
						"yyyyMMddHHmmss");
				originalFormat4.applyPattern("yyyyMMddHHmmss");

				originalFormat4.setLenient(false);
				try{
					Date dd2 = originalFormat4.parse(datevalue);

					formatter4 = new SimpleDateFormat(
							dateFormat, Locale.ENGLISH);
					psqldate4 = formatter4.format(dd2);
				}catch(ParseException pe){
					psqldate4=null;
					throw  new ParseException("invalid date ", 1);
				}
				logger.info("After Date converstion ===>"+psqldate4);
				return psqldate4;
				
			default :
				logger.info("Default Case DateFormat ==>"+format);
				throw new HL7CustomException("Invalid Date data type","AR");	
			}
		}

		else{
			throw new HL7CustomException("Invalid Date data type","AR");
		}

	}catch(Exception ee){
	    
		if(inputstream != null){
			inputstream.close();
		}
		throw ee;

	}finally{
		if(inputstream != null){
			inputstream.close();
		}
	}



	}
	/*


	//Method for getting Field Values
	public void getFieldValues(Message input,Map messageMap){


		try{
			String field_Function;
			String[] fieldMethod;

			String messagetype=(String)messageMap.get("messagetype");
			String tablename=(String)messageMap.get("tablename");


			for(MessageFieldBean mf:getmessageconfiguration().getMessageFieldsConfiguration()){
				segmentName = input.get(mf.getSegment_name());


				field_Function= mf.getField_function();
				fieldMethod=field_Function.split("\\.");


				if(mf.getMessage_type().equals(messagetype)) {

					if(fieldMethod[0].equals("null")){

						// Getting Field Mapping Values
						getMappedValues(mf.getMapping_type(),mf.getColumn_name());

					}else{



						for(SegmentBean sb:getmessageconfiguration().getSegmentConfigurationBean()){


							if(!(sb.getRepeated().equals("1")) && sb.getMessageType().equalsIgnoreCase(messagetype)){


								repeatedSegment= input.getAll(sb.getSegmentName());

								if(repeatedSegment[0].equals(segmentName)){

									// method for Repeated Segment values
									getRepeatedSegments(repeatedSegment,field_Function,mf.getColumn_name());

									break;
								}

							}

						}

						if(mf.getRepeated().equals("1")){
							//System.out.println( "columnName---->"+mf.getColumn_name());

							if(!segmentName.toString().equalsIgnoreCase(repeatedSegment[0].toString())){

								//method for single occurence field
								getField(segmentName,field_Function,mf.getColumn_name());
							}
						}else{
							// Method for Repeated Field Values
							getRepeatedFields(segmentName,field_Function,mf.getColumn_name());
						}



					}

				}
			}

			// Inserting Patient Records in to DB table
			messagedao.insertPatientRecord(tablename,resultMap);

			//Insert request,response message in Audit table
			messagedao.messageAudit(messagetype, input);

		}catch(Exception e){

			e.printStackTrace();

		}

	}



	//Repated Segment Values
	private String getRepeatedSegments(Structure[] repeatedSegment,String field_Function, String column_name) {

		String result=null;

		String[] fieldMethod;

		System.out.println("Repeated Segment");

		fieldMethod=field_Function.split("\\.");
		//Repeated Segments

		for (Structure struct : repeatedSegment) {
			try {
				Class messageSegmentClass =struct.getClass();
				Method m = messageSegmentClass
						.getMethod(getFunctionName(fieldMethod[0]));

				if(!getFunctionName(fieldMethod[1]).equals("encode")){

					Object messageSegmentM = m.invoke(segmentName);
					Class messageSegmentClassM = messageSegmentM.getClass();
					Method m1 = messageSegmentClassM.getMethod(getFunctionName(fieldMethod[1]),Integer.TYPE);

				}


				Object messageSegmentM = m.invoke(struct);
				Class messageSegmentClassM = messageSegmentM.getClass();
				Method m1 = messageSegmentClassM.getMethod("encode");
				System.out.println("Repeated Segment :: "+column_name+ m1.invoke(messageSegmentM));
				result=(String)m1.invoke(messageSegmentM);
			} catch (Exception e) {
				System.out.println(e);
			}

		}

		return result;
	}





	//single Occurrence Field Values
	public String getField(Structure segmentName,String field_Function,String columnName){
		String[] fieldMethod;
		String result = null;
		try{
			fieldMethod=field_Function.split("\\.");


			if(!getFunctionName(fieldMethod[0]).equals(null)){

				Class messageSegmentClass = segmentName.getClass();
				Method m = messageSegmentClass.getMethod(getFunctionName(fieldMethod[0]),noparam);
				System.out.println("fieldMethod =="+fieldMethod[0]);

				if(!getFunctionName(fieldMethod[1]).equals("encode")){
					Object messageSegmentM = m.invoke(segmentName);
					Class messageSegmentClassM = messageSegmentM.getClass();
					Method m1 = messageSegmentClassM.getMethod(getFunctionName(fieldMethod[1]),Integer.TYPE);

				}

				Object messageSegmentM1 = m.invoke(segmentName);
				Class messageSegmentClassM1 = messageSegmentM1.getClass();
				Method m2 = messageSegmentClassM1.getMethod("encode");
				//System.out.println(mf.getColumn_name()+"  Value ::  " + m2.invoke(messageSegmentM1));

				System.out.println(columnName+"   "+m2.invoke(messageSegmentM1));


				resultMap.put(columnName, m2.invoke(messageSegmentM1).toString());

				result = (String) m2.invoke(messageSegmentM1);


			}else{
				System.out.println("Null Field values");

			}
		} catch(Exception e){
			e.printStackTrace();
		}
		return result;

	}


	//Repeated Field Values
	public String getRepeatedFields(Structure segmentName,String field_Function,String columnName){

		String result="";
		String[] fieldMethod;

		try{

			fieldMethod=field_Function.split("\\.");

			if(!getFunctionName(fieldMethod[0]).equals(null)){
				Class messageSegmentClass=segmentName.getClass();
				Method m = messageSegmentClass.getMethod(getFunctionName(fieldMethod[0]));
				System.out.println("FieldMethod ::  "+fieldMethod[0]);
				Object[] messageSegmentMList = (Object[]) m.invoke(segmentName);
				//Object messageSegmentM1 = m.invoke(segmentName);

				int count=0;

				for(Object obj : messageSegmentMList)
				{
					Class	messageSegmentClassM = obj.getClass();
					Method   m1 = messageSegmentClassM.getMethod(getFunctionName(fieldMethod[1]),
							Integer.TYPE);

					Object messageSegmentM1 = m1.invoke(obj,0);
					Class	messageSegmentClassM1 = messageSegmentM1.getClass();
					Method	m2 = messageSegmentClassM1.getMethod("encode");
					//System.out.println(mf.getColumn_name() +" Values :: " + m2.invoke(messageSegmentM1));

					if(count==0){
						result= result+m2.invoke(messageSegmentM1).toString();
					}else{
						result= result+"~"+m2.invoke(messageSegmentM1).toString();

					}

					count++;
					System.out.println("Result===>"+result);

				}
				resultMap.put(columnName,result);
			}else{
				System.out.println("Null Field values");
			}

		}catch(Exception e){
			e.printStackTrace();
		}

		return result;
	}

	//Getting FieldFunction and removing braces
	private String getFunctionName(String function) throws Exception{
		//System.out.println("function=="+function);
		String result = null;
		try{
			int length = function.indexOf("(");
			//System.out.println("length=="+length);
			int param=function.indexOf(")");

			int len=(param-1)-length+1;

			//System.out.println("Param lent==="+len);

			if(length>=0){
				result = function.substring(0, length);


			}else{
				result=function;
			}

			if(len>=2){

				String	resultf=function.substring(length+1,function.length()-1);
				//System.out.println("resultparam "+resultf);

				}

		}catch(Exception e){
			throw e;
		}
		return result;
	}


	// Getting Field Mapping Values
	public String getMappedValues(String mappingType,String columnName){

		String mappedValue=null;

		for(FieldMapBean fmb:getmessageconfiguration().getFieldmap()){

			if(mappingType.equalsIgnoreCase(fmb.getMappingType())){
				logger.info("Mapping Values--->"+fmb.getOriginalValue()+"  "+ fmb.getMappedValue());

				mappedValue=fmb.getMappedValue();


				resultMap.put(columnName,mappedValue);
				break;
			}

		}



		return mappedValue;

	}

	 */

}




