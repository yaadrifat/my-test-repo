package com.velos.integration.adtx.webservice;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import com.velos.services.OperationException_Exception;
import com.velos.services.StudySummary;



@WebService(targetNamespace="http://velos.com/services/", name="StudySEI" )
public interface VelosEspStudySummaryEndpoint {
	
	@WebResult(name="StudySummary",targetNamespace="")
	@RequestWrapper(localName="getStudySummary",targetNamespace="http://velos.com/services/",className="com.velos.services.GetStudySummary")
	@ResponseWrapper(localName="getStudySummaryResponse",targetNamespace="http://velos.com/services/",className="com.velos.services.GetStudySummaryResponse")
	
				public StudySummary getStudySummary(
						@WebParam(name="StudyIdentifier",targetNamespace="")com.velos.services.StudyIdentifier studyIdentifier)throws  OperationException_Exception;
						
	
}
