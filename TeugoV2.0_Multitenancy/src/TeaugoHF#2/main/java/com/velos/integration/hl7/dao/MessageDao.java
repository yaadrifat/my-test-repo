package com.velos.integration.hl7.dao;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;
import javax.sql.DataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.util.Terser;
import com.velos.integration.hl7.notifications.HL7CustomException;
import com.velos.integration.hl7.processor.InterfaceUtil;

public class MessageDao {

	private static Logger logger = Logger.getLogger(MessageDao.class);

	private DataSource vgdbDataSource;
	private JdbcTemplate jdbcTemplate;
	Properties prop=new Properties();
	private InterfaceUtil interfaceUtil;

	public InterfaceUtil getInterfaceUtil() {
		return interfaceUtil;
	}

	public void setInterfaceUtil(InterfaceUtil interfaceUtil) {
		this.interfaceUtil = interfaceUtil;
	}

	public DataSource getVgdbDataSource() {
		return vgdbDataSource;
	}

	public void setVgdbDataSource(DataSource vgdbDataSource) {

		this.vgdbDataSource = vgdbDataSource;
		this.jdbcTemplate = new JdbcTemplate(vgdbDataSource);
		logger.info("Jdbc Template created successfully");

	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public MessageDao()
	{}

	public MessageDao(MessageDao messageDao)
	{
		this.setVgdbDataSource(messageDao.vgdbDataSource);
	}

	// Insert Success Message Audit Record values
	public void insertMessageAudit(String messagetype,Message input,Message response) throws HL7CustomException,IOException{

		try{
			logger.info("Entered Into Insert Success message audit record");
			InputStream inputStream = getClass().getClassLoader().getResourceAsStream("adt.properties");
			prop.load(inputStream);

			/*	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
			//get current date time with Date()
			Date date = new Date();
			String currentDate=dateFormat.format(date);
			 */
			Terser t=new Terser(input);
			Terser t1=new Terser(response);
			String reqMsg= input.toString().replaceAll("'", "`");
			//from incoming message
			String messageId=t.get("/MSH-10");
			String eventId=t.get("/MSH-9-2");
			String mrn=t.get("/.PID-"+prop.getProperty("pid_mrn_field_positionNo")+"-1");
			//from response message acktype
			String ackType=t1.get("/MSA-1");
			String responseMsg= response.toString().replaceAll("'", "`");
			String query = "insert into vgdb_message_audit_details (pk_msg_audit,message_type,event_id,message_id,mrn,request_message,response_message,acknowledge_type,creator,creation_date ) values(nextval('seq_vgdb_message_audit_details')"+",'"+messagetype+"','"+eventId+"','"+messageId+"','"+mrn+"','"+reqMsg+"','"+responseMsg+"','"+ackType+"','"+ prop.getProperty("msg.creator")+"','"+getDate()+"')";

			logger.info("\nQuery for Audit table :: \n"+query);

			int i=jdbcTemplate.update(query);

			if(i==1){
				System.out.println("successfully Inserted Audit Values");
				logger.info("OSuccessfully Inserted Audit Values");
			}
		} catch (Exception e) {
			logger.error("Error While inserting Message Audit Values ");
			throw new HL7CustomException(e.getMessage(),"AE");
		}
	}

	public String getDate(){

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		//get current date time with Date()
		Date date = new Date();
		String currentDate=dateFormat.format(date);

		System.out.println("Current_Date---->"+ currentDate);
		return currentDate;


	}


	//Insert Error Message Audit Record values
	public void insertMessageAudit(String input,Message response,String mrn) throws HL7CustomException, IOException{

		try{
			logger.info("Entered Into Insert Error message audit record");
			InputStream inputStream = getClass().getClassLoader().getResourceAsStream("adt.properties");
			prop.load(inputStream);


			String reqMsg= input.toString().replaceAll("'", "`");
			String outputMessage= response.toString().replaceAll("'", "`");

			String[] segments1 = input.split(System.getProperty("line.separator"));
			String[] segments2 = segments1[0].split("\\|");


			String messageType=segments2[8];
			String messageId = segments2[9];
			String[] messageArray=messageType.split("\\^");

			Terser t=new Terser(response);

			//from response message acktype
			String ackType=t.get("/MSA-1");

			String query = "insert into vgdb_message_audit_details (pk_msg_audit,message_type,event_id,message_id,mrn,request_message,response_message,acknowledge_type,creator,creation_date ) values(nextval('seq_vgdb_message_audit_details')"+",'"+messageArray[0]+"','"+messageArray[1]+"','"+messageId+"','"+mrn+"','"+reqMsg+"','"+outputMessage+"','"+ackType+"','"+prop.getProperty("msg.creator")+"','"+getDate()+"')";


			logger.info("\nQuery for Audit table :: \n"+query);

			int i=jdbcTemplate.update(query);

			if(i==1){
				System.out.println("successfully Inserted Audit Values");
				logger.info("Successfully Inserted Error Message Audit Records ");
			}
		} catch (Exception e) {
			logger.error("Error While inserting Error message Audit values");
			throw new HL7CustomException(e.getMessage(),"AE");
		}
	}





	// Inserting Message Records  based on message type table
	public  int insertMessageRecord(String tableName,Map resultMap) throws HL7CustomException{


		int seqId=0;
		try{

			StringBuffer columnName = new StringBuffer("");
			StringBuffer columnValues = new StringBuffer("");
			Iterator it = resultMap.entrySet().iterator();

			String column = null;
			String value = null;
			int mapSize = resultMap.size()-1;

			int count = 0;
			while(it.hasNext()){
				Map.Entry me = (Map.Entry)it.next();
				column = (String) me.getKey();
				value = (String) me.getValue();

				if(count!=mapSize){
					columnName.append(column+",");
					if(value==null){
						columnValues.append(value+",");
					}else{
						columnValues.append("'"+value+"',");
					}
				}else{
					columnName.append(column);
					if(value==null){
						columnValues.append(value);
					}else{
						columnValues.append("'"+value+"'");
					}
				}
				count++;
			}

			String seq="select nextval('seq_"+tableName+"'"+")";

			seqId=jdbcTemplate.queryForInt(seq);



			//String query = "insert into "+tableName+"(pk_msg_adt,"+columnName.toString()+") values(nextval('seq_vgdb_message_adt')"+","+columnValues.toString()+")";

			String query = "insert into "+tableName+"(pk_"+tableName+","+columnName.toString()+","+"messagedate"+","+"messagestatus"+") values("+seqId+","+columnValues.toString()+",'"+getDate()+"','"+"I"+"')";

			logger.info("\nQuery for ADT table == \n"+query);   

			int i=jdbcTemplate.update(query);
			if(i==1){
				logger.info("Successfully Inserted Message Record");
			}

		} catch (Exception e) {
			logger.error("Error While Inserting Message record values");
			throw new HL7CustomException(e.getMessage(),"AE");
		}
		return seqId;
	}


	public  void insertRepeatedSegmentValue(String tableName,Map<String,String[]> resultMap,int pseqId,String pareTableName,int repSegCount) throws HL7CustomException, IOException{
		InputStream input = null;
		try{

			input = getClass().getClassLoader().getResourceAsStream("adt.properties");
			prop.load(input);

			ArrayList<String> nameList = new ArrayList<String>();
			Iterator it = resultMap.entrySet().iterator();
			while(it.hasNext()){
				Map.Entry me = (Map.Entry) it.next();
				String key = (String)me.getKey();
				nameList.add(key);
			}

			String repSegCol = prop.getProperty("repeated_seg_date_column");
			logger.info("Repeted Seg column Names===>"+repSegCol);
			StringTokenizer st = new StringTokenizer(repSegCol,",");
			List<String> repSegDateColumns = new ArrayList<String>();
			while(st.hasMoreElements()){
				repSegDateColumns.add(st.nextToken());

			}		logger.info("Repeted Seg column Names list===>"+repSegDateColumns);
			StringBuffer columnName = null;
			StringBuffer columnValues = null;
			for(int j=1;j<repSegCount;j++){// to save all records in repeated segment table change j=0
				columnName = new StringBuffer("");
				columnValues = new StringBuffer("");
				for(int i=0;i<nameList.size();i++){
					String column = nameList.get(i);
					//System.out.println("column=="+column);
					String result = getResult(column,resultMap,j);
					//System.out.println("Value=="+result);
					if(i==0){
						columnName.append(column);
						if(result==null){
							columnValues.append(result);
						}else{
							columnValues.append("'"+result+"'");
						}
					}else{
						columnName.append(","+column);
						if(result==null){
							columnValues.append(","+result);
						}else{
							if(repSegDateColumns.contains(column)){
								result = interfaceUtil.ChangeDateFormat(result);
							}
							columnValues.append(",'"+result+"'");
						}
					}
				}
				String query = "insert into "+tableName+"(pk_"+tableName+","+"fk_"+pareTableName+","+columnName.toString()+",messagedate"+")"+" values(nextval('seq_"+tableName+"'"+"),"+pseqId+","+columnValues.toString()+",'"+getDate()+"')";

				logger.info("\n Insert Repeated Segment Value query == \n"+query);

				int i=jdbcTemplate.update(query);

				if(i==1){
					logger.info("Successfully inserted Repeated Segment Records");
				}
			}


		} catch (Exception e) {
			logger.error("Error While Inserting repeated Segment values");
			e.printStackTrace();
			throw new HL7CustomException(e.getMessage(),"AE");
		}finally{
			input.close();
		}
	}

	private String getResult(String colunName, Map<String, String[]> resultMap,int i)  throws HL7CustomException {
		String result = null;
		try{
			String nameArr[] = resultMap.get(colunName);
			result = nameArr[i];
		}catch(Exception e){
			e.printStackTrace();
			throw new HL7CustomException(e.getMessage(),"AE");
		}
		return result;
	}

}
