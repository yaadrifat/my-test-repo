package com.velos.integration.hl7.notifications;

public class HL7CustomException extends Exception {

	private String responeType;
	private String emailNotfMsg;

	public HL7CustomException() {
		super();
	}
	
	public HL7CustomException(String message) {
		super(message);
	}
	

	public HL7CustomException(String message, String responeType) {
		super(message);
		this.responeType = responeType;
	}
	public String getResponeType() {
		return responeType;
	}

	public void setResponeType(String responeType) {
		this.responeType = responeType;
	}
	
	public HL7CustomException(String message,String responeType , String  emailNotfMsg) {
		super(message);
		this.responeType = responeType;
		this.emailNotfMsg= emailNotfMsg;
	}

	public String getEmailNotfMsg() {
		return emailNotfMsg;
	}

	public void setEmailNotfMsg(String emailNotfMsg) {
		this.emailNotfMsg = emailNotfMsg;
	}

}
