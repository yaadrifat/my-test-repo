package com.velos.integration.espclient;

public enum VelosEspMethods {
	StudyGetStudyStat("getStudyStatuses"),
	StudyGetStudySummary("getStudySummary"),
	StudyCalGetStudyCalendar("getStudyCalendar"),
	StudyCalGetStudyCalendarList("getStudyCalendarList"),
	StudyPatAddStudyPatientStatus("addStudyPatientStatus"),
	StudyPatGetStudyPatients("getStudyPatients"),
	StudyPatEnrollPatientToStudy("enrollPatientToStudy"),
	PatDemogSearchPatient("searchPatient"),
	CreatePatient("createPatient"),
	StudyPatientStatus("studyPatientStatus"),
	SysAdminGetObjectInfoFromOID("getObjectInfoFromOID"),
	PatientDetails("getPatientDtails"),
	PatientStudyGetPatientStudyInfo("getPatientStudyInfo"),
	PatientStudyGetStudyPatientStatusHistory("getStudyPatientStatusHistory");
	;
	private String key;
	
	VelosEspMethods(String key) {
		this.key = key;
	}
	
	@Override
	public String toString() {
		return key;
	}
}
