
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for searchPatientResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="searchPatientResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PatientSearchResponse" type="{http://velos.com/services/}patientSearchResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "searchPatientResponse", propOrder = {
    "patientSearchResponse"
})
@XmlRootElement(name="searchPatientResponse", namespace = "http://velos.com/services/")
public class SearchPatientResponse {

    @XmlElement(name = "PatientSearchResponse")
    protected PatientSearchResponse patientSearchResponse;

    /**
     * Gets the value of the patientSearchResponse property.
     * 
     * @return
     *     possible object is
     *     {@link PatientSearchResponse }
     *     
     */
    public PatientSearchResponse getPatientSearchResponse() {
        return patientSearchResponse;
    }

    /**
     * Sets the value of the patientSearchResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientSearchResponse }
     *     
     */
    public void setPatientSearchResponse(PatientSearchResponse value) {
        this.patientSearchResponse = value;
    }

}
