
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getFilterStudyFormResponseResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getFilterStudyFormResponseResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StudyMashupFilterFormResponse" type="{http://velos.com/services/}studyFilterFormResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getFilterStudyFormResponseResponse", propOrder = {
    "studyMashupFilterFormResponse"
})
public class GetFilterStudyFormResponseResponse {

    @XmlElement(name = "StudyMashupFilterFormResponse")
    protected StudyFilterFormResponse studyMashupFilterFormResponse;

    /**
     * Gets the value of the studyMashupFilterFormResponse property.
     * 
     * @return
     *     possible object is
     *     {@link StudyFilterFormResponse }
     *     
     */
    public StudyFilterFormResponse getStudyMashupFilterFormResponse() {
        return studyMashupFilterFormResponse;
    }

    /**
     * Sets the value of the studyMashupFilterFormResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyFilterFormResponse }
     *     
     */
    public void setStudyMashupFilterFormResponse(StudyFilterFormResponse value) {
        this.studyMashupFilterFormResponse = value;
    }

}
