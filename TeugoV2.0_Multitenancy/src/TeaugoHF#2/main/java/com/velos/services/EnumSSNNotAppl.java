
package com.velos.services;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for enumSSNNotAppl.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="enumSSNNotAppl">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="YES"/>
 *     &lt;enumeration value="NO"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "enumSSNNotAppl")
@XmlEnum
public enum EnumSSNNotAppl {

    YES,
    NO;

    public String value() {
        return name();
    }

    public static EnumSSNNotAppl fromValue(String v) {
        return valueOf(v);
    }

}
