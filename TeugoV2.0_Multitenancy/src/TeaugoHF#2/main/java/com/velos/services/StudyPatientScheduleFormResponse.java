
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for studyPatientScheduleFormResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="studyPatientScheduleFormResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}studyPatientFormResponse">
 *       &lt;sequence>
 *         &lt;element name="scheduleEventIdentifier" type="{http://velos.com/services/}scheduleEventIdentifier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "studyPatientScheduleFormResponse", propOrder = {
    "scheduleEventIdentifier"
})
public class StudyPatientScheduleFormResponse
    extends StudyPatientFormResponse
{

    protected ScheduleEventIdentifier scheduleEventIdentifier;

    /**
     * Gets the value of the scheduleEventIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link ScheduleEventIdentifier }
     *     
     */
    public ScheduleEventIdentifier getScheduleEventIdentifier() {
        return scheduleEventIdentifier;
    }

    /**
     * Sets the value of the scheduleEventIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link ScheduleEventIdentifier }
     *     
     */
    public void setScheduleEventIdentifier(ScheduleEventIdentifier value) {
        this.scheduleEventIdentifier = value;
    }

}
