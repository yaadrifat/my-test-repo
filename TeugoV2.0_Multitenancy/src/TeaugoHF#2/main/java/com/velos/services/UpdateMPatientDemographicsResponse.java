
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updateMPatientDemographicsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updateMPatientDemographicsResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="updateMPatientDemographics" type="{http://velos.com/services/}responseHolder" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateMPatientDemographicsResponse", propOrder = {
    "updateMPatientDemographics"
})
public class UpdateMPatientDemographicsResponse {

    protected ResponseHolder updateMPatientDemographics;

    /**
     * Gets the value of the updateMPatientDemographics property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseHolder }
     *     
     */
    public ResponseHolder getUpdateMPatientDemographics() {
        return updateMPatientDemographics;
    }

    /**
     * Sets the value of the updateMPatientDemographics property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseHolder }
     *     
     */
    public void setUpdateMPatientDemographics(ResponseHolder value) {
        this.updateMPatientDemographics = value;
    }

}
