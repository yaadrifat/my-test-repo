
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updateMEventStatus complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updateMEventStatus">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ScheduleEventStatuses" type="{http://velos.com/services/}scheduleEventStatuses" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateMEventStatus", propOrder = {
    "scheduleEventStatuses"
})
public class UpdateMEventStatus {

    @XmlElement(name = "ScheduleEventStatuses")
    protected ScheduleEventStatuses scheduleEventStatuses;

    /**
     * Gets the value of the scheduleEventStatuses property.
     * 
     * @return
     *     possible object is
     *     {@link ScheduleEventStatuses }
     *     
     */
    public ScheduleEventStatuses getScheduleEventStatuses() {
        return scheduleEventStatuses;
    }

    /**
     * Sets the value of the scheduleEventStatuses property.
     * 
     * @param value
     *     allowed object is
     *     {@link ScheduleEventStatuses }
     *     
     */
    public void setScheduleEventStatuses(ScheduleEventStatuses value) {
        this.scheduleEventStatuses = value;
    }

}
