package com.velos.integration.adt.processor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.camel.CamelContext;
import org.apache.camel.CamelExecutionException;
import org.apache.camel.ExchangePattern;
import org.apache.camel.ProducerTemplate;
import org.apache.cxf.message.MessageContentsList;
import org.apache.cxf.ws.security.wss4j.WSS4JOutInterceptor;
import org.apache.log4j.Logger;
import org.apache.ws.security.WSConstants;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import com.velos.integration.adtx.webservice.SimpleAuthCallbackHandler;
import com.velos.integration.adtx.webservice.VelosEspPatientStudyServiceEndpoint;
import com.velos.integration.adtx.webservice.VelosEspSearchPatientEndpoint;
import com.velos.integration.adtx.webservice.VelosEspSearchUserEndpoint;
import com.velos.integration.adtx.webservice.VelosEspStudySummaryEndpoint;
import com.velos.integration.adtx.webservice.VelosEspUpdatePatientEndpoint;
import com.velos.integration.adtx.webservice.VelosEspUpdatePatientFacilityEndpoint;
import com.velos.integration.hl7.notifications.HL7CustomException;
import com.velos.integration.hl7.util.StringToXMLGregorianCalendar;
import com.velos.services.Code;
import com.velos.services.Codes;
import com.velos.services.OperationException_Exception;
import com.velos.services.OperationRolledBackException_Exception;
import com.velos.services.OrganizationIdentifier;
import com.velos.services.PatientDataBean;
import com.velos.services.PatientIdentifier;
import com.velos.services.PatientOrganization;
import com.velos.services.PatientOrganizationIdentifier;
import com.velos.services.PatientSearch;
import com.velos.services.PatientSearchResponse;
import com.velos.services.PatientStudy;
import com.velos.services.ResponseHolder;
import com.velos.services.StudyIdentifier;
import com.velos.services.StudySummary;
import com.velos.services.UpdatePatientDemographics;
import com.velos.services.UserSearch;
import com.velos.services.UserSearchResults;

public class ADTPostProcessorHelper implements VelosEspSearchPatientEndpoint,
VelosEspUpdatePatientEndpoint, VelosEspUpdatePatientFacilityEndpoint,VelosEspPatientStudyServiceEndpoint
,VelosEspStudySummaryEndpoint,VelosEspSearchUserEndpoint{
	private static Logger logger = Logger.getLogger(ADTPostProcessorHelper.class);
	private CamelContext context;

	ADTPostProcessorHelper() {
		configureCamel();
	}

	public CamelContext getContext() {
		return context;
	}

	public void setContext(CamelContext context) {
		this.context = context;
	}

	private CamelContext configureCamel() {
		if (this.context == null) {
			@SuppressWarnings("resource")
			ClassPathXmlApplicationContext springContext = new ClassPathXmlApplicationContext(
					"camel-config.xml");
			this.setContext((CamelContext) springContext.getBean("camel"));
		}

		System.out.println("ADT Post PRocessor");
		return getContext();
	}





	public List<PatientDataBean> searchPatientHelper(
			Map<String, String> resultMap, Properties prop, String eventType,
			int searchType)
			// searchType 1 means search with all filters, 2 means MRN search, 3 means
			// Demographics search
					throws HL7CustomException {
		PatientSearch patientSearch = new PatientSearch();
		PatientSearchResponse patientSearchResponse;
		logger.info("****************organization*******************" + prop.containsKey("organization"));
		logger.info("****************organization*******************" + prop.getProperty("organization").toString().trim());
		if (prop.containsKey("organization")) {
			OrganizationIdentifier organizationIdentifier = new OrganizationIdentifier();
			organizationIdentifier.setSiteName(prop.getProperty("organization")
					.toString().trim());
			patientSearch.setPatOrganization(organizationIdentifier);
			setPatientSearchFilters(patientSearch, resultMap, prop, eventType,
					searchType);
		}
		else{
			setPatientSearchFilters(patientSearch, resultMap, prop, eventType,
					searchType);
		}
		try {
			logger.info("****************before searchPatient*******************");
			patientSearchResponse = searchPatient(patientSearch);
			logger.info("****************after searchPatient*******************");
		} catch (Exception e) {
			logger.info("****************Exception*******************" + e.getMessage());
			throw new HL7CustomException(e.getMessage(), "AR");
		}
		return patientSearchResponse.getPatDataBean();

	}

	public void setPatientSearchFilters(PatientSearch patientSearch,
			Map<String, String> resultMap, Properties prop, String eventType,
			int searchType) {
		patientSearch.setExactSearch(true);
		if (searchType == 1 || searchType == 2) {
			if (prop.containsKey(eventType + ".patientFacilityId")
					&& !"".equals(prop
							.getProperty(eventType + ".patientFacilityId")
							.toString().trim())) {
				System.out.println("#####################patientFacilityId ########################");
				patientSearch.setPatientFacilityID(resultMap.get(prop
						.getProperty(eventType + ".patientFacilityId")
						.toString().trim()));
				patientSearch.setDistinctFacilityRequired(true);
			}

			if (prop.containsKey(eventType + ".patientId")
					&& !"".equals(prop.getProperty(eventType + ".patientId")
							.toString().trim())) {
				System.out.println("#####################Patient id ########################");
				patientSearch.setPatientID(resultMap.get(prop
						.getProperty(eventType + ".patientId").toString()
						.trim()));
			}

		}

		if (searchType == 1 || searchType == 3) {
			if (prop.containsKey(eventType + ".gender")
					&& !"".equals(prop.getProperty(eventType + ".gender")
							.toString().trim()) && resultMap.get(prop
									.getProperty(eventType + ".gender").toString().trim()) != null) {
				Code gender = new Code();
				gender.setCode(resultMap.get(prop
						.getProperty(eventType + ".gender").toString().trim()));
				gender.setType("gender");
				patientSearch.setGender(gender);
			}

			if (prop.containsKey(eventType + ".race")
					&& !"".equals(prop.getProperty(eventType + ".race")
							.toString().trim()) && resultMap.get(prop
									.getProperty(eventType + ".race").toString().trim()) != null) {
				Code race = new Code();
				race.setCode(resultMap.get(prop
						.getProperty(eventType + ".race").toString().trim()));
				race.setType("race");
				patientSearch.setRace(race);
			}

			if (prop.containsKey(eventType + ".firstname")
					&& !"".equals(prop.getProperty(eventType + ".firstname")
							.toString().trim())) {
				patientSearch.setPatFirstName(resultMap.get(prop
						.getProperty(eventType + ".firstname").toString()
						.trim()));
			}

			if (prop.containsKey(eventType + ".lastname")
					&& !"".equals(prop.getProperty(eventType + ".lastname")
							.toString().trim())) {
				patientSearch.setPatLastName(resultMap
						.get(prop.getProperty(eventType + ".lastname")
								.toString().trim()));
			}

			if (prop.containsKey(eventType + ".dob")
					&& !"".equals(prop.getProperty(eventType + ".dob")
							.toString().trim())
							&& prop.containsKey(eventType + ".dateformat")
							&& !"".equals(prop.getProperty(eventType + ".dateformat")
									.toString().trim()) && resultMap.get(prop.getProperty(eventType + ".dob")
											.toString().trim()) != null) {
				XMLGregorianCalendar result = StringToXMLGregorianCalendar
						.convertStringDateToXmlGregorianCalendar(
								resultMap.get(prop
										.getProperty(eventType + ".dob")
										.toString().trim()),
										prop.getProperty(eventType + ".dateformat")
										.toString().trim(), false);
				patientSearch.setPatDateofBirth(result);
			}
		}
	}

	public boolean updatePatientFacilityHelper(Map<String, String> resultMap,
			Properties prop, String eventType,
			List<PatientDataBean> patientDataList) throws HL7CustomException {
		PatientIdentifier patientIdentifier = new PatientIdentifier();
		OrganizationIdentifier organizationIdentifier = new OrganizationIdentifier();
		PatientOrganizationIdentifier patientOrganizationIdentifier = null;
		PatientOrganization patientOrganization = new PatientOrganization();
		logger.info("updatePatientFacilityHelper MRN and ORG");
		for (PatientDataBean patientData : patientDataList) {
			patientIdentifier = patientData.getPatientIdentifier();
			organizationIdentifier = patientData.getPatientIdentifier()
					.getOrganizationId();
			patientOrganization.setFacilityID(resultMap.get(prop
					.getProperty(eventType + ".mrn").toString().trim()));
			patientOrganization.setOrganizationID(patientData
					.getPatientIdentifier().getOrganizationId());
			try {
				updatePatientFacility(patientIdentifier,
						organizationIdentifier, patientOrganizationIdentifier,
						patientOrganization);
			} catch (Exception e) {
				logger.info("Error Message while updatePatientFacilityHelper : " + e.getCause().getMessage());
				throw new HL7CustomException(
						"Exception while merge Patient in eResearch. Please contact System Administrator",
						"AE");
			}
		}
		return true;
	}

	public boolean updatePatientHelper(Map<String, String> resultMap,
			Properties prop, String eventType,
			List<PatientDataBean> patientDataList) throws HL7CustomException {
		logger.info("*** Update Patient Helper ****");
		PatientIdentifier patientIdentifier = new PatientIdentifier();
		UpdatePatientDemographics updatePatientDemographics = new UpdatePatientDemographics();

		for (PatientDataBean patientData : patientDataList) {
			patientIdentifier = patientData.getPatientIdentifier();
			setUpdatePatientFields(resultMap, prop, eventType, patientData,
					updatePatientDemographics);

			logger.info("PatientDataList Size Before Update------>"+patientDataList.size());

			try {
				updatePatient(patientIdentifier, updatePatientDemographics);
			} catch (Exception e) {
				logger.info("Error Message while updatePatientHelper : " + e.getCause().getMessage());
				logger.error("Error while update patient in eResearch. Please Contact System Administrator");
				throw new HL7CustomException(
						e.getCause().getMessage(),
						"AE");
			}
		}
		return true;
	}

	public void setUpdatePatientFields(Map<String, String> resultMap,
			Properties prop, String eventType, PatientDataBean patientData,
			UpdatePatientDemographics updatePatientDemographics) throws HL7CustomException {
		logger.info("***** Setting UpdatePatientFeilds values ****");
		updatePatientDemographics.setPatientIdentifier(patientData
				.getPatientIdentifier());

		String patStatus =null;
		patStatus=resultMap.get(prop.getProperty(eventType + ".survivalstatus").toString().trim());
		String deathCodSubtyp = null;
		try{
			deathCodSubtyp=prop.getProperty("survivalstatus_code_for_death");

			if(deathCodSubtyp == null){
				throw new HL7CustomException("Death codlist subtype configurations missed in adt.properties", "AE");
			}

		}catch(Exception e){
			logger.info("Death codlist subtype configurations missed in adt.properties");
			throw new HL7CustomException("Death codlist subtype configurations missed in adt.properties", "AE");
		}

		logger.info("patient Status ================>"+patStatus);
		if (prop.containsKey(eventType + ".gender")
				&& !"".equals(prop.getProperty(eventType + ".gender")
						.toString().trim())) {
			Code gender = new Code();
			gender.setCode(this.isEmptyValue(resultMap.get(prop
					.getProperty(eventType + ".gender").toString().trim())));
			gender.setType("gender");
			updatePatientDemographics.setGender(gender);
		}

		if (prop.containsKey(eventType + ".race")
				&& !"".equals(prop.getProperty(eventType + ".race").toString()
						.trim())) {
			Code race = new Code();
			race.setCode(this.isEmptyValue(resultMap.get(prop.getProperty(eventType + ".race")
					.toString().trim())));
			race.setType("race");
			updatePatientDemographics.setRace(race);
		}

		if (prop.containsKey(eventType + ".ethnicity")
				&& !"".equals(prop.getProperty(eventType + ".ethnicity")
						.toString().trim())) {
			Code ethnicity = new Code();
			ethnicity.setCode(this.isEmptyValue(resultMap.get(prop
					.getProperty(eventType + ".ethnicity").toString().trim())));
			ethnicity.setType("ethnicity");
			updatePatientDemographics.setEthnicity(ethnicity);
		}


		if (prop.containsKey(eventType + ".maritalstatus")
				&& !"".equals(prop.getProperty(eventType + ".maritalstatus")
						.toString().trim()) ) {
			Code maritalStatus = new Code();
			maritalStatus.setCode(this.isEmptyValue(resultMap.get(prop
					.getProperty(eventType + ".maritalstatus").toString()
					.trim())));
			maritalStatus.setType("marital_st");
			updatePatientDemographics.setMaritalStatus(maritalStatus);
		}

		if (prop.containsKey(eventType + ".survivalstatus")
				&& !"".equals(prop.getProperty(eventType + ".survivalstatus")
						.toString().trim())&& !this.isEmpty(resultMap.get(prop
								.getProperty(eventType + ".survivalstatus").toString().trim()))) {


			if(patStatus.equals(deathCodSubtyp) && resultMap.get(prop
					.getProperty(eventType + ".deathdate").toString().trim()) == null ){

				throw new HL7CustomException("Death Date is Mandatory for Expired Patients","AE");

			}

			Code survivalStatus = new Code();
			survivalStatus.setCode(this.isEmptyValue(resultMap.get(prop
					.getProperty(eventType + ".survivalstatus").toString()
					.trim())));
			survivalStatus.setType("patient_status");
			updatePatientDemographics.setSurvivalStatus(survivalStatus);
		}

		if (prop.containsKey(eventType + ".firstname")
				&& !"".equals(prop.getProperty(eventType + ".firstname")
						.toString().trim())) {
			updatePatientDemographics.setFirstName(this.isEmptyValue(resultMap.get(prop
					.getProperty(eventType + ".firstname").toString().trim())));
		}

		if (prop.containsKey(eventType + ".middlename")
				&& !"".equals(prop.getProperty(eventType + ".middlename")
						.toString().trim())) {
			updatePatientDemographics.setMiddleName(this.isEmptyValue(resultMap.get(prop
					.getProperty(eventType + ".middlename").toString().trim())));
		}

		if (prop.containsKey(eventType + ".lastname")
				&& !"".equals(prop.getProperty(eventType + ".lastname")
						.toString().trim())) {
			updatePatientDemographics.setLastName(this.isEmptyValue(resultMap.get(prop
					.getProperty(eventType + ".lastname").toString().trim())));
		}

		if (prop.containsKey(eventType + ".dob")
				&& !"".equals(prop.getProperty(eventType + ".dob").toString()
						.trim())
						&& prop.containsKey(eventType + ".dateformat")
						&& !"".equals(prop.getProperty(eventType + ".dateformat")
								.toString().trim()) && !this.isEmpty(resultMap.get(prop
										.getProperty(eventType + ".dob").toString().trim()))) {
			XMLGregorianCalendar result = StringToXMLGregorianCalendar
					.convertStringDateToXmlGregorianCalendar(
							resultMap.get(prop.getProperty(eventType + ".dob")
									.toString().trim()),
									prop.getProperty(eventType + ".dateformat")
									.toString().trim(), false);
			updatePatientDemographics.setDateOfBirth(result);
		}

		if (prop.containsKey(eventType + ".deathdate")
				&& !"".equals(prop.getProperty(eventType + ".deathdate")
						.toString().trim())
						&& prop.containsKey(eventType + ".dateformat")
						&& !"".equals(prop.getProperty(eventType + ".dateformat")
								.toString().trim()) && !this.isEmpty(resultMap.get(prop
										.getProperty(eventType + ".deathdate").toString().trim())) ) {

			/*String deathDate =resultMap.get(prop
					.getProperty(eventType + ".deathdate").toString().trim());*/
			try{
				if(!patStatus.equals(prop.getProperty("survivalstatus_code_for_death"))){

					throw new HL7CustomException("SurvivalStatus should not be null or miss match for DeathDate ", "AE");
				}}catch(Exception e){
					throw new HL7CustomException("SurvivalStatus should not be null or miss match configurations in Db ", "AE");
				}

			XMLGregorianCalendar result = StringToXMLGregorianCalendar
					.convertStringDateToXmlGregorianCalendar(
							resultMap.get(prop
									.getProperty(eventType + ".deathdate")
									.toString().trim()),
									prop.getProperty(eventType + ".dateformat")
									.toString().trim(), false);
			updatePatientDemographics.setDeathDate(result);
		}

		if (prop.containsKey(eventType + ".address1")
				&& !"".equals(prop.getProperty(eventType + ".address1")
						.toString().trim())) {
			updatePatientDemographics.setAddress1(this.isEmptyValue(resultMap.get(prop
					.getProperty(eventType + ".address1").toString().trim())));
		}

		if (prop.containsKey(eventType + ".address2")
				&& !"".equals(prop.getProperty(eventType + ".address2")
						.toString().trim()) ) {
			updatePatientDemographics.setAddress2(this.isEmptyValue(resultMap.get(prop
					.getProperty(eventType + ".address2").toString().trim())));
		}

		if (prop.containsKey(eventType + ".city")
				&& !"".equals(prop.getProperty(eventType + ".city").toString()
						.trim())) {
			updatePatientDemographics.setCity(this.isEmptyValue(resultMap.get(prop
					.getProperty(eventType + ".city").toString().trim())));
		}

		if (prop.containsKey(eventType + ".state")
				&& !"".equals(prop.getProperty(eventType + ".state").toString()
						.trim())) {
			updatePatientDemographics.setState(this.isEmptyValue(resultMap.get(prop
					.getProperty(eventType + ".state").toString().trim())));
		}

		if (prop.containsKey(eventType + ".zipcode")
				&& !"".equals(prop.getProperty(eventType + ".zipcode")
						.toString().trim())) {
			updatePatientDemographics.setZipCode(this.isEmptyValue(resultMap.get(prop
					.getProperty(eventType + ".zipcode").toString().trim())));
		}

		if (prop.containsKey(eventType + ".country")
				&& !"".equals(prop.getProperty(eventType + ".country")
						.toString().trim())) {

			updatePatientDemographics.setCountry(this.isEmptyValue(resultMap.get(prop
					.getProperty(eventType + ".country").toString().trim())));
		}

		if (prop.containsKey(eventType + ".county")
				&& !"".equals(prop.getProperty(eventType + ".county")
						.toString().trim())) {


			updatePatientDemographics.setCounty(this.isEmptyValue(resultMap.get(prop
					.getProperty(eventType + ".county").toString().trim())));
		}

		if (prop.containsKey(eventType + ".email")
				&& !"".equals(prop.getProperty(eventType + ".email").toString()
						.trim())) {

			updatePatientDemographics.setEMail(this.isEmptyValue(resultMap.get(prop
					.getProperty(eventType + ".email").toString().trim())));
		}

		if (prop.containsKey(eventType + ".ssn")
				&& !"".equals(prop.getProperty(eventType + ".ssn").toString()
						.trim())) {
			updatePatientDemographics.setSSN(this.isEmptyValue(resultMap.get(prop
					.getProperty(eventType + ".ssn").toString().trim())));
		}

		if (prop.containsKey(eventType + ".homephone")
				&& !"".equals(prop.getProperty(eventType + ".homephone")
						.toString().trim())) {
			updatePatientDemographics.setHomePhone(this.isEmptyValue(resultMap.get(prop
					.getProperty(eventType + ".homephone").toString().trim())));
		}

		if (prop.containsKey(eventType + ".workphone")
				&& !"".equals(prop.getProperty(eventType + ".workphone")
						.toString().trim())) {
			updatePatientDemographics.setWorkPhone(this.isEmptyValue(resultMap.get(prop
					.getProperty(eventType + ".workphone").toString().trim())));
		}

		if (prop.containsKey(eventType + ".notes")
				&& !"".equals(prop.getProperty(eventType + ".notes")
						.toString().trim())) {
			updatePatientDemographics.setNotes(this.isEmptyValue(resultMap.get(prop
					.getProperty(eventType + ".notes").toString().trim())));
		}
		if (prop.containsKey(eventType + ".addlethnicity")
				&& !"".equals(prop.getProperty(eventType + ".addlethnicity")
						.toString().trim())) {logger.info("**Additional Ethnicity ***");

						Code addlEthnicityCode = new Code();
						Codes addlEthnicityCodes = new Codes();
						List<Code> addlEthnicityList = new ArrayList<Code>();

						addlEthnicityCode.setCode(this.isEmptyValue(resultMap.get(prop
								.getProperty(eventType + ".addlethnicity").toString().trim())));

						addlEthnicityList.add(addlEthnicityCode);

						addlEthnicityCodes.setCodes(addlEthnicityList);

						updatePatientDemographics.setAdditionalEthnicity(addlEthnicityCodes);

		}

		if (prop.containsKey(eventType + ".addlrace")
				&& !"".equals(prop.getProperty(eventType + ".addlrace")
						.toString().trim())) {logger.info("**Additional Race ***");
						
						Code addlRaceCode = new Code();
						Codes addlRaceCodes = new Codes();
						List<Code> addlRaceList = new ArrayList<Code>();

						addlRaceCode.setCode(this.isEmptyValue(resultMap.get(prop
								.getProperty(eventType + ".addlrace").toString().trim())));
						
						addlRaceList.add(addlRaceCode);
						
						addlRaceCodes.setCodes(addlRaceList);
						updatePatientDemographics.setAdditionalRace(addlRaceCodes);
		}


		if (prop.containsKey(eventType + ".reasonforchange")
				&& !"".equals(prop.getProperty(eventType + ".reasonforchange")
						.toString().trim())&& !this.isEmpty(resultMap.get(prop
								.getProperty(eventType + ".reasonforchange").toString().trim()))) {
			updatePatientDemographics.setReasonForChange(prop
					.getProperty(eventType + ".reasonforchange").toString().trim());
		}
		else
		{
			updatePatientDemographics.setReasonForChange("Updated Demographics Information through ADT");
		}

	}

	@Override
	public PatientSearchResponse searchPatient(PatientSearch patientSearch) throws Exception {
		try
		{logger.info("inside searchPatient");

		ProducerTemplate producer = context.createProducerTemplate();
		logger.info("inside searchPatient before sendBody");
		MessageContentsList list = (MessageContentsList) producer.sendBody(
				"cxf:bean:espSearchPatientEndpoint", ExchangePattern.InOut,
				patientSearch);
		logger.info("inside searchPatient after sendBody");
		return (PatientSearchResponse) list.get(0);
		}
		catch(Exception e)
		{

			logger.info("inside SearchPatient Catch");
			logger.info("Error Message while SearchPatient : " + e.getCause().getMessage());
			logger.info(e.getMessage());
			throw new HL7CustomException(e.getCause().getMessage(),"AE");
		}


	}

	@Override
	public ResponseHolder updatePatient(PatientIdentifier patientIdentifier,
			UpdatePatientDemographics patientDemographics)
					throws Exception {
		MessageContentsList list = null;
		try{
			logger.info("Before calling updatePatient eSP");
			ProducerTemplate producer = context.createProducerTemplate();
			MessageContentsList inpList = new MessageContentsList();
			inpList.add(patientIdentifier);
			inpList.add(patientDemographics);
			list = (MessageContentsList) producer.sendBody(
					"cxf:bean:espUpdatePatientEndpoint", ExchangePattern.InOut,
					inpList);
			logger.info("After calling updatePatient eSP");
		}catch(Exception e){
			logger.info("Error Message while updatePatient : " + e.getCause().getMessage());
			throw e;
		}
		return (ResponseHolder) list.get(0);
	}

	@Override
	public ResponseHolder updatePatientFacility(
			PatientIdentifier patientIdentifier,
			OrganizationIdentifier organizationIdentifier,
			PatientOrganizationIdentifier patientOrganizationIdentifier,
			PatientOrganization patientOrganization)
					throws OperationException_Exception,
					OperationRolledBackException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList inpList = new MessageContentsList();
		inpList.add(patientIdentifier);
		inpList.add(organizationIdentifier);
		inpList.add(patientOrganizationIdentifier);
		inpList.add(patientOrganization);
		MessageContentsList list = (MessageContentsList) producer.sendBody(
				"cxf:bean:espUpdatePatientFacilityEndpoint",
				ExchangePattern.InOut, inpList);
		return (ResponseHolder) list.get(0);
	}

	//Added For Study Coordinators values 27Oct2015 by Surendra.M
	@Override
	public List<PatientStudy> getPatientStudies(
			PatientIdentifier patientIdentifier)
					throws OperationException_Exception, HL7CustomException {

		try{
			ProducerTemplate producer = context.createProducerTemplate();
			MessageContentsList list = (MessageContentsList) producer.sendBody(
					"cxf:bean:espPatientStudyServiceEndpoint",
					ExchangePattern.InOut,patientIdentifier);
			return (List<PatientStudy>) list.get(0);

		}catch(Exception e){

		}

		return null;

	}





	@Override
	public StudySummary getStudySummary(StudyIdentifier studyIdentifier)
			throws OperationException_Exception {
		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList list = (MessageContentsList) producer.sendBody(
				"cxf:bean:espStudySummaryEndpoint",ExchangePattern.InOut,studyIdentifier);

		return  (StudySummary) list.get(0);
	}

	private boolean isEmpty(String s)
	{
		if(s == null || s.length() == 0)
			return true;
		return false;
	}
	private String isEmptyValue(String s)
	{
		if(s == null || s.length() == 0)
			return "";
		return s;
	}

	@Override
	public UserSearchResults searchUser(UserSearch userSearch)
			throws OperationException_Exception {

		ProducerTemplate producer = context.createProducerTemplate();
		MessageContentsList list = (MessageContentsList) producer.sendBody(
				"cxf:bean:espSearchUserEndpoint",
				ExchangePattern.InOut,userSearch);

		return (UserSearchResults) list.get(0);
	}




}
