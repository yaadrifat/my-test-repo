
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for studyMashup complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="studyMashup">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="study" type="{http://velos.com/services/}study" minOccurs="0"/>
 *         &lt;element name="studyFormDesign" type="{http://velos.com/services/}studyFormDesign" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "studyMashup", propOrder = {
    "study",
    "studyFormDesign"
})
public class StudyMashup
    extends ServiceObject
{

    protected Study study;
    protected StudyFormDesign studyFormDesign;

    /**
     * Gets the value of the study property.
     * 
     * @return
     *     possible object is
     *     {@link Study }
     *     
     */
    public Study getStudy() {
        return study;
    }

    /**
     * Sets the value of the study property.
     * 
     * @param value
     *     allowed object is
     *     {@link Study }
     *     
     */
    public void setStudy(Study value) {
        this.study = value;
    }

    /**
     * Gets the value of the studyFormDesign property.
     * 
     * @return
     *     possible object is
     *     {@link StudyFormDesign }
     *     
     */
    public StudyFormDesign getStudyFormDesign() {
        return studyFormDesign;
    }

    /**
     * Sets the value of the studyFormDesign property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyFormDesign }
     *     
     */
    public void setStudyFormDesign(StudyFormDesign value) {
        this.studyFormDesign = value;
    }

}
