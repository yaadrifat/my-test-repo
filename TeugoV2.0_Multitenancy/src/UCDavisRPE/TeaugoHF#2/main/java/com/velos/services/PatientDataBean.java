
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for patientDataBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="patientDataBean">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}simpleIdentifier">
 *       &lt;sequence>
 *         &lt;element name="gender" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="patDateofBirth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="patFirstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="patLastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="patSurvivalStat" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="patientFacilityId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="patientIdentifier" type="{http://velos.com/services/}patientIdentifier" minOccurs="0"/>
 *         &lt;element name="race" type="{http://velos.com/services/}code" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "patientDataBean", propOrder = {
    "gender",
    "patDateofBirth",
    "patFirstName",
    "patLastName",
    "patSurvivalStat",
    "patientFacilityId",
    "patientIdentifier",
    "race"
})
public class PatientDataBean
    extends SimpleIdentifier
{

    protected Code gender;
    protected String patDateofBirth;
    protected String patFirstName;
    protected String patLastName;
    protected Code patSurvivalStat;
    protected String patientFacilityId;
    protected PatientIdentifier patientIdentifier;
    protected Code race;

    /**
     * Gets the value of the gender property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getGender() {
        return gender;
    }

    /**
     * Sets the value of the gender property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setGender(Code value) {
        this.gender = value;
    }

    /**
     * Gets the value of the patDateofBirth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPatDateofBirth() {
        return patDateofBirth;
    }

    /**
     * Sets the value of the patDateofBirth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPatDateofBirth(String value) {
        this.patDateofBirth = value;
    }

    /**
     * Gets the value of the patFirstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPatFirstName() {
        return patFirstName;
    }

    /**
     * Sets the value of the patFirstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPatFirstName(String value) {
        this.patFirstName = value;
    }

    /**
     * Gets the value of the patLastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPatLastName() {
        return patLastName;
    }

    /**
     * Sets the value of the patLastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPatLastName(String value) {
        this.patLastName = value;
    }

    /**
     * Gets the value of the patSurvivalStat property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getPatSurvivalStat() {
        return patSurvivalStat;
    }

    /**
     * Sets the value of the patSurvivalStat property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setPatSurvivalStat(Code value) {
        this.patSurvivalStat = value;
    }

    /**
     * Gets the value of the patientFacilityId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPatientFacilityId() {
        return patientFacilityId;
    }

    /**
     * Sets the value of the patientFacilityId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPatientFacilityId(String value) {
        this.patientFacilityId = value;
    }

    /**
     * Gets the value of the patientIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link PatientIdentifier }
     *     
     */
    public PatientIdentifier getPatientIdentifier() {
        return patientIdentifier;
    }

    /**
     * Sets the value of the patientIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientIdentifier }
     *     
     */
    public void setPatientIdentifier(PatientIdentifier value) {
        this.patientIdentifier = value;
    }

    /**
     * Gets the value of the race property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getRace() {
        return race;
    }

    /**
     * Sets the value of the race property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setRace(Code value) {
        this.race = value;
    }

}
