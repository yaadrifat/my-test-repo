package com.velos.integration.adtx.webservice;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;
import com.velos.services.OperationException_Exception;
import com.velos.services.OperationRolledBackException_Exception;

@WebService(targetNamespace = "http://velos.com/services/", name = "PatientDemographicsSEI")
public interface VelosEspUpdatePatientFacilityEndpoint {
	@WebResult(name = "updatePatientFacility", targetNamespace = "")
	@RequestWrapper(localName = "updatePatientFacility", targetNamespace = "http://velos.com/services/", className = "com.velos.services.UpdatePatientFacility")
	@WebMethod
	@ResponseWrapper(localName = "updatePatientFacilityResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.UpdatePatientFacilityResponse")
	public com.velos.services.ResponseHolder updatePatientFacility(
			@WebParam(name = "PatientIdentifier", targetNamespace = "") com.velos.services.PatientIdentifier patientIdentifier,
			@WebParam(name = "OrganizationIdentifier", targetNamespace = "") com.velos.services.OrganizationIdentifier organizationIdentifier,
			@WebParam(name = "PatientOrganizationIdentifier", targetNamespace = "") com.velos.services.PatientOrganizationIdentifier patientOrganizationIdentifier,
			@WebParam(name = "PatientOrganization", targetNamespace = "") com.velos.services.PatientOrganization patientOrganization)
			throws OperationException_Exception,
			OperationRolledBackException_Exception;
}
