package com.velos.integration.hl7.processor;

import java.io.IOException;

import ca.uhn.hl7v2.HL7Exception;

import com.velos.integration.hl7.bean.MessageBean;
import com.velos.integration.hl7.dao.MessageConfiguration;
import com.velos.integration.hl7.notifications.HL7CustomException;

public class ComparisonUtil {
	private MessageConfiguration messageConfiguration;



	public MessageConfiguration getMessageConfiguration() {
		return messageConfiguration;
	}



	public void setMessageConfiguration(MessageConfiguration messageConfiguration) {
		this.messageConfiguration = messageConfiguration;
	}

	

	public boolean messageTypeComparison(String messagetype)  throws HL7CustomException,IOException {
		boolean result=false;
		
		try{
	
		for(MessageBean mb:getMessageConfiguration().getMessageConfigurationBean()){
			if(messagetype.equals(mb.getMesssageType()))
			{
				result= true;
				break;
			}
			else{
				result=false;
			}
		}
	
		} catch (Exception e) {
			throw new HL7CustomException(e.getMessage(),"AE");
		}	
		return result;
	}






}
