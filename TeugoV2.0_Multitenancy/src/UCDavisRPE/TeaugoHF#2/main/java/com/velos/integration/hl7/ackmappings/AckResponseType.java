package com.velos.integration.hl7.ackmappings;

public enum AckResponseType {
	
	MSH("MSH"),
	Pipe("|"),
	EncodingCharacters("^~\\&"),
	ResponseMessageType("ACK_"),
	MessageType("ACK")
	;
	
	private String key;

	
	private AckResponseType(String key) {
		this.key=key;
	}
	
	public String toString(){
		return key;
	}
 
	
}
