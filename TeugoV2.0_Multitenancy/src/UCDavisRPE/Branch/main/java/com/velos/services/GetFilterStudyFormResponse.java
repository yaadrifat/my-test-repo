
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getFilterStudyFormResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getFilterStudyFormResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StudyFormFilterParams" type="{http://velos.com/services/}filterParams" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getFilterStudyFormResponse", propOrder = {
    "studyFormFilterParams"
})
public class GetFilterStudyFormResponse {

    @XmlElement(name = "StudyFormFilterParams")
    protected FilterParams studyFormFilterParams;

    /**
     * Gets the value of the studyFormFilterParams property.
     * 
     * @return
     *     possible object is
     *     {@link FilterParams }
     *     
     */
    public FilterParams getStudyFormFilterParams() {
        return studyFormFilterParams;
    }

    /**
     * Sets the value of the studyFormFilterParams property.
     * 
     * @param value
     *     allowed object is
     *     {@link FilterParams }
     *     
     */
    public void setStudyFormFilterParams(FilterParams value) {
        this.studyFormFilterParams = value;
    }

}
