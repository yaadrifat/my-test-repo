package com.velos.integration.gateway;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import com.velos.integration.mapping.ProtocolKeys;
import com.velos.integration.mapping.VelosKeys;

public class RetrieveProtocolDefOutput {
	private static final String outputTemplate =
			"<RetrieveProtocolDefResponse xmlns:ns2=\"urn:ihe:qrph:rpe:2009\" xmlns=\"urn:hl7-org:v3\">"+
					"<query extension=\"%s\" root=\"8.3.5.6.7.78\"/>"+
					"<protocolDef>"+
					"<plannedStudy classCode=\"CLNTRL\" moodCode=\"DEF\" ITSVersion=\"XML_1.0\" "+
					"xmlns=\"urn:hl7-org:v3\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "+
					"xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">"+
				    "<templateId><item root=\"8.3.5.6.7.78\"/></templateId>"+
				    "<id root=\"8.3.5.6.7.78\" extension=\"%s\"/>"+
				    "<title>%s</title>"+
				    "<text>%s</text>"+
				    "%s"+ // <= Component4 added here
				    "%s"+ // <= SubjectOf added here
				    "</plannedStudy>"+
				    "</protocolDef>"+
					"</RetrieveProtocolDefResponse>";
	
	private static final String component4Part1Template =
			"<!-- Timing events, one component4 for each calendar in the study-->"+
					"<component4 typeCode=\"COMP\">"+
					"<timePointEventDefinition classCode=\"CTTEVENT\" moodCode=\"DEF\">"+
					"<templateId><item root=\"8.3.5.6.7.78.2.2\"/></templateId>"+
					"<id root=\"8.3.5.6.7.78.2.2\" extension=\"CELL.%s\"/>"+
					"<code code=\"CELL\" codeSystem=\"8.3.5.6.7.78.2.2\">"+
					"<displayName value=\"%s\"/>"+
					"</code>"+
					"<component1 typeCode=\"COMP\">"+
					"<splitCode code=\"\"/>"+
					"<joinCode code=\"\"/>"+
					"<timePointEventDefinition classCode=\"CTTEVENT\" moodCode=\"DEF\">"+
					"<id root=\"8.3.5.6.7.78.2.2\" extension=\"CELL.%s\"/>"+
					"<code code=\"CYCLE\" codeSystem=\"8.3.5.6.7.78.2.2\">"+
					"<displayName value=\"CELL.%s\"/>"+
					"</code>"+
					"%s"+ // <= sub part added here
					"<effectiveTime>"+
					"<low value=\"%s\"/>"+
					"<high value=\"%s\"/>"+
					"</effectiveTime>"+
					"</timePointEventDefinition>"+
					"</component1>"+
					"</timePointEventDefinition>"+
					"</component4>";
	
	private static final String component4Part1SubTemplate =
			"<component1 typeCode=\"COMP\">"+
					"<sequenceNumber value=\"%s\"/>"+
					"<timePointEventDefinition classCode=\"CTTEVENT\" moodCode=\"DEF\">"+
					"<id root=\"8.3.5.6.7.78.2.2\" extension=\"CELL.%s.%s\"/>"+
					"</timePointEventDefinition>"+
					"</component1>";
	
	private static final String component4Part2Template =
			"<!--Activity Events, add a component4 for each visit in the calendar-->"+
					"<component4 typeCode=\"COMP\">"+
					"<timePointEventDefinition classCode=\"CTTEVENT\" moodCode=\"DEF\">"+
					"<id root=\"8.3.5.6.7.78.2.2\" extension=\"CELL.%s.%s\"/>"+
					"<code code=\"VISIT\" codeSystem=\"CELL.%s\">"+
					"<displayName value=\"CELL.%s\"/>"+
					"</code>"+
					"<!--Visit with Tolerance-->"+
					"<component2 typeCode=\"COMP\">"+
					"<encounter classCode=\"ENC\" moodCode=\"DEF\">"+
					"<effectiveTime>"+
					"<low value=\"%s\"/>"+
					"<high value=\"%s\"/>"+
					"</effectiveTime>"+
					"<activityTime value=\"%s\"/>"+
					"</encounter>"+
					"</component2>"+
					"%s"+ // <= Component 1 added here
					"</timePointEventDefinition>"+
					"</component4>";
	
	private static final String subjectOfForStudyTemplate = 
			"<subjectOf typeCode=\"SUBJ\">"+
					"<studyCharacteristic classCode=\"OBS\" moodCode=\"EVN\">"+
					"<code code=\"%s\"/>"+
					"<value value=\"%s\"/>"+
					"</studyCharacteristic>"+
					"</subjectOf>";
	
	private static final String subjectOfForComponent1Template =
			"<!--Billing modifier, coverag emodifier-->"+
					"<subjectOf typeCode=\"SUBJ\">"+
					"<timePointEventCharacteristic classCode=\"VERIF\" moodCode=\"EVN\">"+
					"<code code=\"BILL_MODIFIER\" codeSystem=\"8.3.5.6.7.78.2.2\" />"+
					"<!--Stored to [research billing modifier type] using a translation table-->"+
					"<value value=\"%s\"/>"+
					"</timePointEventCharacteristic>"+
					"</subjectOf>";
	
	private static final String component1Template =
			"<component1 typeCode=\"COMP\">"+
					"<sequenceNumber value=\"%s\"/>"+
					"<timePointEventDefinition classCode=\"CTTEVENT\" moodCode=\"DEF\">"+
					"<id root=\"8.3.5.6.7.78.2.2\" extension=\"%s.%s.%s\"/>"+
					"<code code=\"PROC\" codeSystem=\"8.3.5.6.7.78.2.2\"/>"+
					"<!--Procedure code-->"+
					"<component2 typeCode=\"COMP\">"+
					"<procedure classCode=\"PROC\" moodCode=\"EVN\">"+
					"<!--Used with EAP lookup (identity, procedure master #, or otherwise)"+
					" to find EAP for [billing procedure id]; send value of CPT code -->"+
					"<code code=\"%s\" codeSystem=\"3.4.2.3.5\"/>"+
					"</procedure>"+
					"</component2>"+
					"%s"+ // <= subjectOf for coverage type added here
					"</timePointEventDefinition>"+
					"</component1>";
	
	public String generateFinalOutput(Map<VelosKeys, Object> studyDataMap, String calendarData) {
		ArrayList<String> argList = new ArrayList<String>();
		argList.add((String)studyDataMap.get(ProtocolKeys.IdExtension));
		argList.add((String)studyDataMap.get(ProtocolKeys.IdExtension));
		argList.add((String)studyDataMap.get(ProtocolKeys.Title));
		argList.add((String)studyDataMap.get(ProtocolKeys.Text));
		argList.add(calendarData);
		StringBuffer moreDetailsSB = new StringBuffer();
		if (studyDataMap.get(ProtocolKeys.IrbNumber) != null) {
			moreDetailsSB.append(
					generateSubjectOfForStudy(ProtocolKeys.Irb.toString(), (String)studyDataMap.get(ProtocolKeys.IrbNumber)));
		}
		if (studyDataMap.get(ProtocolKeys.Irb) != null) {
			moreDetailsSB.append(
					generateSubjectOfForStudy(ProtocolKeys.Irb.toString(), (String)studyDataMap.get(ProtocolKeys.Irb)));
		}
		if (studyDataMap.get(ProtocolKeys.NctNumber) != null) {
			moreDetailsSB.append(
					generateSubjectOfForStudy(ProtocolKeys.Nct.toString(), (String)studyDataMap.get(ProtocolKeys.NctNumber)));
		}
		if (studyDataMap.get(ProtocolKeys.Nct) != null) {
			moreDetailsSB.append(
					generateSubjectOfForStudy(ProtocolKeys.Nct.toString(), (String)studyDataMap.get(ProtocolKeys.Nct)));
		}
		argList.add(moreDetailsSB.toString());
		return String.format(outputTemplate, argList.toArray());
	}
	
	public String generateComponent4Part1(Map<VelosKeys, Object> calendarMap, String subPart) {
		ArrayList<String> argList = new ArrayList<String>();
		argList.add(((String)calendarMap.get(ProtocolKeys.CalendarName)).toUpperCase());
		argList.add(((String)calendarMap.get(ProtocolKeys.CalendarName)).toUpperCase());
		argList.add(((String)calendarMap.get(ProtocolKeys.CalendarName)).toUpperCase());
		argList.add(((String)calendarMap.get(ProtocolKeys.CalendarName)).toUpperCase());
		argList.add(subPart);
		argList.add(formatDate(getReferenceDate()));
		argList.add(formatDate(addDays(getReferenceDate(), (Integer)calendarMap.get(ProtocolKeys.CalendarDuration))));
		argList.add(((String)calendarMap.get(ProtocolKeys.CalendarName)).toUpperCase());
		return String.format(component4Part1Template, argList.toArray());
	}
	
	public String generateComponent4Part2(Map<VelosKeys, Object> visitMap) {
		ArrayList<String> argList = new ArrayList<String>();
		String calName = (String)visitMap.get(ProtocolKeys.CalendarName);
		String visitName = (String)visitMap.get(ProtocolKeys.VisitName);
		argList.add(calName);
		argList.add(visitName);
		argList.add(calName);
		argList.add(calName);
		argList.add((String)visitMap.get(ProtocolKeys.VisitWindowBeforeFormatted));
		argList.add((String)visitMap.get(ProtocolKeys.VisitWindowAfterFormatted));
		argList.add((String)visitMap.get(ProtocolKeys.VisitDisplacementFormatted));
		StringBuffer eventSB = new StringBuffer();
		@SuppressWarnings("unchecked")
		List<Map<VelosKeys, Object>> eventList = (List<Map<VelosKeys, Object>>)visitMap.get(ProtocolKeys.EventList);
		for (Map<VelosKeys, Object> eventMap : eventList) {
			eventSB.append(generateComponent1(eventMap, calName, visitName));
		}
		argList.add(eventSB.toString());
		return String.format(component4Part2Template, argList.toArray());
	}
	
	private String generateComponent1(Map<VelosKeys, Object> eventMap, String calNameUpper, String visitName) {
		ArrayList<String> argList = new ArrayList<String>();
		argList.add(String.valueOf((Integer)eventMap.get(ProtocolKeys.EventSequence)));
		argList.add(calNameUpper);
		argList.add(visitName);
		argList.add((String)eventMap.get(ProtocolKeys.EventName));
		argList.add((String)eventMap.get(ProtocolKeys.EventCpt));
		StringBuffer coverageSB = new StringBuffer();
		if (eventMap.get(ProtocolKeys.CoverageType) != null) {
			Object [] covArgs = { (String)eventMap.get(ProtocolKeys.CoverageType) };
			coverageSB.append(String.format(subjectOfForComponent1Template, covArgs));
		}
		argList.add(coverageSB.toString());
		return String.format(component1Template, argList.toArray());
	}
	
	private String generateSubjectOfForStudy(String key, String value) {
		ArrayList<String> argList = new ArrayList<String>();
		argList.add(key);
		argList.add(value);
		return String.format(subjectOfForStudyTemplate, argList.toArray());
	}
	
	public String generateComponent4Part1SubTemplate(String calendarName,
			String visitName, int visitSeq) {
		ArrayList<String> argList = new ArrayList<String>();
		argList.add(String.valueOf(visitSeq));
		argList.add(calendarName);
		argList.add(visitName);
		return String.format(component4Part1SubTemplate, argList.toArray());
	}
	
    // Our test reference date with Epic is 2014-01-01
    private static Date getReferenceDate() {
		Calendar cal = GregorianCalendar.getInstance();
		cal.set(2014, 0, 1);
		return cal.getTime();
    }

    private static Date addDays(Date date, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days);
        return cal.getTime();
    }
    
    private static String formatDate(Date date) {
    	SimpleDateFormat f = new SimpleDateFormat("yyyyMMdd");
    	return f.format(date);
    }	
}
