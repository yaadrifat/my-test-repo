
package com.velos.services;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for sitesOfService complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="sitesOfService">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="sitesOfService" type="{http://velos.com/services/}organizationIdentifier" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "sitesOfService", propOrder = {
    "sitesOfService"
})
public class SitesOfService
    extends ServiceObject
{

    @XmlElement(nillable = true)
    protected List<OrganizationIdentifier> sitesOfService;

    /**
     * Gets the value of the sitesOfService property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sitesOfService property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSitesOfService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrganizationIdentifier }
     * 
     * 
     */
    public List<OrganizationIdentifier> getSitesOfService() {
        if (sitesOfService == null) {
            sitesOfService = new ArrayList<OrganizationIdentifier>();
        }
        return this.sitesOfService;
    }

}
