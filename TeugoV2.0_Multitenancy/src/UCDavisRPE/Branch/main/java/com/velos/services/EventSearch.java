
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for eventSearch complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="eventSearch">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="costType" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="cptCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="eventCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="eventNameDescNotes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="facility" type="{http://velos.com/services/}organizationIdentifier" minOccurs="0"/>
 *         &lt;element name="libraryType" type="{http://velos.com/services/}code" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "eventSearch", propOrder = {
    "costType",
    "cptCode",
    "eventCategory",
    "eventNameDescNotes",
    "facility",
    "libraryType"
})
public class EventSearch
    extends ServiceObject
{

    protected Code costType;
    protected String cptCode;
    protected String eventCategory;
    protected String eventNameDescNotes;
    protected OrganizationIdentifier facility;
    protected Code libraryType;

    /**
     * Gets the value of the costType property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getCostType() {
        return costType;
    }

    /**
     * Sets the value of the costType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setCostType(Code value) {
        this.costType = value;
    }

    /**
     * Gets the value of the cptCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCptCode() {
        return cptCode;
    }

    /**
     * Sets the value of the cptCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCptCode(String value) {
        this.cptCode = value;
    }

    /**
     * Gets the value of the eventCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventCategory() {
        return eventCategory;
    }

    /**
     * Sets the value of the eventCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventCategory(String value) {
        this.eventCategory = value;
    }

    /**
     * Gets the value of the eventNameDescNotes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventNameDescNotes() {
        return eventNameDescNotes;
    }

    /**
     * Sets the value of the eventNameDescNotes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventNameDescNotes(String value) {
        this.eventNameDescNotes = value;
    }

    /**
     * Gets the value of the facility property.
     * 
     * @return
     *     possible object is
     *     {@link OrganizationIdentifier }
     *     
     */
    public OrganizationIdentifier getFacility() {
        return facility;
    }

    /**
     * Sets the value of the facility property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganizationIdentifier }
     *     
     */
    public void setFacility(OrganizationIdentifier value) {
        this.facility = value;
    }

    /**
     * Gets the value of the libraryType property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getLibraryType() {
        return libraryType;
    }

    /**
     * Sets the value of the libraryType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setLibraryType(Code value) {
        this.libraryType = value;
    }

}
