
package com.velos.services;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for studyOrganization complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="studyOrganization">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="approveEnrollNotificationTo" type="{http://velos.com/services/}userIdentifier" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="localSampleSize" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="newEnrollNotificationTo" type="{http://velos.com/services/}userIdentifier" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="organizationIdentifier" type="{http://velos.com/services/}organizationIdentifier" minOccurs="0"/>
 *         &lt;element name="parentIdentifier" type="{http://velos.com/services/}studyIdentifier" minOccurs="0"/>
 *         &lt;element name="publicInformationFlag" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="reportableFlag" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="reviewBasedEnrollmentFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="type" type="{http://velos.com/services/}code" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "studyOrganization", propOrder = {
    "approveEnrollNotificationTo",
    "localSampleSize",
    "newEnrollNotificationTo",
    "organizationIdentifier",
    "parentIdentifier",
    "publicInformationFlag",
    "reportableFlag",
    "reviewBasedEnrollmentFlag",
    "type"
})
public class StudyOrganization
    extends ServiceObject
{

    @XmlElement(nillable = true)
    protected List<UserIdentifier> approveEnrollNotificationTo;
    protected Integer localSampleSize;
    @XmlElement(nillable = true)
    protected List<UserIdentifier> newEnrollNotificationTo;
    protected OrganizationIdentifier organizationIdentifier;
    protected StudyIdentifier parentIdentifier;
    protected boolean publicInformationFlag;
    protected boolean reportableFlag;
    protected Boolean reviewBasedEnrollmentFlag;
    protected Code type;

    /**
     * Gets the value of the approveEnrollNotificationTo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the approveEnrollNotificationTo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getApproveEnrollNotificationTo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UserIdentifier }
     * 
     * 
     */
    public List<UserIdentifier> getApproveEnrollNotificationTo() {
        if (approveEnrollNotificationTo == null) {
            approveEnrollNotificationTo = new ArrayList<UserIdentifier>();
        }
        return this.approveEnrollNotificationTo;
    }

    /**
     * Gets the value of the localSampleSize property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLocalSampleSize() {
        return localSampleSize;
    }

    /**
     * Sets the value of the localSampleSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLocalSampleSize(Integer value) {
        this.localSampleSize = value;
    }

    /**
     * Gets the value of the newEnrollNotificationTo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the newEnrollNotificationTo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNewEnrollNotificationTo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UserIdentifier }
     * 
     * 
     */
    public List<UserIdentifier> getNewEnrollNotificationTo() {
        if (newEnrollNotificationTo == null) {
            newEnrollNotificationTo = new ArrayList<UserIdentifier>();
        }
        return this.newEnrollNotificationTo;
    }

    /**
     * Gets the value of the organizationIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link OrganizationIdentifier }
     *     
     */
    public OrganizationIdentifier getOrganizationIdentifier() {
        return organizationIdentifier;
    }

    /**
     * Sets the value of the organizationIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganizationIdentifier }
     *     
     */
    public void setOrganizationIdentifier(OrganizationIdentifier value) {
        this.organizationIdentifier = value;
    }

    /**
     * Gets the value of the parentIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link StudyIdentifier }
     *     
     */
    public StudyIdentifier getParentIdentifier() {
        return parentIdentifier;
    }

    /**
     * Sets the value of the parentIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyIdentifier }
     *     
     */
    public void setParentIdentifier(StudyIdentifier value) {
        this.parentIdentifier = value;
    }

    /**
     * Gets the value of the publicInformationFlag property.
     * 
     */
    public boolean isPublicInformationFlag() {
        return publicInformationFlag;
    }

    /**
     * Sets the value of the publicInformationFlag property.
     * 
     */
    public void setPublicInformationFlag(boolean value) {
        this.publicInformationFlag = value;
    }

    /**
     * Gets the value of the reportableFlag property.
     * 
     */
    public boolean isReportableFlag() {
        return reportableFlag;
    }

    /**
     * Sets the value of the reportableFlag property.
     * 
     */
    public void setReportableFlag(boolean value) {
        this.reportableFlag = value;
    }

    /**
     * Gets the value of the reviewBasedEnrollmentFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isReviewBasedEnrollmentFlag() {
        return reviewBasedEnrollmentFlag;
    }

    /**
     * Sets the value of the reviewBasedEnrollmentFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReviewBasedEnrollmentFlag(Boolean value) {
        this.reviewBasedEnrollmentFlag = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setType(Code value) {
        this.type = value;
    }

}
