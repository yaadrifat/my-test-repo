
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for monitorVersion complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="monitorVersion">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="dateFormat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="eResearchCompatibilityVersionNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="eSPBuildDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="eSPBuildNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="eSPVersionNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "monitorVersion", propOrder = {
    "dateFormat",
    "eResearchCompatibilityVersionNumber",
    "espBuildDate",
    "espBuildNumber",
    "espVersionNumber"
})
public class MonitorVersion {

    protected String dateFormat;
    protected String eResearchCompatibilityVersionNumber;
    @XmlElement(name = "eSPBuildDate")
    protected String espBuildDate;
    @XmlElement(name = "eSPBuildNumber")
    protected String espBuildNumber;
    @XmlElement(name = "eSPVersionNumber")
    protected String espVersionNumber;

    /**
     * Gets the value of the dateFormat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateFormat() {
        return dateFormat;
    }

    /**
     * Sets the value of the dateFormat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateFormat(String value) {
        this.dateFormat = value;
    }

    /**
     * Gets the value of the eResearchCompatibilityVersionNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEResearchCompatibilityVersionNumber() {
        return eResearchCompatibilityVersionNumber;
    }

    /**
     * Sets the value of the eResearchCompatibilityVersionNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEResearchCompatibilityVersionNumber(String value) {
        this.eResearchCompatibilityVersionNumber = value;
    }

    /**
     * Gets the value of the espBuildDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getESPBuildDate() {
        return espBuildDate;
    }

    /**
     * Sets the value of the espBuildDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setESPBuildDate(String value) {
        this.espBuildDate = value;
    }

    /**
     * Gets the value of the espBuildNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getESPBuildNumber() {
        return espBuildNumber;
    }

    /**
     * Sets the value of the espBuildNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setESPBuildNumber(String value) {
        this.espBuildNumber = value;
    }

    /**
     * Gets the value of the espVersionNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getESPVersionNumber() {
        return espVersionNumber;
    }

    /**
     * Sets the value of the espVersionNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setESPVersionNumber(String value) {
        this.espVersionNumber = value;
    }

}
