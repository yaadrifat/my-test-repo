package com.velos.epic;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.hornetq.utils.Base64.InputStream;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.velos.epic.service.EpicEndpointClient;
import com.velos.epic.service.StudyProcess;
import com.velos.integration.espclient.VelosEspClient;
import com.velos.integration.espclient.VelosEspMethods;
import com.velos.integration.hl7.notifications.OperationCustomException;
import com.velos.integration.mapping.EndpointKeys;
import com.velos.integration.mapping.ProtocolKeys;
import com.velos.integration.mapping.VelosKeys;
import com.velos.services.StudyStatus;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class XmlParser {


	private static Logger logger = Logger.getLogger("epicLogger");
	StudyProcess studyProcess =new StudyProcess();
	Properties prop = null;
	List <String> patStudyStatusList = new ArrayList<String>();
	int count=1;

	/*public static void main(String args[]) throws SAXException, IOException, ParserConfigurationException{

		enrollPatStudyXmlParser();

	}*/

	// Study trigger  Xml input 
	public  void studyXmlParser(Map<String,String> dataMap,String studyXml) throws ParserConfigurationException, SAXException, IOException, OperationCustomException{
		logger.info("***********studyXmlParser********************");

		try{
			prop = new Properties();

			prop.load(this.getClass().getClassLoader().getResourceAsStream("epic.properties"));
			String studyNumber=null;
			DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(studyXml));

			Document doc = db.parse(is);
			NodeList nodes = doc.getElementsByTagName("changes");
			System.out.println("----------------------------");
			for (int i = 0; i < nodes.getLength(); i++) {
				Element element = (Element) nodes.item(i);

				/*NodeList name = element.getElementsByTagName("statusCodeDesc");
				      Element line = (Element) name.item(0);

				      String studyCodeDesc=getCharacterDataFromElement(line);
				      System.out.println("StatusCodeDesc: " + getCharacterDataFromElement(line));
				 */
				NodeList title = element.getElementsByTagName("studyNumber");
				Element line = (Element) title.item(0);
				studyNumber=getCharacterDataFromElement(line);

				logger.info("Study Number=========>: " +studyNumber);
			}

			String endpoint = EndpointKeys.Epic.toString();
			VelosEspClient espClient = new VelosEspClient();
			Map<VelosKeys, String> requestMap = new HashMap<VelosKeys, String>();
			requestMap.put(EndpointKeys.Endpoint, endpoint);
			requestMap.put(ProtocolKeys.StudyNumber, studyNumber);
			logger.info("VelosEspMethods.StudyGetStudyStat==="+VelosEspMethods.StudyGetStudyStat);
			Map<VelosKeys, Object> resultMap = espClient.handleRequest(VelosEspMethods.StudyGetStudyStat, requestMap);


			String status = (String)resultMap.get(ProtocolKeys.StudyStatuses);
			System.out.println("Status in XMLParser="+status);
			String studyStatus= prop.getProperty("study.statusSubType").trim();

			String action = dataMap.get("StudyAction");
			logger.info("StudyAction =======>"+action);
			System.out.println("StudyAction =====>"+action);
			if("update".equalsIgnoreCase(action) && studyNumber != null){
				System.out.println("update action");
				studyProcess.getStudyDetails(studyNumber);
			}else
				if(studyNumber != null && status!=null && status.trim().equalsIgnoreCase(studyStatus)){
					logger.info("StudyAction =======>"+action);
					studyProcess.getStudyDetails(studyNumber);
				} else {
					logger.info("Study status list does not contain Ready for Epic status");
				}
		}catch(Exception e){
			e.printStackTrace();
			logger.info("Errors in XMLPraser (Retrieveprotocol study) =======>"+e.getMessage());
			throw new OperationCustomException(e.getMessage());
		}

	}
	public static String getCharacterDataFromElement(Element e) {
		Node child = e.getFirstChild();
		if (child instanceof CharacterData) {
			CharacterData cd = (CharacterData) child;
			return cd.getData();
		}
		return "";
	}


	//EnrollPatientStudy trigger xml  parsing 
	public  void  enrollPatStudyXmlParser(String	epsXml) throws Exception{

		logger.info("*********EnrollPatientToStudyXm******");
		/*  
			  epsXml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
					  + "<changes>"
					  + "<change>"
					  + "<action>CREATE</action>"
					  + "<statusSubType>enrolled</statusSubType>"
					  + "<statusCodeDesc>Enrolled</statusCodeDesc>"
					  + "<identifier xsi:type=\"patientStudyStatusIdentifier\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"
					  + "<OID>124d1667-333e-462c-86fd-2287e3934a10</OID>"
					  + "   </identifier>"
					  + " <module>patstudy_status</module>"
					  + "    <timeStamp>07/30/2015 04:24:08</timeStamp>"
					  + "  </change>"
					  + " <parentIdentifier>"
					  + " <id xsi:type=\"patientProtocolIdentifier\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"
					  + "      <OID>588edba1-3d08-4028-a756-2bbc6a9691ad</OID>"
					  + "   <studyIdentifier>"
					  + "       <studyNumber>CTMS-Dev15-0454</studyNumber>"
					  + "    </studyIdentifier>"
					  + "    <patientIdentifier>"
					  + "       <patientId>007</patientId>"
					  + "        <organizationId>"
					  + "         <siteName>UTHSCSA</siteName>"
					  + "       </organizationId>"
					  + "    </patientIdentifier>"
					  + "    </id>"
					  + "   </parentIdentifier>"
					  + "</changes>";
		 */
		prop = new Properties();
		try{
			prop.load(this.getClass().getClassLoader().getResourceAsStream("epic.properties"));
			System.out.println("EnrollPatStudyXmlParser");
			String studyNumber=null;
			String patientId=null;
			String patStudyStatus=null;
			DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(epsXml));

			Document doc = db.parse(is);
			NodeList nodes = doc.getElementsByTagName("changes");
			System.out.println("----------------------------");
			logger.info("Node===========================##########>"+ nodes.getLength());
			for (int i = 0; i < nodes.getLength(); i++) {
				Element element = (Element) nodes.item(i);

				logger.info("Element===========================##########>"+ element.getNodeName());
				NodeList changenodes = doc.getElementsByTagName("change");
				if(changenodes.getLength()>1){
					System.out.println("----------------------------");
					logger.info("Node===========================##########>"+ nodes.getLength());
					for (int j = 0; j < changenodes.getLength(); j++) {
						Element changeElement = (Element) nodes.item(i);
						count = count+1;
						logger.info("count========================----#>"+count);
						NodeList title2 = changeElement.getElementsByTagName("statusSubType");
						Element line3 = (Element) title2.item(0);
						patStudyStatus=getCharacterDataFromElement(line3);
						patStudyStatusList.add(patStudyStatus);

					}}else if(changenodes.getLength()<=1){
						NodeList title2 = element.getElementsByTagName("statusSubType");
						Element line2 = (Element) title2.item(0);
						patStudyStatus=getCharacterDataFromElement(line2);  
						patStudyStatusList.add(patStudyStatus);
					}
				/*NodeList name = element.getElementsByTagName("statusCodeDesc");
			      Element line = (Element) name.item(0);

			      String studyCodeDesc=getCharacterDataFromElement(line);
			      System.out.println("StatusCodeDesc: " + getCharacterDataFromElement(line));
				 */	logger.info("After Loop count========================----#>"+count);
				 logger.info("Array List length ==============>"+patStudyStatusList.size());
				NodeList title = element.getElementsByTagName("studyNumber");
				Element line = (Element) title.item(0);
				studyNumber=getCharacterDataFromElement(line);
				NodeList title1 = element.getElementsByTagName("patientId");
				Element line1 = (Element) title1.item(0);
				patientId=getCharacterDataFromElement(line1);




				System.out.println("Study Number: " +studyNumber);
				System.out.println("patientId: " +patientId);
				System.out.println("patStudyStatus: " +patStudyStatus);
				logger.info("Study Number: " +studyNumber);
				logger.info("patientId: " +patientId);
				logger.info("patStudyStatus: " +patStudyStatus);

			}
			if(patStudyStatusList.size()!=0){
				for(String xmlPatStdStatus:patStudyStatusList){
					String patStdStatus= prop.getProperty("patientStudyStatusCode").trim();
					logger.info("PatientStudyStatusCode =====>"+patStdStatus);
					// Based on patient study status  passing control flow to Enrollpatient or AlertProtocol
					if(studyNumber != null && patStudyStatus != null && patientId != null ){
						if(xmlPatStdStatus.equalsIgnoreCase(patStdStatus)){
							studyProcess.getEnrollPatientStudyDetails(studyNumber,patientId);
						} else if(!xmlPatStdStatus.equalsIgnoreCase(patStdStatus)) {
							studyProcess.getAlertProtocolState(studyNumber,patientId);
						}
					}else{
						logger.info("Study Number, PatientStudyStatus and patientId Should not null");
					}

				}
			}/*else  if(patStudyStatusList.size() == 0){
				String patStdStatus= prop.getProperty("patientStudyStatusCode").trim();
				logger.info("PatientStudyStatusCode =====>"+patStdStatus);
				// Based on patient study status  passing control flow to Enrollpatient or AlertProtocol
				if(studyNumber != null && patStudyStatus != null && patientId != null ){
					if(patStudyStatus.equalsIgnoreCase(patStdStatus)){
						studyProcess.getEnrollPatientStudyDetails(studyNumber,patientId);
					} else if(!patStudyStatus.equalsIgnoreCase(patStdStatus)) {
						studyProcess.getAlertProtocolState(studyNumber,patientId);
					}
				}else{
					logger.info("Study Number, PatientStudyStatus and patientId Should not null");
				}
			}*/
		}catch(Exception e)
		{
			e.printStackTrace();
			logger.info("Errors in XMLPraser  =======>"+e.getMessage());
			throw new OperationCustomException(e.getMessage());
		}

	}

}
