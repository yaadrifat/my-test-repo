package com.velos.epic;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.log4j.Logger;

import com.velos.integration.espclient.VelosEspClient;
import com.velos.integration.espclient.VelosEspMethods;
import com.velos.integration.hl7.notifications.OperationCustomException;
import com.velos.integration.mapping.EndpointKeys;
import com.velos.integration.mapping.ProtocolKeys;
import com.velos.integration.mapping.VelosKeys;
import com.velos.outbound.servlet.EpicServlet;
import com.velos.services.PatientDataBean;

public class XmlProcess extends StudyStatusMessage {
	private static Logger logger = Logger.getLogger("epicLogger");  
	StudyStatusMessage sSM=new StudyStatusMessage();

	Properties prop=null;
	public XmlProcess(){
		try {
			prop = new Properties();
			prop.load(this.getClass().getClassLoader()
					.getResourceAsStream("epic.properties"));
		} catch (IOException e) {

		}
	}
	
	/**
	 * @author SurendraMitta
	 * @param studyNumber
	 * @Description  This Method is used to generate Outbound RetrieveProtocolDef Request 
	 * @return
	 * @throws Exception 
	 */
	//Outbound RetriveProtocolDefResponse Message
	public String xmlGenerating(String studyId) throws Exception
	{
		logger.info(" *** RetrieveProtocolDefRequest Outbound XML Generation starts ****");
		String bmcAccountingUnit = null;
		String bmcActivityNumber = null;
		String bu_IO_SP = null;
		String division = null;
		String section = null;
		String pILoginName = null;
		String nctNumber =null;
		String studyContact=null;
		String StudySummaryText=null;
		String userLoginName = null;
		String studyContLastName = null;
		String studyNumber= null;
		String StudyTitle=null;
		String userCodeDesc = null;
		StringBuffer sd = null;
		StringBuffer groupUsers  = null;
		String moreStudyDetStatus = null;
		Map<String,StringBuffer> groupSearchUsrMap = null;
		VelosEspClient client = null;
		Map<VelosKeys,String> resultMap = null;
		
		if(studyId == null ){
			logger.info("Study Id is Null ");
			throw new OperationCustomException(sd.toString(),"StudyNumber should not be a null value");
		}
		
		Map<String,String> moreStudyMap = null;
		try{
			
		resultMap = new HashMap<VelosKeys,String>();
		resultMap.put(ProtocolKeys.StudyNumber,studyId);

		client = new VelosEspClient();
		resultMap.put(EndpointKeys.Endpoint, EndpointKeys.Epic.toString());
		Map<VelosKeys, Object> dataMap = client.handleRequest(VelosEspMethods.StudyGetStudySummary,resultMap);
		Map<VelosKeys, Object> userDataMap = client.handleRequest(VelosEspMethods.GroupUserService,resultMap);
		
		System.out.println("User Data Map ==================>"+ userDataMap);
		
		 moreStudyMap = (Map<String, String>) dataMap.get(ProtocolKeys.MoreStudyMap);
		
			System.out.println("MoreStudyMap on XML Process ===========>"+moreStudyMap);

			studyNumber=validateNull((String) dataMap.get(ProtocolKeys.StudyNumber));
			System.out.println("studyNumber **--------->"+studyNumber);

			StudyTitle=validateNull((String)dataMap.get(ProtocolKeys.Title));
			System.out.println("StudyTitle **--------->"+StudyTitle);

			StudySummaryText=validateNull((String)dataMap.get(ProtocolKeys.Text));
			logger.info("StudySummaryText--------->"+StudySummaryText);	

			studyContLastName =  validateNull((String)dataMap.get(ProtocolKeys.LastName));	
			userLoginName = validateNull((String)dataMap.get(ProtocolKeys.StudyContact));

			if(userLoginName == null || userLoginName.equalsIgnoreCase("")){

				studyContact=studyContLastName;

			} else if(userLoginName !=null || !userLoginName.equalsIgnoreCase("")){
				studyContact = userLoginName;
			}

			logger.info("****PI Login Name ******");
			pILoginName=validateNull((String)dataMap.get(ProtocolKeys.PIContact));
			logger.info("****PI Login Name ******=>"+pILoginName);
			
			nctNumber =validateNull((String)dataMap.get(ProtocolKeys.NctNumber));
			logger.info("NCT_Number is Null ====>"+nctNumber);
			
			bmcAccountingUnit =validateNull((String) dataMap.get(ProtocolKeys.BMCAccountingUnit)); 
			logger.info("*********** bmcAccountingUnit is  ***********>"+bmcAccountingUnit);
			
			bmcActivityNumber =validateNull((String) dataMap.get(ProtocolKeys.BMCActivityNumber));
			logger.info("*********** bmcActivityNumber is  ***********>"+bmcActivityNumber);
			
			bu_IO_SP 			= validateNull((String) dataMap.get(ProtocolKeys.BMCBU_IO_SP));
			logger.info("*********** bmc_ is bu_IO_SP  is  ***********>"+bu_IO_SP);
			
			section			=validateNull((String) dataMap.get(ProtocolKeys.Section_TherapticArea));
			logger.info("*********** bmc_ is section  is  ***********>"+section);
			
			division		    =validateNull((String) dataMap.get(ProtocolKeys.Division));
			logger.info("*********** bmc_ is division  is  ***********>"+division);
			
			
			 groupSearchUsrMap = (Map<String, StringBuffer>) userDataMap.get(ProtocolKeys.SearchUser);
			logger.info("****************** GroupSearchUser ***********");
			if(groupSearchUsrMap != null){
			
			userCodeDesc = prop.getProperty("userCodeDesc").trim();
			System.out.println("Group Search User Map =========>"+ groupSearchUsrMap);
			groupUsers = groupSearchUsrMap.get(userCodeDesc);
			
			}
		}catch(Exception e){
			logger.info("Error on more study details get preparing retrive protocol request"+e.getMessage());
			throw e;
		}


		sd = new StringBuffer("");
		
		sd.append("<urn:RetrieveProtocolDefResponse xmlns:urn=\"urn:hl7-org:v3\">");
		sd.append("<urn:protocolDef>");
		sd.append("<urn:plannedStudy>");
		sd.append("<urn:id root=\"1.2.5.2.3.4\" extension=\""+studyId+"\"/>");
		sd.append("<urn:title>"+StudyTitle+"</urn:title>");
		sd.append("<urn:text>"+StudySummaryText+"</urn:text>");
		sd.append("<urn:subjectOf typeCode=\"SUBJ\">");
		sd.append("<urn:studyCharacteristic classCode=\"OBS\" moodCode=\"EVN\">"); 
		sd.append("<urn:code code=\"NCT\"/>  <urn:value type=\"ST\" value=\""+nctNumber+"\"/>");
		sd.append("</urn:studyCharacteristic> </urn:subjectOf>");  
		sd.append("<urn:subjectOf typeCode=\"SUBJ\">");
		sd.append("<urn:studyCharacteristic classCode=\"OBS\" moodCode=\"EVN\">");
		sd.append("<urn:code code=\"IRB\"/>");
		sd.append("<urn:value type=\"ST\" value=\""+studyId+"\"/>");
		sd.append("</urn:studyCharacteristic>");
		sd.append("</urn:subjectOf>");
		sd.append("<urn:subjectOf typeCode=\"SUBJ\">");
		sd.append("<urn:studyCharacteristic classCode=\"OBS\" moodCode=\"EVN\">");  
		sd.append("<urn:code code=\"COORDINATORS\"/>"); 
		sd.append("<urn:value type=\"CD\" code=\""+studyContact.toUpperCase()+"\" codeSystem=\"1.2.5.2.3.7\"/>"); 
		sd.append("</urn:studyCharacteristic>"); 
		sd.append("</urn:subjectOf>"); 
		sd.append("<urn:subjectOf typeCode=\"SUBJ\">");
		sd.append("<urn:studyCharacteristic classCode=\"OBS\" moodCode=\"EVN\">");
		sd.append("<urn:code code=\"PI\"/>");
		sd.append("<urn:value type=\"CD\" code=\""+pILoginName.toUpperCase()+"\" codeSystem=\"1.2.5.2.3.6\" />");
		sd.append("</urn:studyCharacteristic>");
		sd.append("</urn:subjectOf>");
/*		sd.append("<subjectOf typeCode=\"SUBJ\">");
		sd.append("<studyCharacteristic classCode=\"OBS\" moodCode=\"EVN\">");
		sd.append("<code code=\"BMC ACCOUNTING UNIT\"/>");
		sd.append("<value type=\"ST\" value=\""+bmcAccountingUnit+"\"/>");
		sd.append("</studyCharacteristic>");
		sd.append("</subjectOf>");
		sd.append("<subjectOf typeCode=\"SUBJ\">");
		sd.append("<studyCharacteristic classCode=\"OBS\" moodCode=\"EVN\">");
		sd.append("<code code=\"BMC Activity Number\"/>");
		sd.append("<value type=\"ST\" value=\""+bmcActivityNumber+"\"/>");
		sd.append("</studyCharacteristic>");
		sd.append("</subjectOf>");
		sd.append("<subjectOf typeCode=\"SUBJ\">");
		sd.append("<studyCharacteristic classCode=\"OBS\" moodCode=\"EVN\">");
		sd.append("<code code=\"BU Internal Order IO number\"/>");
		sd.append("<value type=\"ST\" value=\""+ bu_IO_SP+"\"/>");
		sd.append("</studyCharacteristic>");
		sd.append("</subjectOf>");
*/
		
		sd.append("<subjectOf typeCode=\"SUBJ\">");
		sd.append("<studyCharacteristic classCode=\"OBS\" moodCode=\"EVN\">");
		sd.append("<code code=\"Department\"></code>");
		sd.append("<value type=\"ST\" value=\""+division+"\"/>");
		sd.append("</studyCharacteristic>");
		sd.append("</subjectOf>");
		sd.append("<subjectOf typeCode=\"SUBJ\">");
		sd.append("<studyCharacteristic classCode=\"OBS\" moodCode=\"EVN\">");
		sd.append("<code code=\"Section\"></code>");
		sd.append("<value type=\"ST\" value=\""+section+"\"/>");
		sd.append("</studyCharacteristic>");
		sd.append("</subjectOf>");
		
		// It will create the more study details request xml based property file field entries
		
		if(prop.getProperty("morestudydetails").trim() != null 
				&& prop.containsKey("morestudydetails")
				&&prop.getProperty("morestudydetails").equalsIgnoreCase("yes")){


			for (Entry<Object, Object> e : prop.entrySet()) {

				if ( e.getKey().toString().contains("morestudykey@")) {
					String moreStudy =  e.getKey().toString().substring(13);
					String key = moreStudy.replaceAll("-"," ");
					String des = e.getValue().toString().substring(13);
					sd.append("<subjectOf typeCode=\"SUBJ\">");
					sd.append("<studyCharacteristic classCode=\"OBS\" moodCode=\"EVN\">");
					sd.append("<code code=\""+des+"\"></code>");
					sd.append("<value type=\"ST\" value=\""+validateNull(moreStudyMap.get(key))+"\"/>");
					sd.append("</studyCharacteristic>");
					sd.append("</subjectOf>");
				}
			}

		}

		if(prop.getProperty("groupUsers").trim() != null 
				&& prop.containsKey("groupUsers")
				&&prop.getProperty("groupUsers").equalsIgnoreCase("yes")){

			if(groupSearchUsrMap != null){
				String[] grpusrs = groupUsers.toString().split("\n");

				for(String empId : grpusrs){
					sd.append("<subjectOf typeCode=\"SUBJ\">");
					sd.append("<studyCharacteristic classCode=\"OBS\" moodCode=\"EVN\">");
					sd.append("<code code=\""+userCodeDesc+"\"></code>");
					sd.append("<value type=\"ST\" value=\""+validateNull(empId)+"\"/>");
					sd.append("</studyCharacteristic>");
					sd.append("</subjectOf>");

				}

			}
		}
		sd.append("</urn:plannedStudy>"); 
		sd.append("</urn:protocolDef> "); 
		sd.append("<urn:query root=\"1.2.5.2.3.4\" extension=\""+studyId+"\"/>");  
		sd.append("</urn:RetrieveProtocolDefResponse>"); 
		System.out.println("\n\n\n\n\n Request XML\n\n"+ sd.toString());
		logger.info("\n\n\n\n\n Request XML\n\n"+ sd.toString());

		client = null;
		resultMap = null;
		
		return sd.toString();
	}


	public static Date toDate(XMLGregorianCalendar calendar){
		if(calendar == null) {
			return null;
		}
		return calendar.toGregorianCalendar().getTime();
	}



	/**
	 * @author SurendraMitta
	 * @param Map<VelosKeys,Object> resultMap
	 * @Description  This Method is used to generate Outbound EnrollPatientRequest XML
	 * @return String
	 * @throws OperationCustomException
	 */
	// Outbound EnrollPatientRequestRequest Message
	public String enrollPatientMessage(Map<VelosKeys,Object> resultMap) throws OperationCustomException{

		System.out.println("Result Map-->"+resultMap);
		String studyId=(String) resultMap.get(ProtocolKeys.StudyId);
		String patientId=(String) resultMap.get(ProtocolKeys.PatientFacilityId);
		//String subjectId = (String) resultMap.get(ProtocolKeys.SubjectID);
		String firstName=(String) resultMap.get(ProtocolKeys.FirstName);
		String lastName= (String) resultMap.get(ProtocolKeys.LastName);
		String DOB= (String) resultMap.get(ProtocolKeys.Dob);
		String streeAddressLine =(String)resultMap.get(ProtocolKeys.StreetAddressLine);
		String ctiy= (String)resultMap.get(ProtocolKeys.City);
		String state= (String)resultMap.get(ProtocolKeys.State);
		String postalCode= (String)resultMap.get(ProtocolKeys.PostalCode);
		String country= (String)resultMap.get(ProtocolKeys.Country);
		String studyPatId= (String) resultMap.get(ProtocolKeys.StudyPatId);
		String studyPatStatus = prop.getProperty((String)resultMap.get(ProtocolKeys.StudyPatStatus));

		System.out.println("\n\n\n####################EnrollRequest Map ====>"+resultMap+"\n#################\n\n");

		//TODO		
		//String studyPatStatus =(String) resultMap.get(ProtocolKeys.StudyPatStatus);

		System.out.println("studyPatStatus="+studyPatStatus);
		String message=" <soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:urn=\"urn:ihe:qrph:rpe:2009\" xmlns:urn1=\"urn:hl7-org:v3\">"
				+ "<soap:Header/>"
				+ "<soap:Body>"
				+ "<urn:EnrollPatientRequestRequest>"
				+ "<urn:processState>"+studyPatStatus+"</urn:processState>"
				+ "<urn:patient>"
				+ "<urn:candidateID root=\"1.2.5.8.2.7\" extension=\""+patientId+"\"/>"
				+ " <urn:subjectID root=\"5.8.4.5.7.2\" extension=\""+studyPatId+"\"/>"
				+ "<urn:name>"
				+ "<urn1:given>"+firstName+"</urn1:given>"
				+ "<urn1:family>"+lastName+"</urn1:family>"
				+ "</urn:name>"
				+ "<urn:address>"
				+ "<urn1:streetAddressLine xmlns=\"urn:hl7-org:v3\">"+streeAddressLine+"</urn1:streetAddressLine>"
				+ "<urn1:city xmlns=\"urn:hl7-org:v3\">"+ctiy+"</urn1:city>"
				+ "<urn1:state xmlns=\"urn:hl7-org:v3\">"+state+"</urn1:state>"
				+ "<urn1:postalCode xmlns=\"urn:hl7-org:v3\">"+postalCode+"</urn1:postalCode>"
				+ "<urn1:country xmlns=\"urn:hl7-org:v3\">"+country+"</urn1:country>"
				+ "</urn:address>"
				+ "<urn:dob value=\""+DOB+"\"/>"
				+ "</urn:patient>"
				+ "<urn1:study>"
				+ "<urn1:instantiation>"
				+ "<urn1:plannedStudy>"
				+ "<urn1:id root=\"1.2.5.2.3.4\" extension=\""+studyId+"\"/>"
				+ "</urn1:plannedStudy>"
				+ "</urn1:instantiation>"
				+ "</urn1:study>"
				+ "</urn:EnrollPatientRequestRequest>"
				+ "</soap:Body>"
				+ "</soap:Envelope>";
		System.out.println("Request Message ---->"+message);
		logger.info("\n\n\n\n\nRequest Message ---->\n\n"+message);
		if(studyId == null ){
			throw new OperationCustomException("EnrollPatientRequest","StudyId should not be null value");
		}
		if(patientId == null ){
			throw new OperationCustomException("EnrollPatientRequest","Patient should not be null value");
		}
		if(studyPatStatus == null ){
			throw new OperationCustomException("EnrollPatientRequest","studyPatStatus should not be null value");
		}return message;}




	/**
	 * @author SurendraMitta
	 * @param resultMap
	 * @description This is used to generate "AlertProtocolState"  Outbound XML Message
	 * @return
	 * @throws OperationCustomException
	 */
	//Alert ProtocolState Outbound Message
	public String alertProtocolState(Map<VelosKeys,Object> resultMap) throws OperationCustomException{

		System.out.println("Result Map-->"+resultMap);
		String studyId=(String) resultMap.get(ProtocolKeys.StudyId);
		String patientId=(String) resultMap.get(ProtocolKeys.PatientFacilityId);
		//String subjectId = (String) resultMap.get(ProtocolKeys.SubjectID);
		String firstName=(String) resultMap.get(ProtocolKeys.FirstName);
		String lastName= (String) resultMap.get(ProtocolKeys.LastName);
		String DOB= (String) resultMap.get(ProtocolKeys.Dob);

		String streeAddressLine =(String)resultMap.get(ProtocolKeys.StreetAddressLine);
		String ctiy= (String)resultMap.get(ProtocolKeys.City);
		String state= (String)resultMap.get(ProtocolKeys.State);
		String postalCode= (String)resultMap.get(ProtocolKeys.PostalCode);
		String country= (String)resultMap.get(ProtocolKeys.Country);

		String studyPatId= (String) resultMap.get(ProtocolKeys.StudyPatId);


		//String studyPatStatus =(String) resultMap.get(ProtocolKeys.StudyPatStatus);
		String studyPatStatus = prop.getProperty((String)resultMap.get(ProtocolKeys.StudyPatStatus));
		//String studyPatStatus =(String) resultMap.get(ProtocolKeys.StudyPatStatus);
		String message="<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:urn=\"urn:ihe:qrph:rpe:2009\" xmlns:urn1=\"urn:hl7-org:v3\">"
				+ "<soap:Header/>"
				+ "<soap:Body>"
				+ "<urn:AlertProtocolState>"
				+ "<urn:processState>"+studyPatStatus+"</urn:processState>"
				+ "<urn:patient>"
				+ "<urn:candidateID root=\"1.2.5.8.2.7\" extension=\""+patientId+"\"/>"
				+ " <urn:subjectID root=\"5.8.4.5.7.2\" extension=\""+studyPatId+"\"/>"
				+ "<urn:name>"
				+ "<urn1:given>"+firstName+"</urn1:given>"
				+ "<urn1:family>"+lastName+"</urn1:family>"
				+ "</urn:name>"
				+ "<urn:dob value=\""+DOB+"\"/>"
				+ "</urn:patient>"
				+ "<urn1:study>"
				+ "<urn1:instantiation>"
				+ "<urn1:plannedStudy>"
				+ "<urn1:id root=\"1.2.5.2.3.4\" extension=\""+studyId+"\"/>"
				+ "</urn1:plannedStudy>"
				+ "</urn1:instantiation>"
				+ "</urn1:study>"
				+ "</urn:AlertProtocolState>"
				+ "</soap:Body>"
				+ "</soap:Envelope>";

		System.out.println("Request Message ---->"+message);
		logger.info("\n\n\n\n\nRequest Message ---->\n\n"+message);

		if(studyId == null ){
			throw new OperationCustomException("AlertProtocolState","StudyId should not be null value");
		}
		if(patientId == null ){
			throw new OperationCustomException("AlertProtocolState","Patient should not be null value");
		}
		if(studyPatStatus == null ){
			throw new OperationCustomException("AlertProtocolState","studyPatStatus should not be null value");
		}

		return message;


	}


	private String validateNull(String value) throws Exception{
		if(value==null){
			return "";
		}else{
			return value;
		}
	}
}
