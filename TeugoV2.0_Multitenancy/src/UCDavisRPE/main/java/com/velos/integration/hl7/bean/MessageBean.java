package com.velos.integration.hl7.bean;

public class MessageBean {

	private String messsageType;
	private String messageDataTable;
	private int emailNotification;
	private int realTimeNotfication;
	public void setEmailNotification(int emailNotification) {
		this.emailNotification = emailNotification;
	}
	public void setRealTimeNotfication(int realTimeNotfication) {
		this.realTimeNotfication = realTimeNotfication;
	}
	public int getEmailNotification() {
		return emailNotification;
	}
	public int getRealTimeNotfication() {
		return realTimeNotfication;
	}
		public String getMesssageType() {
		return messsageType;
	}
	public void setMesssageType(String messsageType) {
		this.messsageType = messsageType;
	}
	public String getMessageDataTable() {
		return messageDataTable;
	}
	public void setMessageDataTable(String messageDataTable) {
		this.messageDataTable = messageDataTable;
	}
	
	
}
