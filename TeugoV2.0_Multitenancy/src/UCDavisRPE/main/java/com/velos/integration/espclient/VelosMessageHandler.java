/*
 * Copyright 2002-2011 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.velos.integration.espclient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBElement;

import org.apache.log4j.Logger;
import org.springframework.integration.Message;
import org.springframework.integration.MessageRejectedException;
import org.springframework.integration.MessagingException;
import org.springframework.integration.core.MessageHandler;
import org.springframework.stereotype.Component;

import com.velos.epic.service.NOT_EpicEndpointClient;
import com.velos.integration.mapping.ProtocolKeys;
import com.velos.integration.mapping.VelosKeys;
import com.velos.integration.mapping.VelosPatientMapper;
import com.velos.integration.mapping.VelosStudyCalendarMapper;
import com.velos.integration.mapping.VelosStudyMapper;
import com.velos.integration.mapping.VelosStudyPatientMapper;
import com.velos.services.AddStudyPatientStatusResponse;
import com.velos.services.CreatePatientResponse;
import com.velos.services.EnrollPatientToStudyResponse;
import com.velos.services.GetPatientDemographicsResponse;
import com.velos.services.GetPatientDetailsResponse;
import com.velos.services.GetStudyCalendarListResponse;
import com.velos.services.GetStudyCalendarResponse;
import com.velos.services.GetStudyPatientStatusHistoryResponse;
import com.velos.services.GetStudyPatientStatusResponse;
import com.velos.services.GetStudyPatientsResponse;
import com.velos.services.GetStudyStatusesResponse;
import com.velos.services.GetStudySummaryResponse;
import com.velos.services.Issue;
import com.velos.services.OperationException;
import com.velos.services.SearchPatientResponse;
import com.velos.services.SearchUserResponse;

@Component
public class VelosMessageHandler implements MessageHandler {
	private static Logger logger = Logger.getLogger("epicLogger");
	private Map<VelosKeys, Object> dataMap = new HashMap<VelosKeys, Object>();
	private String endpoint = null;
	private final static String OPERATION_EXECEPTION = "{http://velos.com/services/}OperationException";

	public void handleMessage(Message<?> message) throws MessagingException {
		Object payload = message.getPayload();
		//System.out.println("In VelosMessageHandler, received "+payload.getClass().getSimpleName());
		System.out.println("***********************UserSearch**********************");
		
		if(payload instanceof JAXBElement){
		JAXBElement<?> jaxbElements = (JAXBElement<?>)payload;
		Object payloads = jaxbElements.getValue();
		
		
		try{
		if(payloads instanceof SearchUserResponse){
			System.out.println("SearchUSer__________________");
			VelosStudyMapper mapper = new VelosStudyMapper(this.endpoint);
			dataMap = mapper.mapSearchUser((SearchUserResponse)payloads);
		} }catch(Exception ee){
			System.out.println("SearchUSer___________%%%%%%%%%_______");
			ee.printStackTrace();
			
			//throw ee;
		}
		}else if(payload instanceof GetStudyPatientStatusResponse ){
			VelosStudyPatientMapper mapper = new VelosStudyPatientMapper(this.endpoint);
			dataMap = mapper.mapStudyPatientStatus((GetStudyPatientStatusResponse)payload);
		}else if(payload instanceof GetStudyPatientStatusHistoryResponse){
			VelosStudyPatientMapper mapper = new VelosStudyPatientMapper(this.endpoint);
			dataMap = mapper.mapStudyPatientStatusHistory((GetStudyPatientStatusHistoryResponse)payload);
		}else if (payload instanceof GetPatientDemographicsResponse) {
			GetPatientDemographicsResponse resp = (GetPatientDemographicsResponse) payload;
			System.out.println(resp.getPatientDemographics().getLastName());
		} else if (payload instanceof GetStudySummaryResponse) {
			VelosStudyMapper mapper = new VelosStudyMapper(this.endpoint);
			dataMap = mapper.mapStudySummary((GetStudySummaryResponse) payload);
		} else if (payload instanceof GetStudyCalendarListResponse) {
			VelosStudyCalendarMapper mapper = new VelosStudyCalendarMapper(this.endpoint);
			dataMap = mapper.mapStudyCalendarList((GetStudyCalendarListResponse)payload);
		} else if (payload instanceof GetStudyCalendarResponse) {
			VelosStudyCalendarMapper mapper = new VelosStudyCalendarMapper(this.endpoint);
			dataMap = mapper.mapStudyCalendar((GetStudyCalendarResponse)payload);
		} else if (payload instanceof GetStudyPatientsResponse) {
			VelosStudyPatientMapper mapper = new VelosStudyPatientMapper(this.endpoint);
			dataMap = mapper.mapStudyPatientList((GetStudyPatientsResponse)payload);
		} else if (payload instanceof SearchPatientResponse) {
			System.out.println("handleMessage="+this.endpoint);
			VelosPatientMapper mapper = new VelosPatientMapper(this.endpoint);
			dataMap = mapper.mapSearchPatient((SearchPatientResponse)payload);
			System.out.println("handleMessage dataMap="+dataMap);
		} else if (payload instanceof EnrollPatientToStudyResponse) {
			System.out.println("EnrollPatientToStudyResponse msg");
			VelosStudyPatientMapper mapper = new VelosStudyPatientMapper(this.endpoint);
			dataMap = mapper.mapEnrollPatientToStudy((EnrollPatientToStudyResponse)payload);
			System.out.println("handleMessage dataMap="+dataMap);
		} else if (payload instanceof AddStudyPatientStatusResponse) {
			VelosStudyPatientMapper mapper = new VelosStudyPatientMapper(this.endpoint);
			dataMap = mapper.mapAddStudyPatientStatus((AddStudyPatientStatusResponse)payload);
		}
		else if(payload instanceof CreatePatientResponse){
			VelosPatientMapper mapper = new VelosPatientMapper(this.endpoint);
			dataMap = mapper.mapCreatePatient((CreatePatientResponse)payload);
		}else if(payload instanceof GetPatientDetailsResponse){
			VelosPatientMapper mapper = new VelosPatientMapper(this.endpoint);
			dataMap = mapper.mapPatientDetails((GetPatientDetailsResponse)payload);
		} else if(payload instanceof GetStudyStatusesResponse){
			
			VelosStudyMapper mapper = new VelosStudyMapper(this.endpoint);
			dataMap = mapper.mapStudyStatuses((GetStudyStatusesResponse) payload);
		}
		
		else if (payload instanceof JAXBElement	&&
			OPERATION_EXECEPTION.equals(((JAXBElement<?>)payload).getName().toString())) {
			JAXBElement<?> jaxbElement = (JAXBElement<?>)payload;
			OperationException operationException = (OperationException)jaxbElement.getValue();
			List<Issue> issueList = operationException.getIssues().getIssue();
			List<String> errorList = new ArrayList<String>();
			for (Issue issue : issueList) {
				dataMap.put(ProtocolKeys.FaultString, issue.getMessage());
				errorList.add(issue.getMessage());
				System.out.println("Got operaton exception with issue: "+issue.getMessage());
				logger.info("Got operaton exception with issue: "+issue.getMessage());
			}
			dataMap.put(ProtocolKeys.ErrorList, errorList);
		} else {
			throw new MessageRejectedException(message, "Unknown data type has been received for "+payload.getClass());
		}
	}
	
	public Map<VelosKeys, Object> getDataMap() {
		return dataMap;
	}
	
	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

}
