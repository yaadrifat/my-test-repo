package com.velos.integration.espclient;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import com.velos.services.ObjectInfo;
import com.velos.services.OperationException_Exception;
import com.velos.services.SimpleIdentifier;

@WebService(targetNamespace="http://velos.com/services/", name="StudyPatientSEI")
public interface VelosEspPatStudyStatusInfoEndpoint {
	 @WebResult(name = "PatientStudyStatuses", targetNamespace = "")
	    @RequestWrapper(localName = "getStudyPatientStatusHistory", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetStudyPatientStatusHistory")
	    @WebMethod
	    @ResponseWrapper(localName = "getStudyPatientStatusHistoryResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetStudyPatientStatusHistoryResponse")
	    public com.velos.services.StudyPatientStatuses getStudyPatientStatusHistory(
	        @WebParam(name = "StudyIdentifier", targetNamespace = "")
	        com.velos.services.StudyIdentifier studyIdentifier,
	        @WebParam(name = "PatientIdentifier", targetNamespace = "")
	        com.velos.services.PatientIdentifier patientIdentifier
	    ) throws OperationException_Exception;
}
