package com.velos.integration.mapping;

public enum ProtocolKeys implements VelosKeys {
	// Convention: Each enum type should begin with upper case and use camel case for the rest.
	//             The corresponding string can be in any case to meet the business need.
	//             If the string contains a period, use _ for that in the corresponding enum type.
	AlertProtocolState("AlertProtocolStateRequest"),
	AlertProtocolStateRequest("AlertProtocolStateRequest"),
	Address("address"),
	AssigningAuthorityName("assigningAuthorityName"),
	BMCAccountingUnit("bmcAccountingUnit"),
	BMCActivityNumber("bmcActivityNum"),
	BMCBU_IO_SP("bmcbuIOSP"),
	CalendarId("calendarId"),
	CalendarIdList("calendarIdList"),
	CalendarName("calendarName"),
	CalendarDuration("calendarDuration"),
	CalendarDurUnit("calendarDurUnit"),
	CandidateId("candidateID"),
	CandidateIdUpper("CandidateID"),
	CompletedAction("completedAction"),
	CoverageType("coverageType"),
	Country("country"),//rk added
	City("city"),//rk added
	Description("description"),
	Division("division"),
	Dob("DOB"),
	EffectiveStartTime("effectiveStartTime"),
	EffectiveEndTime("effectiveEndTime"),
	EnrollPatientRequest("EnrollPatientRequestRequest"),
	ErrorList("errorList"),
	EventCpt("eventCpt"),
	EventList("eventList"),
	EventName("eventName"),
	EventSequence("eventSequence"),
	Extension("extension"),
	Family("family"),
	FaultString("faultstring"),
	FirstName("firstName"),
	Id("id"),
	IdExtension("idExtension"),
	Irb("IRB"),
	IrbNumber("irb_number"),
	Given("given"),
	LastName("lastName"),
	LetterA("A"),
	MoreStudyMap("moreStudyMap"),
	Mrn("MRN"),
	MultiplePatientsFoundMsg("Multiple patients found"),
	MiddleName("middleName"),
	Name("name"),
	Nct("NCT"),
	NctNumber("nctNumber"),
	Patient("patient"),
	PatientId("patientId"),
	PatientNotFoundMsg("Patient not found"),
	PostalCode("postalCode"),//rk added
	ProcessState("processState"),
	PILastName("pILastName"),
	PIFirstName("pIFirstName"),
	PIContact("pIContact"),
	RequestType("requestType"),
	RetrievePatient("RetrievePatientRequest"),
	RetrieveProtocol("RetrieveProtocolDefRequest"),	
	Root("root"),
	SearchUser("userSearchGroup"),
	Section_TherapticArea("section_Therapticarea"),
	Study("study"),
	StudyContact("studyContact"),
	StudyNumber("studyNumber"),
	StudyPatId("studypatId"),
	SubjectID("subjectID"),//rk added
	StreetAddressLine("streetAddressLine"),//rk added
	State("state"),//rk added
	StudyPatStatus("studyPatStatus"),
	StudyPatientList("studyPatientList"),
	Text("text"),
	Title("title"),
	TotalCount("totalCount"),
	Value("value"),
	VisitDisplacement("visitDisplacement"),
	VisitDisplacementFormatted("visitDisplacementFormatted"),
	VisitList("visitList"),
	VisitName("visitName"),
	VisitWindowBefore("visitWindowBefore"),
	VisitWindowAfter("visitWindowAfter"),
	VisitWindowBeforeFormatted("visitWindowBeforeFormatted"),
	VisitWindowAfterFormatted("visitWindowAfterFormatted"),
	PatientID("patientID"), // This is for searchPatient method
	PatientFacilityId("patientFacilityId"),
	PatientStudyId("patientStudyId"),
	SiteName("siteName"),
    StudyPatOID("OID"),
    StudyStatuses("studyStatuses"),
	OID("OID"),
	PK("PK"),
	PatProtId("patProtId"),
	StudyId("studyId"),
	Instantiation("instantiation"),
	PlannedStudy("plannedStudy"),
	Query("query");
	;
	
	ProtocolKeys(String key) {
		this.key = key;
	}
	
	private String key;
	
	@Override
	public String toString() {
		return key;
	}
}
