package com.velos.integration.mapping;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.velos.epic.XmlProcess;
import com.velos.services.Code;
import com.velos.services.GetStudyPatientStatusHistoryResponse;
import com.velos.services.GetStudyStatusesResponse;
import com.velos.services.GetStudySummaryResponse;
import com.velos.services.NvPair;
import com.velos.services.SearchUserResponse;
import com.velos.services.StudyStatus;
import com.velos.services.StudyStatuses;
import com.velos.services.StudySummary;
import com.velos.services.User;
import com.velos.services.UserIdentifier;
import com.velos.services.UserSearchResults;
import com.velos.services.UserStatus;

public class VelosStudyMapper {
	private static Logger logger = Logger.getLogger("epicLogger"); 
	private String endpoint = null;
	private Map<VelosKeys, Object> dataMap = new HashMap<VelosKeys, Object>();
	Properties prop = new Properties();

	public VelosStudyMapper(String endpoint) {

		try {
			prop.load(this.getClass().getClassLoader()
					.getResourceAsStream("epic.properties"));
		} catch (IOException e) {
			logger.info("");
			e.printStackTrace();
		}

		this.endpoint = endpoint;
	}

	public Map<VelosKeys, Object> mapStudySummary(GetStudySummaryResponse resp) {
		if (EndpointKeys.Epic.toString().equals(endpoint)) {
			mapStudySummaryForEpic(resp);
		}
		return dataMap;
	}


	public Map<VelosKeys, Object> mapStudyStatuses(GetStudyStatusesResponse resp) {
		if (EndpointKeys.Epic.toString().equals(endpoint)) {
			mapStudyStatusesForEpic(resp);
		}
		return dataMap;
	}


	private void mapStudyStatusesForEpic(GetStudyStatusesResponse resp) {
		StudyStatuses studyStatues = resp.getStudyStatuses();
		List<StudyStatus>  studyStatusList= studyStatues.getStudyStatus();
		System.out.println("studyStatusList=="+studyStatusList);
		String code = null;
		if(studyStatusList!=null && !studyStatusList.isEmpty()){
			for(StudyStatus studyStatus:studyStatusList){

				try{
					code = studyStatus.getStatus().getCode();
				}catch(Exception e){
					e.printStackTrace();
					logger.info("Exception -------===>"+e.getMessage());
				}     code = "ReadyforEpic";
				//System.out.println("Code ======>"+code);
				if(code!=null && code.trim().equalsIgnoreCase(prop.getProperty("study.statusSubType"))){
					dataMap.put(ProtocolKeys.StudyStatuses, code);
				}
			}
		}
	}

	public Map<VelosKeys, Object> mapStudySummary(StudySummary summary) {
		if (EndpointKeys.Epic.toString().equals(endpoint)) {
			mapStudySummaryForEpic(summary);
		}
		return dataMap;
	}

	private void mapStudySummaryForEpic(GetStudySummaryResponse resp) {
		mapStudySummaryForEpic(resp.getStudySummary());
	}

	public Map<VelosKeys, Object> mapSearchUser(SearchUserResponse searchUsrResp) {
		if (EndpointKeys.Epic.toString().equals(endpoint)) {
			mapSearchUserforEpic(searchUsrResp);
		}
		return dataMap;
	}

	public Map<VelosKeys, Object> mapSearchUserforEpic(SearchUserResponse searchUsrResp) {
		logger.info("***********GetUserStatus*********");
		UserSearchResults usrSearchRes =searchUsrResp.getUserSearchResults();
		Map<String,StringBuffer> groupSearchUsrMap = null; 
		groupSearchUsrMap = new HashMap<String, StringBuffer>();
		List<User> userList = usrSearchRes.getUser();
		String configusrStatus = prop.getProperty("userStatus").trim();
		String useCodeDesc = prop.getProperty("userCodeDesc").trim();
		String firstName = null;
		String lastName = null;
		StringBuffer empId = new StringBuffer();
		logger.info("***********GetUsers based on user status on studyMapper class *********");
		for(User usr : userList){
			UserStatus userStatus = usr.getUserStatus();
			if(userStatus.value() != null){
				String usrStat = userStatus.value();
				if(usrStat.equalsIgnoreCase(configusrStatus)){

					firstName = isEmpty(usr.getFirstName());
					lastName = isEmpty(usr.getLastName());
					String email = isEmpty(usr.getEmail());
					String address = isEmpty(usr.getAddress());
					String state = isEmpty(usr.getState());
					String city = isEmpty(usr.getCity());
					String usrLogName = isEmpty(usr.getUserLoginName());
					//System.out.println("FirstName ====="+firstName +"\n LastName =====>"+lastName);
					empId.append("firstName:"+firstName +","+"lastName:"+lastName+",email:"+email+",address:"+address+",city:"+city+",state:"+state +",userLoginName:"+usrLogName);
					empId.append("\n");
				} else if(configusrStatus.equalsIgnoreCase("ALL")){
					firstName = usr.getFirstName();
					lastName =  usr.getLastName();
					//System.out.println("FirstName ====="+firstName +"\n LastName =====>"+lastName);
					empId.append(firstName +"  "+lastName);
					empId.append("\n");
				}
			} else{
				logger.info("************** No GroupSearchUser Values *********");
			}
		}
		System.out.println("Code Desc ===========>"+useCodeDesc);
		groupSearchUsrMap.put(useCodeDesc,empId);

		dataMap.put(ProtocolKeys.SearchUser, groupSearchUsrMap);
		return dataMap ;
	}


	private void mapStudySummaryForEpic(StudySummary summary) {
		logger.info("\n**********************mapStudySummaryForEpic _ Method*****************\n");
		dataMap.put(ProtocolKeys.Title, MapUtil.escapeSpecial(summary.getStudyTitle()));
		dataMap.put(ProtocolKeys.Text, MapUtil.escapeSpecial(summary.getSummaryText()));
		dataMap.put(ProtocolKeys.IdExtension, MapUtil.escapeSpecial(summary.getStudyNumber()));
		Map<String,String> moreStudyMap = new HashMap<String, String>();
		List<NvPair> moreList = summary.getMoreStudyDetails();

		for (NvPair more : moreList) {

			for (Entry<Object, Object> e : prop.entrySet()) {

				if ( e.getKey().toString().contains("morestudykey@")) {
					String moreStudy =  e.getKey().toString().substring(13);
					String key = moreStudy.replaceAll("-"," ");
					if (key.equalsIgnoreCase(more.getKey())) {
						if(!key.equals("") &&key!=null){
							logger.info("MoreStudyDetails --->"+ more.getKey()+"======>"+more.getValue());
							moreStudyMap.put((String)key,(String)more.getValue());
							//dataMap.put(ProtocolKeys.MoreStudyMap, more.getValue());
						}
					} 

				}
			}
		}
		logger.info("MoreStudyDetails Map --->"+moreStudyMap);
		dataMap.put(ProtocolKeys.MoreStudyMap,moreStudyMap);



		String NctNumber =summary.getNctNumber();

		if(NctNumber != null){

			dataMap.put(ProtocolKeys.NctNumber,NctNumber.substring(3));
		} else if(NctNumber == null){
			dataMap.put(ProtocolKeys.NctNumber,"");
		}
		logger.info("NCTNumber --->"+ NctNumber);

		UserIdentifier userIdentifier= summary.getStudyContact();
		//System.out.println("userIdentifier=="+userIdentifier);
		if(userIdentifier!=null){
			dataMap.put(ProtocolKeys.LastName, isEmpty(userIdentifier.getLastName()));
			dataMap.put(ProtocolKeys.FirstName, isEmpty(userIdentifier.getFirstName()));
			dataMap.put(ProtocolKeys.StudyContact,isEmpty( userIdentifier.getUserLoginName()));

		} else if(userIdentifier == null){
			dataMap.put(ProtocolKeys.LastName, "");
			dataMap.put(ProtocolKeys.FirstName,"");
			dataMap.put(ProtocolKeys.StudyContact,"");
		}


		UserIdentifier userIdentifier1= summary.getPrincipalInvestigator();
		System.out.println("userIdentifier1=="+userIdentifier1);
		logger.info("userIdentifier1====>"+userIdentifier1);

		if(userIdentifier1!=null){
			logger.info("***********UserIdentifer is not null*******");
			dataMap.put(ProtocolKeys.PILastName, isEmpty(userIdentifier1.getLastName()));
			dataMap.put(ProtocolKeys.PIFirstName, isEmpty(userIdentifier1.getFirstName()));
			dataMap.put(ProtocolKeys.PIContact,isEmpty(userIdentifier1.getUserLoginName()));
		} else if(userIdentifier1 == null){
			logger.info("***********UserIdentifer1 is null*******");
			dataMap.put(ProtocolKeys.PILastName,"");
			dataMap.put(ProtocolKeys.PIFirstName, "");
			dataMap.put(ProtocolKeys.PIContact,"");

		}
		logger.info("******StudySummaryDivision *********");
		String division = summary.getDivision().getDescription();
		if(!division.equals("") || division !=null){
			logger.info("******StudySummaryDivision *********> "+division);
			dataMap.put(ProtocolKeys.Division, division);
		}

		logger.info(" ******************** Section Theraptic Area **************");
		String section_therapticAre = summary.getTherapeuticArea().getDescription();

		if(section_therapticAre.equals("") || section_therapticAre!=null){
			logger.info(" ******************** Section Theraptic Area *****>"+section_therapticAre);
			dataMap.put(ProtocolKeys.Section_TherapticArea, section_therapticAre);
		}


		logger.info("\n**********************mapStudySummaryForEpic _ Method========>\n"+dataMap);


	}

	public String isEmpty(String s){

		if(s == null){
			return s="";
		}

		return s;
	}



}
