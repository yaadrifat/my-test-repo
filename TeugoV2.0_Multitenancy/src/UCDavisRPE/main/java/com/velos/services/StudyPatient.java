
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for studyPatient complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="studyPatient">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="patStudyIdentifier" type="{http://velos.com/services/}studyIdentifier" minOccurs="0"/>
 *         &lt;element name="patientIdentifier" type="{http://velos.com/services/}patientIdentifier" minOccurs="0"/>
 *         &lt;element name="studyPatAssignedTo" type="{http://velos.com/services/}userIdentifier" minOccurs="0"/>
 *         &lt;element name="studyPatDoneOn" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="studyPatEnrollDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="studyPatEnrolledBy" type="{http://velos.com/services/}userIdentifier" minOccurs="0"/>
 *         &lt;element name="studyPatEnrollingSite" type="{http://velos.com/services/}organizationIdentifier" minOccurs="0"/>
 *         &lt;element name="studyPatFirstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="studyPatId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="studyPatLastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="studyPatLastVisit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="studyPatNextDue" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="studyPatPhysician" type="{http://velos.com/services/}userIdentifier" minOccurs="0"/>
 *         &lt;element name="studyPatStatus" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="studyPatTreatmentOrganization" type="{http://velos.com/services/}organizationIdentifier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "studyPatient", propOrder = {
    "patStudyIdentifier",
    "patientIdentifier",
    "studyPatAssignedTo",
    "studyPatDoneOn",
    "studyPatEnrollDate",
    "studyPatEnrolledBy",
    "studyPatEnrollingSite",
    "studyPatFirstName",
    "studyPatId",
    "studyPatLastName",
    "studyPatLastVisit",
    "studyPatNextDue",
    "studyPatPhysician",
    "studyPatStatus",
    "studyPatTreatmentOrganization"
})
@XmlRootElement(name="studyPatient", namespace = "http://velos.com/services/")
public class StudyPatient
    extends ServiceObject
{

    protected StudyIdentifier patStudyIdentifier;
    protected PatientIdentifier patientIdentifier;
    protected UserIdentifier studyPatAssignedTo;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar studyPatDoneOn;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar studyPatEnrollDate;
    protected UserIdentifier studyPatEnrolledBy;
    protected OrganizationIdentifier studyPatEnrollingSite;
    protected String studyPatFirstName;
    protected String studyPatId;
    protected String studyPatLastName;
    protected String studyPatLastVisit;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar studyPatNextDue;
    protected UserIdentifier studyPatPhysician;
    protected Code studyPatStatus;
    protected OrganizationIdentifier studyPatTreatmentOrganization;

    /**
     * Gets the value of the patStudyIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link StudyIdentifier }
     *     
     */
    public StudyIdentifier getPatStudyIdentifier() {
        return patStudyIdentifier;
    }

    /**
     * Sets the value of the patStudyIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyIdentifier }
     *     
     */
    public void setPatStudyIdentifier(StudyIdentifier value) {
        this.patStudyIdentifier = value;
    }

    /**
     * Gets the value of the patientIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link PatientIdentifier }
     *     
     */
    public PatientIdentifier getPatientIdentifier() {
        return patientIdentifier;
    }

    /**
     * Sets the value of the patientIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientIdentifier }
     *     
     */
    public void setPatientIdentifier(PatientIdentifier value) {
        this.patientIdentifier = value;
    }

    /**
     * Gets the value of the studyPatAssignedTo property.
     * 
     * @return
     *     possible object is
     *     {@link UserIdentifier }
     *     
     */
    public UserIdentifier getStudyPatAssignedTo() {
        return studyPatAssignedTo;
    }

    /**
     * Sets the value of the studyPatAssignedTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserIdentifier }
     *     
     */
    public void setStudyPatAssignedTo(UserIdentifier value) {
        this.studyPatAssignedTo = value;
    }

    /**
     * Gets the value of the studyPatDoneOn property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStudyPatDoneOn() {
        return studyPatDoneOn;
    }

    /**
     * Sets the value of the studyPatDoneOn property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStudyPatDoneOn(XMLGregorianCalendar value) {
        this.studyPatDoneOn = value;
    }

    /**
     * Gets the value of the studyPatEnrollDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStudyPatEnrollDate() {
        return studyPatEnrollDate;
    }

    /**
     * Sets the value of the studyPatEnrollDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStudyPatEnrollDate(XMLGregorianCalendar value) {
        this.studyPatEnrollDate = value;
    }

    /**
     * Gets the value of the studyPatEnrolledBy property.
     * 
     * @return
     *     possible object is
     *     {@link UserIdentifier }
     *     
     */
    public UserIdentifier getStudyPatEnrolledBy() {
        return studyPatEnrolledBy;
    }

    /**
     * Sets the value of the studyPatEnrolledBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserIdentifier }
     *     
     */
    public void setStudyPatEnrolledBy(UserIdentifier value) {
        this.studyPatEnrolledBy = value;
    }

    /**
     * Gets the value of the studyPatEnrollingSite property.
     * 
     * @return
     *     possible object is
     *     {@link OrganizationIdentifier }
     *     
     */
    public OrganizationIdentifier getStudyPatEnrollingSite() {
        return studyPatEnrollingSite;
    }

    /**
     * Sets the value of the studyPatEnrollingSite property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganizationIdentifier }
     *     
     */
    public void setStudyPatEnrollingSite(OrganizationIdentifier value) {
        this.studyPatEnrollingSite = value;
    }

    /**
     * Gets the value of the studyPatFirstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStudyPatFirstName() {
        return studyPatFirstName;
    }

    /**
     * Sets the value of the studyPatFirstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStudyPatFirstName(String value) {
        this.studyPatFirstName = value;
    }

    /**
     * Gets the value of the studyPatId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStudyPatId() {
        return studyPatId;
    }

    /**
     * Sets the value of the studyPatId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStudyPatId(String value) {
        this.studyPatId = value;
    }

    /**
     * Gets the value of the studyPatLastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStudyPatLastName() {
        return studyPatLastName;
    }

    /**
     * Sets the value of the studyPatLastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStudyPatLastName(String value) {
        this.studyPatLastName = value;
    }

    /**
     * Gets the value of the studyPatLastVisit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStudyPatLastVisit() {
        return studyPatLastVisit;
    }

    /**
     * Sets the value of the studyPatLastVisit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStudyPatLastVisit(String value) {
        this.studyPatLastVisit = value;
    }

    /**
     * Gets the value of the studyPatNextDue property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStudyPatNextDue() {
        return studyPatNextDue;
    }

    /**
     * Sets the value of the studyPatNextDue property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStudyPatNextDue(XMLGregorianCalendar value) {
        this.studyPatNextDue = value;
    }

    /**
     * Gets the value of the studyPatPhysician property.
     * 
     * @return
     *     possible object is
     *     {@link UserIdentifier }
     *     
     */
    public UserIdentifier getStudyPatPhysician() {
        return studyPatPhysician;
    }

    /**
     * Sets the value of the studyPatPhysician property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserIdentifier }
     *     
     */
    public void setStudyPatPhysician(UserIdentifier value) {
        this.studyPatPhysician = value;
    }

    /**
     * Gets the value of the studyPatStatus property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getStudyPatStatus() {
        return studyPatStatus;
    }

    /**
     * Sets the value of the studyPatStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setStudyPatStatus(Code value) {
        this.studyPatStatus = value;
    }

    /**
     * Gets the value of the studyPatTreatmentOrganization property.
     * 
     * @return
     *     possible object is
     *     {@link OrganizationIdentifier }
     *     
     */
    public OrganizationIdentifier getStudyPatTreatmentOrganization() {
        return studyPatTreatmentOrganization;
    }

    /**
     * Sets the value of the studyPatTreatmentOrganization property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganizationIdentifier }
     *     
     */
    public void setStudyPatTreatmentOrganization(OrganizationIdentifier value) {
        this.studyPatTreatmentOrganization = value;
    }

}
