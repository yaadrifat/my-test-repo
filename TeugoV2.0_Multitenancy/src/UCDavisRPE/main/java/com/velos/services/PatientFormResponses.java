
package com.velos.services;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for patientFormResponses complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="patientFormResponses">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="patientFormResponses" type="{http://velos.com/services/}patientFormResponse" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="recordCount" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "patientFormResponses", propOrder = {
    "patientFormResponses",
    "recordCount"
})
public class PatientFormResponses
    extends ServiceObject
{

    @XmlElement(nillable = true)
    protected List<PatientFormResponse> patientFormResponses;
    protected Integer recordCount;

    /**
     * Gets the value of the patientFormResponses property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the patientFormResponses property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPatientFormResponses().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PatientFormResponse }
     * 
     * 
     */
    public List<PatientFormResponse> getPatientFormResponses() {
        if (patientFormResponses == null) {
            patientFormResponses = new ArrayList<PatientFormResponse>();
        }
        return this.patientFormResponses;
    }

    /**
     * Gets the value of the recordCount property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRecordCount() {
        return recordCount;
    }

    /**
     * Sets the value of the recordCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRecordCount(Integer value) {
        this.recordCount = value;
    }

}
