
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for patientSearch complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="patientSearch">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="distinctFacilityRequired" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="exactSearch" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="gender" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="pageNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pageSize" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="patDateofBirth" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="patFirstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="patLastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="patOrganization" type="{http://velos.com/services/}organizationIdentifier" minOccurs="0"/>
 *         &lt;element name="patSpecialty" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="patSurvivalStat" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="patientFacilityID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="patientID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="race" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="sortBy" type="{http://velos.com/services/}sorting" minOccurs="0"/>
 *         &lt;element name="sortOrder" type="{http://velos.com/services/}ordering" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "patientSearch", propOrder = {
    "distinctFacilityRequired",
    "exactSearch",
    "gender",
    "pageNumber",
    "pageSize",
    "patDateofBirth",
    "patFirstName",
    "patLastName",
    "patOrganization",
    "patSpecialty",
    "patSurvivalStat",
    "patientFacilityID",
    "patientID",
    "race",
    "sortBy",
    "sortOrder"
})
public class PatientSearch
    extends ServiceObject
{

    protected boolean distinctFacilityRequired;
    protected boolean exactSearch;
    protected Code gender;
    protected String pageNumber;
    protected String pageSize;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar patDateofBirth;
    protected String patFirstName;
    protected String patLastName;
    protected OrganizationIdentifier patOrganization;
    protected Code patSpecialty;
    protected Code patSurvivalStat;
    protected String patientFacilityID;
    protected String patientID;
    protected Code race;
    protected Sorting sortBy;
    protected Ordering sortOrder;

    /**
     * Gets the value of the distinctFacilityRequired property.
     * 
     */
    public boolean isDistinctFacilityRequired() {
        return distinctFacilityRequired;
    }

    /**
     * Sets the value of the distinctFacilityRequired property.
     * 
     */
    public void setDistinctFacilityRequired(boolean value) {
        this.distinctFacilityRequired = value;
    }

    /**
     * Gets the value of the exactSearch property.
     * 
     */
    public boolean isExactSearch() {
        return exactSearch;
    }

    /**
     * Sets the value of the exactSearch property.
     * 
     */
    public void setExactSearch(boolean value) {
        this.exactSearch = value;
    }

    /**
     * Gets the value of the gender property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getGender() {
        return gender;
    }

    /**
     * Sets the value of the gender property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setGender(Code value) {
        this.gender = value;
    }

    /**
     * Gets the value of the pageNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPageNumber() {
        return pageNumber;
    }

    /**
     * Sets the value of the pageNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPageNumber(String value) {
        this.pageNumber = value;
    }

    /**
     * Gets the value of the pageSize property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPageSize() {
        return pageSize;
    }

    /**
     * Sets the value of the pageSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPageSize(String value) {
        this.pageSize = value;
    }

    /**
     * Gets the value of the patDateofBirth property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPatDateofBirth() {
        return patDateofBirth;
    }

    /**
     * Sets the value of the patDateofBirth property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPatDateofBirth(XMLGregorianCalendar value) {
        this.patDateofBirth = value;
    }

    /**
     * Gets the value of the patFirstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPatFirstName() {
        return patFirstName;
    }

    /**
     * Sets the value of the patFirstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPatFirstName(String value) {
        this.patFirstName = value;
    }

    /**
     * Gets the value of the patLastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPatLastName() {
        return patLastName;
    }

    /**
     * Sets the value of the patLastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPatLastName(String value) {
        this.patLastName = value;
    }

    /**
     * Gets the value of the patOrganization property.
     * 
     * @return
     *     possible object is
     *     {@link OrganizationIdentifier }
     *     
     */
    public OrganizationIdentifier getPatOrganization() {
        return patOrganization;
    }

    /**
     * Sets the value of the patOrganization property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganizationIdentifier }
     *     
     */
    public void setPatOrganization(OrganizationIdentifier value) {
        this.patOrganization = value;
    }

    /**
     * Gets the value of the patSpecialty property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getPatSpecialty() {
        return patSpecialty;
    }

    /**
     * Sets the value of the patSpecialty property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setPatSpecialty(Code value) {
        this.patSpecialty = value;
    }

    /**
     * Gets the value of the patSurvivalStat property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getPatSurvivalStat() {
        return patSurvivalStat;
    }

    /**
     * Sets the value of the patSurvivalStat property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setPatSurvivalStat(Code value) {
        this.patSurvivalStat = value;
    }

    /**
     * Gets the value of the patientFacilityID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPatientFacilityID() {
        return patientFacilityID;
    }

    /**
     * Sets the value of the patientFacilityID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPatientFacilityID(String value) {
        this.patientFacilityID = value;
    }

    /**
     * Gets the value of the patientID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPatientID() {
        return patientID;
    }

    /**
     * Sets the value of the patientID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPatientID(String value) {
        this.patientID = value;
    }

    /**
     * Gets the value of the race property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getRace() {
        return race;
    }

    /**
     * Sets the value of the race property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setRace(Code value) {
        this.race = value;
    }

    /**
     * Gets the value of the sortBy property.
     * 
     * @return
     *     possible object is
     *     {@link Sorting }
     *     
     */
    public Sorting getSortBy() {
        return sortBy;
    }

    /**
     * Sets the value of the sortBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link Sorting }
     *     
     */
    public void setSortBy(Sorting value) {
        this.sortBy = value;
    }

    /**
     * Gets the value of the sortOrder property.
     * 
     * @return
     *     possible object is
     *     {@link Ordering }
     *     
     */
    public Ordering getSortOrder() {
        return sortOrder;
    }

    /**
     * Sets the value of the sortOrder property.
     * 
     * @param value
     *     allowed object is
     *     {@link Ordering }
     *     
     */
    public void setSortOrder(Ordering value) {
        this.sortOrder = value;
    }

}
