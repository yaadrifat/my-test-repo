
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for addUnscheduledEventsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="addUnscheduledEventsResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="addUnscheduledEvents" type="{http://velos.com/services/}responseHolder" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "addUnscheduledEventsResponse", propOrder = {
    "addUnscheduledEvents"
})
public class AddUnscheduledEventsResponse {

    protected ResponseHolder addUnscheduledEvents;

    /**
     * Gets the value of the addUnscheduledEvents property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseHolder }
     *     
     */
    public ResponseHolder getAddUnscheduledEvents() {
        return addUnscheduledEvents;
    }

    /**
     * Sets the value of the addUnscheduledEvents property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseHolder }
     *     
     */
    public void setAddUnscheduledEvents(ResponseHolder value) {
        this.addUnscheduledEvents = value;
    }

}
