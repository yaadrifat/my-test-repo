
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for calendarEventSummary complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="calendarEventSummary">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="CPTCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="coverageType" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="eventDuration" type="{http://velos.com/services/}duration" minOccurs="0"/>
 *         &lt;element name="eventIdentifier" type="{http://velos.com/services/}eventIdentifier" minOccurs="0"/>
 *         &lt;element name="eventName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="eventWindowAfter" type="{http://velos.com/services/}duration" minOccurs="0"/>
 *         &lt;element name="eventWindowBefore" type="{http://velos.com/services/}duration" minOccurs="0"/>
 *         &lt;element name="facility" type="{http://velos.com/services/}organizationIdentifier" minOccurs="0"/>
 *         &lt;element name="notes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sequence" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="siteOfService" type="{http://velos.com/services/}organizationIdentifier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "calendarEventSummary", propOrder = {
    "cptCode",
    "coverageType",
    "description",
    "eventDuration",
    "eventIdentifier",
    "eventName",
    "eventWindowAfter",
    "eventWindowBefore",
    "facility",
    "notes",
    "sequence",
    "siteOfService"
})
public class CalendarEventSummary
    extends ServiceObject
{

    @XmlElement(name = "CPTCode")
    protected String cptCode;
    protected Code coverageType;
    protected String description;
    protected Duration eventDuration;
    protected EventIdentifier eventIdentifier;
    protected String eventName;
    protected Duration eventWindowAfter;
    protected Duration eventWindowBefore;
    protected OrganizationIdentifier facility;
    protected String notes;
    protected Integer sequence;
    protected OrganizationIdentifier siteOfService;

    /**
     * Gets the value of the cptCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCPTCode() {
        return cptCode;
    }

    /**
     * Sets the value of the cptCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCPTCode(String value) {
        this.cptCode = value;
    }

    /**
     * Gets the value of the coverageType property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getCoverageType() {
        return coverageType;
    }

    /**
     * Sets the value of the coverageType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setCoverageType(Code value) {
        this.coverageType = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the eventDuration property.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getEventDuration() {
        return eventDuration;
    }

    /**
     * Sets the value of the eventDuration property.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setEventDuration(Duration value) {
        this.eventDuration = value;
    }

    /**
     * Gets the value of the eventIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link EventIdentifier }
     *     
     */
    public EventIdentifier getEventIdentifier() {
        return eventIdentifier;
    }

    /**
     * Sets the value of the eventIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link EventIdentifier }
     *     
     */
    public void setEventIdentifier(EventIdentifier value) {
        this.eventIdentifier = value;
    }

    /**
     * Gets the value of the eventName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventName() {
        return eventName;
    }

    /**
     * Sets the value of the eventName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventName(String value) {
        this.eventName = value;
    }

    /**
     * Gets the value of the eventWindowAfter property.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getEventWindowAfter() {
        return eventWindowAfter;
    }

    /**
     * Sets the value of the eventWindowAfter property.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setEventWindowAfter(Duration value) {
        this.eventWindowAfter = value;
    }

    /**
     * Gets the value of the eventWindowBefore property.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getEventWindowBefore() {
        return eventWindowBefore;
    }

    /**
     * Sets the value of the eventWindowBefore property.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setEventWindowBefore(Duration value) {
        this.eventWindowBefore = value;
    }

    /**
     * Gets the value of the facility property.
     * 
     * @return
     *     possible object is
     *     {@link OrganizationIdentifier }
     *     
     */
    public OrganizationIdentifier getFacility() {
        return facility;
    }

    /**
     * Sets the value of the facility property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganizationIdentifier }
     *     
     */
    public void setFacility(OrganizationIdentifier value) {
        this.facility = value;
    }

    /**
     * Gets the value of the notes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNotes() {
        return notes;
    }

    /**
     * Sets the value of the notes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNotes(String value) {
        this.notes = value;
    }

    /**
     * Gets the value of the sequence property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSequence() {
        return sequence;
    }

    /**
     * Sets the value of the sequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSequence(Integer value) {
        this.sequence = value;
    }

    /**
     * Gets the value of the siteOfService property.
     * 
     * @return
     *     possible object is
     *     {@link OrganizationIdentifier }
     *     
     */
    public OrganizationIdentifier getSiteOfService() {
        return siteOfService;
    }

    /**
     * Sets the value of the siteOfService property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganizationIdentifier }
     *     
     */
    public void setSiteOfService(OrganizationIdentifier value) {
        this.siteOfService = value;
    }

}
