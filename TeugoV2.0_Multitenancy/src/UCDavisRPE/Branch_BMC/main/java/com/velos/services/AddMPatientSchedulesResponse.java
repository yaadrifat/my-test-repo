
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for addMPatientSchedulesResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="addMPatientSchedulesResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="addmpatientschedules" type="{http://velos.com/services/}mPatientScheduleList" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "addMPatientSchedulesResponse", propOrder = {
    "addmpatientschedules"
})
public class AddMPatientSchedulesResponse {

    protected MPatientScheduleList addmpatientschedules;

    /**
     * Gets the value of the addmpatientschedules property.
     * 
     * @return
     *     possible object is
     *     {@link MPatientScheduleList }
     *     
     */
    public MPatientScheduleList getAddmpatientschedules() {
        return addmpatientschedules;
    }

    /**
     * Sets the value of the addmpatientschedules property.
     * 
     * @param value
     *     allowed object is
     *     {@link MPatientScheduleList }
     *     
     */
    public void setAddmpatientschedules(MPatientScheduleList value) {
        this.addmpatientschedules = value;
    }

}
