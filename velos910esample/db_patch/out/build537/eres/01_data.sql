set define off;

update ER_OBJECT_SETTINGS set OBJECT_SEQUENCE = 4 where OBJECT_NAME = 'protocol_tab' and
OBJECT_SUBTYPE = '6';

update ER_OBJECT_SETTINGS set OBJECT_SEQUENCE = 6 where OBJECT_NAME = 'protocol_tab' and
OBJECT_SUBTYPE = '4';

update ER_OBJECT_SETTINGS set OBJECT_SEQUENCE = 7 where OBJECT_NAME = 'protocol_tab' and
OBJECT_SUBTYPE = '5';

commit;

Delete from er_repxsl where fk_report = 109;

commit;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,80,1,'01_data.sql',sysdate,'8.9.0 Build#537');

commit;