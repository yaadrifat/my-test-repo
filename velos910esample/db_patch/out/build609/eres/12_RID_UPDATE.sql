set define off;
DECLARE
  COUNTFLAG NUMBER(5);  
BEGIN
    SELECT COUNT(1) INTO COUNTFLAG FROM  CB_CORD WHERE RID IS NULL;    
    IF (COUNTFLAG > 0) THEN
     update CB_CORD  set RID=SEQ_RID.NEXTVAL WHERE RID IS NULL;       
commit;
end if;
END;
/


DECLARE
  COUNTFLAG NUMBER(5);  
BEGIN
    SELECT COUNT(1) INTO COUNTFLAG FROM  CB_NOTES WHERE RID IS NULL;    
    IF (COUNTFLAG > 0) THEN
     update CB_NOTES  set RID=SEQ_RID.NEXTVAL WHERE RID IS NULL;       
commit;
end if;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,152,12,'12_RID_UPDATE.sql',sysdate,'9.0.0 Build#609');

commit;
