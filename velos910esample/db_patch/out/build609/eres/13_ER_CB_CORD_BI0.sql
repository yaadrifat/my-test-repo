set define off;

create or replace
TRIGGER "ERES".ER_CB_CORD_BI0 BEFORE INSERT ON CB_CORD
       REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW  WHEN (NEW.rid IS NULL OR NEW.rid = 0) DECLARE
  raid NUMBER(10);
  erid NUMBER(10);
  usr VARCHAR(2000);
      insert_data VARCHAR2(4000);
     BEGIN
 BEGIN
 SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
 INTO usr FROM er_user
 WHERE pk_user = :NEW.creator ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
   USR := 'New User' ;
 END ;

   SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  audit_trail.record_transaction(raid, 'CB_CORD',erid, 'I',usr);
       insert_data:=:NEW.PK_CORD || '|' ||		
to_char(:NEW.CORD_CDR_CBU_ID) || '|' ||
to_char(:NEW.CORD_EXTERNAL_CBU_ID) || '|' ||	
to_char(:NEW.CORD_TNC_FROZEN) || '|' ||	
to_char(:NEW.CORD_CD34_CELL_COUNT) || '|' ||	
to_char(:NEW.CORD_TNC_FROZEN_PAT_WT) || '|' ||	
to_char(:NEW.CORD_BABY_BIRTH_DATE) || '|' ||	
to_char(:NEW.CORD_CBU_COLLECTION_DATE) || '|' ||	
to_char(:NEW.CORD_REGISTRY_ID) || '|' ||	
to_char(:NEW.CORD_LOCAL_CBU_ID) || '|' ||	
to_char(:NEW.CORD_ID_NUMBER_ON_CBU_BAG) || '|' ||	
to_char(:NEW.FK_CORD_BACT_CUL_RESULT) || '|' ||	
to_char(:NEW.FK_CORD_FUNGAL_CUL_RESULT) || '|' ||	
to_char(:NEW.FK_CORD_ABO_BLOOD_TYPE) || '|' ||	
to_char(:NEW.FK_CORD_RH_TYPE) || '|' ||	
to_char(:NEW.FK_CORD_BABY_GENDER_ID) || '|' ||	
to_char(:NEW.FK_CORD_CLINICAL_STATUS) || '|' ||	
to_char(:NEW.FK_CORD_CBU_LIC_STATUS) || '|' ||	
to_char(:NEW.CORD_AVAIL_CONFIRM_DATE) || '|' ||	
to_char(:NEW.CREATOR) || '|' ||	
to_char(:NEW.IP_ADD) || '|' ||
to_char(:NEW.CREATED_ON) || '|' ||	
to_char(:NEW.LAST_MODIFIED_BY) || '|' ||	
to_char(:NEW.LAST_MODIFIED_DATE) || '|' ||	
to_char(:NEW.DELETEDFLAG) || '|' ||	
to_char(:NEW.RID) || '|' ||	
to_char(:NEW.ADDITIONAL_INFO_RECQUIRED) || '|' ||	
to_char(:NEW.CORD_ISBI_DIN_CODE) || '|' ||	
to_char(:NEW.CORD_ISIT_PRODUCT_CODE) || '|' ||	
to_char(:NEW.CORD_ELIGIBLE_ADDITIONAL_INFO) || '|' ||	
to_char(:NEW.FK_CBB_ID) || '|' ||	
to_char(:NEW.FK_CORD_CBU_ELIGIBLE_STATUS) || '|' ||	
to_char(:NEW.FK_CORD_CBU_STATUS) || '|' ||	
to_char(:NEW.FK_CORD_CBU_INELIGIBLE_REASON) || '|' ||	
to_char(:NEW.FK_CORD_CBU_UNLICENSED_REASON) || '|' ||	
to_char(:NEW.FK_CODELST_EXTERNAL_NAME)  || '|' ||	
to_char(:NEW.FK_CODELST_EXTERNAL_NUM) || '|' ||	
to_char(:NEW.FK_CODE_OTHER) || '|' ||	
to_char(:NEW.IND_OTHER_NAME) || '|' ||		
to_char(:NEW.IND_OTHER_NUM) || '|' ||		
to_char(:NEW.CORD_CLINICAL_STATUS_DATE) || '|' ||		
to_char(:NEW.CORD_CLINICAL_STATUS_ADDI_INFO) || '|' ||		
to_char(:NEW.FK_SPECIMEN_ID) || '|' ||		
to_char(:NEW.FK_CODELST_EXTERNAL_NAME) || '|' ||		
to_char(:NEW.REGISTRY_MATERNAL_ID) || '|' ||		
to_char(:NEW.MATERNAL_LOCAL_ID) || '|' ||		
to_char(:NEW.FK_CODELST_ETHNICITY) || '|' ||		
to_char(:NEW.FK_CODELST_RACE) || '|' ||		
to_char(:NEW.FUNDING_REQUESTED) || '|' ||		
to_char(:NEW.FUNDED_CBU) || '|' ||		
to_char(:NEW.CORD_CDR_CBU_CREATED_BY) || '|' ||		
to_char(:NEW.CORD_CDR_CBU_LAST_MOD_BY) || '|' ||		
to_char(:NEW.CORD_CDR_CBU_LAST_MOD_DT) || '|' ||		
to_char(:NEW.CORD_CREATION_DATE) || '|' ||		
to_char(:NEW.CORD_ENTRY_PROGRESS) || '|' ||		
to_char(:NEW.CORD_SEARCHABLE) || '|' ||		
to_char(:NEW.CORD_DONOR_IDENTI_NUM) || '|' ||		
to_char(:NEW.MULTIPLE_BIRTH) || '|' ||		
to_char(:NEW.FRZ_DATE) || '|' ||		
to_char(:NEW.HEMOGLOBIN_SCRN) || '|' ||		
to_char(:NEW.IS_CORD_AVAIL_FOR_NMDP) || '|' ||		
to_char(:NEW.CORD_NMDP_CBU_ID) || '|' ||		
to_char(:NEW.CORD_NMDP_MATERNAL_ID) || '|' ||		
to_char(:NEW.CBU_AVAIL_CONFIRM_FLAG) || '|' ||		
to_char(:NEW.FK_UNAVAIL_RSN) || '|' ||		
to_char(:NEW.GUID) || '|' ||		
to_char(:NEW.FK_CBB_PROCEDURE) || '|' ||		
to_char(:NEW.CBB_PROCEDURE_START_DATE) || '|' ||		
to_char(:NEW.CBB_PROCEDURE_END_DATE) || '|' ||		
to_char(:NEW.CBB_PROCEDURE_VERSION) || '|' ||		
to_char(:NEW.ADDITI_TYPING_FLAG) || '|' ||		
to_char(:NEW.HRSA_RACE_ROLLUP) || '|' ||		
to_char(:NEW.NMDP_RACE_ID) || '|' ||		
to_char(:NEW.PRE_NUCL_CBU_CNT_CNCTRN) || '|' ||		
to_char(:NEW.PRE_PRCSNG_START_DATE) || '|' ||		
to_char(:NEW.FK_CBU_STOR_LOC) || '|' ||		
to_char(:NEW.FK_CBU_COLL_SITE) || '|' ||		
to_char(:NEW.FK_CBU_COLL_TYPE) || '|' ||		
to_char(:NEW.FK_CBU_DEL_TYPE) || '|' ||		
to_char(:NEW.BACT_COMMENT) || '|' ||		
to_char(:NEW.FUNG_COMMENT);      
    INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
   END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,152,13,'13_ER_CB_CORD_BI0.sql',sysdate,'9.0.0 Build#609');

commit;
