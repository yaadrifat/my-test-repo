create or replace
function f_get_dynamicformdata(pk_cord number,categry varchar2) return SYS_REFCURSOR IS
dynamicform_data SYS_REFCURSOR;
BEGIN
    OPEN dynamicform_data FOR
 select
                              ques.pk_questions pk_ques,
                              forq.ques_seq QUES_SEQ,
                              qgrp.fk_question_group FK_QUES_GRP,
                              NVL(forq.fk_dependent_ques,'0') DEPENT_QUES_PK,
                              NVL(forq.fk_dependent_ques_value,'0') DEPENT_QUES_VALUE,
                              ques_desc QUES_DESC,
                              formres.assess_response ASSES_RESPONSE,
                              formres.comments COMMENTS,
                              f_codelst_desc(formres.resp_code) DROPDOWN_VALUE,
                              f_codelst_desc(ques.response_type) RESPONSE_TYPE,
                              forq.fk_master_ques MASTER_QUES,
                              ques.add_comment_flag COMMENT_FLAG,
                              ques.assesment_flag ASSES_FLAG,
                              ques.unlicn_prior_to_shipment_flag UNLIC_SHIP_FLAG,
                              formres.pk_form_responses PK_FORM_RESP,
                              nvl(formres.resp_code,'0') RESPONSE_CODE,
                              case
                                  when formres.resp_val is null then '0'
                                  when formres.resp_val is not null and  f_codelst_desc(ques.response_type)='multiselect' and ( LENGTH(formres.resp_val) - LENGTH( REPLACE(formres.resp_val, ',', '') ) )>0 then f_codelstcommaval_to_desc(formres.resp_val,',')
                                  when formres.resp_val is not null and  f_codelst_desc(ques.response_type)='multiselect' and ( LENGTH(formres.resp_val) - LENGTH( REPLACE(formres.resp_val, ',', '') ) )=0 then f_codelst_desc(formres.resp_val)
                                   when formres.resp_val is not null and  f_codelst_desc(ques.response_type)='textfield' then formres.resp_val
                              end RESPONSE_VAL,
                              NVL(formres.resp_val,'0') RESPONSE_VAL12,
                              formres.fk_form_responses FK_FORM_RESP,
                              formres.pk_assessment PK_ASSSES,
                              formres.dynaformDate DYNAFORMDATE,
                              formres.assess_remarks ASSES_NOTES,
                              ques.unlicen_req_flag UNLIC_REQ_FLAG
                              from
                              cb_form_questions forq,
                              cb_question_grp qgrp,
                              cb_questions ques
                              left outer join
                              (select
                                  frmres.pk_form_responses,
                                  frmres.fk_question,
                                  frmres.response_code resp_code,
                                  frmres.response_value resp_val,
                                  frmres.comments,
                                  frmres.fk_form_responses,
                                  asses.pk_assessment pk_assessment,
                                  f_codelst_desc(asses.assessment_for_response) assess_response,
                                  asses.assessment_remarks assess_remarks,
                                  to_char(frmres.DATE_MOMANS_OR_FORMFILL,'Mon DD, YYYY') dynaformDate
                                from
                                    cb_form_responses frmres  left outer join (select pk_assessment pk_assessment,sub_entity_id sub_entity_id,assessment_remarks assessment_remarks,cb_assessment.assessment_for_response assessment_for_response from cb_assessment where sub_entity_type=(select pk_codelst from  er_codelst  where  codelst_type='sub_entity_type' and codelst_subtyp=categry)) asses on( asses.sub_entity_id=frmres.pk_form_responses)
                                where
                                    frmres.entity_id=pk_cord and
                                    frmres.fk_form_version=(select MAX(PK_FORM_VERSION) from cb_form_version where entity_id=pk_cord and fk_form=(select pk_form from cb_forms where FORMS_DESC=categry and cb_forms.is_current_flag=1)) and
                                    frmres.entity_type=(select pk_codelst from er_codelst where codelst_type='test_source' and codelst_subtyp='maternal'))	formres
                              on(formres.FK_QUESTION=ques.pk_questions)
                              where
                              ques.pk_questions=forq.fk_question and
                              ques.pk_questions=qgrp.fk_question and
                              forq.fk_form=(select pk_form from cb_forms where forms_desc=categry and IS_CURRENT_FLAG=1)  and
                              qgrp.fk_form=(select pk_form from cb_forms where forms_desc=categry and IS_CURRENT_FLAG=1)
                              order by  qgrp.pk_question_grp,ques.pk_questions;
                              RETURN DYNAMICFORM_DATA;
END;
/





create or replace
function f_get_dynamicformgrp(categry varchar2) return SYS_REFCURSOR IS

dynamicform_grp SYS_REFCURSOR;
BEGIN
    OPEN dynamicform_grp FOR
select
  distinct pk_question_group PK_QUESTION_GRP,
  nvl(question_grp_desc,'0') QUESTION_GRP_NAME
from
  cb_question_group quesgrp,
  cb_question_grp qgrp
where
  qgrp.fk_question_group=quesgrp.pk_question_group and
  qgrp.fk_form=(select pk_form from cb_forms frms where frms.forms_desc=categry and frms.is_current_flag=1)
order by quesgrp.pk_question_group;

return dynamicform_grp;
end;
/





create or replace
function f_get_createlastmod(pk_cord number,categry varchar2) return SYS_REFCURSOR IS
history_data SYS_REFCURSOR;
BEGIN
    OPEN history_data FOR 
                            
                            select
                                  TO_CHAR(CREATED_ON,'Mon DD, YYYY') CREATED_ON,
                                  F_GETUSER(CREATOR) CREATOR,
                                  F_GETUSER(LAST_MODIFIED_BY) MODIFIED_BY,
                                  TO_CHAR(LAST_MODIFIED_DATE,'MON DD, YYYY') MODIFIED_ON,
                                  max(pk_form_responses) PK_MAX
                            from
                                  cb_form_responses
                            where
                                  fk_form_version=(select max(pk_form_version) from cb_form_version where entity_id=pk_cord and fk_form=(select pk_form from cb_forms where forms_desc=categry and is_current_flag=1)) group by TO_CHAR(CREATED_ON,'Mon DD, YYYY'), F_GETUSER(CREATOR), F_GETUSER(LAST_MODIFIED_BY), TO_CHAR(LAST_MODIFIED_DATE,'MON DD, YYYY');

RETURN HISTORY_DATA;
END;
/

COMMIT;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,179,26,'26_fucntions.sql',sysdate,'9.0.0 Build#636');

commit;