create or replace FUNCTION        "F_EXTSUM4" (p_studyid NUMBER, p_codelst_type VARCHAR2, p_codelst_subtyp VARCHAR2, p_fromdate DATE, p_todate DATE,p_ethnicity VARCHAR2)
RETURN NUMBER
IS
v_retval NUMBER;
v_codelst_type VARCHAR2(20);
BEGIN

	 CASE p_codelst_type
	 WHEN 'G' THEN
	 	   v_codelst_type := 'gender';
	 WHEN 'R' THEN
	 	   v_codelst_type := 'race';
	 WHEN 'E' THEN
	 	   v_codelst_type := 'ethnicity';
	 END CASE;

	 CASE v_codelst_type
	 WHEN 'gender' THEN
	 	 IF p_codelst_subtyp IS NULL THEN
			SELECT COUNT(*) INTO v_retval
			FROM EPAT.person
			WHERE pk_person IN (SELECT fk_per FROM ER_PATPROT WHERE fk_study = p_studyid AND patprot_stat = 1 AND  patprot_enroldt BETWEEN p_fromdate AND p_todate) AND
			fk_codelst_gender IS NULL;
		 ELSE
			SELECT COUNT(*) INTO v_retval
			FROM EPAT.person
			WHERE pk_person IN (SELECT fk_per FROM ER_PATPROT WHERE fk_study = p_studyid AND patprot_stat = 1 AND  patprot_enroldt BETWEEN p_fromdate AND p_todate) AND
			fk_codelst_gender = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type =v_codelst_type AND codelst_subtyp = p_codelst_subtyp);
		END IF;
	 WHEN 'race' THEN
	 	IF p_ethnicity IS NULL THEN
	 	 IF p_codelst_subtyp IS NULL THEN
			SELECT COUNT(*) INTO v_retval
			FROM EPAT.person
			WHERE pk_person IN (SELECT fk_per FROM ER_PATPROT WHERE fk_study = p_studyid AND patprot_stat = 1 AND  patprot_enroldt BETWEEN p_fromdate AND p_todate) AND
			fk_codelst_race IS NULL;
		 ELSE
			SELECT COUNT(*) INTO v_retval
			FROM EPAT.person
			WHERE pk_person IN (SELECT fk_per FROM ER_PATPROT WHERE fk_study = p_studyid AND patprot_stat = 1 AND  patprot_enroldt BETWEEN p_fromdate AND p_todate) AND
			fk_codelst_race = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type =v_codelst_type AND codelst_subtyp = p_codelst_subtyp);
		END IF;
		ELSE
		 	 IF p_codelst_subtyp IS NULL THEN
				SELECT COUNT(*) INTO v_retval
				FROM EPAT.person
				WHERE pk_person IN (SELECT fk_per FROM ER_PATPROT WHERE fk_study = p_studyid AND patprot_stat = 1 AND  patprot_enroldt BETWEEN p_fromdate AND p_todate) AND
				fk_codelst_race IS NULL AND
				fk_codelst_ethnicity = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type ='ethnicity' AND codelst_subtyp = p_ethnicity);
			ELSE
				SELECT COUNT(*) INTO v_retval
				FROM EPAT.person
				WHERE pk_person IN (SELECT fk_per FROM ER_PATPROT WHERE fk_study = p_studyid AND patprot_stat = 1 AND  patprot_enroldt BETWEEN p_fromdate AND p_todate) AND
				fk_codelst_race = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type =v_codelst_type AND codelst_subtyp = p_codelst_subtyp) AND
				fk_codelst_ethnicity = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type ='ethnicity' AND codelst_subtyp = p_ethnicity);
			END IF;
		END IF;
	 WHEN 'ethnicity' THEN
	 	 IF p_codelst_subtyp IS NULL THEN
			SELECT COUNT(*) INTO v_retval
			FROM EPAT.person
			WHERE pk_person IN (SELECT fk_per FROM ER_PATPROT WHERE fk_study = p_studyid AND patprot_stat = 1 AND  patprot_enroldt BETWEEN p_fromdate AND p_todate) AND
			fk_codelst_ethnicity IS NULL;
		ELSE
			SELECT COUNT(*) INTO v_retval
			FROM EPAT.person
			WHERE pk_person IN (SELECT fk_per FROM ER_PATPROT WHERE fk_study = p_studyid AND patprot_stat = 1 AND  patprot_enroldt BETWEEN p_fromdate AND p_todate) AND
			fk_codelst_ethnicity = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type =v_codelst_type AND codelst_subtyp = p_codelst_subtyp);
		END IF;
	END CASE;

  RETURN v_retval;
END ;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,163,5,'05_f_extsum4.sql',sysdate,'9.0.0 Build#620');

commit;
