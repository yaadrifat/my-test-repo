set define off;
Declare
var_text1 clob := 'SELECT (SELECT form_name FROM ER_FORMLIB WHERE pk_formlib = fk_formlib) AS form_name,
(SELECT per_code FROM ER_PER WHERE pk_per = fk_per) AS patient_id,
(SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''enrolled'')) AS enrolled_date,
(SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''active'') AND ROWNUM = 1) AS active_date,
(SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''offtreat'') AND ROWNUM = 1) AS offtreat_date,
(SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''offstudy'') AND ROWNUM = 1) AS offstudy_date,
(SELECT (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_stat) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND patstudystat_date = (SELECT MAX(patstudystat_date) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study ) AND ROWNUM = 1) AS current_status,
(SELECT COUNT(*) FROM ER_PATFORMS WHERE fk_formlib = x.fk_formlib AND fk_patprot = pk_patprot AND record_type <> ''D'') AS tot_forms,
(SELECT COUNT(*) FROM ER_PATFORMS WHERE fk_formlib = x.fk_formlib AND fk_patprot = pk_patprot AND record_type <> ''D'' AND form_completed = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''fillformstat'' AND codelst_subtyp = ''complete'')) AS comp_formstat,
(SELECT COUNT(*) FROM ER_PATFORMS WHERE fk_formlib = x.fk_formlib AND fk_patprot = pk_patprot AND record_type <> ''D'' AND form_completed = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''fillformstat'' AND codelst_subtyp = ''working'')) AS work_formstat,
(SELECT COUNT(*) FROM ER_PATFORMS WHERE fk_formlib = x.fk_formlib AND fk_patprot = pk_patprot AND record_type <> ''D'' AND form_completed NOT IN (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''fillformstat'' AND (codelst_subtyp = ''working'' OR codelst_subtyp = ''complete''))) AS othr_formstat,
(SELECT to_char(MAX(created_on),PKG_DATEUTIL.F_GET_DATETIMEFORMAT) FROM ER_PATFORMS WHERE fk_formlib = x.fk_formlib AND fk_patprot = pk_patprot AND record_type <> ''D'' ) AS form_createdon, (SELECT (SELECT usr_firstname || '' '' || usr_lastname FROM ER_USER WHERE pk_user = y.creator) FROM ER_PATFORMS y WHERE fk_formlib = x.fk_formlib AND fk_patprot = pk_patprot AND record_type <> ''D'' AND created_on = (SELECT MAX(created_on) FROM ER_PATFORMS WHERE fk_formlib = x.fk_formlib AND fk_patprot = pk_patprot AND record_type <> ''D'' ) AND ROWNUM = 1) AS form_createdby,
(SELECT to_char(MAX(last_modified_date),PKG_DATEUTIL.F_GET_DATEtimeFORMAT) FROM ER_PATFORMS WHERE fk_formlib = x.fk_formlib AND fk_patprot = pk_patprot AND record_type <> ''D'' ) AS form_modon,
(SELECT (SELECT usr_firstname || '' '' || usr_lastname FROM ER_USER WHERE pk_user = y.last_modified_by) FROM ER_PATFORMS y WHERE fk_formlib = x.fk_formlib AND fk_patprot = pk_patprot AND record_type <> ''D'' AND last_modified_date = (SELECT MAX(last_modified_date) FROM ER_PATFORMS WHERE fk_formlib = x.fk_formlib AND fk_patprot = pk_patprot AND record_type <> ''D'' ) AND ROWNUM = 1) AS form_modby
FROM
(SELECT DISTINCT a.fk_per,fk_formlib,fk_study, pk_patprot
FROM ER_PATPROT a, ER_PATFORMS b
WHERE pk_patprot = fk_patprot
AND fk_study IN (:studyId) AND a.fk_per IN (:patientId) AND
(b.created_on BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) OR b.last_modified_date BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) ) ) x   UNION ALL
SELECT ''Adverse EVENTS'' AS form_name,
(SELECT per_code FROM ER_PER WHERE pk_per = fk_per) AS patient_id,
(SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''enrolled'')) AS enrolled_date,
(SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''active'') AND ROWNUM = 1) AS active_date,
(SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''offtreat'') AND ROWNUM = 1) AS offtreat_date,
(SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''offstudy'') AND ROWNUM = 1) AS offstudy_date,
(SELECT (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_stat) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND patstudystat_date = (SELECT MAX(patstudystat_date) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study ) AND ROWNUM = 1) AS current_status, (SELECT COUNT(*) FROM ESCH.sch_adverseve WHERE fk_study = x.fk_study AND fk_per = x.fk_per) AS tot_forms,
(SELECT COUNT(*) FROM ESCH.sch_adverseve WHERE fk_study = x.fk_study AND fk_per = x.fk_per AND form_status = (SELECT pk_codelst FROM ESCH.sch_codelst WHERE trim(codelst_type) = ''fillformstat'' AND trim(codelst_subtyp) = ''complete'')) AS comp_formstat,
(SELECT COUNT(*) FROM ESCH.sch_adverseve WHERE fk_study = x.fk_study AND fk_per = x.fk_per AND form_status = (SELECT pk_codelst FROM ESCH.sch_codelst WHERE trim(codelst_type) = ''fillformstat'' AND trim(codelst_subtyp) = ''working'')) AS work_formstat,
0 AS othr_formstat,
(SELECT to_char(MAX(created_on),PKG_DATEUTIL.F_GET_DATETIMEFORMAT) FROM ESCH.sch_adverseve WHERE fk_study = x.fk_study AND fk_per = x.fk_per) AS form_createdon,
(SELECT (SELECT usr_firstname || '' '' || usr_lastname FROM ER_USER WHERE pk_user = y.creator) FROM ESCH.sch_adverseve y WHERE fk_study = x.fk_study AND fk_per = x.fk_per AND created_on = (SELECT MAX(created_on) FROM ESCH.sch_adverseve WHERE fk_study = x.fk_study AND fk_per = x.fk_per) AND ROWNUM = 1) AS form_createdby,
(SELECT  to_char(MAX(last_modified_date),PKG_DATEUTIL.F_GET_DATEtimeFORMAT) FROM ESCH.sch_adverseve WHERE fk_study = x.fk_study AND fk_per = x.fk_per) AS form_modon,
(SELECT (SELECT usr_firstname || '' '' || usr_lastname FROM ER_USER WHERE pk_user =  y.last_modified_by) FROM ESCH.sch_adverseve y WHERE fk_study = x.fk_study AND fk_per = x.fk_per AND last_modified_date = (SELECT MAX(last_modified_date) FROM ESCH.sch_adverseve WHERE fk_study = x.fk_study AND fk_per = x.fk_per) AND ROWNUM = 1) AS form_modby
FROM
(SELECT DISTINCT a.fk_per,a.fk_study
FROM ER_PATPROT a, ESCH.sch_adverseve b
WHERE a.FK_PER = b.FK_PER AND a.fk_study = b.fk_study
AND a.fk_study IN (:studyId) AND Patprot_stat = 1 AND a.fk_per IN (:patientId) AND
(b.created_on BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) OR b.last_modified_date BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) ) ) x
 UNION ALL
SELECT ''Labs'' AS form_name,
(SELECT per_code FROM ER_PER WHERE pk_per = fk_per) AS patient_id, (SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''enrolled'')) AS enrolled_date,
(SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''active'') AND ROWNUM = 1) AS active_date,
(SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''offtreat'') AND ROWNUM = 1) AS offtreat_date,
(SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''offstudy'') AND ROWNUM = 1) AS offstudy_date,
(SELECT (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_stat) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND patstudystat_date = (SELECT MAX(patstudystat_date) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study ) AND ROWNUM = 1) AS current_status,
(SELECT COUNT(*) FROM ER_PATLABS WHERE fk_per = x.fk_per) AS tot_forms,
0 AS comp_formstat,
0 AS work_formstat,
0 AS othr_formstat,
(SELECT to_char(MAX(created_on),PKG_DATEUTIL.F_GET_DATETIMEFORMAT) FROM ER_PATLABS WHERE fk_per = x.fk_per) AS form_createdon,
(SELECT (SELECT usr_firstname || '' '' || usr_lastname FROM ER_USER WHERE pk_user = y.creator) FROM ER_PATLABS y WHERE fk_per = x.fk_per AND created_on = (SELECT MAX(created_on) FROM ER_PATLABS WHERE fk_per = x.fk_per ) AND ROWNUM = 1) AS form_createdby,
(SELECT to_char(MAX(last_modified_date),PKG_DATEUTIL.F_GET_DATEtimeFORMAT) FROM ER_PATLABS WHERE fk_per = x.fk_per) AS form_modon,
(SELECT (SELECT usr_firstname || '' '' || usr_lastname FROM ER_USER WHERE pk_user =  y.last_modified_by) FROM ER_PATLABS y WHERE fk_per = x.fk_per AND last_modified_date = (SELECT MAX(last_modified_date) FROM ER_PATLABS WHERE fk_per = x.fk_per ) AND ROWNUM = 1) AS form_modby
FROM
(SELECT DISTINCT a.fk_per,a.fk_study
FROM ER_PATPROT a, ER_PATLABS b
WHERE a.fk_per = b.fk_per
AND a.fk_study IN (:studyId) AND Patprot_stat = 1 AND a.fk_per IN (:patientId) AND
(b.created_on BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) OR b.last_modified_date BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) ) ) x
ORDER BY 2,1';

BEGIN
UPDATE er_report
SET REP_SQL_CLOB = var_text1
WHERE pk_report=66;

END;
/
commit;
declare
var_text2 clob:= 'select std.study_number study_number, std.study_title study_title, to_char(std.study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT) study_actualdt,
st.site_name site_name,st.pk_site,
nvl((Select count(PK_ADVEVE) from ESCH.EV_ADV_TYPE b
     Where st.pk_site = b.pk_site and b.fk_study = std.pk_study and b.adv_type =''al_adve'' and
      AE_STDATE between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)),0) ADV_COUNT,
nvl((Select count(PK_ADVEVE) from ESCH.EV_ADV_TYPE b
     Where st.pk_site = b.pk_site and b.fk_study = std.pk_study and b.adv_type = ''al_sadve'' and
     AE_STDATE between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)),0) SADV_COUNT,
nvl((select count(PK_ADVEVE)
    from ESCH.EV_ADV_SITE_INFO b
    where st.pk_site = b.pk_site and b.fk_study = std.pk_study and
    b.ADVINFO_TYPE   = ''O'' and
    b.CODELST_SUBTYP = ''al_death'' and
    b.ADVINFO_VALUE = 1 and
    b.AE_STDATE between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
    ),0) DEATH_COUNT,
nvl((Select count(PK_ADVEVE)
    from ESCH.EV_ADV_SITE_INFO b
    where st.pk_site = b.pk_site and b.fk_study = std.pk_study and
    CODELST_SUBTYP = ''adv_unexp''and
    ADVINFO_TYPE   = ''A'' and
    ADVINFO_VALUE = 1 and
    b.AE_STDATE between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)),0) UNEXP_COUNT,
nvl((Select count(PK_ADVEVE)
    from ESCH.EV_ADV_SITE_INFO b
    where st.pk_site = b.pk_site and b.fk_study = std.pk_study and
    CODELST_SUBTYP = ''adv_violation''and
    ADVINFO_TYPE   = ''A'' and
    ADVINFO_VALUE = 1 and 
    b.AE_STDATE between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)),0) VIO_COUNT,
nvl((Select count(PK_ADVEVE)
    from ESCH.EV_ADV_SITE_INFO b
    where st.pk_site = b.pk_site and b.fk_study = std.pk_study and
    CODELST_SUBTYP = ''adv_dopped''and
    ADVINFO_TYPE   = ''A'' and
    ADVINFO_VALUE = 1 and
    b.AE_STDATE between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) ),0) DROP_COUNT,
nvl((Select count(PK_ADVEVE) from ESCH.EV_ADV_TYPE b
     Where st.pk_site = b.pk_site and b.fk_study = std.pk_study and
     b.REL_TYPE = ''ad_def'' and
     AE_STDATE between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) ),0) R_DEF_COUNT,
nvl((Select count(PK_ADVEVE) from ESCH.EV_ADV_TYPE b
     Where st.pk_site = b.pk_site and b.fk_study = std.pk_study and
     b.REL_TYPE = ''ad_nr'' and
     AE_STDATE between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) ),0) R_NR_COUNT,
nvl((Select count(PK_ADVEVE) from ESCH.EV_ADV_TYPE b
     Where st.pk_site = b.pk_site and b.fk_study = std.pk_study and
     b.REL_TYPE = ''ad_pos'' and
     AE_STDATE between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) ),0)
R_POS_COUNT,
nvl((Select count(PK_ADVEVE) from ESCH.EV_ADV_TYPE b
     Where st.pk_site = b.pk_site and b.fk_study = std.pk_study and
     b.REL_TYPE = ''ad_prob'' and
     AE_STDATE between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) ),0)
R_PROB_COUNT,
nvl((Select count(PK_ADVEVE) from ESCH.EV_ADV_TYPE b
     Where st.pk_site = b.pk_site and b.fk_study = std.pk_study and
     b.REL_TYPE = ''ad_unknown'' and
     AE_STDATE between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)),0) R_UNK_COUNT,
nvl((Select count(PK_ADVEVE) from ESCH.EV_ADV_TYPE b
     Where st.pk_site = b.pk_site and b.fk_study = std.pk_study and
     b.REL_TYPE = ''ad_unlike'' and
     AE_STDATE between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) ),0) R_UNL_COUNT
from er_site st, er_study std
where pk_study = :studyId and
pk_site in (Select distinct er_user.FK_SITEID
from er_studyteam team, er_user
Where team.fk_study = std.pk_study and
er_user.pk_user = team.fk_user and
nvl(study_team_usr_type,''D'') = ''D'' ) ';

BEGIN

UPDATE er_report
SET REP_SQL_CLOB = var_text2
WHERE pk_report=81;

END;
/
commit;
declare
var_text3 clob := 'SELECT study_number,
study_title,DECODE(F_Get_Sum4_Data(a.pk_study,''sum4_agent''),''Y'',''Agent OR Device'',
					DECODE(F_Get_Sum4_Data(a.pk_study,''sum4_beh''),''Y'',''Trials Involving other Interventions'',
					DECODE(F_Get_Sum4_Data(a.pk_study,''sum4_na''),''Y'',''Epidemiologic OR other Observational Studies'',
					DECODE(F_Get_Sum4_Data(a.pk_study,''sum4_comp'') ,''Y'',''Companion, ANCILLARY OR Correlative Studies'','''')))) AS trial_type,
F_Get_Codelstdesc(a.fk_codelst_restype) AS research_type,F_Get_Codelstdesc(a.fk_codelst_type) AS study_type,
F_Get_Codelstdesc(a.fk_codelst_scope) AS study_scope,F_Get_Codelstdesc(a.fk_codelst_tarea) AS study_tarea,
F_Get_Sum4_Data(a.pk_study,''sum4_prg'') AS program_code,decode(fk_codelst_sponsor,null,study_sponsor,( select codelst_desc from er_codelst where pk_codelst = fk_codelst_sponsor)) study_sponsor,F_Getdis_Site(a.study_disease_site) AS disease_site,
F_Find_Study_Pi(a.pk_study) || '' '' || DECODE(study_otherprinv, NULL,'''', ''; '' || study_otherprinv )  AS PRINCIPAL_INVESTIGATOR,
F_Get_Codelstdesc(a.fk_codelst_phase) AS study_phase,study_samplsize AS target,TO_CHAR(study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT) study_actualdt,  TO_CHAR(study_end_date,PKG_DATEUTIL.F_GET_DATEFORMAT) study_end_date,
(SELECT COUNT(*) FROM ER_PATPROT,person WHERE fk_study = a.pk_study AND patprot_stat = 1 AND PATPROT_ENROLDT IS NOT NULL AND pk_person = fk_per AND fk_site IN (:orgId)  ) AS ctr_to_date,
(SELECT COUNT(*) FROM ER_PATPROT,person WHERE fk_study = a.pk_study AND patprot_stat = 1 AND patprot_enroldt BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
AND pk_person = fk_per AND fk_site IN (:orgId)  ) AS ctr_12_mos,
race_ne,race_white,race_asian,race_notrep,race_indala,race_blkafr,race_hwnisl,race_unknown,ethnicity_ne,ethnicity_hispanic,ethnicity_unknown,ethnicity_nonhispanic,ethnicity_notreported,gender_ne,gender_male,gender_female,gender_other,gender_unknown
FROM (SELECT fk_study,SUM(race_ne) AS race_ne,SUM(race_white) AS race_white,SUM(race_asian) AS race_asian,SUM(race_notrep) AS  race_notrep,SUM(race_indala) AS race_indala,SUM(race_blkafr) AS race_blkafr,
SUM(race_hwnisl) AS race_hwnisl,SUM(race_unknown) AS race_unknown,SUM(ethnicity_ne) AS ethnicity_ne,SUM(ethnicity_hispanic) AS ethnicity_hispanic,SUM(ethnicity_unknown) AS ethnicity_unknown,
SUM(ethnicity_nonhispanic) AS ethnicity_nonhispanic,SUM(ethnicity_notreported) AS ethnicity_notreported,
SUM(gender_ne) AS gender_ne,SUM(gender_male) AS gender_male,SUM(gender_female) AS gender_female,SUM(gender_other) AS gender_other,
SUM(gender_unknown) AS gender_unknown FROM (SELECT fk_study, DECODE(race,NULL,1,0) AS race_ne,DECODE(race,''race_white'',1,0) AS race_white,DECODE(race,''race_asian'',1,0) AS race_asian,
DECODE(race,''race_notrep'',1,0) AS race_notrep, DECODE(race,''race_indala'',1,0) AS race_indala, DECODE(race,''race_blkafr'',1,0) AS race_blkafr,
DECODE(race,''race_hwnisl'',1,0) AS race_hwnisl, DECODE(race,''race_unknown'',1,0) AS race_unknown,DECODE(ethnicity,NULL,1,0) AS ethnicity_ne,
DECODE(ethnicity,''hispanic'',1,0) AS ethnicity_hispanic, DECODE(ethnicity,''Unknown'',1,0) AS ethnicity_unknown,DECODE(ethnicity,''nonhispanic'',1,0) AS ethnicity_nonhispanic,DECODE(ethnicity,''notreported'',1,0) AS ethnicity_notreported,
DECODE(gender,NULL,1,0) AS gender_ne, DECODE(gender,''male'',1,0) AS gender_male,
DECODE(gender,''female'',1,0) AS gender_female,
DECODE(gender,''other'',1,0) AS gender_other,
DECODE(gender,''Unknown'',1,0) AS gender_unknown FROM  (
SELECT fk_study,
(SELECT codelst_subtyp FROM ER_CODELST WHERE pk_codelst = fk_codelst_race) AS race,
(SELECT codelst_subtyp FROM ER_CODELST WHERE pk_codelst = fk_codelst_gender) AS gender,
(SELECT codelst_subtyp FROM ER_CODELST WHERE pk_codelst = fk_codelst_ethnicity) AS ethnicity
FROM EPAT.person, ER_PATPROT a
WHERE pk_person = fk_per AND fk_site IN (:orgId) AND
patprot_enroldt BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND
patprot_stat = 1
AND fk_study IN(SELECT pk_study FROM ER_STUDY WHERE pk_study IN (SELECT  DISTINCT fk_study FROM ER_STUDYTEAM WHERE
fk_codelst_tmrole =(SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp=''role_prin'')
AND fk_user IN (:userId) OR STUDY_PRINV IN (:userId) OR STUDY_PRINV IS NULL ))
)) GROUP BY fk_study
), ER_STUDY a WHERE pk_study = fk_study AND fk_account = :sessAccId
AND fk_study IN (:studyId) AND fk_codelst_tarea IN (:tAreaId) AND ((study_division IN (:studyDivId)) OR (study_division IS NULL))
ORDER BY study_number';
BEGIN

UPDATE er_report
SET REP_SQL_CLOB = var_text3
WHERE pk_report=117;
END;
/
commit;

UPDATE ER_REPORT
SET REP_SQL =
  'select er_study.study_number,    
er_site.site_name,    
to_char(er_study.study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT)    
study_actualdt,        
er_study.study_title,        
PER_CODE patient_id,        
patprot_patstdid,    
to_char(er_patprot.PATPROT_ENROLDT,PKG_DATEUTIL.F_GET_DATEFORMAT) pe,    
(select name         from ESCH.event_assoc        where    
EVENT_ID = er_patprot.fk_protocol) protocol,    
to_char(er_patprot.patprot_start,PKG_DATEUTIL.F_GET_DATEFORMAT) ps,    
( select codelst_desc      
from er_codelst      
where pk_codelst = (select FK_CODELST_STAT   from      
er_patstudystat      
where fk_per = er_patprot.fk_per      
and fk_study = er_study.pk_study      
and PATSTUDYSTAT_ENDT is null )     
) patient_status
from er_patprot, er_study,  er_per  , er_site
where   er_patprot.PATPROT_STAT = 1
and     er_patprot.fk_study = ~1
and     er_per.PK_PER = er_patprot.fk_per
and     er_per.fk_site in (~4)
and     er_per.fk_site = er_site.pk_site
and     er_study.pk_study = er_patprot.fk_study
and     er_patprot.PATPROT_ENROLDT >= ''~2''
and     er_patprot.PATPROT_ENROLDT <= ''~3'' '
WHERE PK_REPORT = 16;
COMMIT;

UPDATE ER_REPORT
SET REP_SQL_CLOB =
  'select er_study.study_number,    
er_site.site_name,    
to_char(er_study.study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT)    
study_actualdt,        
er_study.study_title,        
PER_CODE patient_id,        
patprot_patstdid,    
to_char(er_patprot.PATPROT_ENROLDT,PKG_DATEUTIL.F_GET_DATEFORMAT) pe,    
(select name         from ESCH.event_assoc        where    
EVENT_ID = er_patprot.fk_protocol) protocol,    
to_char(er_patprot.patprot_start,PKG_DATEUTIL.F_GET_DATEFORMAT) ps,    
( select codelst_desc      
from er_codelst      
where pk_codelst = fk_codelst_stat ) patient_status
from er_patprot, er_study,  er_per  , er_site,ER_PATSTUDYSTAT stat
where   er_patprot.PATPROT_STAT = 1
and     er_patprot.fk_study in (:studyId)
and     er_per.PK_PER = er_patprot.fk_per
and     er_per.fk_site in (:orgId)
and     er_per.fk_site = er_site.pk_site
and     er_study.pk_study = er_patprot.fk_study
AND ER_PATPROT.fk_per=stat.fk_per AND ER_PATPROT.fk_study=stat.fk_study
AND  PATSTUDYSTAT_ENDT IS NULL
and     er_patprot.PATPROT_ENROLDT >= TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
and     er_patprot.PATPROT_ENROLDT <= TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
and fk_codelst_stat in (:patStatusId)'
WHERE PK_REPORT = 16;
COMMIT;

UPDATE ER_REPORT
SET REP_SQL     = 'select * from
(
select 
ceil(dts.days/7) weeks , 
decode(mod(dts.days,7),0,7,mod(dts.days,7)) days , 
a.event_id , 
a.chain_id , 
a.displacement , 
a.name  , 
a.duration , 
a.FUZZY_PERIOD , 
prot.pname , 
study.study_number , 
to_char(study.study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT) study_actualdt , 
ESCH.lst_cost(a.event_id) lstcost, 
ESCH.lst_usr(a.event_id) lstusr, 
ESCH.lst_doc(a.event_id)  lstdoc, 
ESCH.lst_forms(a.event_id)  lstforms 
from 
(select a.name pname from erv_eveassoc a where a.event_id = ~1 ) prot ,  
er_tmprep a,  
( select rownum days      
from all_objects     
where rownum <            
( select max(displacement)+1 days              
from er_tmprep             
)  
) dts , 
(select study_number study_number , study_actualdt from er_study where pk_study = ~2 ) study 
where  a.displacement(+)=dts.days) 
where name is not null order  by 1,2'
WHERE PK_REPORT = 44;
COMMIT;

UPDATE ER_REPORT
SET REP_SQL_CLOB = 'select * from
(
select 
ceil(dts.days/7) weeks , 
decode(mod(dts.days,7),0,7,mod(dts.days,7)) days , 
a.event_id , 
a.chain_id , 
a.displacement , 
a.name  , 
a.duration , 
a.FUZZY_PERIOD , 
prot.pname , 
study.study_number , 
to_char(study.study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT) study_actualdt , 
ESCH.lst_cost(a.event_id) lstcost, 
ESCH.lst_usr(a.event_id) lstusr, 
ESCH.lst_doc(a.event_id)  lstdoc, 
ESCH.lst_forms(a.event_id)  lstforms 
from 
(select a.name pname from erv_eveassoc a where a.event_id in (:protCalId) ) prot ,  
er_tmprep a,  
( select rownum days      
from all_objects     
where rownum <            
( select max(displacement)+1 days              
from er_tmprep             
)  
) dts , 
(select study_number study_number , study_actualdt from er_study where pk_study in (:studyId) ) study 
where  a.displacement(+)=dts.days) 
where name is not null order  by 1,2'
WHERE PK_REPORT  = 44;
COMMIT;

UPDATE ER_REPORT
SET REP_SQL =
  'select PERSON_CODE , PAT_STUDYID, FK_PER ,  STUDY_NUMBER,  STUDY_TITLE ,   
to_char(study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT) study_actualdt ,  
PROTOCOL_NAME,  PATPROT_ENROLDT,  
SITE_NAME, 
to_char(PATPROT_START,PKG_DATEUTIL.F_GET_DATEFORMAT) PATPROT_START,    
EVENT_NAME, MONTH, to_char(EVENT_SCHDATE,''YYYY'') Year ,    
to_char(EVENT_SCHDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) START_DATE_TIME, EVENT_STATUS ,    
to_char(EVENT_EXEON,PKG_DATEUTIL.F_GET_DATEFORMAT) EVENT_EXEON ,     
( select max(codelst_desc)         
from ESCH.SCH_EVENTCOST A, ESCH.SCH_CODELST B        
where  A.FK_CURRENCY = B.PK_CODELST          
and A.fk_event = fk_assoc     
) cost_currency,    
to_char(ESCH.tot_cost(fk_assoc),''9999999999.99'') evecost,    
ESCH.lst_cost(fk_assoc) event_costlst,    
ESCH.lst_costdesc(fk_assoc) event_costdesc ,  
FK_PATPROT, visit_name as VISIT 
from erv_patsch
where protocolid = ~1  
and fk_site in (~4)  
and EVENT_SCHDATE between ''~2'' and ''~3''   and event_status = ''Done''  
Order by person_code, EVENT_SCHDATE'
WHERE PK_REPORT = 45;
COMMIT;

UPDATE ER_REPORT
SET REP_SQL_CLOB =
  'select PERSON_CODE , PAT_STUDYID, FK_PER ,  STUDY_NUMBER,  STUDY_TITLE ,   
to_char(study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT) study_actualdt ,  
PROTOCOL_NAME,  PATPROT_ENROLDT,  
SITE_NAME, 
to_char(PATPROT_START,PKG_DATEUTIL.F_GET_DATEFORMAT) PATPROT_START,    
EVENT_NAME, MONTH, to_char(EVENT_SCHDATE,''YYYY'') Year ,    
to_char(EVENT_SCHDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) START_DATE_TIME, EVENT_STATUS ,    
to_char(EVENT_EXEON,PKG_DATEUTIL.F_GET_DATEFORMAT) EVENT_EXEON ,     
( select max(codelst_desc)         
from ESCH.SCH_EVENTCOST A, ESCH.SCH_CODELST B        
where  A.FK_CURRENCY = B.PK_CODELST          
and A.fk_event = fk_assoc     
) cost_currency,    
to_char(ESCH.tot_cost(fk_assoc),''9999999999.99'') evecost,    
ESCH.lst_cost(fk_assoc) event_costlst,    
ESCH.lst_costdesc(fk_assoc) event_costdesc ,  
FK_PATPROT, visit_name as VISIT 
from erv_patsch 
where protocolid in (:protCalId)  
and fk_site in (:orgId)  
and fk_study in (:studyId)  
and EVENT_SCHDATE between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)   and event_status_subtype = ''ev_done''  
Order by person_code, EVENT_SCHDATE'
WHERE PK_REPORT = 45;
COMMIT;

UPDATE ER_REPORT
SET REP_SQL =
  'select  
PERSON_CODE ,  FK_PER , PAT_STUDYID, STUDY_NUMBER,  SITE_NAME, STUDY_TITLE ,  
to_char(STUDY_ACTUALDT,PKG_DATEUTIL.F_GET_DATEFORMAT) STUDY_ACTUALDT,   
PROTOCOL_NAME,  PATPROT_ENROLDT, 
to_char(PATPROT_START,PKG_DATEUTIL.F_GET_DATEFORMAT) PATPROT_START,  
EVENT_NAME, MONTH, to_char(EVENT_SCHDATE,''YYYY'') Year ,  
to_char(EVENT_SCHDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) START_DATE_TIME,
EVENT_STATUS ,  
to_char(EVENT_EXEON,PKG_DATEUTIL.F_GET_DATEFORMAT) EVENT_EXEON,  
(select max(codelst_desc)     
from ESCH.SCH_EVENTCOST A, ESCH.SCH_CODELST B    
where  A.FK_CURRENCY = B.PK_CODELST      
and A.fk_event = fk_assoc  ) cost_currency,  
trim(to_char(ESCH.tot_cost(fk_assoc),0999999999999999.99))
evecost,visit_name as visit,  
ESCH.lst_cost(fk_assoc) event_costlst,  
ESCH.lst_costdesc(fk_assoc) event_costdesc ,  
trim(to_char(sum(ESCH.tot_cost(fk_assoc)) OVER (PARTITION By person_code),0999999999999999.99))  as tot,  
trim(to_char(sum(ESCH.tot_cost(fk_assoc)) OVER (PARTITION By PROTOCOL_NAME),0999999999999999.99)) as prot_tot,  
FK_PATPROT ,  
trim(to_char(sum(ESCH.tot_cost(fk_assoc)) OVER (PARTITION By FK_PATPROT),0999999999999999.99)) as tot_patprot 
from erv_patsch 
where protocolid = ~1   
and fk_per = ~4   
and EVENT_SCHDATE between ''~2'' and ''~3''   
and event_status = ''Done'' Order by VISIT,EVENT_SCHDATE'
WHERE PK_REPORT = 47;
COMMIT;

UPDATE ER_REPORT
SET REP_SQL_CLOB =
  'select  
PERSON_CODE ,  FK_PER , PAT_STUDYID, STUDY_NUMBER,  SITE_NAME, STUDY_TITLE ,  
to_char(STUDY_ACTUALDT,PKG_DATEUTIL.F_GET_DATEFORMAT) STUDY_ACTUALDT,   
PROTOCOL_NAME,  PATPROT_ENROLDT, 
to_char(PATPROT_START,PKG_DATEUTIL.F_GET_DATEFORMAT) PATPROT_START,  
EVENT_NAME, MONTH, to_char(EVENT_SCHDATE,''YYYY'') Year ,  
to_char(EVENT_SCHDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) START_DATE_TIME,
EVENT_STATUS ,  
to_char(EVENT_EXEON,PKG_DATEUTIL.F_GET_DATEFORMAT) EVENT_EXEON,  
(select max(codelst_desc)     
from ESCH.SCH_EVENTCOST A, ESCH.SCH_CODELST B    
where  A.FK_CURRENCY = B.PK_CODELST      
and A.fk_event = fk_assoc  ) cost_currency,  
trim(to_char(ESCH.tot_cost(fk_assoc),0999999999999999.99))
evecost,visit_name as visit,  
ESCH.lst_cost(fk_assoc) event_costlst,  
ESCH.lst_costdesc(fk_assoc) event_costdesc ,  
trim(to_char(sum(ESCH.tot_cost(fk_assoc)) OVER (PARTITION By person_code),0999999999999999.99))  as tot,  
trim(to_char(sum(ESCH.tot_cost(fk_assoc)) OVER (PARTITION By PROTOCOL_NAME),0999999999999999.99)) as prot_tot,  
FK_PATPROT ,  
trim(to_char(sum(ESCH.tot_cost(fk_assoc)) OVER (PARTITION By FK_PATPROT),0999999999999999.99)) as tot_patprot 
from erv_patsch 
where protocolid in (:protCalId)   
and fk_per in (:patientId)   
and EVENT_SCHDATE between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)   
and event_status = ''Done'' Order by VISIT,EVENT_SCHDATE'
WHERE PK_REPORT = 47;
COMMIT;

UPDATE ER_REPORT
SET REP_SQL =
  'select std.study_number study_number, std.study_title study_title, to_char(std.study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT) study_actualdt,
st.site_name site_name,st.pk_site,
nvl((Select count(PK_ADVEVE) from ESCH.EV_ADV_TYPE b     
Where st.pk_site = b.pk_site and b.fk_study = std.pk_study and b.adv_type =''al_adve'' and      
AE_STDATE between ''~2''and ''~3''),0) ADV_COUNT,
nvl((Select count(PK_ADVEVE) from ESCH.EV_ADV_TYPE b     
Where st.pk_site = b.pk_site and b.fk_study = std.pk_study and b.adv_type = ''al_sadve'' and     
AE_STDATE between ''~2''and ''~3''),0) SADV_COUNT,
nvl((select count(PK_ADVEVE)    
from ESCH.EV_ADV_SITE_INFO b    
where st.pk_site = b.pk_site and b.fk_study = std.pk_study and    
b.ADVINFO_TYPE   = ''O'' and    
b.CODELST_SUBTYP = ''al_death'' and    
b.ADVINFO_VALUE = 1 and    
b.AE_STDATE between ''~2''and ''~3''    
),0) DEATH_COUNT,
nvl((Select count(PK_ADVEVE)    
from ESCH.EV_ADV_SITE_INFO b    
where st.pk_site = b.pk_site and b.fk_study = std.pk_study and    
CODELST_SUBTYP = ''adv_unexp''and    
ADVINFO_TYPE   = ''A'' and    
ADVINFO_VALUE = 1 and    
b.AE_STDATE between ''~2''and ''~3''),0) UNEXP_COUNT,
nvl((Select count(PK_ADVEVE)    
from ESCH.EV_ADV_SITE_INFO b    
where st.pk_site = b.pk_site and b.fk_study = std.pk_study and    
CODELST_SUBTYP = ''adv_violation''and    
ADVINFO_TYPE   = ''A'' and    
ADVINFO_VALUE = 1 and    
b.AE_STDATE between ''~2''and ''~3''),0) VIO_COUNT,
nvl((Select count(PK_ADVEVE)    
from ESCH.EV_ADV_SITE_INFO b    
where st.pk_site = b.pk_site and b.fk_study = std.pk_study and    
CODELST_SUBTYP = ''adv_dopped''and    
ADVINFO_TYPE   = ''A'' and    
ADVINFO_VALUE = 1 and    
b.AE_STDATE between ''~2''and ''~3'' ),0) DROP_COUNT,
nvl((Select count(PK_ADVEVE) from ESCH.EV_ADV_TYPE b     
Where st.pk_site = b.pk_site and b.fk_study = std.pk_study and     
b.REL_TYPE = ''ad_def'' and     
AE_STDATE between ''~2'' and ''~3'' ),0) R_DEF_COUNT,
nvl((Select count(PK_ADVEVE) from ESCH.EV_ADV_TYPE b     
Where st.pk_site = b.pk_site and b.fk_study = std.pk_study and     
b.REL_TYPE = ''ad_nr'' and     
AE_STDATE between ''~2''and ''~3'' ),0) R_NR_COUNT,
nvl((Select count(PK_ADVEVE) from ESCH.EV_ADV_TYPE b     
Where st.pk_site = b.pk_site and b.fk_study = std.pk_study and     
b.REL_TYPE = ''ad_pos'' and     
AE_STDATE between ''~2'' and ''~3'' ),0)
R_POS_COUNT,
nvl((Select count(PK_ADVEVE) from ESCH.EV_ADV_TYPE b     
Where st.pk_site = b.pk_site and b.fk_study = std.pk_study and     
b.REL_TYPE = ''ad_prob'' and     
AE_STDATE between ''~2''and ''~3'' ),0)
R_PROB_COUNT,
nvl((Select count(PK_ADVEVE) from ESCH.EV_ADV_TYPE b     
Where st.pk_site = b.pk_site and b.fk_study = std.pk_study and     
b.REL_TYPE = ''ad_unknown'' and     
AE_STDATE between ''~2''and ''~3''),0) R_UNK_COUNT,
nvl((Select count(PK_ADVEVE) from ESCH.EV_ADV_TYPE b     
Where st.pk_site = b.pk_site and b.fk_study = std.pk_study and     
b.REL_TYPE = ''ad_unlike'' and     
AE_STDATE between ''~2''and ''~3'' ),0) R_UNL_COUNT
from er_site st, er_study std
where pk_study = ~1 and
pk_site in (Select distinct er_user.FK_SITEID
from er_studyteam team, er_user
Where team.fk_study = ~1 and
er_user.pk_user = team.fk_user and
nvl(study_team_usr_type,''D'') = ''D'' ) '
WHERE PK_REPORT = 81;
COMMIT;



UPDATE ER_REPORT
SET REP_SQL =
  'select
a.study_number,
a.study_actualdt,
a.study_title,
a.patient_id,
a.site_name,
a.pe,
a.protocol,
a.ps,
a.patient_status,
DECODE(trim(a.subtyp),''offstudy'',1,0) as offstudy,
DECODE(trim(a.subtyp),''offtreat'',1,0) as offtreat,
DECODE(trim(a.subtyp),''active'',1,0) as active,
DECODE(trim(a.subtyp),''followup'',1,0) as follow
from
(Select   er_study.study_number,
to_char(er_study.study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT) study_actualdt ,     
er_study.study_title,       
PATPROT_PATSTDID patient_id,        
er_site.site_name, 
to_char(er_patprot.PATPROT_ENROLDT,PKG_DATEUTIL.F_GET_DATEFORMAT) pe,    
(select name         from ESCH.event_assoc    
where EVENT_ID = er_patprot.fk_protocol) protocol,
to_char(er_patprot.patprot_start,PKG_DATEUTIL.F_GET_DATEFORMAT) ps, 
( select codelst_desc      from er_codelst    
where pk_codelst = (select FK_CODELST_STAT                  
from er_patstudystat                      
where fk_per = er_patprot.fk_per                          
and fk_study = er_study.pk_study            
and PATSTUDYSTAT_ENDT is null     )   ) patient_status, 
( select codelst_subtyp   from er_codelst    
where pk_codelst = (select FK_CODELST_STAT                  
from er_patstudystat                      
where fk_per = er_patprot.fk_per                          
and fk_study = er_study.pk_study            
and PATSTUDYSTAT_ENDT is null)) subtyp 
from    er_patprot,        er_study,   
er_per, er_site 
where   er_patprot.PATPROT_STAT = 1
and     er_patprot.fk_study = ~1
and     er_per.PK_PER = er_patprot.fk_per
and     er_per.fk_site = ~4
and       er_site.pk_site = er_per.fk_site
and     er_study.pk_study = er_patprot.fk_study
and     er_patprot.PATPROT_ENROLDT >= ''~2''
and     er_patprot.PATPROT_ENROLDT <= ''~3'' ) a '
WHERE PK_REPORT = 83;
COMMIT;

UPDATE ER_REPORT
SET REP_SQL_CLOB =
  'select
a.study_number,
a.study_actualdt,
a.study_title,
a.patient_id,
a.site_name,
a.pe,
a.protocol,
a.ps,
a.patient_status,
DECODE(trim(a.subtyp),''offstudy'',1,0) as offstudy,
DECODE(trim(a.subtyp),''offtreat'',1,0) as offtreat,
DECODE(trim(a.subtyp),''active'',1,0) as active,
DECODE(trim(a.subtyp),''followup'',1,0) as follow
from
(Select   er_study.study_number,
to_char(er_study.study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT) study_actualdt ,     
er_study.study_title,       
PATPROT_PATSTDID patient_id,        
er_site.site_name, 
to_char(er_patprot.PATPROT_ENROLDT,PKG_DATEUTIL.F_GET_DATEFORMAT) pe,    
(select name         from ESCH.event_assoc    
where EVENT_ID = er_patprot.fk_protocol) protocol,
to_char(er_patprot.patprot_start,PKG_DATEUTIL.F_GET_DATEFORMAT) ps, 
( select codelst_desc      from er_codelst    
where pk_codelst = (select FK_CODELST_STAT                  
from er_patstudystat                      
where fk_per = er_patprot.fk_per                          
and fk_study = er_study.pk_study            
and PATSTUDYSTAT_ENDT is null     )   ) patient_status, 
( select codelst_subtyp   from er_codelst    
where pk_codelst = (select FK_CODELST_STAT                  
from er_patstudystat                      
where fk_per = er_patprot.fk_per                          
and fk_study = er_study.pk_study            
and PATSTUDYSTAT_ENDT is null)) subtyp,            
patstat.fk_codelst_stat             
from    er_patprot,        er_study,   
er_per, er_site,er_patstudystat patstat 
where   er_patprot.PATPROT_STAT = 1
and     er_patprot.fk_study in (:studyId)
and     er_per.PK_PER = er_patprot.fk_per
AND    patstat.fk_per=ER_PATPROT.fk_per AND patstat.fk_study=ER_PATPROT.fk_study AND patstudystat_endt IS NULL
and     er_per.fk_site in (:orgId)
and       er_site.pk_site = er_per.fk_site
and     er_study.pk_study = er_patprot.fk_study
and fk_codelst_stat in (:patStatusId)
and     er_patprot.PATPROT_ENROLDT >= TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
and     er_patprot.PATPROT_ENROLDT <= TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) ) a '
WHERE PK_REPORT = 83;
COMMIT;

UPDATE ER_REPORT
SET REP_SQL =
  'select       
a.STUDY_NUMBER           ,       
a.STUDY_TITLE            ,       
a.STUDY_ACTUALDT,        
a.PROTOCOLID   ,       
a.PROTOCOL_NAME          ,       
a.PAT_STUDYID            ,       
a.PERSON_NAME            ,       
a.EVENT_NAME             ,    
a.ENROLDT ,       
a.MONTH                  ,    
a.st    ,       
a.EVENT_STATUS,       
a.FUZZY_PERIOD,     
a.site_name,    
a.DONE_DATE,    
a.before_date ,    
a.after_date ,    
case when  a.done_date is not null and a.done_date >= a.before_date and a.done_date  <= a.after_date then ''NO''  
when a.done_date is not null and a.done_date < a.before_date or a.done_date  > a.after_date then ''YES'' end as out_ofrange    
from    
(select      
STUDY_NUMBER           ,      
STUDY_TITLE            ,     
to_char(STUDY_ACTUALDT,PKG_DATEUTIL.F_GET_DATEFORMAT) STUDY_ACTUALDT,       
PROTOCOLID   ,      
PROTOCOL_NAME          ,      
PAT_STUDYID            ,      
PERSON_NAME            ,      
EVENT_NAME             ,      
to_char(PATPROT_ENROLDT,PKG_DATEUTIL.F_GET_DATEFORMAT) ENROLDT ,      
MONTH                  ,      
to_char(EVENT_SCHDATE,PKG_DATEUTIL.F_GET_DATEFORMAT)    st     ,      
EVENT_STATUS,      
FUZZY_PERIOD,    
er_site.site_name,    
(select to_char(min(nvl(a.eventstat_dt,sysdate)),PKG_DATEUTIL.F_GET_DATEFORMAT)  from ESCH.sch_eventstat a        
where a.fk_event=erv_patsch.event_id        
and eventstat=(select pk_codelst from ESCH.sch_codelst                                
where codelst_type=''eventstatus''                                
and codelst_subtyp = ''ev_done''))  DONE_DATE ,    
substr(f_check_outofrange(EVENT_SCHDATE,to_number(FUZZY_PERIOD))  ,1,  (INSTR(f_check_outofrange(EVENT_SCHDATE,to_number(FUZZY_PERIOD))  ,'','',1)-1)) after_date,    
substr(f_check_outofrange(EVENT_SCHDATE,to_number(FUZZY_PERIOD))  ,(INSTR(f_check_outofrange(EVENT_SCHDATE,to_number(FUZZY_PERIOD))  ,'','',1)+1),length(f_check_outofrange(EVENT_SCHDATE,to_number(FUZZY_PERIOD)))) before_date      
from   erv_patsch, er_site     
where  fk_study = ~1  and            
fk_site = ~2 and        
fk_site = pk_site and            
EVENT_SCHDATE between ''~3'' and ''~4'' ) a'
WHERE PK_REPORT = 84;
COMMIT;

UPDATE ER_REPORT
SET REP_SQL =
  'SELECT
study_number,study_title,TO_CHAR(study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT) AS study_actualdt,(SELECT site_name FROM ER_SITE WHERE pk_site = fk_site) AS site_name,
(SELECT patprot_patstdid FROM ER_PATPROT WHERE fk_per = pk_per AND fk_study = pk_study AND patprot_stat=1) AS pat_studyid,
(SELECT codelst_desc FROM ESCH.sch_codelst WHERE pk_codelst = fk_codlst_aetype) AS TYPE,
ae_grade, ae_name ,
TO_CHAR(ae_stdate,PKG_DATEUTIL.F_GET_DATEFORMAT) AS ae_stdate,TO_CHAR(ae_enddate,PKG_DATEUTIL.F_GET_DATEFORMAT) AS ae_enddate,
(SELECT codelst_desc FROM ESCH.SCH_CODELST WHERE pk_codelst = ae_relationship) AS ae_rel,
DECODE(SUBSTR(ae_addinfo,(SELECT codelst_seq FROM ESCH.sch_codelst WHERE codelst_type = ''adve_info'' AND codelst_subtyp = ''adv_dopped''),1),''1'',''Yes'',''NO'') AS dropped,
DECODE(SUBSTR(ae_addinfo,(SELECT codelst_seq FROM ESCH.sch_codelst WHERE codelst_type = ''adve_info'' AND codelst_subtyp = ''adv_unexp''),1),''1'',''Yes'',''NO'') AS unexpected,
DECODE(SUBSTR(ae_addinfo,(SELECT codelst_seq FROM ESCH.sch_codelst WHERE codelst_type = ''adve_info'' AND codelst_subtyp = ''adv_violation''),1),''1'',''Yes'',''NO'') AS violation,
DECODE(TO_NUMBER(ae_outtype),0,'''',f_getae_outcome(pk_adveve,TO_CHAR(ae_outdate,PKG_DATEUTIL.F_GET_DATEFORMAT))) AS outcome
FROM ESCH.sch_adverseve, ER_STUDY, ER_PER
WHERE pk_study = fk_study AND
pk_per = fk_per AND
fk_study = ~2 AND
fk_site = ~1 AND
AE_STDATE BETWEEN ''~3''AND ''~4''
ORDER BY pat_studyid,ae_stdate '
WHERE PK_REPORT = 85;
COMMIT;

UPDATE ER_REPORT
SET REP_SQL_CLOB =
  'SELECT
study_number,study_title,TO_CHAR(study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT) AS study_actualdt,
site_name,
(SELECT patprot_patstdid FROM ER_PATPROT WHERE fk_per = pk_per AND fk_study = pk_study AND patprot_stat=1) AS pat_studyid,
(SELECT codelst_desc FROM ESCH.sch_codelst WHERE pk_codelst = fk_codlst_aetype) AS TYPE,
ae_grade, ae_name ,
TO_CHAR(ae_stdate,PKG_DATEUTIL.F_GET_DATEFORMAT) AS ae_stdate,TO_CHAR(ae_enddate,PKG_DATEUTIL.F_GET_DATEFORMAT) AS ae_enddate,
(SELECT codelst_desc FROM ESCH.SCH_CODELST WHERE pk_codelst = ae_relationship) AS ae_rel,
DECODE(SUBSTR(ae_addinfo,(SELECT codelst_seq FROM ESCH.sch_codelst WHERE codelst_type = ''adve_info'' AND codelst_subtyp = ''adv_dopped''),1),''1'',''Yes'',''NO'') AS dropped,
DECODE(SUBSTR(ae_addinfo,(SELECT codelst_seq FROM ESCH.sch_codelst WHERE codelst_type = ''adve_info'' AND codelst_subtyp = ''adv_unexp''),1),''1'',''Yes'',''NO'') AS unexpected,
DECODE(SUBSTR(ae_addinfo,(SELECT codelst_seq FROM ESCH.sch_codelst WHERE codelst_type = ''adve_info'' AND codelst_subtyp = ''adv_violation''),1),''1'',''Yes'',''NO'') AS violation,
DECODE(TO_NUMBER(ae_outtype),0,'''',f_getae_outcome(pk_adveve,TO_CHAR(ae_outdate,PKG_DATEUTIL.F_GET_DATEFORMAT))) AS outcome
FROM ESCH.sch_adverseve, ER_STUDY, ER_PER, er_site
WHERE pk_study = fk_study AND
pk_site = fk_site AND
pk_per = fk_per AND
fk_study = :studyId AND
fk_site IN (:orgId) AND
AE_STDATE BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
ORDER BY site_name,pat_studyid,ae_stdate '
WHERE PK_REPORT = 85;
COMMIT;

UPDATE ER_REPORT
SET REP_SQL_CLOB =
  'select f.person_code as patientId,
b.CODELST_DESC as advType,
c.STUDY_NUMBER as studyNumber,
c.STUDY_TITLE as studyTitle,
to_char(c. STUDY_ACTUALDT,PKG_DATEUTIL.F_GET_DATEFORMAT) study_start_dt,
g.site_name site_name,
a.AE_DESC as advDesc,
to_char(a.AE_STDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) as advSdate,
to_char(a.AE_OUTDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) as advOutdate,
(select codelst_desc from ESCH.sch_codelst where pk_codelst = a.AE_RELATIONSHIP) as advRelation,
(select distinct patprot_patstdid from er_patprot where fk_study=c.pk_study and fk_per = a.fk_per) as pat_studyid
from ESCH.sch_adverseve a,
ESCH.sch_codelst b,
er_study c,
ESCH.sch_advInfo d,
ESCH.sch_codelst e,
EPAT.person f,
er_site g
where  a.FK_CODLST_AETYPE=b.pk_codelst
and d.FK_CODELST_INFO=e.pk_codelst
and a.FK_PER=f.pk_person
and d.FK_ADVERSE = a.PK_ADVEVE
and a.FK_STUDY = c.pk_study
and e.CODELST_SUBTYP = ''al_death''
and d.ADVINFO_VALUE = 1
and f.FK_SITE in (:orgId)
and c.PK_STUDY in (:studyId)
and a.AE_OUTDATE between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
and g.pk_site in (:orgId)'
WHERE PK_REPORT = 86;
COMMIT;

UPDATE ER_REPORT
SET REP_SQL =
  'select  b.PAT_STUDYID , b.FK_STUDY, b.STUDY_NUMBER,b.SITE_NAME, b.STUDY_TITLE, to_char(b.STUDY_ACTUALDT,PKG_DATEUTIL.F_GET_DATEFORMAT) STUDY_ACTUALDT,   b.PER_CODE,       
ad.PK_ADVEVE, ad.AE_TYPE_DESC,to_char(ad.AE_STDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) AE_STDATE,       
to_char(ad.AE_ENDDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) AE_ENDDATE,  ad.AE_SEVERITY,  ad.AE_DESC,   ad.AE_RELATIONSHIP_DESC,       
ad.AE_BDSYSTEM_AFF,  ad.AE_RECOVERY_DESC, ''O'' REC_TYPE,    ev.info_desc OUTCOME, null ADVNOTIFY_DATE,  ad.AE_OUTNOTES,   
(Select Decode(ADVINFO_VALUE,0,''No'',1,''Yes'') from ESCH.EV_ADV_CHKINFO    
Where fk_adverse = ad.pk_adveve and trim(ADVINFO_TYPE) = ''A'' and trim(codelst_subtyp) = ''adv_unexp'')  unexpected ,   
(Select  Decode(ADVINFO_VALUE,0,''No'',1,''Yes'') from ESCH.EV_ADV_CHKINFO    
Where fk_adverse = ad.pk_adveve and trim(ADVINFO_TYPE) = ''A'' and trim(codelst_subtyp) = ''adv_dopped'') adv_dropped,   
(Select  Decode(ADVINFO_VALUE,0,''No'',1,''Yes'') from ESCH.EV_ADV_CHKINFO    
Where fk_adverse = ad.pk_adveve and trim(ADVINFO_TYPE) = ''A'' and trim(codelst_subtyp) = ''adv_violation'') adv_violation ,   
(select count(*) from ESCH.sch_Adverseve e      
where e.fk_study = ~1  and e.fk_per=~2 and trunc(ad.AE_STDATE) between to_date(''~3'',PKG_DATEUTIL.F_GET_DATEFORMAT)and to_date(''~4'',PKG_DATEUTIL.F_GET_DATEFORMAT)) adv_cnt,
(ad.AE_ADV_GRADE) adv_grade   
from  ESCH.ev_adv_type ad, erv_patstudy b, ESCH.EV_ADV_CHKINFO_CHECKED ev   
where  ad.fk_study = b.fk_study and   ad.fk_per =  b.fk_per and  ad.PK_ADVEVE = ev.fk_adverse(+) and 
ad.fk_study=~1 and   
b.fk_per = ~2 and  trunc(ad.AE_STDATE) between to_date(''~3'',PKG_DATEUTIL.F_GET_DATEFORMAT)and to_date(''~4'',PKG_DATEUTIL.F_GET_DATEFORMAT) 
UNION 
select b.PAT_STUDYID, b.FK_STUDY, b.STUDY_NUMBER, b.SITE_NAME, b.STUDY_TITLE, to_char(b.STUDY_ACTUALDT,PKG_DATEUTIL.F_GET_DATEFORMAT) STUDY_ACTUALDT,       
b.PER_CODE,   ad.PK_ADVEVE, ad.AE_TYPE_DESC,  to_char(ad.AE_STDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) AE_STDATE,       
to_char(ad.AE_ENDDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) AE_ENDDATE, ad.AE_SEVERITY,  ad.AE_DESC,    ad.AE_RELATIONSHIP_DESC,       
ad.AE_BDSYSTEM_AFF,    ad.AE_RECOVERY_DESC,  ''N'' REC_TYPE,  adnot.ADV_NOT_SENT_TO, to_char(adnot.ADVNOTIFY_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) ADVNOTIFY_DATE, ad.AE_OUTNOTES,   
(Select Decode(ADVINFO_VALUE,0,''No'',1,''Yes'') from ESCH.EV_ADV_CHKINFO    
Where fk_adverse = ad.pk_adveve and trim(ADVINFO_TYPE) = ''A'' and trim(codelst_subtyp) = ''adv_unexp'') unexpected ,   
(Select  Decode(ADVINFO_VALUE,0,''No'',1,''Yes'') from ESCH.EV_ADV_CHKINFO    
Where fk_adverse = ad.pk_adveve and trim(ADVINFO_TYPE) = ''A'' and trim(codelst_subtyp) = ''adv_dopped'') adv_dropped,   
(Select  Decode(ADVINFO_VALUE,0,''No'',1,''Yes'') from ESCH.EV_ADV_CHKINFO    
Where fk_adverse = ad.pk_adveve and trim(ADVINFO_TYPE) = ''A'' and trim(codelst_subtyp) = ''adv_violation'') adv_violation , 
(select count(*) from ESCH.sch_Adverseve e      
where e.fk_study = ~1  and e.fk_per=~2      and trunc(e.AE_STDATE) between to_date(''~3'',PKG_DATEUTIL.F_GET_DATEFORMAT)and to_date(''~4'',PKG_DATEUTIL.F_GET_DATEFORMAT)) adv_cnt,
(ad.AE_ADV_GRADE) adv_grade   
from  ESCH.ev_adv_type ad, erv_patstudy b, ERV_SCH_ADVNOTIY_CHECKED adnot   
where ad.fk_study = b.fk_study and ad.fk_per =  b.fk_per and    
ad.pk_adveve = adnot.pk_adveve(+) and     
ad.fk_study=~1 and  b.fk_per = ~2 and   
trunc(ad.AE_STDATE) between to_date(''~3'',PKG_DATEUTIL.F_GET_DATEFORMAT)and to_date(''~4'',PKG_DATEUTIL.F_GET_DATEFORMAT)'
WHERE PK_REPORT = 88;
COMMIT;

UPDATE ER_REPORT
SET REP_SQL_CLOB =
  'select  b.PAT_STUDYID , b.FK_STUDY, b.STUDY_NUMBER,b.SITE_NAME, b.STUDY_TITLE, to_char(b.STUDY_ACTUALDT,PKG_DATEUTIL.F_GET_DATEFORMAT) STUDY_ACTUALDT,   b.PER_CODE,       
ad.PK_ADVEVE, ad.AE_TYPE_DESC,to_char(ad.AE_STDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) AE_STDATE,       
to_char(ad.AE_ENDDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) AE_ENDDATE,  ad.AE_SEVERITY,  ad.AE_DESC,   ad.AE_RELATIONSHIP_DESC,       
ad.AE_BDSYSTEM_AFF,  ad.AE_RECOVERY_DESC, ''O'' REC_TYPE,    ev.info_desc OUTCOME, null ADVNOTIFY_DATE,  ad.AE_OUTNOTES,   
(Select Decode(ADVINFO_VALUE,0,''No'',1,''Yes'') from ESCH.EV_ADV_CHKINFO    
Where fk_adverse = ad.pk_adveve and trim(ADVINFO_TYPE) = ''A'' and trim(codelst_subtyp) = ''adv_unexp'')  unexpected ,   
(Select  Decode(ADVINFO_VALUE,0,''No'',1,''Yes'') from ESCH.EV_ADV_CHKINFO    
Where fk_adverse = ad.pk_adveve and trim(ADVINFO_TYPE) = ''A'' and trim(codelst_subtyp) = ''adv_dopped'') adv_dropped,   
(Select  Decode(ADVINFO_VALUE,0,''No'',1,''Yes'') from ESCH.EV_ADV_CHKINFO    
Where fk_adverse = ad.pk_adveve and trim(ADVINFO_TYPE) = ''A'' and trim(codelst_subtyp) = ''adv_violation'') adv_violation ,   
(select count(*) from ESCH.sch_Adverseve e      
where e.fk_study in (:studyId)  and e.fk_per in (:patientId) and trunc(ad.AE_STDATE) between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)) adv_cnt,
(ad.AE_ADV_GRADE) adv_grade   
from  ESCH.ev_adv_type ad, erv_patstudy b, ESCH.EV_ADV_CHKINFO_CHECKED ev   
where  ad.fk_study = b.fk_study and   ad.fk_per =  b.fk_per and  ad.PK_ADVEVE = ev.fk_adverse(+) and 
ad.fk_study in (:studyId) and   
b.fk_per in (:patientId) and  trunc(ad.AE_STDATE) between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) 
UNION 
select b.PAT_STUDYID, b.FK_STUDY, b.STUDY_NUMBER, b.SITE_NAME, b.STUDY_TITLE, to_char(b.STUDY_ACTUALDT,PKG_DATEUTIL.F_GET_DATEFORMAT) STUDY_ACTUALDT,       
b.PER_CODE,   ad.PK_ADVEVE, ad.AE_TYPE_DESC,  to_char(ad.AE_STDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) AE_STDATE,       
to_char(ad.AE_ENDDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) AE_ENDDATE, ad.AE_SEVERITY,  ad.AE_DESC,    ad.AE_RELATIONSHIP_DESC,       
ad.AE_BDSYSTEM_AFF,    ad.AE_RECOVERY_DESC,  ''N'' REC_TYPE,  adnot.ADV_NOT_SENT_TO, to_char(adnot.ADVNOTIFY_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) ADVNOTIFY_DATE, ad.AE_OUTNOTES,   
(Select Decode(ADVINFO_VALUE,0,''No'',1,''Yes'') from ESCH.EV_ADV_CHKINFO    
Where fk_adverse = ad.pk_adveve and trim(ADVINFO_TYPE) = ''A'' and trim(codelst_subtyp) = ''adv_unexp'') unexpected ,   
(Select  Decode(ADVINFO_VALUE,0,''No'',1,''Yes'') from ESCH.EV_ADV_CHKINFO    
Where fk_adverse = ad.pk_adveve and trim(ADVINFO_TYPE) = ''A'' and trim(codelst_subtyp) = ''adv_dopped'') adv_dropped,   
(Select  Decode(ADVINFO_VALUE,0,''No'',1,''Yes'') from ESCH.EV_ADV_CHKINFO    
Where fk_adverse = ad.pk_adveve and trim(ADVINFO_TYPE) = ''A'' and trim(codelst_subtyp) = ''adv_violation'') adv_violation , 
(select count(*) from ESCH.sch_Adverseve e      
where e.fk_study in (:studyId)  and e.fk_per in (:patientId)      and trunc(e.AE_STDATE) between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)) adv_cnt,
(ad.AE_ADV_GRADE) adv_grade   
from ESCH. ev_adv_type ad, erv_patstudy b, ERV_SCH_ADVNOTIY_CHECKED adnot   
where ad.fk_study = b.fk_study and ad.fk_per =  b.fk_per and    
ad.pk_adveve = adnot.pk_adveve(+) and     
ad.fk_study in (:studyId) and  b.fk_per in (:patientId) and   
trunc(ad.AE_STDATE) between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)'
WHERE PK_REPORT = 88;
COMMIT;

UPDATE ER_REPORT
SET REP_SQL =
  'select  protocolid,        
PERSON_CODE , PAT_STUDYID, FK_PER , STUDY_NUMBER, SITE_NAME,        
STUDY_TITLE ,to_char(study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT) study_actualdt  ,        
PROTOCOL_NAME, PATPROT_ENROLDT,        
to_char(PATPROT_START,PKG_DATEUTIL.F_GET_DATEFORMAT) PATPROT_START,        
EVENT_NAME,MONTH,        
to_char(EVENT_SCHDATE,''YYYY'') Year ,        
to_char(EVENT_SCHDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) START_DATE_TIME,        
EVENT_STATUS ,  to_char(EVENT_EXEON,PKG_DATEUTIL.F_GET_DATEFORMAT) EVENT_EXEON ,        
( select        max(codelst_desc)                
from    ESCH.SCH_EVENTCOST A,                
ESCH.SCH_CODELST B                
where   A.FK_CURRENCY = B.PK_CODELST                
and     A.fk_event = fk_assoc        
) cost_currency,        
to_char(ESCH.tot_cost(fk_assoc),''9999999999.99'') evecost,        
trim(to_char(EVENTCOST_VALUE,0999999999.99)) supply_cost,        
b.codelst_desc supply_desc ,        
FK_PATPROT
from    erv_patsch,        
ESCH.sch_eventcost ,        
ESCH.sch_codelst a,        
ESCH.sch_codelst b
where protocolid = ~1
and EVENT_SCHDATE between ''~2'' and ''~3''
and     event_status = ''Done''
and     fk_assoc = sch_eventcost.fk_event
and     FK_CURRENCY = a.pk_codelst
and     FK_COST_DESC = b.pk_codelst
and     FK_SITE in (~4)
Order by supply_desc, person_code,   EVENT_SCHDATE'
WHERE PK_REPORT = 89;
COMMIT;

UPDATE ER_REPORT
SET REP_SQL_CLOB =
  'select  protocolid,        
PERSON_CODE , PAT_STUDYID, FK_PER , STUDY_NUMBER, SITE_NAME,        
STUDY_TITLE ,to_char(study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT) study_actualdt  ,        
PROTOCOL_NAME, PATPROT_ENROLDT,        
to_char(PATPROT_START,PKG_DATEUTIL.F_GET_DATEFORMAT) PATPROT_START,        
EVENT_NAME,MONTH,        
to_char(EVENT_SCHDATE,''YYYY'') Year ,        
to_char(EVENT_SCHDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) START_DATE_TIME,        
EVENT_STATUS ,  to_char(EVENT_EXEON,PKG_DATEUTIL.F_GET_DATEFORMAT) EVENT_EXEON ,        
( select        max(codelst_desc)                
from    ESCH.SCH_EVENTCOST A, ESCH.SCH_CODELST B                
where   A.FK_CURRENCY = B.PK_CODELST                
and     A.fk_event = fk_assoc        
) cost_currency,        
to_char(ESCH.tot_cost(fk_assoc),''9999999999.99'') evecost,        
trim(to_char(EVENTCOST_VALUE,0999999999.99)) supply_cost,        
b.codelst_desc supply_desc ,        
FK_PATPROT
from    erv_patsch,  ESCH.sch_eventcost , ESCH.sch_codelst a,  ESCH.sch_codelst b
where protocolid in (:protCalId)
and EVENT_SCHDATE between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) 
and event_status_subtype = ''ev_done''
and     fk_assoc = sch_eventcost.fk_event
and     FK_CURRENCY = a.pk_codelst
and     FK_COST_DESC = b.pk_codelst
and     FK_SITE in (:orgId)  
and fk_study in (:studyId)
Order by supply_desc, person_code,   EVENT_SCHDATE'
WHERE PK_REPORT = 89;
COMMIT;

UPDATE ER_REPORT
SET REP_SQL =
  'select     protocolid,    
PERSON_CODE , PAT_STUDYID , FK_PER , STUDY_NUMBER, SITE_NAME,    
STUDY_TITLE ,to_char(study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT) study_actualdt  ,    
PROTOCOL_NAME, PATPROT_ENROLDT,    
to_char(PATPROT_START,PKG_DATEUTIL.F_GET_DATEFORMAT) PATPROT_START,        
EVENT_NAME,MONTH,    
to_char(EVENT_SCHDATE,''YYYY'') Year ,    
to_char(EVENT_SCHDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) START_DATE_TIME,    
EVENT_STATUS ,  to_char(EVENT_EXEON,PKG_DATEUTIL.F_GET_DATEFORMAT) EVENT_EXEON ,         
( select     max(codelst_desc)             
from     ESCH.SCH_EVENTCOST A,        
ESCH.SCH_CODELST B            
where      A.FK_CURRENCY = B.PK_CODELST              
and     A.fk_event = fk_assoc         
) cost_currency,        
to_char(ESCH.tot_cost(fk_assoc),''9999999999.99'') evecost,    
trim(to_char(EVENTCOST_VALUE,0999999999.99)) supply_cost,    
b.codelst_desc supply_desc ,    
FK_PATPROT
from     erv_patsch,    
ESCH.sch_eventcost ,    
ESCH.sch_codelst a,    
ESCH.sch_codelst b
where protocolid = ~1
and EVENT_SCHDATE between ''~2'' and ''~3''
and     event_status = ''Done''
and     fk_assoc = sch_eventcost.fk_event
and     FK_CURRENCY = a.pk_codelst
and     FK_COST_DESC = b.pk_codelst
and     FK_SITE in (~4)
Order by supply_desc, person_code,   EVENT_SCHDATE'
WHERE PK_REPORT = 90;
COMMIT;

UPDATE ER_REPORT
SET REP_SQL_CLOB =
  'select     protocolid,    
PERSON_CODE , PAT_STUDYID , FK_PER , STUDY_NUMBER, SITE_NAME,    
STUDY_TITLE ,to_char(study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT) study_actualdt  ,    
PROTOCOL_NAME, PATPROT_ENROLDT,    
to_char(PATPROT_START,PKG_DATEUTIL.F_GET_DATEFORMAT) PATPROT_START,        
EVENT_NAME,MONTH,    
to_char(EVENT_SCHDATE,''YYYY'') Year ,    
to_char(EVENT_SCHDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) START_DATE_TIME,    
EVENT_STATUS ,  to_char(EVENT_EXEON,PKG_DATEUTIL.F_GET_DATEFORMAT) EVENT_EXEON ,         
( select     max(codelst_desc)             
from     ESCH.SCH_EVENTCOST A,        
ESCH.SCH_CODELST B            
where      A.FK_CURRENCY = B.PK_CODELST              
and     A.fk_event = fk_assoc         
) cost_currency,        
to_char(ESCH.tot_cost(fk_assoc),''9999999999.99'') evecost,    
trim(to_char(EVENTCOST_VALUE,0999999999.99)) supply_cost,    
b.codelst_desc supply_desc ,    
FK_PATPROT
from     erv_patsch,    
ESCH.sch_eventcost ,    
ESCH.sch_codelst a,    
ESCH.sch_codelst b
where protocolid in (:protCalId)
and EVENT_SCHDATE between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
and     event_status = ''Done''
and     fk_assoc = sch_eventcost.fk_event
and     FK_CURRENCY = a.pk_codelst
and     FK_COST_DESC = b.pk_codelst
and     FK_SITE in (:orgId)
Order by supply_desc, person_code,   EVENT_SCHDATE'
WHERE PK_REPORT = 90;
COMMIT;

UPDATE ER_REPORT
SET REP_SQL =
  'SELECT TO_CHAR(PATFORMS_FILLDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) AS PATFORMS_FILLDATE,trim(TO_CHAR(PATFORMS_FILLDATE,''MONTH'')) || '' '' || TO_CHAR(PATFORMS_FILLDATE,''yyyy'') AS PATFORMS_FILLDATE_MONTH, form_name, description, study_number, study_title, per_code, patprot_patstdid, TO_CHAR(patprot_enroldt,PKG_DATEUTIL.F_GET_DATEFORMAT) AS patprot_enroldt FROM (
SELECT PATFORMS_FILLDATE,
(SELECT form_name FROM ER_FORMLIB WHERE pK_FORMLIB = a.FK_FORMLIB) form_name,
F_Getformbrowsedata(fk_formlib, pk_patforms) AS Description,
a.FK_PER,
FK_PATPROT, study_number, study_title, per_code, patprot_patstdid, patprot_enroldt
FROM ER_PATFORMS a, ER_PATPROT, ER_STUDY, ER_PER
WHERE pk_patprot = fk_patprot AND
fk_study = ~2 AND
a.fk_per = ~1 AND
PATFORMS_FILLDATE between to_date(''~3'',PKG_DATEUTIL.F_GET_DATEFORMAT)and to_date(''~4'',PKG_DATEUTIL.F_GET_DATEFORMAT) and
pk_study = fk_study AND
a.fk_per = pk_per AND
NVL(a.record_type,''N'') <> ''D''
UNION
SELECT test_date,''Lab'', (SELECT labtest_name FROM ER_LABTEST WHERE pk_labtest = fk_testid) || ''  :  '' ||  test_result || '' '' || test_unit ,
a.fk_per, fk_patprot,study_number, study_title, per_code, NULL AS patprot_patstdid, NULL AS patprot_enroldt
FROM ER_PATLABS a, ER_STUDY, ER_PER
WHERE a.fk_per = ~1 AND
a.fk_study = ~2
AND test_date between to_date(''~3'',PKG_DATEUTIL.F_GET_DATEFORMAT)and to_date(''~4'',PKG_DATEUTIL.F_GET_DATEFORMAT)
AND pk_study = a.fk_study AND
a.fk_per = pk_per
UNION
SELECT ae_stdate, ''Adverse Event'', ''Adverse Event : '' || AE_NAME || DECODE(fk_codlst_aetype,(SELECT pk_codelst FROM ESCH.sch_codelst WHERE codelst_type = ''adve_type'' AND codelst_subtyp=''al_sadve''),''X@*@X'','''') || ''X@@@XGrade : '' || AE_GRADE || ''X@@@XStop DATE : '' ||  TO_CHAR(AE_ENDDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) || ''X@@@XRelationship TO THE study : '' || (SELECT codelst_desc FROM ESCH.sch_codelst WHERE pk_codelst=ae_relationship) || ''X@@@XOutcome TYPE : '' || DECODE(SUBSTR(AE_OUTTYPE,1,1),''1'',''[ Death '' || to_char(ae_outdate,PKG_DATEUTIL.F_GET_DATEFORMAT)  || '' ] '') || DECODE(SUBSTR(AE_OUTTYPE,2,1),''1'',''[ Life-threatening ] '') || DECODE(SUBSTR(AE_OUTTYPE,3,1),''1'',''[ Hospitalization ] '')  || DECODE(SUBSTR(AE_OUTTYPE,4,1),''1'',''[ Disability ] '') || DECODE(SUBSTR(AE_OUTTYPE,5,1),''1'',''[ Congenital Anomaly ] '') || DECODE(SUBSTR(AE_OUTTYPE,6,1),''1'',''[ Required Intervention ] '') || DECODE(SUBSTR(AE_OUTTYPE,7,1),''1'',''[ Recovered ]''),
pk_per, pk_patprot, study_number, study_title, per_code, patprot_patstdid, patprot_enroldt
FROM ESCH.sch_adverseve a, ER_PER b,ER_STUDY c, ER_PATPROT d
WHERE a.fk_study = c.pk_study AND
a.fk_per = b.pk_per AND
d.patprot_stat = 1 AND
d.PATPROT_ENROLDT IS NOT NULL AND
a.fk_study = d.fk_study AND
a.fk_per = d.fk_per AND
a.fk_per = ~1 AND
a.fk_study = ~2 AND
ae_stdate between to_date(''~3'',PKG_DATEUTIL.F_GET_DATEFORMAT)and to_date(''~4'',PKG_DATEUTIL.F_GET_DATEFORMAT)
ORDER BY 1)'
WHERE PK_REPORT = 93;
COMMIT;

UPDATE ER_REPORT
SET REP_SQL_CLOB =
  'SELECT TO_CHAR(PATFORMS_FILLDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) AS PATFORMS_FILLDATE,trim(TO_CHAR(PATFORMS_FILLDATE,''MONTH'')) || '' '' || TO_CHAR(PATFORMS_FILLDATE,''yyyy'') AS PATFORMS_FILLDATE_MONTH, form_name, description, study_number, study_title, per_code, patprot_patstdid, TO_CHAR(patprot_enroldt,PKG_DATEUTIL.F_GET_DATEFORMAT) AS patprot_enroldt FROM (
SELECT PATFORMS_FILLDATE,
(SELECT form_name FROM ER_FORMLIB WHERE pK_FORMLIB = a.FK_FORMLIB) form_name,
F_Getformbrowsedata(fk_formlib, pk_patforms) AS Description,
a.FK_PER,
FK_PATPROT, study_number, study_title, per_code, patprot_patstdid, patprot_enroldt
FROM ER_PATFORMS a, ER_PATPROT, ER_STUDY, ER_PER
WHERE pk_patprot = fk_patprot AND
fk_study in (:studyId) AND
a.fk_per in (:patientId)  AND
PATFORMS_FILLDATE BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND
pk_study = fk_study AND
a.fk_per = pk_per AND
NVL(a.record_type,''N'') <> ''D''
UNION
SELECT test_date,''Lab'', (SELECT labtest_name FROM ER_LABTEST WHERE pk_labtest = fk_testid) || ''  :  '' ||  test_result || '' '' || test_unit ,
a.fk_per, fk_patprot,study_number, study_title, per_code, NULL AS patprot_patstdid, NULL AS patprot_enroldt
FROM ER_PATLABS a, ER_STUDY, ER_PER
WHERE a.fk_per in (:patientId) AND
a.fk_study in (:studyId)
AND test_date BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
AND pk_study = a.fk_study AND
a.fk_per = pk_per
UNION
SELECT ae_stdate, ''Adverse Event'', ''Adverse Event : '' || AE_NAME || DECODE(fk_codlst_aetype,(SELECT pk_codelst FROM ESCH.sch_codelst WHERE codelst_type = ''adve_type'' AND codelst_subtyp=''al_sadve''),''X@*@X'','') || ''X@@@XGrade : '' || AE_GRADE || ''X@@@XStop DATE : '' ||  TO_CHAR(AE_ENDDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) || ''X@@@XRelationship TO THE study : '' || (SELECT codelst_desc FROM ESCH.sch_codelst WHERE pk_codelst=ae_relationship) || ''X@@@XOutcome TYPE : '' || DECODE(SUBSTR(AE_OUTTYPE,1,1),''1'',''[ Death '' || to_char(ae_outdate,PKG_DATEUTIL.F_GET_DATEFORMAT) || '' ] '') || DECODE(SUBSTR(AE_OUTTYPE,2,1),''1'',''[ Life-threatening ] '') || DECODE(SUBSTR(AE_OUTTYPE,3,1),''1'',''[ Hospitalization ] '')  || DECODE(SUBSTR(AE_OUTTYPE,4,1),''1'',''[ Disability ] '') || DECODE(SUBSTR(AE_OUTTYPE,5,1),''1'',''[ Congenital Anomaly ] '') || DECODE(SUBSTR(AE_OUTTYPE,6,1),''1'',''[ Required Intervention ] '') || DECODE(SUBSTR(AE_OUTTYPE,7,1),''1'',''[ Recovered ]''),
pk_per, pk_patprot, study_number, study_title, per_code, patprot_patstdid, patprot_enroldt
FROM ESCH.sch_adverseve a, ER_PER b,ER_STUDY c, ER_PATPROT d
WHERE a.fk_study = c.pk_study AND
a.fk_per = b.pk_per AND
d.patprot_stat = 1 AND
d.PATPROT_ENROLDT IS NOT NULL AND
a.fk_study = d.fk_study AND
a.fk_per = d.fk_per AND
a.fk_per in (:patientId) AND
a.fk_study in (:studyId)  AND
ae_stdate BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
ORDER BY 1)'
WHERE PK_REPORT = 93;
COMMIT;

UPDATE ER_REPORT
SET REP_SQL =
  'SELECT (SELECT study_number FROM ER_STUDY WHERE pk_study = ~4) AS study_number,                        
(SELECT study_title FROM ER_STUDY WHERE pk_study = ~4) AS study_title,                  
(SELECT  per_code FROM ER_PER WHERE pk_per = ~1) AS per_code,                  
(SELECT  DISTINCT patprot_patstdid FROM ER_PATPROT WHERE fk_per = ~1 AND fk_study = ~4) AS patprot_patstdid,                  
(SELECT  DISTINCT  TO_CHAR(patprot_enroldt,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATPROT WHERE fk_per = ~1 AND fk_study = ~4) AS  patprot_enroldt,
TO_CHAR(test_date,PKG_DATEUTIL.F_GET_DATEFORMAT) AS test_date, psa, testosterone, adverse_event, treatment FROM  (
SELECT test_date, F_Getlabresult(''psa'',test_date,~1, ~4) AS PSA, F_Getlabresult(''TEST'',test_date,~1, ~4) AS TESTOSTERONE, F_Getae(test_date,~1, ~4) AS ADVERSE_EVENT,
F_Getformdatabykeyword(test_date, ~1, ~4, ''TREATMENT'') AS Treatment
FROM (
SELECT test_date
FROM ER_PATLABS, ER_LABTEST
WHERE
pk_labtest = fk_testid AND
LOWER(labtest_shortname) IN (''psa'',''TEST'') AND
test_date BETWEEN ''~2'' AND ''~3'' AND
fk_per = ~1 AND
fk_study = ~4
UNION
SELECT ae_stdate
FROM ESCH.sch_adverseve
WHERE
fk_per = ~1 AND
fk_study = ~4 AND
ae_stdate BETWEEN ''~2'' AND ''~3''
UNION
SELECT patforms_filldate
FROM ER_PATFORMS a, ER_PATPROT, ER_FORMLIB
WHERE patforms_filldate BETWEEN ''~2'' AND ''~3'' AND
a.fk_per = ~1 AND
pk_patprot = fk_patprot AND
fk_study = ~4 AND
pk_formlib = fk_formlib AND
form_keyword =''TREATMENT''
)
)'
WHERE PK_REPORT = 96;
COMMIT;

UPDATE ER_REPORT
SET REP_SQL_CLOB =
  'SELECT (SELECT study_number FROM ER_STUDY WHERE pk_study = ~4) AS study_number,                        
(SELECT study_title FROM ER_STUDY WHERE pk_study = ~4) AS study_title,                  
(SELECT  per_code FROM ER_PER WHERE pk_per = ~1) AS per_code,                  
(SELECT  DISTINCT patprot_patstdid FROM ER_PATPROT WHERE fk_per = ~1 AND fk_study = ~4) AS patprot_patstdid,                  
(SELECT  DISTINCT  TO_CHAR(patprot_enroldt,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATPROT WHERE fk_per = ~1 AND fk_study = ~4) AS  patprot_enroldt,
TO_CHAR(test_date,PKG_DATEUTIL.F_GET_DATEFORMAT) AS test_date, psa, testosterone, adverse_event, treatment FROM  (
SELECT test_date, F_Getlabresult(''psa'',test_date,~1, ~4) AS PSA, F_Getlabresult(''TEST'',test_date,~1, ~4) AS TESTOSTERONE, F_Getae(test_date,~1, ~4) AS ADVERSE_EVENT,
F_Getformdatabykeyword(test_date, ~1, ~4, ''TREATMENT'') AS Treatment
FROM (
SELECT test_date
FROM ER_PATLABS, ER_LABTEST
WHERE
pk_labtest = fk_testid AND
LOWER(labtest_shortname) IN (''psa'',''TEST'') AND
test_date BETWEEN ''~2'' AND ''~3'' AND
fk_per = ~1 AND
fk_study = ~4
UNION
SELECT ae_stdate
FROM ESCH.sch_adverseve
WHERE
fk_per = ~1 AND
fk_study = ~4 AND
ae_stdate BETWEEN ''~2'' AND ''~3''
UNION
SELECT patforms_filldate
FROM ER_PATFORMS a, ER_PATPROT, ER_FORMLIB
WHERE patforms_filldate BETWEEN ''~2'' AND ''~3'' AND
a.fk_per = ~1 AND
pk_patprot = fk_patprot AND
fk_study = ~4 AND
pk_formlib = fk_formlib AND
form_keyword =''TREATMENT''
)
)'
WHERE PK_REPORT = 96;
COMMIT;

UPDATE ER_REPORT
SET REP_SQL =
  'SELECT study_number AS study_number,
person_code AS patient_id,
patprot_patstdid AS patient_study_id,
person_lname || '', '' || person_fname AS patient_name,
(SELECT codelst_desc FROM er_codelst WHERE pk_codelst = fk_codelst_race) AS race,
(SELECT codelst_desc FROM er_codelst WHERE pk_codelst = fk_codelst_ethnicity) AS ethnicity,
TO_CHAR(patprot_enroldt,PKG_DATEUTIL.F_GET_DATEFORMAT) AS enrolled_on,
(SELECT TO_CHAR(MAX(patstudystat_date),PKG_DATEUTIL.F_GET_DATEFORMAT) FROM er_patstudystat WHERE fk_per = a.fk_per AND fk_study = a.FK_STUDY AND fk_codelst_stat = (SELECT pk_codelst FROM er_codelst WHERE codelst_type =''patStatus'' AND codelst_subtyp = ''active'')) AS active_on,
(SELECT TO_CHAR(MAX(patstudystat_date),PKG_DATEUTIL.F_GET_DATEFORMAT) FROM er_patstudystat WHERE fk_per = a.fk_per AND fk_study = a.FK_STUDY AND fk_codelst_stat = (SELECT pk_codelst FROM er_codelst WHERE codelst_type =''patStatus'' AND codelst_subtyp = ''followup'')) AS followup_on,
(SELECT MAX(TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) ||  (SELECT '' - '' || codelst_desc FROM er_codelst WHERE pk_codelst = patstudystat_reason) || DECODE(patstudystat_note,NULL,'''','' - '' || patstudystat_note)) FROM er_patstudystat WHERE fk_per = a.fk_per AND fk_study = a.FK_STUDY AND fk_codelst_stat = (SELECT pk_codelst FROM er_codelst WHERE codelst_type =''patStatus'' AND codelst_subtyp = ''offtreat'')) AS off_treatment_on,
(SELECT MAX(TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) || (SELECT '' - '' || codelst_desc FROM er_codelst WHERE pk_codelst = patstudystat_reason) ||  DECODE(patstudystat_note,NULL,'''','' - '' || patstudystat_note)) FROM er_patstudystat WHERE fk_per = a.fk_per AND fk_study = a.FK_STUDY AND fk_codelst_stat = (SELECT pk_codelst FROM er_codelst WHERE codelst_type =''patStatus'' AND codelst_subtyp = ''offstudy'')) AS off_study_on
FROM er_patprot a, er_study b,EPAT.person c
WHERE a.FK_PER = c.pk_person AND
EXISTS (SELECT * FROM er_usersite WHERE fk_site = c.fk_site AND fk_user = ~1  AND usersite_right > 0)    AND
a.FK_STUDY = b.pk_study AND
patprot_enroldt BETWEEN ''~2'' AND ''~3'' AND
patprot_stat = 1
ORDER BY study_number,patient_id'
WHERE PK_REPORT = 103;
COMMIT;

UPDATE ER_REPORT
SET REP_SQL_CLOB =
  'SELECT disease_site, total,    
(SELECT COUNT(*) FROM EPAT.person a, EPAT.pat_perid, ER_SITE b  WHERE a.fk_account = :sessAccId AND pk_person = fk_per AND pk_site = fk_site AND site_stat = ''Y'' AND    
person_regdate BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND  TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND    
perid_id = disease_site_subtyp) AS new_reg
FROM (
SELECT disease_site, disease_site_subtyp, SUM(cnt) AS total FROM (
(SELECT disease_site, disease_site_subtyp, COUNT(DISTINCT fk_per || ''*'' ||fk_study) AS CNT FROM (
SELECT DECODE(INSTR(study_disease_site,'',''),0,(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = f_to_number(a.study_disease_site)),''MULTIPLE'') AS disease_site,       
DECODE(INSTR(study_disease_site,'',''),0,(SELECT codelst_subtyp FROM ER_CODELST WHERE pk_codelst = f_to_number(a.study_disease_site)),''MULTIPLE'') AS disease_site_subtyp,        
b.fk_per,b.fk_study
FROM ER_STUDY a,ER_PATPROT b,ER_STUDYID c
WHERE fk_account = :sessAccId AND        
pk_study = b.fk_study  AND pk_study = c.fk_study AND        
fk_codelst_idtype = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp = ''sum4_agent'') AND studyId_id=''Y''    AND        
patprot_enroldt BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND  TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND fk_site_enrolling = (SELECT pk_site FROM ER_SITE WHERE fk_account = :sessAccId AND site_stat = ''Y'')
) GROUP BY disease_site, disease_site_subtyp)
UNION
(SELECT codelst_desc AS disease_site,codelst_subtyp AS disease_site_subtyp,0 AS total FROM ER_CODELST WHERE  codelst_type =''disease_site'')
) GROUP BY disease_site, disease_site_subtyp ORDER BY disease_site, disease_site_subtyp)'
WHERE PK_REPORT = 114;
COMMIT;

UPDATE ER_REPORT
SET REP_SQL =
  'SELECT (SELECT study_number FROM er_study WHERE pk_study = fk_study) AS study_number,
(SELECT study_title FROM ER_STUDY WHERE pk_study = fk_study) AS study_title,
TO_CHAR((SELECT study_actualdt FROM ER_STUDY WHERE pk_study = fk_study),PKG_DATEUTIL.F_GET_DATEFORMAT) AS study_startdate,
site_name, race,SUM(race_count) AS race_count,
SUM(DECODE(gender,''female'',race_count,0)) AS female_count,
SUM(DECODE(gender,''male'',race_count,0)) AS male_count,
SUM(DECODE(gender,''other'',race_count,0)) AS other_count,
SUM(DECODE(gender,''Unknown'',race_count,0)) AS unknown_count,
SUM(DECODE(gender,''zzzz'',race_count,0)) AS gen_ne_count,
SUM(DECODE(ethnicity,''hispanic'',race_count,0)) AS hispanic_count,
SUM(DECODE(ethnicity,''nonhispanic'',race_count,0)) AS nonhispanic_count,
SUM(DECODE(ethnicity,''notreported'',race_count,0)) AS notreported_count,
SUM(DECODE(ethnicity,''Unknown'',race_count,0)) AS eunknown_count,
SUM(DECODE(ethnicity,''zzzz'',race_count,0)) AS eth_ne_count
FROM (
SELECT fk_study,
(SELECT site_name FROM ER_SITE WHERE pk_site = fk_site) AS site_name,
DECODE(fk_codelst_race,NULL,''zzzz'',(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_race)) AS race,
DECODE(fk_codelst_gender,NULL,''zzzz'',(SELECT codelst_subtyp FROM ER_CODELST WHERE pk_codelst = fk_codelst_gender)) AS gender,
DECODE(fk_codelst_ethnicity,NULL,''zzzz'',(SELECT codelst_subtyp FROM ER_CODELST WHERE pk_codelst = fk_codelst_ethnicity)) AS ethnicity,
COUNT(fk_codelst_race || ''*'' || fk_codelst_gender || ''*'' || fk_codelst_ethnicity) AS race_count
FROM EPAT.person, ER_PATPROT
WHERE pk_person = fk_per AND
fk_study = ~1 AND
patprot_enroldt BETWEEN ''~2'' AND ''~3'' AND
patprot_stat = 1
GROUP BY
GROUPING SETS ( (fk_study,fk_site,fk_codelst_race,fk_codelst_gender,fk_codelst_ethnicity))
ORDER BY fk_site, fk_codelst_race)
GROUP BY fk_study,site_name, race
ORDER BY site_name,race'
WHERE PK_REPORT = 116;
COMMIT;

UPDATE ER_REPORT
SET REP_SQL_CLOB =
  'SELECT (SELECT study_number FROM er_study WHERE pk_study = fk_study) AS study_number,
(SELECT study_title FROM ER_STUDY WHERE pk_study = fk_study) AS study_title,
TO_CHAR((SELECT study_actualdt FROM ER_STUDY WHERE pk_study = fk_study),PKG_DATEUTIL.F_GET_DATEFORMAT) AS study_startdate,
site_name, race,SUM(race_count) AS race_count,
SUM(DECODE(gender,''female'',race_count,0)) AS female_count,
SUM(DECODE(gender,''male'',race_count,0)) AS male_count,
SUM(DECODE(gender,''other'',race_count,0)) AS other_count,
SUM(DECODE(gender,''unknown'',race_count,0)) AS unknown_count,
SUM(DECODE(gender,''zzzz'',race_count,0)) AS gen_ne_count,
SUM(DECODE(ethnicity,''hispanic'',race_count,0)) AS hispanic_count,
SUM(DECODE(ethnicity,''nonhispanic'',race_count,0)) AS nonhispanic_count,
SUM(DECODE(ethnicity,''notreported'',race_count,0)) AS notreported_count,
SUM(DECODE(ethnicity,''unknown'',race_count,0)) AS eunknown_count,
SUM(DECODE(ethnicity,''zzzz'',race_count,0)) AS eth_ne_count
FROM (
SELECT fk_study,
(SELECT site_name FROM ER_SITE WHERE pk_site = fk_site) AS site_name,
DECODE(fk_codelst_race,NULL,''zzzz'',(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_race)) AS race,
DECODE(fk_codelst_gender,NULL,''zzzz'',(SELECT codelst_subtyp FROM ER_CODELST WHERE pk_codelst = fk_codelst_gender)) AS gender,
DECODE(fk_codelst_ethnicity,NULL,''zzzz'',(SELECT codelst_subtyp FROM ER_CODELST WHERE pk_codelst = fk_codelst_ethnicity)) AS ethnicity,
COUNT(fk_codelst_race || ''*'' || fk_codelst_gender || ''*'' || fk_codelst_ethnicity) AS race_count
FROM EPAT.person, ER_PATPROT
WHERE pk_person = fk_per AND
fk_study in (:studyId) AND
patprot_enroldt BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND
patprot_stat = 1
GROUP BY
GROUPING SETS ( (fk_study,fk_site,fk_codelst_race,fk_codelst_gender,fk_codelst_ethnicity))
ORDER BY fk_site, fk_codelst_race)
GROUP BY fk_study,site_name, race
ORDER BY site_name,race'
WHERE PK_REPORT = 116;
COMMIT;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,163,2,'02_UPDATE_ER_REPORT.sql',sysdate,'9.0.0 Build#620');

commit;
