--STARTS DELETING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'hla_locus'
    AND codelst_subtyp = 'hla_drb2';
  if (v_record_exists = 1) then
     delete from ER_CODELST where CODELST_TYPE='hla_locus' and CODELST_SUBTYP='hla_drb2';
	commit;
  end if;
end;
/

--STARTS DELETING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'hla_locus'
    AND codelst_subtyp = 'hla_dqa1';
  if (v_record_exists = 1) then
     delete from ER_CODELST where CODELST_TYPE='hla_locus' and CODELST_SUBTYP='hla_dqa1';
	commit;
  end if;
end;
/


--STARTS DELETING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'hla_locus'
    AND codelst_subtyp = 'hla_dpa1';
  if (v_record_exists = 1) then
     delete from ER_CODELST where CODELST_TYPE='hla_locus' and CODELST_SUBTYP='hla_dpa1';
	commit;
  end if;
end;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,171,4,'04_ER_CODELST_DELETE.sql',sysdate,'9.0.0 Build#628');

commit;