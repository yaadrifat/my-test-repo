SET define OFF;
/*This code disables update triggers */
DECLARE 

valueCountDisable NUMBER(5);
valueCountDisable1 NUMBER(5);
valueCountEnable NUMBER(5);
valueCountEnable1 NUMBER(5);
BEGIN
    SELECT COUNT(*) into valueCountDisable FROM DBA_OBJECTS WHERE OBJECT_TYPE = 'TRIGGER' AND OBJECT_NAME = 'CB_USER_ALERTS_AU0';
    IF (valueCountDisable = 1) THEN
       EXECUTE IMMEDIATE 'ALTER TRIGGER CB_USER_ALERTS_AU0 DISABLE';
        COMMIT;
    END IF;    
	SELECT COUNT(*) into valueCountDisable1 FROM DBA_OBJECTS WHERE OBJECT_TYPE = 'TRIGGER' AND OBJECT_NAME = 'CB_USER_ALERTS_AU1';
    IF (valueCountDisable1 = 1) THEN
       EXECUTE IMMEDIATE 'ALTER TRIGGER CB_USER_ALERTS_AU1 DISABLE';
        COMMIT;
    END IF;
    
      UPDATE CB_USER_ALERTS  SET RID=SEQ_RID.NEXTVAL WHERE RID IS NULL;       
  COMMIT;
 SELECT COUNT(*) into valueCountEnable FROM DBA_OBJECTS WHERE OBJECT_TYPE = 'TRIGGER' AND OBJECT_NAME = 'CB_USER_ALERTS_AU0';
    IF (valueCountEnable = 1) THEN
       EXECUTE IMMEDIATE 'ALTER TRIGGER CB_USER_ALERTS_AU0 ENABLE';
        COMMIT;
    END IF;
	SELECT COUNT(*) into valueCountEnable1 FROM DBA_OBJECTS WHERE OBJECT_TYPE = 'TRIGGER' AND OBJECT_NAME = 'CB_USER_ALERTS_AU1';
    IF (valueCountEnable1 = 1) THEN
       EXECUTE IMMEDIATE 'ALTER TRIGGER CB_USER_ALERTS_AU1 ENABLE';
        COMMIT;
    END IF;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,172,47,'47_CB_USER_ALERTS_RID.sql',sysdate,'9.0.0 Build#629');

commit;