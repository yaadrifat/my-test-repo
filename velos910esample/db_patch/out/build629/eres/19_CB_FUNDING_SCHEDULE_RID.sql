set define off;
DECLARE
  COUNTFLAG NUMBER(5);  
BEGIN
    SELECT COUNT(1) INTO COUNTFLAG FROM  CB_FUNDING_SCHEDULE WHERE RID IS NULL;    
    IF (COUNTFLAG > 0) THEN
     update CB_FUNDING_SCHEDULE  set RID=SEQ_RID.NEXTVAL WHERE RID IS NULL;       
commit;
end if;
END;
/



INSERT INTO track_patches
VALUES(seq_track_patches.nextval,172,19,'19_CB_FUNDING_SCHEDULE_RID.sql',sysdate,'9.0.0 Build#629');

commit;