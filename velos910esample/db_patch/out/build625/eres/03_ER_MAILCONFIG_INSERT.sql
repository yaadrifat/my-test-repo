-- forentry in  ER_MAILCONFIG

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_mailconfig
    where mailconfig_mailcode = 'CBBAddChg';
	
    if (v_record_exists = 0) then
insert into er_mailconfig(pk_mailconfig, mailconfig_mailcode, mailconfig_subject, mailconfig_content, mailconfig_contenttype ,
mailconfig_from, mailconfig_bcc, mailconfig_cc, rid)
values(seq_er_mailconfig.nextval, 'CBBAddChg', 'CBB Contact Information is changed/modified for CBB ID #[SITEID]', 'Information for CBB :- [CBBNAME]<br/> CBB ID :-[SITEID] has been modified',
'text/html', 'velos_garuda@del.aithent.com', '', 'cordliaisions@nmdp.org', 1 ) ;
		commit;
  end if;
END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,168,3,'03_ER_MAILCONFIG_INSERT.sql',sysdate,'9.0.0 Build#625');

commit;