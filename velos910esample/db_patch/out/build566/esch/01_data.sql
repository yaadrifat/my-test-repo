update sch_lineitem set LINEITEM_OTHERCOST = nvl((to_char(to_number(LINEITEM_SPONSORUNIT ) * to_number(LINEITEM_CLINICNOFUNIT) ) ), '0.0')
where NVL(SUBCOST_ITEM_FLAG, 0) =1;

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,109,1,'01_data.sql',sysdate,'8.10.0 Build#566');

commit;