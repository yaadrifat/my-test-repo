--STARTS DELETING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'storage_temp';
  if (v_record_exists > 1) then
	DELETE FROM ER_CODELST where codelst_type = 'storage_temp';
	commit;
  end if;
end;
/
--END--
--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'storage_temp'
    AND codelst_subtyp = '<-150';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD) 
  VALUES(SEQ_ER_CODELST.nextval,'storage_temp','<-150','<-150','N',NULL,SYSDATE,SYSDATE,NULL);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'storage_temp'
    AND codelst_subtyp = '>=-150to<=-135';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD) 
  VALUES(SEQ_ER_CODELST.nextval,'storage_temp','>=-150to<=-135','>=-150 to <=-135','N',NULL,SYSDATE,SYSDATE,NULL);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'storage_temp'
    AND codelst_subtyp = '>-135';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD) 
  VALUES(SEQ_ER_CODELST.nextval,'storage_temp','>-135','>-135','N',NULL,SYSDATE,SYSDATE,NULL);
	commit;
  end if;
end;
/
--END--




INSERT INTO track_patches
VALUES(seq_track_patches.nextval,161,36,'36_HotFix1.sql',sysdate,'9.0.0 Build#618');

commit;

