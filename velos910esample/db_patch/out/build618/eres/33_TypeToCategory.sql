set define off;

update er_object_settings set OBJECT_DISPLAYTEXT = 'Add Category'
where object_type ='M' and OBJECT_Name='callib_menu' and object_subtype ='newcal_type';

update er_object_settings set OBJECT_DISPLAYTEXT = 'Add Category'
where object_type ='M' and OBJECT_Name='formlib_menu' and object_subtype ='form_type';

commit;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,161,33,'33_TypeToCategory.sql',sysdate,'9.0.0 Build#618');

commit;
