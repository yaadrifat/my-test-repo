
drop function codelststocodelstdesc;

commit;


---------FUNCTION F_CODELSTCOMMAVAL_T0_DESC

create or replace
function f_codelstcommaval_to_desc(v_str  varchar2,v_delimiter char) return varchar2 is
v_temp varchar2(4000):='';
v_temp1 varchar2(4000):='';
v_str1 varchar2(4000):=v_str||',';
v_cstr varchar2(4000):=v_str1;
v_count number:=1;
v_tot_commas number:=to_number((LENGTH(v_cstr) - LENGTH(REPLACE(v_cstr, ',', ''))) ,999999999999);
v_temp_no number:=0;
begin

WHILE v_count <= v_tot_commas
LOOP
    --dbms_output.put_line('count:::::'||v_count||'input str'||v_str1);
    v_str1:=trim(v_str1);
    v_temp:=to_number(substr(v_str1, 0, instr(v_str1, v_delimiter)),9999999999);
    --dbms_output.put_line('seperated str:::::'||v_temp);
    if v_count != v_tot_commas then
      v_temp1:=v_temp1||f_codelst_desc(v_temp)||',';
    else
     v_temp1:=v_temp1||f_codelst_desc(v_temp);
    end if;
    --dbms_output.put_line('Append comma:::::'||v_temp1);
    v_temp_no:=to_number(LENGTH(v_temp)+1,'9999999999');
    --dbms_output.put_line('after comma replace:::::'||v_temp_no);
    v_temp:=substr(v_str1, 0, v_temp_no);
    --dbms_output.put_line('after comma replace:::::'||v_temp);
    v_str1:=replace(v_str1,v_temp,'');
    --dbms_output.put_line('after str replace:::::'||v_str1);
    v_count:=v_count+1;
END LOOP;


return v_temp1;
end;
/



---------FUNCTION F_GET_DYNAMICFORMDATA


create or replace
function f_get_dynamicformdata(pk_cord number,categry varchar2) return SYS_REFCURSOR IS
dynamicform_data SYS_REFCURSOR;
BEGIN
    OPEN dynamicform_data FOR
 select
                              ques.pk_questions pk_ques,
                              forq.ques_seq QUES_SEQ,
                              qgrp.fk_question_group FK_QUES_GRP,
                              NVL(forq.fk_dependent_ques,'0') DEPENT_QUES_PK,
                              NVL(forq.fk_dependent_ques_value,'0') DEPENT_QUES_VALUE,
                              ques_desc QUES_DESC,
                              formres.assess_response ASSES_RESPONSE,
                              formres.comments COMMENTS,
                              f_codelst_desc(formres.resp_code) DROPDOWN_VALUE,
                              f_codelst_desc(ques.response_type) RESPONSE_TYPE,
                              qres.response_value TEXTFIELD_VALUE,
                              forq.fk_master_ques MASTER_QUES,
                              ques.add_comment_flag COMMENT_FLAG,
                              ques.assesment_flag ASSES_FLAG,
                              qres.unexpected_response_value UNEXPECTED_RESP,
                              ques.unlicn_prior_to_shipment_flag UNLIC_SHIP_FLAG,
                              formres.pk_form_responses PK_FORM_RESP,
                              formres.fk_question PK_QUES,
                              formres.resp_code RESPONSE_CODE,
                              case
                                  when formres.resp_val is null then '0'
                                  when formres.resp_val is not null and  f_codelst_desc(ques.response_type)='multiselect' and ( LENGTH(formres.resp_val) - LENGTH( REPLACE(formres.resp_val, ',', '') ) )>0 then f_codelstcommaval_to_desc(formres.resp_val,',')
                                  when formres.resp_val is not null and  f_codelst_desc(ques.response_type)='multiselect' and ( LENGTH(formres.resp_val) - LENGTH( REPLACE(formres.resp_val, ',', '') ) )=0 then f_codelst_desc(formres.resp_val)
                                   when formres.resp_val is not null and  f_codelst_desc(ques.response_type)='textfield' then formres.resp_val
                              end RESPONSE_VAL,
                              NVL(formres.resp_val,'0') RESPONSE_VAL12,
                              formres.fk_form_responses FK_FORM_RESP,
                              formres.pk_assessment PK_ASSSES,
                              formres.dynaformDate DYNAFORMDATE,
                              formres.assess_remarks ASSES_NOTES,
                              ques.unlicen_req_flag UNLIC_REQ_FLAG
                              from
                              cb_form_questions forq,
                              cb_question_responses qres,
                              cb_question_grp qgrp,
                              cb_questions ques
                              left outer join
                              (select
                                  frmres.pk_form_responses,
                                  frmres.fk_question,
                                  frmres.response_code resp_code,
                                  frmres.response_value resp_val,
                                  frmres.comments,
                                  frmres.fk_form_responses,
                                  asses.pk_assessment pk_assessment,
                                  f_codelst_desc(asses.assessment_for_response) assess_response,
                                  asses.assessment_remarks assess_remarks,
                                  to_char(frmres.DATE_MOMANS_OR_FORMFILL,'Mon DD, YYYY') dynaformDate
                                from
                                    cb_form_responses frmres  left outer join (select pk_assessment pk_assessment,sub_entity_id sub_entity_id,assessment_remarks assessment_remarks,cb_assessment.assessment_for_response assessment_for_response from cb_assessment where sub_entity_type=(select pk_codelst from  er_codelst  where  codelst_type='sub_entity_type' and codelst_subtyp=categry)) asses on( asses.sub_entity_id=frmres.pk_form_responses)
                                where
                                    frmres.entity_id=pk_cord and
                                    frmres.fk_form_version=(select MAX(PK_FORM_VERSION) from cb_form_version where entity_id=pk_cord and fk_form=(select pk_form from cb_forms where FORMS_DESC=categry and cb_forms.is_current_flag=1)) and
                                    frmres.entity_type=(select pk_codelst from er_codelst where codelst_type='test_source' and codelst_subtyp='maternal'))	formres
                              on(formres.FK_QUESTION=ques.pk_questions)
                              where
                              ques.pk_questions=forq.fk_question and
                              ques.pk_questions=qres.fk_question and
                              ques.pk_questions=qgrp.fk_question and
                              forq.fk_form=(select pk_form from cb_forms where forms_desc=categry and IS_CURRENT_FLAG=1)  and
                              qgrp.fk_form=(select pk_form from cb_forms where forms_desc=categry and IS_CURRENT_FLAG=1)
                              order by  qgrp.pk_question_grp,ques.pk_questions;
                              RETURN DYNAMICFORM_DATA;
END;
/




---------FUNCTION F_GET_DYNAMICFORMGRP


create or replace
function f_get_dynamicformgrp(categry varchar2) return SYS_REFCURSOR IS

dynamicform_grp SYS_REFCURSOR;
BEGIN
    OPEN dynamicform_grp FOR 
select 
  distinct pk_question_group PK_QUESTION_GRP,
  question_grp_desc QUESTION_GRP_NAME
from 
  cb_question_group quesgrp,
  cb_question_grp qgrp 
where 
  qgrp.fk_question_group=quesgrp.pk_question_group and 
  qgrp.fk_form=(select pk_form from cb_forms frms where frms.forms_desc=categry and frms.is_current_flag=1) 
order by quesgrp.pk_question_group;

return dynamicform_grp;
end;
/

COMMIT;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,178,15,'15_FUNCTIONS.sql',sysdate,'9.0.0 Build#635');

commit;