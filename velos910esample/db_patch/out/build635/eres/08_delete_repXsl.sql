set define off;

DELETE FROM "ERES"."ER_REPXSL" WHERE FK_REPORT IN (110, 159);

Commit;



INSERT INTO track_patches
VALUES(seq_track_patches.nextval,178,8,'08_delete_repXsl.sql',sysdate,'9.0.0 Build#635');

commit;
