
set define off;

-- INSERTING into ER_REPORT


DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_report
    where pk_report = 171;
  if (v_record_exists = 0) then
   Insert into ER_REPORT (PK_REPORT,REP_NAME,REP_DESC,REP_SQL,FK_ACCOUNT,FK_CODELST_TYPE,IP_ADD,REP_HIDE,REP_COLUMNS,REP_FILTERBY,GENERATE_XML,REP_TYPE,REP_SQL_CLOB,REP_FILTERKEYWORD,REP_FILTERAPPLICABLE) values (171,'Licensure and Eligibility Report','Licensure and Eligibility Report','',0,null,null,'Y',null,null,1,'rep_cbu','',null,null);

COMMIT;	

Update ER_REPORT set REP_SQL = 'SELECT 
F_CODELST_DESC(CORD.FK_CORD_CBU_ELIGIBLE_STATUS) ELIGSTATUS,
ROWTOCOL(''SELECT F_CODELST_DESC(REASON.FK_REASON_ID) FROM CB_CORD CORD INNER JOIN CB_ENTITY_STATUS STAT ON STAT.ENTITY_ID=CORD.PK_CORD INNER JOIN CB_ENTITY_STATUS_REASON REASON ON REASON.FK_ENTITY_STATUS=STAT.PK_ENTITY_STATUS WHERE CORD.FK_CORD_CBU_INELIGIBLE_REASON=STAT.PK_ENTITY_STATUS AND CORD.PK_CORD=~1'','', '') INELIGIBLEREASON,
F_CODELST_DESC(CORD.FK_CORD_CBU_LIC_STATUS) LICSTATUS,
ROWTOCOL(''SELECT F_CODELST_DESC(REASON.FK_REASON_ID) FROM CB_CORD CORD INNER JOIN CB_ENTITY_STATUS STAT ON STAT.ENTITY_ID=CORD.PK_CORD INNER JOIN CB_ENTITY_STATUS_REASON REASON ON REASON.FK_ENTITY_STATUS=STAT.PK_ENTITY_STATUS WHERE CORD.FK_CORD_CBU_UNLICENSED_REASON=STAT.PK_ENTITY_STATUS AND CORD.PK_CORD=~1'','', '') UNLICENSEREASON,
CORD.CORD_ELIGIBLE_ADDITIONAL_INFO ELIGCOMMENTS,
(SELECT TO_CHAR(MAX(CREATED_ON),''Mon DD, YYYY'') FROM CB_ENTITY_STATUS WHERE ENTITY_ID=~1 AND STATUS_TYPE_CODE=''eligibility'') ELGIBLE_MODI_DT,
(select to_char(max(CREATED_ON),''Mon DD, YYYY'') from CB_ENTITY_STATUS where ENTITY_ID=~1 and STATUS_TYPE_CODE=''licence'') LIC_MODI_DT,
CORD.CORD_REGISTRY_ID REGID,
SITE.SITE_NAME SITENAME,
SITE.SITE_ID SITEID,
F_CORD_ID_ON_BAG(~1) IDSONBAG,
CORD.CORD_LOCAL_CBU_ID LOCALCBUID,
CORD.CORD_NMDP_MATERNAL_ID MATREGID,
CORD.CORD_ISBI_DIN_CODE ISTBTDIN,
F_CORD_ADD_IDS(~1) ADDIDS,
DECODE(CORD.FK_CORD_CBU_ELIGIBLE_STATUS,
  (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE=''eligibility'' AND CODELST_SUBTYP=''eligible''),''0'',
  (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE=''eligibility'' AND CODELST_SUBTYP=''ineligible''),''1'',
  (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE=''eligibility'' AND CODELST_SUBTYP=''incomplete''),''2'',
  (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE=''eligibility'' AND CODELST_SUBTYP=''not_appli_prior''),''3'') ELI_FLAG,
DECODE(CORD.FK_CORD_CBU_LIC_STATUS,
  (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE=''licence'' AND CODELST_SUBTYP=''licensed''),''0'',
  (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE=''licence'' AND CODELST_SUBTYP=''unlicensed''),''1'') LIC_FLAG
FROM 
CB_CORD CORD
LEFT OUTER JOIN ER_SITE SITE ON
(CORD.FK_CBB_ID=SITE.PK_SITE)
WHERE 
CORD.PK_CORD=~1' where pk_report = 171;
	COMMIT;

	Update ER_REPORT set REP_SQL_CLOB = 'SELECT 
F_CODELST_DESC(CORD.FK_CORD_CBU_ELIGIBLE_STATUS) ELIGSTATUS,
ROWTOCOL(''SELECT F_CODELST_DESC(REASON.FK_REASON_ID) FROM CB_CORD CORD INNER JOIN CB_ENTITY_STATUS STAT ON STAT.ENTITY_ID=CORD.PK_CORD INNER JOIN CB_ENTITY_STATUS_REASON REASON ON REASON.FK_ENTITY_STATUS=STAT.PK_ENTITY_STATUS WHERE CORD.FK_CORD_CBU_INELIGIBLE_REASON=STAT.PK_ENTITY_STATUS AND CORD.PK_CORD=~1'','', '') INELIGIBLEREASON,
F_CODELST_DESC(CORD.FK_CORD_CBU_LIC_STATUS) LICSTATUS,
ROWTOCOL(''SELECT F_CODELST_DESC(REASON.FK_REASON_ID) FROM CB_CORD CORD INNER JOIN CB_ENTITY_STATUS STAT ON STAT.ENTITY_ID=CORD.PK_CORD INNER JOIN CB_ENTITY_STATUS_REASON REASON ON REASON.FK_ENTITY_STATUS=STAT.PK_ENTITY_STATUS WHERE CORD.FK_CORD_CBU_UNLICENSED_REASON=STAT.PK_ENTITY_STATUS AND CORD.PK_CORD=~1'','', '') UNLICENSEREASON,
CORD.CORD_ELIGIBLE_ADDITIONAL_INFO ELIGCOMMENTS,
(SELECT TO_CHAR(MAX(CREATED_ON),''Mon DD, YYYY'') FROM CB_ENTITY_STATUS WHERE ENTITY_ID=~1 AND STATUS_TYPE_CODE=''eligibility'') ELGIBLE_MODI_DT,
(select to_char(max(CREATED_ON),''Mon DD, YYYY'') from CB_ENTITY_STATUS where ENTITY_ID=~1 and STATUS_TYPE_CODE=''licence'') LIC_MODI_DT,
CORD.CORD_REGISTRY_ID REGID,
SITE.SITE_NAME SITENAME,
SITE.SITE_ID SITEID,
F_CORD_ID_ON_BAG(~1) IDSONBAG,
CORD.CORD_LOCAL_CBU_ID LOCALCBUID,
CORD.CORD_NMDP_MATERNAL_ID MATREGID,
CORD.CORD_ISBI_DIN_CODE ISTBTDIN,
F_CORD_ADD_IDS(~1) ADDIDS,
DECODE(CORD.FK_CORD_CBU_ELIGIBLE_STATUS,
  (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE=''eligibility'' AND CODELST_SUBTYP=''eligible''),''0'',
  (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE=''eligibility'' AND CODELST_SUBTYP=''ineligible''),''1'',
  (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE=''eligibility'' AND CODELST_SUBTYP=''incomplete''),''2'',
  (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE=''eligibility'' AND CODELST_SUBTYP=''not_appli_prior''),''3'') ELI_FLAG,
DECODE(CORD.FK_CORD_CBU_LIC_STATUS,
  (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE=''licence'' AND CODELST_SUBTYP=''licensed''),''0'',
  (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE=''licence'' AND CODELST_SUBTYP=''unlicensed''),''1'') LIC_FLAG
FROM 
CB_CORD CORD
LEFT OUTER JOIN ER_SITE SITE ON
(CORD.FK_CBB_ID=SITE.PK_SITE)
WHERE 
CORD.PK_CORD=~1' where pk_report = 171;
	COMMIT;

  end if;
end;
/




DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_report
    where pk_report = 172;
  if (v_record_exists = 0) then
   Insert into ER_REPORT 				       (PK_REPORT,REP_NAME,REP_DESC,REP_SQL,FK_ACCOUNT,FK_CODELST_TYPE,IP_ADD,REP_HIDE,REP_COLUMNS,REP_FILTERBY,GENERATE_XML,REP_TYPE,REP_SQL_CLOB,REP_FILTERKEYWORD,REP_FILTERAPPLICABLE) values (172,'Infectious Disease Markers Report','Infectious Disease Markers Report','',0,null,null,'Y',null,null,1,'rep_cbu','',null,null);

COMMIT;	

Update ER_REPORT set REP_SQL = 'SELECT 
  '''' TESTDATEVAL,
  '''' LABTESTNAME,
  '''' FK_TEST_OUTCOME,
  1 CMS_APPROVED_LAB,
  1 FDA_LICENSED_LAB,
  '''' ASSESSMENT_REMARKS,
  '''' FLAG_FOR_LATER,
  1  ASSESSMENT_FOR_RESPONSE,
  ''''  TC_VISIBILITY_FLAG,
  ''''  CONSULTE_FLAG,
  ''''  ASSESSMENT_POSTEDBY,
  ''''  ASSESSMENT_POSTEDON,
  ''''  ASSESSMENT_CONSULTBY,
  '''' ASSESSMENT_CONSULTON,
  CRD.CORD_ISBI_DIN_CODE ISTBTDIN,
  CRD.CORD_REGISTRY_ID REGID,
  CRD.CORD_LOCAL_CBU_ID LOCALCBUID,
  F_CORD_ADD_IDS(~1) ADDIDS,
  F_CORD_ID_ON_BAG(~1) IDSONBAG,
  SITE.SITE_NAME SITENAME,
  SITE.SITE_ID SITEID,
  '''' SOURCEVAL,
  1 lab_test
FROM 
  CB_CORD CRD,
  er_site site 
WHERE 
  CRD.FK_CBB_ID=SITE.PK_SITE AND CRD.PK_CORD=~1
union all
SELECT
        PL.TESTDATE,
        ltest.labtest_name,
        f_codelst_desc(pl.FK_TEST_OUTCOME),
        pl.CMS_APPROVED_LAB,
        pl.FDA_LICENSED_LAB,
        pl.ASSESSMENT_REMARKS,
        PL.FLAG_FOR_LATER,
        pl.ASSESSMENT_FOR_RESPONSE,
        PL.TC_VISIBILITY_FLAG,
        PL.CONSULTE_FLAG,
        F_GETUSER(PL.ASSESSMENT_POSTEDBY),
        to_char(PL.ASSESSMENT_POSTEDON,''Mon DD, YYYY''),
        F_GETUSER(PL.ASSESSMENT_CONSULTBY),
        to_char(PL.ASSESSMENT_CONSULTON,''Mon DD, YYYY''),
        '''',
        '''',
        '''',
        F_CORD_ADD_IDS(~1) ADDIDS,
        F_CORD_ID_ON_BAG(~1) IDSONBAG,
        '''',
        '''',
        pl.sourceval,
        ltest.pk_labtest lab_test
    from
        er_labtest ltest  
    left outer join
        (
            select
                plabs.fk_testid fk_testid,
                plabs.fk_test_outcome fk_test_outcome,
                plabs.ft_test_source ft_test_source,
                plabs.cms_approved_lab cms_approved_lab,
                plabs.fda_licensed_lab fda_licensed_lab,
                f_codelst_desc(PLABS.FT_TEST_SOURCE) sourceval,
                to_char(plabs.test_date,
                ''Mon DD, YYYY'') testDate,
                plabs.custom003 CUSTOM003,
                ass.ASSESSMENT_REMARKS ASSESSMENT_REMARKS,
                ass.FLAG_FOR_LATER FLAG_FOR_LATER,
                ass.ASSESSMENT_FOR_RESPONSE ASSESSMENT_FOR_RESPONSE,
                ass.TC_VISIBILITY_FLAG TC_VISIBILITY_FLAG,
                ass.SENT_TO_REVIEW_FLAG SENT_TO_REVIEW_FLAG,
                ass.CONSULTE_FLAG CONSULTE_FLAG,
                ass.ASSESSMENT_POSTEDBY ASSESSMENT_POSTEDBY,
                ass.ASSESSMENT_POSTEDON ASSESSMENT_POSTEDON,
                ass.ASSESSMENT_CONSULTBY ASSESSMENT_CONSULTBY,
                ass.ASSESSMENT_CONSULTON ASSESSMENT_CONSULTON 
            from
                er_patlabs plabs 
            left outer join
                cb_assessment ass 
                    on(
                        ass.sub_entity_id=plabs.pk_patlabs
                    ) 
            where
                plabs.pk_patlabs in(
                    select
                        pk_patlabs 
                    from
                        er_patlabs 
                    WHERE
                        fk_specimen=(select CORD.FK_SPECIMEN_ID from cb_cord cord where cord.pk_cord=~1)
                )
            ) pl 
                on(
                    ltest.pk_labtest=pl.fk_testid
                )  
        left outer join
            er_labtestgrp ltestgrp 
                on(
                    ltestgrp.fk_testid=ltest.pk_labtest 
                )  
        where
            ltestgrp.fk_labgroup = (
                select
                    lg.pk_labgroup 
                from
                    er_labgroup lg  
                where
                    lg.group_type=''I'' 
            ) 
            or ltestgrp.fk_labgroup = (
                select
                    lg.pk_labgroup 
                from
                    er_labgroup lg  
                where
                    LG.GROUP_TYPE=''IOG'' 
            )
            order by lab_test' where pk_report = 172;
	COMMIT;
        

Update ER_REPORT set REP_SQL_CLOB = 'SELECT 
  '''' TESTDATEVAL,
  '''' LABTESTNAME,
  '''' FK_TEST_OUTCOME,
  1 CMS_APPROVED_LAB,
  1 FDA_LICENSED_LAB,
  '''' ASSESSMENT_REMARKS,
  '''' FLAG_FOR_LATER,
  1  ASSESSMENT_FOR_RESPONSE,
  ''''  TC_VISIBILITY_FLAG,
  ''''  CONSULTE_FLAG,
  ''''  ASSESSMENT_POSTEDBY,
  ''''  ASSESSMENT_POSTEDON,
  ''''  ASSESSMENT_CONSULTBY,
  '''' ASSESSMENT_CONSULTON,
  CRD.CORD_ISBI_DIN_CODE ISTBTDIN,
  CRD.CORD_REGISTRY_ID REGID,
  CRD.CORD_LOCAL_CBU_ID LOCALCBUID,
  F_CORD_ADD_IDS(~1) ADDIDS,
  F_CORD_ID_ON_BAG(~1) IDSONBAG,
  SITE.SITE_NAME SITENAME,
  SITE.SITE_ID SITEID,
  '''' SOURCEVAL,
  1 lab_test
FROM 
  CB_CORD CRD,
  er_site site 
WHERE 
  CRD.FK_CBB_ID=SITE.PK_SITE AND CRD.PK_CORD=~1
union all
SELECT
        PL.TESTDATE,
        ltest.labtest_name,
        f_codelst_desc(pl.FK_TEST_OUTCOME),
        pl.CMS_APPROVED_LAB,
        pl.FDA_LICENSED_LAB,
        pl.ASSESSMENT_REMARKS,
        PL.FLAG_FOR_LATER,
        pl.ASSESSMENT_FOR_RESPONSE,
        PL.TC_VISIBILITY_FLAG,
        PL.CONSULTE_FLAG,
        F_GETUSER(PL.ASSESSMENT_POSTEDBY),
        to_char(PL.ASSESSMENT_POSTEDON,''Mon DD, YYYY''),
        F_GETUSER(PL.ASSESSMENT_CONSULTBY),
        to_char(PL.ASSESSMENT_CONSULTON,''Mon DD, YYYY''),
        '''',
        '''',
        '''',
        F_CORD_ADD_IDS(~1) ADDIDS,
        F_CORD_ID_ON_BAG(~1) IDSONBAG,
        '''',
        '''',
        pl.sourceval,
        ltest.pk_labtest lab_test
    from
        er_labtest ltest  
    left outer join
        (
            select
                plabs.fk_testid fk_testid,
                plabs.fk_test_outcome fk_test_outcome,
                plabs.ft_test_source ft_test_source,
                plabs.cms_approved_lab cms_approved_lab,
                plabs.fda_licensed_lab fda_licensed_lab,
                f_codelst_desc(PLABS.FT_TEST_SOURCE) sourceval,
                to_char(plabs.test_date,
                ''Mon DD, YYYY'') testDate,
                plabs.custom003 CUSTOM003,
                ass.ASSESSMENT_REMARKS ASSESSMENT_REMARKS,
                ass.FLAG_FOR_LATER FLAG_FOR_LATER,
                ass.ASSESSMENT_FOR_RESPONSE ASSESSMENT_FOR_RESPONSE,
                ass.TC_VISIBILITY_FLAG TC_VISIBILITY_FLAG,
                ass.SENT_TO_REVIEW_FLAG SENT_TO_REVIEW_FLAG,
                ass.CONSULTE_FLAG CONSULTE_FLAG,
                ass.ASSESSMENT_POSTEDBY ASSESSMENT_POSTEDBY,
                ass.ASSESSMENT_POSTEDON ASSESSMENT_POSTEDON,
                ass.ASSESSMENT_CONSULTBY ASSESSMENT_CONSULTBY,
                ass.ASSESSMENT_CONSULTON ASSESSMENT_CONSULTON 
            from
                er_patlabs plabs 
            left outer join
                cb_assessment ass 
                    on(
                        ass.sub_entity_id=plabs.pk_patlabs
                    ) 
            where
                plabs.pk_patlabs in(
                    select
                        pk_patlabs 
                    from
                        er_patlabs 
                    WHERE
                        fk_specimen=(select CORD.FK_SPECIMEN_ID from cb_cord cord where cord.pk_cord=~1)
                )
            ) pl 
                on(
                    ltest.pk_labtest=pl.fk_testid
                )  
        left outer join
            er_labtestgrp ltestgrp 
                on(
                    ltestgrp.fk_testid=ltest.pk_labtest 
                )  
        where
            ltestgrp.fk_labgroup = (
                select
                    lg.pk_labgroup 
                from
                    er_labgroup lg  
                where
                    lg.group_type=''I'' 
            ) 
            or ltestgrp.fk_labgroup = (
                select
                    lg.pk_labgroup 
                from
                    er_labgroup lg  
                where
                    LG.GROUP_TYPE=''IOG'' 
            )
            order by lab_test' where pk_report = 172;
          commit;
  end if;
end;
/
  

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_report
    where pk_report = 174;
  if (v_record_exists = 0) then
   Insert into ER_REPORT 				       (PK_REPORT,REP_NAME,REP_DESC,REP_SQL,FK_ACCOUNT,FK_CODELST_TYPE,IP_ADD,REP_HIDE,REP_COLUMNS,REP_FILTERBY,GENERATE_XML,REP_TYPE,REP_SQL_CLOB,REP_FILTERKEYWORD,REP_FILTERAPPLICABLE) values (174,'Packaging Slip','Packaging Slip','',0,null,null,'Y',null,null,1,'rep_cbu','',null,null);

COMMIT;	

Update ER_REPORT set REP_SQL ='SELECT  
CRD.CORD_REGISTRY_ID REGID, 
CRD.CORD_LOCAL_CBU_ID LOCALCBUID, 
CRD.CORD_ISBI_DIN_CODE ISBTDIN, 
CBB.SITE_NAME SITENAME, 
ADDR.ADDRESS ADDRESS, 
ADDR.ADD_CITY CITY, 
ADDR.ADD_ZIPCODE ZIPCODE, 
ADDR.ADD_STATE STATE, 
ADDR.ADD_COUNTRY COUNTRY, 
nvl(to_char(pslip.SCH_SHIPMENT_DATE,''Mon DD, YYYY''),''Not Available'') SHIPDATE, 
NVL(F_CODELST_DESC(PSLIP.FK_SAMPLE_TYPE_AVAIL),''Not Available'') SAMPLE_TYPE, 
NVL(F_CODELST_DESC(PSLIP.FK_ALIQUOTS_TYPE),''Not Available'') ALIQUOTS_TYPE, 
TO_CHAR(SYSDATE,''Mon DD, YYYY'') RUN_DATE, 
TO_CHAR(SYSTIMESTAMP, ''HH:MI AM'') RUN_TIME, 
nvl(f_codelst_desc(hla.FK_HLA_CODE_ID),''Not Available'') LUCUS, 
NVL(F_CODELST_DESC(HLA.FK_HLA_METHOD_ID),''Not Available'') METH, 
NVL(HLA.HLA_VALUE_TYPE1,''-'') HLA1, 
NVL(HLA.HLA_VALUE_TYPE2,''-'') HLA2, 
NVL(TO_CHAR(HLA.CREATED_ON,''Mon DD, YYYY''),''Not Available'') HLA_CREATED_DT  
from cb_packing_slip pslip  
LEFT OUTER JOIN  
CB_CORD CRD  
on(pslip.fk_cord_id=crd.pk_cord) 
LEFT OUTER JOIN  
ER_SITE CBB  
on(pslip.cbb_id=cbb.pk_site)  
LEFT OUTER JOIN  
ER_ADD ADDR  
ON(CBB.FK_PERADD=ADDR.PK_ADD)  
LEFT OUTER JOIN  
ER_ORDER ORD  
on(pslip.fk_order_id=ord.pk_order)  
LEFT OUTER JOIN CB_HLA HLA  
ON(ORD.FK_CURRENT_HLA=HLA.PK_HLA)  
where pslip.pk_packing_slip=~1' where pk_report = 174;

COMMIT;
Update ER_REPORT set REP_SQL_CLOB = 'SELECT  
CRD.CORD_REGISTRY_ID REGID, 
CRD.CORD_LOCAL_CBU_ID LOCALCBUID, 
CRD.CORD_ISBI_DIN_CODE ISBTDIN, 
CBB.SITE_NAME SITENAME, 
ADDR.ADDRESS ADDRESS, 
ADDR.ADD_CITY CITY, 
ADDR.ADD_ZIPCODE ZIPCODE, 
ADDR.ADD_STATE STATE, 
ADDR.ADD_COUNTRY COUNTRY, 
nvl(to_char(pslip.SCH_SHIPMENT_DATE,''Mon DD, YYYY''),''Not Available'') SHIPDATE, 
NVL(F_CODELST_DESC(PSLIP.FK_SAMPLE_TYPE_AVAIL),''Not Available'') SAMPLE_TYPE, 
NVL(F_CODELST_DESC(PSLIP.FK_ALIQUOTS_TYPE),''Not Available'') ALIQUOTS_TYPE, 
TO_CHAR(SYSDATE,''Mon DD, YYYY'') RUN_DATE, 
TO_CHAR(SYSTIMESTAMP, ''HH:MI AM'') RUN_TIME, 
nvl(f_codelst_desc(hla.FK_HLA_CODE_ID),''Not Available'') LUCUS, 
NVL(F_CODELST_DESC(HLA.FK_HLA_METHOD_ID),''Not Available'') METH, 
NVL(HLA.HLA_VALUE_TYPE1,''-'') HLA1, 
NVL(HLA.HLA_VALUE_TYPE2,''-'') HLA2, 
NVL(TO_CHAR(HLA.CREATED_ON,''Mon DD, YYYY''),''Not Available'') HLA_CREATED_DT  
from cb_packing_slip pslip  
LEFT OUTER JOIN  
CB_CORD CRD  
on(pslip.fk_cord_id=crd.pk_cord) 
LEFT OUTER JOIN  
ER_SITE CBB  
on(pslip.cbb_id=cbb.pk_site)  
LEFT OUTER JOIN  
ER_ADD ADDR  
ON(CBB.FK_PERADD=ADDR.PK_ADD)  
LEFT OUTER JOIN  
ER_ORDER ORD  
on(pslip.fk_order_id=ord.pk_order)  
LEFT OUTER JOIN CB_HLA HLA  
ON(ORD.FK_CURRENT_HLA=HLA.PK_HLA)  
where pslip.pk_packing_slip=~1' where pk_report = 174;

COMMIT;

  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_report
    where pk_report = 173;
  if (v_record_exists = 0) then
   Insert into ER_REPORT (PK_REPORT,REP_NAME,REP_DESC,REP_SQL,FK_ACCOUNT,FK_CODELST_TYPE,IP_ADD,REP_HIDE,REP_COLUMNS,REP_FILTERBY,GENERATE_XML,REP_TYPE,REP_SQL_CLOB,REP_FILTERKEYWORD,REP_FILTERAPPLICABLE) values (173,'Lab Summary Report','Lab Summary Report','',0,null,null,'Y',null,null,1,'rep_cbu','',null,null);

COMMIT;	

Update ER_REPORT set REP_SQL = 'select cbbid, cbb_id, cord_registry_id, cord_local_cbu_id, cord_isbi_din_code, to_char(prcsng_start_date,''Mon DD, YYYY'') prcsng_start_date, bact_culture, bact_comment,
fungal_culture, fung_comment, abo_blood_type, hemoglobin_scrn, rh_type, to_char(bact_cul_strt_dt,''Mon DD, YYYY'') bact_cul_strt_dt, to_char(fung_cul_strt_dt,''Mon DD, YYYY'') fung_cul_strt_dt, to_char(frz_date,''Mon DD, YYYY'') frz_date,
(select f_lab_summary(~1) from dual) lab_summary, f_get_attachments(~1,(f_codelst_id(''doc_categ'',''lab_sum_cat''))) labsum_attachments
from rep_cbu_details where pk_cord = ~1 ' where pk_report = 173;
	COMMIT;

	Update ER_REPORT set REP_SQL_CLOB = 'select cbbid, cbb_id, cord_registry_id, cord_local_cbu_id, cord_isbi_din_code, to_char(prcsng_start_date,''Mon DD, YYYY'') prcsng_start_date, bact_culture, bact_comment,
fungal_culture, fung_comment, abo_blood_type, hemoglobin_scrn, rh_type, to_char(bact_cul_strt_dt,''Mon DD, YYYY'') bact_cul_strt_dt, to_char(fung_cul_strt_dt,''Mon DD, YYYY'') fung_cul_strt_dt, to_char(frz_date,''Mon DD, YYYY'') frz_date,
(select f_lab_summary(~1) from dual) lab_summary, f_get_attachments(~1,(f_codelst_id(''doc_categ'',''lab_sum_cat''))) labsum_attachments
from rep_cbu_details where pk_cord = ~1 ' where pk_report = 173;
	COMMIT;

  end if;
end;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,166,20,'20_ER_REPORT.sql',sysdate,'9.0.0 Build#623');

commit;
