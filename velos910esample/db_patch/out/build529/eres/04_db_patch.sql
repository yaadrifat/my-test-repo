Update er_report set rep_type =null where pk_report = 159;

commit;


update er_codelst set CODELST_CUSTOM_COL = 'service' 
where CODELST_TYPE ='site_type'
and CODELST_SUBTYP = 'service';

commit;


DELETE FROM ER_REPXSL WHERE FK_REPORT = 159;

commit;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,72,4,'04_db_patch.sql',sysdate,'8.9.0 Build#529');

commit;
