set define off;

UPDATE ER_CTRLTAB SET CTRL_VALUE = '8.9.0 Build#529' 
where CTRL_KEY = 'app_version' ; 

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,72,0,'00_er_version.sql',sysdate,'8.9.0 Build#529');

commit;
