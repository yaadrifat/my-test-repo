set define off;
declare 

qCount Number;

begin

select count(*) into qCount from ER_CODELST where CODELST_TYPE='tarea' and CODELST_SUBTYP='cutaneous';

if qCount=1 then
  Update ER_CODELST set CODELST_DESC ='Cutaneous (Skin)' where CODELST_TYPE='tarea' and CODELST_SUBTYP='cutaneous';
  commit;
end if;

end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,196,9,'09_ER_CODELISTUPDATE.sql',sysdate,'9.1 Build#644');

commit;