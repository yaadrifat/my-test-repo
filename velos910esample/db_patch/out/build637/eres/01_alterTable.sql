--STARTS ADDING COLUMN TO CB_FORM_VERSION--

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_FORM_VERSION'
    AND column_name = 'VOID_FLAG';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_FORM_VERSION ADD(VOID_FLAG VARCHAR2(1 BYTE))';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_FORM_VERSION'
    AND column_name = 'VOID_DATE';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_FORM_VERSION ADD(VOID_DATE DATE)';
  end if;
end;
/
commit;


COMMENT ON COLUMN CB_FORM_VERSION.VOID_FLAG IS 'Stores 0 or 1 to void the form responses';

COMMENT ON COLUMN CB_FORM_VERSION.VOID_DATE IS 'Stores date when the reponse version get void';


--START UPDATING er_codelst TABLE --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'note_assess' and codelst_subtyp='tu';
  if (v_record_exists = 1) then
	delete from er_codelst where codelst_type = 'note_assess' and codelst_subtyp='tu';
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'resp_type' and codelst_subtyp='label';
  if (v_record_exists = 0) then
	Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'resp_type','label','label','N',6,null,null,null,null ,sysdate,sysdate,null,null,null,null);
commit;
  end if;
end;
/
--END--
--START UPDATING CB_FORM_QUESTIONS TABLE --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_contag_person_understand_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2G');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set UNEXPECTED_RESPONSE_VALUE='mrq_unres2' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_contag_person_understand_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2G');
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set RESPONSE_VALUE='fmhq_res1' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--END--
--START INSERTING RECORD IN TO CB_QUESTIONS TABLE --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'unexplained_symptom_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Do you have any of the following', NULL, '0', '0', '0', '0', '0', NULL, '0', '0', (select PK_CODELST from ER_CODELST where CODELST_TYPE='resp_type' and CODELST_SUBTYP='label'), 'unexplained_symptom_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--END--

--START INSERTING RECORD IN TO CB_QUESTION_GRP TABLE --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexplained_symptom_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2G');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC ='B. Behavior and Exposure Risk'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='unexplained_symptom_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2G'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--END--
--START INSERTING RECORD IN TO CB_FORM_QUESTIONS TABLE --
--Question 36--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexplained_symptom_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2G');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2G'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexplained_symptom_ind'),
null,null,null,'36',null,NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 36.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_night_sweat')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2G');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set FK_MASTER_QUES=(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexplained_symptom_ind') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_night_sweat') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2G');
commit;
  end if;
end;
/
--Question 36.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_blue_prpl_spot_kaposi_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2G');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set FK_MASTER_QUES=(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexplained_symptom_ind') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_blue_prpl_spot_kaposi_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2G');
commit;
  end if;
end;
/
--Question 36.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_weight_loss')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2G');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set FK_MASTER_QUES=(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexplained_symptom_ind') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_weight_loss') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2G');
commit;
  end if;
end;
/
--Question 36.d--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_persist_diarrhea')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2G');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set FK_MASTER_QUES=(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexplained_symptom_ind') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_persist_diarrhea') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2G');
commit;
  end if;
end;
/
--Question 36.e--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_cough_short_breath')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2G');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set FK_MASTER_QUES=(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexplained_symptom_ind') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_cough_short_breath') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2G');
commit;
  end if;
end;
/
--Question 36.f--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_temp_over_ten_days')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2G');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set FK_MASTER_QUES=(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexplained_symptom_ind') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_temp_over_ten_days') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2G');
commit;
  end if;
end;
/
--Question 36.g--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_mouth_sores_wht_spt')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2G');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set FK_MASTER_QUES=(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexplained_symptom_ind') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_mouth_sores_wht_spt') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2G');
commit;
  end if;
end;
/
--Question 36.h--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_1m_lump_nk_apit_grn_mult_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2G');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set FK_MASTER_QUES=(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexplained_symptom_ind') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_1m_lump_nk_apit_grn_mult_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2G');
commit;
  end if;
end;
/
--Question 36.i--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inf_dur_preg_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2G');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set FK_MASTER_QUES=(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexplained_symptom_ind') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inf_dur_preg_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2G');
commit;
  end if;
end;
/
--END--

--START UPDATING CB_QUESTIONS TABLE--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'donate_cb_prev_ind';
  if (v_record_exists = 0) then
	UPDATE CB_QUESTIONS SET QUES_HELP='<u>Current Evaluation and Action</u> <br/>If Yes - Accept response, cord blood bank to link records of previous donation if it occurred at the bank.  Bank staff to review available historical records to determine if the mother is or is not eligible to donate. <br/><br/><u>Historical Evaluation</u><br/>Same as current evaluation and action.' WHERE QUES_CODE = 'donate_cb_prev_ind';
commit;
  end if;
end;
/
--END--

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,180,1,'01_alterTable.sql',sysdate,'9.0.0 Build#637');

commit;