--STARTS INSERTING RECORD INTO UI_PAGE TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_PAGE
    where PAGE_NAME = 'Pending Orders';
  if (v_record_exists = 0) then
      INSERT INTO 
  UI_PAGE(PK_PAGE,PAGE_CODE,PAGE_NAME) 
  VALUES(SEQ_UI_PAGE.nextval,'26','Pending Orders');
	commit;
  end if;
end;
/
--END--



--STARTS INSERTING RECORD INTO UI_WIDGET_CONTAINER TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_WIDGET_CONTAINER
    where CONTAINER_NAME = 'Pending Orders';
  if (v_record_exists = 0) then
      INSERT INTO 
  UI_WIDGET_CONTAINER(PK_CONTAINER,FK_PAGE_ID,CONTAINER_NAME) 
  VALUES(SEQ_UI_WIDGET_CONTAINER.nextval,(SELECT PK_PAGE FROM UI_PAGE WHERE PAGE_NAME='Pending Orders'),'Pending Orders');
	commit;
  end if;
end;
/
--END--

--------------------------------------------------------------for Active Request part--------------------------------------------------------------------

--STARTS INSERTING RECORD INTO UI_WIDGET TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_WIDGET
    where WIDGET_DIV_ID = 'activeOrdDiv';
  if (v_record_exists = 0) then
      INSERT INTO 
  UI_WIDGET(PK_WIDGET,NAME,WIDGET_DIV_ID,DESCRIPTION,IS_AUTHENTICATED,IS_MINIZABLE,IS_CLOSABLE,IS_RESIZEABLE,IS_DRAGABLE,
IS_MOUSE_HOVER_ENABLED,RESIZE_MIN_HEIGHT,RESIZE_MIN_WIDTH,RESIZE_MAX_HEIGHT,RESIZE_MAX_WIDTH) 
  VALUES(SEQ_UI_WIDGET.nextval,'Active Requests','activeOrdDiv','Active Requests',1,1,1,1,1,1,100,400,1024,1024);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO UI_CONTAINER_WIDGET_MAP TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_CONTAINER_WIDGET_MAP
    where FK_CONTAINER_ID = (SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='Pending Orders')
    AND FK_WIDGET_ID=(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='activeOrdDiv');
  if (v_record_exists = 0) then
      INSERT INTO 
  UI_CONTAINER_WIDGET_MAP(PK_PG_CONTI_WID,FK_CONTAINER_ID,FK_WIDGET_ID) 
  VALUES(SEQ_UI_CONTAINER_WIDGET_MAP.nextval,(SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='Pending Orders'),(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='activeOrdDiv'));
	commit;
  end if;
end;
/
--END--




------------------------------------------------------------------for Other Request part--------------------------------------------------------------------


--STARTS INSERTING RECORD INTO UI_WIDGET TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_WIDGET
    where WIDGET_DIV_ID = 'otherOrdDiv';
  if (v_record_exists = 0) then
      INSERT INTO 
  UI_WIDGET(PK_WIDGET,NAME,WIDGET_DIV_ID,DESCRIPTION,IS_AUTHENTICATED,IS_MINIZABLE,IS_CLOSABLE,IS_RESIZEABLE,IS_DRAGABLE,
IS_MOUSE_HOVER_ENABLED,RESIZE_MIN_HEIGHT,RESIZE_MIN_WIDTH,RESIZE_MAX_HEIGHT,RESIZE_MAX_WIDTH) 
  VALUES(SEQ_UI_WIDGET.nextval,'Other Requests','otherOrdDiv','Other Requests',1,1,1,1,1,1,100,400,1024,1024);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO UI_CONTAINER_WIDGET_MAP TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_CONTAINER_WIDGET_MAP
    where FK_CONTAINER_ID = (SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='Pending Orders')
    AND FK_WIDGET_ID=(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='otherOrdDiv');
  if (v_record_exists = 0) then
      INSERT INTO 
  UI_CONTAINER_WIDGET_MAP(PK_PG_CONTI_WID,FK_CONTAINER_ID,FK_WIDGET_ID) 
  VALUES(SEQ_UI_CONTAINER_WIDGET_MAP.nextval,(SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='Pending Orders'),(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='otherOrdDiv'));
	commit;
  end if;
end;
/
--END--


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,180,10,'10_UI_WIDGET.sql',sysdate,'9.0.0 Build#637');

commit;
