update event_assoc set status = null where status is not null and upper(event_type) = 'P';

update SCH_PROTSTAT set PROTSTAT = null where PROTSTAT is not null;

update event_def set status = null where status is not null and upper(event_type) = 'P';



commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,110,20,'20_update_event_def.sql',sysdate,'8.10.0 Build#567');

commit;

