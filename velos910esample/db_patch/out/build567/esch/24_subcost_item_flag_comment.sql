Comment on column SCH_LINEITEM.SUBCOST_ITEM_FLAG is 'This column stores 1 if lineitem is generated from Subject Cost Item else stores 0';

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,110,24,'24_subcost_item_flag_comment.sql',sysdate,'8.10.0 Build#567');

commit;