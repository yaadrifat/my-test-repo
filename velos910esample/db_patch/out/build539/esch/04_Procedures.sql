-- 1st Procedure
create or replace
PROCEDURE        "SP_ADDPROTTOSTUDY" (
p_protocol IN NUMBER,
p_studyId IN NUMBER,
p_userid IN NUMBER ,
P_newProtocol OUT NUMBER,
p_ip IN VARCHAR,
p_calAssocTo CHAR
)
AS


/****************************************************************************************************
** Procedure to copy a protocol to a study

** user can add an existing calender to a study

** Parameter Description

** p_protocol event id of the protocol that is to be copied in a protocol
** p_studyId study id of the study in which protocol is to be copied
** P_newProtocol protocol id of the new copied protocol

** Author: Dinesh Khurana 15th June 2001
**
** Modification History
**
** Modified By			Date				Remarks
** Dinesh Khurana		23July2001			To apply check of duplicacy in a study.
** Dinesh Khurana		1Aug2001			Extra columns added in the table related to messages.
** Sam Varadarajan		30Sep2004			Calendar related enh. copying duration unit (type) and protocol visits.
** Jnanamay			15Apr2008			to provide the event sequence
** Manimaran			18Apr2008			new column to associate
** Sammie			29Apr2010			new columns SERVICE_SITE_ID,FACILITY_ID,FK_CODELST_COVERTYPE
** Bikash     			28Jul2010     			new column COVERAGE_NOTES
*****************************************************************************************************
*/

event_id NUMBER;
chain_id NUMBER;
event_type CHAR(1);
NAME VARCHAR2(4000); --KM 08Sep09
notes VARCHAR2(1000);
COST NUMBER;
cost_description VARCHAR2(200);
duration NUMBER;
user_id NUMBER;
linked_uri VARCHAR2(200);
fuzzy_period VARCHAR2(10);
msg_to CHAR(1);
status CHAR(1);
description VARCHAR2(1000);
displacement NUMBER;
eventMsg VARCHAR2(2000);
eventRes CHAR(1);
eventFlag NUMBER;

pat_daysbefore NUMBER(3);
pat_daysafter NUMBER(3);
usr_daysbefore NUMBER(3);
usr_daysafter NUMBER(3);
pat_msgbefore VARCHAR2(4000);
pat_msgafter VARCHAR2(4000);
usr_msgbefore VARCHAR2(4000);
usr_msgafter VARCHAR2(4000);
usr_event_sequence number;--JM: 15Apr2008

p_name VARCHAR2(4000);
org_id NUMBER;
p_chain_id NUMBER;
p_currval NUMBER;
p_status NUMBER;
p_count NUMBER;

v_duration_unit CHAR(1); -- SV, 09/30/04, added to copy visits from old protocol to new study protocol.
v_event_visit_id NUMBER; -- SV, 09/30/04, fk_visit
v_ret NUMBER; -- SV, 09/30/04,
-- The new protocol id generated from the EVENT_ASSOCIATION_SEQ sequence. This is stored in the -- p_chain_id
-- This variable is an OUT parameter
v_catlib NUMBER;
v_calAssocTo CHAR(1);
v_visit_num NUMBER;
v_cptcode VARCHAR2(255);
v_fuzzyAfter VARCHAR2(10);
v_durAfter CHAR(1);
v_durBefore CHAR(1);
v_event_category varchar2(100); --KM
v_event_library_type Number;
v_event_line_category Number;
v_SERVICE_SITE_ID Number;
v_FACILITY_ID Number;
v_FK_CODELST_COVERTYPE Number;
V_COVERAGE_NOTES varchar2(4000);  --BK 28JUL2010

-- SV, 09/30/04, cal-enh- added code to copy duration_unit (type) and fk_visit (visits in event) to new protocol.
CURSOR C1 IS SELECT EVENT_ID,
		CHAIN_ID,
		EVENT_TYPE,
		NAME,
		NOTES,
		COST,
		COST_DESCRIPTION,
		DURATION,
		USER_ID,
		LINKED_URI,
		FUZZY_PERIOD,
		MSG_TO,
		STATUS,
		DESCRIPTION,
		DISPLACEMENT,
		ORG_ID ,
		EVENT_MSG,
		EVENT_RES,
		EVENT_FLAG,
		PAT_DAYSBEFORE,
		PAT_DAYSAFTER ,
		USR_DAYSBEFORE,
		USR_DAYSAFTER ,
		PAT_MSGBEFORE ,
		PAT_MSGAFTER ,
		USR_MSGBEFORE ,
		USR_MSGAFTER,
		DURATION_UNIT,
		FK_VISIT,FK_CATLIB,event_cptcode,event_fuzzyafter,event_durationafter,event_durationbefore, event_category,event_sequence,
		event_library_type ,
		event_line_category,
		SERVICE_SITE_ID,
		FACILITY_ID,
		FK_CODELST_COVERTYPE,
		COVERAGE_NOTES
		FROM EVENT_DEF WHERE chain_id = p_protocol ORDER BY event_type DESC ,COST,displacement ;

BEGIN

	IF (LENGTH(p_calAssocTo)=0) THEN
		v_calAssocTo:='P';
	ELSE
		v_calAssocTo:=p_calAssocTo;
	END IF;

	-- to check if the protocol name already exists in the study

	SELECT NAME INTO p_name FROM EVENT_DEF WHERE event_id=p_protocol;
	SELECT COUNT(event_id) INTO p_count FROM EVENT_ASSOC WHERE NAME=p_name AND EVENT_TYPE='P'AND CHAIN_ID=p_studyId
	AND event_calassocto=v_calAssocTo;

	IF p_count > 0 THEN
		p_newProtocol:=-1;
		RETURN;
	END IF;

	OPEN C1;

	LOOP
		--SV, 09/30/04, ++ duration_unit, fk_vist
		FETCH C1 INTO event_id, chain_id , event_type, NAME, notes, COST, cost_description,
		duration, user_id, linked_uri, fuzzy_period, msg_to, status,description, displacement,
		org_id,eventMsg,eventRes, eventFlag,pat_daysbefore,pat_daysafter,usr_daysbefore,
		usr_daysafter,pat_msgbefore,pat_msgafter,usr_msgbefore,usr_msgafter, v_duration_unit, v_event_visit_id,v_catlib,v_cptcode,
		v_fuzzyAfter,v_durAfter,v_durBefore,v_event_category,usr_event_sequence,v_event_library_type, v_event_line_category,
		v_SERVICE_SITE_ID,v_FACILITY_ID,v_FK_CODELST_COVERTYPE,V_COVERAGE_NOTES;

		SELECT EVENT_DEFINITION_SEQ.NEXTVAL INTO p_currval FROM dual;

		IF C1%ROWCOUNT = 1 THEN
			p_chain_id:=p_studyId;
			p_newProtocol:=p_currval;
			status:='W';

		ELSE
			p_chain_id:=p_newProtocol;
		END IF;

		--event id of the the protocol generated is the chain id of all the child events

		EXIT WHEN C1%NOTFOUND;
		--SV, ++ duration unit. visits handled separately below.
		INSERT INTO EVENT_ASSOC(
			EVENT_ID,
			CHAIN_ID,
			EVENT_TYPE,
			NAME,
			NOTES,
			COST,
			COST_DESCRIPTION,
			DURATION,
			USER_ID,
			LINKED_URI,
			FUZZY_PERIOD,
			MSG_TO,
			STATUS,
			DESCRIPTION,
			DISPLACEMENT,
			ORG_ID ,
			EVENT_MSG,
			EVENT_RES,
			EVENT_FLAG,
			PAT_DAYSBEFORE,
			PAT_DAYSAFTER ,
			USR_DAYSBEFORE,
			USR_DAYSAFTER ,
			PAT_MSGBEFORE ,
			PAT_MSGAFTER ,
			USR_MSGBEFORE ,
			USR_MSGAFTER ,
			STATUS_DT ,
			STATUS_CHANGEBY ,
			CREATOR,
			IP_ADD, --Amarnadh
			DURATION_UNIT,FK_CATLIB,event_calassocto,event_cptcode,event_fuzzyafter,event_durationafter,event_durationbefore,event_category, event_sequence,
			event_library_type ,
			event_line_category,
			SERVICE_SITE_ID,
			FACILITY_ID,
			FK_CODELST_COVERTYPE,
			COVERAGE_NOTES
		)
 		VALUES(
			p_currval,
			p_chain_id ,
			event_type,
			NAME,
			notes,
			COST,
			cost_description,
			duration,
			user_id,
			linked_uri,
			fuzzy_period,
			msg_to,
			status,
			description,
			displacement,
			org_id,
			eventMsg,
			eventRes,
			eventFlag,
			pat_daysbefore,
			pat_daysafter,
			usr_daysbefore,
			usr_daysafter,
			pat_msgbefore,
			pat_msgafter ,
			usr_msgbefore,
			usr_msgafter ,
			SYSDATE ,
			p_userid ,
			p_userid,
			p_ip,
			v_duration_unit,v_catlib,v_calAssocTo,v_cptcode,v_fuzzyAfter,v_durAfter,v_durBefore, v_event_category,usr_event_sequence,
			v_event_library_type,
			v_event_line_category,
			v_SERVICE_SITE_ID,
			v_FACILITY_ID,
			v_FK_CODELST_COVERTYPE,
      V_COVERAGE_NOTES
		); --KM

		IF C1%ROWCOUNT = 1 THEN
			Sp_Copy_Protocol_Visits(P_PROTOCOL, P_NEWPROTOCOL, P_USERID,P_IP, V_RET); --SV, 09/30, copy "all" the visits from old to new calendar.
		end if;

		IF C1%ROWCOUNT != 1 THEN

			-- SV, 9/30, copy the visit for the event to the new protocol calendar and update the event record with the visit id. Then update the new event record with new visit id.
 			IF (v_event_visit_id > 0) THEN

 				--Get the visit no to differentiate between 2 events with same displacement
 				SELECT visit_no INTO v_visit_num FROM SCH_PROTOCOL_VISIT WHERE pk_protocol_visit=v_event_visit_id;

				--SV, 09/30
				Sp_Get_Prot_Visit(P_NEWPROTOCOL,DISPLACEMENT, P_USERID,P_IP, v_ret,v_visit_num);

				IF (v_ret > 0) THEN
 					UPDATE EVENT_ASSOC
 					SET FK_VISIT = v_ret
 					WHERE EVENT_ID = P_CURRVAL;
				END IF;
	 		END IF;
 	 		
 	 		--Modified by Sonia Abrol, 06/05/06, send '0' as propagation flag, do not propagate in case of copy calendars
 			Sp_Cpevedtls(event_id,p_currval,'s',p_status,TO_CHAR(p_userid),p_ip, 0);
		END IF;
 	END LOOP;
	CLOSE C1;

	/**********************************
	TEST
	set serveroutput on
	declare
	newProtId number ;
	begin
	sp_addprottostudy(7739,1850,newProtId) ;
	dbms_output.put_line(to_char(newProtId) ) ;
	end ;
	**************************************/
	COMMIT;
END;
/

--2nd Procedure
create or replace
PROCEDURE             "SP_COPYPROTOCOL" (
   P_PROTOCOL      IN       NUMBER,
   P_NAME          IN       VARCHAR,
   p_cal_type	   IN	    NUMBER,--JM:
   P_NEWPROTOCOL   OUT      NUMBER,
   P_USER          IN       VARCHAR,
   P_IPADD         IN       VARCHAR
)
AS
/****************************************************************************************************
** Procedure to copy a protocol with a different name

** user can copy an existing protocol with a different name

** Parameter Description

** p_protocol     event id of the protocol that is to be copied
** P_name         new name of the protocol
** P_newProtocol  protocol id of the new copied protocol

**
** Author: Dinesh Khurana 15th June 2001
**
** Modification History
**
** Modified By		Date		Remarks
** Dinesh			26June		(1)To add events attributes along with event details
								(2) To return status of new protocol as 'Work in Progress'
** SamV				Sep092004	added code to handle sharedwith option for Calendar.
** Sam V			Sep292004	Added code to copy Duration Unit, Calendar SharedWith
** Sonia Abrol					copy new fields in event details
** JM				21May2007	p_cal_type calendar type, the one user has selected from the GUI, it may be same or different than the existing one
** Manimaran 					new field in copy event details
** Sammie			30Apr2010	new columns SERVICE_SITE_ID,FACILITY_ID,FK_CODELST_COVERTYPE
** Bikash			28jul2010	new columns COVERAGE_NOTES
****************************************************************************************************************************************************
*/
EVENT_ID           NUMBER;
CHAIN_ID           NUMBER;
EVENT_TYPE         CHAR (1);
NAME               VARCHAR2 (4000); --KM
NOTES              VARCHAR2 (1000);
COST               NUMBER;
COST_DESCRIPTION   VARCHAR2 (200);
DURATION           NUMBER;
USER_ID            NUMBER;
LINKED_URI         VARCHAR2 (200);
FUZZY_PERIOD       VARCHAR2 (10);
MSG_TO             CHAR (1);
STATUS             CHAR (1);
DESCRIPTION        VARCHAR2 (1000);
DISPLACEMENT       NUMBER;
EVENTMSG           VARCHAR2 (2000);
EVENTRES           CHAR (1);
EVENTFLAG          NUMBER;

PAT_DAYSBEFORE     NUMBER (3);
PAT_DAYSAFTER      NUMBER (3);
USR_DAYSBEFORE     NUMBER (3);
USR_DAYSAFTER      NUMBER (3);
PAT_MSGBEFORE      VARCHAR2 (4000);
PAT_MSGAFTER       VARCHAR2 (4000);
USR_MSGBEFORE      VARCHAR2 (4000);
USR_MSGAFTER       VARCHAR2 (4000);

ORG_ID             NUMBER;
P_CHAIN_ID         NUMBER;
P_CURRVAL          NUMBER;
P_COUNT            NUMBER;
P_USER_ID          NUMBER;
P_STATUS           NUMBER;
V_DURATION_UNIT	  CHAR(1); --SV, 9/29
V_CALENDAR_SHAREDWITH  CHAR(1); --SV, 9/29

V_CAL_SHAREWITH CHAR(1); -- RK, 01/28/05
v_objectids VARCHAR2(1000);

V_EVENT_VISIT_ID	  NUMBER; --SV, 9/30
V_RET			  NUMBER;

V_CATLIB NUMBER;

v_visit_num NUMBER;

v_event_fuzzyafter VARCHAR2(10);
v_event_durationafter CHAR(1);
v_event_cptcode  VARCHAR2(255);
v_event_durationbefore CHAR(1);
event_category varchar2(100); --KM,4/18
event_sequence number(22);--KM

v_event_library_type Number;
v_event_line_category Number;
v_SERVICE_SITE_ID Number;
v_FACILITY_ID Number;
v_FK_CODELST_COVERTYPE Number;

v_new_protocolPK Number;
V_COVERAGE_NOTES VARCHAR2(4000); --BK 28JUL2010 SWFIN5b

pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SP_copyprotocol', pLEVEL  => Plog.LDEBUG);

-- The new protocol id generated from the EVENT_DEFINIITON_SEQ sequence. This is stored in the
-- p_newProtocol
-- This variable is an OUT parameter
--KM-041808
	CURSOR C1
	IS
		SELECT EVENT_ID, CHAIN_ID, EVENT_TYPE, NAME, NOTES, COST,
		COST_DESCRIPTION, DURATION, USER_ID, LINKED_URI,
		FUZZY_PERIOD, MSG_TO, STATUS, DESCRIPTION, DISPLACEMENT,
		ORG_ID, EVENT_MSG, EVENT_RES, EVENT_FLAG, PAT_DAYSBEFORE,
		PAT_DAYSAFTER, USR_DAYSBEFORE, USR_DAYSAFTER, PAT_MSGBEFORE,
		PAT_MSGAFTER, USR_MSGBEFORE, USR_MSGAFTER, CALENDAR_SHAREDWITH, DURATION_UNIT, FK_VISIT,FK_CATLIB,
		event_fuzzyafter, event_durationafter, event_cptcode,event_durationbefore,event_category,event_sequence,
		event_library_type,event_line_category,
		SERVICE_SITE_ID,FACILITY_ID,FK_CODELST_COVERTYPE,COVERAGE_NOTES
		FROM EVENT_DEF
		WHERE CHAIN_ID = P_PROTOCOL
		ORDER BY EVENT_TYPE DESC, COST, DISPLACEMENT;
	BEGIN

	-- to check if the protocol name already exists in the user Protocol library
	SELECT USER_ID
	INTO P_USER_ID
	FROM EVENT_DEF
	WHERE EVENT_ID = P_PROTOCOL;
	SELECT COUNT (EVENT_ID)
	INTO P_COUNT
	FROM EVENT_DEF
	WHERE LOWER(trim(NAME)) = LOWER(trim(P_NAME)) --KM--to fix the Bug1756
	AND USER_ID = P_USER_ID
	AND EVENT_TYPE = 'P';

	IF P_COUNT > 0 THEN
		P_NEWPROTOCOL := -1;
		RETURN;
	END IF;

	OPEN C1;

	LOOP
		FETCH C1 INTO EVENT_ID,
		CHAIN_ID,
		EVENT_TYPE,
		NAME,
		NOTES,
		COST,
		COST_DESCRIPTION,
		DURATION,
		USER_ID,
		LINKED_URI,
		FUZZY_PERIOD,
		MSG_TO,
		STATUS,
		DESCRIPTION,
		DISPLACEMENT,
		ORG_ID,
		EVENTMSG,
		EVENTRES,
		EVENTFLAG,
		PAT_DAYSBEFORE,
		PAT_DAYSAFTER,
		USR_DAYSBEFORE,
		USR_DAYSAFTER,
		PAT_MSGBEFORE,
		PAT_MSGAFTER,
		USR_MSGBEFORE,
		USR_MSGAFTER,
		V_CALENDAR_SHAREDWITH,
		V_DURATION_UNIT, V_EVENT_VISIT_ID,V_CATLIB,
		v_event_fuzzyafter, v_event_durationafter, v_event_cptcode,v_event_durationbefore,event_category,event_sequence,
		v_event_library_type,v_event_line_category,
		v_SERVICE_SITE_ID,v_FACILITY_ID,v_FK_CODELST_COVERTYPE,V_COVERAGE_NOTES;

		EXIT WHEN C1%NOTFOUND;

		SELECT EVENT_DEFINITION_SEQ.NEXTVAL
		INTO P_CURRVAL
		FROM DUAL;

		IF C1%ROWCOUNT = 1 THEN
			/*
			Name for the Protocol as passed in the procedure, the name we get from the cursor will be the old
			name. Status 'W' for the Protocol and from 2nd row onwards events will have their own status
			P_Chain_Id will remain the same for all the records inserted ie the id of the first protocol
			The new protocol is is returned from the procedure.
			*/
			NAME := P_NAME;
			V_CATLIB := p_cal_type;--JM:
			STATUS := 'W';
			P_CHAIN_ID := P_CURRVAL;
			P_NEWPROTOCOL := P_CHAIN_ID;

			v_new_protocolPK := P_CURRVAL;

			V_CAL_SHAREWITH := V_CALENDAR_SHAREDWITH;

			FOR i IN (SELECT fk_objectshare_id
				FROM er_objectshare
				WHERE fk_object = p_protocol AND
				objectshare_type = V_CAL_SHAREWITH
				)
			LOOP
				v_objectids := v_objectids || i.fk_objectshare_id || ',';
			END LOOP;
			v_objectids := SUBSTR(v_objectids,1,LENGTH(v_objectids)-1);
			Sp_Objectsharewith( P_NEWPROTOCOL,'3', TO_CHAR(v_objectids)  , V_CAL_SHAREWITH, TO_CHAR(p_user) , P_IPADD , v_ret, 'N' ); --SV, 9/29/04, cal-enh-01. 'N' at the end is to avoid commits inside proc.

		END IF;
		--event id of the the protocol generated is the chain id of all the child events
plog.DEBUG(pCTX,'EVENTID IS'||EVENT_ID||V_COVERAGE_NOTES);
		INSERT INTO EVENT_DEF(
			EVENT_ID,
			CHAIN_ID,
			EVENT_TYPE,
			NAME,
			NOTES,
			COST,
			COST_DESCRIPTION,
			DURATION,
			USER_ID,
			LINKED_URI,
			FUZZY_PERIOD,
			MSG_TO,
			STATUS,
			DESCRIPTION,
			DISPLACEMENT,
			ORG_ID,
			EVENT_MSG,
			EVENT_RES,
			EVENT_FLAG,
			PAT_DAYSBEFORE,
			PAT_DAYSAFTER,
			USR_DAYSBEFORE,
			USR_DAYSAFTER,
			PAT_MSGBEFORE,
			PAT_MSGAFTER,
			USR_MSGBEFORE,
			USR_MSGAFTER, CREATOR,IP_ADD,
			CALENDAR_SHAREDWITH,
			DURATION_UNIT,FK_CATLIB,
			event_fuzzyafter, event_durationafter, event_cptcode,event_durationbefore,event_category,event_sequence,
			event_library_type,event_line_category,
			SERVICE_SITE_ID,FACILITY_ID,FK_CODELST_COVERTYPE,COVERAGE_NOTES
		) VALUES(
			P_CURRVAL,
			P_CHAIN_ID,
			EVENT_TYPE,
			NAME,
			NOTES,
			COST,
			COST_DESCRIPTION,
			DURATION,
			USER_ID,
			LINKED_URI,
			FUZZY_PERIOD,
			MSG_TO,
			STATUS,
			DESCRIPTION,
			DISPLACEMENT,
			ORG_ID,
			EVENTMSG,
			EVENTRES,
			EVENTFLAG,
			PAT_DAYSBEFORE,
			PAT_DAYSAFTER,
			USR_DAYSBEFORE,
			USR_DAYSAFTER,
			PAT_MSGBEFORE,
			PAT_MSGAFTER,
			USR_MSGBEFORE,
			USR_MSGAFTER,TO_CHAR(p_user),P_IPADD,
			V_CALENDAR_SHAREDWITH,
			V_DURATION_UNIT,V_CATLIB,
			v_event_fuzzyafter, v_event_durationafter, v_event_cptcode,v_event_durationbefore,event_category,event_sequence,
			v_event_library_type,v_event_line_category,
			v_SERVICE_SITE_ID,v_FACILITY_ID,v_FK_CODELST_COVERTYPE,V_COVERAGE_NOTES
		);

		IF C1%ROWCOUNT = 1 THEN
			--copy all visits
			Sp_Copy_Protocol_Visits(P_PROTOCOL, P_NEWPROTOCOL, P_USER, P_IPADD, V_RET); --SV, 09/30, copy "all" the visits from old to new calendar.
		end if;

		IF C1%ROWCOUNT != 1 THEN

			-- SV, 9/30, copy the visit for the event to the new protocol calendar and update the event record with the visit id. Then update the new event record with new visit id.
			IF (v_event_visit_id > 0) THEN

				--Get the visit no to differentiate between 2 events with same displacement
				SELECT visit_no INTO v_visit_num FROM SCH_PROTOCOL_VISIT  WHERE pk_protocol_visit=v_event_visit_id;

				--plog.DEBUG(pCTX,'visit_no'||v_visit_num||'Visit'||v_event_visit_id);

				Sp_Get_Prot_Visit(P_NEWPROTOCOL,DISPLACEMENT, P_USER, P_IPADD, v_ret,v_visit_num);
				IF (v_ret > 0) THEN
					UPDATE EVENT_DEF
					SET FK_VISIT = v_ret
					WHERE EVENT_ID = P_CURRVAL;

					-- plog.DEBUG(pCTX,'v_ret-new fk_visit'||v_ret);
				END IF;
			END IF;
			/*
			call the cpevedtls procedure to copy the details of the events associated with the protocol.
			*/
			Sp_Cpevedtls (EVENT_ID, P_CURRVAL,'C', P_STATUS, P_USER, P_IPADD,0);

		END IF;
	END LOOP;
	CLOSE C1;
	/**********************************
	TEST
	set serveroutput on
	declare
	myevent_id number ;

	begin
	sp_copyprotocol(5128,'checkup1',myevent_id) ;
	dbms_output.put_line(to_char(myevent_id) ) ;
	end ;
	**************************************/

	--SONIA , apparently P_NEWPROTOCOL was getting reset somewhere,set it again

	P_NEWPROTOCOL := v_new_protocolPK ;
	COMMIT;
END;
/

--3rd Procedure
create or replace
PROCEDURE "SP_CPROTINSTUDY" (

   p_protocol IN NUMBER ,
   p_protname IN VARCHAR ,
   p_userid IN NUMBER ,
   p_ip IN VARCHAR2 ,
   p_newprot OUT NUMBER,
   p_accId IN NUMBER
)
AS
/************************************************************************************************
**  Author: Charanjiv S Kalha 14th Sept 2001
**
** This will copy an existing protocols in a study to the same study with a diferent name
** Paramters
** p_prodotcol - the protocl which has to be copied to the new study
** p_protname the name of the new protocol ,
** p_userid the name of the user who is doing the task
** p_ip the ip address
** p_newprot the OUT paramter wich will be "-1" if the name is found to be already existing
**  or the protocol id which will be generated
**
** Modification History
**
** Modified By		Date		Remarks
** Sonia Abrol		06/02/06
** JM				29Apr2008	modified to add event_category and event_sequence
** Sammie			30Apr2010	SW_FIN1
** Mukul						BUG 3335
** Bikash			29jul2010	SW_FIN5b
*************************************************************************************************
*/
   v_CHAIN_ID         NUMBER;
   v_CURRVAL          NUMBER;
   v_COUNT            NUMBER;
   v_USER_ID          NUMBER;
   v_STATUS           NUMBER;
   v_NEWPROTOCOL      NUMBER;
   v_ret NUMBER;
-- The new protocol id generated from the EVENT_DEFINIITON_SEQ sequence. This is stored in the
-- p_newProtocol
-- This variable is an OUT parameter

-- This protocol should give all the protocols attached to a study

BEGIN
	BEGIN
		SELECT COUNT(event_id)
		INTO v_status
		FROM EVENT_ASSOC
		WHERE  chain_id = (SELECT chain_id FROM EVENT_ASSOC WHERE event_id = p_protocol )
		AND event_type = 'P'
		AND LOWER(trim(NAME)) = LOWER(trim(p_protname)) ;--KM--to fix the Bug 1756

		IF v_status > 0 THEN
			p_newprot := -1 ;
			RETURN ;
		END IF ;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
			NULL ;
	END ;


	SELECT EVENT_DEFINITION_SEQ.NEXTVAL
	INTO p_newprot
	FROM DUAL;

	INSERT INTO EVENT_ASSOC
	(
		EVENT_ID, CHAIN_ID, EVENT_TYPE, NAME, NOTES, COST,
		COST_DESCRIPTION, DURATION, USER_ID, LINKED_URI,
		FUZZY_PERIOD, MSG_TO, STATUS, DESCRIPTION, DISPLACEMENT,
		ORG_ID, EVENT_MSG, EVENT_RES, EVENT_FLAG, PAT_DAYSBEFORE,
		PAT_DAYSAFTER, USR_DAYSBEFORE, USR_DAYSAFTER, PAT_MSGBEFORE,
		PAT_MSGAFTER, USR_MSGBEFORE, USR_MSGAFTER, STATUS_DT,
		STATUS_CHANGEBY, CREATOR,IP_ADD,FK_CATLIB ,EVENT_CALASSOCTO,
		DURATION_UNIT, event_fuzzyafter, event_durationafter, event_cptcode,event_durationbefore, event_category,
		event_sequence,event_library_type, event_line_category,
		SERVICE_SITE_ID,FACILITY_ID,FK_CODELST_COVERTYPE,COVERAGE_NOTES
	)
	(
		SELECT p_newprot , CHAIN_ID, EVENT_TYPE, p_protname , NOTES, COST,
		COST_DESCRIPTION, DURATION, p_accId, LINKED_URI,
		FUZZY_PERIOD, MSG_TO, 'W', DESCRIPTION, DISPLACEMENT,
		ORG_ID, EVENT_MSG, EVENT_RES, EVENT_FLAG, PAT_DAYSBEFORE,
		PAT_DAYSAFTER, USR_DAYSBEFORE, USR_DAYSAFTER, PAT_MSGBEFORE,
		PAT_MSGAFTER, USR_MSGBEFORE, USR_MSGAFTER, SYSDATE STATUS_DT,
		p_userid, p_userid,P_ip,FK_CATLIB,EVENT_CALASSOCTO,
		DURATION_UNIT, event_fuzzyafter, event_durationafter, event_cptcode,event_durationbefore, event_category,
		event_sequence,event_library_type, event_line_category,
		SERVICE_SITE_ID,FACILITY_ID,FK_CODELST_COVERTYPE,COVERAGE_NOTES
		FROM EVENT_ASSOC
		WHERE EVENT_ID = p_protocol
		AND event_type = 'P'
	) ;

 ---
 --- call to procedure to copy all events of the protocol just copied, parameters are
 --- the old protocol id, and the new protocol (event id)
 --- Creator added in insert query to fix Bug 3335
 --- Last_modified column removed from Insert query to fix BUG 3335

   Sp_Cpprotevents(p_protocol , p_newprot, p_userid, p_ip ) ;
   COMMIT;
END;
/


--4th Procedure
create or replace
PROCEDURE        "SP_CPPROTEVENTS" (
   P_oldprot NUMBER ,
   p_newprot NUMBER ,
   p_userid  NUMBER,
   p_ip IN VARCHAR2
)
AS

/** Author: Charanjiv S Kalha 23rd Aug 2001
**  This procedure copies the events of one protocol to another.
** the input parameters are the id of the old and new protocols and hte user id of the
** user making the changes, ip
**
** Modification History
**
**Modified By		Date		Remarks
**Sonika Talwar		10March04	Added ip in the input parameter
**								The orig_id field incase of copy calendar event would
**								store the event id whose copy is being made
**Sonia Abrol		06/06/06	passed a new parameter to sp_cpefedtls to not to copy propagate_from
**JM				29Apr2008	modified to add event_category and event_sequence
**Sammie			30Apr2010	added new columns SERVICE_SITE_ID,FACILITY_ID,FK_CODELST_COVERTYPE
**Rohit				03May2010   added creator column
**Bikash      29jul2010   SW-FIN5b
*/

V_STATUS NUMBER ;
V_CURRVAL NUMBER ;
v_ret NUMBER;
v_visit_num NUMBER;

-- This cursor gets all the events of the old protocol, these are then inserted for the
-- new protocol. The details for these events are copied using the cp_evedtls proc.

CURSOR cur_eventprot
IS
	SELECT EVENT_ID, CHAIN_ID, EVENT_TYPE, NAME, NOTES, COST,
	COST_DESCRIPTION, DURATION, USER_ID, LINKED_URI,
	FUZZY_PERIOD, MSG_TO, STATUS, DESCRIPTION, DISPLACEMENT,
	ORG_ID, EVENT_MSG, EVENT_RES, EVENT_FLAG, PAT_DAYSBEFORE,
	PAT_DAYSAFTER, USR_DAYSBEFORE, USR_DAYSAFTER, PAT_MSGBEFORE,
	PAT_MSGAFTER, USR_MSGBEFORE, USR_MSGAFTER, STATUS_DT, STATUS_CHANGEBY, fk_visit,fk_catlib,
	DURATION_UNIT, event_fuzzyafter, event_durationafter, event_cptcode,event_durationbefore, event_category, event_sequence,
	hide_flag, event_library_type ,event_line_category,
	SERVICE_SITE_ID,FACILITY_ID,FK_CODELST_COVERTYPE,COVERAGE_NOTES
	FROM EVENT_ASSOC
	WHERE CHAIN_ID = p_oldprot AND event_type = 'A' ;
BEGIN

	Sp_Copy_Protocol_Visits(p_oldprot, p_newprot, P_USERID, P_IP, V_RET);

	FOR studyprot_rec IN cur_eventprot LOOP
		SELECT EVENT_DEFINITION_SEQ.NEXTVAL
		INTO V_CURRVAL FROM dual ;

		--The orig_id field incase of copy calendar event would store the event id whose copy is being made
		INSERT INTO EVENT_ASSOC
		(
			EVENT_ID, CHAIN_ID, EVENT_TYPE, NAME, NOTES,
			COST, COST_DESCRIPTION, DURATION, USER_ID, LINKED_URI,
			FUZZY_PERIOD, MSG_TO, STATUS, DESCRIPTION, DISPLACEMENT,
			ORG_ID, EVENT_MSG, EVENT_RES,
			EVENT_FLAG,
			PAT_DAYSBEFORE,
			PAT_DAYSAFTER, USR_DAYSBEFORE, USR_DAYSAFTER, PAT_MSGBEFORE,
			PAT_MSGAFTER, USR_MSGBEFORE, USR_MSGAFTER, STATUS_DT,
			STATUS_CHANGEBY,fk_catlib, DURATION_UNIT, event_fuzzyafter, event_durationafter, event_cptcode,event_durationbefore,
			event_category, event_sequence, hide_flag,
			event_library_type ,event_line_category,CREATOR,
			SERVICE_SITE_ID,FACILITY_ID,FK_CODELST_COVERTYPE,COVERAGE_NOTES
		)
		VALUES
		(
			v_currval , p_newprot, studyprot_rec.EVENT_TYPE, studyprot_rec.NAME,
			studyprot_rec.NOTES, studyprot_rec.COST, studyprot_rec.COST_DESCRIPTION,
			studyprot_rec.DURATION, studyprot_rec.USER_ID, studyprot_rec.LINKED_URI,
			studyprot_rec.FUZZY_PERIOD, studyprot_rec.MSG_TO, studyprot_rec.status,
			studyprot_rec.DESCRIPTION, studyprot_rec.DISPLACEMENT,
			studyprot_rec.EVENT_ID,studyprot_rec.EVENT_MSG, studyprot_rec.EVENT_RES,
			studyprot_rec.EVENT_FLAG,
			studyprot_rec.PAT_DAYSBEFORE, studyprot_rec.PAT_DAYSAFTER,
			studyprot_rec.USR_DAYSBEFORE, studyprot_rec.USR_DAYSAFTER,
			studyprot_rec.PAT_MSGBEFORE, studyprot_rec.PAT_MSGAFTER,
			studyprot_rec.USR_MSGBEFORE, studyprot_rec.USR_MSGAFTER,
			SYSDATE , p_userid,studyprot_rec.FK_CATLIB,
			studyprot_rec.DURATION_UNIT, studyprot_rec.event_fuzzyafter, studyprot_rec.event_durationafter, studyprot_rec.event_cptcode,studyprot_rec.event_durationbefore,
			studyprot_rec.event_category, studyprot_rec.event_sequence,studyprot_rec.hide_flag,
			studyprot_rec.event_library_type ,studyprot_rec.event_line_category,p_userid,
			studyprot_rec.SERVICE_SITE_ID,studyprot_rec.FACILITY_ID,studyprot_rec.FK_CODELST_COVERTYPE,studyprot_rec.COVERAGE_NOTES
		);

		-- the SP_CPEVEDTLS procedure is call once for all the events of the procedure.
		-- the parameters are
		-- event_id  IN, event whoes details are to be copied
		-- v_currval IN, the id of the new event in which the details are to be copied.
		-- v_status - out parameter.
		-- creater column added in insert query with values p_userid for Bug 3335

		IF (studyprot_rec.fk_visit > 0) THEN

			--Get the visit no to differentiate between 2 events with same displacement
			SELECT visit_no INTO v_visit_num FROM SCH_PROTOCOL_VISIT  WHERE pk_protocol_visit=studyprot_rec.fk_visit;
			Sp_Get_Prot_Visit(P_NEWPROT,studyprot_rec.DISPLACEMENT, P_USERid, P_IP, v_ret,v_visit_num);
			IF (v_ret > 0) THEN
				UPDATE EVENT_ASSOC
				SET FK_VISIT = v_ret
				WHERE EVENT_ID = v_currval;
			END IF;

		END IF;

		Sp_Cpevedtls (studyprot_rec.EVENT_ID, V_CURRVAL,'S', V_STATUS,P_USERId, P_IP,0);
	END LOOP ;
END ;
/


--5th Procedure
create or replace
PROCEDURE        "SP_COPYPROTTOLIB" (
p_protocol IN NUMBER,
p_studyId IN NUMBER,
p_protname IN VARCHAR,
p_protdesc IN VARCHAR,
p_prottype IN NUMBER,
p_protsharewith VARCHAR,
p_userid IN NUMBER ,
P_newProtocol OUT NUMBER,
p_ip IN VARCHAR
)
AS


/****************************************************************************************************
** Procedure to copy a protocol from a study back to library


** Parameter Description

** p_protocol event id of the protocol that is to be copied in a protocol
** p_studyId study id of the study in which protocol is to be copied
** P_newProtocol protocol id of the new copied protocol

**Modified by		Date		Reason
**Sonia Abrol		06/06/06	do not send propagate_from while copying events
**Manimaran			04/18/08	to add new columns
**Sammie			04/30/2010	added new columns SERVICE_SITE_ID,FACILITY_ID,FK_CODELST_COVERTYPE
**Bikash      29jul2010   SW-FIN5b added new column COVERAGE_NOTES
*****************************************************************************************************
*/

event_id NUMBER;
chain_id NUMBER;
event_type CHAR(1);
NAME VARCHAR2(4000);--KM-08Sep09
notes VARCHAR2(1000);
COST NUMBER;
cost_description VARCHAR2(200);
duration NUMBER;
user_id NUMBER;
linked_uri VARCHAR2(200);
fuzzy_period VARCHAR2(10);
msg_to CHAR(1);
status CHAR(1);
description VARCHAR2(1000);
displacement NUMBER;
eventMsg VARCHAR2(2000);
eventRes CHAR(1);
eventFlag NUMBER;

pat_daysbefore NUMBER(3);
pat_daysafter NUMBER(3);
usr_daysbefore NUMBER(3);
usr_daysafter NUMBER(3);
pat_msgbefore VARCHAR2(4000);
pat_msgafter VARCHAR2(4000);
usr_msgbefore VARCHAR2(4000);
usr_msgafter VARCHAR2(4000);

p_name VARCHAR2(4000);--KM-08Sep09
org_id NUMBER;
p_chain_id NUMBER;
p_currval NUMBER;
p_status NUMBER;
p_count NUMBER;

v_duration_unit CHAR(1); -- SV, 09/30/04, added to copy visits from old protocol to new study protocol.
v_event_visit_id NUMBER; -- SV, 09/30/04, fk_visit
v_ret NUMBER; -- SV, 09/30/04,
v_name VARCHAR2(4000);--KM-08Sep09
v_desc VARCHAR2(2000);
v_catlib NUMBER;
v_type NUMBER;
v_visit_num NUMBER;
--JM: 13Dec added
v_event_cptcode VARCHAR2(255);
v_event_fuzzyafter VARCHAR2(10);
v_event_durationafter CHAR(1);
v_event_durationbefore CHAR(1);
v_event_category varchar2(100); --KM
v_event_sequence number;

v_account number;
v_event_library_type number;
v_event_line_category number;

v_SERVICE_SITE_ID Number;
v_FACILITY_ID Number;
v_FK_CODELST_COVERTYPE Number;
v_COVERAGE_NOTES varchar2(4000);

-- The new protocol id generated from the EVENT_ASSOCIATION_SEQ sequence. This is stored in the -- p_chain_id
-- This variable is an OUT parameter

-- SV, 09/30/04, cal-enh- added code to copy duration_unit (type) and fk_visit (visits in event) to new protocol.
-- JM: 13Dec added EVENT_CPTCODE,EVENT_FUZZYAFTER, EVENT_DURATIONAFTER, EVENT_DURATIONBEFORE
CURSOR C1 IS SELECT EVENT_ID,
	CHAIN_ID,
	EVENT_TYPE,
	NAME,
	NOTES,
	COST,
	COST_DESCRIPTION,
	DURATION,
	USER_ID,
	LINKED_URI,
	FUZZY_PERIOD,
	MSG_TO,
	STATUS,
	DESCRIPTION,
	DISPLACEMENT,
	ORG_ID ,
	EVENT_MSG,
	EVENT_RES,
	EVENT_FLAG,
	PAT_DAYSBEFORE,
	PAT_DAYSAFTER ,
	USR_DAYSBEFORE,
	USR_DAYSAFTER ,
	PAT_MSGBEFORE ,
	PAT_MSGAFTER ,
	USR_MSGBEFORE ,
	USR_MSGAFTER,
	DURATION_UNIT,
	EVENT_CPTCODE,EVENT_FUZZYAFTER, EVENT_DURATIONAFTER, EVENT_DURATIONBEFORE,
	FK_VISIT,FK_CATLIB,EVENT_CATEGORY,EVENT_SEQUENCE,event_library_type,event_line_category,
	SERVICE_SITE_ID,FACILITY_ID,FK_CODELST_COVERTYPE,COVERAGE_NOTES
	FROM EVENT_ASSOC WHERE (chain_id = p_protocol OR event_id=p_protocol) ORDER BY event_type DESC ,COST,displacement;
BEGIN

	-- to check if the protocol name already exists in the study

	SELECT NAME ,fk_account INTO p_name ,v_account FROM EVENT_ASSOC ,er_study WHERE event_id=p_protocol and pk_study=chain_id;
	--KM--to fix the Bug1756
	SELECT COUNT(event_id) INTO p_count FROM EVENT_DEF WHERE LOWER(trim(NAME)) = LOWER(trim(p_protname)) AND EVENT_TYPE='P'
	and user_id = v_account ;

	IF p_count > 0 THEN
		p_newProtocol:=-1;
		RETURN;
	END IF;

	OPEN C1;

	LOOP
		--SV, 09/30/04, ++ duration_unit, fk_vist
		--JM: added v_event_cptcode,v_event_fuzzyafter,v_event_durationafter,v_event_durationbefore,
		--KM: added v_event_category,v_event_sequence
		FETCH C1 INTO event_id, chain_id , event_type, NAME, notes, COST, cost_description,
		duration, user_id, linked_uri, fuzzy_period, msg_to, status,description, displacement,
		org_id,eventMsg,eventRes, eventFlag,pat_daysbefore,pat_daysafter,usr_daysbefore,
		usr_daysafter,pat_msgbefore,pat_msgafter,usr_msgbefore,usr_msgafter, v_duration_unit,
		v_event_cptcode,v_event_fuzzyafter,v_event_durationafter,v_event_durationbefore,
		v_event_visit_id,v_catlib,v_event_category,v_event_sequence ,v_event_library_type,v_event_line_category,
		v_SERVICE_SITE_ID,v_FACILITY_ID,v_FK_CODELST_COVERTYPE,v_coverage_notes;

		SELECT EVENT_DEFINITION_SEQ.NEXTVAL INTO p_currval FROM dual;

		IF C1%ROWCOUNT = 1 THEN
			p_chain_id:=p_currval;
			p_newProtocol:=p_currval;
			status:='W';
		ELSE
			p_chain_id:=p_newProtocol;
		END IF;

		IF (event_type='P') THEN
			v_name:=p_protname;
			v_desc:=p_protdesc;
			v_type:=p_prottype;
		ELSE
			v_name:=NAME;
			v_desc:=description;
			v_type:=v_catlib;
		END IF;

		--event id of the the protocol generated is the chain id of all the child events

		EXIT WHEN C1%NOTFOUND;
		--SV, ++ duration unit. visits handled separately below.
		--JM: added EVENT_CPTCODE,EVENT_FUZZYAFTER, EVENT_DURATIONAFTER, EVENT_DURATIONBEFORE,
		--JM: added v_event_cptcode,v_event_fuzzyafter,v_event_durationafter,v_event_durationbefore,
		INSERT INTO EVENT_DEF(
			EVENT_ID,
			CHAIN_ID,
			EVENT_TYPE,
			NAME,
			NOTES,
			COST,
			COST_DESCRIPTION,
			DURATION,
			USER_ID,
			LINKED_URI,
			FUZZY_PERIOD,
			MSG_TO,
			STATUS,
			DESCRIPTION,
			DISPLACEMENT,
			ORG_ID ,
			EVENT_MSG,
			EVENT_RES,
			EVENT_FLAG,
			PAT_DAYSBEFORE,
			PAT_DAYSAFTER ,
			USR_DAYSBEFORE,
			USR_DAYSAFTER ,
			PAT_MSGBEFORE ,
			PAT_MSGAFTER ,
			USR_MSGBEFORE ,
			USR_MSGAFTER ,
			CREATOR,
			DURATION_UNIT,
			EVENT_CPTCODE,EVENT_FUZZYAFTER, EVENT_DURATIONAFTER, EVENT_DURATIONBEFORE,
			calendar_sharedwith,fk_catlib,event_category,event_sequence, event_library_type,event_line_category,
			SERVICE_SITE_ID,FACILITY_ID,FK_CODELST_COVERTYPE,COVERAGE_NOTES
		) VALUES(
			p_currval,
			p_chain_id ,
			event_type,
			v_name,
			notes,
			COST,
			cost_description,
			duration,
			user_id,
			linked_uri,
			fuzzy_period,
			msg_to,
			status,
			v_desc,
			displacement,
			org_id,
			eventMsg,
			eventRes,
			eventFlag,
			pat_daysbefore,
			pat_daysafter,
			usr_daysbefore,
			usr_daysafter,
			pat_msgbefore,
			pat_msgafter ,
			usr_msgbefore,
			usr_msgafter ,
			p_userid,
			v_duration_unit,
			v_event_cptcode,v_event_fuzzyafter,v_event_durationafter,v_event_durationbefore,
			p_protsharewith
			,v_type,
			v_event_category,
			v_event_sequence,v_event_library_type, v_event_line_category,
			v_SERVICE_SITE_ID,v_FACILITY_ID,v_FK_CODELST_COVERTYPE,v_coverage_notes
		); --KM

		IF C1%ROWCOUNT = 1 THEN
			Sp_Copy_Protocol_Visits(P_PROTOCOL, P_NEWPROTOCOL, P_USERID, P_IP, V_RET); --SV, 09/30, copy "all" the visits from old to new calendar.
		END IF;

		IF C1%ROWCOUNT != 1 THEN

		 	IF (v_event_visit_id > 0) THEN
				--Get the visit no to differentiate between 2 events with same displacement
				SELECT visit_no INTO v_visit_num FROM SCH_PROTOCOL_VISIT WHERE pk_protocol_visit=v_event_visit_id;

				Sp_Get_Prot_Visit(P_NEWPROTOCOL,DISPLACEMENT, P_USERID, P_IP, v_ret,v_visit_num);

				IF (v_ret > 0) THEN
					UPDATE EVENT_DEF
					SET FK_VISIT = v_ret
					WHERE EVENT_ID = P_CURRVAL;
				END IF;
			END IF;

			Sp_Cpevedtls(event_id,p_currval,'s',p_status,TO_CHAR(p_userid),p_ip, 0);
		END IF;
	END LOOP;
	CLOSE C1;

	/**********************************
	TEST
	set serveroutput on
	declare
	newProtId number ;
	begin
	sp_addprottostudy(7739,1850,newProtId) ;
	dbms_output.put_line(to_char(newProtId) ) ;
	end ;
	**************************************/
	COMMIT;
END;
/

--6th Procedure
create or replace
PROCEDURE "SP_COPYEVENTTOCAT" (
   P_EVENT        IN   NUMBER,
   P_WHICHTABLE   IN   NUMBER,
   P_CATEGORY     IN   NUMBER,
   P_EVTLIBTYPE   IN   NUMBER,
   P_NAME         IN   VARCHAR2,
   P_USER         IN   VARCHAR,
   P_IPADD        IN   VARCHAR
)
AS

   P_CURRVAL   NUMBER;
   P_STATUS    NUMBER;
   P_EVELINECATEGORY NUMBER;

   --Modified by Manimaran to copy Event Library Type --02Sep09
   --BK 29JUL2010 ADDED NEW FIELD COVERAGE NOTES FOR SW-FIN5b
   CURSOR CUR_DEF
   IS
      SELECT EVENT_DEFINITION_SEQ.NEXTVAL, P_CATEGORY, P_EVTLIBTYPE,'E', P_NAME,
             NOTES, COST, COST_DESCRIPTION, DURATION, USER_ID,
             LINKED_URI, FUZZY_PERIOD, MSG_TO, STATUS, DESCRIPTION,
             DISPLACEMENT, ORG_ID, EVENT_MSG, EVENT_RES, CREATED_ON,
             EVENT_FLAG, PAT_DAYSBEFORE, PAT_DAYSAFTER, USR_DAYSBEFORE,
             USR_DAYSAFTER, PAT_MSGBEFORE, PAT_MSGAFTER, USR_MSGBEFORE,
             USR_MSGAFTER,EVENT_FUZZYAFTER,EVENT_DURATIONAFTER,EVENT_CPTCODE,
             EVENT_DURATIONBEFORE,DURATION_UNIT,FK_CATLIB,P_IPADD,EVENT_LINE_CATEGORY,
             SERVICE_SITE_ID,FACILITY_ID,FK_CODELST_COVERTYPE,COVERAGE_NOTES 
        FROM EVENT_DEF
       WHERE EVENT_ID = P_EVENT;


   CURSOR CUR_ASSOC
   IS
      SELECT EVENT_DEFINITION_SEQ.NEXTVAL, P_CATEGORY, P_EVTLIBTYPE,'E', P_NAME,
      		 NOTES, COST, COST_DESCRIPTION, DURATION, USER_ID, LINKED_URI,
             FUZZY_PERIOD, MSG_TO, STATUS, DESCRIPTION, DISPLACEMENT,
             ORG_ID, EVENT_MSG, EVENT_RES, CREATED_ON, EVENT_FLAG,
             PAT_DAYSBEFORE, PAT_DAYSAFTER, USR_DAYSBEFORE, USR_DAYSAFTER,
             PAT_MSGBEFORE, PAT_MSGAFTER, USR_MSGBEFORE, USR_MSGAFTER,
             EVENT_FUZZYAFTER,EVENT_DURATIONAFTER,EVENT_CPTCODE,
             EVENT_DURATIONBEFORE,DURATION_UNIT,FK_CATLIB,P_IPADD,EVENT_LINE_CATEGORY,
             SERVICE_SITE_ID,FACILITY_ID,FK_CODELST_COVERTYPE,COVERAGE_NOTES 
        FROM EVENT_ASSOC
       WHERE EVENT_ID = P_EVENT;
-- Added by Ganapathy on 06/01/05 BUG NO -2224 For Preventing duplicate event name
   		 EXP EXCEPTION;
		 counter NUMBER;

BEGIN

    SELECT EVENT_DEFINITION_SEQ.NEXTVAL
    INTO P_CURRVAL
    FROM DUAL;
   --KM -01Dec09
   SELECT EVENT_LINE_CATEGORY INTO P_EVELINECATEGORY FROM EVENT_DEF WHERE EVENT_ID = P_CATEGORY;

   IF (P_WHICHTABLE = 1) THEN
      FOR DEF_REC IN CUR_DEF
      LOOP
 -- Added by Ganapathy on 06/01/05 BUG NO -2224 For Preventing duplicate event name
 --KM -Modified for Event line category--20Nov09
SELECT COUNT(*) INTO COUNTER FROM EVENT_DEF WHERE LOWER(NAME)=TRIM(LOWER(P_NAME)) AND (USER_ID=DEF_REC.USER_ID AND EVENT_TYPE='E');

   IF(COUNTER>0) THEN
   RAISE EXP;
   ELSE

         INSERT INTO EVENT_DEF
                 (
				EVENT_ID,
				CHAIN_ID,
				EVENT_LIBRARY_TYPE,
				EVENT_TYPE,
				NAME,
				NOTES,
				COST,
				COST_DESCRIPTION,
				DURATION,
				USER_ID,
				LINKED_URI,
				FUZZY_PERIOD,
				MSG_TO,
				STATUS,
				DESCRIPTION,
				DISPLACEMENT,
				ORG_ID,
				EVENT_MSG,
				EVENT_RES,
				CREATED_ON,
				EVENT_FLAG,
				PAT_DAYSBEFORE,
				PAT_DAYSAFTER,
				USR_DAYSBEFORE,
				USR_DAYSAFTER,
				PAT_MSGBEFORE,
				PAT_MSGAFTER,
				USR_MSGBEFORE,
				USR_MSGAFTER,
				EVENT_FUZZYAFTER,
				EVENT_DURATIONAFTER,
				EVENT_CPTCODE,
				EVENT_DURATIONBEFORE,
				DURATION_UNIT,FK_CATLIB,IP_ADD,EVENT_LINE_CATEGORY,CREATOR,
				SERVICE_SITE_ID,FACILITY_ID,FK_CODELST_COVERTYPE,COVERAGE_NOTES
                 )
              VALUES(
				P_CURRVAL,
				P_CATEGORY,
				P_EVTLIBTYPE,
				'E',
				P_NAME,
				DEF_REC.NOTES,
				DEF_REC.COST,
				DEF_REC.COST_DESCRIPTION,
				DEF_REC.DURATION,
				DEF_REC.USER_ID,
				DEF_REC.LINKED_URI,
				DEF_REC.FUZZY_PERIOD,
				DEF_REC.MSG_TO,
				DEF_REC.STATUS,
				DEF_REC.DESCRIPTION,
				DEF_REC.DISPLACEMENT,
				DEF_REC.ORG_ID,
				DEF_REC.EVENT_MSG,
				DEF_REC.EVENT_RES,
				sysdate,
				DEF_REC.EVENT_FLAG,
				DEF_REC.PAT_DAYSBEFORE,
				DEF_REC.PAT_DAYSAFTER,
				DEF_REC.USR_DAYSBEFORE,
				DEF_REC.USR_DAYSAFTER,
				DEF_REC.PAT_MSGBEFORE,
				DEF_REC.PAT_MSGAFTER,
				DEF_REC.USR_MSGBEFORE,
				DEF_REC.USR_MSGAFTER,
				DEF_REC.EVENT_FUZZYAFTER,
				DEF_REC.EVENT_DURATIONAFTER,
				DEF_REC.EVENT_CPTCODE,
				DEF_REC.EVENT_DURATIONBEFORE,
				DEF_REC.DURATION_UNIT, DEF_REC.FK_CATLIB, DEF_REC.P_IPADD,P_EVELINECATEGORY,P_USER,
				DEF_REC.SERVICE_SITE_ID,DEF_REC.FACILITY_ID,DEF_REC.FK_CODELST_COVERTYPE,DEF_REC.COVERAGE_NOTES
              );
			  END IF;
      END LOOP;
   ELSE

      FOR ASSOC_REC IN CUR_ASSOC
      LOOP
	  -- Added by Ganapathy on 06/01/05 BUG NO -2224 For Preventing duplicate event name
    --KM-Modified for Event line category name
 SELECT COUNT(*) INTO COUNTER FROM EVENT_DEF WHERE LOWER(NAME)=TRIM(LOWER(P_NAME)) AND (USER_ID=ASSOC_REC.USER_ID AND EVENT_TYPE='E');

   IF(COUNTER>0) THEN
   RAISE EXP;
   ELSE
         INSERT INTO EVENT_DEF
			(
				EVENT_ID,
				CHAIN_ID,
				EVENT_LIBRARY_TYPE,
				EVENT_TYPE,
				NAME,
				NOTES,
				COST,
				COST_DESCRIPTION,
				DURATION,
				USER_ID,
				LINKED_URI,
				FUZZY_PERIOD,
				MSG_TO,
				STATUS,
				DESCRIPTION,
				DISPLACEMENT,
				ORG_ID,
				EVENT_MSG,
				EVENT_RES,
				CREATED_ON,
				EVENT_FLAG,
				PAT_DAYSBEFORE,
				PAT_DAYSAFTER,
				USR_DAYSBEFORE,
				USR_DAYSAFTER,
				PAT_MSGBEFORE,
				PAT_MSGAFTER,
				USR_MSGBEFORE,
				USR_MSGAFTER,
				EVENT_FUZZYAFTER,
				EVENT_DURATIONAFTER,
				EVENT_CPTCODE,
				EVENT_DURATIONBEFORE,
				DURATION_UNIT,FK_CATLIB,IP_ADD,EVENT_LINE_CATEGORY,CREATOR,
				SERVICE_SITE_ID,FACILITY_ID,FK_CODELST_COVERTYPE,COVERAGE_NOTES
			)
            VALUES(
				P_CURRVAL,
				P_CATEGORY,
				P_EVTLIBTYPE,
				'E',
				P_NAME,
				ASSOC_REC.NOTES,
				ASSOC_REC.COST,
				ASSOC_REC.COST_DESCRIPTION,
				ASSOC_REC.DURATION,
				ASSOC_REC.USER_ID,
				ASSOC_REC.LINKED_URI,
				ASSOC_REC.FUZZY_PERIOD,
				ASSOC_REC.MSG_TO,
				ASSOC_REC.STATUS,
				ASSOC_REC.DESCRIPTION,
				ASSOC_REC.DISPLACEMENT,
				ASSOC_REC.ORG_ID,
				ASSOC_REC.EVENT_MSG,
				ASSOC_REC.EVENT_RES,
				sysdate,
				ASSOC_REC.EVENT_FLAG,
				ASSOC_REC.PAT_DAYSBEFORE,
				ASSOC_REC.PAT_DAYSAFTER,
				ASSOC_REC.USR_DAYSBEFORE,
				ASSOC_REC.USR_DAYSAFTER,
				ASSOC_REC.PAT_MSGBEFORE,
				ASSOC_REC.PAT_MSGAFTER,
				ASSOC_REC.USR_MSGBEFORE,
				ASSOC_REC.USR_MSGAFTER,
				ASSOC_REC.EVENT_FUZZYAFTER,
				ASSOC_REC.EVENT_DURATIONAFTER,
				ASSOC_REC.EVENT_CPTCODE,
				ASSOC_REC.EVENT_DURATIONBEFORE,
				ASSOC_REC.DURATION_UNIT,
				ASSOC_REC.FK_CATLIB, ASSOC_REC.P_IPADD,P_EVELINECATEGORY, P_USER,
				ASSOC_REC.SERVICE_SITE_ID,ASSOC_REC.FACILITY_ID,ASSOC_REC.FK_CODELST_COVERTYPE,ASSOC_REC.COVERAGE_NOTES
              );
			     END IF;
      END LOOP;

   END IF;


   Sp_Cpevedtls (P_EVENT, P_CURRVAL,'E',P_STATUS,P_USER,P_IPADD,0);
   COMMIT;
   -- Added by Ganapathy on 06/01/05 BUG NO -2224 For Preventing duplicate event name
EXCEPTION
WHEN EXP THEN
RAISE_APPLICATION_ERROR(-20002,'DUPLICATE EVENT NAME');
END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,82,4,'04_Procedures.sql',sysdate,'8.9.0 Build#539');

commit;

