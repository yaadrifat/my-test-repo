SET DEFINE OFF;

DELETE from ER_REPXSL where FK_REPORT = 176; 

DELETE from ER_REPORT where PK_REPORT = 176; 

Insert into ERES.ER_REPORT
   (PK_REPORT, REP_NAME, REP_DESC, REP_SQL, FK_ACCOUNT, 
    FK_CODELST_TYPE, IP_ADD, REP_HIDE, REP_COLUMNS, REP_FILTERBY, 
    GENERATE_XML, REP_TYPE, REP_SQL_CLOB, REP_FILTERKEYWORD, REP_FILTERAPPLICABLE)
 Values
   (176, 'Calendar Change Log', 'Calendar Audit Trail Report', 'Select  ESCH.PKG_AUDIT_TRAIL_MODULE.RPT_CALENDARAUDITREPORT(~1,~2,~3,''~4'',''~5'',''~6'',''~7'',''~8'',''~9'') from dual', 0, 
    NULL, NULL, 'N', NULL, NULL, 
    0, 'rep_calendar', 'Select  ESCH.PKG_AUDIT_TRAIL_MODULE.RPT_CALENDARAUDITREPORT(~1,~2,~3,''~4'',''~5'',''~6'',''~7'',''~8'',''~9'') from dual', NULL, 'date');
COMMIT;
/




INSERT INTO track_patches
VALUES(seq_track_patches.nextval,184,15,'15_InsertScript_For_REPORT_Calendar.sql',sysdate,'9.0.1 Build#638');

commit;