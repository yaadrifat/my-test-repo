set define off;

UPDATE "ERES"."ER_REPORT" 
SET REP_FILTERKEYWORD = 'accessYN:budgetId:bgtCalendarId' 
WHERE PK_REPORT IN (108, 109, 110, 160);

Commit;

UPDATE "ERES"."ER_REPORT" 
SET REP_FILTERKEYWORD = 'budgetId' 
WHERE PK_REPORT IN (159);

Commit;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,184,3,'03_update_report_keywords.sql',sysdate,'9.0.1 Build#638');

commit;