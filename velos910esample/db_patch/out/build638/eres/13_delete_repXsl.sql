set define off;

DELETE FROM "ERES"."ER_REPXSL" WHERE FK_REPORT IN (110, 159);

Commit;
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,184,13,'13_delete_repXsl.sql',sysdate,'9.0.1 Build#638');

commit;