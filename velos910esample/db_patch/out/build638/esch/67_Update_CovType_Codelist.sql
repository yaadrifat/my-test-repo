set define off;

update sch_codelst set codelst_study_role = 'default_data' where codelst_type = 'coverage_type';

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,184,67,'67_Update_CovType_Codelist.sql',sysdate,'9.0.1 Build#638');

commit;