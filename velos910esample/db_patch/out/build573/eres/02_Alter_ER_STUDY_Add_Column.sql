set define off;
DECLARE countFlage1 NUMBER(5);
BEGIN
SELECT COUNT(1) INTO countFlage1 FROM USER_TABLES WHERE TABLE_NAME='ER_STUDY';
if (countFlage1 > 0) then
EXECUTE IMMEDIATE 'ALTER TABLE ER_STUDY ADD (FK_CODELST_SET_MILESTATUS NUMBER)' ;
end if;
END;
/
COMMENT ON COLUMN "ERES"."ER_STUDY"."FK_CODELST_SET_MILESTATUS" IS 'This column stores the Milestone status as a set';



INSERT INTO track_patches
VALUES(seq_track_patches.nextval,116,2,'02_Alter_ER_STUDY_Add_Column.sql',sysdate,'8.10.0 Build#573');

commit;