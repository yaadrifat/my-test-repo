set define off;

update sch_codelst set codelst_study_role = null where codelst_type = 'coverage_type';

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,191,1,'01_Update_CovType_Codelist.sql',sysdate,'9.0.1 Build#641');

commit;