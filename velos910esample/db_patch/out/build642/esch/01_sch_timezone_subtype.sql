SET DEFINE OFF;

DECLARE 
count_x NUMBER DEFAULT 0;

BEGIN
        SELECT count(*)
        INTO count_x
        FROM user_tab_cols
       WHERE upper(table_name) = 'SCH_TIMEZONES' AND upper(column_name) = 'TZ_SUBTYPE';

      IF (count_x = 0)
      THEN
         EXECUTE IMMEDIATE 'ALTER TABLE ESCH.SCH_TIMEZONES ADD (TZ_SUBTYPE  VARCHAR2(16))';
      END IF;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,193,1,'01_sch_timezone_subtype.sql',sysdate,'9.0.1 Build#642');

commit;