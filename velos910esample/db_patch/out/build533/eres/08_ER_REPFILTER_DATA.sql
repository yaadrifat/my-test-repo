SET DEFINE OFF;

update ER_REPFILTERMAP set REPFILTERMAP_COLUMN = 
    '<td><DIV id="studyDIV"><Font class=comments><A href="javascript:openLookup(document.dashboardpg,''viewId=6013&form=dashboardpg&maxselect=1&seperator=,&defaultvalue=&keyword=selstudyId|STUDY_NUMBER~paramstudyId|LKP_PK|[VELHIDE]'',''dfilter=formQuery'')">Select Study</A> <FONT class="Mandatory">* </FONT></Font></DIV></td>
	       	<td><DIV id="studydataDIV"> <input TYPE="text" NAME="selstudyId" readonly value=""><input TYPE="hidden" NAME="paramstudyId" value=""></DIV></td>' where PK_REPFILTERMAP = 123;


update ER_REPFILTER set REPFILTER_COLUMN =
    '<td><DIV id="protcalDIV"><FONT class = "comments"><A href="javascript:void(0);" onClick="if (checkDepend(''protCalId'')) return openLookup(document.reports,''viewId=6016&form=reports&seperator=,&defaultvalue=[ALL]&keyword=selprotCalId|name~paramprotCalId|event_id|[VELHIDE]'',''dfilter=selProtCal|:studyId'')">Select Protocol Calendar</A> </FONT></DIV> </td>
               <td><DIV id="protcaldataDIV"><Input TYPE="text" NAME="selprotCalId" SIZE="20" READONLY value="[ALL]">
               <Input TYPE="hidden" NAME="paramprotCalId" value="[ALL]"></DIV>
               </td>' where PK_REPFILTER = 9;
               

update ER_REPFILTER set REPFILTER_COLUMN =
    '<td><DIV id="txarmDIV"><FONT class = "comments"><A href="javascript:void(0);" onClick="if (checkDepend(''txArmId'')) return openLookup(document.reports,''viewId=6015&form=reports&seperator=,&defaultvalue=[ALL]&keyword=seltxArmId|tx_name~paramtxArmId|pk_studytxarm|[VELHIDE]'',''dfilter=study|:studyId'')">Select Tx Arm</A> </FONT></DIV> </td>
               <td><DIV id="txarmdataDIV"><Input TYPE="text" NAME="seltxArmId" SIZE="20" READONLY VALUE="[ALL]">
               <Input TYPE="hidden" NAME="paramtxArmId" VALUE="[ALL]"></DIV>
               </td>' where PK_REPFILTER = 4;

COMMIT;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,76,8,'08_ER_REPFILTER_DATA.sql',sysdate,'8.9.0 Build#533');

commit;
