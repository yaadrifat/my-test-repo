BEGIN
--Insert script

--insert script for WORKFLOW table
Insert into WORKFLOW (PK_WORKFLOW,WORKFLOW_NAME,WORKFLOW_DESC,ORGANIZATIONID,WORKFLOW_FAILEDEMAILID,WORKFLOW_STARTMODE,WORKFLOW_TYPE,CREATEDON,CREATEDBY,DELETEDFLAG,AUDITID,WORKFLOW_MODULE) values (SQ_WORKFLOW.nextval,'CT Order Workflow','Work flow for CT Order',null,null,null,'CTORDER',to_timestamp('28-SEP-11 03.23.02.900000000 PM','DD-MON-RR HH.MI.SS.FF AM'),null,0,null,null);

--insert script for ACTIVITY table
Insert into ACTIVITY (PK_ACTIVITY,ACTIVITY_NAME,ACTIVITY_DESC,ORGANIZATIONID,CREATEDON,CREATEDBY,DELETEDFLAG,AUDITID) values (SQ_ACTIVITY.nextval,'cbuavailconfbar','CBU Availability Conformation',100,null,null,0,null);
Insert into ACTIVITY (PK_ACTIVITY,ACTIVITY_NAME,ACTIVITY_DESC,ORGANIZATIONID,CREATEDON,CREATEDBY,DELETEDFLAG,AUDITID) values (SQ_ACTIVITY.nextval,'reqclininfobar','Required Clinical Information',100,null,null,1,null);
Insert into ACTIVITY (PK_ACTIVITY,ACTIVITY_NAME,ACTIVITY_DESC,ORGANIZATIONID,CREATEDON,CREATEDBY,DELETEDFLAG,AUDITID) values (SQ_ACTIVITY.nextval,'additityingandtest','Enter Additional Typing and Testing',100,to_timestamp('31-OCT-11 05.05.15.000000000 PM','DD-MON-RR HH.MI.SS.FF AM'),null,0,null);
Insert into ACTIVITY (PK_ACTIVITY,ACTIVITY_NAME,ACTIVITY_DESC,ORGANIZATIONID,CREATEDON,CREATEDBY,DELETEDFLAG,AUDITID) values (SQ_ACTIVITY.nextval,'ctshipmentbar','CT Shipping Info',100,null,null,0,null);
Insert into ACTIVITY (PK_ACTIVITY,ACTIVITY_NAME,ACTIVITY_DESC,ORGANIZATIONID,CREATEDON,CREATEDBY,DELETEDFLAG,AUDITID) values (SQ_ACTIVITY.nextval,'genpackageslipbar','Generate Package Slip',100,to_timestamp('31-OCT-11 05.08.53.000000000 PM','DD-MON-RR HH.MI.SS.FF AM'),null,0,null);
Insert into ACTIVITY (PK_ACTIVITY,ACTIVITY_NAME,ACTIVITY_DESC,ORGANIZATIONID,CREATEDON,CREATEDBY,DELETEDFLAG,AUDITID) values (SQ_ACTIVITY.nextval,'ctresolbar','CT Resolution',100,null,null,1,null);


--insert script for WORKFLOW_ACTIVITY and WFACTIVITY_CONDITION tables
Insert into WORKFLOW_ACTIVITY (PK_WORKFLOWACTIVITY,WA_NAME,FK_WORKFLOWID,FK_ACTIVITYID,WA_CALLACTION,WA_SUCESSFORWARDACTION,WA_DEFAULTFORWARDACTION,WA_SEQUENCE,WA_TERMINATEFLAG,FK_USERORROLEID,WA_USERORROLETYPE,WA_DISPLAYQUERY,WA_KEYTABLE,WA_KEYCOLUMN,CREATEDON,CREATEDBY,DELETEDFLAG,AUDITID) values (SQ_WORKFLOWACTIVITY.nextval,null,SQ_WORKFLOW.currval,(SELECT PK_ACTIVITY FROM ACTIVITY WHERE ACTIVITY_NAME='cbuavailconfbar'),'cdrcbuView',null,null,1,null,100,'ROLE','select ''test'' from dual',null,'PK_ORDER = #keycolumn',to_timestamp('17-OCT-11 04.54.19.546000000 PM','DD-MON-RR HH.MI.SS.FF AM'),null,0,null);

Insert into WFACTIVITY_CONDITION (PK_WFACONDITION,FK_WORKFLOWACTIVITY,EA_TYPE,EA_STARTBRACE,EA_TABLENAME,EA_COLUMNNAME,EA_VALUE,EA_ARITHMETICOPERATOR,EA_LOGICALOPERATOR,EA_ENDBRACE,CREATEDON,CREATEDBY,DELETEDFLAG,AUDITID,EA_KEYCONDITION) values (SQ_WFACTIVITYCONDITION.nextval,SQ_WORKFLOWACTIVITY.currval,'PRE',null,'ER_ORDER','PK_ORDER','0','>',null,null,null,null,0,null,'PK_ORDER = #keycolumn');

Insert into WFACTIVITY_CONDITION (PK_WFACONDITION,FK_WORKFLOWACTIVITY,EA_TYPE,EA_STARTBRACE,EA_TABLENAME,EA_COLUMNNAME,EA_VALUE,EA_ARITHMETICOPERATOR,EA_LOGICALOPERATOR,EA_ENDBRACE,CREATEDON,CREATEDBY,DELETEDFLAG,AUDITID,EA_KEYCONDITION) values (SQ_WFACTIVITYCONDITION.nextval,SQ_WORKFLOWACTIVITY.currval,'POST',null,'CB_CORD','CBU_AVAIL_CONFIRM_FLAG','select ''Y'' from dual','=',null,null,null,null,0,null,'PK_CORD=(SELECT ORDER_ENTITYID FROM ER_ORDER WHERE PK_ORDER = #keycolumn)');

Insert into WORKFLOW_ACTIVITY (PK_WORKFLOWACTIVITY,WA_NAME,FK_WORKFLOWID,FK_ACTIVITYID,WA_CALLACTION,WA_SUCESSFORWARDACTION,WA_DEFAULTFORWARDACTION,WA_SEQUENCE,WA_TERMINATEFLAG,FK_USERORROLEID,WA_USERORROLETYPE,WA_DISPLAYQUERY,WA_KEYTABLE,WA_KEYCOLUMN,CREATEDON,CREATEDBY,DELETEDFLAG,AUDITID) values (SQ_WORKFLOWACTIVITY.nextval,null,SQ_WORKFLOW.currval,(SELECT PK_ACTIVITY FROM ACTIVITY WHERE ACTIVITY_NAME='additityingandtest'),'cdrcbuView',null,null,2,null,100,'ROLE','select ''test'' from dual',null,'PK_ORDER = #keycolumn',to_timestamp('17-OCT-11 04.54.19.546000000 PM','DD-MON-RR HH.MI.SS.FF AM'),null,0,null);

Insert into WFACTIVITY_CONDITION (PK_WFACONDITION,FK_WORKFLOWACTIVITY,EA_TYPE,EA_STARTBRACE,EA_TABLENAME,EA_COLUMNNAME,EA_VALUE,EA_ARITHMETICOPERATOR,EA_LOGICALOPERATOR,EA_ENDBRACE,CREATEDON,CREATEDBY,DELETEDFLAG,AUDITID,EA_KEYCONDITION) values (SQ_WFACTIVITYCONDITION.nextval,SQ_WORKFLOWACTIVITY.currval,'PRE',null,'ER_ORDER','PK_ORDER','0','>',null,null,null,null,0,null,'PK_ORDER = #keycolumn');

Insert into WFACTIVITY_CONDITION (PK_WFACONDITION,FK_WORKFLOWACTIVITY,EA_TYPE,EA_STARTBRACE,EA_TABLENAME,EA_COLUMNNAME,EA_VALUE,EA_ARITHMETICOPERATOR,EA_LOGICALOPERATOR,EA_ENDBRACE,CREATEDON,CREATEDBY,DELETEDFLAG,AUDITID,EA_KEYCONDITION) values (SQ_WFACTIVITYCONDITION.nextval,SQ_WORKFLOWACTIVITY.currval,'POST',null,'CB_CORD','ADDITI_TYPING_FLAG','select ''Y'' from dual','=',null,null,null,null,0,null,'PK_CORD=(SELECT ORDER_ENTITYID FROM ER_ORDER WHERE PK_ORDER = #keycolumn)');

Insert into WORKFLOW_ACTIVITY (PK_WORKFLOWACTIVITY,WA_NAME,FK_WORKFLOWID,FK_ACTIVITYID,WA_CALLACTION,WA_SUCESSFORWARDACTION,WA_DEFAULTFORWARDACTION,WA_SEQUENCE,WA_TERMINATEFLAG,FK_USERORROLEID,WA_USERORROLETYPE,WA_DISPLAYQUERY,WA_KEYTABLE,WA_KEYCOLUMN,CREATEDON,CREATEDBY,DELETEDFLAG,AUDITID) values (SQ_WORKFLOWACTIVITY.nextval,null,SQ_WORKFLOW.currval,(SELECT PK_ACTIVITY FROM ACTIVITY WHERE ACTIVITY_NAME='ctshipmentbar'),'cdrcbuView',null,null,3,null,100,'ROLE','select ''test'' from dual',null,'PK_ORDER = #keycolumn',to_timestamp('17-OCT-11 04.54.19.546000000 PM','DD-MON-RR HH.MI.SS.FF AM'),null,0,null);


Insert into WFACTIVITY_CONDITION (PK_WFACONDITION,FK_WORKFLOWACTIVITY,EA_TYPE,EA_STARTBRACE,EA_TABLENAME,EA_COLUMNNAME,EA_VALUE,EA_ARITHMETICOPERATOR,EA_LOGICALOPERATOR,EA_ENDBRACE,CREATEDON,CREATEDBY,DELETEDFLAG,AUDITID,EA_KEYCONDITION) values (SQ_WFACTIVITYCONDITION.nextval,SQ_WORKFLOWACTIVITY.currval,'PRE',null,'ER_ORDER','PK_ORDER','0','>',null,null,null,null,0,null,'PK_ORDER = #keycolumn');

Insert into WFACTIVITY_CONDITION (PK_WFACONDITION,FK_WORKFLOWACTIVITY,EA_TYPE,EA_STARTBRACE,EA_TABLENAME,EA_COLUMNNAME,EA_VALUE,EA_ARITHMETICOPERATOR,EA_LOGICALOPERATOR,EA_ENDBRACE,CREATEDON,CREATEDBY,DELETEDFLAG,AUDITID,EA_KEYCONDITION) values (SQ_WFACTIVITYCONDITION.nextval,SQ_WORKFLOWACTIVITY.currval,'POST',null,'CB_SHIPMENT','SHIPMENT_SCH_FLAG','select ''Y'' from dual','=',null,null,null,null,0,null,'FK_ORDER_ID = #keycolumn');

Insert into WORKFLOW_ACTIVITY (PK_WORKFLOWACTIVITY,WA_NAME,FK_WORKFLOWID,FK_ACTIVITYID,WA_CALLACTION,WA_SUCESSFORWARDACTION,WA_DEFAULTFORWARDACTION,WA_SEQUENCE,WA_TERMINATEFLAG,FK_USERORROLEID,WA_USERORROLETYPE,WA_DISPLAYQUERY,WA_KEYTABLE,WA_KEYCOLUMN,CREATEDON,CREATEDBY,DELETEDFLAG,AUDITID) values (SQ_WORKFLOWACTIVITY.nextval,null,SQ_WORKFLOW.currval,(SELECT PK_ACTIVITY FROM ACTIVITY WHERE ACTIVITY_NAME='genpackageslipbar'),'cdrcbuView',null,null,4,1,100,'ROLE','select ''test'' from dual',null,'PK_ORDER = #keycolumn',to_timestamp('17-OCT-11 04.54.19.546000000 PM','DD-MON-RR HH.MI.SS.FF AM'),null,0,null);


Insert into WFACTIVITY_CONDITION (PK_WFACONDITION,FK_WORKFLOWACTIVITY,EA_TYPE,EA_STARTBRACE,EA_TABLENAME,EA_COLUMNNAME,EA_VALUE,EA_ARITHMETICOPERATOR,EA_LOGICALOPERATOR,EA_ENDBRACE,CREATEDON,CREATEDBY,DELETEDFLAG,AUDITID,EA_KEYCONDITION) values (SQ_WFACTIVITYCONDITION.nextval,SQ_WORKFLOWACTIVITY.currval,'PRE',null,'ER_ORDER','PK_ORDER','0','>',null,null,null,null,0,null,'PK_ORDER = #keycolumn');

Insert into WFACTIVITY_CONDITION (PK_WFACONDITION,FK_WORKFLOWACTIVITY,EA_TYPE,EA_STARTBRACE,EA_TABLENAME,EA_COLUMNNAME,EA_VALUE,EA_ARITHMETICOPERATOR,EA_LOGICALOPERATOR,EA_ENDBRACE,CREATEDON,CREATEDBY,DELETEDFLAG,AUDITID,EA_KEYCONDITION) values (SQ_WFACTIVITYCONDITION.nextval,SQ_WORKFLOWACTIVITY.currval,'POST',null,'CB_SHIPMENT','PACKAGE_SLIP_FLAG','select ''Y'' from dual','=',null,null,null,null,0,null,'FK_ORDER_ID = #keycolumn');

END;
/

Commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,148,11,'11_WORKFLOW_INSERT.sql',sysdate,'9.0.0 Build#605');

commit;