create or replace
TRIGGER ESCH."SCH_PROTOCOL_VISIT_AU1"
AFTER UPDATE
ON ESCH.SCH_PROTOCOL_VISIT REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
v_pk_budget Number;
v_pk_bgtcal Number;
maxsection  NUMBER(38,0); --ADDED BY BIKASH
newsection  NUMBER(38,0); --ADDED BY BIKASH

pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SCH_PROTOCOL_VISIT_AU1', pLEVEL  => Plog.LDEBUG);

BEGIN

--get calendar's default budget
begin
--    plog.debug(pctx,':new.fk_protocol '||:new.fk_protocol );

    select pk_budget,PK_BGTCAL
    into v_pk_budget,v_pk_bgtcal
    from sch_budget,sch_bgtcal
    where budget_calendar = :new.fk_protocol and pk_budget = fk_budget and
     bgtcal_protid = :new.fk_protocol and rownum < 2;
     
if :new.pk_protocol_visit is not null 
   then   
  maxsection := GETMAXSECTION(v_pk_bgtcal);
  
  newsection := :new.displacement + maxsection ; 
end if;

    Update SCH_BGTSECTION
    set BGTSECTION_NAME = :new.visit_name, BGTSECTION_SEQUENCE = newsection,
    IP_ADD = :new.ip_add,BGTSECTION_NOTES =  :new.description, last_modified_by = :new.last_modified_by ,last_modified_date = sysdate
    where fk_visit = :new.pk_protocol_visit and FK_BGTCAL = v_pk_bgtcal;
    
        

exception when no_data_found then
    v_pk_budget := 0;
    v_pk_bgtcal := 0;
    plog.fatal(pctx,'exception: :new.fk_protocol '||:new.fk_protocol );

end;

END; 
/



INSERT INTO track_patches
VALUES(seq_track_patches.nextval,83,3,'03_SCH_PROTOCOL_VISIT_AUI.SQL',sysdate,'8.9.0 Build#540');

commit;


