/* Formatted on 2/9/2010 1:41:09 PM (QP5 v5.115.810.9015) */

  CREATE OR REPLACE FORCE VIEW "ESCH"."ERV_BUDGET" ("PK_BUDGET", "BGTCAL_PROTID", "PK_BGTCAL", "PROT_CALENDAR", "BUDGET_NAME", "BUDGET_VERSION", "BUDGET_DESC", "STUDY_NUMBER", "FK_STUDY", "STUDY_TITLE", "SITE_NAME", "BGTSECTION_NAME", "BGTSECTION_PATNO", "LINEITEM_NAME", "TOTAL_COST", "TOTAL_COST_AFTER", "TOTAL_PAT_COST_AFTER", "TOTAL_LINE_FRINGE", "PER_PATIENT_LINE_FRINGE", "CATEGORY", "CATEGORY_SEQ", "CATEGORY_SUBTYP", "UNIT_COST", "NUMBER_OF_UNIT", "STANDARD_OF_CARE", "COST_PER_PATIENT", "COST_DISCOUNT_ON_LINE_ITEM", "PER_PAT_LINE_ITEM_DISCOUNT", "TOTAL_COST_DISCOUNT", "CPT_CODE", "TMID", "CDM", "LINE_ITEM_INDIRECTS_FLAG", "INDIRECTS", "BUDGET_INDIRECT_FLAG", "FRINGE_BENEFIT", "FRINGE_FLAG", "BUDGET_DISCOUNT", "BUDGET_DISCOUNT_FLAG", "PERPAT_INDIRECT", "TOTAL_COST_INDIRECT", "BUDGET_CURRENCY", "LAST_MODIFIED_BY", "LAST_MODIFIED_DATE", "BUDGETSECTION_SEQUENCE", "BGTCAL_INDIRECTCOST", "LINEITEM_DESC", "LINEITEM_SPONSORAMOUNT", "LINEITEM_VARIANCE", "BGTSECTION_TYPE", "LINEITEM_OTHERCOST", "FK_CODELST_COST_TYPE", "COST_TYPE_DESC", "LINEITEM_SEQ", "LINITEM_SPONSOR_PERPAT", "LINEITEM_DIRECT_PERPAT", "TOTAL_COST_PER_PAT", "TOTAL_COST_ALL_PAT", "COST_CUSTOMCOL", "PK_BUDGETSEC", "BUDGETSEC_FKVISIT", "PK_LINEITEM", "LINEITEM_INPERSEC", "LINEITEM_FKEVENT", "BGTCAL_EXCLDSOCFLAG", "BUDGET_COMBFLAG", "COVERAGE_TYPE") AS
  SELECT DISTINCT pk_budget,
        bgtcal_protid,
        pk_bgtcal,
        prot_calendar,
        budget_name,
        budget_version,
        budget_desc,
        study_number,
        fk_study,
        study_title,
        site_name,
        bgtsection_name,
        bgtsection_patno,
        lineitem_name,
        total_cost,
        NVL (total_cost_after, 0) total_cost_after,
        NVL (total_pat_cost_after, 0) total_pat_cost_after,
        total_line_fringe,
        per_patient_line_fringe,
        CATEGORY,
        category_seq,
        category_subtyp,
        unit_cost,
        number_of_unit,
        standard_of_care,
        cost_per_patient,
        cost_discount_on_line_item,
        per_pat_line_item_discount,
        total_cost_discount,
        cpt_code,
        tmid,
        cdm,
        line_item_indirects_flag,
        indirects,
        budget_indirect_flag,
        fringe_benefit,
        fringe_flag,
        budget_discount,
        budget_discount_flag,
        NVL (
           DECODE (
              budget_indirect_flag,
              'Y',
                total_pat_cost_after
              * line_item_indirects_flag
              * indirects
              / 100,
              0
           ),
           0
        )
           AS per_pat_indirect,
        NVL (
           DECODE (
              budget_indirect_flag,
              'Y',
                total_cost_after
              * line_item_indirects_flag
              * indirects
              / 100,
              0
           ),
           0
        )
           AS total_cost_indirect,
        budget_currency,
        last_modified_by,
        last_modified_date,
        bgtsection_sequence,
        NVL (bgtcal_indirectcost, 0) AS bgtcal_indirectcost,
        lineitem_desc,
        lineitem_sponsoramount,
        (NVL (lineitem_sponsoramount, 0)
         - NVL (
              (DECODE (
                  budget_indirect_flag,
                  'Y',
                    total_pat_cost_after
                  * line_item_indirects_flag
                  * indirects
                  / 100,
                  0
               )
               + lineitem_direct_perpat)
              * bgtsection_patno,
              0
           ))
           AS lineitem_variance,
        bgtsection_type,
        LINEITEM_OTHERCOST,
        FK_CODELST_COST_TYPE,
        COST_TYPE_DESC,
        lineitem_seq,
        linitem_sponsor_perpat,
         DECODE (bgtsection_type,
                  'P',NVL (lineitem_direct_perpat, 0),'O',0 ) lineitem_direct_perpat,
        DECODE (bgtsection_type,
                'P',NVL (
           DECODE (
              budget_indirect_flag,
              'Y',
                total_pat_cost_after
              * line_item_indirects_flag
              * indirects
              / 100,
              0
           )
           + lineitem_direct_perpat,
           0
        ),'O',0)
           AS total_cost_per_pat,
        NVL (
           (DECODE (
               budget_indirect_flag,
               'Y',
                 total_pat_cost_after
               * line_item_indirects_flag
               * indirects
               / 100,
               0
            )
            + lineitem_direct_perpat)
           * bgtsection_patno,
           0
        )
           AS total_cost_all_pat,
        (SELECT   codelst_custom_col1
           FROM  sch_codelst
          WHERE   pk_codelst = FK_CODELST_COST_TYPE)
           COST_CUSTOMCOL,
        pk_budgetsec,
        budgetsec_fkvisit,
        pk_lineitem,
        lineitem_inpersec,
        lineitem_fkevent,
        bgtcal_excldsocflag,
        BUDGET_COMBFLAG,
	    coverage_type
 FROM   (SELECT   pk_budget,
                  bgtcal_protid,
                  pk_bgtcal,
                  DECODE(bgtcal_prottype,
                          'L',
                          (SELECT   NAME
                             FROM   event_def
                            WHERE   event_id = bgtcal_protid),
                          'S',
                          (SELECT   NAME
                             FROM   event_assoc
                            WHERE   event_id = bgtcal_protid),
                          NULL,
                          NULL)
                     AS prot_calendar,
                  budget_name,
                  budget_version,
                  budget_desc,
                  study_number,
                  fk_study,
                  study_title,
                  site_name,
                  bgtsection_name,
                  bgtsection_patno,
                  lineitem_name,
                  NVL (total_cost, 0) AS total_cost,
                  ROUND (
                     (total_line_fringe + (total_cost))
                     - DECODE (
                          bgtcal_discountflag,
                          0,
                          0,
                          2,
                          0,
                          1,
                            (total_line_fringe + (total_cost))
                          * bgtcal_discount
                          * lineitem_incostdisc
                          / 100
                       )
                     + DECODE (
                          bgtcal_discountflag,
                          0,
                          0,
                          1,
                          0,
                          2,
                            (total_line_fringe + (total_cost))
                          * bgtcal_discount
                          * lineitem_incostdisc
                          / 100
                       ),
                     2
                  )
                     AS total_cost_after,
                  ROUND (
                     (per_patient_line_fringe + (lineitem_othercost))
                     - DECODE (
                          bgtcal_discountflag,
                          0,
                          0,
                          2,
                          0,
                          1,
                          (per_patient_line_fringe + (lineitem_othercost))
                          * bgtcal_discount
                          * lineitem_incostdisc
                          / 100
                       )
                     + DECODE (
                          bgtcal_discountflag,
                          0,
                          0,
                          1,
                          0,
                          2,
                          (per_patient_line_fringe + (lineitem_othercost))
                          * bgtcal_discount
                          * lineitem_incostdisc
                          / 100
                       ),
                     2
                  )
                     AS total_pat_cost_after,
                  total_line_fringe,
                  per_patient_line_fringe,
                  CATEGORY,
                  category_seq,
                  category_subtyp,
                  lineitem_sponsorunit AS unit_cost,
                  lineitem_clinicnofunit AS number_of_unit,
                  DECODE (lineitem_stdcarecost,
                          0, 0,
                          lineitem_stdcarecost)
                     AS standard_of_care,
                  lineitem_othercost AS cost_per_patient,
                  lineitem_incostdisc AS cost_discount_on_line_item,
                  DECODE (
                     bgtcal_discountflag,
                     0,
                     0,
                     (lineitem_incostdisc
                      * (lineitem_othercost
                         * DECODE (
                              category_subtyp,
                              'ctgry_per',
                              NVL2 (bgtcal_frgbenefit,
                                    bgtcal_frgbenefit / 100,
                                    0),
                              0
                           )
                         + lineitem_othercost)
                      * bgtcal_discount
                      / 100)
                  )
                     AS per_pat_line_item_discount,
                  DECODE (
                     bgtcal_discountflag,
                     0,
                     0,
                     lineitem_incostdisc
                     * (total_cost
                        * DECODE (
                             category_subtyp,
                             'ctgry_per',
                             NVL2 (bgtcal_frgbenefit,
                                   bgtcal_frgbenefit / 100,
                                   0),
                             0
                          )
                        + total_cost)
                     * bgtcal_discount
                     / 100
                  )
                     AS total_cost_discount,
                  lineitem_cptcode AS cpt_code,
                  lineitem_tmid AS tmid,
                  lineitem_cdm AS cdm,
                  lineitem_applyindirects AS line_item_indirects_flag,
                  NVL (bgtcal_sponsorohead, 0) AS indirects,
                  bgtcal_sponsorflag AS budget_indirect_flag,
                  NVL (bgtcal_frgbenefit, 0) AS fringe_benefit,
                  bgtcal_frgflag AS fringe_flag,
                  NVL (bgtcal_discount, 0) AS budget_discount,
                  bgtcal_discountflag AS budget_discount_flag,
                  budget_currency,
                  last_modified_by,
                  last_modified_date,
                  bgtsection_sequence,
                  bgtcal_indirectcost,
                  lineitem_desc,
                  lineitem_sponsoramount,
                  bgtsection_type,
                  LINEITEM_OTHERCOST,
                  FK_CODELST_COST_TYPE,
                  COST_TYPE_DESC,
                  lineitem_seq,
                  linitem_sponsor_perpat,
                  ROUND (   (NVL (per_patient_line_fringe, 0)
                      + (lineitem_othercost))
                     - DECODE(
                          bgtcal_discountflag,
                          0,0,2,0,1,
                          (per_patient_line_fringe + (lineitem_othercost))
                          * bgtcal_discount
                          * lineitem_incostdisc
                          / 100
                       )
                     + DECODE(
                          bgtcal_discountflag,
                          0,0,1,0,2,(per_patient_line_fringe + (lineitem_othercost))
                          * bgtcal_discount
                          * lineitem_incostdisc
                          / 100
                       ),
                     2
                  ) AS lineitem_direct_perpat,
                  pk_budgetsec,
                  budgetsec_fkvisit,
                  pk_lineitem,
                  lineitem_inpersec,
                  lineitem_fkevent,
                  bgtcal_excldsocflag,
                  BUDGET_COMBFLAG,
		  		  coverage_type
           FROM   (SELECT   pk_budget,
                            bgtcal_prottype,
                            bgtcal_protid,
                            pk_bgtcal,
                            budget_name,
                            budget_version,
                            budget_desc,
                            bgtsection_name,
                            bgtsection_type,
                            NVL (bgtsection_patno, 1) bgtsection_patno,
                            lineitem_name,
                            DECODE (
                               bgtsection_type,
                               'P',
                                 NVL (bgtsection_patno, 1)
                               * lineitem_othercost
                               * DECODE (lineitem_clinicnofunit, 0, 0, 1),
                               'O',
                               lineitem_sponsorunit
                               * lineitem_clinicnofunit
                            )
                               AS total_cost,
                            DECODE (
                               TRIM( (SELECT   codelst_subtyp
                                        FROM sch_codelst
                                       WHERE   pk_codelst =
                                                  fk_codelst_category)),
                               'ctgry_per',
                               DECODE (
                                  bgtsection_type,
                                  'P',
                                  NVL (bgtsection_patno, 1)
                                  * lineitem_othercost,
                                  'O',
                                  lineitem_sponsorunit
                                  * lineitem_clinicnofunit
                               )
                               * bgtcal_frgbenefit
                               * NVL (bgtcal_frgflag, 0)
                               / 100,
                               0
                            )
                               AS total_line_fringe,
                            DECODE (
                               TRIM( (SELECT   codelst_subtyp
                                        FROM   sch_codelst
                                       WHERE   pk_codelst =
                                                  fk_codelst_category)),
                               'ctgry_per',
                                 lineitem_othercost
                               * bgtcal_frgbenefit
                               * NVL (bgtcal_frgflag, 0)
                               / 100,
                               0
                            )
                               AS per_patient_line_fringe,
                            fk_codelst_category,
                            lineitem_sponsorunit,
                            lineitem_clinicnofunit,
                            lineitem_stdcarecost,
                            lineitem_rescost,
                            lineitem_incostdisc,
                            lineitem_cptcode,
                            (SELECT   codelst_desc
                               FROM   sch_codelst
                              WHERE   pk_codelst = fk_codelst_category)
                               AS CATEGORY,
                            (SELECT   codelst_seq
                               FROM   sch_codelst
                              WHERE   pk_codelst = fk_codelst_category)
                               AS category_seq,
                            (SELECT   TRIM (codelst_subtyp)
                               FROM   sch_codelst
                              WHERE   pk_codelst = fk_codelst_category)
                               AS category_subtyp,
                            lineitem_tmid,
                            lineitem_cdm,
                            NVL (lineitem_applyindirects, 0)
                               lineitem_applyindirects,
                            NVL (bgtcal_sponsorohead, 0)
                               bgtcal_sponsorohead,
                            NVL (bgtcal_sponsorflag, 0)
                               bgtcal_sponsorflag,
                            NVL (bgtcal_frgbenefit, 0) bgtcal_frgbenefit,
                            NVL (bgtcal_frgflag, 0) bgtcal_frgflag,
                            NVL (bgtcal_discount, 0) bgtcal_discount,
                            NVL (bgtcal_discountflag, 0)
                               bgtcal_discountflag,
                            (SELECT   study_number
                               FROM   er_study
                              WHERE   pk_study = fk_study)
                               AS study_number,
                            (SELECT   pk_study
                               FROM   er_study
                              WHERE   pk_study = fk_study)
                               AS fk_study,
                            (SELECT   study_title
                               FROM   er_study
                              WHERE   pk_study = fk_study)
                               AS study_title,
                            (SELECT   site_name
                               FROM   er_site
                              WHERE   pk_site = fk_site)
                               AS site_name,
                            (SELECT   NVL (codelst_subtyp, codelst_desc)
                               FROM   sch_codelst
                              WHERE   pk_codelst = budget_currency)
                               AS budget_currency,
                            (SELECT      usr_firstname
                                      || ' '
                                      || usr_lastname
                               FROM   er_user
                              WHERE   pk_user =
                                         NVL (
                                            sch_budget.last_modified_by,
                                            sch_budget.creator
                                         ))
                               AS last_modified_by,
                            TO_CHAR (
                               NVL (sch_budget.last_modified_date,
                                    sch_budget.created_on),
                               PKG_DATEUTIL.F_GET_DATEFORMAT
                               || ' hh:mi:ss'
                            )
                               AS last_modified_date,
                            bgtsection_sequence,
                            bgtcal_indirectcost,
                            lineitem_desc,
                            lineitem_sponsoramount,
                            LINEITEM_OTHERCOST,
                            FK_CODELST_COST_TYPE,
                            (SELECT   codelst_desc
                               FROM   sch_codelst
                              WHERE   pk_codelst = FK_CODELST_COST_TYPE)
                               COST_TYPE_DESC,
                            lineitem_seq,
                            NULL AS linitem_sponsor_perpat,
                            pk_budgetsec,
                            fk_visit budgetsec_fkvisit,
                            pk_lineitem,
                            NVL (lineitem_inpersec, 0) lineitem_inpersec,
                            sch_lineitem.fk_event lineitem_fkevent,
                            bgtcal_excldsocflag,
                            BUDGET_COMBFLAG,
							DECODE(
								bgtcal_prottype,
			  					'L',
                          			(SELECT   codelst_desc
                               			FROM   sch_codelst
                              			WHERE   pk_codelst = (select FK_CODELST_COVERTYPE from EVENT_DEF where event_id= sch_lineitem.fk_event)),
                          		'S',
                          			(SELECT   codelst_desc
                               			FROM   sch_codelst
                              			WHERE   pk_codelst = (select FK_CODELST_COVERTYPE from EVENT_ASSOC where event_id= sch_lineitem.fk_event)),
                          		NULL,
                          			'No Coverage Type'
                          	) AS coverage_type
                     FROM   sch_bgtcal,
                            sch_budget,
                            sch_bgtsection,
                            sch_lineitem
                    WHERE       pk_budget = fk_budget
                            AND pk_bgtcal = fk_bgtcal
                            AND NVL (bgtsection_delflag, 'N') = 'N'
                            AND pk_budgetsec = fk_bgtsection(+)
                            AND NVL (lineitem_delflag, 'N') = 'N')) ;

/



INSERT INTO track_patches
VALUES(seq_track_patches.nextval,83,5,'05_erv_budget.sql',sysdate,'8.9.0 Build#540');

commit;



