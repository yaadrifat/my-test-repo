#Build 608

==========================================================================================
Garuda :

For Testing the Work flow functionality we are providing 
  Orders_Insert_script.sql
with this build. It contans script to insert CT, HE and OR Orders.
Before executing this script, you need to change the values for "ORDER_NUMBER" and 
"ORDER_ENTITYID" fields.
	For ORDER_NUMER, give values like "ORD001" and 
	For ORDER_ENTITYID, you need to provide the existing CB_CORD.PK_CORD value.

Note that the general-release scripts in the db_patch folder have to be executed first.
Then execute Orders_Insert_script.sql in the ERES schema for in the DB in which NMDP module
is used.

------------------------------
This build consists of two insert patches specific to NMDP. These patches have been made 
with the source data provided by NMDP. Execute these scripts only in the DB in which 
the NMDP module is used and execute them in the ERES schema.

1. CB_ANTIGEN_ENCODE_INSERT.SQL 
2. CB_ANTIGEN_INSERT.SQL

------------------------------
This build consists of a new top menu (Maintenance) and some updates to menu items specific to Garuda.
Script: 11_ER_OBJECT_SETTINGS_INSERT.sql 
This script creates a new Top menu (Maintenance) and its sub-menus:
i)  Center Management (It itself contains two sub-menus: CBB Profile and Processing Procedure).
ii) My Profile:

Visibility of these menu is by default false. To view these menu we need to set the visibility as true.

------------------------------
Updates in Menus (specific to Garuda):-

1. 12_ER_OBJECT_SETTINGS_ALTER.sql
This script is meant to rename two menu items Manage CBB,CBB Processing Procedure of 
Product Fulfillment to CBB Profile,Processing Procedures respectively.

2. 10_ER_OBJECT_SETTINGS_DELETE.SQL
This script is meant to delete menu item of Product Fulfillment menu. 

	
==========================================================================================
eResearch:

Upon the request from Garuda team we have added one Column GUID in ER_SITE table.
All Audit triggers related to this table are modified accordingly.


==========================================================================================
eResearch Localization:

Copy these properties files (messageBundle.properties and labelBundle.properties) 
in ereshome\ and paste them to the folder specified by ERES_HOME 
(where eresearch.xml is also hosted), over-writing the existing files.

Following Files have been Modified:

1. labelBundle.properties
2. LC.java
3. LC.jsp
4. MC.java
5. MC.jsp
6. messageBundle.properties
7. patientsearchworkflow.jsp
==========================================================================================
