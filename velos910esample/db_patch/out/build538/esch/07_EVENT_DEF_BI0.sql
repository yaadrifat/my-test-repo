set define off;

create or replace
TRIGGER "ESCH"."EVENT_DEF_BI0" BEFORE INSERT ON EVENT_DEF
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  erid NUMBER(10);
  USR VARCHAR2(70);
  v_new_creator NUMBER;
  insert_data CLOB;

BEGIN
 BEGIN
  v_new_creator := :NEW.creator;
  SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' ||usr_firstname
   INTO usr FROM er_user WHERE pk_user = v_new_creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
  USR := 'New User' ;
 END ;
p('USR2' || USR);
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid FROM dual;
  :NEW.rid := erid ;
p(':new.rid' || :NEW.rid);
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
p('raid' || raid);
  audit_trail.record_transaction
    (raid, 'EVENT_DEF', erid, 'I', USR );
-- Added by Ganapathy on 06/23/05 for Audit insert
insert_data:=:NEW.CALENDAR_SHAREDWITH||'|'||:NEW.DURATION_UNIT||'|'||
    :NEW.FK_VISIT||'|'|| :NEW.EVENT_ID||'|'|| :NEW.CHAIN_ID||'|'||:NEW.EVENT_TYPE||'|'||
    :NEW.NAME||'|'||:NEW.NOTES||'|'|| :NEW.COST||'|'||:NEW.COST_DESCRIPTION||'|'||
    :NEW.DURATION||'|'|| :NEW.USER_ID||'|'||:NEW.LINKED_URI||'|'||:NEW.FUZZY_PERIOD||'|'||
    :NEW.MSG_TO||'|'||:NEW.STATUS||'|'||:NEW.DESCRIPTION||'|'|| :NEW.DISPLACEMENT||'|'||
    :NEW.ORG_ID||'|'||:NEW.EVENT_MSG||'|'||:NEW.EVENT_RES||'|'|| :NEW.EVENT_FLAG||'|'||
     TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'|| :NEW.PAT_DAYSBEFORE||'|'||:NEW.PAT_DAYSAFTER||'|'||
    :NEW.USR_DAYSBEFORE||'|'|| :NEW.USR_DAYSAFTER||'|'||:NEW.PAT_MSGBEFORE||'|'||
    :NEW.PAT_MSGAFTER||'|'||:NEW.USR_MSGBEFORE||'|'||:NEW.USR_MSGAFTER||'|'||
    :NEW.RID||'|'|| :NEW.CREATOR||'|'|| :NEW.LAST_MODIFIED_BY||'|'||
     TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||:NEW.IP_ADD||'|'||
    :NEW.EVENT_FUZZYAFTER||'|'||:NEW.EVENT_DURATIONAFTER||'|'||	:NEW.EVENT_CPTCODE||'|'||:NEW.EVENT_DURATIONBEFORE ||'|'||
    :NEW.FK_CATLIB ||'|'|| :NEW.EVENT_CATEGORY ||'|'|| :NEW.EVENT_LIBRARY_TYPE ||'|'|| :NEW.EVENT_LINE_CATEGORY ||'|'||
    :NEW.SERVICE_SITE_ID ||'|'|| :NEW.FACILITY_ID ||'|'|| :NEW.FK_CODELST_COVERTYPE ||'|'|| :NEW.COVERAGE_NOTES;  --BK 23jul10

 INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,81,7,'07_EVENT_DEF_BI0.sql',sysdate,'8.9.0 Build#538');

commit;
