set define off;

UPDATE event_assoc e 
SET e.BUDGET_TEMPLATE = (SELECT b.BUDGET_TEMPLATE
FROM sch_budget b
WHERE e.EVENT_ID = b.budget_calendar 
AND e.chain_id = b.fk_study
AND b.FK_STUDY IS NOT NULL AND NVL(b.BUDGET_DELFLAG,'Z') !='Y')
WHERE EVENT_TYPE='P' ;

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,81,4,'04_update_old_bgtTemplates.sql',sysdate,'8.9.0 Build#538');

commit;
