CREATE OR REPLACE TRIGGER CB_QUESTIONS_AU0  AFTER UPDATE OF PK_QUESTIONS,
QUES_DESC,
QUES_HELP,
UNLICEN_REQ_FLAG,
LICEN_REQ_FLAG,
UNLICN_PRIOR_TO_SHIPMENT_FLAG,
LICEN_PRIOR_SHIPMENT_FLAG,
CBB_NOT_USE_SYSTEM,
QUES_HOVER,
ADD_COMMENT_FLAG,
ASSESMENT_FLAG,
FK_MASTER_QUES,
FK_DEPENDENT_QUES,
FK_DEPENDENT_QUES_VALUE,
RESPONSE_TYPE,
QUES_CODE,
QUES_SEQ,
CREATOR,
CREATED_ON,
LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,
IP_ADD,
DELETEDFLAG,
RID  ON CB_QUESTIONS REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
   RAID NUMBER(10);
   USR VARCHAR(200); 
   OLD_MODIFIED_BY VARCHAR2(100);
   NEW_MODIFIED_BY VARCHAR2(100);
  
  BEGIN
  SELECT SEQ_AUDIT.NEXTVAL INTO RAID FROM DUAL;
     USR := GETUSER(:NEW.LAST_MODIFIED_BY);
  AUDIT_TRAIL.RECORD_TRANSACTION(RAID, 'CB_QUESTIONS', :OLD.RID, 'U', USR);    
	
  IF NVL(:OLD.PK_QUESTIONS,0) != NVL(:NEW.PK_QUESTIONS,0) THEN
     AUDIT_TRAIL.COLUMN_UPDATE(RAID, 'PK_QUESTIONS',:OLD.PK_QUESTIONS, :NEW.PK_QUESTIONS);
  END IF;   
    IF NVL(:OLD.QUES_DESC,' ') != NVL(:NEW.QUES_DESC,' ') THEN
   AUDIT_TRAIL.COLUMN_UPDATE(RAID, 'QUES_DESC', :OLD.QUES_DESC, :NEW.QUES_DESC);
    END IF;  	
    IF NVL(:OLD.QUES_HELP,' ') != NVL(:NEW.QUES_HELP,' ') THEN
   AUDIT_TRAIL.COLUMN_UPDATE(RAID, 'QUES_HELP', :OLD.QUES_HELP, :NEW.QUES_HELP);
    END IF;  
    IF NVL(:OLD.UNLICEN_REQ_FLAG,' ') != NVL(:NEW.UNLICEN_REQ_FLAG,' ') THEN
   AUDIT_TRAIL.COLUMN_UPDATE(RAID, 'UNLICEN_REQ_FLAG', :OLD.UNLICEN_REQ_FLAG, :NEW.UNLICEN_REQ_FLAG);
    END IF;  
    IF NVL(:OLD.LICEN_REQ_FLAG,' ') != NVL(:NEW.LICEN_REQ_FLAG,' ') THEN
   AUDIT_TRAIL.COLUMN_UPDATE(RAID, 'LICEN_REQ_FLAG', :OLD.LICEN_REQ_FLAG, :NEW.LICEN_REQ_FLAG);
    END IF;  	
     IF NVL(:OLD.UNLICN_PRIOR_TO_SHIPMENT_FLAG,' ') != NVL(:NEW.UNLICN_PRIOR_TO_SHIPMENT_FLAG,' ') THEN
   AUDIT_TRAIL.COLUMN_UPDATE(RAID, 'UNLICN_PRIOR_TO_SHIPMENT_FLAG', :OLD.UNLICN_PRIOR_TO_SHIPMENT_FLAG, :NEW.UNLICN_PRIOR_TO_SHIPMENT_FLAG);
    END IF;
    IF NVL(:OLD.LICEN_PRIOR_SHIPMENT_FLAG,' ') != NVL(:NEW.LICEN_PRIOR_SHIPMENT_FLAG,' ') THEN
   AUDIT_TRAIL.COLUMN_UPDATE(RAID, 'LICEN_PRIOR_SHIPMENT_FLAG', :OLD.LICEN_PRIOR_SHIPMENT_FLAG, :NEW.LICEN_PRIOR_SHIPMENT_FLAG);
    END IF;
     IF NVL(:OLD.CBB_NOT_USE_SYSTEM,' ') != NVL(:NEW.CBB_NOT_USE_SYSTEM,' ') THEN
   AUDIT_TRAIL.COLUMN_UPDATE(RAID, 'CBB_NOT_USE_SYSTEM', :OLD.CBB_NOT_USE_SYSTEM, :NEW.CBB_NOT_USE_SYSTEM);
    END IF;
     IF NVL(:OLD.QUES_HOVER,' ') != NVL(:NEW.QUES_HOVER,' ') THEN
   AUDIT_TRAIL.COLUMN_UPDATE(RAID, 'QUES_HOVER', :OLD.QUES_HOVER, :NEW.QUES_HOVER);
    END IF;
    IF NVL(:OLD.ADD_COMMENT_FLAG,' ') != NVL(:NEW.ADD_COMMENT_FLAG,' ') THEN
   AUDIT_TRAIL.COLUMN_UPDATE(RAID, 'ADD_COMMENT_FLAG', :OLD.ADD_COMMENT_FLAG, :NEW.ADD_COMMENT_FLAG);
    END IF;
     IF NVL(:OLD.ASSESMENT_FLAG,' ') != NVL(:NEW.ASSESMENT_FLAG,' ') THEN
   AUDIT_TRAIL.COLUMN_UPDATE(RAID, 'ASSESMENT_FLAG', :OLD.ASSESMENT_FLAG, :NEW.ASSESMENT_FLAG);
    END IF;
   IF NVL(:OLD.FK_MASTER_QUES,0) != NVL(:NEW.FK_MASTER_QUES,0) THEN
   AUDIT_TRAIL.COLUMN_UPDATE(RAID, 'FK_MASTER_QUES', :OLD.FK_MASTER_QUES, :NEW.FK_MASTER_QUES);
    END IF;
  IF NVL(:OLD.FK_DEPENDENT_QUES,0) != NVL(:NEW.FK_DEPENDENT_QUES,0) THEN
   AUDIT_TRAIL.COLUMN_UPDATE(RAID, 'FK_DEPENDENT_QUES', :OLD.FK_DEPENDENT_QUES, :NEW.FK_DEPENDENT_QUES);
    END IF;
     IF NVL(:OLD.FK_DEPENDENT_QUES_VALUE,' ') != NVL(:NEW.FK_DEPENDENT_QUES_VALUE,' ') THEN
   AUDIT_TRAIL.COLUMN_UPDATE(RAID, 'FK_DEPENDENT_QUES_VALUE', :OLD.FK_DEPENDENT_QUES_VALUE, :NEW.FK_DEPENDENT_QUES_VALUE);
    END IF;
    IF NVL(:OLD.RESPONSE_TYPE,0) != NVL(:NEW.RESPONSE_TYPE,0) THEN
      AUDIT_TRAIL.COLUMN_UPDATE (RAID,'RESPONSE_TYPE',:OLD.RESPONSE_TYPE, :NEW.RESPONSE_TYPE);     
    END IF;
     IF NVL(:OLD.QUES_CODE,' ') != NVL(:NEW.QUES_CODE,' ') THEN
      AUDIT_TRAIL.COLUMN_UPDATE (RAID,'QUES_CODE',:OLD.QUES_CODE, :NEW.QUES_CODE);
      END IF;
      IF NVL(:OLD.QUES_SEQ,' ') != NVL(:NEW.QUES_SEQ,' ') THEN
      AUDIT_TRAIL.COLUMN_UPDATE (RAID,'QUES_SEQ',:OLD.QUES_SEQ, :NEW.QUES_SEQ);
    END IF;
  IF NVL(:OLD.CREATOR,0) != NVL(:NEW.CREATOR,0) THEN
     AUDIT_TRAIL.COLUMN_UPDATE(RAID, 'CREATOR',:OLD.CREATOR, :NEW.CREATOR);
  END IF;  
   IF NVL(:OLD.CREATED_ON,TO_DATE('31-DEC-9595','DD-MON-YYYY')) !=
     NVL(:NEW.CREATED_ON,TO_DATE('31-DEC-9595','DD-MON-YYYY')) THEN
     AUDIT_TRAIL.COLUMN_UPDATE
       (RAID, 'CREATED_ON',
       TO_CHAR(:OLD.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;  
  IF NVL(:OLD.IP_ADD,' ') !=   NVL(:NEW.IP_ADD,' ') THEN
     AUDIT_TRAIL.COLUMN_UPDATE(RAID, 'IP_ADD', :OLD.IP_ADD, :NEW.IP_ADD);
  END IF;   
    IF NVL(:OLD.LAST_MODIFIED_BY,0) != NVL(:NEW.LAST_MODIFIED_BY,0) THEN
       Begin
      Select  to_char(pk_user) || ',' ||  usr_lastname ||', ' || usr_firstname
      into old_modified_by from er_user  where pk_user = :old.last_modified_by ;
      Exception When NO_DATA_FOUND then
			old_modified_by := null;
    End ;
    Begin
      Select  to_char(pk_user) || ',' || usr_lastname ||', ' || usr_firstname
      into new_modified_by   from er_user   where pk_user = :new.LAST_MODIFIED_BY ;
      Exception When NO_DATA_FOUND then
			new_modified_by := null;
    End ;
      audit_trail.column_update (raid,'LAST_MODIFIED_BY' , old_modified_by, new_modified_by);    
    END IF; 
	 IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE('31-DEC-9595','DD-MON-YYYY')) !=
     NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE('31-DEC-9595','DD-MON-YYYY')) THEN
     AUDIT_TRAIL.COLUMN_UPDATE
       (RAID, 'LAST_MODIFIED_DATE',
       TO_CHAR(:OLD.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;    
    IF NVL(:OLD.RID,0) != NVL(:NEW.RID,0) THEN
     AUDIT_TRAIL.COLUMN_UPDATE (RAID, 'RID',:OLD.RID, :NEW.RID);     
    END IF;   	
	IF NVL(:OLD.DELETEDFLAG,0) != NVL(:NEW.DELETEDFLAG,0) THEN
     AUDIT_TRAIL.COLUMN_UPDATE (RAID, 'DELETEDFLAG', :OLD.DELETEDFLAG, :NEW.DELETEDFLAG);
	END IF;		 
END ;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,173,20,'20_CB_QUESTIONS_AU0.sql',sysdate,'9.0.0 Build#630');

commit;