create or replace
FUNCTION        "F_SCH_CODELST_ID" (p_type VARCHAR2, p_subtype VARCHAR2)
RETURN NUMBER
IS
  v_pk_codelst NUMBER;
BEGIN
	 SELECT pk_codelst INTO v_pk_codelst FROM SCH_CODELST WHERE trim(codelst_type) = p_type AND trim(codelst_subtyp) = p_subtype;
  RETURN v_pk_codelst ;
END ;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,164,1,'01_F_SCH_CODELST_ID.sql',sysdate,'9.0.0 Build#621');

commit;