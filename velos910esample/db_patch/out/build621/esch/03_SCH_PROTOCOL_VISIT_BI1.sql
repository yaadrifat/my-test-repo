CREATE OR REPLACE TRIGGER "ESCH"."SCH_PROTOCOL_VISIT_BI1"
BEFORE INSERT
ON SCH_PROTOCOL_VISIT
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
WHEN (
NEW.displacement IS NULL OR NEW.displacement = 0 
      )
DECLARE
ntpd Number;
BEGIN
ntpd :=0;
Select F_SCH_CODELST_ID('timepointtype','NTPD') into ntpd from dual;
IF (:NEW.INSERT_AFTER <0) THEN
   :NEW.NO_INTERVAL_FLAG:=ntpd;
   :NEW.displacement := null;
ELSIF NVL(:NEW.NO_INTERVAL_FLAG,0) = ntpd  THEN
    :NEW.displacement := null;
ELSE
    IF NVL(:NEW.displacement ,0) = 0 THEN
		:NEW.displacement := 1;
    END IF;
  END IF;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,164,3,'03_SCH_PROTOCOL_VISIT_BI1.sql',sysdate,'9.0.0 Build#621');

commit;