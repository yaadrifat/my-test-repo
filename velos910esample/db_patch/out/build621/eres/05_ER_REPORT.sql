set define off;

-- INSERTING into ER_REPORT


DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_report
    where pk_report = 168;
  if (v_record_exists = 0) then
	Insert into ER_REPORT (PK_REPORT,REP_NAME,REP_DESC,REP_SQL,FK_ACCOUNT,FK_CODELST_TYPE,IP_ADD,REP_HIDE,REP_COLUMNS,REP_FILTERBY,GENERATE_XML,REP_TYPE,REP_SQL_CLOB,REP_FILTERKEYWORD,REP_FILTERAPPLICABLE) 
	values (168,'CBU Comprehensive Report','CBU Comprehensive Report','',0,null,null,'Y',null,null,1,'rep_cbu','',null,null);
	COMMIT;	
	
	Update ER_REPORT set REP_SQL = 'select pk_cord, cord_registry_id, registry_maternal_id, cord_local_cbu_id, maternal_local_id, cord_isbi_din_code,
idnotes, cbbid, storage_loc, cbu_collection_site, cbu_info_notes_visible_tc, eligible_status, eligibility_notes_visible_tc,
prcsng_start_date, bact_culture , bact_comment, frz_date, fungal_culture, fung_comment, abo_blood_type,
hemoglobin_scrn, rh_type, lic_status, lab_sum_notes_visible_tc, processing, automated_type,
other_processing, product_modification, other_product_modi, individual_frac, no_of_bags, other_bag,
heparin_thou_per,heparin_thou_ml,heparin_five_per,heparin_five_ml,heparin_ten_per,heparin_ten_ml,heparin_six_per,heparin_six_ml,cpda_per,
cpda_ml,cpd_per,cpd_ml,acd_per,acd_ml,othr_anti_per,othr_anti_ml,speci_othr_anti,
hun_dmso_per,hun_dmso_ml,hun_glycerol_per,hun_glycerol_ml,
ten_dextran_40_per,ten_dextran_40_ml,five_human_albu_per,
five_human_albu_ml,twentyfive_human_albu_per,twentyfive_human_albu_ml, plasmalyte_per,plasmalyte_ml,othr_cryoprotectant_per,
othr_cryoprotectant_ml,spec_othr_cryoprotectant,five_dextrose_per,
five_dextrose_ml,
filter_paper, rpc_pellets, extr_dna, serum_aliquotes, plasma_aliquotes, nonviable_aliquotes, viable_samp_final_prod, no_of_segments,point_nine_nacl_per,point_nine_nacl_ml,othr_diluents_per,
othr_diluents_ml,spec_othr_diluents,productcode,
no_of_oth_rep_alliquots_f_prod, no_of_oth_rep_alliq_alter_cond, no_of_serum_mater_aliquots, no_of_plasma_mater_aliquots,
no_of_extr_dna_mater_aliquots, no_of_cell_mater_aliquots, (select f_hla_cord(~1) from dual) hla FROM rep_cbu_details where pk_cord = ~1' where pk_report = 168;
	COMMIT;

	Update ER_REPORT set REP_SQL_CLOB = 'select pk_cord, cord_registry_id, registry_maternal_id, cord_local_cbu_id, maternal_local_id, cord_isbi_din_code,
idnotes, cbbid, storage_loc, cbu_collection_site, cbu_info_notes_visible_tc, eligible_status, eligibility_notes_visible_tc,
prcsng_start_date, bact_culture , bact_comment, frz_date, fungal_culture, fung_comment, abo_blood_type,
hemoglobin_scrn, rh_type, lic_status, lab_sum_notes_visible_tc, processing, automated_type,
other_processing, product_modification, other_product_modi, individual_frac, no_of_bags, other_bag,
heparin_thou_per,heparin_thou_ml,heparin_five_per,heparin_five_ml,heparin_ten_per,heparin_ten_ml,heparin_six_per,heparin_six_ml,cpda_per,
cpda_ml,cpd_per,cpd_ml,acd_per,acd_ml,othr_anti_per,othr_anti_ml,speci_othr_anti,
hun_dmso_per,hun_dmso_ml,hun_glycerol_per,hun_glycerol_ml,
ten_dextran_40_per,ten_dextran_40_ml,five_human_albu_per,
five_human_albu_ml,twentyfive_human_albu_per,twentyfive_human_albu_ml, plasmalyte_per,plasmalyte_ml,othr_cryoprotectant_per,
othr_cryoprotectant_ml,spec_othr_cryoprotectant,five_dextrose_per,
five_dextrose_ml,
filter_paper, rpc_pellets, extr_dna, serum_aliquotes, plasma_aliquotes, nonviable_aliquotes, viable_samp_final_prod, no_of_segments,point_nine_nacl_per,point_nine_nacl_ml,othr_diluents_per,
othr_diluents_ml,spec_othr_diluents,productcode,
no_of_oth_rep_alliquots_f_prod, no_of_oth_rep_alliq_alter_cond, no_of_serum_mater_aliquots, no_of_plasma_mater_aliquots,
no_of_extr_dna_mater_aliquots, no_of_cell_mater_aliquots, (select f_hla_cord(~1) from dual) hla FROM rep_cbu_details where pk_cord = ~1' where pk_report = 168;
	COMMIT;

  end if;
end;
/


COMMIT;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,164,5,'05_ER_REPORT.sql',sysdate,'9.0.0 Build#621');

commit;

