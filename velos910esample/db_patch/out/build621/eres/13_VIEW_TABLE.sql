set define off;
  
insert into ER_LKPLIB(PK_LKPLIB,
LKPTYPE_NAME,
LKPTYPE_VERSION,
LKPTYPE_DESC,
LKPTYPE_TYPE,
FK_ACCOUNT,
FK_SITE) 
values
((select MAX(PK_LKPLIB)+1 FROM ER_LKPLIB),'dynReports',Null,'CBU INFO VIEW','dyn_a',Null,Null);

  
Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CBU INFO VIEW'),'CORD_REGISTRY_ID','CBU Registry ID','varchar',50,'REP_CBU_ID_VIEW','CORD_REGISTRY_ID' );
  
  
Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CBU INFO VIEW'),'CORD_LOCAL_CBU_ID','CBU Local ID','varchar',50,'REP_CBU_ID_VIEW','CORD_LOCAL_CBU_ID' );
  
  

Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CBU INFO VIEW'),'REGISTRY_MATERNAL_ID','Maternal Registry ID','varchar',50,'REP_CBU_ID_VIEW','REGISTRY_MATERNAL_ID' );
    
  
Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CBU INFO VIEW'),'MATERNAL_LOCAL_ID','Maternal Local ID','varchar',50,'REP_CBU_ID_VIEW','MATERNAL_LOCAL_ID' );
  
  

Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CBU INFO VIEW'),'FUNDING_REQUESTED','Funding Requested','varchar',25,'REP_CBU_ID_VIEW','FUNDING_REQUESTED' );
    


Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CBU INFO VIEW'),'FUNDED_CBU','Funded CBU','varchar',1,'REP_CBU_ID_VIEW','FUNDED_CBU' );
    
Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CBU INFO VIEW'),'CORD_BABY_BIRTH_DATE','Baby Birth Date','date',null,'REP_CBU_ID_VIEW','CORD_BABY_BIRTH_DATE' );
    


Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CBU INFO VIEW'),'CORD_BABY_BIRTH_MONTH','Baby Birth Month','varchar',36,'REP_CBU_ID_VIEW','CORD_BABY_BIRTH_MONTH' );
    


Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CBU INFO VIEW'),'CORD_BABY_BIRTH_YEAR','Baby Birth Year','varchar',4,'REP_CBU_ID_VIEW','CORD_BABY_BIRTH_YEAR' );
    


Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CBU INFO VIEW'),'CBU_BABY_BIR_TIM','Baby Birth Time','varchar',200,'REP_CBU_ID_VIEW','CBU_BABY_BIR_TIM' );
    

Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CBU INFO VIEW'),'BACT_CULT_DATE','Bacterial Culture Start Date','date',null,'REP_CBU_ID_VIEW','BACT_CULT_DATE' );



Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CBU INFO VIEW'),'FUNG_CULT_DATE','Fungal Culture Start Date','date',Null,'REP_CBU_ID_VIEW','FUNG_CULT_DATE' );

Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CBU INFO VIEW'),'BACT_COMMENT','Bacterial Culture Comment','varchar',255,'REP_CBU_ID_VIEW','BACT_COMMENT' );



Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CBU INFO VIEW'),'FUNG_COMMENT','Fungal Culture Comment','varchar',255,'REP_CBU_ID_VIEW','FUNG_COMMENT' );


Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CBU INFO VIEW'),'CORD_BACT_CUL_RESULT','Bacterial Culture','varchar',200,'REP_CBU_ID_VIEW','CORD_BACT_CUL_RESULT' );



Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CBU INFO VIEW'),'CORD_FUNGAL_CUL_RESULT','Fungal Culture','varchar',200,'REP_CBU_ID_VIEW','CORD_FUNGAL_CUL_RESULT' );


Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CBU INFO VIEW'),'CBU_COLLECTION_TIM','CBU Collection Time','varchar',200,'REP_CBU_ID_VIEW','CBU_COLLECTION_TIM' );


Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CBU INFO VIEW'),'CBU_COLLECTION_SITE','CBU Collection Site','varchar',200,'REP_CBU_ID_VIEW','CBU_COLLECTION_SITE' );



Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CBU INFO VIEW'),'CORD_BABY_GENDER_ID','Baby Sex','varchar',200,'REP_CBU_ID_VIEW','CORD_BABY_GENDER_ID' );



Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CBU INFO VIEW'),'CORD_CBU_LIC_STATUS','CBU License','varchar',200,'REP_CBU_ID_VIEW','CORD_CBU_LIC_STATUS' );



Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CBU INFO VIEW'),'CORD_ABO_BLOOD_TYPE','ABO Blood Type','varchar',200,'REP_CBU_ID_VIEW','CORD_ABO_BLOOD_TYPE' );


Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CBU INFO VIEW'),'CORD_RH_TYPE','RH Type','varchar',200,'REP_CBU_ID_VIEW','CORD_RH_TYPE' );

  
Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CBU INFO VIEW'),'CODELST_ETHNICITY','Ethinicity','varchar',200,'REP_CBU_ID_VIEW','CODELST_ETHNICITY');


Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CBU INFO VIEW'),'CBU_COLL_TYPE','Type Of Collection','varchar',200,'REP_CBU_ID_VIEW','CBU_COLL_TYPE');



Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CBU INFO VIEW'),'CBU_DEL_TYPE','Type Of Delivery','varchar',200,'REP_CBU_ID_VIEW','CBU_DEL_TYPE');


Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CBU INFO VIEW'),'MULTIPLE_BIRTH','Multiple Birth','varchar',4000,'REP_CBU_ID_VIEW','MULTIPLE_BIRTH');




Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CBU INFO VIEW'),'RACE','Race','varchar',200,'REP_CBU_ID_VIEW','RACE');



Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CBU INFO VIEW'),'NMDP_RACE_ID','NMDP Breed Race','varchar',200,'REP_CBU_ID_VIEW','NMDP_RACE_ID');



Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CBU INFO VIEW'),'HRSA_RACE_ROLLUP','HRSA Breed Race','varchar',200,'REP_CBU_ID_VIEW','HRSA_RACE_ROLLUP');



Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CBU INFO VIEW'),'HEMOGLOBIN_SCRN','Hemoglobin Screen','varchar',50,'REP_CBU_ID_VIEW','HEMOGLOBIN_SCRN');



Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CBU INFO VIEW'),'CBU_STORAGE_LOCATION','CBU Storage Location','varchar',50,'REP_CBU_ID_VIEW','CBU_STORAGE_LOCATION');



Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CBU INFO VIEW'),'CORD_BLOOD_BANK','Cord Blood Bank','varchar',50,'REP_CBU_ID_VIEW','CORD_BLOOD_BANK');



Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CBU INFO VIEW'),'SPEC_COLLECTION_DATE','Collection Date','date',null,'REP_CBU_ID_VIEW','SPEC_COLLECTION_DATE');


Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CBU INFO VIEW'),'FK_ACCOUNT','Account ID','number',Null,'REP_CBU_ID_VIEW','lkp_pk');


Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CBU INFO VIEW'),'FRZ_DATE','Freeze Date','date',null,'REP_CBU_ID_VIEW','FRZ_DATE' );



Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((Select Max(Pk_Lkpcol)+1 From Er_Lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CBU INFO VIEW'),'CREATOR','Created By','varchar',200,'REP_CBU_ID_VIEW','CREATOR' );


Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((Select Max(Pk_Lkpcol)+1 From Er_Lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CBU INFO VIEW'),'CREATED_ON','Creation Date','date',null,'REP_CBU_ID_VIEW','CREATED_ON' );



Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((Select Max(Pk_Lkpcol)+1 From Er_Lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CBU INFO VIEW'),'LAST_MODIFIED_BY','Last Modified By','number',10,'REP_CBU_ID_VIEW','LAST_MODIFIED_BY' );




Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((Select Max(Pk_Lkpcol)+1 From Er_Lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CBU INFO VIEW'),'LAST_MODIFIED_DATE','Last Modified Date','date',null,'REP_CBU_ID_VIEW','LAST_MODIFIED_DATE' );

insert into ER_LKPVIEW 
(Pk_Lkpview,Lkpview_Name,Lkpview_Desc,Fk_Lkplib)
Values((select max(pk_lkpview)+1 from er_lkpview),'CBU INFO VIEW',Null,(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CBU INFO VIEW'));


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
LKPVIEW_IS_DISPLAY) values((select max(pk_lkpviewcol)+1 from er_lkpviewcol),(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME ='CORD_REGISTRY_ID' and LKPCOL_TABLE='REP_CBU_ID_VIEW'),
Null,1,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU INFO VIEW'),'10%','Y' );





insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
LKPVIEW_IS_DISPLAY) values((select max(pk_lkpviewcol)+1 from er_lkpviewcol),(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME ='CORD_LOCAL_CBU_ID' and LKPCOL_TABLE='REP_CBU_ID_VIEW'),
Null,2,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU INFO VIEW'),'10%','Y' );



insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='REGISTRY_MATERNAL_ID' And Lkpcol_Table='REP_CBU_ID_VIEW'),
Null,3,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU INFO VIEW'),'10%','Y' );




insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='MATERNAL_LOCAL_ID' And Lkpcol_Table='REP_CBU_ID_VIEW'),
Null,4,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU INFO VIEW'),'10%','Y' );



insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FUNDING_REQUESTED' And Lkpcol_Table='REP_CBU_ID_VIEW'),
Null,5,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU INFO VIEW'),'10%','Y' );



insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FUNDED_CBU' And Lkpcol_Table='REP_CBU_ID_VIEW'),
Null,6,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU INFO VIEW'),'10%','Y' );



insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='CORD_BABY_BIRTH_DATE' And Lkpcol_Table='REP_CBU_ID_VIEW'),
Null,7,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU INFO VIEW'),'10%','Y' );



insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='CORD_BABY_BIRTH_MONTH' And Lkpcol_Table='REP_CBU_ID_VIEW'),
Null,8,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU INFO VIEW'),'10%','Y' );



insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='CORD_BABY_BIRTH_YEAR' And Lkpcol_Table='REP_CBU_ID_VIEW'),
Null,9,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU INFO VIEW'),'10%','Y' );




insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='CBU_BABY_BIR_TIM' And Lkpcol_Table='REP_CBU_ID_VIEW'),
Null,10,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU INFO VIEW'),'10%','Y' );



insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FRZ_DATE' And Lkpcol_Table='REP_CBU_ID_VIEW'),
Null,11,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU INFO VIEW'),'10%','Y' );



insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='BACT_CULT_DATE' And Lkpcol_Table='REP_CBU_ID_VIEW'),
Null,12,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU INFO VIEW'),'10%','Y' );



insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FUNG_CULT_DATE' And Lkpcol_Table='REP_CBU_ID_VIEW'),
Null,13,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU INFO VIEW'),'10%','Y' );




insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='BACT_COMMENT' And Lkpcol_Table='REP_CBU_ID_VIEW'),
Null,14,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU INFO VIEW'),'10%','Y' );



insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FUNG_COMMENT' And Lkpcol_Table='REP_CBU_ID_VIEW'),
Null,15,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU INFO VIEW'),'10%','Y' );



insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='CORD_BACT_CUL_RESULT' And Lkpcol_Table='REP_CBU_ID_VIEW'),
Null,16,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU INFO VIEW'),'10%','Y' );





insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='CORD_FUNGAL_CUL_RESULT' And Lkpcol_Table='REP_CBU_ID_VIEW'),
Null,17,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU INFO VIEW'),'10%','Y' );



insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='CBU_COLLECTION_TIM' And Lkpcol_Table='REP_CBU_ID_VIEW'),
Null,18,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU INFO VIEW'),'10%','Y' );



insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='CBU_COLLECTION_SITE' And Lkpcol_Table='REP_CBU_ID_VIEW'),
Null,19,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU INFO VIEW'),'10%','Y' );



insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='CORD_BABY_GENDER_ID' And Lkpcol_Table='REP_CBU_ID_VIEW'),
Null,20,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU INFO VIEW'),'10%','Y' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='CORD_CBU_LIC_STATUS' And Lkpcol_Table='REP_CBU_ID_VIEW'),
Null,21,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU INFO VIEW'),'10%','Y' );



insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='CORD_ABO_BLOOD_TYPE' And Lkpcol_Table='REP_CBU_ID_VIEW'),
Null,22,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU INFO VIEW'),'10%','Y' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='CORD_RH_TYPE' And Lkpcol_Table='REP_CBU_ID_VIEW'),
Null,23,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU INFO VIEW'),'10%','Y' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='CODELST_ETHNICITY' And Lkpcol_Table='REP_CBU_ID_VIEW'),
Null,24,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU INFO VIEW'),'10%','Y' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='CBU_COLL_TYPE' And Lkpcol_Table='REP_CBU_ID_VIEW'),
Null,25,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU INFO VIEW'),'10%','Y' );



insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='CBU_DEL_TYPE' And Lkpcol_Table='REP_CBU_ID_VIEW'),
Null,26,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU INFO VIEW'),'10%','Y' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='MULTIPLE_BIRTH' And Lkpcol_Table='REP_CBU_ID_VIEW'),
Null,27,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU INFO VIEW'),'10%','Y' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='RACE' And Lkpcol_Table='REP_CBU_ID_VIEW'),
Null,28,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU INFO VIEW'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='NMDP_RACE_ID' And Lkpcol_Table='REP_CBU_ID_VIEW'),
Null,29,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU INFO VIEW'),'10%','Y' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='HRSA_RACE_ROLLUP' And Lkpcol_Table='REP_CBU_ID_VIEW'),
Null,30,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU INFO VIEW'),'10%','Y' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='HEMOGLOBIN_SCRN' And Lkpcol_Table='REP_CBU_ID_VIEW'),
Null,31,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU INFO VIEW'),'10%','Y' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='CBU_STORAGE_LOCATION' And Lkpcol_Table='REP_CBU_ID_VIEW'),
Null,32,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU INFO VIEW'),'10%','Y' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='CORD_BLOOD_BANK' And Lkpcol_Table='REP_CBU_ID_VIEW'),
Null,33,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU INFO VIEW'),'10%','Y' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='SPEC_COLLECTION_DATE' And Lkpcol_Table='REP_CBU_ID_VIEW'),
Null,34,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU INFO VIEW'),'10%','Y' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FK_ACCOUNT' And Lkpcol_Table='REP_CBU_ID_VIEW'),
Null,35,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU INFO VIEW'),'10%','N' );



insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='CREATOR' And Lkpcol_Table='REP_CBU_ID_VIEW'),
Null,36,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU INFO VIEW'),'10%','Y' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='CREATED_ON' And Lkpcol_Table='REP_CBU_ID_VIEW'),
Null,37,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU INFO VIEW'),'10%','Y' );




insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='LAST_MODIFIED_BY' And Lkpcol_Table='REP_CBU_ID_VIEW'),
Null,38,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU INFO VIEW'),'10%','N' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='LAST_MODIFIED_DATE' And Lkpcol_Table='REP_CBU_ID_VIEW'),
Null,39,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU INFO VIEW'),'10%','Y' );

commit;


------------------------------CBU_CBB_PROCESSING_PROCEDURE---------------------

insert into ER_LKPLIB(PK_LKPLIB,
LKPTYPE_NAME,
LKPTYPE_VERSION,
LKPTYPE_DESC,
LKPTYPE_TYPE,
FK_ACCOUNT,
FK_SITE) values((select MAX(PK_LKPLIB)+1 FROM ER_LKPLIB),'dynReports',null,'CBU CBB PROCESSING PROCEDURE','dyn_a',null,null);
  
 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'CORD_LOCAL_CBU_ID','CBU LOCAL ID','VARCHAR',50,'REP_CBU_CBB_PROCESSING_PROCEDURE','CORD_LOCAL_CBU_ID' ); 
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'CORD_REGISTRY_ID','CBU REGISTRY ID','VARCHAR',50,'REP_CBU_CBB_PROCESSING_PROCEDURE','CORD_REGISTRY_ID' ); 

 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'REGISTRY_MATERNAL_ID','MATERNAL REGISTRY ID','VARCHAR',50,'REP_CBU_CBB_PROCESSING_PROCEDURE','REGISTRY_MATERNAL_ID' ); 
   
   insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'MATERNAL_LOCAL_ID','MATERNAL LOCAL ID','VARCHAR',50,'REP_CBU_CBB_PROCESSING_PROCEDURE','MATERNAL_LOCAL_ID' );
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'  FK_ACCOUNT',' ACCOUNT ID ','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','  lkp_pk' );
 

 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'PROC_NAME','Procedure Name','VARCHAR',50,'REP_CBU_CBB_PROCESSING_PROCEDURE','PROC_NAME' );
  

   insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'PROC_START_DATE','Procedure Start Date','DATE',NULL,'REP_CBU_CBB_PROCESSING_PROCEDURE','PROC_START_DATE' );
  
 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'PRODUCT_CODE','Product Code','VARCHAR',50,'REP_CBU_CBB_PROCESSING_PROCEDURE','PRODUCT_CODE' );
  

insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'PROC_TERMI_DATE','Procedure Termination Date','DATE',NULL,'REP_CBU_CBB_PROCESSING_PROCEDURE','PROC_TERMI_DATE' );
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'FK_PROC_METH_ID','Processing','VARCHAR',50,'REP_CBU_CBB_PROCESSING_PROCEDURE','FK_PROC_METH_ID' );
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'FK_IF_AUTOMATED','Automated process to modify CBU ','NUMBER',20,'REP_CBU_CBB_PROCESSING_PROCEDURE','FK_IF_AUTOMATED' );
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'OTHER_PROCESSING','Please Specify ','VARCHAR',1020,'REP_CBU_CBB_PROCESSING_PROCEDURE','OTHER_PROCESSING' );
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'FK_PRODUCT_MODIFICATION','Product Modification ','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','FK_PRODUCT_MODIFICATION' );
  
 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'OTHER_PROD_MODI','Please Specify ','VARCHAR',1020,'REP_CBU_CBB_PROCESSING_PROCEDURE','OTHER_PROD_MODI' );
  
 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'FK_STOR_METHOD','Storage Method','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','FK_STOR_METHOD' );
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'FK_FREEZ_MANUFAC','Freezer Manufacture','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','FK_FREEZ_MANUFAC' );
  
 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'OTHER_FREEZ_MANUFAC','Please Specify','VARCHAR',1020,'REP_CBU_CBB_PROCESSING_PROCEDURE','OTHER_FREEZ_MANUFAC' );
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'FK_FROZEN_IN','Frozen vials,tubes,bags or others','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','FK_FROZEN_IN' );
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'OTHER_FROZEN_CONT','Please Specify','VARCHAR',1020,'REP_CBU_CBB_PROCESSING_PROCEDURE','OTHER_FROZEN_CONT' );
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'FK_NUM_OF_BAGS','Number of bags','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','FK_NUM_OF_BAGS' );
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'CRYOBAG_MANUFAC','CryoBag Manufacturer','VARCHAR',1020,'REP_CBU_CBB_PROCESSING_PROCEDURE','CRYOBAG_MANUFAC' );
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'FK_BAG_TYPE','Bag Type','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','FK_BAG_TYPE' );
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'FK_BAG_1_TYPE','Bag 1 Type','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','FK_BAG_1_TYPE' );
   
 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'FK_BAG_2_TYPE','Bag 2 Type','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','FK_BAG_2_TYPE' );
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'OTHER_BAG_TYPE','Please Specify','VARCHAR',120,'REP_CBU_CBB_PROCESSING_PROCEDURE','OTHER_BAG_TYPE' );
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'FK_STOR_TEMP','Storage Temperature','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','FK_STOR_TEMP' );
   
 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'MAX_VALUE','Maximum Volume(ml)','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','MAX_VALUE' );
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'FK_CONTRL_RATE_FREEZING','Controlled Rate Freezing','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','FK_CONTRL_RATE_FREEZING' );
   
 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'NO_OF_INDI_FRAC','Number OF Individual Fractions','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','NO_OF_INDI_FRAC' );
   

 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'THOU_UNIT_PER_ML_HEPARIN_PER','1000 unit/ml Heparin(%)','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','THOU_UNIT_PER_ML_HEPARIN_PER' );
   
  insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'THOU_UNIT_PER_ML_HEPARIN','1000 unit/ml Heparin(ML)','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','THOU_UNIT_PER_ML_HEPARIN' );
   
 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'FIVE_UNIT_PER_ML_HEPARIN_PER','5000 unit/ml Heparin(%)','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','FIVE_UNIT_PER_ML_HEPARIN_PER' );
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'FIVE_UNIT_PER_ML_HEPARIN','5000 unit/ml Heparin(ML)','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','FIVE_UNIT_PER_ML_HEPARIN' );
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'TEN_UNIT_PER_ML_HEPARIN_PER','10,000 unit/ml Heparin(%)','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','TEN_UNIT_PER_ML_HEPARIN_PER' );
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'TEN_UNIT_PER_ML_HEPARIN','10,000 unit/ml Heparin(ML)','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','TEN_UNIT_PER_ML_HEPARIN' );
    
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),' SIX_PER_HYDRO_STARCH_PER','6% Hydroxyethyl Starch( %)','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE',' SIX_PER_HYDRO_STARCH_PER' );
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),' SIX_PER_HYDROXYETHYL_STARCH','6% Hydroxyethyl Starch( ML)','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE',' SIX_PER_HYDROXYETHYL_STARCH' );
   
 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),' CPDA_PER','CPDA( % ) ','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE',' CPDA_PER' );
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),' CPDA','CPDA( ML ) ','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE',' CPDA' );
   
 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'CPD_PER','CPD( % ) ','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','CPD_PER' );
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'CPD','CPD( ML ) ','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','CPD' );
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'ACD_PER','ACD( % ) ','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','ACD_PER' );
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'ACD','ACD( ML ) ','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','ACD' );
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'OTHER_ANTICOAGULANT_PER','Other Anticoagulant( % )','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','OTHER_ANTICOAGULANT_PER' );
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'OTHER_ANTICOAGULANT','Other Anticoagulant( ML )','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','OTHER_ANTICOAGULANT' );
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'SPECIFY_OTH_ANTI','Specify Other Anticoagulant','VARCHAR',1020,'REP_CBU_CBB_PROCESSING_PROCEDURE','SPECIFY_OTH_ANTI' );
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'HUN_PER_DMSO_PER','100% DMSO( % )','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','HUN_PER_DMSO_PER' );
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'HUN_PER_DMSO','100% DMSO( ML )','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','HUN_PER_DMSO' );
   
 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'HUN_PER_GLYCEROL_PER','100% Glycerol( % )','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','HUN_PER_GLYCEROL_PER' );
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'HUN_PER_GLYCEROL','100% Glycerol( ML )','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','HUN_PER_GLYCEROL' );
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'TEN_PER_DEXTRAN_40_PER','10% Dextran-40( % )','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','TEN_PER_DEXTRAN_40_PER' );
   
 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'TEN_PER_DEXTRAN_40','10% Dextran-40( ML )','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','TEN_PER_DEXTRAN_40' );
   
 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'FIVE_PER_HUMAN_ALBU_PER','5% Human Albumin( % )','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','FIVE_PER_HUMAN_ALBU_PER' );
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'FIVE_PER_HUMAN_ALBU','5% Human Albumin( ML )','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','FIVE_PER_HUMAN_ALBU' );
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'TWEN_FIVE_HUM_ALBU_PER','25% Human Albumin( % )','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','TWEN_FIVE_HUM_ALBU_PER' );
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'TWEN_FIVE_HUM_ALBU','25% Human Albumin( ML )','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','TWEN_FIVE_HUM_ALBU' );
   
 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'PLASMALYTE_PER','Plasmalyte( % )','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','PLASMALYTE_PER' );
   
 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'PLASMALYTE','Plasmalyte( ML )','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','PLASMALYTE' );
   
 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'OTH_CRYOPROTECTANT_PER','Other Cryoprotectant( % )','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','OTH_CRYOPROTECTANT_PER' );
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'OTH_CRYOPROTECTANT','Other Cryoprotectant( ML )','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','OTH_CRYOPROTECTANT' );
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'SPEC_OTH_CRYOPRO','Specify Other Cryoprotectant','VARCHAR',1024,'REP_CBU_CBB_PROCESSING_PROCEDURE','SPEC_OTH_CRYOPRO' );
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'FIVE_PER_DEXTROSE_PER','5% Dextrose( % )','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','FIVE_PER_DEXTROSE_PER' );
   
 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'FIVE_PER_DEXTROSE','5% Dextrose( ML )','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','FIVE_PER_DEXTROSE' );
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'POINNT_NINE_PER_NACL_PER','0.9% NaCl( % )','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','POINNT_NINE_PER_NACL_PER' );
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'POINNT_NINE_PER_NACL','0.9% NaCl( ML )','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','POINNT_NINE_PER_NACL' );
  
 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'OTH_DILUENTS_PER','Other Diluents( % )','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','OTH_DILUENTS_PER' );
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'OTH_DILUENTS','Other Diluents( ML )','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','OTH_DILUENTS' );
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'SPEC_OTH_DILUENTS','Specify Other Diluents','VARCHAR',1020,'REP_CBU_CBB_PROCESSING_PROCEDURE','SPEC_OTH_DILUENTS' );
  
 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'FILTER_PAPER','Number of Filter Paper Samples','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','FILTER_PAPER' );
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'RBC_PALLETS','Number of RBC Pellets ','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','RBC_PALLETS' );
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'NO_OF_EXTR_DNA_ALIQUOTS','Number of Extracted DNA Samples ','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','NO_OF_EXTR_DNA_ALIQUOTS' );
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'NO_OF_SERUM_ALIQUOTS','Number of Serum Samples ','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','NO_OF_SERUM_ALIQUOTS' );
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'NO_OF_PLASMA_ALIQUOTS','Number of Plasma Samples ','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','NO_OF_PLASMA_ALIQUOTS' );
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'NO_OF_NONVIABLE_ALIQUOTS','Number of Non-Viable Cell samples ','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','NO_OF_NONVIABLE_ALIQUOTS' );
  
 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'NO_OF_VIABLE_SMPL_FINAL_PROD','NO. OF viable samples not represent final product ','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','NO_OF_VIABLE_SMPL_FINAL_PROD' );
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'NO_OF_SEGMENTS',' Number of Segments ','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','NO_OF_SEGMENTS' );
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'NO_OF_OTH_REP_ALLIQUOTS_F_PROD',' No. of other representative aliquots ','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','NO_OF_OTH_REP_ALLIQUOTS_F_PROD' );
  
 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'NO_OF_OTH_REP_ALLIQ_ALTER_COND',' Number of Maternal Serum Samples ','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','NO_OF_OTH_REP_ALLIQ_ALTER_COND' );
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'NO_OF_SERUM_MATER_ALIQUOTS',' Number of Maternal Plasma Samples ','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','NO_OF_SERUM_MATER_ALIQUOTS' );
  
  insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'NO_OF_PLASMA_MATER_ALIQUOTS',' Number of Maternal Extracted DNA Samples ','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','NO_OF_PLASMA_MATER_ALIQUOTS' );
  
 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'NO_OF_EXTR_DNA_MATER_ALIQUOTS',' Number of Miscellaneous Maternal Samples ','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','NO_OF_EXTR_DNA_MATER_ALIQUOTS' );
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'NO_OF_CELL_MATER_ALIQUOTS',' NO. of Miscellaneous Maternal Samples','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROCEDURE','NO_OF_CELL_MATER_ALIQUOTS' );
  
  
insert into ER_LKPVIEW 
(PK_LKPVIEW,LKPVIEW_NAME,LKPVIEW_DESC,FK_LKPLIB)
values((select MAX(PK_LKPVIEW)+1 FROM ER_LKPVIEW),'CBU CBB PROCESSING PROCEDURE',null,(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'));

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='CORD_LOCAL_CBU_ID' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,1,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='CORD_REGISTRY_ID' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,2,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='REGISTRY_MATERNAL_ID' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,3,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='MATERNAL_LOCAL_ID' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,4,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FK_ACCOUNT' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,5,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','N' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='PROC_NAME' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,6,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='PROC_START_DATE' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,7,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='PRODUCT_CODE' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,8,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='PROC_TERMI_DATE' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,9,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FK_PROC_METH_ID' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,10,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FK_IF_AUTOMATED' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,11,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='OTHER_PROCESSING' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,12,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FK_PRODUCT_MODIFICATION' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,13,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='OTHER_PROD_MODI' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,14,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FK_STOR_METHOD' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,15,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FK_FREEZ_MANUFAC' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,16,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='OTHER_FREEZ_MANUFAC' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,17,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FK_FROZEN_IN' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,18,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='OTHER_FROZEN_CONT' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,19,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FK_NUM_OF_BAGS' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,20,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='CRYOBAG_MANUFAC' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,21,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FK_BAG_TYPE' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,22,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FK_BAG_1_TYPE' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,23,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FK_BAG_2_TYPE' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,24,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='OTHER_BAG_TYPE' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,25,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FK_STOR_TEMP' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,26,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='MAX_VALUE' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,27,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FK_CONTRL_RATE_FREEZING' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,28,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='NO_OF_INDI_FRAC' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,29,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='THOU_UNIT_PER_ML_HEPARIN_PER' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,30,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='THOU_UNIT_PER_ML_HEPARIN' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,31,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FIVE_UNIT_PER_ML_HEPARIN_PER' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,32,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FIVE_UNIT_PER_ML_HEPARIN' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,33,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='TEN_UNIT_PER_ML_HEPARIN_PER' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,34,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='TEN_UNIT_PER_ML_HEPARIN' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,35,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='SIX_PER_HYDRO_STARCH_PER' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,36,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='SIX_PER_HYDROXYETHYL_STARCH' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,37,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='CPDA_PER' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,38,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='CPDA' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,39,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='CPD_PER' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,40,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='CPD' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,41,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='ACD_PER' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,42,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='ACD' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,43,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='OTHER_ANTICOAGULANT_PER' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,44,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='OTHER_ANTICOAGULANT' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,45,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='SPECIFY_OTH_ANTI' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,46,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='HUN_PER_DMSO_PER' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,47,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='HUN_PER_DMSO' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,48,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='HUN_PER_GLYCEROL_PER' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,49,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='HUN_PER_GLYCEROL' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,50,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='TEN_PER_DEXTRAN_40_PER' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,51,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='TEN_PER_DEXTRAN_40' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,52,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FIVE_PER_HUMAN_ALBU_PER' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,53,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FIVE_PER_HUMAN_ALBU' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,54,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='TWEN_FIVE_HUM_ALBU_PER' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,55,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='TWEN_FIVE_HUM_ALBU' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,56,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='PLASMALYTE_PER' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,57,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='PLASMALYTE' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,58,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='OTH_CRYOPROTECTANT_PER' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,59,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='OTH_CRYOPROTECTANT' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,60,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='SPEC_OTH_CRYOPRO' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,61,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FIVE_PER_DEXTROSE_PER' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,62,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FIVE_PER_DEXTROSE' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,63,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='POINNT_NINE_PER_NACL_PER' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,64,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='POINNT_NINE_PER_NACL' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,65,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='OTH_DILUENTS_PER' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,66,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='OTH_DILUENTS' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,67,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='SPEC_OTH_DILUENTS' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,68,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FILTER_PAPER' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,69,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='RBC_PALLETS' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,70,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='NO_OF_EXTR_DNA_ALIQUOTS' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,71,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','N' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='NO_OF_SERUM_ALIQUOTS' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,72,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','N' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='NO_OF_PLASMA_ALIQUOTS' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,73,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','N' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='NO_OF_NONVIABLE_ALIQUOTS' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,74,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','N' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='NO_OF_VIABLE_SMPL_FINAL_PROD' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,75,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','N' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='NO_OF_SEGMENTS' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,76,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','N' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='NO_OF_OTH_REP_ALLIQUOTS_F_PROD' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,77,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','N' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='NO_OF_OTH_REP_ALLIQ_ALTER_COND' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,78,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','N' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='NO_OF_SERUM_MATER_ALIQUOTS' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,79,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','N' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='NO_OF_PLASMA_MATER_ALIQUOTS' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,80,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','N' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='NO_OF_EXTR_DNA_MATER_ALIQUOTS' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,81,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','N' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='NO_OF_CELL_MATER_ALIQUOTS' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROCEDURE'),
Null,82,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','N' );

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,164,13,'13_VIEW_TABLE.sql',sysdate,'9.0.0 Build#621');

commit;

