set define off;

Update track_patches set APP_VERSION = '8.9.0 Build#525' where APP_VERSION = '8.8.0 Build#525';

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,68,1,'01_update_er_version.sql',sysdate,'8.9.0 Build#525');

commit;