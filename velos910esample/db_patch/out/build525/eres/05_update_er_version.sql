set define off;

UPDATE ER_CTRLTAB SET CTRL_VALUE = '8.9.0 Build#525' where CTRL_KEY = 'app_version' ; 

commit;

Update track_patches set APP_VERSION = '8.9.0 Build#525' where APP_VERSION = '8.8.0 Build#525';

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,68,5,'05_update_er_version.sql',sysdate,'8.9.0 Build#525');

commit;