alter table ER_FORMQUERYSTATUS modify (
LAST_MODIFIED_DATE Date default (null)
);


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,68,2,'02_alter_ER_FORMQUERYSTATUS.sql',sysdate,'8.8.0 Build#525');

commit;
