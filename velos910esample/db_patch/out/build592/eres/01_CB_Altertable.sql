----------------------------CBB-----------------------------------------
ALTER TABLE CBB ADD MEMBER VARCHAR2(1);
COMMENT ON COLUMN "CBB"."MEMBER" IS 'This column denotes whether this CBB is a member or not'; 
-------------------------------------------------------------------------

------------------------------CB_CORD Alter-----------------------------------------------------------------

alter table CB_CORD add (REGISTRY_MATERNAL_ID varchar2(50), 
MATERNAL_LOCAL_ID varchar2(50), 
FK_CODELST_ETHNICITY number(10), 
FK_CODELST_RACE number(10), 
FUNDING_REQUESTED varchar2(25), FUNDED_CBU varchar2(1) ,CORD_CDR_CBU_CREATED_BY varchar2(255),CORD_CDR_CBU_LAST_MOD_BY varchar2(255),CORD_CDR_CBU_LAST_MOD_DT DATE,CORD_CREATION_DATE DATE);

------------------------------------------------------------------------------------------------------------

COMMENT ON COLUMN "CB_CORD"."FK_CODELST_ETHNICITY" IS 'Code List reference for Ethnicity ( Alerts are stored in ER_CODELST)';
COMMENT ON COLUMN "CB_CORD"."FK_CODELST_RACE" IS 'Code List reference for Race (Values are stored in ER_CODELST)';
COMMENT ON COLUMN "CB_CORD"."REGISTRY_MATERNAL_ID" IS 'Maternal Registry ID for Cord';
COMMENT ON COLUMN "CB_CORD"."MATERNAL_LOCAL_ID" IS 'Maternal Local ID for Cord';
COMMENT ON COLUMN "CB_CORD"."FUNDING_REQUESTED" IS 'Information for CBU Funding';
COMMENT ON COLUMN "CB_CORD"."FUNDED_CBU" IS 'Flag for marking this CBU as funded';
COMMENT ON COLUMN "CB_CORD"."CORD_CDR_CBU_CREATED_BY" IS 'Cord record created by - ESB Message storage.';
COMMENT ON COLUMN "CB_CORD"."CORD_CDR_CBU_LAST_MOD_BY" IS  'Cord record modified by - ESB Message storage.';
COMMENT ON COLUMN "CB_CORD"."CORD_CDR_CBU_LAST_MOD_DT" IS 'Cord Record Last Modified - From ESB messages.';
COMMENT ON COLUMN "CB_CORD"."CORD_CREATION_DATE" IS 'Cord Creation date from ESB Messages.';


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,135,1,'01_CB_Altertable.sql',sysdate,'9.0.0 Build#592');

commit;
