set define off;

DECLARE

countFlage1 NUMBER(5);
BEGIN

SELECT COUNT(1) INTO countFlage1 FROM USER_TABLES WHERE TABLE_NAME='ER_OBJECT_SETTINGS';
if (countFlage1 > 0) then
   UPDATE ER_OBJECT_SETTINGS SET OBJECT_SEQUENCE = 7 WHERE object_name='protocol_tab' and OBJECT_DISPLAYTEXT = 'Calendar Milestone Setup';
  UPDATE ER_OBJECT_SETTINGS SET OBJECT_SEQUENCE = 8 WHERE object_name='protocol_tab' and OBJECT_DISPLAYTEXT = 'Manage Budget';
  UPDATE ER_OBJECT_SETTINGS SET OBJECT_SEQUENCE = 9 WHERE object_name='protocol_tab' and OBJECT_DISPLAYTEXT = 'Manage Milestones';
  commit;
end if;

END;
/

DECLARE 
  row_count NUMBER; 
BEGIN 
  select count(*) into row_count from
  ER_OBJECT_SETTINGS where OBJECT_SUBTYPE = '12' and 
  OBJECT_NAME = 'study_tab';
  if (row_count = 1) then 
  
	Update ER_OBJECT_SETTINGS Set OBJECT_VISIBLE = 1 where OBJECT_NAME = 'study_tab' and OBJECT_SEQUENCE = 6;   
  end if;	
  
   
END;
/

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,114,6,'06_ER_OBJECT_SETTINGS.sql',sysdate,'8.10.0 Build#571');

commit;