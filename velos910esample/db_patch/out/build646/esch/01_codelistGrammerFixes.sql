set define off;

update esch.sch_codelst set codelst_desc = 'Not done' where codelst_desc = 'Not Done';

update esch.sch_codelst set codelst_desc = 'Not Required' where codelst_desc = 'Not Required';

update esch.sch_codelst set codelst_desc = 'To be rescheduled' where codelst_desc = 'To Be Rescheduled';

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,198,1,'01_codelistGrammerFixes.sql',sysdate,'9.1.0 Build#646');

commit;
