CREATE OR REPLACE FORCE VIEW ERV_PATIENT_MORE_DETAILS
(
   FIELD_NAME,
   FIELD_VALUE,
   FK_PER,
   CREATED_ON,
   FK_ACCOUNT,
   PTMPD_PAT_ID,
   PTMPD_PAT_NAME,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   RID,
   PTMPD_RESPONSE_ID
)
AS
   SELECT   (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = fk_codelst_idtype)
               AS field_name,
            perid_id AS field_value,
            fk_per,
	    to_date(pkg_util.date_to_char_with_time (a.CREATED_ON) , PKG_DATEUTIL.f_get_datetimeformat) CREATED_ON,
            fk_account,
            PER_CODE PTMPD_PAT_ID,
            (SELECT   PERSON_FNAME || ' ' || PERSON_LNAME
               FROM   PERSON
              WHERE   PK_PER = PK_PERSON)
               PTMPD_PAT_NAME,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   er_user
              WHERE   pk_user = A.CREATOR)
               creator,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   er_user
              WHERE   pk_user = A.LAST_MODIFIED_BY)
               last_modified_by,
	    to_date(pkg_util.date_to_char_with_time (a.LAST_MODIFIED_DATE) , PKG_DATEUTIL.f_get_datetimeformat) LAST_MODIFIED_DATE,
            A.RID,
            PK_PERID PTMPD_RESPONSE_ID
     FROM   pat_perid a, ER_PER
    WHERE   pk_per = fk_per;



CREATE OR REPLACE FORCE VIEW REP_ADV_MORE_DETAILS
(
   PK_MOREDETAILS,
   FIELD_NAME,
   FIELD_VALUE,
   FK_PER,
   CREATED_ON,
   FK_ACCOUNT,
   PAT_ID,
   PAT_NAME,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   RID,
   RESPONSE_ID,
   ADVE_TYPE,
   ADVE_NAME,
   AE_STDATE,
   STUDY_NUMBER,
   FK_STUDY
)
AS
   SELECT   PK_MOREDETAILS,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = MD_MODELEMENTPK)
               field_name,
            MD_MODELEMENTDATA AS field_value,
            pk_person,
            to_date(pkg_util.date_to_char_with_time (a.CREATED_ON) , PKG_DATEUTIL.f_get_datetimeformat) CREATED_ON,
            fk_account,
            PERson_CODE PAT_ID,
            (PERSON_FNAME || ' ' || PERSON_LNAME) PAT_NAME,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = A.CREATOR)
               creator,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = A.LAST_MODIFIED_BY)
               last_modified_by,
            to_date(pkg_util.date_to_char_with_time (a.LAST_MODIFIED_DATE) , PKG_DATEUTIL.f_get_datetimeformat) LAST_MODIFIED_DATE,
            A.RID,
            PK_MOREDETAILS RESPONSE_ID,
            (SELECT   codelst_desc
               FROM   SCH_CODELST
              WHERE   pk_codelst = fk_codlst_aetype)
               adve_type,
            ae_name,
            ae_stdate,
            (SELECT   study_number
               FROM   er_study
              WHERE   pk_study = fk_study)
               study_number,
            fk_study
     FROM   er_moredetails a, person, sch_adverseve
    WHERE       a.MD_MODNAME = 'advtype'
            AND FK_MODPK = pk_adveve
            AND pk_person = fk_per;


/* Formatted on 2/9/2010 1:39:50 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW REP_BUDGET_LINEITEM
(
   BGTLI_BUDGET_NAME,
   BGTLI_BUDGET_STATUS,
   BGTLI_STUDY_NUM,
   BGTLI_ORGANIZATION,
   BGTLI_STUDY_TITLE,
   BGTLI_SECTION,
   BGTLI_EVENT,
   BGTLI_CATEGORY,
   BGTLI_APPLY_INDIR,
   BGTLI_SOC,
   BGTLI_UNIT_COST,
   BGTLI_NUM_UNITS,
   BGTLI_APPLY_DISCOUNT,
   BGTLI_RESPONSE_ID,
   RID,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATED_ON,
   FK_STUDY,
   FK_ACCOUNT,
   LINEITM_TOTAL_COST,
   LINEITEM_RESCOST
)
AS
   SELECT   B.BUDGET_NAME BGTLI_BUDGET_NAME,
            F_Get_Fstat (B.BUDGET_STATUS) BGTLI_BUDGET_STATUS,
            (SELECT   STUDY_NUMBER
               FROM   ER_STUDY
              WHERE   PK_STUDY = B.FK_STUDY)
               BGTLI_STUDY_NUM,
            (SELECT SITE_NAME 
               FROM  ER_SITE
              WHERE PK_SITE = B.FK_SITE)
               BGTLI_ORGANIZATION,
            (SELECT   STUDY_TITLE
               FROM   ER_STUDY
              WHERE   PK_STUDY = B.FK_STUDY)
               BGTLI_STUDY_TITLE,
            BGTSECTION_NAME BGTLI_SECTION,
            LINEITEM_NAME BGTLI_EVENT,
            F_Get_Codelstdesc (FK_CODELST_CATEGORY) BGTLI_CATEGORY,
            F_Get_Yesno (LINEITEM_APPLYINDIRECTS) BGTLI_APPLY_INDIR,
            LINEITEM_STDCARECOST BGTLI_SOC,
            LINEITEM_SPONSORUNIT BGTLI_UNIT_COST,
            LINEITEM_CLINICNOFUNIT BGTLI_NUM_UNITS,
            F_Get_Yesno (LINEITEM_INCOSTDISC) BGTLI_APPLY_DISCOUNT,
            PK_LINEITEM BGTLI_RESPONSE_ID,
            L.RID,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = L.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = L.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            L.LAST_MODIFIED_DATE,
            L.CREATED_ON,
            B.FK_STUDY FK_STUDY,
            B.FK_ACCOUNT FK_ACCOUNT,
            lineitem_totalcost LINEITM_TOTAL_COST,
            LINEITEM_RESCOST LINEITEM_RESCOST
     FROM   SCH_LINEITEM L,
            SCH_BGTSECTION S,
            SCH_BGTCAL Q,
            SCH_BUDGET B
    WHERE       L.FK_BGTSECTION = S.PK_BUDGETSEC
            AND S.FK_BGTCAL = Q.PK_BGTCAL
            AND Q.FK_BUDGET = B.PK_Budget
	    AND Q.BGTCAL_PROTID is not null
            AND (bgtsection_DELFLAG = 'N' OR bgtsection_delflag IS NULL);


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,84,3,'03_Views.sql',sysdate,'8.9.0 Build#541');

commit;

