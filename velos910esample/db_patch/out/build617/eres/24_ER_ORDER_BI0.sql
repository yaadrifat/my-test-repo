create or replace TRIGGER "ERES".ER_ORDER_BI0 BEFORE INSERT ON ER_ORDER
       REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW  WHEN (NEW.rid IS NULL OR NEW.rid = 0) DECLARE
  raid NUMBER(10);
  erid NUMBER(10);
  usr VARCHAR(2000);
      insert_data CLOB;
     BEGIN
 BEGIN
 SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
 INTO usr FROM er_user
 WHERE pk_user = :NEW.creator ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
   USR := 'New User' ;
 END ;

                  SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  audit_trail.record_transaction(raid, 'ER_ORDER',erid, 'I',usr);
       INSERT_DATA:=:NEW.PK_ORDER || '|' ||	
       to_char(:NEW.ORDER_TYPE) || '|' ||
to_char(:NEW.FK_ORDER_HEADER) || '|' ||
to_char(:NEW.ORDER_STATUS) || '|' ||
to_char(:NEW.RECENT_HLA_ENTERED_FLAG) || '|' ||
to_char(:NEW.ADDITI_TYPING_FLAG) || '|' ||
to_char(:NEW.CBU_AVAIL_CONFIRM_FLAG) || '|' ||
to_char(:NEW.IS_CORD_AVAIL_FOR_NMDP) || '|' ||
to_char(:NEW.CORD_AVAIL_CONFIRM_DATE) || '|' ||
to_char(:NEW.SHIPMENT_SCH_FLAG) || '|' ||
to_char(:NEW.CORD_SHIPPED_FLAG) || '|' ||
to_char(:NEW.PACKAGE_SLIP_FLAG) || '|' ||
to_char(:NEW.NMDP_SAMPLE_SHIPPED_FLAG) || '|' ||
to_char(:NEW.COMPLETE_REQ_INFO_TASK_FLAG) || '|' ||
to_char(:NEW.FINAL_REVIEW_TASK_FLAG) || '|' ||
to_char(:NEW.ORDER_LAST_VIEWED_BY) || '|' ||
to_char(:NEW.ORDER_LAST_VIEWED_DATE) || '|' ||
to_char(:NEW.ORDER_VIEW_CONFIRM_FLAG) || '|' ||
to_char(:NEW.ORDER_VIEW_CONFIRM_DATE) || '|' ||
to_char(:NEW.ORDER_PRIORITY) || '|' ||
to_char(:NEW.FK_CURRENT_HLA) || '|' ||
to_char(:NEW.CREATOR) || '|' ||
to_char(:NEW.CREATED_ON) || '|' ||
to_char(:NEW.LAST_MODIFIED_BY) || '|' ||
to_char(:NEW.LAST_MODIFIED_DATE) || '|' ||
to_char(:NEW.IP_ADD) || '|' ||
to_char(:NEW.DELETEDFLAG) || '|' ||
to_char(:NEW.RID) || '|' ||
to_char(:NEW.TASK_ID) || '|' ||
to_char(:NEW.TASK_NAME) || '|' ||
to_char(:NEW.ASSIGNED_TO) || '|' ||
to_char(:NEW.ORDER_STATUS_DATE) || '|' ||
to_char(:NEW.ORDER_REVIEWED_BY) || '|' ||
to_char(:NEW.ORDER_REVIEWED_DATE) || '|' ||
to_char(:NEW.ORDER_ASSIGNED_DATE) || '|' ||
to_char(:NEW.FK_ORDER_RESOL_BY_CBB) || '|' ||
to_char(:NEW.FK_ORDER_RESOL_BY_TC) || '|' ||
to_char(:NEW.ORDER_RESOL_DATE) || '|' ||
to_char(:NEW.ORDER_ACK_FLAG) || '|' ||
to_char(:NEW.ORDER_ACK_DATE) || '|' ||
to_char(:NEW.ORDER_ACK_BY) || '|' ||
to_char(:NEW.RESULT_REC_DATE) || '|' ||
to_char(:NEW.ACCPT_TO_CANCEL_REQ) || '|' ||
to_char(:NEW.CANCEL_CONFORM_DATE) || '|' ||
to_char(:NEW.CANCELED_BY) || '|' ||
to_char(:NEW.CLINIC_INFO_CHECKLIST_STAT) || '|' ||
to_char(:NEW.RECENT_HLA_TYPING_AVAIL) || '|' ||
to_char(:NEW.ADDI_TEST_RESULT_AVAIL) || '|' ||
to_char(:NEW.ORDER_SAMPLE_AT_LAB) || '|' ||
to_char(:NEW.FK_CASE_MANAGER) || '|' ||
to_char(:NEW.CASE_MANAGER) || '|' ||
to_char(:NEW.TRANS_CENTER_ID) || '|' ||
to_char(:NEW.TRANS_CENTER_NAME) || '|' ||
to_char(:NEW.SEC_TRANS_CENTER_NAME) || '|' ||
to_char(:NEW.ORDER_PHYSICIAN) || '|' ||
to_char(:NEW.SEC_TRANS_CENTER_ID) || '|' ||
to_char(:NEW.CM_MAIL_ID) || '|' ||
to_char(:NEW.CM_CONTACT_NO) || '|' ||
to_char(:NEW.REQ_CLIN_INFO_FLAG) || '|' ||
to_char(:NEW.RESOL_ACK_FLAG) || '|' ||
to_char(:NEW.FK_SAMPLE_TYPE_AVAIL) || '|' ||
to_char(:NEW.FK_ALIQUOTS_TYPE);
    INSERT INTO AUDIT_INSERT(RAID, ROW_DATA) VALUES (RAID, INSERT_DATA);
END;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,160,24,'24_ER_ORDER_BI0.sql',sysdate,'9.0.0 Build#617');

commit;