set define off;
--Sql for Table CB_CORD to add column Bacterial Culture Start Date.'; 
DECLARE 
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_CORD' AND column_name = 'BACT_CULT_DATE'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_CORD add BACT_CULT_DATE DATE');
  end if;
end;
/
COMMENT ON COLUMN CB_CORD.BACT_CULT_DATE IS 'This Column will store the value of Bacterial Culture Start Date.'; 
commit;



--Sql for Table CB_CORD to add column Fungal Culture Start Date.'; 

DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_CORD' AND column_name = 'FUNG_CULT_DATE'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_CORD add FUNG_CULT_DATE DATE');
  end if;
end;
/
COMMENT ON COLUMN CB_CORD.FUNG_CULT_DATE IS 'This Column will store the value of Fungal Culture Start Date.'; 
commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,160,28,'28_CB_CORD_ALTER.sql',sysdate,'9.0.0 Build#617');

commit;