set define off;
--STARTS ALTERING COLUMN
DECLARE
  v_column_exists NUMBER :=0;
BEGIN
  SELECT COUNT(*) INTO v_column_exists FROM USER_TAB_COLS WHERE TABLE_NAME = 'cb_funding_schedule' AND COLUMN_NAME = 'generated_status';
IF (v_column_exists > 0) THEN
    EXECUTE IMMEDIATE 'alter table cb_funding_schedule modify generated_status VARCHAR2(1)';
END IF;
END;
/
--END 
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,160,31,'31_MISC_ALTER.sql',sysdate,'9.0.0 Build#617');

commit;