set define off;

update er_report set rep_hide = 'Y' where pk_report in (161,165);
commit;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,160,30,'30_ER_REPORT_UPDATE.sql',sysdate,'9.0.0 Build#617');

commit;