--STARTS UPDATING RECORDS FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists from ER_CODELST where CODELST_TYPE='alert' AND CODELST_SUBTYP IN('alert_10','alert_12','alert_13','alert_14','alert_21','alert_22');
  if (v_record_exists > 1) then
    UPDATE ER_CODELST SET CODELST_HIDE='Y' where CODELST_TYPE='alert' AND CODELST_SUBTYP IN('alert_10','alert_12','alert_13','alert_14','alert_21','alert_22');
    commit;
  end if;
end;
/
--END--


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,175,6,'06_ER_CODELST_UPDATE.sql',sysdate,'9.0.0 Build#632');

commit;