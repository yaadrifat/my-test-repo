create or replace
function getAlertCondition(alertId number,alerttype varchar2) return varchar2 is
var_alertCondition varchar2(32767):='';
var_keycond varchar2(32767):='';
var_temp varchar2(32767) := '';
var_start_brace varchar2(32767):='';
var_table_name varchar2(32767):='';
var_column_name varchar2(32767):='';
var_condition_value varchar2(32767):='';
var_arithematic_operator varchar2(32767):='';
var_logical_operator varchar2(32767):='';
var_end_brace varchar2(32767):='';
var_key_condition varchar2(32767):='';

begin
for alerts in (select START_BRACE,TABLE_NAME,COLUMN_NAME,CONDITION_VALUE,ARITHMETIC_OPERATOR,LOGICAL_OPERATOR,END_BRACE,KEY_CONDITION from cb_alert_conditions where upper(condition_type)=alerttype and fk_codelst_alert=alertId order by PK_ALERT_CONDITIONS)
loop
var_start_brace :=alerts.START_BRACE;
var_table_name:=alerts.TABLE_NAME;
var_column_name :=alerts.COLUMN_NAME;
var_condition_value :=alerts.CONDITION_VALUE;
var_arithematic_operator :=alerts.ARITHMETIC_OPERATOR;
var_logical_operator :=alerts.LOGICAL_OPERATOR;
var_end_brace :=alerts.END_BRACE;
var_key_condition :=alerts.KEY_CONDITION;

--dbms_output.put_line('var_temp:::::::::'||var_temp||'...............');
--dbms_output.put_line('alerts.key_condition:::::::::'||var_key_condition);
if var_temp = var_key_condition then
  dbms_output.put_line('Alert condition message executed......');
else
  var_keycond := var_keycond||'AND '||var_key_condition;
  --dbms_output.put_line('???????????????????????????????'||var_keycond);
  var_temp:=alerts.key_condition;
  end if;
  var_alertcondition := var_alertcondition||var_start_brace||var_table_name||'.'||var_column_name||var_arithematic_operator||var_condition_value||var_end_brace||' '||var_logical_operator||' ';
end loop;
--dbms_output.put_line('Alert condition is===========>'||var_alertcondition);
if var_alertcondition='' then
var_alertcondition:='select count(*) from er_order where pk_order=0';
end if;
var_alertcondition:=var_alertcondition||var_keycond;
return var_alertcondition;
EXCEPTION
WHEN OTHERS THEN
  --DBMS_OUTPUT.PUT_LINE('ERROR IS:::');
var_alertcondition:=var_alertcondition||var_keycond;
return var_alertcondition;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,175,8,'08_GETALERTCONDITION.sql',sysdate,'9.0.0 Build#632');

commit;