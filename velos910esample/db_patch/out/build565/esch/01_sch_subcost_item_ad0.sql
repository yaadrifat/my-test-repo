create or replace TRIGGER ESCH.SCH_SUBCOST_ITEM_AD0 AFTER DELETE ON SCH_SUBCOST_ITEM
FOR EACH ROW
declare
  raid number(10);
  deleted_data clob;

begin
select seq_audit.nextval into raid from dual;
audit_trail.record_transaction
    (raid, 'ESCH_SUBCOST_ITEM', :old.rid, 'D');
--Created by Manimaran for Audit delete
deleted_data :=
to_char(:old.PK_SUBCOST_ITEM) || '|' ||
to_char(:old.FK_CALENDAR) || '|' ||
:old.SUBCOST_ITEM_NAME || '|' ||
to_char(:old.SUBCOST_ITEM_COST) || '|' ||
to_char(:old.SUBCOST_ITEM_UNIT) || '|' ||
to_char(:old.SUBCOST_ITEM_SEQ) || '|' ||
to_char(:old.FK_CODELST_CATEGORY) || '|' ||
to_char(:old.FK_CODELST_COST_TYPE) || '|' ||
:old.SUBCOST_ITEM_DELFLAG || '|' ||
to_char(:old.RID) || '|' ||
to_char(:old.CREATOR) || '|' ||
to_char(:old.CREATED_ON) || '|' ||
to_char(:old.LAST_MODIFIED_BY) || '|' ||
to_char(:old.LAST_MODIFIED_DATE) || '|' ||
:old.IP_ADD;

insert into AUDIT_DELETE
(raid, row_data) values (raid, deleted_data);
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,108,1,'01_sch_subcost_item_ad0.sql',sysdate,'8.10.0 Build#565');

commit;