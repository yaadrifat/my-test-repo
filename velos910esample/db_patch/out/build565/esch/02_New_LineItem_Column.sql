set define off;

DECLARE
 col_count NUMBER default 0;
BEGIN
 select count(*) into col_count
 from all_tab_columns where owner = 'ESCH' and
 table_name = 'SCH_LINEITEM' and column_name = 'SUBCOST_ITEM_FLAG';
 if (col_count = 0) then
   execute immediate 'alter table esch.SCH_LINEITEM add SUBCOST_ITEM_FLAG NUMBER DEFAULT 0';
 end if;
END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,108,2,'02_New_LineItem_Column.sql',sysdate,'8.10.0 Build#565');

commit;