set define off;

CREATE OR REPLACE PACKAGE ERES."PKG_BROADCAST" 
IS

PROCEDURE SP_BROADCAST_STUDY(p_study in number, o_ret OUT NUMBER);

pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'PKG_BROADCAST', pLEVEL  => Plog.LFATAL);


END PKG_BROADCAST;
/


CREATE OR REPLACE PACKAGE BODY ERES."PKG_BROADCAST"
IS

PROCEDURE SP_BROADCAST_STUDY(p_study in number, o_ret OUT NUMBER)
IS

v_tarea_desc varchar2(500);
v_studynumber varchar2(500);
v_study_title varchar2(1000);

v_tarea  number; 

v_msgtemplate VARCHAR2(4000);
v_mail VARCHAR2(4000);
v_fparam VARCHAR2(1000);


BEGIN

begin
  -- get study info 
    
  select study_number, fk_codelst_tarea, study_title
  into v_studynumber ,v_tarea  ,v_study_title 
  from er_study
  where pk_study = p_study; 

  -- get therapeutic area description

  select codelst_desc
  into v_tarea_desc
  from er_codelst
  where pk_codelst = v_tarea  ;



       v_msgtemplate   :=    Pkg_Common.SCH_GETMAILMSG('broadcast');

     v_fparam :=  v_tarea_desc ||'~'|| v_studynumber ||'~'|| v_study_title ;


     v_mail :=    Pkg_Common.SCH_GETMAIL(v_msgtemplate,v_fparam);


  -- get all subscribers for study's therapeutic area and prepare messages

    INSERT INTO SCH_DISPATCHMSG (PK_MSG ,MSG_SENDON ,MSG_STATUS  ,
                        MSG_TYPE    ,
                        MSG_TEXT,FK_PAT)
            select  SCH_DISPATCHMSG_SEQ1.NEXTVAL ,SYSDATE ,0,
                        'U',
                        v_mail  ,
                        fk_user
           from er_studynotify
           where fk_codelst_tarea =v_tarea   and  studynotify_type = 'tarea';

 


   COMMIT;
   o_ret:=0;

 exception when others then

       o_ret:=-1;  
       plog.fatal(pctx,'SP_BROADCAST_STUDY.SP_BROADCAST_STUDY:'||sqlerrm);


  end;

END SP_BROADCAST_STUDY;


END PKG_BROADCAST;
/


CREATE OR REPLACE PROCEDURE        "SP_GET_LKP_RECORDS" (p_page IN NUMBER, p_rec_per_page IN NUMBER,p_viewId IN NUMBER,p_search IN VARCHAR2, p_cursor OUT Gk_Cv_Types.GenericCursorType,p_retrows OUT NUMBER)
    IS
        l_query LONG := 'select ';
 		v_colname VARCHAR2(4000);
		v_coldisplayName VARCHAR2(100);
		v_collist LONG;
		v_countSQL VARCHAR2(5000);
		v_retrows NUMBER;
		v_lkplib NUMBER ;
		v_from VARCHAR2(500);
		v_search VARCHAR2(4000);
		v_where VARCHAR2(4000);
		v_dispcolcount NUMBER;
		v_orderstr VARCHAR2(100);
		v_dispflag CHAR(1);
		v_colorder_setting NUMBER;
		v_where_order NUMBER;

		/* YK 12Jan- BUG 5713*/
		v_increment NUMBER;	-- Used for loop increment value
		v_increment_value NUMBER; -- Used to store v_orderBy_pos value after casting for comparison
		v_column_name VARCHAR2(500); -- Used to store column name
		v_where_Col VARCHAR2(4000); -- Used to construct where condition on column name
		v_orderBy_pos VARCHAR2(10); -- Used to get the order by position value

		pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'Sp_Get_Lkp_Records', pLEVEL  => Plog.LFATAL);
/*
Parameters:
		   p_page - Page Number for which records are required
		   p_rec_per_page - Number of records per page
		   p_cursor - output type, will hold rows returned
		   p_retrows - output type, returns total number of rows for the query, used by pagination object
*/
 BEGIN
 -- p_cursor in out cursorType
 -- get column names
 -- get from list
	v_search  := p_search;
	v_dispcolcount := 0;
	/* YK 12Jan- BUG 5713*/
	v_increment := 0;
	v_increment_value :=0; 
	v_colorder_setting := 0;
	v_colorder_setting := NVL(INSTR(LOWER(p_search),'order by'),0);
  /* YK 12Jan- BUG 5713*/
  IF v_colorder_setting > 0 THEN
  v_orderBy_pos := SUBSTR(p_search, NVL(INSTR(LOWER(p_search),'order by'),0)+9,2);
  v_increment_value := to_number(v_orderBy_pos);
  END IF;
  
 FOR j IN (SELECT DISTINCT trim(lkpcol_table) AS lkpcol_table FROM ER_LKPCOL a,ER_LKPVIEWCOL b WHERE 	b.fk_lkpcol=a.pk_lkpcol AND b.fk_lkpview=p_viewId)
 LOOP
 v_from := v_from ||','|| j.lkpcol_table;
 END LOOP;
 -- remove  extra ','
 v_from := SUBSTR(v_from, 2, LENGTH(v_from));
 FOR i IN (SELECT a.lkpcol_name,a.lkpcol_dispval,NVL(b.lkpview_seq,0) lkpview_seq ,a.FK_LKPLIB,a.lkpcol_table, lkpview_is_display FROM ER_LKPCOL a,ER_LKPVIEWCOL b WHERE b.fk_lkpcol=a.pk_lkpcol AND b.fk_lkpview=p_viewId   ORDER BY lkpview_seq)
 LOOP
  -- this will get all column rows
 
  v_colname := i.LKPCOL_NAME;
  /* YK 12Jan- BUG 5713*/
  v_increment := v_increment+1;
  IF v_colorder_setting > 0 THEN
    IF v_increment_value=v_increment THEN
    	/*BUG 5758 union needs column numbers in order by clause*/
    	if (INSTR(LOWER(p_search),'union') > 0) then
          v_column_name:= i.lkpview_seq;
      	else
     	  v_column_name:=v_colname;
     	end if;
    END IF;
   END IF;
  v_coldisplayName := i.LKPCOL_DISPVAL;
  v_lkplib:=i.FK_LKPLIB ;
  
  v_coldisplayName := SUBSTR(v_coldisplayName,1,30);
  
  -- generate column list
  v_collist := v_collist || ',' || v_colname  || ' "' || v_coldisplayName ||'"';
  --check if its a visible column
  v_dispflag := i.lkpview_is_display;
  IF v_dispflag = 'Y' AND v_dispcolcount < 4 THEN
    v_orderstr := v_orderstr || ','  || i.lkpview_seq;
	v_dispcolcount := v_dispcolcount + 1;
  END IF;
 END LOOP;
 v_increment:=0; -- reset Loop value
   -- remove first ',' from v_orderstr and add order by
   /* YK 12Jan- BUG 5713*/
   IF v_colorder_setting > 0 THEN
   	  /*BUG 5758 union needs column numbers in order by clause*/
   	  if (INSTR(LOWER(p_search),'union') > 0) then
        v_orderstr := ' order by ' || v_column_name ||'';
      else
      	v_orderstr := ' order by lower(' || v_column_name ||')';
      END IF;
    ELSE
      v_orderstr := ' order by ' || SUBSTR(v_orderstr,2);
   END IF;
     v_collist := SUBSTR(v_collist,2);
  l_query := l_query || v_collist;
   IF INSTR(LOWER(v_from),'er_lkpdata')>0 THEN
	v_where := ' Where  fk_lkplib = '|| v_lkplib ;
 END IF	;
 IF LENGTH(p_search) >0 THEN
  IF LENGTH(v_where)>0 THEN
  v_where := v_where || p_search ;
  ELSE
  IF INSTR(p_search,'and')>0 THEN
  	 /*BUG 5758 orginal code retained*/
  	v_where:=	' Where ' || SUBSTR(p_search,INSTR(p_search,'and')+ 3);
  ELSE
    v_where:=p_search;
  END IF ;
  END IF;
 END IF;
 l_query := l_query || ' from  ' ||v_from || v_where ;
 IF v_colorder_setting <= 0 THEN
 	 l_query := l_query || v_orderstr;
 END IF;
 --remove order by
  v_where_order := NVL(INSTR(LOWER(v_where),'order by'),0);
  IF v_where_order > 0 THEN
    v_where := SUBSTR(v_where,1, v_where_order - 1);
  END IF;

--   INSERT INTO T(C) VALUES (l_query);
--COMMIT;
--  v_countSQL := 'Select count(*) from ' ||v_from || v_where ;
  v_countSQL := 'Select count(*) from (' ||l_query||')' ;

 -- we have final query for sp_get_page_records()
 -- Prepare count sql for sp_get_page_records()
--	open p_cursor for l_query;

  plog.DEBUG(pctx,'LOOKUP SQL' || l_query);

	Sp_Get_Page_Records (p_page,p_rec_per_page, l_query,v_countSQL,p_cursor,p_retrows);
END; 
/

SET DEFINE OFF; 

CREATE OR REPLACE FORCE VIEW "ERES"."ERV_PATSCH" ("PERSON_CODE", "PAT_STUDYID", "FK_SITE", "SITE_NAME", "PERSON_NAME", "FK_ACCOUNT", "STUDY_NUMBER", "STUDY_TITLE", "STUDY_ACTUALDT", "STUDY_VER_NO", "PATPROT_ENROLDT", "PROTOCOL_NAME", "PATPROT_START", "EVENT_ID", "EVENT_NAME", "MONTH", "START_DATE_TIME", "SORT_DATE", "EVENT_STATUS", "FK_ASSOC", "PROTOCOLID", "FK_STUDY", "FK_PER", "EVENT_EXEON", "EVENT_SCHDATE", "FK_PATPROT", "VISIT", "SCHEVE_STATUS", "FUZZY_PERIOD", "VISIT_NAME", "FK_CODELST_STAT", "TX_ARM", "FK_USERASSTO", "EVENT_STATUS_SUBTYPE", "EVENT_STATUS_DESC", "NOTES", "PATPROT_STAT", "EVENT_SEQUENCE")
AS
  SELECT pr.person_code,
    pp.patprot_patstdid pat_studyid,
    pr.fk_site,
    pr.site_name,
    pr.person_lname
    || ', '
    || pr.person_fname person_name,
    st.fk_account,
    st.study_number,
    st.study_title,
    st.study_actualdt,
    st.study_ver_no,
    pp.patprot_enroldt,
    (SELECT NAME FROM erv_eveassoc WHERE event_id = sc.session_id
    ) protocol_name,
    pp.patprot_start,
    sc.event_id,
    sc.description event_name,
    trim(TO_CHAR (sc.event_schdate, 'Month YYYY')) MONTH,
    sc.start_date_time,
    TO_CHAR(sc.event_schdate,'YYYYMM') AS sort_date,
    sc.isconfirmed event_status,
    sc.fk_assoc,
    sc.session_id protocolid,
    pp.fk_study,
    pp.fk_per,
    sc.event_exeon,
    sc.event_schdate,
    pp.pk_patprot,
    sc.visit,
    sc.status,
    sc.visit_type_id,
    visit_name,
    fk_codelst_stat,
    (SELECT fk_studytxarm
    FROM ER_PATTXARM
    WHERE pk_pattxarm =
      (SELECT MAX (pk_pattxarm)
      FROM ER_PATTXARM b
      WHERE b.fk_patprot = pp.pk_patprot
      AND tx_start_date  =
        (SELECT MAX (tx_start_date)
        FROM ER_PATTXARM a
        WHERE a.fk_patprot = pp.pk_patprot
        )
      )
    ) AS tx_arm,
    pp.fk_userassto,
    sc.event_status_subtype event_status_subtype,
    sc.codelst_desc status_desc,
    notes,
    patprot_stat ,
    sc.event_sequence
  FROM erv_scheve sc,
    ER_PATPROT pp,
    ER_STUDY st,
    erv_person pr,
    ER_PATSTUDYSTAT ps
  WHERE sc.fk_patprot       = pp.pk_patprot
  AND sc.patient_id         = pr.pk_person
  AND pp.fk_study           = st.pk_study
  AND pp.fk_study           = ps.fk_study
  AND sc.patient_id         = ps.fk_per
  AND ps.patstudystat_endt IS NULL;  


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,108,3,'03_PLSQL.sql',sysdate,'8.10.0 Build#565');

commit;