LOAD DATA
INFILE *
INTO TABLE SCH_MSGTXT 
APPEND
FIELDS TERMINATED BY ','
(pk_msgtxt, msgtxt_type, fk_account, msgtxt_subject,
 msg_file filler char,
"MSGTXT_LONG" LOBFILE (msg_file) TERMINATED BY EOF NULLIF msg_file = 'NONE'
)
BEGINDATA 
41,broadcast,0,New Trial Added,msg_broadcast.txt