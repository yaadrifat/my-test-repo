Dear Velos eResearch Member, 

You are receiving this message from the Velos eResearch New Trial Notification system because you have expressed an interest in being notified about new trials being added in the areas of your interest. 

Basic information about the new trial added is given below: 

Therapeutic Area: ~1~ 
Study Number: ~2~ 
Study Title: ~3~ 

To access more details, please login into your account. If you have any questions, please feel free to contact your system administrator.

Velos eResearch 

------------------------------------------------------------------------------------------------------------------------------
This email transmission and any documents, files or previous email messages attached to it may contain information that is confidential or legally privileged. If you are not the intended recipient or a person responsible for delivering this transmission to the intended recipient you are hereby notified that you must not read this transmission and that any disclosure, copying, printing, distribution or use of this transmission is strictly prohibited. If you have received this transmission in error, please immediately notify the sender by telephone or return email and delete the original transmission and its attachments without reading or saving in any manner.
------------------------------------------------------------------------------------------------------------------------------