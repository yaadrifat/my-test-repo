<?xml version="1.0"?> 
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" version="4.0" encoding="iso-8859-1" indent="yes"/>

<xsl:key name="RecordsByMonth" match="ROW" use="SCH_DATE_GRP" />
<xsl:key name="RecordsByMonthVisit" match="ROW" use="concat(SCH_DATE_GRP, ' ', VISIT_NAME)" />
<xsl:key name="RecordsByMonthVisitEvent" match="ROW" use="concat(SCH_DATE_GRP, ' ', VISIT_NAME, ' ', EVENT_NAME)" />

<xsl:param name="hdrFileName" />
<xsl:param name="ftrFileName" />
<xsl:param name="repTitle"/>
<xsl:param name="repId"/>
<xsl:param name="repName"/>
<xsl:param name="repBy"/>
<xsl:param name="repDate"/>
<xsl:param name="dtFrom"/>
<xsl:param name="dtTo"/>
<xsl:param name="argsStr"/>
<xsl:param name="wd"/>
<xsl:param name="xd"/>
<xsl:param name="hd"/>
<xsl:param name="cond"/>
<xsl:param name="hdrflag"/>
<xsl:param name="ftrflag"/>
<xsl:param name="showSelect"/>

<xsl:template match="/">
<HTML>
<HEAD><TITLE> <xsl:value-of select="$repName" /> </TITLE>

<SCRIPT language="JavaScript"><![CDATA[
	function fselect(day,weekNum)
	{

		if (document.layers) 

		{

		window.opener.document.div1.document.enroll.selDay.value = day;

		window.opener.document.div1.document.enroll.dispSelDay.value = weekNum;

		}

		else

		{

		window.opener.document.enroll.selDay.value = day;

		window.opener.document.enroll.dispSelDay.value = weekNum;

		}

		self.close();

	} 

]]></SCRIPT>


<link rel="stylesheet" href="./styles/common.css" type="text/css"/>

	</HEAD>
<BODY class="repBody">
<xsl:if test="$cond='T'">
<table width="100%" >
<tr class="reportGreyRow">
<td class="reportPanel"> 
Download the report in: 
<A href='{$wd}' >
Word Format
</A> 
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<A href='{$xd}' >
Excel Format
</A>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<A href='{$hd}' >
Printer Friendly Format
</A> 
</td>
</tr>
</table>
</xsl:if>
<xsl:apply-templates select="ROWSET"/>
</BODY>
</HTML>
</xsl:template> 

<xsl:template match="ROWSET">
<TABLE WIDTH="100%" >
<xsl:if test="$hdrflag='1'">
<TR>
<TD WIDTH="100%" ALIGN="CENTER">
<img src="{$hdrFileName}"/>
</TD>
</TR>
</xsl:if>
<TR>
<TD class="reportName" WIDTH="100%" ALIGN="CENTER">
<xsl:value-of select="$repName" />
</TD>
</TR>
</TABLE>
<TABLE WIDTH="100%" >
	<TR>
		<TD class="reportGrouping" ALIGN="Left" width="22%">
		Protocol Calendar Name: </TD><TD class="reportData"><b><xsl:value-of select="//PROT_NAME" /></b>
		</TD>
	</TR>
	<TR>
		<TD class="reportGrouping" ALIGN="Left" width="22%">
		Description: </TD><TD class="reportData"><b><xsl:value-of select="//PROT_DESC" /></b>
		</TD>
	</TR>
	<TR>
		<TD class="reportGrouping" ALIGN="Left" width="22%">
		Duration: </TD><TD class="reportData"><b><xsl:value-of select="//PROT_DURATION" /></b>
		</TD>
	</TR>
	<TR>
		<TD class="reportGrouping" ALIGN="Left" width="22%">
		Calendar Start Date: </TD><TD class="reportData"><b><xsl:value-of select="//CAL_START_DATE" /></b>
		</TD>
	</TR>
</TABLE>
<hr class="thickLine" />
<TABLE WIDTH="100%" BORDER="1">
<TR>
	<th class="reportHeading" WIDTH="10%" ALIGN="CENTER">Event Date</th>
	<th class="reportHeading" WIDTH="15%" ALIGN="CENTER">Event</th>
	<th class="reportHeading" WIDTH="25%" ALIGN="CENTER">Description</th>
	<th class="reportHeading" WIDTH="15%" ALIGN="CENTER">Event Window</th>
	<th class="reportHeading" WIDTH="15%" ALIGN="CENTER">Associated Forms</th>
	<th class="reportHeading" WIDTH="20%" ALIGN="CENTER">VELLABEL[Evt_CoverageType]</th>
</TR>

<xsl:for-each select="ROW[count(. | key('RecordsByMonth', SCH_DATE_GRP)[1])=1]">
<xsl:sort select="SCH_DATE_GRP" />
<TR>
	<TD class="reportGrouping" ALIGN="Left" colspan="6">
	<xsl:value-of select="SCH_DATE_GRP_DISP" />
	</TD>
</TR>
<xsl:variable name="str" select="key('RecordsByMonth', SCH_DATE_GRP)" />
<xsl:for-each select="$str[count(. | key('RecordsByMonthVisit',concat(SCH_DATE_GRP, ' ', VISIT_NAME))[1])=1]">

<xsl:variable name="disp" select="DISPLACEMENT" />
<xsl:variable name="visitname" select="VISIT_NAME" />

<TR>
<td>
 	<xsl:if test="$showSelect='1'">
 			<A href="#" onClick="javascript:fselect('{$disp}','{$visitname}')">Select</A>
 	</xsl:if>
 &#xa0;
 </td>
	<TD class="reportGrouping" ALIGN="Left" colspan="2">
	Visit: <b><xsl:value-of select="VISIT_NAME" /></b>
	</TD>
	<TD class="reportGrouping" ALIGN="Left" colspan="2">
	Description: <b><xsl:value-of select="VISIT_DESC" /></b>
	</TD>
	<TD class="reportGrouping" ALIGN="Left" colspan="1">
	<!--KM-#4867 -->
	Visit Window: <b><xsl:value-of select="VISIT_WIN_BEFORE" /></b><b><xsl:value-of select="VISIT_WIN_AFTER" /></b>
	</TD>
	

</TR>

<xsl:variable name="str1" select="key('RecordsByMonthVisit', concat(SCH_DATE_GRP, ' ', VISIT_NAME))" />
<xsl:for-each select="$str1[count(. | key('RecordsByMonthVisitEvent',concat(SCH_DATE_GRP, ' ', VISIT_NAME,' ',EVENT_NAME))[1])=1]">

<xsl:variable name="class">
<xsl:choose>
<xsl:when test="number(position() mod 2)=0" >reportEvenRow</xsl:when> 
<xsl:otherwise>reportOddRow</xsl:otherwise>
</xsl:choose> 
</xsl:variable>
<TR> <xsl:attribute name="class"><xsl:value-of select="$class"/></xsl:attribute>
	<td WIDTH="10%" ALIGN="LEFT"><xsl:value-of select="SCH_DATE"/>&#xa0;</td>
	<td WIDTH="15%" ALIGN="LEFT"><xsl:value-of select="EVENT_NAME"/>&#xa0;</td>
	<td WIDTH="25%" ALIGN="LEFT"><xsl:value-of select="EVENT_DESC"/>&#xa0;</td>
	<td WIDTH="15%" ALIGN="LEFT"><xsl:value-of select="FUZZY_DATES"/>&#xa0;</td>
	<td WIDTH="15%" ALIGN="LEFT"><xsl:value-of select="FORMS"/>&#xa0;</td>
	<td WIDTH="20%" ALIGN="LEFT"><xsl:value-of select="COVERAGE_TYPE"/>&#xa0;</td>
	  
</TR>
</xsl:for-each>
</xsl:for-each>
</xsl:for-each>
</TABLE>
<BR/>

<hr class="thickLine" />

<xsl:if test="$showSelect='0'">
	<TABLE WIDTH="100%" >
	<TR>
	<TD class="reportFooter" WIDTH="50%" ALIGN="LEFT">
	Report By:<xsl:value-of select="$repBy" />
	</TD>
	<TD class="reportFooter" WIDTH="50%" ALIGN="RIGHT">
	Date:<xsl:value-of select="$repDate" />
	</TD>
	</TR>
	</TABLE>
</xsl:if>

<xsl:if test="$ftrflag='1'">
<TABLE>
<TR>
<TD WIDTH="100%" ALIGN="CENTER">
<img src="{$ftrFileName}"/>
</TD>
</TR>
</TABLE>
</xsl:if>


</xsl:template> 
</xsl:stylesheet>