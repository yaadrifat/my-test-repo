LOAD DATA
INFILE *
INTO TABLE er_repxsl
APPEND
FIELDS TERMINATED BY ','
(PK_REPXSL,
 FK_REPORT,
 FK_ACCOUNT,
 REPXSL_NAME,
 xsl_file filler char,
"REPXSL" LOBFILE (xsl_file) TERMINATED BY EOF NULLIF XSL_FILE = 'NONE',
 xsl_file2 filler char,
"REPXSL_XSL" LOBFILE (xsl_file2) TERMINATED BY EOF NULLIF XSL_FILE2 = 'NONE'
)
BEGINDATA
110,110,0,Patient Budget by Visit,110.xsl,110.xsl
159,159,0,Combined Patient Budget,159.xsl,159.xsl
111,111,0,Study Budget by Type,111.xsl,111.xsl
112,112,0,Study Budget by Category,112.xsl,112.xsl
106,106,0,Protocol Calendar Preview,106.xsl,106.xsl
107,107,0,Mock Schedule,107.xsl,107.xsl