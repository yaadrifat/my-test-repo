Set Define Off;

alter table CB_CORD add(
NUCL_CELL_CNT_START_PROCESS NUMBER(10,2),
NUCL_CELL_CNCTRN_START_PROCESS NUMBER(10,2),
NUCL_CELL_CNT_FRZ NUMBER(10,2),
NUCL_CELL_CNT_CNCTRN_FROZEN NUMBER(10,2),
CD3_CELL_CNT_FRZ NUMBER(10,2),
NUCL_CELL_CNT_FRZ_UNCORRECT NUMBER(10,2),
MONO_FRZ_NUCL_CELL_COUNT NUMBER(10,2),
POST_PROCESS_RBC_CNT NUMBER(10,2),
PER_FRZ_CD34_CELL_CNT NUMBER(10,2),
PER_FRZ_CD3_CELL_CNT NUMBER(10,2),
PER_FRZ_CELL_CNT NUMBER(10,2),
PER_CBU_POST_PROCESS_RBC NUMBER(10,2),
ADD_PRE_PROCESS_VOL NUMBER(10,2),
CBU_VOL_START_PROCESS NUMBER(10,2),
PRE_PROCESS_VOLUME NUMBER(10,2),
CBU_VOL_FRZ NUMBER(10,2));


COMMENT ON COLUMN "CB_CORD"."NUCL_CELL_CNT_START_PROCESS" IS 'This column will store Nucleated Cell Count Start Processing';
COMMENT ON COLUMN "CB_CORD"."NUCL_CELL_CNCTRN_START_PROCESS" IS 'This column will store Nucleated Cell Cnctrn Start Processing';	
COMMENT ON COLUMN "CB_CORD"."NUCL_CELL_CNT_FRZ" IS 'This column will store frozen Nucleated Cell Count';	
COMMENT ON COLUMN "CB_CORD"."NUCL_CELL_CNT_CNCTRN_FROZEN" IS 'This column will store frozen Nucleated cncnrt Cell Count';	
COMMENT ON COLUMN "CB_CORD"."CD3_CELL_CNT_FRZ" IS 'This column will store CD3 frozen cell count';	
COMMENT ON COLUMN "CB_CORD"."NUCL_CELL_CNT_FRZ_UNCORRECT" IS 'This column will store Nucleated frozen uncorrected Cell Count Start Processing';	
COMMENT ON COLUMN "CB_CORD"."MONO_FRZ_NUCL_CELL_COUNT" IS 'This column will store mono frozen Nucleated Cell Count Start Processing';	
COMMENT ON COLUMN "CB_CORD"."POST_PROCESS_RBC_CNT" IS 'This column will store Post processing RBC count';	
COMMENT ON COLUMN "CB_CORD"."PER_FRZ_CD34_CELL_CNT" IS 'This column will store percentage of mono nuclear frozen CD34 cell count';	
COMMENT ON COLUMN "CB_CORD"."PER_FRZ_CD3_CELL_CNT" IS 'This column will store percentage of mono nuclear CD3 frozen cell count';	
COMMENT ON COLUMN "CB_CORD"."PER_FRZ_CELL_CNT" IS 'This column will store Nucleated Cell Count Start Processing';	
COMMENT ON COLUMN "CB_CORD"."PER_CBU_POST_PROCESS_RBC" IS 'This column will store percentage of CBU post processing RBC';	
COMMENT ON COLUMN "CB_CORD"."ADD_PRE_PROCESS_VOL" IS 'This column will store Additive Pre Processing volume';	
COMMENT ON COLUMN "CB_CORD"."CBU_VOL_START_PROCESS" IS 'This column will CBU volume start processing';	
COMMENT ON COLUMN "CB_CORD"."PRE_PROCESS_VOLUME" IS 'This column will store pre processing volume';	
COMMENT ON COLUMN "CB_CORD"."CBU_VOL_FRZ" IS 'This column will store CBU Frozen Volume';		


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,139,1,'01_CB_cord_alter.sql',sysdate,'9.0.0 Build#596');

commit;