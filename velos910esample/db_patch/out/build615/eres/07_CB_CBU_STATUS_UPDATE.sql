set define off;

update cb_cbu_status set deletedflag=1 where code_type='Response' and cbu_status_code='RSO';

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,158,7,'07_CB_CBU_STATUS_UPDATE.sql',sysdate,'9.0.0 Build#615');

commit;
