set define off;

--Renaming the hidden Sub Menu 'Personalize' subtype
DELETE FROM "ERES"."ER_OBJECT_SETTINGS" WHERE OBJECT_SUBTYPE = 'personal_menu'
AND OBJECT_NAME='manage_accnt' AND OBJECT_VISIBLE = '0' ;

Commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,158,26,'26_menuChanges.sql',sysdate,'9.0.0 Build#615');

commit;