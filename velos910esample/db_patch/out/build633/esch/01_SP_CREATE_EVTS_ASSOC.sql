set define off;

CREATE OR REPLACE PROCEDURE ESCH.SP_CREATE_EVTS_ASSOC (
  P_EIDS         IN       ARRAY_STRING,
  P_VIDS         IN       ARRAY_STRING,
  P_SEQS         IN       ARRAY_STRING,
  P_DISPS        IN       ARRAY_STRING,
  P_HIDEFLGS     IN       ARRAY_STRING,
  P_STATUS       OUT      NUMBER,
  p_user         IN       NUMBER,
  p_ip           IN       VARCHAR2,
  p_copy_propagateflag IN NUMBER default 1
)
AS
  v_total NUMBER;
  v_idx NUMBER;
  v_event_id EVENT_ASSOC.EVENT_ID%TYPE;
  v_chain_id EVENT_ASSOC.CHAIN_ID%TYPE;
  v_event_type EVENT_ASSOC.EVENT_TYPE%TYPE;
  v_name EVENT_ASSOC.NAME%TYPE;
  v_notes EVENT_ASSOC.NOTES%TYPE;
  v_cost EVENT_ASSOC.COST%TYPE;
  v_cost_description EVENT_ASSOC.COST_DESCRIPTION%TYPE;
  v_duration EVENT_ASSOC.DURATION%TYPE;
  v_user_id EVENT_ASSOC.USER_ID%TYPE;
  v_linked_uri EVENT_ASSOC.LINKED_URI%TYPE;
  v_fuzzy_period EVENT_ASSOC.FUZZY_PERIOD%TYPE;
  v_msg_to EVENT_ASSOC.MSG_TO%TYPE;
  v_status EVENT_ASSOC.STATUS%TYPE;
  v_description EVENT_ASSOC.DESCRIPTION%TYPE;
  v_org_id EVENT_ASSOC.ORG_ID%TYPE;
  v_event_msg EVENT_ASSOC.EVENT_MSG%TYPE;
  v_event_res EVENT_ASSOC.EVENT_RES%TYPE;
  v_event_flag EVENT_ASSOC.EVENT_FLAG%TYPE;
  v_pat_daysbefore EVENT_ASSOC.PAT_DAYSBEFORE%TYPE;
  v_pat_daysafter EVENT_ASSOC.PAT_DAYSAFTER%TYPE;
  v_usr_daysbefore EVENT_ASSOC.USR_DAYSBEFORE%TYPE;
  v_usr_daysafter EVENT_ASSOC.USR_DAYSAFTER%TYPE;
  v_pat_msgbefore EVENT_ASSOC.PAT_MSGBEFORE%TYPE;
  v_pat_msgafter EVENT_ASSOC.PAT_MSGAFTER%TYPE;
  v_usr_msgbefore EVENT_ASSOC.USR_MSGBEFORE%TYPE;
  v_usr_msgafter EVENT_ASSOC.USR_MSGAFTER%TYPE;
  v_status_dt EVENT_ASSOC.STATUS_DT%TYPE;
  v_status_changeby EVENT_ASSOC.STATUS_CHANGEBY%TYPE;
  v_orig_study EVENT_ASSOC.ORIG_STUDY%TYPE;
  v_orig_cal EVENT_ASSOC.ORIG_CAL%TYPE;
  v_orig_event EVENT_ASSOC.ORIG_EVENT%TYPE;
  v_duration_unit EVENT_ASSOC.DURATION_UNIT%TYPE;
  v_event_cptcode EVENT_ASSOC.EVENT_CPTCODE%TYPE;
  v_event_fuzzyafter EVENT_ASSOC.EVENT_FUZZYAFTER%TYPE;
  v_event_durationafter EVENT_ASSOC.EVENT_DURATIONAFTER%TYPE;
  v_event_durationbefore EVENT_ASSOC.EVENT_DURATIONBEFORE%TYPE;
  v_fk_catlib EVENT_ASSOC.FK_CATLIB%TYPE;
  v_event_calassocto EVENT_ASSOC.EVENT_CALASSOCTO%TYPE;
  v_event_calschdate EVENT_ASSOC.EVENT_CALSCHDATE%TYPE;
  v_event_category EVENT_ASSOC.EVENT_CATEGORY%TYPE;
  v_offline_flag EVENT_ASSOC.OFFLINE_FLAG%TYPE;
  v_event_library_type EVENT_ASSOC.EVENT_LIBRARY_TYPE%TYPE;
  v_event_line_category EVENT_ASSOC.EVENT_LINE_CATEGORY%TYPE;
  v_service_site_id EVENT_ASSOC.SERVICE_SITE_ID%TYPE;
  v_facility_id EVENT_ASSOC.FACILITY_ID%TYPE;
  v_fk_codelst_covertype EVENT_ASSOC.FK_CODELST_COVERTYPE%TYPE;
  v_budget_template EVENT_ASSOC.BUDGET_TEMPLATE%TYPE;
  v_coverage_notes EVENT_ASSOC.COVERAGE_NOTES%TYPE;
  v_fk_codelst_calstat EVENT_ASSOC.FK_CODELST_CALSTAT%TYPE;
  v_temp NUMBER;
BEGIN
  v_total := P_EIDS.COUNT;
  v_idx := 1;
  WHILE v_idx <= v_total LOOP
    v_event_id := 0;
    select EVENT_ID, CHAIN_ID, EVENT_TYPE, NAME,
      NOTES, COST, COST_DESCRIPTION, DURATION, USER_ID,
      LINKED_URI, FUZZY_PERIOD, MSG_TO, STATUS, DESCRIPTION,
      ORG_ID, EVENT_MSG, EVENT_RES, EVENT_FLAG, PAT_DAYSBEFORE,
      PAT_DAYSAFTER, USR_DAYSBEFORE, USR_DAYSAFTER, PAT_MSGBEFORE,
      PAT_MSGAFTER, USR_MSGBEFORE, USR_MSGAFTER, STATUS_DT,
      STATUS_CHANGEBY, ORIG_STUDY, ORIG_CAL, ORIG_EVENT,
      DURATION_UNIT, EVENT_CPTCODE, EVENT_FUZZYAFTER, 
      EVENT_DURATIONAFTER, EVENT_DURATIONBEFORE, FK_CATLIB, 
      EVENT_CALASSOCTO, EVENT_CALSCHDATE, EVENT_CATEGORY, 
      OFFLINE_FLAG, EVENT_LIBRARY_TYPE, 
      EVENT_LINE_CATEGORY, SERVICE_SITE_ID, FACILITY_ID, 
      FK_CODELST_COVERTYPE, BUDGET_TEMPLATE, COVERAGE_NOTES, 
      FK_CODELST_CALSTAT
    into 
      v_event_id, v_chain_id, v_event_type, v_name,
      v_notes, v_cost, v_cost_description, v_duration, 
      v_user_id, v_linked_uri, v_fuzzy_period, v_msg_to, 
      v_status, v_description, 
      v_org_id, v_event_msg, v_event_res, v_event_flag, 
      v_pat_daysbefore, v_pat_daysafter, v_usr_daysbefore, 
      v_usr_daysafter, v_pat_msgbefore, v_pat_msgafter, 
      v_usr_msgbefore, v_usr_msgafter, v_status_dt, 
      v_status_changeby, v_orig_study, v_orig_cal, 
      v_orig_event, v_duration_unit, v_event_cptcode, 
      v_event_fuzzyafter, v_event_durationafter, 
      v_event_durationbefore, v_fk_catlib, 
      v_event_calassocto, v_event_calschdate, 
      v_event_category, v_offline_flag, v_event_library_type, 
      v_event_line_category, v_service_site_id, v_facility_id, 
      v_fk_codelst_covertype, v_budget_template, 
      v_coverage_notes, v_fk_codelst_calstat
    from event_assoc where EVENT_ID = P_EIDS(v_idx);
    
    if (v_event_id > 0) then
      --insert into T(C) values('update row for eid=' || v_event_id||' vid='||P_VIDS(v_idx));
      insert into EVENT_ASSOC (
        EVENT_ID, CHAIN_ID, EVENT_TYPE, NAME, 
        FK_VISIT, EVENT_SEQUENCE, DISPLACEMENT, HIDE_FLAG,
        NOTES, COST, COST_DESCRIPTION, DURATION, USER_ID,
        LINKED_URI, FUZZY_PERIOD, MSG_TO, STATUS, DESCRIPTION,
        ORG_ID, EVENT_MSG, EVENT_RES, EVENT_FLAG, PAT_DAYSBEFORE,
        PAT_DAYSAFTER, USR_DAYSBEFORE, USR_DAYSAFTER, PAT_MSGBEFORE,
        PAT_MSGAFTER, USR_MSGBEFORE, USR_MSGAFTER, STATUS_DT,
        STATUS_CHANGEBY, ORIG_STUDY, ORIG_CAL, ORIG_EVENT,
        DURATION_UNIT, EVENT_CPTCODE, EVENT_FUZZYAFTER, 
        EVENT_DURATIONAFTER, EVENT_DURATIONBEFORE, FK_CATLIB, 
        EVENT_CALASSOCTO, EVENT_CALSCHDATE, EVENT_CATEGORY, 
        OFFLINE_FLAG, EVENT_LIBRARY_TYPE, 
        EVENT_LINE_CATEGORY, SERVICE_SITE_ID, FACILITY_ID, 
        FK_CODELST_COVERTYPE, BUDGET_TEMPLATE, COVERAGE_NOTES, 
        FK_CODELST_CALSTAT
      ) values (
        EVENT_DEFINITION_SEQ.nextval, v_chain_id, v_event_type, v_name,
        TO_NUMBER(P_VIDS(v_idx)), TO_NUMBER(P_SEQS(v_idx)),
        TO_NUMBER(P_DISPS(v_idx)), TO_NUMBER(P_HIDEFLGS(v_idx)),
        v_notes, v_cost, v_cost_description, v_duration, 
        v_user_id, v_linked_uri, v_fuzzy_period, v_msg_to, 
        v_status, v_description, 
        v_org_id, v_event_msg, v_event_res, v_event_flag, 
        v_pat_daysbefore, v_pat_daysafter, v_usr_daysbefore, 
        v_usr_daysafter, v_pat_msgbefore, v_pat_msgafter, 
        v_usr_msgbefore, v_usr_msgafter, v_status_dt, 
        v_status_changeby, v_orig_study, v_orig_cal, 
        v_orig_event, v_duration_unit, v_event_cptcode, 
        v_event_fuzzyafter, v_event_durationafter, 
        v_event_durationbefore, v_fk_catlib, 
        v_event_calassocto, v_event_calschdate, 
        v_event_category, v_offline_flag, v_event_library_type, 
        v_event_line_category, v_service_site_id, v_facility_id, 
        v_fk_codelst_covertype, v_budget_template, 
        v_coverage_notes, v_fk_codelst_calstat
      );
      select EVENT_DEFINITION_SEQ.CURRVAL into v_temp from dual;
      ESCH.SP_CPEVEDTLS(v_event_id, v_temp, 'S', P_STATUS, p_user,
        p_ip, p_copy_propagateflag);
    end if;
    
    v_idx := v_idx+1;
  END LOOP;
  COMMIT;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,176,1,'01_SP_CREATE_EVTS_ASSOC.sql',sysdate,'9.0.0 Build#633');

commit;