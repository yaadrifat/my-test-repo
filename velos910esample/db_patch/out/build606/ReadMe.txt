/* This readMe is specific to Velos eResearch version 9.0 build #606 */
=========================================================================================================================
eResearch 
For Localization 

1. Copy files -> messageBundle.properties and labelBundle.properties 
in ereshome\ and paste it to the folder specified by ERES_HOME 
(where eresearch.xml is also hosted), over-writing the existing files.

=========================================================================================================================
Garuda :
1. After deploying the velos.ear file successfully.
	
	we can find two jar files ( inside the $JBOSS_HOME/server/eresearch/deploy/velos.ear/velos.war/WEB-INF/lib directory ) 
	named "hibernate-validator.jar" and "hibernate-validator-3.1.0.GA.jar", where the application is deployed.
	
	1.Remove "hibernate-validator-3.1.0.GA.jar" and
	2.Keep "hibernate-validator.jar".

========================================================================================================================
