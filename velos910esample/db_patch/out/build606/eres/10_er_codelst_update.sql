set define off;

DECLARE
countFlag NUMBER(5);
BEGIN
	SELECT COUNT(1) INTO countFlag FROM USER_TABLES WHERE TABLE_NAME='ER_CODELST';
	if (countFlag > 0) then
		UPDATE ER_CODELST SET 
		CODELST_CUSTOM_COL = 'IND' WHERE codelst_type='INDIDEGrantor' AND codelst_subtyp='CDRH';
		
		UPDATE ER_CODELST SET 
		CODELST_CUSTOM_COL = 'IDE' WHERE codelst_type='INDIDEGrantor' AND codelst_subtyp IN ('CDER','CBER');
		commit;
	end if;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,149,10,'10_er_codelst_update.sql',sysdate,'9.0.0 Build#606');

commit;
