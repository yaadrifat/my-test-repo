set define off;

Create table CB_MULTIPLE_VALUES
(
PK_MULTIPLE_VALUES NUMBER(10) Primary Key,
FK_CODELST_TYPE VARCHAR2(50 BYTE),
FK_CODELST	NUMBER(10),
ENTITY_ID number(10),
ENTITY_TYPE varchar2(50 BYTE),
CREATOR NUMBER(10),
CREATED_ON DATE,
IP_ADD VARCHAR2(15),
LAST_MODIFIED_BY NUMBER(10),
LAST_MODIFIED_DATE DATE,
RID NUMBER(10),
DELETEDFLAG VARCHAR2(1)
);
COMMENT ON TABLE "CB_MULTIPLE_VALUES" IS 'Table to maintain multiple values in the entity';
COMMENT ON COLUMN "CB_MULTIPLE_VALUES"."PK_MULTIPLE_VALUES" IS 'Primary key of the table';
COMMENT ON COLUMN "CB_MULTIPLE_VALUES"."FK_CODELST_TYPE" IS 'This column store the type which value is to be multiple';
COMMENT ON COLUMN "CB_MULTIPLE_VALUES"."FK_CODELST" IS 'This column store the pk of mutiple value from code list';
COMMENT ON COLUMN "CB_MULTIPLE_VALUES"."ENTITY_ID" IS 'This column store the entity id related to the mutiple values';
COMMENT ON COLUMN "CB_MULTIPLE_VALUES"."ENTITY_TYPE" IS 'This column store the entity type related to the mutiple values';
COMMENT ON COLUMN "CB_MULTIPLE_VALUES"."CREATOR" IS 'This column is used for Audit Trail. The Creator identifies the user who created this row.'; 
COMMENT ON COLUMN "CB_MULTIPLE_VALUES"."CREATED_ON" IS 'This column is used for Audit Trail. Stores the date on which this row was created.'; 
COMMENT ON COLUMN "CB_MULTIPLE_VALUES"."IP_ADD" IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine. '; 
COMMENT ON COLUMN "CB_MULTIPLE_VALUES"."LAST_MODIFIED_BY" IS 'This column stores the name of user who last modified the row in the table.'; 
COMMENT ON COLUMN "CB_MULTIPLE_VALUES"."LAST_MODIFIED_DATE" IS 'This column stores the date on which column last modified.'; 
COMMENT ON COLUMN "CB_MULTIPLE_VALUES"."RID" IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database.'; 
COMMENT ON COLUMN "CB_MULTIPLE_VALUES"."DELETEDFLAG" IS 'This column denotes whether record is deleted.'; 


CREATE SEQUENCE SEQ_CB_MULTIPLE_VALUES MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1;


commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,149,3,'03_CREATE_TABLE.sql',sysdate,'9.0.0 Build#606');

commit;

