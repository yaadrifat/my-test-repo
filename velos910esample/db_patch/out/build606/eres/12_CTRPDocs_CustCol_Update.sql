update er_codelst set CODELST_CUSTOM_COL = 'industrial'
where CODELST_TYPE='ctrpDocType' 
and CODELST_SUBTYP in ('other', 'abbrTrialTempl');

update er_codelst set CODELST_CUSTOM_COL = 'nonIndustrial'
where CODELST_TYPE='ctrpDocType' 
and CODELST_SUBTYP not in ('other', 'abbrTrialTempl');

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,149,12,'12_CTRPDocs_CustCol_Update.sql',sysdate,'9.0.0 Build#606');

commit;
