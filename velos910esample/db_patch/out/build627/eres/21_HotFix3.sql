set define off;
create or replace function f_lab_summary(pk_cordid number) return SYS_REFCURSOR
IS
p_lab_refcur SYS_REFCURSOR;
begin
OPEN p_lab_refcur FOR select distinct a.pk_labtest, labtest_name--,
--(select distinct test_result from er_patlabs c where c.fk_timing_of_test = f_codelst_id('timing_of_test','pre_procesing') and c.fk_testid = a.pk_labtest and c.fk_specimen = b.fk_specimen ) pre_test_result,
--(select distinct test_result from er_patlabs c where c.fk_timing_of_test = f_codelst_id('timing_of_test','post_procesing') and c.fk_testid = a.pk_labtest and c.fk_specimen = b.fk_specimen) post_test_result
from er_labtest a, er_patlabs b where b.fk_specimen = (select fk_specimen_id from cb_cord where pk_cord = pk_cordid) and (a.pk_labtest in
(select fk_testid from er_labtestgrp where fk_labgroup = (select pk_labgroup from er_labgroup where group_name ='PrcsGrp' and group_type = 'PC'))) order by pk_labtest;
RETURN p_lab_refcur;
end;
/
commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,170,21,'21_HotFix2.sql',sysdate,'9.0.0 Build#627');

commit;