CREATE OR REPLACE FORCE VIEW "ERES"."REP_MUL_VALUE_VIEW" ("CORD_REGISTRY_ID", "CORD_LOCAL_CBU_ID", "REGISTRY_MATERNAL_ID", "MATERNAL_LOCAL_ID", "CREATOR", "CREATED_ON", "IP_ADDRESS", "LAST_MODIFIED_BY", "LAST_MODIFIED_DATE", "CODELST_TYPE", "REASON", "PK_ROW_ID", "MODULE_ID")
AS
  SELECT Cord.Cord_Registry_Id,
    Cord.Cord_Local_Cbu_Id,
    Cord.Registry_Maternal_Id,
    Cord.Maternal_Local_Id,
    (SELECT Er_User.Usr_Firstname
      || ' '
      || Er_User.Usr_Lastname
    FROM Er_User
    WHERE Pk_User = val.Creator
    )Creator,
    val.Created_On,
    val.Ip_Add,
    (SELECT ER_USER.USR_FIRSTNAME
      || ' '
      || Er_User.Usr_Lastname
    FROM Er_User
    WHERE Pk_User = val.Last_Modified_By
    ) Last_Modified_By,
    val.Last_Modified_Date,
    val.FK_CODELST_TYPE,
    F_Codelst_Desc(val.FK_CODELST) AS REASON,
    arm.PK_ROW_ID,
    arm.module_id
  FROM CB_MULTIPLE_VALUES val,
    AUDIT_ROW_MODULE arm,
    Cb_Cord Cord
  WHERE val.ENTITY_ID        = cord.PK_CORD
  AND VAL.PK_MULTIPLE_VALUES = arm.module_id
  AND ARM.TABLE_NAME         = 'CB_MULTIPLE_VALUES';
  

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,170,7,'07_REP_MUL_VALUE_VIEW.sql',sysdate,'9.0.0 Build#627');

commit;