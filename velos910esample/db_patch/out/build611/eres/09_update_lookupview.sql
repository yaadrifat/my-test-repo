set define off;
set serveroutput on;

DECLARE
  v_item_exists NUMBER :=0;
  v_lkpview_id NUMBER :=0;
  v_rows_updated NUMBER :=0;
  
BEGIN
  select count(*) into v_item_exists from ER_LKPVIEW where 
    LKPVIEW_KEYWORD = 'study_appendix';

  if (v_item_exists = 0) then
    update ER_LKPVIEW set LKPVIEW_KEYWORD = 'study_appendix' where 
      PK_LKPVIEW = 6500 and LKPVIEW_NAME = 'Study Appendix';
    v_rows_updated := sql%rowcount;
    commit;
    dbms_output.put_line(v_rows_updated||' row(s) updated');
  else
    dbms_output.put_line('No rows updated.');
  end if;
  
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,154,9,'09_update_lookupview.sql',sysdate,'9.0.0 Build#611');

commit;