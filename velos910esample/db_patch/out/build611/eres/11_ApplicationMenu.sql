
--Update Sub-menu 'Application' to appear under 'Manage'
UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_NAME = 'manage_acct' 
WHERE OBJECT_TYPE ='M' AND OBJECT_NAME ='accnt_menu' and OBJECT_SUBTYPE ='application_menu';

commit;

--Update Sub-menu 'Milestones' to appear under 'Manage'
UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_TYPE = 'M', OBJECT_NAME = 'manage_accnt', 
OBJECT_SUBTYPE = 'milestone_menu', OBJECT_DISPLAYTEXT ='Milestones'
WHERE OBJECT_SUBTYPE = 'mile_menu' AND OBJECT_NAME = 'top_menu';

Commit;

--Rename Reports as Data Extraction
UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_DISPLAYTEXT = 'Data Extraction' 
WHERE OBJECT_TYPE ='TM' and OBJECT_SUBTYPE ='report_menu';

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,154,11,'11_ApplicationMenu.sql',sysdate,'9.0.0 Build#611');

commit;