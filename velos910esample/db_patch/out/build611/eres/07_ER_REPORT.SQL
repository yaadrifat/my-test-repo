
-- INSERTING into ER_REPORT

Insert into ER_REPORT (PK_REPORT,REP_NAME,REP_DESC,REP_SQL,FK_ACCOUNT,FK_CODELST_TYPE,IP_ADD,REP_HIDE,REP_COLUMNS,REP_FILTERBY,GENERATE_XML,REP_TYPE,REP_SQL_CLOB,REP_FILTERKEYWORD,REP_FILTERAPPLICABLE) 
values (161,'CBU Summary Report','CBU Summary Report','',0,null,null,'N',null,null,1,'rep_cbu','',null,null);
COMMIT;

Update ER_REPORT set REP_SQL = 'select pk_cord,cord_registry_id, cord_isbi_din_code, cord_isit_product_code,
cord_tnc_frozen, (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_cord_cbu_lic_status) as licence_status,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_cord_cbu_eligible_status) as eligible_status, cord_external_cbu_id, cord_local_cbu_id from cb_cord  order by pk_cord' where pk_report = 161;
COMMIT;

Update ER_REPORT set REP_SQL_CLOB = 'select pk_cord,cord_registry_id, cord_isbi_din_code, cord_isit_product_code,
cord_tnc_frozen, (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_cord_cbu_lic_status) as licence_status,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_cord_cbu_eligible_status) as eligible_status, cord_external_cbu_id, cord_local_cbu_id from cb_cord  order by pk_cord' where pk_report = 161;
COMMIT;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,154,7,'07_ER_REPORT.sql',sysdate,'9.0.0 Build#611');

commit;