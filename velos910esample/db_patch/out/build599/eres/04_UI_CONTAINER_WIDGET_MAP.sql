

Insert into UI_CONTAINER_WIDGET_MAP
(PK_PG_CONTI_WID,FK_CONTAINER_ID,FK_WIDGET_ID) values 
(SEQ_UI_CONTAINER_WIDGET_MAP.nextval,(select PK_CONTAINER from ui_widget_container where container_name='CBU Clinical Record'),(select pk_widget from ui_widget where WIDGET_DIV_ID='labsummarydiv'));


Insert into UI_CONTAINER_WIDGET_MAP
(PK_PG_CONTI_WID,FK_CONTAINER_ID,FK_WIDGET_ID) values 
(SEQ_UI_CONTAINER_WIDGET_MAP.nextval,(select PK_CONTAINER from ui_widget_container where container_name='CBU Clinical Record'),(select pk_widget from ui_widget where WIDGET_DIV_ID='cbuprocessinginfodiv'));

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,142,4,'04_UI_CONTAINER_WIDGET_MAP.sql',sysdate,'9.0.0 Build#599');

commit;