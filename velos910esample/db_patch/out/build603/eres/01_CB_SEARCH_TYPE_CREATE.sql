set define off;

--Sql for Table CB_SEARCH_TYPING to create and alter table CB_SEARCH_TYPING

Create table CB_SEARCH_TYPING 
(
    PK_SEARCH_TYPING      NUMBER primary key
   , ENTITY_ID            NUMBER(19,0)
   , ENTITY_TYPE          NUMBER(19,0)
   , ANTGN_EID1           NUMBER(10)
   , ANTGN_EID2           NUMBER(10)
   , FK_HLA_CODE          NUMBER(19,0)
   , CREATOR NUMBER(10)
   , IP_ADD  VARCHAR2(15)	
   , CREATED_ON           DATE
   , LAST_MODIFIED_DATE   DATE
   , LAST_MODIFIED_BY     DATE
   , HLA_RECEIVED_DATE    DATE
   , HLA_TYPING_DATE      DATE
   ,RID NUMBER(10) 
   );
   
   ----------------------Comment on CB_SEARCH_TYPING Table's column---------------------------------
   COMMENT ON COLUMN CB_SEARCH_TYPING.PK_SEARCH_TYPING IS 'Primary key';
   COMMENT ON COLUMN CB_SEARCH_TYPING.ENTITY_ID IS 'This column will store the Entity ID.';
   COMMENT ON COLUMN CB_SEARCH_TYPING.ENTITY_TYPE IS 'This column will store the Entity Type.';
   COMMENT ON COLUMN CB_SEARCH_TYPING.ANTGN_EID1 IS 'This column will store the Antigen Eid 1.';
   COMMENT ON COLUMN CB_SEARCH_TYPING.ANTGN_EID2 IS 'This column will store the Antigen Eid 2.';
   COMMENT ON COLUMN CB_SEARCH_TYPING.FK_HLA_CODE IS 'This column will store the FK HLA CODE.';
   COMMENT ON COLUMN CB_SEARCH_TYPING.CREATED_ON IS 'This column is used for Audit Trail. The Creator identifies the user who created this row.';
   COMMENT ON COLUMN CB_SEARCH_TYPING.LAST_MODIFIED_DATE IS 'This column is used for Audit Trail. Stores the date on which this row was last modified.';
   COMMENT ON COLUMN CB_SEARCH_TYPING.LAST_MODIFIED_BY IS 'This column is used for Audit Trail. Stores the date on which this row was last modified By.';
   COMMENT ON COLUMN CB_SEARCH_TYPING.HLA_RECEIVED_DATE IS 'HLA Received date.';
   COMMENT ON COLUMN CB_SEARCH_TYPING.HLA_TYPING_DATE IS 'HLA Typing date.';
COMMENT ON COLUMN CB_SEARCH_TYPING.CREATOR IS 'This column is used 
for Audit Trail. The Creator identifies the user who created this row. '; 
COMMENT ON COLUMN CB_SEARCH_TYPING.IP_ADD IS 'This column is used 
for Audit Trail. Stores the IP ADDRESS of the client machine. '; 
COMMENT ON COLUMN CB_SEARCH_TYPING.RID  IS 'This column is used for 
the audit trail. The RID uniquely identifies the row in the database. ';
   
 ----------------------CB_SEARCH_TYPING Table Sequence---------------------------------
   
CREATE SEQUENCE SEQ_CB_SEARCH_TYPING MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE NOORDER NOCYCLE;


commit;
  
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,146,1,'01_CB_SEARCH_TYPE_CREATE.sql',sysdate,'9.0.0 Build#603');

commit;


