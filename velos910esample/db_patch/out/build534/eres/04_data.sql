declare 
	v_code_count number;
begin

	select count(*)
	into v_code_count 
	from er_codelst where codelst_type = 'subm_status' and codelst_subtyp = 'reviewer';

	if (v_code_count = 0) then

			Insert into ER_CODELST
			   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
			    CODELST_HIDE, CODELST_SEQ)
			 Values
			   (seq_er_codelst.nextval, NULL, 'subm_status', 'reviewer', 'Reviewer', 
			    'N', 10);
		
		COMMIT;

	end if;
end;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,77,4,'04_data.sql',sysdate,'8.9.0 Build#534');

commit;

