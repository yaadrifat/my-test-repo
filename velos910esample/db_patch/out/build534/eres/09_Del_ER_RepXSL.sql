DELETE FROM ER_REPXSL WHERE FK_REPORT in (110, 159);

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,77,9,'09_Del_ER_RepXSL.sql',sysdate,'8.9.0 Build#534');

commit;