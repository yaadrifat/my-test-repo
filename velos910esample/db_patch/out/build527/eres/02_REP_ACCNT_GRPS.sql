CREATE OR REPLACE FORCE VIEW "ERES"."REP_ACCNT_GRPS" ("ACGRP_NAME", "ACGRP_DESC", "ACGRP_SUPUSR_FLAG", "ACGRP_SUPBUD_RIGHTS", "ACGRP_SUPBUD_FLAG", "ACGRP_ACCOUNT", "CREATOR", "LAST_MODIFIED_BY", "LAST_MODIFIED_DATE", "CREATED_ON", "RID", "ACGRP_RESPONSE_ID", "FK_ACCOUNT", "FK_CODELST_ST_ROLE") AS 
  SELECT GRP_NAME ACGRP_NAME,GRP_DESC ACGRP_DESC,
       F_GET_YESNO(GRP_SUPUSR_FLAG) ACGRP_SUPUSR_FLAG,
       GRP_SUPBUD_RIGHTS ACGRP_SUPBUD_RIGHTS,
       F_GET_YESNO(GRP_SUPBUD_FLAG) ACGRP_SUPBUD_FLAG,
       (SELECT AC_NAME FROM ER_ACCOUNT
        WHERE PK_ACCOUNT = FK_ACCOUNT) ACGRP_ACCOUNT,
       (SELECT USR_FIRSTNAME || ' ' || USR_LASTNAME FROM ER_USER
        WHERE PK_USER = ER_GRPS.CREATOR) CREATOR,
       (SELECT USR_FIRSTNAME || ' ' || USR_LASTNAME FROM ER_USER
        WHERE PK_USER = ER_GRPS.LAST_MODIFIED_BY) LAST_MODIFIED_BY,
       LAST_MODIFIED_DATE,CREATED_ON,RID,PK_GRP ACGRP_RESPONSE_ID,FK_ACCOUNT, 
      (select codelst_Desc from er_codelst where pk_Codelst=FK_CODELST_ST_ROLE) FK_CODELST_ST_ROLE
FROM ER_GRPS ;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,70,2,'02_REP_ACCNT_GRPS.sql',sysdate,'8.9.0 Build#527');

commit;