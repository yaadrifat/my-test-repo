   
Set Define off;

declare 
v_max number default 0;
n_seqval number default 0;
begin

	select MAX(PK_LABGROUP) into v_max from ER_LABGROUP;
	
	select SEQ_ER_LABGROUP.nextval into n_seqval from dual;

    if (v_max > n_seqval) then
        execute immediate 'DROP SEQUENCE ERES.SEQ_ER_LABGROUP';

        execute immediate 'CREATE SEQUENCE ERES.SEQ_ER_LABGROUP
          START WITH ' || v_max 
          ||' MAXVALUE 999999999999999999999999999
          MINVALUE 1
          NOCYCLE
          NOCACHE
          NOORDER';
    end if;
end;
/

    --Sql for IDM Widget to create the Group in ER_LABGROUP and HGsAg
    
     Insert into ER_LABGROUP (PK_LABGROUP,GROUP_NAME,GROUP_TYPE) values 
     (SEQ_ER_LABGROUP.nextval,'IDMGroup','I');
   
     Insert into ER_LABTEST (PK_LABTEST,LABTEST_NAME,LABTEST_SHORTNAME,LABTEST_CPT,FK_LKP_ADV_NCI,GRADE_CALC) values 
     (SEQ_ER_LABTEST.nextval,'HGsAg','HG',null,null,null);
   
      Insert into ER_LABTESTGRP (PK_LABTESTGRP,FK_TESTID,FK_LABGROUP) values
      (SEQ_ER_LABTESTGRP.nextval,SEQ_ER_LABTEST.currval,SEQ_ER_LABGROUP.currval);
   
 

 
 
  --Sql for the IDM Widget to create the Test Anti-HBc in ER_LABTEST 

	  Insert into ER_LABTEST (PK_LABTEST,LABTEST_NAME,LABTEST_SHORTNAME,LABTEST_CPT,FK_LKP_ADV_NCI,GRADE_CALC) values 
	  (SEQ_ER_LABTEST.nextval,'Anti-HBc','ANHB',null,null,null);
     
      Insert into ER_LABTESTGRP (PK_LABTESTGRP,FK_TESTID,FK_LABGROUP) values
      (SEQ_ER_LABTESTGRP.nextval,SEQ_ER_LABTEST.currval,SEQ_ER_LABGROUP.currval);
   
 

 
    --Sql for the IDM Widget to create the Test Anti-HCV in ER_LABTEST 

	  Insert into ER_LABTEST (PK_LABTEST,LABTEST_NAME,LABTEST_SHORTNAME,LABTEST_CPT,FK_LKP_ADV_NCI,GRADE_CALC) values 
	  (SEQ_ER_LABTEST.nextval,'Anti-HCV','ANHCV',null,null,null);
 
 
		Insert into ER_LABTESTGRP (PK_LABTESTGRP,FK_TESTID,FK_LABGROUP) values
		(SEQ_ER_LABTESTGRP.nextval,SEQ_ER_LABTEST.currval,SEQ_ER_LABGROUP.currval);
  
 
  
 
  --Sql for the IDM Widget to create the Test Anti-HIV 1/2 + O in ER_LABTEST 

  Insert into ER_LABTEST (PK_LABTEST,LABTEST_NAME,LABTEST_SHORTNAME,LABTEST_CPT,FK_LKP_ADV_NCI,GRADE_CALC) values 
   (SEQ_ER_LABTEST.nextval,'Anti-HIV 1/2 + O','ANHIV',null,null,null);
   
   Insert into ER_LABTESTGRP (PK_LABTESTGRP,FK_TESTID,FK_LABGROUP) values
      (SEQ_ER_LABTESTGRP.nextval,SEQ_ER_LABTEST.currval,SEQ_ER_LABGROUP.currval);
     
	 
    
	  
	  
   --Sql for the IDM Widget to create the Test Anti-HIV 1/2 in ER_LABTEST 

  Insert into ER_LABTEST (PK_LABTEST,LABTEST_NAME,LABTEST_SHORTNAME,LABTEST_CPT,FK_LKP_ADV_NCI,GRADE_CALC) values 
  (SEQ_ER_LABTEST.nextval,'Anti-HIV 1/2','ANHIV1',null,null,null);
   
  Insert into ER_LABTESTGRP (PK_LABTESTGRP,FK_TESTID,FK_LABGROUP) values
  (SEQ_ER_LABTESTGRP.nextval,SEQ_ER_LABTEST.currval,SEQ_ER_LABGROUP.currval);
      
  
 
   --Sql for the IDM Widget to create the Test Anti-HTLV I/II in ER_LABTEST 

  Insert into ER_LABTEST (PK_LABTEST,LABTEST_NAME,LABTEST_SHORTNAME,LABTEST_CPT,FK_LKP_ADV_NCI,GRADE_CALC) values 
  (SEQ_ER_LABTEST.nextval,'Anti-HTLV I/II','ANHTLV',null,null,null); 
   
   
   Insert into ER_LABTESTGRP (PK_LABTESTGRP,FK_TESTID,FK_LABGROUP) values
  (SEQ_ER_LABTESTGRP.nextval,SEQ_ER_LABTEST.currval,SEQ_ER_LABGROUP.currval);
      
 
  
     --Sql for the IDM Widget to create the Test Anti-CMV Total in ER_LABTEST 

  Insert into ER_LABTEST (PK_LABTEST,LABTEST_NAME,LABTEST_SHORTNAME,LABTEST_CPT,FK_LKP_ADV_NCI,GRADE_CALC) values 
  (SEQ_ER_LABTEST.nextval,'Anti-CMV Total','ANCMV',null,null,null);
  
  Insert into ER_LABTESTGRP (PK_LABTESTGRP,FK_TESTID,FK_LABGROUP) values
  (SEQ_ER_LABTESTGRP.nextval,SEQ_ER_LABTEST.currval,SEQ_ER_LABGROUP.currval);
      
  
  
  
    --Sql for the IDM Widget to create the Test NAT HIV-1 / HCV / HBV (MPX) in ER_LABTEST 

  Insert into ER_LABTEST (PK_LABTEST,LABTEST_NAME,LABTEST_SHORTNAME,LABTEST_CPT,FK_LKP_ADV_NCI,GRADE_CALC) values 
   (SEQ_ER_LABTEST.nextval,'NAT HIV-1 / HCV / HBV (MPX)','NATH',null,null,null); 
  
   Insert into ER_LABTESTGRP (PK_LABTESTGRP,FK_TESTID,FK_LABGROUP) values
  (SEQ_ER_LABTESTGRP.nextval,SEQ_ER_LABTEST.currval,SEQ_ER_LABGROUP.currval);
      
 
  
  
      --Sql for the IDM Widget to create the Test HIV-1 NAT in ER_LABTEST 

  Insert into ER_LABTEST (PK_LABTEST,LABTEST_NAME,LABTEST_SHORTNAME,LABTEST_CPT,FK_LKP_ADV_NCI,GRADE_CALC) values  
   (SEQ_ER_LABTEST.nextval,'HIV-1 NAT','HIVNAT',null,null,null);
   
     Insert into ER_LABTESTGRP (PK_LABTESTGRP,FK_TESTID,FK_LABGROUP) values
  (SEQ_ER_LABTESTGRP.nextval,SEQ_ER_LABTEST.currval,SEQ_ER_LABGROUP.currval);
      
  
  
  
     --Sql for the IDM Widget to create the Test HIV p24 Ag in ER_LABTEST 

  Insert into ER_LABTEST (PK_LABTEST,LABTEST_NAME,LABTEST_SHORTNAME,LABTEST_CPT,FK_LKP_ADV_NCI,GRADE_CALC) values   
   (SEQ_ER_LABTEST.nextval,'HIV p24 Ag','HIVP',null,null,null); 
   
       Insert into ER_LABTESTGRP (PK_LABTESTGRP,FK_TESTID,FK_LABGROUP) values
  (SEQ_ER_LABTESTGRP.nextval,SEQ_ER_LABTEST.currval,SEQ_ER_LABGROUP.currval);
      

 
  
     --Sql for the IDM Widget to create the Test HCV NAT in ER_LABTEST 

  Insert into ER_LABTEST (PK_LABTEST,LABTEST_NAME,LABTEST_SHORTNAME,LABTEST_CPT,FK_LKP_ADV_NCI,GRADE_CALC) values   
   (SEQ_ER_LABTEST.nextval,'HCV NAT','HCNAT',null,null,null);
  
      Insert into ER_LABTESTGRP (PK_LABTESTGRP,FK_TESTID,FK_LABGROUP) values
  (SEQ_ER_LABTESTGRP.nextval,SEQ_ER_LABTEST.currval,SEQ_ER_LABGROUP.currval);
      
  
   
   --Sql for the IDM Widget to create the Test HBV NAT in ER_LABTEST 

  Insert into ER_LABTEST (PK_LABTEST,LABTEST_NAME,LABTEST_SHORTNAME,LABTEST_CPT,FK_LKP_ADV_NCI,GRADE_CALC) values   
   (SEQ_ER_LABTEST.nextval,'HBV NAT','HBNAT',null,null,null); 
  
  
      Insert into ER_LABTESTGRP (PK_LABTESTGRP,FK_TESTID,FK_LABGROUP) values
  (SEQ_ER_LABTESTGRP.nextval,SEQ_ER_LABTEST.currval,SEQ_ER_LABGROUP.currval);
      
 
  
     --Sql for the IDM Widget to create the Test Syphilis in ER_LABTEST 

  Insert into ER_LABTEST (PK_LABTEST,LABTEST_NAME,LABTEST_SHORTNAME,LABTEST_CPT,FK_LKP_ADV_NCI,GRADE_CALC) values   
   (SEQ_ER_LABTEST.nextval,'Syphilis','SYPH',null,null,null);
   
  
    Insert into ER_LABTESTGRP (PK_LABTESTGRP,FK_TESTID,FK_LABGROUP) values
  (SEQ_ER_LABTESTGRP.nextval,SEQ_ER_LABTEST.currval,SEQ_ER_LABGROUP.currval);
    
  
     --Sql for the IDM Widget to create the Test WNV NAT in ER_LABTEST 

  Insert into ER_LABTEST (PK_LABTEST,LABTEST_NAME,LABTEST_SHORTNAME,LABTEST_CPT,FK_LKP_ADV_NCI,GRADE_CALC) values   
   (SEQ_ER_LABTEST.nextval,'WNV NAT','WNNAT',null,null,null);
   
       Insert into ER_LABTESTGRP (PK_LABTESTGRP,FK_TESTID,FK_LABGROUP) values
  (SEQ_ER_LABTESTGRP.nextval,SEQ_ER_LABTEST.currval,SEQ_ER_LABGROUP.currval);
      


 
    --Sql for the IDM Widget to create the Test Chagas in ER_LABTEST 

  Insert into ER_LABTEST (PK_LABTEST,LABTEST_NAME,LABTEST_SHORTNAME,LABTEST_CPT,FK_LKP_ADV_NCI,GRADE_CALC) values   
   (SEQ_ER_LABTEST.nextval,'Chagas','CHA',null,null,null);
  
         Insert into ER_LABTESTGRP (PK_LABTESTGRP,FK_TESTID,FK_LABGROUP) values
  (SEQ_ER_LABTESTGRP.nextval,SEQ_ER_LABTEST.currval,SEQ_ER_LABGROUP.currval);
      
  
 
 
 
    --Sql for the IDM Widget to create the Test Syphilis Confirmatory / Supplemental Test in ER_LABTEST 

  Insert into ER_LABTEST (PK_LABTEST,LABTEST_NAME,LABTEST_SHORTNAME,LABTEST_CPT,FK_LKP_ADV_NCI,GRADE_CALC) values   
   (SEQ_ER_LABTEST.nextval,'Syphilis Confirmatory / Supplemental Test','SYP_SUPP',null,null,null);
 
   Insert into ER_LABTESTGRP (PK_LABTESTGRP,FK_TESTID,FK_LABGROUP) values
  (SEQ_ER_LABTESTGRP.nextval,SEQ_ER_LABTEST.currval,SEQ_ER_LABGROUP.currval);
      
  
    --Sql for the IDM Widget to create the Test Syphilis Confirmatory / Supplemental Test (FTA-ABS) in ER_LABTEST 

  Insert into ER_LABTEST (PK_LABTEST,LABTEST_NAME,LABTEST_SHORTNAME,LABTEST_CPT,FK_LKP_ADV_NCI,GRADE_CALC) values   
   (SEQ_ER_LABTEST.nextval,'Syphilis Confirmatory / Supplemental Test (FTA-ABS)','SYP_SUPP_FT',null,null,null);
   
        Insert into ER_LABTESTGRP (PK_LABTESTGRP,FK_TESTID,FK_LABGROUP) values
  (SEQ_ER_LABTESTGRP.nextval,SEQ_ER_LABTEST.currval,SEQ_ER_LABGROUP.currval);
      
 
 --Sql for Lab Summary Widget to create the Group in ER_LABGROUP and Total Nucleated Cell Count
     
      Insert into ER_LABGROUP (PK_LABGROUP,GROUP_NAME,GROUP_TYPE) values 
      (SEQ_ER_LABGROUP.nextval,'LabSummGroup','LS');
          
      Insert into ER_LABTEST (PK_LABTEST,LABTEST_NAME,LABTEST_SHORTNAME,LABTEST_CPT,FK_LKP_ADV_NCI,GRADE_CALC) values 
      (SEQ_ER_LABTEST.nextval,'Total Nucleated Cell Count','TNCC',null,null,null);
   
      Insert into ER_LABTESTGRP (PK_LABTESTGRP,FK_TESTID,FK_LABGROUP) values
      (SEQ_ER_LABTESTGRP.nextval,SEQ_ER_LABTEST.currval,SEQ_ER_LABGROUP.currval);
     
--Sql for Lab Summary Widget to create the Group in ER_LABGROUP and nRBC Absolute Number
     
      Insert into ER_LABTEST (PK_LABTEST,LABTEST_NAME,LABTEST_SHORTNAME,LABTEST_CPT,FK_LKP_ADV_NCI,GRADE_CALC) values 
      (SEQ_ER_LABTEST.nextval,'nRBC Absolute Number','nRBC',null,null,null);
   
      Insert into ER_LABTESTGRP (PK_LABTESTGRP,FK_TESTID,FK_LABGROUP) values
      (SEQ_ER_LABTESTGRP.nextval,SEQ_ER_LABTEST.currval,SEQ_ER_LABGROUP.currval);
     
     
--Sql for Lab Summary Widget to create the Group in ER_LABGROUP and Total CD34 Cell Count
     
      Insert into ER_LABTEST (PK_LABTEST,LABTEST_NAME,LABTEST_SHORTNAME,LABTEST_CPT,FK_LKP_ADV_NCI,GRADE_CALC) values 
      (SEQ_ER_LABTEST.nextval,'Total CD34 Cell Count','TCD34',null,null,null);
   
      Insert into ER_LABTESTGRP (PK_LABTESTGRP,FK_TESTID,FK_LABGROUP) values
      (SEQ_ER_LABTESTGRP.nextval,SEQ_ER_LABTEST.currval,SEQ_ER_LABGROUP.currval);
      
--Sql for Lab Summary Widget to create the Group in ER_LABGROUP and Total CD3 Cell Count
     
      Insert into ER_LABTEST (PK_LABTEST,LABTEST_NAME,LABTEST_SHORTNAME,LABTEST_CPT,FK_LKP_ADV_NCI,GRADE_CALC) values 
      (SEQ_ER_LABTEST.nextval,'Total CD3 Cell Count','TCD3',null,null,null);
   
      Insert into ER_LABTESTGRP (PK_LABTESTGRP,FK_TESTID,FK_LABGROUP) values
      (SEQ_ER_LABTESTGRP.nextval,SEQ_ER_LABTEST.currval,SEQ_ER_LABGROUP.currval);
     
--Sql for Lab Summary Widget to create the Group in ER_LABGROUP and CFU Sample Test Count
     
      Insert into ER_LABTEST (PK_LABTEST,LABTEST_NAME,LABTEST_SHORTNAME,LABTEST_CPT,FK_LKP_ADV_NCI,GRADE_CALC) values 
      (SEQ_ER_LABTEST.nextval,'CFU Count','CFUC',null,null,null);
   
      Insert into ER_LABTESTGRP (PK_LABTESTGRP,FK_TESTID,FK_LABGROUP) values
      (SEQ_ER_LABTESTGRP.nextval,SEQ_ER_LABTEST.currval,SEQ_ER_LABGROUP.currval);
      
--Sql for Lab Summary Widget to create the Group in ER_LABGROUP and Viability  Test Result
     
      Insert into ER_LABTEST (PK_LABTEST,LABTEST_NAME,LABTEST_SHORTNAME,LABTEST_CPT,FK_LKP_ADV_NCI,GRADE_CALC) values 
      (SEQ_ER_LABTEST.nextval,'Viability Result','VIAR',null,null,null);
   
      Insert into ER_LABTESTGRP (PK_LABTESTGRP,FK_TESTID,FK_LABGROUP) values
      (SEQ_ER_LABTESTGRP.nextval,SEQ_ER_LABTEST.currval,SEQ_ER_LABGROUP.currval);
      

 Commit;
  

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,145,2,'02_ER_LAB_INSERT.sql',sysdate,'9.0.0 Build#602');

commit;