set define off;

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_OBJECT_SETTINGS
    where OBJECT_SUBTYPE = 'view_data_req'
    AND OBJECT_NAME = 'cbu_tools_menu';
  if (v_record_exists = 0) then
     Insert into ER_OBJECT_SETTINGS (PK_OBJECT_SETTINGS,OBJECT_TYPE,OBJECT_SUBTYPE,OBJECT_NAME,OBJECT_SEQUENCE,OBJECT_VISIBLE,OBJECT_DISPLAYTEXT,FK_ACCOUNT) values (SEQ_ER_OBJECT_SETTINGS.nextval,'M','view_data_req','cbu_tools_menu',4,0,'View Data Request',0);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_OBJECT_SETTINGS
    where OBJECT_SUBTYPE = 'creat_data_req'
    AND OBJECT_NAME = 'cbu_tools_menu';
  if (v_record_exists = 0) then
    Insert into ER_OBJECT_SETTINGS (PK_OBJECT_SETTINGS,OBJECT_TYPE,OBJECT_SUBTYPE,OBJECT_NAME,OBJECT_SEQUENCE,OBJECT_VISIBLE,OBJECT_DISPLAYTEXT,FK_ACCOUNT) values (SEQ_ER_OBJECT_SETTINGS.nextval,'M','creat_data_req','cbu_tools_menu',3,0,'Create Data Request',0);

	commit;
  end if;
end;
/
--END--
commit;



INSERT INTO track_patches
VALUES(seq_track_patches.nextval,157,24,'24_ER_OBJECT_SETTINGS_INSERT.sql',sysdate,'9.0.0 Build#614');

commit;