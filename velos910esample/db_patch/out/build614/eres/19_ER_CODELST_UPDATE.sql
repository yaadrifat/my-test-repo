set define off;

--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'note_assess'
    AND codelst_subtyp = 'tu';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_DESC='Temporarily unavailable' where codelst_type = 'note_assess' AND codelst_subtyp = 'tu';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'shipper'
    AND codelst_subtyp = 'ups';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_CUSTOM_COL='http://www.ups.com/tracking/tracking.html' where codelst_type = 'shipper'AND codelst_subtyp = 'ups';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'shipper'
    AND codelst_subtyp = 'quick';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_CUSTOM_COL='http://quick.aero/quickintl' where codelst_type = 'shipper'AND codelst_subtyp = 'quick';
	commit;
  end if;
end;
/
--END--


commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,157,19,'19_ER_CODELST_UPDATE.sql',sysdate,'9.0.0 Build#614');

commit;