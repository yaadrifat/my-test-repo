CREATE TABLE "ERES"."AUDIT_ROW_MODULE"
(
	"PK_ROW_ID"			NUMBER(10,0) NOT NULL,
	"TABLE_NAME"		VARCHAR2(30 BYTE),
	"RID"				NUMBER(10,0) ,
	"MODULE_ID"			NUMBER(10,0),
	"ACTION"			VARCHAR2(1 BYTE),
	"TIMESTAMP"			DATE,
	"USER_ID"			NUMBER(10,0),
	PRIMARY KEY ("PK_ROW_ID")
);

   COMMENT ON COLUMN "ERES"."AUDIT_ROW_MODULE"."PK_ROW_ID" IS 'Primary key of the table , this is generated from the Database sequence';
 
   COMMENT ON COLUMN "ERES"."AUDIT_ROW_MODULE"."TABLE_NAME" IS 'This column stores name of the table for which the auditing is being done.';
 
   COMMENT ON COLUMN "ERES"."AUDIT_ROW_MODULE"."RID" IS 'This column stores Record ID (RID) from the table.This will identify the row on which the audit is being done.';
   
   COMMENT ON COLUMN "ERES"."AUDIT_ROW_MODULE"."MODULE_ID" IS 'This column stores Primary key of the module (for example pk_budget/fk_budget for budget module).';
 
   COMMENT ON COLUMN "ERES"."AUDIT_ROW_MODULE"."ACTION" IS 'This column stores the action performed (I - Inserted, U-Updated, D-Deleted)';
 
   COMMENT ON COLUMN "ERES"."AUDIT_ROW_MODULE"."TIMESTAMP" IS 'This column stores the Date and Time of the action';
 
   COMMENT ON COLUMN "ERES"."AUDIT_ROW_MODULE"."USER_ID" IS 'This column stores the userid who made the transaction.';
 
   COMMENT ON TABLE "ERES"."AUDIT_ROW_MODULE"  IS 'This table stores the ModuleID, Table(s) in the module and transaction actions on which the audit operation is being done.';
 
CREATE TABLE "ERES"."AUDIT_COLUMN_MODULE"
(
	"PK_COLUMN_ID"			NUMBER(10,0) NOT NULL,
	"FK_ROW_ID"		 		NUMBER(10,0) NOT NULL ,
	"COLUMN_NAME"			VARCHAR2(30 BYTE) ,
	"OLD_VALUE"				VARCHAR2(4000 BYTE),
	"NEW_VALUE"				VARCHAR2(4000 BYTE),
	"FK_CODELST_REASON" 	NUMBER(10,0),
	"REMARKS"				VARCHAR2(4000 BYTE), 
  PRIMARY KEY ("PK_COLUMN_ID")
);

   COMMENT ON COLUMN "ERES"."AUDIT_COLUMN_MODULE"."PK_COLUMN_ID" IS 'Primary key of the table, this is generated from the Database sequence';
 
   COMMENT ON COLUMN "ERES"."AUDIT_COLUMN_MODULE"."FK_ROW_ID" IS 'Foreign key of the table AUDIT_ROW_MODULE.';
 
   COMMENT ON COLUMN "ERES"."AUDIT_COLUMN_MODULE"."COLUMN_NAME" IS 'The column name of the the audited column.';
 
   COMMENT ON COLUMN "ERES"."AUDIT_COLUMN_MODULE"."OLD_VALUE" IS 'Old data value of the affected column.';
 
   COMMENT ON COLUMN "ERES"."AUDIT_COLUMN_MODULE"."NEW_VALUE" IS 'New data value of the affected column.';
   
   COMMENT ON COLUMN "ERES"."AUDIT_COLUMN_MODULE"."FK_CODELST_REASON" IS 'This column stores the REASON ID from code list table.';
      
   COMMENT ON COLUMN "ERES"."AUDIT_COLUMN_MODULE"."REMARKS" IS 'This column stores the REMARKS (Additional details/comments) related to changes' ;
   
   COMMENT ON TABLE "ERES"."AUDIT_COLUMN_MODULE"  IS 'This table stores the column details ie the old and new values of the updated/inserted/deleted  columns';
 


--Primary key and Foreigm Key reference between "AUDIT_ROW_MODULE" and "AUDIT_COLUMN_MODULE"
ALTER TABLE "ERES"."AUDIT_COLUMN_MODULE"
ADD FOREIGN KEY ("FK_ROW_ID") 
REFERENCES "ERES"."AUDIT_ROW_MODULE" ("PK_ROW_ID");

--Sequence SEQ_AUDIT_ROW_MODULE for PK_ROW_ID column of AUDIT_ROW_MODULE Table 
CREATE SEQUENCE "ERES"."SEQ_AUDIT_ROW_MODULE"
            MINVALUE 1 
            MAXVALUE 999999999999999999999999999 
            INCREMENT BY 1 
            START WITH 1
            NOCACHE  ORDER  NOCYCLE;
--Sequence SEQ_AUDIT_COLUMN_MODULE for PK_COLUMN_ID column of AUDIT_COLUMN_MODULE Table 			
CREATE SEQUENCE "ERES"."SEQ_AUDIT_COLUMN_MODULE"
            MINVALUE 1 
            MAXVALUE 999999999999999999999999999 
            INCREMENT BY 1 
            START WITH 1
            NOCACHE  ORDER  NOCYCLE;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,157,4,'04_AUDIT_MODULE_TABLE.sql',sysdate,'9.0.0 Build#614');

commit;