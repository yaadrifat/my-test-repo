/* This readMe is specific to Velos eResearch version 9.0 build #614 */

=====================================================================================================================================
Garuda :
1. The "24_ER_OBJECT_SETTINGS_INSERT.sql" patch released with this build is specific to NMDP.
This patch consist two new sub menus of CBU Tools menu.
a). Create Data Request
b). View Data Request
=====================================================================================================================================
eResearch:

S-CTMS-4 : (not for QA release)
Please execute DB patch (03_CREATE_TABLE.sql,06_PKG_USER_SCRIPT.sql,08_PKG_USER_BODY.sql) for this enhancement files first, then build your project/start jboss.
AS new EJB entity file has been written for this enhancement, but QA has to follow this process.

CTRP-20579-6 & CTRP-20552-7:
1] CTRP Non-industrial and Industrial Drafts can now be marked as 'Ready for submission'. 
2] Validation Report link and eSign will be hidden for 'Ready for Submission' drafts. 
3] Saving scheme is now changed. When a draft is saved the screen will be refreshed, showing a standard 'Data saved successfully' message.
4] Alignment fixed.

=====================================================================================================================================
eResearch Localization:

1.Copy files -> messageBundle.properties and labelBundle.properties 
in ereshome\ and paste it to the folder specified by ERES_HOME 
(where eresearch.xml is also hosted), over-writing the existing files.

Following Files have been Modified:

1	43.xsl
2	MC.java
3	MC.jsp
4	LC.java
5	LC.jsp
6	messageBundle.properties
7	labelBundle.properties
8	milestonegrid.js
9	formflddelmultiple.jsp
10	lookupType.jsp

One report (XSL Based) has been corrected. The related script (01_data.sql) is Specific to this.

We need to execute the following in the sequence: 
1. 01_data.sql
2. loadxsl.bat
=====================================================================================================================================
