* This readMe is specific to Velos eResearch version 9.0 build #604 */
=======================================================================================================================
Garuda :
1. hibernate.cfg.xml database configurations are no longer needed. Please leave the file as it is.

=======================================================================================================================
eResearch :

# As an effort to discontinue support for 800*600 screen resolution, following files 
1. ie_800.css
2. ns_800.css

have to be removed from following locations:
1] \server\eresearch\deploy\velos.ear\velos.war\jsp\styles
2] \server\eresearch\deploy\velos.ear\velos.war\jsp\styles\bethematch
3] \server\eresearch\deploy\velos.ear\velos.war\jsp\styles\default
4] \server\eresearch\deploy\velos.ear\velos.war\jsp\styles\redmond

-------------------------------------------------------------------------------------------------------------------------
# While re-organizing the existing skins, following files 
1. ns_1024.css
2. ns_gt1024.css
3. velos_style.css
4. velos_style_old.css
5. velos_style-old.css

have to be removed from following locations:
1] \server\eresearch\deploy\velos.ear\velos.war\jsp\styles\bethematch
2] \server\eresearch\deploy\velos.ear\velos.war\jsp\styles\default
3] \server\eresearch\deploy\velos.ear\velos.war\jsp\styles\redmond

=========================================================================================================================
For Localization 

1. Copy files -> messageBundle.properties and labelBundle.properties 
in ereshome\ and paste it to the folder specified by ERES_HOME 
(where eresearch.xml is also hosted), over-writing the existing files.

2. These properties files and their custom properties files can now be dynamically loaded to eResearch.
Once the changes in these files are saved in ERES_HOME, the admin group user can go to Manage Users and 
click "Refresh tabs/menus" and reload the page. The changes should appear.

=========================================================================================================================
Following Files have been Modified for localization:

Following Files have been Modified:

1	accountforms.jsp
2	addeventuser.jsp
3	adveventbrowser.jsp
4	adveventlookup.jsp
5	ajaxFinancialDataFetch.jsp
6	ajaxStudyDataFetch.jsp
7	budgetbrowserpg.jsp
8	copyFromFieldLibrary.jsp
9	dataRecvd.jsp
10	dynfilterbrowse.jsp
11	dynrepbrowse.jsp
12	editbgtprotocols.jsp
13	editPersonnelCost.jsp
14	editRepeatLineItems.jsp
15	enrolledpatient.jsp
16	enrollpatientsearch.jsp
17	eventlibrary.jsp
18	eventlibrarycstest.jsp
19	fieldBrowser.jsp
20	fieldLibraryBrowser.jsp
21	formdatarecords.jsp
22	formfldbrowser.jsp
23	formLibraryBrowser.jsp
24	getlookup.jsp
25	getmultilookup.jsp
26	groupbrowserpg.jsp
27	labbrowser.jsp
28	labelBundle.properties
29	LC.java
30	LC.jsp
31	manageportallogins.jsp
32	MC.java
33	MC.jsp
34	messageBundle.properties
35	milestonehome.jsp
36	multipleChoiceBox.jsp
37	multipleChoiceSection.jsp
38	multipleusersearchdetails.jsp
39	multipleusersearchdetailswithsave.jsp
40	patientsearch.jsp
41	portal.jsp
42	protocollist.jsp
43	revSetup.jsp
44	savedrepbrowser.jsp
45	sectionBrowserNew.jsp
46	selecteventus.jsp
47	selectremoveusers.jsp
48	specimenbrowser.jsp
49	storageadminbrowser.jsp
50	storagekitbrowser.jsp
51	studyadmincal.jsp
52	studybrowserpg.jsp
53	studyprotocols.jsp
54	studyschedule.jsp
55	studyVerBrowser.jsp
56	updateaccountuser.jsp
57	usersearchdetails.jsp
58	viemsgbrowse.jsp
59	viemsglist.jsp
60	viewexpstatus.jsp
61	viewimpstatus.jsp
========================================================================================================================
Garuda :
	
	After deploying the velos.ear file successfully.
	
	we can find two jar files ( inside the $JBOSS_HOME/server/eresearch/deploy/velos.ear/velos.war/WEB-INF/lib directory ) 
	named "hibernate-validator.jar" and "hibernate-validator-3.1.0.GA.jar" from which we need to remove one jar file based
	on the Operating System, where the application is deployed.
	
	1.In case of Windows we need to remove "hibernate-validator-3.1.0.GA.jar" and
	2.In case of Unix/Linux we need to remove "hibernate-validator.jar".

=========================================================================================================================

