set define off;

DECLARE
  v_item_exists number := 0;  
BEGIN
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ProgramCode' and codelst_subtyp = 'NEI';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ProgramCode','NEI','NEI-National Eye Institute','N',1, 'NIH');
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:NEI inserted');
	else
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:NEI already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ProgramCode' and codelst_subtyp = 'NHLBI';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ProgramCode','NHLBI','NHLBI-National Heart, Lung, and Blood Institute','N',2, 'NIH');
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:NHLBI inserted');
	else
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:NHLBI already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ProgramCode' and codelst_subtyp = 'NHGRI';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ProgramCode','NHGRI','NHGRI-National Human Genome Research Institute','N',3, 'NIH');
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:NHGRI inserted');
	else
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:NHGRI already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ProgramCode' and codelst_subtyp = 'NIA';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ProgramCode','NIA','NIA-National Institute on Aging','N',4, 'NIH');
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:NIA inserted');
	else
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:NIA already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ProgramCode' and codelst_subtyp = 'NIAAA';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ProgramCode','NIAAA','NIAAA-National Institute on Alcohol Abuse and Alcoholism','N',5, 'NIH');
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:NIAAA inserted');
	else
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:NIAAA already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ProgramCode' and codelst_subtyp = 'NIAID';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ProgramCode','NIAID','NIAID-National Institute of Allergy and Infectious Diseases','N',6, 'NIH');
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:NIAID inserted');
	else
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:NIAID already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ProgramCode' and codelst_subtyp = 'NIAMS';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ProgramCode','NIAMS','NIAMS-National Institute of Arthritis and Musculoskeletal and Skin Diseases','N',7, 'NIH');
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:NIAMS inserted');
	else
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:NIAMS already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ProgramCode' and codelst_subtyp = 'NIBIB';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ProgramCode','NIBIB','NIBIB-National Institute of Biomedical Imaging and Bioengineering','N',8, 'NIH');
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:NIBIB inserted');
	else
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:NIBIB already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ProgramCode' and codelst_subtyp = 'NICHD';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ProgramCode','NICHD','NICHD-Eunice Kennedy Shriver National Institute of Child Health and Human Development','N',9, 'NIH');
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:NICHD inserted');
	else
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:NICHD already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ProgramCode' and codelst_subtyp = 'NIDCD';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ProgramCode','NIDCD','NIDCD-National Institute on Deafness and Other Communication Disorders','N',10, 'NIH');
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:NIDCD inserted');
	else
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:NIDCD already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ProgramCode' and codelst_subtyp = 'NIDCR';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ProgramCode','NIDCR','NIDCR-National Institute of Dental and Craniofacial Research','N',11, 'NIH');
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:NIDCR inserted');
	else
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:NIDCR already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ProgramCode' and codelst_subtyp = 'NIDDK';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ProgramCode','NIDDK','NIDDK-National Institute of Diabetes and Digestive and Kidney Diseases','N',12, 'NIH');
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:NIDDK inserted');
	else
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:NIDDK already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ProgramCode' and codelst_subtyp = 'NIDA';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ProgramCode','NIDA','NIDA-National Institute on Drug Abuse','N',13, 'NIH');
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:NIDA inserted');
	else
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:NIDA already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ProgramCode' and codelst_subtyp = 'NIEHS';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ProgramCode','NIEHS','NIEHS-National Institute of Environmental Health Sciences','N',14, 'NIH');
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:NIEHS inserted');
	else
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:NIEHS already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ProgramCode' and codelst_subtyp = 'NIGMS';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ProgramCode','NIGMS','NIGMS-National Institute of General Medical Sciences','N',15, 'NIH');
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:NIGMS inserted');
	else
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:NIGMS already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ProgramCode' and codelst_subtyp = 'NIMH';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ProgramCode','NIMH','NIMH-National Institute of Mental Health','N',16, 'NIH');
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:NIMH inserted');
	else
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:NIMH already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ProgramCode' and codelst_subtyp = 'NINDS';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ProgramCode','NINDS','NINDS-National Institute of Neurological Disorders and Stroke','N',17, 'NIH');
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:NINDS inserted');
	else
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:NINDS already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ProgramCode' and codelst_subtyp = 'NINR';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ProgramCode','NINR','NINR-National Institute of Nursing Research','N',18, 'NIH');
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:NINR inserted');
	else
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:NINR already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ProgramCode' and codelst_subtyp = 'NLM';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ProgramCode','NLM','NLM-National Library of Medicine','N',19, 'NIH');
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:NLM inserted');
	else
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:NLM already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ProgramCode' and codelst_subtyp = 'CIT';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ProgramCode','CIT','CIT-Center for Information Technology','N',20, 'NIH');
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:CIT inserted');
	else
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:CIT already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ProgramCode' and codelst_subtyp = 'CSR';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ProgramCode','CSR','CSR-Center for Scientific Review','N',21, 'NIH');
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:CSR inserted');
	else
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:CSR already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ProgramCode' and codelst_subtyp = 'FIC';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ProgramCode','FIC','FIC-John E. Fogarty International Center for Advanced Study in the Health Sciences','N',22, 'NIH');
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:FIC inserted');
	else
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:FIC already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ProgramCode' and codelst_subtyp = 'NCCAM';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ProgramCode','NCCAM','NCCAM-National Center for Complementary and Alternative Medicine','N',23, 'NIH');
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:NCCAM inserted');
	else
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:NCCAM already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ProgramCode' and codelst_subtyp = 'NCMHD';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ProgramCode','NCMHD','NCMHD-National Center on Minority Health and Health Disparities','N',24, 'NIH');
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:NCMHD inserted');
	else
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:NCMHD already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ProgramCode' and codelst_subtyp = 'NCRR';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ProgramCode','NCRR','NCRR-National Center for Research Resources (NCRR','N',25, 'NIH');
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:NCRR inserted');
	else
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:NCRR already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ProgramCode' and codelst_subtyp = 'CC';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ProgramCode','CC','CC-NIH Clinical Center','N',26, 'NIH');
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:CC inserted');
	else
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:CC already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ProgramCode' and codelst_subtyp = 'OD';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ProgramCode','OD','OD-Office of the Director','N',27, 'NIH');
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:OD inserted');
	else
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:OD already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ProgramCode' and codelst_subtyp = 'CCR';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ProgramCode','CCR','CCR','N',28, 'NCI');
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:CCR inserted');
	else
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:CCR already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ProgramCode' and codelst_subtyp = 'CTEP';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ProgramCode','CTEP','CTEP','N',29, 'NCI');
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:CTEP inserted');
	else
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:CTEP already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ProgramCode' and codelst_subtyp = 'DCB';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ProgramCode','DCB','DCB','N',30, 'NCI');
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:DCB inserted');
	else
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:DCB already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ProgramCode' and codelst_subtyp = 'DCCPS';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ProgramCode','DCCPS','DCCPS','N',31, 'NCI');
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:DCCPS inserted');
	else
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:DCCPS already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ProgramCode' and codelst_subtyp = 'DCEG';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ProgramCode','DCEG','DCEG','N',32, 'NCI');
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:DCEG inserted');
	else
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:DCEG already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ProgramCode' and codelst_subtyp = 'DTP';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ProgramCode','DTP','DTP','N',33, 'NCI');
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:DTP inserted');
	else
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:DTP already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ProgramCode' and codelst_subtyp = 'DCP';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ProgramCode','DCP','DCP','N',34, 'NCI');
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:DCP inserted');
	else
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:DCP already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ProgramCode' and codelst_subtyp = 'DEA';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ProgramCode','DEA','DEA','N',35, 'NCI');
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:DEA inserted');
	else
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:DEA already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ProgramCode' and codelst_subtyp = 'OD';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ProgramCode','OD','OD','N',36, 'NCI');
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:OD inserted');
	else
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:OD already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ProgramCode' and codelst_subtyp = 'OSB/SPOREs';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ProgramCode','OSB/SPOREs','OSB/SPOREs','N',37, 'NCI');
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:OSB/SPOREs inserted');
	else
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:OSB/SPOREs already exists');
	end if;
	
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ProgramCode' and codelst_subtyp = 'CIP';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ProgramCode','CIP','CIP','N',38, 'NCI');
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:CIP inserted');
	else
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:CIP already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ProgramCode' and codelst_subtyp = 'CDP';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ProgramCode','CDP','CDP','N',39, 'NCI');
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:CDP inserted');
	else
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:CDP already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ProgramCode' and codelst_subtyp = 'TRP';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ProgramCode','TRP','TRP','N',40, 'NCI');
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:TRP inserted');
	else
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:TRP already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ProgramCode' and codelst_subtyp = 'RRP';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ProgramCode','RRP','RRP','N',41, 'NCI');
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:RRP inserted');
	else
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:RRP already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ProgramCode' and codelst_subtyp = 'N/A';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ProgramCode','N/A','N/A','N',42, 'NCI');
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:N/A inserted');
	else
		dbms_output.put_line('Code-list item Type:ProgramCode Subtype:N/A already exists');
	end if;

	COMMIT;

end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,147,7,'07_codelist_items_ProgramCode.sql',sysdate,'9.0.0 Build#604');

commit;