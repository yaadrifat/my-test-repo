-----------------------------------------------Alter in  ER_ORDER---------------------------------------------------------

ALTER TABLE ER_ORDER ADD(RESULT_REC_DATE DATE);
COMMENT ON COLUMN "ER_ORDER"."RESULT_REC_DATE" IS 'Date the HLA typing results were received from the laboratory';

ALTER TABLE ER_ORDER ADD(ACCPT_TO_CANCEL_REQ VARCHAR2(2 BYTE));
COMMENT ON COLUMN "ER_ORDER"."ACCPT_TO_CANCEL_REQ" IS 'Indication that the CBB confirmed a cancellation request by the CM';

ALTER TABLE ER_ORDER ADD(CANCEL_CONFORM_DATE DATE);
COMMENT ON COLUMN "ER_ORDER"."CANCEL_CONFORM_DATE" IS 'Date when the CBB confirms or rejects a cancellation request from CM';

ALTER TABLE ER_ORDER ADD(CANCELED_BY NUMBER(10,0));
COMMENT ON COLUMN "ER_ORDER"."CANCELED_BY" IS 'User Namethat acknowledged receipt of the system generated resolution';

ALTER TABLE ER_ORDER ADD(ORDER_CBB_AVAIL_FOR_NMDP VARCHAR2(2 BYTE));
COMMENT ON COLUMN "ER_ORDER"."ORDER_CBB_AVAIL_FOR_NMDP" IS 'Indication that CBB can proceed with the request, meaning that the actual CBU is available for the TC';

ALTER TABLE ER_ORDER ADD(ORDER_SAMPLE_TYPE_AVAIL VARCHAR2(2 BYTE));
COMMENT ON COLUMN "ER_ORDER"."ORDER_SAMPLE_TYPE_AVAIL" IS 'Indication that the CBB has samples available that can be sent for typing';

ALTER TABLE ER_ORDER ADD(ORDER_ALIQUOTS_TYPE VARCHAR2(20 BYTE));
COMMENT ON COLUMN "ER_ORDER"."ORDER_ALIQUOTS_TYPE" IS 'aliquot type which will be sent to the laboratory for the CT';

ALTER TABLE ER_ORDER ADD(ORDER_APPROVED_BY NUMBER(10,0));
COMMENT ON COLUMN "ER_ORDER"."ORDER_APPROVED_BY" IS 'Name of user confirmed that the CBB will be proceeding with the CT';

ALTER TABLE ER_ORDER ADD(ORDER_APPROVED_DATE DATE);
COMMENT ON COLUMN "ER_ORDER"."ORDER_APPROVED_DATE" IS 'Date of when the CBB user confirmed that they would be proceeding with the CT';


ALTER TABLE ER_ORDER ADD(CLINIC_INFO_CHECKLIST_STAT VARCHAR2(20 BYTE));
COMMENT ON COLUMN "ER_ORDER"."CLINIC_INFO_CHECKLIST_STAT" IS 'Status of the Required Clinical Information Entry task (Pending, Complete)';


ALTER TABLE ER_ORDER ADD(RECENT_HLA_TYPING_AVAIL VARCHAR2(2 BYTE));
COMMENT ON COLUMN "ER_ORDER"."RECENT_HLA_TYPING_AVAIL" IS 'Indication of whether the HLA typing at the time of the request is the most current or not';


ALTER TABLE ER_ORDER ADD(ADDI_TEST_RESULT_AVAIL VARCHAR2(2 BYTE));
COMMENT ON COLUMN "ER_ORDER"."ADDI_TEST_RESULT_AVAIL" IS 'Indication if additional CFU/Viability testing was completed by the bank at the time of the request';


ALTER TABLE ER_ORDER ADD(ORDER_SAMPLE_AT_LAB VARCHAR2(2 BYTE));
COMMENT ON COLUMN "ER_ORDER"."ORDER_SAMPLE_AT_LAB" IS 'Indication of whether the Laboratory has a sample from a previous CT';

ALTER TABLE ER_ORDER ADD (FK_CASE_MANAGER NUMBER(10), 
CASE_MANAGER VARCHAR2(20));

COMMENT ON COLUMN "ER_ORDER"."FK_CASE_MANAGER" IS 'Foreign Key reference for case maneger';

COMMENT ON COLUMN "ER_ORDER"."CASE_MANAGER" IS 'This field contains the information of Case Maneger';

-----------------------------------------------Alter in ER_ORDER ends here---------------------------------------------------------

-----------------------------------------------Alter in CB_CORD---------------------------------------------------------

ALTER TABLE CB_CORD ADD(CORD_LAST_REVIEWED_DATE DATE);
COMMENT ON COLUMN "CB_CORD"."CORD_LAST_REVIEWED_DATE" IS 'Date of when the CBU file was last reviewed by';
ALTER TABLE CB_CORD ADD(CORD_LAST_REVIEWED_BY NUMBER(10,0));
COMMENT ON COLUMN "CB_CORD"."CORD_LAST_REVIEWED_BY" IS 'Name of person that last reviewed any of the CBU information/file for the request.';

ALTER TABLE CB_CORD ADD(CORD_UNAVAIL_REASON VARCHAR2(20 BYTE));
COMMENT ON COLUMN "CB_CORD"."CORD_UNAVAIL_REASON" IS 'Stores reason for cord unavailability';
-----------------------------------------------Alter in CB_CORD ends here---------------------------------------------------------


ALTER TABLE CBB ADD  USING_CDR VARCHAR(1);

COMMENT ON COLUMN "CBB"."USING_CDR" IS 'This field indicates that this CBB being CDR';

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,137,10,'10_CB_ALTER_TABLES.sql',sysdate,'9.0.0 Build#594');

commit;