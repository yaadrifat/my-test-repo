SET DEFINE OFF;

update er_codelst set codelst_subtyp='eligible', codelst_desc='Eligible' where codelst_subtyp=' eligible';
Update er_codelst set codelst_type=ltrim(codelst_type) where codelst_type like '%eligibility';

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,137,8,'08_Er_codelst_update.sql',sysdate,'9.0.0 Build#594');

commit;