CREATE OR REPLACE TRIGGER "ESCH"."SCH_PROTOCOL_VISIT_BI1"
BEFORE INSERT
ON SCH_PROTOCOL_VISIT
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
WHEN (
NEW.displacement IS NULL OR NEW.displacement = 0
      )
DECLARE
BEGIN
  IF NVL(:NEW.NO_INTERVAL_FLAG,0) = 1 THEN
    :NEW.displacement := null;
  ELSE
    IF NVL(:NEW.displacement ,0) = 0 THEN
		:NEW.displacement := 1;
    END IF;
  END IF;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,137,1,'01_583a_sch_protocol_visit_bi1.sql',sysdate,'9.0.0 Build#594');

commit;