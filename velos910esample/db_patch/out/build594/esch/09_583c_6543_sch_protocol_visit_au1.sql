CREATE OR REPLACE TRIGGER "SCH_PROTOCOL_VISIT_AU1"
AFTER UPDATE
ON SCH_PROTOCOL_VISIT REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
v_pk_budget Number;
v_pk_bgtcal Number;

minsection  NUMBER(38,0); 
maxsection  NUMBER(38,0); --ADDED BY BIKASH

pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SCH_PROTOCOL_VISIT_AU1', pLEVEL  => Plog.LDEBUG);

BEGIN

--get calendar's default budget
begin

--    plog.debug(pctx,':new.fk_protocol '||:new.fk_protocol );

    select pk_budget,PK_BGTCAL
    into v_pk_budget,v_pk_bgtcal
    from sch_budget,sch_bgtcal
    where budget_calendar = :new.fk_protocol and pk_budget = fk_budget 
    and bgtcal_protid = :new.fk_protocol 
    and nvl(BUDGET_DELFLAG, 'Z') <> 'Y' and rownum < 2;
	

    select DECODE(:new.NUM_DAYS,0,0,:new.displacement) into minsection from dual;
  
	Update SCH_BGTSECTION
	set BGTSECTION_NAME = :new.visit_name, BGTSECTION_SEQUENCE = minsection,
	IP_ADD = :new.ip_add,BGTSECTION_NOTES =  :new.description, last_modified_by = :new.last_modified_by ,last_modified_date = sysdate
	where fk_visit = :new.pk_protocol_visit and FK_BGTCAL = v_pk_bgtcal;
   
  select max(BGTSECTION_SEQUENCE) into maxsection from SCH_BGTSECTION where FK_VISIT is null 
  and BGTSECTION_TYPE is not null and FK_BGTCAL = v_pk_bgtcal;
 
  --plog.debug(pctx, 'maxsection: '||maxsection|| ' minsection: ' || minsection );
  while (minsection <= maxsection)
  loop
    if maxsection < 0 then
      Update SCH_BGTSECTION set BGTSECTION_SEQUENCE = BGTSECTION_SEQUENCE + maxsection
      where FK_VISIT is null and FK_BGTCAL = v_pk_bgtcal;
    else
       if maxsection = 0 then
         maxsection := 1;
       end if;
      
      Update SCH_BGTSECTION set BGTSECTION_SEQUENCE = BGTSECTION_SEQUENCE - maxsection
      where FK_VISIT is null and FK_BGTCAL = v_pk_bgtcal;
    end if;
    
    select max(BGTSECTION_SEQUENCE) into maxsection from SCH_BGTSECTION where FK_VISIT is null 
    and BGTSECTION_TYPE is not null and FK_BGTCAL = v_pk_bgtcal;
    
    select min(BGTSECTION_SEQUENCE) into minsection from SCH_BGTSECTION where FK_VISIT is not null and FK_BGTCAL = v_pk_bgtcal;
  end loop;

exception when no_data_found then
    v_pk_budget := 0;
    v_pk_bgtcal := 0;
    plog.fatal(pctx,'exception: :new.fk_protocol '||:new.fk_protocol );

end;

END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,137,9,'09_583c_6543_sch_protocol_visit_au1.sql',sysdate,'9.0.0 Build#594');

commit;