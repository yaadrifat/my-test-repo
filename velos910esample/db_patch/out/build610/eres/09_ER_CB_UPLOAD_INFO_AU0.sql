create or replace
TRIGGER "ERES".ER_CB_UPLOAD_INFO_AU0  AFTER UPDATE OF PK_UPLOAD_INFO,DESCRIPTION,COMPLETION_DATE,TEST_DATE,PROCESS_DATE,VERIFICATION_TYPING,RECEIVED_DATE,FK_CATEGORY,FK_SUBCATEGORY,FK_ATTACHMENTID,DELETEDFLAG,REPORT_DATE,CREATOR,CREATED_ON,IP_ADD,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,RID  
ON CB_UPLOAD_INFO REFERENCING OLD AS OLD NEW AS NEW

FOR EACH ROW
DECLARE
   raid NUMBER(10);
   usr VARCHAR(200); 
   old_modified_by VARCHAR2(100);
   new_modified_by VARCHAR2(100);
BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  
   usr := getuser(:NEW.LAST_MODIFIED_BY);
   
  audit_trail.record_transaction
    (raid, 'CB_UPLOAD_INFO', :OLD.RID, 'U', usr);
    
  IF NVL(:OLD.PK_UPLOAD_INFO,0) !=
     NVL(:NEW.PK_UPLOAD_INFO,0) THEN
     audit_trail.column_update
       (raid, 'PK_UPLOAD_INFO',
       :OLD.PK_UPLOAD_INFO, :NEW.PK_UPLOAD_INFO);
  END IF;
  IF NVL(:OLD.DESCRIPTION,' ') !=
     NVL(:NEW.DESCRIPTION,' ') THEN
     audit_trail.column_update
       (raid, 'DESCRIPTION',
       :OLD.DESCRIPTION, :NEW.DESCRIPTION);
  END IF;  
  IF NVL(:OLD.COMPLETION_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:NEW.COMPLETION_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
     audit_trail.column_update
       (raid, 'COMPLETION_DATE',
       to_char(:OLD.COMPLETION_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.COMPLETION_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.TEST_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:NEW.TEST_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
     audit_trail.column_update
       (raid, 'TEST_DATE',
       to_char(:OLD.TEST_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.TEST_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
    END IF;  
	IF NVL(:OLD.PROCESS_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:NEW.PROCESS_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
     audit_trail.column_update
       (raid, 'PROCESS_DATE',
       to_char(:OLD.PROCESS_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.PROCESS_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
    END IF;
	IF NVL(:OLD.VERIFICATION_TYPING,' ') !=
     NVL(:NEW.VERIFICATION_TYPING,' ') THEN
     audit_trail.column_update
       (raid, 'VERIFICATION_TYPING',
       :OLD.VERIFICATION_TYPING, :NEW.VERIFICATION_TYPING);
  END IF;	
	IF NVL(:OLD.RECEIVED_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:NEW.RECEIVED_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
     audit_trail.column_update
       (raid, 'RECEIVED_DATE',
       to_char(:OLD.RECEIVED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.RECEIVED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
    END IF;
  IF NVL(:OLD.FK_CATEGORY,0) !=
     NVL(:NEW.FK_CATEGORY,0) THEN
     audit_trail.column_update
       (raid, 'FK_CATEGORY',
       :OLD.FK_CATEGORY, :NEW.FK_CATEGORY);
  END IF;   
  IF NVL(:OLD.FK_SUBCATEGORY,0) !=
     NVL(:NEW.FK_SUBCATEGORY,0) THEN
     audit_trail.column_update
       (raid, 'FK_SUBCATEGORY',
       :OLD.FK_SUBCATEGORY, :NEW.FK_SUBCATEGORY);
  END IF;   
   IF NVL(:OLD.FK_ATTACHMENTID,0) !=
     NVL(:NEW.FK_ATTACHMENTID,0) THEN
     audit_trail.column_update
       (raid, 'FK_ATTACHMENTID',
       :OLD.FK_ATTACHMENTID, :NEW.FK_ATTACHMENTID);
  END IF;
  IF NVL(:OLD.DELETEDFLAG,' ') !=
     NVL(:NEW.DELETEDFLAG,' ') THEN
     audit_trail.column_update
       (raid, 'DELETEDFLAG',
       :OLD.DELETEDFLAG, :NEW.DELETEDFLAG);
    END IF;
	IF NVL(:OLD.REPORT_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:NEW.REPORT_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
     audit_trail.column_update
       (raid, 'REPORT_DATE',
       to_char(:OLD.REPORT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.REPORT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
    END IF;
  IF NVL(:OLD.CREATOR,0) !=
     NVL(:NEW.CREATOR,0) THEN
     audit_trail.column_update
       (raid, 'CREATOR',
       :OLD.CREATOR, :NEW.CREATOR);
  END IF;  
  IF NVL(:OLD.CREATED_ON,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:NEW.CREATED_ON,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
     audit_trail.column_update
       (raid, 'CREATED_ON',
       to_char(:OLD.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT));
    END IF;
  IF NVL(:OLD.IP_ADD,0) !=
     NVL(:NEW.IP_ADD,0) THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.IP_ADD, :NEW.IP_ADD);     
    END IF;    
  if nvl(:old.LAST_MODIFIED_BY,0) !=
    NVL(:new.LAST_MODIFIED_BY,0) then
    Begin
      Select  to_char(pk_user) || ',' ||  usr_lastname ||', ' || usr_firstname
      into old_modified_by from er_user  where pk_user = :old.last_modified_by ;
      Exception When NO_DATA_FOUND then
			old_modified_by := null;
    End ;
    Begin
      Select  to_char(pk_user) || ',' || usr_lastname ||', ' || usr_firstname
      into new_modified_by   from er_user   where pk_user = :new.LAST_MODIFIED_BY ;
      Exception When NO_DATA_FOUND then
			new_modified_by := null;
    End ;
      audit_trail.column_update (raid,'LAST_MODIFIED_BY' , old_modified_by, new_modified_by);
    end if;

    IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
      NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:OLD.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
    END IF;
    IF NVL(:OLD.RID,0) !=
     NVL(:NEW.RID,0) THEN
     audit_trail.column_update
       (raid, 'RID',
       :OLD.RID, :NEW.RID);     
    END IF;
END ;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,153,9,'09_ER_CB_UPLOAD_INFO_AU0.sql',sysdate,'9.0.0 Build#610');

commit;
