set define off;

----------------------------CB CORD TAble--------------------------------------------------
 
 CREATE TABLE "CB_CORD" 
   (    "PK_CORD" NUMBER(10,0) PRIMARY KEY, 
    "CORD_CDR_CBU_ID" VARCHAR2(10 BYTE), 
    "CORD_EXTERNAL_CBU_ID" VARCHAR2(20 BYTE), 
    "CORD_TNC_FROZEN" NUMBER(10,2), 
    "CORD_CD34_CELL_COUNT" NUMBER(10,2), 
    "CORD_TNC_FROZEN_PAT_WT" NUMBER(10,2), 
    "CORD_BABY_BIRTH_DATE" DATE, 
    "CORD_CBU_COLLECTION_DATE" DATE, 
    "CORD_REGISTRY_ID" VARCHAR2(20 BYTE), 
    "CORD_LOCAL_CBU_ID" VARCHAR2(20 BYTE), 
    "CORD_ID_NUMBER_ON_CBU_BAG" VARCHAR2(10 BYTE), 
    "FK_CORD_BACT_CUL_RESULT" NUMBER(10,0), 
    "FK_CORD_FUNGAL_CUL_RESULT" NUMBER(10,0), 
    "FK_CORD_ABO_BLOOD_TYPE" NUMBER(10,0), 
    "FK_CORD_RH_TYPE" NUMBER(10,0), 
    "FK_CORD_BABY_GENDER_ID" NUMBER(10,0), 
    "FK_CORD_CLINICAL_STATUS" NUMBER(10,0), 
    "FK_CORD_CDR_CBU_STATUS" NUMBER(10,0), 
    "FK_CORD_CBU_LIC_STATUS" NUMBER(10,0), 
    "CREATOR" NUMBER(10,0), 
    "CREATED_ON" DATE, 
    "LAST_MODIFIED_BY" NUMBER(10,0), 
    "LAST_MODIFIED_DATE" DATE, 
    "IP_ADD" VARCHAR2(15 BYTE), 
    "DELETEDFLAG" VARCHAR2(1 BYTE), 
    "RID" NUMBER(10,0), 
    "ADDITIONAL_INFO_RECQUIRED" VARCHAR2(1 BYTE), 
    "CORD_ISBI_DIN_CODE" VARCHAR2(255 CHAR), 
    "CORD_ISIT_PRODUCT_CODE" VARCHAR2(255 CHAR), 
    "CORD_ELIGIBLE_ADDITIONAL_INFO" VARCHAR2(255 CHAR), 
    "FK_CBB_ID" NUMBER(19,0), 
    "FK_CORD_CBU_ELIGIBLE_STATUS" NUMBER(19,0), 
    "FK_CORD_CBU_STATUS" NUMBER(19,0), 
    "FK_CORD_CBU_INELIGIBLE_REASON" NUMBER(19,0), 
    "FK_CORD_CBU_UNLICENSED_REASON" NUMBER(19,0), 
    "FK_CODELST_EXTERNAL_NAME " NUMBER(19,0), 
    "FK_CODELST_EXTERNAL_NUM" NUMBER(19,0), 
    "FK_CODELST_EXTERNAL " NUMBER(19,0), 
    "FK_CODE_OTHER" NUMBER(19,0), 
    "IND_OTHER_NAME" VARCHAR2(255 CHAR), 
    "IND_OTHER_NUM" VARCHAR2(255 CHAR), 
    "CORD_CLINICAL_STATUS_DATE" DATE, 
    "CORD_CLINICAL_STATUS_ADDI_INFO" VARCHAR2(200 BYTE), 
    "FK_SPECIMEN_ID" NUMBER(10,0)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "ERES_USER" ;
  
 
   COMMENT ON TABLE "CB_CORD"  IS 'This table stores cord records.';

   COMMENT ON COLUMN "CB_CORD"."PK_CORD" IS 'Primary Key ';
 
   COMMENT ON COLUMN "CB_CORD"."CORD_CDR_CBU_ID" IS 'CBU ID';
 
   COMMENT ON COLUMN "CB_CORD"."CORD_EXTERNAL_CBU_ID" IS 'CBU External Id';
 
   COMMENT ON COLUMN "CB_CORD"."CORD_TNC_FROZEN" IS 'TNC Frozen Volume';
 
   COMMENT ON COLUMN "CB_CORD"."CORD_CD34_CELL_COUNT" IS 'CD 34 Cell Count';
 
   COMMENT ON COLUMN "CB_CORD"."CORD_TNC_FROZEN_PAT_WT" IS 'TNC frozen pattren';
 
   COMMENT ON COLUMN "CB_CORD"."CORD_BABY_BIRTH_DATE" IS 'Date of Birth for Baby';
 
   COMMENT ON COLUMN "CB_CORD"."CORD_CBU_COLLECTION_DATE" IS 'Collection date of the cord';
 
   COMMENT ON COLUMN "CB_CORD"."CORD_REGISTRY_ID" IS 'Registry id of the cord';
 
   COMMENT ON COLUMN "CB_CORD"."CORD_LOCAL_CBU_ID" IS 'Local CBU ID';
 
   COMMENT ON COLUMN "CB_CORD"."CORD_ID_NUMBER_ON_CBU_BAG" IS 'ID Number given over Bag';
 
   COMMENT ON COLUMN "CB_CORD"."FK_CORD_BACT_CUL_RESULT" IS 'Code for bacterial culture result  referenced from Code List(ER_CODELST)';
 
   COMMENT ON COLUMN "CB_CORD"."FK_CORD_FUNGAL_CUL_RESULT" IS 'Code for bacterial culture result  referenced from Code List(ER_CODELST)';
 
   COMMENT ON COLUMN "CB_CORD"."FK_CORD_ABO_BLOOD_TYPE" IS 'Code for blood type referenced from Code List(ER_CODELST)';
 
   COMMENT ON COLUMN "CB_CORD"."FK_CORD_RH_TYPE" IS 'Code for rh type referenced from Code List(ER_CODELST)';
 
   COMMENT ON COLUMN "CB_CORD"."FK_CORD_BABY_GENDER_ID" IS 'Code for baby gender referenced from Code List(ER_CODELST)';
 
   COMMENT ON COLUMN "CB_CORD"."FK_CORD_CLINICAL_STATUS" IS 'Code for clinical status referenced from Code List(ER_CODELST)';
 
   COMMENT ON COLUMN "CB_CORD"."FK_CORD_CDR_CBU_STATUS" IS 'Code for CBU status referenced from Code List(ER_CODELST)';
 
   COMMENT ON COLUMN "CB_CORD"."FK_CORD_CBU_LIC_STATUS" IS 'Code for license status referenced from Code List(ER_CODELST)';
 
   COMMENT ON COLUMN "CB_CORD"."CREATOR" IS 'This column is used for Audit Trail. The Creator identifies the user who created this row. ';
 
   COMMENT ON COLUMN "CB_CORD"."CREATED_ON" IS 'This column is used for Audit Trail. Stores the date on which this row was created.';
 
   COMMENT ON COLUMN "CB_CORD"."LAST_MODIFIED_BY" IS 'This column is used for Audit Trail. The last_modified_by identifies the user who last modified this row.';
 
   COMMENT ON COLUMN "CB_CORD"."LAST_MODIFIED_DATE" IS 'This column is used for Audit Trail. Stores the date on which this row was last modified. ';
 
   COMMENT ON COLUMN "CB_CORD"."IP_ADD" IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine. ';
 
   COMMENT ON COLUMN "CB_CORD"."DELETEDFLAG" IS 'This column denotes whether record is deleted. ';
 
   COMMENT ON COLUMN "CB_CORD"."RID" IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database.';
 
   COMMENT ON COLUMN "CB_CORD"."ADDITIONAL_INFO_RECQUIRED" IS 'Is addtional information required for cord';
 
   COMMENT ON COLUMN "CB_CORD"."CORD_ISBI_DIN_CODE" IS 'ISBI DIN code';
 
   COMMENT ON COLUMN "CB_CORD"."CORD_ISIT_PRODUCT_CODE" IS 'ISIT product code.';
 
   COMMENT ON COLUMN "CB_CORD"."CORD_ELIGIBLE_ADDITIONAL_INFO" IS 'Addtional information regarding the eligibility of cord.';
 
   COMMENT ON COLUMN "CB_CORD"."FK_CBB_ID" IS 'Reference to CBB';
 
   COMMENT ON COLUMN "CB_CORD"."FK_CORD_CBU_ELIGIBLE_STATUS" IS 'Code for eligible status referenced from Code List(ER_CODELST)';
 
   COMMENT ON COLUMN "CB_CORD"."FK_CORD_CBU_STATUS" IS 'Code for CBU status referenced from Code List(ER_CODELST)';
 
   COMMENT ON COLUMN "CB_CORD"."FK_CORD_CBU_INELIGIBLE_REASON" IS 'Code for ineligible reason referenced from Code List(ER_CODELST)';
 
   COMMENT ON COLUMN "CB_CORD"."FK_CORD_CBU_UNLICENSED_REASON" IS 'Code for unlicensed reason referenced from Code List(ER_CODELST)';
 
   COMMENT ON COLUMN "CB_CORD"."FK_CODELST_EXTERNAL_NAME " IS 'Code for customer-preferred name referenced from Code List(ER_CODELST)';
 
   COMMENT ON COLUMN "CB_CORD"."FK_CODELST_EXTERNAL_NUM" IS 'Code for customer-preferred number referenced from Code List(ER_CODELST)';
 
   COMMENT ON COLUMN "CB_CORD"."FK_CODELST_EXTERNAL " IS 'Code for customer referenced from Code List(ER_CODELST)';
 
   COMMENT ON COLUMN "CB_CORD"."FK_CODE_OTHER" IS 'Code for Other referenced from Code List(ER_CODELST)';
 
   COMMENT ON COLUMN "CB_CORD"."CORD_CLINICAL_STATUS_DATE" IS 'Clinical Status Date';
 
   COMMENT ON COLUMN "CB_CORD"."CORD_CLINICAL_STATUS_ADDI_INFO" IS 'Cord Clinical Status additional comments';
 
   COMMENT ON COLUMN "CB_CORD"."FK_SPECIMEN_ID" IS 'Reference to Specimen ID';
 
----------------------------CB CORD Table Ends Here--------------------------------------------------

----------------------------CB CORD Table Sequence-------------------------------------

   CREATE SEQUENCE  "SEQ_CB_CORD"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;

----------------------------CB CORD Table Sequence Ends Here----------------------------


----------------------------CB HLA Table--------------------------------------------------


  CREATE TABLE "CB_HLA" 
   (    "PK_HLA" NUMBER(10,0) PRIMARY KEY, 
    "FK_HLA_CODE_ID" NUMBER(10,0), 
    "FK_HLA_METHOD_ID" NUMBER(10,0), 
    "HLA_VALUE_TYPE1" VARCHAR2(20 BYTE), 
    "HLA_VALUE_TYPE2" VARCHAR2(20 BYTE), 
    "CREATOR" NUMBER(10,0), 
    "CREATED_ON" DATE, 
    "LAST_MODIFIED_BY" NUMBER(10,0), 
    "LAST_MODIFIED_DATE" DATE, 
    "IP_ADD" VARCHAR2(15 BYTE), 
    "DELETEDFLAG" NUMBER(22,0), 
    "RID" NUMBER(15,0), 
    "ENTITY_ID" NUMBER(10,0), 
    "ENTITY_TYPE" NUMBER(20,0)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "ERES_USER" ;
 

   COMMENT ON TABLE "CB_HLA"  IS 'This table stores HLA records.';

   COMMENT ON COLUMN "CB_HLA"."PK_HLA" IS 'Primary Key';
 
   COMMENT ON COLUMN "CB_HLA"."FK_HLA_CODE_ID" IS 'Code Id for HLA';
 
   COMMENT ON COLUMN "CB_HLA"."FK_HLA_METHOD_ID" IS 'Method ID from Code List for HLA';
 
   COMMENT ON COLUMN "CB_HLA"."HLA_VALUE_TYPE1" IS 'Value for type1 HLA';
 
   COMMENT ON COLUMN "CB_HLA"."HLA_VALUE_TYPE2" IS 'Value for type2 HLA';
 
   COMMENT ON COLUMN "CB_HLA"."CREATOR" IS 'This column is used for Audit Trail. The Creator identifies the user who created this row.';
 
   COMMENT ON COLUMN "CB_HLA"."CREATED_ON" IS 'This column is used for Audit Trail. Stores the date on which this row was created.';
 
   COMMENT ON COLUMN "CB_HLA"."LAST_MODIFIED_BY" IS 'This column is used for Audit Trail. The last_modified_by identifies the user who last modified this row.';
 
   COMMENT ON COLUMN "CB_HLA"."LAST_MODIFIED_DATE" IS 'This column is used for Audit Trail. Stores the date on which this row was last modified.';
 
   COMMENT ON COLUMN "CB_HLA"."IP_ADD" IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine.';
 
   COMMENT ON COLUMN "CB_HLA"."DELETEDFLAG" IS 'This column denotes whether record is deleted. ';
 
   COMMENT ON COLUMN "CB_HLA"."RID" IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database.';
 
   COMMENT ON COLUMN "CB_HLA"."ENTITY_ID" IS 'Id of the entity like CBU, Doner etc';

----------------------------CB HLA Table Ends Here--------------------------------------------------

----------------------------CB HLA Table Sequence-------------------------------------


   CREATE SEQUENCE  "SEQ_CB_HLA"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
 
------------------------CB HLA Table Sequence Ends Here----------------------------

----------------------------CB USER ALERTS Table----------------------------------------------------

 CREATE TABLE "CB_USER_ALERTS" 
   (    "PK_ALERT" NUMBER(10,0) PRIMARY KEY, 
    "FK_USER" NUMBER(10,0), 
    "FK_CODELST_ALERT" NUMBER(10,0), 
    "CREATOR" NUMBER(10,0), 
    "CREATED_ON" DATE, 
    "LAST_MODIFIED_BY" NUMBER(10,0), 
    "LAST_MODIFIED_DATE" DATE, 
    "IP_ADD" VARCHAR2(15 BYTE), 
    "DELETEDFLAG" VARCHAR2(1 BYTE), 
    "RID" NUMBER(10,0) 
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "ERES_USER" ;
 

   COMMENT ON TABLE "CB_USER_ALERTS"  IS 'This table stores user alerts.';

   COMMENT ON COLUMN "CB_USER_ALERTS"."PK_ALERT" IS 'Primary Key';
 
   COMMENT ON COLUMN "CB_USER_ALERTS"."FK_USER" IS 'User ID for which alerts to be defined';
 
   COMMENT ON COLUMN "CB_USER_ALERTS"."FK_CODELST_ALERT" IS 'Code List reference for Alert ( Alerts are stored in ER_CODELST)';
 
   COMMENT ON COLUMN "CB_USER_ALERTS"."CREATOR" IS 'This column is used for Audit Trail. The Creator identifies the user who created this row.  ';
 
   COMMENT ON COLUMN "CB_USER_ALERTS"."CREATED_ON" IS 'This column is used for Audit Trail. Stores the date on which this row was created.';
 
   COMMENT ON COLUMN "CB_USER_ALERTS"."LAST_MODIFIED_BY" IS 'This column is used for Audit Trail. The last_modified_by identifies the user who last modified this row. ';
 
   COMMENT ON COLUMN "CB_USER_ALERTS"."LAST_MODIFIED_DATE" IS 'This column is used for Audit Trail. Stores the date on which this row was last modified.';
 
   COMMENT ON COLUMN "CB_USER_ALERTS"."IP_ADD" IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine. ';
 
   COMMENT ON COLUMN "CB_USER_ALERTS"."DELETEDFLAG" IS 'This column denotes whether record is deleted. ';
 
   COMMENT ON COLUMN "CB_USER_ALERTS"."RID" IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database.';

----------------------------CB USER ALERTS Table Ends Here----------------------------------------------------

----------------------------CB USER ALERTS Table Sequence-------------------------------------------------------------

   CREATE SEQUENCE  "SEQ_CB_USER_ALERTS"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
 
----------------------------CB USER ALERTS Table Sequence Ends Here------------------------------------------------------------

----------------------------CBB Table-----------------------------------------------------


  CREATE TABLE "CBB" 
   (	"PK_CBB" NUMBER(10,0) PRIMARY KEY, 
	"CBB_ID" VARCHAR2(20 BYTE), 
	"CREATOR" NUMBER(10,0), 
	"CREATED_ON" DATE, 
	"LAST_MODIFIED_BY" NUMBER(10,0), 
	"LAST_MODIFIED_DATE" DATE, 
	"IP_ADD" VARCHAR2(15 BYTE), 
	"DELETEDFLAG" VARCHAR2(1 BYTE), 
	"RID" NUMBER(10,0), 
	"FK_PERSON" NUMBER, 
	"FK_ADD" NUMBER(38,0), 
	"FK_SITE" NUMBER(10,0), 
	"ENABLE_EDIT_ANTIGENS" VARCHAR2(1 BYTE), 
	"DISPLAY_MATERNAL_ANTIGENS" VARCHAR2(1 BYTE), 
	"ENABLE_CBU_INVENT_FEATURE" VARCHAR2(1 BYTE), 
	"DEFAULT_CT_SHIPPER" VARCHAR2(10 BYTE), 
	"DEFAULT_OR_SHIPPER" VARCHAR2(10 BYTE), 
	"MICRO_CONTAM_DATE" VARCHAR2(1 BYTE), 
	"CFU_TEST_DATE" VARCHAR2(1 BYTE), 
	"VIAB_TEST_DATE" VARCHAR2(1 BYTE), 
	"ENABLE_CORD_INFO_LOCK" VARCHAR2(1 BYTE), 
	"DATE_FORMAT" VARCHAR2(20 BYTE)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "ERES_USER" ;
 

   COMMENT ON TABLE "CBB"  IS 'This table stores Cord Blood Bank records.';

   COMMENT ON COLUMN "CBB"."PK_CBB" IS 'Primary Key';
 
   COMMENT ON COLUMN "CBB"."CREATOR" IS 'This column is used for Audit Trail. The Creator identifies the user who created this row. ';
 
   COMMENT ON COLUMN "CBB"."CREATED_ON" IS 'This column is used for Audit Trail. Stores the date on which this row was created.';
 
   COMMENT ON COLUMN "CBB"."LAST_MODIFIED_BY" IS 'This column is used for Audit Trail. The last_modified_by identifies the user who last modified this row. ';
 
   COMMENT ON COLUMN "CBB"."LAST_MODIFIED_DATE" IS 'This column is used for Audit Trail. Stores the date on which this row was last modified.';
 
   COMMENT ON COLUMN "CBB"."IP_ADD" IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine. ';
 
   COMMENT ON COLUMN "CBB"."DELETEDFLAG" IS 'This column denotes whether record is deleted. ';
 
   COMMENT ON COLUMN "CBB"."RID" IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database. ';
 
   COMMENT ON COLUMN "CBB"."FK_PERSON" IS 'Reference to Person.';
 
   COMMENT ON COLUMN "CBB"."FK_ADD" IS 'Reference to ER_ADD';
 
   COMMENT ON COLUMN "CBB"."FK_SITE" IS 'Reference to ER_SITE';
 
   COMMENT ON COLUMN "CBB"."ENABLE_EDIT_ANTIGENS" IS 'Flag to Enable Antigen Edit';
 
   COMMENT ON COLUMN "CBB"."DISPLAY_MATERNAL_ANTIGENS" IS 'Flag to display maternal antigen';
 
   COMMENT ON COLUMN "CBB"."ENABLE_CBU_INVENT_FEATURE" IS 'Flag to enable Inventory feature';
 
   COMMENT ON COLUMN "CBB"."DEFAULT_CT_SHIPPER" IS 'Default CT Shipper info ( Fadex. UPS)';
 
   COMMENT ON COLUMN "CBB"."DEFAULT_OR_SHIPPER" IS 'Default OR Shipper info ( Fadex. UPS)';
 
   COMMENT ON COLUMN "CBB"."CFU_TEST_DATE" IS 'CFU Test Date';
 
   COMMENT ON COLUMN "CBB"."VIAB_TEST_DATE" IS 'VIAB Test Date';
 
   COMMENT ON COLUMN "CBB"."ENABLE_CORD_INFO_LOCK" IS 'Flag to Enable Cord Info Lock';
 
   COMMENT ON COLUMN "CBB"."DATE_FORMAT" IS 'Default Date Format';
 
----------------------------CBB Table Ends Here-----------------------------------------------------

----------------------------CBB PROCESSING PROCEDURES Table-----------------------------------------------------

 CREATE TABLE "CBB_PROCESSING_PROCEDURES" 
   (	"PK_PROC" NUMBER(19,0) PRIMARY KEY, 
	"CREATOR" NUMBER(19,0), 
	"CREATED_ON" DATE, 
	"IP_ADD" VARCHAR2(255 CHAR), 
	"LAST_MODIFIED_BY" NUMBER(19,0), 
	"LAST_MODIFIED_DATE" DATE, 
	"PROC_NO" FLOAT(126), 
	"PROC_START_DATE" DATE, 
	"PROC_TERMI_DATE" DATE, 
	"PROC_VERSION" VARCHAR2(255 CHAR), 
	"RID" NUMBER(10,0), 
	"FK_PROCESSING_ID" NUMBER(10,0), 
	"FK_SITE" NUMBER(10,0)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "ERES_USER" ;
 

   COMMENT ON TABLE "CBB_PROCESSING_PROCEDURES"  IS 'This table stores CBB processing procedures.';

   COMMENT ON COLUMN "CBB_PROCESSING_PROCEDURES"."PK_PROC" IS 'Primary Key';
 
   COMMENT ON COLUMN "CBB_PROCESSING_PROCEDURES"."CREATOR" IS 'This column is used for Audit Trail. The Creator identifies the user who created this row. ';
 
   COMMENT ON COLUMN "CBB_PROCESSING_PROCEDURES"."CREATED_ON" IS 'This column is used for Audit Trail. Stores the date on which this row was created.';
 
   COMMENT ON COLUMN "CBB_PROCESSING_PROCEDURES"."IP_ADD" IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine.';
 
   COMMENT ON COLUMN "CBB_PROCESSING_PROCEDURES"."LAST_MODIFIED_BY" IS 'This column is used for Audit Trail. Stores the date on which this row was last modified.';
 
   COMMENT ON COLUMN "CBB_PROCESSING_PROCEDURES"."LAST_MODIFIED_DATE" IS 'This column is used for Audit Trail. Stores the date on which this row was last modified.';
 
   COMMENT ON COLUMN "CBB_PROCESSING_PROCEDURES"."PROC_NO" IS 'Procedure number';
 
   COMMENT ON COLUMN "CBB_PROCESSING_PROCEDURES"."PROC_START_DATE" IS 'Procedure start date';
 
   COMMENT ON COLUMN "CBB_PROCESSING_PROCEDURES"."PROC_TERMI_DATE" IS 'Procedure End date';
 
   COMMENT ON COLUMN "CBB_PROCESSING_PROCEDURES"."PROC_VERSION" IS 'Version of the procedure';
 
   COMMENT ON COLUMN "CBB_PROCESSING_PROCEDURES"."RID" IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database. ';
 
   COMMENT ON COLUMN "CBB_PROCESSING_PROCEDURES"."FK_PROCESSING_ID" IS 'Reference to CBB_PROCESSING PROCEDURES';
 
   COMMENT ON COLUMN "CBB_PROCESSING_PROCEDURES"."FK_SITE" IS 'Reference to ER_SITE';
 
----------------------------CBB PROCESSING PROCEDURES Table Ends Here-----------------------------------------------------

------------------------------CBB PROCESSING PROCEDURES Table Sequence---------------------------------------------

   CREATE SEQUENCE  "SEQ_CBB_PROCESSING_PROCEDURES"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
 
------------------------------CBB PROCESSING PROCEDURES Table Sequence Ends Here---------------------------------------------


----------------------------CBB PROCESSING PROCEDURES INFO Table-----------------------------------------------------

 CREATE TABLE "CBB_PROCESSING_PROCEDURES_INFO" 
   (	"PK_PROC_INFO" NUMBER(19,0) PRIMARY KEY, 
	"CREATOR" NUMBER(19,0), 
	"CREATED_ON" DATE, 
	"IP_ADD" VARCHAR2(255 CHAR), 
	"LAST_MODIFIED_BY" NUMBER(19,0), 
	"LAST_MODIFIED_DATE" DATE, 
	"ACD" NUMBER(19,0), 
	"CPD" NUMBER(19,0), 
	"CPDA" NUMBER(19,0), 
	"CRYOBAG_MANUFAC" VARCHAR2(255 CHAR), 
	"FILTER_PAPER" NUMBER(19,0), 
	"FIVE_PER_DEXTROSE" NUMBER(19,0), 
	"FIVE_PER_HUMAN_ALBU" NUMBER(19,0), 
	"FIVE_UNIT_PER_ML_HEPARIN" NUMBER(19,0), 
	"FK_BAG_TYPE" NUMBER(19,0), 
	"FK_CONTRL_RATE_FREEZING" NUMBER(19,0), 
	"FK_FREEZ_MANUFAC" NUMBER(19,0), 
	"FK_FROZEN_IN" NUMBER(19,0), 
	"FK_IF_AUTOMATED" NUMBER(19,0), 
	"FK_PROC_METH_ID" NUMBER(19,0), 
	"FK_PROCESSING_ID" NUMBER(19,0), 
	"FK_PRODUCT_MODIFICATION" NUMBER(19,0), 
	"FK_STOR_METHOD" NUMBER(19,0), 
	"FK_STOR_TEMP" NUMBER(19,0), 
	"HUN_PER_DMSO" NUMBER(19,0), 
	"HUN_PER_GLYCEROL" NUMBER(19,0), 
	"MAX_VALUE" NUMBER(19,0), 
	"NO_OF_CELL_MATER_ALIQUOTS" NUMBER(19,0), 
	"NO_OF_EXTR_DNA_ALIQUOTS" NUMBER(19,0), 
	"NO_OF_EXTR_DNA_MATER_ALIQUOTS" NUMBER(19,0), 
	"NO_OF_INDI_FRAC" NUMBER(19,0), 
	"NO_OF_NONVIABLE_ALIQUOTS" NUMBER(19,0), 
	"NO_OF_PLASMA_ALIQUOTS" NUMBER(19,0), 
	"NO_OF_PLASMA_MATER_ALIQUOTS" NUMBER(19,0), 
	"NO_OF_SEGMENTS" NUMBER(19,0), 
	"NO_OF_SERUM_ALIQUOTS" NUMBER(19,0), 
	"NO_OF_SERUM_MATER_ALIQUOTS" NUMBER(19,0), 
	"NO_OF_VIABLE_CELL_ALIQUOTS" NUMBER(19,0), 
	"OTH_CRYOPROTECTANT" NUMBER(19,0), 
	"OTH_DILUENTS" NUMBER(19,0), 
	"OTHER_ANTICOAGULANT" NUMBER(19,0), 
	"OTHER_FREEZ_MANUFAC" VARCHAR2(255 CHAR), 
	"OTHER_FROZEN_CONT" VARCHAR2(255 CHAR), 
	"OTHER_PROCESSING" VARCHAR2(255 CHAR), 
	"OTHER_PROD_MODI" VARCHAR2(255 CHAR), 
	"PLASMALYTE" NUMBER(19,0), 
	"POINNT_NINE_PER_NACL" NUMBER(19,0), 
	"RBC_PALLETS" NUMBER(19,0), 
	"SIX_PER_HYDROXYETHYL_STARCH" NUMBER(19,0), 
	"SOP_REF_NO" VARCHAR2(255 CHAR), 
	"SOP_STRT_DATE" DATE, 
	"SOP_TERMI_DATE" DATE, 
	"SOP_TITLE" VARCHAR2(255 CHAR), 
	"SPEC_OTH_CRYOPRO" VARCHAR2(255 CHAR), 
	"SPEC_OTH_DILUENTS" VARCHAR2(255 CHAR), 
	"SPECIFY_OTH_ANTI" VARCHAR2(255 CHAR), 
	"TEN_PER_DEXTRAN_40" NUMBER(19,0), 
	"TEN_UNIT_PER_ML_HEPARIN" NUMBER(19,0), 
	"THOU_UNIT_PER_ML_HEPARIN" NUMBER(19,0), 
	"TOT_CBU_ALIQUOTS" NUMBER(19,0), 
	"TOT_MATER_ALIQUOTS" NUMBER(19,0), 
	"TWEN_FIVE_HUM_ALBU" NUMBER(19,0), 
	"RID" NUMBER(10,0), 
	 CONSTRAINT "FKBF93AE5D7B93058F" FOREIGN KEY ("FK_PROCESSING_ID")
	  REFERENCES "CBB_PROCESSING_PROCEDURES" ("PK_PROC") ENABLE
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "ERES_USER" ;
 

   COMMENT ON TABLE "CBB_PROCESSING_PROCEDURES"  IS 'This table stores CBB processing procedures info records.';

   COMMENT ON COLUMN "CBB_PROCESSING_PROCEDURES_INFO"."PK_PROC_INFO" IS 'Primary Key';
 
   COMMENT ON COLUMN "CBB_PROCESSING_PROCEDURES_INFO"."CREATOR" IS 'This column is used for Audit Trail. The Creator identifies the user who created this row.  ';
 
   COMMENT ON COLUMN "CBB_PROCESSING_PROCEDURES_INFO"."CREATED_ON" IS 'This column is used for Audit Trail. Stores the date on which this row was created.';
 
   COMMENT ON COLUMN "CBB_PROCESSING_PROCEDURES_INFO"."IP_ADD" IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine. ';
 
   COMMENT ON COLUMN "CBB_PROCESSING_PROCEDURES_INFO"."LAST_MODIFIED_BY" IS 'This column is used for Audit Trail. The last_modified_by identifies the user who last modified this row. ';
 
   COMMENT ON COLUMN "CBB_PROCESSING_PROCEDURES_INFO"."LAST_MODIFIED_DATE" IS 'This column is used for Audit Trail. Stores the date on which this row was last modified.';
 
   COMMENT ON COLUMN "CBB_PROCESSING_PROCEDURES_INFO"."FK_BAG_TYPE" IS 'Code Id for type of bag.';
 
   COMMENT ON COLUMN "CBB_PROCESSING_PROCEDURES_INFO"."FK_CONTRL_RATE_FREEZING" IS 'Code Id for control rate freezing.';
 
   COMMENT ON COLUMN "CBB_PROCESSING_PROCEDURES_INFO"."FK_FREEZ_MANUFAC" IS 'Code Id for freezer Manufracture.';
 
   COMMENT ON COLUMN "CBB_PROCESSING_PROCEDURES_INFO"."FK_FROZEN_IN" IS 'Code Id for frozen in.';
 
   COMMENT ON COLUMN "CBB_PROCESSING_PROCEDURES_INFO"."FK_IF_AUTOMATED" IS 'Code id for if automated.';
 
   COMMENT ON COLUMN "CBB_PROCESSING_PROCEDURES_INFO"."FK_PROCESSING_ID" IS 'Reference of CBB_PROCESSING_PROCEDURES';
 
   COMMENT ON COLUMN "CBB_PROCESSING_PROCEDURES_INFO"."FK_PRODUCT_MODIFICATION" IS 'Code Id for product modification.';
 
   COMMENT ON COLUMN "CBB_PROCESSING_PROCEDURES_INFO"."FK_STOR_METHOD" IS 'Code Id for storage method';
 
   COMMENT ON COLUMN "CBB_PROCESSING_PROCEDURES_INFO"."FK_STOR_TEMP" IS 'Code Id for storage temperature';
 
   COMMENT ON COLUMN "CBB_PROCESSING_PROCEDURES_INFO"."RID" IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database.';

----------------------------CBB PROCESSING PROCEDURES INFO Table Ends Here-----------------------------------------------------

---------------------------CBB PROCESSING PROCED INFO Table Sequence------------------------------

   CREATE SEQUENCE  "SEQ_CBB_PROCESSING_PROCED_INFO"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;

---------------------------CBB PROCESSING PROCED INFO Table Sequence Ends Here--------------------------------------


----------------------------CBU UNIT REPORT Table-------------------------------------------------------


  CREATE TABLE "CBU_UNIT_REPORT" 
   (
	"PK_CBU_UNIT_REPORT" NUMBER(10,0) PRIMARY KEY, 
	"FK_CORD_CDR_CBU_ID" VARCHAR2(10 BYTE), 
	"FK_CORD_PROC_METHOD" NUMBER(10,0), 
	"FK_CORD_TYPE_BAG" NUMBER(10,0), 
	"FK_CORD_PROD_MODIFICATION" NUMBER(10,0), 
	"FK_CORD_HEMOG_SCREEN" NUMBER(10,0), 
	"FK_CORD_BACTERIAL_CUL" NUMBER(10,0), 
	"FK_CORD_FUNGAL_CUL" NUMBER(10,0), 
	"CORD_EXPLAIN_NOTES" VARCHAR2(2000 BYTE), 
	"CORD_ADD_NOTES" VARCHAR2(2000 BYTE), 
	"CREATOR" NUMBER(10,0), 
	"CREATED_ON" DATE, 
	"LAST_MODIFIED_BY" NUMBER(10,0), 
	"LAST_MODIFIED_DATE" DATE, 
	"IP_ADD" VARCHAR2(15 BYTE), 
	"DELETEDFLAG" VARCHAR2(1 BYTE), 
	"RID" NUMBER(10,0), 
	"CORD_VIAB_POST_PROCESS" NUMBER(10,0), 
	"FK_CORD_VIAB_METHOD" NUMBER(10,0), 
	"CORD_VIAB_POST_TEST_DATE" DATE, 
	"CORD_VIAB_POST_STATUS" VARCHAR2(1 BYTE), 
	"CORD_VIAB_POST_QUES1" VARCHAR2(1 BYTE), 
	"CORD_VIAB_POST_QUES2" VARCHAR2(1 BYTE), 
	"CORD_CFU_POST_PROCESS_COUNT" NUMBER(10,0), 
	"CORD_CFU_POST_METHOD" NUMBER(10,0), 
	"CORD_CFU_POST_STATUS" VARCHAR2(1 BYTE), 
	"CORD_CFU_POST_TEST_DATE" DATE, 
	"CORD_CFU_POST_QUES1" VARCHAR2(1 BYTE), 
	"CORD_CFU_POST_QUES2" VARCHAR2(1 BYTE), 
	"CORD_CBU_POST_QUES2" VARCHAR2(1 BYTE), 
	"CORD_CBU_POST_QUES1" VARCHAR2(1 BYTE), 
	"CORD_CBU_POST_TEST_DATE" DATE, 
	"CORD_CBU_POST_PROCESS_COUNT" NUMBER(10,0), 
	"CORD_CBU_POST_STATUS" VARCHAR2(1 BYTE), 
	"CORD_COMPLETED_BY" VARCHAR2(100 BYTE), 
	"CORD_COMPLETED_STATUS" NUMBER(10,0), 
	"CORD_SERIAL" NUMBER(10,0), 
	"FK_ATTACHMENT" NUMBER(30,0)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "ERES_USER" ;
 

   COMMENT ON TABLE "CBU_UNIT_REPORT"  IS 'This table stores Cord Blood Unit report.';

   COMMENT ON COLUMN "CBU_UNIT_REPORT"."PK_CBU_UNIT_REPORT" IS 'Primary Key';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT"."FK_CORD_CDR_CBU_ID" IS 'Reference to CB_CORD';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT"."FK_CORD_TYPE_BAG" IS 'Code for bag type referenced from Code List(ER_CODELST)';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT"."FK_CORD_PROD_MODIFICATION" IS 'Code for product modification referenced from Code List(ER_CODELST)';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT"."FK_CORD_HEMOG_SCREEN" IS 'Code for hemoglobinopathy screening referenced from Code List(ER_CODELST).';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT"."FK_CORD_BACTERIAL_CUL" IS 'Code for bacterial culture referenced from Code List(ER_CODELST)';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT"."FK_CORD_FUNGAL_CUL" IS 'Code for fungal culture referenced from  Code List(ER_CODELST)';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT"."CORD_EXPLAIN_NOTES" IS 'Explain note for cord.';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT"."CORD_ADD_NOTES" IS 'Addtional notes for cord.';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT"."CREATOR" IS 'This column is used for Audit Trail. The Creator identifies the user who created this row. ';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT"."CREATED_ON" IS 'This column is used for Audit Trail. Stores the date on which this row was created.';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT"."LAST_MODIFIED_BY" IS 'This column is used for Audit Trail. The last_modified_by identifies the user who last modified this row. ';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT"."LAST_MODIFIED_DATE" IS 'This column is used for Audit Trail. Stores the date on which this row was last modified.';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT"."IP_ADD" IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine.';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT"."DELETEDFLAG" IS 'This column denotes whether record is deleted. ';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT"."RID" IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database. ';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT"."FK_CORD_VIAB_METHOD" IS 'Code for VIAB method  referenced from  Code List(ER_CODELST)';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT"."CORD_VIAB_POST_TEST_DATE" IS 'Test date for VIAB';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT"."CORD_VIAB_POST_STATUS" IS 'Status of VIAB test for cord .';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT"."CORD_VIAB_POST_QUES1" IS 'Question associated with the VIAB test.';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT"."CORD_VIAB_POST_QUES2" IS 'Question associated with the VIAB test.';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT"."CORD_CFU_POST_PROCESS_COUNT" IS 'Count for CFU.';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT"."CORD_CFU_POST_METHOD" IS 'Code for CFU method  referenced from  Code List(ER_CODELST)';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT"."CORD_CFU_POST_STATUS" IS 'Status of CFU test for CORD.';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT"."CORD_CFU_POST_TEST_DATE" IS 'Test date for VIAB';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT"."CORD_CFU_POST_QUES1" IS 'Question associated with the CFU test.';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT"."CORD_CFU_POST_QUES2" IS 'Question associated with the CFU test.';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT"."CORD_CBU_POST_QUES2" IS 'Question associated with the CBU test.';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT"."CORD_CBU_POST_QUES1" IS 'Question associated with the CBU test.';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT"."CORD_CBU_POST_TEST_DATE" IS 'Test date for CBU';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT"."CORD_CBU_POST_PROCESS_COUNT" IS 'Count for CBU.';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT"."CORD_CBU_POST_STATUS" IS 'Status of CBU test for cord .';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT"."CORD_COMPLETED_BY" IS 'This column stores the information of user who has completed the unit report.';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT"."CORD_COMPLETED_STATUS" IS 'This column stores the status of the unit report(completed or pending).';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT"."FK_ATTACHMENT" IS 'Reference to Attachement( CB_Attachment).';

----------------------------CBU UNIT REPORT Table Ends Here-----------------------------------------------------

---------------------------CBU UNIT REPORT Table Sequence----------------------------------------

   CREATE SEQUENCE  "SEQ_CBU_UNIT_REPORT"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
 
---------------------------CBU UNIT REPORT Table Sequence Ends Here----------------------------------------

----------------------------CBU UNIT REPORT HLA Table------------------------------------------------------


  CREATE TABLE "CBU_UNIT_REPORT_HLA" 
   (	"PK_CBU_UNIT_REPORT_HLA" NUMBER(10,0) PRIMARY KEY, 
	"FK_CORD_CDR_CBU_ID" VARCHAR2(10 BYTE), 
	"CORD_HLA_TWICE" VARCHAR2(1 BYTE), 
	"CORD_HLA_SECOND_LAB" VARCHAR2(1 BYTE), 
	"CORD_HLA_CONTI_SEG" VARCHAR2(1 BYTE), 
	"CORD_HLA_CONTI_SEG_BEF_REL" VARCHAR2(1 BYTE), 
	"CORD_HLA_IND_BEF_REL" VARCHAR2(1 BYTE), 
	"CREATOR" NUMBER(10,0), 
	"CREATED_ON" DATE, 
	"LAST_MODIFIED_BY" NUMBER(10,0), 
	"LAST_MODIFIED_DATE" DATE, 
	"IP_ADD" VARCHAR2(15 BYTE), 
	"DELETEDFLAG" VARCHAR2(1 BYTE), 
	"RID" NUMBER(10,0), 
	"CORD_HLA_CONTI_SEG_NUM" NUMBER(10,0), 
	"CORD_IND_HLA_SAMPLES" NUMBER(1,0), 
	"FK_CORD_EXT_INFO" NUMBER(10,0)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "ERES_USER" ;
 

   COMMENT ON TABLE "CBU_UNIT_REPORT_HLA"  IS 'This table stores Cord Blood Unit Report HLA.';

   COMMENT ON COLUMN "CBU_UNIT_REPORT_HLA"."PK_CBU_UNIT_REPORT_HLA" IS 'Primary Key';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_HLA"."CORD_HLA_TWICE" IS 'Question related with HLA';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_HLA"."CORD_HLA_SECOND_LAB" IS 'Question related with HLA';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_HLA"."CORD_HLA_CONTI_SEG" IS 'Question related with HLA';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_HLA"."CORD_HLA_CONTI_SEG_BEF_REL" IS 'Question related with HLA';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_HLA"."CORD_HLA_IND_BEF_REL" IS 'Question related with HLA';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_HLA"."CREATOR" IS 'This column is used for Audit Trail. The Creator identifies the user who created this row. ';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_HLA"."CREATED_ON" IS 'This column is used for Audit Trail. Stores the date on which this row was created.';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_HLA"."LAST_MODIFIED_BY" IS 'This column is used for Audit Trail. The last_modified_by identifies the user who last modified this row. ';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_HLA"."LAST_MODIFIED_DATE" IS 'This column is used for Audit Trail. Stores the date on which this row was last modified.';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_HLA"."IP_ADD" IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine. ';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_HLA"."DELETEDFLAG" IS 'This column denotes whether record is deleted. ';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_HLA"."RID" IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database.';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_HLA"."CORD_HLA_CONTI_SEG_NUM" IS 'Question associated with HLA.';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_HLA"."CORD_IND_HLA_SAMPLES" IS 'Question related with HLA';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_HLA"."FK_CORD_EXT_INFO" IS 'Reference to CBU_UNIT_REPORT';


----------------------------CBU UNIT REPORT HLA Table Ends Here------------------------------------------------------

-------------------------CBU UNIT REPORT HLA Table Sequence---------------------------------


   CREATE SEQUENCE  "SEQ_CBU_UNIT_REPORT_HLA"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
 
---------------------------CBU UNIT REPORT HLA Table Sequence Ends Here---------------------

----------------------------CBU UNIT REPORT HLA REVIEW Table --------------------------------------------------------


CREATE TABLE "CBU_UNIT_REPORT_HLA_REVIEW" 
   (	"PK_CBU_UNIT_REPORT_HLA_REVIEW" NUMBER(10,0) PRIMARY KEY, 
	"FK_CORD_CDR_CBU_ID" VARCHAR2(10 BYTE), 
	"CORD_HLA_TWICE" VARCHAR2(1 BYTE), 
	"CORD_HLA_SECOND_LAB" VARCHAR2(1 BYTE), 
	"CORD_HLA_CONTI_SEG" VARCHAR2(1 BYTE), 
	"CORD_HLA_CONTI_SEG_BEF_REL" VARCHAR2(1 BYTE), 
	"CORD_HLA_IND_BEF_REL" VARCHAR2(1 BYTE), 
	"CREATOR" NUMBER(10,0), 
	"CREATED_ON" DATE, 
	"LAST_MODIFIED_BY" NUMBER(10,0), 
	"LAST_MODIFIED_DATE" DATE, 
	"IP_ADD" VARCHAR2(15 BYTE), 
	"DELETEDFLAG" VARCHAR2(1 BYTE), 
	"RID" NUMBER(10,0), 
	"CORD_HLA_CONTI_SEG_NUM" NUMBER(10,0), 
	"FK_CORD_HLA_EXT" NUMBER(10,0), 
	"CORD_IND_HLA_SAMPLES" NUMBER(1,0), 
	"FK_CORD_EXT_INFO_TEMP" NUMBER(10,0)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "ERES_USER" ;
 

   COMMENT ON TABLE "CBU_UNIT_REPORT_HLA"  IS 'This table stores Cord Blood Unit Report HLA for review purposes.';

   COMMENT ON COLUMN "CBU_UNIT_REPORT_HLA_REVIEW"."PK_CBU_UNIT_REPORT_HLA_REVIEW" IS 'Primary Key';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_HLA_REVIEW"."CORD_HLA_TWICE" IS 'Question related with HLA';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_HLA_REVIEW"."CORD_HLA_SECOND_LAB" IS 'Question related with HLA';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_HLA_REVIEW"."CORD_HLA_CONTI_SEG" IS 'Question related with HLA';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_HLA_REVIEW"."CORD_HLA_CONTI_SEG_BEF_REL" IS 'Question related with HLA';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_HLA_REVIEW"."CORD_HLA_IND_BEF_REL" IS 'Question related with HLA';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_HLA_REVIEW"."CREATOR" IS 'This column is used for Audit Trail. The Creator identifies the user who created this row. ';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_HLA_REVIEW"."CREATED_ON" IS 'This column is used for Audit Trail. Stores the date on which this row was created.';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_HLA_REVIEW"."LAST_MODIFIED_BY" IS 'This column is used for Audit Trail. The last_modified_by identifies the user who last modified this row. ';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_HLA_REVIEW"."LAST_MODIFIED_DATE" IS 'This column is used for Audit Trail. Stores the date on which this row was last modified.';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_HLA_REVIEW"."IP_ADD" IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine.';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_HLA_REVIEW"."DELETEDFLAG" IS 'This column denotes whether record is deleted. ';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_HLA_REVIEW"."RID" IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database. ';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_HLA_REVIEW"."CORD_HLA_CONTI_SEG_NUM" IS 'Question related with HLA';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_HLA_REVIEW"."FK_CORD_HLA_EXT" IS 'Reference to CBU Unit Report from CBU _UNIT_REPORT_HLA';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_HLA_REVIEW"."CORD_IND_HLA_SAMPLES" IS 'Question related with HLA';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_HLA_REVIEW"."FK_CORD_EXT_INFO_TEMP" IS 'Reference to CBU Unit Report from CBU _UNIT_REPORT';

----------------------------CBU UNIT REPORT HLA REVIEW Table --------------------------------------------------------

---------------------------CBU UNIT REPORT HLA REVIEW Table-------------------------------------


   CREATE SEQUENCE  "SEQ_CBU_UNIT_REPORT_HLA_REVIEW"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 58 NOCACHE  NOORDER  NOCYCLE ;
 
---------------------------CBU UNIT REPORT HLA REVIEW Table Ends Here----------------------------------

----------------------------CBU UNIT REPORT REVIEW Table --------------------------------------------------------

CREATE TABLE "CBU_UNIT_REPORT_REVIEW" 
   (
	"PK_CBU_UNIT_REPORT_REVIEW" NUMBER(10,0) PRIMARY KEY, 
	"FK_CORD_CDR_CBU_ID" VARCHAR2(10 BYTE), 
	"FCORD_PROC_METHOD" NUMBER(10,0), 
	"FK_CORD_TYPE_BAG" NUMBER(10,0), 
	"FK_CORD_PROD_MODIFICATION" NUMBER(10,0), 
	"FK_CORD_HEMOG_SCREEN" NUMBER(10,0), 
	"FK_CORD_BACTERIAL_CUL" NUMBER(10,0), 
	"FK_CORD_FUNGAL_CUL" NUMBER(10,0), 
	"CORD_EXPLAIN_NOTES" VARCHAR2(2000 BYTE), 
	"CORD_ADD_NOTES" VARCHAR2(2000 BYTE), 
	"CREATOR" NUMBER(10,0), 
	"CREATED_ON" DATE, 
	"LAST_MODIFIED_BY" NUMBER(10,0), 
	"LAST_MODIFIED_DATE" DATE, 
	"IP_ADD" VARCHAR2(15 BYTE), 
	"DELETEDFLAG" VARCHAR2(1 BYTE), 
	"RID" NUMBER(10,0), 
	"CORD_VIAB_POST_PROCESS" NUMBER(10,0), 
	"FK_CORD_VIAB_METHOD" NUMBER(10,0), 
	"CORD_VIAB_POST_TEST_DATE" DATE, 
	"CORD_VIAB_POST_STATUS" VARCHAR2(1 BYTE), 
	"CORD_VIAB_POST_QUES1" VARCHAR2(1 BYTE), 
	"CORD_VIAB_POST_QUES2" VARCHAR2(1 BYTE), 
	"CORD_CFU_POST_PROCESS_COUNT" NUMBER(10,0), 
	"FK_CORD_CFU_POST_METHOD" NUMBER(10,0), 
	"CORD_CFU_POST_STATUS" VARCHAR2(1 BYTE), 
	"CORD_CFU_POST_TEST_DATE" DATE, 
	"CORD_CFU_POST_QUES1" VARCHAR2(1 BYTE), 
	"CORD_CFU_POST_QUES2" VARCHAR2(1 BYTE), 
	"CORD_CBU_POST_QUES2" VARCHAR2(1 BYTE), 
	"CORD_CBU_POST_QUES1" VARCHAR2(1 BYTE), 
	"CORD_CBU_POST_TEST_DATE" DATE, 
	"CORD_CBU_POST_PROCESS_COUNT" NUMBER(10,0), 
	"CORD_CBU_POST_STATUS" VARCHAR2(1 BYTE), 
	"CORD_SERIAL" NUMBER(10,0), 
	"CORD_COMPLETED_BY" VARCHAR2(100 BYTE), 
	"CORD_COMPLETED_STATUS" NUMBER(10,0), 
	"FK_ATTACHMENT" NUMBER(30,0), 
	"FK_CORD_EXT_INFO" NUMBER(10,0)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "ERES_USER" ;
 

   COMMENT ON TABLE "CBU_UNIT_REPORT_REVIEW"  IS 'This table stores Cord Blood Unit Report for review purposes.';

   COMMENT ON COLUMN "CBU_UNIT_REPORT_REVIEW"."PK_CBU_UNIT_REPORT_REVIEW" IS 'Primary Key';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_REVIEW"."FK_CORD_CDR_CBU_ID" IS 'Reference to CB_CORD';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_REVIEW"."FK_CORD_TYPE_BAG" IS 'Code for bag type referenced from Code List(ER_CODELST)';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_REVIEW"."FK_CORD_PROD_MODIFICATION" IS 'Code for product modification referenced from Code List(ER_CODELST)';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_REVIEW"."FK_CORD_HEMOG_SCREEN" IS 'Code for hemoglobinopathy screening referenced from Code List(ER_CODELST).';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_REVIEW"."FK_CORD_BACTERIAL_CUL" IS 'Code for bacterial culture referenced from Code List(ER_CODELST)';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_REVIEW"."FK_CORD_FUNGAL_CUL" IS 'Code for fungal culture referenced from  Code List(ER_CODELST)';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_REVIEW"."CORD_EXPLAIN_NOTES" IS 'Expalin Notes for CORD.';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_REVIEW"."CORD_ADD_NOTES" IS 'Addtional notes for cord.';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_REVIEW"."CREATOR" IS 'This column is used for Audit Trail. The Creator identifies the user who created this row. ';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_REVIEW"."CREATED_ON" IS 'This column is used for Audit Trail. Stores the date on which this row was created.';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_REVIEW"."LAST_MODIFIED_BY" IS 'This column is used for Audit Trail. The last_modified_by identifies the user who last modified this row. ';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_REVIEW"."LAST_MODIFIED_DATE" IS 'This column is used for Audit Trail. Stores the date on which this row was last modified.';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_REVIEW"."IP_ADD" IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine.';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_REVIEW"."DELETEDFLAG" IS 'This column denotes whether record is deleted. ';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_REVIEW"."RID" IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database.';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_REVIEW"."FK_CORD_VIAB_METHOD" IS 'Code for VIAB method  referenced from  Code List(ER_CODELST)';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_REVIEW"."CORD_VIAB_POST_TEST_DATE" IS 'Test date for VIAB';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_REVIEW"."CORD_VIAB_POST_STATUS" IS 'Status of VIAB test for cord .';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_REVIEW"."CORD_VIAB_POST_QUES1" IS 'Question associated with the VIAB test.';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_REVIEW"."CORD_VIAB_POST_QUES2" IS 'Question associated with the VIAB test.';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_REVIEW"."CORD_CFU_POST_PROCESS_COUNT" IS 'Count for CFU.';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_REVIEW"."FK_CORD_CFU_POST_METHOD" IS 'Code for CFU method  referenced from  Code List(ER_CODELST)';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_REVIEW"."CORD_CFU_POST_STATUS" IS 'Status of CFU test for CORD.';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_REVIEW"."CORD_CFU_POST_TEST_DATE" IS 'Test date for CFU';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_REVIEW"."CORD_CFU_POST_QUES1" IS 'Question associated with the CFU test.';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_REVIEW"."CORD_CFU_POST_QUES2" IS 'Question associated with the CFU test.';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_REVIEW"."CORD_CBU_POST_QUES2" IS 'Question associated with the CBU test.';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_REVIEW"."CORD_CBU_POST_QUES1" IS 'Question associated with the CBU test.';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_REVIEW"."CORD_CBU_POST_TEST_DATE" IS 'Test date for CBU';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_REVIEW"."CORD_CBU_POST_PROCESS_COUNT" IS 'Count for CBU.';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_REVIEW"."CORD_CBU_POST_STATUS" IS 'Status of CBU test for cord .';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_REVIEW"."CORD_COMPLETED_BY" IS 'This column stores the information of user who has completed the unit report.';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_REVIEW"."CORD_COMPLETED_STATUS" IS 'This column stores the status of the unit report(completed or pending). ';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_REVIEW"."FK_ATTACHMENT" IS 'Reference to CB_ATTACHMENTS.';
 
   COMMENT ON COLUMN "CBU_UNIT_REPORT_REVIEW"."FK_CORD_EXT_INFO" IS 'Reference to CBU_UNIT_REPORT';

----------------------------CBU UNIT REPORT REVIEW Table Ends Here --------------------------------------------------------

---------------------------CBU UNIT REPORT REVIEW Table Sequence-----------------------------------------


   CREATE SEQUENCE  "SEQ_CBU_UNIT_REPORT_REVIEW"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 59 NOCACHE  NOORDER  NOCYCLE ;
 
---------------------------CBU UNIT REPORT REVIEW Table Sequence Ends Here-----------------------------------------

--------------------------CB ADDITIONAL IDS Table----------------------------------

  CREATE TABLE "CB_ADDITIONAL_IDS" 
   (  "PK_CB_ADDITIONAL_IDS" NUMBER(10,0) NOT NULL ENABLE, 	
    "CB_ADDITIONAL_ID" VARCHAR2(20 BYTE), 
    "FK_CB_ADDITIONAL_ID_TYPE" NUMBER (10,0),
    "CB_ADDITIONAL_ID_DESC" VARCHAR2(100 BYTE), 	
	"CREATED_ON" DATE, 
	"CREATOR" NUMBER(10,0), 
	"IP_ADD" VARCHAR2(15 BYTE), 
	"RID" NUMBER(10,0), 
	"LAST_MODIFIED_BY" NUMBER(10,0), 
	"LAST_MODIFIED_DATE" DATE, 
	"ENTITY_ID" NUMBER(10,0), 
	"ENTITY_TYPE" NUMBER(10,0)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "ERES_USER" ;
 

   COMMENT ON TABLE "CB_ADDITIONAL_IDS"  IS 'This table stores additional IDs for cords.';

   COMMENT ON COLUMN "CB_ADDITIONAL_IDS"."CB_ADDITIONAL_ID" IS 'Addtional Id  for an entity';
   
   COMMENT ON COLUMN "CB_ADDITIONAL_IDS"."FK_CB_ADDITIONAL_ID_TYPE" IS 'Type of Addtional Id  for an entity';
 
   COMMENT ON COLUMN "CB_ADDITIONAL_IDS"."CB_ADDITIONAL_ID_DESC" IS 'Description for addtional id';
 
   COMMENT ON COLUMN "CB_ADDITIONAL_IDS"."PK_CB_ADDITIONAL_IDS" IS 'Primary Key';
 
   COMMENT ON COLUMN "CB_ADDITIONAL_IDS"."CREATED_ON" IS 'This column is used for Audit Trail. The Creator identifies the user who created this row.  ';
 
   COMMENT ON COLUMN "CB_ADDITIONAL_IDS"."CREATOR" IS 'This column is used for Audit Trail. The Creator identifies the user who created this row. ';
 
   COMMENT ON COLUMN "CB_ADDITIONAL_IDS"."IP_ADD" IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine.';
 
   COMMENT ON COLUMN "CB_ADDITIONAL_IDS"."RID" IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database.';
 
   COMMENT ON COLUMN "CB_ADDITIONAL_IDS"."LAST_MODIFIED_BY" IS 'This column is used for Audit Trail. The last_modified_by identifies the user who last modified this row.  ';
 
   COMMENT ON COLUMN "CB_ADDITIONAL_IDS"."LAST_MODIFIED_DATE" IS 'This column is used for Audit Trail. Stores the date on which this row was last modified.';
   
   COMMENT ON COLUMN "CB_ADDITIONAL_IDS"."ENTITY_ID" IS 'ID of an entity whose additional ids stored in this table';
   
   COMMENT ON COLUMN "CB_ADDITIONAL_IDS"."ENTITY_TYPE" IS 'Entity Type of an entity whose additional ids stored in this table';
 
--------------------------CB ADDTIONAL IDS Table Ends Here --------------------------------------------------

--------------------------CB ADDTIONAL IDS Table Sequence --------------------------------------------------


   CREATE SEQUENCE  "SEQ_CB_ADDTIONAL_IDS"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
 
--------------------------CB ADDTIONAL IDS Table Sequence Ends Here --------------------------------------------------

--------------------------DCMS FILE UPLOAD Sequence------------------------------------------------------


   CREATE SEQUENCE  "SEQ_DCMS_FILE_UPLOAD"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
 
--------------------------DCMS FILE UPLOAD Sequence Ends Here --------------------------------------------------



INSERT INTO track_patches
VALUES(seq_track_patches.nextval,133,8,'08_CB_Object_Creation.sql',sysdate,'9.0.0 Build#590');

commit;