set define off;

CREATE TABLE "CB_HLA_TEMP"
   (    "PK_HLA_TEMP" NUMBER(10,0) PRIMARY KEY,
    "FK_HLA_CODE_ID" NUMBER(10,0),
    "FK_HLA_METHOD_ID" NUMBER(10,0),
    "HLA_VALUE_TYPE1" VARCHAR2(20 BYTE),
    "HLA_VALUE_TYPE2" VARCHAR2(20 BYTE),
    "CREATOR" NUMBER(10,0),
    "CREATED_ON" DATE,
    "LAST_MODIFIED_BY" NUMBER(10,0),
    "LAST_MODIFIED_DATE" DATE,
    "IP_ADD" VARCHAR2(15 BYTE),
    "DELETEDFLAG" NUMBER(22,0),
    "RID" NUMBER(15,0),
    "ENTITY_ID" NUMBER(10,0),
    "ENTITY_TYPE" NUMBER(20,0),
    "FK_CORD_CDR_CBU_ID" VARCHAR2(255 CHAR),
    "FK_HLA_OVERCLS_CODE" NUMBER(10,0),
    "HLA_CRIT_OVER_DATE" DATE,
    "HLA_OVER_IND_ANTGN1" VARCHAR2(1 BYTE),
    "HLA_OVER_IND_ANTGN2" VARCHAR2(1 BYTE),
    "FK_HLA_PRIM_DNA_ID" NUMBER(10,0),
    "FK_HLA_PROT_DIPLOID" NUMBER(10,0),
    "FK_HLA_ANTIGENEID1" NUMBER(10,0),
    "FK_HLA_ANTIGENEID2" NUMBER(10,0),
    "HLA_RECEIVED_DATE" DATE,
    "HLA_TYPING_DATE" DATE,
    "FK_HLA_REPORTING_LAB" NUMBER(10,0),
    "FK_HLA_REPORT_METHOD" NUMBER(10,0),
    "FK_ORDER_ID" NUMBER(10,0),
    "FK_CORD_CT_STATUS" NUMBER(10,0),
    "FK_SPEC_TYPE" NUMBER(10,0),
    "CBU_REGISTRY_ID" NUMBER(10,0),
    "FK_SOURCE" VARCHAR2(50 CHAR));


   COMMENT ON COLUMN "CB_HLA_TEMP"."PK_HLA_TEMP" IS 'Primary Key';

   COMMENT ON COLUMN "CB_HLA_TEMP"."FK_HLA_CODE_ID" IS 'Code Id for HLA';

   COMMENT ON COLUMN "CB_HLA_TEMP"."FK_HLA_METHOD_ID" IS 'Method ID from Code List for HLA';

   COMMENT ON COLUMN "CB_HLA_TEMP"."HLA_VALUE_TYPE1" IS 'Value for type1 HLA';

   COMMENT ON COLUMN "CB_HLA_TEMP"."HLA_VALUE_TYPE2" IS 'Value for type2 HLA';

   COMMENT ON COLUMN "CB_HLA_TEMP"."CREATOR" IS 'This column is used for Audit Trail. The Creator identifies the user who created this row.';

   COMMENT ON COLUMN "CB_HLA_TEMP"."CREATED_ON" IS 'This column is used for Audit Trail. Stores the date on which this row was created.';

   COMMENT ON COLUMN "CB_HLA_TEMP"."LAST_MODIFIED_BY" IS 'This column is used for Audit Trail. The last_modified_by identifies the user who last modified this row.';

   COMMENT ON COLUMN "CB_HLA_TEMP"."LAST_MODIFIED_DATE" IS 'This column is used for Audit Trail. Stores the date on which this row was last modified.';

   COMMENT ON COLUMN "CB_HLA_TEMP"."IP_ADD" IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine.';

   COMMENT ON COLUMN "CB_HLA_TEMP"."DELETEDFLAG" IS 'This column denotes whether record is deleted. ';

   COMMENT ON COLUMN "CB_HLA_TEMP"."RID" IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database.';

   COMMENT ON COLUMN "CB_HLA_TEMP"."ENTITY_ID" IS 'Id of the entity like CBU, Doner etc';

   COMMENT ON COLUMN "CB_HLA_TEMP"."FK_HLA_OVERCLS_CODE" IS 'HLA Override class code.';

   COMMENT ON COLUMN "CB_HLA_TEMP"."HLA_CRIT_OVER_DATE" IS 'HLA Critical override date - esb mapping.';

   COMMENT ON COLUMN "CB_HLA_TEMP"."HLA_OVER_IND_ANTGN1" IS 'Indicator for override Antigen1.';

   COMMENT ON COLUMN "CB_HLA_TEMP"."HLA_OVER_IND_ANTGN2" IS 'Indicator for override Antigen2';

   COMMENT ON COLUMN "CB_HLA_TEMP"."FK_HLA_PRIM_DNA_ID" IS 'Primary DNA ID - esb message.';

   COMMENT ON COLUMN "CB_HLA_TEMP"."FK_HLA_PROT_DIPLOID" IS 'HLA ProteinDiplotypeListId - esb message.';

   COMMENT ON COLUMN "CB_HLA_TEMP"."FK_HLA_ANTIGENEID1" IS 'HLA ANTIGENEID2 - Type 2.';

   COMMENT ON COLUMN "CB_HLA_TEMP"."HLA_RECEIVED_DATE" IS 'HLA Received date.';

   COMMENT ON COLUMN "CB_HLA_TEMP"."HLA_TYPING_DATE" IS 'HLA Typing date.';

   COMMENT ON COLUMN "CB_HLA_TEMP"."FK_HLA_REPORTING_LAB" IS 'Reference for reporting LAB.';

   COMMENT ON COLUMN "CB_HLA_TEMP"."FK_HLA_REPORT_METHOD" IS 'HLA Reporting method code, reference to ER_CODELST.';

   COMMENT ON COLUMN "CB_HLA_TEMP"."FK_ORDER_ID" IS 'ORDER reference ID';

   COMMENT ON COLUMN "CB_HLA_TEMP"."FK_CORD_CT_STATUS" IS 'This column will store the fk of cord ct status table';

   COMMENT ON COLUMN "CB_HLA_TEMP"."FK_SPEC_TYPE" IS 'This field store FK of Specimen Type.';

COMMENT ON COLUMN "CB_HLA_TEMP"."CBU_REGISTRY_ID" IS 'Stores CBU Registry Id.';

   COMMENT ON COLUMN "CB_HLA_TEMP"."FK_SOURCE" IS 'This field store source of antigen.';

   COMMENT ON TABLE "CB_HLA_TEMP"  IS 'This table stores HLA Temporary records.';

   CREATE SEQUENCE SEQ_CB_HLA_TEMP MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1;

   CREATE TABLE "CB_BEST_HLA"
   (    "PK_BEST_HLA" NUMBER(10,0) PRIMARY KEY,
    "FK_HLA_CODE_ID" NUMBER(10,0),
    "FK_HLA_METHOD_ID" NUMBER(10,0),
    "HLA_VALUE_TYPE1" VARCHAR2(20 BYTE),
    "HLA_VALUE_TYPE2" VARCHAR2(20 BYTE),
    "CREATOR" NUMBER(10,0),
    "CREATED_ON" DATE,
    "LAST_MODIFIED_BY" NUMBER(10,0),
    "LAST_MODIFIED_DATE" DATE,
    "IP_ADD" VARCHAR2(15 BYTE),
    "DELETEDFLAG" NUMBER(22,0),
    "RID" NUMBER(15,0),
    "ENTITY_ID" NUMBER(10,0),
    "ENTITY_TYPE" NUMBER(20,0),
    "FK_CORD_CDR_CBU_ID" VARCHAR2(255 CHAR),
    "FK_HLA_OVERCLS_CODE" NUMBER(10,0),
    "HLA_CRIT_OVER_DATE" DATE,
    "HLA_OVER_IND_ANTGN1" VARCHAR2(1 BYTE),
    "HLA_OVER_IND_ANTGN2" VARCHAR2(1 BYTE),
    "FK_HLA_PRIM_DNA_ID" NUMBER(10,0),
    "FK_HLA_PROT_DIPLOID" NUMBER(10,0),
    "FK_HLA_ANTIGENEID1" NUMBER(10,0),
    "FK_HLA_ANTIGENEID2" NUMBER(10,0),
    "HLA_RECEIVED_DATE" DATE,
    "HLA_TYPING_DATE" DATE,
    "FK_HLA_REPORTING_LAB" NUMBER(10,0),
    "FK_HLA_REPORT_METHOD" NUMBER(10,0),
    "FK_ORDER_ID" NUMBER(10,0),
    "FK_CORD_CT_STATUS" NUMBER(10,0),
    "FK_SPEC_TYPE" NUMBER(10,0),
    "FK_SOURCE" VARCHAR2(50 CHAR));


   COMMENT ON COLUMN "CB_BEST_HLA"."PK_BEST_HLA" IS 'Primary Key';

   COMMENT ON COLUMN "CB_BEST_HLA"."FK_HLA_CODE_ID" IS 'Code Id for HLA';

   COMMENT ON COLUMN "CB_BEST_HLA"."FK_HLA_METHOD_ID" IS 'Method ID from Code List for HLA';

   COMMENT ON COLUMN "CB_BEST_HLA"."HLA_VALUE_TYPE1" IS 'Value for type1 HLA';

   COMMENT ON COLUMN "CB_BEST_HLA"."HLA_VALUE_TYPE2" IS 'Value for type2 HLA';

   COMMENT ON COLUMN "CB_BEST_HLA"."CREATOR" IS 'This column is used for Audit Trail. The Creator identifies the user who created this row.';

   COMMENT ON COLUMN "CB_BEST_HLA"."CREATED_ON" IS 'This column is used for Audit Trail. Stores the date on which this row was created.';

   COMMENT ON COLUMN "CB_BEST_HLA"."LAST_MODIFIED_BY" IS 'This column is used for Audit Trail. The last_modified_by identifies the user who last modified this row.';

   COMMENT ON COLUMN "CB_BEST_HLA"."LAST_MODIFIED_DATE" IS 'This column is used for Audit Trail. Stores the date on which this row was last modified.';

   COMMENT ON COLUMN "CB_BEST_HLA"."IP_ADD" IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine.';

   COMMENT ON COLUMN "CB_BEST_HLA"."DELETEDFLAG" IS 'This column denotes whether record is deleted. ';

   COMMENT ON COLUMN "CB_BEST_HLA"."RID" IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database.';

   COMMENT ON COLUMN "CB_BEST_HLA"."ENTITY_ID" IS 'Id of the entity like CBU, Doner etc';

   COMMENT ON COLUMN "CB_BEST_HLA"."FK_HLA_OVERCLS_CODE" IS 'HLA Override class code.';

   COMMENT ON COLUMN "CB_BEST_HLA"."HLA_CRIT_OVER_DATE" IS 'HLA Critical override date - esb mapping.';

   COMMENT ON COLUMN "CB_BEST_HLA"."HLA_OVER_IND_ANTGN1" IS 'Indicator for override Antigen1.';

   COMMENT ON COLUMN "CB_BEST_HLA"."HLA_OVER_IND_ANTGN2" IS 'Indicator for override Antigen2';

   COMMENT ON COLUMN "CB_BEST_HLA"."FK_HLA_PRIM_DNA_ID" IS 'Primary DNA ID - esb message.';

   COMMENT ON COLUMN "CB_BEST_HLA"."FK_HLA_PROT_DIPLOID" IS 'HLA ProteinDiplotypeListId - esb message.';

   COMMENT ON COLUMN "CB_BEST_HLA"."FK_HLA_ANTIGENEID1" IS 'HLA ANTIGENEID2 - Type 2.';

   COMMENT ON COLUMN "CB_BEST_HLA"."HLA_RECEIVED_DATE" IS 'HLA Received date.';

   COMMENT ON COLUMN "CB_BEST_HLA"."HLA_TYPING_DATE" IS 'HLA Typing date.';

   COMMENT ON COLUMN "CB_BEST_HLA"."FK_HLA_REPORTING_LAB" IS 'Reference for reporting LAB.';

   COMMENT ON COLUMN "CB_BEST_HLA"."FK_HLA_REPORT_METHOD" IS 'HLA Reporting method code, reference to ER_CODELST.';

   COMMENT ON COLUMN "CB_BEST_HLA"."FK_ORDER_ID" IS 'ORDER reference ID';

   COMMENT ON COLUMN "CB_BEST_HLA"."FK_CORD_CT_STATUS" IS 'This column will store the fk of cord ct status table';

   COMMENT ON COLUMN "CB_BEST_HLA"."FK_SPEC_TYPE" IS 'This field store FK of Specimen Type.';

   COMMENT ON COLUMN "CB_BEST_HLA"."FK_SOURCE" IS 'This field store source of antigen.';

   COMMENT ON TABLE "CB_BEST_HLA"  IS 'This table stores BEST HLA records.';

   CREATE SEQUENCE SEQ_CB_BEST_HLA MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1;


CREATE SEQUENCE SEQ_CB_ADDITIONAL_IDS MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1;


--Sql for Create Table CB_ADDITIONAL_IDS
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tables where TABLE_NAME = 'CB_ADDITIONAL_IDS' ; 
  if (v_column_exists = 0) then 
 execute immediate 'create table CB_ADDITIONAL_IDS 
   (	PK_CB_ADDITIONAL_IDS NUMBER(10,0) NOT NULL , 
	CB_ADDITIONAL_ID VARCHAR2(20 BYTE), 
	FK_CB_ADDITIONAL_ID_TYPE NUMBER(10,0), 
	CB_ADDITIONAL_ID_DESC VARCHAR2(100 BYTE), 
	CREATED_ON DATE, 
	CREATOR NUMBER(10,0), 
	IP_ADD VARCHAR2(15 BYTE), 
	RID NUMBER(10,0), 
	LAST_MODIFIED_BY NUMBER(10,0), 
	LAST_MODIFIED_DATE DATE, 
	ENTITY_ID NUMBER(10,0), 
	ENTITY_TYPE NUMBER(10,0))';
    end if;
end;
/
COMMENT ON COLUMN "CB_ADDITIONAL_IDS"."PK_CB_ADDITIONAL_IDS" IS 'Primary Key';
 
   COMMENT ON COLUMN "CB_ADDITIONAL_IDS"."CB_ADDITIONAL_ID" IS 'Addtional Id  for an entity';
 
   COMMENT ON COLUMN "CB_ADDITIONAL_IDS"."FK_CB_ADDITIONAL_ID_TYPE" IS 'Type of Addtional Id  for an entity';
 
   COMMENT ON COLUMN "CB_ADDITIONAL_IDS"."CB_ADDITIONAL_ID_DESC" IS 'Description for addtional id';
 
   COMMENT ON COLUMN "CB_ADDITIONAL_IDS"."CREATED_ON" IS 'This column is used for Audit Trail. The Creator identifies the user who created this row.  ';
 
   COMMENT ON COLUMN "CB_ADDITIONAL_IDS"."CREATOR" IS 'This column is used for Audit Trail. The Creator identifies the user who created this row. ';
 
   COMMENT ON COLUMN "CB_ADDITIONAL_IDS"."IP_ADD" IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine.';
 
   COMMENT ON COLUMN "CB_ADDITIONAL_IDS"."RID" IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database.';
 
   COMMENT ON COLUMN "CB_ADDITIONAL_IDS"."LAST_MODIFIED_BY" IS 'This column is used for Audit Trail. The last_modified_by identifies the user who last modified this row.  ';
 
   COMMENT ON COLUMN "CB_ADDITIONAL_IDS"."LAST_MODIFIED_DATE" IS 'This column is used for Audit Trail. Stores the date on which this row was last modified.';
 
   COMMENT ON COLUMN "CB_ADDITIONAL_IDS"."ENTITY_ID" IS 'ID of an entity whose additional ids stored in this table';
 
   COMMENT ON COLUMN "CB_ADDITIONAL_IDS"."ENTITY_TYPE" IS 'Entity Type of an entity whose additional ids stored in this table';
 
   COMMENT ON TABLE "CB_ADDITIONAL_IDS"  IS 'This table stores additional IDs for cords.';

Commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,150,12,'12_Create_Table.sql',sysdate,'9.0.0 Build#607');

commit;
