set define off;

--For Report : Invoice by Patient
Update er_report set rep_sql = 'select inv_number, int_acc_num,
(select study_number from er_study where pk_study = a.fk_study) as studynum,
(select study_title from er_study where pk_study = a.fk_study) as studytitle,
to_char(inv_date,PKG_DATEUTIL.F_GET_DATEFORMAT) inv_date,
DECODE(trim(inv_payunit), ''D'' ,  to_char(to_date(to_char(inv_date, PKG_DATEUTIL.F_GET_DATEFORMAT), PKG_DATEUTIL.F_GET_DATEFORMAT) + inv_payment_dueby , PKG_DATEUTIL.F_GET_DATEFORMAT ), 
                          ''W'' ,  to_char(to_date(to_char(inv_date, PKG_DATEUTIL.F_GET_DATEFORMAT), PKG_DATEUTIL.F_GET_DATEFORMAT) + inv_payment_dueby * 7 , PKG_DATEUTIL.F_GET_DATEFORMAT ), 
                          ''M'' ,  to_char(add_months(to_date(inv_date , PKG_DATEUTIL.F_GET_DATEFORMAT) , inv_payment_dueby),PKG_DATEUTIL.F_GET_DATEFORMAT),
                                 to_char(add_months(to_date(inv_date , PKG_DATEUTIL.F_GET_DATEFORMAT) , inv_payment_dueby * 12),PKG_DATEUTIL.F_GET_DATEFORMAT)
) as duedays,
(SELECT USR_FIRSTNAME || '' '' || USR_LASTNAME FROM ER_USER WHERE PK_USER = INV_ADDRESSEDTO) AS addto,
(SELECT address FROM ER_ADD, ER_USER WHERE pk_add = fk_peradd AND PK_USER = INV_ADDRESSEDTO) AS addto_add,
(SELECT add_city || '' '' || add_state FROM ER_ADD, ER_USER WHERE pk_add = fk_peradd AND PK_USER = INV_ADDRESSEDTO) AS addto_add1,
(SELECT add_country || '' '' || add_zipcode FROM ER_ADD, ER_USER WHERE pk_add = fk_peradd AND PK_USER = INV_ADDRESSEDTO) AS addto_add2,
(SELECT USR_FIRSTNAME || '' '' || USR_LASTNAME FROM ER_USER WHERE PK_USER = inv_sentfrom) AS sentfrom,
(SELECT address FROM ER_ADD, ER_USER WHERE pk_add = fk_peradd AND PK_USER = inv_sentfrom) AS sentfrom_add,
(SELECT add_city || '' '' || add_state FROM ER_ADD, ER_USER WHERE pk_add = fk_peradd AND PK_USER = inv_sentfrom) AS sentfrom_add1,
(SELECT add_country || '' '' || add_zipcode FROM ER_ADD, ER_USER WHERE pk_add = fk_peradd AND PK_USER = inv_sentfrom) AS sentfrom_add2,
(SELECT patprot_patstdid FROM ER_PATPROT WHERE ER_PATPROT.fk_study = a.fk_study AND ER_PATPROT.fk_per = b.fk_per AND patprot_stat = 1) fk_per,
milestones_achieved,
to_char(achieved_on,PKG_DATEUTIL.F_GET_DATEFORMAT) achieved_on,
amount_due,
round(amount_invoiced,2) amount_invoiced, detail_type, display_detail,
inv_notes,
milestone_type AS TYPE,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = milestone_status) AS status,
(SELECT name FROM esch.event_assoc WHERE event_id = fk_cal) AS cal,
(SELECT name FROM esch.event_assoc WHERE event_id = fk_eventassoc) AS event,
(SELECT visit_name FROM esch.sch_protocol_visit WHERE pk_protocol_visit = fk_visit) AS visit,
((SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = milestone_status) || '' '' || (SELECT visit_name FROM esch.sch_protocol_visit WHERE pk_protocol_visit = fk_visit)) AS milestone_type, fk_per as pk_per
FROM ER_INVOICE a, ER_INVOICE_DETAIL b, er_milestone c
WHERE fk_inv = pk_invoice
and fk_milestone = pk_milestone
and pk_invoice = ~1 and nvl(b.fk_per,0) <> 0 and milestone_type <> ''SM'' ' where pk_report = 131;

commit;

--For Report : Invoice by Milestone Type
Update er_report set rep_sql = 'select inv_number, int_acc_num,
(select study_number from er_study where pk_study = a.fk_study) as studynum,
(select study_title from er_study where pk_study = a.fk_study) as studytitle,
to_char(inv_date,PKG_DATEUTIL.F_GET_DATEFORMAT) inv_date, 
DECODE(trim(inv_payunit), ''D'' ,  to_char(to_date(to_char(inv_date, PKG_DATEUTIL.F_GET_DATEFORMAT), PKG_DATEUTIL.F_GET_DATEFORMAT) + inv_payment_dueby , PKG_DATEUTIL.F_GET_DATEFORMAT ), 
                          ''W'' ,  to_char(to_date(to_char(inv_date, PKG_DATEUTIL.F_GET_DATEFORMAT), PKG_DATEUTIL.F_GET_DATEFORMAT) + inv_payment_dueby * 7 , PKG_DATEUTIL.F_GET_DATEFORMAT ), 
                          ''M'' ,  to_char(add_months(to_date(inv_date , PKG_DATEUTIL.F_GET_DATEFORMAT) , inv_payment_dueby),PKG_DATEUTIL.F_GET_DATEFORMAT),
                                 to_char(add_months(to_date(inv_date , PKG_DATEUTIL.F_GET_DATEFORMAT) , inv_payment_dueby * 12),PKG_DATEUTIL.F_GET_DATEFORMAT)
) as duedays,
(SELECT USR_FIRSTNAME || '' '' || USR_LASTNAME FROM ER_USER WHERE PK_USER = INV_ADDRESSEDTO) AS addto,
(SELECT address FROM ER_ADD, ER_USER WHERE pk_add = fk_peradd AND PK_USER = INV_ADDRESSEDTO) AS addto_add,
(SELECT add_city || '' '' || add_state FROM ER_ADD, ER_USER WHERE pk_add = fk_peradd AND PK_USER = INV_ADDRESSEDTO) AS addto_add1,
(SELECT add_country || '' '' || add_zipcode FROM ER_ADD, ER_USER WHERE pk_add = fk_peradd AND PK_USER = INV_ADDRESSEDTO) AS addto_add2,
(SELECT USR_FIRSTNAME || '' '' || USR_LASTNAME FROM ER_USER WHERE PK_USER = inv_sentfrom) AS sentfrom,
(SELECT address FROM ER_ADD, ER_USER WHERE pk_add = fk_peradd AND PK_USER = inv_sentfrom) AS sentfrom_add,
(SELECT add_city || '' '' || add_state FROM ER_ADD, ER_USER WHERE pk_add = fk_peradd AND PK_USER = inv_sentfrom) AS sentfrom_add1,
(SELECT add_country || '' '' || add_zipcode FROM ER_ADD, ER_USER WHERE pk_add = fk_peradd AND PK_USER = inv_sentfrom) AS sentfrom_add2,
(SELECT patprot_patstdid FROM ER_PATPROT WHERE ER_PATPROT.fk_study = a.fk_study AND ER_PATPROT.fk_per = b.fk_per AND patprot_stat = 1) fk_per,
milestones_achieved,
to_char(achieved_on,PKG_DATEUTIL.F_GET_DATEFORMAT) achieved_on, amount_due,
round(amount_invoiced,2) amount_invoiced, detail_type, display_detail,
inv_notes,
milestone_type AS TYPE,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = milestone_status) AS status,
(SELECT name FROM esch.event_assoc WHERE event_id = fk_cal) AS cal,
(SELECT name FROM esch.event_assoc WHERE event_id = fk_eventassoc) AS event,
(SELECT visit_name FROM esch.sch_protocol_visit WHERE pk_protocol_visit = fk_visit) AS visit,
((SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = milestone_status) || '' '' || (SELECT visit_name FROM esch.sch_protocol_visit WHERE pk_protocol_visit = fk_visit)) AS milestone_type ,
decode(detail_type,''H'',round(amount_invoiced,2),0.00) AMOUNT_INVOICED_FOR_GT
FROM ER_INVOICE a, ER_INVOICE_DETAIL b, er_milestone c
WHERE fk_inv = pk_invoice
and fk_milestone = pk_milestone
and pk_invoice = ~1 ' where pk_report = 132;

commit;

--For Report : Patient Events Status History
Update er_report set rep_sql_clob = 'SELECT   v.fk_per,
         v.fk_study,
    v.PER_CODE,
    v.STUDY_NUMBER,
    (SELECT site_name FROM ER_SITE WHERE pk_site = v.PER_SITE) AS site,
    (SELECT patprot_patstdid FROM ER_PATPROT WHERE v.pk_patprot= pk_patprot) AS patstudy_id,
        TO_CHAR(ev.EVENTSTAT_DT,PKG_DATEUTIL.F_GET_DATEFORMAT) EVENTSTAT_DT,
    NVL((SELECT usr_firstname || usr_lastname FROM ER_USER WHERE pk_user = s.EVENT_EXEBY) , (SELECT usr_firstname || usr_lastname FROM ER_USER WHERE pk_user = s.creator)) AS entered_by,
    TO_CHAR(s.ACTUAL_SCHDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) ACTUAL_SCHDATE,
    (select visit_name from sch_protocol_visit where pk_protocol_visit = s.FK_VISIT) visit,
    c.CODELST_DESC,
    s.DESCRIPTION,
       b.COST AS event_cost,
    b.EVENT_RES,
    ev.created_on,ev.last_modified_date
FROM    ERV_ALLPATSTUDYSTAT v, SCH_EVENTS1 s, SCH_CODELST c, sch_eventstat ev, EVENT_ASSOC b
WHERE  v.PK_PATPROT = s.fk_patprot AND
s.EVENT_ID = ev.FK_EVENT AND
s.FK_ASSOC = b.EVENT_ID AND
c.pk_codelst = ev.EVENTSTAT AND
c.codelst_type = ''eventstatus''
AND
v.fk_per IN (:patientId)  AND
v.fk_study IN (:studyId)  AND
v.per_site IN (:orgId) AND
v.fk_protocol IN (:protCalId) AND
NVL(s.ACTUAL_SCHDATE,TO_DATE(''01/01/1900'',PKG_DATEUTIL.F_GET_DATEFORMAT)) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)  AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
 ORDER BY v.PER_CODE, s.ACTUAL_SCHDATE, s.DESCRIPTION, ev.EVENTSTAT_DT' where pk_report = 11;

commit;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,150,6,'06_ER_REPORT_UPDATE.sql',sysdate,'9.0.0 Build#607');

commit;
