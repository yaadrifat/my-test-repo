set define off;

UPDATE ER_CODELST set CODELST_SUBTYP = 'draft'
WHERE CODELST_TYPE = 'mile_setstat' AND  CODELST_SUBTYP = 'mile_setstat_1';

UPDATE ER_CODELST set CODELST_SUBTYP = 'ready_review'
WHERE CODELST_TYPE = 'mile_setstat' AND  CODELST_SUBTYP = 'mile_setstat_2';

UPDATE ER_CODELST set CODELST_SUBTYP = 'approved'
WHERE CODELST_TYPE = 'mile_setstat' AND  CODELST_SUBTYP = 'mile_setstat_3';

UPDATE ER_CODELST set CODELST_SUBTYP = 'offline'
WHERE CODELST_TYPE = 'mile_setstat' AND  CODELST_SUBTYP = 'mile_setstat_4';
	
COMMIT;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,118,1,'01_DFIN20_Codelist.sql',sysdate,'8.10.0 Build#575');

commit;