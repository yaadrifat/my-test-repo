SET DEFINE OFF;
DECLARE 
  rowCount NUMBER ; 
BEGIN
SELECT COUNT(*) INTO ROWCOUNT FROM eres.ER_OBJECT_SETTINGS  WHERE OBJECT_TYPE='SM';
if (rowCount > 0) then

UPDATE eres.ER_OBJECT_SETTINGS SET OBJECT_TYPE='M' WHERE OBJECT_TYPE='SM';
commit;

END IF;

END;
/

INSERT INTO eres.track_patches
VALUES(eres.seq_track_patches.nextval,134,3,'03_Bug#6808.sql',sysdate,'9.0.0 Build#591');

commit;