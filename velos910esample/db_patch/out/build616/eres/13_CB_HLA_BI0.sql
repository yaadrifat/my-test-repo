create or replace TRIGGER "ERES".CB_HLA_BI0 BEFORE INSERT ON CB_HLA       REFERENCING OLD AS OLD NEW AS NEW

FOR EACH ROW  WHEN (NEW.rid IS NULL OR NEW.rid = 0) 
  DECLARE
  raid NUMBER(10);
  erid NUMBER(10);
  usr VARCHAR(2000);
      insert_data VARCHAR2(4000);
BEGIN
 BEGIN
 SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname INTO usr FROM er_user
 WHERE pk_user = :NEW.creator ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
   USR := 'New User' ;
 END ;
 SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  audit_trail.record_transaction(raid, 'CB_HLA',erid, 'I',usr);
       insert_data:=:NEW.PK_HLA || '|' ||
to_char(:NEW.FK_HLA_CODE_ID) || '|' ||
to_char(:NEW.FK_HLA_METHOD_ID) || '|' ||
to_char(:NEW.HLA_VALUE_TYPE1) || '|' ||
to_char(:NEW.HLA_VALUE_TYPE2) || '|' ||
to_char(:NEW.CREATOR) || '|' ||
to_char(:NEW.CREATED_ON) || '|' ||
to_char(:NEW.LAST_MODIFIED_BY) || '|' ||
to_char(:NEW.LAST_MODIFIED_DATE) || '|' ||
to_char(:NEW.IP_ADD) || '|' ||
to_char(:NEW.DELETEDFLAG) || '|' ||
to_char(:NEW.RID) || '|' ||
to_char(:NEW.ENTITY_ID) || '|' ||
to_char(:NEW.ENTITY_TYPE) || '|' ||
to_char(:NEW.FK_CORD_CDR_CBU_ID) || '|' ||
to_char(:NEW.FK_HLA_OVERCLS_CODE) || '|' ||
to_char(:NEW.HLA_CRIT_OVER_DATE) || '|' ||
to_char(:NEW.HLA_OVER_IND_ANTGN1) || '|' ||
to_char(:NEW.HLA_OVER_IND_ANTGN2) || '|' ||
to_char(:NEW.FK_HLA_PRIM_DNA_ID) || '|' ||
to_char(:NEW.FK_HLA_PROT_DIPLOID) || '|' ||
to_char(:NEW.FK_HLA_ANTIGENEID1) || '|' ||
to_char(:NEW.FK_HLA_ANTIGENEID2) || '|' ||
to_char(:NEW.HLA_RECEIVED_DATE) || '|' ||
to_char(:NEW.HLA_TYPING_DATE) || '|' ||
to_char(:NEW.FK_HLA_REPORTING_LAB) || '|' ||
to_char(:NEW.FK_HLA_REPORT_METHOD) || '|' ||
to_char(:NEW.FK_ORDER_ID) || '|' ||
to_char(:NEW.FK_CORD_CT_STATUS) || '|' ||
to_char(:NEW.FK_SPEC_TYPE) || '|' ||
to_char(:NEW.FK_SOURCE);
 INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
   END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,159,13,'13_CB_HLA_BI0.sql',sysdate,'9.0.0 Build#616');

commit;