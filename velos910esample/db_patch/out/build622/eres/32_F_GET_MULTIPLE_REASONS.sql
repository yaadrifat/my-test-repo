Create OR REPLACE
FUNCTION F_GET_MULTIPLE_REASONS (ID NUMBER)
RETURN VARCHAR2
AS
V_MULTI_VALUES VARCHAR2(1000);
Begin
	FOR i IN  (Select f_codelst_desc(cb_entity_status_reason.fk_reason_id) as multiple_values from cb_entity_status_reason where cb_entity_status_reason.fk_entity_status = id )
  	LOOP
	    V_MULTI_VALUES := V_MULTI_VALUES || i.multiple_values || ', ';
	END LOOP;
    Return Substr(V_Multi_Values,1,Length(V_Multi_Values)-2);
End; 

/



INSERT INTO track_patches
VALUES(seq_track_patches.nextval,165,32,'32_F_GET_MULTIPLE_REASONS.sql',sysdate,'9.0.0 Build#622');

commit;