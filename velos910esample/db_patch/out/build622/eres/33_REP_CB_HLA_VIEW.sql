Create Or Replace View Rep_Cb_Hla_View(Cord_Registry_Id,
Cord_Local_Cbu_Id,
Registry_Maternal_Id,
Maternal_Local_Id,
Created_On, 
Creator, 
Last_Modified_By,
Last_Modified_Date,
Status_Reasons, 
Entity_Type, 
Fk_Hla_Antigeneid1, 
Fk_Hla_Antigeneid2, 
Fk_Hla_Code_Id, 
Fk_Hla_Method_Id,
Fk_Source,
Fk_Spec_Type,
Hla_Value_Type1, 
Hla_Value_Type2,
Hla_Received_Date, 
Hla_Typing_Date,
Site_Name,
FK_ACCOUNT)
As
Select 
Cb_Cord.Cord_Registry_Id,
Cb_Cord.Cord_Local_Cbu_Id,
Cb_Cord.Registry_Maternal_Id,
cb_cord.maternal_local_id,
Cb_Hla.Created_On , 
(SELECT ER_USER.USR_FIRSTNAME
      || ' '
      || ER_USER.USR_LASTNAME
From Er_User
WHERE Pk_User = Cb_Cord.Creator) Creator,
Cb_Hla.Last_Modified_By,
Cb_Hla.Last_Modified_Date,
F_GET_MULTIPLE_REASONS(CB_HLA.ENTITY_ID) AS STATUS_REASONS, 
Cb_Hla.Entity_Type , 
Cb_Hla.Fk_Hla_Antigeneid1 , 
Cb_Hla.Fk_Hla_Antigeneid2 , 
F_Codelst_Desc(Cb_Hla.Fk_Hla_Code_Id) , 
F_Codelst_Desc(Cb_Hla.Fk_Hla_Method_Id),
F_Codelst_Desc(Cb_Hla.Fk_Source) ,
F_Codelst_Desc(Cb_Hla.Fk_Spec_Type),
Cb_Hla.Hla_Value_Type1 , 
Cb_Hla.Hla_Value_Type2,
Cb_Hla.Hla_Received_Date , 
Cb_Hla.Hla_Typing_Date ,
Er_Site.Site_Name As Cbb_Name,
Er_Site.Fk_Account 
From Cb_Hla, Cb_Cord, Er_Site
Where Cb_Hla.Entity_Id =Cb_Cord.Pk_Cord
And  Cb_Hla.Entity_Type = ( Select Pk_Codelst From Er_Codelst Where Lower(Codelst_Type) = 'entity_type' And Lower(Codelst_Subtyp) ='cbu')
And Cb_Cord.Fk_Cbb_Id =Er_Site.Pk_Site;


commit;



INSERT INTO track_patches
VALUES(seq_track_patches.nextval,165,33,'33_REP_CB_HLA_VIEW.sql',sysdate,'9.0.0 Build#622');

commit;