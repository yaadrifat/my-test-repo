set define off;

DECLARE
countFlag NUMBER(5);
BEGIN
	SELECT COUNT(1) INTO countFlag FROM USER_TABLES WHERE TABLE_NAME='ER_REPORT';
	if (countFlag > 0) then
		--REP_NAME='IRB Protocol History'
		UPDATE ER_REPORT SET 
		REP_COLUMNS = 'Study Title, PI, Submission Type, Submission Status, Submission Status Date', 
		REP_FILTERBY = 'Study' WHERE PK_REPORT = 153;
		
		--REP_NAME='IRB Continuing Review'
		UPDATE ER_REPORT SET 
		REP_COLUMNS = 'Study Number, Study Title, PI, Expiration Date, Study Status, Review Type, Review Board Name, Approval Date', 
		REP_FILTERBY = 'Study' WHERE PK_REPORT = 152;
		
		--REP_NAME='Resource Time Tracking'
		UPDATE ER_REPORT SET 
		REP_COLUMNS = 'Organization, Patient ID, Patient Study ID, Calendar, Visit, Event, Scheduled Date, Status, Status Date, Planned Time, Actual Time', 
		REP_FILTERBY = 'Study' WHERE PK_REPORT in (133, 135);
		commit;
	end if;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,131,1,'01_ER_REPORT.sql',sysdate,'9.0.0 Build#588');

commit;
