Set Define Off;

Update er_codelst set codelst_desc='CBU Registry ID'  where  codelst_type='all_ids' and codelst_subtyp = 'cbu_reg_id';

Update er_codelst set codelst_desc='Maternal Registry ID'  where  codelst_type='all_ids' and codelst_subtyp = 'regis_mat_id';

Update er_codelst set codelst_desc='Maternal Local ID'  where  codelst_type='all_ids' and codelst_subtyp = 'mat_local_id';

Commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,138,2,'02_ER_Codelst_Update.sql',sysdate,'9.0.0 Build#595');

commit;

