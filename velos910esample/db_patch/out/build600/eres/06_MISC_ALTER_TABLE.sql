Set Define Off;

--STARTS DROP THE COLUMN PK_PG_CONTI_WID_ID FROM UI_CONTAINER_WIDGET_MAP TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'UI_CONTAINER_WIDGET_MAP'
    AND column_name = 'PK_PG_CONTI_WID_ID';
  if (v_column_exists != 0) then
      execute immediate 'alter table ui_container_widget_map drop column PK_PG_CONTI_WID_ID';
  end if;
end;
/
--END--


--STARTS DROP THE COLUMN CREATED_DATE FROM UI_WIDGET TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from
      all_tab_columns 
    WHERE
      TABLE_NAME = 'UI_WIDGET'
    AND COLUMN_NAME = 'created_date';  
  if (v_column_exists != 0) then
      execute immediate 'alter table UI_WIDGET drop column created_date';
  end if;
end;
/
--END---


--STARTS DROP THE COLUMN ICONSPRITE FROM UI_WIDGET TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from
      all_tab_columns 
    WHERE
      TABLE_NAME = 'UI_WIDGET'
    AND COLUMN_NAME = 'iconsprite';  
  if (v_column_exists != 0) then
      execute immediate 'alter table UI_WIDGET drop column iconsprite';
  end if;
end;
/
--END--

ALTER TABLE ERES.CB_SHIPMENT DROP COLUMN SHIPMENT_COMPANY;

ALTER TABLE ERES.CB_SHIPMENT ADD(FK_SHIP_COMPANY_ID NUMBER(10,0));


--STARTS DROP THE COLUMN ORDER_SAMPLE_TYPE_AVAIL FROM ER_ORDER TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'ER_ORDER'
    AND column_name = 'ORDER_SAMPLE_TYPE_AVAIL';
  if (v_column_exists != 0) then
      execute immediate 'ALTER TABLE ERES.ER_ORDER DROP COLUMN ORDER_SAMPLE_TYPE_AVAIL';
  end if;
end;
/
--END--


--STARTS ADDING COLUMN FK_SAMPLE_TYPE_AVAIL TO ER_ORDER TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'ER_ORDER'
    AND column_name = 'FK_SAMPLE_TYPE_AVAIL';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.ER_ORDER ADD(FK_SAMPLE_TYPE_AVAIL NUMBER(10,0))';
  end if;
end;
/
--END--

COMMENT ON COLUMN ERES.ER_ORDER.FK_SAMPLE_TYPE_AVAIL IS 'Stores fk codelst for sample types';


--STARTS DROP THE COLUMN ORDER_ALIQUOTS_TYPE FROM ER_ORDER TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'ER_ORDER'
    AND column_name = 'ORDER_ALIQUOTS_TYPE';
  if (v_column_exists != 0) then
      execute immediate 'ALTER TABLE ERES.ER_ORDER DROP COLUMN ORDER_ALIQUOTS_TYPE';
  end if;
end;
/
--END--


--STARTS ADDING COLUMN FK_ALIQUOTS_TYPE TO ER_ORDER TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'ER_ORDER'
    AND column_name = 'FK_ALIQUOTS_TYPE';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.ER_ORDER ADD(FK_ALIQUOTS_TYPE NUMBER(10,0))';
  end if;
end;
/
--END--

COMMENT ON COLUMN ERES.ER_ORDER.FK_ALIQUOTS_TYPE IS 'Stores fk codelst for aliquots type'; 


--STARTS DROP THE COLUMN CORD_UNAVAIL_REASON FROM CB_CORD TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD'
    AND column_name = 'CORD_UNAVAIL_REASON';
  if (v_column_exists != 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD DROP COLUMN CORD_UNAVAIL_REASON';
  end if;
end;
/
--END--

--STARTS ADDING COLUMN FK_UNAVAIL_RSN TO CB_CORD TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD'
    AND column_name = 'FK_UNAVAIL_RSN';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD ADD(FK_UNAVAIL_RSN NUMBER(10,0))';
  end if;
end;
/
--END--

COMMENT ON COLUMN ERES.CB_CORD.FK_UNAVAIL_RSN IS 'Stores fk codelst for unvaialable reasons';

--SQL for the Cord Entry ->Lab Summary ->Processing & Counts ->Total CBU Nucleated Cell Count Frozen (unknown if UnCorrected)



DECLARE
  countFlage NUMBER(5);
BEGIN
  Select count(*) into countFlage from user_tab_cols  where TABLE_NAME = 'CB_CORD'  AND column_name = 'NUCL_CELL_CNT_FRZ_UNKNOWN';
   if (countFlage = 0) then
 execute immediate ('alter table CB_CORD add NUCL_CELL_CNT_FRZ_UNKNOWN number(10,2)');
  end if;
end;
/
 COMMENT ON COLUMN CB_CORD.NUCL_CELL_CNT_FRZ_UNKNOWN IS 'This column will store Nucleated frozen unknown if uncorrected Cell Count Start Processing.'; 
commit;

--SQL for the Cord -Entry ->Lab Summary ->Processing and Counts -> CBU Post Processing nRBC Absolute Number 

DECLARE
  countFlage NUMBER(5);
BEGIN
  Select count(*) into countFlage from user_tab_cols  where TABLE_NAME = 'CB_CORD'  AND column_name = 'POST_PRCSNG_NUCL_RBC_CN';
   if (countFlage = 0) then
 execute immediate ('alter table CB_CORD add POST_PRCSNG_NUCL_RBC_CN number(10,2)');
  end if;
end;
/
 COMMENT ON COLUMN CB_CORD.POST_PRCSNG_NUCL_RBC_CN IS 'This column will store CBU Post Processing nRBC Absolute Number.';
commit;


--SQL for the Cord-Entry -> Lab Summary -Processing and Counts ->Total CD34 Cell Count Frozen

DECLARE
  countFlage NUMBER(5);
BEGIN
  Select count(*) into countFlage from user_tab_cols  where TABLE_NAME = 'CB_CORD'  AND column_name = 'CD34_CELL_CNT_FRZN';
   if (countFlage = 0) then
 execute immediate ('alter table CB_CORD add CD34_CELL_CNT_FRZN number(10,2)');
  end if;
end;
/
COMMENT ON COLUMN CB_CORD.CD34_CELL_CNT_FRZN IS 'This column will store Total CD34 Cell Count Frozen.';
commit;



--SQL for the Cord-Entry -> Lab Summary - Procesing and Counts -> % of CD34 Viable
DECLARE
  countFlage NUMBER(5);
BEGIN
  Select count(*) into countFlage from user_tab_cols  where TABLE_NAME = 'CB_CORD'  AND column_name = 'PRCNT_CD34_VIABLE';
   if (countFlage = 0) then
 execute immediate ('alter table CB_CORD add PRCNT_CD34_VIABLE number(10,2)');
  end if;
end;
/
COMMENT ON COLUMN CB_CORD.PRCNT_CD34_VIABLE IS 'This column will store Percentage of CD34 Viable.';
commit;


--SQL for the Cord-Entry -> Lab Summary - Procesing and Counts -> CFU Test Date

DECLARE
  countFlage NUMBER(5);
BEGIN
  Select count(*) into countFlage from user_tab_cols  where TABLE_NAME = 'CB_CORD'  AND column_name = 'CFU_TST_DATE';
   if (countFlage = 0) then
 execute immediate ('alter table CB_CORD add CFU_TST_DATE date');
  end if;
end;
/
COMMENT ON COLUMN CB_CORD.CFU_TST_DATE IS 'This column will store CFU Test Date.';
commit;



--SQL for the Cord-Entry -> Lab Summary - Procesing and Counts -> Viability Test Date

DECLARE
  countFlage NUMBER(5);
BEGIN
  Select count(*) into countFlage from user_tab_cols  where TABLE_NAME = 'CB_CORD'  AND column_name = 'VIA_TST_DATE';
   if (countFlage = 0) then
 execute immediate ('alter table CB_CORD add VIA_TST_DATE date');
  end if;
end;
/
COMMENT ON COLUMN CB_CORD.VIA_TST_DATE IS 'This column will store Viability Test Date.';
commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,143,6,'06_MISC_ALTER_TABLE.sql',sysdate,'9.0.0 Build#600');

commit;