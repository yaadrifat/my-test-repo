delete from er_repxsl where fk_report in (5,10,11,13,16,45,81,85,86,95,120,131,133,134,152);

commit;

--CTA
Declare
	browserID INTEGER DEFAULT 0;
BEGIN

SELECT PK_BROWSER INTO browserID
FROM ER_BROWSER 
WHERE BROWSER_MODULE = 'allSchedules' AND BROWSER_NAME = 'Patient Schedule Browser';


-- DELETE report link FROM ER_BROWSERCONF
Delete from ER_BROWSERCONF WHERE FK_BROWSER = browserID AND BROWSERCONF_COLNAME='REPORT_LINK' and BROWSERCONF_SEQ =26;

commit;

END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,94,1,'01_data.sql',sysdate,'8.10.0 Build#551');

commit;