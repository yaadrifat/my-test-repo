1. All Reports are completed for Report Central module in this build except ad-hoc query.

Reports Modified for Study and Patient Label Change in Report Central Module for this Release
---------------------------------------------------------------------------------------------
5	Study IRB Expiration Review
10	Patient Schedule
11	Patient Events Status History
13	Patient Event List
16	Study Patient List 
45	Cost Estimate by Month
81	Adverse Event Tracking
85	Adverse Events by Study
86	Deaths on Study
95	Patient Visit Calendar
120	Audit Trail
131	Invoice by Patient
133	Resource Time Tracking
134	AE Audit Trail
152	IRB Continuing Review