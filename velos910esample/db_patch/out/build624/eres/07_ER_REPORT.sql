set define off;

delete from er_repxsl where pk_repxsl in (161,167,168,171);
commit;


	Update ER_REPORT set REP_SQL = 'select pk_cord, cord_registry_id, registry_maternal_id, cord_local_cbu_id, maternal_local_id, cord_isbi_din_code,
IDNOTES, CBBID, STORAGE_LOC, CBU_COLLECTION_SITE, CBU_INFO_NOTES_VISIBLE_TC, ELIGIBLE_STATUS, ELIGIBILITY_NOTES_VISIBLE_TC,
prcsng_start_date, bact_culture,bact_cul_strt_dt, bact_comment, frz_date, fungal_culture, fung_comment, abo_blood_type,
hemoglobin_scrn, rh_type, lic_status, lab_sum_notes_visible_tc, processing, automated_type,
other_processing, product_modification, other_product_modi, individual_frac, no_of_bags, other_bag,
heparin_thou_per,heparin_thou_ml,heparin_five_per,heparin_five_ml,heparin_ten_per,heparin_ten_ml,heparin_six_per,heparin_six_ml,cpda_per,
cpda_ml,cpd_per,cpd_ml,acd_per,acd_ml,othr_anti_per,othr_anti_ml,speci_othr_anti,
hun_dmso_per,hun_dmso_ml,hun_glycerol_per,hun_glycerol_ml,ten_dextran_40_per,ten_dextran_40_ml,five_human_albu_per,
FIVE_HUMAN_ALBU_ML,TWENTYFIVE_HUMAN_ALBU_PER,TWENTYFIVE_HUMAN_ALBU_ML, PLASMALYTE_PER,PLASMALYTE_ML,OTHR_CRYOPROTECTANT_PER,
othr_cryoprotectant_ml,spec_othr_cryoprotectant,five_dextrose_per,hlanotes_visible_tc,(select f_lab_summary(~1) from dual) lab_summary,
five_dextrose_ml,filter_paper, rpc_pellets, extr_dna, serum_aliquotes, plasma_aliquotes, nonviable_aliquotes, viable_samp_final_prod, no_of_segments,point_nine_nacl_per,point_nine_nacl_ml,othr_diluents_per,
OTHR_DILUENTS_ML,SPEC_OTHR_DILUENTS,PRODUCTCODE,NO_OF_OTH_REP_ALLIQUOTS_F_PROD, NO_OF_OTH_REP_ALLIQ_ALTER_COND, NO_OF_SERUM_MATER_ALIQUOTS, NO_OF_PLASMA_MATER_ALIQUOTS,
NO_OF_EXTR_DNA_MATER_ALIQUOTS, NO_OF_CELL_MATER_ALIQUOTS, (SELECT F_HLA_CORD(~1) FROM DUAL) HLA,BACT_CUL_STRT_DT,FUNG_CUL_STRT_DT,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''cbu_info_cat''))) CBU_ATTACHMENTS,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''hla''))) HLA_ATTACHMENTS,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''eligibile_cat''))) ELIGIBLE_ATTACHMENTS,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''lab_sum_cat''))) LABSUMMARY_ATTACHMENTS,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''process_info''))) PROCESSINFO_ATTACHMENTS,
F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''infect_mark''))) IDM_ATTACHMENTS,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''hlth_scren''))) HHS_ATTACHMENTS,CBB_ID,F_CORD_ID_ON_BAG(~1) IDONBAG, F_CORD_ADD_IDS(~1) ADDIDS,
INELIGIBLEREASON,UNLICENSEREASON,ELGIBLE_MODI_DT,LIC_MODI_DT,LIC_FLAG,ELI_FLAG,ELIGCOMMENTS,F_GETIDMDETAILS(~1) IDMDETAILS,IDM_NOTES_VISIBLE_TC,F_CBU_HISTORY(~1) CBUHISTORY,F_CBU_REQ_HISTORY(~1,~2) CBUREQHISTORY,
DECODE((SELECT F_CODELST_DESC(ORD.ORDER_TYPE) FROM ER_ORDER ORD WHERE ORD.PK_ORDER=~2),
        NULL,''0'',
        (SELECT F_CODELST_DESC(ORD.ORDER_TYPE) FROM ER_ORDER ORD WHERE ORD.PK_ORDER=~2),''1'')ordertypedata
FROM 
 REP_CBU_DETAILS
WHERE 
 PK_CORD = ~1' where pk_report = 168;
	COMMIT;




	Update ER_REPORT set REP_SQL_CLOB = 'select pk_cord, cord_registry_id, registry_maternal_id, cord_local_cbu_id, maternal_local_id, cord_isbi_din_code,
IDNOTES, CBBID, STORAGE_LOC, CBU_COLLECTION_SITE, CBU_INFO_NOTES_VISIBLE_TC, ELIGIBLE_STATUS, ELIGIBILITY_NOTES_VISIBLE_TC,
prcsng_start_date, bact_culture,bact_cul_strt_dt, bact_comment, frz_date, fungal_culture, fung_comment, abo_blood_type,
hemoglobin_scrn, rh_type, lic_status, lab_sum_notes_visible_tc, processing, automated_type,
other_processing, product_modification, other_product_modi, individual_frac, no_of_bags, other_bag,
heparin_thou_per,heparin_thou_ml,heparin_five_per,heparin_five_ml,heparin_ten_per,heparin_ten_ml,heparin_six_per,heparin_six_ml,cpda_per,
cpda_ml,cpd_per,cpd_ml,acd_per,acd_ml,othr_anti_per,othr_anti_ml,speci_othr_anti,
hun_dmso_per,hun_dmso_ml,hun_glycerol_per,hun_glycerol_ml,ten_dextran_40_per,ten_dextran_40_ml,five_human_albu_per,
FIVE_HUMAN_ALBU_ML,TWENTYFIVE_HUMAN_ALBU_PER,TWENTYFIVE_HUMAN_ALBU_ML, PLASMALYTE_PER,PLASMALYTE_ML,OTHR_CRYOPROTECTANT_PER,
othr_cryoprotectant_ml,spec_othr_cryoprotectant,five_dextrose_per,hlanotes_visible_tc,(select f_lab_summary(~1) from dual) lab_summary,
five_dextrose_ml,filter_paper, rpc_pellets, extr_dna, serum_aliquotes, plasma_aliquotes, nonviable_aliquotes, viable_samp_final_prod, no_of_segments,point_nine_nacl_per,point_nine_nacl_ml,othr_diluents_per,
OTHR_DILUENTS_ML,SPEC_OTHR_DILUENTS,PRODUCTCODE,NO_OF_OTH_REP_ALLIQUOTS_F_PROD, NO_OF_OTH_REP_ALLIQ_ALTER_COND, NO_OF_SERUM_MATER_ALIQUOTS, NO_OF_PLASMA_MATER_ALIQUOTS,
NO_OF_EXTR_DNA_MATER_ALIQUOTS, NO_OF_CELL_MATER_ALIQUOTS, (SELECT F_HLA_CORD(~1) FROM DUAL) HLA,BACT_CUL_STRT_DT,FUNG_CUL_STRT_DT,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''cbu_info_cat''))) CBU_ATTACHMENTS,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''hla''))) HLA_ATTACHMENTS,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''eligibile_cat''))) ELIGIBLE_ATTACHMENTS,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''lab_sum_cat''))) LABSUMMARY_ATTACHMENTS,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''process_info''))) PROCESSINFO_ATTACHMENTS,
F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''infect_mark''))) IDM_ATTACHMENTS,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''hlth_scren''))) HHS_ATTACHMENTS,CBB_ID,F_CORD_ID_ON_BAG(~1) IDONBAG, F_CORD_ADD_IDS(~1) ADDIDS,
INELIGIBLEREASON,UNLICENSEREASON,ELGIBLE_MODI_DT,LIC_MODI_DT,LIC_FLAG,ELI_FLAG,ELIGCOMMENTS,F_GETIDMDETAILS(~1) IDMDETAILS,IDM_NOTES_VISIBLE_TC,F_CBU_HISTORY(~1) CBUHISTORY,F_CBU_REQ_HISTORY(~1,~2) CBUREQHISTORY,
DECODE((SELECT F_CODELST_DESC(ORD.ORDER_TYPE) FROM ER_ORDER ORD WHERE ORD.PK_ORDER=~2),
        NULL,''0'',
        (SELECT F_CODELST_DESC(ORD.ORDER_TYPE) FROM ER_ORDER ORD WHERE ORD.PK_ORDER=~2),''1'')ordertypedata
FROM 
 REP_CBU_DETAILS
WHERE 
 PK_CORD = ~1' where pk_report = 168;
	COMMIT;

 


	Update ER_REPORT set REP_SQL = 'select pk_cord, cord_registry_id, registry_maternal_id, cord_local_cbu_id, maternal_local_id, cord_isbi_din_code,
idnotes, cbbid, cbb_id, storage_loc, cbu_collection_site, cbu_info_notes_visible_tc, (select f_hla_cord(~1) from dual) hla, hlanotes_visible_tc, eligible_status, eligibility_notes_visible_tc,
prcsng_start_date, bact_culture , bact_comment, frz_date, fungal_culture, fung_comment, abo_blood_type,
hemoglobin_scrn, rh_type, lic_status, lab_sum_notes_visible_tc,(select f_lab_summary(~1) from dual) lab_summary, processing, automated_type,
other_processing, product_modification, other_product_modi, individual_frac, no_of_bags, other_bag,
filter_paper, rpc_pellets, extr_dna, serum_aliquotes, plasma_aliquotes, nonviable_aliquotes, viable_samp_final_prod, no_of_segments,
no_of_oth_rep_alliquots_f_prod, no_of_oth_rep_alliq_alter_cond, no_of_serum_mater_aliquots, no_of_plasma_mater_aliquots,
no_of_extr_dna_mater_aliquots, no_of_cell_mater_aliquots,
ineligiblereason,unlicensereason,elgible_modi_dt,lic_modi_dt,lic_flag,eli_flag,eligcomments,f_getidmdetails(~1) idmdetails,idm_notes_visible_tc,
f_get_attachments(~1,(f_codelst_id(''doc_categ'',''cbu_info_cat''))) cbu_attachments,
f_get_attachments(~1,(f_codelst_id(''doc_categ'',''hla''))) hla_attachments, f_get_attachments(~1,(f_codelst_id(''doc_categ'',''eligibile_cat''))) eligible_attachments,
f_get_attachments(~1,(f_codelst_id(''doc_categ'',''infect_mark''))) idm_attachments, f_get_attachments(~1,(f_codelst_id(''doc_categ'',''lab_sum_cat''))) labsummary_attachments
FROM rep_cbu_details where pk_cord = ~1' where pk_report = 161;
	COMMIT;




	Update ER_REPORT set REP_SQL_CLOB = 'select pk_cord, cord_registry_id, registry_maternal_id, cord_local_cbu_id, maternal_local_id, cord_isbi_din_code,
idnotes, cbbid, cbb_id, storage_loc, cbu_collection_site, cbu_info_notes_visible_tc, (select f_hla_cord(~1) from dual) hla, hlanotes_visible_tc, eligible_status, eligibility_notes_visible_tc,
prcsng_start_date, bact_culture , bact_comment, frz_date, fungal_culture, fung_comment, abo_blood_type,
hemoglobin_scrn, rh_type, lic_status, lab_sum_notes_visible_tc,(select f_lab_summary(~1) from dual) lab_summary, processing, automated_type,
other_processing, product_modification, other_product_modi, individual_frac, no_of_bags, other_bag,
filter_paper, rpc_pellets, extr_dna, serum_aliquotes, plasma_aliquotes, nonviable_aliquotes, viable_samp_final_prod, no_of_segments,
no_of_oth_rep_alliquots_f_prod, no_of_oth_rep_alliq_alter_cond, no_of_serum_mater_aliquots, no_of_plasma_mater_aliquots,
no_of_extr_dna_mater_aliquots, no_of_cell_mater_aliquots,
ineligiblereason,unlicensereason,elgible_modi_dt,lic_modi_dt,lic_flag,eli_flag,eligcomments,f_getidmdetails(~1) idmdetails,idm_notes_visible_tc,
f_get_attachments(~1,(f_codelst_id(''doc_categ'',''cbu_info_cat''))) cbu_attachments,
f_get_attachments(~1,(f_codelst_id(''doc_categ'',''hla''))) hla_attachments, f_get_attachments(~1,(f_codelst_id(''doc_categ'',''eligibile_cat''))) eligible_attachments,
f_get_attachments(~1,(f_codelst_id(''doc_categ'',''infect_mark''))) idm_attachments, f_get_attachments(~1,(f_codelst_id(''doc_categ'',''lab_sum_cat''))) labsummary_attachments
FROM rep_cbu_details where pk_cord = ~1' where pk_report = 161;
	COMMIT;



	Update ER_REPORT set REP_SQL = 'select pk_cord, cord_registry_id, registry_maternal_id, cord_local_cbu_id, maternal_local_id, cord_isbi_din_code,
idnotes, cbbid, storage_loc, cbu_collection_site, cbu_info_notes, (select f_hla_cord(~1) from dual) hla, hlanotes_visible_tc, eligible_status, eligibility_notes,
ineligiblereason,unlicensereason,elgible_modi_dt,lic_modi_dt,lic_flag,eli_flag,eligcomments,prcsng_start_date, bact_culture , bact_comment, frz_date, fungal_culture, fung_comment, abo_blood_type,
hemoglobin_scrn, rh_type, lic_status, lab_sum_notes, lab_sum_notes_visible_tc,(select f_lab_summary(~1) from dual) lab_summary, proc_name, proc_start_date, proc_termi_date, processing, automated_type,
other_processing, product_modification, other_product_modi, storage_method, freezer_manufact, other_freezer_manufact,
FROZEN_IN, OTHER_FROZEN_CONT, NO_OF_BAGS, CRYOBAG_MANUFAC, BAGTYPE, BAG1TYPE, BAG2TYPE,   OTHER_BAG,
storage_temperature, max_vol, ctrl_rate_freezing, individual_frac,CBB_ID,
filter_paper, rpc_pellets, extr_dna, serum_aliquotes, plasma_aliquotes, nonviable_aliquotes, viable_samp_final_prod, no_of_segments,
NO_OF_OTH_REP_ALLIQUOTS_F_PROD, NO_OF_OTH_REP_ALLIQ_ALTER_COND, NO_OF_SERUM_MATER_ALIQUOTS, NO_OF_PLASMA_MATER_ALIQUOTS,
no_of_extr_dna_mater_aliquots, no_of_cell_mater_aliquots, f_getidmdetails(~1) idmdetails,idm_notes_visible_tc idmdetails,f_get_attachments(~1,(f_codelst_id(''doc_categ'',''cbu_info_cat''))) cbu_attachments,
f_get_attachments(~1,(f_codelst_id(''doc_categ'',''hla''))) hla_attachments, f_get_attachments(~1,(f_codelst_id(''doc_categ'',''eligibile_cat''))) eligible_attachments,
f_get_attachments(~1,(f_codelst_id(''doc_categ'',''infect_mark''))) idm_attachments, f_get_attachments(~1,(f_codelst_id(''doc_categ'',''lab_sum_cat''))) labsummary_attachments,
F_CBU_HISTORY(~1) CBUHISTORY,F_CBU_REQ_HISTORY(~1,~2) CBUREQHISTORY,
DECODE((SELECT F_CODELST_DESC(ORD.ORDER_TYPE) FROM ER_ORDER ORD WHERE ORD.PK_ORDER=~2),NULL,''0'',(SELECT F_CODELST_DESC(ORD.ORDER_TYPE) FROM ER_ORDER ORD WHERE ORD.PK_ORDER=~2),''1'') ordertypedata
FROM rep_cbu_details where pk_cord = ~1' where pk_report = 167;
	COMMIT;




	Update ER_REPORT set REP_SQL_CLOB = 'select pk_cord, cord_registry_id, registry_maternal_id, cord_local_cbu_id, maternal_local_id, cord_isbi_din_code,
idnotes, cbbid, storage_loc, cbu_collection_site, cbu_info_notes, (select f_hla_cord(~1) from dual) hla, hlanotes_visible_tc, eligible_status, eligibility_notes,
ineligiblereason,unlicensereason,elgible_modi_dt,lic_modi_dt,lic_flag,eli_flag,eligcomments,prcsng_start_date, bact_culture , bact_comment, frz_date, fungal_culture, fung_comment, abo_blood_type,
hemoglobin_scrn, rh_type, lic_status, lab_sum_notes, lab_sum_notes_visible_tc,(select f_lab_summary(~1) from dual) lab_summary, proc_name, proc_start_date, proc_termi_date, processing, automated_type,
other_processing, product_modification, other_product_modi, storage_method, freezer_manufact, other_freezer_manufact,
FROZEN_IN, OTHER_FROZEN_CONT, NO_OF_BAGS, CRYOBAG_MANUFAC, BAGTYPE, BAG1TYPE, BAG2TYPE,   OTHER_BAG,
storage_temperature, max_vol, ctrl_rate_freezing, individual_frac,CBB_ID,
filter_paper, rpc_pellets, extr_dna, serum_aliquotes, plasma_aliquotes, nonviable_aliquotes, viable_samp_final_prod, no_of_segments,
NO_OF_OTH_REP_ALLIQUOTS_F_PROD, NO_OF_OTH_REP_ALLIQ_ALTER_COND, NO_OF_SERUM_MATER_ALIQUOTS, NO_OF_PLASMA_MATER_ALIQUOTS,
no_of_extr_dna_mater_aliquots, no_of_cell_mater_aliquots, f_getidmdetails(~1) idmdetails,idm_notes_visible_tc idmdetails,f_get_attachments(~1,(f_codelst_id(''doc_categ'',''cbu_info_cat''))) cbu_attachments,
f_get_attachments(~1,(f_codelst_id(''doc_categ'',''hla''))) hla_attachments, f_get_attachments(~1,(f_codelst_id(''doc_categ'',''eligibile_cat''))) eligible_attachments,
f_get_attachments(~1,(f_codelst_id(''doc_categ'',''infect_mark''))) idm_attachments, f_get_attachments(~1,(f_codelst_id(''doc_categ'',''lab_sum_cat''))) labsummary_attachments,
F_CBU_HISTORY(~1) CBUHISTORY,F_CBU_REQ_HISTORY(~1,~2) CBUREQHISTORY,
DECODE((SELECT F_CODELST_DESC(ORD.ORDER_TYPE) FROM ER_ORDER ORD WHERE ORD.PK_ORDER=~2),NULL,''0'',(SELECT F_CODELST_DESC(ORD.ORDER_TYPE) FROM ER_ORDER ORD WHERE ORD.PK_ORDER=~2),''1'') ordertypedata
FROM rep_cbu_details where pk_cord = ~1' where pk_report = 167;
	COMMIT;

 Update ER_REPORT set REP_SQL = 'SELECT
F_CODELST_DESC(CORD.FK_CORD_CBU_ELIGIBLE_STATUS) ELIGSTATUS,
ROWTOCOL(''SELECT F_CODELST_DESC(REASON.FK_REASON_ID) FROM CB_CORD CORD INNER JOIN CB_ENTITY_STATUS STAT ON STAT.ENTITY_ID=CORD.PK_CORD INNER JOIN CB_ENTITY_STATUS_REASON REASON ON REASON.FK_ENTITY_STATUS=STAT.PK_ENTITY_STATUS WHERE CORD.FK_CORD_CBU_INELIGIBLE_REASON=STAT.PK_ENTITY_STATUS AND CORD.PK_CORD=~1'','', '') INELIGIBLEREASON,
F_CODELST_DESC(CORD.FK_CORD_CBU_LIC_STATUS) LICSTATUS,
ROWTOCOL(''SELECT F_CODELST_DESC(REASON.FK_REASON_ID) FROM CB_CORD CORD INNER JOIN CB_ENTITY_STATUS STAT ON STAT.ENTITY_ID=CORD.PK_CORD INNER JOIN CB_ENTITY_STATUS_REASON REASON ON REASON.FK_ENTITY_STATUS=STAT.PK_ENTITY_STATUS WHERE CORD.FK_CORD_CBU_UNLICENSED_REASON=STAT.PK_ENTITY_STATUS AND CORD.PK_CORD=~1'','', '') UNLICENSEREASON,
CORD.CORD_ELIGIBLE_ADDITIONAL_INFO ELIGCOMMENTS,
(SELECT TO_CHAR(MAX(CREATED_ON),''Mon DD, YYYY'') FROM CB_ENTITY_STATUS WHERE ENTITY_ID=~1 AND STATUS_TYPE_CODE=''eligibility'') ELGIBLE_MODI_DT,
(select to_char(max(CREATED_ON),''Mon DD, YYYY'') from CB_ENTITY_STATUS where ENTITY_ID=~1 and STATUS_TYPE_CODE=''licence'') LIC_MODI_DT,
CORD.CORD_REGISTRY_ID REGID,
SITE.SITE_NAME SITENAME,
SITE.SITE_ID SITEID,
F_CORD_ID_ON_BAG(~1) IDSONBAG,
CORD.CORD_LOCAL_CBU_ID LOCALCBUID,
CORD.CORD_NMDP_MATERNAL_ID MATREGID,
CORD.CORD_ISBI_DIN_CODE ISTBTDIN,
F_CORD_ADD_IDS(~1) ADDIDS,
DECODE(CORD.FK_CORD_CBU_ELIGIBLE_STATUS,
  (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE=''eligibility'' AND CODELST_SUBTYP=''eligible''),''0'',
  (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE=''eligibility'' AND CODELST_SUBTYP=''ineligible''),''1'',
  (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE=''eligibility'' AND CODELST_SUBTYP=''incomplete''),''2'',
  (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE=''eligibility'' AND CODELST_SUBTYP=''not_appli_prior''),''3'') ELI_FLAG,
DECODE(CORD.FK_CORD_CBU_LIC_STATUS,
  (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE=''licence'' AND CODELST_SUBTYP=''licensed''),''0'',
  (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE=''licence'' AND CODELST_SUBTYP=''unlicensed''),''1'') LIC_FLAG,
  F_GET_ATTACHMENTS(~1,F_CODELST_ID(''doc_categ'',''eligibile_cat'')) ELIGIBILITY_ATTACH
FROM
CB_CORD CORD
LEFT OUTER JOIN ER_SITE SITE ON
(CORD.FK_CBB_ID=SITE.PK_SITE)
WHERE
CORD.PK_CORD=~1' where pk_report = 171;
	COMMIT;




	Update ER_REPORT set REP_SQL_CLOB = 'SELECT
F_CODELST_DESC(CORD.FK_CORD_CBU_ELIGIBLE_STATUS) ELIGSTATUS,
ROWTOCOL(''SELECT F_CODELST_DESC(REASON.FK_REASON_ID) FROM CB_CORD CORD INNER JOIN CB_ENTITY_STATUS STAT ON STAT.ENTITY_ID=CORD.PK_CORD INNER JOIN CB_ENTITY_STATUS_REASON REASON ON REASON.FK_ENTITY_STATUS=STAT.PK_ENTITY_STATUS WHERE CORD.FK_CORD_CBU_INELIGIBLE_REASON=STAT.PK_ENTITY_STATUS AND CORD.PK_CORD=~1'','', '') INELIGIBLEREASON,
F_CODELST_DESC(CORD.FK_CORD_CBU_LIC_STATUS) LICSTATUS,
ROWTOCOL(''SELECT F_CODELST_DESC(REASON.FK_REASON_ID) FROM CB_CORD CORD INNER JOIN CB_ENTITY_STATUS STAT ON STAT.ENTITY_ID=CORD.PK_CORD INNER JOIN CB_ENTITY_STATUS_REASON REASON ON REASON.FK_ENTITY_STATUS=STAT.PK_ENTITY_STATUS WHERE CORD.FK_CORD_CBU_UNLICENSED_REASON=STAT.PK_ENTITY_STATUS AND CORD.PK_CORD=~1'','', '') UNLICENSEREASON,
CORD.CORD_ELIGIBLE_ADDITIONAL_INFO ELIGCOMMENTS,
(SELECT TO_CHAR(MAX(CREATED_ON),''Mon DD, YYYY'') FROM CB_ENTITY_STATUS WHERE ENTITY_ID=~1 AND STATUS_TYPE_CODE=''eligibility'') ELGIBLE_MODI_DT,
(select to_char(max(CREATED_ON),''Mon DD, YYYY'') from CB_ENTITY_STATUS where ENTITY_ID=~1 and STATUS_TYPE_CODE=''licence'') LIC_MODI_DT,
CORD.CORD_REGISTRY_ID REGID,
SITE.SITE_NAME SITENAME,
SITE.SITE_ID SITEID,
F_CORD_ID_ON_BAG(~1) IDSONBAG,
CORD.CORD_LOCAL_CBU_ID LOCALCBUID,
CORD.CORD_NMDP_MATERNAL_ID MATREGID,
CORD.CORD_ISBI_DIN_CODE ISTBTDIN,
F_CORD_ADD_IDS(~1) ADDIDS,
DECODE(CORD.FK_CORD_CBU_ELIGIBLE_STATUS,
  (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE=''eligibility'' AND CODELST_SUBTYP=''eligible''),''0'',
  (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE=''eligibility'' AND CODELST_SUBTYP=''ineligible''),''1'',
  (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE=''eligibility'' AND CODELST_SUBTYP=''incomplete''),''2'',
  (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE=''eligibility'' AND CODELST_SUBTYP=''not_appli_prior''),''3'') ELI_FLAG,
DECODE(CORD.FK_CORD_CBU_LIC_STATUS,
  (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE=''licence'' AND CODELST_SUBTYP=''licensed''),''0'',
  (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE=''licence'' AND CODELST_SUBTYP=''unlicensed''),''1'') LIC_FLAG,
  F_GET_ATTACHMENTS(~1,F_CODELST_ID(''doc_categ'',''eligibile_cat'')) ELIGIBILITY_ATTACH
FROM
CB_CORD CORD
LEFT OUTER JOIN ER_SITE SITE ON
(CORD.FK_CBB_ID=SITE.PK_SITE)
WHERE
CORD.PK_CORD=~1' where pk_report = 171;
	COMMIT;



Update ER_REPORT set REP_SQL = 'select pk_cord, cord_registry_id, cord_local_cbu_id, cord_isbi_din_code, cbbid, cbb_id, proc_name, proc_start_date, proc_termi_date, processing, automated_type,
other_processing, product_modification, other_product_modi, storage_method, individual_frac, freezer_manufact, other_freezer_manufact,
bagtype, other_bag, storage_temperature, cryobag_manufac, ctrl_rate_freezing, max_vol, frozen_in, other_frozen_cont,
heparin_thou_per, heparin_thou_ml, heparin_five_per, heparin_five_ml, heparin_ten_per,heparin_ten_ml,heparin_six_per,heparin_six_ml,cpda_per,
cpda_ml,cpd_per,cpd_ml,acd_per,acd_ml,othr_anti_per,othr_anti_ml,speci_othr_anti, hun_dmso_per,hun_dmso_ml,hun_glycerol_per,hun_glycerol_ml,
ten_dextran_40_per,ten_dextran_40_ml,five_human_albu_per,five_human_albu_ml,twentyfive_human_albu_per,twentyfive_human_albu_ml, plasmalyte_per,plasmalyte_ml,othr_cryoprotectant_per,
othr_cryoprotectant_ml,spec_othr_cryoprotectant,five_dextrose_per,five_dextrose_ml,point_nine_nacl_per,point_nine_nacl_ml,othr_diluents_per,
othr_diluents_ml,spec_othr_diluents,no_of_segments,extr_dna,filter_paper,no_of_cell_mater_aliquots,rpc_pellets,
no_of_serum_mater_aliquots,no_of_extr_dna_mater_aliquots, no_of_plasma_mater_aliquots,serum_aliquotes,plasma_aliquotes,nonviable_aliquotes, f_get_attachments(~1,(f_codelst_id(''doc_categ'',''process_info''))) processinfo_attachments
FROM rep_cbu_details where pk_cord = ~1' where pk_report = 170;
	COMMIT;




	Update ER_REPORT set REP_SQL_CLOB = 'select pk_cord, cord_registry_id, cord_local_cbu_id, cord_isbi_din_code, cbbid, cbb_id, proc_name, proc_start_date, proc_termi_date, processing, automated_type,
other_processing, product_modification, other_product_modi, storage_method, individual_frac, freezer_manufact, other_freezer_manufact,
bagtype, other_bag, storage_temperature, cryobag_manufac, ctrl_rate_freezing, max_vol, frozen_in, other_frozen_cont,
heparin_thou_per, heparin_thou_ml, heparin_five_per, heparin_five_ml, heparin_ten_per,heparin_ten_ml,heparin_six_per,heparin_six_ml,cpda_per,
cpda_ml,cpd_per,cpd_ml,acd_per,acd_ml,othr_anti_per,othr_anti_ml,speci_othr_anti, hun_dmso_per,hun_dmso_ml,hun_glycerol_per,hun_glycerol_ml,
ten_dextran_40_per,ten_dextran_40_ml,five_human_albu_per,five_human_albu_ml,twentyfive_human_albu_per,twentyfive_human_albu_ml, plasmalyte_per,plasmalyte_ml,othr_cryoprotectant_per,
othr_cryoprotectant_ml,spec_othr_cryoprotectant,five_dextrose_per,five_dextrose_ml,point_nine_nacl_per,point_nine_nacl_ml,othr_diluents_per,
othr_diluents_ml,spec_othr_diluents,no_of_segments,extr_dna,filter_paper,no_of_cell_mater_aliquots,rpc_pellets,
no_of_serum_mater_aliquots,no_of_extr_dna_mater_aliquots, no_of_plasma_mater_aliquots,serum_aliquotes,plasma_aliquotes,nonviable_aliquotes, f_get_attachments(~1,(f_codelst_id(''doc_categ'',''process_info''))) processinfo_attachments
FROM rep_cbu_details where pk_cord = ~1' where pk_report = 170;
	COMMIT;




Update ER_REPORT set REP_SQL = 'select cbbid, cbb_id, cord_registry_id, cord_local_cbu_id, cord_isbi_din_code, prcsng_start_date, bact_culture, bact_comment,
fungal_culture, fung_comment, abo_blood_type, hemoglobin_scrn, rh_type, bact_cul_strt_dt, fung_cul_strt_dt, frz_date,
(select f_lab_summary(~1) from dual) lab_summary, f_get_attachments(~1,(f_codelst_id(''doc_categ'',''lab_sum_cat''))) labsum_attachments
from rep_cbu_details where pk_cord = ~1' where pk_report = 173;
	COMMIT;




	Update ER_REPORT set REP_SQL_CLOB = 'select cbbid, cbb_id, cord_registry_id, cord_local_cbu_id, cord_isbi_din_code, prcsng_start_date, bact_culture, bact_comment,
fungal_culture, fung_comment, abo_blood_type, hemoglobin_scrn, rh_type, bact_cul_strt_dt, fung_cul_strt_dt, frz_date,
(select f_lab_summary(~1) from dual) lab_summary, f_get_attachments(~1,(f_codelst_id(''doc_categ'',''lab_sum_cat''))) labsum_attachments
from rep_cbu_details where pk_cord = ~1' where pk_report = 173;
	COMMIT;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,167,7,'07_ER_REPORT.sql',sysdate,'9.0.0 Build#624');

commit;