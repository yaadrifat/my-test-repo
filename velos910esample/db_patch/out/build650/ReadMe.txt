/////*********This readMe is specific to v9.1.0 build #650**********////
----------------------------------------------------------------------------------------------------------

As part of INF-22497, a new file "releaseinfo.xml" is introduced under 
\server\eresearch\deploy\velos.ear\velos.war\jsp\xml.

This file has following structure-
<ReleaseInfo>
	<VersionNo>v[Three letter version] Build#[build number]</VersionNo>
	<ReleaseDate>[Release Date]</ReleaseDate>
</ReleaseInfo>

----------------------------------------------------------------------------------------------------------

<VersionNo> tag contents should be same as ER_CTRLTAB version entry. 
If its not the same, there is a version inconsistency between Application and Database.

----------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------

Note to QA:- 
QA can change <VersionNo> tag contents to test the functionality.