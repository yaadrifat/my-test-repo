set define off;

Update er_codelst set codelst_desc = 'SER' where codelst_type = 'hla_meth' and codelst_subtyp = 'serology';


commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,169,80,'80_ER_CODELST_UPDATE.sql',sysdate,'9.0.0 Build#626');

commit;