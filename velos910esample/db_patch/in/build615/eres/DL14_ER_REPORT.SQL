set define off;

-- INSERTING into ER_REPORT

Insert into ER_REPORT (PK_REPORT,REP_NAME,REP_DESC,REP_SQL,FK_ACCOUNT,FK_CODELST_TYPE,IP_ADD,REP_HIDE,REP_COLUMNS,REP_FILTERBY,GENERATE_XML,REP_TYPE,REP_SQL_CLOB,REP_FILTERKEYWORD,REP_FILTERAPPLICABLE) 
	values (165,'CBU Assessment','CBU Assessment PDF','',0,null,null,'N',null,null,1,'rep_cbu','',null,null);
	COMMIT;

	Update ER_REPORT set REP_SQL = 'select CORD_REGISTRY_ID,(select site_name from er_site where pk_site = fk_cbb_id) as cbb_name from cb_cord where PK_CORD=:cordId' where pk_report = 165;
	COMMIT;

	Update ER_REPORT set REP_SQL_CLOB = 'select CORD_REGISTRY_ID,(select site_name from er_site where pk_site = fk_cbb_id) as cbb_name from cb_cord where PK_CORD=:cordId' where pk_report = 165;
	COMMIT;

Update ER_REPORT set REP_SQL = 'select  fk_cbb_id,
(select site_name from er_site where pk_site = fk_cbb_id) as cbb_name,
(select count(*) from cb_cord where fk_cbb_id = a.fk_cbb_id and  cord_baby_birth_date between (to_date('':repDate'',''MM/dd/yyyy'') - 30*12) and to_date('':repDate'',''MM/dd/yyyy'')) as one_yr,
(select count(*) from cb_cord where fk_cbb_id = a.fk_cbb_id and  cord_baby_birth_date  between (to_date('':repDate'',''MM/dd/yyyy'') - 30*24) and (to_date('':repDate'',''MM/dd/yyyy'') - 30*12)) as two_yr,
(select count(*) from cb_cord where fk_cbb_id = a.fk_cbb_id and  cord_baby_birth_date  between (to_date('':repDate'',''MM/dd/yyyy'') - 30*36) and (to_date('':repDate'',''MM/dd/yyyy'') - 30*24)) as three_yr,
(select count(*) from cb_cord where fk_cbb_id = a.fk_cbb_id and  cord_baby_birth_date  between (to_date('':repDate'',''MM/dd/yyyy'') - 30*48) and (to_date('':repDate'',''MM/dd/yyyy'') - 30*36)) as four_yr,
(select count(*) from cb_cord where fk_cbb_id = a.fk_cbb_id and  cord_baby_birth_date  between (to_date('':repDate'',''MM/dd/yyyy'') - 30*60) and (to_date('':repDate'',''MM/dd/yyyy'') - 30*48)) as five_yr,
(select count(*) from cb_cord where fk_cbb_id = a.fk_cbb_id and  cord_baby_birth_date  between (to_date('':repDate'',''MM/dd/yyyy'') - 30*72) and (to_date('':repDate'',''MM/dd/yyyy'') - 30*60)) as six_yr,
(select count(*) from cb_cord where fk_cbb_id = a.fk_cbb_id and  cord_baby_birth_date  < (to_date('':repDate'',''MM/dd/yyyy'') - 30*72) ) as seven_yr_plus
from cb_cord a group by fk_cbb_id order by fk_cbb_id' where pk_report = 162;
COMMIT;

Update ER_REPORT set REP_SQL_CLOB = 'select  fk_cbb_id,
(select site_name from er_site where pk_site = fk_cbb_id) as cbb_name,
(select count(*) from cb_cord where fk_cbb_id = a.fk_cbb_id and  cord_baby_birth_date between (to_date('':repDate'',''MM/dd/yyyy'') - 30*12) and to_date('':repDate'',''MM/dd/yyyy'')) as one_yr,
(select count(*) from cb_cord where fk_cbb_id = a.fk_cbb_id and  cord_baby_birth_date  between (to_date('':repDate'',''MM/dd/yyyy'') - 30*24) and (to_date('':repDate'',''MM/dd/yyyy'') - 30*12)) as two_yr,
(select count(*) from cb_cord where fk_cbb_id = a.fk_cbb_id and  cord_baby_birth_date  between (to_date('':repDate'',''MM/dd/yyyy'') - 30*36) and (to_date('':repDate'',''MM/dd/yyyy'') - 30*24)) as three_yr,
(select count(*) from cb_cord where fk_cbb_id = a.fk_cbb_id and  cord_baby_birth_date  between (to_date('':repDate'',''MM/dd/yyyy'') - 30*48) and (to_date('':repDate'',''MM/dd/yyyy'') - 30*36)) as four_yr,
(select count(*) from cb_cord where fk_cbb_id = a.fk_cbb_id and  cord_baby_birth_date  between (to_date('':repDate'',''MM/dd/yyyy'') - 30*60) and (to_date('':repDate'',''MM/dd/yyyy'') - 30*48)) as five_yr,
(select count(*) from cb_cord where fk_cbb_id = a.fk_cbb_id and  cord_baby_birth_date  between (to_date('':repDate'',''MM/dd/yyyy'') - 30*72) and (to_date('':repDate'',''MM/dd/yyyy'') - 30*60)) as six_yr,
(select count(*) from cb_cord where fk_cbb_id = a.fk_cbb_id and  cord_baby_birth_date  < (to_date('':repDate'',''MM/dd/yyyy'') - 30*72) ) as seven_yr_plus
from cb_cord a group by fk_cbb_id order by fk_cbb_id' where pk_report = 162;
COMMIT;


Insert into ER_REPORT (PK_REPORT,REP_NAME,REP_DESC,REP_SQL,FK_ACCOUNT,FK_CODELST_TYPE,IP_ADD,REP_HIDE,REP_COLUMNS,REP_FILTERBY,GENERATE_XML,REP_TYPE,REP_SQL_CLOB,REP_FILTERKEYWORD,REP_FILTERAPPLICABLE) 
values (163,'NCBI Cord Blood Bank Inventory by Age of CBU','NCBI Cord Blood Bank Inventory by Age of CBU','',0,null,null,'N',null,null,1,'rep_cbu','',null,null);
COMMIT;

Update ER_REPORT set REP_SQL = 'select  fk_cbb_id,
(select site_name from er_site where pk_site = fk_cbb_id) as cbb_name,
(select count(*) from cb_cord cord, er_specimen sp where cord.fk_specimen_id = sp.pk_specimen and cord.fk_cbb_id = a.fk_cbb_id  and  sp.spec_collection_date  between (to_date('':repDate'',''MM/dd/yyyy'') - 30*12) and to_date('':repDate'',''MM/dd/yyyy'')) as one_yr,
(select count(*) from cb_cord cord, er_specimen sp where cord.fk_specimen_id = sp.pk_specimen and cord.fk_cbb_id = a.fk_cbb_id  and  sp.spec_collection_date  between (to_date('':repDate'',''MM/dd/yyyy'') - 30*24) and (to_date('':repDate'',''MM/dd/yyyy'') - 30*12)) as two_yr,
(select count(*) from cb_cord cord, er_specimen sp where cord.fk_specimen_id = sp.pk_specimen and cord.fk_cbb_id = a.fk_cbb_id  and  sp.spec_collection_date  between (to_date('':repDate'',''MM/dd/yyyy'') - 30*36) and (to_date('':repDate'',''MM/dd/yyyy'') - 30*24)) as three_yr,
(select count(*) from cb_cord cord, er_specimen sp where cord.fk_specimen_id = sp.pk_specimen and cord.fk_cbb_id = a.fk_cbb_id  and  sp.spec_collection_date  between (to_date('':repDate'',''MM/dd/yyyy'') - 30*48) and (to_date('':repDate'',''MM/dd/yyyy'') - 30*36)) as four_yr, 
(select count(*) from cb_cord cord, er_specimen sp where cord.fk_specimen_id = sp.pk_specimen and cord.fk_cbb_id = a.fk_cbb_id  and  sp.spec_collection_date  between (to_date('':repDate'',''MM/dd/yyyy'') - 30*60) and (to_date('':repDate'',''MM/dd/yyyy'') - 30*48)) as five_yr,
(select count(*) from cb_cord cord, er_specimen sp where cord.fk_specimen_id = sp.pk_specimen and cord.fk_cbb_id = a.fk_cbb_id  and  sp.spec_collection_date  between (to_date('':repDate'',''MM/dd/yyyy'') - 30*72) and (to_date('':repDate'',''MM/dd/yyyy'') - 30*60)) as six_yr,
(select count(*) from cb_cord cord, er_specimen sp where cord.fk_specimen_id = sp.pk_specimen and cord.fk_cbb_id = a.fk_cbb_id  and  sp.spec_collection_date  < (to_date('':repDate'',''MM/dd/yyyy'') - 30*72) ) as seven_yr_plus
from cb_cord a group by fk_cbb_id order by fk_cbb_id' where pk_report = 163;
COMMIT;

Update ER_REPORT set REP_SQL_CLOB = 'select  fk_cbb_id,
(select site_name from er_site where pk_site = fk_cbb_id) as cbb_name,
(select count(*) from cb_cord cord, er_specimen sp where cord.fk_specimen_id = sp.pk_specimen and cord.fk_cbb_id = a.fk_cbb_id  and  sp.spec_collection_date  between (to_date('':repDate'',''MM/dd/yyyy'') - 30*12) and to_date('':repDate'',''MM/dd/yyyy'')) as one_yr,
(select count(*) from cb_cord cord, er_specimen sp where cord.fk_specimen_id = sp.pk_specimen and cord.fk_cbb_id = a.fk_cbb_id  and  sp.spec_collection_date  between (to_date('':repDate'',''MM/dd/yyyy'') - 30*24) and (to_date('':repDate'',''MM/dd/yyyy'') - 30*12)) as two_yr,
(select count(*) from cb_cord cord, er_specimen sp where cord.fk_specimen_id = sp.pk_specimen and cord.fk_cbb_id = a.fk_cbb_id  and  sp.spec_collection_date  between (to_date('':repDate'',''MM/dd/yyyy'') - 30*36) and (to_date('':repDate'',''MM/dd/yyyy'') - 30*24)) as three_yr,
(select count(*) from cb_cord cord, er_specimen sp where cord.fk_specimen_id = sp.pk_specimen and cord.fk_cbb_id = a.fk_cbb_id  and  sp.spec_collection_date  between (to_date('':repDate'',''MM/dd/yyyy'') - 30*48) and (to_date('':repDate'',''MM/dd/yyyy'') - 30*36)) as four_yr, 
(select count(*) from cb_cord cord, er_specimen sp where cord.fk_specimen_id = sp.pk_specimen and cord.fk_cbb_id = a.fk_cbb_id  and  sp.spec_collection_date  between (to_date('':repDate'',''MM/dd/yyyy'') - 30*60) and (to_date('':repDate'',''MM/dd/yyyy'') - 30*48)) as five_yr,
(select count(*) from cb_cord cord, er_specimen sp where cord.fk_specimen_id = sp.pk_specimen and cord.fk_cbb_id = a.fk_cbb_id  and  sp.spec_collection_date  between (to_date('':repDate'',''MM/dd/yyyy'') - 30*72) and (to_date('':repDate'',''MM/dd/yyyy'') - 30*60)) as six_yr,
(select count(*) from cb_cord cord, er_specimen sp where cord.fk_specimen_id = sp.pk_specimen and cord.fk_cbb_id = a.fk_cbb_id  and  sp.spec_collection_date  < (to_date('':repDate'',''MM/dd/yyyy'') - 30*72) ) as seven_yr_plus
from cb_cord a group by fk_cbb_id order by fk_cbb_id' where pk_report = 163;
COMMIT;
