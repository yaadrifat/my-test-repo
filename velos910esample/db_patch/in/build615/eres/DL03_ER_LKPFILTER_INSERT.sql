SET DEFINE OFF;

DECLARE
  table_count        NUMBER;
  seq_count          NUMBER;
  row_countNIHFilter NUMBER;
  row_countNCIFilter NUMBER;
BEGIN
  SELECT COUNT(*) INTO table_count FROM all_tables
  WHERE table_name = 'ER_LKPFILTER';
  
  SELECT COUNT(*) INTO seq_count FROM ALL_SEQUENCES
  WHERE SEQUENCE_NAME ='SEQ_ER_LKPFILTER';
  
  SELECT COUNT(*) INTO row_countNCIFilter FROM ER_LKPFILTER
  WHERE LKPFILTER_KEY = 'NCIProgramCode';
 
  SELECT COUNT(*) INTO row_countNIHFilter FROM ER_LKPFILTER
  WHERE LKPFILTER_KEY   ='NIHProgramCode';
  
  IF (table_count = 1 AND seq_count =1) THEN
    IF (row_countNCIFilter  < 1) THEN
      INSERT
      INTO ER_LKPFILTER
        (
          PK_LKPFILTER,
          LKPFILTER_KEY,
          LKPFILTER_SQL,
          LKPFILTER_COMMENT
        )
        VALUES
        (
          SEQ_ER_LKPFILTER.nextval,
          'NCIProgramCode',
          'CODELST_TYPE=''ProgramCode'' AND CODELST_CUSTOM_COL=''NCI''',
          'For NCI Division/Program Code'
        );
    END IF;
    IF(row_countNIHFilter < 1)
      THEN
      INSERT
      INTO ER_LKPFILTER
        (
          PK_LKPFILTER,
          LKPFILTER_KEY,
          LKPFILTER_SQL,
          LKPFILTER_COMMENT
        )
        VALUES
        (
          SEQ_ER_LKPFILTER.nextval,
          'NIHInstitutionCode',
          'CODELST_TYPE=''ProgramCode'' AND CODELST_CUSTOM_COL=''NIH''',
          'For NIH Institution Code'
        );
    END IF;
  END IF;
END;
/

COMMIT;