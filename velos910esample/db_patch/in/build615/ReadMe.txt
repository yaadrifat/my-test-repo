/* This readMe is specific to Velos eResearch version 9.0 build #615 */

=====================================================================================================================================
Garuda :
1.The "DL15_ER_OBJECT_SETTINGS_INSERT.sql" patch released with this build is specific to NMDP.
This patch consist two new sub menus of Data Extraction.
a). Published Reports
b). Chart
By default visibility of these menus are false.
=====================================================================================================================================
eResearch:


=====================================================================================================================================
eResearch Localization:

Following Files have been Modified:

1	accountforms.jsp
2	accountsave.jsp
3	address.jsp
4	adminauditreports.jsp
5	adminusersearchdetails.jsp
6	completeProtocol.jsp
7	contactus.jsp
8	copyEvent.jsp
9	crfnotify.jsp
10	crfstatus.jsp
11	editBoxSection.jsp
12	editmilestonerule.jsp
13	eventmilestone.jsp
14	getLookup.jsp
15	groupRights.jsp
16	labelBundle.properties
17	LC.java
18	LC.jsp
19	manageportallogins.jsp
20	manageVisitEvents.jsp
21	MC.java
22	MC.jsp
23	messageBundle.properties
24	milepaymentbrowser.jsp
25	milepayments.jsp
26	milestone.jsp
27	multipleChoiceSection.jsp
28	myHome.jsp
29	patientschedule.jsp
30	patstatmilestone.jsp
31	reportcentral.jsp
32	savecaltolibrary.jsp
33	saveMultiMilestones.jsp
34	sectionBrowser.jsp
35	selectevent.jsp
36	selectprotocol.jsp
37	specimendetails.jsp
38	studybudget.jsp
39	studybudgetprint.jsp
40	vis_multipleusersearchdetails.jsp
41	visitdetail.jsp
42	visitmilestone.jsp
43	xeventusersearch.jsp
44	xstudysearchbrowser.jsp
45	createCombinedBudget.jsp
46	db_datamanager.jsp
47	createMultimilestone.jsp
48	valueselect.jsp
=====================================================================================================================================
