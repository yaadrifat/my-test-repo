<?xml version="1.0" encoding="UTF-8"?><xsl:stylesheet version="1.0"
            xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
            xmlns:fo="http://www.w3.org/1999/XSL/Format">
            
    <xsl:key name="cordRegistryId" match="ROW" use="concat(CORD_REGISTRY_ID,' ')"/>
	<xsl:key name="cbbName" match="ROW" use="concat(CBB_NAME,' ')"/>    
	
	<xsl:template match="/">
		<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
		     <fo:layout-master-set>
			    <fo:simple-page-master master-name="pdf" page-height="11in" page-width="10in" 
                       margin-top="0.5in" margin-bottom="0.5in" margin-left="0.5in" margin-right="0.5in">
			         <fo:region-body margin-top="1.5in" margin-bottom="1.0in"/>
	                 <fo:region-before extent="1.0in"/>
                     <fo:region-after extent="0.5in"/>
			    </fo:simple-page-master>
		     </fo:layout-master-set>
		     <xsl:apply-templates select="ROWSET"/>
		</fo:root>
	</xsl:template>
	 <xsl:template match="ROWSET">
	     <fo:page-sequence master-reference="pdf" initial-page-number="1" font-family="serif">
			  <fo:static-content flow-name="xsl-region-before">
			       <fo:block font-size="10pt" font-weight="bold"> NMDP Proprietory and Confidential</fo:block>
			       <fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				   </fo:block>
			       <fo:block font-family="Helvetica" text-align="center" font-size="13pt" font-weight="bold">
					  National Marrow Donor Program&#174;
					</fo:block>					
					<fo:block font-family="Helvetica" text-align="center" font-size="11pt" font-weight="bold"  space-after.optimum="12pt">
					Cord Blood Assessment
					</fo:block>				
					<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
					</fo:block>	
					<xsl:for-each select="ROW[count(. | key('cordRegistryId', concat(CORD_REGISTRY_ID,' '))[1])=1]">
						<fo:table width="640pt" table-layout="fixed"  border-width="0.4mm" border-style="solid">
						       <fo:table-body >
						           <fo:table-row>
						                <fo:table-cell>
						                     <fo:block text-align="left" font-size="10pt">Patient Identification Number:</fo:block>
						                </fo:table-cell>
						                <fo:table-cell>
						                     <fo:block text-align="left" font-size="10pt"></fo:block>						                     
						                </fo:table-cell>
						           </fo:table-row>
						           <fo:table-row>
						                <fo:table-cell>
						                     <fo:block text-align="left" font-size="10pt">Cord Blood Bank/Registry Name:</fo:block>
						                </fo:table-cell>
						                <fo:table-cell>
						                     <fo:block text-align="left" font-size="10pt">
						                        <xsl:variable name="cbb_Name" select="CBB_NAME" />         
						                        <xsl:value-of select="$cbb_Name"></xsl:value-of>
						                     </fo:block>						                     
						                </fo:table-cell>						                
						           </fo:table-row>
						           <fo:table-row>
						                <fo:table-cell>
						                     <fo:block text-align="left" font-size="10pt">CBU Registry ID:</fo:block>
						                </fo:table-cell>
						                <fo:table-cell>
						                     <fo:block text-align="left" font-size="10pt">
						                        <xsl:variable name="cord_Reg" select="CORD_REGISTRY_ID" />         
						                        <xsl:value-of select="$cord_Reg"></xsl:value-of> 
						                     </fo:block>
						                </fo:table-cell>
						                <fo:table-cell>
						                     <fo:block text-align="left" font-size="10pt">ID On Bag:</fo:block>
						                </fo:table-cell>
						                <fo:table-cell>
						                     <fo:block text-align="left" font-size="10pt"></fo:block>
						                </fo:table-cell>
						           </fo:table-row>
						       </fo:table-body>
						</fo:table>	
					</xsl:for-each>	
			  </fo:static-content>
			  <fo:static-content flow-name="xsl-region-after">
				   <fo:table >
					   <fo:table-body>
						   <fo:table-row>
						    <fo:table-cell>  <fo:block text-align="left" font-size="10pt">Cord Blood Assessment</fo:block></fo:table-cell>	
						   </fo:table-row>
						   <fo:table-row>
							     <fo:table-cell><fo:block text-align="left" font-size="10pt" font-weight="bold">Document Number</fo:block>	</fo:table-cell>
							     <fo:table-cell><fo:block  text-align="left" font-size="10pt" font-weight="bold">Page <fo:page-number/> </fo:block>	</fo:table-cell>
						   </fo:table-row>
						   <fo:table-row>
						    <fo:table-cell> <fo:block text-align="left" font-size="10pt">Replaces:</fo:block></fo:table-cell>
						    <fo:table-cell><fo:block text-align="left">N/A</fo:block></fo:table-cell>	
						   </fo:table-row>
						   <fo:table-row>
						     <fo:table-cell>
						           <fo:block text-align="left" font-size="10pt"></fo:block>
						     </fo:table-cell>
						   </fo:table-row>
						    <fo:table-row>
							     <fo:table-cell>
							           <fo:block text-align="center" font-size="10pt">
							              Copyright &#169; 2011 National Marrow Donor Program. All Rights Reserved.
							           </fo:block>
							     </fo:table-cell>						     
						   </fo:table-row>	
						   <fo:table-row>
						         <fo:table-cell>
						           <fo:block text-align="center" font-size="10pt">
						             National Marrow Donor&#174;Program Proprietary and Confidential Information.
						           </fo:block>
						        </fo:table-cell>
						   </fo:table-row>					   
					   </fo:table-body>
				   </fo:table>
			  </fo:static-content>
			  <fo:flow flow-name="xsl-region-body">			     
				<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				</fo:block>			
				<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				</fo:block>
				<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				</fo:block>			
				<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				</fo:block>
				<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				</fo:block>			
				<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				</fo:block>
				<fo:table width="680pt">
				    <fo:table-column column-width="40pt" column-number="1"/>
					<fo:table-column column-width="500pt" column-number="2"/>
					<fo:table-column column-width="40pt" column-number="3"/>
					<fo:table-column column-width="100pt" column-number="4"/>
				    <fo:table-body >
				        <fo:table-row>
				            <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" font-weight="bold">A.</fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm">
					            <fo:block text-align="left" font-size="10pt" font-weight="bold">Cord Blood Maternal Risk Questionnaire</fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm">
					            <fo:block text-align="left" font-size="10pt" font-weight="bold">
					               <fo:external-graphic width="10pt" height="10pt" content-width="10pt" content-height="10pt" src="images/checkbox.jpeg"/> 
					            </fo:block>
					        </fo:table-cell >
					        <fo:table-cell padding="0.0mm">
					            <fo:block text-align="left" font-size="10pt" font-weight="bold">Attached(REQUIRED)</fo:block>
					       </fo:table-cell>					       
				        </fo:table-row>	
				      </fo:table-body>
				   </fo:table>
				  <fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				  </fo:block>			
				  <fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				  </fo:block>
				   <fo:table width="680">
				     <fo:table-column column-width="40pt" column-number="1"/>
					 <fo:table-column column-width="640pt" column-number="2"/>
				      <fo:table-body >
				        <fo:table-row>
				            <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" font-weight="bold">B.</fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm">
					            <fo:block text-align="left" font-size="10pt" font-weight="bold">
					              Review of relevant Medical records on file at the CBB and any identified clinical evidence of infection (RCADADs)
					            </fo:block>
					       </fo:table-cell>
				        </fo:table-row>	
				         <fo:table-row>
				            <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" font-weight="bold"></fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" font-weight="bold">
					              obtained from the review of available relevant medical records at the time of delivery, including any relevant
					            </fo:block>
					       </fo:table-cell>
				        </fo:table-row>	
				        <fo:table-row>
				            <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" font-weight="bold"></fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm">
					            <fo:block text-align="left" font-size="10pt" font-weight="bold">
					              physical findings
					            </fo:block>
					       </fo:table-cell>
				        </fo:table-row>			       
				    </fo:table-body>
				</fo:table>
				<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				</fo:block>			
				<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				</fo:block>
				<fo:table width="680">
				     <fo:table-column column-width="40pt" column-number="1"/>
					 <fo:table-column column-width="40pt" column-number="2"/>
					 <fo:table-column column-width="300pt" column-number="3"/>
					 <fo:table-column column-width="300pt" column-number="4"/>
				      <fo:table-body >
				          <fo:table-row>
				            <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" >1.</fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm">
					            <fo:block text-align="left" font-size="10pt" >
					              Was this review completed?
					            </fo:block>
					       </fo:table-cell>
					       <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
				        </fo:table-row>	
				      </fo:table-body>
				</fo:table> 
				<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				</fo:block>			
				<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				</fo:block>
				<fo:table width="680">
				     <fo:table-column column-width="40pt" column-number="1"/>
					 <fo:table-column column-width="40pt" column-number="2"/>
					 <fo:table-column column-width="300pt" column-number="3"/>
					 <fo:table-column column-width="300pt" column-number="4"/>
				      <fo:table-body >
				          <fo:table-row>
				            <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm">
					            <fo:block text-align="left" font-size="10pt" >
					              <fo:external-graphic width="10pt" height="10pt" content-width="10pt" content-height="10pt" src="images/checkbox.jpeg"/> Yes -> Proceed to question 2
					            </fo:block>
					       </fo:table-cell>
					       <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
				        </fo:table-row>	
				      </fo:table-body>
				</fo:table>
				<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				</fo:block>			
				<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				</fo:block>
				<fo:table width="680">
				     <fo:table-column column-width="40pt" column-number="1"/>
					 <fo:table-column column-width="40pt" column-number="2"/>
					 <fo:table-column column-width="40pt" column-number="3"/>
					 <fo:table-column column-width="460pt" column-number="4"/>
					 <fo:table-column column-width="100pt" column-number="5"/>
				      <fo:table-body >
				          <fo:table-row>
				            <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
					         <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" >
					            <fo:external-graphic width="10pt" height="10pt" content-width="10pt" content-height="10pt" src="images/checkbox.jpeg"/> No ->
					            </fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm">
					            <fo:block text-align="left" font-size="10pt" >
					              1.a Will the review of relevant medical records be completed prior to the release of this CBU?
					              <fo:external-graphic width="10pt" height="10pt" content-width="10pt" content-height="10pt" src="images/checkbox.jpeg"/> Yes
					              <fo:external-graphic width="10pt" height="10pt" content-width="10pt" content-height="10pt" src="images/checkbox.jpeg"/> No
					            </fo:block>
					       </fo:table-cell>
					       <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
				        </fo:table-row>	
				      </fo:table-body>
				</fo:table>
				<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				</fo:block>			
				<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				</fo:block>
				<fo:table width="680">
				     <fo:table-column column-width="40pt" column-number="1"/>
					 <fo:table-column column-width="40pt" column-number="2"/>
					 <fo:table-column column-width="40pt" column-number="3"/>
					 <fo:table-column column-width="460pt" column-number="4"/>
					 <fo:table-column column-width="100pt" column-number="5"/>
				      <fo:table-body >
				          <fo:table-row>
				            <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm">
					            <fo:block text-align="left" font-size="10pt" >
					               1.b Will the review of relevant medical records be completed?
					               <fo:external-graphic width="10pt" height="10pt" content-width="10pt" content-height="10pt" src="images/checkbox.jpeg"/> Yes
					               <fo:external-graphic width="10pt" height="10pt" content-width="10pt" content-height="10pt" src="images/checkbox.jpeg"/> No
					            </fo:block>
					       </fo:table-cell>
					       <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
				        </fo:table-row>	
				      </fo:table-body>
				</fo:table>
				<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				</fo:block>			
				<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				</fo:block>
				<fo:table width="680">
				     <fo:table-column column-width="40pt" column-number="1"/>
					 <fo:table-column column-width="40pt" column-number="2"/>
					 <fo:table-column column-width="40pt" column-number="3"/>
					 <fo:table-column column-width="460pt" column-number="4"/>
					 <fo:table-column column-width="100pt" column-number="5"/>
				      <fo:table-body >
				          <fo:table-row>
				            <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm">
					            <fo:block text-align="left" font-size="10pt" >
					               Additional Detail
					            </fo:block>
					       </fo:table-cell>
					       <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
				        </fo:table-row>	
				      </fo:table-body>
				</fo:table>
				<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				</fo:block>			
				<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				</fo:block>
				<fo:table width="680" >
				     <fo:table-column column-width="40pt" column-number="1"/>
					 <fo:table-column column-width="40pt" column-number="2"/>
					 <fo:table-column column-width="40pt" column-number="3"/>
					 <fo:table-column column-width="460pt" column-number="4"/>
					 <fo:table-column column-width="100pt" column-number="5"/>
				      <fo:table-body >
				          <fo:table-row>
				            <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm">
					            <fo:block text-align="left" font-size="10pt" >
					               <fo:table border-width="0.4mm" border-style="solid" height="200">
					                    <fo:table-body>
					                      <fo:table-row>
					                         <fo:table-cell>
					                           <fo:block>	  
					                                  <fo:external-graphic width="100pt" height="30pt" content-width="50pt" content-height="50pt" src="images/textbox.jpeg"/>                       
					                           </fo:block>
					                         </fo:table-cell>
					                      </fo:table-row>
					                    </fo:table-body>
					               </fo:table>
					            </fo:block>
					       </fo:table-cell>
					       <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
				        </fo:table-row>	
				      </fo:table-body>
				</fo:table>
				<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				</fo:block>			
				<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				</fo:block>
				<fo:table width="680">
				     <fo:table-column column-width="40pt" column-number="1"/>
					 <fo:table-column column-width="40pt" column-number="2"/>
					 <fo:table-column column-width="300pt" column-number="3"/>
					 <fo:table-column column-width="300pt" column-number="4"/>
				      <fo:table-body >
				          <fo:table-row>
				            <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" >2.</fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm">
					            <fo:block text-align="left" font-size="10pt" >
					              Were any risk factor identified in relevant medical records?
					            </fo:block>
					       </fo:table-cell>
					       <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
				        </fo:table-row>	
				      </fo:table-body>
				</fo:table> 
				<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				</fo:block>			
				<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				</fo:block>
				<fo:table width="680">
				     <fo:table-column column-width="40pt" column-number="1"/>
					 <fo:table-column column-width="40pt" column-number="2"/>
					 <fo:table-column column-width="300pt" column-number="3"/>
					 <fo:table-column column-width="300pt" column-number="4"/>
				      <fo:table-body >
				          <fo:table-row>
				            <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm">
					            <fo:block text-align="left" font-size="10pt" >
					               <fo:external-graphic width="10pt" height="10pt" content-width="10pt" content-height="10pt" src="images/checkbox.jpeg"/> No -> Proceed to question 3
					            </fo:block>
					       </fo:table-cell>
					       <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
				        </fo:table-row>	
				      </fo:table-body>
				</fo:table>
				<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				</fo:block>			
				<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				</fo:block>
				<fo:table width="680">
				     <fo:table-column column-width="40pt" column-number="1"/>
					 <fo:table-column column-width="40pt" column-number="2"/>
					 <fo:table-column column-width="300pt" column-number="3"/>
					 <fo:table-column column-width="300pt" column-number="4"/>
				      <fo:table-body >
				          <fo:table-row>
				            <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm">
					            <fo:block text-align="left" font-size="10pt" >
					               <fo:external-graphic width="10pt" height="10pt" content-width="10pt" content-height="10pt" src="images/checkbox.jpeg"/> Yes -> Additional Detail:
					            </fo:block>
					       </fo:table-cell>
					       <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
				        </fo:table-row>	
				      </fo:table-body>
				</fo:table>
				<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				</fo:block>			
				<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				</fo:block>
				<fo:table width="680" >
				     <fo:table-column column-width="40pt" column-number="1"/>
					 <fo:table-column column-width="40pt" column-number="2"/>
					 <fo:table-column column-width="40pt" column-number="3"/>
					 <fo:table-column column-width="460pt" column-number="4"/>
					 <fo:table-column column-width="100pt" column-number="5"/>
				      <fo:table-body >
				          <fo:table-row>
				            <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm">
					            <fo:block text-align="left" font-size="10pt" >
					               <fo:table border-width="0.4mm" border-style="solid" height="200">
					                    <fo:table-body>
					                      <fo:table-row>
					                         <fo:table-cell>
					                           <fo:block>	   
					                              <fo:external-graphic width="100pt" height="30pt" content-width="50pt" content-height="50pt" src="images/textbox.jpeg"/>                        
					                           </fo:block>
					                         </fo:table-cell>
					                      </fo:table-row>
					                    </fo:table-body>
					               </fo:table>
					            </fo:block>
					       </fo:table-cell>
					       <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
				        </fo:table-row>	
				      </fo:table-body>
				</fo:table>
				<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				</fo:block>			
				<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				</fo:block>
				<fo:table width="680">
				     <fo:table-column column-width="40pt" column-number="1"/>
					 <fo:table-column column-width="40pt" column-number="2"/>
					 <fo:table-column column-width="300pt" column-number="3"/>
					 <fo:table-column column-width="300pt" column-number="4"/>
				      <fo:table-body >
				          <fo:table-row>
				            <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" >3.</fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm">
					            <fo:block text-align="left" font-size="10pt" >
					              Were any risk factor identified in physical finding Assessment?
					            </fo:block>
					       </fo:table-cell>
					       <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
				        </fo:table-row>	
				      </fo:table-body>
				</fo:table> 
				<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				</fo:block>			
				<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				</fo:block>
				<fo:table width="680">
				     <fo:table-column column-width="40pt" column-number="1"/>
					 <fo:table-column column-width="40pt" column-number="2"/>
					 <fo:table-column column-width="300pt" column-number="3"/>
					 <fo:table-column column-width="300pt" column-number="4"/>
				      <fo:table-body >
				          <fo:table-row>
				            <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm">
					            <fo:block text-align="left" font-size="10pt" >
					               <fo:external-graphic width="10pt" height="10pt" content-width="10pt" content-height="10pt" src="images/checkbox.jpeg"/> No -> Proceed to Section C
					            </fo:block>
					       </fo:table-cell>
					       <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
				        </fo:table-row>	
				      </fo:table-body>
				</fo:table>
				<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				</fo:block>			
				<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				</fo:block>
				<fo:table width="680">
				     <fo:table-column column-width="40pt" column-number="1"/>
					 <fo:table-column column-width="40pt" column-number="2"/>
					 <fo:table-column column-width="300pt" column-number="3"/>
					 <fo:table-column column-width="300pt" column-number="4"/>
				      <fo:table-body >
				          <fo:table-row>
				            <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm">
					            <fo:block text-align="left" font-size="10pt" >
					               <fo:external-graphic width="10pt" height="10pt" content-width="10pt" content-height="10pt" src="images/checkbox.jpeg"/> Yes -> Please answer the following:
					            </fo:block>
					       </fo:table-cell>
					       <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" >
					               <fo:external-graphic width="100pt" height="180pt" content-width="50pt" content-height="50pt" src="images/questions.jpeg"/>
					            </fo:block>
					        </fo:table-cell>
				        </fo:table-row>	
				      </fo:table-body>
				</fo:table>
				<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				</fo:block>			
				<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				</fo:block>
				<fo:table width="680">
				     <fo:table-column column-width="40pt" column-number="1"/>
					 <fo:table-column column-width="40pt" column-number="2"/>
					 <fo:table-column column-width="40pt" column-number="3"/>
					 <fo:table-column column-width="460pt" column-number="4"/>
					 <fo:table-column column-width="100pt" column-number="5"/>
				      <fo:table-body >
				          <fo:table-row>
				            <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm">
					            <fo:block text-align="left" font-size="10pt" >
					               Additional Detail
					            </fo:block>
					       </fo:table-cell>
					       <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" >					                 
					            </fo:block>
					        </fo:table-cell>
				        </fo:table-row>	
				      </fo:table-body>
				</fo:table>
				<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				</fo:block>			
				<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				</fo:block>
				<fo:table width="680" >
				     <fo:table-column column-width="40pt" column-number="1"/>
					 <fo:table-column column-width="40pt" column-number="2"/>
					 <fo:table-column column-width="40pt" column-number="3"/>
					 <fo:table-column column-width="460pt" column-number="4"/>
					 <fo:table-column column-width="100pt" column-number="5"/>
				      <fo:table-body >
				          <fo:table-row>
				            <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm">
					            <fo:block text-align="left" font-size="10pt" >
					               <fo:table border-width="0.4mm" border-style="solid" height="200">
					                    <fo:table-body>
					                      <fo:table-row>
					                         <fo:table-cell>
					                           <fo:block>	
					                                <fo:external-graphic width="100pt" height="60pt" content-width="50pt" content-height="50pt" src="images/textbox.jpeg"/>                              
					                           </fo:block>
					                         </fo:table-cell>
					                      </fo:table-row>
					                    </fo:table-body>
					               </fo:table>
					            </fo:block>
					       </fo:table-cell>
					       <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" ></fo:block>
					        </fo:table-cell>
				        </fo:table-row>	
				      </fo:table-body>
				</fo:table>
				<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				</fo:block>			
				<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				</fo:block>
				<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				</fo:block>			
				<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				</fo:block>
				<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				</fo:block>			
				<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				</fo:block>
				<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				</fo:block>			
				<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				</fo:block>
				<fo:table width="680pt">
				    <fo:table-column column-width="40pt" column-number="1"/>
					<fo:table-column column-width="600pt" column-number="2"/>					
				    <fo:table-body >
				        <fo:table-row>
				            <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" font-weight="bold">C.</fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm">
					            <fo:block text-align="left" font-size="10pt" font-weight="bold">Infectious Disease Marker Testing Laboratory Reports</fo:block>
					        </fo:table-cell>					        				       
				        </fo:table-row>	
				      </fo:table-body>
				   </fo:table>
				  <fo:block font-family="Helvetica" text-align="center" font-size="14pt">
			      </fo:block>			
				  <fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				  </fo:block>
				  <fo:table width="680pt">
				    <fo:table-column column-width="40pt" column-number="1"/>
					<fo:table-column column-width="40pt" column-number="2"/>	
					<fo:table-column column-width="280pt" column-number="3"/>	
					<fo:table-column column-width="40pt" column-number="4"/>
					<fo:table-column column-width="280pt" column-number="5"/>					
				    <fo:table-body >
				        <fo:table-row>
				            <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" font-weight="bold"></fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" font-weight="bold"></fo:block>
					        </fo:table-cell>
				            <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" font-weight="bold"><fo:external-graphic width="10pt" height="10pt" content-width="10pt" content-height="10pt" src="images/checkbox.jpeg"/>
					            (ATTACHED)Required</fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" font-weight="bold"></fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm">
					            <fo:block text-align="left" font-size="10pt" font-weight="bold"><fo:external-graphic width="10pt" height="10pt" content-width="50pt" content-height="50pt" src="images/checkbox.jpeg"/> 
					            Not Attached:previously sent</fo:block>
					        </fo:table-cell>					        				       
				        </fo:table-row>	
				      </fo:table-body>
				   </fo:table>
				   <fo:block font-family="Helvetica" text-align="center" font-size="14pt">
			      </fo:block>			
				  <fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				  </fo:block>
				  <fo:block font-family="Helvetica" text-align="center" font-size="14pt">
			      </fo:block>			
				  <fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				  </fo:block>
				  <fo:block font-family="Helvetica" text-align="center" font-size="14pt">
			      </fo:block>			
				  <fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				  </fo:block>
				  <fo:block font-family="Helvetica" text-align="center" font-size="14pt">
			      </fo:block>			
				  <fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				  </fo:block>
				  <fo:block font-family="Helvetica" text-align="center" font-size="14pt">
			      </fo:block>			
				  <fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				  </fo:block>
				  <fo:block font-family="Helvetica" text-align="center" font-size="14pt">
			      </fo:block>			
				  <fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				  </fo:block>
				  <fo:block font-family="Helvetica" text-align="center" font-size="14pt">
			      </fo:block>			
				  <fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				  </fo:block>
				  <fo:table width="680pt">
				    <fo:table-column column-width="200pt" column-number="1"/>
					<fo:table-column column-width="40pt" column-number="2"/>	
					<fo:table-column column-width="200pt" column-number="3"/>	
					<fo:table-column column-width="40pt" column-number="4"/>
					<fo:table-column column-width="200pt" column-number="5"/>					
				    <fo:table-body >
				        <fo:table-row>
				            <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" font-weight="bold">-----------------------</fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" font-weight="bold"></fo:block>
					        </fo:table-cell>
				            <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" font-weight="bold">-----------------------</fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" font-weight="bold"></fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm">
					            <fo:block text-align="left" font-size="10pt" font-weight="bold">----------------------</fo:block>
					        </fo:table-cell>					        				       
				        </fo:table-row>	
				      </fo:table-body>
				   </fo:table>
				   <fo:block font-family="Helvetica" text-align="center" font-size="14pt">
			      </fo:block>			
				  <fo:block font-family="Helvetica" text-align="center" font-size="14pt">
				  </fo:block>
				  <fo:table width="680pt">
				    <fo:table-column column-width="200pt" column-number="1"/>
					<fo:table-column column-width="40pt" column-number="2"/>	
					<fo:table-column column-width="200pt" column-number="3"/>	
					<fo:table-column column-width="40pt" column-number="4"/>
					<fo:table-column column-width="200pt" column-number="5"/>					
				    <fo:table-body >
				        <fo:table-row>
				            <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" font-weight="bold">Name of the person completing form</fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" font-weight="bold"></fo:block>
					        </fo:table-cell>
				            <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" font-weight="bold">Signature of the person completing form</fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm" >
					            <fo:block text-align="left" font-size="10pt" font-weight="bold"></fo:block>
					        </fo:table-cell>
					        <fo:table-cell padding="0.0mm">
					            <fo:block text-align="left" font-size="10pt" font-weight="bold">MM---/--DD---/---YY---</fo:block>
					        </fo:table-cell>					        				       
				        </fo:table-row>	
				      </fo:table-body>
				   </fo:table>
			  </fo:flow>
	     </fo:page-sequence>	    
	 </xsl:template>
</xsl:stylesheet>