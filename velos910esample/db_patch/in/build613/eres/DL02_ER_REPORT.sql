set define off;

DELETE FROM ER_REPXSL WHERE FK_REPORT in (161);

commit;

-- INSERTING into ER_REPORT

Insert into ER_REPORT (PK_REPORT,REP_NAME,REP_DESC,REP_SQL,FK_ACCOUNT,FK_CODELST_TYPE,IP_ADD,REP_HIDE,REP_COLUMNS,REP_FILTERBY,GENERATE_XML,REP_TYPE,REP_SQL_CLOB,REP_FILTERKEYWORD,REP_FILTERAPPLICABLE) 
values (162,'Cord Blood Bank Inventory by Age of CBU','Total Member Cord Blood Bank Inventory by Age of CBU* (NCBI and Non-NCBI)','',0,null,null,'N',null,null,1,'rep_cbu','',null,null);
COMMIT;

Update ER_REPORT set REP_SQL = 'select  fk_cbb_id,
(select site_name from er_site where pk_site = fk_cbb_id) as cbb_name,
(select count(*) from cb_cord where fk_cbb_id = a.fk_cbb_id and  cord_cbu_collection_date  between (sysdate - 30*12) and sysdate) as one_yr,
(select count(*) from cb_cord where fk_cbb_id = a.fk_cbb_id and  cord_cbu_collection_date  between (sysdate - 30*24) and (sysdate - 30*12)) as two_yr,
(select count(*) from cb_cord where fk_cbb_id = a.fk_cbb_id and  cord_cbu_collection_date  between (sysdate - 30*36) and (sysdate - 30*24)) as three_yr,
(select count(*) from cb_cord where fk_cbb_id = a.fk_cbb_id and  cord_cbu_collection_date  between (sysdate - 30*48) and (sysdate - 30*36)) as four_yr, 
(select count(*) from cb_cord where fk_cbb_id = a.fk_cbb_id and  cord_cbu_collection_date  between (sysdate - 30*60) and (sysdate - 30*48)) as five_yr,
(select count(*) from cb_cord where fk_cbb_id = a.fk_cbb_id and  cord_cbu_collection_date  between (sysdate - 30*72) and (sysdate - 30*60)) as six_yr,
(select count(*) from cb_cord where fk_cbb_id = a.fk_cbb_id and  cord_cbu_collection_date  < (sysdate - 30*72) ) as seven_yr_plus
from cb_cord a group by fk_cbb_id order by fk_cbb_id' where pk_report = 162;
COMMIT;

Update ER_REPORT set REP_SQL_CLOB = 'select  fk_cbb_id,
(select site_name from er_site where pk_site = fk_cbb_id) as cbb_name,
(select count(*) from cb_cord where fk_cbb_id = a.fk_cbb_id and  cord_cbu_collection_date  between (sysdate - 30*12) and sysdate) as one_yr,
(select count(*) from cb_cord where fk_cbb_id = a.fk_cbb_id and  cord_cbu_collection_date  between (sysdate - 30*24) and (sysdate - 30*12)) as two_yr,
(select count(*) from cb_cord where fk_cbb_id = a.fk_cbb_id and  cord_cbu_collection_date  between (sysdate - 30*36) and (sysdate - 30*24)) as three_yr,
(select count(*) from cb_cord where fk_cbb_id = a.fk_cbb_id and  cord_cbu_collection_date  between (sysdate - 30*48) and (sysdate - 30*36)) as four_yr, 
(select count(*) from cb_cord where fk_cbb_id = a.fk_cbb_id and  cord_cbu_collection_date  between (sysdate - 30*60) and (sysdate - 30*48)) as five_yr,
(select count(*) from cb_cord where fk_cbb_id = a.fk_cbb_id and  cord_cbu_collection_date  between (sysdate - 30*72) and (sysdate - 30*60)) as six_yr,
(select count(*) from cb_cord where fk_cbb_id = a.fk_cbb_id and  cord_cbu_collection_date  < (sysdate - 30*72) ) as seven_yr_plus
from cb_cord a group by fk_cbb_id order by fk_cbb_id' where pk_report = 162;
COMMIT;

