set define off;
declare

begin

  UPDATE ER_MILESTONE SET milestone_isactive = 1,  milestone_limit = ( case when  milestone_limit = 0  then null else  milestone_limit end )    
  where milestone_isactive = 0 and FK_CODELST_MILESTONE_STAT = 
 (select pk_codelst from er_codelst where codelst_type = 'milestone_stat' and
  codelst_subtyp = 'A');
 
 
 
 UPDATE ER_MILESTONE SET milestone_isactive = -1  where milestone_isactive = 0  and FK_CODELST_MILESTONE_STAT = 
 (select pk_codelst from er_codelst where codelst_type = 'milestone_stat' and
 codelst_subtyp = 'IA');
 
 commit;
 end;
 