Set Define Off;

ALTER TABLE  CB_CORD ADD CORD_DONER_IDENTI_NUM VARCHAR2(255);

COMMENT ON COLUMN "CB_CORD"."CORD_DONER_IDENTI_NUM" IS 'Donar Identification number part of product code validation.';

