-- Build 599
=========================================================================================================================
eResearch: 

=========================================================================================================================
Garuda :
	DL02_ER_CODELST_CDR.sql - This SQL contains the CDR/PF specific ER_CODELST entries. These are specific to NMDP.
=========================================================================================================================
eResearch Localization: 

Following Files have been Modified:

1	additempatientback.jsp
2	address.jsp
3	advevent.jsp
4	adveventlookup.jsp
5	ajaxValidation.jsp
6	alertnotify.jsp
7	alertnotifyglobal.jsp
8	alertnotifysettings.jsp
9	allpatientdetails.jsp
10	appendix_get.jsp
11	appendix_getforstudy.jsp
12	appendix_set.jsp
13	assignStudyCopy.jsp
14	assignToTeam.jsp
15	assignuserstogroup.jsp
16	assignuserstogroup_usrsearch.jsp
17	assignuserstoGroup1.jsp
18	bgtnewsection.jsp
19	blockedaccusers.jsp
20	budgetbrowser.jsp
21	budgetbrowserns.jsp
22	budPaymentDetails.jsp
23	calculateGrade.jsp
24	calMileStoneJSON.jsp
25	calMilestoneSetup.jsp
26	calpreview.jsp
27	caslogin.jsp
28	categoryselectus.jsp
29	catLibDelete.jsp
30	changedate.jsp
31	changefieldseq.jsp
32	checksession.jsp
33	child.jsp
34	Clientlogin.jsp
35	contactus.jsp
36	copy_stdy_for_user.jsp
37	copyFormForAccountSubmit.jsp
38	copyFormForStudySubmit.jsp
39	copyvelosmsgs.jsp
40	createCalMileStone.jsp
41	createPatientLogins.jsp
42	createversion.jsp
43	crfformdetails.jsp
44	crfngbrowser.jsp
45	crfnotify.jsp
46	crfnotifybrowser.jsp
47	crfnotifyglobal.jsp
48	crfstatus.jsp
49	crfstatusbrowser.jsp
50	crfstatusbrowserpreschedule.jsp
51	datamonitoring.jsp
52	defaultGrade.jsp
53	deleteEventKit.jsp
54	deleteformnotify.jsp
55	deleteInvoice.jsp
56	deleteLabs.jsp
57	deleteMeetingTopic.jsp
58	deletepatient.jsp
59	deleteschedules.jsp
60	deletestudy.jsp
61	deleteUser.jsp
62	deleventstatus.jsp
63	discdetails.jsp
64	discontinuation.jsp
65	displayDOW.jsp
66	dynadvanced.jsp
67	dynfilter.jsp
68	dynfilterdelete.jsp
69	dynfiltermod.jsp
70	dynorder.jsp
71	dynpreview.jsp
72	dynrep.jsp
73	dynrepcopy.jsp
74	dynrepdelete.jsp
75	dynrepexport.jsp
76	dynreplookup.jsp
77	dynreport.jsp
78	dynreportpat.jsp
79	dynreportstudy.jsp
80	dynrepselect.jsp
81	dynreptype.jsp
82	dynsasexport.jsp
83	dynselectflds.jsp
84	editeventfile.jsp
85	editglobalsetting.jsp
86	editMulEventDetails.jsp
87	editpatstatmilestone.jsp
88	editPerCostDelete.jsp
89	enrolledpatient.jsp
90	ereshelp.jsp
91	errorpage403.jsp
92	errorpage404.jsp
93	EventInfo.jsp
94	eventlibrarycstest.jsp
95	eventrulesave.jsp
96	eventsave.jsp
97	eventstatushistory.jsp
98	eventusersearch.jsp
99	exturights.jsp
100	faqs.jsp
101	fetchCoverageExport.jsp
102	fetchCoverageJSON.jsp
103	fetchMilestoneJSON.jsp
104	fetchProtJSON.jsp
105	fetchSCIJSON.jsp
106	fieldCatDelete.jsp
107	fieldLibDelete.jsp
108	filesave.jsp
109	forgotmail.jsp
110	forgotpassword.jsp
111	forgotpwd.jsp
112	formusersearch.jsp
113	getAdvToxicity.jsp
114	getCodeDropDown.jsp
115	getJSON.jsp
116	getStudySetupCalendars.jsp
117	groupbrowser.jsp
118	groupbrowser_page.jsp
119	groupcreation.jsp
120	grprights.jsp
121	hideunhidecalendar.jsp
122	homepage.jsp
123	homepanelcheck.jsp
124	importReverseFile.jsp
125	insertStudySiteApndxFile.jsp
126	invite.jsp
127	invoicePrint.jsp
128	labdata.jsp
129	labdetails.jsp
130	labmodify.jsp
131	labtextresult.jsp
132	lineitemdelete.jsp
133	linkBudPayment.jsp
134	linkInvPayment.jsp
135	loginpage.jsp
136	loginpatient.jsp
137	logout.jsp
138	managepatients.jsp
139	MarkDone.jsp
140	MarkEventDone.jsp
141	mdynsasexport.jsp
142	mulEventsSave.jsp
143	mulStudyEventsSave.jsp
144	multipleusersave.jsp
145	multipleusersearchdetails1.jsp
146	multiplevisits.jsp
147	multiplevisitssave.jsp
148	nhome.jsp
149	notfound.jsp
150	notification.jsp
151	notificationglobal.jsp
152	patientlogin.jsp
153	patientscheduletracking.jsp
154	patientSearchWorkflow.jsp
155	patienttabs.jsp 
156	patrepfilter.jsp
157	patstudystatusdelete.jsp
158	pattrtarm.jsp
159	pattrtarmsubmit.jsp
160	pattxarmdelete.jsp
161	perfileupdate.jsp
162	personalize.jsp
163	pl.jsp
164	popuplnkfrmstudy.jsp
165	popuplnkfrmstudysave.jsp
166	portalformaccesspg.jsp
167	postFileDownload.jsp
168	pricinginfo.jsp
169	promptMarkEventDone.jsp
170	protLinkedBudget.jsp
171	protLinkedMilestones.jsp
172	protocoleventsave.jsp
173	protocolPopup.jsp
174	protocolTracking.jsp
175	pwdexpired.jsp
176	querymodal.jsp
177	queryReview.jsp
178	readmsg.jsp
179	refreshFormFields.jsp
180	removemsgusers.jsp
181	repall_pat.jsp
182	repGet.jsp
183	repGet_PatSch.jsp
184	repGetHtml.jsp
185	repGetRtf.jsp
186	repGetWord.jsp
187	repmain.jsp
188	repmilegen.jsp
189	repmileretrieve.jsp
190	repmilestone.jsp
191	report.jsp
192	reportselect.jsp
193	repRetrieveInvoice.jsp
194	reptest.jsp
195	researcher.jsp
196	researchorg.jsp
197	sendinvitation.jsp
198	showdoc.jsp
199	studyrepfilter.jsp
200	studyreports.jsp
201	studywizard.jsp
202	viemsgbrowse.jsp
203	viemsglist.jsp
204	viesettings.jsp

=========================================================================================================================