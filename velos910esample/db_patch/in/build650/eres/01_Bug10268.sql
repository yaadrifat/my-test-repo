set define off;

declare
v_lkplibId number := 0;
v_lkpviewId number := 0;
begin
	select PK_LKPLIB into v_lkplibId 
	from er_lkplib where 
	LKPTYPE_NAME='dynReports' AND LKPTYPE_DESC='Patient Study Status' AND LKPTYPE_TYPE='dyn_p';
	
	select PK_LKPVIEW into v_lkpviewId
	from er_lkpview where FK_LKPLIB = 6008 and lkpview_name = 'Patient Study Stat';
	
	update er_lkpview set lkpview_filter = 'fk_account=[:ACCID] and EXISTS (SELECT * FROM ER_USERSITE  usr, ER_PATFACILITY fac 
	WHERE fk_user =[:USERID] AND usersite_right>=4 AND usr.fk_site = fac.fk_site AND fac.patfacility_accessright > 0 AND fac.fk_per = erv_patient_study_status.fk_per)'
	where PK_LKPVIEW = v_lkpviewId;

	v_lkpviewId := 0;
	select PK_LKPVIEW into v_lkpviewId
	from er_lkpview where FK_LKPLIB = 6008 and lkpview_name = 'Patient Study ID';
	
	update er_lkpview set lkpview_filter = 'fk_account=[:ACCID] and upper(CURRENT_STAT) = ''YES'' and EXISTS (SELECT * FROM ER_USERSITE  usr, ER_PATFACILITY fac 
	WHERE fk_user =[:USERID] AND usersite_right>=4 AND usr.fk_site = fac.fk_site AND fac.patfacility_accessright > 0 AND fac.fk_per = erv_patient_study_status.fk_per)'
	where PK_LKPVIEW = v_lkpviewId;

	commit;
end;
/