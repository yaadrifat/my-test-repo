update er_milestone set fk_codelst_milestone_stat =(select pk_codelst from er_codelst where codelst_type ='milestone_stat' and codelst_subtyp ='A') 
where milestone_isactive = 1;


update er_milestone set fk_codelst_milestone_stat =(select pk_codelst from er_codelst where codelst_type ='milestone_stat' and codelst_subtyp ='IA') 
where milestone_isactive = -1;


update er_milestone set fk_codelst_milestone_stat =(select pk_codelst from er_codelst where codelst_type ='milestone_stat' and codelst_subtyp ='WIP') 
where milestone_isactive = 0;


update er_codelst set codelst_study_role ='default_data' where codelst_type ='milestone_stat';

update er_lkpcol set lkpcol_dispval = 'Event Window' where lkpcol_name ='PTSCH_VISIT_WIN' and lkpcol_table='REP_PAT_SCHEDULE' and lkpcol_dispval='Visit Window';

update er_dynrepview set dynrepview_dispname = 'Event Window', dynrepview_colname = 'Event Window' where
dynrepview_col = 'PTSCH_VISIT_WIN' and dynrepview_dispname = 'Visit Window' and dynrepview_colname = 'Visit Window';

update er_lkpcol set lkpcol_dispval = 'Event Window - Before' where lkpcol_name ='LBEVE_VISIT_WIN_B4' AND
lkpcol_table ='REP_LIB_EVENTS' and lkpcol_dispval = 'Visit Window - Before';

update er_lkpcol set lkpcol_dispval = 'Event Window - After' where lkpcol_name ='LBEVE_VISIT_WIN_AFT' AND
lkpcol_table ='REP_LIB_EVENTS' and lkpcol_dispval = 'Visit Window - After';

update er_dynrepview set dynrepview_dispname = 'Event Window - Before', dynrepview_colname = 'Event Window - Before' where dynrepview_col = 'LBEVE_VISIT_WIN_B4' and dynrepview_dispname = 'Visit Window - Before' and dynrepview_colname = 'Visit Window - Before';

update er_dynrepview set dynrepview_dispname = 'Event Window - After', dynrepview_colname = 'Event Window - After' where dynrepview_col = 'LBEVE_VISIT_WIN_AFT' and dynrepview_dispname = 'Visit Window - After' and dynrepview_colname = 'Visit Window - After';


commit;

insert into er_report (
PK_REPORT, REP_NAME, REP_DESC, REP_SQL, FK_ACCOUNT, FK_CODELST_TYPE,
IP_ADD, REP_HIDE, REP_COLUMNS, REP_FILTERBY,GENERATE_XML, REP_TYPE, REP_SQL_CLOB, REP_FILTERKEYWORD,
REP_FILTERAPPLICABLE )values( 160, 'By CT', 'Patient Budget by Coverage Type', null, 0, null, 
null, 'N', null, null, 1, 'pat_bud', null, null, null);

commit;


update er_report set REP_SQL='SELECT budgetsection_sequence, category_seq,BUDGET_NAME,PROT_CALENDAR,BUDGET_VERSION,BUDGET_DESC,STUDY_NUMBER,STUDY_TITLE,SITE_NAME, NVL(COVERAGE_TYPE,''CT NOT specified'') AS COVERAGE_TYPE,
BGTSECTION_NAME,
LINEITEM_NAME,lineitem_seq,
CPT_CODE,
NVL(COST_PER_PATIENT,0) AS COST_PER_PATIENT,
NVL(TOTAL_COST,0) AS TOTAL_COST,
DECODE(LINE_ITEM_INDIRECTS_FLAG,1,''Yes'','''') AS LINE_ITEM_INDIRECTS_FLAG,
DECODE(COST_DISCOUNT_ON_LINE_ITEM,1,''Yes'','''') AS COST_DISCOUNT_ON_LINE_ITEM,
INDIRECTS,
BUDGET_INDIRECT_FLAG,
FRINGE_BENEFIT,
FRINGE_FLAG,
PER_PATIENT_LINE_FRINGE,
TOTAL_LINE_FRINGE,
BUDGET_DISCOUNT,
DECODE(BUDGET_DISCOUNT_FLAG,1,''Discount'',2,''Markup'','''') AS BUDGET_DISCOUNT_FLAG,
TOTAL_COST_AFTER,
TOTAL_PAT_COST_AFTER,
PER_PAT_LINE_ITEM_DISCOUNT,
TOTAL_COST_DISCOUNT,
PERPAT_INDIRECT,
TOTAL_COST_INDIRECT,
BUDGET_CURRENCY,
DECODE(COST_CUSTOMCOL,''research'',''No'',''soc'',''Yes'',''Other'') AS standard_of_care,
LINEITEM_SPONSORAMOUNT AS  SPONSOR_AMOUNT,
LINEITEM_VARIANCE AS  L_VARIANCE,LINEITEM_DIRECT_PERPAT,TOTAL_COST_PER_PAT,TOTAL_COST_ALL_PAT
FROM erv_budget
WHERE pk_budget =  ~1  AND
pk_bgtcal = ~2
ORDER BY COVERAGE_TYPE, budgetsection_sequence,lineitem_seq, lower(LINEITEM_NAME)' 
where pk_report = 160;

commit;

update er_report set REP_SQL_CLOB='SELECT budgetsection_sequence, category_seq,BUDGET_NAME,PROT_CALENDAR,BUDGET_VERSION,BUDGET_DESC,STUDY_NUMBER,STUDY_TITLE,SITE_NAME, NVL(COVERAGE_TYPE,''CT NOT specified'') AS COVERAGE_TYPE,
BGTSECTION_NAME,
LINEITEM_NAME,lineitem_seq,
CPT_CODE,
NVL(COST_PER_PATIENT,0) AS COST_PER_PATIENT,
NVL(TOTAL_COST,0) AS TOTAL_COST,
DECODE(LINE_ITEM_INDIRECTS_FLAG,1,''Yes'','''') AS LINE_ITEM_INDIRECTS_FLAG,
DECODE(COST_DISCOUNT_ON_LINE_ITEM,1,''Yes'','''') AS COST_DISCOUNT_ON_LINE_ITEM,
INDIRECTS,
BUDGET_INDIRECT_FLAG,
FRINGE_BENEFIT,
FRINGE_FLAG,
PER_PATIENT_LINE_FRINGE,
TOTAL_LINE_FRINGE,
BUDGET_DISCOUNT,
DECODE(BUDGET_DISCOUNT_FLAG,1,''Discount'',2,''Markup'','''') AS BUDGET_DISCOUNT_FLAG,
TOTAL_COST_AFTER,
TOTAL_PAT_COST_AFTER,
PER_PAT_LINE_ITEM_DISCOUNT,
TOTAL_COST_DISCOUNT,
PERPAT_INDIRECT,
TOTAL_COST_INDIRECT,
BUDGET_CURRENCY,
DECODE(COST_CUSTOMCOL,''research'',''No'',''soc'',''Yes'',''Other'') AS standard_of_care,
LINEITEM_SPONSORAMOUNT AS  SPONSOR_AMOUNT,
LINEITEM_VARIANCE AS  L_VARIANCE,LINEITEM_DIRECT_PERPAT,TOTAL_COST_PER_PAT,TOTAL_COST_ALL_PAT
FROM erv_budget
WHERE pk_budget =  ~1  AND
pk_bgtcal = ~2
ORDER BY COVERAGE_TYPE, budgetsection_sequence,lineitem_seq, lower(LINEITEM_NAME)' where pk_report = 160;

commit;


