Update ER_REPORT set REP_SQL = 'SELECT
PROT_NAME,
PROT_DESC,
PROT_DURATION,
VISIT_NAME,
VISIT_DESC,
nvl(VISIT_INTERVAL,''No Interval Defined'') as VISIT_INTERVAL,
EVENT_NAME,
EVENT_DESC,
DECODE(trim(vwindow_bef),''(BEFORE)'','''',vwindow_bef) AS vwindow_bef,
DECODE(trim(vwindow_aft),''(AFTER)'','''',vwindow_aft) AS vwindow_aft,
EVENT_COST ,
RESOURCES,
APPENDIX,
MESSAGES,
FORMS,insert_after,decode(nvl(hide_flag,0),1,''Yes'',''No'')
 is_hidden,
 COVERAGE_TYPE
FROM  erv_calendar_template
WHERE prot_id = ~1 and nvl(EVENT_TYPE,''A'') <>''U''
ORDER BY displacement,event_sequence' where pk_report = 106;



Update ER_REPORT set REP_SQL_CLOB = 'SELECT
PROT_NAME,
PROT_DESC,
PROT_DURATION,
VISIT_NAME,
VISIT_DESC,
nvl(VISIT_INTERVAL,''No Interval Defined'') as VISIT_INTERVAL,
EVENT_NAME,
EVENT_DESC,
DECODE(trim(vwindow_bef),''(BEFORE)'','''',vwindow_bef) AS vwindow_bef,
DECODE(trim(vwindow_aft),''(AFTER)'','''',vwindow_aft) AS vwindow_aft,
EVENT_COST ,
RESOURCES,
APPENDIX,
MESSAGES,
FORMS,insert_after,decode(nvl(hide_flag,0),1,''Yes'',''No'')
 is_hidden,
 COVERAGE_TYPE
FROM  erv_calendar_template
WHERE prot_id = ~1 and nvl(EVENT_TYPE,''A'') <>''U''
ORDER BY displacement,event_sequence' where pk_report = 106;




commit;

