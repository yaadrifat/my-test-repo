alter table er_milestone add fk_codelst_milestone_stat number;
comment on column er_milestone.fk_codelst_milestone_stat is 'FK to er_codelst. Stores the code for Milestone status.';
