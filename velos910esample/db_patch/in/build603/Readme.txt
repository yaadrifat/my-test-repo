-- Build 603
=========================================================================================================================
eResearch Localization:

Following Files have been Modified:

1	accountbrowser.jsp
2	accountlinkbrowser.jsp
3	advStudysearchpg.jsp
4	allPatient.jsp
5	appendixbrowser.jsp
6	budget.js
7	budgetapndx.jsp
8	budgetrights.jsp
9	budgetsections.jsp
10	changestudydates.jsp
11	child.jsp
12	copyField.jsp
13	crflibbrowser.jsp
14	dashboardcentral.jsp
15	db_datamanager.jsp
16	db_financial.jsp
17	db_queryMgmt.jsp
18	dynadvanced.jsp
19	dynfilter.jsp
20	dynfilterbrowse.jsp
21	dynfiltermod.jsp
22	dynrepbrowse.jsp
23	editstoragekit.jsp
24	enrollpatient.jsp
25	eventappendix.jsp
26	eventbrowser.jsp
27	eventcost.jsp
28	eventresource.jsp
29	fetchProt.jsp
30	fieldBrowser.jsp
31	formfldbrowser.jsp
32	importStudyForms.jsp
33	invbrowsercommon.jsp
34	invite.jsp
35	irbactionwin.jsp
36	irbProvisos.jsp
37	irbsendback.jsp
38	jqueryUtils.jsp
39	labdata.jsp
40	labelBundle.properties
41	LC.java
42	LC.jsp
43	lookupType.jsp
44	MC.java
45	MC.jsp
46	mdynfilter.jsp
47	meetingBrowser.jsp
48	messageBundle.properties
49	mileapndx.jsp
50	milestone.jsp
51	multipleChoiceSection.jsp
52	newOrganization.jsp
53	notification.jsp
54	patientdetails.jsp
55	patientdetailsquick.jsp
56	patientFacility.jsp
57	perapndx.jsp
58	popupeditor.jsp
59	popuplnkfrmstudy.jsp
60	portal.jsp
61	preparationAreaBrowser.jsp
62	specimenapndx.jsp
63	studybudget.jsp
64	studypatients.jsp
65	studyprotocols.jsp
66	studystatusbrowser.jsp
67	supbudgetrights.jsp
68	ulinkBrowser.jsp
69	updateinvoice.jsp
70	validations.js
71	valueselect.jsp
72	visitlist.jsp

DB Patch : BT01_ER_CTRLTAB_INSERT.sql - This SQL contains insert script for adding copyright year at the login page. 

=========================================================================================================================
Garuda :

	After deploying the velos.ear file successfully.
	
	we can find two jar files ( inside the $JBOSS_HOME/server/eresearch/deploy/velos.ear/velos.war/WEB-INF/lib directory ) 
	named "hibernate-validator.jar" and "hibernate-validator-3.1.0.GA.jar" from which we need to remove one jar file based
	on the Operating System, where the application is deployed.
	
	1.In case of Windows we need to remove "hibernate-validator-3.1.0.GA.jar" and
	2.In case of Unix/Linux we need to remove "hibernate-validator.jar".
	

	DB Patch :1. DL06_CBU_GROUP_RIGHTS.sql - This SQL contains the CBU specific group access rights. These are specific to Garuda Project.
		  2. DL08_ER_CODELST_INSERT.sql - This SQL contains the CDR/PF specific ER_CODELST entries. These are specific to NMDP. 	

=========================================================================================================================

=========================================================================================================================


============================================================================================================================================================================================
Comment on DCMS Server:

	DCMS Application is also installed on 103.3.230.103:8083.
	If any connectivity issue with 83.68.223.157, replace 83.68.223.157 with 103.3.230.103:8083 in "$JBOSS_HOME/server/eresearch/deploy/velos.ear/velos.war/jsp/xml/dcmsAttachment.xml" file.
	Also make sure the IP Address changed is bypassed the Proxy.
	
=============================================================================================================================================================================================