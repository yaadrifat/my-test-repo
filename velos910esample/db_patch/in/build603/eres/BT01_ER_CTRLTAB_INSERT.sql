DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM ER_CTRLTAB WHERE CTRL_KEY = 'copyright';
  
  IF (index_count = 0) THEN
    INSERT INTO ER_CTRLTAB (PK_CTRLTAB,CTRL_VALUE,CTRL_DESC,CTRL_KEY,CREATED_ON,CTRL_SEQ)
    VALUES(SEQ_ER_CTRLTAB.NEXTVAL,'2012','copyright year on login screens','copyright',SYSDATE,1);
    dbms_output.put_line('One row created');
  ELSE
    dbms_output.put_line('CTRL_KEY value "copyright" already exists');
  END IF;
END;
/
COMMIT;
