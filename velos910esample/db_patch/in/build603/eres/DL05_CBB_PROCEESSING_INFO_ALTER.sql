set define off;
--Sql for Table CBB_PROCESSING_PROCEDURES_INFO to add column plasma Rbc Reduction Ind
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO' AND column_name = 'PLA_RBC_RED_IND'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CBB_PROCESSING_PROCEDURES_INFO add PLA_RBC_RED_IND VARCHAR2(1)');
  end if;
end;
/
COMMENT ON COLUMN CBB_PROCESSING_PROCEDURES_INFO.PLA_RBC_RED_IND IS ' This coloumn is for  PLASMA RBC REDUCTION IND.'; 
commit;


--Sql for Table CBB_PROCESSING_PROCEDURES_INFO to add column ProcessingTypeCode

DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO' AND column_name = 'PROC_TYPE_CODE'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CBB_PROCESSING_PROCEDURES_INFO add PROC_TYPE_CODE Number(10)');
  end if;
end;
/
COMMENT ON COLUMN CBB_PROCESSING_PROCEDURES_INFO.PROC_TYPE_CODE IS ' This coloumn is for ProcessingTypeCode.'; 
commit;


--Sql for Table CBB_PROCESSING_PROCEDURES_INFO to add column redCellDeplInd

DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO' AND column_name = 'RED_CEL_DEP_IND'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CBB_PROCESSING_PROCEDURES_INFO add RED_CEL_DEP_IND VARCHAR2(1)');
  end if;
end;
/
COMMENT ON COLUMN CBB_PROCESSING_PROCEDURES_INFO.RED_CEL_DEP_IND IS ' This coloumn is for redCellDeplInd.'; 
commit;


--Sql for Table CBB_PROCESSING_PROCEDURES_INFO to add column supplierBusinessGuid

DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO' AND column_name = 'SUPP_BUSI_GUID'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CBB_PROCESSING_PROCEDURES_INFO add SUPP_BUSI_GUID VARCHAR2(50)');
  end if;
end;
/
COMMENT ON COLUMN CBB_PROCESSING_PROCEDURES_INFO.SUPP_BUSI_GUID IS ' This coloumn is for supplierBusinessGuid.'; 
commit;



--Sql for Table CBB_PROCESSING_PROCEDURES_INFO to add column additivecode

DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO' AND column_name = 'ADDI_CORD'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CBB_PROCESSING_PROCEDURES_INFO add ADDI_CORD NUMBER(10)');
  end if;
end;
/
COMMENT ON COLUMN CBB_PROCESSING_PROCEDURES_INFO.ADDI_CORD IS ' This coloumn is for ADDITIVECORD.'; 
commit;




--Sql for Table CBB_PROCESSING_PROCEDURES_INFO to add column additivedescription

DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO' AND column_name = 'ADDI_DESC'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CBB_PROCESSING_PROCEDURES_INFO add ADDI_DESC VARCHAR2(255 CHAR)');
  end if;
end;
/
COMMENT ON COLUMN CBB_PROCESSING_PROCEDURES_INFO.ADDI_DESC IS ' This coloumn is for ADDITIVEDESCRIPTION.'; 
commit;


--Sql for Table CBB_PROCESSING_PROCEDURES_INFO to add column additive volume

DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO' AND column_name = 'ADDI_VOL'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CBB_PROCESSING_PROCEDURES_INFO add ADDI_VOL Number(10,2)');
  end if;
end;
/
COMMENT ON COLUMN CBB_PROCESSING_PROCEDURES_INFO.ADDI_VOL IS ' This coloumn is for ADDITIVE VOLUME.'; 
commit;

--Sql for Table CBB_PROCESSING_PROCEDURES_INFO to add column OTHER_BAG_TYPE
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO' AND column_name = 'OTHER_BAG_TYPE';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CBB_PROCESSING_PROCEDURES_INFO ADD(OTHER_BAG_TYPE VARCHAR2(30 CHAR))';
  end if;
end;
/
--END--

COMMENT ON COLUMN ERES.CBB_PROCESSING_PROCEDURES_INFO.OTHER_BAG_TYPE IS 'Stores other bag type description';
commit;
