set define off;

Update ER_REPORT set 
REP_SQL = 'select pk_formquery, pk_patforms, pk_patprot, FK_FORMLIBVER, pk_study, study_number, fk_site,
(select site_name from er_site where pk_site = fk_site) as site_name,fk_per, FK_CODELST_QUERYSTATUS, PATSTDID,
EVENT_ID, event_name, VISIT_NAME,
formcount, fk_form, form_name, fld_name, fmod
from er_study,
(SELECT distinct pk_formquery, pk_patforms, pk_patprot, FK_FORMLIBVER, g.fk_study, f.fk_per, 
FK_CODELST_QUERYSTATUS, PATPROT_PATSTDID as PATSTDID, FK_SITE_ENROLLING as fk_site,
(select j.EVENT_ID from esch.sch_events1 h, esch.event_assoc j
where j.event_id = h.fk_assoc AND f.FK_SCH_EVENTS1 = f_to_number(h.EVENT_ID) AND g.pk_patprot = h.fk_patprot AND h.status = 0) AS EVENT_ID,
(select j.NAME from esch.sch_events1 h, esch.event_assoc j
where j.event_id = h.fk_assoc AND f.FK_SCH_EVENTS1 = f_to_number(h.EVENT_ID) AND g.pk_patprot = h.fk_patprot AND h.status = 0) AS event_name,
(select VISIT_NO from esch.sch_events1 h, esch.event_assoc j, esch.SCH_PROTOCOL_VISIT k 
where j.event_id = h.fk_assoc AND f.FK_SCH_EVENTS1 = f_to_number(h.EVENT_ID) 
AND h.fk_visit = k.PK_PROTOCOL_VISIT AND j.fk_visit = k.PK_PROTOCOL_VISIT) AS VISIT_NO, 
(select VISIT_NAME from esch.sch_events1 h, esch.event_assoc j, esch.SCH_PROTOCOL_VISIT k 
where j.event_id = h.fk_assoc AND f.FK_SCH_EVENTS1 = f_to_number(h.EVENT_ID) 
AND h.fk_visit = k.PK_PROTOCOL_VISIT AND j.fk_visit = k.PK_PROTOCOL_VISIT) AS VISIT_NAME, 
(select count(*) from ER_PATFORMS pf where pk_formlib = pf.fk_formlib AND pf.fk_per = f.fk_per) formcount,
pk_formlib as fk_form, form_name, 
(select fld_name from er_fldlib where pk_field = c.fk_field) as fld_name,
case 
  when (f.CREATED_ON is not null and f.LAST_MODIFIED_DATE is null) then f.CREATED_ON
  else f.LAST_MODIFIED_DATE
end As fmod
FROM ER_FORMQUERY a, ER_FORMQUERYSTATUS b, ER_FORMFLD c, ER_FORMSEC d, ER_FORMLIB e, ER_PATFORMS f, ER_PATPROT g
WHERE b.fk_formquery = a.pk_formquery AND a.FK_QUERYMODULE = pk_patforms
AND a.fk_field = c.fk_field AND d.pk_formsec = c.fk_formsec 
AND e.pk_formlib = d.fk_formlib AND pk_formlib = f.fk_formlib
AND e.pk_formlib NOT IN (SELECT lf.fk_formlib FROM er_linkedforms lf WHERE e.pk_formlib = lf.fk_formlib AND (lf.RECORD_TYPE = ''D'' OR LF_HIDE = 1))
AND e.pk_formlib NOT IN (SELECT LF.FK_FORMLIB FROM ER_SETTINGS, ER_LINKEDFORMS lf WHERE
  SETTINGS_KEYWORD = ''FORM_HIDE'' AND SETTINGS_MODNAME = 3
  AND SETTINGS_MODNUM = g.fk_study AND SETTINGS_VALUE = lf.pk_lf )
AND g.pk_patprot = f.fk_patprot AND f.fk_per = g.fk_per 
AND f.fk_per in (:patientId) 
AND b.pk_formquerystatus = (SELECT max(x.pk_formquerystatus) FROM ER_FORMQUERYSTATUS x WHERE x.fk_formquery = a.pk_formquery AND x.fk_codelst_querystatus = b.fk_codelst_querystatus 
AND trunc(b.entered_on) = (SELECT max(trunc(y.entered_on)) FROM ER_FORMQUERYSTATUS y WHERE y.fk_formquery = a.pk_formquery AND y.fk_codelst_querystatus = b.fk_codelst_querystatus)) AND f.RECORD_TYPE <> ''D''
AND (Pkg_FormQuery.F_Is_FQueryCreator(pk_formquery, :studyId, a.Creator,'':fqCreatorId'') = 1)
) aa where pk_study = aa.fk_study 
AND pk_study in (:studyId)
AND (pkg_util.f_getStudyRight(pkg_studystat.F_GET_USERRIGHTS_FOR_STUDY(:sessUserId,:studyId),(select CTRL_SEQ from er_ctrltab where CTRL_KEY = ''study_rights'' and upper(ctrl_value) = ''STUDYMPAT'')) > 0)
AND FK_PER > 0 AND (0 < pkg_user.f_chk_studyright_using_pat(FK_PER,FK_STUDY,:sessUserId))
AND FK_CODELST_QUERYSTATUS= :statId
AND fmod between TO_DATE('':fromDate'', pkg_dateUtil.f_get_dateformat) and TO_DATE('':toDate'', pkg_dateUtil.f_get_dateformat)
order by study_number, PATSTDID, form_name, fld_name'  where pk_report = 156;

COMMIT;

Update ER_REPORT set REP_SQL_CLOB = 'select pk_formquery, pk_patforms, pk_patprot, FK_FORMLIBVER, pk_study, study_number, fk_site,
(select site_name from er_site where pk_site = fk_site) as site_name,fk_per, FK_CODELST_QUERYSTATUS, PATSTDID,
EVENT_ID, event_name, VISIT_NAME,
formcount, fk_form, form_name, fld_name, fmod
from er_study,
(SELECT distinct pk_formquery, pk_patforms, pk_patprot, FK_FORMLIBVER, g.fk_study, f.fk_per, 
FK_CODELST_QUERYSTATUS, PATPROT_PATSTDID as PATSTDID, FK_SITE_ENROLLING as fk_site,
(select j.EVENT_ID from esch.sch_events1 h, esch.event_assoc j
where j.event_id = h.fk_assoc AND f.FK_SCH_EVENTS1 = f_to_number(h.EVENT_ID) AND g.pk_patprot = h.fk_patprot AND h.status = 0) AS EVENT_ID,
(select j.NAME from esch.sch_events1 h, esch.event_assoc j
where j.event_id = h.fk_assoc AND f.FK_SCH_EVENTS1 = f_to_number(h.EVENT_ID) AND g.pk_patprot = h.fk_patprot AND h.status = 0) AS event_name,
(select VISIT_NO from esch.sch_events1 h, esch.event_assoc j, esch.SCH_PROTOCOL_VISIT k 
where j.event_id = h.fk_assoc AND f.FK_SCH_EVENTS1 = f_to_number(h.EVENT_ID) 
AND h.fk_visit = k.PK_PROTOCOL_VISIT AND j.fk_visit = k.PK_PROTOCOL_VISIT) AS VISIT_NO, 
(select VISIT_NAME from esch.sch_events1 h, esch.event_assoc j, esch.SCH_PROTOCOL_VISIT k 
where j.event_id = h.fk_assoc AND f.FK_SCH_EVENTS1 = f_to_number(h.EVENT_ID) 
AND h.fk_visit = k.PK_PROTOCOL_VISIT AND j.fk_visit = k.PK_PROTOCOL_VISIT) AS VISIT_NAME, 
(select count(*) from ER_PATFORMS pf where pk_formlib = pf.fk_formlib AND pf.fk_per = f.fk_per) formcount,
pk_formlib as fk_form, form_name, 
(select fld_name from er_fldlib where pk_field = c.fk_field) as fld_name,
case 
  when (f.CREATED_ON is not null and f.LAST_MODIFIED_DATE is null) then f.CREATED_ON
  else f.LAST_MODIFIED_DATE
end As fmod
FROM ER_FORMQUERY a, ER_FORMQUERYSTATUS b, ER_FORMFLD c, ER_FORMSEC d, ER_FORMLIB e, ER_PATFORMS f, ER_PATPROT g
WHERE b.fk_formquery = a.pk_formquery AND a.FK_QUERYMODULE = pk_patforms
AND a.fk_field = c.fk_field AND d.pk_formsec = c.fk_formsec 
AND e.pk_formlib = d.fk_formlib AND pk_formlib = f.fk_formlib
AND e.pk_formlib NOT IN (SELECT lf.fk_formlib FROM er_linkedforms lf WHERE e.pk_formlib = lf.fk_formlib AND (lf.RECORD_TYPE = ''D'' OR LF_HIDE = 1))
AND e.pk_formlib NOT IN (SELECT LF.FK_FORMLIB FROM ER_SETTINGS, ER_LINKEDFORMS lf WHERE
  SETTINGS_KEYWORD = ''FORM_HIDE'' AND SETTINGS_MODNAME = 3
  AND SETTINGS_MODNUM = g.fk_study AND SETTINGS_VALUE = lf.pk_lf )
AND g.pk_patprot = f.fk_patprot AND f.fk_per = g.fk_per 
AND f.fk_per in (:patientId) 
AND b.pk_formquerystatus = (SELECT max(x.pk_formquerystatus) FROM ER_FORMQUERYSTATUS x WHERE x.fk_formquery = a.pk_formquery AND x.fk_codelst_querystatus = b.fk_codelst_querystatus 
AND trunc(b.entered_on) = (SELECT max(trunc(y.entered_on)) FROM ER_FORMQUERYSTATUS y WHERE y.fk_formquery = a.pk_formquery AND y.fk_codelst_querystatus = b.fk_codelst_querystatus)) AND f.RECORD_TYPE <> ''D''
AND (Pkg_FormQuery.F_Is_FQueryCreator(pk_formquery, :studyId, a.Creator,'':fqCreatorId'') = 1)
) aa where pk_study = aa.fk_study 
AND pk_study in (:studyId)
AND (pkg_util.f_getStudyRight(pkg_studystat.F_GET_USERRIGHTS_FOR_STUDY(:sessUserId,:studyId),(select CTRL_SEQ from er_ctrltab where CTRL_KEY = ''study_rights'' and upper(ctrl_value) = ''STUDYMPAT'')) > 0)
AND FK_PER > 0 AND (0 < pkg_user.f_chk_studyright_using_pat(FK_PER,FK_STUDY,:sessUserId))
AND FK_CODELST_QUERYSTATUS= :statId
AND fmod between TO_DATE('':fromDate'', pkg_dateUtil.f_get_dateformat) and TO_DATE('':toDate'', pkg_dateUtil.f_get_dateformat)
order by study_number, PATSTDID, form_name, fld_name'  where pk_report = 156;

COMMIT;