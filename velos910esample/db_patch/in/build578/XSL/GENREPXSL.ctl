LOAD DATA
INFILE *
INTO TABLE er_repxsl
APPEND
FIELDS TERMINATED BY ','
(PK_REPXSL,
 FK_REPORT,
 FK_ACCOUNT,
 REPXSL_NAME,
 xsl_file filler char,
"REPXSL" LOBFILE (xsl_file) TERMINATED BY EOF NULLIF XSL_FILE = 'NONE',
 xsl_file2 filler char,
"REPXSL_XSL" LOBFILE (xsl_file2) TERMINATED BY EOF NULLIF XSL_FILE2 = 'NONE'
)
BEGINDATA
108,108,0,Patient Budget by Category,108.xsl,108.xsl
109,109,0,Patient Budget by Standard of Care,109.xsl,109.xsl
124,124,0,Patient Budget Overview,124.xsl,124.xsl
160,160,0,Patient Budget By Coverage Type,160.xsl,160.xsl
