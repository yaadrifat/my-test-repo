ALTER TABLE er_user
ADD USR_CURRNTSESSID VARCHAR(50);

commit;

COMMENT ON COLUMN er_user.USR_CURRNTSESSID 
   IS 'This column stores the Current Logged In Session ID';
   
commit; 
