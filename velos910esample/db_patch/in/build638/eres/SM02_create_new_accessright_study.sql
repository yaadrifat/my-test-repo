set define off

declare

/*******************
    Instrcutions: 
    
    This script is an example of how to add a new access study team right. 
	
	This example is a new study team right: 'Financial Details'.
	
    Execute this script in the ERES schema.
    
    1. initialize v_newacc_va  - with the ctrl_value for the new access right to be available in study rights
    
    2. initialize v_newaccdesc_va  - with the ctrl_desc for the new access right to be available in study rights
	
	3. initialize v_newaccseq_va - with the sequence of the new access right
	
	4. initialize v_newaccdefault_va - default access given to existing team for the new right

  *****************/

  v_count number ;

	v_change_made boolean :=false;
	
  type newacc_va is varray(50) of VARCHAR2(150);
  type newacc_vanum is varray(50) of Number;

  v_newacc_va newacc_va;
  v_newaccdesc_va newacc_va;
 v_newaccdefault_va newacc_va;

  v_newaccseq_va newacc_vanum;

  v_null varchar2(10) := null ;

  

  v_studyacc_seq number;
  v_studyacc_totalseq number;
  
  v_rights_str varchar2(200);

   
  v_exist_acc_count number;

  pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'ACCESS_SCR', pLEVEL  => Plog.LDEBUG);

begin

     

    -- ctrl_value for new access right

    v_newacc_va :=newacc_va('STUDYFIN');


    -- ctrl_desc for new access right

    
    v_newaccdesc_va := newacc_va('Financial Details');

    --sequence within the rights page
	
	v_newaccseq_va:= newacc_vanum(13);
	
    v_newaccdefault_va := newacc_va('7');

    --check for existence of the first access right
    
    select count(*) into v_exist_acc_count from er_ctrltab 
    where CTRL_KEY = 'study_rights' and 
      CTRL_VALUE = v_newacc_va(1) ;

    plog.debug(pctx,'v_exist_acc_count:' || v_exist_acc_count);

    plog.debug(pctx,'v_newacc_va.first:' || v_newacc_va(1) );

    
    if v_exist_acc_count = 0 then
        
        select max(CTRL_SEQ)
		 into v_studyacc_totalseq
		 from er_ctrltab   
		 where CTRL_KEY = 'study_rights' ;


         for k in v_newacc_va.first..v_newacc_va.last
             loop
			 
			
				v_studyacc_seq := v_newaccseq_va(k);
			
				plog.debug(pctx,'v_newacc_va.kt:' || v_newacc_va(k) );

					 
				-- insert group rights
				INSERT INTO ER_CTRLTAB ( PK_CTRLTAB, CTRL_VALUE, CTRL_DESC, CTRL_KEY, CTRL_SEQ )
				VALUES ( seq_er_ctrltab.nextval, v_newacc_va(k), v_newaccdesc_va(k), 'study_rights', v_studyacc_seq );
				
				
					--right is inserted at the sequence of choice and move the rest to right
					
				UPDATE ER_STUDYTEAM
				SET study_team_rights = substr(study_team_rights,1,(v_studyacc_seq-1)) || v_newaccdefault_va(k) || substr(study_team_rights,v_studyacc_seq)
				where rid is not null;
				
				UPDATE ER_GRPS
				set grp_supusr_rights = substr(grp_supusr_rights,1,(v_studyacc_seq-1)) || v_newaccdefault_va(k) || substr(grp_supusr_rights,v_studyacc_seq)
				where rid is not null and grp_supusr_rights is not null;

					-- fix the default set for all role types
						for rl in (select codelst_subtyp from er_codelst where codelst_type='role')
						 loop
								
								update er_ctrltab
								set ctrl_value = substr(ctrl_value,1,(v_studyacc_seq-1)) || v_newaccdefault_va(k) || substr(ctrl_value,v_studyacc_seq)
								where ctrl_key = rl.codelst_subtyp;
									
						 end loop;
						 
             end loop;



        
        v_change_made := true;

    end if; -- if study rights are not setup

     if v_change_made  then

        COMMIT;
    end if;
   


end;
/

