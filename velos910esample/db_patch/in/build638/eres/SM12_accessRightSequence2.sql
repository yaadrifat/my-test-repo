set define off;

declare
v_finRows number := 0;
begin

	select count(*) into v_finRows from er_ctrltab where CTRL_KEY = 'study_rights' and ctrl_seq = 13;
	
	if (v_finRows > 1) then
		Update er_ctrltab set ctrl_seq = ctrl_seq + 1 where CTRL_KEY = 'study_rights' and ctrl_seq >= 13 and ctrl_value != 'STUDYFIN';
		dbms_output.put_line(v_finRows || ' rows updated');
		
		commit;
	else
		dbms_output.put_line('No rows updated');
	end if;
end;
/
