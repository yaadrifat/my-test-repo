SET DEFINE OFF;

DELETE from ER_REPXSL where FK_REPORT = 175; 

DELETE from ER_REPORT where PK_REPORT = 175; 

Insert into ERES.ER_REPORT
   (PK_REPORT, REP_NAME, REP_DESC, REP_SQL, FK_ACCOUNT, 
    FK_CODELST_TYPE, IP_ADD, REP_HIDE, REP_COLUMNS, REP_FILTERBY, 
    GENERATE_XML, REP_TYPE, REP_SQL_CLOB, REP_FILTERKEYWORD, REP_FILTERAPPLICABLE)
 Values
   (175, 'Budget Change Log', 'Budget Audit Trail Report', 'Select ESCH.PKG_AUDIT_TRAIL_MODULE.RPT_BUDGETAUDITREPORT(~1,~2,~3,''~4'',''~5'',''~6'',''~7'',''~8'',''~9'') from dual', 0, 
    NULL, NULL, 'N', NULL, NULL, 
    0, 'pat_bud', 'Select ESCH.PKG_AUDIT_TRAIL_MODULE.RPT_BUDGETAUDITREPORT(~1,~2,~3,''~4'',''~5'',''~6'',''~7'',''~8'',''~9'') from dual', NULL, 'date');
	
COMMIT;
/
