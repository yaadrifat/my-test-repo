set define off;

declare
v_table_exists number default 0;
begin
	Select count(*) into v_table_exists
    from USER_TABLES
    where UPPER(TABLE_NAME) = 'AUDIT_ERROR_LOG'; 
	
	if (v_table_exists > 0) then
		DELETE FROM "ESCH"."AUDIT_ERROR_LOG";
		COMMIT;
		
		execute immediate 'DROP TABLE "ESCH"."AUDIT_ERROR_LOG" ';
	end if;	
end;
/

CREATE TABLE ESCH.AUDIT_ERROR_LOG (
PK_AUDIT_ERROR_LOG   NUMBER(10) NOT NULL,
MODULE_NAME          VARCHAR2(100),
ERROR_TYPE           VARCHAR2(20),
ERROR_SOURCE         VARCHAR2(200),
ERROR_MESSAGE        VARCHAR2(4000),
ERROR_SQL            VARCHAR2(4000),
ERROR_DESCRIPTION    VARCHAR2(4000),
STATUS_FLAG          NUMBER(10) DEFAULT 0,
LAST_MODIFIED_DATE   DATE DEFAULT SYSDATE,
PRIMARY KEY ("PK_AUDIT_ERROR_LOG")
);

declare
v_seq_exists number default 0;
begin
	Select count(*) into v_seq_exists
    from USER_SEQUENCES
    where UPPER(SEQUENCE_NAME) = 'SEQ_AUDIT_ERROR_LOG'; 
	
	if (v_seq_exists = 0) then
		--Sequence SEQ_AUDIT_ERROR_LOG for PK_AUDIT_ERROR_LOG column of AUDIT_ERROR_LOG Table 	
		execute immediate 'CREATE SEQUENCE "ESCH"."SEQ_AUDIT_ERROR_LOG"
            MINVALUE 1 
            MAXVALUE 999999999999999999999999999 
            INCREMENT BY 1 
            START WITH 1
            NOCACHE  ORDER  NOCYCLE ';
	end if;	
end;
/

