set define off;

DECLARE
  v_column_exists number := 0;  
BEGIN
	Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'SCH_CODELST'
    AND COLUMN_NAME = 'CODELST_KIND'; 
	
	if (v_column_exists = 0) then
		execute immediate 'ALTER TABLE ESCH.SCH_CODELST ADD (CODELST_KIND VARCHAR2(4000))';
		dbms_output.put_line('Column CODELST_KIND is added to SCH_CODELST');
	else
		dbms_output.put_line('Column CODELST_KIND already exists in SCH_CODELST');
	end if;
END;
/

COMMENT ON COLUMN SCH_CODELST.CODELST_KIND IS 'Identifies Code-list item kind';
