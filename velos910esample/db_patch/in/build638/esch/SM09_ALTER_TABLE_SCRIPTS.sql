--Alter Table Script   

set define off;

DECLARE
  v_column_exists number;  
BEGIN
	Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'AUDIT_ROW_MODULE'
    AND COLUMN_NAME = 'CHANGE_LOCATION'; 
	
	if (v_column_exists = 1) then
		execute immediate 'ALTER TABLE ESCH.AUDIT_ROW_MODULE RENAME COLUMN CHANGE_LOCATION TO TABLE_DISPLAY_NAME';
		dbms_output.put_line('Column CHANGE_LOCATION is renames to TABLE_DISPLAY_NAME');
	else
		dbms_output.put_line('Column CHANGE_LOCATION is not found in exists in AUDIT_ROW_MODULE');
	end if;
END;
/


ALTER TABLE ESCH.AUDIT_ROW_MODULE MODIFY(TABLE_DISPLAY_NAME VARCHAR2(200 BYTE));

set define off;

DECLARE
  v_column_exists number;  
BEGIN
	Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'AUDIT_COLUMN_MODULE'
    AND COLUMN_NAME = 'COLUMN_DISPLAY_NAME'; 
	
	if (v_column_exists < 1) then
		execute immediate 'ALTER TABLE ESCH.AUDIT_COLUMN_MODULE ADD (COLUMN_DISPLAY_NAME  VARCHAR2(200 BYTE))';
		dbms_output.put_line('Column COLUMN_DISPLAY_NAME is added to AUDIT_COLUMN_MODULE');
	else
		dbms_output.put_line('Column COLUMN_DISPLAY_NAME already exists in AUDIT_COLUMN_MODULE');
	end if;
END;
/

ALTER TABLE ESCH.AUDIT_MAPPEDVALUES ADD ( CONSTRAINT UNIQUE_FROM_VALUE UNIQUE (FROM_VALUE));

