declare last_value number:=0;
begin 
  Select max(PK_ENTITY_STATUS) into last_value from cb_entity_status;
  execute immediate 'Alter sequence SEQ_CB_ENTITY_STATUS increment by '||last_value;
  Select SEQ_CB_ENTITY_STATUS.nextval into last_value from dual;
  execute immediate 'Alter sequence SEQ_CB_ENTITY_STATUS increment by 1';
end ;

/

declare last_value number:=0;
begin 
  Select max(PK_ENTITY_STATUS_REASON) into last_value from cb_entity_status_reason;
  execute immediate 'Alter sequence SEQ_CB_ENTITY_STATUS_REASON increment by '||last_value;
  Select SEQ_CB_ENTITY_STATUS_REASON.nextval into last_value from dual;
  execute immediate 'Alter sequence SEQ_CB_ENTITY_STATUS_REASON increment by 1';
end ;
/