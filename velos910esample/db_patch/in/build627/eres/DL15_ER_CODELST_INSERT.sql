--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert_type'
    AND codelst_subtyp = 'mail';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE) 
  VALUES(SEQ_ER_CODELST.nextval,'alert_type','mail','E MAIL','N',NULL,SYSDATE,SYSDATE);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert_type'
    AND codelst_subtyp = 'notify';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE) 
  VALUES(SEQ_ER_CODELST.nextval,'alert_type','notify','NOTIFY','N',NULL,SYSDATE,SYSDATE);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert_type'
    AND codelst_subtyp = 'prompt';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE) 
  VALUES(SEQ_ER_CODELST.nextval,'alert_type','prompt','PROMPT','N',NULL,SYSDATE,SYSDATE);
	commit;
  end if;
end;
/
--END--

 
 --STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'test_reason'
    AND codelst_subtyp = 'relTest';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE) 
  VALUES(SEQ_ER_CODELST.nextval,'test_reason','relTest','Release Testing','N',NULL,SYSDATE,SYSDATE);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'samp_type'
    AND codelst_subtyp = 'segment';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_DATE) 
  VALUES(SEQ_ER_CODELST.nextval,'samp_type','segment','Segment','N','1',NULL,SYSDATE,SYSDATE);
	commit;
  end if;
end;
/
--END--



 --STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'samp_type'
    AND codelst_subtyp = 'filtPap';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_DATE) 
  VALUES(SEQ_ER_CODELST.nextval,'samp_type','filtPap','Filter Paper','N','2',NULL,SYSDATE,SYSDATE);
	commit;
  end if;
end;
/
--END--


 --STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'samp_type'
    AND codelst_subtyp = 'RBC';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_DATE) 
  VALUES(SEQ_ER_CODELST.nextval,'samp_type','RBC','RBC Pellet','N','3',NULL,SYSDATE,SYSDATE);
	commit;
  end if;
end;
/
--END--


 --STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'samp_type'
    AND codelst_subtyp = 'extDna';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_DATE) 
  VALUES(SEQ_ER_CODELST.nextval,'samp_type','extDna','Extracted DNA Aliquot','N','4',NULL,SYSDATE,SYSDATE);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'samp_type'
    AND codelst_subtyp = 'nonViab';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_DATE) 
  VALUES(SEQ_ER_CODELST.nextval,'samp_type','nonViab','Non-Viable Cell Aliquot','N','5',NULL,SYSDATE,SYSDATE);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'samp_type'
    AND codelst_subtyp = 'plasAli';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_DATE) 
  VALUES(SEQ_ER_CODELST.nextval,'samp_type','plasAli','Plasma Aliquot','N','6',NULL,SYSDATE,SYSDATE);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'samp_type'
    AND codelst_subtyp = 'serAli';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_DATE) 
  VALUES(SEQ_ER_CODELST.nextval,'samp_type','serAli','Serum Aliquot','N','7',NULL,SYSDATE,SYSDATE);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'samp_type'
    AND codelst_subtyp = 'viabCel';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_DATE) 
  VALUES(SEQ_ER_CODELST.nextval,'samp_type','viabCel','Viable Cell Aliquot','N','8',NULL,SYSDATE,SYSDATE);
	commit;
  end if;
end;
/
--END--



